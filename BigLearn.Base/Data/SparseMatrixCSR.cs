using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class SparseMatrixCSR : MatrixData
    // {
    //     public override DataSourceID Type { get { return DataSourceID.SparseMatrixCSR; } }

    //     public CudaPieceInt SampleIdx;
    //     public CudaPieceInt FeatureIdx;
    //     public CudaPieceFloat FeatureValue { get { return Output; } }

    //     public override int Row
    //     {
    //         get { return SampleIdx.EffectiveSize - 1; }
    //         set
    //         {
    //             if (value > MaxRow) throw new Exception(string.Format("Row {0} should be smaller than Max Row {1}", value, MaxRow));
    //             SampleIdx.EffectiveSize = value + 1;
    //         }
    //     }
        
    //     public override int Length
    //     {
    //         get
    //         {
    //             return base.Length;
    //         }
    //         set
    //         {
    //             base.Length = value;
    //             FeatureValue.EffectiveSize = value;
    //             FeatureIdx.EffectiveSize = value;
    //         }
    //     }

    //     public SparseMatrixCSR(int column, int maxRow, int maxLength, DeviceType device) 
    //             : this(column, maxRow, maxLength, true, device)
    //     { }


    //     public SparseMatrixCSR(int column, int maxRow, int maxLength, bool isDeriv, DeviceType device)  
    //                             : this(column, maxRow, maxLength,
    //                             new CudaPieceInt(maxRow + 1, device), 
    //                             new CudaPieceInt(maxLength, device), 
    //                             new CudaPieceFloat(maxLength, device), 
    //                             isDeriv ? new CudaPieceFloat(maxLength, device) : CudaPieceFloat.Empty, device)
    //     { }

    //     public SparseMatrixCSR(int column, int maxRow, int maxLength, CudaPieceInt smpIdx, CudaPieceInt feaIdx, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) 
    //                 : base(column, maxRow, maxLength, data, deriv, device)
    //     {
    //         SampleIdx = smpIdx;
    //         FeatureIdx = feaIdx;
    //     }
    // }
}
