﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // it is equals to GeneralBatchInputData. 
    public class SparseMatrixData : BatchData
    {
        public override DataSourceID Type { get { return DataSourceID.SparseMatrixData; } }

        public CudaPieceInt SampleIdx;
        public CudaPieceInt FeatureIdx;
        public CudaPieceFloat FeatureValue;
        public CudaPieceFloat FeatureDeriv;
        public int Column { get { return Shape[0].Default; } set { Shape[0].Default = value; } }
        public int MaxRow { get { return Shape[1].Default; } set { Shape[1].Default = value; } }
        public int MaxLength { get { return Shape[2].Default; } set { Shape[2].Default = value; } }

        public IntArgument ShapeRow { get { return Shape[1]; } }

        public int Row
        {
            get { return SampleIdx.EffectiveSize; }
            set
            {
                //if (value > MaxRow) throw new Exception(string.Format("Row {0} should be smaller than Max Row {1}", value, MaxRow));
                SampleIdx.EffectiveSize = value;
                ShapeRow.Value = value;
            }
        }
        
        public int Length
        {
            get
            {
                return FeatureIdx.EffectiveSize;
            }
            set
            {
                FeatureValue.EffectiveSize = value;
                if(FeatureDeriv != null && !FeatureDeriv.IsEmpty) FeatureDeriv.EffectiveSize = value;
                FeatureIdx.EffectiveSize = value;
                Shape[2].Value = value;
            }
        }

        public SparseMatrixData(int column, int maxRow, int maxLength, DeviceType device, bool isDeriv = true) : this(column, maxRow, maxLength,
            new CudaPieceInt(maxRow, device), 
            new CudaPieceInt(maxLength, device), 
            new CudaPieceFloat(maxLength, device), 
            isDeriv ? new CudaPieceFloat(maxLength, device) : CudaPieceFloat.Empty, device)
        { }

        public SparseMatrixData(int column, int maxRow, int maxLength, CudaPieceInt smpIdx, CudaPieceInt feaIdx, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) 
            : base(device, new IntArgument("DIM", column), new IntArgument("MAX_ROW", maxRow), new IntArgument("MAX_LENGTH", maxLength))
        {
            SampleIdx = smpIdx;
            FeatureIdx = feaIdx;
            FeatureValue = data;
            FeatureDeriv = deriv;

            Column = column;
            MaxRow = maxRow;
            MaxLength = maxLength;
            //ShapeRow = new IntArgument("ShapeRow", MaxRow);
        }

        public override void SyncFromCPU()
        {
            SampleIdx.SyncFromCPU();
            FeatureIdx.SyncFromCPU();
            FeatureValue.SyncFromCPU();
        }

        public override void SyncToCPU()
        {
            SampleIdx.SyncToCPU();
            FeatureIdx.SyncToCPU();
            FeatureValue.SyncToCPU();
        }
    }
}
