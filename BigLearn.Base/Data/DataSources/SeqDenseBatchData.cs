﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Dense Feature Sequence Input Data.
    /// </summary>
    public class SeqDenseBatchData : BatchData //<SequenceDataStat>
    {
        public int MAX_BATCHSIZE { get { return Shape[1].Default; } set { Shape[1].Default = value; } }
        public int MAX_SENTSIZE { get { return Shape[2].Default; } set { Shape[2].Default = value; } }
        //public int MAX_SEQLENGTH { get { return Shape[3].Default; } set { Shape[3].Default = value; } }

        public int Dim { get { return Shape[0].Default; } set { Shape[0].Default = value; } }

        public IntArgument ShapeBatchSize;
        public IntArgument ShapeSentSize;
        public IntArgument ShapeDim;

        public virtual int BatchSize
        {
            get
            {
                return SampleIdx.EffectiveSize;
            }
            set
            {
                SampleIdx.EffectiveSize = value;
                ShapeBatchSize.Value = value;
                if (MAX_BATCHSIZE < value) MAX_BATCHSIZE = value;
                Shape[1].Value = value;
            }
        }

        public virtual int SentSize
        {
            get
            {
                return SentOutput.EffectiveSize / Dim;
            }
            set
            {
                ShapeSentSize.Value = value;
                SentOutput.EffectiveSize = value * Dim;
                if(SentDeriv != null) SentDeriv.EffectiveSize = value * Dim;
                if(SentMargin != null) SentMargin.EffectiveSize = value;
                if (MAX_SENTSIZE < value) MAX_SENTSIZE = value;
                Shape[2].Value = value;
            }
        }

        public CudaPieceInt SampleIdx = null;
        public CudaPieceInt SentMargin = null;
        public CudaPieceFloat SentOutput;
        public CudaPieceFloat SentDeriv;

        public override DataSourceID Type { get { return DataSourceID.SeqDenseBatchData; } }

        public SeqDenseBatchData(CudaPieceInt smpIdx, CudaPieceInt sentMargin, int maxBatchSize, int maxSentSize, int dim, DeviceType device) :
            this(smpIdx, sentMargin, maxBatchSize, maxSentSize, dim, 
                 new CudaPieceFloat(dim * maxSentSize, device),
                 new CudaPieceFloat(dim * maxSentSize, device), device)
        { }

        public SeqDenseBatchData(int maxBatchSize, int maxSentSize, int dim, DeviceType device) :
            this(null, null, maxBatchSize, maxSentSize, dim, device)
        { }


        public SeqDenseBatchData(CudaPieceInt smpIdx, CudaPieceInt sentMargin, int maxBatchSize, int maxSentSize, int dim, 
                                 CudaPieceFloat sentOutput, CudaPieceFloat sentDeriv, DeviceType device)
            : base(device, new IntArgument("DIM", dim), new IntArgument("MAX_BATCHSIZE", maxBatchSize), new IntArgument("MAX_SENTSIZE", maxSentSize))
                                //, new IntArgument("MAX_SEQLENGTH", 0))
        {
            SampleIdx = smpIdx;
            SentMargin = sentMargin;
            SentOutput = sentOutput;
            SentDeriv = sentDeriv;

            MAX_BATCHSIZE = maxBatchSize;
            MAX_SENTSIZE = maxSentSize;
            Dim = dim;

            ShapeBatchSize = new IntArgument("BATCHSIZE", MAX_BATCHSIZE);
            ShapeSentSize = new IntArgument("SENTSIZE", MAX_SENTSIZE);
            ShapeDim = new IntArgument("DIM", Dim);
        }

        public SeqDenseBatchData(SeqMatrixData data) : this(data.SegmentIdx, data.SegmentMargin, data.MaxSegment, data.MaxRow, data.Column, data.Output, data.Deriv, data.DeviceType) 
        { }

        public override void SyncToCPU()
        {
            SentOutput.SyncToCPU();
            SentDeriv.SyncToCPU();
            if (SampleIdx != null) SampleIdx.SyncToCPU();
            if (SentMargin != null) SentMargin.SyncToCPU();
        }

    }

    public class RecurrentSeqInfoRunner : StructRunner<Structure, BatchData>
    {
        public new RecurrentSeqInfo Output { get { return (RecurrentSeqInfo)base.Output; } set { base.Output = value; } }
        public bool IsReverse { get; set; }
        public RecurrentSeqInfoRunner(int maxBatchSize, int maxSentSize, CudaPieceInt smpIdx, bool isReverse, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            IsReverse = isReverse;
            Output = new RecurrentSeqInfo(maxBatchSize, maxSentSize, smpIdx, behavior.Device);
        }

        public override void Forward()
        {
            if (!IsReverse) Output.InitTransposeInfo();
            else Output.InitReverseAndTransposeInfo();
        }
    }

    public class RecurrentSeqInfo : BatchData
    {
        public int BatchSize { get { return BatchShuffle.EffectiveSize; } set { BatchShuffle.EffectiveSize = value; } }
        public int SentSize { get { return MapForward.EffectiveSize; }set
            {
                MapForward.EffectiveSize = value;
                MapBackward.EffectiveSize = value;
                RecurrentForward.EffectiveSize = value;
                RecurrentBackward.EffectiveSize = value;
            } }

        public int MaxLag;
        public int[] LagSeqIdx;

        public CudaPieceInt BatchShuffle;
        public int[] LagSeqElement;

        CudaPieceInt SampleIdx;
        public int[] SampleIdxMem { get { return SampleIdx.MemPtr; } }

        public CudaPieceInt MapForward;
        public int[] MapForwardMem { get { return MapForward.MemPtr; } }

        public CudaPieceInt MapBackward;

        public CudaPieceInt RecurrentForward;
        public CudaPieceInt RecurrentBackward;

        public RecurrentSeqInfo(int maxBatchSize, int maxSentSize, CudaPieceInt smpIdx, DeviceType device) 
         : base(device, new IntArgument("MAX_BATCHSIZE", maxBatchSize), new IntArgument("MAX_SENTSIZE", maxSentSize))
        {
            MapForward = new CudaPieceInt(maxSentSize, true, device == DeviceType.GPU);
            MapBackward = new CudaPieceInt(maxSentSize, true, device == DeviceType.GPU);

            LagSeqIdx = new int[maxSentSize];
            LagSeqElement = new int[maxSentSize];
            BatchShuffle = new CudaPieceInt(maxBatchSize, true, device == DeviceType.GPU);

            SampleIdx = smpIdx;

            RecurrentForward = new CudaPieceInt(maxSentSize, true, device == DeviceType.GPU);
            RecurrentBackward = new CudaPieceInt(maxSentSize, true, device == DeviceType.GPU);
        }
        
        //public RecurrentSeqInfo(DataStat stat, DeviceType device) : this(stat.Args["MAX_BATCHSIZE"], stat.Args["MAX_SENTSIZE"], null, device)
        //{
            //base(stat, device); 
        //}

        public void InitTransposeInfo(CudaPieceInt sampleIdx, int batchSize, int sentSize)
        {
            sampleIdx.SyncToCPU(batchSize);
            SampleIdx = sampleIdx;

            BatchSize = batchSize;
            SentSize = sentSize;
            List<KeyValuePair<int, int>> instances = Enumerable.Range(0, BatchSize).Select(i => new KeyValuePair<int, int>(i, SampleIdx[i] - (i == 0 ? 0 : SampleIdx[i - 1]))).ToList();
            instances.Sort((a, b) => (b.Value.CompareTo(a.Value)));
            MaxLag = instances.Select(i => i.Value).Max();
            int newPos = 0;
            int oldPos = 0;
            for (int i = 0; i < BatchSize; i++) { RecurrentBackward[i] = -1; }
            for (int lag = 0; lag < MaxLag; lag++)
            {
                KeyValuePair<int, int>[] result = instances.Where(i => i.Value > lag).ToArray();
                foreach (KeyValuePair<int, int> av in result)
                {
                    oldPos = (av.Key == 0 ? 0 : SampleIdxMem[av.Key - 1]) + lag;
                    MapForward[oldPos] = newPos;
                    MapBackward[newPos] = oldPos;
                    LagSeqElement[newPos] = av.Key;
                    int newForwardPos = av.Value == lag + 1 ? -1 : (newPos + result.Length);
                    RecurrentForward[newPos] = newForwardPos;
                    if (newForwardPos > -1) { RecurrentBackward[newForwardPos] = newPos; }
                    newPos++;
                }
                LagSeqIdx[lag] = (lag == 0 ? 0 : LagSeqIdx[lag - 1]) + result.Length;
            }
            BatchShuffle.SyncFromCPU(0, LagSeqElement, 0, BatchSize);
            MapForward.SyncFromCPU(SentSize);
            MapBackward.SyncFromCPU(SentSize);
            RecurrentForward.SyncFromCPU(SentSize);
            RecurrentBackward.SyncFromCPU(SentSize);
        }

        public void InitTransposeInfo()
        {
            BatchSize = SampleIdx.EffectiveSize;
            SampleIdx.SyncToCPU();
            //SentSize = sentSize;
            List<KeyValuePair<int, int>> instances = Enumerable.Range(0, BatchSize).Select(i => new KeyValuePair<int, int>(i, SampleIdxMem[i] - (i == 0 ? 0 : SampleIdxMem[i - 1]))).ToList();
            instances.Sort((a, b) => (b.Value.CompareTo(a.Value)));
            MaxLag = instances.Select(i => i.Value).Max();
            int newPos = 0;
            int oldPos = 0;
            for (int i = 0; i < BatchSize; i++) { RecurrentBackward.MemPtr[i] = -1; }
            for (int lag = 0; lag < MaxLag; lag++)
            {
                KeyValuePair<int, int>[] result = instances.Where(i => i.Value > lag).ToArray();
                foreach (KeyValuePair<int, int> av in result)
                {
                    oldPos = (av.Key == 0 ? 0 : SampleIdxMem[av.Key - 1]) + lag;
                    MapForward.MemPtr[oldPos] = newPos;
                    MapBackward.MemPtr[newPos] = oldPos;
                    LagSeqElement[newPos] = av.Key;
                    int newForwardPos = av.Value == lag + 1 ? -1 : (newPos + result.Length);
                    RecurrentForward.MemPtr[newPos] = newForwardPos;
                    if (newForwardPos > -1) { RecurrentBackward.MemPtr[newForwardPos] = newPos; }
                    newPos++;
                }
                LagSeqIdx[lag] = (lag == 0 ? 0 : LagSeqIdx[lag - 1]) + result.Length;
            }
            SentSize = newPos;
            BatchShuffle.SyncFromCPU(0, LagSeqElement, 0, BatchSize);
            MapForward.SyncFromCPU(SentSize);
            MapBackward.SyncFromCPU(SentSize);
            RecurrentForward.SyncFromCPU(SentSize);
            RecurrentBackward.SyncFromCPU(SentSize);
        }

        public void InitReverseAndTransposeInfo(CudaPieceInt sampleIdx, int batchSize, int sentSize)
        {
            sampleIdx.SyncToCPU(batchSize);
            SampleIdx = sampleIdx;
            BatchSize = batchSize;
            SentSize = sentSize;
            List<KeyValuePair<int, int>> instances = Enumerable.Range(0, BatchSize).Select(i => new KeyValuePair<int, int>(i, SampleIdxMem[i] - (i == 0 ? 0 : SampleIdxMem[i - 1]))).ToList();
            instances.Sort((a, b) => (b.Value.CompareTo(a.Value)));
            MaxLag = instances.Select(i => i.Value).Max();
            int newPos = 0;
            int oldPos = 0;
            for (int i = 0; i < BatchSize; i++) { RecurrentBackward[i] = -1; }
            for (int lag = 0; lag < MaxLag; lag++)
            {
                KeyValuePair<int, int>[] result = instances.Where(i => i.Value > lag).ToArray();
                foreach (KeyValuePair<int, int> av in result)
                {
                    oldPos = SampleIdxMem[av.Key] - 1 - lag;  //(av.Key == 0 ? 0 : SampleIdx[av.Key - 1]) + lag;

                    MapForward[oldPos] = newPos;
                    MapBackward[newPos] = oldPos;
                    LagSeqElement[newPos] = av.Key;
                    int newForwardPos = av.Value == lag + 1 ? -1 : (newPos + result.Length);
                    RecurrentForward[newPos] = newForwardPos;
                    if (newForwardPos > -1) { RecurrentBackward[newForwardPos] = newPos; }
                    newPos++;
                }
                LagSeqIdx[lag] = (lag == 0 ? 0 : LagSeqIdx[lag - 1]) + result.Length;
            }
            BatchShuffle.SyncFromCPU(0, LagSeqElement, 0, BatchSize);
            MapForward.SyncFromCPU(SentSize);

            MapBackward.SyncFromCPU(SentSize);
            RecurrentForward.SyncFromCPU(SentSize);
            RecurrentBackward.SyncFromCPU(SentSize);
        }

        public void InitReverseAndTransposeInfo()
        {
            SampleIdx.SyncToCPU();
            BatchSize = SampleIdx.EffectiveSize;
            List<KeyValuePair<int, int>> instances = Enumerable.Range(0, BatchSize).Select(i => new KeyValuePair<int, int>(i, SampleIdxMem[i] - (i == 0 ? 0 : SampleIdxMem[i - 1]))).ToList();
            instances.Sort((a, b) => (b.Value.CompareTo(a.Value)));
            MaxLag = instances.Select(i => i.Value).Max();
            int newPos = 0;
            int oldPos = 0;
            for (int i = 0; i < BatchSize; i++) { RecurrentBackward[i] = -1; }
            for (int lag = 0; lag < MaxLag; lag++)
            {
                KeyValuePair<int, int>[] result = instances.Where(i => i.Value > lag).ToArray();
                foreach (KeyValuePair<int, int> av in result)
                {
                    oldPos = SampleIdxMem[av.Key] - 1 - lag;  //(av.Key == 0 ? 0 : SampleIdx[av.Key - 1]) + lag;

                    MapForward[oldPos] = newPos;
                    MapBackward[newPos] = oldPos;
                    LagSeqElement[newPos] = av.Key;
                    int newForwardPos = av.Value == lag + 1 ? -1 : (newPos + result.Length);
                    RecurrentForward[newPos] = newForwardPos;
                    if (newForwardPos > -1) { RecurrentBackward[newForwardPos] = newPos; }
                    newPos++;
                }
                LagSeqIdx[lag] = (lag == 0 ? 0 : LagSeqIdx[lag - 1]) + result.Length;
            }
            SentSize = newPos;
            BatchShuffle.SyncFromCPU(0, LagSeqElement, 0, BatchSize);
            MapForward.SyncFromCPU(SentSize);
            MapBackward.SyncFromCPU(SentSize);
            RecurrentForward.SyncFromCPU(SentSize);
            RecurrentBackward.SyncFromCPU(SentSize);
        }
    }

    public class SeqDenseRecursiveData : SeqDenseBatchData 
    {
        public override int BatchSize { get { return base.BatchSize; } set { RecurrentInfo.BatchSize = value; base.BatchSize = value; } }
        public override int SentSize { get { return base.SentSize; } set { RecurrentInfo.SentSize = value; base.SentSize = value; } }
        public int MaxLag { get { return RecurrentInfo.MaxLag; }set { RecurrentInfo.MaxLag = value; } }

        public int[] LagSeqIdx { get { return RecurrentInfo.LagSeqIdx; } set { RecurrentInfo.LagSeqIdx = value; } }
        public int[] LagSeqElement { get { return RecurrentInfo.LagSeqElement; } set { RecurrentInfo.LagSeqElement = value; } }

        public CudaPieceInt MapForward { get { return RecurrentInfo.MapForward; } set { RecurrentInfo.MapForward = value; } }
        public CudaPieceInt MapBackward { get { return RecurrentInfo.MapBackward; } set { RecurrentInfo.MapBackward = value; } }
        public CudaPieceInt RecurrentForward { get { return RecurrentInfo.RecurrentForward; } set { RecurrentInfo.RecurrentForward = value; } }
        public CudaPieceInt RecurrentBackward { get { return RecurrentInfo.RecurrentBackward; } set { RecurrentInfo.RecurrentBackward = value; } }

        public RecurrentSeqInfo RecurrentInfo;

        public SeqDenseRecursiveData(CudaPieceInt smpIdx, CudaPieceInt sentMargin, 
                                    int maxBatchSize, int maxSentSize, int dim, DeviceType device) :
            this(smpIdx, sentMargin, new RecurrentSeqInfo(maxBatchSize, maxSentSize, smpIdx, device),
                maxBatchSize, maxSentSize, dim, device)
        { }

        public SeqDenseRecursiveData(CudaPieceInt smpIdx, CudaPieceInt sentMargin, RecurrentSeqInfo recurrentInfo, 
                                    int maxBatchSize, int maxSentSize, int dim, DeviceType device) :
            base(smpIdx, sentMargin, maxBatchSize, maxSentSize, dim, device)
        {
            RecurrentInfo = recurrentInfo; 
        }

        public SeqDenseRecursiveData(CudaPieceInt smpIdx, CudaPieceInt sentMargin, RecurrentSeqInfo recurrentInfo, 
                                    int maxBatchSize, int maxSentSize, int dim, CudaPieceFloat sentOutput, CudaPieceFloat sentDeriv, DeviceType device) :
            base(smpIdx, sentMargin, maxBatchSize, maxSentSize, dim, sentOutput, sentDeriv, device)
        {
             RecurrentInfo = recurrentInfo;
        }
    }
}
