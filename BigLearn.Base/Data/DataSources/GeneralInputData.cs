﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum FeatureDataType
    {
        SparseFeature = 0,
        DenseFeature = 1,
        NoIdeaFeature = 2,
    }

    // public class GeneralBatchInputDataStat : DataStat
    // {
    //     public int MAX_FEATUREDIM = 0;
    //     public int MAX_BATCHSIZE = 0;
    //     public int MAX_ELEMENTSIZE = 0;
    //     public FeatureDataType FeatureType = FeatureDataType.NoIdeaFeature;

    //     public override void Save(BinaryWriter writer)
    //     {
    //         if (FeatureType == FeatureDataType.NoIdeaFeature) { throw new Exception("Feature Type must be explicitly expressed as SparseFeature or DenseFeature"); }
    //         if (FeatureType == FeatureDataType.DenseFeature) { MAX_ELEMENTSIZE = MAX_BATCHSIZE * MAX_FEATUREDIM + 1; }

    //         writer.Write(MAX_FEATUREDIM);
    //         writer.Write(MAX_BATCHSIZE);
    //         writer.Write(MAX_ELEMENTSIZE);
    //         writer.Write(this.TotalBatchNumber);
    //         writer.Write(this.TotalSampleNumber);
    //     }

    //     public override void Load(BinaryReader reader)
    //     {
    //         MAX_FEATUREDIM = reader.ReadInt32();
    //         MAX_BATCHSIZE = reader.ReadInt32();
    //         MAX_ELEMENTSIZE = reader.ReadInt32();
    //         TotalBatchNumber = reader.ReadInt32();
    //         TotalSampleNumber = reader.ReadInt32();

    //         if (MAX_ELEMENTSIZE <= MAX_BATCHSIZE * MAX_FEATUREDIM) { FeatureType = FeatureDataType.SparseFeature; }
    //         else { FeatureType = FeatureDataType.DenseFeature; }
    //     }


    //     public override void Init(BinaryReader reader)
    //     {
    //         reader.BaseStream.Seek(-5 * sizeof(Int32), SeekOrigin.End);
    //         Load(reader);
    //     }

    //     // public override string ToString()
    //     // {
    //     //     StringBuilder sb = new StringBuilder();
    //     //     TextWriter tw = new StringWriter(sb);
    //     //     foreach (var kv in this.ToKeyValues())
    //     //     {
    //     //         tw.WriteLine("{0}\t{1}", kv.Key, kv.Value);
    //     //     }
    //     //     tw.Flush();
    //     //     return sb.ToString();
    //     // }
    //     // public override IEnumerable<KeyValuePair<string, string>> ToKeyValues()
    //     // {
    //     //     // Keep order
    //     //     yield return new KeyValuePair<string, string>("FEATURE_DIM", this.MAX_FEATUREDIM.ToString());
    //     //     yield return new KeyValuePair<string, string>("MAX_BATCHSIZE", this.MAX_BATCHSIZE.ToString());
    //     //     yield return new KeyValuePair<string, string>("MAX_ELEMENTSIZE", this.MAX_ELEMENTSIZE.ToString());
    //     //     yield return new KeyValuePair<string, string>("TOTAL_BATCHNUM", this.TotalBatchNumber.ToString());
    //     //     yield return new KeyValuePair<string, string>("TOTAL_SAMPLENUM", this.TotalSampleNumber.ToString());
    //     // }
    // }
    

    public class GeneralBatchInputData : BatchData //<GeneralBatchInputDataStat>
    {
        public override DataSourceID Type { get { return DataSourceID.GeneralBatchInputData; } }
        
        public int FEATURE_DIM { get { return Shape[0].Default; } set { Shape[0].Default = value; } }
        public int MAX_BATCHSIZE { get { return Shape[1].Default; } set { Shape[1].Default = value; } }
        public int MAX_ELEMENTSIZE { get { return Shape[2].Default; } set { Shape[2].Default = value; } }
        public FeatureDataType FeatureType { get { return (FeatureDataType)Shape[3].Default; } set { Shape[3].Default = (int)value; } }

        public int BatchSize
        {
            get
            {
                switch (FeatureType)
                {
                    case FeatureDataType.SparseFeature: return BatchIdx.EffectiveSize;
                    case FeatureDataType.DenseFeature: return DenseFeatureData.EffectiveSize / FEATURE_DIM;
                }
                throw new Exception("Feature Type must be explicitly expressed as SparseFeature or DenseFeature");
            }
            set
            {
                InstanceLabel.EffectiveSize = value;
                switch (FeatureType)
                {
                    case FeatureDataType.SparseFeature: BatchIdx.EffectiveSize = value; break;
                    case FeatureDataType.DenseFeature: DenseFeatureData.EffectiveSize = value * FEATURE_DIM; break;
                    case FeatureDataType.NoIdeaFeature: throw new Exception("Feature Type must be explicitly expressed as SparseFeature or DenseFeature");
                }
                if (MAX_BATCHSIZE < value) MAX_BATCHSIZE = value;
                Shape[1].Value = value;
            }
        }

        public int ElementSize
        {
            get
            {
                switch (FeatureType)
                {
                    case FeatureDataType.SparseFeature: return FeatureIdx.EffectiveSize;
                }
                return 0;
            }
            set
            {
                switch (FeatureType)
                {
                    case FeatureDataType.SparseFeature: FeatureIdx.EffectiveSize = value; FeatureValue.EffectiveSize = value; break;
                }
                if (MAX_ELEMENTSIZE < value) MAX_ELEMENTSIZE = value;
                Shape[2].Value = value;
            }
        }

        /// <summary>
        /// sparse feature representation.
        /// </summary>
        public CudaPieceInt BatchIdx;
        public CudaPieceInt FeatureIdx;
        public CudaPieceFloat FeatureValue;
        
        /// <summary>
        /// Dense Feature Representation.
        /// </summary>
        public CudaPieceFloat DenseFeatureData;
        public CudaPieceFloat InstanceLabel;

        //public GeneralBatchInputData() { }

        //public GeneralBatchInputData(DataStat stat, DeviceType device) 
        //    : this(stat.Args["FEATURE_DIM"], stat.Args["MAX_BATCHSIZE"], stat.Args["MAX_ELEMENTSIZE"], (FeatureDataType)stat.Args["FEA_TYPE"], device) 
        //{
        //    Stat = stat;
            //base(stat, device); 
        //}

        /// <summary>
        /// if featureType = SparseFeature, feaDim allows be set to zero (it will automatically infer the feature dimension);
        /// if featureType = DenseFeature, feaDim should not be zero;
        /// </summary>
        /// <param name="feaDim"></param>
        /// <param name="maxBatchSize"></param>
        /// <param name="device"></param>
        public GeneralBatchInputData(int feaDim, int maxBatchSize, int maxElementSize, FeatureDataType featureType, DeviceType device)
        : base(device, new IntArgument("FEATURE_DIM", feaDim), new IntArgument("MAX_BATCHSIZE", maxBatchSize),
                      new IntArgument("MAX_ELEMENTSIZE", maxElementSize), new IntArgument("FEA_TYPE", (int)featureType))
            //this(new GeneralBatchInputDataStat() { FEATUREDIM = feaDim, MAX_BATCHSIZE = maxBatchSize, FeatureType = featureType }, device) 
        {
            FeatureType = featureType;
            FEATURE_DIM = feaDim;
            MAX_BATCHSIZE = maxBatchSize;
            MAX_ELEMENTSIZE = maxElementSize;
            
            DeviceType = device;

            switch (FeatureType)
            {
                case FeatureDataType.SparseFeature: // Sparse Feature Representation.
                    BatchIdx = new CudaPieceInt(MAX_BATCHSIZE, device);
                    FeatureIdx = new CudaPieceInt(MAX_ELEMENTSIZE, device);
                    FeatureValue = new CudaPieceFloat(MAX_ELEMENTSIZE, device);
                    break;
                case FeatureDataType.DenseFeature: // Dense Feature Representation.
                    DenseFeatureData = new CudaPieceFloat(MAX_BATCHSIZE * FEATURE_DIM, device);
                    break;
            }
            InstanceLabel = new CudaPieceFloat(MAX_BATCHSIZE, device);

        }

        
        public override void Deserialize(BinaryReader reader)
        {
            BatchSize = reader.ReadInt32();
            FeatureType = (FeatureDataType)reader.ReadInt32();

            switch (FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    ElementSize = reader.ReadInt32();
                    BatchIdx.Deserialize(reader);
                    FeatureIdx.Deserialize(reader);
                    FeatureValue.Deserialize(reader);
                    //Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * BatchSize), 0, BatchIdx.MemPtr, 0, sizeof(Int32) * BatchSize);
                    //Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * ElementSize), 0, FeatureIdx.MemPtr, 0, sizeof(Int32) * ElementSize);
                    //FeatureValue.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0, sizeof(float) * ElementSize);
                    //Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0, FeatureValue.MemPtr, 0, sizeof(float) * ElementSize);
                    break;
                case FeatureDataType.DenseFeature:
                    //Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize * Stat.MAX_FEATUREDIM), 0, DenseFeatureData.MemPtr, 0, sizeof(float) * BatchSize * Stat.MAX_FEATUREDIM);
                    //DenseFeatureData.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize * Stat.MAX_FEATUREDIM), 0, sizeof(float) * BatchSize * Stat.MAX_FEATUREDIM);
                    DenseFeatureData.Deserialize(reader);
                    break;
            }
            //Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize), 0, InstanceLabel.MemPtr, 0, sizeof(float) * BatchSize);
            //InstanceLabel.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize), 0, sizeof(float) * BatchSize);
            //SyncFromCPU();
            InstanceLabel.Deserialize(reader);
        }

        public override void CopyFrom(IData other)
        {
            GeneralBatchInputData block = (GeneralBatchInputData)other;
            BatchSize = block.BatchSize;
            FeatureType = block.FeatureType;

            switch (FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    ElementSize = block.ElementSize;
                    if (BatchIdx != null) BatchIdx.CopyFrom(block.BatchIdx);
                    if (FeatureIdx != null) FeatureIdx.CopyFrom(block.FeatureIdx);
                    if (FeatureValue != null) FeatureValue.CopyFrom(block.FeatureValue);
                    break;
                case FeatureDataType.DenseFeature:
                    if (DenseFeatureData != null) DenseFeatureData.CopyFrom(block.DenseFeatureData);
                    break;
            }
            if (InstanceLabel != null) InstanceLabel.CopyFrom(block.InstanceLabel);
        }


        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        // public override void SkipBlock(BinaryReader reader)
        // {
        //     int b = reader.ReadInt32();
        //     FeatureDataType feaType = (FeatureDataType)reader.ReadInt32();

        //     //GeneralBatchInputDataStat MatchStat = ((GeneralBatchInputDataStat)stat);
        //     switch (feaType)
        //     {
        //         case FeatureDataType.SparseFeature:
        //             int e = reader.ReadInt32();
        //             CudaPieceInt.Skip(reader);
        //             CudaPieceInt.Skip(reader);
        //             CudaPieceFloat.Skip(reader);
        //             //reader.BaseStream.Seek((b + e) * sizeof(int) + e * sizeof(float), SeekOrigin.Current);
        //             break;
        //         case FeatureDataType.DenseFeature:
        //             CudaPieceFloat.Skip(reader);
        //             //reader.BaseStream.Seek((b * MatchStat.MAX_FEATUREDIM) * sizeof(float), SeekOrigin.Current);
        //             break;
        //     }
        //     CudaPieceFloat.Skip(reader);
        //     //reader.BaseStream.Seek(b * sizeof(float), SeekOrigin.Current);
        // }

        public override void Serialize(BinaryWriter writer)
        {
            if (BatchSize == 0) return;
            
            writer.Write(BatchSize);
            writer.Write((int)FeatureType);

            switch (FeatureType)
                {
                    case FeatureDataType.SparseFeature:
                        writer.Write(ElementSize);
                        BatchIdx.Serialize(writer);
                        FeatureIdx.Serialize(writer);
                        FeatureValue.Serialize(writer);
                        //for (int i = 0; i < BatchSize; i++) writer.Write(BatchIdx[i]);
                        //for (int i = 0; i < ElementSize; i++) writer.Write(FeatureIdx[i]);
                        //for (int i = 0; i < ElementSize; i++) writer.Write(FeatureValue[i]);
                        break;
                    case FeatureDataType.DenseFeature:
                        DenseFeatureData.Serialize(writer);
                        //for (int i = 0; i < BatchSize * MAX_FEATUREDIM; i++) writer.Write(DenseFeatureData[i]);
                        break;
                }
                InstanceLabel.Serialize(writer);

                //for (int i = 0; i < BatchSize; i++) writer.Write(InstanceLabel[i]);

            //{
             //   SyncToCPU();
            //    writer.Write(BatchSize);
            //}
        }

        public void Clear()
        {
            BatchSize = 0;
            ElementSize = 0;
        }


        // public void PushSample(Dictionary<int, float> fea, float label = 0)
        // {
        //     switch (FeatureType)
        //     {
        //         case FeatureDataType.SparseFeature:
        //             this.PushSparseSample(fea.Keys.ToArray(), fea.Values.ToArray(), fea.Count, label);
        //             break;
        //         case FeatureDataType.DenseFeature:
        //             this.PushDenseSample(fea.Keys.ToArray(), fea.Values.ToArray(), fea.Count, label);
        //             break;
        //     }
        // }

        // public void PushSparseSample(int[] fIds, float[] values, int size, float label = 0)
        // {
        //     int oldBatchSize = BatchSize;
        //     int oldElementSize = ElementSize;
             
        //     BatchSize = oldBatchSize + 1;
        //     ElementSize = oldElementSize + size;

        //     BatchIdx.SyncFromCPU(oldBatchSize, new int[] { ElementSize }, 0, 1);
        //     InstanceLabel.SyncFromCPU(oldBatchSize, new float[] { label }, 0, 1);

        //     FeatureIdx.SyncFromCPU(oldElementSize, fIds, 0, size);
        //     FeatureValue.SyncFromCPU(oldElementSize, values, 0, size);

        //     int maxFid = fIds.Max();
        //     if (maxFid >= MAX_FEATUREDIM) { MAX_FEATUREDIM = maxFid + 1; }
        // }

        // public void PushDenseSample(int[] fIds, float[] values, int size, float label = 0)
        // {
        //     float[] newValues = new float[MAX_FEATUREDIM];
        //     for (int i = 0; i < size; i++) { newValues[fIds[i]] = values[i]; }
        //     PushDenseSample(newValues, MAX_FEATUREDIM, label);
        // }

        // public void PushDenseSample(float[] values, int size, float label = 0)
        // {
        //     int oldBatchSize = BatchSize;
        //     BatchSize = oldBatchSize + 1;

        //     InstanceLabel.SyncFromCPU(oldBatchSize, new float[] { label }, 0, 1);
        //     DenseFeatureData.SyncFromCPU(oldBatchSize * MAX_FEATUREDIM, values, 0, size);
        // }

        public override void SyncToCPU()
        {
            switch (FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    BatchIdx.SyncToCPU(BatchSize);
                    FeatureIdx.SyncToCPU(ElementSize);
                    FeatureValue.SyncToCPU(ElementSize);
                    break;
                case FeatureDataType.DenseFeature:
                    DenseFeatureData.SyncToCPU(BatchSize * FEATURE_DIM);
                    break;
            }
            InstanceLabel.SyncToCPU(BatchSize);
        }

        public override void SyncFromCPU()
        {
            switch (FeatureType)
            {
                case FeatureDataType.SparseFeature:
                    BatchIdx.SyncFromCPU(BatchSize);
                    FeatureIdx.SyncFromCPU(ElementSize);
                    FeatureValue.SyncFromCPU(ElementSize);
                    break;
                case FeatureDataType.DenseFeature:
                    DenseFeatureData.SyncFromCPU(BatchSize * FEATURE_DIM);
                    break;
            }
            InstanceLabel.SyncFromCPU(BatchSize);
        }

    }
}
