﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class SeqSentenceInputDataStat : DataStat
    // {
    //     public override DataSourceID Type { get { return DataSourceID.SeqSentenceDataMeta; } }

    //     /// <summary>
    //     /// Maximum Feature Dimension.
    //     /// </summary>
    //     public int MAX_FEATUREDIM { get; set; }

    //     /// <summary>
    //     /// Maximum Batch Size.
    //     /// </summary>
    //     public int MAX_BATCHSIZE { get; set; }

    //     /// <summary>
    //     /// Maximum Segment Size.
    //     /// </summary>
    //     public int MAX_SENTENCESIZE { get; set; }
    //     /// <summary>
    //     /// Maximum Segment Size.
    //     /// </summary>
    //     public int MAX_WORDSIZE { get; set; }

    //     /// <summary>
    //     /// Maximum Element Size.
    //     /// </summary>
    //     public int MAX_ELEMENTSIZE { get; set; }

    //     public SeqSentenceInputDataStat()
    //     {
    //         MAX_FEATUREDIM = 0;
    //         MAX_BATCHSIZE = 0;
    //         MAX_SENTENCESIZE = 0;
    //         MAX_WORDSIZE = 0;
    //         MAX_ELEMENTSIZE = 0;
    //     }

    //     public override void Save(BinaryWriter writer)
    //     {
    //         writer.Write(MAX_FEATUREDIM);
    //         writer.Write(MAX_BATCHSIZE);
    //         writer.Write(MAX_SENTENCESIZE);
    //         writer.Write(MAX_WORDSIZE);
    //         writer.Write(MAX_ELEMENTSIZE);
    //         writer.Write(this.TotalBatchNumber);
    //         writer.Write(this.TotalSampleNumber);
    //     }

    //     public override void Load(BinaryReader reader)
    //     {
    //         MAX_FEATUREDIM = reader.ReadInt32();
    //         MAX_BATCHSIZE = reader.ReadInt32();
    //         MAX_SENTENCESIZE = reader.ReadInt32();
    //         MAX_WORDSIZE = reader.ReadInt32();
    //         MAX_ELEMENTSIZE = reader.ReadInt32();
    //         this.TotalBatchNumber = reader.ReadInt32();
    //         this.TotalSampleNumber = reader.ReadInt32();
    //     }

    //     public override void Init(BinaryReader reader)
    //     {
    //         reader.BaseStream.Seek(-7 * sizeof(Int32), SeekOrigin.End);
    //         Load(reader);
    //     }

    //     // public override IEnumerable<KeyValuePair<string, string>> ToKeyValues()
    //     // {
    //     //     // Keep order
    //     //     yield return new KeyValuePair<string, string>("TYPE", this.Type.ToString());
    //     //     yield return new KeyValuePair<string, string>("FEATURE_DIM", this.MAX_FEATUREDIM.ToString());
    //     //     yield return new KeyValuePair<string, string>("MAX_BATCHSIZE", this.MAX_BATCHSIZE.ToString());
    //     //     yield return new KeyValuePair<string, string>("MAX_SENTENCESIZE", this.MAX_SENTENCESIZE.ToString());
    //     //     yield return new KeyValuePair<string, string>("MAX_WORDSIZE", this.MAX_WORDSIZE.ToString());
    //     //     yield return new KeyValuePair<string, string>("MAX_ELEMENTSIZE", this.MAX_ELEMENTSIZE.ToString());
    //     //     yield return new KeyValuePair<string, string>("TOTAL_BATCHNUM", this.TotalBatchNumber.ToString());
    //     //     yield return new KeyValuePair<string, string>("TOTAL_SAMPLENUM", this.TotalSampleNumber.ToString());
    //     // }
    //     // public override string ToString()
    //     // {
    //     //     StringBuilder sb = new StringBuilder();
    //     //     TextWriter tw = new StringWriter(sb);
    //     //     foreach (var kv in this.ToKeyValues())
    //     //     {
    //     //         tw.WriteLine("{0}\t{1}", kv.Key, kv.Value);
    //     //     }

    //     //     tw.Flush();
    //     //     return sb.ToString();
    //     // }
    // }

    /// <summary>
    /// sample x : <sequence1 of feature set>, <sequence2 of feature set>
    /// Sequence of Sequence of Feature Set;
    /// Consider:
    ///     A Sentence consists of sequence of letter-tri-grams
    ///     A Paragraph consists of sequence of sentences.
    /// </summary>
    public class SeqSentenceBatchInputData : BatchData //<SeqSentenceInputDataStat>
    {
        /// <summary>
        /// Maximum Feature Dimension.
        /// </summary>
        public int FEATURE_DIM { get { return Shape[0].Default; } set { Shape[0].Default = value; } }

        /// <summary>
        /// Maximum Batch Size.
        /// </summary>
        public int MAX_BATCHSIZE { get { return Shape[1].Default; } set { Shape[1].Default = value; } }

        /// <summary>
        /// Maximum Segment Size.
        /// </summary>
        public int MAX_SENTSIZE { get { return Shape[2].Default; } set { Shape[2].Default = value; } }
        /// <summary>
        /// Maximum Segment Size.
        /// </summary>
        public int MAX_WORDSIZE { get { return Shape[3].Default; } set { Shape[3].Default = value; } }

        /// <summary>
        /// Maximum Element Size.
        /// </summary>
        public int MAX_ELEMENTSIZE { get { return Shape[4].Default; } set { Shape[4].Default = value; } }

        public override DataSourceID Type { get { return DataSourceID.SeqSentenceBatchInputData; } }

        public int BatchSize
        {
            get
            {
                return SampleIdx.EffectiveSize;
            }
            set
            {
                SampleIdx.EffectiveSize = value;
                if (MAX_BATCHSIZE < value) MAX_BATCHSIZE = value;
                Shape[1].Value = value;
            }
        }
        public int SentSize // the total length of the full sentences.
        {
            get
            {
                return SentIdx.EffectiveSize;
            }
            set
            {
                SentIdx.EffectiveSize = value;
                SentMargin.EffectiveSize = value;
                if (MAX_SENTSIZE < value) MAX_SENTSIZE = value;
                Shape[2].Value = value;
            }
        }
        public int WordSize // the total length of the full words.
        {
            get
            {
                return WordIdx.EffectiveSize;
            }
            set
            {
                WordIdx.EffectiveSize = value;
                WordMargin.EffectiveSize = value;
                if (MAX_WORDSIZE < value) MAX_WORDSIZE = value;
                Shape[3].Value = value;
            }
        }

        public int ElementSize // the total length of the full feature elements.
        {
            get
            {
                return FeaIdx.EffectiveSize;
            }
            set
            {
                FeaIdx.EffectiveSize = value;
                FeaValue.EffectiveSize = value;
                if (MAX_ELEMENTSIZE < value) MAX_ELEMENTSIZE = value;
                Shape[4].Value = value;
            }
        }

        public CudaPieceInt SampleIdx;

        public CudaPieceInt SentIdx;
        public CudaPieceInt SentMargin;

        public CudaPieceInt WordIdx;
        public CudaPieceInt WordMargin;

        public CudaPieceInt FeaIdx;
        public CudaPieceFloat FeaValue;
        
        //public SeqSentenceBatchInputData(DataStat stat, DeviceType deviceType) : 
        //    this(stat.Args["FEATURE_DIM"], stat.Args["MAX_BATCHSIZE"], stat.Args["MAX_SENTSIZE"], stat.Args["MAX_WORDSIZE"], stat.Args["MAX_ELEMENTSIZE"], deviceType) 
        //{
        //    Stat = stat;
            //base(stat, deviceType); 
        //}
        
        public SeqSentenceBatchInputData(int featureDim, int maxBatchSize, int maxSentSize, int maxWordSize, int maxElementSize, DeviceType deviceType)
            : base(deviceType, new IntArgument("FEATURE_DIM", featureDim), new IntArgument("MAX_BATCHSIZE", maxBatchSize),
                               new IntArgument("MAX_SENTSIZE", maxSentSize), new IntArgument("MAX_WORDSIZE", maxWordSize),
                               new IntArgument("MAX_ELEMENTSIZE", maxElementSize))
        {
            FEATURE_DIM = featureDim;
            MAX_BATCHSIZE = maxBatchSize;
            MAX_SENTSIZE = maxSentSize;
            MAX_WORDSIZE = maxWordSize;
            MAX_ELEMENTSIZE = maxElementSize;

            DeviceType = deviceType;

            SampleIdx = new CudaPieceInt(MAX_BATCHSIZE, DeviceType);
            SentIdx = new CudaPieceInt(MAX_SENTSIZE, DeviceType);
            SentMargin = new CudaPieceInt(MAX_SENTSIZE, DeviceType);
            WordIdx = new CudaPieceInt(MAX_WORDSIZE, DeviceType);
            WordMargin = new CudaPieceInt(MAX_WORDSIZE, DeviceType);
            FeaIdx = new CudaPieceInt(MAX_ELEMENTSIZE, DeviceType);
            FeaValue = new CudaPieceFloat(MAX_ELEMENTSIZE, DeviceType);
        }

        

        public override void SyncFromCPU()
        {
            SampleIdx.SyncFromCPU(BatchSize);
            SentIdx.SyncFromCPU(SentSize);
            WordIdx.SyncFromCPU(WordSize);
            FeaIdx.SyncFromCPU(ElementSize);
            FeaValue.SyncFromCPU(ElementSize);

            SentMargin.SyncFromCPU(SentSize);
            WordMargin.SyncFromCPU(WordSize);
        }

        public override void SyncToCPU()
        {
            SampleIdx.SyncToCPU(BatchSize);
            SentIdx.SyncToCPU(SentSize);
            WordIdx.SyncToCPU(WordSize);
            FeaIdx.SyncToCPU(ElementSize);
            FeaValue.SyncToCPU(ElementSize);

            SentMargin.SyncToCPU(SentSize);
            WordMargin.SyncToCPU(WordSize);
        }

        public override void CopyFrom(IData other)
        {
            SeqSentenceBatchInputData block = (SeqSentenceBatchInputData)other;
            BatchSize = block.BatchSize;
            SentSize = block.SentSize;
            WordSize = block.WordSize;
            ElementSize = block.ElementSize;

            SampleIdx.CopyFrom(block.SampleIdx);
            SentIdx.CopyFrom(block.SentIdx);
            SentMargin.CopyFrom(block.SentMargin);
            WordIdx.CopyFrom(block.WordIdx);
            WordMargin.CopyFrom(block.WordMargin);
            FeaIdx.CopyFrom(block.FeaIdx);
            FeaValue.CopyFrom(block.FeaValue);
        }

        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        // public override void SkipBlock(BinaryReader reader)
        // {
        //     int b = reader.ReadInt32();
        //     int s = reader.ReadInt32();
        //     int w = reader.ReadInt32();
        //     int e = reader.ReadInt32();

        //     CudaPieceInt.Skip(reader);
        //     CudaPieceInt.Skip(reader);
        //     CudaPieceInt.Skip(reader);
        //     CudaPieceInt.Skip(reader);
        //     CudaPieceFloat.Skip(reader);

        //     //reader.BaseStream.Seek((b + s + w + e) * sizeof(int) + e * sizeof(float), SeekOrigin.Current);
        // }

        public override void Serialize(BinaryWriter writer)
        {
            if (BatchSize == 0) return;
            {
                //SyncToCPU();

                writer.Write(BatchSize);
                writer.Write(SentSize);
                writer.Write(WordSize);
                writer.Write(ElementSize);

                SampleIdx.Serialize(writer);
                SentIdx.Serialize(writer);
                WordIdx.Serialize(writer);
                FeaIdx.Serialize(writer);
                FeaValue.Serialize(writer);

                //for (int i = 0; i < BatchSize; ++i) writer.Write(SampleIdx[i]);
                //for (int i = 0; i < SentSize; ++i) writer.Write(SentIdx[i]);
                //for (int i = 0; i < WordSize; ++i) writer.Write(WordIdx[i]);
                //for (int i = 0; i < ElementSize; ++i) writer.Write(FeaIdx[i]);
                //for (int i = 0; i < ElementSize; ++i) writer.Write(FeaValue[i]);
            }
        }

        public override void Deserialize(BinaryReader reader)
        {
            BatchSize = reader.ReadInt32();
            SentSize = reader.ReadInt32();
            WordSize = reader.ReadInt32();
            ElementSize = reader.ReadInt32();

            SampleIdx.Deserialize(reader);
            SentIdx.Deserialize(reader);
            WordIdx.Deserialize(reader);
            FeaIdx.Deserialize(reader);
            FeaValue.Deserialize(reader);

            //Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * BatchSize), 0, SampleIdx.MemPtr, 0, sizeof(Int32) * BatchSize);
            //Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * SentSize), 0, SentIdx.MemPtr, 0, sizeof(Int32) * SentSize);
            //Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * WordSize), 0, WordIdx.MemPtr, 0, sizeof(Int32) * WordSize);
            //Buffer.BlockCopy(reader.ReadBytes(sizeof(Int32) * ElementSize), 0, FeaIdx.MemPtr, 0, sizeof(Int32) * ElementSize);
            //FeaValue.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0, sizeof(float) * ElementSize);
            //Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0, FeaValue.MemPtr, 0, sizeof(float) * ElementSize);

            for (int batch = 0; batch < BatchSize; batch++)
            {
                for (int sent = batch == 0 ? 0 : SampleIdx[batch - 1]; sent < SampleIdx[batch]; sent++)
                {
                    SentMargin[sent] = batch;
                    for (int fea = sent == 0 ? 0 : SentIdx[sent - 1]; fea < SentIdx[sent]; fea++)
                    {
                        WordMargin[fea] = sent;
                    }
                }
            }
            SentMargin.SyncFromCPU(SentSize);
            WordMargin.SyncFromCPU(WordSize);

            //SyncFromCPU();
        }


        public void Clear()
        {
            BatchSize = 0;
            SentSize = 0;
            WordSize = 0;
            ElementSize = 0;
        }

        // public void PushSample(List<List<Dictionary<int, float>>> wordDictList)
        // {
        //     int batchNum = 1;
        //     int sentNum = wordDictList.Count;
        //     int wordNum = wordDictList.Select(feaDictList => feaDictList.Count).Sum();
        //     int elementNum = wordDictList.Select(feaDictList => feaDictList.Select(i => i.Count).Sum()).Sum();

        //     int oldBatchSize = BatchSize;
        //     int oldSentSize = SentSize;
        //     int oldWordSize = WordSize;
        //     int oldElementSize = ElementSize;

        //     BatchSize = oldBatchSize + batchNum;
        //     SentSize = oldSentSize + sentNum;
        //     WordSize = oldWordSize + wordNum;
        //     ElementSize = oldElementSize + elementNum;

        //     SampleIdx.MemPtr[oldBatchSize] = SentSize; //  BatchSize > 0 ? SampleIdx.MemPtr[BatchSize - 1] + wordDictList.Count : wordDictList.Count;
        //     int sentcursor = oldSentSize;
        //     int wordcursor = oldWordSize;
        //     int elementcursor = oldElementSize;

        //     foreach (List<Dictionary<int, float>> feaDictList in wordDictList)
        //     {
        //         //int sentStart = (SentSize == 0) ? 0 : SentIdx.MemPtr[SentSize - 1];
        //         SentIdx[sentcursor] = wordcursor + feaDictList.Count;
        //         SentMargin[sentcursor] = oldBatchSize;
        //         foreach (Dictionary<int, float> seg in feaDictList)
        //         {
        //             //int segStart = (WordSize == 0) ? 0 : WordIdx.MemPtr[WordSize - 1];
        //             WordIdx[wordcursor] = elementcursor + seg.Count;
        //             WordMargin[wordcursor] = sentcursor;
        //             foreach (KeyValuePair<int, float> fv in seg)
        //             {
        //                 FeaIdx[elementcursor] = fv.Key;
        //                 FeaValue[elementcursor] = fv.Value;
        //                 elementcursor += 1;
        //                 if (fv.Key >= Stat.MAX_FEATUREDIM) Stat.MAX_FEATUREDIM = fv.Key + 1;
        //             }
        //             wordcursor += 1;
        //         }
        //         sentcursor += 1;
        //     }

        //     SampleIdx.SyncFromCPU();
        //     SentIdx.SyncFromCPU();
        //     SentMargin.SyncFromCPU();
        //     WordIdx.SyncFromCPU();
        //     WordMargin.SyncFromCPU();
        //     FeaIdx.SyncFromCPU();
        //     FeaValue.SyncFromCPU();
        // }
    }

    /// <summary>
    /// Add label for each sample in SeqSentenceBatchInputData
    /// </summary>
    //public class SeqSentenceBatchInputLabelData : SeqSentenceBatchInputData
    //{
    //    public override DataSourceID Type { get { return DataSourceID.SeqSentenceBatchInputLabelData; } }

    //    public CudaPieceFloat Label;

    //    public override int BatchSize
    //    {
    //        get
    //        {
    //            return base.BatchSize;
    //        }
    //        set
    //        {
    //            base.BatchSize = value;
    //            Label.EffectiveSize = value;
    //        }
    //    }


    //    public SeqSentenceBatchInputLabelData() { }
    //    public SeqSentenceBatchInputLabelData(SeqSentenceInputDataStat stat, DeviceType device) : base(stat, device) { }

    //    public override void Init(SeqSentenceInputDataStat stat, DeviceType deviceType)
    //    {
    //        base.Init(stat, deviceType);
    //        Label = new CudaPieceFloat(stat.MAX_BATCHSIZE, true, deviceType == DeviceType.GPU);
    //    }

    //    public override void Load(BinaryReader reader)
    //    {
    //        base.Load(reader);
    //        Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize), 0, Label.MemPtr, 0, sizeof(float) * BatchSize);
    //        Label.SyncFromCPU(BatchSize);
    //    }
    //    public override void Save(BinaryWriter writer)
    //    {
    //        base.Save(writer);
    //        Label.SyncToCPU(BatchSize);
    //        for (int i = 0; i < BatchSize; ++i) writer.Write(Label.MemPtr[i]);
    //    }

    //    public override void CopyFrom(IData other)
    //    {
    //        base.CopyFrom(other);
    //        SeqSentenceBatchInputLabelData block = (SeqSentenceBatchInputLabelData)other;
    //        Label.CopyFrom(0, block.Label, 0, block.BatchSize);
    //    }

    //    public override void SyncFromCPU()
    //    {
    //        Label.SyncFromCPU(BatchSize);
    //    }

    //    public override void SyncToCPU()
    //    {
    //        Label.SyncToCPU(BatchSize);
    //    }

    //    public override void SkipBlock(BinaryReader reader, DataStat stat)
    //    {
    //        int b = reader.ReadInt32();
    //        int s = reader.ReadInt32();
    //        int w = reader.ReadInt32();
    //        int e = reader.ReadInt32();
    //        reader.BaseStream.Seek((b + s + w + e) * sizeof(int) + (e + b) * sizeof(float), SeekOrigin.Current);
    //    }


    //    public void PushSample(List<List<Dictionary<int, float>>> segDictList, float label)
    //    {
    //        int oldBatchSize = BatchSize;
    //        PushSample(segDictList);

    //        Label.MemPtr[oldBatchSize] = label;
    //        Label.SyncFromCPU(oldBatchSize, Label.MemPtr, oldBatchSize, 1);
    //    }

    //    protected override void Dispose(bool disposing)
    //    {
    //        base.Dispose(disposing);
    //        if (disposing)
    //        {
    //            if (Label != null) Label.Dispose(); Label = null;
    //        }
    //    }
    //}
}
