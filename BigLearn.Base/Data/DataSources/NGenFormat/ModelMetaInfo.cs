﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class ModelMetaInfo : IMetaInfo
    // {
    //     public const string KEY_LOSS = "LOSS";
    //     public const string KEY_TRAINNING_PARAMETERS = "TRAINNING_PARAMETERS";
    //     public const string PREFIX_DATA = "DATAMETA::";
    //     public DataSourceID Type { get { return DataSourceID.ModelMetaInfo; } }

    //     public DeviceType DeviceType { get { return DeviceType.CPU; } }

    //     public Dictionary<string, string> TrainningParameters { get; set; }

    //     public Dictionary<int, double> LossInfo { get; private set; }

    //     public IMetaInfo DataMeta { get; set; }

    //     public IOUnitHeader Head { get; set; }

    //     public void Dispose()
    //     {
            
    //     }
    //     public ModelMetaInfo()
    //     {
    //         this.TrainningParameters = new Dictionary<string, string>();
    //         this.LossInfo = new Dictionary<int, double>();
    //     }

    //     public void Load(BinaryReader input)
    //     {
    //         string trainningStr = input.ReadString();
    //         this.SetTrainingParameters(trainningStr);
    //         string lossStr = input.ReadString();
    //         this.SetLossInfo(lossStr);
    //         try
    //         {
    //             IOUnitReader rd = new IOUnitReader(input.BaseStream);
    //             this.DataMeta = (IMetaInfo)rd.Read();
    //         }
    //         catch (EndOfStreamException)
    //         {
    //             this.DataMeta = null;
    //         }
    //     }

    //     public void Save(BinaryWriter output)
    //     {
    //         output.Write(DictionaryToString(this.TrainningParameters));
    //         output.Write(DictionaryToString(this.LossInfo));
    //         if (this.DataMeta != null)
    //         {
    //             IOUnitWriter wr = new IOUnitWriter(output.BaseStream);
    //             wr.Write(this.DataMeta);
    //         }
    //     }

    //     public void LoadFromKeyValues(IDictionary<string, string> kvs)
    //     {
    //         if (kvs.ContainsKey(ModelMetaInfo.KEY_TRAINNING_PARAMETERS))
    //         {
    //             this.SetTrainingParameters(kvs[ModelMetaInfo.KEY_TRAINNING_PARAMETERS]);
    //         }

    //         if (kvs.ContainsKey(ModelMetaInfo.KEY_LOSS))
    //         {
    //             this.SetLossInfo(kvs[ModelMetaInfo.KEY_LOSS]);
    //         }

    //         Dictionary<string, string> dataKvs = kvs.Where(kv => kv.Key.StartsWith(PREFIX_DATA)).ToDictionary(kv => kv.Key.Substring(PREFIX_DATA.Length), kv => kv.Value);
    //         if (dataKvs.Count > 0)
    //         {
    //             this.DataMeta = TextMetaReader.CreateMetaInfo(dataKvs);
    //         }
    //     }

    //     public IEnumerable<KeyValuePair<string, string>> ToKeyValues()
    //     {
    //         // Keep order
    //         yield return new KeyValuePair<string, string>("TYPE", this.Type.ToString());
    //         yield return new KeyValuePair<string, string>(ModelMetaInfo.KEY_TRAINNING_PARAMETERS, DictionaryToString(this.TrainningParameters));
    //         yield return new KeyValuePair<string, string>(ModelMetaInfo.KEY_LOSS, DictionaryToString(this.LossInfo));
    //         if (this.DataMeta != null)
    //         {
    //             foreach (var kv in this.DataMeta.ToKeyValues())
    //             {
    //                 yield return new KeyValuePair<string, string>(PREFIX_DATA + kv.Key, kv.Value); ;
    //             }
    //         }
    //     }

    //     public void Add(IMetaInfo meta)
    //     {
    //         throw new NotImplementedException();
    //     }

    //     public void CopyFrom(IMetaInfo meta)
    //     {
    //         throw new NotImplementedException();
    //     }

    //     public void SyncFromCPU() { throw new NotImplementedException(); }
    //     public void SyncToCPU() { throw new NotImplementedException(); }


    //     public void SetTrainingParameters(string paramsString)
    //     {
    //         this.TrainningParameters = StringToDictionary(paramsString, s => s, v => v);
    //     }

    //     public void UpdateParameters(string paramsString)
    //     {
    //         if (this.TrainningParameters == null)
    //         {
    //             this.TrainningParameters = new Dictionary<string, string>();
    //         }

    //         UpdateParameters(StringToDictionary(paramsString, s => s, v => v));
    //     }

    //     public void UpdateParameters(Dictionary<string, string> argsDict)
    //     {
    //         foreach (var kv in argsDict)
    //         {
    //             this.TrainningParameters[kv.Key] = kv.Value;
    //         }
    //     }

    //     public void SetLossInfo(string lossInfo)
    //     {
    //         this.LossInfo = StringToDictionary(lossInfo, s => int.Parse(s), v => double.Parse(v));
    //     }

    //     public override string ToString()
    //     {
    //         StringBuilder sb = new StringBuilder();
    //         TextWriter tw = new StringWriter(sb);
    //         foreach (var kv in this.ToKeyValues())
    //         {
    //             tw.WriteLine("{0}\t{1}", kv.Key, kv.Value);
    //         }

    //         tw.Flush();
    //         return sb.ToString();
    //     }

    //     public static string DictionaryToString<TKey, TValue>(Dictionary<TKey, TValue> dict)
    //     {
    //         return string.Join(";", dict.Select(kv => kv.Key.ToString() + ":" + kv.Value.ToString()));
    //     }

    //     public static Dictionary<TKey, TValue> StringToDictionary<TKey, TValue>(string input, Func<string, TKey> ParseKey, Func<string, TValue> ParseValue)
    //     {
    //         Dictionary<TKey, TValue> dict = new Dictionary<TKey, TValue>();

    //         if (!string.IsNullOrEmpty(input))
    //         {
    //             dict = input.Split(";".ToArray(), StringSplitOptions.RemoveEmptyEntries).ToDictionary((p) =>
    //             {
    //                 int idx = p.IndexOf(':');
    //                 return ParseKey(p.Substring(0, idx).Trim("\r\n\t ".ToArray()));
    //             }, (p) =>
    //             {
    //                 int idx = p.IndexOf(':');
    //                 return ParseValue(p.Substring(idx + 1).Trim("\r\n\t ".ToArray()));
    //             });
    //         }

    //         return dict;
    //     }

    // }
}
