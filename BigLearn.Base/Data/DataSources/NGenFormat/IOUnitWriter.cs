﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class IOUnitWriter
    // {
    //     protected BinaryWriter writer;

    //     /// <summary>
    //     /// Path with extension .v2 is used for the new format
    //     /// In new format data and stat are separated into different files
    //     /// </summary>
    //     /// <param name="dataStream"></param>
    //     public IOUnitWriter(Stream dataStream)
    //     {
    //         this.writer = new BinaryWriter(dataStream);
    //     }

    //     public virtual void Write(IIOUnit data)
    //     {
    //         this.Write(data.Head, data);
    //     }

    //     public virtual void Write(IOUnitHeader head, IIOUnit data)
    //     {
    //         if (head == null)
    //         {
    //             head = new IOUnitHeader();
    //         }

    //         head.PayloadDataType = data.Type;
    //         MemoryStream ms = new MemoryStream();
    //         BinaryWriter bw = new BinaryWriter(ms);

    //         // Set head to preallocate space in the stream.
    //         head.Serialize(bw);
    //         data.Save(bw);

    //         // Seek to the beginning to set actual head.
    //         ms.Seek(0, SeekOrigin.Begin);
    //         head.PayloadSize = (int)ms.Length - head.HeadSize;
    //         head.Serialize(bw);

    //         // We write data to memory first, then flush the bulk of entire data to underline storage to speedup the write and avoid data split in cosmos.
    //         this.writer.Write(ms.ToArray(), 0, (int)ms.Length);
    //     }

    //     public virtual void Flush()
    //     {
    //         this.writer.Flush();
    //     }

    //     public virtual void Close()
    //     {
    //         this.writer.Close();
    //     }
    // }
}
