﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class CachableIOUnitFactory : IOUnitFactory
    // {
    //     protected Dictionary<DataSourceID, ConcurrentQueue<IIOUnit>> freeCPUIOUnitList;
    //     protected Dictionary<DataSourceID, ConcurrentQueue<IIOUnit>> freeGPUIOUnitList;
    //     private bool disposed = false;

    //     public CachableIOUnitFactory(IMetaInfo meta)
    //         : base(meta)
    //     {
    //         freeCPUIOUnitList = new Dictionary<DataSourceID, ConcurrentQueue<IIOUnit>>();
    //         freeCPUIOUnitList.Add(DataSourceID.SeqSparseDataSource, new ConcurrentQueue<IIOUnit>());
    //         freeCPUIOUnitList.Add(DataSourceID.DSSMData, new ConcurrentQueue<IIOUnit>());

    //         freeGPUIOUnitList = new Dictionary<DataSourceID, ConcurrentQueue<IIOUnit>>();
    //         freeGPUIOUnitList.Add(DataSourceID.SeqSparseDataSource, new ConcurrentQueue<IIOUnit>());
    //     }

    //     public CachableIOUnitFactory()
    //         : this(null)
    //     {
    //     }

    //     ~CachableIOUnitFactory()
    //     {
    //         this.Dispose(false);
    //     }

    //     public override void SetMeta(IMetaInfo meta)
    //     {
    //         if (this.meta == null)
    //         {
    //             this.meta = meta;
    //         }
    //         else
    //         {
    //             throw new InvalidOperationException("The meta inforamtion can only be set once.");
    //         }
    //     }

    //     protected override ConcurrentQueue<IIOUnit> GetFreeList(DataSourceID type, DeviceType device)
    //     {
    //         if (this.Meta == null)
    //         {
    //             return null;
    //         }

    //         if (device == DeviceType.CPU || device == DeviceType.CPU_FAST_VECTOR)
    //         {
    //             if (!this.freeCPUIOUnitList.ContainsKey(type))
    //             {
    //                 return null;
    //             }

    //             return this.freeCPUIOUnitList[type];
    //         }
    //         else
    //         {
    //             if (!this.freeGPUIOUnitList.ContainsKey(type))
    //             {
    //                 return null;
    //             }

    //             return this.freeGPUIOUnitList[type];
    //         }
    //     }

    //     public override void FreeIOUnit(IIOUnit data)
    //     {
    //         if (data == null)
    //         {
    //             return;
    //         }

    //         var freeList = this.GetFreeList(data.Type, data.DeviceType);
    //         if (freeList != null)
    //         {
    //             freeList.Enqueue(data);
    //         }
    //         else
    //         {
    //             data.Dispose();
    //         }
    //     }

    //     protected override void Dispose(bool disposing)
    //     {
    //         if (this.disposed)
    //         {
    //             return;
    //         }

    //         this.disposed = true;

    //         if (disposing)
    //         {
    //             if (this.freeCPUIOUnitList != null)
    //             {
    //                 foreach (var u in this.freeCPUIOUnitList)
    //                 {
    //                     IIOUnit io;
    //                     while (u.Value.TryDequeue(out io))
    //                     {
    //                         io.Dispose();
    //                     }
    //                 }

    //                 this.freeCPUIOUnitList.Clear();
    //                 this.freeCPUIOUnitList = null;
    //             }

    //             if (this.freeGPUIOUnitList != null)
    //             {
    //                 foreach (var u in this.freeGPUIOUnitList)
    //                 {
    //                     IIOUnit io;
    //                     while (u.Value.TryDequeue(out io))
    //                     {
    //                         io.Dispose();
    //                     }
    //                 }

    //                 this.freeGPUIOUnitList.Clear();
    //                 this.freeGPUIOUnitList = null;
    //             }
    //         }

    //         base.Dispose(disposing);
    //     }
    // }
}
