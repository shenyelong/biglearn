﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class IOUnitFactory : IDisposable
    // {
    //     protected IMetaInfo meta;
    //     private bool disposed = true;

    //     public IOUnitFactory(IMetaInfo meta)
    //     {
    //         this.meta = meta;
    //     }

    //     public IOUnitFactory()
    //         : this(null)
    //     {
    //     }

    //     ~IOUnitFactory()
    //     {
    //         this.Dispose(false);
    //     }

    //     public virtual IMetaInfo Meta
    //     {
    //         get
    //         {
    //             return this.meta;
    //         }
    //     }

    //     public virtual void SetMeta(IMetaInfo meta)
    //     {
    //         this.meta = meta;
    //     }

    //     public virtual IIOUnit GetIOUnit(DataSourceID type, DeviceType deviceType)
    //     {
    //         IOUnitHeader head = new IOUnitHeader()
    //         {
    //             PayloadDataType = type
    //         };

    //         return this.GetIOUnit(head, deviceType);
    //     }

    //     public virtual IIOUnit GetIOUnit(IOUnitHeader head, DeviceType deviceType)
    //     {
    //         throw new Exception();
    //     }

    //     public virtual void FreeIOUnit(IIOUnit data)
    //     {
    //         if (data == null)
    //         {
    //             return;
    //         }

    //         data.Dispose();
    //     }

    //     public void Dispose()
    //     {
    //         this.Dispose(true);
    //         GC.SuppressFinalize(this);
    //     }

    //     protected virtual void Dispose(bool disposing)
    //     {
    //         if (this.disposed)
    //         {
    //             return;
    //         }

    //         this.meta = null;
    //         this.disposed = true;
    //     }

    //     protected virtual ConcurrentQueue<IIOUnit> GetFreeList(DataSourceID type, DeviceType device)
    //     {
    //         return null;
    //     }

    //     public static IOUnitFactory Default = new IOUnitFactory();
    // }
}
