﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class TextMetaReader : IOUnitReader
    // {
    //     private TextReader textReader;

    //     public TextMetaReader(Stream dataStream)
    //         : base(dataStream)
    //     {
    //     }

    //     public TextMetaReader(TextReader textReader)
    //         : base(null)
    //     {
    //         this.textReader = textReader;
    //     }

    //     private TextReader TextReader
    //     {
    //         get
    //         {
    //             if (this.textReader == null)
    //             {
    //                 this.textReader = new StreamReader(this.BaseStream);
    //             }

    //             return this.textReader;
    //         }
    //     }

    //     public override IIOUnit Read()
    //     {
    //         string text = this.TextReader.ReadToEnd();

    //         Dictionary<string, string> kvs = ParseStr2KeyValues(text);

    //         return CreateMetaInfo(kvs, this.DataFactory);

    //     }

    //     public static IMetaInfo CreateMetaInfo(Dictionary<string, string> kvs, IOUnitFactory dataFactory = null)
    //     {
    //         dataFactory = dataFactory ?? IOUnitFactory.Default;
    //         DataSourceID type;
    //         if (kvs.ContainsKey("TYPE") && Enum.TryParse(kvs["TYPE"], true, out type))
    //         {
    //             IMetaInfo meta = (IMetaInfo)dataFactory.GetIOUnit(type, DeviceType.CPU);
    //             meta.LoadFromKeyValues(kvs);
    //             return meta;
    //         }

    //         throw new InvalidDataException("The data type can't be determined by the contents.");
    //     }

    //     public static Dictionary<string, string> ParseStr2KeyValues(string text)
    //     {
    //         Dictionary<string, string> kvs = new Dictionary<string, string>();
    //         StringReader sr = new StringReader(text);
    //         string line = null;
    //         while ((line = sr.ReadLine()) != null)
    //         {
    //             string[] kv = line.Split('\t');
    //             if (kv.Length < 2 || line.StartsWith("#"))
    //             {
    //                 continue;
    //             }

    //             kvs[kv[0]] = kv[1];
    //         }

    //         return kvs;
    //     }
    // }
}
