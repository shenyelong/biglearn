﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class IOUnitReader : IDisposable
    // {
    //     private BinaryReader binaryReader;
    //     private bool disposed = false;
    //     private Stream baseStream;
    //     private IOUnitFactory dataFactory;

    //     public IOUnitReader(Stream dataStream)
    //         : this(dataStream, IOUnitFactory.Default)
    //     {
    //     }

    //     public IOUnitReader(Stream dataStream, IOUnitFactory factroy)
    //     {
    //         this.dataFactory = factroy;
    //         this.baseStream = dataStream;
    //     }

    //     ~IOUnitReader()
    //     {
    //         this.Dispose(false);
    //     }

    //     protected virtual BinaryReader BinaryReader
    //     {
    //         get
    //         {
    //             if (this.binaryReader == null)
    //             {
    //                 this.binaryReader = new BinaryReader(BaseStream, Encoding.UTF8, true);
    //             }

    //             return this.binaryReader;
    //         }
    //     }

    //     public virtual IIOUnit Read()
    //     {
    //         IIOUnit data = null;
    //         IOUnitHeader head = new IOUnitHeader();
    //         head.Load(this.BinaryReader);
    //         data = this.DataFactory.GetIOUnit(head, DeviceType.CPU);
    //         data.Load(this.BinaryReader);
    //         return data;
    //     }

    //     public virtual Stream BaseStream
    //     {
    //         get
    //         {
    //             return this.baseStream;
    //         }
    //     }

    //     public virtual IOUnitFactory DataFactory
    //     {
    //         get
    //         {
    //             return this.dataFactory;
    //         }
    //     }

    //     public void Dispose()
    //     {
    //         this.Dispose(true);
    //         GC.SuppressFinalize(this);
    //     }

    //     protected virtual void Dispose(bool disposing)
    //     {
    //         if (this.disposed)
    //         {
    //             return;
    //         }

    //         if (disposing)
    //         {
    //             if (this.binaryReader != null)
    //             {
    //                 this.binaryReader.Dispose();
    //                 this.binaryReader = null;
    //             }

    //             this.dataFactory.Dispose();
    //             this.dataFactory = null;
    //         }

    //         this.disposed = true;
    //     }
    // }
}
