﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RelationGraphData
    {
        public Dictionary<string, int> EntityId = null;
        public Dictionary<string, int> RelationId = null;

        int mEntityNum;
        int mRelationNum;
        public int EntityNum { get { return EntityId == null ? mEntityNum : EntityId.Count; } set { mEntityNum = value; } }
        public int RelationNum { get { return RelationId == null ? mRelationNum : RelationId.Count; }set { mRelationNum = value; } }

        public List<Tuple<int, int, int>> Train;
        public List<Tuple<int, int, int>> Test;
        public List<Tuple<int, int, int>> Valid;

        public List<Tuple<int, int, Dictionary<int, float>>> SignedValid;
        public List<Tuple<int, int, Dictionary<int, float>>> SignedTest;

        public List<Tuple<int, int, List<int>, List<float>>> SignedValidV2;
        public List<Tuple<int, int, List<int>, List<float>>> SignedTestV2;


        public HashSet<string> ValidGraphHash = new HashSet<string>();
        public HashSet<string> TrainGraphHash = new HashSet<string>();

        public double[] HeadProb;
        public double[] TailProb;

        public Dictionary<int, int> RelationCount = new Dictionary<int, int>();
        public Dictionary<int, HashSet<int>> RelationHead = new Dictionary<int, HashSet<int>>();
        public Dictionary<int, HashSet<int>> RelationTail = new Dictionary<int, HashSet<int>>();

        /// <summary>
        /// Value : 
        /// Item1 : +1, pos link; -1, neg link;
        /// Item2 : relation idx
        /// Item3 : node Idx;
        /// </summary>
        public Dictionary<int, List<Tuple<int, int, int>>> TrainNeighborLink = new Dictionary<int, List<Tuple<int, int, int>>>();

        public Dictionary<int, Dictionary<int, HashSet<int>>> TrainNeighborHash = new Dictionary<int, Dictionary<int, HashSet<int>>>
            ();
        public Dictionary<int, Dictionary<int, HashSet<int>>> AllNeighborHash = new Dictionary<int, Dictionary<int, HashSet<int>>>
            ();

        public HashSet<int> NeighborSet(int e, int maxHop)
        {
            List<KeyValuePair<int, int>> searchNodes = new List<KeyValuePair<int, int>>();
            HashSet<int> history = new HashSet<int>();

            searchNodes.Add(new KeyValuePair<int, int>(e, 0));
            history.Add(e);

            int begin = 0;
            int end = searchNodes.Count;
            int iter = 0;
            for (iter = 0; iter <= maxHop; iter++)
            {
                for (int i = begin; i < end; i++)
                {
                    KeyValuePair<int, int> xnode = searchNodes[i];

                    // no neighbor continue;
                    if (!TrainNeighborLink.ContainsKey(xnode.Key)) continue;

                    foreach (Tuple<int, int, int> transitem in TrainNeighborLink[xnode.Key])
                    {
                        // histroy connected node : continue;
                        if (history.Contains(transitem.Item3)) continue;

                        int step = xnode.Value + 1;
                        history.Add(transitem.Item3);
                        searchNodes.Add(new KeyValuePair<int, int>(transitem.Item3, step));
                    }
                }
                begin = end;
                end = searchNodes.Count;
            }
            return history;
        }

        /// <summary>
        /// calculate the shortest path from entity e1 and entity e2.
        /// </summary>
        /// <param name="e1"></param>
        /// <param name="e2"></param>
        /// <param name="blackrid"></param>
        /// <param name="maxHop"></param>
        /// <returns></returns>
        public int ShortestPath(int e1, int e2, int blackrid, int maxHop)
        {
            List<KeyValuePair<int, int>> searchNodes = new List<KeyValuePair<int, int>>();
            HashSet<int> history = new HashSet<int>();

            searchNodes.Add(new KeyValuePair<int, int>(e1, 0));
            history.Add(e1);

            int begin = 0;
            int end = searchNodes.Count;
            int iter = 0;
            for (iter = 0; iter <= maxHop; iter++)
            {
                for (int i = begin; i < end; i++)
                {
                    KeyValuePair<int, int> xnode = searchNodes[i];

                    // no neighbor continue;
                    if (!TrainNeighborLink.ContainsKey(xnode.Key)) continue;

                    foreach (Tuple<int, int, int> transitem in TrainNeighborLink[xnode.Key])
                    {
                        // histroy connected node : continue;
                        if (history.Contains(transitem.Item3)) continue;

                        // black link : continue;
                        if (xnode.Key == e1 && transitem.Item2 == blackrid) continue;

                        int step = xnode.Value + 1;
                        history.Add(transitem.Item3);
                        searchNodes.Add(new KeyValuePair<int, int>(transitem.Item3, step));

                        if (transitem.Item3 == e2) return step;
                    }
                }
                begin = end;
                end = searchNodes.Count;
            }

            return -1;
        }

        public static Dictionary<string, int> LoadMapId(string fileName)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] items = line.Trim().Split('\t');
                    string name = items[0];
                    int Id = int.Parse(items[1]);
                    result[name] = Id;
                }
            }
            return result;
        }

        /// Entity 1, Entity 2, Relation.
        public List<Tuple<int, int, int>> LoadGraph(string fileName,
            Dictionary<string, int> eID, Dictionary<string, int> rID, bool IsTrain)
        {
            if (IsTrain)
            {
                if (rID == null)
                {
                    for (int i = 0; i < RelationNum; i++)
                    {
                        RelationCount.Add(i, 0);
                        RelationHead.Add(i, new HashSet<int>());
                        RelationTail.Add(i, new HashSet<int>());
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, int> rItem in rID)
                    {
                        RelationCount[rItem.Value] = 0;
                        RelationHead[rItem.Value] = new HashSet<int>();
                        RelationTail[rItem.Value] = new HashSet<int>();
                    }
                }

                if (eID == null)
                {
                    for (int i = 0; i < EntityNum; i++)
                    {
                        TrainNeighborLink.Add(i, new List<Tuple<int, int, int>>());
                        TrainNeighborHash.Add(i, new Dictionary<int, HashSet<int>>());
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, int> eItem in eID)
                    {
                        TrainNeighborLink.Add(eItem.Value, new List<Tuple<int, int, int>>());
                        TrainNeighborHash.Add(eItem.Value, new Dictionary<int, HashSet<int>>());
                    }
                }
            }

            List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] items = line.Trim().Split('\t');
                    string e1 = items[0];
                    string e2 = items[1];
                    string r = items[2];

                    int rIdx = -1;
                    if (rID == null) rIdx = int.Parse(r);
                    else rIdx = rID[r];

                    int eIdx1 = -1;
                    if (eID == null) eIdx1 = int.Parse(e1);
                    else eIdx1 = eID[e1];

                    int eIdx2 = -1;
                    if (eID == null) eIdx2 = int.Parse(e2);
                    else eIdx2 = eID[e2];


                    if (IsTrain)
                    {
                        RelationCount[rIdx] += 1;
                        RelationHead[rIdx].Add(eIdx1);
                        RelationTail[rIdx].Add(eIdx2);

                        TrainNeighborLink[eIdx1].Add(new Tuple<int, int, int>(1, rIdx, eIdx2));
                        TrainNeighborLink[eIdx2].Add(new Tuple<int, int, int>(-1, rIdx, eIdx1));


                        int Rr = rIdx + RelationNum;

                        if (!TrainNeighborHash[eIdx1].ContainsKey(rIdx))
                        {
                            TrainNeighborHash[eIdx1].Add(rIdx, new HashSet<int>());
                        }
                        TrainNeighborHash[eIdx1][rIdx].Add(eIdx2);

                        if (!TrainNeighborHash[eIdx2].ContainsKey(Rr))
                        {
                            TrainNeighborHash[eIdx2].Add(Rr, new HashSet<int>());
                        }
                        TrainNeighborHash[eIdx2][Rr].Add(eIdx1);

                        TrainGraphHash.Add(string.Format("{0}#{1}#{2}", eIdx1, eIdx2, rIdx));
                    }
                    else
                    {
                        ValidGraphHash.Add(string.Format("{0}#{1}#{2}", eIdx1, eIdx2, rIdx));
                    }
                    result.Add(new Tuple<int, int, int>(eIdx1, eIdx2, rIdx));
                }
            }

            if (IsTrain)
            {
                TailProb = new double[RelationNum];
                HeadProb = new double[RelationNum];
                for (int i = 0; i < RelationNum; i++)
                {
                    TailProb[i] = RelationCount[i] * 1.0 / RelationTail[i].Count;
                    HeadProb[i] = RelationCount[i] * 1.0 / RelationHead[i].Count;
                }
            }
            return result;
        }


        /// <summary>
        /// Entity Id1, relation, Entity Id2.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="eID"></param>
        /// <param name="rID"></param>
        /// <param name="IsTrain"></param>
        /// <returns></returns>
        public List<Tuple<int, int, int>> LoadGraphV2(string fileName,
            Dictionary<string, int> eID, Dictionary<string, int> rID, bool IsTrain)
        {
            if (IsTrain)
            {
                if (rID == null)
                {
                    for (int i = 0; i < RelationNum; i++)
                    {
                        RelationCount.Add(i, 0);
                        RelationHead.Add(i, new HashSet<int>());
                        RelationTail.Add(i, new HashSet<int>());
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, int> rItem in rID)
                    {
                        RelationCount[rItem.Value] = 0;
                        RelationHead[rItem.Value] = new HashSet<int>();
                        RelationTail[rItem.Value] = new HashSet<int>();
                    }
                }

                if (eID == null)
                {
                    for (int i = 0; i < EntityNum; i++)
                    {
                        TrainNeighborLink.Add(i, new List<Tuple<int, int, int>>());
                        TrainNeighborHash.Add(i, new Dictionary<int, HashSet<int>>());
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, int> eItem in eID)
                    {
                        TrainNeighborLink.Add(eItem.Value, new List<Tuple<int, int, int>>());
                        TrainNeighborHash.Add(eItem.Value, new Dictionary<int, HashSet<int>>());
                    }
                }
            }

            List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] items = line.Trim().Split('\t');
                    int e1 = int.Parse(items[0]);
                    int r = int.Parse(items[1]);
                    int e2 = int.Parse(items[2]);
                    
                    if (IsTrain)
                    {
                        RelationCount[r] += 1;
                        RelationHead[r].Add(e1);
                        RelationTail[r].Add(e2);

                        TrainNeighborLink[e1].Add(new Tuple<int, int, int>(1, r, e2));
                        TrainNeighborLink[e2].Add(new Tuple<int, int, int>(-1, r, e1));

                        int Rr = r + RelationNum;

                        if (!TrainNeighborHash[e1].ContainsKey(r))
                        {
                            TrainNeighborHash[e1].Add(r, new HashSet<int>());
                        }
                        TrainNeighborHash[e1][r].Add(e2);

                        if (!TrainNeighborHash[e2].ContainsKey(Rr))
                        {
                            TrainNeighborHash[e2].Add(Rr, new HashSet<int>());
                        }
                        TrainNeighborHash[e2][Rr].Add(e1);


                        TrainGraphHash.Add(string.Format("{0}#{1}#{2}", e1, e2, r));
                    }
                    else
                    {
                        ValidGraphHash.Add(string.Format("{0}#{1}#{2}", e1, e2, r));
                    }
                    result.Add(new Tuple<int, int, int>(e1, e2, r));
                }
            }

            if (IsTrain)
            {
                TailProb = new double[RelationNum];
                HeadProb = new double[RelationNum];
                for (int i = 0; i < RelationNum; i++)
                {
                    TailProb[i] = RelationCount[i] * 1.0 / RelationTail[i].Count;
                    HeadProb[i] = RelationCount[i] * 1.0 / RelationHead[i].Count;
                }
            }
            return result;
        }


        public List<Tuple<int, int, Dictionary<int, float>>> LoadSignedGraph(string fileName)
        {
            List<Tuple<int, int, Dictionary<int, float>>> results = new List<Tuple<int, int, Dictionary<int, float>>>(); 
            using (StreamReader reader = new StreamReader(fileName))
            {
                while(!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] items = line.Trim().Split('\t');
                    int e1 = int.Parse(items[0]);
                    int rid = int.Parse(items[1]);

                    Dictionary<int, float> d = new Dictionary<int, float>();
                    string[] tokens = items[2].Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach(string token in tokens)
                    {
                        string[] kv = token.Trim().Split(':');
                        int e2 = int.Parse(kv[0]);
                        float l = float.Parse(kv[1]);
                        d.Add(e2, l);
                    }
                    results.Add(new Tuple<int, int, Dictionary<int, float>>(e1, rid, d));
                }
            }
            return results;
        }

        public List<Tuple<int, int, List<int>, List<float>>> LoadSignedGraphV2(string fileName)
        {
            List<Tuple<int, int, List<int>, List<float>>> results = new List<Tuple<int, int, List<int>, List<float>>>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] items = line.Trim().Split('\t');
                    int e1 = int.Parse(items[0]);
                    int rid = int.Parse(items[1]);

                    List<int> k = new List<int>();
                    List<float> v = new List<float>();
                    string[] tokens = items[2].Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string token in tokens)
                    {
                        string[] kv = token.Trim().Split(':');
                        int e2 = int.Parse(kv[0]);
                        float l = float.Parse(kv[1]);
                        k.Add(e2);
                        v.Add(l);
                    }
                    results.Add(new Tuple<int, int, List<int>, List<float>>(e1, rid, k, v));
                }
            }
            return results;
        }

        /// <summary>
        /// Entity Id1, relation, Entity Id2.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="eID"></param>
        /// <param name="rID"></param>
        /// <param name="IsTrain"></param>
        /// <returns></returns>
        public List<Tuple<int, int, int>> LoadGraphV3(string fileName, int type)
        {
            if (type == 0)
            {
                for (int i = 0; i < RelationNum; i++)
                {
                    RelationCount.Add(i, 0);
                    RelationHead.Add(i, new HashSet<int>());
                    RelationTail.Add(i, new HashSet<int>());
                }
                for (int i = 0; i < EntityNum; i++)
                {
                    TrainNeighborLink.Add(i, new List<Tuple<int, int, int>>());
                    TrainNeighborHash.Add(i, new Dictionary<int, HashSet<int>>());
                }
            }

            for (int i = 0; i < EntityNum; i++)
            {
                if (!AllNeighborHash.ContainsKey(i))
                {
                    AllNeighborHash.Add(i, new Dictionary<int, HashSet<int>>());
                }
            }

            List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] items = line.Trim().Split('\t');
                    int e1 = int.Parse(items[0]);
                    int r = int.Parse(items[1]);
                    int e2 = int.Parse(items[2]);

                    int Rr = r + RelationNum;

                    if (type == 0)
                    {
                        RelationCount[r] += 1;
                        RelationHead[r].Add(e1);
                        RelationTail[r].Add(e2);

                        TrainNeighborLink[e1].Add(new Tuple<int, int, int>(1, r, e2));
                        TrainNeighborLink[e2].Add(new Tuple<int, int, int>(-1, r, e1));
                    }

                    if(type == 0 || type == 1)
                    { 
                        if (!TrainNeighborHash[e1].ContainsKey(r))
                        {
                            TrainNeighborHash[e1].Add(r, new HashSet<int>());
                        }
                        TrainNeighborHash[e1][r].Add(e2);

                        if (!TrainNeighborHash[e2].ContainsKey(Rr))
                        {
                            TrainNeighborHash[e2].Add(Rr, new HashSet<int>());
                        }
                        TrainNeighborHash[e2][Rr].Add(e1);

                        TrainGraphHash.Add(string.Format("{0}#{1}#{2}", e1, e2, r));
                    }
                    

                    if(type == 1 || type == 2)
                    {
                        ValidGraphHash.Add(string.Format("{0}#{1}#{2}", e1, e2, r));
                    }

                    if (!AllNeighborHash[e1].ContainsKey(r))
                    {
                        AllNeighborHash[e1].Add(r, new HashSet<int>());
                    }
                    AllNeighborHash[e1][r].Add(e2);

                    if (!AllNeighborHash[e2].ContainsKey(Rr))
                    {
                        AllNeighborHash[e2].Add(Rr, new HashSet<int>());
                    }
                    AllNeighborHash[e2][Rr].Add(e1);

                    result.Add(new Tuple<int, int, int>(e1, e2, r));
                }
            }

            if (type == 0)
            {
                TailProb = new double[RelationNum];
                HeadProb = new double[RelationNum];
                for (int i = 0; i < RelationNum; i++)
                {
                    TailProb[i] = RelationCount[i] * 1.0 / RelationTail[i].Count;
                    HeadProb[i] = RelationCount[i] * 1.0 / RelationHead[i].Count;
                }
            }
            return result;
        }


        public List<Tuple<int, int, int>> LoadGraphV4(string fileName)
        {

            for (int i = 0; i < RelationNum; i++)
            {
                RelationCount.Add(i, 0);
                RelationHead.Add(i, new HashSet<int>());
                RelationTail.Add(i, new HashSet<int>());
            }
            for (int i = 0; i < EntityNum; i++)
            {
                TrainNeighborLink.Add(i, new List<Tuple<int, int, int>>());
                TrainNeighborHash.Add(i, new Dictionary<int, HashSet<int>>());
                AllNeighborHash.Add(i, new Dictionary<int, HashSet<int>>());
            }

            List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] items = line.Trim().Split('\t');
                    int e1 = int.Parse(items[0]);
                    int r = int.Parse(items[1]);
                    int e2 = int.Parse(items[2]);

                    RelationCount[r] += 1;
                    RelationHead[r].Add(e1);
                    RelationTail[r].Add(e2);

                    TrainNeighborLink[e1].Add(new Tuple<int, int, int>(1, r, e2));
                    TrainGraphHash.Add(string.Format("{0}#{1}#{2}", e1, e2, r));
                    result.Add(new Tuple<int, int, int>(e1, e2, r));

                    if (!TrainNeighborHash[e1].ContainsKey(r))
                    {
                        TrainNeighborHash[e1].Add(r, new HashSet<int>());
                    }
                    TrainNeighborHash[e1][r].Add(e2);

                    if (!AllNeighborHash[e1].ContainsKey(r))
                    {
                        AllNeighborHash[e1].Add(r, new HashSet<int>());
                    }
                    AllNeighborHash[e1][r].Add(e2);
                }
            }

            TailProb = new double[RelationNum];
            HeadProb = new double[RelationNum];
            for (int i = 0; i < RelationNum; i++)
            {
                TailProb[i] = RelationCount[i] * 1.0 / RelationTail[i].Count;
                HeadProb[i] = RelationCount[i] * 1.0 / RelationHead[i].Count;
            }
            return result;
        }


        public List<Tuple<int, int, int>> LoadGraphV5(string fileName)
        {
            for (int i = 0; i < RelationNum; i++)
            {
                RelationCount.Add(i, 0);
                RelationHead.Add(i, new HashSet<int>());
                RelationTail.Add(i, new HashSet<int>());
            }

            for (int i = 0; i < EntityNum; i++)
            {
                TrainNeighborLink.Add(i, new List<Tuple<int, int, int>>());
                TrainNeighborHash.Add(i, new Dictionary<int, HashSet<int>>());
            }

            //for (int i = 0; i < EntityNum; i++)
            //{
            //    AllNeighborHash.Add(i, new Dictionary<int, HashSet<int>>());
            //}

            List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] items = line.Trim().Split('\t');
                    int e1 = int.Parse(items[0]);
                    int r = int.Parse(items[1]);
                    int e2 = int.Parse(items[2]);

                    RelationCount[r] += 1;
                    RelationHead[r].Add(e1);
                    RelationTail[r].Add(e2);

                    TrainNeighborLink[e1].Add(new Tuple<int, int, int>(1, r, e2));
                    TrainGraphHash.Add(string.Format("{0}#{1}#{2}", e1, e2, r));
                    result.Add(new Tuple<int, int, int>(e1, e2, r));

                    if (!TrainNeighborHash[e1].ContainsKey(r))
                    {
                        TrainNeighborHash[e1].Add(r, new HashSet<int>());
                    }
                    TrainNeighborHash[e1][r].Add(e2);

                    //if (!AllNeighborHash[e1].ContainsKey(r))
                    //{
                    //    AllNeighborHash[e1].Add(r, new HashSet<int>());
                    //}
                    //AllNeighborHash[e1][r].Add(e2);
                }
            }

            TailProb = new double[RelationNum];
            HeadProb = new double[RelationNum];
            for (int i = 0; i < RelationNum; i++)
            {
                TailProb[i] = RelationCount[i] * 1.0 / RelationTail[i].Count;
                HeadProb[i] = RelationCount[i] * 1.0 / RelationHead[i].Count;
            }
            return result;
        }
    }
}
