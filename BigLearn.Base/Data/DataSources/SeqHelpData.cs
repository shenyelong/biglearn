﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// this class will be staled soon.
    /// </summary>
    // public class SeqHelpData : BatchData<SequenceDataStat>
    // {
    //     public CudaPieceInt TransSeqIndex;
    //     public int[] TransSeqIndexMem;

    //     public int[] LagSeqIndex;

    //     public CudaPieceInt LagSeqElement;
    //     public int[] LagSeqElementMem;

    //     public CudaPieceInt LagForward;
    //     public CudaPieceInt LagBackward;

    //     public CudaPieceInt LagSeqPrevious;

    //     public CudaPieceInt Lag1SourceIndex;
    //     public CudaPieceInt Lag1TargetIndex;
    //     public int Lag1Length;

    //     public int MaxLag;
    //     public int[] SampleIdx;

    //     public override DeviceType DeviceType { get; set; }

    //     public SeqHelpData(SequenceDataStat stat, DeviceType device)
    //     {
    //         Stat = stat;
    //         DeviceType = device;
    //         TransSeqIndex = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, true, device == DeviceType.GPU);

    //         LagSeqIndex = new int[Stat.MAX_SEQUENCESIZE];

    //         LagSeqElement = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, true, device == DeviceType.GPU);
    //         LagSeqPrevious = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, true, device == DeviceType.GPU);

    //         Lag1SourceIndex = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, true, device == DeviceType.GPU);
    //         Lag1TargetIndex = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, true, device == DeviceType.GPU);

    //         LagForward = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, true, device == DeviceType.GPU);
    //         LagBackward = new CudaPieceInt(Stat.MAX_SEQUENCESIZE, true, device == DeviceType.GPU);

    //         TransSeqIndexMem = TransSeqIndex.MemPtr;
    //         LagSeqElementMem = LagSeqElement.MemPtr;
    //     }

    //     public void Process(CudaPieceInt smpIdx, int batchSize, int sentSize, bool isReverse = true)
    //     {
    //         smpIdx.SyncToCPU(batchSize);
    //         SampleIdx = smpIdx.MemPtr;

    //         if (!isReverse) { TransSeqIndexMem = null; return; }

    //         /// length for each sequence.
    //         KeyValuePair<int, int>[] seqLen = new KeyValuePair<int, int>[batchSize];
    //         for (int i = 0; i < batchSize; i++)
    //         {
    //             seqLen[i] = new KeyValuePair<int, int>(i, i == 0 ? SampleIdx[i] : (SampleIdx[i] - SampleIdx[i - 1]));
    //         }
    //         MaxLag = seqLen.Select(i => i.Value).Max();

    //         int time = 0;
    //         int elementCount = 0;
    //         int lag1Index = 0;
    //         int[] preTSet = null;
    //         for (int lag = 0; lag < MaxLag; lag++)
    //         {
    //             IEnumerable<KeyValuePair<int, int>> result = seqLen.Where(i => i.Value > lag);
    //             int[] tSet = result.Select(i => i.Value - lag - 1).ToArray();
    //             int[] avSet = result.Select(i => i.Key).ToArray();

    //             foreach (int av in avSet)
    //             {
    //                 LagForward.MemPtr[time] = (av == 0 ? 0 : SampleIdx[av - 1]) + lag;
    //                 if (lag > 0) LagBackward.MemPtr[time] = (av == 0 ? 0 : SampleIdx[av - 1]) + lag - 1;
    //                 TransSeqIndex.MemPtr[(av == 0 ? 0 : SampleIdx[av - 1]) + lag] = time++;
    //             }

    //             //if (Output.TransSeqIndex.MemPtr[0] > 10) { Console.ReadLine(); }
    //             LagSeqIndex[lag] = lag == 0 ? avSet.Length : (avSet.Length + LagSeqIndex[lag - 1]);

    //             Buffer.BlockCopy(avSet, 0, LagSeqElement.MemPtr, elementCount * sizeof(int), avSet.Length * sizeof(int));

    //             if (lag > 0)
    //             {
    //                 int melement = elementCount;
    //                 for (int i = 0; i < preTSet.Length; i++)
    //                 {
    //                     if (preTSet[i] > 0)
    //                     {
    //                         Lag1TargetIndex.MemPtr[lag1Index] = elementCount;
    //                         Lag1SourceIndex.MemPtr[lag1Index] = melement - preTSet.Length + i;
    //                         lag1Index++;

    //                         LagSeqPrevious.MemPtr[elementCount++] = i;
    //                     }
    //                 }
    //             }
    //             else
    //             {
    //                 elementCount = avSet.Length;
    //             }

    //             preTSet = tSet;
    //         }

    //         Lag1Length = elementCount - LagSeqIndex[0];

    //         if (Lag1Length != lag1Index)
    //         {
    //             Logger.WriteLog("Error !! Preprocess Sequence Data  Length does n't match {0}, {1}. Stop here!", lag1Index, Lag1Length);
    //             Console.ReadLine();
    //         }


    //         TransSeqIndex.SyncFromCPU(sentSize);
    //         LagSeqElement.SyncFromCPU(sentSize);

    //         LagSeqPrevious.SyncFromCPU(sentSize);
    //         Lag1SourceIndex.SyncFromCPU(Lag1Length);
    //         Lag1TargetIndex.SyncFromCPU(Lag1Length);

    //         LagForward.SyncFromCPU(sentSize);
    //         LagBackward.SyncFromCPU(sentSize);
    //     }
    // }
}
