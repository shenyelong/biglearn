﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum DataSourceID : int
    {
        BatchData = 0,
        SourceBatchData,
        DenseBatchData,
        HiddenBatchData,
        SeqBatchInputData,

        NdArrayData,
        MatrixData,
        VectorData,
        Tensor4DData,
        Tensor3DData,
        SeqVectorData,
        SeqMatrixData,
        SparseMatrixData,
        SparseVectorData,
        SparseMatrixCSR,
        
        SeqDenseBatchData,
        SeqSparseBatchData,
        SeqIndexBatchData,

        SeqSentenceBatchInputData,
        SeqSentenceBatchInputLabelData,
        BatchInputLabelData,
        GeneralBatchInputData,
        SeqBatchInputLabelData,
        DSSMData,
        JDSSMData,
        ImageData,
        ImageLabelData,
        TensorConvStructure,
        
        SeqSparseDataSource,
        SequenceSlotData,

        SeqDenseSlotBatchData,
        SeqSparseSlotBatchData,

        GeneralSequenceData,
        PairGeneralSequenceData,
        PairGeneralSequenceDataSource,

        MetaInfo = 1024,
        SequenceDataStat = 1025,
        ModelMetaInfo = 1026,
        DSSMDataMeta = 1027,
        DenseDataMeta = 1028,
        CheckPointInfo = 1029,
        SeqSentenceDataMeta = 1030,
        MatrixDataMeta = 1031,
        VectorDataMeta = 1032,
        SeqVectorDataMeta = 1033,
        SeqMatrixDataMeta = 1034,

        LayerStructure = 4096,
        NormLayerStructure = 4097,
        LSTMStructure = 4098,
        MemoryStructure = 4099,
        EmbedStructure = 4100,
        MLPAttentionStructure = 4101,
        DNNStructure = 4102,
        ConvStructure = 4103,
        GRNNStructure = 4104,
        GRUCell = 4105,
        GRUStructure = 4106,
        RLLayerStructure = 4107,
        RLImageLinkStructure = 4108,
        MatrixStructure = 4109,
        LSTMCell = 4110,
        BanditLayerStructure = 4111,
        ImageLinkStructure = 4112,
        GRUCellV2 = 4113,
        TransformerStructure = 4114,
        RRUCell = 4115,
        RANStructure = 4116,
        SelfAttPool = 4117,
        DynamicMemoryStructure = 4118,
        
        ExternalTransformerStructure = 4119,
        AttentionTransformer = 4120,
    }
}
