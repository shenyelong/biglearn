﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Basic Image Data. Format : NCHW. 
    /// </summary>
    public class ImageDataSource : BatchData //<ImageDataStat>
    {
        public int Width { get { return Shape[0].Default; } set { Shape[0].Default = value; } }
        public int Height { get { return Shape[1].Default; } set { Shape[1].Default = value; } }
        public int Depth { get { return Shape[2].Default; } set { Shape[2].Default = value; } }
        public bool IsSource { get; set; }
        public int MAX_BATCHSIZE { get { return Shape[3].Default; } set { Shape[3].Default = value; } }

        public override DataSourceID Type { get { return DataSourceID.ImageData; } }
        
        public CudaPieceFloat Deriv;
        public CudaPieceFloat Data;
        public int Size { get { return Data.EffectiveSize; } }
        
        public int BatchSize
        {
            get
            {
                return Data.EffectiveSize / (Depth * Width * Height);
            }

            set
            {
                Data.EffectiveSize = value * Width * Height * Depth;
                if(Deriv != null && Deriv != CudaPieceFloat.Empty) Deriv.EffectiveSize = value * Width * Height * Depth;
                if (MAX_BATCHSIZE < value) MAX_BATCHSIZE = value;
                Shape[3].Value = value;
            }
        }
        
        //public ImageDataSource() { }
        //public ImageDataSource(DataStat stat, DeviceType device) : 
        //    this(stat.Args["MAX_BATCHSIZE"], stat.Args["WIDTH"], stat.Args["HEIGHT"], stat.Args["DEPTH"], true, device) 
        //{
        //    Stat = stat;
            //base(stat, device); 
        //}

        // public override void Init(ImageDataStat stat, DeviceType deviceType)
        // {
        //     base.Init(stat, deviceType);
        //     Data = new CudaPieceFloat(Stat.MAX_BATCHSIZE * Stat.Width * Stat.Height * Stat.Depth, true, deviceType == DeviceType.GPU);
        //     if (!IsSource) { Deriv = new CudaPieceFloat(Stat.MAX_BATCHSIZE * Stat.Width * Stat.Height * Stat.Depth, true, deviceType == DeviceType.GPU); }
        //     else { Deriv = CudaPieceFloat.Empty; }
        // }

        public ImageDataSource(int maxBatchSize, int width, int height, int depth, DeviceType device): 
            this(maxBatchSize, width, height, depth, false, device)
        { }

        public ImageDataSource(int maxBatchSize, int width, int height, int depth, bool isSource, DeviceType device) 
            : base(device, new IntArgument("WIDTH", width), new IntArgument("HEIGHT", height),
                           new IntArgument("DEPTH", depth), new IntArgument("MAX_BATCHSIZE", maxBatchSize))
        {
            MAX_BATCHSIZE = maxBatchSize;
            Width = width;
            Height = height;
            Depth = depth; 
            IsSource = isSource;
            
            DeviceType = device;

            Data = new CudaPieceFloat(MAX_BATCHSIZE * Width * Height * Depth, device);
            if (!IsSource) { Deriv = new CudaPieceFloat(MAX_BATCHSIZE * Width * Height * Depth, device); }
            else { Deriv = null; }
        }
        

        public override void CopyFrom(IData other)
        {
            ImageDataSource source = (ImageDataSource)other;
            BatchSize = source.BatchSize;
            Data.CopyFrom(source.Data);
            //Data.CopyFrom(0, source.Data, 0, source.BatchSize * source.Stat.Width * source.Stat.Height * source.Stat.Depth);
        }

        public override void Deserialize(BinaryReader reader)
        {
            BatchSize = reader.ReadInt32();
            Data.Deserialize(reader);
            //Data.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize * Stat.Width * Stat.Height * Stat.Depth), 0,
            //                sizeof(float) * BatchSize * Stat.Width * Stat.Height * Stat.Depth);
            //Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * BatchSize * Stat.Width * Stat.Height * Stat.Depth), 0,
            //                 Data.MemPtr, 0, sizeof(float) * BatchSize * Stat.Width * Stat.Height * Stat.Depth);
        }

        /// <summary>
        /// Do not change the function Name.
        /// </summary>
        /// <param name="reader"></param>
        // public override void SkipBlock(BinaryReader reader)
        // {
        //     int b = reader.ReadInt32();
        //     CudaPieceFloat.Skip(reader);
        //     //reader.BaseStream.Seek((b * imageStat.Width * imageStat.Height * imageStat.Depth) * sizeof(float), SeekOrigin.Current);
        // }

        public override void Serialize(BinaryWriter writer)
        {
            if (BatchSize == 0) return;
            writer.Write(BatchSize);
            Data.Serialize(writer);
            //    for (int i = 0; i < BatchSize * Stat.Width * Stat.Height * Stat.Depth; ++i)
            //        writer.Write(Data[i]);
            
        }

        // public void PushSample(float[] value, int batchNum)
        // {
        //     int oldBatchSize = BatchSize;
        //     BatchSize = oldBatchSize + batchNum;
        //     Data.SyncFromCPU(oldBatchSize * Stat.Width * Stat.Height * Stat.Depth, value, 0, Stat.Width * Stat.Height * Stat.Depth * batchNum);
        //     //SyncToCPU(int offset, float[] data, int offsetdata, int length);
        //     //Array.Copy(value, 0, Data.MemPtr, oldBatchSize * Stat.Width * Stat.Height * Stat.Depth, Stat.Width * Stat.Height * Stat.Depth * batchNum);
        // }

        public void Clear()
        {
            BatchSize = 0;
        }

    }
    
}
