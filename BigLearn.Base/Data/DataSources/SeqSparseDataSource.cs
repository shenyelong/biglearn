﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class SeqSparseDataSource : BatchData<SequenceDataStat>
    // {
    //     public override DataSourceID Type { get { return DataSourceID.SeqSparseDataSource; } }

    //     public override int BatchSize { get { return SequenceData.BatchSize; } set { SequenceData.BatchSize = value; InstanceLabel.EffectiveSize = value; } }
    //     public int SeqSize { get { return SequenceData.SentSize; } set { SequenceData.SentSize = value; SequenceLabel.EffectiveSize = value; } }
    //     public int ElementSize { get { return SequenceData.ElementSize; } set { SequenceData.ElementSize = value; } }

    //     /// <summary>
    //     /// Instance 1 : 7 time steps;
    //     /// Instance 2 : 9 time steps;
    //     /// SampleIdx: [7, 16, ...., N]
    //     /// SequenceIdex : 
    //     /// SequenceLabel : label of time step;
    //     /// </summary>
    //     public CudaPieceInt SampleIdx { get { return SequenceData.SampleIdx; } set { SequenceData.SampleIdx = value; } }
    //     public CudaPieceInt SequenceIdx { get { return SequenceData.SequenceIdx; } set { SequenceData.SequenceIdx = value; } }
    //     public CudaPieceInt FeaIdx { get { return SequenceData.FeaIdx; } set { SequenceData.FeaIdx = value; } }
    //     public CudaPieceFloat FeaValue { get { return SequenceData.FeaValue; } set { SequenceData.FeaValue = value; } }

    //     public SeqSparseBatchData SequenceData;
    //     public CudaPieceFloat SequenceLabel; // Label Per Time Step.
    //     public CudaPieceFloat InstanceLabel; // Label Per Instance.

    //     public SeqSparseDataSource() { }
    //     public SeqSparseDataSource(SequenceDataStat stat, DeviceType deviceType) : base(stat, deviceType) { }
    //     //public SeqSparseDataSource(IMetaInfo meta, DeviceType deviceType)
    //     //            : this(new IOUnitHeader(DataSourceID.SeqSparseDataSource), meta, deviceType)
    //     //{ }
    //     // public SeqSparseDataSource(IOUnitHeader head, IMetaInfo meta, DeviceType deviceType) : this((SequenceDataStat)meta, deviceType)
    //     // {
    //     //     this.Head = head;
    //     // }

    //     public override void Init(SequenceDataStat stat, DeviceType deviceType)
    //     {
    //         base.Init(stat, deviceType);
    //         SequenceData = new SeqSparseBatchData(stat, deviceType);
    //         SequenceLabel = new CudaPieceFloat(Stat.MAX_SEQUENCESIZE, deviceType);
    //         InstanceLabel = new CudaPieceFloat(Stat.MAX_BATCHSIZE, deviceType);
    //     }
       
    //     /// <summary>
    //     /// Data Load from Stream.
    //     /// </summary>
    //     /// <param name="reader"></param>
    //     public unsafe override void Load(BinaryReader reader)
    //     {
    //         SequenceData.BatchSize = reader.ReadInt32();
    //         SeqSize = reader.ReadInt32();
    //         ElementSize = reader.ReadInt32();

    //         Buffer.BlockCopy(reader.ReadBytes(sizeof(int) * BatchSize), 0, SampleIdx.MemPtr, 0, sizeof(int) * BatchSize);
    //         Buffer.BlockCopy(reader.ReadBytes(sizeof(int) * SeqSize), 0, SequenceIdx.MemPtr, 0, sizeof(int) * SeqSize);
    //         //Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * SeqSize), 0, SequenceLabel.MemPtr, 0, sizeof(float) * SeqSize);
    //         SequenceLabel.BlockCopy(reader.ReadBytes(sizeof(float) * SeqSize), 0,  sizeof(float) * SeqSize);
    //         Buffer.BlockCopy(reader.ReadBytes(sizeof(int) * ElementSize), 0, FeaIdx.MemPtr, 0, sizeof(int) * ElementSize);
    //         //Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0, FeaValue.MemPtr, 0, sizeof(float) * ElementSize);
    //         FeaValue.BlockCopy(reader.ReadBytes(sizeof(float) * ElementSize), 0,  sizeof(float) * ElementSize);
    //         BasicMathlib.LastTimeStepExtractSeqMatrix(SampleIdx.CpuPtr, BatchSize, SequenceLabel.CpuPtr, 1, InstanceLabel.CpuPtr);
    //         for (int batch = 0; batch < BatchSize; batch++)
    //             for (int seg = batch == 0 ? 0 : SampleIdx[batch - 1]; seg < SampleIdx[batch]; seg++)
    //                 SequenceData.SentMargin[seg] = batch;

    //         SampleIdx.SyncFromCPU(BatchSize);
    //         SequenceIdx.SyncFromCPU(SeqSize);
    //         SequenceLabel.SyncFromCPU(SeqSize);
    //         FeaIdx.SyncFromCPU(ElementSize);
    //         FeaValue.SyncFromCPU(ElementSize);
    //         InstanceLabel.SyncFromCPU(BatchSize);
    //         SequenceData.SentMargin.SyncFromCPU(SeqSize);
    //     }
    //     public override void Save(BinaryWriter writer)
    //     {
    //         if (BatchSize > 0)
    //         {
    //             writer.Write(BatchSize);
    //             writer.Write(SeqSize);
    //             writer.Write(ElementSize);

    //             SampleIdx.SyncToCPU(BatchSize);
    //             SequenceIdx.SyncToCPU(SeqSize);
    //             SequenceLabel.SyncToCPU(SeqSize);
    //             FeaIdx.SyncToCPU(ElementSize);
    //             FeaValue.SyncToCPU(ElementSize);

    //             for (int i = 0; i < BatchSize; ++i) writer.Write(SampleIdx[i]);
    //             for (int i = 0; i < SeqSize; ++i) writer.Write(SequenceIdx[i]);
    //             for (int i = 0; i < SeqSize; ++i) writer.Write(SequenceLabel[i]);
    //             for (int i = 0; i < ElementSize; ++i) writer.Write(FeaIdx[i]);
    //             for (int i = 0; i < ElementSize; ++i) writer.Write(FeaValue[i]);
    //         }
    //     }

    //     public override void Clear()
    //     {
    //         SequenceData.BatchSize = 0;
    //         SequenceData.SentSize = 0;
    //         SequenceData.ElementSize = 0;
    //     }


    //     public override void SkipBlock(BinaryReader reader, DataStat stat)
    //     {
    //         int b = reader.ReadInt32();
    //         int s = reader.ReadInt32();
    //         int e = reader.ReadInt32();
    //         reader.BaseStream.Seek((b + s + e) * sizeof(int) + (e + s) * sizeof(float), SeekOrigin.Current);
    //     }

    //     public void PushSample(List<Dictionary<int, float>> sequenceData, float defaultLabel = 0)
    //     {
    //         PushSample(sequenceData.Select(i => new Tuple<Dictionary<int, float>, float>(i, defaultLabel)).ToList());
    //     }
    //     public void PushSample(List<Tuple<Dictionary<int, float>, float>> sequenceData, float defaultLabel = 0)
    //     {
    //         int oldBatchSize = BatchSize;
    //         int oldSentSize = SeqSize;
    //         int oldElementSize = ElementSize;

    //         BatchSize = oldBatchSize + 1;
    //         SeqSize = oldSentSize + sequenceData.Count;
    //         ElementSize = oldElementSize + sequenceData.Select(i => i.Item1.Count).Sum();
            
    //         SequenceData.SampleIdx[oldBatchSize] = SeqSize;
    //         if (sequenceData.Count == 0)
    //         {
    //             InstanceLabel[oldBatchSize] = defaultLabel;
    //         }
    //         else
    //         {
    //             InstanceLabel[oldBatchSize] = sequenceData.Last().Item2;
    //         }
    //         int seqcursor = oldSentSize;
    //         int elementcursor = oldElementSize;

    //         foreach (Tuple<Dictionary<int, float>, float> seg in sequenceData)
    //         {
    //             SequenceData.SequenceIdx[seqcursor] = elementcursor + seg.Item1.Count;
    //             SequenceData.SentMargin[seqcursor] = oldBatchSize;
    //             foreach (KeyValuePair<int, float> fv in seg.Item1)
    //             {
    //                 SequenceData.FeaIdx[elementcursor] = fv.Key;
    //                 SequenceData.FeaValue[elementcursor] = fv.Value;
    //                 elementcursor++;

    //                 if (fv.Key >= Stat.FEATURE_DIM) Stat.FEATURE_DIM = fv.Key + 1;
    //             }
    //             SequenceLabel[seqcursor] = seg.Item2;
    //             seqcursor ++;
    //         }

    //         SampleIdx.SyncFromCPU(oldBatchSize, 1);
    //         InstanceLabel.SyncFromCPU(oldBatchSize, 1);

    //         SequenceIdx.SyncFromCPU(oldSentSize, SeqSize - oldSentSize);
    //         SequenceData.SentMargin.SyncFromCPU(oldSentSize, SeqSize - oldSentSize);
    //         SequenceLabel.SyncFromCPU(oldSentSize, SeqSize - oldSentSize);

    //         FeaIdx.SyncFromCPU(oldElementSize, ElementSize - oldElementSize);
    //         FeaValue.SyncFromCPU(oldElementSize, ElementSize - oldElementSize);
    //     }

    //     // public override IData CloneAs(DeviceType deviceType)
    //     // {
    //     //     SeqSparseDataSource ss = new SeqSparseDataSource(this.Head, this.Stat, deviceType);
    //     //     ss.CopyFrom(this);
    //     //     return ss;
    //     // }

    //     public override void CopyFrom(IData other)
    //     {
    //         SeqSparseDataSource otherData = (SeqSparseDataSource)other;
    //         SequenceData.CopyFrom(otherData.SequenceData);
    //         SequenceLabel.CopyFrom(otherData.SequenceLabel);
    //         InstanceLabel.CopyFrom(otherData.InstanceLabel);
    //     }
    // }
}
