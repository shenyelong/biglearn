﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class HiddenBatchData : BatchData
    {
        public DenseBatchData Output = null;
        public DenseBatchData Deriv = null;
        public override DataSourceID Type { get { return DataSourceID.HiddenBatchData; } }

        public int MAX_BATCHSIZE { get { return Shape[1].Default; } set { Shape[1].Default = value; } }
        public int Dim { get { return Shape[0].Default; } set { Shape[0].Default = value; } }
        public IntArgument ShapeBatchSize { get { return Shape[1]; }}

        public int BatchSize
        {
            get
            {
                int new_batchsize = ShapeBatchSize.Value;
                int old_batch = Output != null ?  Output.BatchSize : 0;

                return (old_batch > new_batchsize) ? new_batchsize : old_batch;
                //return ShapeBatchSize.Value;
                //if (Output != null) return Output.BatchSize;
                //if (Deriv != null) return Deriv.BatchSize;
                //return 0;
            }
            set
            {
                if (MAX_BATCHSIZE < value) { throw new Exception(string.Format("the batch size {0} is larger than MAX_BATCHSIZE {1}", value, MAX_BATCHSIZE)); }
                if (Output != null) Output.BatchSize = value;
                if (Deriv != null) Deriv.BatchSize = value;
                ShapeBatchSize.Value = value;
                //Shape[1].Value = value;
            }
        }

        public HiddenBatchData(int maxBatchSize, int dim, DNNRunMode mode, DeviceType device) : this(maxBatchSize, dim, new CudaPieceFloat(maxBatchSize * dim, device), new CudaPieceFloat(maxBatchSize * dim, device), device) { }
        public HiddenBatchData(int maxBatchSize, int dim, bool isDeriv, DeviceType device) : this(maxBatchSize, dim, new CudaPieceFloat(maxBatchSize * dim, device), isDeriv ? new CudaPieceFloat(maxBatchSize * dim, device) : null, device) { }
        
        public HiddenBatchData(MatrixData data): this(data.MaxRow, data.Column, data.Output, data.Deriv, data.DeviceType) { }
        public HiddenBatchData(VectorData data) : this(data.MaxLength, 1, data.Output, data.Deriv, data.DeviceType) { }
        public HiddenBatchData(EmbedStructure data) : this(data.VocabSize, data.Dim, data.Embedding, data.EmbeddingGrad, data.DeviceType) { }
        
        public HiddenBatchData(int maxBatchSize, int dim, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device)
            : this(device, data, deriv, new IntArgument("DIM", dim), new IntArgument("MAX_BATCHSIZE", maxBatchSize))
        {
            //MAX_BATCHSIZE = maxBatchSize;
            //Dim = dim;
            //DeviceType = device;
            //ShapeBatchSize = new IntArgument("BatchSize", maxBatchSize);
            //Output = new DenseBatchData(maxBatchSize, dim, data, device);
            //if(deriv != null) Deriv = new DenseBatchData(maxBatchSize, dim, deriv, device);
        }

        public HiddenBatchData(DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] args)
            : base(device, args)
        {
            Output = new DenseBatchData(MAX_BATCHSIZE, Dim, data, device);
            if(deriv != null) Deriv = new DenseBatchData(MAX_BATCHSIZE, Dim, deriv, device);
        }

        public override void SyncToCPU()
        {
            Output.SyncToCPU();
            Deriv.SyncToCPU();
        }


    }
}
