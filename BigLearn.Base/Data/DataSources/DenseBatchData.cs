﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    public class DenseBatchData : BatchData //<DenseDataStat>
    {
        public int Dim { get { return Shape[0].Default; } set { Shape[0].Default = value; } }
        public int MAX_BATCHSIZE { get { return Shape[1].Default; } set { Shape[1].Default = value; } }

        #region main data region.
        public CudaPieceFloat Data;
        #endregion.

        public override DataSourceID Type { get { return DataSourceID.DenseBatchData; } }

        public int BatchSize
        {
            get { return Data.EffectiveSize == 0 ? 0 : Data.EffectiveSize / Dim; }
            set { Data.EffectiveSize = value * Dim; if (MAX_BATCHSIZE < value) MAX_BATCHSIZE = value; Shape[1].Value = value;  }
        }

        //public DenseBatchData(DenseDataStat stat, DeviceType device) : base(stat, device) { }
        
        public DenseBatchData(int maxBatchSize, int dim, DeviceType device) 
            : this(maxBatchSize, dim, new CudaPieceFloat(maxBatchSize * dim, device), device)
        { }

        public DenseBatchData(int maxBatchSize, int dim, CudaPieceFloat data, DeviceType device) 
            : base( device, new IntArgument("DIM", dim), new IntArgument("MAX_BATCHSIZE", maxBatchSize))
        {
            DeviceType = device;
            MAX_BATCHSIZE = maxBatchSize;
            Dim = dim;
            Data = data; 
        }

        public override void Deserialize(BinaryReader reader)
        {
            BatchSize = reader.ReadInt32();
            Dim = reader.ReadInt32();
            Data.Deserialize(reader);
        }

        public override void Serialize(BinaryWriter writer)
        {
            if(BatchSize == 0) return;
            writer.Write(BatchSize);
            writer.Write(Dim);
            Data.Serialize(writer);
        }

        public override void SyncFromCPU()
        {
            Data.SyncFromCPU(BatchSize * Dim);
        }

        public override void SyncToCPU()
        {
            Data.SyncToCPU(BatchSize * Dim);
        }

        public override void CopyFrom(IData other)
        {
            DenseBatchData src = ((DenseBatchData)other);
            {
                BatchSize = src.BatchSize;
                Data.CopyFrom(src.Data);
            }
        }
    }
}
