using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class VectorData : BatchData
    {
        public CudaPieceFloat Output = null;
        public CudaPieceFloat Deriv = null;
        public override DataSourceID Type { get { return DataSourceID.VectorData; } }

        public int MaxLength { get { return Shape[0].Default; } set { Shape[0].Default = value; } }
        public virtual int Length
        {
            get { return Output.EffectiveSize; }
            set
            {
                if (value > MaxLength) throw new Exception(string.Format("Length {0} should be smaller than Max Length {1}", value, MaxLength));
                Output.EffectiveSize = value ;
                if(Deriv != null && !Deriv.IsEmpty) Deriv.EffectiveSize = value;
                VectorDim.Value = value;
            }
        }

        public IntArgument VectorDim { get { return Shape[0]; } }

        public VectorData(int maxLength, DeviceType device) 
            : this(maxLength, true, device) 
        { }

        public VectorData(int maxLength, bool requireGrad, DeviceType device) 
            : this(maxLength, 
                new CudaPieceFloat(maxLength, true, device == DeviceType.GPU), 
                requireGrad ? new CudaPieceFloat(maxLength, true, device == DeviceType.GPU) : null,
                device)
        { }

        public VectorData(HiddenBatchData data) 
            : this(data.Dim * data.MAX_BATCHSIZE, data.Output.Data, data.Deriv.Data, data.DeviceType)
        { }


        public VectorData(int maxLength, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) : 
            base(device, new IntArgument("MAX_LENGTH", maxLength))
        {
            Output = data;
            Deriv = deriv;
            MaxLength = maxLength;
            DeviceType = device;

            //VectorDim = new IntArgument("dim", MaxLength);
        }

        public override void SyncToCPU()
        {
            Output.SyncToCPU();
            if(Deriv != null && !Deriv.IsEmpty) 
                Deriv.SyncToCPU();
        }
    }
}
