using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class IntsFlatRunner : StructRunner
    {
        public IntArgument[] Args;
        
        public IntArgument Output;
        
        public IntsFlatRunner(RunnerBehavior behavior, params IntArgument[] args) : base(Structure.Empty, behavior)
        {
            Args = args;
            
            int s_d = 1;
            foreach(IntArgument i in Args) s_d = s_d * i.Default;

            Output = new IntArgument("flat", s_d);
        }

        public override void Forward() 
        {
            int s_v = 1;
            foreach(IntArgument i in Args) s_v = s_v * i.Value;
            Output.Value = s_v; 
        }
    }

    public class IntsDividRunner : StructRunner
    {
        public IntArgument Arg_A;
        public IntArgument Arg_B;
            
        public IntArgument Output;
        
        public IntsDividRunner(RunnerBehavior behavior, IntArgument arg_a, IntArgument arg_b) : base(Structure.Empty, behavior)
        {
            Arg_A = arg_a;
            Arg_B = arg_b;

            Output = new IntArgument("Divid", Arg_A.Default / Arg_B.Default);
        }

        public override void Forward() 
        {
            Output.Value = Arg_A.Value / Arg_B.Value; 
        }
    }

    public static class ShapeConvertor  
    {       
        public static IntArgument Flat(this List<StructRunner> session, RunnerBehavior behavior, params IntArgument[] args)
        {
            IntsFlatRunner runner = new IntsFlatRunner(behavior, args);
            session.Add(runner);
            return runner.Output;
        }

        public static IntArgument Divid(this List<StructRunner> session, RunnerBehavior behavior, IntArgument arg_a, IntArgument arg_b)
        {
            IntsDividRunner runner = new IntsDividRunner(behavior, arg_a, arg_b);
            session.Add(runner);
            return runner.Output;
        }
           
    }
}
