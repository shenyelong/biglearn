﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SparseVectorData : BatchData
    {
        public override DataSourceID Type { get { return DataSourceID.SparseVectorData; } }

        public CudaPieceInt Idx;
        VectorData Context { get; set; }

        public CudaPieceFloat Value { get { return Context.Output; } }
        public CudaPieceFloat Output { get { return Context.Output; } set { Context.Output = value; } }
        public CudaPieceFloat Deriv { get { return Context.Deriv; } set { Context.Deriv = value; } }
        
        public int MaxLength { get { return Shape[0].Default; } set { Shape[0].Default = value; Context.MaxLength = value; } }
        public int Length
        {
            get
            {
                return Context.Length;
            }
            set
            {
                Context.Length = value;
                Idx.EffectiveSize = value;
                Shape[0].Value = value;
            }
        }

        public SparseVectorData(int maxLength, DeviceType device, bool isDeriv = true) : this(maxLength,
            new CudaPieceInt(maxLength, device), 
            new CudaPieceFloat(maxLength, device), 
            isDeriv ? new CudaPieceFloat(maxLength, device) : null, device)
        {
        }

        public SparseVectorData(int maxLength, CudaPieceInt idx, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) : 
            base(device, new IntArgument("MAXLENGTH", maxLength))
        {
            Context = new VectorData(maxLength, data, deriv, device);
            //maxLength, data, deriv, 
            Idx = idx;
        }
        
        public override void SyncFromCPU()
        {
            Idx.SyncFromCPU();
            Value.SyncFromCPU();
        }

        public override void SyncToCPU()
        {
            Idx.SyncToCPU();
            Value.SyncToCPU();
        }
    }
}
