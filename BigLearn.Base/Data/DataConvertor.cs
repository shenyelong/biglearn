using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public static class DataConvertor  
    {       
        public static Tensor4DData ToT4D(this NdArrayData data)
        {
            if(data.Shape.Length == 4)
            {
                return new Tensor4DData(data.DeviceType, data.Output, data.Deriv, data.Shape);
            }
            else if( data.Shape.Length < 4)
            {
                IntArgument[] tmp4D = new IntArgument[4];
                for(int i = 0; i < 4; i++)
                {
                    if(i < data.Shape.Length) tmp4D[i] = data.Shape[i];
                    else tmp4D[i] = new IntArgument("default_dim", 1);
                }
                return new Tensor4DData(data.DeviceType, data.Output, data.Deriv, tmp4D);
            }
            else
            {
                throw new Exception("dropout can't be applied to over 4 d dim data.");
            }
        }
        public static Tensor4DData ToT4D(this HiddenBatchData data)
        {
            return new Tensor4DData(data.DeviceType, data.Output.Data, data.Deriv.Data, data.Shape[0], data.Shape[1], 
                                                            new IntArgument("DEPTH", 1), new IntArgument("MAX_BATCHSIZE", 1));
        }

        public static NdArrayData ToND(this HiddenBatchData data)
        {
            return new NdArrayData(data.DeviceType, data.Output.Data, data.Deriv.Data, data.Shape[0], data.Shape[1]);
        }

        public static NdArrayData ToND(this EmbedStructure data)
        {
            return data.NDEmbed;
            //return new NdArrayData(data.DeviceType, data.Embedding, data.EmbeddingGrad, new IntArgument("embed", data.Dim), new IntArgument("vocab", data.VocabSize));
        }

        public static NdArrayData ToND(this MatrixData data)
        {
            return new NdArrayData(data.DeviceType, data.Output, data.Deriv, data.Shape[0], data.Shape[1]);
        }

        public static MatrixData ToMD(this HiddenBatchData data)
        {
            return new MatrixData(data);
        }

        public static NdArrayData ToND(this Tensor4DData data)
        {
            return new NdArrayData(data.DeviceType, data.Output, data.Deriv, data.Shape);
        }

        public static NdArrayData Reshape(this NdArrayData data, params IntArgument[] shape)
        {
            return new NdArrayData(data.DeviceType, data.Output, data.Deriv, shape);
        }

        public static HiddenBatchData ToHDB(this NdArrayData data)
        {
            return new HiddenBatchData(data.DeviceType, data.Output, data.Deriv, data.Shape[0], data.Shape[1]);  
        }

        public static MatrixData ToMD(this NdArrayData data)
        {
            return new MatrixData(data.DeviceType, data.Output, data.Deriv, data.Shape[0], data.Shape[1]);
            //return data.ToHDB().ToMD();
        }

        public static HiddenBatchData ToHDB(this Tensor4DData data)
        {
            return new HiddenBatchData(data.DeviceType, data.Output, data.Deriv, data.Shape[0], data.Shape[1]);
        }

        public static HiddenBatchData ToHDB(this MatrixData data)
        {
            return data.ToND().ToHDB();
        }

        public static HiddenBatchData ToHDB(this SeqDenseBatchData data)
        {
            return new HiddenBatchData(data.DeviceType, data.SentOutput, data.SentDeriv, data.ShapeDim, data.ShapeSentSize);
        }

        public static VectorData ToVec(this HiddenBatchData data)
        {
            return new VectorData(data);
        }

        public static VectorData ToVec(this Tensor4DData data)
        {
            return data.ToHDB().ToVec();
        }

        public static NdArrayData ToND(this VectorData data)
        {
            return new NdArrayData(data.DeviceType, data.Output, data.Deriv, data.Shape[0]);
        }
    }
}
