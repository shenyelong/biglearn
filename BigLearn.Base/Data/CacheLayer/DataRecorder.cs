using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;

namespace BigLearn
{
    /// <summary>
    /// Data Type mush Support IData.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    public class DataRecorder //, new() //<MetaType>, new()
        //where MetaType : DataStat, new()
    {
        BinaryWriter BinWriter { get; set; }
        BinaryWriter IdxWriter { get; set; }
        
        int TotalBatchNumber { get; set; }

        public DataRecorder(BinaryWriter binWriter, BinaryWriter idxWriter)
        {
            BinWriter = binWriter;
            IdxWriter = idxWriter;
            TotalBatchNumber = 0;
        }

        public void SerializeHeader(params IntArgument[] shape)
        {
            int number = shape.Length;
            BinWriter.Write(number);
            foreach(IntArgument arg in shape)
            {
                BinWriter.Write(arg.Name);
            }
        }

        public void SerializeData<T>(T data) where T : BatchData
        {
            long idx = BinWriter.BaseStream.Position;
            IdxWriter.Write(idx);
            data.Serialize(BinWriter);
            TotalBatchNumber += 1;
        }

        public void SerializeTail(params IntArgument[] shape)
        {
            foreach(IntArgument arg in shape)
            {
                BinWriter.Write(arg.Default);
            }
            BinWriter.Write(TotalBatchNumber);
        }

        public void Close()
        {
            BinWriter.Close();
            IdxWriter.Close();
        }
    }
}
