﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public interface IDataCashier<DataType> 
    {
        /// <summary>
        /// Meta Data for Instances.
        /// </summary>
        IntArgument[] Shape { get; } 

        /// <summary>
        /// Get Instances from Stream.
        /// </summary>
        /// <param name="randomProcessor"></param>
        /// <returns></returns>
        // IEnumerable<DataType> GetInstances(bool randomProcessor);

        /// <summary>
        /// Initialize Data Cache. (it is staled, pls don't use it any more.)
        /// </summary>
        /// <param name="l1"></param>
        /// <param name="l2"></param>
        //void InitPipelineCashier(int l1, int l2);

        /// <summary>
        /// Only L1 Cache Works.
        /// </summary>
        /// <param name="l1Cache"></param>
        /// <param name="isRandom"></param>
        //void InitThreadSafePipelineCashier(int l1Cache, bool isRandom);

        /// <summary>
        /// Fetch Data From DataCashier.
        /// It could support multiple consumers.
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        bool FetchDataThreadSafe(DataType output);

        void ResetDataFlow();

        void CloseDataFlow();
    }

    /// <summary>
    /// Data Runner Support ThreadSafe.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    public class DataRunner<DataType> : StructRunner where DataType : BatchData
        //where MetaType : DataStat, new()
    {
        public new DataType Output { get { return (DataType)base.Output; } protected set { base.Output = value; } }

        IDataCashier<DataType> DataInterface { get; set; }

        public DataRunner(IDataCashier<DataType> dataCashier, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            /// Suppose we have a data factory.
            DataInterface = dataCashier;

            Output = (DataType)Activator.CreateInstance(typeof(DataType), Behavior.Device, DataInterface.Shape); 
            //Output = new DataType(); // DataInterface.Stat, Behavior.Device);
            //Output.Init(DataInterface.Stat, Behavior.Device);
        }

        public DataRunner(IDataCashier<DataType> dataCashier, DataType cache, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            DataInterface = dataCashier;
            Output = cache;
        }

        public override void Init()
        {
            IsTerminate = false;
            IsContinue = true;
            DataInterface.ResetDataFlow();
        }

        public override void Forward()
        {
            // Perf is not threadSafe.
            //var dataCounter = PerfCounter.Manager.Instance["Data"].Begin(); 
            //Output.Clear();
            if (!DataInterface.FetchDataThreadSafe(Output)) IsTerminate = true;
            //PerfCounter.Manager.Instance["Data"].TakeCount(dataCounter);
        }
    }

}
