﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;

namespace BigLearn
{
    /// <summary>
    /// Data Type mush Support IData.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    public class DataCashier<DataType> : IDataCashier<DataType> where DataType : BatchData //, new() //<MetaType>, new()
        //where MetaType : DataStat, new()
    {
        public IntArgument[] Shape { get; private set; }

        List<DataType> L1Caches = new List<DataType>();

        Stream IStream { get { return Reader.BaseStream; } }
        BinaryReader Reader = null;
        long[] SeekPosition = null;
        public string Name { get; set; }
        public int TotalBatchNumber { get; private set; }
        long DataPosition { get; set; }
        public DataCashier(BinaryReader binReader, BinaryReader idxReader, int l1Cache, int groupseed, bool isRandom, string name = "")
        {
            Reader = binReader;
            Name = name;

            IsRandom = isRandom;
            dataRandom = new Random(groupseed);

            DeserializeShape(binReader);

            //Stat = new DataStat(reader); // .Init(Reader);
            InitPipelineCashier(l1Cache, 0);

            InitBlockPosition(idxReader);
        }

        void DeserializeShape(BinaryReader binReader)
        {
            binReader.BaseStream.Seek(0, SeekOrigin.Begin);
            int num = binReader.ReadInt32();
            Shape = new IntArgument[num];

            for(int i=0; i < num; i++)
            {
                Shape[i] = new IntArgument(binReader.ReadString());
            }
            DataPosition = binReader.BaseStream.Position;

            binReader.BaseStream.Seek(- (num + 1) * sizeof(Int32), SeekOrigin.End);
            for(int i = 0; i < num; i++)
            {
                Shape[i].Default = binReader.ReadInt32(); 
            }
            TotalBatchNumber = binReader.ReadInt32();
        }


        void InitPipelineCashier(int l1Cache, int l2Cache)
        {
            for (int i = 0; i < l1Cache; i++)
            {
                L1Caches.Add((DataType)Activator.CreateInstance(typeof(DataType), DeviceType.CPU, Shape));
                //L1Caches.Add( new DataType());
                //L1Caches[i].Init(Stat, DeviceType.CPU);
            }

            //for (int i = 0; i < l2Cache; i++)
            //{
            //    L2Caches.Add(new DataType());
            //    L2Caches[i].Init(Stat, DeviceType.GPU);
            //}
        }


        void InitBlockPosition(BinaryReader idxReader)
        {
            SeekPosition = new long[TotalBatchNumber];
            for(int idx = 0; idx < TotalBatchNumber; idx ++)
            {
                SeekPosition[idx] = idxReader.ReadInt64();
            }
        }

        //public void InitReader(BinaryReader reader, string cursorStream = "")
        //{
        //    Reader = reader;
        //    IStream = Reader.BaseStream;
        //    Stat.Init(Reader);
        //    InitBlockPosition(cursorStream);
        //}

        // public IEnumerable<DataType> GetInstances(bool randomProcessor)
        // {
        //     return GetInstances(randomProcessor, Util.URandom);
        // }

        public IEnumerable<DataType> GetInstances(bool randomProcessor, Random randomizer)
        {
            //Random localRandom = new Random((int)DateTime.Now.TimeOfDay.TotalSeconds);
            {
                BlockingCollection<int> dataBuffer = new BlockingCollection<int>(L1Caches.Count - 2);

                var disk2CPUParallel = Task.Run(() =>
                {
                    if (randomProcessor)
                    {
                        int[] RandomBatchIdx = Enumerable.Range(0, TotalBatchNumber).ToArray();
                        int batIdx = 0;
                        while (batIdx < TrainBatchNum && batIdx < TotalBatchNumber)
                        {
                            int randomPos = randomizer.Next(batIdx, TotalBatchNumber);
                            int id = RandomBatchIdx[randomPos];
                            RandomBatchIdx[randomPos] = RandomBatchIdx[batIdx];
                            RandomBatchIdx[batIdx] = id;

                            IStream.Position = SeekPosition[id];
                            L1Caches[batIdx % L1Caches.Count].Deserialize(Reader);
                            dataBuffer.Add(batIdx % L1Caches.Count);
                            batIdx += 1;
                        }
                    }
                    else
                    {
                        IStream.Seek(DataPosition, SeekOrigin.Begin);
                        int batIdx = 0;
                        while (batIdx < TrainBatchNum && batIdx < TotalBatchNumber)
                        {
                            L1Caches[batIdx % L1Caches.Count].Deserialize(Reader);
                            dataBuffer.Add(batIdx % L1Caches.Count);
                            batIdx += 1;
                        }
                    }
                    // Let consumer know we are done.
                    dataBuffer.CompleteAdding();
                });

                // if (L2Caches.Count > 2)
                // {
                //     BlockingCollection<int> gpuBuffer = new BlockingCollection<int>(L2Caches.Count - 2);
                //     var cpu2gpuParallel = Task.Run(() =>
                //     {
                //         Cudalib.AttachToInitialContext();
                //         int batIdx = 0;
                //         while (!dataBuffer.IsCompleted)
                //         {
                //             try
                //             {
                //                 int l1Idx = dataBuffer.Take(); ///CPU buffer.
                //                 L2Caches[batIdx % L2Caches.Count].CopyFrom(L1Caches[l1Idx]);
                //                 gpuBuffer.Add(batIdx % L2Caches.Count);
                //                 batIdx += 1;
                //             }
                //             catch (InvalidOperationException)
                //             {
                //             }
                //         }
                //         gpuBuffer.CompleteAdding();
                //     });

                //     while (!gpuBuffer.IsCompleted)
                //     {
                //         int gpuId = -1;
                //         try
                //         {
                //             gpuId = gpuBuffer.Take();
                //         }
                //         catch (InvalidOperationException) { }

                //         if (gpuId != -1) yield return L2Caches[gpuId];
                //     }
                // }
                // else
                {
                    while (!dataBuffer.IsCompleted)
                    {
                        int cpuId = -1;
                        try
                        {
                            cpuId = dataBuffer.Take();
                        }
                        catch (InvalidOperationException)
                        { }

                        if (cpuId != -1)
                        {
                            yield return L1Caches[cpuId];
                        }
                    }
                }
            }
        }



        

        bool DebugMode = false;
        int DebugBatchNum = 0;
        public int TrainBatchNum { get { return DebugMode ? DebugBatchNum : TotalBatchNumber; } set { if (value > 0) { DebugMode = true; DebugBatchNum = value; } } }

        IEnumerator<DataType> DataFlow { get; set; }
        object DataFlowLock = new object();
        bool IsRandom { get; set; }
        Random dataRandom;

        // void InitThreadSafePipelineCashier(int l1Cache, bool isRandom)
        // {
        //     IsRandom = isRandom;
        //     InitPipelineCashier(l1Cache, 0);
        //     dataRandom = new Random(ParameterSetting.RANDOM_SEED);//Util.URandom;
        // }

        // void InitThreadSafePipelineCashier(int l1Cache, int groupRandomSeed)
        // {
        //     IsRandom = true;
        //     InitPipelineCashier(l1Cache, 0);
        //     dataRandom = new Random(groupRandomSeed);
        // }

        bool IsReseted = false;

        /// <summary>
        /// Only need to start once. 
        /// </summary>
        public void ResetDataFlow()
        {
            if (!IsReseted)
            {
                IsReseted = true;
                DataFlow = GetInstances(IsRandom, dataRandom).GetEnumerator();
            }
        }

        /// <summary>
        /// ThreadSafe for Fetching Training Data.
        /// </summary>
        /// <param name="output"> DataType must have IData Interface.</param>
        /// <returns></returns>
        public bool FetchDataThreadSafe(DataType output)
        {
            lock (DataFlowLock)
            {
                if (DataFlow.MoveNext())
                {
                    //using (var stream = new MemoryStream())
                    //{
                    output.CopyFrom(DataFlow.Current);
                    //}
                    return true;
                }
                else
                {
                    //output.Clear();
                    IsReseted = false;
                    return false;
                }
            }
        }
        public void CloseDataFlow()
        {
            Reader.Close();
            IStream.Close();
        }
        
    }

    

    /// <summary>
    /// Data Runner Support ThreadSafe.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    //public class EnumlateDataRunner<DataType, MetaType> : StructRunner
    //    where DataType : BatchData<MetaType>, new()
    //    where MetaType : DataStat, new()
    //{
    //    public new DataType Output { get { return (DataType)base.Output; } protected set { base.Output = value; } }

    //    IEnumerator<DataType> DataFlow;
    //    public EnumlateDataRunner(IEnumerable<DataType> dataFlow, MetaType stat, RunnerBehavior behavior)
    //        : base(Structure.Empty, behavior)
    //    {
    //        DataFlow = dataFlow.GetEnumerator();
    //        Output = new DataType(); Output.Init(stat == null ? new MetaType() : stat, Behavior.Device);
    //    }

    //    public override void Init()
    //    {
    //        IsTerminate = false;
    //        IsContinue = true;
    //    }

    //    public override void Forward()
    //    {
    //        if (DataFlow.MoveNext())
    //        {
    //            Output.CopyFrom(DataFlow.Current);
    //        }
    //        else
    //        {
    //            Output.Clear();
    //            IsTerminate = true;
    //            IsContinue = false;
    //        }
    //    }

    //}
}
