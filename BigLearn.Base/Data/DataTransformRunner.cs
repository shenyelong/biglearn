﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Convert the data format from one type to another type.
    /// </summary>
    public class Transform_SeqSentence2SequenceRunner : StructRunner
    {
        public new SeqSparseBatchData Output { get { return (SeqSparseBatchData)base.Output; } set { base.Output = value; } }
        public new SeqSentenceBatchInputData Input { get { return (SeqSentenceBatchInputData)base.Input; } set { base.Input = value; } }

        public Transform_SeqSentence2SequenceRunner(SeqSentenceBatchInputData data, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = data;
            Output = new SeqSparseBatchData(Input.MAX_SENTSIZE, Input.MAX_WORDSIZE, Input.MAX_ELEMENTSIZE, Input.FEATURE_DIM,
                                            Input.SentIdx, Input.WordIdx, Input.WordMargin, Input.FeaIdx, Input.FeaValue, Behavior.Device);

            //    int maxBatchSize, int maxSentSize, int maxElementSize, int featureDim, DeviceType deviceType)

            //            );
            //Output.Stat.CopyFrom(Input.Stat);
            //Output.FEATURE_DIM = Input.Stat.MAX_FEATUREDIM;
            //Output.MAX_BATCHSIZE = Input.Stat.MAX_SENTENCESIZE;
            //Output.MAX_SEQUENCESIZE = Input.Stat.MAX_WORDSIZE;
            //Output.Stat.MAX_ELEMENTSIZE = Input.Stat.MAX_ELEMENTSIZE;

            //Output.FeaIdx = Input.FeaIdx;
            //Output.FeaValue = Input.FeaValue;
            //Output.SampleIdx = Input.SentIdx;
            //Output.SentMargin = Input.WordMargin;
            //Output.SequenceIdx = Input.WordIdx;
        }

        //public override void Forward()
        //{
        //    Output.BatchSize = Input.SentSize;
        //    Output.SentSize = Input.WordSize;
        //    Output.ElementSize = Input.ElementSize;
        //}

    }

    public class Transform_Matrix2SeqMatrixRunner : StructRunner
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
        public SeqSentenceBatchInputData SeqInput { get; set; }
        public HiddenBatchData SentVec { get; set; }
        public Transform_Matrix2SeqMatrixRunner(HiddenBatchData data, SeqSentenceBatchInputData paraSeqData,
           RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            SeqInput = paraSeqData;
            SentVec = data;

            //CudaPieceInt smpIdx, CudaPieceInt sentMargin, int maxBatchSize, int maxSentSize, int dim, 
            //                     CudaPieceFloat sentOutput, CudaPieceFloat sentDeriv, DeviceType device)

            //Output = new SeqDenseBatchData() { SampleIdx = paraSeqData.SampleIdx, SentMargin = paraSeqData.SentMargin, SentOutput = data.Output.Data, SentDeriv = data.Deriv.Data };
            Output = new SeqDenseBatchData(
                //new SequenceDataStat( data.MAX_BATCHSIZE, data.Dim),
                paraSeqData.SampleIdx, paraSeqData.SentMargin, 
                                    paraSeqData.MAX_BATCHSIZE, data.MAX_BATCHSIZE, data.Dim,
                                    data.Output.Data, (data.Deriv == null ? null : data.Deriv.Data), paraSeqData.DeviceType);

            //Output.Stat.MAX_BATCHSIZE = ;
            //Output.Stat.MAX_SEQUENCESIZE = ;
            //Output.Stat.FEATURE_DIM = ;
            //Output.Stat.MAX_ELEMENTSIZE = data.Dim * data.MAX_BATCHSIZE;
        }

        //public override void Forward()
        //{
        //    Output.BatchSize = SeqInput.BatchSize;
        //    Output.SentSize = SentVec.BatchSize;
        //}

    }

    //public class Transform_Matrix2TransSeqMatrixRunner : StructRunner
    //{
    //    public new  SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }
    //    public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
    //    RecurrentSeqInfo RecurrentInfo { get; set; }
    //    public Transform_Matrix2TransSeqMatrixRunner(HiddenBatchData input, SequenceDataStat stat, CudaPieceInt smpIdx, CudaPieceInt sentMargin, RecurrentSeqInfo recurrentInfo, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //    {
    //        Input = input;
    //        RecurrentInfo = recurrentInfo;
    //        Output = new SeqDenseRecursiveData(new SequenceDataStat() { FEATURE_DIM = input.Dim, MAX_BATCHSIZE = stat.MAX_BATCHSIZE, MAX_SEQUENCESIZE = stat.MAX_SEQUENCESIZE }, 
    //            smpIdx, sentMargin, input.Output.Data, input.Deriv == null? null : input.Deriv.Data, recurrentInfo, behavior.Device);
    //    }

    //    public override void Forward()
    //    {
    //        Output.BatchSize = RecurrentInfo.BatchSize ;
    //        Output.SentSize = RecurrentInfo.SentSize;
    //    }
    //}

    public class Cook_TransposeSeq2SeqRunner : StructRunner
    {
        public new SeqDenseRecursiveData Input { get { return (SeqDenseRecursiveData)base.Input; } set { base.Input = value; } }
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        public Cook_TransposeSeq2SeqRunner(SeqDenseRecursiveData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Output = new SeqDenseBatchData(Input.SampleIdx, Input.SentMargin, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Input.Dim, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;
            Behavior.Computelib.Matrix_AdditionMask(Input.SentOutput, 0, Input.MapForward, 0,
                                                    Output.SentOutput, 0, CudaPieceInt.Empty, 0,
                                                    Output.SentOutput, 0, CudaPieceInt.Empty, 0,
                                                    Input.Dim, Input.SentSize, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Matrix_AdditionMask(Output.SentDeriv, 0, CudaPieceInt.Empty, 0,
                                           Input.SentDeriv, 0, Input.MapForward, 0,
                                           Input.SentDeriv, 0, Input.MapForward, 0,
                                           Input.Dim, Input.SentSize, 1, 1, 0);
        }
    }

    public class Cook_Seq2TransposeSeqRunner : StructRunner
    {
        public new SeqDenseBatchData Input { get { return (SeqDenseBatchData)base.Input; } set { base.Input = value; } }
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }

        bool IsReverse = false;
        public Cook_Seq2TransposeSeqRunner(SeqDenseBatchData input, bool isReverse, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Output = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin,
                                               Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Input.Dim,
                                                //Input.Stat, 
                                                Behavior.Device);
            IsReverse = isReverse;
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;
            if (IsReverse)
            {
                Output.RecurrentInfo.InitReverseAndTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize);
            }
            else
            {
                Output.RecurrentInfo.InitTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize);
            }
            Behavior.Computelib.Matrix_AdditionMask(Input.SentOutput, 0, CudaPieceInt.Empty, 0,
                                           Output.SentOutput, 0, Output.MapForward, 0,
                                           Output.SentOutput, 0, Output.MapForward, 0,
                                           Input.Dim, Input.SentSize, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Input.SentDeriv != null && !Input.SentDeriv.IsEmpty)
            {
                ComputeLib.Matrix_AdditionMask(Output.SentDeriv, 0, CudaPieceInt.Empty, 0,
                                               Input.SentDeriv, 0, Output.MapBackward, 0,
                                               Input.SentDeriv, 0, Output.MapBackward, 0,
                                               Input.Dim, Input.SentSize, 1, 1, 0);
            }
        }
    }
}
