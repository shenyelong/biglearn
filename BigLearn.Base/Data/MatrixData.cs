﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixData : BatchData //VectorData
    {
        public VectorData Context;
        public CudaPieceFloat Output { get { return Context.Output; } set { Context.Output = value; }  }
        public CudaPieceFloat Deriv { get { return Context.Deriv; } set { Context.Deriv = value; }  }
        
        public int Length { get { return Context.Length; } set { Context.Length = value; }  }
        public int MaxLength { get { return Context.MaxLength; } set { Context.MaxLength = value; }  }

        public override DataSourceID Type { get { return DataSourceID.MatrixData; } }

        public int Column { get { return Shape[0].Default; } }
        public int MaxRow { get { return Shape[1].Default; } }

        public IntArgument IColumn { get { return Shape[0]; } }
        public IntArgument IRow { get { return Shape[1]; } }

        
        public virtual int Row
        {
            get { return Length / Column; }
            set
            {
                if (value > MaxRow) throw new Exception(string.Format("Row {0} should be smaller than Max Row {1}", value, MaxRow));
                Length = Column * value;
            }
        }


        public MatrixData(int column, int maxRow, bool requireGrad, DeviceType device) 
            : base(device, new IntArgument("Column", column), new IntArgument("MaxRow", maxRow)) // column * maxRow, requireGrad, device)
        {
            Context = new VectorData(column * maxRow, requireGrad, device); 
        }

        public MatrixData(int column, int maxRow, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) 
            : base(device, new IntArgument("Column", column), new IntArgument("MaxRow", maxRow)) // column * maxRow, data, deriv, device) //this(column, maxRow, column * maxRow, data, deriv, device)
        {
            Context = new VectorData(column * maxRow, data, deriv, device); 
        }

        public MatrixData(DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, IntArgument column, IntArgument row)
            : base(device, column, row)
        {
            Context = new VectorData(column.Default * row.Default, data, deriv, device);   
        }

        public MatrixData(DeviceType device, IntArgument column, IntArgument row)
            : base(device, column, row)
        {
            Context = new VectorData(column.Default * row.Default, true, device); 
            //Context = new VectorData(column.Default * row.Default, data, deriv, device);   
        }


        public MatrixData(int column, int maxRow, DeviceType device) : this(column, maxRow, true, device)
        { }

        public MatrixData(LayerStructure layer) : this(layer.Neural_Out, layer.Neural_In, layer.weight, layer.weightGrad, layer.DeviceType)
        { }

        public MatrixData(ImageDataSource imageSet) : this(imageSet.Width * imageSet.Height * imageSet.Depth, imageSet.MAX_BATCHSIZE, imageSet.Data, imageSet.Deriv, imageSet.DeviceType)
        { }

        //public MatrixData(MatrixStructure matrix) : this(matrix.Dim, matrix.Size, matrix.Memory, matrix.MemoryGrad, matrix.DeviceType)
        //{ }

        public MatrixData(HiddenBatchData data) : this(data.DeviceType, data.Output.Data, data.Deriv == null ? null : data.Deriv.Data, data.Shape[0], data.Shape[1])
        { }

        public MatrixData(SeqDenseBatchData data) : this(data.Dim, data.MAX_SENTSIZE, data.SentOutput, data.SentDeriv, data.DeviceType)
        { }

        public MatrixData(EmbedStructure data) : this(data.Dim, data.VocabSize, data.Embedding, data.EmbeddingGrad, data.DeviceType )
        { }

        public MatrixData(VectorData data) : this(1, data.MaxLength, data.Output, data.Deriv, data.DeviceType)
        { }

    }

}
