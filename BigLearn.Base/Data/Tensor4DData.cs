using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class Tensor4DData : BatchData
    {
        public override DataSourceID Type { get { return DataSourceID.Tensor4DData; } }

        public int Width { get { return Shape[0].Default; } set { Shape[0].Default = value; } }
        public int Height { get { return Shape[1].Default; } set { Shape[1].Default = value; } }
        
        public int Depth { get { return Shape[2].Default; } set { Shape[2].Default = value; } }
        public IntArgument DepthArg { get { return Shape[2]; } }

        public int MaxBatchSize { get { return Shape[3].Default; } set { Shape[3].Default = value; } }
        public IntArgument BatchArg { get { return Shape[3]; } }

        public MatrixData Context;

        public CudaPieceFloat Output { get { return Context.Output; } set { Context.Output = value; } }
        public CudaPieceFloat Deriv { get { return Context.Deriv; } set { Context.Deriv = value; } }
        public int Length { get { return Context.Length; } set { Context.Length = value; } }
        public int MaxLength { get { return Context.MaxLength; } set { Context.MaxLength = value; }  }

        public int BatchSize
        {
            get { return Length / (Width * Height * Depth); }
            set
            {
                if (value > MaxBatchSize) throw new Exception(string.Format("BatchSize {0} should be smaller than Max BatchSize {1}", value, MaxBatchSize));
                Length = (Width * Height * Depth) * value;
                Shape[3].Value = value;
            }
        }

        public Tensor4DData(int width, int height, int depth, int maxBatchSize, DeviceType device) 
                    : this(width, height, depth, maxBatchSize, 
                            new CudaPieceFloat(width * height * depth * maxBatchSize, device),
                            new CudaPieceFloat(width * height * depth * maxBatchSize, device), device)
        { }

        public Tensor4DData(int width, int height, int depth, int maxBatchSize, bool isDeriv, DeviceType device) 
                    : this(width, height, depth, maxBatchSize, 
                            new CudaPieceFloat(width * height * depth * maxBatchSize, device),
                            isDeriv ? new CudaPieceFloat(width * height * depth * maxBatchSize, device) : null
                            , device)
        { }

        public Tensor4DData(DeviceType device, params IntArgument[] shape) 
                    : this(device,
                           new CudaPieceFloat(shape[0].Default * shape[1].Default * shape[2].Default * shape[3].Default, device),
                           new CudaPieceFloat(shape[0].Default * shape[1].Default * shape[2].Default * shape[3].Default, device), 
                           shape)
        { }

        public Tensor4DData(DeviceType device, bool isDeriv, params IntArgument[] shape) 
                    : this(device,
                           new CudaPieceFloat(shape[0].Default * shape[1].Default * shape[2].Default * shape[3].Default, device),
                           isDeriv ? new CudaPieceFloat(shape[0].Default * shape[1].Default * shape[2].Default * shape[3].Default, device) : null, 
                           shape)
        { }

        public Tensor4DData(int width, int height, int depth, int maxBatchSize, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) 
                    : this(device, data, deriv, 
                           new IntArgument("WIDTH", width), 
                           new IntArgument("HEIGHT", height), 
                           new IntArgument("DEPTH", depth), 
                           new IntArgument("MAX_BATCHSIZE", maxBatchSize))
        { }

        public Tensor4DData(DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] shape) 
                    : base(device, shape)
        {
             Context = new MatrixData(Width * Height * Depth, MaxBatchSize, data, deriv, device);
        }

        public override void SyncFromCPU()
        {
            Output.SyncFromCPU();
        }
        
        public override void CopyFrom(IData other)
        {
            Tensor4DData src = (Tensor4DData)other;
            Output.CopyFrom(src.Output);
        }
    }

}
