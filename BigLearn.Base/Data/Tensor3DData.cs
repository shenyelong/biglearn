using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class Tensor3DData : BatchData //VectorData
    {
        public override DataSourceID Type { get { return DataSourceID.Tensor3DData; } }

        public VectorData Context;
        public CudaPieceFloat Output { get { return Context.Output; } set { Context.Output = value; }  }
        public CudaPieceFloat Deriv { get { return Context.Deriv; } set { Context.Deriv = value; }  }
        
        public int Length { get { return Context.Length; } set { Context.Length = value; }  }
        public int MaxLength { get { return Context.MaxLength; } set { Context.MaxLength = value; }  }

        public int Width { get { return Shape[0].Default; } }
        public int Height { get { return Shape[1].Default; } }
        public int MaxBatchSize { get { return Shape[2].Default; } }
        
        public virtual int BatchSize
        {
            get { return Length / (Width * Height); }
            set
            {
                if (value > MaxBatchSize) throw new Exception(string.Format("BatchSize {0} should be smaller than Max BatchSize {1}", value, MaxBatchSize));
                Length = (Width * Height) * value;
            }
        }

        public Tensor3DData(int width, int height, int maxBatchSize, DeviceType device) 
                    : base(device,
                           new IntArgument("WIDTH", width), 
                           new IntArgument("HEIGHT", height), 
                           new IntArgument("MAX_BATCHSIZE", maxBatchSize))
        {
            Context = new VectorData(width * height * maxBatchSize, device); 
            //MaxBatchSize = maxBatchSize;
            //Width = width;
            //Height = height;
        }


        public Tensor3DData(int width, int height, int maxBatchSize, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) 
                    : base(device,
                           new IntArgument("WIDTH", width), 
                           new IntArgument("HEIGHT", height), 
                           new IntArgument("MAX_BATCHSIZE", maxBatchSize))
                        //width * height * maxBatchSize, data, deriv, device)
        {
            Context = new VectorData(width * height * maxBatchSize, data, deriv, device);
            //MaxBatchSize = maxBatchSize;
            //Width = width;
            //Height = height;
        }
    }

}
