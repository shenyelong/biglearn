﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class NdArrayData : BatchData
    {
        public override DataSourceID Type { get { return DataSourceID.NdArrayData; } }

        public IntArgument[] Dimensions { get { return Shape; } set { Shape = value; } }

        public int NdDim { get { return Dimensions.Aggregate((a, x) => new IntArgument("sum", a.Value * x.Value)).Value; } }

        public VectorData Context;

        public CudaPieceFloat Output { get { return Context.Output; } set { Context.Output = value; }  }
        public CudaPieceFloat Deriv { get { return Context.Deriv; } set { Context.Deriv = value; }  }
        
        public int Length { get { return Context.Length; } set { Context.Length = value; }  }
        public int MaxLength { get { return Context.MaxLength; } set { Context.MaxLength = value; }  }
        
        public NdArrayData(IntArgument[] dimensions, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device)
            : this(device, data, deriv, dimensions)  //dimensions.Aggregate((a, x) => new IntArgument("sum", a.Value * x.Value)).Value, data, deriv, device)
        { }
        
        public NdArrayData(DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions)
            : base(device, dimensions)  //dimensions.Aggregate((a, x) => new IntArgument("sum", a.Value * x.Value)).Value, data, deriv, device)
        {
            int size = dimensions.Aggregate((a, x) => new IntArgument("sum", a.Default * x.Default)).Value; //data, deriv, device)
            Context = new VectorData(size, data, deriv, device); 
        }

        public NdArrayData(IntArgument[] dimensions, DeviceType device) 
            : this(device, dimensions)
        { }

        public NdArrayData(DeviceType device, params IntArgument[] dimensions) 
            : this(device, true, dimensions) // dimensions, device)
        { }

        public NdArrayData(DeviceType device, bool requireGrad, params IntArgument[] dimensions) 
            : base(device, dimensions)
        {
            int size = dimensions.Aggregate((a, x) => new IntArgument("sum", a.Default * x.Default)).Value; //data, deriv, device)
            Context = new VectorData(size, requireGrad, device);  
        }

        public NdArrayData NoGrad()
        {
            return new NdArrayData(DeviceType, Output, null, Shape);
        }
    }
}
