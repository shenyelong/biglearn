﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using BigLearn;

namespace BigLearn
{
    public static class EvaluationToolkit
    {
        static string _path = @"\\deep-05\EvaluationToolkit\";
        public static string Path { get { return _path; } set { _path = value; }  }
        //BuilderParameters.EvalToolPath; }

        public static string EvalToolExe(EvaluationType evalType)
        {
            return string.Format(EvaluationToolkit.Path + @"\Evaluation_{0}\{0}.exe", evalType.ToString());
        }

        public static string EvalToolPython(EvaluationType evalType)
        {
            return string.Format(EvaluationToolkit.Path + @"\Evaluation_{0}\{0}.py", evalType.ToString());
        }
    }
    public enum EvaluationType
    {
        AUC = 0, NDCG = 1, MSE = 2, ACCURACY = 3, MAP = 4, ORDINALACCURACY = 5, ORDEDLOGITACCURACY = 6, PLAINOUTPUT = 7,
        PERPLEXITY = 8, VECTOROUTPUT = 9, BSBLEU = 10, ROUGE = 11, TOPKBEAM = 12, PRECISIONK = 13, NLTK_BLEU = 14, PLAIN_BLEU = 15,
    }

    public enum HRSItem
    {
        Bad = 0,
        Fair = 1,
        Good = 2,
        Excellent = 3,
        Perfect = 4
    } //Unjudged = 1, 

    public static class HRSItemValue
    {
        static Dictionary<string, float> LabelScore = new Dictionary<string, float>();

        static HRSItemValue()
        {
            var list = Enum.GetValues(typeof(HRSItem));
            foreach (var item in list) LabelScore.Add(item.ToString().ToLower(), (int)item);
        }
        public static float HRS(string str)
        {
            if (LabelScore.ContainsKey(str.ToLower()))
                return LabelScore[str.ToLower()];

            float r = 0;
            if (float.TryParse(str, out r))
                return r;
            Console.WriteLine("un used label str {0}, will be set to zero", str);
            return 0;
        }
    }

    public class BasicEvalParameter
    {
        public float LabelMinValue;
        public float LabelMaxValue;
        public float Gamma = 1;
        public string ScoreFile;
        public string MetricFile;
        public string labelFile;
    }

    public abstract class EvaluationSet 
    {
        public string ScoreFile = "output.score";
        public string MetricFile = "output.metric";

        public abstract EvaluationType Type { get; }

        public virtual string EvalTool { get { return EvaluationToolkit.EvalToolExe(Type); } }
        public virtual string EvalToolBackup { get { return string.Format(@"EvaluationToolkit\Evaluation_{0}\{0}.exe", Type.ToString()); } }
        protected virtual void Save(string scoreFile)
        { }

        public static List<string> CallEvaluationTool(string scoreFile, string metricFile, string validate_tool)
        {
            List<string> results = new List<string>();
            // remove previous result
            if (File.Exists(metricFile)) File.Delete(metricFile);
            if (!File.Exists(validate_tool)) { results.Add("0"); results.Add(string.Format("Evaluation Tool {0} Could not be found!", validate_tool)); return results; }

            if (validate_tool.EndsWith(".exe"))
            {
                using (Process callProcess = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = validate_tool,
                        Arguments = string.Format("\"{0}\" \"{1}\"", scoreFile, metricFile),
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                    }
                })
                {
                    callProcess.Start();
                    callProcess.WaitForExit();
                }
            }
            else if (validate_tool.EndsWith(".py"))
            {
                using (Process callProcess = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "python", // executiveFile,
                        Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"", validate_tool, scoreFile, metricFile),
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                    }
                })
                {
                    callProcess.Start();
                    callProcess.WaitForExit();
                }
            }

            if (!File.Exists(metricFile)) { results.Add("0"); results.Add(string.Format("Metric Result File {0} Could not be found!", metricFile)); return results; }

            using (StreamReader sr = new StreamReader(metricFile))
            {
                string line = sr.ReadLine();    // read the first line only
                results.Add(line);
                while ((line = sr.ReadLine()) != null) results.Add(line);
            }
            return results;
        }

        void CallExternalMetricEXE(string executiveFile, string inputFile, string outputResultFile)
        {
            if (File.Exists(outputResultFile))
            {
                // remove previous result
                File.Delete(outputResultFile);
            }
            using (Process callProcess = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = executiveFile,
                    Arguments = string.Format("\"{0}\" \"{1}\"", inputFile, outputResultFile),
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                }
            })
            {
                callProcess.Start();
                callProcess.WaitForExit();
            }
        }

        void CallExternalMetricPYTHON(string executiveFile, string inputFile, string outputResultFile)
        {
            if (File.Exists(outputResultFile))
            {
                // remove previous result
                File.Delete(outputResultFile);
            }
            using (Process callProcess = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "python", // executiveFile,
                    Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"", executiveFile, inputFile, outputResultFile),
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                }
            })
            {
                callProcess.Start();
                callProcess.WaitForExit();
            }
        }


        float ReadExternalObjectiveMetric(string metricEvalResultFile, out List<string> validationFileLines)
        {
            if (!File.Exists(metricEvalResultFile))
            {
                throw new Exception(string.Format("Missing objective metric result file {0}, check your validation evaluation process!", metricEvalResultFile));
            }
            validationFileLines = new List<string>();

            StreamReader sr = new StreamReader(metricEvalResultFile);
            string line = sr.ReadLine();    // read the first line only
            float objectiveMetric = 0;
            if (!float.TryParse(line, out objectiveMetric))
                throw new Exception(string.Format("Cannot read objective metric from the result file {0}, check your validation evaluation process!", metricEvalResultFile));
            validationFileLines.Add(line);
            while ((line = sr.ReadLine()) != null)
            {
                validationFileLines.Add(line);
            }
            sr.Close();

            return objectiveMetric;
        }

        public virtual void Init(BasicEvalParameter parameter) { ScoreFile = parameter.ScoreFile; MetricFile = parameter.MetricFile; }

        public virtual float Evaluation(out List<string> validationFileLines)
        {
            if (File.Exists(EvalTool))
                return Evaluation(out validationFileLines, EvalTool);
            else if (File.Exists(EvalToolBackup))
                return Evaluation(out validationFileLines, EvalToolBackup);
            validationFileLines = new List<string>();
            return 0;
        }

        public float Evaluation(out List<string> validationFileLines, string validate_tool)
        {
            if (File.Exists(ScoreFile))
            {
                File.Delete(ScoreFile);
            }
            if (File.Exists(MetricFile))
            {
                File.Delete(MetricFile);
            }
            Save(ScoreFile);

            if (validate_tool.EndsWith(".exe"))
                CallExternalMetricEXE(validate_tool, ScoreFile, MetricFile);
            else if (validate_tool.EndsWith(".py"))
                CallExternalMetricPYTHON(validate_tool, ScoreFile, MetricFile);

            validationFileLines = null;
            float result = ReadExternalObjectiveMetric(MetricFile, out validationFileLines);
            return result;
        }

        /// <summary>
        /// PlainText
        /// </summary>
        /// <param name="score"></param>
        /// <param name="batchSize"></param>
        public virtual void PushScore(float[] score, int dim, int batchSize)
        { }

        /// <summary>
        /// Regression, Classification, and Point-Wise Evaluation. 
        /// </summary>
        /// <param name="groundTrue"></param>
        /// <param name="score"></param>
        /// <param name="batchSize"></param>
        public virtual void PushScore(float[] groundTrue, float[] score, int batchSize)
        { }

        /// <summary>
        /// Multiclass Classification Evaluation.
        /// </summary>
        /// <param name="groundTrue"></param>
        /// <param name="score"></param>
        /// <param name="dim"></param>
        /// <param name="batchSize"></param>
        public virtual void PushScore(float[] groundTrue, float[] score, int dim, int batchSize) { }

        /// <summary>
        /// Multiclass Classification Evaluation include labelBias. 
        /// </summary>
        /// <param name="groundTrue"></param>
        /// <param name="score"></param>
        /// <param name="labelBias"></param>
        /// <param name="dim"></param>
        /// <param name="batchSize"></param>
        public virtual void PushScore(float[] groundTrue, float[] score, float labelBias, int dim, int batchSize) { }

        /// <summary>
        /// Matching, Ranking, and List-Wise Evaluation.
        /// </summary>
        /// <param name="srcIdx"></param>
        /// <param name="tgtIdx"></param>
        /// <param name="groundTrue"></param>
        /// <param name="score"></param>
        /// <param name="batchSize"></param>
        public virtual void PushScore(int[] srcIdx, int[] tgtIdx, float[] groundTrue, float[] score, int batchSize)
        { }

        /// <summary>
        /// ORDINAL Regression ACCURACY Evaluation.
        /// </summary>
        /// <param name="groundTrue"></param>
        /// <param name="score"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="batchSize"></param>
        public virtual void PushScore(float[] groundTrue, float[] score, float minValue, float maxValue, int batchSize)
        { }

        /// <summary>
        /// Ordered Logit Regression Accuracy Evaluation.
        /// </summary>
        /// <param name="groundTrue"></param>
        /// <param name="score"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="gamma"></param>
        /// <param name="batchSize"></param>
        public virtual void PushScore(float[] groundTrue, float[] score, float minValue, float maxValue, float gamma, int batchSize)
        { }

        public virtual void PushScore(CudaPieceFloat groundTrue, CudaPieceFloat score, int batchSize)
        { }

        /// <summary>
        /// PERPLEXITYEvaluationSet
        /// </summary>
        /// <param name="smpIdx"></param>
        /// <param name="prob"></param>
        /// <param name="batchSize"></param>
        public virtual void PushScore(int[] smpIdx, float[] prob, int batchSize)
        { }

        /// <summary>
        /// BLEU Score.
        /// </summary>
        /// <param name="groundTrue"></param>
        /// <param name="decode"></param>
        public virtual void PushScore(string groundTrue, string decode)
        { }
    }
    public class PLAINOUTPUTEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.PLAINOUTPUT; } }

        StreamWriter ScoreWriter;

        public override void Init(BasicEvalParameter parameter)
        {
            base.Init(parameter);
            ScoreWriter = new StreamWriter(ScoreFile);
        }

        public override void PushScore(float[] score, int dim, int batchSize)
        {
            for (int i = 0; i < batchSize; i++)
            {
                ScoreWriter.WriteLine(string.Join(",", score.Skip(i * dim).Take(dim)));
            }
        }

        public override float Evaluation(out List<string> validationFileLines)
        {
            ScoreWriter.Close();
            validationFileLines = new List<string>();
            return 0;
        }
    }
    public class VECTOROUTPUTEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.VECTOROUTPUT; } }

        StreamWriter ScoreWriter;

        public override void Init(BasicEvalParameter parameter)
        {
            base.Init(parameter);
            ScoreWriter = new StreamWriter(ScoreFile);
        }

        public override void PushScore(float[] score, int dim, int batchSize)
        {
            for (int i = 0; i < batchSize; i++)
            {
                ScoreWriter.WriteLine(string.Join(",", score.Skip(i * dim).Take(dim)));
            }
        }

        public override float Evaluation(out List<string> validationFileLines)
        {
            ScoreWriter.Close();
            validationFileLines = new List<string>();
            return 0;
        }
    }
    public class PERPLEXITYEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.PERPLEXITY; } }

        public float Perplexity = 0;
        public int BatchSize = 0;
        public override void Init(BasicEvalParameter parameter)
        {
            base.Init(parameter);
            Perplexity = 0;
            BatchSize = 0;
        }
        public override void PushScore(int[] smpIdx, float[] prob, int batchSize)
        {
            for (int i = 0; i < batchSize; i++)
            {
                int seqB = i == 0 ? 0 : smpIdx[i - 1];
                int seqE = smpIdx[i];

                float perplexity = 0;
                for (int j = seqB; j < seqE; j++)
                {
                    perplexity += (float)-prob[j];
                }
                //perplexity = perplexity / (seqE - seqB + float.Epsilon);
                Perplexity += perplexity; // (float)Math.Exp(perplexity);
                BatchSize += seqE - seqB;
            }
        }

        public override void PushScore(float[] score, int dim, int batchSize)
        {
            for (int i = 0; i < batchSize; i++)
            {
                Perplexity += (float)-score[i];
            }
            BatchSize += batchSize;
        }

        public override float Evaluation(out List<string> validationFileLines)
        {
            validationFileLines = new List<string>();
            validationFileLines.Add(string.Format("Perplexity {0}\t Entropy {1} \t BatchSize {2}",
               Math.Exp(Perplexity * 1.0f / (BatchSize + float.Epsilon)), Perplexity * 1.0f / (BatchSize + float.Epsilon) / Math.Log(2), BatchSize));
            return (float)Perplexity * 1.0f / (BatchSize + float.Epsilon);
        }
    }
    public class MAPEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.MAP; } }
        List<float> LabelInfo = new List<float>();
        List<float> PredInfo = new List<float>();

        public override void PushScore(float[] groundTrue, float[] score, int batchSize)
        {
            for (int i = 0; i < batchSize; i++)
            {
                LabelInfo.Add(groundTrue[i]);
                PredInfo.Add(score[i]);
            }
        }

        protected override void Save(string scoreFile)
        {
            using (StreamWriter mwriter = new StreamWriter(scoreFile))
            {
                for (int i = 0; i < LabelInfo.Count; i++)
                    mwriter.WriteLine("{0}\t{1}", LabelInfo[i], PredInfo[i]);
            }
        }
    }
    public class AUCEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.AUC; } }
        List<float> LabelInfo = new List<float>();
        List<float> PredInfo = new List<float>();

        public override void PushScore(float[] groundTrue, float[] score, int batchSize)
        {
            for (int i = 0; i < batchSize; i++)
            {
                LabelInfo.Add(groundTrue[i]);
                PredInfo.Add(score[i]);
            }
        }

        protected override void Save(string scoreFile)
        {
            using (StreamWriter mwriter = new StreamWriter(scoreFile))
            {
                for (int i = 0; i < LabelInfo.Count; i++)
                    mwriter.WriteLine("{0}\t{1}", LabelInfo[i], PredInfo[i]);
            }
        }

        public static Tuple<List<string>, float> EvaluateAUC(string filePath, string metricPath)
        {
            List<string> result = EvaluationSet.CallEvaluationTool(filePath, metricPath, EvaluationToolkit.EvalToolExe(EvaluationType.AUC));

            return new Tuple<List<string>, float>(result, float.Parse(result[0]));

        }
        public static double AucForPR(IEnumerable<float> a, IEnumerable<float> p)
        {
            // AUC requires int array as dependent
            var all = a.Zip(p,
                            (actual, pred) => new { actualValue = actual < 0.5 ? 0 : 1, predictedValue = pred })
                       .OrderByDescending(ap => ap.predictedValue)
                       .ToArray();

            double n = (double)all.Length;
            long ones = all.Sum(v => v.actualValue);
            if (0 == ones || n == ones) return 1;

            double correct = 0;
            double prev_recall = 0;
            double prev_prec = 1;
            double auc = 0;

            for (int i = 0; i < n; ++i)
            {
                if (all[i].actualValue == 1)
                {
                    correct += 1;
                    double prec = correct / (double)(i + 1.0);
                    double recall = correct / ones;
                    auc += (prev_prec + prec) * 0.5 * (recall - prev_recall);
                    prev_prec = prec;
                    prev_recall = recall;
                }
            }
            return auc;
        }

        // roc curve.
        public static double Auc(IEnumerable<float> a, IEnumerable<float> p)
        {
            // AUC requires int array as dependent
            var all = a.Zip(p,
                            (actual, pred) => new { actualValue = actual < 0.5 ? 0 : 1, predictedValue = pred })
                       .OrderBy(ap => ap.predictedValue)
                       .ToArray();
            long n = all.Length;
            long ones = all.Sum(v => v.actualValue);
            if (0 == ones || n == ones) return 1;
            long tp0, tn;
            long truePos = tp0 = ones; long accum = tn = 0; double threshold = all[0].predictedValue;
            for (int i = 0; i < n; i++)
            {
                if (all[i].predictedValue != threshold)
                { // threshold changes
                    threshold = all[i].predictedValue;
                    accum += tn * (truePos + tp0); //2* the area of  trapezoid
                    tp0 = truePos;
                    tn = 0;
                }
                tn += 1 - all[i].actualValue; // x-distance between adjacent points
                truePos -= all[i].actualValue;
            }
            accum += tn * (truePos + tp0); // 2 * the area of trapezoid
            return (double)accum / (2 * ones * (n - ones));
        }
    }
    public class MSEEvaluationSet : EvaluationSet 
    {
        public override EvaluationType Type { get { return EvaluationType.MSE; } }

        public static CudaPieceFloat MSEResult = new CudaPieceFloat(10, true, true);
        List<float> LabelInfo = new List<float>();
        List<float> PredInfo = new List<float>();

        public override void Init(BasicEvalParameter parameter)
        {
            //MSEResult = new CudaPieceFloat(10, true, true);
            base.Init(parameter);
            MSEResult.Init(0);
        }

        public override void PushScore(CudaPieceFloat groundTrue, CudaPieceFloat score, int batchSize)
        {
            score.CopyOutFromCuda();
            Cudalib.Calculate_MSE(groundTrue.CudaPtr, score.CudaPtr, batchSize, MSEResult.CudaPtr, 0);
            MSEResult.CopyOutFromCuda();
            //if (MSEResult.MemPtr[0] / MSEResult.MemPtr[1] >= 1)
            //    Console.WriteLine("fasdgasdf");
        }



        public override void PushScore(float[] groundTrue, float[] score, int batchSize)
        {
            for (int i = 0; i < batchSize; i++)
            {
                LabelInfo.Add(groundTrue[i]);
                PredInfo.Add(score[i]);
            }
        }

        protected override void Save(string scoreFile)
        {
            StreamWriter mwriter = new StreamWriter(scoreFile);
            for (int i = 0; i < LabelInfo.Count; i++)
                mwriter.WriteLine("{0}\t{1}", LabelInfo[i], PredInfo[i]);
            mwriter.Close();
        }

        public float EvaluationCPU(out List<string> validationFileLines)
        {
            return base.Evaluation(out validationFileLines);
        }

        public override float Evaluation(out List<string> validationFileLines)
        {
            MSEResult.CopyOutFromCuda();
            validationFileLines = new List<string>();
            validationFileLines.Add(string.Format("MSE {0}\t RMSE {1}", MSEResult[0] * 1.0f / MSEResult[1], Math.Sqrt(MSEResult[0] * 1.0f / MSEResult[1])));
            return (float)Math.Sqrt(MSEResult[0] * 1.0f / MSEResult[1]);
        }

    }
    public class ACCURACYEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.ACCURACY; } }
        List<float> LabelInfo = new List<float>();
        List<float> PredInfo = new List<float>();
        int Dim;
        public override void PushScore(float[] groundTrue, float[] score, int dim, int batchSize)
        {
            Dim = dim;
            for (int i = 0; i < batchSize; i++)
            {
                LabelInfo.Add(groundTrue[i] - 1);
                for (int d = 0; d < dim; d++)
                {
                    PredInfo.Add(score[i * dim + d]);
                }
            }
        }

        public static float CalculateMultiAccuracy(List<int> label, List<float[]> pred, int dim, int num)
        {
            int trueNum = 0;
            for (int i = 0; i < num; i++)
            {
                int target = label[i];
                int pidx = Util.MaximumValue(pred[i], 0, dim);

                if (pidx == target) trueNum += 1;
                
            }
            return trueNum * 1.0f / num;
        }

        public static int[] CalculateConfuseMatrix(List<int> label, List<float[]> pred, int dim, int num)
        {
            int[] confuseMatrix = new int[dim * dim];
            for (int i = 0; i < num; i++)
            {
                int target = (int)(label[i]);
                int maxIndex = Util.MaximumValue(pred[i], 0, dim);
                confuseMatrix[target * dim + maxIndex] += 1;
            }
            return confuseMatrix;
        }

        public override void PushScore(float[] groundTrue, float[] score, float labelBias, int dim, int batchSize)
        {
            Dim = dim;
            for (int i = 0; i < batchSize; i++)
            {
                LabelInfo.Add(groundTrue[i] - labelBias);
                for (int d = 0; d < dim; d++)
                {
                    PredInfo.Add(score[i * dim + d]);
                }
            }
        }

        protected override void Save(string scoreFile)
        {
            using (StreamWriter writer = new StreamWriter(scoreFile))
            {
                for (int i = 0; i < LabelInfo.Count; i++)
                {
                    StringBuilder strBuilder = new StringBuilder();
                    //LabelInfo[i].ToString() + "\t" +
                    float[] prob = Util.Softmax(PredInfo.Skip(i * Dim).Take(Dim).ToArray(), 1);
                    strBuilder.Append(string.Join("\t", prob));
                    writer.WriteLine(strBuilder.ToString());
                }
            }
        }

        float CalculateConfuseMatrix(int[] label, int[] pred, int num, out int[] confuseMatrix)
        {
            confuseMatrix = new int[Dim * Dim];
            int trueNum = 0;
            int totalNum = 0;
            for (int i = 0; i < num; i++)
            {
                int target = (int)(label[i]);
                if (target < 0) continue;
                int maxIndex = pred[i];
                if (maxIndex == target) trueNum += 1;
                totalNum += 1;
                confuseMatrix[target * Dim + maxIndex] += 1;
            }
            return trueNum * 1.0f / (totalNum + float.Epsilon);
        }

        float KappaScore(int[] label, int[] pred, int[] confuseMatrix, int num)
        {
            int[] label_distr = new int[Dim];
            int[] pred_distr = new int[Dim];

            for (int i = 0; i < num; i++)
            {
                if (label[i] >= 0) label_distr[label[i]] += 1;
                pred_distr[pred[i]] += 1;
            }

            float[] weight = new float[Dim * Dim];
            for (int i = 0; i < Dim; i++)
                for (int j = 0; j < Dim; j++)
                    weight[i * Dim + j] = (i - j) * (i - j) / ((Dim - 1.0f) * (Dim - 1.0f));

            float alpha = 0;
            float beta = 0;
            for (int i = 0; i < Dim; i++)
                for (int j = 0; j < Dim; j++)
                {
                    alpha += weight[i * Dim + j] * confuseMatrix[i * Dim + j];
                    beta += weight[i * Dim + j] * label_distr[i] * pred_distr[j] * 1.0f / num;
                }

            //IEnumerable<int> confuseMatrix = 
            return 1 - alpha / (beta + float.Epsilon);
        }

        public override float Evaluation(out List<string> validationFileLines)
        {
            if (File.Exists(ScoreFile))
            {
                File.Delete(ScoreFile);
            }

            if (!ScoreFile.Equals(string.Empty))
            {
                Save(ScoreFile);
            }

            int[] predId = new int[LabelInfo.Count];
            for (int i = 0; i < LabelInfo.Count; i++)
            {
                int target = (int)(LabelInfo[i]);
                if (target < 0) continue;

                float max = PredInfo[i * Dim];
                int maxIndex = 0;
                for (int d = 1; d < Dim; d++)
                {
                    if (PredInfo[i * Dim + d] > max)
                    {
                        max = PredInfo[i * Dim + d];
                        maxIndex = d;
                    }
                }
                predId[i] = maxIndex;
            }

            int[] confuseMatrix;
            float accuracy = CalculateConfuseMatrix(LabelInfo.Select(i => (int)i).ToArray(), predId, LabelInfo.Count, out confuseMatrix);

            float kappa = KappaScore(LabelInfo.Select(i => (int)i).ToArray(), predId, confuseMatrix, LabelInfo.Count);

            validationFileLines = new List<string>();
            validationFileLines.Add(string.Format("Score {0}\t Kappa {1}", accuracy, kappa));
            for (int i = 0; i < Dim; i++) validationFileLines.Add(string.Join("\t", confuseMatrix.Skip(i * Dim).Take(Dim)));
            return accuracy;
        }
    }
    public class ORDINALACCURACYEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.ORDINALACCURACY; } }
        List<float> LabelInfo = new List<float>();
        List<float> PredInfo = new List<float>();
        int Dim;
        public override void PushScore(float[] groundTrue, float[] score, float minValue, float maxValue, int batchSize)
        {
            Dim = (int)(maxValue - minValue + 1);
            for (int i = 0; i < batchSize; i++)
            {
                LabelInfo.Add(groundTrue[i] - minValue);
                float mv = Util.Logistic(score[i]) * (maxValue - minValue);
                float[] mvSet = Enumerable.Range(0, Dim).Select(d => (mv - d) * (mv - d)).ToArray();
                int id = Util.MinimumValue(mvSet);
                int count = PredInfo.Count;
                for (int d = 0; d < Dim; d++)
                    PredInfo.Add(0);
                PredInfo[count + id] = 1;
            }
        }

        protected override void Save(string scoreFile)
        {
            using (StreamWriter writer = new StreamWriter(scoreFile))
            {
                for (int i = 0; i < LabelInfo.Count; i++)
                {
                    StringBuilder strBuilder = new StringBuilder();
                    strBuilder.Append(LabelInfo[i].ToString() + "\t" + string.Join(",", PredInfo.Skip(i * Dim).Take(Dim)));
                    writer.WriteLine(strBuilder.ToString());
                }
            }
        }

        float CalculateConfuseMatrix(int[] label, int[] pred, int num, out int[] confuseMatrix)
        {
            confuseMatrix = new int[Dim * Dim];
            int trueNum = 0;
            int totalNum = 0;
            for (int i = 0; i < num; i++)
            {
                int target = (int)(label[i]);
                if (target < 0) continue;
                int maxIndex = pred[i];
                if (maxIndex == target) trueNum += 1;
                totalNum += 1;
                confuseMatrix[target * Dim + maxIndex] += 1;
            }
            return trueNum * 1.0f / (totalNum + float.Epsilon);
        }

        float KappaScore(int[] label, int[] pred, int[] confuseMatrix, int num)
        {
            int[] label_distr = new int[Dim];
            int[] pred_distr = new int[Dim];

            for (int i = 0; i < num; i++)
            {
                if (label[i] >= 0) label_distr[label[i]] += 1;
                pred_distr[pred[i]] += 1;
            }

            float[] weight = new float[Dim * Dim];
            for (int i = 0; i < Dim; i++)
                for (int j = 0; j < Dim; j++)
                    weight[i * Dim + j] = (i - j) * (i - j) / ((Dim - 1.0f) * (Dim - 1.0f));

            float alpha = 0;
            float beta = 0;
            for (int i = 0; i < Dim; i++)
                for (int j = 0; j < Dim; j++)
                {
                    alpha += weight[i * Dim + j] * confuseMatrix[i * Dim + j];
                    beta += weight[i * Dim + j] * label_distr[i] * pred_distr[j] * 1.0f / num;
                }

            //IEnumerable<int> confuseMatrix = 
            return 1 - alpha / (beta + float.Epsilon);
        }

        public override float Evaluation(out List<string> validationFileLines)
        {
            if (File.Exists(ScoreFile))
            {
                File.Delete(ScoreFile);
            }

            //Save(ScoreFile);

            int[] predId = new int[LabelInfo.Count];
            for (int i = 0; i < LabelInfo.Count; i++)
            {
                int target = (int)(LabelInfo[i]);
                if (target < 0)
                    continue;

                float max = PredInfo[i * Dim];
                int maxIndex = 0;
                for (int d = 1; d < Dim; d++)
                {
                    if (PredInfo[i * Dim + d] > max)
                    {
                        max = PredInfo[i * Dim + d];
                        maxIndex = d;
                    }
                }
                predId[i] = maxIndex;
            }

            int[] confuseMatrix;
            float accuracy = CalculateConfuseMatrix(LabelInfo.Select(i => (int)i).ToArray(), predId, LabelInfo.Count, out confuseMatrix);

            float kappa = KappaScore(LabelInfo.Select(i => (int)i).ToArray(), predId, confuseMatrix, LabelInfo.Count);

            validationFileLines = new List<string>();
            validationFileLines.Add(string.Format("Score {0}\t Kappa {1}", accuracy, kappa));
            for (int i = 0; i < Dim; i++) validationFileLines.Add(string.Join("\t", confuseMatrix.Skip(i * Dim).Take(Dim)));
            return accuracy;
        }
    }
    public class ORDEDLOGITACCURACYEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.ORDEDLOGITACCURACY; } }
        List<float> LabelInfo = new List<float>();
        List<float> PredInfo = new List<float>();
        int Dim;

        public override void PushScore(float[] groundTrue, float[] score, float minValue, float maxValue, float gamma, int batchSize)
        {
            Dim = (int)(maxValue - minValue + 1);
            for (int i = 0; i < batchSize; i++)
            {
                for (int targetId = (int)minValue; targetId <= (int)maxValue; targetId++)
                {
                    float a = targetId == (int)minValue ? 1 : Util.Logistic(gamma * (score[i] - (targetId - 1 - minValue) * 1.0f / (maxValue - minValue)));
                    float b = targetId == (int)maxValue ? 0 : Util.Logistic(gamma * (score[i] - (targetId - minValue) * 1.0f / (maxValue - minValue)));// beta[targetId]));
                    PredInfo.Add(a - b);
                }
                LabelInfo.Add(groundTrue[i] - minValue);
            }
        }

        protected override void Save(string scoreFile)
        {
            using (StreamWriter writer = new StreamWriter(scoreFile))
            {
                for (int i = 0; i < LabelInfo.Count; i++)
                {
                    StringBuilder strBuilder = new StringBuilder();
                    strBuilder.Append(LabelInfo[i].ToString() + "\t" + string.Join(",", PredInfo.Skip(i * Dim).Take(Dim)));
                    writer.WriteLine(strBuilder.ToString());
                }
            }
        }

        float CalculateConfuseMatrix(int[] label, int[] pred, int num, out int[] confuseMatrix)
        {
            confuseMatrix = new int[Dim * Dim];
            int trueNum = 0;
            int totalNum = 0;
            for (int i = 0; i < num; i++)
            {
                int target = (int)(label[i]);
                if (target < 0) continue;
                int maxIndex = pred[i];
                if (maxIndex == target) trueNum += 1;
                totalNum += 1;
                confuseMatrix[target * Dim + maxIndex] += 1;
            }
            return trueNum * 1.0f / (totalNum + float.Epsilon);
        }

        float KappaScore(int[] label, int[] pred, int[] confuseMatrix, int num)
        {
            int[] label_distr = new int[Dim];
            int[] pred_distr = new int[Dim];

            for (int i = 0; i < num; i++)
            {
                if (label[i] >= 0) label_distr[label[i]] += 1;
                pred_distr[pred[i]] += 1;
            }

            float[] weight = new float[Dim * Dim];
            for (int i = 0; i < Dim; i++)
                for (int j = 0; j < Dim; j++)
                    weight[i * Dim + j] = (i - j) * (i - j) / ((Dim - 1.0f) * (Dim - 1.0f));

            float alpha = 0;
            float beta = 0;
            for (int i = 0; i < Dim; i++)
                for (int j = 0; j < Dim; j++)
                {
                    alpha += weight[i * Dim + j] * confuseMatrix[i * Dim + j];
                    beta += weight[i * Dim + j] * label_distr[i] * pred_distr[j] * 1.0f / num;
                }

            //IEnumerable<int> confuseMatrix = 
            return 1 - alpha / (beta + float.Epsilon);
        }

        public override float Evaluation(out List<string> validationFileLines)
        {
            if (File.Exists(ScoreFile))
            {
                File.Delete(ScoreFile);
            }

            //Save(ScoreFile);

            int[] predId = new int[LabelInfo.Count];
            for (int i = 0; i < LabelInfo.Count; i++)
            {
                int target = (int)(LabelInfo[i]);
                if (target < 0)
                    continue;

                float max = PredInfo[i * Dim];
                int maxIndex = 0;
                for (int d = 1; d < Dim; d++)
                {
                    if (PredInfo[i * Dim + d] > max)
                    {
                        max = PredInfo[i * Dim + d];
                        maxIndex = d;
                    }
                }
                predId[i] = maxIndex;
            }

            int[] confuseMatrix;
            float accuracy = CalculateConfuseMatrix(LabelInfo.Select(i => (int)i).ToArray(), predId, LabelInfo.Count, out confuseMatrix);

            float kappa = KappaScore(LabelInfo.Select(i => (int)i).ToArray(), predId, confuseMatrix, LabelInfo.Count);

            validationFileLines = new List<string>();
            validationFileLines.Add(string.Format("Score {0}\t Kappa {1}", accuracy, kappa));
            for (int i = 0; i < Dim; i++) validationFileLines.Add(string.Join("\t", confuseMatrix.Skip(i * Dim).Take(Dim)));
            return accuracy;
        }
    }
    public class NDCGEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.NDCG; } }
        List<string> QueryIds = new List<string>();
        List<string> DocIds = new List<string>();
        List<HRSItem> Ratings = new List<HRSItem>();
        List<float> Scores = new List<float>();

        public bool IsEmpty { get { return QueryIds.Count == 0 || DocIds.Count == 0; } }

        public override void Init(BasicEvalParameter parameter)
        {
            base.Init(parameter);
            InitMappingFile(parameter.labelFile);
        }

        public void InitMappingFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return;
            }

            Stream fs = null;
            try
            {
                fs = FileUtil.CreateReadFS(fileName); //.GetReadStream(fileName, false);
            }
            catch
            {
                return;
            }

            if (fs != null)
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    string header = reader.ReadLine();
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        string[] columns = line.Trim().Split('\t');
                        QueryIds.Add(columns[0]);
                        DocIds.Add(columns[1]);
                    }
                }
            }
        }

        public override void PushScore(float[] groundTrue, float[] score, int batchSize)
        {
            for (int i = 0; i < batchSize; i++)
            {
                Ratings.Add((HRSItem)((int)groundTrue[i]));
                Scores.Add(score[i]);
            }
        }

        public override void PushScore(int[] srcIdx, int[] tgtIdx, float[] groundTrue, float[] score, int batchSize)
        {
            int queryId = QueryIds.Count;
            int docId = DocIds.Count;

            for (int i = 0; i < batchSize; i++)
            {
                QueryIds.Add((queryId + srcIdx[i]).ToString());
                DocIds.Add((docId + tgtIdx[i]).ToString());
                Ratings.Add((HRSItem)((int)groundTrue[i]));
                Scores.Add(score[i]);
            }
        }

        protected override void Save(string scoreFile)
        {
            StreamWriter mwriter = new StreamWriter(scoreFile);
            mwriter.WriteLine("{0}\t{1}\t{2}\t{3}", "m:QueryId", "m:Url", "m:Rating", "Hashing");
            for (int i = 0; i < Ratings.Count; i++)
            {
                mwriter.WriteLine("{0}\t{1}\t{2}\t{3}", QueryIds[i], DocIds[i], Ratings[i], Scores[i]);
            }
            mwriter.Close();
        }

        public static List<string> EvaluateNDCG(string filePath, string metricPath)
        {
            return EvaluationSet.CallEvaluationTool(filePath, metricPath, EvaluationToolkit.EvalToolExe(EvaluationType.NDCG));
        }
    }
    public class BLEUEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.BSBLEU; } }
        public override string EvalTool { get { return string.Format(EvaluationToolkit.Path + @"\Evaluation_{0}\{0}.py", Type.ToString()); } }

        List<Tuple<string, string>> PairList = new List<Tuple<string, string>>();

        public override void PushScore(string groundTrue, string decode)
        {
            PairList.Add(new Tuple<string, string>(groundTrue, decode));
        }

        public static Tuple<List<string>, float> EvaluateBLEU(string filePath, string metricPath)
        {
            List<string> result = EvaluationSet.CallEvaluationTool(filePath, metricPath, EvaluationToolkit.EvalToolPython(EvaluationType.BSBLEU));
            return new Tuple<List<string>, float>(result, float.Parse(result[0]));
        }

        protected override void Save(string scoreFile)
        {
            StreamWriter mwriter = new StreamWriter(scoreFile);
            for (int i = 0; i < PairList.Count; i++)
            {
                mwriter.WriteLine(PairList[i].Item1 + "\t" + PairList[i].Item2);
            }
            mwriter.Close();
        }

        public static float[] EvaluateBLEU(string[] reference, string[] candidate)
        {
            string tmpInFile = string.Format("tmp_bleu_{0}.in", ParameterSetting.Random.Next());
            string tmpOutFile = string.Format("tmp_bleu_{0}.out", ParameterSetting.Random.Next());
            using (StreamWriter writer = new StreamWriter(tmpInFile))
            {
                for (int i = 0; i < reference.Length; i++)
                {
                    writer.WriteLine(string.Format("{0}\t{1}", reference[i], candidate[i]));
                }
            }
            float[] result = EvaluationSet.CallEvaluationTool(tmpInFile, tmpOutFile, EvaluationToolkit.EvalToolPython(EvaluationType.BSBLEU)).Select(i => float.Parse(i)).ToArray();
            File.Delete(tmpInFile);
            File.Delete(tmpOutFile);
            return result;
        }

    }

    public class PlainBLEUEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.PLAIN_BLEU; } }
        public override string EvalTool { get { return string.Empty; } }
        /// <summary>
        /// Return a dictionary of ngram counts(up to length /max_n/)
        /// for sentence(list of words) /snt/.
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="ngram"></param>
        static Dictionary<string, int> count_ngrams(List<int> seq, int ngram, HashSet<string> filterNgrams = null)
        {
            Dictionary<string, int> ret = new Dictionary<string, int>();
            for (int i = 0; i < seq.Count; i++)
            {
                for (int k = 1; k <= ngram; k++)
                {
                    if (i + k > seq.Count) continue;
                    string mkey = string.Join("-", seq.Skip(i).Take(k));
                    if (filterNgrams != null && filterNgrams.Contains(mkey))
                        continue;

                    if (!ret.ContainsKey(mkey)) ret.Add(mkey, 1);
                    else ret[mkey] += 1;
                }
            }
            return ret;
        }

        static Dictionary<string, int> MaxCount(Dictionary<string, int> d1, Dictionary<string, int> d2)
        {
            Dictionary<string, int> ret = new Dictionary<string, int>();
            foreach (string mkey in d1.Keys.Union(d2.Keys))
            {
                int v1 = 0;
                if (d1.ContainsKey(mkey))
                {
                    v1 = d1[mkey];
                }

                int v2 = 0;
                if (d2.ContainsKey(mkey))
                {
                    v2 = d2[mkey];
                }
                ret.Add(mkey, Math.Max(v1, v2));
            }
            return ret;
        }

        static int[] NGramHits(Dictionary<string, int> candidate, Dictionary<string, int> reference, int ngram)
        {
            int[] hitArray = new int[ngram];
            //for every candidate ngram
            foreach (KeyValuePair<string, int> item in candidate)
            {
                int g = item.Key.Where(i => i == '-').Count();

                int c = item.Value;
                if (!reference.ContainsKey(item.Key)) { c = 0; }
                else if (reference[item.Key] < c) { c = reference[item.Key]; }
                hitArray[g] += c;
            }
            return hitArray;
        }

        static float Bleu(List<int> candidate, List<int> reference, float[] wngram, HashSet<string> filterNGrams = null)
        {
            int ngram = wngram.Length;
            Dictionary<string, int> c_ngram = count_ngrams(candidate, ngram, filterNGrams);
            Dictionary<string, int> r_ngram = count_ngrams(reference, ngram, filterNGrams);
            int[] hits = NGramHits(c_ngram, r_ngram, ngram);

            int candlen = candidate.Count;
            int reflen = reference.Count;

            float ret = 0;
            for (int n = 0; n < ngram; n++)
            {
                float prec = hits[n] == 0 ? 0 : hits[n] * 1.0f / (candlen - n);
                ret = ret + (float)Math.Log(prec + 0.00000001f) * wngram[n];
            }
            float bp = (float)Math.Min(1, Math.Exp(1 - reflen * 1.0f / candlen));
            ret += (float)Math.Log(bp);
            return (float)Math.Exp(ret);
        }

        public static List<float> EvaluateBLEU(List<List<int>> reference, List<List<int>> candidate, float[] wngram, HashSet<string> filterNGrams)
        {
            //string tmpInFile = string.Format("tmp_bleu_{0}.in", ParameterSetting.Random.Next());
            //using (StreamWriter writer = new StreamWriter(tmpInFile))
            //{
            //    for (int i = 0; i < reference.Count; i++)
            //    {
            //        writer.WriteLine(string.Format("{0}\t{1}", string.Join(" ", reference[i]), string.Join(" ", candidate[i])));
            //    }
            //}
            //File.Delete(tmpInFile);
            //remove the last character.

            List<float> results = new List<float>();
            for (int i = 0; i < reference.Count; i++)
            {
                //if (isremove)
                //{
                //.Where(t => t != 2)
                results.Add(Bleu(candidate[i].Take(candidate[i].Count - 1).ToList(), reference[i].Take(reference[i].Count - 1).ToList(), wngram, filterNGrams));
                //}
                //else
                //{
                //    results.Add(Bleu(candidate[i], reference[i], ngram));
                //}
            }
            return results;
        }
    }

    public class NLTKBLEUEvaluationSet : EvaluationSet
    {
        public override EvaluationType Type { get { return EvaluationType.NLTK_BLEU; } }
        public override string EvalTool { get { return string.Format(EvaluationToolkit.Path + @"\Evaluation_{0}\{0}.py", Type.ToString()); } }

        List<Tuple<string, string>> PairList = new List<Tuple<string, string>>();

        public override void PushScore(string groundTrue, string decode)
        {
            PairList.Add(new Tuple<string, string>(groundTrue, decode));
        }

        public static float[] EvaluateBLEU(string[] reference, string[] candidate)
        {
            string tmpInFile = string.Format("tmp_bleu_{0}.in", ParameterSetting.Random.Next());
            string tmpOutFile = string.Format("tmp_bleu_{0}.out", ParameterSetting.Random.Next());
            using (StreamWriter writer = new StreamWriter(tmpInFile))
            {
                for(int i=0;i<reference.Length;i++)
                {
                    writer.WriteLine(string.Format("{0}\t{1}", reference[i], candidate[i]));
                }
            }
            float[] result = EvaluationSet.CallEvaluationTool(tmpInFile, tmpOutFile, EvaluationToolkit.EvalToolPython(EvaluationType.NLTK_BLEU)).Select(i => float.Parse(i)).ToArray();
            File.Delete(tmpInFile);
            File.Delete(tmpOutFile);
            return result;
        }

        protected override void Save(string scoreFile)
        {
            StreamWriter mwriter = new StreamWriter(scoreFile);
            for (int i = 0; i < PairList.Count; i++)
            {
                mwriter.WriteLine(PairList[i].Item1 + "\t" + PairList[i].Item2);
            }
            mwriter.Close();
        }
    }

}
