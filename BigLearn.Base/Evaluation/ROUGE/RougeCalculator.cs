﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BigLearn
{
    public static class RougeCalculator
    {
        public static long BinCoef(long n, long k)
        {
            long answer = 1;
            if (k > n)
            {
                return 0;
            }
            for(int i = 1; i <= k; i++)
            {
                answer *= (n + 1 - i);
                answer /= i;
            }
            return answer;
        }

        public static RougeScore EvalNGram(string[] candidate, List<string[]> references, int n)
        {
            // Extract n-grams
            string[] candidateGrams = ExtractNgrams(candidate, 0, n);

            float[,] scores = new float[references.Count, 2];

            int referenceIndex = 0;
            foreach (string[] reference in references)
            {
                // Extract grams from the tokenized reference
                string[] referenceGrams = ExtractNgrams(reference, 0, n);

                //making 1-Gram's unique made deviation larger. change! Actually maybe distinct grams increases 2-gram.
                scores[referenceIndex, 0] = Score(candidateGrams, referenceGrams);
                scores[referenceIndex, 1] = Score(referenceGrams, candidateGrams);
                referenceIndex++;
            }

            float precision = 0;
            float recall = 0;
            for(int i = 0; i < referenceIndex; i++)
            {
                precision += scores[i, 0];
                recall += scores[i, 1];
            }
            precision /= referenceIndex;
            recall /= referenceIndex;
            return new RougeScore(precision, recall);
        }

        //N-grams are every collection of N words, diregarding order, that exist in the original string
        //Actually, we do care about order (in some sense). Every n-gram, by default, comes in the order
        //that they do in the actual string. 
        //We find these recursively.
        //Returns an array of n-grams, words separated by spaces
        public static string[] ExtractNgrams(string[] text, int index, int n)
        {
            // Sanity check: If there are fewer than n words, no way we're
            // going to produce any n-grams
            if (n == 0)
            {
                return new string[1] { "" };
            }

            string[] ngrams = new string[BinCoef((long)text.Length - index, n)];
            int curindex = 0;
            for(int i = index; i <= text.Length - n; i++)
            {
                string[] subgrams = ExtractNgrams(text, i + 1, n - 1);
                for(int j = 0; j < subgrams.Length; j++)
                {
                    ngrams[curindex] = text[i] + " " + subgrams[j];
                    curindex++;
                }
            }
            if(index == 0)
            {
                for(int i = 0; i < ngrams.Length; i++)
                {
                    string[] tokens = ngrams[i].Split();
                    Array.Sort(tokens);
                    ngrams[i] = String.Join(" ", tokens);
                }
                Array.Sort(ngrams);
            }
            return ngrams;
        }

        public static float Score(string[] ngrams1, string[] ngrams2)
        {
            int overlap = NumOverlap(ngrams1, ngrams2);
            return (float)overlap / (float)(ngrams1.Length + float.Epsilon);
        }

        //Returns # of elements in a that are in b
        public static int NumOverlap(string[] a, string[] b)
        {
            int overlap = 0;
            int indexa = 0;
            int indexb = 0;
            while(indexa < a.Length && indexb < b.Length)
            {
                int compare = a[indexa].CompareTo(b[indexb]);
                if(compare == 0)
                {
                    overlap++;
                    indexa++;
                    indexb++;
                }
                else if(compare < 0)
                {
                    indexa++;
                }
                else
                {
                    indexb++;
                }
            }
            return overlap;
        }

        //Not used for Rouge-N yet
        public static RougeScore EvalLCS(string[] candidate, List<string[]> references)
        {
            float recall = 0f;
            float precision = 0f;
            float bestRecall = 0f;
            float bestPrecision = 0f;
            foreach (string[] reference in references)
            {
                // ROUGE-L Calculations: refer to equations (2)-(4) in http://www.aclweb.org/anthology/W04-1013
                int lcs = LongestCommonSubsequence(candidate, reference);
                recall = (float)lcs / (float)reference.Length;  // (2)
                precision = (float)lcs / (float)candidate.Length;  // (3)

                if (recall > bestRecall)
                {
                    bestRecall = recall;
                }

                if (precision > bestPrecision)
                {
                    bestPrecision = precision;
                }
            }

            return new RougeScore(bestPrecision, bestRecall);
        }

        static int LongestCommonSubsequence(string[] candidate, string[] reference)
        {
            // Normalize inputs by converting them to lowercase
            candidate = (from word in candidate select word.ToLower()).ToArray();
            reference = (from word in reference select word.ToLower()).ToArray();

            // initialize pointers for navigating the search space
            int startIndex = 0,
                candidateEndIdx = candidate.Length - 1,
                referenceEndIdx = reference.Length - 1,
                similarityCount = 0;

            // If the first two entries are identical, ignore them in the final comparison,
            // but bump the count.
            while (candidate[startIndex] == reference[startIndex])
            {
                startIndex++;
                similarityCount++;
            }

            // If the last two entries are identical, ignore them in the final comparison,
            // but bump the count.
            while (candidate[candidateEndIdx] == reference[referenceEndIdx])
            {
                candidateEndIdx--;
                referenceEndIdx--;
                similarityCount++;
            }

            // Slice the original arrays with the new indices
            string[] trimmedCandidate = candidate.Skip(startIndex).Take(candidateEndIdx - startIndex).ToArray();
            string[] trimmedReference = reference.Skip(startIndex).Take(referenceEndIdx - startIndex).ToArray();

            // Preallocate an update table
            var lcsTable = new int[trimmedCandidate.Length, trimmedReference.Length];

            // Perform the Bellman-Ford Update by looping through the update table.
            // If entries match, increment the current value in the table. Otherwise,
            // propagate the previous value. Note that this table is triangular.
            for (var i = 1; i <= trimmedReference.Length; i++)
            {
                for (var j = 1; j <= trimmedCandidate.Length; j++)
                {
                    if (trimmedReference[i - 1] == trimmedCandidate[j - 1])
                    {
                        lcsTable[i, j] = lcsTable[i - 1, j - 1] + 1;
                    }
                    else
                    {
                        lcsTable[i, j] = Math.Max(lcsTable[i, j - 1], lcsTable[i - 1, j]);
                    }
                }
            }

            // The last value in the update table is the length of the LCS for the
            // reduced search space. Add that to the original number of matches
            // to get the final result
            return lcsTable[trimmedReference.Length, trimmedCandidate.Length] + similarityCount;
        }

        // Come back to this later if we need to use the jack knife (bootstrap) approach.
        // This should work with ROUGE-N, but not with ROUGE-L in its current state.
        public static float JackKnife(string[] candidate, List<string[]> references, int n, EvalMethod evaluate)
        {
            // Preallocate an array to hold the results of performing
            // pairwise comparisons
            var pairwiseResults = new float[references.Count];

            for (var i = 0; i < references.Count; i++)
            {
                // Go through each reference in order
                float[] result = new float[references.Count];
                for (var j = 0; j < references.Count; j++)
                {
                    // Exclude the current reference as we go through, effectively
                    // performing a leave-one-out comparison
                    if (j != i)
                    {
                        result[j] = evaluate(candidate, references[j], n);
                    }
                }
                // Only consider the argmax for the current set of results
                pairwiseResults[i] = result.Max();
            }
            // Average all the pairwise results            
            return pairwiseResults.Sum() / pairwiseResults.Count();
        }

        public delegate float EvalMethod(string[] candidate, string[] reference, int n);
    }

}

