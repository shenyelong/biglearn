﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BigLearn
{
    public class RougeScore
    {
        float _r;
        float _p;
        float _f;

        public RougeScore(float r=0f, float p=0f, float f=0f)
        {
            Recall = r;
            Precision = p;
            UpdateFscore(p, r);
        }

        public RougeScore (float precision, float recall)
        {
            Recall = recall;
            Precision = precision;
            UpdateFscore(precision, recall);
        }

        public float Recall
        {
            get { return _r; }
            set
            {
                if (value <= float.Epsilon && _p <= float.Epsilon)
                {
                    // If R == P == 0, define F=0 so it isn't NaN.
                    _r = 0;
                    _f = 0;
                }
                else
                {
                    _r = value;
                    UpdateFscore(Precision, Recall);
                }
            }
        }

        public float Precision
        {
            get { return _p; }
            set
            {
                if (value <= float.Epsilon && _r <= float.Epsilon)
                {
                    // If R == P == 0, define F=0 so it isn't NaN.
                    _p = 0;
                    _f = 0;
                }
                else
                {
                    _p = value;
                    UpdateFscore(Precision, Recall);
                }
            }
        }

        public float Fscore
        {
            get { return _f; }
        }

        void UpdateFscore(float precision, float recall)
        {
            if (precision <= float.Epsilon || recall <= float.Epsilon)
            {
                _f = 0;
            }
            else
            {
                //alpha is any value between 0 and 1. 
                //a larger alpha means we prefer Precision over recall, and vice versa
                float alpha = 0.5f;
                _f = 1 / ((alpha / precision) + ((1 - alpha) / recall));
            }
        }

        public static RougeScore Average(IEnumerable<RougeScore> scores)
        {
            float averagePrecision = 0f;
            float averageRecall = 0f;
            int count = scores.Count();

            foreach(RougeScore rougeScore in scores)
            {
                if (rougeScore.isNaN())
                {
                    Console.WriteLine("NaN ROUGE score detected");
                    count--;
                    continue;
                }
                averagePrecision += rougeScore.Precision;
                averageRecall += rougeScore.Recall;
            }

            averagePrecision /= count;
            averageRecall /= count;

            return new RougeScore(averagePrecision, averageRecall);
        }

        bool isNaN()
        {
            if (Recall.Equals(float.NaN) || Precision.Equals(float.NaN) || Fscore.Equals(float.NaN))
            {
                return true;
            }
            return false;
        }
    }
}
