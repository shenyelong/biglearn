﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace BigLearn
{
    public class RougeHelper
    {
        // split a text into individual words
        public static string[] Tokenize(string text)
        {
            // convert words to lowercase
            text = text.ToLower();
            // remove punctuation
            Regex rgx1 = new Regex("[^ a-zA-Z0-9]");
            Regex rgx2 = new Regex("\\s+");
            text = rgx2.Replace(text, " ");
            text = rgx1.Replace(text, "");

            // split on spaces
            return text.Split().ToArray();
        }

        // Find all reference summaries in referenceDirectory for a particular candidate.
        public static List<string> SummaryMatcher(string candidate, string[] references)
        {
            string candidateFileName = Path.GetFileNameWithoutExtension(candidate);
            var referenceFiles = new List<string>();
            foreach (string referenceFile in references)
            {
                if (Path.GetFileNameWithoutExtension(referenceFile).StartsWith(candidateFileName))
                {
                    referenceFiles.Add(referenceFile);
                }
            }

            return referenceFiles;
        }

    }
}
