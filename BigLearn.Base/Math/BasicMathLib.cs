﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public unsafe class BasicMathlib
    {
        public static int THREAD_NUMBER = ParameterSetting.BasicMathLibThreadNum;

        public unsafe static void Transpose(float * src, int row, int column)
        {
            float[] copy = new float[row * column];
            for (int i = 0; i < row * column; i++) copy[i] = src[i];

            for(int i = 0; i < row * column; i++)
            {
                int next_i = (i % column) * row + i / column; 
                src[next_i] = copy[i];
            }

            /*
            int i = 1;
            while (i < row * column) 
            { 
                int cycleBegin = i; 
                if(!ismoved[cycleBegin])
                {
                    float t = src[i]; 
                    do
                    { 
                        // Input matrix [r x c] 
                        // Output matrix  
                        // i_new = (i*r)%(N-1) 
                        int next_i = (i % column) * row + i / column; 
                        
                        float tmp = src[next_i];
                        src[next_i] = t;
                        ismoved[next_i] = true;
                        t = tmp;
                        i = next_i; 
                    } 
                    while (i != cycleBegin); 
                }
                i = cycleBegin + 1;
            } */
        }

        public unsafe static void Scale_Vector(float* floats_a, int m, float mweight)
        {
            int process_len = (m + THREAD_NUMBER - 1) / THREAD_NUMBER;
            Parallel.For(0, THREAD_NUMBER, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < m)
                    {
                        floats_a[cnt] = floats_a[cnt] * mweight; //(float)log( (float)gpu_floats_a[idx]);
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }


        static int GetTransposeIdx(int[] dims, int[] trans, int[] out_transDim)
        {
            int length = 1;
            for (int i = 0; i < dims.Length; i++)
            {
                length *= dims[i];
            }

            int d = 1;
            for (int i = 0; i < dims.Length; i++) //dims.Length - 1; i >= 0; i--)
            {
                int newi = trans[i];
                out_transDim[newi] = d;
                d = d * dims[newi];
            }

            return length;
        }


        public unsafe static void Transpose(float * src, int[] dims, int[] transpose)
        {
            int[] out_transDim = new int[dims.Length];
            int length = GetTransposeIdx(dims, transpose, out_transDim);

            float[] copy = new float[length];
            for (int i = 0; i < length; i++) copy[i] = src[i];

            for (int idx = 0; idx < length; idx++)
            {
                int c = idx;
                int newc = 0;
                for (int d = 0; d < dims.Length; d++) 
                {
                    int pk = c % dims[d];
                    newc = newc + pk * out_transDim[d];
                    c = c / dims[d];
                }
                src[newc] = copy[idx];
            }

        }
//0 1
//2 3
//4 5
//6 7
//8 9
// 0 1 2 3 4 5 6 7 8 9
// 0 2 4 x 8 1 x 5 7 x
        public unsafe static void Inner_Product_Matching(float* a, float* b, float* c, int * matchIdxA, int * matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = matchSize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int aID = matchIdxA[idx];
                        int bID = matchIdxB[idx];
                        float sumxy = 0;
                        aID = aID * dimension;
                        bID = bID * dimension;
                        for (int i = 0; i < dimension; i++)
                        {
                            sumxy += a[aID + i] * b[bID + i];
                        }
                        c[idx] = (float)(sumxy * 1.0f);
                    }
                    else
                        break;
                }
            });
        }

        public unsafe static void Cosine_Similarity_Matching(float* a, float* b, float* c, int* matchIdxA, int* matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, float* aa, float* bb, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = matchSize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int aID = matchIdxA[idx];
                        int bID = matchIdxB[idx];
                        float sumxx = aa[aID];
                        float sumyy = bb[bID];
                        float sumxy = 0;
                        aID = aID * dimension;
                        bID = bID * dimension;
                        for (int i = 0; i < dimension; i++)
                        {
                            sumxy += a[aID + i] * b[bID + i];
                        }
                        c[idx] = (float)(sumxy * 1.0f / ((float)(sumxx * sumyy) + eps));
                    }
                    else
                        break;
                }
            });
        }

        public unsafe static void Square_Matrix(float * a, int batchSize, int dim, float * aSquare)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchSize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int aID = idx * dim;
                        float sumxx = 0;
                        for (int i = 0; i < dim; i++)
                            sumxx += a[aID + i] * a[aID + i];
                        aSquare[idx] = (float)Math.Sqrt(sumxx);
                    }
                    else
                        break;
                }
            });
        }

        public unsafe static void SEQ_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int* Seg_Index, int* Seg_Margin, int seg_size, 
                            int * Fea_Index, float * Fea_Value, int elementsize, float * mul_weight, float * output, int inputDimension, int outputDimension, int winSize)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * outputDimension;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int id = thread_idx * process_len + t;
                    if (id < total)
                    {
                        int batch_idx = id / outputDimension;
                        int output_idx = id % outputDimension;

                        int seg_end = Smp_Index[batch_idx];
                        int seg_begin = 0;
                        if (batch_idx > 0)
                        {
                            seg_begin = Smp_Index[batch_idx - 1];
                        }

                        float sum = 0;
                        for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
                        {
                            int col_end = Seg_Index[word_idx];
                            int col_begin = 0;
                            if (word_idx > 0)
                            {
                                col_begin = Seg_Index[word_idx - 1];
                            }
                            for (int i = col_begin; i < col_end; ++i)
                            {
                                int fea_idx = Fea_Index[i];
                                sum += Fea_Value[i] * mul_weight[((word_idx - seg_begin) * inputDimension + fea_idx) * outputDimension + output_idx];
                            }
                        }
                        output[batch_idx * outputDimension + output_idx] = sum;
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public unsafe static void Sparse_matrix_multiply_INTEX_weight(int * outputSmpIndex, int * outputItemIndex, float * outputDeriv, int batchsize,
                int outputItemNum, float * weight, float * hidden_deriv, int hiddenDim, float wei)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * hiddenDim;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int id = thread_idx * process_len + t;
                    if (id < total)
                    {
                        int idx = id / hiddenDim;
                        int idy = id % hiddenDim;

                        int col_end = outputSmpIndex[idx];
                        int col_begin = idx > 0 ? outputSmpIndex[idx - 1] : 0;

                        float sum = 0;
                        for (int i = col_begin; i < col_end; ++i)
                        {
                            int inputIdx = outputItemIndex[i];
                            float deriv = outputDeriv == null ? 1 : outputDeriv[i];
                            sum += deriv * weight[inputIdx * hiddenDim + idy];
                        }

                        hidden_deriv[idx * hiddenDim + idy] = wei * hidden_deriv[idx * hiddenDim + idy] + sum;
                    }
                    else
                        break;
                }
            });
        }


        /// <summary>
        /// Forward Propagation, Used for CNN for sparse input. 
        /// Smp_Index, batchsize Seg_Index Seg_Margin seg_size Fea_Index Fea_Value elementsize
        /// Sequence of sparse feature:
        /// MiniBatch : x1 x2 x3 x4 x5 x6
        /// x1 = = (f1:v1 f2:v2 .. ) (f1:v1 f2:v2 ..)
        /// 
        /// 
        /// </summary>
        /// <param name="Smp_Index"></param>
        /// <param name="batchsize"></param>
        /// <param name="Seg_Index"></param>
        /// <param name="Seg_Margin"></param>
        /// <param name="seg_size"></param>
        /// <param name="Fea_Index"></param>
        /// <param name="Fea_Value"></param>
        /// <param name="elementsize"></param>
        /// <param name="mul_weight"></param>
        /// <param name="output"></param>
        /// <param name="inputDimension"></param>
        /// <param name="outputDimension"></param>
        /// <param name="winSize"></param>
        public unsafe static void Convolution_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, 
                int* Fea_Index, float * Fea_Value, int elementsize, float * mul_weight, float * output, int inputDimension, int outputDimension, int winSize)
        {
            int THREAD_NUM = 32;
            int total = outputDimension * seg_size;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, seg_size, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, sed_idx =>
            {
                int ws = winSize / 2;
                int mSmp_idx = Seg_Margin[sed_idx];
                int outRowIdx = sed_idx * outputDimension;
                for (int idx = 0; idx < outputDimension; idx++)
                {
                    output[outRowIdx + idx] = 0;
                }

                for (int w = -ws; w <= ws; w++)
                {
                    if (sed_idx + w >= 0 && sed_idx + w < seg_size)
                    {
                        if (Seg_Margin[sed_idx + w] == mSmp_idx)
                        {
                            int row = sed_idx + w; // idx / n;
                            int col_end = Seg_Index[row];
                            int col_begin = 0;
                            if (row > 0)
                            {
                                col_begin = Seg_Index[row - 1];
                            }

                            for (int i = col_begin; i < col_end; i++)
                            {
                                int fea_idx = Fea_Index[i];
                                float v = Fea_Value[i];
                                int rightRowIdx = ((w + ws) * inputDimension + fea_idx) * outputDimension;
                                for (int idx = 0; idx < outputDimension; idx++)
                                {
                                    output[outRowIdx + idx] += v * mul_weight[rightRowIdx + idx];
                                }
                            }
                        }
                    }
                }
            });
        }

        public static void Max_Pooling_Mask(float* pooling_feas, int* Smp_Index, int batchsize, int* Seq_mask, float* output, int* maxpooling_index, int output_dimension)
        {
            int THREAD_NUM = 32;
            Parallel.For(0, batchsize, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, batch_idx =>
            {
                int col_end = Smp_Index[batch_idx];
                int col_begin = 0;
                if (batch_idx > 0)
                {
                    col_begin = Smp_Index[batch_idx - 1];
                }

                long batchOutputOffset = batch_idx * output_dimension;
                for (long output_idx = batchOutputOffset; output_idx < batchOutputOffset + output_dimension; output_idx++)
                {
                    output[output_idx] = 0; //float.MinValue;
                }

                //int seg_idx = col_begin * output_dimension;
                for (int i = col_begin; i < col_end; i++)
                {
                    int newPos = Seq_mask[i];
                    for (long output_idx = 0; output_idx < output_dimension; output_idx++)
                    {
                        if (i == col_begin || pooling_feas[newPos * output_dimension + output_idx] > output[batchOutputOffset + output_idx])
                        {
                            output[batchOutputOffset + output_idx] = pooling_feas[newPos * output_dimension + output_idx];
                            maxpooling_index[batchOutputOffset + output_idx] = newPos;
                        }
                    }

                    //seg_idx += output_dimension;
                }
            });
        }


        public unsafe static void Max_Pooling(float* pooling_feas, int * Smp_Index, int batchsize, float* output, int * maxpooling_index, int output_dimension)
        {
            int THREAD_NUM = 32;
            Parallel.For(0, batchsize, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, batch_idx =>
            {
                int col_end = Smp_Index[batch_idx];
                int col_begin = 0;
                if (batch_idx > 0)
                {
                    col_begin = Smp_Index[batch_idx - 1];
                }

                long batchOutputOffset = batch_idx * output_dimension;
                for (long output_idx = batchOutputOffset; output_idx < batchOutputOffset + output_dimension; output_idx++)
                {
                    output[output_idx] = 0;
                }

                int seg_idx = col_begin * output_dimension;
                for (int i = col_begin; i < col_end; i++)
                {
                    for (long output_idx = 0; output_idx < output_dimension; output_idx++)
                    {
                        if (i == col_begin || pooling_feas[seg_idx + output_idx] > output[batchOutputOffset + output_idx])
                        {
                            output[batchOutputOffset + output_idx] = pooling_feas[seg_idx + output_idx];
                            maxpooling_index[batchOutputOffset + output_idx] = i;
                        }
                    }

                    seg_idx += output_dimension;
                }
            });
        }

        public static void Deriv_Max_Pooling(float* deriv, int* maxpoolingIndex, float* poolingDeriv, int batchsize, int output_dimension, float alpha, float beta)
        {
            Parallel.For(0, batchsize, i =>
            {
                for (int j = 0; j < output_dimension; j++)
                {
                    int oid = i * output_dimension + j;
                    poolingDeriv[maxpoolingIndex[oid]] = alpha * poolingDeriv[maxpoolingIndex[oid]] + beta * deriv[oid];
                }
            });
        }

        public static void GetWindowMatrixs(int* instanceMargin, int sentsize, float* seqMatrixs, int* mapForward, int dim, int winsize, float* matrix, float alpha, float beta)
        {
            Parallel.For(0, sentsize, idx =>
            {
                for (int idy = 0; idy < dim; idy++)
                {
                    int half_win = winsize / 2;

                    int smpidx = instanceMargin[idx];
                    for (int i = -half_win; i < winsize - half_win; i++)
                    {
                        int curidx = idx + i;
                        matrix[idx * (winsize * dim) + (i + half_win) * dim + idy] =
                            alpha * matrix[idx * (winsize * dim) + (i + half_win) * dim + idy];

                        if (curidx >= 0 && curidx < sentsize && instanceMargin[curidx] == smpidx)
                        {
                            int newPos = mapForward == null ? curidx : mapForward[curidx];
                            matrix[idx * (winsize * dim) + (i + half_win) * dim + idy] += beta * seqMatrixs[newPos * dim + idy];
                        }
                    }
                }
            });
        }

        public static void SetWindowMatrixs(int* instanceMargin, int sentsize, float* seqMatrixs, int* mapForward, int dim, int winsize, float* matrix, float alpha, float beta)
        {
            Parallel.For(0, sentsize, i =>
            {
                for (int j = 0; j < dim; j++)
                {
                    int half_win = winsize / 2;
                    float sum = 0;
                    int smpIdx = instanceMargin[i];
                    for (int k = -half_win; k < winsize - half_win; k++)
                    {
                        int curIdx = i + k;
                        if (curIdx >= 0 && curIdx < sentsize && instanceMargin[curIdx] == instanceMargin[i])
                        {
                            sum += matrix[curIdx * (winsize * dim) + (winsize - 1 - (i + half_win)) * dim + j];
                        }
                    }

                    int newPos = mapForward == null ? i : mapForward[i];
                    seqMatrixs[newPos * dim + j] = alpha * seqMatrixs[newPos * dim + j] + beta * sum;
                }
            });
        }

        public static void Random_Walk(float* prob, float* norm, float beta, float* probWeight, float* probLast, int inputDim, int outputDim)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = outputDim;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int id = thread_idx * process_len + t;
                    if (id < total)
                    {
                        int idx = id % outputDim;

                        float sum = 0;
                        for (int i = 0; i < inputDim; i++)
                        {
                            sum += prob[i] * (probWeight[i * outputDim + idx] + beta) * 1.0f / (norm[i] + beta * outputDim);
                        }
                        probLast[idx] = sum;
                    }
                    else
                    {
                        break;
                    }
                }
            });

        }

        public unsafe static void Matrix_Multipy(float* input, float* weight, float* output, int batchsize, int inputDimension, int outputDimension, float alpha, float beta, int inverse)
        {
            //int THREAD_NUM = THREAD_NUMBER;
            //int total = outputDimension * batchsize;
            //int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            //Parallel.For(0, THREAD_NUM, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, thread_idx =>

            Parallel.For(0, outputDimension * batchsize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, id =>
            {
                {
                    int idy = id / outputDimension;
                    int idx = id % outputDimension;

                    int row = idy; // / n;
                    int col = idx; // % n;
                    float sum = 0;
                    for (int i = 0; i < inputDimension; i++)
                    {
                        if (inverse == 1)
                        {
                            sum += input[row * inputDimension + i] * weight[col * inputDimension + i];
                        }
                        else
                        {
                            sum += input[row * inputDimension + i] * weight[i * outputDimension + col];
                        }
                    }
                    output[idy * outputDimension + idx] = alpha * output[idy * outputDimension + idx] + beta * sum;
                }
            });

        }

        public unsafe static void Matrix_Add_Tanh(float * output, float * bias, int batchsize, int outputDim)
        {
            int THREAD_NUM = 32;
            int total = batchsize * outputDim;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, batchsize, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, sampleIndex =>
            {
                int outputBase = sampleIndex * outputDim;
                for (int output_idx = 0; output_idx < outputDim; output_idx++)
                {
                    float m = output[outputBase + output_idx] + bias[output_idx];
                    output[outputBase + output_idx] = (float)(Math.Tanh(m));
                }
            });
        }

        public unsafe static void Matrix_Add_Vector(float * output, float * bias, int batchsize, int output_number)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * output_number;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int id = thread_idx * process_len + t;
                    if (id < total)
                    {
                        int batch_idx = id / output_number;
                        int output_idx = id % output_number;

                        float m = output[batch_idx * output_number + output_idx] + bias[output_idx];
                        output[batch_idx * output_number + output_idx] = m;
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public unsafe static void Matrix_Rectified_Vector(float * output, float * bias, int batchsize, int output_number)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * output_number;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int id = thread_idx * process_len + t;
                    if (id < total)
                    {
                        int batch_idx = id / output_number;
                        int output_idx = id % output_number;

                        float m = output[batch_idx * output_number + output_idx] + bias[output_idx];
                        if (m < 0)
                        {
                            m = 0;
                        }
                        output[batch_idx * output_number + output_idx] = m;
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public unsafe static void Cosine_Similarity(float * a, float * b, float * c, int nTrialPlus1, int BATCH_SIZE, int mindex, int batchsize, int dimension, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int process_len = (batchsize + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < batchsize)
                    {
                        float sumxx = 0;
                        float sumyy = 0;
                        float sumxy = 0;
                        for (int i = 0; i < dimension; i++)
                        {
                            sumxx += a[idx * dimension + i] * a[idx * dimension + i];
                            sumyy += b[idx * dimension + i] * b[idx * dimension + i];
                            sumxy += a[idx * dimension + i] * b[idx * dimension + i];
                        }
                        c[mindex * BATCH_SIZE + idx] = (float)(sumxy * 1.0f / (Math.Sqrt((float)(sumxx * sumyy)) + eps));
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        static void cal_alpha(float* alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = (nTrial - 1) * batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int row = idx / batchsize;
                        int col = idx % batchsize;
                        alpha[row * BATCHSIZE + col + BATCHSIZE] = (float)Math.Exp((float)(-gamma * (alpha[col] - alpha[row * BATCHSIZE + col + BATCHSIZE])));
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }


        static void cal_alpha_sum(float* alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma, int init)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float sum = init;
                        for (int i = 1; i < nTrial; i++)
                        {
                            sum += alpha[i * BATCHSIZE + idx];
                        }
                        alpha[idx] = sum;
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        static void cal_alpha_norm(float* alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = (nTrial - 1) * batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int row = idx / batchsize;
                        int col = idx % batchsize;
                        alpha[row * BATCHSIZE + col + BATCHSIZE] = (float)((gamma * alpha[row * BATCHSIZE + col + BATCHSIZE]) / alpha[col]);
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        static void cal_alpha_norm_MXE(float* alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = (nTrial - 1) * batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int row = idx / batchsize;
                        int col = idx % batchsize;
                        alpha[row * BATCHSIZE + col + BATCHSIZE] = (float)((gamma * alpha[row * BATCHSIZE + col + BATCHSIZE]) / alpha[col] / alpha[col]);
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Calculate_Alpha(float* alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            cal_alpha(alpha, nTrial, BATCHSIZE, batchsize, gamma);
            cal_alpha_sum(alpha, nTrial, BATCHSIZE, batchsize, gamma, 1);
            cal_alpha_norm(alpha, nTrial, BATCHSIZE, batchsize, gamma);
            cal_alpha_sum(alpha, nTrial, BATCHSIZE, batchsize, gamma, 0);
        }

        public static void Calculate_Alpha_MXE(float* alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            cal_alpha(alpha, nTrial, BATCHSIZE, batchsize, gamma);
            cal_alpha_sum(alpha, nTrial, BATCHSIZE, batchsize, gamma, 1);
            cal_alpha_norm_MXE(alpha, nTrial, BATCHSIZE, batchsize, gamma);
            cal_alpha_sum(alpha, nTrial, BATCHSIZE, batchsize, gamma, 0);
        }


        static void cal_alpha_nce(float* alpha, float* dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = nTrial * batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int row = idx / batchsize;
                        int col = idx % batchsize;
                        alpha[row * BATCHSIZE + col] = (float)(gamma / (1.0f + (nTrial - 1) * Math.Exp(dist[row * BATCHSIZE + col] - gamma * alpha[row * BATCHSIZE + col] + gamma))); //+gamma is from hxd, sd doesn't have this

                        if (idx < batchsize) alpha[idx] = gamma - alpha[idx];
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Calculate_Alpha_NCE(float* alpha, float* dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            cal_alpha_nce(alpha, dist, nTrial, BATCHSIZE, batchsize, gamma);
        }

        static void cal_alpha_nce2(float* alpha, float* dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = nTrial * batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int row = idx / batchsize;
                        int col = idx % batchsize;
                        float s = (float)(1.0f / (1.0f + (nTrial - 1) * Math.Exp(dist[row * BATCHSIZE + col] - gamma * alpha[row * BATCHSIZE + col] + gamma))); //+gamma is from hxd, sd doesn't have this
                        alpha[row * BATCHSIZE + col] = gamma * s * (1.0f - s);
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Calculate_Alpha_NCE2(float* alpha, float* dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            cal_alpha_nce2(alpha, dist, nTrial, BATCHSIZE, batchsize, gamma);
        }

        public static void Calculate_Alpha_PAIRRANK(float* alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
        {
            //if (idx < batchsize)
            //{
            //    float msum = 0;
            //    for (int n = 1; n < nTrial; n++)
            //    {
            //        float a = gamma * (1.0f - 1.0f / (1 + expf(-gamma * (alpha[idx] - alpha[n * BATCHSIZE + idx]))));
            //        alpha[n * BATCHSIZE + idx] = a;
            //        msum += a;
            //    }
            //    alpha[idx] = msum;
            //}

            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float msum = 0;
                        for (int n = 1; n < nTrial; n++)
                        {
                            float a = (float)(gamma * (1.0f - 1.0f / (1 + Math.Exp(-gamma * (alpha[idx] - alpha[n * BATCHSIZE + idx])))));
                            alpha[n * BATCHSIZE + idx] = a;
                            msum += a;
                        }
                        alpha[idx] = msum;
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void FillOut_Dist_NCE(float* dist, int* neg_list, int nTrailPlus1, int BATCH_SIZE, int mindex, int batchsize)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int process_len = (batchsize + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < batchsize)
                    {
                        int mtindex = neg_list[idx];
                        dist[mindex * BATCH_SIZE + idx] = dist[mtindex];
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Deriv_Cosine(float* q, float* d, float* dcq, float* dcd, int batchsize, int m, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float a = 0;
                        float b = eps;
                        float c = eps;
                        for (int i = 0; i < m; i++)
                        {
                            a += q[idx * m + i] * d[idx * m + i];
                            b += q[idx * m + i] * q[idx * m + i];
                            c += d[idx * m + i] * d[idx * m + i];
                        }
                        b = (float)Math.Sqrt(b);
                        c = (float)Math.Sqrt(c);
                        for (int i = 0; i < m; i++)
                        {
                            dcq[idx * m + i] = (float)((1 - q[idx * m + i]) * (1 + q[idx * m + i]) * (d[idx * m + i] * 1.0f / (b * c) - q[idx * m + i] * a * 1.0f / (b * b * b * c)));
                            dcd[idx * m + i] = (float)((1 - d[idx * m + i]) * (1 + d[idx * m + i]) * (q[idx * m + i] * 1.0f / (b * c) - d[idx * m + i] * a * 1.0f / (b * c * c * c)));
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Derive_Cosine_Linear(float* q, float* d, float* dcq, float* dcd, int batchsize, int m, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float a = 0;
                        float b = eps;
                        float c = eps;
                        for (int i = 0; i < m; i++)
                        {
                            a += q[idx * m + i] * d[idx * m + i];
                            b += q[idx * m + i] * q[idx * m + i];
                            c += d[idx * m + i] * d[idx * m + i];
                        }
                        b = (float)Math.Sqrt(b);
                        c = (float)Math.Sqrt(c);
                        for (int i = 0; i < m; i++)
                        {
                            dcq[idx * m + i] = (float)((d[idx * m + i] * 1.0f / (b * c) - q[idx * m + i] * a * 1.0f / (b * b * b * c)));
                            dcd[idx * m + i] = (float)((q[idx * m + i] * 1.0f / (b * c) - d[idx * m + i] * a * 1.0f / (b * c * c * c)));
                        }

                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Derive_Cosine_Rectified(float* q, float* d, float* dcq, float* dcd, int batchsize, int m, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float a = 0;
                        float b = eps;
                        float c = eps;
                        for (int i = 0; i < m; i++)
                        {
                            a += q[idx * m + i] * d[idx * m + i];
                            b += q[idx * m + i] * q[idx * m + i];
                            c += d[idx * m + i] * d[idx * m + i];
                        }
                        b = (float)Math.Sqrt(b);
                        c = (float)Math.Sqrt(c);
                        for (int i = 0; i < m; i++)
                        {
                            if (q[idx * m + i] == 0)    //TODO: float ==
                            {
                                dcq[idx * m + i] = 0;
                            }
                            else
                            {
                                dcq[idx * m + i] = (float)((d[idx * m + i] * 1.0f / (b * c) - q[idx * m + i] * a * 1.0f / (b * b * b * c)));
                            }
                            //dcq[idx * m + i] = dcq[idx * m + i] * 1.0f / batchsize;

                            if (d[idx * m + i] == 0)    //TODO: float ==
                            {
                                dcd[idx * m + i] = 0;
                            }
                            else
                            {
                                dcd[idx * m + i] = (float)((q[idx * m + i] * 1.0f / (b * c) - d[idx * m + i] * a * 1.0f / (b * c * c * c)));
                            }
                            //dcd[idx * m + i] = dcd[idx * m + i] * 1.0f / batchsize;
                        }


                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Deriv_Cosine_EX(float* q, float* d, int* neg_list, float* dcq, float* dcd, int batchsize, int m, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float a = 0;
                        float b = eps;
                        float c = eps;
                        int mIndex = neg_list[idx];
                        for (int i = 0; i < m; i++)
                        {
                            a += q[idx * m + i] * d[mIndex * m + i];
                            b += q[idx * m + i] * q[idx * m + i];
                            c += d[mIndex * m + i] * d[mIndex * m + i];
                        }
                        b = (float)Math.Sqrt(b);
                        c = (float)Math.Sqrt(c);
                        for (int i = 0; i < m; i++)
                        {
                            dcq[idx * m + i] = (float)((1 - q[idx * m + i]) * (1 + q[idx * m + i]) * (d[mIndex * m + i] * 1.0f / (b * c) - q[idx * m + i] * a * 1.0f / (b * b * b * c)));
                            dcd[idx * m + i] = (float)((1 - d[mIndex * m + i]) * (1 + d[mIndex * m + i]) * (q[idx * m + i] * 1.0f / (b * c) - d[mIndex * m + i] * a * 1.0f / (b * c * c * c)));
                            //dcq[idx * m + i] = dcq[idx * m + i] * 1.0f / batchsize;
                            //dcd[idx * m + i] = dcd[idx * m + i] * 1.0f / batchsize;
                        }

                    }
                    else
                    {
                        break;
                    }
                }
            });
        }
        public static void Derive_Cosine_Linear_EX(float* q, float* d, int* neg_list, float* dcq, float* dcd, int batchsize, int m, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        {
                            float a = 0;
                            float b = eps;
                            float c = eps;
                            int mIndex = neg_list[idx];
                            for (int i = 0; i < m; i++)
                            {
                                a += q[idx * m + i] * d[mIndex * m + i];
                                b += q[idx * m + i] * q[idx * m + i];
                                c += d[mIndex * m + i] * d[mIndex * m + i];
                            }
                            b = (float)Math.Sqrt(b);
                            c = (float)Math.Sqrt(c);
                            for (int i = 0; i < m; i++)
                            {
                                dcq[idx * m + i] = (float)((d[mIndex * m + i] * 1.0f / (b * c) - q[idx * m + i] * a * 1.0f / (b * b * b * c)));
                                dcd[idx * m + i] = (float)((q[idx * m + i] * 1.0f / (b * c) - d[mIndex * m + i] * a * 1.0f / (b * c * c * c)));
                                //dcq[idx * m + i] = dcq[idx * m + i] * 1.0f / batchsize;
                                //dcd[idx * m + i] = dcd[idx * m + i] * 1.0f / batchsize;
                            }
                        }

                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Derive_Cosine_Rectified_EX(float* q, float* d, int* neg_list, float* dcq, float* dcd, int batchsize, int m, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float a = 0;
                        float b = eps;
                        float c = eps;
                        int mIndex = neg_list[idx];
                        for (int i = 0; i < m; i++)
                        {
                            a += q[idx * m + i] * d[mIndex * m + i];
                            b += q[idx * m + i] * q[idx * m + i];
                            c += d[mIndex * m + i] * d[mIndex * m + i];
                        }
                        b = (float)Math.Sqrt(b);
                        c = (float)Math.Sqrt(c);
                        for (int i = 0; i < m; i++)
                        {
                            if (q[idx * m + i] == 0)    //TODO: float ==
                            {
                                dcq[idx * m + i] = 0;
                            }
                            else
                            {
                                dcq[idx * m + i] = (float)((d[mIndex * m + i] * 1.0f / (b * c) - q[idx * m + i] * a * 1.0f / (b * b * b * c)));
                            }
                            //dcq[idx * m + i] = dcq[idx * m + i] * 1.0f / batchsize;


                            if (d[mIndex * m + i] == 0) //TODO: float ==
                            {
                                dcd[idx * m + i] = 0;
                            }
                            else
                            {
                                dcd[idx * m + i] = (float)((q[idx * m + i] * 1.0f / (b * c) - d[mIndex * m + i] * a * 1.0f / (b * c * c * c)));
                            }
                            //dcd[idx * m + i] = dcd[idx * m + i] * 1.0f / batchsize;
                        }


                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Matrix_WeightAdd(float* gpu_floats_a, float* gpu_floats_b, int batchsize, int dimension, float* mweight, int start, int keep)
        {
            int THREAD_NUM = 32;
            Parallel.For(0, batchsize, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, sampleIndex =>
            {
                long outputBase = sampleIndex * dimension;

                if (keep != 0)
                {
                    float w = keep * mweight[start + sampleIndex];
                    for (int idx = 0; idx < dimension; idx++)
                    {
                        gpu_floats_a[outputBase + idx] += gpu_floats_b[outputBase + idx] * w;
                    }
                }
                else
                {
                    float w = mweight[start + sampleIndex];
                    for (int idx = 0; idx < dimension; idx++)
                    {
                        gpu_floats_a[outputBase + idx] = gpu_floats_b[outputBase + idx] * w;
                    }
                }
            });
        }

        public static void Matrix_WeightAdd_EX(float* gpu_floats_a, float* gpu_floats_b, int* inver_neg_index, int* inver_neg_value, int batchsize, int dimension, float* mweight, int start, int keep)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * dimension;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int id = thread_idx * process_len + t;
                    if (id < total)
                    {
                        int idy = id / dimension;
                        int idx = id % dimension;
                        int col_end = inver_neg_index[idy];
                        int col_begin = 0;
                        if (idy > 0)
                        {
                            col_begin = inver_neg_index[idy - 1];
                        }
                        float sum = 0;
                        for (int i = col_begin; i < col_end; i++)
                        {
                            int row = inver_neg_value[i];
                            sum += gpu_floats_b[row * dimension + idx] * mweight[start + row];
                        }
                        if (keep != 0)
                        {
                            gpu_floats_a[idy * dimension + idx] += keep * sum;
                        }
                        else
                        {
                            gpu_floats_a[idy * dimension + idx] = sum;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public unsafe static void Deriv_Tanh(float * delta, float * layer_output, int batchsize, int m)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * m;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int id = thread_idx * process_len + t;
                    if (id < total)
                    {
                        int idy = id / m;
                        int idx = id % m;
                        delta[idy * m + idx] = delta[idy * m + idx] * (1 - layer_output[idy * m + idx]) * (1 + layer_output[idy * m + idx]);
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public unsafe static void Deriv_Tanh(float * delta, float * layer_output, int m)
        {
            for (int i = 0; i < m; i++)
            {
                delta[i] = delta[i] * (1 - layer_output[i]) * (1 + layer_output[i]);
            }
        }

        public unsafe static void Deriv_Rectified(float * delta, float * layer_output, int batchsize, int m)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * m;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int id = thread_idx * process_len + t;
                    if (id < total)
                    {
                        int idy = id / m;
                        int idx = id % m;
                        if (layer_output[idy * m + idx] == 0)   // TODO: Float ==
                        {
                            delta[idy * m + idx] = 0; // delta[idy * m +idx] ;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }
        //

        public static void Deriv_Rectified(float* delta, float* layer_output, int m)
        {
            for (int i = 0; i < m; i++)
            {
                if (layer_output[i] <= 0)
                    delta[i] = 0;
            }
        }

        public static void Deriv_Logstic(float* delta, float* layer_output, int m, float gamma)
        {
            for (int i = 0; i < m; i++)
            {
                delta[i] = delta[i] * layer_output[i] * (1 - layer_output[i]) * gamma;
            }
        }

        public unsafe static void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin,
                                                              int seg_size, int * Fea_Index,
                                                   float * Fea_Value, int elementsize,
                                                   float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = output_dimension;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int seg_begin = 0;
                        for (int sample = 0; sample < batchsize; ++sample)
                        {
                            int seg_end = Smp_Index[sample];

                            for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
                            {
                                int col_end = Seg_Index[word_idx];
                                int col_begin = 0;
                                if (word_idx > 0)
                                {
                                    col_begin = Seg_Index[word_idx - 1];
                                }
                                for (int i = col_begin; i < col_end; ++i)
                                {
                                    int fea_idx = Fea_Index[i];

                                    mul_weight[((word_idx - seg_begin) * Feature_dimension + fea_idx) * output_dimension + idx] += Fea_Value[i] * output[sample * output_dimension + idx];
                                }
                            }
                            seg_begin = seg_end;
                        }
                    }

                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(int* Smp_Index, int batchsize, int* Seg_Index,
            int seg_size, int* Fea_Index, float* Fea_Value, int elementsize,
            float* mul_weight, float* output, int Feature_dimension, int output_dimension, float weight)
        {
            Parallel.For(0, output_dimension, new ParallelOptions { MaxDegreeOfParallelism = 32 }, idx =>
            {
                int seg_begin = 0;
                for (int sample = 0; sample < batchsize; ++sample)
                {
                    int seg_end = Smp_Index[sample];
                    for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
                    {
                        int col_end = Seg_Index[word_idx];
                        int col_begin = 0;
                        if (word_idx > 0) col_begin = Seg_Index[word_idx - 1];

                        for (int i = col_begin; i < col_end; ++i)
                        {
                            int fea_idx = Fea_Index[i];
                            mul_weight[fea_idx * output_dimension + idx] += Fea_Value[i] * output[sample * output_dimension + idx] * weight;
                        }
                    }
                    seg_begin = seg_end;
                }
            });
        }

        public static void SEQ_Sparse_Matrix_Multiply_INT(int* Smp_Index, int batchsize, int* Seg_Index,
            int seg_size, int* Fea_Index, float* Fea_Value, int elementsize,
            float* mul_weight, float* output, int Feature_dimension, int output_dimension)
        {
            Parallel.For(0, output_dimension * batchsize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, threadIdx =>
            {
                int idx = threadIdx % output_dimension;
                int idy = threadIdx / output_dimension;

                int seg_end = Smp_Index[idy];
                int seg_begin = 0;
                if (idy > 0) seg_begin = Smp_Index[idy - 1];

                float sum = 0;
                for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
                {
                    int col_end = Seg_Index[word_idx];
                    int col_begin = 0;
                    if (word_idx > 0) col_begin = Seg_Index[word_idx - 1];

                    for (int i = col_begin; i < col_end; ++i)
                    {
                        int fea_idx = Fea_Index[i];
                        sum += Fea_Value[i] * mul_weight[fea_idx * output_dimension + idx];
                    }
                }
                output[idy * output_dimension + idx] = sum;
            });
        }

        public static void Convolution_Sparse_Matrix_Product_INTEX(float* deriv, int* maxpooling_index, int* Seg_Index, int* SegMargin_Index, int seg_size, int win_size,
                                        int batchsize, int output_dimension, int* Fea_Index, float* Fea_Value, float* grad, int Feature_Dimension)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = output_dimension * win_size;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        int output_idx = idx / win_size;
                        int win_idx = idx % win_size;

                        //float sum = 0;
                        for (int b = 0; b < batchsize; b++)
                        {
                            int target_seg = maxpooling_index[b * output_dimension + output_idx];

                            if (target_seg == -1)
                            {
                                continue;
                            }
                            int target_smp = SegMargin_Index[target_seg];
                            //deriv[i * output_dimension + idx] *  
                            int ws = win_size / 2;
                            int w = win_idx - ws;
                            int row = target_seg + w; // idx / n;
                            if (row >= 0 && row < seg_size)
                            {
                                if (SegMargin_Index[row] == target_smp)
                                {
                                    int col_end = Seg_Index[row];
                                    int col_begin = 0;
                                    if (row > 0)
                                    {
                                        col_begin = Seg_Index[row - 1];
                                    }
                                    //float sum = 0;
                                    for (int i = col_begin; i < col_end; i++)
                                    {
                                        int fea_idx = Fea_Index[i];
                                        if (fea_idx >= Feature_Dimension)
                                        {
                                            continue;
                                        }
                                        float m = Fea_Value[i] * deriv[b * output_dimension + output_idx];
                                        // con_weight[((w+ws) * Feature_dimension + fea_idx)*output_dimension+idx];
                                        grad[(win_idx * Feature_Dimension + fea_idx) * output_dimension + output_idx] += m;
                                    }
                                }
                            }
                        }

                    }

                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Convolution_Sparse_Matrix_Product_INTEX_Weight(
float* deriv, int* maxpooling_index, int* Seg_Index, int* SegMargin_Index, int seg_size, int win_size,
        int batchsize, int output_dimension, int* Fea_Index, float* Fea_Value, float* grad, int Feature_Dimension, float learnRate)
        {
            int threadNum = 32;
            int outputSizePerBlock = 32;
            int outputBlockNum = (output_dimension + outputSizePerBlock - 1) / outputSizePerBlock;

            Parallel.For(0, outputBlockNum * win_size, new ParallelOptions { MaxDegreeOfParallelism = threadNum }, idx =>
            {
                int block_idx = idx / win_size;
                int win_idx = idx % win_size;
                int gradOffset = win_idx * Feature_Dimension * output_dimension;
                int outputIndexStart = block_idx * outputSizePerBlock;
                int outputIndexEnd = Math.Min((block_idx + 1) * outputSizePerBlock, output_dimension);
                //float sum = 0;
                for (int batchIndex = 0; batchIndex < batchsize; batchIndex++)
                {
                    int sampleOffset = batchIndex * output_dimension;
                    for (int out_idx = outputIndexStart; out_idx < outputIndexEnd;)
                    {
                        int target_seg = maxpooling_index[sampleOffset + out_idx];
                        int out_inner_idx_start = out_idx;
                        while (out_idx < outputIndexEnd && (maxpooling_index[sampleOffset + out_idx] == target_seg))
                        {
                            out_idx++;
                        }

                        int target_smp = SegMargin_Index[target_seg];
                        int ws = win_size / 2;
                        int w = win_idx - ws;
                        int row = target_seg + w; // idx / n;
                        if (row >= 0 && row < seg_size)
                        {
                            if (SegMargin_Index[row] == target_smp)
                            {
                                int col_end = Seg_Index[row];
                                int col_begin = 0;
                                if (row > 0) col_begin = Seg_Index[row - 1];
                                for (int output_idx = out_inner_idx_start; output_idx < out_idx; output_idx++)
                                {
                                    float d = learnRate * deriv[sampleOffset + output_idx];
                                    for (int i = col_begin; i < col_end; i++)
                                    {
                                        int fea_idx = Fea_Index[i];
                                        float v = Fea_Value[i];
                                        int goff = gradOffset + fea_idx * output_dimension + output_idx;
                                        grad[goff] += v * d;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

        // A'*B
        public static void Matrix_Product_Weight(float* a, float* b, float* c, int batchsize, int m, int n, float weight)
        {
            //if (idx < n && idy < m)
            Parallel.For(0, n * m, new ParallelOptions { MaxDegreeOfParallelism = 32 }, threadIdx =>
            {
                int idx = threadIdx % n;
                int idy = threadIdx / n;

                int row = idy; // / n;
                int col = idx;// % n;
                float sum = 0;
                int a_iter = row;
                int b_iter = col;
                int a_end_pt = a_iter + (m * batchsize);
                while (a_iter < a_end_pt)
                {
                    sum += a[a_iter] * b[b_iter];
                    a_iter += m;
                    b_iter += n;
                }
                c[idy * n + idx] += weight * sum;
            });
        }

        public static void Matrix_Product(float* a, float* b, float* c, int batchsize, int m, int n)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = n * m;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idx = cnt / m;
                        int idy = cnt % m;
                        int row = idy; // / n;
                        int col = idx;// % n;
                        float sum = 0;
                        for (int i = 0; i < batchsize; i++)
                        {
                            sum += a[i * m + row] * b[i * n + col]; // * alpha[alpha_index * BATCH_SIZE + i]; // vAlpha(i);
                        }
                        c[idy * n + idx] = sum;
                    }
                    else
                    {
                        break;
                    }
                }
            });

        }

        public static void Matrix_Add(float* gpu_floats_a, float* gpu_floats_b, int m, int n, float mweight)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = n * m;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idx = cnt / m;
                        int idy = cnt % m;
                        gpu_floats_a[idy * n + idx] = gpu_floats_a[idy * n + idx] + gpu_floats_b[idy * n + idx] * mweight;
                    }
                    else
                    {
                        break;
                    }
                }
            });

        }

        public static float VectorInnerProduct(float* a, int offset_a, float* b, int offset_b, int len)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int process_len = (len + THREAD_NUM - 1) / THREAD_NUM;

            float[] tmp = new float[THREAD_NUM];
            Array.Clear(tmp, 0, THREAD_NUM);

            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                tmp[thread_idx] = 0;
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < len)
                    {
                        tmp[thread_idx] += a[offset_a + cnt] * b[offset_b + cnt];
                    }
                    else
                    {
                        break;
                    }
                }
            });

            float sum = 0;
            for (int i = 0; i < THREAD_NUM; i++)
            {
                sum += tmp[i];
            }
            return sum;
        }

        public static void Scale_Matrix(float* gpu_floats_a, int m, int n, float mweight)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = n * m;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idx = cnt / m;
                        int idy = cnt % m;
                        gpu_floats_a[idy * n + idx] = gpu_floats_a[idy * n + idx] * mweight; //(float)log( (float)gpu_floats_a[idx]);
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Matrix_Aggragate(float* a, float* b, int batchsize, int m)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = m;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        float sum = 0;
                        for (int i = 0; i < batchsize; i++)
                        {
                            sum += a[i * m + cnt]; //* alpha[alpha_index * BATCH_SIZE + i];
                        }
                        b[cnt] = sum;
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Sparse2Dense_Matrix(int* Smp_Idx, int* Fea_Idx, float* Fea_Value, float* matrix, int batchsize, int outputDimension)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int end = Smp_Idx[cnt];
                        int begin = cnt >= 1 ? Smp_Idx[cnt - 1] : 0;
                        for (int k = begin; k < end; k++)
                        {
                            matrix[cnt * outputDimension + Fea_Idx[k]] = Fea_Value[k];
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Cosine_Similarity_EX_Full(float* a, float* b, int* neg_list, float* c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * nTrial;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idy = cnt / batchsize;
                        int idx = cnt % batchsize;

                        float sumxx = 0;
                        float sumyy = 0;
                        float sumxy = 0;
                        //float * a_iter = a + (idx * dimension);
                        //float * b_iter = b + (neg_list[idy * batchsize + idx] * dimension);
                        //float * a_iter_end = a_iter + dimension;

                        int nid = neg_list[idy * batchsize + idx] * dimension;
                        int pid = idx * dimension;
                        for (int i = 0; i < dimension; i++)
                        {
                            sumxx += a[pid + i] * a[pid + i];
                            sumyy += b[nid + i] * b[nid + i];
                            sumxy += a[pid + i] * b[nid + i];
                        }
                        c[(idy + 1) * BATCHSIZE + idx] = (float)(sumxy / ((float)Math.Sqrt(sumxx * sumyy) + eps));
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void FillOut_Dist_NCE_Full(float* dist, int* neg_list, int nTrail, int BATCH_SIZE, int batchsize)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * nTrail;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idy = cnt / batchsize;
                        int idx = cnt % batchsize;
                        int mtindex = neg_list[idy * BATCH_SIZE + idx];
                        dist[BATCH_SIZE + idy * BATCH_SIZE + idx] = dist[mtindex];
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Deriv_Cosine_EX_Full(float* q, float* d, int* neg_list, float* dcq, float* dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * nTrail;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idy = cnt / batchsize;
                        int idx = cnt % batchsize;

                        float a = 0;
                        float b = 0;
                        float c = 0;
                        float bc, a_bbbc, a_bccc, batchsizenorm;

                        int qid = idx * m;
                        int did = neg_list[idy * BATCHSIZE + idx] * m;
                        for (int i = 0; i < m; i++)
                        {
                            b += q[qid + i] * q[qid + i];
                            c += d[did + i] * d[did + i];
                            a += q[qid + i] * d[did + i];
                        }

                        b = (float)Math.Sqrt(b);
                        c = (float)Math.Sqrt(c);
                        bc = b * c + eps;
                        a_bbbc = a / (b * b * b * c + eps);
                        a_bccc = a / (b * c * c * c + eps);

                        batchsizenorm = 1; // 1.0f / batchsize;

                        int dc_qd_id = idy * (BATCHSIZE * m) + idx * m;
                        for (int i = 0; i < m; i++)
                        {
                            dcq[dc_qd_id + i] = (1.0f - q[qid + i]) * (1.0f + q[qid + i]) * (d[did + i] / bc - q[qid + i] * a_bbbc) * batchsizenorm;
                            dcd[dc_qd_id + i] = (1.0f - d[did + i]) * (1.0f + d[did + i]) * (q[qid + i] / bc - d[did + i] * a_bccc) * batchsizenorm;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Deriv_Cosine_Linear_EX_Full(float* q, float* d, int* neg_list, float* dcq, float* dcd, int nTrail, int MAX_BATCHSIZE, int batchsize, int outputDimension, float eps)
        {
            int THREAD_NUM = 32;
            int total = batchsize * nTrail;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, batchsize, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, sampleIndex =>
            {
                for (int trailIndex = 0; trailIndex < nTrail; trailIndex++)
                {
                    float a = 0;
                    float b = eps;
                    float c = eps;
                    int mIndex = neg_list[trailIndex * MAX_BATCHSIZE + sampleIndex];
                    long qBase = sampleIndex * outputDimension;
                    long dBase = mIndex * outputDimension;
                    for (int i = 0; i < outputDimension; i++)
                    {
                        a += q[qBase + i] * d[dBase + i];
                        b += q[qBase + i] * q[qBase + i];
                        c += d[dBase + i] * d[dBase + i];
                    }

                    b = (float)Math.Sqrt(b);
                    c = (float)Math.Sqrt(c);
                    long dcqBase = trailIndex * MAX_BATCHSIZE * outputDimension + sampleIndex * outputDimension;
                    for (int i = 0; i < outputDimension; i++)
                    {
                        dcq[dcqBase + i] = (float)((d[dBase + i] * b * b - q[qBase + i] * a) / (b * b * b * c));
                        dcd[dcqBase + i] = (float)((q[qBase + i] * c * c - d[dBase + i] * a) / (b * c * c * c));
                    }
                }
            });
        }

        public static void Deriv_Cosine_Rectified_EX_Full(float* q, float* d, int* neg_list, float* dcq, float* dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * nTrail;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idy = cnt / batchsize;
                        int idx = cnt % batchsize;

                        float a = 0;
                        float b = eps;
                        float c = eps;
                        int mIndex = neg_list[idy * BATCHSIZE + idx];
                        for (int i = 0; i < m; i++)
                        {
                            a += q[idx * m + i] * d[mIndex * m + i];
                            b += q[idx * m + i] * q[idx * m + i];
                            c += d[mIndex * m + i] * d[mIndex * m + i];
                        }
                        b = (float)Math.Sqrt(b);
                        c = (float)Math.Sqrt(c);

                        for (int i = 0; i < m; i++)
                        {
                            if (q[idx * m + i] == 0)
                            {
                                dcq[idy * BATCHSIZE * m + idx * m + i] = 0;
                            }
                            else
                            {
                                dcq[idy * BATCHSIZE * m + idx * m + i] = (float)((d[mIndex * m + i] * 1.0f / (b * c) - q[idx * m + i] * a * 1.0f / (b * b * b * c)));
                            }
                            //dcq[idy * BATCHSIZE * m + idx * m + i] = dcq[idy * BATCHSIZE * m + idx * m + i] * 1.0f / batchsize;

                            if (d[mIndex * m + i] == 0)
                            {
                                dcd[idy * BATCHSIZE * m + idx * m + i] = 0;
                            }
                            else
                            {
                                dcd[idy * BATCHSIZE * m + idx * m + i] = (float)((q[idx * m + i] * 1.0f / (b * c) - d[mIndex * m + i] * a * 1.0f / (b * c * c * c)));
                            }
                            //dcd[idy * BATCHSIZE * m + idx * m + i] = dcd[idy * BATCHSIZE * m + idx * m + i] * 1.0f / batchsize;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Matrix_WeightAdd_Full(float* gpu_floats_a, float* gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension, float* mweight, int start, int keep)
        {
            int THREAD_NUM = 32;
            int total = dimension * batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, batchsize, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, sampleIdx =>
            {
                int dstRowOffset = sampleIdx * dimension;
                for (int i = 0; i < nTrail; i++)
                {
                    int rightMatBase = i * BATCHSIZE * dimension + sampleIdx * dimension;
                    float w = keep * mweight[start + i * BATCHSIZE + sampleIdx];
                    for (int idx = 0; idx < dimension; idx++)
                    {
                        gpu_floats_a[dstRowOffset + idx] += gpu_floats_b[rightMatBase + idx] * w;
                    }
                }
            });
        }

        public static void Matrix_WeightAdd_EX_Full(float* dstMat, float* rightMat,
                int* inver_neg_index, int* inver_neg_value, int nTrial, int MAX_BATCHSIZE, int batchsize, int dimension, float* leftVector, int start, int keep)
        {
            int THREAD_NUM = 32;
            Parallel.For(0, batchsize, new ParallelOptions() { MaxDegreeOfParallelism = THREAD_NUM }, sampleIndex =>
            {
                long weightOffset = 0;
                long matAOffset = sampleIndex * dimension;
                for (int n = 0; n < nTrial; n++)
                {
                    weightOffset = start + n * MAX_BATCHSIZE;
                    long matBBase = n * MAX_BATCHSIZE * dimension;
                    int col_end = inver_neg_index[n * MAX_BATCHSIZE + sampleIndex];
                    int col_begin = 0;
                    if (sampleIndex > 0)
                    {
                        col_begin = inver_neg_index[n * MAX_BATCHSIZE + sampleIndex - 1];
                    }

                    for (int i = col_begin; i < col_end; i++)
                    {
                        int row = inver_neg_value[n * MAX_BATCHSIZE + i];
                        float weight = keep * leftVector[weightOffset + row];
                        long matBOffset = matBBase + row * dimension;
                        for (int idx = 0; idx < dimension; idx++)
                        {
                            dstMat[matAOffset + idx] += weight * rightMat[matBOffset + idx];
                        }
                    }
                }
            });
        }

        public static void Cosine_Similarity_SubSpace(float* a, float* b, float* c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * labelDim;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idx = cnt / labelDim;
                        int idy = cnt % labelDim;
                        float sumxx = 0;
                        float sumyy = 0;
                        float sumxy = 0;
                        int id_start = idx * (labelDim * subspaceDim) + idy * subspaceDim;
                        for (int i = 0; i < subspaceDim; i++)
                        {
                            sumxx += a[id_start + i] * a[id_start + i];
                            sumyy += b[id_start + i] * b[id_start + i];
                            sumxy += a[id_start + i] * b[id_start + i];
                        }
                        c[idx * labelDim + idy] = (float)(sumxy * 1.0f / (Math.Sqrt((float)(sumxx * sumyy)) + eps));
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static float[] Softmax(float[] x, double gamma)
        {
            float[] y = new float[x.Length];
            float sum = 0;
            float d = 0;
            for (int i = 0; i < x.Length; ++i)
            {
                d = (float)Math.Min(50, Math.Max(-50, gamma * x[i])); // for numerical stability                
                y[i] = (float)Math.Exp(d);
                sum += y[i];
            }
            for (int i = 0; i < y.Length; ++i)
                y[i] /= sum;
            return y;
        }

        public static float[] Logistical(float[] x, double gamma)
        {
            float[] y = new float[x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                y[i] = (float)(Math.Tanh(x[i] * gamma / 2) + 1) / 2.0f;
            }
            return y;
        }

        public static void Logistical(float* x, float* y, int size, double gamma)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                y[i] = (float)(Math.Tanh(x[i] * gamma / 2) + 1) / 2.0f;
            }
        }

        public static void Tanh(float* x, float* y, int size)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                y[i] = (float)Math.Tanh(x[i]);
            }
        }

        public static unsafe void ElementwiseProduct(float * x, float * y, float* z, int size, float weight)
        {
            for (int i = 0; i < size; i++)
            {
                z[i] = z[i] * weight + x[i] * y[i];
            }
        }

        public static void SoftMax(float * a, float * b, int labelDim, int batchsize, float gamma)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float log_sum = 0;

                        for (int i = 0; i < labelDim; i++)
                        {
                            float tmpa = gamma * a[idx * labelDim + i];
                            if (i == 0)
                            {
                                log_sum = tmpa;
                                continue;
                            }
                            else
                            {
                                if (log_sum >= tmpa)
                                {
                                    log_sum = log_sum + (float)Math.Log(1 + Math.Exp((tmpa - log_sum)));
                                }
                                else
                                {
                                    log_sum = tmpa + (float)Math.Log(1 + Math.Exp((log_sum - tmpa)));
                                }
                            }
                        }

                        for (int i = 0; i < labelDim; i++)
                        {
                            float tmpa = gamma * a[idx * labelDim + i];
                            b[idx * labelDim + i] = (float)Math.Exp(tmpa - log_sum);
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Deriv_Cosine_Subspace(float * q, float * d, float* dcq, float* dcd, float* alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize * labelDim;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idx = cnt / labelDim;
                        int idy = cnt % labelDim;

                        float alpha_v = gamma * alpha[idx * labelDim + idy];
                        int id_start = idx * labelDim * subspaceDim + idy * subspaceDim;
                        float a = 0;
                        float b = eps;
                        float c = eps;
                        for (int i = 0; i < subspaceDim; i++)
                        {
                            a += q[id_start + i] * d[id_start + i];
                            b += q[id_start + i] * q[id_start + i];
                            c += d[id_start + i] * d[id_start + i];
                        }
                        b = (float)Math.Sqrt(b);
                        c = (float)Math.Sqrt(c);

                        /// tanh function.
                        if (act_type == 0)
                        {
                            for (int i = 0; i < subspaceDim; i++)
                            {
                                dcq[id_start + i] = (float)((1 - q[id_start + i]) * (1 + q[id_start + i]) * (d[id_start + i] * 1.0f / (b * c) - q[id_start + i] * a * 1.0f / (b * b * b * c)));
                                dcd[id_start + i] = (float)((1 - d[id_start + i]) * (1 + d[id_start + i]) * (q[id_start + i] * 1.0f / (b * c) - d[id_start + i] * a * 1.0f / (b * c * c * c)));
                                dcq[id_start + i] = alpha_v * dcq[id_start + i]; // *1.0f / batchsize;
                                dcd[id_start + i] = alpha_v * dcd[id_start + i]; // *1.0f / batchsize;
                            }
                        }
                        /// linear function.
                        else if (act_type == 1)
                        {
                            for (int i = 0; i < subspaceDim; i++)
                            {
                                dcq[id_start + i] = (float)((d[id_start + i] * 1.0f / (b * c) - q[id_start + i] * a * 1.0f / (b * b * b * c)));
                                dcd[id_start + i] = (float)((q[id_start + i] * 1.0f / (b * c) - d[id_start + i] * a * 1.0f / (b * c * c * c)));
                                dcq[id_start + i] = alpha_v * dcq[id_start + i];// *1.0f / batchsize;
                                dcd[id_start + i] = alpha_v * dcd[id_start + i];// *1.0f / batchsize;
                            }
                        }
                        /// 
                        else if (act_type == 2)
                        {
                            for (int i = 0; i < subspaceDim; i++)
                            {
                                if (Math.Abs(q[id_start + i]) < eps)
                                {
                                    dcq[id_start + i] = 0;
                                }
                                else
                                {
                                    dcq[id_start + i] = (float)((d[id_start + i] * 1.0f / (b * c) - q[id_start + i] * a * 1.0f / (b * b * b * c)));
                                }
                                dcq[id_start + i] = alpha_v * dcq[id_start + i]; // *1.0f / batchsize;

                                if (Math.Abs(d[id_start + i]) < eps)
                                {
                                    dcd[id_start + i] = 0;
                                }
                                else
                                {
                                    dcd[id_start + i] = (float)((q[id_start + i] * 1.0f / (b * c) - d[id_start + i] * a * 1.0f / (b * c * c * c)));
                                }
                                dcd[id_start + i] = alpha_v * dcd[id_start + i]; // *1.0f / batchsize;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void InnerProduct_Similarity(float* a, float* b, float* c, int batchsize, int dimension)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {
                        float sumxy = 0;
                        for (int i = 0; i < dimension; i++)
                        {
                            sumxy += a[idx * dimension + i] * b[idx * dimension + i];
                        }
                        c[idx] = (float)(sumxy * 1.0f);
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Deriv_InnerProduct(float* q, float* d, float* dcq, float* dcd, float* alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = batchsize;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int idx = thread_idx * process_len + t;
                    if (idx < total)
                    {

                        float alpha_v = gamma * alpha[idx];
                        int id_start = idx * Dim;

                        /// tanh function.
                        if (act_type == 0)
                        {
                            for (int i = 0; i < Dim; i++)
                            {
                                dcq[id_start + i] = (float)((1 - q[id_start + i]) * (1 + q[id_start + i]) * d[id_start + i] * alpha_v * 1.0f);
                                dcd[id_start + i] = (float)((1 - d[id_start + i]) * (1 + d[id_start + i]) * q[id_start + i] * alpha_v * 1.0f);
                                //dcq[id_start + i] = alpha_v * dcq[id_start + i] ;
                                //dcd[id_start + i] = alpha_v * dcd[id_start + i] ;
                            }
                        }
                        /// linear function.
                        else if (act_type == 1)
                        {
                            for (int i = 0; i < Dim; i++)
                            {
                                dcq[id_start + i] = (float)(d[id_start + i] * alpha_v * 1.0f);
                                dcd[id_start + i] = (float)(q[id_start + i] * alpha_v * 1.0f);
                                // dcq[id_start + i] = alpha_v * dcq[id_start + i] * 1.0f / batchsize;
                                // dcd[id_start + i] = alpha_v * dcd[id_start + i] * 1.0f / batchsize;
                            }
                        }
                        /// 
                        else if (act_type == 2)
                        {
                            for (int i = 0; i < Dim; i++)
                            {
                                if (Math.Abs(q[id_start + i]) < eps)
                                {
                                    dcq[id_start + i] = 0;
                                }
                                else
                                {
                                    dcq[id_start + i] = (float)(d[id_start + i] * alpha_v * 1.0f);
                                }


                                if (Math.Abs(d[id_start + i]) < eps)
                                {
                                    dcd[id_start + i] = 0;
                                }
                                else
                                {
                                    dcd[id_start + i] = (float)(q[id_start + i] * alpha_v * 1.0f);
                                }

                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Matrix_Add_OFFSET(float* a, int offset_a, float* b, int offset_b, int len, float mweight)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = len;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        a[offset_a + cnt] += b[offset_b + cnt] * mweight; //* alpha[alpha_index * BATCH_SIZE + i];
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }

        public static void Matrix_Add_Sigmoid(float* a, float* b, int m, int n)
        {
            int THREAD_NUM = THREAD_NUMBER;
            int total = m * n;
            int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
            Parallel.For(0, THREAD_NUM, thread_idx =>
            {
                for (int t = 0; t < process_len; t++)
                {
                    int cnt = thread_idx * process_len + t;
                    if (cnt < total)
                    {
                        int idx = cnt / m;
                        int idy = cnt % m;
                        float tvalue = a[idy * n + idx] + b[idx];
                        a[idy * n + idx] = (float)(Math.Tanh(tvalue / 2) + 1) / 2;
                    }
                    else
                    {
                        break;
                    }
                }
            });
        }


        public static void RecursiveForwardPropagateBatch(int* InstanceIndex, int batchSize,
                                            float* SeqInputMatrixs, int dim,
                                            float* WMatrix, int lag, int af, float* SeqOutputMatrix)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                for (int i = seqBegin; i < seqEnd; i++)
                {
                    int outStart = i * dim;
                    for (int l = 0; l < lag; l++)
                    {
                        int history = i - l - 1;
                        int hIndex = history * dim;
                        if (history >= seqBegin)
                        {
                            for (int h = 0; h < dim; h++)
                            {
                                int windex = l * dim * dim + h * dim;
                                float historyS = SeqInputMatrixs[hIndex + h];
                                for (int hIn = 0; hIn < dim; hIn++)
                                    SeqOutputMatrix[outStart + hIn] += historyS * WMatrix[windex + hIn];
                            }
                        }
                        else
                            break;
                    }

                    // Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
                    if (af == 1)
                    {
                        for (int h = 0; h < dim; h++)
                        {
                            SeqOutputMatrix[outStart + h] = (float)Math.Tanh(SeqOutputMatrix[outStart + h]);
                        }
                    }
                    else if (af == 2)
                    {
                        for (int h = 0; h < dim; h++)
                        {
                            float v = SeqOutputMatrix[outStart + h];
                            SeqOutputMatrix[outStart + h] = v <= 0 ? 0 : v;
                        }
                    }
                    else if (af == 3)
                    {
                        for (int h = 0; h < dim; h++)
                        {
                            SeqOutputMatrix[outStart + h] = (float)(Math.Tanh((SeqOutputMatrix[outStart + h]) / 2) + 1) / 2;
                        }
                    }
                }
            });
        }


        /// <summary>
        /// b = b * weight + M * a;
        /// </summary>
        /// <param name="a">len : inputSize</param>
        /// <param name="M">row : outputSize; column : inputSize</param>
        /// <param name="b">len : outputSize</param>
        /// <param name="inputSize"></param>
        /// <param name="outputSize"></param>
        /// <param name="weight"></param>
        public static void VectorProjectionTranspose(float* a, float* M, float* b, int inputSize, int outputSize, float weight)
        {
            for (int o = 0; o < outputSize; o++)
            {
                float sum = 0;
                for (int i = 0; i < inputSize; i++)
                {
                    sum += a[i] * M[o * inputSize + i];
                }
                b[o] = b[o] * weight + sum;
            }
        }

        /// <summary>
        ///  pDst = pDst * weight + pLeft * pRight'
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="leftRowCnt"></param>
        /// <param name="rightColumnCnt"></param>
        /// <param name="rightRowCnt"></param>
        /// <param name="weight"></param>
        public static void MatrixMultiplicationTranspose(float* pLeft, float* pRight, float* pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
        {
            for (int i = 0; i < leftRowCnt; i++)
            {
                for (int j = 0; j < rightRowCnt; j++)
                {
                    float sum = 0;
                    int leftCursor = i * rightColumnCnt;
                    int rightCursor = j * rightColumnCnt;
                    for (int k = 0; k < rightColumnCnt; k++)
                    {
                        sum += pLeft[leftCursor] * pRight[rightCursor];
                        leftCursor++;
                        rightCursor++;
                    }
                    pDst[i * rightRowCnt + j] = pDst[i * rightRowCnt + j] * weight + sum;
                }
            }
        }

        public static unsafe void RecursiveBackwardPropagateBatch(int* InstanceIndex, int batchSize,
                                    float* SeqOutputMatrixs, int dim,
                                    float* WMatrix, int lag, int af, float* SeqDerivMatrixs)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqLen = InstanceIndex[idx] - seqBegin;
                for (int i = seqLen - 1; i >= 0; i--)
                {
                    int seqIdx = seqBegin + i;
                    int cId = seqIdx * dim;
                    //float[] pDeriv = SeqDerivMatrixs.Skip(seqIdx * dim).Take(dim).ToArray();
                    //float[] pOutput = SeqOutputMatrixs.Skip(seqIdx * dim).Take(dim).ToArray();

                    if (af == 1)
                    {
                        //Deriv_Tanh(pDeriv, pOutput, dim);
                        for (int h1 = 0; h1 < dim; h1++)
                            SeqDerivMatrixs[cId + h1] = SeqDerivMatrixs[cId + h1] * (1 - SeqOutputMatrixs[cId + h1]) * (1 + SeqOutputMatrixs[cId + h1]);
                    }
                    else if (af == 2)
                    {
                        //Deriv_Rectified(pDeriv, pOutput, dim);
                        for (int h1 = 0; h1 < dim; h1++)
                            SeqDerivMatrixs[cId + h1] = SeqOutputMatrixs[cId + h1] > 0 ? SeqDerivMatrixs[cId + h1] : 0;
                    }
                    else if (af == 3)
                    {
                        //Deriv_Logstic(pDeriv, pOutput, dim, 1);
                        for (int h1 = 0; h1 < dim; h1++)
                            SeqDerivMatrixs[cId + h1] = SeqDerivMatrixs[cId + h1] * (1 - SeqOutputMatrixs[cId + h1]) * (SeqOutputMatrixs[cId + h1]);
                    }


                    ///back propagate to history states.
                    for (int h = 0; h < lag; h++)
                    {
                        int cursor = i - h - 1;
                        int hStart = h * dim * dim;
                        int hId = (seqBegin + cursor) * dim;

                        if (cursor >= 0)
                        {
                            //float[] pHistoryDeriv = SeqDerivMatrixs.Skip( (seqBegin + cursor) * dim).Take(dim).ToArray(); //DerivMatrix + cursor * dim;
                            //float[] pW = WMatrix.Skip(h * dim * dim).Take(dim * dim).ToArray();
                            //VectorProjectionTranspose(pDeriv, pW, pHistoryDeriv, dim, dim, 1);

                            for (int h2 = 0; h2 < dim; h2++)
                            {
                                float o = SeqDerivMatrixs[cId + h2];
                                for (int h1 = 0; h1 < dim; h1++)
                                {
                                    SeqDerivMatrixs[hId + h1] += o * WMatrix[hStart + h1 * dim + h2];
                                }
                            }
                        }
                    }
                }
            });
        }


        /// <summary>
        /// pDst = pDst + pLeft' * pRight * wei
        /// </summary>
        /// <param name="pLeft"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="leftRowCnt"></param>
        /// <param name="leftColumnCnt"></param>
        /// <param name="rightColumnCnt"></param>
        /// <param name="wei"></param>
        public unsafe static void MatrixMultiplicationLeftTranspose(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float alpha, float beta)
        {
            Parallel.For(0, leftColumnCnt, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, i =>
            {
                for (int j = 0; j < rightColumnCnt; j++)
                {
                    float sum = 0;
                    int leftCursor = i;
                    int rightCursor = j;
                    for (int k = 0; k < leftRowCnt; k++)
                    {
                        sum += pLeft[leftCursor] * pRight[rightCursor];
                        leftCursor += leftColumnCnt;
                        rightCursor += rightColumnCnt;
                    }
                    pDst[i * rightColumnCnt + j] = alpha * pDst[i * rightColumnCnt + j] + beta * sum;
                }
            });
        }

        /// <summary>
        /// (pRowIndex, RowNum, pColumnIndex, pValue, elementsize) -> Sparse Matrix S
        /// pDst = pDst + weight * S' * pRight;
        /// </summary>
        /// <param name="Smp_Index"></param>
        /// <param name="batchsize"></param>
        /// <param name="Fea_Index"></param>
        /// <param name="Fea_Value"></param>
        /// <param name="elementsize"></param>
        /// <param name="pRight"></param>
        /// <param name="pDst"></param>
        /// <param name="leftRowCnt"></param>
        /// <param name="leftColumnCnt"></param>
        /// <param name="rightColumnCnt"></param>
        /// <param name="wei"></param>
        public static void MatrixMultiplicationSparseLeftTranspose(int* pRowIndex, int RowNum, int* pColumnIndex, float* pValue, int elementsize,
                float* pRight, float* pDst, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            //for (int j = 0; j < rightColumnCnt; j++)
           Parallel.For(0, rightColumnCnt, new ParallelOptions { MaxDegreeOfParallelism = 32 }, j =>
           {
               for (int i = 0; i < RowNum; i++)
               {
                   int feaBegin = i == 0 ? 0 : pRowIndex[i - 1];
                   int feaEnd = pRowIndex[i];
                   for (int k = feaBegin; k < feaEnd; k++)
                   {
                       int aIdx = pColumnIndex[k];
                       float aValue = pValue[k] * pRight[i * rightColumnCnt + j];
                       pDst[aIdx * rightColumnCnt + j] += wei * aValue;
                   }
               }
           });
        }


        /// <summary>
        /// result = result + wei * \sum_i=0..row-1(matrix_i)
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="result"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="wei"></param>
        public static void ColumnWiseSum(float* matrix, float* result, int row, int col, float wei)
        {
            Parallel.For(0, col, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i => //for (int i = 0; i < col; i++)
            {
                float sum = 0;
                for (int j = 0; j < row; j++)
                {
                    sum += matrix[j * col + i];
                }
                result[i] = result[i] + wei * sum;
            });
        }

        public static void RecursiveUpdateBatch(int* InstanceIndex, int batchSize, int seqSize, float* SeqOutputMatrixs, int dim, float* SeqDerivMatrixs, int lag, float* gradient, float weight)
        {

            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                for (int h = seqEnd - 1; h >= seqBegin; h--)
                {
                    int cId = h * dim;
                    for (int l = 0; l < lag; l++)
                    {
                        int history = h - l - 1;
                        int hStart = l * dim * dim;
                        int hId = history * dim;
                        if (history >= seqBegin)
                        {
                            for (int h2 = 0; h2 < dim; h2++)
                            {
                                float o = SeqDerivMatrixs[cId + h2];
                                for (int h1 = 0; h1 < dim; h1++)
                                {
                                    gradient[hStart + h1 * dim + h2] += weight * SeqOutputMatrixs[hId + h1] * o;
                                }
                            }
                        }
                    }
                }
            });
        }

        public static void DerivCrossEntropy(float* outputScore, float* outputLabel, float* outputDeriv, int outputSize)
        {
            for (int idx = 0; idx < outputSize; idx++)
            {
                float score = (float)(Math.Tanh(outputScore[idx] / 2) + 1) / 2.0f;
                if (outputLabel[idx] == ParameterSetting.MissingLabel) outputDeriv[idx] = 0;
                else if (outputLabel[idx] > 0) outputDeriv[idx] = (1 - score);
                else outputDeriv[idx] = (-score);
            }
        }

        public static void ClipVector(float* a, int m, float maxThreshold, float minThreshold)
        {
            //for(int i=0;i<m;i++)
            Parallel.For(0, m, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
            {
                if (a[i] > maxThreshold) a[i] = maxThreshold;
                if (a[i] < minThreshold) a[i] = minThreshold;
            });
        }

        public static void Add_Vector(float* a, float* b, float* c, int m, float awei, float bwei)
        {
            Parallel.For(0, m, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
            {
                c[i] = a[i] * awei + b[i] * bwei;
            });
        }

        public static void Ada_Gradient(float* ada, float* grad, int m)
        {
            Parallel.For(0, m, new ParallelOptions { MaxDegreeOfParallelism = 32 }, id =>
            {
                ada[id] = ada[id] + grad[id] * grad[id];
                grad[id] = (float)(grad[id] / (Math.Sqrt(ada[id]) + 1e-6));
            });
        }

        //
        public static void Inner_Product_EX_Full(float* a, float* b, int* neg_list, float* c, int nTrial, int BATCHSIZE,
            int batchsize, int dimension, float eps)
        {
            Parallel.For(0, batchsize * nTrial, new ParallelOptions { MaxDegreeOfParallelism = 32 }, id =>
            {
                int idx = id % batchsize;
                int idy = id / batchsize;
                float sumxy = 0;
                int a_iter = (idx * dimension);
                int b_iter = (neg_list[idy * BATCHSIZE + idx] * dimension);
                int a_iter_end = a_iter + dimension;
                while (a_iter < a_iter_end)
                {
                    //sumxx += (*a_iter) * (*a_iter);
                    //sumyy += (*b_iter) * (*b_iter);
                    sumxy += a[a_iter] * b[b_iter];
                    a_iter++;
                    b_iter++;
                }
                c[(idy + 1) * BATCHSIZE + idx] = (float)(sumxy); /// ((float)sqrtf(sumxx * sumyy) + eps));
            });
        }

        //For LSTM    
        /// <summary>
        /// y[yidx : yidx + row] =  y[yidx : yidx + row] * weight + w^t * x[xidx: xid + col]
        /// 
        /// </summary>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="x"></param>
        /// <param name="begin"></param>
        /// <param name="col"></param>
        /// <param name="dim"></param>
        /// <param name="idx"></param>
        private static void SeqTransMatrixMultiplication(float* y, float* w, float* x, int row, int col, int yidx, int xidx, float weight)
        {
            Parallel.For(0, row, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, i =>
            {
                float sum = 0;
                for (int j = 0; j < col; j++)
                {
                    sum += w[j * row + i] * x[xidx + j];
                }
                y[yidx + i] = y[yidx + i] * weight + sum;
            });
        }

        /// <summary>
        /// y[yidx : yidx + row] =  y[yidx : yidx + row] * weight + w * x[xidx: xid + col]
        /// 
        /// </summary>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="x"></param>
        /// <param name="begin"></param>
        /// <param name="col"></param>
        /// <param name="dim"></param>
        /// <param name="idx"></param>
        private static void SeqMatrixMultiplication(float* y, float* W, float* x, int row, int col, int yidx, int xidx, float weight)
        {
            Parallel.For(0, row, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, i =>
            {
                float sum = 0;
                for (int j = 0; j < col; j++)
                {
                    sum += W[i * row + j] * x[xidx + j];
                }
                y[yidx + i] = y[yidx + i] * weight + sum;
            });
        }


        private static void Logistical(float* y, float* x, int yidx, int xidx, int size, float gamma)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                y[yidx + i] = (float)(Math.Tanh(x[xidx + i] * gamma / 2) + 1) / 2.0f;
            }
        }

        private static void Deriv_Logistic(float* error, float* output, int eidx, int oidx, int size, float gamma)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                error[eidx + i] = error[eidx + i] * (1 - output[oidx + i]) * output[oidx + i] * gamma;
            }
        }

        private static void Tanh(float* y, float* x, int yidx, int xidx, int size)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                y[yidx + i] = (float)Math.Tanh(x[xidx + i]);
            }
        }

        public static void Deriv_Tanh(float* error, float* output, int eidx, int oidx, int size)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                error[eidx + i] = error[eidx + i] * (1 - output[oidx + i]) * (1 + output[oidx + i]);
            }
        }

        private static void ElementwiseProduct(float* z, float* x, float* y, int size, int zidx, int xidx, int yidx, float weight)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                z[zidx + i] = z[zidx + i] * weight + x[xidx + i] * y[yidx + i];
            }
        }

        /// <summary>
        /// y = y * weight + x
        /// </summary>
        /// <param name="y"></param>
        /// <param name="x"></param>
        /// <param name="size"></param>
        /// <param name="xidx"></param>
        /// <param name="yidx"></param>
        /// <param name="weight"></param>
        private static void SeqMatrixAdd(float* y, float* x, int size, int yidx, int xidx, float weight)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                y[yidx + i] = y[yidx + i] * weight + x[xidx + i];
            }
        }

        /// <summary>
        /// Note that @ denotes the element-wise product
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="batchSize"></param>
        /// <param name="o"></param>
        /// <param name="gate_i"></param>
        /// <param name="c_hat"></param>
        /// <param name="gate_f"></param>
        /// <param name="c"></param>
        /// <param name="gate_o"></param>
        /// <param name="tanhc"></param>
        /// <param name="cell"></param>
        /// <param name="Ui"></param>
        /// <param name="Uc"></param>
        /// <param name="Uf"></param>
        /// <param name="Uo"></param>
        /// <param name="Vo"></param>
        public static void LSTMForwardPropagateBatch(int* InstanceIndex, int batchSize,  //->x;
            float* o, //-> output,
            float* gate_i, //->gate i,
            float* c_hat, //->candidate memory,
            float* gate_f, //->forget gate,
            float* c, //->memory,
            float* gate_o, //->gate o,
            float* tanhc, //->tanh memory
            int cell,
            float* Ui,
            float* Uc,
            float* Uf,
            float* Uo, float* Vo)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                ///time step
                for (int t = seqBegin; t < seqEnd; t++)
                {
                    if (t > seqBegin)
                    {
                        //gate_i += U_{i} * o_{t-1}
                        SeqTransMatrixMultiplication(gate_i, Ui, o, cell, cell, t * cell, (t - 1) * cell, 1);
                        //gate_f += U_{f} * o_{t-1}
                        SeqTransMatrixMultiplication(gate_f, Uf, o, cell, cell, t * cell, (t - 1) * cell, 1);
                        //c_hat += U_{c} * o_{t-1}
                        SeqTransMatrixMultiplication(c_hat, Uc, o, cell, cell, t * cell, (t - 1) * cell, 1);
                        //gate_o += U_{o} * o_{t-1}
                        SeqTransMatrixMultiplication(gate_o, Uo, o, cell, cell, t * cell, (t - 1) * cell, 1);
                    }

                    //gate_i = sigmoid(gate_i)
                    Logistical(gate_i, gate_i, t * cell, t * cell, cell, 1);
                    //gate_f = sigmoid(gate_f)
                    Logistical(gate_f, gate_f, t * cell, t * cell, cell, 1);
                    //c_hat = tanh(c_hat)
                    Tanh(c_hat, c_hat, t * cell, t * cell, cell);
                    //c = gate_i @ c_hat
                    ElementwiseProduct(c, c_hat, gate_i, cell, t * cell, t * cell, t * cell, 0);

                    //c += gate_f @ c(t-1)
                    if (t > seqBegin)
                    {
                        ElementwiseProduct(c, c, gate_f, cell, t * cell, (t - 1) * cell, t * cell, 1);
                    }

                    //gate_o += Vo * c(t)
                    SeqTransMatrixMultiplication(gate_o, Vo, c, cell, cell, t * cell, t * cell, 1);
                    //gate_o = simgoid(gate_o)
                    Logistical(gate_o, gate_o, t * cell, t * cell, cell, 1);

                    //tanhc = tanh(c)
                    Tanh(tanhc, c, t * cell, t * cell, cell);

                    //o = gate_o @ tanhc
                    ElementwiseProduct(o, gate_o, tanhc, cell, t * cell, t * cell, t * cell, 0);
                }
            });
        }

        public static void LSTMBackwardPropagateBatch(int* InstanceIndex, int batchSize, int seqSize, int cell,
            float* o, float* gate_i, float* c_hat, float* gate_f, float* c, float* gate_o, float* tanhc,
            float* deriv_o, float* deriv_gate_i, float* deriv_cHat, float* deriv_gate_f, float* deriv_c, float* deriv_gate_o, float* deriv_tanhc,
            float* Ui, float* Uc, float* Uf, float* Uo, float* Vo)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                ///time step
                for (int t = seqEnd - 1; t >= seqBegin; t--)
                {
                    // deriv_gate_o = deriv_o @ tanhc
                    ElementwiseProduct(deriv_gate_o, deriv_o, tanhc, cell, t * cell, t * cell, t * cell, 0);
                    // deriv_gate_o = deriv_gate_o *  gate_o * (1 - gate_o)
                    Deriv_Logistic(deriv_gate_o, gate_o, t * cell, t * cell, cell, 1.0f);

                    //deriv_tanhc = deriv_o @ gate_o
                    ElementwiseProduct(deriv_tanhc, deriv_o, gate_o, cell, t * cell, t * cell, t * cell, 0);
                    // deriv_tanhc = deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
                    Deriv_Tanh(deriv_tanhc, tanhc, t * cell, t * cell, cell);

                    if (t > seqBegin)
                    {
                        // deriv_o(t-1) += deriv_gate * Uo
                        SeqMatrixMultiplication(deriv_o, Uo, deriv_gate_o, cell, cell, (t - 1) * cell, t * cell, 1);
                    }

                    // deriv_c += deriv_gate_o * Vo
                    SeqMatrixMultiplication(deriv_c, Vo, deriv_gate_o, cell, cell, t * cell, t * cell, 1);
                    //deriv_c += deriv_tanhc
                    SeqMatrixAdd(deriv_c, deriv_tanhc, cell, t * cell, t * cell, 1.0f);

                    // deriv_c_hat = deriv_c @ gate_i
                    ElementwiseProduct(deriv_cHat, deriv_c, gate_i, cell, t * cell, t * cell, t * cell, 0);
                    // deriv_c_hat = deriv_c_hat * (1 + c_hat)*( 1- c_hat)
                    Deriv_Tanh(deriv_cHat, c_hat, t * cell, t * cell, cell);

                    //deriv_gate_i = deriv_c @ c_hat
                    ElementwiseProduct(deriv_gate_i, deriv_c, c_hat, cell, t * cell, t * cell, t * cell, 0);
                    //deriv_gate_i = deriv_gate_i * gate_i * (1 - gate_i)
                    Deriv_Logistic(deriv_gate_i, gate_i, t * cell, t * cell, cell, 1.0f);

                    if (t > seqBegin)
                    {
                        //deriv_gate_f = deriv_c * c(t-1)
                        ElementwiseProduct(deriv_gate_f, deriv_c, c, cell, t * cell, t * cell, (t - 1) * cell, 0);
                        //deriv_gate_f = deriv_gate_f * (1 -gate_f) * gate_f
                        Deriv_Logistic(deriv_gate_f, gate_f, t * cell, t * cell, cell, 1.0f);
                        //deriv_o(t -1) += deriv_gate_f * Uf
                        SeqMatrixMultiplication(deriv_o, Uf, deriv_gate_f, cell, cell, (t - 1) * cell, t * cell, 1);

                        //deriv_o(t-1) += deriv_gate_i * Ui
                        SeqMatrixMultiplication(deriv_o, Ui, deriv_gate_i, cell, cell, (t - 1) * cell, t * cell, 1);
                        //deriv_o(t-1) += deriv_cHat(t) * Uc
                        SeqMatrixMultiplication(deriv_o, Uc, deriv_cHat, cell, cell, (t - 1) * cell, t * cell, 1);

                        //deriv_c(t-1) += deriv_c @ gate_f
                        ElementwiseProduct(deriv_c, deriv_c, gate_f, cell, (t - 1) * cell, t * cell, t * cell, 1);
                    }
                }
            });
        }

        public static void Calculate_SampleCrossEntropyDeriv(float* output, float* deriv, int dim, float* label, int batchsize, float gamma)
        {
            Parallel.For(0, dim * batchsize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                deriv[idx] = -gamma * (float)(Math.Tanh(gamma * output[idx] / 2) + 1) / 2.0f;
                if (idx % dim == 0) deriv[idx] += gamma * label[idx / dim];
            });
        }

        public static void RNNLabelOutput(int* smpIdx, int batchSize, float* seqInput, int seqSize, float* Wmatrix, int lag, int feaDim, int hiddenDim, float* seqOut)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                if (idx < batchSize)
                {
                    int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
                    int seqEnd = smpIdx[idx] - 1;
                    int seqStart = (seqEnd - lag);
                    if (seqStart < seqBegin) seqStart = seqBegin;

                    int outStart = idx * hiddenDim;
                    for (int h = 0; h < hiddenDim; h++)
                    {
                        float sum = 0f;
                        for (int i = seqEnd; i >= seqStart; i--)
                        {
                            int j = i * feaDim;
                            int m = (seqEnd - i) * feaDim * hiddenDim + h;
                            for (int f = 0; f < feaDim; f++)
                            {
                                sum += seqInput[j++] * Wmatrix[m];
                                m += hiddenDim;
                            }
                            seqOut[outStart + h] = sum;
                        }
                    }
                }
            });
        }

        public static void DerivRNNLabelOutput(int* smpIdx, int batchSize, float* seqInputDeriv, int seqSize, float* Wmatrix, int lag, int feaDim, int hiddenDim, float* seqOutDeriv)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                if (idx < batchSize)
                {
                    int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
                    int seqEnd = smpIdx[idx] - 1;
                    int seqStart = (seqEnd - lag);
                    if (seqStart < seqBegin) seqStart = seqBegin;

                    int outStart = idx * hiddenDim;

                    for (int i = seqEnd; i >= seqStart; i--)
                    {
                        int j = i * feaDim;
                        int m = (seqEnd - i) * feaDim * hiddenDim;
                        for (int f = 0; f < feaDim; f++)
                        {
                            float sum = 0;
                            for (int h = 0; h < hiddenDim; h++)
                            {
                                sum += seqOutDeriv[outStart + h] * Wmatrix[m++];
                            }
                            ///Keep Previous Input Deriv.
                            seqInputDeriv[j + f] += sum;
                        }
                    }
                }
            });


        }

        public static void UpdateRNNLabeloutput(int* smpIdx, int batchSize, float* seqInput, int seqSize, float* Wmatrix, int lag, int feaDim, int hiddenDim, float* seqOutDeriv, float wei)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
                int seqEnd = smpIdx[idx] - 1;
                int seqStart = (seqEnd - lag);
                if (seqStart < seqBegin) seqStart = seqBegin;

                int outStart = idx * hiddenDim;

                for (int i = seqEnd; i >= seqStart; i--)
                {
                    int j = i * feaDim;
                    int m = (seqEnd - i) * feaDim * hiddenDim;
                    for (int f = 0; f < feaDim; f++)
                    {
                        float seqInputValue = seqInput[j];
                        for (int h = 0; h < hiddenDim; h++)
                        {
                            Wmatrix[m] += seqOutDeriv[outStart + h] * seqInputValue * wei;
                            m++;
                        }
                        j++;
                    }
                }
            });
        }

        /// <summary>
        /// Extract last time step states in the seq matrix
        /// </summary>
        /// <param name="InstanceIndex"></param>
        /// <param name="batchSize"></param>
        /// <param name="SeqMatrixs"></param>
        /// <param name="dim"></param>
        /// <param name="matrix"></param>
        public static void LastTimeStepExtractSeqMatrix(int* InstanceIndex, int batchSize, float* SeqMatrixs, int dim, float* matrix)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                if (seqEnd > seqBegin)
                {
                    for (int i = 0; i < dim; i++)
                    {
                        matrix[idx * dim + i] = SeqMatrixs[(seqEnd - 1) * dim + i];
                    }
                }
                else
                {
                    for (int i = 0; i < dim; i++)
                    {
                        matrix[idx * dim + i] = 0;
                    }
                }
            });

        }


        public static void Deriv_CosineSimilarity_partialMatching(float* src, float* tgt, float* srcSquare, float* tgtSquare, int dim,
                int* src2MatchIdx, int* src2MatchElement, int* tgtIdx, int srcSize, int matchSize, float* simi, float* derivSimi,
                float* dcSrc, float eps)
        {
            Parallel.For(0, srcSize * dim, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, index =>
            {
                int idx = index / dim;
                int idy = index % dim;
                int matchBeginIdx = idx == 0 ? 0 : src2MatchIdx[idx - 1];
                int matchEndIdx = src2MatchIdx[idx];
                float sum = 0;
                float qRoot = srcSquare[idx];
                float qv = src[idx * dim + idy];
                float qSquare = qRoot * qRoot;
                for (int match = matchBeginIdx; match < matchEndIdx; match++)
                {
                    int mIdx = src2MatchElement[match];
                    int dIdx = tgtIdx[mIdx];
                    float dRoot = tgtSquare[dIdx];
                    sum += derivSimi[mIdx] * (tgt[dIdx * dim + idy] / (qRoot * dRoot + eps) - qv * simi[mIdx] / (qSquare + eps));
                }
                dcSrc[idx * dim + idy] += sum;

            });
        }

        public static void Deriv_CosineSimilarity_Matching(float* q, float* d, float* qSquare, float* dSquare, int dim,
        int* src2MatchIdx, int* src2MatchElement, int* tgt2MatchIdx, int* tgt2MatchElement, int* srcIdx, int* tgtIdx, int srcSize, int tgtSize, int matchSize,
        float* simi, float* derivSimi, float* dcq, float* dcd, float eps)
        {

            Deriv_CosineSimilarity_partialMatching(q, d, qSquare, dSquare, dim,
                src2MatchIdx, src2MatchElement, tgtIdx, srcSize, matchSize, simi, derivSimi, dcq, eps);

            Deriv_CosineSimilarity_partialMatching(d, q, dSquare, qSquare, dim,
                tgt2MatchIdx, tgt2MatchElement, srcIdx, tgtSize, matchSize, simi, derivSimi,
                dcd, eps);
        }


        public static void Deriv_InnerProduct_partialMatching(float* d, int dim,
            int* src2MatchIdx, int* src2MatchElement, int* tgtIdx, int srcSize, int matchSize, float* derivSimi,
            float* dcq, float eps)
        {
            Parallel.For(0, srcSize * dim, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, index =>
            {
                int idx = index / dim;
                int idy = index % dim;
                int matchBeginIdx = idx == 0 ? 0 : src2MatchIdx[idx - 1];
                int matchEndIdx = src2MatchIdx[idx];
                float sum = 0;
                for (int match = matchBeginIdx; match < matchEndIdx; match++)
                {
                    int mIdx = src2MatchElement[match];
                    int dIdx = tgtIdx[mIdx];
                    sum += derivSimi[mIdx] * d[dIdx * dim + idy];
                }
                dcq[idx * dim + idy] += sum;
            });
        }

        public static void Deriv_InnerProduct_Matching(float* q, float* d, int dim,
            int* src2MatchIdx, int* src2MatchElement, int* tgt2MatchIdx, int* tgt2MatchElement, int* srcIdx, int* tgtIdx, int srcSize, int tgtSize, int matchSize,
            float* derivSimi, float* dcq, float* dcd, float eps)
        {
            Deriv_InnerProduct_partialMatching(d, dim,
                src2MatchIdx, src2MatchElement, tgtIdx, srcSize, matchSize, derivSimi, dcq, eps);

        }


        //GRU
        /// <summary>
        /// y = x * weight + scalar 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="size"></param>
        /// <param name="xidx"></param>
        /// <param name="yidx"></param>
        /// <param name="weight"></param>
        public static void ScalarMatrixAdd(float* y, float* x, int size, int yidx, int xidx, float scalar, float weight)
        {
            for (int i = 0; i < size && i < size; i++)
            {
                y[yidx + i] = x[xidx + i] * weight + scalar;
            }
        }

        public static void GRUForwardPropagateBatch(int* InstanceIndex, int batchSize, int Dim,  //->x;
            float* H, //->hidden,
            float* GateR, //->reset gate,
            float* GateZ, //->update gate,
            float* HHat, //->chat
            float* RestH,
            float* Ur,
            float* Uz,
            float* Uh)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];

                ///time step
                for (int t = seqBegin; t < seqEnd; t++)
                {
                    if (t > seqBegin)
                    {
                        //GateZ += Uz * h(t-1)
                        SeqTransMatrixMultiplication(GateZ, Uz, H, Dim, Dim, t * Dim, (t - 1) * Dim, 1);

                        //GateR += Ur * h(t-1)
                        SeqTransMatrixMultiplication(GateR, Ur, H, Dim, Dim, t * Dim, (t - 1) * Dim, 1);
                    }

                    //Sigmoid(GateR)
                    Logistical(GateR, GateR, t * Dim, t * Dim, Dim, 1);
                    //Sigmoid(GateZ)
                    Logistical(GateZ, GateZ, t * Dim, t * Dim, Dim, 1);

                    //compute HHat
                    if (t > seqBegin)
                    {
                        //RestH = GateR @ H(t-1)
                        ElementwiseProduct(RestH, H, GateR, Dim, t * Dim, (t - 1) * Dim, t * Dim, 0);
                        //HHat += Uh @ RestH
                        SeqTransMatrixMultiplication(HHat, Uh, RestH, Dim, Dim, t * Dim, t * Dim, 1);
                    }
                    //Tanh(HHat)
                    Tanh(HHat, HHat, t * Dim, t * Dim, Dim);

                    //compute H
                    //H(t) = (1 - GateZ(t))
                    ScalarMatrixAdd(H, GateZ, Dim, t * Dim, t * Dim, 1.0f, -1.0f);
                    //H(t) = H(t) @ HHat(t)
                    ElementwiseProduct(H, HHat, H, Dim, t * Dim, t * Dim, t * Dim, 0);

                    if (t > seqBegin)
                    {
                        //H(t) += H(t-1) @ GateZ(t)
                        ElementwiseProduct(H, GateZ, H, Dim, t * Dim, t * Dim, (t - 1) * Dim, 1.0f);
                    }
                }
            });
        }

        public static void GRUBackwardPropagateBatch(int* InstanceIndex, int batchSize, int Dim,//->x
            float* H, //->hidden
            float* GateR, //->reset gate,
            float* GateZ, //->update gate,
            float* HHat, //->hat
            float* RestH, //H @ R
            float* DerivH,
            float* DerivGateR,
            float* DerivGateZ,
            float* DerivHHat,
            float* DerivRestH,
            float* Ur,
            float* Uz,
            float* Uh)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int seqBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int seqEnd = InstanceIndex[idx];
                float[] temp = new float[Dim];

                ///time step
                fixed(float * ptmp = &temp[0])
                for (int t = seqEnd - 1; t > seqBegin; t--)
                {
                    //temp = 1 - GateZ
                    ScalarMatrixAdd(ptmp, GateZ, Dim, 0, t * Dim, 1.0f, -1.0f);

                    //DerivHHat = DerivH @ temp
                    ElementwiseProduct(DerivHHat, ptmp, DerivH, Dim, t * Dim, 0, t * Dim, 0);
                    //DerivHHat = DerivHHat * ( 1- HHat) * HHat 
                    Deriv_Tanh(DerivHHat, HHat, t * Dim, t * Dim, Dim);

                    //temp = -HHat
                    ScalarMatrixAdd(ptmp, HHat, Dim, 0, t * Dim, 0, -1.0f);
                    //DerivGateZ = temp @ HHat
                    ElementwiseProduct(DerivGateZ, ptmp, DerivH, Dim, t * Dim, 0, t * Dim, 0);
                    if (t > seqBegin)
                    {
                        //DerivGateZ += DerivH @ H(t-1)
                        ElementwiseProduct(DerivGateZ, H, DerivH, Dim, t * Dim, (t - 1) * Dim, t * Dim, 1.0f);
                    }

                    //DerivGateZ = DerivGateZ * (1 + GateZ) * (1 - GateZ)
                    Deriv_Logistic(DerivGateZ, GateZ, t * Dim, t * Dim, Dim, 1.0f);

                    //DerivRestH = Uh * DerivHHat
                    SeqMatrixMultiplication(DerivRestH, Uh, DerivHHat, Dim, Dim, t * Dim, t * Dim, 0);

                    //DerivGateR = DerivRestH @ H(t -1)
                    if (t > seqBegin)
                    {
                        ElementwiseProduct(DerivGateR, H, DerivRestH, Dim, t * Dim, (t - 1) * Dim, t * Dim, 0);
                    }
                    //DerivGateR = DerivGateR * GateR * (1 - GateR)
                    Deriv_Logistic(DerivGateR, GateR, t * Dim, t * Dim, Dim, 1.0f);

                    //compute DerivH(t-1)
                    if (t > seqBegin)
                    {
                        //DerivH(t-1) += GateZ @ DerivH
                        ElementwiseProduct(DerivH, DerivH, GateZ, Dim, (t - 1) * Dim, t * Dim, t * Dim, 1.0f);

                        //DerivH(t-1) += DerivRestH @ GateR
                        ElementwiseProduct(DerivH, DerivRestH, GateR, Dim, (t - 1) * Dim, t * Dim, t * Dim, 1.0f);

                        //DerivH(t-1) += Uz * DerivGateZ
                        SeqMatrixMultiplication(DerivH, Uz, DerivGateZ, Dim, Dim, (t - 1) * Dim, t * Dim, 1.0f);

                        //DerivH(t-1) += Ur * DerivGater
                        SeqMatrixMultiplication(DerivH, Ur, DerivGateR, Dim, Dim, (t - 1) * Dim, t * Dim, 1.0f);
                    }
                }
            });
        }

        public static void RMSPropGradient(float* ada, float* grad, float gamma, float epsilon, int m)
        {
            Parallel.For(0, m, new ParallelOptions { MaxDegreeOfParallelism = 32 }, id =>
            {
                ada[id] = ada[id] * gamma + (1 - gamma) * grad[id] * grad[id];
                grad[id] = (float)(grad[id] / Math.Sqrt(ada[id] + epsilon));
            });
        }

        public static void VectorSquareAdd(float* dst, float* X, float* Y, float wx, float wy, int m)
        {
            Parallel.For(0, m, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, id =>
            {
                dst[id] = X[id] * wx + Y[id] * Y[id] * wy;
            });
        }

        /// <summary>
        /// UpdateGradient
        /// 
        /// </summary>
        public static void AdaMGradient(float* M, float* V, float* G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
        {
            Parallel.For(0, size, new ParallelOptions { MaxDegreeOfParallelism = 32 }, id =>
            {
                float fd = (float)(Math.Sqrt(V[id] / (1 - Math.Pow(beta2, t))) + epsilon);
                G[id] = (float)(alpha * M[id] / ((1 - Math.Pow(beta1, t))) / fd);
            });
        }

        public static void AdaDeltaGradient(float* deltaX, float* AccumGrad, float* AccumUpdate, float* Grad, float epsilon, int size)
        {
            Parallel.For(0, size, new ParallelOptions { MaxDegreeOfParallelism = 32 }, id =>
            {
                deltaX[id] = (float)(Grad[id] * (Math.Sqrt(AccumUpdate[id]) + epsilon) / ((Math.Sqrt(AccumGrad[id]) + epsilon)));
            });
        }

        public static void AdaMax(float* V, float* M, float* G, float alpha, float beta, float epsilon, int size)
        {
            Parallel.For(0, size, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, id =>
               {
                   float val = beta * V[id];
                   float absG = G[id] > 0 ? G[id] : -G[id];
                   V[id] = Math.Max(val, absG);
                   G[id] = alpha * M[id] / (V[id] + epsilon);
               });
        }

        public static float L2Norm(float* X, int size)
        {
            float sum = 0;
            for (int i = 0; i < size && i < size; i++)
            {
                sum += X[i] * X[i];
            }

            return (float)Math.Sqrt(sum);
        }

        public static void AddAndClear(float* a, float* b, float bwei, int size)
        {
            for (int i = 0; i < size; i++)
            {
                a[i] += b[i] * bwei;
                b[i] = 0;
            }
        }

        public unsafe static void Matrix_AdditionMask(float * a, int * aMask, float * b, int * bMask, float * c, int * cMask, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            //for(int i=0;i<batchSize;i++)
            //Enumerable.Range(0, batchSize).AsParallel().ForAll(i =>
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, i =>
            {
                int aIdx = aMask == null ? i : aMask[ i];
                int bIdx = bMask == null ? i : bMask[ i];
                int cIdx = cMask == null ? i : cMask[ i];

                for (int d = 0; d < dim; d++)
                {
                    c[cIdx * dim + d] =
                           cwei * c[cIdx * dim + d] +
                           awei * a[aIdx * dim + d] +
                           bwei * b[bIdx * dim + d];
                }
            });
        }

        public unsafe static void SparseSoftmax(int * smpIdx, float * outputScore, float * outputProb, float gamma, int batchSize)
        {
            //Enumerable.Range(0, batchSize).AsParallel().ForAll(idx =>
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int start = idx == 0 ? 0 : smpIdx[idx - 1];
                int end = smpIdx[idx];
                //get the max
                float max = float.MinValue;
                for (int i = start; i < end; i++)
                {
                    max = Math.Max(max, outputScore[i]);
                }

                double sum = 0;
                for (int i = start; i < end; i++)
                {
                    float exp = (float)Math.Exp(gamma * (outputScore[i] - max));
                    outputProb[i] = exp;
                    sum += exp;
                }

                for (int i = start; i < end; i++)
                {
                    outputProb[i] = (float)(outputProb[i] / sum);
                }
            });
        }

        public unsafe static void ColumnWiseSumMask(float * matrix, int * matrixMask, 
            float * weight, int * smpIdx, int batchSize, float * output,
            int * outputMask, int row, int col, float alpha, float beta)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
            {
                int start = idx == 0 ? 0 : smpIdx[idx - 1];
                int end = smpIdx[idx];

                float[] sum = new float[col];
                for (int i = start; i < end; i++)
                {
                    for (int j = 0; j < col; j++)
                    {
                        int ij = matrixMask == null ? (i * col + j) : (matrixMask[i] * col + j);
                        sum[j] += matrix[ij] * (weight == null ? 1 : weight[i]);
                    }
                }

                for (int j = 0; j < col; j++)
                {
                    int mj = outputMask == null ? (idx * col + j) : (outputMask[idx] * col + j);
                    output[mj] = alpha * output[mj] + beta * sum[j];
                }
            });
        }

        public static void ColumnWiseSumMask_Safe(float* matrix,  int* matrixMask, 
            float* weight, int* smpIdx, int batchSize, float* output,
            int* outputMask, int row, int col, float alpha, float beta)
        {
            Parallel.For(0, batchSize * col, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, id =>
            {
                int x = id % col;
                int y = id / col;

                int start = y == 0 ? 0 : smpIdx[y - 1];
                int end = smpIdx[y];

                int op = outputMask == null ? y : outputMask[y];

                //float* sum = new float[col];
                float sum = 0;
                for (int i = start; i < end; i++)
                {
                    int mp = matrixMask == null ? i : matrixMask[i];
                    sum += matrix[mp * col + x] * (weight == null ? 1 : weight[i]);
                }

                output[op * col + x] = alpha * output[op * col + x] + beta * sum;
            });
        }


        public unsafe static void GetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * MapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
        {
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            Parallel.For(0, batchSize * dim, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, id =>
            {
                int idx = id / dim;
                int idy = id % dim;

                int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
                int instanceEnd = InstanceIndex[idx] - 1;
                int pos = -1;
                if (isReverse == 0)
                {
                    pos = instanceBegin + order;
                }
                else
                {
                    pos = instanceEnd - order;
                }

                if (pos >= instanceBegin && pos <= instanceEnd)
                {
                    int newPos = MapForward == null ? pos : MapForward[pos];
                    matrix[idx * dim + idy] = alpha * matrix[idx * dim + idy] + beta * SeqMatrixs[newPos * dim + idy];
                }
                else matrix[idx * dim + idy] = 0;
            });
        }

        public static void Matrix_Mask(float* output, int* dropoutMask, int batchSize, int dim)
        {
            Parallel.For(0, batchSize, i =>
            {
                for (int j = 0; j < dim; j++)
                {
                    if (dropoutMask[j] == 0)
                    {
                        output[i * dim + j] = 0;
                    }
                }
            });
        }

        public unsafe static void Matrix_AdditionExMask(float * a, int * aMask, int skipA, float * b, int * bMask, int skipB, float * c, int * cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            Parallel.For(0, batchSize * dim, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, id =>
            {
                int x = id % dim;
                int y = id / dim;
                int ya = aMask == null ? y : aMask[y];
                int yb = bMask == null ? y : bMask[y];
                int yc = cMask == null ? y : cMask[y];

                int aPos = ya * skipA + x;
                int bPos = yb * skipB + x;
                int cPos = yc * skipC + x;
                c[cPos] = cwei * c[cPos] + awei * a[aPos] + bwei * b[bPos];
            });
        }

        public unsafe static void Matrix_AdditionEx(float * a, int skipA, float * b, int skipB, float * c, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            // Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            Parallel.For(0, batchSize * dim, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, id =>
            {
                int x = id % dim;
                int y = id / dim;
                int aPos = y * skipA + x;
                int bPos = y * skipB + x;
                int cPos = y * skipC + x;
                c[cPos] = cwei * c[cPos] + awei * a[aPos] + bwei * b[bPos];
            });
        }
        public unsafe static void MaxoutPooling(float* input, int batchSize, int dim, int* maxpoolingIndex, int maxoutDim, int outDim, float* output, int format)
        {
            Parallel.For(0, batchSize * outDim, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, threadIdx =>
            {
                int idx = threadIdx / outDim;
                int idy = threadIdx % outDim;

                int maxIndex = -1; // 
                float max = 0; // input[maxIndex];

                if (format == 0)
                {
                    maxIndex = idx * dim + idy * maxoutDim;
                    max = input[maxIndex];

                    for (int i = 1; i < maxoutDim; i++)
                    {
                        int id = idx * dim + idy * maxoutDim + i;
                        float v = input[id];
                        if (v > max)
                        {
                            max = v;
                            maxIndex = id;
                        }
                    }
                }
                else
                {
                    maxIndex = idx * dim + idy;
                    max = input[maxIndex];

                    int id = idx * dim + idy;
                    for (int i = 1; i < maxoutDim; i++)
                    {
                        id = id + outDim;
                        float v = input[id];
                        if (v > max)
                        {
                            max = v;
                            maxIndex = id;
                        }
                    }
                }
                output[idx * outDim + idy] = max;
                maxpoolingIndex[idx * outDim + idy] = maxIndex;
            });
        }


        public unsafe static void SoftAttention_Matching(float* a, int* aMask, float* b, int* bMask, int dim, int a_func, int op, float* vec, float* c, int* cMask, int batchSize, float alpha, float beta)
        {
            Parallel.For(0, batchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, x =>
            {
                int aPos = (aMask == null ? x : aMask[x]) * dim;
                int bPos = (bMask == null ? x : bMask[x]) * dim;
                int cPos = (cMask == null ? x : cMask[x]);

                float sum = 0;

                if (op == 0) // addition.
                {
                    if (a_func == 0) //linear.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            sum += (a[aPos + i] + b[bPos + i]) * vec[i];
                        }
                    }
                    else if (a_func == 1) //tanh.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            sum += (float)Math.Tanh(a[aPos + i] + b[bPos + i]) * vec[i];
                        }
                    }
                    else if (a_func == 2)//rectified.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            float v = a[aPos + i] + b[bPos + i];
                            if (v < 0) v = 0;
                            sum += v * vec[i];
                        }
                    }
                    else if (a_func == 3)//sigmoid.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            float v = a[aPos + i] + b[bPos + i];
                            v = (float)Math.Tanh(v / 2.0f);
                            v = (v + 1.0f) / 2.0f;
                            sum += v * vec[i];
                        }
                    }
                }
                else if (op == 1)
                {
                    if (a_func == 0) //linear.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            sum += (a[aPos + i] * b[bPos + i]) * vec[i];
                        }
                    }
                    else if (a_func == 1) //tanh.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            sum += (float)Math.Tanh(a[aPos + i] * b[bPos + i]) * vec[i];
                        }
                    }
                    else if (a_func == 2)//rectified.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            float v = a[aPos + i] * b[bPos + i];
                            if (v < 0) v = 0;
                            sum += v * vec[i];
                        }
                    }
                    else if (a_func == 3)//sigmoid.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            float v = a[aPos + i] * b[bPos + i];
                            v = (float)Math.Tanh(v / 2.0f);
                            v = (v + 1.0f) / 2.0f;
                            sum += v * vec[i];
                        }
                    }
                }
                c[cPos] = alpha * c[cPos] + beta * sum;
            });
        }

        public unsafe static void SoftAttention(float* a, float* b, int dim, int a_func, int op, float* vec, float* c, int aBatchSize, int bBatchSize, float alpha, float beta)
        {
            Parallel.For(0, aBatchSize * bBatchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, xy =>
            {
                int x = xy / bBatchSize;
                int y = xy % bBatchSize;

                int aPos = x * dim;
                int bPos = y * dim;
                int cPos = x * bBatchSize + y;

                float sum = 0;

                if (op == 0) // addition.
                {
                    if (a_func == 0) //linear.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            sum += (a[aPos + i] + b[bPos + i]) * vec[i];
                        }
                    }
                    else if (a_func == 1) //tanh.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            sum += (float)Math.Tanh(a[aPos + i] + b[bPos + i]) * vec[i];
                        }
                    }
                    else if (a_func == 2)//rectified.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            float v = a[aPos + i] + b[bPos + i];
                            if (v < 0) v = 0;
                            sum += v * vec[i];
                        }
                    }
                    else if (a_func == 3)//sigmoid.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            float v = a[aPos + i] + b[bPos + i];
                            v = (float)Math.Tanh(v / 2.0f);
                            v = (v + 1.0f) / 2.0f;
                            sum += v * vec[i];
                        }
                    }
                }
                else if (op == 1)
                {
                    if (a_func == 0) //linear.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            sum += (a[aPos + i] * b[bPos + i]) * vec[i];
                        }
                    }
                    else if (a_func == 1) //tanh.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            sum += (float)Math.Tanh(a[aPos + i] * b[bPos + i]) * vec[i];
                        }
                    }
                    else if (a_func == 2)//rectified.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            float v = a[aPos + i] * b[bPos + i];
                            if (v < 0) v = 0;
                            sum += v * vec[i];
                        }
                    }
                    else if (a_func == 3)//sigmoid.
                    {
                        for (int i = 0; i < dim; i++)
                        {
                            float v = a[aPos + i] * b[bPos + i];
                            v = (float)Math.Tanh(v / 2.0f);
                            v = (v + 1.0f) / 2.0f;
                            sum += v * vec[i];
                        }
                    }
                }
                c[cPos] = alpha * c[cPos] + beta * sum;
            });
        }

        public unsafe static void SparseSgemmMask(int* AIndex, int* AFeaIndex, float* AFeaValue, float* B, float* C, int batchsize, int m, int n,
                int* aMask, int* bMask, int* cMask, float alpha, float beta, bool transA, bool transB)
        {
            if (!transA && !transB)
            {
                Parallel.For(0, batchsize * n, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, id =>
                {
                    int idx = id / n;
                    int idy = id % n;
                    int aIdx = aMask == null ? idx : aMask[idx];
                    int cIdx = cMask == null ? idx : cMask[idx];
                    int col_end = AIndex[aIdx];
                    int col_begin = aIdx == 0 ? 0 : AIndex[aIdx - 1];
                    float sum = 0;
                    for (int i = col_begin; i < col_end; ++i)
                    {
                        sum += (AFeaValue == null ? 1 : AFeaValue[i]) * B[AFeaIndex[i] * n + idy];
                    }
                    C[cIdx * n + idy] = alpha * C[cIdx * n + idy] + beta * sum;
                });
                //outputSmpIndex, outputItemIndex, outputDeriv,
                //batchsize, outputItemNum, weight, hidden_deriv, hiddenDim, wei);
                /// c = alpha A * B + beta C
                //Matrix_Multipy
                //cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, batchsize, m, &alpha, B, n, A, m, &beta, C, n);
            }
            else if (transA && !transB)
            {
                //dim3 thread_backward(256);
                //dim3 block_weight((n + 256 - 1) / 256); // (m + 32 - 1) / 32);
                //cuda_sparse_matrix_backward_weight << <block_weight, thread_backward >> >(
                //	AIndex, AFeaIndex, AFeaValue, B, C, batchsize, m, n, aMask, bMask, cMask, alpha, beta);

                Parallel.For(0, n, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, idx =>
                {
                    for (int sample = 0; sample < batchsize; ++sample)
                    {
                        int Asmp = aMask == null ? sample : aMask[sample];
                        int Bsmp = bMask == null ? sample : bMask[sample];
                        int col_end = AIndex[Asmp];
                        int col_begin = Asmp == 0 ? 0 : AIndex[Asmp - 1];
                        float sum = 0;
                        for (int i = col_begin; i < col_end; ++i)
                        {
                            int fea_idx = AFeaIndex[i];
                            C[fea_idx * n + idx] = C[fea_idx * n + idx] * alpha + (AFeaValue == null ? 1 : AFeaValue[i]) * B[Bsmp * n + idx] * beta;
                        }
                    }
                });

                /// c = alpha A * op(B) + beta C
                //cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, m, batchsize, &alpha, B, n, A, m, &beta, C, n);
            }
        }

    }
}