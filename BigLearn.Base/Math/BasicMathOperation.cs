﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BigLearn
{
    public unsafe class BasicMathOperation : IMathOperationManager
    {
        static BasicMathOperation()
        {
            int minThreads;
            int minCPThreads;
            ThreadPool.GetMinThreads(out minThreads, out minCPThreads);
            int maxThreads;
            int maxCPThreads;
            ThreadPool.GetMaxThreads(out maxThreads, out maxCPThreads);
            int availableThreads;
            int availableCPThreads;
            ThreadPool.GetAvailableThreads(out availableThreads, out availableCPThreads);

            ThreadPool.SetMaxThreads(maxThreads, maxCPThreads);
            ThreadPool.SetMinThreads(Environment.ProcessorCount, minCPThreads);
        }

        public unsafe void Square_Matrix(CudaPieceFloat a, int batchSize, int dim, CudaPieceFloat aSquare)
        {
            BasicMathlib.Square_Matrix(a.CpuPtr, batchSize, dim, aSquare.CpuPtr);
        }

        public unsafe void Inner_Product_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            BasicMathlib.Inner_Product_Matching(a.CpuPtr, b.CpuPtr, c.CpuPtr, matchIdxA.CpuPtr, matchIdxB.CpuPtr, aSize, bSize, matchSize, dimension, eps);
        }

        public unsafe void Cosine_Similarity_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, CudaPieceFloat aa, CudaPieceFloat bb, float eps)
        {
            BasicMathlib.Cosine_Similarity_Matching(a.CpuPtr, b.CpuPtr, c.CpuPtr, matchIdxA.CpuPtr, matchIdxB.CpuPtr, aSize, bSize, matchSize, dimension, aa.CpuPtr, bb.CpuPtr, eps);
        }

        public unsafe void SEQ_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat output, int inputDimension, int outputDimension, int winSize)
        {
            BasicMathlib.SEQ_Sparse_Matrix_Multiply_INTEX(data.SampleIdx.CpuPtr, data.BatchSize, data.SequenceIdx.CpuPtr, data.SentMargin.CpuPtr, data.SentSize, data.FeaIdx.CpuPtr, data.FeaValue.CpuPtr, data.ElementSize,
                                        weight.CpuPtr, output.CpuPtr, inputDimension, outputDimension, winSize);
        }

        public unsafe void Convolution_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat layerPoolingOutput, int inputDimension, int outputDimension, int winSize)
        {
            BasicMathlib.Convolution_Sparse_Matrix_Multiply_INTEX(data.SampleIdx.CpuPtr, data.BatchSize, data.SequenceIdx.CpuPtr, data.SentMargin.CpuPtr, data.SentSize, data.FeaIdx.CpuPtr, data.FeaValue.CpuPtr, data.ElementSize,
                            weight.CpuPtr, layerPoolingOutput.CpuPtr, inputDimension, outputDimension, winSize);

        }

        public unsafe void Max_Pooling(CudaPieceFloat layerPoolingOutput, SeqSparseBatchData data, CudaPieceFloat output, CudaPieceInt layerMaxPooling_Index, int outputDimension)
        {
            BasicMathlib.Max_Pooling(layerPoolingOutput.CpuPtr, data.SampleIdx.CpuPtr, data.BatchSize, output.CpuPtr, layerMaxPooling_Index.CpuPtr, outputDimension);
        }

        public unsafe void Matrix_Multipy(CudaPieceFloat input, CudaPieceFloat weight, CudaPieceFloat output, int batchsize, int inputDimension, int outputDimension, int inverse)
        {
            BasicMathlib.Matrix_Multipy(input.CpuPtr, weight.CpuPtr, output.CpuPtr, batchsize, inputDimension, outputDimension, 0, 1, inverse);
        }

        public unsafe void Matrix_Add_Tanh(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            BasicMathlib.Matrix_Add_Tanh(output.CpuPtr, bias.CpuPtr, batchsize, outputDimension);
        }

        public unsafe void Matrix_Add_Linear(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            BasicMathlib.Matrix_Add_Vector(output.CpuPtr, bias.CpuPtr, batchsize, outputDimension);
        }

        public unsafe void Matrix_Add_Rectified(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            BasicMathlib.Matrix_Rectified_Vector(output.CpuPtr, bias.CpuPtr, batchsize, outputDimension);
        }

        public unsafe void Cosine_Similarity(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int topLayerSize, float eps)
        {
            BasicMathlib.Cosine_Similarity(srcTopLayerOutput.CpuPtr,
                    tgtTopLayerOutput.CpuPtr, alpha.CpuPtr, nTrailPlus1, BATCH_SIZE, mIndex,
                    batchsize, topLayerSize, eps);
        }


        public unsafe void Deriv_Tanh(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension)
        {
            BasicMathlib.Deriv_Tanh(errorDeriv.CpuPtr, output.CpuPtr, batchsize, inputDimension);
        }

        public unsafe void Deriv_Rectified(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension)
        {
            BasicMathlib.Deriv_Rectified(errorDeriv.CpuPtr, output.CpuPtr, batchsize, inputDimension);
        }

        public unsafe void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(SeqSparseBatchData input_batch, CudaPieceFloat weightDeriv, CudaPieceFloat upperOutputErrorDeriv, int inputDimension, int outputDimension, int winSize)
        {
            BasicMathlib.SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(input_batch.SampleIdx.CpuPtr, input_batch.BatchSize, input_batch.SequenceIdx.CpuPtr, input_batch.SentMargin.CpuPtr, input_batch.SentSize, 
                    input_batch.FeaIdx.CpuPtr, input_batch.FeaValue.CpuPtr, input_batch.ElementSize, weightDeriv.CpuPtr, upperOutputErrorDeriv.CpuPtr, inputDimension, outputDimension, winSize);
        }
 
        public unsafe void Scale_Matrix(CudaPieceFloat matrix, int inputDimension, int outputDimnsion, float momentum)
        {
            BasicMathlib.Scale_Matrix(matrix.CpuPtr, inputDimension, outputDimnsion, momentum);
        }

        public void Matrix_Add(CudaPieceFloat matrix, CudaPieceFloat updates, int inputDimension, int outputDimnsion, float learning_rate)
        {
            BasicMathlib.Matrix_Add(matrix.CpuPtr, updates.CpuPtr, inputDimension, outputDimnsion, learning_rate);
        } 
        public void Zero(CudaPieceFloat matrix, int size)
        {
            //matrix.Zero(size);
            PerfCounter.Manager.Instance["Zero"].CountOperation(() => matrix.Zero(size)); 
            //    Array.Clear(matrix.MemPtr, 0, size));
        }

        //public void Matrix_Aggragate(CudaPieceFloat a, CudaPieceFloat b, int batchsize, int m)
        //{
        //    BasicMathlib.Matrix_Aggragate(a.MemPtr, b.MemPtr, batchsize, m);
        //}

        //public void Cosine_Similarity_SubSpace(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps)
        //{
        //    BasicMathlib.Cosine_Similarity_SubSpace(a.MemPtr, b.MemPtr, c.MemPtr, labelDim, BATCHSIZE, batchsize, subspaceDim, eps);
        //}

        public void SoftMax(CudaPieceFloat a, CudaPieceFloat b, int labelDim, int batchsize, float gamma)
        {
            BasicMathlib.SoftMax(a.CpuPtr, b.CpuPtr, labelDim, batchsize, gamma);
        }
 

        public void InnerProduct_Similarity(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int dimension)
        {
            BasicMathlib.InnerProduct_Similarity(a.CpuPtr, b.CpuPtr, c.CpuPtr, batchsize, dimension);
        }

        public void Deriv_InnerProduct(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
        {
            BasicMathlib.Deriv_InnerProduct(q.CpuPtr, d.CpuPtr, dcq.CpuPtr, dcd.CpuPtr, alpha.CpuPtr, act_type, batchsize, Dim, gamma, eps);
        }

        //public void Matrix_Add_OFFSET(CudaPieceFloat a, int offset_a, CudaPieceFloat b, int offset_b, int len, float mweight)
        //{
        //    BasicMathlib.Matrix_Add_OFFSET(a.CpuPtr, offset_a, b.CpuPtr, offset_b, len, mweight);
        //}

        public void Matrix_Add_Sigmoid(CudaPieceFloat a, CudaPieceFloat b, int m, int n)
        {
            BasicMathlib.Matrix_Add_Sigmoid(a.CpuPtr, b.CpuPtr, m, n);
        }

        public unsafe void Sparse_Matrix_Multiply_INTEX_Weight(CudaPieceInt outputSmpIndex, CudaPieceInt outputItemIndex, CudaPieceFloat outputDeriv, int batchsize,
                int outputItemNum, CudaPieceFloat weight, CudaPieceFloat hidden_deriv, int hiddenDim, float wei)
        {
            BasicMathlib.Sparse_matrix_multiply_INTEX_weight(outputSmpIndex.CpuPtr, outputItemIndex.CpuPtr, outputDeriv.CpuPtr, batchsize,
                    outputItemNum, weight.CpuPtr, hidden_deriv.CpuPtr, hiddenDim, wei);
        }

        //public void SEQ_Sparse_Matrix_Multiply_MASK_INTEX(SeqSparseBatchData data, CudaPieceInt mask, CudaPieceFloat weight, CudaPieceFloat output, int inputDimension, int outputDimension, int winSize)
        //{
        //    throw new NotImplementedException();
        //}
        //public void SEQ_Sparse_Matrix_Decode_INTEX(int elementsize, CudaPieceInt fea_Margin, CudaPieceInt seg_Margin, CudaPieceInt Fea_Index,
        //                                        int Feature_dimension, CudaPieceFloat mul_weight, CudaPieceFloat Fea_Value_Decode,
        //                                         CudaPieceFloat input, int input_dimension)
        //{
        //    throw new NotImplementedException();
        //}


        public void RecursiveForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqInputMatrixs, int dim, CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqOutputMatrix)
        {
            PerfCounter.Manager.Instance["RecursiveForwardPropagateBatch"].CountOperation(() =>
            BasicMathlib.RecursiveForwardPropagateBatch(InstanceIndex.CpuPtr, batchSize, SeqInputMatrixs.CpuPtr,
                                        dim, WMatrix.CpuPtr, lag, af, SeqOutputMatrix.CpuPtr));
        }


        public void RecursiveBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqDerivMatrixs)
        {
            PerfCounter.Manager.Instance["RecursiveBackwardPropagateBatch"].CountOperation(() =>
                BasicMathlib.RecursiveBackwardPropagateBatch(InstanceIndex.CpuPtr, batchSize, SeqOutputMatrixs.CpuPtr, dim, WMatrix.CpuPtr, lag, af, SeqDerivMatrixs.CpuPtr));
        }

        public void MatrixMultiplicationTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
        {
            PerfCounter.Manager.Instance["MatrixMultiplicationTranspose"].CountOperation(() =>
                BasicMathlib.MatrixMultiplicationTranspose(pLeft.CpuPtr, pRight.CpuPtr, pDst.CpuPtr, leftRowCnt, rightColumnCnt, rightRowCnt, weight));
        }

        public unsafe void MatrixMultiplicationLeftTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            PerfCounter.Manager.Instance["MatrixMultiplicationLeftTranspose"].CountOperation(() =>
                BasicMathlib.MatrixMultiplicationLeftTranspose(pLeft.CpuPtr, pRight.CpuPtr, pDst.CpuPtr, leftRowCnt, leftColumnCnt, rightColumnCnt, 1, wei));
        }


        public void MatrixMultiplicationSparseLeftTranspose(CudaPieceInt pRowIndex, int RowNum, CudaPieceInt pColumnIndex, CudaPieceFloat pValue, int elementsize,
            CudaPieceFloat pRight, CudaPieceFloat pDst, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            PerfCounter.Manager.Instance["MatrixMultiplicationSparseLeftTranspose"].CountOperation(() =>
            BasicMathlib.MatrixMultiplicationSparseLeftTranspose(pRowIndex.CpuPtr, RowNum, pColumnIndex.CpuPtr, pValue.CpuPtr, elementsize,
                pRight.CpuPtr, pDst.CpuPtr, leftColumnCnt, rightColumnCnt, wei));
        }

        public void ColumnWiseSum(CudaPieceFloat matrix, CudaPieceFloat result, int row, int col, float wei)
        {
            PerfCounter.Manager.Instance["ColumnWiseSum"].CountOperation(() =>
                BasicMathlib.ColumnWiseSum(matrix.CpuPtr, result.CpuPtr, row, col, wei));
        }


        public void RecursiveUpdateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat SeqDerivMatrixs, int lag, CudaPieceFloat gradient, float weight)
        {
            PerfCounter.Manager.Instance["RecursiveUpdateBatch"].CountOperation(() =>
                BasicMathlib.RecursiveUpdateBatch(InstanceIndex.CpuPtr, batchSize, seqSize, SeqOutputMatrixs.CpuPtr, dim, SeqDerivMatrixs.CpuPtr, lag, gradient.CpuPtr, weight));
        }

        public void DerivCrossEntropy(CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, int outputSize)
        {
            PerfCounter.Manager.Instance["DerivCrossEntropy"].CountOperation(() =>
                BasicMathlib.DerivCrossEntropy(outputScore.CpuPtr, outputLabel.CpuPtr, outputDeriv.CpuPtr, outputSize));
        }


        public void ClipVector(CudaPieceFloat gpu_floats_a, int m, float maxThreshold, float minThreshold)
        {
            PerfCounter.Manager.Instance["ClipVector"].CountOperation(() =>
                BasicMathlib.ClipVector(gpu_floats_a.CpuPtr, m, maxThreshold, minThreshold));
        }


        public void Add_Vector(CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei)
        {
            PerfCounter.Manager.Instance["Add_Vector"].CountOperation(() =>
                BasicMathlib.Add_Vector(a.CpuPtr, b.CpuPtr, a.CpuPtr, m, awei, bwei));
        }


        public void Ada_Gradient(CudaPieceFloat ada, CudaPieceFloat grad, int m)
        {
            PerfCounter.Manager.Instance["Ada_Gradient"].CountOperation(() => BasicMathlib.Ada_Gradient(ada.CpuPtr, grad.CpuPtr, m));
        }


        public void Convolution_Sparse_Matrix_Product_INTEX_Weight(CudaPieceFloat deriv, CudaPieceInt maxpooling_index, CudaPieceInt Seg_Index, CudaPieceInt SegMargin_Index, int seg_size, int win_size, int batchsize, int output_dimension, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, CudaPieceFloat grad, int Feature_Dimension, float learnRate)
        {
            PerfCounter.Manager.Instance["Convolution_Sparse_Matrix_Product_INTEX_Weight"].CountOperation(() =>
            BasicMathlib.Convolution_Sparse_Matrix_Product_INTEX_Weight(deriv.CpuPtr, maxpooling_index.CpuPtr, Seg_Index.CpuPtr, SegMargin_Index.CpuPtr, seg_size, win_size, batchsize, output_dimension, Fea_Index.CpuPtr, Fea_Value.CpuPtr, grad.CpuPtr, Feature_Dimension, learnRate));
        }


        public void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, float weight)
        {
            BasicMathlib.SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(Smp_Index.CpuPtr, batchsize, Seg_Index.CpuPtr, seg_size, Fea_Index.CpuPtr, Fea_Value.CpuPtr, elementsize, mul_weight.CpuPtr, output.CpuPtr, Feature_dimension, output_dimension, weight);
        }


        public void SEQ_Sparse_Matrix_Multiply_INT(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension)
        {
            BasicMathlib.SEQ_Sparse_Matrix_Multiply_INT(Smp_Index.CpuPtr, batchsize, Seg_Index.CpuPtr, seg_size, Fea_Index.CpuPtr, Fea_Value.CpuPtr, elementsize, mul_weight.CpuPtr, output.CpuPtr, Feature_dimension, output_dimension);
        }


        //public void Matrix_Product_Weight(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int m, int n, float weight)
        //{
        //    BasicMathlib.Matrix_Product_Weight(a.CpuPtr, b.CpuPtr, c.CpuPtr, batchsize, m, n, weight);
        //}
        //public void Inner_Product_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps)
        //{
        //    BasicMathlib.Inner_Product_EX_Full(a.CpuPtr, b.CpuPtr, neg_list.CpuPtr, c.CpuPtr, nTrial, BATCHSIZE, batchsize, dimension, eps);
        //}

        public void LSTMForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c,
            CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo)
        {
            BasicMathlib.LSTMForwardPropagateBatch(InstanceIndex.CpuPtr, batchSize, o.CpuPtr, gate_i.CpuPtr, c_hat.CpuPtr, gate_f.CpuPtr, c.CpuPtr, gate_o.CpuPtr, tanhc.CpuPtr, cell, Ui.CpuPtr, Uc.CpuPtr, Uf.CpuPtr, Uo.CpuPtr, Vo.CpuPtr);
        }

        public void LSTMBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell,
           CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc,
           CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat, CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc,
           CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo)
        {
            BasicMathlib.LSTMBackwardPropagateBatch(InstanceIndex.CpuPtr, batchSize, seqSize, cell, o.CpuPtr, gate_i.CpuPtr, c_hat.CpuPtr, gate_f.CpuPtr, c.CpuPtr, gate_o.CpuPtr, tanhc.CpuPtr,
                deriv_o.CpuPtr, deriv_gate_i.CpuPtr, deriv_cHat.CpuPtr, deriv_gate_f.CpuPtr, deriv_c.CpuPtr, deriv_gate_o.CpuPtr, deriv_tanhc.CpuPtr, Ui.CpuPtr, Uc.CpuPtr, Uf.CpuPtr, Uo.CpuPtr, Vo.CpuPtr);
        }

        public void RNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOut)
        {
            BasicMathlib.RNNLabelOutput(smpIdx.CpuPtr, batchSize, seqInput.CpuPtr, seqSize, Wmatrix.CpuPtr, lag, feaDim, hiddenDim, seqOut.CpuPtr);
        }

        public void DerivRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInputDeriv, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv)
        {
            BasicMathlib.DerivRNNLabelOutput(smpIdx.CpuPtr, batchSize, seqInputDeriv.CpuPtr, seqSize, Wmatrix.CpuPtr, lag, feaDim, hiddenDim, seqOutDeriv.CpuPtr);
        }

        public void UpdateRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv, float wei)
        {
            BasicMathlib.UpdateRNNLabeloutput(smpIdx.CpuPtr, batchSize, seqInput.CpuPtr, seqSize, Wmatrix.CpuPtr, lag, feaDim, hiddenDim, seqOutDeriv.CpuPtr, wei);
        }

        public void LastStepExtractSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix)
        {
            BasicMathlib.LastTimeStepExtractSeqMatrix(InstanceIndex.CpuPtr, batchSize, SeqMatrixs.CpuPtr, dim, matrix.CpuPtr);
        }


        public void Calculate_SampleCrossEntropyDeriv(CudaPieceFloat output, CudaPieceFloat deriv, int dim, CudaPieceFloat label, int batchsize, float gamma)
        {
            BasicMathlib.Calculate_SampleCrossEntropyDeriv(output.CpuPtr, deriv.CpuPtr, dim, label.CpuPtr, batchsize, gamma);
        }


        public unsafe void ElementwiseProduct(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int row, int column, float weiDst)
        {
            BasicMathlib.ElementwiseProduct(pLeft.CpuPtr, pRight.CpuPtr, pDst.CpuPtr, row * column, weiDst);
        }

        public void GRUForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim, //->x;
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh)
        {
            BasicMathlib.GRUForwardPropagateBatch(InstanceIndex.CpuPtr, batchSize, Dim, H.CpuPtr, GateR.CpuPtr, GateZ.CpuPtr, HHat.CpuPtr, RestH.CpuPtr, Ur.CpuPtr, Uz.CpuPtr, Uh.CpuPtr);
        }

        public void GRUBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim,//->x
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH, //H @ R
            CudaPieceFloat DerivH, CudaPieceFloat DerivGateR, CudaPieceFloat DerivGateZ, CudaPieceFloat DerivHHat, CudaPieceFloat DerivRestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh)
        {
            BasicMathlib.GRUBackwardPropagateBatch(InstanceIndex.CpuPtr, batchSize, Dim, H.CpuPtr, GateR.CpuPtr, GateZ.CpuPtr, HHat.CpuPtr, RestH.CpuPtr,
                DerivH.CpuPtr, DerivGateR.CpuPtr, DerivGateZ.CpuPtr, DerivHHat.CpuPtr, DerivRestH.CpuPtr, Ur.CpuPtr, Uz.CpuPtr, Uh.CpuPtr);
        }


        public void Deriv_CosineSimilarity_Matching(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat qSquare, CudaPieceFloat dSquare, int dim, CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize, CudaPieceFloat simi, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps)
        {
            BasicMathlib.Deriv_CosineSimilarity_Matching(q.CpuPtr, d.CpuPtr, qSquare.CpuPtr, dSquare.CpuPtr, dim,
                src2MatchIdx.CpuPtr, src2MatchElement.CpuPtr, tgt2MatchIdx.CpuPtr, tgt2MatchElement.CpuPtr, srcIdx.CpuPtr,
                tgtIdx.CpuPtr, srcSize, tgtSize, matchSize, simi.CpuPtr, derivSimi.CpuPtr, dcq.CpuPtr, dcd.CpuPtr, eps);
        }


        public void Deriv_InnerProduct_Matching(CudaPieceFloat q, CudaPieceFloat d, int dim, CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps)
        {
            BasicMathlib.Deriv_InnerProduct_Matching(q.CpuPtr, d.CpuPtr, dim, src2MatchIdx.CpuPtr, src2MatchElement.CpuPtr, tgt2MatchIdx.CpuPtr, tgt2MatchElement.CpuPtr,
                    srcIdx.CpuPtr, tgtIdx.CpuPtr, srcSize, tgtSize, matchSize, derivSimi.CpuPtr, dcq.CpuPtr, dcd.CpuPtr, eps);
        }

        public void LSTMForwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo, CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag)
        {
            BasicMathlib.LSTMForwardPropagateBatch(InstanceIndex.CpuPtr, batchSize, o.CpuPtr, gate_i.CpuPtr, c_hat.CpuPtr, gate_f.CpuPtr, c.CpuPtr, gate_o.CpuPtr, tanhc.CpuPtr, cell, Ui.CpuPtr, Uc.CpuPtr, Uf.CpuPtr, Uo.CpuPtr, Vo.CpuPtr);
        }

        public void LSTMBackwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc, CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat, CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo, CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag)
        {
            BasicMathlib.LSTMBackwardPropagateBatch(InstanceIndex.CpuPtr, batchSize, seqSize, cell, o.CpuPtr, gate_i.CpuPtr, c_hat.CpuPtr, gate_f.CpuPtr, c.CpuPtr, gate_o.CpuPtr, tanhc.CpuPtr,
                deriv_o.CpuPtr, deriv_gate_i.CpuPtr, deriv_cHat.CpuPtr, deriv_gate_f.CpuPtr, deriv_c.CpuPtr, deriv_gate_o.CpuPtr, deriv_tanhc.CpuPtr, Ui.CpuPtr, Uc.CpuPtr, Uf.CpuPtr, Uo.CpuPtr, Vo.CpuPtr);
        }

        public float L2Norm(CudaPieceFloat x, int size)
        {
            return BasicMathlib.L2Norm(x.CpuPtr, size);
        }

        public void RMSPropGradient(CudaPieceFloat ada, CudaPieceFloat grad, float gamma, float epsilon, int m)
        {
            BasicMathlib.RMSPropGradient(ada.CpuPtr, grad.CpuPtr, gamma, epsilon, m);
        }

        /// <summary>
        /// Z=wx*X + wy*Y^2
        /// </summary>
        /// <param name="Z"></param>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="wx"></param>
        /// <param name="wy"></param>
        /// <param name="m"></param>
        public void VectorSquareAdd(CudaPieceFloat Z, CudaPieceFloat X, CudaPieceFloat Y, float wx, float wy, int m)
        {
            BasicMathlib.VectorSquareAdd(Z.CpuPtr, X.CpuPtr, Y.CpuPtr, wx, wy, m);
        }

        public void AdaMGradient(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
        {
            BasicMathlib.AdaMGradient(M.CpuPtr, V.CpuPtr, G.CpuPtr, alpha, beta1, beta2, epsilon, t, size);
        }

        public void AdaDeltaGradient(CudaPieceFloat deltaX, CudaPieceFloat AccumGrad, CudaPieceFloat AccumUpdate, CudaPieceFloat Grad, float epsilon, int size)
        {
            BasicMathlib.AdaDeltaGradient(deltaX.CpuPtr, AccumGrad.CpuPtr, AccumUpdate.CpuPtr, Grad.CpuPtr, epsilon, size);
        }

        public void AdaMax(CudaPieceFloat V, CudaPieceFloat M, CudaPieceFloat G, float alpha, float beta, float epsilon, int size)
        {
            BasicMathlib.AdaMax(V.CpuPtr, M.CpuPtr, G.CpuPtr, alpha, beta, epsilon, size);
        }


        public unsafe void Max_Pooling(CudaPieceFloat pooling_feas, CudaPieceInt Smp_Index, int batchsize, CudaPieceFloat output, CudaPieceInt maxpooling_index, int output_dimension)
        {
            BasicMathlib.Max_Pooling(pooling_feas.CpuPtr, Smp_Index.CpuPtr, batchsize, output.CpuPtr, maxpooling_index.CpuPtr, output_dimension);
        }

        public void Tanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize)
        {
            for (int i = 0; i < batchsize; i++)
            {
                b[offsetB + i] = (float)(Math.Tanh(a[offsetA + i]));
            }
            //FastVector.Tanh(FastVector.Create(a.CpuPtr, offsetA + a.Offset, batchsize, false), FastVector.Create(b.CpuPtr, offsetB + b.Offset, batchsize, false), batchsize);
        }

        public void Logistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize, float gamma)
        {
            for (int i = 0; i < batchsize; i++)
            {
                b[offsetB + i] = (float)(Math.Tanh(a[offsetA + i] * gamma / 2) + 1) / 2.0f;
            }
        }

        public void ReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize)
        {
            for (int i = 0; i < batchsize; i++)
            {
                b[offsetB + i] = Math.Min(a[offsetA + i], 0);
            }
        }


        public void ElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, int batchSize, int dim, float alpha)
        {
            for (int i = 0; i < batchSize * dim; i++)
            {
                pDst[i + offsetDst] = alpha * pDst[i + offsetDst] + pLeft[i + offsetLeft] * pRight[i + offsetRight];
            }
        }


        public void ElementwiseProductMask(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst,
            CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float alpha, float beta)
        {
            for (int i = 0; i < row; i++)
            {
                int leftidx = leftmask.IsEmpty ? i : leftmask[offsetlm + i];
                int rightidx = rightmask.IsEmpty ? i : rightmask[offsetrm + i];
                int dstidx = dstmask.IsEmpty ? i : dstmask[offsetdm + i];

                for (int c = 0; c < column; c++)
                    pDst[offsetDst + dstidx * column + c] =
                        alpha * pDst[offsetDst + dstidx * column + c] +
                        beta * pLeft[offsetLeft + leftidx * column + c] * pRight[offsetRight + rightidx * column + c];
            }
        }


        public unsafe void Sgemm(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int rowA, int inDim, int outDim, float alpha, float beta, bool transA, bool transB)
        {
            if (!transA && !transB)
            {
                BasicMathlib.Matrix_Multipy(A.CpuPtr + offsetA, B.CpuPtr + offsetB, C.CpuPtr + offsetC, rowA, inDim, outDim, alpha, beta, 0);
                //FastVector.MatrixMulRowMajor(FastVector.Create(A.CpuPtr, offsetA + A.Offset, rowA * inDim, false),
                //                             FastVector.Create(B.CpuPtr, offsetB + B.Offset, inDim * outDim, false),
                //                             FastVector.Create(C.CpuPtr, offsetC + C.Offset, rowA * outDim, false),
                //                             rowA, inDim, outDim, false, false, alpha, beta);
            }
            else if (!transA && transB)
            {
                BasicMathlib.Matrix_Multipy(A.CpuPtr + offsetA, B.CpuPtr + offsetB, C.CpuPtr + offsetC, rowA, inDim, outDim, alpha, beta, 1);
                //FastVector.MatrixMulRowMajor(FastVector.Create(A.CpuPtr, offsetA + A.Offset, rowA * inDim, false),
                //                             FastVector.Create(B.CpuPtr, offsetB + B.Offset, outDim * inDim, false),
                //                             FastVector.Create(C.CpuPtr, offsetC + C.Offset, rowA * outDim, false),
                //                             rowA, inDim, outDim, false, true, alpha, beta);
            }
            else if (transA && !transB)
            {
                BasicMathlib.MatrixMultiplicationLeftTranspose(A.CpuPtr + offsetA, B.CpuPtr + offsetB, C.CpuPtr + offsetC, rowA, inDim, outDim, alpha, beta);
                //FastVector.MatrixMulRowMajor(FastVector.Create(A.CpuPtr, offsetA + A.Offset, rowA * inDim, false),
                //                             FastVector.Create(B.CpuPtr, offsetB + A.Offset, rowA * outDim, false),
                //                             FastVector.Create(C.CpuPtr, offsetC + A.Offset, inDim * outDim, false), inDim, rowA, outDim, true, false, alpha, beta);
            }
        }


        public void SgemmMask(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int batchsize, int m, int n, CudaPieceInt aMask, int offsetAMask, CudaPieceInt bMask, int offsetBMask, CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB)
        {
            throw new NotImplementedException();
        }


        public unsafe void SparseSgemmMask(CudaPieceInt AIndex, CudaPieceInt AFeaIndex, CudaPieceFloat AFeaValue, CudaPieceFloat B, int offsetB,
            CudaPieceFloat C, int offsetC, int batchsize, int m, int n,
            CudaPieceInt aMask, int offsetAMask, 
            CudaPieceInt bMask, int offsetBMask, 
            CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB)
        {
            BasicMathlib.SparseSgemmMask(AIndex.CpuPtr, AFeaIndex.CpuPtr, AFeaValue.CpuPtr, B.CpuPtr + offsetB, C.CpuPtr + offsetC, batchsize, m, n,
                aMask.CpuPtr + offsetAMask, bMask.CpuPtr + offsetBMask, cMask.CpuPtr + offsetCMask, alpha, beta, transA, transB);
        }


        public void DerivTanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            for (int i = 0; i < batchsize; i++)
            {
                c[offsetC + i] = c[offsetC + i] * alpha +
                    b[offsetB + i] * (1 + a[offsetA + i]) * (1 - a[offsetA + i]) * beta;
            }
        }

        public void DerivLogistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            for (int i = 0; i < batchsize; i++)
            {
                c[offsetC + i] = c[offsetC + i] * alpha +
                    b[offsetB + i] * (a[offsetA + i]) * (1 - a[offsetA + i]) * beta;
            }
        }

        public void DerivReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            throw new NotImplementedException();
        }


        public void MaxPoolingMask(CudaPieceFloat poolingValues, CudaPieceInt smpIdx, CudaPieceInt smpMask, int batchsize, CudaPieceFloat output, CudaPieceInt maxpoolingIndex, int output_dimension)
        {
            if (smpMask.IsEmpty)
                Max_Pooling(poolingValues, smpIdx, batchsize, output, maxpoolingIndex, output_dimension);
            else
                BasicMathlib.Max_Pooling_Mask(poolingValues.CpuPtr, smpIdx.CpuPtr, batchsize, smpMask.CpuPtr, output.CpuPtr, maxpoolingIndex.CpuPtr, output_dimension);

            //int[] seqIndex = smpMask == null ? null : smpMask.CpuPtr;
            //int[] smpIndex = smpIdx.CpuPtr;
            //float[] values = poolingValues.CpuPtr;
            //float[] outValues = output.CpuPtr;
            //int[] outIdx = maxpoolingIndex.CpuPtr;
            //for (int i = 0; i < batchsize; i++)
            //{
            //    int colStart = i == 0 ? 0 : smpIndex[i-1];
            //    int colEnd = smpIndex[i];
            //    for (int j = 0; j < output_dimension; j++)
            //    {
            //        float maxValue = 0;
            //        int maxIdx = -1;
            //        if (colStart < colEnd)
            //        {
            //            maxValue = values[i * output_dimension + j];
            //            maxIdx = seqIndex == null ? colStart : seqIndex[colStart];
            //        }

            //        for (int k = colStart + 1; k < colEnd; k++)
            //        {
            //            int dim = seqIndex == null ? k : seqIndex[k];
            //            float idv = values[dim*output_dimension + j];

            //            if (idv > maxValue)
            //            {
            //                maxIdx = dim;
            //                maxValue = idv;
            //            }
            //        }

            //        outValues[i * output_dimension + j] = maxValue;
            //        outIdx[i * output_dimension + j] = maxIdx;
            //    }
            //}
        }

        public void DerivMaxPooling(CudaPieceFloat deriv, CudaPieceInt maxpoolingIndex, CudaPieceFloat poolingDeriv, int batchsize, int output_dimension, float alpha, float beta)
        {
            BasicMathlib.Deriv_Max_Pooling(deriv.CpuPtr, maxpoolingIndex.CpuPtr, poolingDeriv.CpuPtr, batchsize, output_dimension, alpha, beta);
        }


        public void LastStepExtractSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, CudaPieceFloat matrixMask, int maskOffset)
        {
            throw new NotImplementedException();
        }

        public void DerivLastStepSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, float alpha)
        {
            throw new NotImplementedException();
        }

        public void DerivLastStepSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, CudaPieceFloat matrixMask, int maskOffset, float alpha)
        {
            throw new NotImplementedException();
        }


        public float LogLossDeriv(CudaPieceFloat outputProb, int dim, int batchSize, CudaPieceFloat smpProb, CudaPieceFloat label, CudaPieceFloat labelwei, CudaPieceInt labelMask, float gamma, float eps)
        {
            throw new NotImplementedException();
        }


        public void Add_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int m, float awei, float bwei)
        {
            //Add_Vector(float * a, int a_idx, float * b, int b_idx,  int size, float a_wei, float b_wei)
            FastVector.Add_Vector(a.CpuPtr, offsetA, b.CpuPtr, offsetB, m, awei, bwei);

            //throw new NotImplementedException();
            //for (int i = 0; i < m; i++)
            //{
            //    a.CpuPtr[offsetA + i] = awei * a.CpuPtr[offsetA + i] + bwei * b.CpuPtr[offsetB + i];
            //}
        }


        public unsafe void Matrix_AdditionMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAMask, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBMask, CudaPieceFloat c, int offsetC, CudaPieceInt cMask, int offsetCMask, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionMask(a.CpuPtr + offsetA, aMask.CpuPtr + offsetAMask, b.CpuPtr + offsetB, bMask.CpuPtr + offsetBMask, c.CpuPtr + offsetC, cMask.CpuPtr + offsetCMask, dim, batchSize, awei, bwei, cwei);

            //Enumerable.Range(0, batchSize).AsParallel().ForAll(i =>
            //{
            //    int aIdx = aMask.IsEmpty ? i : aMask.CpuPtr[offsetAMask + i];
            //    int bIdx = bMask.IsEmpty ? i : bMask.CpuPtr[offsetBMask + i];
            //    int cIdx = cMask.IsEmpty ? i : cMask.CpuPtr[offsetCMask + i];

            //    for (int d = 0; d < dim; d++)
            //    {
            //        c.CpuPtr[offsetC + cIdx * dim + d] =
            //               cwei * c.CpuPtr[offsetC + cIdx * dim + d] +
            //               awei * a.CpuPtr[offsetA + aIdx * dim + d] +
            //               bwei * b.CpuPtr[offsetB + bIdx * dim + d];
            //    }
            //});
        }


        public unsafe void SparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputScore, CudaPieceFloat outputProb, float gamma, int batchSize)
        {
            BasicMathlib.SparseSoftmax(smpIdx.CpuPtr, outputScore.CpuPtr, outputProb.CpuPtr, gamma, batchSize);
            
        }


        public void DerivSparseMultiClassSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat probDeriv, CudaPieceFloat outputDeriv, float gamma, int batchSize, float alpha = 0, float beta = 1)
        {
            throw new NotImplementedException();
        }

        public unsafe void ColumnWiseSumMask(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM, CudaPieceFloat weight, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM, int row, int col, float alpha, float beta)
        {
            BasicMathlib.ColumnWiseSumMask(matrix.CpuPtr + offsetM, matrixMask.CpuPtr + offsetMM, weight.CpuPtr, smpIdx.CpuPtr, batchSize, output.CpuPtr + offsetO, outputMask.CpuPtr + offsetOM, row, col, alpha, beta);
            
        }

        public void Scale_MatrixMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM, int dim, int batchSize, CudaPieceFloat awei, float bwei)
        {
            throw new NotImplementedException();
        }

        public void Inner_Product_Matching(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB, int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            throw new NotImplementedException();
        }

        public void Sgemv(CudaPieceFloat matrix, CudaPieceFloat x, int row, int col, CudaPieceFloat y, bool transM, float alpha, float beta)
        {
            if (!transM)
            {
                Sgemm(matrix, 0, x, 0, y, 0, row, col, 1, alpha, beta, false, false);
            }
            else
            {
                Sgemm(x, 0, matrix, 0, y, 0, 1, row, col, alpha, beta, false, false);
            }
        }

        public void ClipAdaGradUpdate(CudaPieceFloat ada, CudaPieceFloat grad, CudaPieceFloat param, float updateRate, float gradClip, float weightClip)
        {
            if (gradClip > 0)
            {
                ClipVector(grad, grad.Size, gradClip, -gradClip);
            }

            Ada_Gradient(ada, grad, grad.Size);
            Add_Vector(param, grad, grad.Size, 1, updateRate);
            if (weightClip > 0)
            {
                ClipVector(param, param.Size, weightClip, -weightClip);
            }
            Zero(grad, grad.Size);
        }

        public void ClipAdamUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, float beta1, float beta2, int iter, float updateRate, float gradClip, float weightClip, float decay)
        {
            // float epsilon = 1.0e-8f;
            // if (gradClip > 0)
            // {
            //     ClipVector(grad, grad.Size, gradClip, -gradClip);
            // }

            // updateRate = (float)(updateRate * Math.Sqrt(1 - Math.Pow(beta2, iter)) / (1 - Math.Pow(beta1, iter)));

            // //M = Beta1 * M + ( 1 - Beta1 ) * g
            // Add_Vector(M, grad, param.Size, beta1, 1 - beta1);
            // //V = beta2 * V + (1 - Beta2) * g ^ 2
            // VectorSquareAdd(V, V, grad, beta2, 1 - beta2, param.Size);
            // //MHat = M/(1 - Beta1 ^ Count)
            // //Vhat = V/(1 - Beta2 ^ Count)
            // //Gradient = Alpha * MHat/(Sqrt(VHat) + elpsion)
            // AdaMGradient(M, V, grad, updateRate, beta1, beta2, epsilon, iter, param.Size);
            // //Parameter += Gradient
            // AddAndClear(param, grad, param.Size, 1);
            // if (weightClip > 0)
            // {
            //     ClipVector(param, param.Size, weightClip, -weightClip);
            // }
            throw new NotImplementedException();
        }

        public void LogBayesianRatingLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss, CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, uint outputSize, float eplison, float gamma)
        {
            throw new NotImplementedException();
        }


        public void AddAndClear(CudaPieceFloat a, CudaPieceFloat b, int m, float bwei)
        {
            BasicMathlib.AddAndClear(a.CpuPtr, b.CpuPtr, bwei, m);
        }

        public unsafe void GetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.GetSeqOrderMatrixs(InstanceIndex.CpuPtr, batchSize, SeqMatrixs.CpuPtr, MapForward.CpuPtr, dim, isReverse, order, matrix.CpuPtr, alpha, beta);
        }

        public void SetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void GetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.GetWindowMatrixs(InstanceMargin.CpuPtr, sentsize, SeqMatrixs.CpuPtr, mapForward.CpuPtr, dim, winsize, matrix.CpuPtr, alpha, beta);
        }

        public void SetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta)
        {
            BasicMathlib.SetWindowMatrixs(InstanceMargin.CpuPtr, sentsize, SeqMatrixs.CpuPtr, mapForward.CpuPtr, dim, winsize, matrix.CpuPtr, alpha, beta);
        }

        public void DerivSoftmax(CudaPieceFloat softmaxData, CudaPieceFloat softmaxDeriv, CudaPieceFloat gradData, int dim, int batchsize, float alpha)
        {
            throw new NotImplementedException();
        }

        public unsafe void MaxoutPooling(CudaPieceFloat input, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output, int format)
        {
            BasicMathlib.MaxoutPooling(input.CpuPtr, batchSize, dim, maxpoolingIndex.CpuPtr, maxoutDim, outDim, output.CpuPtr, format);
        }

        public void DerivMaxoutPooling(CudaPieceFloat input_deriv, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output_deriv)
        {
            throw new NotImplementedException();
        }

        public void CNNForwardPropagate(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, 
                        int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatch, int outHeight, int outWidth)
        {
            throw new NotImplementedException();
        }

        public void CNNForwardPropagateEx(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, 
                        int filterHeight, int filterWidth, int padHeight, int padWidth, int strideHeight, int strideWidth, 
                        CudaPieceFloat imageOutBatch, int outHeight, int outWidth, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void MomentumUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, float momentumRate)
        {
            BasicMathlib.Add_Vector(weight.CpuPtr, momentum.CpuPtr, weight.CpuPtr, weight.EffectiveSize, 1, 1);
            BasicMathlib.Scale_Matrix(momentum.CpuPtr, 1, momentum.EffectiveSize, momentumRate);
        }

        public void NAGUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, CudaPieceFloat trueWeight, float momentumRate)
        {
            BasicMathlib.Add_Vector(trueWeight.CpuPtr, momentum.CpuPtr, trueWeight.CpuPtr, weight.EffectiveSize, 1, 1);
            BasicMathlib.Scale_Matrix(momentum.CpuPtr, 1, momentum.EffectiveSize, momentumRate);
            BasicMathlib.Add_Vector(trueWeight.CpuPtr, momentum.CpuPtr, weight.CpuPtr, weight.EffectiveSize, 1, 1);
        }

        public void BayesianRatingProb(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss, CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputProb, int outputSize, float eplison, float gamma)
        {
            throw new NotImplementedException();
        }

        public void MatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score)
        {
            throw new NotImplementedException();
        }

        public void MatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score)
        {
            throw new NotImplementedException();
        }

        public void DerivMatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score, CudaPieceFloat l1ScoreDeriv, CudaPieceFloat xDeriv)
        {
            throw new NotImplementedException();
        }

        public void DerivMatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score, CudaPieceFloat l2ScoreDeriv, CudaPieceFloat xDeriv)
        {
            throw new NotImplementedException();
        }

        public void Matrix_Mask(CudaPieceFloat output, CudaPieceInt dropoutMask, int batchSize, int dim)
        {
            BasicMathlib.Matrix_Mask(output.CpuPtr, dropoutMask.CpuPtr, batchSize, dim);
        }

        public void SpanMaxPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, CudaPieceInt outMaxIndex, int dim)
        {
            throw new NotImplementedException();
        }

        public void DerivSpanMaxPool(CudaPieceFloat outputSentDeriv, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt outMaxIndex, int dim, float beta)
        {
            throw new NotImplementedException();
        }

        public void SpanLastPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, int dim)
        {
            throw new NotImplementedException();
        }

        public void DerivSpanLastPool(CudaPieceFloat outputSentDeriv, CudaPieceInt outSmpIdx, CudaPieceFloat start, CudaPieceFloat end, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt inSmpIdx, int dim, float beta)
        {
            throw new NotImplementedException();
        }

        public void AccurateScale_Matrix(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM, int dim, int batchSize, CudaPieceFloat awei)
        {
            throw new NotImplementedException();
        }


        public void AccurateElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float beta)
        {
            throw new NotImplementedException();
        }

        public void Convolution_Sparse_Matrix_Multiply_INTEX(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, CudaPieceInt Seg_Margin, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat con_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, int win_size)
        {
            throw new NotImplementedException();
        }


        public unsafe void Matrix_AdditionEx(CudaPieceFloat a, int offsetA, int skipA, CudaPieceFloat b, int offsetB, int skipB, CudaPieceFloat c, int offsetC, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionEx(a.CpuPtr + offsetA, skipA, b.CpuPtr + offsetB, skipB, c.CpuPtr + offsetC, skipC, dim, batchSize, awei, bwei, cwei);
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            //{
            //    int x = id % dim;
            //    int y = id / dim;
            //    int aPos = y * skipA + x;
            //    int bPos = y * skipB + x;
            //    int cPos = y * skipC + x;
            //    c.CpuPtr[c.Offset + offsetC + cPos] = cwei * c.CpuPtr[c.Offset + offsetC + cPos] + awei * a.CpuPtr[a.Offset + offsetA + aPos] + bwei * b.CpuPtr[b.Offset + offsetB + bPos];
            //});
        }

        public unsafe void Matrix_AdditionExMask(CudaPieceFloat a, CudaPieceInt aMask, int skipA, CudaPieceFloat b, CudaPieceInt bMask, int skipB, CudaPieceFloat c, CudaPieceInt cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            BasicMathlib.Matrix_AdditionExMask(a.CpuPtr, aMask.CpuPtr, skipA, b.CpuPtr, bMask.CpuPtr, skipB, c.CpuPtr, cMask.CpuPtr, skipC, dim, batchSize, awei, bwei, cwei);
            //Enumerable.Range(0, batchSize * dim).AsParallel().ForAll(id =>
            //{
            //    int x = id % dim;
            //    int y = id / dim;
            //    int ya = aMask.IsEmpty ? y : aMask.CpuPtr[y];
            //    int yb = bMask.IsEmpty ? y : bMask.CpuPtr[y];
            //    int yc = cMask.IsEmpty ? y : cMask.CpuPtr[y];

            //    int aPos = ya * skipA + x;
            //    int bPos = yb * skipB + x;
            //    int cPos = yc * skipC + x;
            //    c.CpuPtr[c.Offset + cPos] = cwei * c.CpuPtr[c.Offset + cPos] + awei * a.CpuPtr[a.Offset + aPos] + bwei * b.CpuPtr[b.Offset + bPos];
            //});
        }

        public unsafe void SoftAttention_Matching(CudaPieceFloat a, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, CudaPieceInt cMask, int batchSize, float alpha, float beta)
        {
            BasicMathlib.SoftAttention_Matching(a.CpuPtr, aMask.CpuPtr, b.CpuPtr, bMask.CpuPtr, dim, a_func, op, vec.CpuPtr, c.CpuPtr, cMask.CpuPtr, batchSize, alpha, beta);
        }

        public void DerivSoftAttention_Matching(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceFloat bDeriv, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, CudaPieceInt cMask, CudaPieceInt aSmpIdx, CudaPieceInt aElementIdx, int aSize, CudaPieceInt bSmpIdx, CudaPieceInt bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei)
        {
            throw new NotImplementedException();
        }

        public unsafe void SoftAttention(CudaPieceFloat a, CudaPieceFloat b, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, int aBatchSize, int bBatchSize, float alpha, float beta)
        {
            BasicMathlib.SoftAttention(a.CpuPtr, b.CpuPtr, dim, a_func, op, vec.CpuPtr, c.CpuPtr, aBatchSize, bBatchSize, alpha, beta);
        }

        public void DerivSoftAttention(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceFloat b, CudaPieceFloat bDeriv, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateData(CudaPieceFloat imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateDataEx(CudaPieceFloat imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth,
                            CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int padHeight, int padWidth, int strideHeight, int strideWidth,
                            CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alpha, float beta)
        {
            throw new NotImplementedException();
        }


        public void CuDNNBackwardPropagateFilter(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateFilterEx(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
                            CudaPieceFloat filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int padHeight, int padWidth, int strideHeight, int strideWidth,
                            CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            throw new NotImplementedException();
        }

        public void RMSPropV2_Gradient(CudaPieceFloat ada, CudaPieceFloat g, CudaPieceFloat grad, float gamma, float epsilon, int m)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBackwardPropagateBias(CudaPieceFloat biasDeriv, CudaPieceFloat imageOutBatchDeriv, int batch, int outDepth, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            throw new NotImplementedException();
        }

        public void NDArrayTranspose(CudaPieceFloat input, CudaPieceInt indim, CudaPieceInt transDim, CudaPieceFloat output, int dimNum, int length, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void KLargestValueBatch(CudaPieceFloat float_array, int batchSize, int dim, int K, int mode, CudaPieceFloat bestValues, CudaPieceFloat bestIndexes)
        {
            throw new NotImplementedException();
        }

        public void DerivLogSparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, CudaPieceFloat outputDeriv, float gamma, float alpha, float beta, int batchSize)
        {
            throw new NotImplementedException();
        }

        public void ColumnWiseSumMaskEx(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM, CudaPieceFloat weight, int skipMatrix, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM, int skipOutput, int row, int col, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void DerivLogProbEntropy(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, float alpha, float beta, float epislon, int batchSize)
        {
            throw new NotImplementedException();
        }

        public void LookupForward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, CudaPieceFloat weight, CudaPieceFloat output, int inputDim, int outputDim)
        {
            throw new NotImplementedException();
        }
        
        public void LookupBackward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, int inputItemNum, CudaPieceFloat deriv, int inputDim, int outputDim, CudaPieceFloat output, float lr)
        {
            throw new NotImplementedException();
        }

        public void SparseVectorAdd(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float beta, int length)
        {
            throw new NotImplementedException();
        }

        public float VectorSum(CudaPieceFloat x, int index, int len, int norm)
        {
            throw new NotImplementedException();
        }

        public void SparseVectorGivens(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float alpha, float beta, int length)
        {
            throw new NotImplementedException();
        }


        /// b = bwei b + awei * a;
        public void Scale_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei)
        {
            throw new NotImplementedException();
        }

        public void Init_Vector(CudaPieceFloat a, int offsetA, float b, int m, float awei)
        {
            throw new NotImplementedException();
        }

        public void Log(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, float alpha, float beta, int size)
        {
            throw new NotImplementedException();
        }

        public void GraphNeighborNum(CudaPieceInt srcIdx, int batchSize, CudaPieceInt graphIdx, CudaPieceInt graphNeiNodes, CudaPieceInt graphNeiRels, 
                            CudaPieceInt maskSrc, CudaPieceInt maskRel, CudaPieceInt maskInvRel, CudaPieceInt maskTgt, CudaPieceInt neiNum, int maskType)
        {
            throw new NotImplementedException();
        }

        // calculate neighbor info.
        public void GraphNeighborInfo(CudaPieceInt srcIdx, int batchSize, CudaPieceInt graphIdx, CudaPieceInt graphNeiNodes, CudaPieceInt graphNeiRels, 
                               CudaPieceInt maskSrc, CudaPieceInt maskRel, CudaPieceInt maskInvRel, CudaPieceInt maskTgt, CudaPieceInt neiNum,
                               CudaPieceInt neiNodes, CudaPieceInt neiRels, CudaPieceInt neiMargin, int defaultR, int maskType)
        {
            throw new NotImplementedException();
        }

        public void CuSparseSgemm(CudaPieceFloat A, CudaPieceInt zero_bIndex, CudaPieceInt bfeaIndex, CudaPieceFloat bfeaValue, int elementSize, 
                                  CudaPieceFloat C, int batchSize, int aDim, int bDim, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void CuCsr2Csc(CudaPieceInt aIndex, CudaPieceInt afeaIndex, CudaPieceFloat afeaValue,
                       CudaPieceInt bIndex, CudaPieceInt bfeaIndex, CudaPieceFloat bfeaValue,
                                                   int elementSize, int aRow, int aCol)
        {
            throw new NotImplementedException();
        }

        public void Inner_Product_v1(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB, int aSize, int bSize, int matchSize, int dimension, float alpha, float beta)
        {
            throw new NotImplementedException();
        }
        
        public void Maxpooling1D(CudaPieceFloat values, int row, int column, int batchsize, CudaPieceFloat output, CudaPieceInt maxpooling_index)
        {
            throw new NotImplementedException();
        }
        
        public void Meanpooling1D(CudaPieceFloat values, int row, int column, int batchsize, CudaPieceFloat output)
        {
            throw new NotImplementedException();
        }

        public void SparseGthr(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat src, int length)
        {
            throw new NotImplementedException();
        }

        public void SgemmStrideBatched(CudaPieceFloat tensorA, int rowA, int colA, CudaPieceFloat tensorB, int rowB, int colB, int batch, 
                    CudaPieceFloat tensorC, int transA, int transB, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void ElementwiseProductEx(CudaPieceFloat pLeft, int strideLeft, int leftLen, CudaPieceFloat pRight, int strideRight, int rightLen, CudaPieceFloat pDst, int strideDst, int dstLen, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void CuDNNMaxPooling2DForward(CudaPieceFloat input, int in_width, int in_height, int depth, int batch, 
                                      int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
                                      CudaPieceFloat output, int out_width, int out_height, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void CuDNNMaxPooling2DBackward(CudaPieceFloat input, CudaPieceFloat dx, int in_width, int in_height, int depth, int batch, 
                                        int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
                                        CudaPieceFloat output, CudaPieceFloat dy, int out_width, int out_height, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        /////////////// register dropout 
        public IntPtr RegisterDropout(int n, int c, int h, int w, float drop)
        {
            throw new NotImplementedException();
        }

        // dropout forward.
        public void CuDNNDropoutForward(CudaPieceFloat x, int batch, int depth, int height, int width, 
                                 CudaPieceFloat y, float drop, IntPtr state)
        {
            throw new NotImplementedException();
        }

        // dropout backward.
        public void CuDNNDropoutBackward(CudaPieceFloat dy, int batch, int depth, int height, int width, 
                                         CudaPieceFloat dx, float drop, IntPtr state)
        {
            throw new NotImplementedException();
        }
        
        // UnregisterDropout state.
        public void UnregisterDropout(IntPtr state)
        {
            throw new NotImplementedException();
        }

        // vector mul
        // c = alpah c + beta ( a \dot b );
        public void CuDNNVectorMul(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, float alpha, float beta, int length)
        {
            throw new NotImplementedException();
        }

        // vector sqrt.
        // b = alpha b + beta sqrt(a); 
        public void Sqrt(CudaPieceFloat a, CudaPieceFloat b, float alpha, float beta, int length)
        {
            throw new NotImplementedException();
        }

        public void Sger(CudaPieceFloat x, CudaPieceFloat y, CudaPieceFloat matrix, int row, int col, float beta)
        {
            throw new NotImplementedException();
        }

        public IntPtr RegisteTensorReduce(int reduceOp, int dimNum, int[] aDims, int[] cDims)
        {
            throw new NotImplementedException();
        }

        public IntPtr RegisteTensorLazyReduce(int reduceOp, int dimNum, int[] aDims, int[] cDims)
        {
            throw new NotImplementedException();
        }
        
        public void UnRegisteTensorReduce(IntPtr state)
        {
            throw new NotImplementedException();
        }

        public void TensorReduceOp(CudaPieceFloat a, CudaPieceFloat c, float alpha, float beta, IntPtr state)
        {
            throw new NotImplementedException();
        }


        public void Reciprocal(CudaPieceFloat a, CudaPieceFloat b, float alpha, float beta, int size)
        {
            throw new NotImplementedException();
        }

        public void TensorOp(int opType, int dimNum, int[] aDims, CudaPieceFloat a, 
                                                     int[] bDims, CudaPieceFloat b, 
                                                     int[] cDims, CudaPieceFloat c, 
                                                     float beta1, float beta2, float alpha)
        {
            throw new NotImplementedException();
        }


        public void Gelu(CudaPieceFloat x, CudaPieceFloat y, int size)
        {
            throw new NotImplementedException();
        }

        public void DerivGelu(CudaPieceFloat x, CudaPieceFloat y, CudaPieceFloat dy, CudaPieceFloat dx, int size)
        {
            throw new NotImplementedException();
        }


        public void NDArraySliceForward(CudaPieceFloat input, int dimNum, CudaPieceInt indim, CudaPieceFloat output, CudaPieceInt offset, CudaPieceInt outdim, int length, float alpha, float beta)
        {
            throw new NotImplementedException();
        }
        
        public void NDArraySliceBackward(CudaPieceFloat outputDeriv, int dimNum, CudaPieceInt offset, CudaPieceInt outdim, CudaPieceFloat inputDeriv, CudaPieceInt indim, int length, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void ClipAdamBertUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, 
                                float beta1, float beta2, float epsilon, float updateRate, float gradClip, float weightClip, float decay, int size)
        {
            throw new NotImplementedException();
        }

        public float DotProduct(CudaPieceFloat x, CudaPieceFloat y, int len)
        {   
            throw new NotImplementedException();
        }

        public void DerivProbEntropy(CudaPieceFloat probDeriv, CudaPieceFloat prob, float alpha, float beta, float epislon, int dim, int batchSize)
        {
            throw new NotImplementedException();
        }

        public void CuDNNMeanPooling2DForward(CudaPieceFloat input, int in_width, int in_height, int depth, int batch, 
                                      int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
                                      CudaPieceFloat output, int out_width, int out_height, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public void CuDNNMeanPooling2DBackward(CudaPieceFloat input, CudaPieceFloat dx, int in_width, int in_height, int depth, int batch, 
                                        int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
                                        CudaPieceFloat output, CudaPieceFloat dy, int out_width, int out_height, float alpha, float beta)
        {
            throw new NotImplementedException();
        }

        public int GetReduceIndiceSize(IntPtr state)
        {
            throw new NotImplementedException();
        }

        public int GetReduceWorkspaceSize(IntPtr state)
        {
            throw new NotImplementedException();
        }

        public void CudaDeallocMem(IntPtr mem)
        {
            throw new NotImplementedException();
        }

        public IntPtr CudaAllocMem(int size)
        {
            throw new NotImplementedException();
        }

        public int AssignReduceState(IntPtr state, IntPtr indicePtr, int indiceSize, IntPtr workspacePtr, int workspaceSize)
        {
            throw new NotImplementedException();
        }

        public void CuDNNBatchNormalizationForwardTraining(CudaPieceFloat x, int batch, int depth, int height, int width, CudaPieceFloat y, CudaPieceFloat bnScale, CudaPieceFloat bnBias, float momentum, float alpha, float beta, CudaPieceFloat runningMean, CudaPieceFloat runningVariance, CudaPieceFloat saveMean, CudaPieceFloat saveVariance)
        {
            throw new NotImplementedException();
        }

        // normalization forward inference.
        public void CuDNNBatchNormalizationForwardInference(CudaPieceFloat x, int batch, int depth, int height, int width, CudaPieceFloat y, CudaPieceFloat bnScale, CudaPieceFloat bnBias, float alpha, float beta, CudaPieceFloat runningMean, CudaPieceFloat runningVariance)
        {
            throw new NotImplementedException();
        }

        // normalization backward.
        public void CuDNNBatchNormalizationBackward(CudaPieceFloat x, int batch, int depth, int height, int width, CudaPieceFloat dy, CudaPieceFloat dx, CudaPieceFloat bnScale, float alpha, float beta, float param_alpha, float param_beta, CudaPieceFloat saveMean, CudaPieceFloat saveVariance, CudaPieceFloat dbnScale, CudaPieceFloat dbnBias)
        {
            throw new NotImplementedException();
        }

        public void Update_Vector(CudaPieceFloat src, CudaPieceFloat delta, int size, float decay)
        {
            throw new NotImplementedException();
        }

        public void ClipAdamDeltaUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, CudaPieceFloat orig_param,
                                float beta1, float beta2, float epsilon, float updateRate, float gradClip, float weightClip, float decay, int size)
        {
            throw new NotImplementedException();
        }

        public int GetReduceIndices(IntPtr state, CudaPieceInt indices, int length)
        {  
            throw new NotImplementedException();
        }

        public void NDArrayMaxBackward(CudaPieceFloat input, CudaPieceInt sparse_index, CudaPieceInt indim, CudaPieceFloat output, CudaPieceInt outDim, int max_dim, int dimNum, int length, float beta)
        {
            throw new NotImplementedException();
        }

        public void NDArray2SegForward(CudaPieceFloat input, int dim, int max_seq_len, int batch_size, CudaPieceInt sampleIdx, CudaPieceInt sentMargin, int sent_size, CudaPieceFloat output)
        {
            throw new NotImplementedException();
        }

        public void NDArray2SegBackward(CudaPieceFloat inputDeriv, int dim, int max_seq_len, int batch_size, CudaPieceInt sampleIdx, CudaPieceInt sentMargin, int sent_size, CudaPieceFloat outputDeriv, float beta)
        {
            throw new NotImplementedException();
        }
        
        public void GenerateRand(CudaPieceFloat x, int len)
        {
            throw new NotImplementedException();
        }

        public void Gumbel(CudaPieceFloat x, float eps, int size)
        {
            throw new NotImplementedException();
        }

        public void Exp(CudaPieceFloat a, CudaPieceFloat b, float alpha, float beta, int size)
        {
            throw new NotImplementedException();
        }

        public void CuDNNTensorArgmaxK(CudaPieceFloat input, int k, CudaPieceFloat output, CudaPieceInt local_indices, CudaPieceInt global_indices, int olength, CudaPieceInt out_dim, CudaPieceInt in_index, int max_dim, int dim_num, IntPtr state)
        {
            throw new NotImplementedException();
        }

        public void Fast_KLargestValueBatch(CudaPieceFloat float_array, int batchSize, int dim, int K, int mode, CudaPieceFloat bestValues, CudaPieceInt bestIndexes, CudaPieceFloat _bestValues, CudaPieceInt _bestIndexes)
        {
            throw new NotImplementedException();
        }

        public void NDArrayScatter(CudaPieceFloat memory, int dimNum, CudaPieceInt indim, CudaPieceFloat data, CudaPieceInt dimindex, int length, float mem_alpha, float mem_beta, float data_alpha, float data_beta)
        {
            throw new NotImplementedException();
        }

        //public void NDArraySliceForwardV2(CudaPieceFloat input, int dimNum, CudaPieceInt indim, CudaPieceFloat output, CudaPieceInt dimindex, CudaPieceInt dimoffset, CudaPieceInt outdim, int length, float alpha, float beta)
        //{
        //    throw new NotImplementedException();
        //}

        //public void NDArraySliceBackwardV2(CudaPieceFloat outputDeriv, int dimNum, CudaPieceInt indim, CudaPieceFloat inputDeriv, CudaPieceInt dimindex, CudaPieceInt dimoffset, CudaPieceInt outdim, int length, float beta)
        //{
        //    throw new NotImplementedException();
        //}

        //public void NDArrayScatterForward(CudaPieceFloat memory, int dimNum, CudaPieceInt indim, CudaPieceFloat data, CudaPieceInt dimindex, CudaPieceInt dimoffset, CudaPieceInt scatdim, int length, float beta)
        //{
        //    throw new NotImplementedException();
        //}

        //public void NDArrayScatterBackward(CudaPieceFloat memoryDeriv, int dimNum, CudaPieceInt indim, CudaPieceFloat dataDeriv, CudaPieceInt dimindex, CudaPieceInt dimoffset, CudaPieceInt scatdim, int length, float beta)
        //{
        //    throw new NotImplementedException();
        //}


    }
}
