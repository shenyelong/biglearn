using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class CudaMathOperation : IMathOperationManager
    {
        IntPtr CuBlasHandle = IntPtr.Zero;
        IntPtr CuDnnHandle = IntPtr.Zero;
        IntPtr CuSparseHandle = IntPtr.Zero;
        IntPtr CuRandHandle = IntPtr.Zero;

        public CudaMathOperation() { }

        public CudaMathOperation(bool initCuBlas, bool initCuDnn)
        {
            if (initCuBlas) CuBlasHandle = Cudalib.CudaCreateCuBlas();
            if (initCuDnn) CuDnnHandle = Cudalib.CudaCreateCuDnn();
            CuSparseHandle = Cudalib.CudaCreateCuSparse();
            CuRandHandle = Cudalib.CudaCreateCuRand(Util.URandom.Next());
        }

        void Dispose()
        {
            if (CuBlasHandle != IntPtr.Zero) Cudalib.CudaDestroyCuBlas(CuBlasHandle);
            if (CuDnnHandle != IntPtr.Zero) Cudalib.CudaDestroyCuDnn(CuDnnHandle);
            Cudalib.CudaDestroyCuSparse(CuSparseHandle);
            Cudalib.CudaDestroyCuRand(CuRandHandle);
        }

        ~CudaMathOperation()
        {
            Dispose();
        }

        public void Joint_Dense_Matrix_Multiply_INTEX(
            CudaPieceFloat Src_Input, int SrcSize,
            CudaPieceFloat Tgt_Input, int TgtSize,
            CudaPieceInt Src_Match_Idx, CudaPieceInt Tgt_Match_Idx, CudaPieceFloat Match_Score, int MatchSize,
            CudaPieceFloat ConFea_Value,
            int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
            CudaPieceFloat Weight, CudaPieceFloat Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
        {
            Cudalib.Joint_Dense_Matrix_Multiply_INTEX(Src_Input == null ? IntPtr.Zero : Src_Input.CudaPtr, SrcSize,
                                                        Tgt_Input == null ? IntPtr.Zero : Tgt_Input.CudaPtr, TgtSize, Src_Match_Idx.CudaPtr,
                    Tgt_Match_Idx.CudaPtr, Match_Score.CudaPtr, MatchSize, ConFea_Value.CudaPtr, SrcDimension, TgtDimension,
                    ConDimension, IsConScore, Weight.CudaPtr, Output.CudaPtr, OutputDimension, IsSrc, IsTgt, IsSim);
        }

        public void Joint_Sparse_Matrix_Multiply_INTEX(
            CudaPieceFloat Src_Input, int SrcSize,
            CudaPieceFloat Tgt_Input, int TgtSize,
            CudaPieceInt Src_Match_Idx, CudaPieceInt Tgt_Match_Idx, CudaPieceFloat Match_Score, int MatchSize,
            CudaPieceInt ConSmp_Idx, CudaPieceInt ConFea_Idx, CudaPieceFloat ConFea_Value,
            int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
            CudaPieceFloat Weight, CudaPieceFloat Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
        {
            Cudalib.Joint_Sparse_Matrix_Multiply_INTEX(Src_Input.CudaPtr, SrcSize, Tgt_Input.CudaPtr, TgtSize,
                Src_Match_Idx.CudaPtr, Tgt_Match_Idx.CudaPtr, Match_Score.CudaPtr, MatchSize, ConSmp_Idx.CudaPtr,
                ConFea_Idx.CudaPtr, ConFea_Value.CudaPtr, SrcDimension, TgtDimension, ConDimension, IsConScore, Weight.CudaPtr,
                Output.CudaPtr, OutputDimension, IsSrc, IsTgt, IsSim);
        }

        public void Sparse_Matrix_Multiply_INTEX_Weight(CudaPieceInt outputSmpIndex, CudaPieceInt outputItemIndex, CudaPieceFloat outputDeriv, int batchsize,
                int outputItemNum, CudaPieceFloat weight, CudaPieceFloat hidden_deriv, int hiddenDim, float wei)
        {
            Cudalib.Sparse_Matrix_Multiply_INTEX_Weight(outputSmpIndex.CudaPtr, outputItemIndex.CudaPtr, outputDeriv.CudaPtr, batchsize,
                outputItemNum, weight.CudaPtr, hidden_deriv.CudaPtr, hiddenDim, wei);
        }


        public void Square_Matrix(CudaPieceFloat a, int batchSize, int dim, CudaPieceFloat aSquare)
        {
            Cudalib.Square_Matrix(a.CudaPtr, batchSize, dim, aSquare.CudaPtr);
        }

        public void Cosine_Similarity_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, CudaPieceFloat aa, CudaPieceFloat bb, float eps)
        {
            Cudalib.Cosine_Similarity_Matching(a.CudaPtr, b.CudaPtr, c.CudaPtr, matchIdxA.CudaPtr, matchIdxB.CudaPtr,
                aSize, bSize, matchSize, dimension, aa.CudaPtr, bb.CudaPtr, eps);
        }

        public void Inner_Product_Matching(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB,
            int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            Cudalib.Inner_Product_Matching(a.CudaPtr, b.CudaPtr, c.CudaPtr, matchIdxA.CudaPtr, matchIdxB.CudaPtr,
                aSize, bSize, matchSize, dimension, eps);
        }

        public void SEQ_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat output, int inputDimension, int outputDimension, int winSize)
        {
            Cudalib.SEQ_Sparse_Matrix_Multiply_INTEX(data.SampleIdx.CudaPtr, data.BatchSize, data.Seg_Idx, data.Seg_Margin, data.SentSize, data.Fea_Idx, data.Fea_Value, data.ElementSize,
                                        weight.CudaPtr, output.CudaPtr, inputDimension, outputDimension, winSize);

        }

        public void Convolution_Sparse_Matrix_Multiply_INTEX(SeqSparseBatchData data, CudaPieceFloat weight, CudaPieceFloat layerPoolingOutput, int inputDimension, int outputDimension, int winSize)
        {
            Cudalib.Convolution_Sparse_Matrix_Multiply_INTEX(data.SampleIdx.CudaPtr, data.BatchSize, data.SequenceIdx.CudaPtr, data.SentMargin.CudaPtr,
                                        data.SentSize, data.Fea_Idx, data.Fea_Value, data.ElementSize,
                                        weight.CudaPtr, layerPoolingOutput.CudaPtr, inputDimension, outputDimension, winSize);

        }

        public void Zero(CudaPieceFloat matrix, int size)
        {
            Cudalib.Zero(matrix.CudaPtr, size);
        }

        public void Max_Pooling(CudaPieceFloat layerPoolingOutput, SeqSparseBatchData data, CudaPieceFloat output, CudaPieceInt layerMaxPooling_Index, int outputDimension)
        {
            Cudalib.Max_Pooling(layerPoolingOutput.CudaPtr, data.Sample_Idx, data.BatchSize, output.CudaPtr, layerMaxPooling_Index.CudaPtr, outputDimension);
        }

        public void Max_Pooling(CudaPieceFloat pooling_feas, CudaPieceInt Smp_Index, int batchsize, CudaPieceFloat output, CudaPieceInt maxpooling_index, int output_dimension)
        {
            Cudalib.Max_Pooling(pooling_feas.CudaPtr, Smp_Index.CudaPtr, batchsize, output.CudaPtr, maxpooling_index.CudaPtr, output_dimension);
        }

        public void MaxPoolingMask(CudaPieceFloat poolingValues, CudaPieceInt smpIdx, CudaPieceInt smpMask, int batchsize, CudaPieceFloat output, CudaPieceInt maxpoolingIndex, int output_dimension)
        {
            Cudalib.MaxPoolingMask(poolingValues.CudaPtr, smpIdx.CudaPtr, smpMask.CudaPtr, batchsize, output.CudaPtr, maxpoolingIndex.CudaPtr, output_dimension);
        }


        public void DerivMaxPooling(CudaPieceFloat deriv, CudaPieceInt maxpoolingIndex, CudaPieceFloat poolingDeriv, int batchsize, int output_dimension, float alpha, float beta)
        {
            Cudalib.DerivMaxPooling(deriv.CudaPtr, maxpoolingIndex.CudaPtr, poolingDeriv.CudaPtr, batchsize, output_dimension, alpha, beta);
        }

        public void Matrix_Multipy(CudaPieceFloat input, CudaPieceFloat weight, CudaPieceFloat output, int batchsize, int inputDimension, int outputDimension, int inverse)
        {
            if (inverse == 0)
                Cudalib.CuBLAS_Matrix_Multiplication(input.CudaPtr, weight.CudaPtr, output.CudaPtr, batchsize, inputDimension, outputDimension, 1, 0, false, false);
            else
                Cudalib.CuBLAS_Matrix_Multiplication(input.CudaPtr, weight.CudaPtr, output.CudaPtr, batchsize, inputDimension, outputDimension, 1, 0, false, true);
            
        }


        public void Matrix_Add_Tanh(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            Cudalib.Matrix_Add_Tanh(output.CudaPtr, bias.CudaPtr, batchsize, outputDimension);
        }


        public void Matrix_Add_Linear(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            Cudalib.Matrix_Add_Vector(output.CudaPtr, bias.CudaPtr, batchsize, outputDimension);
        }

        public void Matrix_Add_Rectified(CudaPieceFloat output, CudaPieceFloat bias, int batchsize, int outputDimension)
        {
            Cudalib.Matrix_Rectified_Vector(output.CudaPtr, bias.CudaPtr, batchsize, outputDimension);
        }

        public void Cosine_Similarity(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int topLayerSize, float eps)
        {
            Cudalib.Cosine_Similarity(srcTopLayerOutput.CudaPtr,
                    tgtTopLayerOutput.CudaPtr, alpha.CudaPtr, nTrailPlus1, BATCH_SIZE, mIndex,
                    batchsize, topLayerSize, eps); // float.Epsilon);
        }

        public void Cosine_Similarity_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize, int topLayerSize, float eps)
        {
            Cudalib.Cosine_Similarity_EX(srcTopLayerOutput.CudaPtr,
                    tgtTopLayerOutput.CudaPtr, GPU_negative_index.CudaPtr, alpha.CudaPtr, nTrailPlus1, BATCH_SIZE, mIndex,
                    batchsize, topLayerSize, eps); // float.Epsilon);
        }

        public void Calculate_Alpha(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            Cudalib.Calculate_Alpha(alpha.CudaPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        }

        public void Calculate_Alpha_MXE(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            Cudalib.Calculate_Alpha_MXE(alpha.CudaPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        }

        public void Calculate_Alpha_NCE(CudaPieceFloat alpha, CudaPieceFloat dist, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            Cudalib.Calculate_Alpha_NCE(alpha.CudaPtr, dist.CudaPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        }

        public void Calculate_Alpha_NCE2(CudaPieceFloat alpha, CudaPieceFloat dist, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            Cudalib.Calculate_Alpha_NCE2(alpha.CudaPtr, dist.CudaPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        }

        public void Calculate_Alpha_PAIRRANK(CudaPieceFloat alpha, int nTrailPlus1, int BATCH_SIZE, int batchsize, float GAMMA)
        {
            Cudalib.Calculate_Alpha_PAIRRANK(alpha.CudaPtr, nTrailPlus1, BATCH_SIZE, batchsize, GAMMA);
        }

        public void FillOut_Dist_NCE(CudaPieceFloat dist, CudaPieceInt GPU_negative_index, int nTrailPlus1, int BATCH_SIZE, int mIndex, int batchsize)
        {
            Cudalib.FillOut_Dist_NCE(dist.CudaPtr, GPU_negative_index.CudaPtr, nTrailPlus1, BATCH_SIZE, mIndex, batchsize);
        }

        public void Deriv_Cosine(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            Cudalib.Deriv_Cosine(srcTopLayerOutput.CudaPtr, tgtTopLayerOutput.CudaPtr, srcTopLayerOutputDeriv.CudaPtr, tgtTopLayerOutputDeriv.CudaPtr, batchsize, outputLayerSize, eps);
        }

        public void Derive_Cosine_Linear(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            Cudalib.Derive_Cosine_Linear(srcTopLayerOutput.CudaPtr, tgtTopLayerOutput.CudaPtr, srcTopLayerOutputDeriv.CudaPtr, tgtTopLayerOutputDeriv.CudaPtr, batchsize, outputLayerSize, eps);
        }

        public void Derive_Cosine_Rectified(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            Cudalib.Derive_Cosine_Rectified(srcTopLayerOutput.CudaPtr, tgtTopLayerOutput.CudaPtr, srcTopLayerOutputDeriv.CudaPtr, tgtTopLayerOutputDeriv.CudaPtr, batchsize, outputLayerSize, eps);
        }

        public void Deriv_Cosine_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            Cudalib.Deriv_Cosine_EX(srcTopLayerOutput.CudaPtr, tgtTopLayerOutput.CudaPtr, GPU_negative_index.CudaPtr, srcTopLayerOutputDeriv.CudaPtr, tgtTopLayerOutputDeriv.CudaPtr, batchsize, outputLayerSize, eps);
        }

        public void Derive_Cosine_Linear_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            Cudalib.Derive_Cosine_Linear_EX(srcTopLayerOutput.CudaPtr, tgtTopLayerOutput.CudaPtr, GPU_negative_index.CudaPtr, srcTopLayerOutputDeriv.CudaPtr, tgtTopLayerOutputDeriv.CudaPtr, batchsize, outputLayerSize, eps);
        }

        public void Derive_Cosine_Rectified_EX(CudaPieceFloat srcTopLayerOutput, CudaPieceFloat tgtTopLayerOutput, CudaPieceInt GPU_negative_index, CudaPieceFloat srcTopLayerOutputDeriv, CudaPieceFloat tgtTopLayerOutputDeriv, int batchsize, int outputLayerSize, float eps)
        {
            Cudalib.Derive_Cosine_Rectified_EX(srcTopLayerOutput.CudaPtr, tgtTopLayerOutput.CudaPtr, GPU_negative_index.CudaPtr, srcTopLayerOutputDeriv.CudaPtr, tgtTopLayerOutputDeriv.CudaPtr, batchsize, outputLayerSize, eps);
        }

        public void Matrix_WeightAdd(CudaPieceFloat result, CudaPieceFloat addTerm, int batchsize, int outputLayerSize, CudaPieceFloat mweight, int start, int keep)
        {
            Cudalib.Matrix_WeightAdd(result.CudaPtr, addTerm.CudaPtr, batchsize, outputLayerSize, mweight.CudaPtr, start, keep);
        }

        public void Matrix_WeightAdd_EX(CudaPieceFloat result, CudaPieceFloat addTerm, CudaPieceInt GPU_Inver_negative_index, CudaPieceInt GPU_Inver_negative_value, int batchsize, int outputLayerSize, CudaPieceFloat mweight, int start, int keep)
        {
            Cudalib.Matrix_WeightAdd_EX(result.CudaPtr, addTerm.CudaPtr, GPU_Inver_negative_index.CudaPtr, GPU_Inver_negative_value.CudaPtr, batchsize, outputLayerSize, mweight.CudaPtr, start, keep);
        }

        public void Deriv_Tanh(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension)
        {
            Cudalib.Deriv_Tanh(errorDeriv.CudaPtr, output.CudaPtr, batchsize, inputDimension);
        }

        public void Deriv_Rectified(CudaPieceFloat errorDeriv, CudaPieceFloat output, int batchsize, int inputDimension)
        {
            Cudalib.Deriv_Rectified(errorDeriv.CudaPtr, output.CudaPtr, batchsize, inputDimension);
        }


        public void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(SeqSparseBatchData input_batch, CudaPieceFloat weightDeriv, CudaPieceFloat upperOutputErrorDeriv, int inputDimension, int outputDimension, int winSize)
        {
            Cudalib.SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(input_batch.Sample_Idx, input_batch.BatchSize, input_batch.Seg_Idx, input_batch.Seg_Margin, input_batch.SentSize, input_batch.Fea_Idx, input_batch.Fea_Value, input_batch.ElementSize,
                               weightDeriv.CudaPtr, upperOutputErrorDeriv.CudaPtr, inputDimension, outputDimension, winSize);
        }


        public void Convolution_Sparse_Matrix_Product_INTEX(CudaPieceFloat upperOutputErrorDeriv, CudaPieceInt layerMaxPooling_Index, SeqSparseBatchData input_batch, int winSize, int batchsize, int outputDimension, CudaPieceFloat weightDeriv, int inputDimension)
        {
            Cudalib.Convolution_Sparse_Matrix_Product_INTEX(upperOutputErrorDeriv.CudaPtr, layerMaxPooling_Index.CudaPtr, input_batch.Seg_Idx, input_batch.Seg_Margin, input_batch.SentSize, winSize,
                                     batchsize, outputDimension, input_batch.Fea_Idx, input_batch.Fea_Value, weightDeriv.CudaPtr, inputDimension);
        }


        public void Matrix_Product(CudaPieceFloat lowerOutput, CudaPieceFloat upperOutputErrorDeriv, CudaPieceFloat weightDeriv, int batchsize, int inputDimension, int outputDimension)
        {
            Cudalib.Matrix_Product(lowerOutput.CudaPtr, upperOutputErrorDeriv.CudaPtr, weightDeriv.CudaPtr,
                        batchsize, inputDimension, outputDimension);
        }

        public void Matrix_Aggragate(CudaPieceFloat a, CudaPieceFloat b, int batchsize, int m)
        {
            Cudalib.Matrix_Aggragate(a.CudaPtr, b.CudaPtr, batchsize, m);
        }

        public void Scale_Matrix(CudaPieceFloat matrix, int inputDimension, int outputDimnsion, float momentum)
        {
            Cudalib.Scale_Matrix(matrix.CudaPtr, inputDimension, outputDimnsion, momentum);
        }

        public void Matrix_Add(CudaPieceFloat matrix, CudaPieceFloat updates, int inputDimension, int outputDimnsion, float learning_rate)
        {
            Cudalib.Matrix_Add(matrix.CudaPtr, updates.CudaPtr, inputDimension, outputDimnsion, learning_rate);
        }

        public void Sparse2Dense_Matrix(SeqSparseBatchData data, CudaPieceFloat matrix, int batchsize, int outputDimension)
        {
            Cudalib.Sparse2Dense_Matrix(data.Seg_Idx, data.Fea_Idx, data.Fea_Value, matrix.CudaPtr, batchsize, outputDimension);
        }


        public void Cosine_Similarity_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int BATCHSIZE,
                int batchsize, int dimension, float eps)
        {
            Cudalib.Cosine_Similarity_EX_Full(a.CudaPtr, b.CudaPtr, neg_list.CudaPtr, c.CudaPtr, nTrial, BATCHSIZE, batchsize, dimension, eps);
        }

        public void FillOut_Dist_NCE_Full(CudaPieceFloat dist, CudaPieceInt neg_list, int nTrail, int BATCH_SIZE, int batchsize)
        {
            Cudalib.FillOut_Dist_NCE_Full(dist.CudaPtr, neg_list.CudaPtr, nTrail, BATCH_SIZE, batchsize);
        }

        public void Deriv_Cosine_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq,
                CudaPieceFloat dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        {
            Cudalib.Deriv_Cosine_EX_Full(q.CudaPtr, d.CudaPtr, neg_list.CudaPtr, dcq.CudaPtr, dcd.CudaPtr, nTrail, BATCHSIZE, batchsize, m, eps);
        }

        public void Deriv_Cosine_Linear_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd,
                int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        {
            Cudalib.Deriv_Cosine_Linear_EX_Full(q.CudaPtr, d.CudaPtr, neg_list.CudaPtr, dcq.CudaPtr, dcd.CudaPtr, nTrail, BATCHSIZE, batchsize, m, eps);
        }

        public void Deriv_Cosine_Rectified_EX_Full(CudaPieceFloat q, CudaPieceFloat d, CudaPieceInt neg_list, CudaPieceFloat dcq, CudaPieceFloat dcd,
                int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
        {
            Cudalib.Deriv_Cosine_Rectified_EX_Full(q.CudaPtr, d.CudaPtr, neg_list.CudaPtr, dcq.CudaPtr, dcd.CudaPtr, nTrail, BATCHSIZE, batchsize, m, eps);
        }

        public void Matrix_WeightAdd_Full(CudaPieceFloat gpu_floats_a, CudaPieceFloat gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension,
                CudaPieceFloat mweight, int start, int keep)
        {
            Cudalib.Matrix_WeightAdd_Full(gpu_floats_a.CudaPtr, gpu_floats_b.CudaPtr, nTrail, BATCHSIZE, batchsize, dimension,
                mweight.CudaPtr, start, keep);
        }

        public void Matrix_WeightAdd_EX_Full(CudaPieceFloat gpu_floats_a, CudaPieceFloat gpu_floats_b, CudaPieceInt inver_neg_index,
                CudaPieceInt inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, CudaPieceFloat mweight,
                int start, int keep)
        {
            Cudalib.Matrix_WeightAdd_EX_Full(gpu_floats_a.CudaPtr, gpu_floats_b.CudaPtr, inver_neg_index.CudaPtr,
                inver_neg_value.CudaPtr, nTrial, BATCHSIZE, batchsize, dimension, mweight.CudaPtr, start, keep);
        }

        public void Cosine_Similarity_SubSpace(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps)
        {
            Cudalib.Cosine_Similarity_SubSpace(a.CudaPtr, b.CudaPtr, c.CudaPtr, labelDim, BATCHSIZE, batchsize, subspaceDim, eps);
        }

        public void SoftMax(CudaPieceFloat a, CudaPieceFloat b, int labelDim, int batchsize, float gamma)
        {
            //if (CuDnnHandle == IntPtr.Zero)
            {
                /// this function will be stale.
                //Cudalib.MultiClassSoftmax(a.CudaPtr, b.CudaPtr, labelDim, gamma, batchsize);
                //throw new Exception("cudnnhandle is zero..");
            }
            //else
            {
                Cudalib.CuDNNSoftmax(CuDnnHandle, a.CudaPtr, b.CudaPtr, batchsize, labelDim);
            }
        }

        public void DerivSoftmax(CudaPieceFloat softmaxData, CudaPieceFloat softmaxDeriv, CudaPieceFloat gradData, int dim, int batchsize, float alpha)
        {
            Cudalib.CuDNNDerivSoftmax(CuDnnHandle, softmaxData.CudaPtr, softmaxDeriv.CudaPtr, gradData.CudaPtr, batchsize, dim, alpha);
        }


        public void Deriv_Cosine_Subspace(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps)
        {
            Cudalib.Deriv_Cosine_Subspace(q.CudaPtr, d.CudaPtr, dcq.CudaPtr, dcd.CudaPtr, alpha.CudaPtr, act_type, batchsize, labelDim, subspaceDim, gamma, eps);
        }

        public void InnerProduct_Similarity(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int dimension)
        {
            Cudalib.InnerProduct_Similarity(a.CudaPtr, b.CudaPtr, c.CudaPtr, batchsize, dimension);
        }

        public void Deriv_InnerProduct(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat dcq, CudaPieceFloat dcd, CudaPieceFloat alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
        {
            Cudalib.Deriv_InnerProduct(q.CudaPtr, d.CudaPtr, dcq.CudaPtr, dcd.CudaPtr, alpha.CudaPtr, act_type, batchsize, Dim, gamma, eps);
        }

        public void Matrix_Add_OFFSET(CudaPieceFloat a, int offset_a, CudaPieceFloat b, int offset_b, int len, float mweight)
        {
            Cudalib.Matrix_Add_OFFSET(a.CudaPtr, offset_a, b.CudaPtr, offset_b, len, mweight);
        }

        public void Matrix_Add_Sigmoid(CudaPieceFloat a, CudaPieceFloat b, int m, int n)
        {
            Cudalib.Matrix_Add_Sigmoid(a.CudaPtr, b.CudaPtr, m, n);
        }

        public void RecursiveForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqInputMatrixs, int dim, CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqOutputMatrix)
        {
            //PerfCounter.Manager.Instance["RecursiveForwardPropagateBatch"].CountOperation(() =>
            Cudalib.RecursiveForwardPropagateBatch(InstanceIndex.CudaPtr, batchSize, SeqInputMatrixs.CudaPtr, dim, WMatrix.CudaPtr, lag, af, SeqOutputMatrix.CudaPtr); //);
        }

        public void RecursiveBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat WMatrix, int lag, int af, CudaPieceFloat SeqDerivMatrixs)
        {
            //PerfCounter.Manager.Instance["RecursiveBackwardPropagateBatch"].CountOperation(() =>
            Cudalib.RecursiveBackwardPropagateBatch(InstanceIndex.CudaPtr, batchSize, SeqOutputMatrixs.CudaPtr, dim, WMatrix.CudaPtr, lag, af, SeqDerivMatrixs.CudaPtr); //);
        }

        public void MatrixMultiplicationTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt, float weight)
        {
            //if (ParameterSetting.CuBlasEnable)
            //{
                Cudalib.CuBLAS_Matrix_Multiplication(pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, leftRowCnt, rightColumnCnt, rightRowCnt, 1, weight, false, true);
            //}
            //else
            //{
            //    Cudalib.Matrix_Multipy_Weight(pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, leftRowCnt, rightColumnCnt, rightRowCnt, 1, weight);
            //}
        }

        public void MatrixMultiplicationLeftTranspose(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            //if (ParameterSetting.CuBlasEnable)
            //{
                Cudalib.CuBLAS_Matrix_Multiplication(pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, leftRowCnt, leftColumnCnt, rightColumnCnt, wei, 1, true, false);
            //}
            //else
            //{
            //    Cudalib.Matrix_Product_Weight(pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, leftRowCnt, leftColumnCnt, rightColumnCnt, wei);
            //}
        }

        public void MatrixMultiplicationSparseLeftTranspose(CudaPieceInt pRowIndex, int RowNum, CudaPieceInt pColumnIndex, CudaPieceFloat pValue, int elementsize,
                    CudaPieceFloat pRight, CudaPieceFloat pDst, int leftColumnCnt, int rightColumnCnt, float wei)
        {
            Cudalib.Sparse_Matrix_Transpose_Multiply_INTEX_Weight(pRowIndex.CudaPtr, RowNum, pColumnIndex.CudaPtr, pValue.CudaPtr, elementsize,
                                                    pDst.CudaPtr, pRight.CudaPtr, leftColumnCnt, rightColumnCnt, wei);
        }


        public void ColumnWiseSum(CudaPieceFloat matrix, CudaPieceFloat result, int row, int col, float wei)
        {
            Cudalib.Matrix_Aggragate_Weight(matrix.CudaPtr, result.CudaPtr, row, col, wei);
        }

        public void RecursiveUpdateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, CudaPieceFloat SeqOutputMatrixs, int dim, CudaPieceFloat SeqDerivMatrixs, int lag, CudaPieceFloat gradient, float weight)
        {
            Cudalib.RecursiveUpdateBatch(InstanceIndex.CudaPtr, batchSize, seqSize, SeqOutputMatrixs.CudaPtr, dim, SeqDerivMatrixs.CudaPtr, lag, gradient.CudaPtr, weight);
        }


        public void DerivCrossEntropy(CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, int outputSize)
        {
            Cudalib.CrossEntropyLoss(outputScore.CudaPtr, outputLabel.CudaPtr, outputDeriv.CudaPtr, outputSize);
        }


        public void ClipVector(CudaPieceFloat gpu_floats_a, int m, float maxThreshold, float minThreshold)
        {
            Cudalib.Clip_Vector(gpu_floats_a.CudaPtr, m, maxThreshold, minThreshold);
        }


        public void Add_Vector(CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei)
        {
            Cudalib.Add_Vector(a.CudaPtr, b.CudaPtr, m, awei, bwei);
        }


        public void Ada_Gradient(CudaPieceFloat ada, CudaPieceFloat grad, int m)
        {
            Cudalib.Ada_Gradient(ada.CudaPtr, grad.CudaPtr, m);
        }


        public void Convolution_Sparse_Matrix_Product_INTEX_Weight(CudaPieceFloat deriv, CudaPieceInt maxpooling_index, CudaPieceInt Seg_Index, CudaPieceInt SegMargin_Index, int seg_size, int win_size, int batchsize, int output_dimension, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, CudaPieceFloat grad, int Feature_Dimension, float learnRate)
        {
            Cudalib.Convolution_Sparse_Matrix_Product_INTEX_Weight(deriv.CudaPtr, maxpooling_index.CudaPtr, Seg_Index.CudaPtr, SegMargin_Index.CudaPtr, seg_size, win_size, batchsize, output_dimension, Fea_Index.CudaPtr, Fea_Value.CudaPtr, grad.CudaPtr, Feature_Dimension, learnRate);
        }

        public void Matrix_Mask(CudaPieceFloat output, CudaPieceInt dropoutMask, int batchSize, int dim)
        {
            Cudalib.Matrix_Mask(output.CudaPtr, dropoutMask.CudaPtr, batchSize, dim);
        }

        public void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, float weight)
        {
            Cudalib.SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(Smp_Index.CudaPtr, batchsize, Seg_Index.CudaPtr, seg_size, Fea_Index.CudaPtr, Fea_Value.CudaPtr, elementsize, mul_weight.CudaPtr, output.CudaPtr, Feature_dimension, output_dimension, weight);
        }


        public void SEQ_Sparse_Matrix_Multiply_INT(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat mul_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension)
        {
            Cudalib.SEQ_Sparse_Matrix_Multiply_INT(Smp_Index.CudaPtr, batchsize, Seg_Index.CudaPtr, seg_size, Fea_Index.CudaPtr, Fea_Value.CudaPtr, elementsize, mul_weight.CudaPtr, output.CudaPtr, Feature_dimension, output_dimension);
        }


        public void Matrix_Product_Weight(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, int batchsize, int m, int n, float weight)
        {
            Cudalib.Matrix_Product_Weight(a.CudaPtr, b.CudaPtr, c.CudaPtr, batchsize, m, n, weight);
        }


        public void Inner_Product_EX_Full(CudaPieceFloat a, CudaPieceFloat b, CudaPieceInt neg_list, CudaPieceFloat c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps)
        {
            Cudalib.Inner_Product_EX_Full(a.CudaPtr, b.CudaPtr, neg_list.CudaPtr, c.CudaPtr, nTrial, BATCHSIZE, batchsize, dimension, eps);
        }

        public void LSTMForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c,
           CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo)
        {
            Cudalib.LSTMForwardPropagateBatch(InstanceIndex.CudaPtr, batchSize, o.CudaPtr, gate_i.CudaPtr, c_hat.CudaPtr, gate_f.CudaPtr, c.CudaPtr, gate_o.CudaPtr, tanhc.CudaPtr, cell, Ui.CudaPtr, Uc.CudaPtr, Uf.CudaPtr, Uo.CudaPtr, Vo.CudaPtr);
        }

        public void LSTMBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell,
            CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc,
            CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat, CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc,
            CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo)
        {
            Cudalib.LSTMBackwardPropagateBatch(InstanceIndex.CudaPtr, batchSize, seqSize, cell, o.CudaPtr, gate_i.CudaPtr, c_hat.CudaPtr, gate_f.CudaPtr, c.CudaPtr, gate_o.CudaPtr, tanhc.CudaPtr,
                deriv_o.CudaPtr, deriv_gate_i.CudaPtr, deriv_cHat.CudaPtr, deriv_gate_f.CudaPtr, deriv_c.CudaPtr, deriv_gate_o.CudaPtr, deriv_tanhc.CudaPtr, Ui.CudaPtr, Uc.CudaPtr, Uf.CudaPtr, Uo.CudaPtr, Vo.CudaPtr);
        }

        public void RNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOut)
        {
            Cudalib.RNNLabelOutput(smpIdx.CudaPtr, batchSize, seqInput.CudaPtr, seqSize, Wmatrix.CudaPtr, lag, feaDim, hiddenDim, seqOut.CudaPtr);
        }

        public void DerivRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInputDeriv, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv)
        {
            Cudalib.DeRNNLabelOutput(smpIdx.CudaPtr, batchSize, seqInputDeriv.CudaPtr, seqSize, Wmatrix.CudaPtr, lag, feaDim, hiddenDim, seqOutDeriv.CudaPtr);
        }
        public void UpdateRNNLabelOutput(CudaPieceInt smpIdx, int batchSize, CudaPieceFloat seqInput, int seqSize, CudaPieceFloat Wmatrix, int lag, int feaDim, int hiddenDim, CudaPieceFloat seqOutDeriv, float wei)
        {
            Cudalib.UpdateRNNLabelOutput(smpIdx.CudaPtr, batchSize, seqInput.CudaPtr, seqSize, Wmatrix.CudaPtr, lag, feaDim, hiddenDim, seqOutDeriv.CudaPtr, wei);
        }

        public void LastStepExtractSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix)
        {
            Cudalib.CalculateLastMatrixs(InstanceIndex.CudaPtr, batchSize, SeqMatrixs.CudaPtr, dim, matrix.CudaPtr);
        }

        public void Calculate_SampleCrossEntropyDeriv(CudaPieceFloat output, CudaPieceFloat deriv, int dim, CudaPieceFloat label, int batchsize, float gamma)
        {
            Cudalib.Calculate_SampleCrossEntropyDeriv(output.CudaPtr, deriv.CudaPtr, dim, label.CudaPtr, batchsize, gamma);
        }


        public void ElementwiseProduct(CudaPieceFloat pLeft, CudaPieceFloat pRight, CudaPieceFloat pDst, int row, int column, float alpha)
        {
            Cudalib.ElementwiseProduct(pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, row, column, alpha);
            
        }

        public void GRUForwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim, //->x;
            CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH,
            CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh)
        {
            Cudalib.GRUForwardPropagateBatch(InstanceIndex.CudaPtr, batchSize, Dim, H.CudaPtr, GateR.CudaPtr, GateZ.CudaPtr, HHat.CudaPtr, RestH.CudaPtr,
                Ur.CudaPtr, Uz.CudaPtr, Uh.CudaPtr);
        }

        public void GRUBackwardPropagateBatch(CudaPieceInt InstanceIndex, int batchSize, int Dim,//->x
         CudaPieceFloat H, CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat, CudaPieceFloat RestH, //H @ R
         CudaPieceFloat DerivH, CudaPieceFloat DerivGateR, CudaPieceFloat DerivGateZ, CudaPieceFloat DerivHHat, CudaPieceFloat DerivRestH,
         CudaPieceFloat Ur, CudaPieceFloat Uz, CudaPieceFloat Uh)
        {
            Cudalib.GRUBackwardPropagateBatch(InstanceIndex.CudaPtr, batchSize, Dim, H.CudaPtr, GateR.CudaPtr, GateZ.CudaPtr, HHat.CudaPtr, RestH.CudaPtr,
                DerivH.CudaPtr, DerivGateR.CudaPtr, DerivGateZ.CudaPtr, DerivHHat.CudaPtr, DerivRestH.CudaPtr,
                Ur.CudaPtr, Uz.CudaPtr, Uh.CudaPtr);
        }

        public void Deriv_CosineSimilarity_Matching(CudaPieceFloat q, CudaPieceFloat d, CudaPieceFloat qSquare, CudaPieceFloat dSquare, int dim,
            CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize,
            CudaPieceFloat simi, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps)
        {
            Cudalib.Deriv_CosineSimilarity_Matching(q.CudaPtr, d.CudaPtr, qSquare.CudaPtr, dSquare.CudaPtr, dim,
                src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, tgt2MatchIdx.CudaPtr, tgt2MatchElement.CudaPtr, srcIdx.CudaPtr,
                tgtIdx.CudaPtr, srcSize, tgtSize, matchSize, simi.CudaPtr, derivSimi.CudaPtr, dcq.CudaPtr, dcd.CudaPtr, eps);
        }

        public void Deriv_InnerProduct_Matching(CudaPieceFloat q, CudaPieceFloat d, int dim, CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, CudaPieceInt tgt2MatchIdx, CudaPieceInt tgt2MatchElement, CudaPieceInt srcIdx, CudaPieceInt tgtIdx, int srcSize, int tgtSize, int matchSize, CudaPieceFloat derivSimi, CudaPieceFloat dcq, CudaPieceFloat dcd, float eps)
        {
            Cudalib.Deriv_InnerProduct_Matching(q.CudaPtr, d.CudaPtr, dim, src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, tgt2MatchIdx.CudaPtr, tgt2MatchElement.CudaPtr, srcIdx.CudaPtr,
                  tgtIdx.CudaPtr, srcSize, tgtSize, matchSize, derivSimi.CudaPtr, dcq.CudaPtr, dcd.CudaPtr, eps);
        }


        public void LSTMForwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat o, CudaPieceFloat gate_i,
           CudaPieceFloat c_hat, CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc, int cell,
           CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf, CudaPieceFloat Uo, CudaPieceFloat Vo,
           CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag)
        {
            unsafe
            {
                fixed (int* rIdx = &RecursiveIdx.MemPtr[0])
                {
                    Cudalib.LSTMForwardPropagateBatchV2(InstanceIndex.CudaPtr, batchSize, o.CudaPtr, gate_i.CudaPtr, c_hat.CudaPtr, gate_f.CudaPtr, c.CudaPtr, gate_o.CudaPtr,
                        tanhc.CudaPtr, cell, Ui.CudaPtr, Uc.CudaPtr, Uf.CudaPtr, Uo.CudaPtr, Vo.CudaPtr,
                        (IntPtr)rIdx, RecursiveMatrix.CudaPtr, MaxLag);
                }
            }
        }

        unsafe public void LSTMBackwardPropagateBatchV2(CudaPieceInt InstanceIndex, int batchSize, int seqSize, int cell, CudaPieceFloat o, CudaPieceFloat gate_i, CudaPieceFloat c_hat,
            CudaPieceFloat gate_f, CudaPieceFloat c, CudaPieceFloat gate_o, CudaPieceFloat tanhc, CudaPieceFloat deriv_o, CudaPieceFloat deriv_gate_i, CudaPieceFloat deriv_cHat,
            CudaPieceFloat deriv_gate_f, CudaPieceFloat deriv_c, CudaPieceFloat deriv_gate_o, CudaPieceFloat deriv_tanhc, CudaPieceFloat Ui, CudaPieceFloat Uc, CudaPieceFloat Uf,
            CudaPieceFloat Uo, CudaPieceFloat Vo, CudaPieceInt RecursiveIdx, CudaPieceInt RecursiveMatrix, int MaxLag)
        {
            unsafe
            {
                fixed (int* rIdx = &RecursiveIdx.MemPtr[0])
                {
                    Cudalib.LSTMBackwardPropagateBatchV2(InstanceIndex.CudaPtr, batchSize, seqSize, cell, o.CudaPtr, gate_i.CudaPtr, c_hat.CudaPtr, gate_f.CudaPtr, c.CudaPtr,
                        gate_o.CudaPtr, tanhc.CudaPtr, deriv_o.CudaPtr, deriv_gate_i.CudaPtr, deriv_cHat.CudaPtr, deriv_gate_f.CudaPtr, deriv_c.CudaPtr, deriv_gate_o.CudaPtr,
                        deriv_tanhc.CudaPtr, Ui.CudaPtr, Uc.CudaPtr, Uf.CudaPtr, Uo.CudaPtr, Vo.CudaPtr, (IntPtr)rIdx, RecursiveMatrix.CudaPtr, MaxLag);
                }
            }
        }

        public float L2Norm(CudaPieceFloat x, int size)
        {
            return Cudalib.L2Norm(x.CudaPtr, size);
        }

        public void RMSPropGradient(CudaPieceFloat ada, CudaPieceFloat grad, float gamma, float epsilon, int m)
        {
            Cudalib.RMSPropGradient(ada.CudaPtr, grad.CudaPtr, gamma, epsilon, m);
        }

        public void VectorSquareAdd(CudaPieceFloat Z, CudaPieceFloat X, CudaPieceFloat Y, float wx, float wy, int m)
        {
            Cudalib.VectorSquareAdd(Z.CudaPtr, X.CudaPtr, Y.CudaPtr, 0, wx, wy, m);
        }

        public void AdaMGradient(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
        {
            Cudalib.AdaMGradient(M.CudaPtr, V.CudaPtr, G.CudaPtr, alpha, beta1, beta2, epsilon, t, size);
        }
        public void AdaDeltaGradient(CudaPieceFloat deltaX, CudaPieceFloat AccumGrad, CudaPieceFloat AccumUpdate, CudaPieceFloat Grad, float epsilon, int size)
        {
            Cudalib.AdaDeltaGradient(deltaX.CudaPtr, AccumGrad.CudaPtr, AccumUpdate.CudaPtr, Grad.CudaPtr, epsilon, size);
        }

        public void AdaMax(CudaPieceFloat V, CudaPieceFloat M, CudaPieceFloat G, float alpha, float beta, float epsilon, int size)
        {
            Cudalib.AdaMax(V.CudaPtr, M.CudaPtr, G.CudaPtr, alpha, beta, epsilon, size);
        }


        

        /************************* Basic Computation Lib.******************************/

        public void Tanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize)
        {
            Cudalib.Tanh(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), batchsize);
        }

        public void Logistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize, float gamma)
        {
            Cudalib.Logistic(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), batchsize, gamma);
        }

        public void ReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int batchsize)
        {
            Cudalib.ReLU(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), batchsize);
        }

        public void ElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst, int batchSize, int dim, float alpha)
        {
            Cudalib.ElementwiseProduct(IntPtr.Add(pLeft.CudaPtr, offsetLeft * sizeof(float)), IntPtr.Add(pRight.CudaPtr, offsetRight * sizeof(float)),
                IntPtr.Add(pDst.CudaPtr, offsetDst * sizeof(float)), batchSize, dim, alpha);
        }

        public void ElementwiseProductMask(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst,
            CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float alpha, float beta)
        {
            Cudalib.ElementwiseProductMask(IntPtr.Add(pLeft.CudaPtr, offsetLeft * sizeof(float)),
                IntPtr.Add(pRight.CudaPtr, offsetRight * sizeof(float)), IntPtr.Add(pDst.CudaPtr, offsetDst * sizeof(float)),
                IntPtr.Add(leftmask.CudaPtr, offsetlm * sizeof(int)), IntPtr.Add(rightmask.CudaPtr, offsetrm * sizeof(int)),
                IntPtr.Add(dstmask.CudaPtr, offsetdm * sizeof(int)), row, column, alpha, beta);
        }


        public void Sgemm(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int rowA, int inDim, int outDim, float alpha, float beta, bool transA, bool transB)
        {
            if (CuBlasHandle == IntPtr.Zero)
                Cudalib.CuBLAS_Matrix_Multiplication(IntPtr.Add(A.CudaPtr, offsetA * sizeof(float)),
                    IntPtr.Add(B.CudaPtr, offsetB * sizeof(float)), IntPtr.Add(C.CudaPtr, offsetC * sizeof(float)), rowA, inDim, outDim, alpha, beta, transA, transB);
            else
                Cudalib.CuBLAS_Sgemm(CuBlasHandle, IntPtr.Add(A.CudaPtr, offsetA * sizeof(float)),
                    IntPtr.Add(B.CudaPtr, offsetB * sizeof(float)), IntPtr.Add(C.CudaPtr, offsetC * sizeof(float)), rowA, inDim, outDim, alpha, beta, transA, transB);
        }


        public void SgemmMask(CudaPieceFloat A, int offsetA, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC,
            int batchsize, int m, int n, CudaPieceInt aMask, int offsetAMask, CudaPieceInt bMask, int offsetBMask, CudaPieceInt cMask, int offsetCMask,
            float alpha, float beta, bool transA, bool transB)
        {
            Cudalib.SgemmMask(IntPtr.Add(A.CudaPtr, offsetA * sizeof(float)), IntPtr.Add(B.CudaPtr, offsetB * sizeof(float)), IntPtr.Add(C.CudaPtr, offsetC * sizeof(float)),
                batchsize, m, n, IntPtr.Add(aMask.CudaPtr, offsetAMask * sizeof(int)), IntPtr.Add(bMask.CudaPtr, offsetBMask * sizeof(int)), IntPtr.Add(cMask.CudaPtr, offsetCMask * sizeof(int)),
                alpha, beta, transA, transB);
        }


        public void SparseSgemmMask(CudaPieceInt AIndex, CudaPieceInt AFeaIndex, CudaPieceFloat AFeaValue, CudaPieceFloat B, int offsetB, CudaPieceFloat C, int offsetC, int batchsize, int m, int n, CudaPieceInt aMask, int offsetAMask, CudaPieceInt bMask, int offsetBMask, CudaPieceInt cMask, int offsetCMask, float alpha, float beta, bool transA, bool transB)
        {
            Cudalib.SparseSgemmMask(AIndex.CudaPtr, AFeaIndex.CudaPtr, AFeaValue.CudaPtr, IntPtr.Add(B.CudaPtr, offsetB * sizeof(float)),
                    IntPtr.Add(C.CudaPtr, offsetC * sizeof(float)), batchsize, m, n, IntPtr.Add(aMask.CudaPtr, offsetAMask * sizeof(int)),
                    IntPtr.Add(bMask.CudaPtr, offsetBMask * sizeof(int)), IntPtr.Add(cMask.CudaPtr, offsetCMask * sizeof(int)), alpha, beta, transA, transB);
        }

        public void DerivTanh(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            Cudalib.DerivTanh(IntPtr.Add(c.CudaPtr, offsetC * sizeof(float)),
                IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), batchsize, 1, alpha);
        }

        public void DerivLogistic(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            Cudalib.DerivSigmoid(IntPtr.Add(c.CudaPtr, offsetC * sizeof(float)),
                IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), batchsize, 1, alpha);
        }

        public void DerivReLU(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC, int batchsize, float alpha, float beta)
        {
            Cudalib.DerivRectified(IntPtr.Add(c.CudaPtr, offsetC * sizeof(float)),
                IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), batchsize, 1, alpha);
        }


       


        public void LastStepExtractSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim,
            CudaPieceFloat matrix, CudaPieceFloat matrixMask, int maskOffset)
        {
            Cudalib.CalculateLastMatrixsMask(InstanceIndex.CudaPtr, SeqTransIdx.CudaPtr, batchSize, SeqMatrixs.CudaPtr, dim, matrix.CudaPtr,
                IntPtr.Add(matrixMask.CudaPtr, maskOffset * sizeof(float)));
        }

        public void DerivLastStepSeqMatrix(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, int dim, CudaPieceFloat matrix, float alpha)
        {
            Cudalib.DerivCalculateLastMatrixs(InstanceIndex.CudaPtr, batchSize, SeqMatrixs.CudaPtr, dim, matrix.CudaPtr, alpha);
        }

        public void DerivLastStepSeqMatrixMask(CudaPieceInt InstanceIndex, CudaPieceInt SeqTransIdx, int batchSize, CudaPieceFloat SeqMatrixs, int dim,
            CudaPieceFloat matrix, CudaPieceFloat matrixMask, int maskOffset, float alpha)
        {
            Cudalib.DerivCalculateLastMatrixsMask(InstanceIndex.CudaPtr, SeqTransIdx.CudaPtr, batchSize, SeqMatrixs.CudaPtr, dim, matrix.CudaPtr,
                IntPtr.Add(matrixMask.CudaPtr, maskOffset * sizeof(float)), alpha);
        }


        public float LogLossDeriv(CudaPieceFloat outputProb, int dim, int batchSize, CudaPieceFloat smpProb,
            CudaPieceFloat label, CudaPieceFloat labelwei, CudaPieceInt labelMask, float gamma, float eps)
        {
            if (CuBlasHandle != null)
            {
                if (!labelwei.IsEmpty) { throw new Exception("label wei should be empty in the function LogLossDeriv."); }
                return Cudalib.CuBLAS_LogLossDeriv(CuBlasHandle, outputProb.CudaPtr, dim, batchSize, smpProb.CudaPtr, label.CudaPtr, labelwei.CudaPtr, labelMask.CudaPtr, gamma, eps);
            }
            return 0;
        }


        public void Add_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int m, float awei, float bwei)
        {
            Cudalib.Add_Vector(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)),
                               IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), m, awei, bwei);
        }


        public void Matrix_AdditionMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAMask,
            CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBMask,
            CudaPieceFloat c, int offsetC, CudaPieceInt cMask, int offsetCMask,
            int dim, int batchSize, float awei, float bwei, float cwei)
        {
            Cudalib.Matrix_AdditionMask(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), IntPtr.Add(aMask.CudaPtr, offsetAMask * sizeof(int)),
                                        IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), IntPtr.Add(bMask.CudaPtr, offsetBMask * sizeof(int)),
                                        IntPtr.Add(c.CudaPtr, offsetC * sizeof(float)), IntPtr.Add(cMask.CudaPtr, offsetCMask * sizeof(int)),
                                        dim, batchSize, awei, bwei, cwei);

        }


        public void SparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputScore, CudaPieceFloat outputProb, float gamma, int batchSize)
        {
            Cudalib.SparseMutliClassSoftmax(smpIdx.CudaPtr, outputScore.CudaPtr, outputProb.CudaPtr, gamma, batchSize);
        }


        public void DerivSparseMultiClassSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat probDeriv, CudaPieceFloat outputDeriv, float gamma, int batchSize, float alpha = 0, float beta = 1)
        {
            Cudalib.DerivSparseMultiClassSoftmax(smpIdx.CudaPtr, outputProb.CudaPtr, probDeriv.CudaPtr, outputDeriv.CudaPtr, gamma, alpha, beta, batchSize);
        }

        public void ColumnWiseSumMask(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM, CudaPieceFloat weight, CudaPieceInt smpIdx, int batchSize,
            CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM, int row, int col, float alpha, float beta)
        {
            Cudalib.ColumnWiseSumMaskV2(IntPtr.Add(matrix.CudaPtr, offsetM * sizeof(float)), IntPtr.Add(matrixMask.CudaPtr, offsetMM * sizeof(int)),
                weight.CudaPtr, smpIdx.CudaPtr, batchSize, IntPtr.Add(output.CudaPtr, offsetO * sizeof(float)), IntPtr.Add(outputMask.CudaPtr, offsetOM * sizeof(int)),
                row, col, alpha, beta);
        }

        public void ColumnWiseSumMaskEx(CudaPieceFloat matrix, int offsetM, CudaPieceInt matrixMask, int offsetMM, CudaPieceFloat weight, int skipMatrix, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat output, int offsetO, CudaPieceInt outputMask, int offsetOM, int skipOutput, int row, int col, float alpha, float beta)
        {
            Cudalib.ColumnWiseSumMaskV3(IntPtr.Add(matrix.CudaPtr, offsetM * sizeof(float)), IntPtr.Add(matrixMask.CudaPtr, offsetMM * sizeof(int)),
                weight.CudaPtr, skipMatrix, smpIdx.CudaPtr, batchSize, IntPtr.Add(output.CudaPtr, offsetO * sizeof(float)), IntPtr.Add(outputMask.CudaPtr, offsetOM * sizeof(int)),
                skipOutput, row, col, alpha, beta);
        }

        public void Scale_MatrixMask(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM,
            int dim, int batchSize, CudaPieceFloat awei, float bwei)
        {
            Cudalib.Scale_MatrixMask(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)),
                IntPtr.Add(aMask.CudaPtr, offsetAM * sizeof(int)), IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)),
                IntPtr.Add(bMask.CudaPtr, offsetBM * sizeof(int)), dim, batchSize, awei.CudaPtr, bwei);
        }

        public void Inner_Product_Matching(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, CudaPieceFloat c, int offsetC,
            CudaPieceInt matchIdxA, CudaPieceInt matchIdxB, int aSize, int bSize, int matchSize, int dimension, float eps)
        {
            Cudalib.Inner_Product_Matching(IntPtr.Add(a.CudaPtr, offsetA * sizeof(int)), IntPtr.Add(b.CudaPtr, offsetB * sizeof(int)),
                IntPtr.Add(c.CudaPtr, offsetC * sizeof(int)), matchIdxA.CudaPtr, matchIdxB.CudaPtr, aSize, bSize, matchSize, dimension, eps);
        }

        public void Sgemv(CudaPieceFloat matrix, CudaPieceFloat x, int row, int col, CudaPieceFloat y, bool transM, float alpha, float beta)
        {
            Cudalib.CuBLAS_Sgemv(CuBlasHandle, matrix.CudaPtr, x.CudaPtr, row, col, y.CudaPtr, transM, alpha, beta);
        }

        public void ClipAdaGradUpdate(CudaPieceFloat ada, CudaPieceFloat grad, CudaPieceFloat param, float updateRate, float gradClip, float weightClip)
        {
            if (gradClip > 0) { Cudalib.Clip_Vector(grad.CudaPtr, grad.Size, gradClip, -gradClip); }
            Cudalib.Ada_Gradient(ada.CudaPtr, grad.CudaPtr, grad.Size);
            Cudalib.Add_Vector(param.CudaPtr, grad.CudaPtr, grad.Size, 1, updateRate);
            if (weightClip > 0) { Cudalib.Clip_Vector(param.CudaPtr, param.Size, weightClip, -weightClip); }
            Cudalib.Zero(grad.CudaPtr, grad.Size);
        }

        public void ClipAdamUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, float beta1, float beta2, int iter, float updateRate, float gradClip, float weightClip, float decay)
        {
            float epsilon = 1.0e-8f; //want to change this.
            float new_updateRate = (float)(updateRate * Math.Sqrt(1 - Math.Pow(beta2, iter)) / (1 - Math.Pow(beta1, iter)));  
            Cudalib.CudaClipAdamUpdate(grad.CudaPtr, M.CudaPtr, V.CudaPtr, param.CudaPtr, beta1, beta2, epsilon, new_updateRate, weightClip, gradClip, decay, iter, param.Size);
        }

        public void ClipAdamBertUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, 
                                float beta1, float beta2, float epsilon, float updateRate, float gradClip, float weightClip, float decay, int size)
        {
            Cudalib.ClipAdamBertUpdate(grad.CudaPtr, M.CudaPtr, V.CudaPtr, param.CudaPtr, beta1, beta2, epsilon, updateRate, weightClip, gradClip, decay, 0, size);
        }

        public void ClipAdamDeltaUpdate(CudaPieceFloat M, CudaPieceFloat V, CudaPieceFloat grad, CudaPieceFloat param, CudaPieceFloat orig_param,
                                float beta1, float beta2, float epsilon, float updateRate, float gradClip, float weightClip, float decay, int size)
        {
            Cudalib.ClipAdamDeltaUpdate(grad.CudaPtr, M.CudaPtr, V.CudaPtr, param.CudaPtr, orig_param.CudaPtr, beta1, beta2, epsilon, updateRate, weightClip, gradClip, decay, 0, size);
        }

        public void LogBayesianRatingLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss, CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputDeriv, uint outputSize, float eplison, float gamma)
        {
            Cudalib.LogBayesianRatingLoss(src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, srcBatchSize, srcBatchLoss.CudaPtr, outputScore.CudaPtr, outputLabel.CudaPtr, outputDeriv.CudaPtr, outputSize, eplison, gamma);
        }

        public void AddAndClear(CudaPieceFloat a, CudaPieceFloat b, int m, float bwei)
        {
            this.Add_Vector(a, b, m, 1, bwei);
            this.Zero(b, b.Size);
        }

        public void GetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta)
        {
            Cudalib.GetSeqOrderMatrixs(InstanceIndex.CudaPtr, batchSize, SeqMatrixs.CudaPtr, MapForward.CudaPtr, dim, isReverse, order, matrix.CudaPtr, alpha, beta);
        }

        public void SetSeqOrderMatrixs(CudaPieceInt InstanceIndex, int batchSize, CudaPieceFloat SeqMatrixs, CudaPieceInt MapForward, int dim, int isReverse, int order, CudaPieceFloat matrix, float alpha, float beta)
        {
            Cudalib.SetSeqOrderMatrixs(InstanceIndex.CudaPtr, batchSize, SeqMatrixs.CudaPtr, MapForward.CudaPtr, dim, isReverse, order, matrix.CudaPtr, alpha, beta);
        }

        public void GetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta)
        {
            Cudalib.GetWindowMatrixs(InstanceMargin.CudaPtr, sentsize, SeqMatrixs.CudaPtr, mapForward.CudaPtr, dim, winsize, matrix.CudaPtr, alpha, beta);
        }

        public void SetWindowMatrixs(CudaPieceInt InstanceMargin, int sentsize, CudaPieceFloat SeqMatrixs, CudaPieceInt mapForward, int dim, int winsize, CudaPieceFloat matrix, float alpha, float beta)
        {
            Cudalib.SetWindowMatrixs(InstanceMargin.CudaPtr, sentsize, SeqMatrixs.CudaPtr, mapForward.CudaPtr, dim, winsize, matrix.CudaPtr, alpha, beta);
        }

        

        public void MaxoutPooling(CudaPieceFloat input, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output, int format)
        {
            Cudalib.MaxoutPooling(input.CudaPtr, batchSize, dim, maxpoolingIndex.CudaPtr, maxoutDim, outDim, output.CudaPtr, format);
        }

        public void DerivMaxoutPooling(CudaPieceFloat input_deriv, int batchSize, int dim, CudaPieceInt maxpoolingIndex, int maxoutDim, int outDim, CudaPieceFloat output_deriv)
        {
            Cudalib.DerivMaxoutPooling(input_deriv.CudaPtr, batchSize, dim, maxpoolingIndex.CudaPtr, maxoutDim, outDim, output_deriv.CudaPtr);
        }

        public void MomentumUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, float momentumRate)
        {
            Cudalib.Add_Vector(weight.CudaPtr, momentum.CudaPtr, weight.EffectiveSize, 1, 1);
            Cudalib.Scale_Matrix(momentum.CudaPtr, 1, momentum.EffectiveSize, momentumRate);
        }

        public void NAGUpdate(CudaPieceFloat weight, CudaPieceFloat momentum, CudaPieceFloat trueWeight, float momentumRate)
        {
            Cudalib.Add_Vector(trueWeight.CudaPtr, momentum.CudaPtr, weight.EffectiveSize, 1, 1);
            Cudalib.Scale_Matrix(momentum.CudaPtr, 1, momentum.EffectiveSize, momentumRate);
            Cudalib.Matrix_Addition(momentum.CudaPtr, trueWeight.CudaPtr, weight.CudaPtr, 1, weight.EffectiveSize, 1, 1, 0);
        }

        public void CNNForwardPropagate(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapNum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatch, int outHeight, int outWidth)
        {
            Cudalib.CuDNNForwardPropagate(CuDnnHandle, imageInBatch.CudaPtr, nBatch, inFeatureMap, inHeight, inWidth, filterMap.CudaPtr,
                filterMapNum, filterHeight, filterWidth, pad, stride, imageOutBatch.CudaPtr, outHeight, outWidth);
        }

        public void CNNForwardPropagateEx(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, 
                        int filterHeight, int filterWidth, int padHeight, int padWidth, int strideHeight, int strideWidth, CudaPieceFloat imageOutBatch, int outHeight, int outWidth, float alpha, float beta)
        {
            Cudalib.CuDNNForwardPropagateEx(CuDnnHandle, imageInBatch.CudaPtr, nBatch, inFeatureMap, inHeight, inWidth, filterMap.CudaPtr,
                filterMapBum, filterHeight, filterWidth, padHeight, padWidth, strideHeight, strideWidth, imageOutBatch.CudaPtr, outHeight, outWidth, alpha, beta);
        }

        public void BayesianRatingProb(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize, CudaPieceFloat srcBatchLoss, CudaPieceFloat outputScore, CudaPieceFloat outputLabel, CudaPieceFloat outputProb, int outputSize, float eplison, float gamma)
        {
            Cudalib.BayesianRatingProb(src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, srcBatchSize, srcBatchLoss.CudaPtr, outputScore.CudaPtr, outputLabel.CudaPtr, outputProb.CudaPtr, outputSize, eplison, gamma);
        }

        public void MatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score)
        {
            Cudalib.MatrixL1Norm(x.CudaPtr, dim, batchSize, l1Score.CudaPtr);
        }

        public void MatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score)
        {
            Cudalib.MatrixL2Norm(x.CudaPtr, dim, batchSize, l2Score.CudaPtr);
        }

        public void DerivMatrixL1Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l1Score, CudaPieceFloat l1ScoreDeriv, CudaPieceFloat xDeriv)
        {
            Cudalib.DerivMatrixL1Norm(x.CudaPtr, dim, batchSize, l1Score.CudaPtr, l1ScoreDeriv.CudaPtr, xDeriv.CudaPtr);
        }

        public void DerivMatrixL2Norm(CudaPieceFloat x, int dim, int batchSize, CudaPieceFloat l2Score, CudaPieceFloat l2ScoreDeriv, CudaPieceFloat xDeriv)
        {
            Cudalib.DerivMatrixL1Norm(x.CudaPtr, dim, batchSize, l2Score.CudaPtr, l2ScoreDeriv.CudaPtr, xDeriv.CudaPtr);
        }

        public void SpanMaxPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, CudaPieceInt outMaxIndex, int dim)
        {
            Cudalib.SpanMaxPool(inputSent.CudaPtr, smpIdx.CudaPtr, start.CudaPtr, end.CudaPtr, sentMargin.CudaPtr, matchSize, outputSent.CudaPtr, outMaxIndex.CudaPtr, dim);
        }

        public void DerivSpanMaxPool(CudaPieceFloat outputSentDeriv, CudaPieceInt smpIdx, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt outMaxIndex, int dim, float beta)
        {
            Cudalib.DerivSpanMaxPool(outputSentDeriv.CudaPtr, smpIdx.CudaPtr, batchSize, inputSentDeriv.CudaPtr, outMaxIndex.CudaPtr, dim, beta);
        }

        public void SpanLastPool(CudaPieceFloat inputSent, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, CudaPieceInt sentMargin, int matchSize, CudaPieceFloat outputSent, int dim)
        {
            Cudalib.SpanLastPool(inputSent.CudaPtr, smpIdx.CudaPtr, start.CudaPtr, end.CudaPtr, sentMargin.CudaPtr, matchSize, outputSent.CudaPtr, dim);
        }

        public void DerivSpanLastPool(CudaPieceFloat outputSentDeriv, CudaPieceInt outSmpIdx, CudaPieceFloat start, CudaPieceFloat end, int batchSize, CudaPieceFloat inputSentDeriv, CudaPieceInt inSmpIdx, int dim, float beta)
        //CudaPieceFloat outputSentDeriv, CudaPieceInt smpIdx, CudaPieceFloat start, CudaPieceFloat end, int batchSize, CudaPieceFloat inputSentDeriv, int dim, float beta)
        {
            Cudalib.DerivSpanLastPool(outputSentDeriv.CudaPtr, outSmpIdx.CudaPtr, start.CudaPtr, end.CudaPtr, batchSize, inputSentDeriv.CudaPtr, inSmpIdx.CudaPtr, dim, beta);
        }

        public void AccurateScale_Matrix(CudaPieceFloat a, int offsetA, CudaPieceInt aMask, int offsetAM, CudaPieceFloat b, int offsetB, CudaPieceInt bMask, int offsetBM, int dim, int batchSize, CudaPieceFloat awei)
        {
            Cudalib.Accurate_Scale_Matrix(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)),
                IntPtr.Add(aMask.CudaPtr, offsetAM * sizeof(int)), IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)),
                IntPtr.Add(bMask.CudaPtr, offsetBM * sizeof(int)), dim, batchSize, awei.CudaPtr);
        }

        public void Matrix_AdditionEx(CudaPieceFloat a, int offsetA, int skipA, CudaPieceFloat b, int offsetB, int skipB, CudaPieceFloat c, int offsetC, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            Cudalib.Matrix_AdditionEx(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), skipA,
                                      IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)), skipB,
                                      IntPtr.Add(c.CudaPtr, offsetC * sizeof(float)), skipC,
                                      dim, batchSize, awei, bwei, cwei);
        }

        public void AccurateElementwiseProduct(CudaPieceFloat pLeft, int offsetLeft, CudaPieceFloat pRight, int offsetRight, CudaPieceFloat pDst, int offsetDst,
            CudaPieceInt leftmask, int offsetlm, CudaPieceInt rightmask, int offsetrm, CudaPieceInt dstmask, int offsetdm, int row, int column, float beta)
        {
            Cudalib.AccurateElementwiseProduct(IntPtr.Add(pLeft.CudaPtr, offsetLeft * sizeof(float)),
                                               IntPtr.Add(pRight.CudaPtr, offsetRight * sizeof(float)),
                                               IntPtr.Add(pDst.CudaPtr, offsetDst * sizeof(float)),
                                               IntPtr.Add(leftmask.CudaPtr, offsetlm * sizeof(int)),
                                               IntPtr.Add(rightmask.CudaPtr, offsetrm * sizeof(int)),
                                               IntPtr.Add(dstmask.CudaPtr, offsetdm * sizeof(int)),
                                               row, column, beta);
        }

        public void Convolution_Sparse_Matrix_Multiply_INTEX(CudaPieceInt Smp_Index, int batchsize, CudaPieceInt Seg_Index, CudaPieceInt Seg_Margin, int seg_size, CudaPieceInt Fea_Index, CudaPieceFloat Fea_Value, int elementsize, CudaPieceFloat con_weight, CudaPieceFloat output, int Feature_dimension, int output_dimension, int win_size)
        {
            Cudalib.Convolution_Sparse_Matrix_Multiply_INTEX(Smp_Index.CudaPtr, batchsize, Seg_Index.CudaPtr, Seg_Margin.CudaPtr, seg_size,
                                                            Fea_Index.CudaPtr, Fea_Value.CudaPtr, elementsize, con_weight.CudaPtr, output.CudaPtr,
                                                            Feature_dimension, output_dimension, win_size);
        }

        public void Matrix_AdditionExMask(CudaPieceFloat a, CudaPieceInt aMask, int skipA, CudaPieceFloat b, CudaPieceInt bMask, int skipB, CudaPieceFloat c, CudaPieceInt cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
        {
            Cudalib.Matrix_AdditionExMask(a.CudaPtr, aMask.CudaPtr, skipA, b.CudaPtr, bMask.CudaPtr, skipB, c.CudaPtr, cMask.CudaPtr, skipC, dim, batchSize, awei, bwei, cwei);
        }

        public void SoftAttention_Matching(CudaPieceFloat a, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, CudaPieceInt cMask, int batchSize, float alpha, float beta)
        {
            Cudalib.SoftAttention_Matching(a.CudaPtr, aMask.CudaPtr, b.CudaPtr, bMask.CudaPtr, dim, a_func, op, vec.CudaPtr, c.CudaPtr, cMask.CudaPtr, batchSize, alpha, beta);
        }

        public void DerivSoftAttention_Matching(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceInt aMask, CudaPieceFloat b, CudaPieceFloat bDeriv, CudaPieceInt bMask, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, CudaPieceInt cMask, CudaPieceInt aSmpIdx, CudaPieceInt aElementIdx, int aSize, CudaPieceInt bSmpIdx, CudaPieceInt bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei)
        {
            Cudalib.DerivSoftAttention_Matching(a.CudaPtr, aDeriv.CudaPtr, aMask.CudaPtr, b.CudaPtr, bDeriv.CudaPtr, bMask.CudaPtr, dim, a_func, op, vec.CudaPtr, vecDeriv.CudaPtr, c.CudaPtr, cDeriv.CudaPtr,
                cMask.CudaPtr, aSmpIdx.CudaPtr, aElementIdx.CudaPtr, aSize, bSmpIdx.CudaPtr, bElementIdx.CudaPtr, bSize, batchSize, awei, bwei, vwei);
        }

        public void SoftAttention(CudaPieceFloat a, CudaPieceFloat b, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat c, int aBatchSize, int bBatchSize, float alpha, float beta)
        {
            Cudalib.SoftAttention(a.CudaPtr, b.CudaPtr, dim, a_func, op, vec.CudaPtr, c.CudaPtr, aBatchSize, bBatchSize, alpha, beta);
        }

        public void DerivSoftAttention(CudaPieceFloat a, CudaPieceFloat aDeriv, CudaPieceFloat b, CudaPieceFloat bDeriv, int dim, int a_func, int op, CudaPieceFloat vec, CudaPieceFloat vecDeriv, CudaPieceFloat c, CudaPieceFloat cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei)
        {
            Cudalib.DerivSoftAttention(a.CudaPtr, aDeriv.CudaPtr, b.CudaPtr, bDeriv.CudaPtr, dim, a_func, op, vec.CudaPtr, vec.CudaPtr, c.CudaPtr, cDeriv.CudaPtr, aBatchSize, bBatchSize, awei, bwei, vwei);
        }

        public void CuDNNBackwardPropagateData(CudaPieceFloat imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth)
        {
            Cudalib.CuDNNBackwardPropagateData(CuDnnHandle, imageInBatchDeriv.CudaPtr, nBatch, inFeatureMap, inHeight, inWidth, filterMap.CudaPtr,
                                               filterMapBum, filterHeight, filterWidth, pad, stride, imageOutBatchDeriv.CudaPtr, outHeight, outWidth);
        }

        public void CuDNNBackwardPropagateDataEx(CudaPieceFloat imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth,
                            CudaPieceFloat filterMap, int filterMapBum, int filterHeight, int filterWidth, int padHeight, int padWidth, int strideHeight, int strideWidth,
                            CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alpha, float beta)
        {
            Cudalib.CuDNNBackwardPropagateDataEx(CuDnnHandle, imageInBatchDeriv.CudaPtr, nBatch, inFeatureMap, inHeight, inWidth, filterMap.CudaPtr,
                                               filterMapBum, filterHeight, filterWidth, padHeight, padWidth, strideHeight, strideWidth, imageOutBatchDeriv.CudaPtr, outHeight, outWidth, alpha, beta);
        }

        public void CuDNNBackwardPropagateFilter(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            Cudalib.CuDNNBackwardPropagateFilter(CuDnnHandle, imageInBatch.CudaPtr, nBatch, inFeatureMap, inHeight, inWidth,
                                                filterMapDeriv.CudaPtr, filterMapBum, filterHeight, filterWidth, pad, stride,
                                                imageOutBatchDeriv.CudaPtr, outHeight, outWidth, alphaValue, betaValue);
        }


        public void CuDNNBackwardPropagateFilterEx(CudaPieceFloat imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth, CudaPieceFloat filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int padHeight, int padWidth, int strideHeight, int strideWidth, CudaPieceFloat imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            Cudalib.CuDNNBackwardPropagateFilterEx(CuDnnHandle, imageInBatch.CudaPtr, nBatch, inFeatureMap, inHeight, inWidth,
                                                filterMapDeriv.CudaPtr, filterMapBum, filterHeight, filterWidth, padHeight, padWidth, strideHeight, strideWidth,
                                                imageOutBatchDeriv.CudaPtr, outHeight, outWidth, alphaValue, betaValue);
        }

        public void RMSPropV2_Gradient(CudaPieceFloat ada, CudaPieceFloat g, CudaPieceFloat grad, float gamma, float epsilon, int m)
        {
            Cudalib.RMSPropV2_Gradient(ada.CudaPtr, g.CudaPtr, grad.CudaPtr, gamma, epsilon, m);
        }

        public void CuDNNBackwardPropagateBias(CudaPieceFloat biasDeriv, CudaPieceFloat imageOutBatchDeriv, int batch, int outDepth, int outHeight, int outWidth, float alphaValue, float betaValue)
        {
            Cudalib.CuDNNBackwardPropagateBias(CuDnnHandle, biasDeriv.CudaPtr, imageOutBatchDeriv.CudaPtr, batch, outDepth, outHeight, outWidth, alphaValue, betaValue);
        }

        public void NDArrayTranspose(CudaPieceFloat input, CudaPieceInt indim, CudaPieceInt transDim, CudaPieceFloat output, int dimNum, int length, float alpha, float beta)
        {
            Cudalib.NDArrayTranspose(input.CudaPtr, indim.CudaPtr, transDim.CudaPtr, output.CudaPtr, dimNum, length, alpha, beta);
        }

        public void KLargestValueBatch(CudaPieceFloat float_array, int batchSize, int dim, int K, int mode, CudaPieceFloat bestValues, CudaPieceFloat bestIndexes)
        {
            Cudalib.KLargestValueBatch(float_array.CudaPtr, batchSize, dim, K, mode, bestValues.CudaPtr, bestIndexes.CudaPtr);
        }

        public void DerivLogSparseSoftmax(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, CudaPieceFloat outputDeriv, float gamma, float alpha, float beta, int batchSize)
        {
            Cudalib.DerivLogSparseSoftmax(smpIdx.CudaPtr, outputProb.CudaPtr, logProbDeriv.CudaPtr, outputDeriv.CudaPtr, gamma, alpha, beta, batchSize);
        }

        public void DerivLogProbEntropy(CudaPieceInt smpIdx, CudaPieceFloat outputProb, CudaPieceFloat logProbDeriv, float alpha, float beta, float epislon, int batchSize)
        {
            Cudalib.DerivLogProbEntropy(smpIdx.CudaPtr, outputProb.CudaPtr, logProbDeriv.CudaPtr, alpha, beta, epislon, batchSize);
        }

        public void LookupForward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, CudaPieceFloat weight, CudaPieceFloat output, int inputDim, int outputDim)
        {
            Cudalib.SparseForward(inputSmpIdx.CudaPtr, inputItemIdx.CudaPtr, batchSize, weight.CudaPtr, output.CudaPtr, inputDim, outputDim);
        }
        
        public void LookupBackward(CudaPieceInt inputSmpIdx, CudaPieceInt inputItemIdx, int batchSize, int inputItemNum, CudaPieceFloat deriv, int inputDim, int outputDim, CudaPieceFloat output, float lr)
        {
            Cudalib.SparseBackward(inputSmpIdx.CudaPtr, inputItemIdx.CudaPtr, batchSize, inputItemNum, deriv.CudaPtr, inputDim, outputDim, output.CudaPtr, lr);
        }

        public void SparseVectorAdd(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float beta, int length)
        {
            Cudalib.SparseVectorAdd(CuSparseHandle, idx.CudaPtr, value.CudaPtr, dst.CudaPtr, beta, length);
        }

        public float VectorSum(CudaPieceFloat x, int index, int len, int norm)
        {
            return Cudalib.CUBLAS_Sasum(CuBlasHandle, IntPtr.Add(x.CudaPtr, index * sizeof(float)), len, norm);
        }

        public void SparseVectorGivens(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat dst, float alpha, float beta, int length)
        {
            Cudalib.SparseVectorGivens(CuSparseHandle, idx.CudaPtr, value.CudaPtr, dst.CudaPtr, alpha, beta, length);
        }

        public void Scale_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei)
        {
            IntPtr baddr = IntPtr.Add(b.CudaPtr, offsetB);
            IntPtr aaddr = IntPtr.Add(a.CudaPtr, offsetA);
            
            if(bwei != 1) Cudalib.CuBLAS_Scal(CuBlasHandle, baddr, bwei, len);
            if(awei != 0) Cudalib.CuBLAS_Saxpy(CuBlasHandle, aaddr, awei, baddr, len);
        }

        public void Init_Vector(CudaPieceFloat a, int offsetA, float b, int m, float awei)
        {
            Cudalib.Init_Vector(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), b, m, awei);
        }

        public void Log(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, float alpha, float beta, int size)
        {
            Cudalib.Log(IntPtr.Add(a.CudaPtr, offsetA * sizeof(float)), 
                        IntPtr.Add(b.CudaPtr, offsetB * sizeof(float)),
                        alpha, beta, size);
        }

        public void GraphNeighborNum(CudaPieceInt srcIdx, int batchSize, CudaPieceInt graphIdx, CudaPieceInt graphNeiNodes, CudaPieceInt graphNeiRels, 
                            CudaPieceInt maskSrc, CudaPieceInt maskRel, CudaPieceInt maskInvRel, CudaPieceInt maskTgt, CudaPieceInt neiNum, int maskType)
        {
            Cudalib.GraphNeighborNum(srcIdx.CudaPtr, batchSize, graphIdx.CudaPtr, graphNeiNodes.CudaPtr, graphNeiRels.CudaPtr,
                                    maskSrc.CudaPtr, maskRel.CudaPtr, maskInvRel.CudaPtr, maskTgt.CudaPtr, neiNum.CudaPtr, maskType);
        }

        // calculate neighbor info.
        public void GraphNeighborInfo(CudaPieceInt srcIdx, int batchSize, CudaPieceInt graphIdx, CudaPieceInt graphNeiNodes, CudaPieceInt graphNeiRels, 
                               CudaPieceInt maskSrc, CudaPieceInt maskRel, CudaPieceInt maskInvRel, CudaPieceInt maskTgt, CudaPieceInt neiNum,
                               CudaPieceInt neiNodes, CudaPieceInt neiRels, CudaPieceInt neiMargin, int defaultR, int maskType)
        {
            Cudalib.GraphNeighborInfo(srcIdx.CudaPtr, batchSize, graphIdx.CudaPtr, graphNeiNodes.CudaPtr, graphNeiRels.CudaPtr,
                                    maskSrc.CudaPtr, maskRel.CudaPtr, maskInvRel.CudaPtr, maskTgt.CudaPtr, neiNum.CudaPtr,
                                    neiNodes.CudaPtr, neiRels.CudaPtr, neiMargin.CudaPtr, defaultR, maskType);
        }

        public void CuSparseSgemm(CudaPieceFloat A, CudaPieceInt zero_bIndex, CudaPieceInt bfeaIndex, CudaPieceFloat bfeaValue, int elementSize, 
                                  CudaPieceFloat C, int batchSize, int aDim, int bDim, float alpha, float beta)
        {
            Cudalib.CuSparseSgemm(CuSparseHandle, A.CudaPtr, zero_bIndex.CudaPtr, bfeaIndex.CudaPtr, bfeaValue.CudaPtr, elementSize, 
                                  C.CudaPtr, batchSize, aDim, bDim, alpha, beta);
        }

        public void CuCsr2Csc(CudaPieceInt aIndex, CudaPieceInt afeaIndex, CudaPieceFloat afeaValue,
                       CudaPieceInt bIndex, CudaPieceInt bfeaIndex, CudaPieceFloat bfeaValue,
                                                   int elementSize, int aRow, int aCol)
        {
            Cudalib.CuCsr2Csc(CuSparseHandle, aIndex.CudaPtr, afeaIndex.CudaPtr, afeaValue.CudaPtr, 
                                              bIndex.CudaPtr, bfeaIndex.CudaPtr, bfeaValue.CudaPtr,
                                              elementSize, aRow, aCol);
        }

        public void Inner_Product_v1(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, CudaPieceInt matchIdxA, CudaPieceInt matchIdxB, int aSize, int bSize, int matchSize, int dimension, float alpha, float beta)
        {
            Cudalib.Inner_Product_v1(a.CudaPtr, b.CudaPtr, c.CudaPtr, matchIdxA.CudaPtr, matchIdxB.CudaPtr, aSize, bSize, matchSize, dimension, alpha, beta);
        }

        public void Maxpooling1D(CudaPieceFloat values, int row, int column, int batchsize, CudaPieceFloat output, CudaPieceInt maxpooling_index)
        {
            Cudalib.Maxpooling1D(values.CudaPtr, row, column, batchsize, output.CudaPtr, maxpooling_index.CudaPtr);
        }
        
        public void Meanpooling1D(CudaPieceFloat values, int row, int column, int batchsize, CudaPieceFloat output)
        {
            Cudalib.Meanpooling1D(values.CudaPtr, row, column, batchsize, output.CudaPtr);
        }

        public void SparseGthr(CudaPieceInt idx, CudaPieceFloat value, CudaPieceFloat src, int length)
        {
            Cudalib.SparseGthr(CuSparseHandle, idx.CudaPtr, value.CudaPtr, src.CudaPtr, length);
        }

        public void SgemmStrideBatched(CudaPieceFloat tensorA, int rowA, int colA, CudaPieceFloat tensorB, int rowB, int colB, int batch, 
                    CudaPieceFloat tensorC, int transA, int transB, float alpha, float beta)
        {
            Cudalib.SgemmStrideBatched(CuBlasHandle, tensorA.CudaPtr, rowA, colA, tensorB.CudaPtr, rowB, colB, batch, tensorC.CudaPtr, transA, transB, alpha, beta);
        }

        public void ElementwiseProductEx(CudaPieceFloat pLeft, int strideLeft, int leftLen, CudaPieceFloat pRight, int strideRight, int rightLen, CudaPieceFloat pDst, int strideDst, int dstLen, float alpha, float beta)
        {
            Cudalib.ElementwiseProductEx(pLeft.CudaPtr, strideLeft, leftLen, pRight.CudaPtr, strideRight, rightLen, pDst.CudaPtr, strideDst, dstLen, alpha, beta);
        }

        //Input.Output, Input.Width, Input.Height, Input.Depth, Input.BatchSize, 
        //                                        SXWidth, SXHeight, 0, 0, StrideWidth, StrideHeight, 
        //                                        Output.Output, Output.Width, Output.Height, 0, 1);

        public void CuDNNMaxPooling2DForward(CudaPieceFloat input, int in_width, int in_height, int depth, int batch, 
                                      int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
                                      CudaPieceFloat output, int out_width, int out_height, float alpha, float beta)
        {
            Cudalib.CuDNNMaxPooling2DForward(CuDnnHandle, input.CudaPtr, in_width, in_height, depth, batch, 
                                                sxWidth, sxHeight, padWidth, padHeight, strideWidth, strideHeight,
                                                output.CudaPtr, out_width, out_height, alpha, beta);
        }

        public void CuDNNMaxPooling2DBackward(CudaPieceFloat input, CudaPieceFloat dx, int in_width, int in_height, int depth, int batch, 
                                        int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
                                        CudaPieceFloat output, CudaPieceFloat dy, int out_width, int out_height, float alpha, float beta)
        {
            Cudalib.CuDNNMaxPooling2DBackward(CuDnnHandle, input.CudaPtr, dx.CudaPtr, in_width, in_height, depth, batch, 
                                                sxWidth, sxHeight, padWidth, padHeight, strideWidth, strideHeight, 
                                                output.CudaPtr, dy.CudaPtr, out_width, out_height, alpha, beta);
        }


        public IntPtr RegisterDropout(int n, int c, int h, int w, float drop)
        {
            return Cudalib.RegisterDropout(CuDnnHandle, n, c, h, w, drop);
        }

        // dropout forward.
        public void CuDNNDropoutForward(CudaPieceFloat x, int batch, int depth, int height, int width, 
                                 CudaPieceFloat y, float drop, IntPtr state)
        {
            Cudalib.CuDNNDropoutForward(CuDnnHandle, x.CudaPtr, batch, depth, height, width, 
                                                     y.CudaPtr, drop, state);
        }

        // dropout backward.
        public void CuDNNDropoutBackward(CudaPieceFloat dy, int batch, int depth, int height, int width, 
                                         CudaPieceFloat dx, float drop, IntPtr state)
        {
            Cudalib.CuDNNDropoutBackward(CuDnnHandle, dy.CudaPtr, batch, depth, height, width,
                                                      dx.CudaPtr, drop, state);
        }
        
        // UnregisterDropout state.
        public void UnregisterDropout(IntPtr state)
        {
            Cudalib.UnregisterDropout(state);
        }

        // vector mul
        // c = alpah c + beta ( a \dot b );
        public void CuDNNVectorMul(CudaPieceFloat a, CudaPieceFloat b, CudaPieceFloat c, float alpha, float beta, int length)
        {
            if(CuDnnHandle == IntPtr.Zero)
            {
                throw new Exception("cudnn handle is not initialized.");
            }
            Cudalib.CuDNNVectorMul(CuDnnHandle, a.CudaPtr, b.CudaPtr, c.CudaPtr, alpha, beta, length);
        }

        // vector sqrt.
        // b = alpha b + beta sqrt(a); 
        public void Sqrt(CudaPieceFloat a, CudaPieceFloat b, float alpha, float beta, int length)
        {
            Cudalib.Sqrt(a.CudaPtr, b.CudaPtr, alpha, beta, length);
        }

        // one rank update.
        public void Sger(CudaPieceFloat x, CudaPieceFloat y, CudaPieceFloat matrix, int row, int col, float beta)
        {
            Cudalib.CuBLAS_Sger(CuBlasHandle, x.CudaPtr, y.CudaPtr, matrix.CudaPtr, row, col, beta);
        }

        public unsafe IntPtr RegisteTensorReduce(int reduceOp, int dimNum, int[] aDims, int[] cDims)
        {
            fixed(int * pa = &aDims[0])
            fixed(int * pc = &cDims[0])
            {
                return Cudalib.CuDNNRegisteReduce(CuDnnHandle, reduceOp, dimNum, (IntPtr)pa, (IntPtr)pc);
            }
        }

        public unsafe IntPtr RegisteTensorLazyReduce(int reduceOp, int dimNum, int[] aDims, int[] cDims)
        {
            fixed(int * pa = &aDims[0])
            fixed(int * pc = &cDims[0])
            {
                return Cudalib.CuDNNRegisteLazyReduce(CuDnnHandle, reduceOp, dimNum, (IntPtr)pa, (IntPtr)pc);
            }
        }
        

        public void UnRegisteTensorReduce(IntPtr state)
        {
            Cudalib.CuDNNUnRegisteReduce(state);
        }

        public void TensorReduceOp(CudaPieceFloat a, CudaPieceFloat c, float alpha, float beta, IntPtr state)
        {
            Cudalib.CuDNNReduceOp(CuDnnHandle, a.CudaPtr, c.CudaPtr, alpha, beta, state);
        }

        public void Reciprocal(CudaPieceFloat a, CudaPieceFloat b, float alpha, float beta, int size)
        {
            Cudalib.Reciprocal(a.CudaPtr, b.CudaPtr, alpha, beta, size);
        }

        public unsafe void TensorOp(int opType, int dimNum, int[] aDims, CudaPieceFloat a, 
                                                     int[] bDims, CudaPieceFloat b, 
                                                     int[] cDims, CudaPieceFloat c, 
                                                     float beta1, float beta2, float alpha)
        {
            fixed(int * pa = &aDims[0])
            fixed(int * pb = &bDims[0])
            fixed(int * pc = &cDims[0])
            {
                Cudalib.CuDNNTensorOp(CuDnnHandle, opType, dimNum, (IntPtr)pa, a.CudaPtr,
                                                                   (IntPtr)pb, b.CudaPtr,
                                                                   (IntPtr)pc, c.CudaPtr,
                                                                   beta1, beta2, alpha);        
            }
        }

        public void Gelu(CudaPieceFloat x, CudaPieceFloat y, int size)
        {
            Cudalib.Gelu(x.CudaPtr, y.CudaPtr, size);
        }

        public void DerivGelu(CudaPieceFloat x, CudaPieceFloat y, CudaPieceFloat dy, CudaPieceFloat dx, int size)
        {
            Cudalib.DerivGelu(x.CudaPtr, y.CudaPtr, dy.CudaPtr, dx.CudaPtr, size);
        }

        public void NDArraySliceForward(CudaPieceFloat input, int dimNum, CudaPieceInt indim, CudaPieceFloat output, CudaPieceInt offset, CudaPieceInt outdim, int length, float alpha, float beta)
        {
            Cudalib.NDArraySliceForward(input.CudaPtr, dimNum, indim.CudaPtr, output.CudaPtr, offset.CudaPtr, outdim.CudaPtr, length, alpha, beta);
        }
        
        public void NDArraySliceBackward(CudaPieceFloat outputDeriv, int dimNum, CudaPieceInt offset, CudaPieceInt outdim, CudaPieceFloat inputDeriv, CudaPieceInt indim, int length, float alpha, float beta)
        {
            Cudalib.NDArraySliceBackward(outputDeriv.CudaPtr, dimNum, offset.CudaPtr, outdim.CudaPtr, inputDeriv.CudaPtr, indim.CudaPtr, length, alpha, beta);
        }

        public float DotProduct(CudaPieceFloat x, CudaPieceFloat y, int len)
        {   
            return Cudalib.CUBLAS_Sdot(CuBlasHandle, x.CudaPtr, y.CudaPtr, len);
        }

        public void DerivProbEntropy(CudaPieceFloat probDeriv, CudaPieceFloat prob, float alpha, float beta, float epislon, int dim, int batchSize)
        {
            Cudalib.DerivProbEntropy(probDeriv.CudaPtr, prob.CudaPtr, alpha, beta, epislon, dim, batchSize);
        }

        public void CuDNNMeanPooling2DForward(CudaPieceFloat input, int in_width, int in_height, int depth, int batch, 
                                      int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
                                      CudaPieceFloat output, int out_width, int out_height, float alpha, float beta)
        {
            Cudalib.CuDNNMeanPooling2DForward(CuDnnHandle, input.CudaPtr, in_width, in_height, depth, batch, 
                                                sxWidth, sxHeight, padWidth, padHeight, strideWidth, strideHeight,
                                                output.CudaPtr, out_width, out_height, alpha, beta);
        }

        public void CuDNNMeanPooling2DBackward(CudaPieceFloat input, CudaPieceFloat dx, int in_width, int in_height, int depth, int batch, 
                                        int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
                                        CudaPieceFloat output, CudaPieceFloat dy, int out_width, int out_height, float alpha, float beta)
        {
            Cudalib.CuDNNMeanPooling2DBackward(CuDnnHandle, input.CudaPtr, dx.CudaPtr, in_width, in_height, depth, batch, 
                                                sxWidth, sxHeight, padWidth, padHeight, strideWidth, strideHeight, 
                                                output.CudaPtr, dy.CudaPtr, out_width, out_height, alpha, beta);
        }

        public int GetReduceIndiceSize(IntPtr state)
        {
            return Cudalib.GetReduceStateIndiceSize(state);
        }

        public int GetReduceWorkspaceSize(IntPtr state)
        {
            return Cudalib.GetReduceStateWorkspaceSize(state);
        }

        public void CudaDeallocMem(IntPtr mem)
        {
            Cudalib.CudaDeallocMem(mem);
        }

        public IntPtr CudaAllocMem(int size)
        {
            return Cudalib.CudaAllocMem(size);
        }

        public int AssignReduceState(IntPtr state, IntPtr indicePtr, int indiceSize, IntPtr workspacePtr, int workspaceSize)
        {
            Cudalib.AssignReduceState(state, indicePtr, indiceSize, workspacePtr, workspaceSize);
            return 0;
        }

        public void CuDNNBatchNormalizationForwardTraining(CudaPieceFloat x, int batch, int depth, int height, int width, CudaPieceFloat y, CudaPieceFloat bnScale, CudaPieceFloat bnBias, float momentum, float alpha, float beta, CudaPieceFloat runningMean, CudaPieceFloat runningVariance, CudaPieceFloat saveMean, CudaPieceFloat saveVariance)
        {
            Cudalib.CuDNNBatchNormalizationForwardTraining(CuDnnHandle, x.CudaPtr, batch, depth, height, width, y.CudaPtr, bnScale.CudaPtr, bnBias.CudaPtr, momentum, alpha, beta, runningMean.CudaPtr, runningVariance.CudaPtr, saveMean.CudaPtr, saveVariance.CudaPtr);

        }

        // normalization forward inference.
        public void CuDNNBatchNormalizationForwardInference(CudaPieceFloat x, int batch, int depth, int height, int width, CudaPieceFloat y, CudaPieceFloat bnScale, CudaPieceFloat bnBias, float alpha, float beta, CudaPieceFloat runningMean, CudaPieceFloat runningVariance)
        {
            Cudalib.CuDNNBatchNormalizationForwardInference(CuDnnHandle, x.CudaPtr, batch, depth, height, width, y.CudaPtr, bnScale.CudaPtr, bnBias.CudaPtr, alpha, beta, runningMean.CudaPtr, runningVariance.CudaPtr);
        }

        // normalization backward.
        public void CuDNNBatchNormalizationBackward(CudaPieceFloat x, int batch, int depth, int height, int width, CudaPieceFloat dy, CudaPieceFloat dx, CudaPieceFloat bnScale, float alpha, float beta, float param_alpha, float param_beta, CudaPieceFloat saveMean, CudaPieceFloat saveVariance, CudaPieceFloat dbnScale, CudaPieceFloat dbnBias)
        {
            Cudalib.CuDNNBatchNormalizationBackward(CuDnnHandle, x.CudaPtr, batch, depth, height, width, dy.CudaPtr, dx.CudaPtr, bnScale.CudaPtr, alpha, beta, param_alpha, param_beta, saveMean.CudaPtr, saveVariance.CudaPtr, dbnScale.CudaPtr, dbnBias.CudaPtr);
        }

        public void Update_Vector(CudaPieceFloat src, CudaPieceFloat delta, int size, float decay)
        {
            Cudalib.Update_Vector(src.CudaPtr, delta.CudaPtr, size, decay);
        }

        public int GetReduceIndices(IntPtr state, CudaPieceInt indices, int length)
        {
            return Cudalib.GetReduceIndices(state, indices.CudaPtr, length);
        }

        public void NDArrayMaxBackward(CudaPieceFloat input, CudaPieceInt sparse_index, CudaPieceInt indim, CudaPieceFloat output, CudaPieceInt outDim, int max_dim, int dimNum, int length, float beta)
        {
            Cudalib.NDArrayMaxBackward(input.CudaPtr, sparse_index.CudaPtr, indim.CudaPtr, output.CudaPtr, outDim.CudaPtr, max_dim, dimNum, length, beta);
        }

        public void NDArray2SegForward(CudaPieceFloat input, int dim, int max_seq_len, int batch_size, CudaPieceInt sampleIdx, CudaPieceInt sentMargin, int sent_size, CudaPieceFloat output)
        {
            Cudalib.NDArray2SegForward(input.CudaPtr, dim, max_seq_len, batch_size, sampleIdx.CudaPtr, sentMargin.CudaPtr, sent_size, output.CudaPtr);
        }

        public void NDArray2SegBackward(CudaPieceFloat inputDeriv, int dim, int max_seq_len, int batch_size, CudaPieceInt sampleIdx, CudaPieceInt sentMargin, int sent_size, CudaPieceFloat outputDeriv, float beta)
        {
            Cudalib.NDArray2SegBackward(inputDeriv.CudaPtr, dim, max_seq_len, batch_size, sampleIdx.CudaPtr, sentMargin.CudaPtr, sent_size, outputDeriv.CudaPtr, beta);
        }

        public void GenerateRand(CudaPieceFloat x, int len)
        {
            Cudalib.CudaGenerateRand(CuRandHandle, x.CudaPtr, len);
        }

        public void Gumbel(CudaPieceFloat x, float eps, int size)
        {
            Cudalib.Gumbel(x.CudaPtr, eps, size);
        }

        public void Exp(CudaPieceFloat a, CudaPieceFloat b, float alpha, float beta, int size)
        {
            Cudalib.Exp(a.CudaPtr, b.CudaPtr, alpha, beta, size);
        }

        public void CuDNNTensorArgmaxK(CudaPieceFloat input, int k, CudaPieceFloat output, CudaPieceInt local_indices, CudaPieceInt global_indices, int olength, CudaPieceInt out_dim, CudaPieceInt in_index, int max_dim, int dim_num, IntPtr state)
        {
            Cudalib.CuDNNTensorArgmaxK(CuDnnHandle, CuSparseHandle, input.CudaPtr, k, output.CudaPtr, local_indices.CudaPtr, global_indices.CudaPtr, olength, out_dim.CudaPtr, in_index.CudaPtr, max_dim, dim_num, state);
        }

        public void Fast_KLargestValueBatch(CudaPieceFloat float_array, int batchSize, int dim, int K, int mode, CudaPieceFloat bestValues, CudaPieceInt bestIndexes, CudaPieceFloat _bestValues, CudaPieceInt _bestIndexes)
        {
            Cudalib.Fast_KLargestValueBatch(float_array.CudaPtr, batchSize, dim, K, mode, bestValues.CudaPtr, bestIndexes.CudaPtr, _bestValues.CudaPtr, _bestIndexes.CudaPtr);
        }

        public void NDArrayScatter(CudaPieceFloat memory, int dimNum, CudaPieceInt indim, CudaPieceFloat data, CudaPieceInt dimindex, int length, float mem_alpha, float mem_beta, float data_alpha, float data_beta)
        {
            Cudalib.NDArrayScatter(memory.CudaPtr, dimNum, indim.CudaPtr, data.CudaPtr, dimindex.CudaPtr, length, mem_alpha, mem_beta, data_alpha, data_beta);
        }
        
        //public void NDArraySliceForwardV2(CudaPieceFloat input, int dimNum, CudaPieceInt indim, CudaPieceFloat output, CudaPieceInt dimindex, CudaPieceInt dimoffset, CudaPieceInt outdim, int length, float alpha, float beta)
        //{
        //    Cudalib.NDArraySliceForwardV2(input.CudaPtr, dimNum, indim.CudaPtr, output.CudaPtr, dimindex.CudaPtr, dimoffset.CudaPtr, outdim.CudaPtr, length, alpha, beta);
        //}

        //public void NDArraySliceBackwardV2(CudaPieceFloat outputDeriv, int dimNum, CudaPieceInt indim, CudaPieceFloat inputDeriv, CudaPieceInt dimindex, CudaPieceInt dimoffset, CudaPieceInt outdim, int length, float beta)
        //{
        //    Cudalib.NDArraySliceBackwardV2(outputDeriv.CudaPtr, dimNum, indim.CudaPtr, inputDeriv.CudaPtr, dimindex.CudaPtr, dimoffset.CudaPtr, outdim.CudaPtr, length, beta);
        //}

        //public void NDArrayScatterForward(CudaPieceFloat memory, int dimNum, CudaPieceInt indim, CudaPieceFloat data, CudaPieceInt dimindex, CudaPieceInt dimoffset, CudaPieceInt scatdim, int length, float beta)
        //{
        //    Cudalib.NDArrayScatterForward(memory.CudaPtr, dimNum, indim.CudaPtr, data.CudaPtr, dimindex.CudaPtr, dimoffset.CudaPtr, scatdim.CudaPtr, length, beta);
        //}

        //public void NDArrayScatterBackward(CudaPieceFloat memoryDeriv, int dimNum, CudaPieceInt indim, CudaPieceFloat dataDeriv, CudaPieceInt dimindex, CudaPieceInt dimoffset, CudaPieceInt scatdim, int length, float beta)
        //{
        //    Cudalib.NDArrayScatterBackward(memoryDeriv.CudaPtr, dimNum, indim.CudaPtr, dataDeriv.CudaPtr, dimindex.CudaPtr, dimoffset.CudaPtr, scatdim.CudaPtr, length, beta);
        //}
    }
}
