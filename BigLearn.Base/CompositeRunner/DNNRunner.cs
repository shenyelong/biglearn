﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class DNNRunner<IN> : StructRunner<DNNStructure, IN> where IN : BatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public override IN Input { get { return (IN)LinkRunners.First().Input; } set { LinkRunners.First().Input = value; } }

        public List<StructRunner> LinkRunners = new List<StructRunner>();

        public DNNRunner(DNNStructure model, SeqSparseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            int CNNLevel = 0;
            for (int i = 0; i < model.NeuralLinks.Count; i++)
                if (model.NeuralLinks[i].Nt == N_Type.Convolution_layer) CNNLevel = i;

            if (CNNLevel > 0)
            {
                LinkRunners.Add(new SeqSparseConvRunner<SeqSparseBatchData>((model.NeuralLinks[0]), data, Behavior));
            }
            else
            {
                LinkRunners.Add(new ConvSparseRunner<SeqSparseBatchData>(model.NeuralLinks[0], data, Behavior));
            }

            for (int i = 1; i < model.NeuralLinks.Count; i++)
            {
                if (i > CNNLevel) LinkRunners.Add(new FullyConnectHiddenRunner<HiddenBatchData>(model.NeuralLinks[i],
                     (HiddenBatchData)LinkRunners.Last().Output, Behavior));
                else if (i < CNNLevel) LinkRunners.Add(new SeqDenseConvRunner<SeqDenseBatchData>((model.NeuralLinks[i]),
                        (SeqDenseBatchData)LinkRunners.Last().Output, Behavior));
                else LinkRunners.Add(new ConvDenseRunner<SeqDenseBatchData>(model.NeuralLinks[i],
                    (SeqDenseBatchData)LinkRunners.Last().Output, Behavior));
            }
            Output = (HiddenBatchData)LinkRunners.Last().Output;
        }


        public DNNRunner(DNNStructure model, GeneralBatchInputData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            LinkRunners.Add(new FullyConnectInputRunner<GeneralBatchInputData>(model.NeuralLinks[0], data, Behavior));
            for (int i = 1; i < model.NeuralLinks.Count; i++)
                LinkRunners.Add(new FullyConnectHiddenRunner<HiddenBatchData>(model.NeuralLinks[i], (HiddenBatchData)LinkRunners.Last().Output, Behavior));
            Output = (HiddenBatchData)LinkRunners.Last().Output;
        }

        public DNNRunner(DNNStructure model, HiddenBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            LinkRunners.Add(new FullyConnectHiddenRunner<HiddenBatchData>(model.NeuralLinks[0], data, Behavior));
            for (int i = 1; i < model.NeuralLinks.Count; i++)
                LinkRunners.Add(new FullyConnectHiddenRunner<HiddenBatchData>(model.NeuralLinks[i], (HiddenBatchData)LinkRunners.Last().Output, Behavior));
            Output = (HiddenBatchData)LinkRunners.Last().Output;
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in LinkRunners) runner.CleanDeriv();
        }

        public override void Forward()
        {
            //if(name.StartsWith("V_SCORE"))
            //{
            //    ((HiddenBatchData)Input).Output.Data.SyncToCPU();
            //}
            //var perf_dnn = PerfCounter.Manager.Instance["dnn_forward"].Begin();

            foreach (StructRunner runner in LinkRunners) runner.Forward();

            //PerfCounter.Manager.Instance["dnn_forward"].TakeCount(perf_dnn);
        }

        public override void Backward(bool cleanDeriv)
        {
            LinkRunners.Reverse();
            foreach (StructRunner runner in LinkRunners) runner.Backward(cleanDeriv);
            LinkRunners.Reverse();

            foreach (StructRunner runner in LinkRunners) { runner.Update(); }
        }

        public override void Update()
        {
            //var perf_dnn = PerfCounter.Manager.Instance["dnn_update"].Begin();
            //PerfCounter.Manager.Instance["dnn_update"].TakeCount(perf_dnn);
        }

    }
}
