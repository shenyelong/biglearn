using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class DynMemRunner : StructRunner<Structure, BatchData> //StructRunner
    {
        public NdArrayData MemOldKey { get; set; }
        public NdArrayData MemOldValue { get; set; }

        public NdArrayData MemNewKey { get; set; }
        public NdArrayData MemNewValue { get; set; }

        public NdArrayData MemMaskScale { get; set; }
        public NdArrayData MemMaskBias { get; set; }

        public DynMemRunner(NdArrayData memKey, NdArrayData memValue, int incrSize, RunnerBehavior behavior) : base(Structure.Empty, behavior) //base(behavior)
        {
            MemOldKey = memKey;
            MemOldValue = memValue;

            IntArgument active_size = new IntArgument("act-len", memKey.Dimensions[1].Default + incrSize);

            MemNewKey = new NdArrayData(behavior.Device, memKey.Dimensions[0], active_size, memKey.Dimensions[2], memKey.Dimensions[3]);
            MemNewValue = new NdArrayData(behavior.Device, memValue.Dimensions[0], active_size, memValue.Dimensions[2], memValue.Dimensions[3]);

            IntArgument default_size = new IntArgument("default-size", 1);
            
            MemMaskScale = new NdArrayData(behavior.Device, false, active_size, default_size, default_size, default_size);
            MemMaskBias = new NdArrayData(behavior.Device, false, active_size, default_size, default_size, default_size);
            for(int i = 0; i < active_size.Default; i++)
            {
                if(i < memKey.Dimensions[1].Default) 
                {
                    MemMaskScale.Output[i] = 1;
                    MemMaskBias.Output[i] = 0;
                }
                else
                { 
                    MemMaskScale.Output[i] = 0;
                    MemMaskBias.Output[i] = (float)-1e9;
                }
            }
            MemMaskScale.Output.SyncFromCPU();
            MemMaskBias.Output.SyncFromCPU();
        }

        public override void Forward()
        {
            ComputeLib.Matrix_AdditionEx(MemOldKey.Output, 0, MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default,
                                         MemOldKey.Output, 0, MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default,
                                         MemNewKey.Output, 0, MemNewKey.Dimensions[0].Default * MemNewKey.Dimensions[1].Default,
                                         MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default, 
                                         MemOldKey.Dimensions[2].Default * MemOldKey.Dimensions[3].Default, 1, 0, 0);

            ComputeLib.Matrix_AdditionEx(MemOldValue.Output, 0, MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default,
                                         MemOldValue.Output, 0, MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default,
                                         MemNewValue.Output, 0, MemNewValue.Dimensions[0].Default * MemNewValue.Dimensions[1].Default,
                                         MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default, 
                                         MemOldValue.Dimensions[2].Default * MemOldValue.Dimensions[3].Default, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(MemNewValue.Deriv, MemNewValue.Length);
            ComputeLib.Zero(MemNewKey.Deriv, MemNewKey.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Matrix_AdditionEx(MemNewKey.Deriv, 0, MemNewKey.Dimensions[0].Default * MemNewKey.Dimensions[1].Default,
                                         MemNewKey.Deriv, 0, MemNewKey.Dimensions[0].Default * MemNewKey.Dimensions[1].Default,
                                         MemOldKey.Deriv, 0, MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default,
                                         MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default,
                                         MemOldKey.Dimensions[2].Default * MemOldKey.Dimensions[3].Default, 1, 0, 1);

            ComputeLib.Matrix_AdditionEx(MemNewValue.Deriv, 0, MemNewValue.Dimensions[0].Default * MemNewValue.Dimensions[1].Default,
                                         MemNewValue.Deriv, 0, MemNewValue.Dimensions[0].Default * MemNewValue.Dimensions[1].Default,
                                         MemOldValue.Deriv, 0, MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default,
                                         MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default,
                                         MemOldValue.Dimensions[2].Default * MemOldValue.Dimensions[3].Default, 1, 0, 1);
        }

    }

    public class DynMemMaskRunner : StructRunner<Structure, BatchData> //StructRunner
    {
        public NdArrayData MemOldKey { get; set; }
        public NdArrayData MemOldValue { get; set; }

        public NdArrayData MemNewKey { get; set; }
        public NdArrayData MemNewValue { get; set; }

        public NdArrayData OldMaskScale { get; set; }
        public NdArrayData OldMaskBias { get; set; }

        public NdArrayData NewMaskScale { get; set; }
        public NdArrayData NewMaskBias { get; set; }

        public DynMemMaskRunner(NdArrayData memKey, NdArrayData memValue, NdArrayData maskScale, NdArrayData maskBias, int incrSize, RunnerBehavior behavior) : base(Structure.Empty, behavior) //base(behavior)
        {
            MemOldKey = memKey;
            MemOldValue = memValue;

            IntArgument active_size = new IntArgument("act-len", memKey.Dimensions[1].Default + incrSize);
            IntArgument default_size = new IntArgument("default-size", 1);

            MemNewKey = new NdArrayData(behavior.Device, memKey.Dimensions[0], active_size, memKey.Dimensions[2], memKey.Dimensions[3]);
            MemNewValue = new NdArrayData(behavior.Device, memValue.Dimensions[0], active_size, memValue.Dimensions[2], memValue.Dimensions[3]);

            OldMaskScale = maskScale;
            OldMaskBias = maskBias;

            NewMaskScale = new NdArrayData(behavior.Device, false, active_size, default_size, default_size, OldMaskScale.Dimensions[3]);
            NewMaskScale.Output.Init(0);

            NewMaskBias = new NdArrayData(behavior.Device, false, active_size, default_size, default_size, OldMaskBias.Dimensions[3]);
            NewMaskBias.Output.Init(((float)-1e9));
        }

        public override void Forward()
        {
            ComputeLib.Matrix_AdditionEx(MemOldKey.Output, 0, MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default,
                                         MemOldKey.Output, 0, MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default,
                                         MemNewKey.Output, 0, MemNewKey.Dimensions[0].Default * MemNewKey.Dimensions[1].Default,
                                         MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default, 
                                         MemOldKey.Dimensions[2].Default * MemOldKey.Dimensions[3].Default, 1, 0, 0);

            ComputeLib.Matrix_AdditionEx(MemOldValue.Output, 0, MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default,
                                         MemOldValue.Output, 0, MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default,
                                         MemNewValue.Output, 0, MemNewValue.Dimensions[0].Default * MemNewValue.Dimensions[1].Default,
                                         MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default, 
                                         MemOldValue.Dimensions[2].Default * MemOldValue.Dimensions[3].Default, 1, 0, 0);

            ComputeLib.Matrix_AdditionEx(OldMaskScale.Output, 0, OldMaskScale.Dimensions[0].Default * OldMaskScale.Dimensions[1].Default,
                                         OldMaskScale.Output, 0, OldMaskScale.Dimensions[0].Default * OldMaskScale.Dimensions[1].Default,
                                         NewMaskScale.Output, 0, NewMaskScale.Dimensions[0].Default * NewMaskScale.Dimensions[1].Default,
                                         OldMaskScale.Dimensions[0].Default * OldMaskScale.Dimensions[1].Default, 
                                         OldMaskScale.Dimensions[2].Default * OldMaskScale.Dimensions[3].Default, 1, 0, 0);

            ComputeLib.Matrix_AdditionEx(OldMaskBias.Output, 0, OldMaskBias.Dimensions[0].Default * OldMaskBias.Dimensions[1].Default,
                                         OldMaskBias.Output, 0, OldMaskBias.Dimensions[0].Default * OldMaskBias.Dimensions[1].Default,
                                         NewMaskBias.Output, 0, NewMaskBias.Dimensions[0].Default * NewMaskBias.Dimensions[1].Default,
                                         OldMaskBias.Dimensions[0].Default * OldMaskBias.Dimensions[1].Default, 
                                         OldMaskBias.Dimensions[2].Default * OldMaskBias.Dimensions[3].Default, 1, 0, 0);
            

        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(MemNewValue.Deriv, MemNewValue.Length);
            ComputeLib.Zero(MemNewKey.Deriv, MemNewKey.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Matrix_AdditionEx(MemNewKey.Deriv, 0, MemNewKey.Dimensions[0].Default * MemNewKey.Dimensions[1].Default,
                                         MemNewKey.Deriv, 0, MemNewKey.Dimensions[0].Default * MemNewKey.Dimensions[1].Default,
                                         MemOldKey.Deriv, 0, MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default,
                                         MemOldKey.Dimensions[0].Default * MemOldKey.Dimensions[1].Default,
                                         MemOldKey.Dimensions[2].Default * MemOldKey.Dimensions[3].Default, 1, 0, 1);

            ComputeLib.Matrix_AdditionEx(MemNewValue.Deriv, 0, MemNewValue.Dimensions[0].Default * MemNewValue.Dimensions[1].Default,
                                         MemNewValue.Deriv, 0, MemNewValue.Dimensions[0].Default * MemNewValue.Dimensions[1].Default,
                                         MemOldValue.Deriv, 0, MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default,
                                         MemOldValue.Dimensions[0].Default * MemOldValue.Dimensions[1].Default,
                                         MemOldValue.Dimensions[2].Default * MemOldValue.Dimensions[3].Default, 1, 0, 1);
        }

    }


    public class AppendMemRunner : StructRunner<Structure, BatchData> //StructRunner
    {
        public NdArrayData MemKey { get; set; }
        public NdArrayData MemValue { get; set; }

        public NdArrayData AppMemKey { get; set; }
        public NdArrayData AppMemValue { get; set; }

        public NdArrayData MemMaskScale { get; set; }
        public NdArrayData MemMaskBias { get; set; }

        public int Index { get; set; }
        public AppendMemRunner(NdArrayData srcMemKey, NdArrayData srcMemValue, int index, NdArrayData nMemKey, NdArrayData nMemValue, RunnerBehavior behavior) : base(Structure.Empty, behavior) //base(behavior)
        {
            MemKey = srcMemKey;
            MemValue = srcMemValue;
            
            AppMemKey = nMemKey;
            AppMemValue = nMemValue;
            
            Index = index;

            IntArgument default_size = new IntArgument("default-size", 1);
            
            MemMaskScale = new NdArrayData(behavior.Device, false, MemKey.Dimensions[1], default_size, default_size, default_size);
            MemMaskBias = new NdArrayData(behavior.Device, false, MemKey.Dimensions[1], default_size, default_size, default_size);

            for(int i = 0; i < MemKey.Dimensions[1].Default; i++)
            {
                if(i < index) 
                {
                    MemMaskScale.Output[i] = 1;
                    MemMaskBias.Output[i] = 0;
                }
                else
                { 
                    MemMaskScale.Output[i] = 0;
                    MemMaskBias.Output[i] = (float)-1e9;
                }
            }
            MemMaskScale.Output.SyncFromCPU();
            MemMaskBias.Output.SyncFromCPU();
        }

        public override void Forward()
        {
            ComputeLib.Matrix_AdditionEx(AppMemKey.Output, 0, AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default,
                                         AppMemKey.Output, 0, AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default,
                                         
                                         MemKey.Output, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         
                                         AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default, 
                                         AppMemKey.Dimensions[2].Default * AppMemKey.Dimensions[3].Default, 1, 0, 0);

            ComputeLib.Matrix_AdditionEx(AppMemValue.Output, 0, AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default,
                                         AppMemValue.Output, 0, AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default,
                                         
                                         MemValue.Output, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         
                                         AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default, 
                                         AppMemValue.Dimensions[2].Default * AppMemValue.Dimensions[3].Default, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Matrix_AdditionEx(MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         
                                         MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         
                                         AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default, 
                                         AppMemKey.Dimensions[2].Default * AppMemKey.Dimensions[3].Default, 0, 0, 0);


            ComputeLib.Matrix_AdditionEx(MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         
                                         AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default, 
                                         AppMemValue.Dimensions[2].Default * AppMemValue.Dimensions[3].Default, 0, 0, 0);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Matrix_AdditionEx(MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         AppMemKey.Deriv, 0, AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default,

                                         AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default,
                                         AppMemKey.Dimensions[2].Default * AppMemKey.Dimensions[3].Default, 1, 0, 1);

            ComputeLib.Matrix_AdditionEx(MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         AppMemValue.Deriv, 0, AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default,
                                         
                                         AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default,
                                         AppMemValue.Dimensions[2].Default * AppMemValue.Dimensions[3].Default, 1, 0, 1);
        }

    }


    public class AppendMemMaskRunner : StructRunner<Structure, BatchData> //StructRunner
    {
        public NdArrayData MemKey { get; set; }
        public NdArrayData MemValue { get; set; }

        public NdArrayData AppMemKey { get; set; }
        public NdArrayData AppMemValue { get; set; }

        public NdArrayData OldMaskScale { get; set; }
        public NdArrayData OldMaskBias { get; set; }

        public NdArrayData NewMaskScale { get; set; }
        public NdArrayData NewMaskBias { get; set; }

        public int Index { get; set; }
        public AppendMemMaskRunner(NdArrayData srcMemKey, NdArrayData srcMemValue, int index, NdArrayData oldMaskScale, NdArrayData oldMaskBias, NdArrayData nMemKey, NdArrayData nMemValue, RunnerBehavior behavior) : base(Structure.Empty, behavior) //base(behavior)
        {
            MemKey = srcMemKey;
            MemValue = srcMemValue;
            
            AppMemKey = nMemKey;
            AppMemValue = nMemValue;
            
            Index = index;

            OldMaskScale = oldMaskScale;
            OldMaskBias = oldMaskBias;

            IntArgument default_size = new IntArgument("default-size", 1);
            
            NewMaskScale = new NdArrayData(behavior.Device, false, MemKey.Dimensions[1], default_size, default_size, MemKey.Dimensions[3]);
            NewMaskBias = new NdArrayData(behavior.Device, false, MemKey.Dimensions[1], default_size, default_size, MemKey.Dimensions[3]);

            for(int i = 0; i < MemKey.Dimensions[3].Default; i++)
            {
                NewMaskScale.Output[i * MemKey.Dimensions[1].Default + index] = 1;
                NewMaskBias.Output[i * MemKey.Dimensions[1].Default + index] = 0;
                
                for(int k = index + 1; k < MemKey.Dimensions[1].Default; k++)
                {
                    NewMaskScale.Output[i * MemKey.Dimensions[1].Default + k] = 0;
                    NewMaskBias.Output[i * MemKey.Dimensions[1].Default + k] = (float)-1e9;
                }
            }
            NewMaskScale.Output.SyncFromCPU();
            NewMaskBias.Output.SyncFromCPU();
        }

        public override void Forward()
        {
            ComputeLib.Matrix_AdditionEx(AppMemKey.Output, 0, AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default,
                                         AppMemKey.Output, 0, AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default,
                                         
                                         MemKey.Output, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         
                                         AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default, 
                                         AppMemKey.Dimensions[2].Default * AppMemKey.Dimensions[3].Default, 1, 0, 0);

            ComputeLib.Matrix_AdditionEx(AppMemValue.Output, 0, AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default,
                                         AppMemValue.Output, 0, AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default,
                                         
                                         MemValue.Output, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         
                                         AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default, 
                                         AppMemValue.Dimensions[2].Default * AppMemValue.Dimensions[3].Default, 1, 0, 0);


            ComputeLib.Matrix_AdditionEx(OldMaskScale.Output, 0, OldMaskScale.Dimensions[0].Default * OldMaskScale.Dimensions[1].Default,
                                         OldMaskScale.Output, 0, OldMaskScale.Dimensions[0].Default * OldMaskScale.Dimensions[1].Default,
                                         NewMaskScale.Output, 0, NewMaskScale.Dimensions[0].Default * NewMaskScale.Dimensions[1].Default,

                                         Index,
                                         OldMaskScale.Dimensions[2].Default * OldMaskScale.Dimensions[3].Default, 1, 0, 0);

            ComputeLib.Matrix_AdditionEx(OldMaskBias.Output, 0, OldMaskBias.Dimensions[0].Default * OldMaskBias.Dimensions[1].Default,
                                         OldMaskBias.Output, 0, OldMaskBias.Dimensions[0].Default * OldMaskBias.Dimensions[1].Default,
                                         NewMaskBias.Output, 0, NewMaskBias.Dimensions[0].Default * NewMaskBias.Dimensions[1].Default,
                                         
                                         Index,
                                         OldMaskBias.Dimensions[2].Default * OldMaskBias.Dimensions[3].Default, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Matrix_AdditionEx(MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         
                                         MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         
                                         AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default, 
                                         AppMemKey.Dimensions[2].Default * AppMemKey.Dimensions[3].Default, 0, 0, 0);


            ComputeLib.Matrix_AdditionEx(MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         
                                         AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default, 
                                         AppMemValue.Dimensions[2].Default * AppMemValue.Dimensions[3].Default, 0, 0, 0);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Matrix_AdditionEx(MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         MemKey.Deriv, MemKey.Dimensions[0].Default * Index, MemKey.Dimensions[0].Default * MemKey.Dimensions[1].Default,
                                         AppMemKey.Deriv, 0, AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default,

                                         AppMemKey.Dimensions[0].Default * AppMemKey.Dimensions[1].Default,
                                         AppMemKey.Dimensions[2].Default * AppMemKey.Dimensions[3].Default, 1, 0, 1);

            ComputeLib.Matrix_AdditionEx(MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         MemValue.Deriv, MemValue.Dimensions[0].Default * Index, MemValue.Dimensions[0].Default * MemValue.Dimensions[1].Default,
                                         AppMemValue.Deriv, 0, AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default,
                                         
                                         AppMemValue.Dimensions[0].Default * AppMemValue.Dimensions[1].Default,
                                         AppMemValue.Dimensions[2].Default * AppMemValue.Dimensions[3].Default, 1, 0, 1);
        }

    }
}
