using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    unsafe public class BaseBertRunner : ComputationGraph
    {
        public override string name { get { return "BaseBertRunner" ; } }

        BaseBertModel Model { get; set; } 
        
        public CudaPieceInt TokenIds { get; set; }

        public NdArrayData TokenEmds { get; set; }

        public CudaPieceInt SegmentIds { get; set; }
        public CudaPieceInt MaskIds { get; set; }

        public NdArrayData Featurer { get; set; }
        public HiddenBatchData Slice { get; set; }
        public HiddenBatchData Pool { get; set; }

        public Dictionary<string, NdArrayData> LayerFeatuers = new Dictionary<string, NdArrayData>();

        public NdArrayData[] LayerMaps;

        public int head { get; set; }

        public void BuildCG(int max_batch_size, int max_seq_len)
        {
            NdArrayData W_tensor = TokenEmds;
            HiddenBatchData t_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(SegmentIds, SegmentIds.Size, Model.TokenTypeEmbed, Behavior));

            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", max_batch_size);
            IntArgument[] shape = new IntArgument[] { emd_dim, sent_size, batch_size };
            
            
            NdArrayData T_tensor = new NdArrayData(shape, t_embed.Output.Data, t_embed.Deriv.Data, Behavior.Device);
            NdArrayData WT_tensor = W_tensor.Add(T_tensor, Session, Behavior);

            Logger.WriteLog("Position embeding module.");
            IntArgument b_size = new IntArgument("b", 1);
            IntArgument[] p_shape = new IntArgument[] { emd_dim, sent_size, b_size};

            NdArrayData P_tensor = new NdArrayData(p_shape, Model.PosEmbed.Embedding.SubArray(0, Model.embed * max_seq_len), 
                                                            Model.PosEmbed.EmbeddingGrad == null ? null : Model.PosEmbed.EmbeddingGrad.SubArray(0, Model.embed * max_seq_len), Behavior.Device);

            Logger.WriteLog("Combine Word Type and Position Embedding.");
            NdArrayData WTP_tensor = WT_tensor.AddExpand(P_tensor, Session, Behavior);

            NdArrayData _normWTP = WTP_tensor.Norm(0, Session, Behavior);
            NdArrayData normWTP = _normWTP.DotAndAdd(Model.NormScale, Model.NormBias, Session, Behavior);
            
            
            int attdim_per_head = Model.embed / head;
            Logger.WriteLog("attention dim per head {0}", attdim_per_head);

            //normWTP = normWTP.Dropout(0.1f, Session, Behavior); 
            CudaPieceFloat scaleVec = new CudaPieceFloat(2, Behavior.Device);
            scaleVec[0] = 0;
            scaleVec[1] = (float)(1.0f / Math.Sqrt(attdim_per_head));
            scaleVec.SyncFromCPU();

            CudaPieceFloat biasVec = new CudaPieceFloat(2, Behavior.Device);
            biasVec[0] = (float)-1e9;
            biasVec[1] = 0;
            biasVec.SyncFromCPU();

            CudaPieceFloat MaskScale = null;
            CudaPieceFloat MaskBias = null; 

            if(MaskIds != null && !MaskIds.IsEmpty)
            {
                MaskScale = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskIds, MaskIds.Size, new EmbedStructure(2, 1, scaleVec, Behavior.Device), Behavior))).Output.Data; 
                MaskBias = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskIds, MaskIds.Size, new EmbedStructure(2, 1, biasVec, Behavior.Device), Behavior))).Output.Data; 
            }

            LayerMaps = new NdArrayData[Model.layer + 1];
            
            Logger.WriteLog("Build Transformer for Language.");
            for(int i = 0; i < Model.layer; i++)
            {
                LayerMaps[i] = normWTP;

                TransformerRunner blockRunner = new TransformerRunner(Model.Blocks[i], head, max_seq_len, normWTP, MaskScale, MaskBias, Behavior);
                AddRunner(blockRunner);
                normWTP = blockRunner.Output;
                
                LayerFeatuers.Add("transformer-"+i.ToString(), normWTP);
            }
            LayerMaps[Model.layer] = normWTP;

            Featurer = normWTP;

            IntArgument firstSlice = new IntArgument("first-slice", 1);
            IntArgument offsetDim = new IntArgument("offset", 0);
            
            TensorSliceRunner sliceRunner = new TensorSliceRunner(Featurer, new IntArgument[] { offsetDim, offsetDim, offsetDim },
                                                                            new IntArgument[] { emd_dim, firstSlice, batch_size },
                                                                            Behavior);
            Logger.WriteLog("Build CG in slice runner.");

            AddRunner(sliceRunner);
            Slice = sliceRunner.Output.Reshape(new IntArgument[] { emd_dim, batch_size }).ToHDB();

            FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Pooler, Slice, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fcRunner);
            Pool = fcRunner.Output;
        }
        public BaseBertRunner(BaseBertModel model, CudaPieceInt token_ids, CudaPieceInt segment_ids, CudaPieceInt mask_ids, int max_batch_size, int max_seq_len, RunnerBehavior behavior) 
            : this(model, token_ids, segment_ids, mask_ids, max_batch_size, max_seq_len, 12, behavior)
        { }

        public BaseBertRunner(BaseBertModel model, CudaPieceInt token_ids, CudaPieceInt segment_ids, CudaPieceInt mask_ids, int max_batch_size, int max_seq_len, int nhead, RunnerBehavior behavior) : base(behavior)
        {
            Model = model;

            TokenIds = token_ids;
            SegmentIds = segment_ids;
            MaskIds = mask_ids;

            head = nhead;

            Logger.WriteLog("Word and Type embeding module.");
            
            HiddenBatchData w_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.TokenEmbed, Behavior));
            
            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", max_batch_size);
            IntArgument[] shape = new IntArgument[] { emd_dim, sent_size, batch_size };

            TokenEmds = new NdArrayData(shape, w_embed.Output.Data, w_embed.Deriv.Data, Behavior.Device);    
            
            BuildCG(max_batch_size, max_seq_len);
        }
        

        public BaseBertRunner(BaseBertModel model, NdArrayData token_embeds, CudaPieceInt segment_ids, CudaPieceInt mask_ids, int max_batch_size, int max_seq_len, int nhead, RunnerBehavior behavior) : base(behavior)
        {
            Model = model;

            SegmentIds = segment_ids;
            MaskIds = mask_ids;

            head = nhead;

            TokenEmds = token_embeds;
            
            BuildCG(max_batch_size, max_seq_len);
        }

    }
}
