﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LSTMRunner : StructRunner
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        List<StructRunner> SubRunners = new List<StructRunner>();

        LSTMStructure FWLSTM { get; set; }
        SeqDenseBatchData DATA { get; set; }

        public HiddenBatchData LastStatus { get { return LastStatusO.Last(); } }

        public List<HiddenBatchData> LastStatusO { get; set; }
        public List<HiddenBatchData> LastStatusC { get; set; }

        public LSTMRunner(LSTMStructure fwLSTM, SeqDenseBatchData input, RunnerBehavior behavior, bool lastState = false)
            : this(fwLSTM, input, null, null, behavior, lastState)
        { }
        
        public LSTMRunner(LSTMStructure fwLSTM, SeqDenseBatchData input, List<HiddenBatchData> lastO, List<HiddenBatchData> lastC, RunnerBehavior behavior, bool lastState = false)
            : base(Structure.Empty, behavior)
        {
            DATA = input;
            FWLSTM = fwLSTM;

            if (input.Dim != fwLSTM.LSTMCells[0].InputFeatureDim)
            { throw new Exception(string.Format("fw LSTM input dim {0}, input dim {1} not matched", fwLSTM.LSTMCells[0].InputFeatureDim, input.Dim)); }

            if(lastState)
            {
                LastStatusO = new List<HiddenBatchData>();
                LastStatusC = new List<HiddenBatchData>();
            }

            Cook_Seq2TransposeSeqRunner fwInputRunner = new Cook_Seq2TransposeSeqRunner(input, false, Behavior);
            SubRunners.Add(fwInputRunner);
            SeqDenseRecursiveData fwInputData = fwInputRunner.Output;
            for (int i = 0; i < fwLSTM.LSTMCells.Count; i++)
            {
                int r_i = lastO == null ? 0 : fwLSTM.LSTMCells.Count - lastO.Count;

                HiddenBatchData tmpO = (lastO == null || r_i > i) ? null : lastO[i - r_i];
                HiddenBatchData tmpC = (lastC == null || r_i > i) ? null : lastC[i - r_i];
                
                FastLSTMDenseRunner<SeqDenseRecursiveData> fwRunner =
                    new FastLSTMDenseRunner<SeqDenseRecursiveData>(fwLSTM.LSTMCells[i], fwInputData, tmpO, tmpC, null, Behavior);
                SubRunners.Add(fwRunner);
                fwInputData = fwRunner.Output;

                if(lastState)
                {
                    SeqOrderMatrixRunner fwOlastRunner = new SeqOrderMatrixRunner(fwRunner.Output, true, 0, fwRunner.Output.MapForward, Behavior);
                    SubRunners.Add(fwOlastRunner);
                    LastStatusO.Add(fwOlastRunner.Output);

                    SeqOrderMatrixRunner fwClastRunner = new SeqOrderMatrixRunner(fwRunner.C, true, 0, fwRunner.C.MapForward, Behavior);
                    SubRunners.Add(fwClastRunner);
                    LastStatusC.Add(fwClastRunner.Output);
                }
            }

            Cook_TransposeSeq2SeqRunner fwOutputRunner = new Cook_TransposeSeq2SeqRunner(fwInputData, Behavior);
            SubRunners.Add(fwOutputRunner);
            SeqDenseBatchData fwOutputData = fwOutputRunner.Output;
            
            Output = fwOutputData;
        }

        public override void Forward()
        {
            iteration++;
            foreach (StructRunner runner in SubRunners) { runner.Forward(); }
        }

        public override void Backward(bool cleanDeriv)
        {
            foreach (StructRunner runner in Enumerable.Reverse(SubRunners)) { runner.Backward(false); }
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in SubRunners) { runner.CleanDeriv(); }
        }

        public override void Update()
        {
            foreach (StructRunner runner in SubRunners) { runner.Update(); }
        }
    }

}
