﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class CompositeNetRunner : StructRunner
    {
        public List<StructRunner> LinkRunners = new List<StructRunner>();

        public List<StructRunner> Session { get { return LinkRunners; } }
        
        public CompositeNetRunner(RunnerBehavior behavior) : base(Structure.Empty, behavior)
        { }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in LinkRunners) runner.CleanDeriv();
        }

        public override void Forward()
        {
            IsTerminate = false;
            IsContinue = true;
            int idx = 0;
            foreach (StructRunner runner in LinkRunners)
            {
                runner.Forward();
                IsTerminate = runner.IsTerminate;
                IsContinue = runner.IsContinue;
                if (IsTerminate || !IsContinue) break;

                idx += 1;
            }

        }

        public override void Backward(bool cleanDeriv)
        {
            LinkRunners.Reverse();
            foreach (StructRunner runner in LinkRunners) { if (runner.IsBackProp) runner.Backward(cleanDeriv); }
            LinkRunners.Reverse();
        }

        public override void Update()
        {
            foreach (StructRunner runner in LinkRunners) { if (runner.IsBackProp && runner.IsUpdate) { runner.Update(); } }
        }

        public override void Init()
        {
            foreach (StructRunner runner in LinkRunners) { runner.Init(); }
        }

        public override void Complete()
        {
            foreach (StructRunner runner in LinkRunners) { runner.Complete(); }
        }
    }
}
