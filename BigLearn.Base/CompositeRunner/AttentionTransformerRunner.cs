using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class AttentionTransformerRunner : CompositeNetRunner 
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

		new AttentionTransformer Model { get; set; }

        public AttentionTransformerRunner(AttentionTransformer model, int nhead, float dropout, NdArrayData input, NdArrayData memory, RunnerBehavior behavior) : base(behavior)
        {
            Input = input;
            Model = model;

            NdArrayData x = input;

            IntArgument embed = x.Dimensions[0];
            IntArgument head = new IntArgument("attention head", nhead);
            IntArgument slice = Session.Divid(Behavior, embed, head);
            
            IntArgument seq = x.Dimensions[1];
            IntArgument batch = x.Dimensions[2];
            
            IntArgument memslot = memory.Dimensions[1];

            float att_scale = (float)(1.0f / Math.Sqrt(slice.Default));

            NdArrayData q = x.FNN(model.QProject, Session, Behavior).Reshape(slice, head, seq, batch).Transpose(new int[] { 0, 2, 1, 3 }, Session, Behavior);
            Logger.WriteLog("Init FNN q k v");

            NdArrayData k = memory.FNN(model.KProject, Session, Behavior).Reshape(slice, head, memslot, batch).Transpose(new int[] { 0, 2, 1, 3 }, Session, Behavior);
            NdArrayData v = memory.FNN(model.VProject, Session, Behavior).Reshape(slice, head, memslot, batch).Transpose(new int[] { 0, 2, 1, 3 }, Session, Behavior);

            NdArrayData attScore = q.MatMul(0, k, 1, Session, Behavior).Scale(att_scale, Session, Behavior).Softmax(Session, Behavior).Dropout(dropout, Session, Behavior);

            NdArrayData vData = attScore.MatMul(0, v, 0, Session, Behavior).Transpose(new int[] { 0, 2, 1, 3 }, Session, Behavior).Reshape(embed, seq, batch); // {slice_dim, src_seq_len, slice_num, batch_size }

            NdArrayData hid_v = vData.FNN(model.AttProject, Session, Behavior).Dropout(dropout, Session, Behavior);
            
            NdArrayData x_v = x.Add(hid_v, Session, Behavior);

            NdArrayData _normxv = x_v.Norm(0, Session, Behavior);
            NdArrayData normxv = _normxv.DotAndAdd(model.Norm1Scale, model.Norm1Bias, Session, Behavior);

            // Output = normxy            
            NdArrayData h1 = normxv.FNN(model.MLP1, Session, Behavior);
            NdArrayData h1_act = h1.Act(A_Func.Gelu, Session, Behavior);
            NdArrayData h2 = h1_act.FNN(model.MLP2, Session, Behavior);
            
            h2 = h2.Dropout(dropout, Session, Behavior);

            NdArrayData o = normxv.Add(h2, Session, Behavior);
            NdArrayData _normo = o.Norm(0, Session, Behavior);
            Output = _normo.DotAndAdd(model.Norm2Scale, model.Norm2Bias, Session, Behavior);
        }
    }
}
