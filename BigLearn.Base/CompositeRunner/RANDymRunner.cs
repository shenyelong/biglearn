using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    public class RANDymRunner : CompositeNetRunner 
    {
        //public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }
        public new HiddenBatchData Output  { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } } 
		public new List<HiddenBatchData> Outputs { get; set; }

        new RANStructure Model { get; set; }
        //NdArrayData MaskScale { get; set; }
        //NdArrayData MaskBias { get; set; }
        public List<CudaPieceInt> AttIndex { get; set; }

        public List<HiddenBatchData> AttData { get; set; }
        
        public RANDymRunner(RANStructure model, NdArrayData input, int nhead, RateScheduler tempRate, bool hardGumbel, List<HiddenBatchData> states, int step, RunnerBehavior behavior) : base(behavior)
        {
            Model = model;
            Input = input;
            
            IntArgument emd_dim = new IntArgument("emd", Model.StateDim);
            IntArgument active_dim = Input.Dimensions[1];  
            IntArgument batch_size = Input.Dimensions[2];  

            if(Model.StateDim % nhead != 0)
            {
                throw new Exception(string.Format("the model state dim {0}, nhead {1}, they are dividable", Model.StateDim, nhead));
            }

            IntArgument head_dim = new IntArgument("head-dim", Model.StateDim / nhead);
            IntArgument head_num = new IntArgument("head-num", nhead);
            IntArgument head_batch = new IntArgument("head-batch", nhead * batch_size.Default);

            NdArrayData kfeature = Input.FNN(Model.KProject, Session, Behavior).Reshape(head_dim, head_num, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);
            NdArrayData vfeature = Input.FNN(Model.VProject, Session, Behavior).Reshape(head_dim, head_num, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);

            AttIndex = new List<CudaPieceInt>();

            List<HiddenBatchData> si_list = new List<HiddenBatchData>();
            for(int l = 0; l < Model.RecurrentLayer; l++) si_list.Add(states[l]);

            AttData = new List<HiddenBatchData>();

            Outputs = new List<HiddenBatchData>();
            //List<HiddenBatchData> Ts = new List<HiddenBatchData>();

            // int incMemRate, int nhead, RunnerBehavior behavior)
            int incMemRate = step + 1;

            //NdArrayData memKey, NdArrayData memValue, int incrSize, RunnerBehavior behavior)
            DynMemRunner memRunner = new DynMemRunner(kfeature, vfeature, incMemRate, behavior);  //batch_size.Default, incMemRate, nhead, Behavior);
            Session.Add(memRunner);

            NdArrayData new_kfeature = memRunner.MemNewKey;
            NdArrayData new_vfeature = memRunner.MemNewValue;
            
            NdArrayData memMaskScale = memRunner.MemMaskScale;
            NdArrayData memMaskBias = memRunner.MemMaskBias;

            IntArgument large_mem = new IntArgument("dy-mem-size", incMemRate + active_dim.Default);
            IntArgument default_arg = new IntArgument("default-arg", 1);

            for(int i = 0; i < step; i++)
            {
                // attention on input memory.
                NdArrayData si_q = si_list.Last().FNN(Model.QProject, Session, Behavior).ToND().Reshape(head_dim, default_arg, head_num, batch_size);
                NdArrayData attScore = si_q.MatMul(0, new_kfeature, 1, Session, Behavior);

                attScore = attScore.DotAndAdd(memMaskScale, memMaskBias, Session, Behavior);

                // softmax and sampling. 
                GumbelSoftmaxRunner smRunner = new GumbelSoftmaxRunner(attScore.Reshape(large_mem, head_batch).ToHDB(), tempRate, hardGumbel, Behavior);                
                Session.Add(smRunner);
                AttIndex.Add(smRunner.maxIndex);
                attScore = smRunner.Output.ToND().Reshape(attScore.Shape);
                NdArrayData aData = attScore.MatMul(0, new_vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(emd_dim, batch_size).ToHDB();

                AttData.Add(attData);

                List<HiddenBatchData> next_si_list = new List<HiddenBatchData>();
                
                for(int l = 0; l < Model.RecurrentLayer; l++)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCells[l], si_list[l], attData, Behavior);
                    Session.Add(stateRunner);
                    attData = stateRunner.Output;
                    next_si_list.Add(attData);
                }

                si_list = next_si_list;

                Outputs.Add(si_list.Last());

                // push memory.
                NdArrayData n_kfeature = si_list.Last().FNN(Model.KProject, Session, Behavior).ToND().Reshape(head_dim, default_arg, head_num, batch_size);
                NdArrayData n_vfeature = si_list.Last().FNN(Model.VProject, Session, Behavior).ToND().Reshape(head_dim, default_arg, head_num, batch_size);


                AppendMemRunner appMemRunner = new AppendMemRunner(new_kfeature, new_vfeature, active_dim.Default + i, n_kfeature, n_vfeature, Behavior);
                Session.Add(appMemRunner);

                memMaskScale = appMemRunner.MemMaskScale;
                memMaskBias = appMemRunner.MemMaskBias;
                //HiddenBatchData tp = si_list.Last().FNN(Model.T, Session, Behavior);
                //Ts.Add(tp);
            }

            Output = Outputs.Last();
        }
    }

}
