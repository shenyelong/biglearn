using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    public class RANRunner : CompositeNetRunner 
    {
        //public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }
        public new HiddenBatchData Output  { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } } 
		public new List<HiddenBatchData> Outputs { get; set; }

        new RANStructure Model { get; set; }

        //NdArrayData MaskScale { get; set; }
        //NdArrayData MaskBias { get; set; }
        public List<CudaPieceInt> AttIndex { get; set; }

        public List<HiddenBatchData> AttData { get; set; }
        
        public RANRunner(RANStructure model, NdArrayData input, int nhead, RateScheduler tempRate, bool hardGumbel, List<HiddenBatchData> states, int step, RunnerBehavior behavior) : base(behavior)
        {
            Model = model;
            Input = input;
            
            IntArgument emd_dim = new IntArgument("emd", Model.StateDim);
            IntArgument active_dim = Input.Dimensions[1];  
            IntArgument batch_size = Input.Dimensions[2];  

            if(Model.StateDim % nhead != 0)
            {
                throw new Exception(string.Format("the model state dim {0}, nhead {1}, they are dividable", Model.StateDim, nhead));
            }

            IntArgument head_dim = new IntArgument("head-dim", Model.StateDim / nhead);
            IntArgument head_num = new IntArgument("head-num", nhead);
            IntArgument head_batch = new IntArgument("head-batch", nhead * batch_size.Default);

            NdArrayData kfeature = Input.FNN(Model.KProject, Session, Behavior).Reshape(head_dim, head_num, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
            NdArrayData vfeature = Input.FNN(Model.VProject, Session, Behavior).Reshape(head_dim, head_num, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;

            AttIndex = new List<CudaPieceInt>();

            List<HiddenBatchData> si_list = new List<HiddenBatchData>();
            for(int l = 0; l < Model.RecurrentLayer; l++) si_list.Add(states[l]);

            AttData = new List<HiddenBatchData>();

            Outputs = new List<HiddenBatchData>();
            //List<HiddenBatchData> Ts = new List<HiddenBatchData>();

            for(int i = 0; i < step; i++)
            {
                NdArrayData si_q = si_list.Last().FNN(Model.QProject, Session, Behavior).ToND().Reshape(head_dim, new IntArgument("seq", 1), head_num, batch_size);

                NdArrayData attScore = si_q.MatMul(0, kfeature, 1, Session, Behavior);

                // softmax and sampling. 
                GumbelSoftmaxRunner smRunner = new GumbelSoftmaxRunner(attScore.Reshape(active_dim, head_batch).ToHDB(), tempRate, hardGumbel, Behavior);                
                Session.Add(smRunner);
                AttIndex.Add(smRunner.maxIndex);

                attScore = smRunner.Output.ToND().Reshape(attScore.Shape);

                NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(emd_dim, batch_size).ToHDB();

                AttData.Add(attData);

                List<HiddenBatchData> next_si_list = new List<HiddenBatchData>();
                
                for(int l = 0; l < Model.RecurrentLayer; l++)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCells[l], si_list[l], attData, Behavior);
                    Session.Add(stateRunner);
                    attData = stateRunner.Output;
                    next_si_list.Add(attData);
                }

                si_list = next_si_list;

                Outputs.Add(si_list.Last());

                //HiddenBatchData tp = si_list.Last().FNN(Model.T, Session, Behavior);
                //Ts.Add(tp);
            }

            Output = Outputs.Last();
        }
    }


    public class DumpAttentionRunner : StructRunner
    {
        public List<CudaPieceInt> AttIndex { get; set; }
        
        IntArgument Batch { get; set; }
        int Head { get; set; }
        public string DumpPath { get; set; }
        StreamWriter DumpWriter { get; set; }
        public DumpAttentionRunner(List<CudaPieceInt> attIdx, IntArgument batch, int head, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            AttIndex = attIdx;
            Head = head;
            DumpPath = string.Empty;
            Batch = batch;
        }

        public override void Init()
        {
            if(!DumpPath.Equals(string.Empty)) DumpWriter = new StreamWriter(DumpPath);
            else DumpWriter = null;
        }

        public override void Complete()
        {
            if(DumpWriter != null)
            {
                DumpWriter.Close();
            }
        }

        public override void Forward()
        {
            if(DumpWriter != null)
            {
                for(int i = 0; i < AttIndex.Count; i++) AttIndex[i].SyncToCPU();

                for(int i = 0; i < Batch.Value; i++)
                {
                    for(int s = 0; s < AttIndex.Count; s++)
                    {
                        DumpWriter.Write(string.Join(",", AttIndex[s].TakeCPU(i * Head, Head)) + "\t");
                    }
                    DumpWriter.Write("\n");
                }
            }
        }
    }

}
