using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SingleTransformerRunner : CompositeNetRunner 
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public NdArrayData Query { get; set; }
        public NdArrayData Memory { get; set; }

		new TransformerStructure Model { get; set; }

        NdArrayData MaskScale { get; set; }
        NdArrayData MaskBias { get; set; }


        public SingleTransformerRunner(TransformerStructure model, int nhead, int maxSentSize, NdArrayData query, NdArrayData memory, 
                CudaPieceFloat maskScale, CudaPieceFloat maskBias, float dropout, RunnerBehavior behavior) : base(behavior)
        {
            Query = input;
            Memory = memory;

            Model = model;

            NdArrayData x = input;

            NdArrayData att_q = query.FNN(model.QKVProject, Session, Behavior);
            NdArrayData att_kv = memory.FNN(model.QKVProject, Session, Behavior);

            Logger.WriteLog("Init FNN log");

            if(Query.Dimensions[0].Value % nhead > 0)
            {
                 throw new Exception(string.Format("input Dim {0} is not assert with the head dim {1}", Query.Dimensions[0].Value, nhead));
            }

            int slice_dim = Query.Dimensions[0].Value / nhead;
            Logger.WriteLog("Slice Dim {0}", slice_dim);

            int slice_num = att_q.Dimensions[0].Value / slice_dim;
            Logger.WriteLog("Slice num {0}", slice_num);

            IntArgument ShapeSlice = new IntArgument("slice-dim", slice_dim);
            IntArgument ShapeSlice_Num = new IntArgument("slice-num", slice_num);
            IntArgument ShapeSent = new IntArgument("sent-size", maxSentSize);

            Logger.WriteLog("batch size {0}", x.Dimensions[2].Value);

            IntArgument default_arg = new IntArgument("default_arg", 1);

            
            if(maskScale == null || maskBias == null)
            {
                 // int slice_num = input.Dimensions[0].Default / nhead;
                InitMask(maxSentSize, IsLN ? 1.0f / (float)Math.Sqrt(slice_dim) : 1, behavior.Device);
            }
            else if(maskScale.Size == maxSentSize * x.Dimensions[2].Default && maskBias.Size == maxSentSize * x.Dimensions[2].Default )
            {
                MaskScale = new NdArrayData(behavior.Device, maskScale, null, new IntArgument[] { ShapeSent, default_arg, x.Dimensions[2], default_arg } );
                
                MaskBias = new NdArrayData(behavior.Device, maskBias, null, new IntArgument[] { ShapeSent, default_arg, x.Dimensions[2], default_arg } );
            }
            else if(maskScale.Size == maxSentSize * maxSentSize * x.Dimensions[2].Default && maskBias.Size == maxSentSize * maxSentSize * x.Dimensions[2].Default )
            {
                MaskScale = new NdArrayData(behavior.Device, maskScale, null, new IntArgument[] { ShapeSent, ShapeSent, x.Dimensions[2], default_arg } );
            
                MaskBias = new NdArrayData(behavior.Device, maskBias, null, new IntArgument[] { ShapeSent, ShapeSent, x.Dimensions[2], default_arg } );
            }
            else
            {
                throw new NotImplementedException();
            }

            NdArrayData nd_x = att_x.Reshape( ShapeSlice, ShapeSlice_Num, ShapeSent, x.Dimensions[2] );
            
            Logger.WriteLog("start transpose. ");

            NdArrayData nd_y = nd_x.Transpose(new int[] {0, 2, 3, 1}, Session, Behavior);

            IntArgument ShapeHead = new IntArgument("head-num", nhead);
            
            Logger.WriteLog("Init Shape");

            IntArgument[] subDims = new IntArgument[] { ShapeSlice, ShapeSent, x.Dimensions[2], ShapeHead };
            List<NdArrayData> splits = nd_y.Split(new List<IntArgument[]>() { subDims, subDims, subDims }, Session, Behavior);
            
            NdArrayData Q = splits[0]; 
            NdArrayData K = splits[1]; 
            NdArrayData V = splits[2]; 
            
            NdArrayData attScore = Q.MatMul(0, K, 1, Session, Behavior); 
            Logger.WriteLog("Attention dims {0}, {1}, {2}, {3}", attScore.Dimensions[0].Value, attScore.Dimensions[1].Value, attScore.Dimensions[2].Value, attScore.Dimensions[3].Value);
            Logger.WriteLog("Attention..");

            attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior);

            attScore = attScore.Softmax(Session, Behavior);
            
            attScore = attScore.Dropout(dropout, Session, Behavior);

            Logger.WriteLog("mask and softmax .");

            NdArrayData aData = attScore.MatMul(0, V, 0, Session, Behavior);
            
            NdArrayData aMergeData = aData.Transpose(new int[] {0, 3, 1, 2}, Session, Behavior);

            NdArrayData y = aMergeData.Reshape(x.Dimensions);

            Logger.WriteLog("reshape and transpose .");

            // Output = y;

            NdArrayData att_y = y.FNN(model.AttProject, Session, Behavior);
            
            att_y = att_y.Dropout(dropout, Session, Behavior);
            
            NdArrayData xy = x.Add(att_y, Session, Behavior);

            NdArrayData _normxy = IsLN ? xy.Norm(0, Session, Behavior) : xy;
            NdArrayData normxy = _normxy.DotAndAdd(model.Norm1Scale, model.Norm1Bias, Session, Behavior);

            // Output = normxy            
            NdArrayData h1 = normxy.FNN(model.MLP1, Session, Behavior);
            NdArrayData h1_act = h1.Act(A_Func.Gelu, Session, Behavior);
            NdArrayData h2 = h1_act.FNN(model.MLP2, Session, Behavior);
            
            h2 = h2.Dropout(dropout, Session, Behavior);

            NdArrayData o = normxy.Add(h2, Session, Behavior);
            NdArrayData _normo = IsLN ? o.Norm(0, Session, Behavior) : o;
            Output = _normo.DotAndAdd(model.Norm2Scale, model.Norm2Bias, Session, Behavior);
        }

        


    }
}
