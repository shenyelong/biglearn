﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class EmbeddingLossDerivParameter
    // {
    //     public int NegativeNum;
    //     //public CudaPieceFloat Embed;
    //     //public CudaPieceFloat Deriv;
    //     //public int Dim;

    //     public CudaPieceFloat vSpace;
    //     public CudaPieceFloat vBias;
    //     public int vocabSize;

    //     public CudaPieceFloat cSpace;
    //     public CudaPieceFloat cBias;
    //     public int classNum;

    //     public CudaPieceInt vocab2class;
    //     public CudaPieceInt classIndex;
    //     public CudaPieceInt wordIndex;
    //     public CudaPieceInt wordClassIndex;
        
    //     public CudaPieceFloat classOutput;
    //     public CudaPieceFloat wordOutput;
    //     public int classVocabNum;
    //     public CudaPieceFloat Prob;

    //     public CudaPieceInt wordAct1;
    //     public CudaPieceInt wordAct2;

    //     public float stepSize;
    //     public CudaPieceFloat Simi;
    //     public CudaPieceFloat Alpha;
    //     public float gamma;
    //     public CudaPieceInt NegativeIndex = null;

    //     public Dictionary<int, int> Vocab = null;
    //     public CudaPieceInt VocabSampleTable = null;
    //     public int VocabSampleSize = 100000000;

    //     //public List<List<int>> 
    // }

    // public class LMLSTMRunner<IN> : StructRunner<LSTMPredStructure, IN> where IN : SeqSparseDataSource
    // {
    //     public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

    //     //BatchData input,  input, 
    //     public List<StructRunner> LinkRunners = new List<StructRunner>();

    //     public EmbeddingLossDerivParameter lossDerivParameter = new EmbeddingLossDerivParameter();

    //     public LMLSTMRunner(LSTMPredStructure model, RunnerBehavior behavior)
    //         : base(model, behavior)
    //     {
    //         Type inputType = typeof(SeqSparseBatchData);
    //         foreach (LSTMCell linkModel in Model.LSTM.LSTMCells)
    //         {
    //             StructRunner linkRunner = StructRunner.CreateRunner(linkModel, inputType, behavior);
    //             LinkRunners.Add(linkRunner);
    //             inputType = linkRunner.Output.GetType();
    //         }
    //         for (int i = 0; i < LinkRunners.Count - 1; i++)
    //         {
    //             LinkRunners[i + 1].Input = LinkRunners[i].Output;
    //         }

    //         Output = (SeqDenseBatchData)LinkRunners.Last().Output;
    //     }

    //     public CudaPieceFloat GroundTruth;
    //     public override IN Input
    //     {
    //         get
    //         {
    //             return (IN)new SeqSparseDataSource()
    //             {
    //                 Stat = ((SeqSparseBatchData)LinkRunners.First().Input).Stat,
    //                 SequenceData = ((SeqSparseBatchData)LinkRunners.First().Input),
    //                 SequenceLabel = GroundTruth
    //             };
    //         }
    //         set
    //         {
    //             LinkRunners.First().Input = value.SequenceData;
    //             GroundTruth = value.SequenceLabel;
    //         }
    //     }
        
    //     public override void Forward()
    //     {
    //         MathOperatorManager.MathDevice = Behavior.Device;
    //         foreach (StructRunner runner in LinkRunners) runner.Forward();
    //     }

    //     public override void Backward(bool cleanDeriv)
    //     {
    //         LinkRunners.Reverse();
    //         foreach (StructRunner runner in LinkRunners) runner.Backward(cleanDeriv);
    //         LinkRunners.Reverse();
    //     }

    //     public override void Update()
    //     {
    //         foreach (StructRunner runner in LinkRunners)
    //             runner.Update();
    //     }

    // }
}
