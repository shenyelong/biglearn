using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RecurrentTransformerRunner : CompositeNetRunner 
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

		new RecurrentTransformerStructure Model { get; set; }

        NdArrayData MaskScale { get; set; }
        NdArrayData MaskBias { get; set; }
        
        public RecurrentTransformerRunner(RecurrentTransformerStructure model, int nhead, float dropout, NdArrayData input, NdArrayData externalKey, NdArrayData externalValue, CudaPieceFloat maskScale, CudaPieceFloat maskBias, RunnerBehavior behavior) : base(behavior)
        {
            Input = input;
            Model = model;

            NdArrayData x = input;

            NdArrayData q = x.FNN(model.QProject, Session, Behavior);
            Logger.WriteLog("Init FNN q log");

            IntArgument slice_dim = new IntArgument("slice-dim", x.Dimensions[0].Value / nhead);
            IntArgument slice_num = new IntArgument("slice-num", nhead);
            IntArgument src_seq_len = x.Dimensions[1];
            IntArgument batch_size = x.Dimensions[2];
            
            IntArgument mem_seq_len = externalKey.Dimensions[1];

            IntArgument default_arg = new IntArgument("default_arg", 1);

            q = q.Reshape(new IntArgument[] { slice_dim, slice_num, src_seq_len, batch_size}).Transpose(new int[] { 0, 2, 1, 3 }, Session, Behavior);

            NdArrayData attScore = q.MatMul(0, externalKey, 1, Session, Behavior); // {mem_seq_len, src_seq_len, slice_num, batch_size}

            MaskScale = new NdArrayData(behavior.Device, maskScale, null, new IntArgument[] { mem_seq_len, default_arg, default_arg, batch_size } );
                
            MaskBias = new NdArrayData(behavior.Device, maskBias, null, new IntArgument[] { mem_seq_len, default_arg, default_arg, batch_size } );

            attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior).Softmax(Session, Behavior).Dropout(0.1f, Session, Behavior);

            NdArrayData aData = attScore.MatMul(0, externalValue, 0, Session, Behavior); // {slice_dim, src_seq_len, slice_num, batch_size }

            NdArrayData aMergeData = aData.Transpose(new int[] {0, 2, 1, 3}, Session, Behavior); // {slice_dim, slice_num, src_seq_len, batch_size }

            NdArrayData y = aMergeData.Reshape(x.Dimensions); //{dim, src_seq_len, batch_size}

            Logger.WriteLog("reshape and transpose .");

            // Output = y;
            NdArrayData att_y = y.FNN(model.AttProject, Session, Behavior).Dropout(0.1f, Session, Behavior);
            
            NdArrayData xy = x.Add(att_y, Session, Behavior);

            NdArrayData _normxy = xy.Norm(0, Session, Behavior);
            NdArrayData normxy = _normxy.DotAndAdd(model.Norm1Scale, model.Norm1Bias, Session, Behavior);

            // Output = normxy            
            NdArrayData h1 = normxy.FNN(model.MLP1, Session, Behavior);
            NdArrayData h1_act = h1.Act(A_Func.Gelu, Session, Behavior);
            NdArrayData h2 = h1_act.FNN(model.MLP2, Session, Behavior);
            
            h2 = h2.Dropout(0.1f, Session, Behavior);

            NdArrayData o = normxy.Add(h2, Session, Behavior);
            NdArrayData _normo = o.Norm(0, Session, Behavior);
            Output = _normo.DotAndAdd(model.Norm2Scale, model.Norm2Bias, Session, Behavior);
            
        }
    }
}
