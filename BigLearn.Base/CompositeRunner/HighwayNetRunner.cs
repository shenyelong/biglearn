﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    class HighwayEnsembleRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        //CudaPieceInt OutputMask;
        HiddenBatchData AData { get; set; }
        HiddenBatchData BData { get; set; }
        HiddenBatchData GateSwitch { get; set; }

        /// <summary>
        /// o = gate * aData + (1-gate) * bData
        /// </summary>
        /// <param name="aData"></param>
        /// <param name="bData"></param>
        /// <param name="gateSwitch"></param>
        /// <param name="behavior"></param>
        public HighwayEnsembleRunner(HiddenBatchData aData, HiddenBatchData bData, HiddenBatchData gateSwitch, RunnerBehavior behavior)
        : base(Structure.Empty, behavior)
        {
            AData = aData;
            BData = bData;
            GateSwitch = gateSwitch;

            Output = new HiddenBatchData(AData.MAX_BATCHSIZE, AData.Dim, behavior.RunMode, behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = AData.BatchSize;

            // o = a * gate;
            ComputeLib.ElementwiseProductMask(AData.Output.Data, 0, GateSwitch.Output.Data, 0, Output.Output.Data, 0,
                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              AData.BatchSize, AData.Dim, 0, 1);
            // o += b;
            ComputeLib.Matrix_AdditionEx(BData.Output.Data, 0, BData.Dim,
                                         BData.Output.Data, 0, BData.Dim,
                                         Output.Output.Data, 0, Output.Dim,
                                         BData.Dim, BData.BatchSize, 1, 0, 1);

            // o += (-1) * b * gate;
            ComputeLib.ElementwiseProductMask(BData.Output.Data, 0, GateSwitch.Output.Data, 0, Output.Output.Data, 0,
                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              BData.BatchSize, BData.Dim, 1, -1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            // a_deriv += o_deriv * gate;
            ComputeLib.ElementwiseProductMask(Output.Deriv.Data, 0, GateSwitch.Output.Data, 0, AData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                                          CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                          AData.BatchSize, AData.Dim, 1, 1);

            // b_deriv += o_deriv;
            ComputeLib.Matrix_AdditionEx(Output.Deriv.Data, 0, Output.Dim,
                                         Output.Deriv.Data, 0, Output.Dim,
                                         BData.Deriv.Data, 0, BData.Dim,
                                         BData.Dim, BData.BatchSize, 1, 0, 1);

            // b_deriv += (-1) * o_deriv * gate;
            ComputeLib.ElementwiseProductMask(Output.Deriv.Data, 0, GateSwitch.Output.Data, 0, BData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              BData.BatchSize, BData.Dim, 1, -1);

            // gate_deriv += o_deriv * a;
            ComputeLib.ElementwiseProductMask(Output.Deriv.Data, 0, AData.Output.Data, 0, GateSwitch.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              GateSwitch.BatchSize, GateSwitch.Dim, 1, 1);

            // gate_deriv += (-1) * o_deriv * b;
            ComputeLib.ElementwiseProductMask(Output.Deriv.Data, 0, BData.Output.Data, 0, GateSwitch.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              GateSwitch.BatchSize, GateSwitch.Dim, 1, -1);
        }
    }

    public class HighwayNetRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        List<StructRunner> SubRunners = new List<StructRunner>();
        HiddenBatchData DATA;

        public HighwayNetRunner(List<LayerStructure> highwayConnect, List<LayerStructure> highwayGate, HiddenBatchData input, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            DATA = input;
            HiddenBatchData Input = input;
            for (int i = 0; i < highwayConnect.Count; i++)
            {
                FullyConnectHiddenRunner<HiddenBatchData> gateRunner = new FullyConnectHiddenRunner<HiddenBatchData>(
                    highwayGate[i], Input, Behavior);
                SubRunners.Add(gateRunner);

                FullyConnectHiddenRunner<HiddenBatchData> connectRunner = new FullyConnectHiddenRunner<HiddenBatchData>(
                    highwayConnect[i], Input, Behavior);
                SubRunners.Add(connectRunner);

                HighwayEnsembleRunner highwayEnsembleRunner = new HighwayEnsembleRunner(
                    Input, connectRunner.Output, gateRunner.Output, Behavior);
                SubRunners.Add(highwayEnsembleRunner);

                Input = highwayEnsembleRunner.Output;
            }
            Output = Input;
        }

        public override void Forward()
        {
            iteration++;
            //if(name.Equals("docEbd") && iteration == 31)
            //{
            //    DATA.SyncToCPU();
            //}
            foreach (StructRunner runner in SubRunners) { runner.Forward(); }
            //if (name.Equals("docEbd") && iteration == 31)
            //{
            //    Output.SyncToCPU();
            //}
        }

        public override void Backward(bool cleanDeriv)
        {
            //if(iteration == 30 && name.Equals("queryEbd"))
            //{
            //    Output.Deriv.SyncToCPU();
            //    DATA.Deriv.SyncToCPU();
            //}
            foreach (StructRunner runner in Enumerable.Reverse(SubRunners)) { runner.Backward(false); }
            //if (iteration == 30 && name.Equals("queryEbd"))
            //{
            //    Output.Deriv.SyncToCPU();
            //}
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in SubRunners) { runner.CleanDeriv(); }
        }

        public override void Update()
        {
            foreach (StructRunner runner in SubRunners) { runner.Update(); }
        }
    }

}
