﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class ImageCNNRunner : StructRunner<ImageCNNStructure, ImageDataSource> 
    // {
    //     public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

    //     public List<StructRunner> LinkRunners = new List<StructRunner>();

    //     public ImageCNNRunner(ImageCNNStructure model, ImageDataSource input, RunnerBehavior behavior) :
    //         base(model, input, behavior)
    //     {
    //         ImageDataSource layerInput = input;
    //         foreach (ImageLinkStructure linkModel in Model.CNNNeuralLinks)
    //         {
    //             ImageConvRunner<ImageDataSource> linkRunner = new ImageConvRunner<ImageDataSource>(linkModel, layerInput, Behavior);
    //             LinkRunners.Add(linkRunner);
    //             layerInput = linkRunner.Output;
    //         }

    //         ImageFullyConnectRunner<ImageDataSource> fullRunner = new ImageFullyConnectRunner<ImageDataSource>(Model.NNNeuralLinks[0], layerInput, Behavior);
    //         LinkRunners.Add(fullRunner);
    //         HiddenBatchData nnLayerInput = fullRunner.Output;
    //         for (int i = 1; i < model.NNNeuralLinks.Count; i++)
    //         {
    //             FullyConnectHiddenRunner<HiddenBatchData> nnRunner = new FullyConnectHiddenRunner<HiddenBatchData>(model.NNNeuralLinks[i], nnLayerInput, Behavior);
    //             LinkRunners.Add(nnRunner);
    //             nnLayerInput = nnRunner.Output;
    //         }
    //         Output = nnLayerInput;
    //     }

    //     public override void CleanDeriv()
    //     {
    //         foreach (StructRunner runner in LinkRunners) runner.CleanDeriv();
    //     }
    //     public override void Forward()
    //     {
    //         foreach (StructRunner runner in LinkRunners) runner.Forward();
    //     }

    //     public override void Backward(bool cleanDeriv)
    //     {
    //         LinkRunners.Reverse();
    //         foreach (StructRunner runner in LinkRunners) runner.Backward(cleanDeriv);
    //         LinkRunners.Reverse();
    //     }

    //     public override void Update()
    //     {
    //         foreach (StructRunner runner in LinkRunners) { runner.Update(); }
    //     }
    // }
}
