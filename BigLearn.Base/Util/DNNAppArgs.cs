﻿using System;
using System.Linq;

namespace DSMlib
{
    public class DNNAppArgs : CmdArgumentsBase
    {
        #region command line arguments
        [Argument("Paths of the input streams.")]
        public string Inputs { get; set; }

        [Argument("Paths of the output streams.")]
        public string Outputs { get; set; }

        [Argument("Path of the server list file.")]
        public string Servers { get; set; }

        [Argument("The arguements of the application.")]
        public string AppArgs { get; set; }

        [Argument("Maxium Degree of Parallesim of managed thread pool.")]
        public int? MaxDop { get; set; }

        [Argument("Conent format of the input streams.")]
        public StreamFormat InFormat { get; set; }

        [Argument("Conent format of the output streams.")]
        public StreamFormat OutFormat { get; set; }

        [Argument("-f", "Load arguemnts from configure file.")]
        public string ConfigureFile { get; set; }

        [Argument("-v|-verbose", "Run program in verbose mode.", IsPair = false)]
        public bool Verbose { get; set; }
        #endregion
        // Path, Tag
        public TaggedStreamPath[] TaggedInputs
        {
            get
            {
                string ins = this.Inputs;
                return ParseTaggedPaths(ins);
            }
        }

        public TaggedStreamPath[] TaggedOutputs
        {
            get
            {
                string outs = this.Outputs;
                return ParseTaggedPaths(outs);
            }
        }

        public string LegacyConfigureFile
        {
            get
            {
                if (this.UnrecognizedArgs != null)
                {
                    var conf = this.UnrecognizedArgs.Where(p => !p.Value.StartsWith("-")).FirstOrDefault();
                    return conf.Value;
                }

                return null;
            }
        }

        private static TaggedStreamPath[] ParseTaggedPaths(string pathsWithTag)
        {
            if (string.IsNullOrEmpty(pathsWithTag))
            {
                return null;
            }
            else
            {
                return pathsWithTag.Split(";".ToArray(), StringSplitOptions.RemoveEmptyEntries).Select(s => new TaggedStreamPath(s)).ToArray();
            }
        }

        public class TaggedStreamPath
        {
            /// <summary>
            /// TaggedPath format:
            /// Path|Tag
            /// Tag is the last string after seperator '|'
            /// Path can also contains '|', by which can carry schema information for SStream, e.g. 
            /// cosmos://xxx  | BatchId:long, SampleBuf:byte[] | input0
            /// [actual path] |       [Schema]                 | [Tag]
            /// </summary>
            /// <param name="input"></param>
            public TaggedStreamPath(string input)
            {
                int index = input.LastIndexOf('|');
                if (index < 0)
                {
                    this.Path = input;
                    this.Tag = null;
                }
                else
                {
                    this.Path = input.Substring(0, index);
                    this.Tag = input.Substring(index+1);
                }
            }

            public string Path { get; set; }

            public string Tag { get; set; }
        }
    }

    public enum StreamFormat
    {
        Binary,
        IntermedeateSStream
    }
}
