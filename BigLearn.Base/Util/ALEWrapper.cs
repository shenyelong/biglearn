﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum ALEAction
    {
        PLAYER_A_NOOP = 0,
        PLAYER_A_FIRE = 1,
        PLAYER_A_UP = 2,
        PLAYER_A_RIGHT = 3,
        PLAYER_A_LEFT = 4,
        PLAYER_A_DOWN = 5,
        PLAYER_A_UPRIGHT = 6,
        PLAYER_A_UPLEFT = 7,
        PLAYER_A_DOWNRIGHT = 8,
        PLAYER_A_DOWNLEFT = 9,
        PLAYER_A_UPFIRE = 10,
        PLAYER_A_RIGHTFIRE = 11,
        PLAYER_A_LEFTFIRE = 12,
        PLAYER_A_DOWNFIRE = 13,
        PLAYER_A_UPRIGHTFIRE = 14,
        PLAYER_A_UPLEFTFIRE = 15,
        PLAYER_A_DOWNRIGHTFIRE = 16,
        PLAYER_A_DOWNLEFTFIRE = 17,
        PLAYER_B_NOOP = 18,
        PLAYER_B_FIRE = 19,
        PLAYER_B_UP = 20,
        PLAYER_B_RIGHT = 21,
        PLAYER_B_LEFT = 22,
        PLAYER_B_DOWN = 23,
        PLAYER_B_UPRIGHT = 24,
        PLAYER_B_UPLEFT = 25,
        PLAYER_B_DOWNRIGHT = 26,
        PLAYER_B_DOWNLEFT = 27,
        PLAYER_B_UPFIRE = 28,
        PLAYER_B_RIGHTFIRE = 29,
        PLAYER_B_LEFTFIRE = 30,
        PLAYER_B_DOWNFIRE = 31,
        PLAYER_B_UPRIGHTFIRE = 32,
        PLAYER_B_UPLEFTFIRE = 33,
        PLAYER_B_DOWNRIGHTFIRE = 34,
        PLAYER_B_DOWNLEFTFIRE = 35,
        RESET = 40, // MGB: Use SYSTEM_RESET to reset the environment. 
        UNDEFINED = 41,
        RANDOM = 42,
        SAVE_STATE = 43,
        LOAD_STATE = 44,
        SYSTEM_RESET = 45,
        LAST_ACTION_INDEX = 50
    };

    public static class ALELib
    {
        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr CreateEnvironment(int argc, String[] argv);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool FIFOController_IsDone(IntPtr envPtr);
        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int FIFOController_getScreen(IntPtr envPtr, StringBuilder buffer, int bufferLength);
        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int FIFOController_getRAM(IntPtr envPtr, StringBuilder buffer, int bufferLength);
        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool FIFOController_isTerminal(IntPtr envPtr);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int FIFOController_applyActions(IntPtr envPtr, ALEAction action_a, ALEAction action_b);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void FIFOController_display(IntPtr envPtr);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void GetRGBFromValue(IntPtr envPtr, int value, IntPtr rgb);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int FIFOController_getScreenRGB(IntPtr envPtr, IntPtr R, IntPtr G, IntPtr B, int bufferLength);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int FIFOController_getScreenGrey(IntPtr envPtr, IntPtr v, int bufferLength);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int FIFOController_getScreenGreyScale(IntPtr envPtr, IntPtr v, int width, int height, int x1, int y1, int x2, int y2);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int FIFOController_getScreenValueScale(IntPtr envPtr, IntPtr v, int width, int height, int x1, int y1, int x2, int y2);

        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int FIFOController_getScreenValueScaleV2(IntPtr envPtr, IntPtr v, int width, int height, int x1, int y1, int x2, int y2);


        [DllImport("ALE.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void ExitEnvironment(IntPtr envPtr);
    }
}
