﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum LossFunctionType { BinaryClassification = 0, Regression = 1, PairwiseRank = 2, ListwiseRank = 3, OrdinalLogisticRegression = 4, 
                                   BayesianRating = 5, MultiClassification = 6, SampleSoftmax = 7, SampleLogisicalRegression = 8, 
                                   SamplePairRank = 9, OrderedLogitRegression = 10, LambdaRankingLoss = 11, LogBayesianRating = 12, 
                                   SampleNCE = 13, HierarcialSoftmax = 14 }

    // public class LossDerivParameter
    // {
    //     public LossFunctionType lossType;
    //     public DataSourceID InputDataType;
    //     public DataSourceID OutputDataType;
    //     public float LabelMinValue;
    //     public float LabelMaxValue;
    //     public float Gamma = 1;
    //     public object obj;
    // }

    // public class LossFunction
    // {
    //     public static bool IsRankFunction(LossFunctionType type)
    //     {
    //         switch (type)
    //         {
    //             case LossFunctionType.BinaryClassification:
    //             case LossFunctionType.Regression:
    //             case LossFunctionType.MultiClassification:
    //             case LossFunctionType.OrdinalLogisticRegression:
    //             case LossFunctionType.OrderedLogitRegression:
    //                 return false;
    //             case LossFunctionType.PairwiseRank:
    //             case LossFunctionType.ListwiseRank:
    //             case LossFunctionType.BayesianRating:
    //             case LossFunctionType.LogBayesianRating:
    //                 return true;
    //             case LossFunctionType.SampleSoftmax:
    //             case LossFunctionType.SampleLogisicalRegression:
    //             case LossFunctionType.SamplePairRank:
    //             case LossFunctionType.LambdaRankingLoss:
    //                 return false;
    //         }
    //         return true;
    //     }


    //     public static double LossDerivFunction(LossDerivParameter lossParameter, BatchData input, BatchData output)
    //     {
    //         double loss = 0;

    //         switch (lossParameter.lossType)
    //         {
    //             case LossFunctionType.BinaryClassification:      //0
    //             case LossFunctionType.Regression:                //1
    //             case LossFunctionType.MultiClassification:       //2
    //             case LossFunctionType.OrdinalLogisticRegression: //3
    //             case LossFunctionType.OrderedLogitRegression:    //4
    //                 CudaPieceFloat outputPoint = null;
    //                 CudaPieceFloat labelPoint = null;
    //                 CudaPieceFloat derivPoint = null;
    //                 int batchSize = 0;
    //                 int dim = 0;
    //                 switch(lossParameter.OutputDataType)
    //                 {
    //                     case DataSourceID.HiddenBatchData:
    //                         outputPoint = ((HiddenBatchData)output).Output.Data;
    //                         derivPoint = ((HiddenBatchData)output).Deriv.Data;
    //                         dim = ((HiddenBatchData)output).Dim;
    //                         break;
    //                     case DataSourceID.SeqDenseBatchData:
    //                         outputPoint = ((SeqDenseBatchData)output).SentOutput;
    //                         derivPoint = ((SeqDenseBatchData)output).SentDeriv;
    //                         dim = ((SeqDenseBatchData)output).Dim;
    //                         break;
    //                 }
                    
    //                 switch(lossParameter.InputDataType)
    //                 {
    //                     case DataSourceID.GeneralBatchInputData:
    //                         batchSize = ((GeneralBatchInputData)input).BatchSize;
    //                         break;
    //                     case DataSourceID.JDSSMData:
    //                         throw new Exception("LossobjFunction.cs is not support JDSSMData");
    //                     case DataSourceID.DSSMData:
    //                         labelPoint = ((DSSMData)input).ScoreInput.Data;
    //                         batchSize = ((DSSMData)input).BatchSize;
    //                         break;
    //                     case DataSourceID.ImageLabelData:
    //                         throw new Exception("the following code is staled.");
    //                     case DataSourceID.SeqSparseDataSource:
    //                         labelPoint = ((SeqSparseDataSource)input).SequenceLabel;
    //                         batchSize = ((SeqSparseDataSource)input).SeqSize;
    //                         break;
    //                 }
                    
    //                 switch(lossParameter.lossType)
    //                 {
    //                     case LossFunctionType.BinaryClassification:
    //                         loss -= LossFunction.CrossEntropyLoss(outputPoint, labelPoint, derivPoint, batchSize);
    //                         break;
    //                     case LossFunctionType.Regression:
    //                         loss += LossFunction.RegressionLoss(outputPoint, labelPoint, derivPoint, batchSize);
    //                         break;
    //                     case LossFunctionType.MultiClassification:
    //                         /// multiclass classification starts with label 0.
    //                         loss -= LossFunction.MulticlassSoftmaxLoss(outputPoint, labelPoint, derivPoint, null, dim, lossParameter.Gamma, batchSize);
    //                         break;
    //                     case LossFunctionType.OrdinalLogisticRegression:
    //                         loss += LossFunction.OrdinalLogisticRegressionLoss(outputPoint, labelPoint, derivPoint, lossParameter.LabelMinValue, lossParameter.LabelMaxValue, batchSize);
    //                         break;
    //                     case LossFunctionType.OrderedLogitRegression:
    //                         loss += LossFunction.OrderedLogitRegressionLoss(outputPoint, labelPoint, derivPoint, lossParameter.LabelMinValue, lossParameter.LabelMaxValue, lossParameter.Gamma, batchSize);
    //                         break;
    //                 }
    //                 break;

    //             case LossFunctionType.PairwiseRank:         //2
    //             case LossFunctionType.ListwiseRank:         //3
    //             case LossFunctionType.BayesianRating:       //4
    //             case LossFunctionType.LambdaRankingLoss:    //5
    //             case LossFunctionType.LogBayesianRating:
    //                 CudaPieceFloat outputRank = null;
    //                 CudaPieceFloat derivRank = null;
    //                 int dimRank = 0;

    //                 CudaPieceFloat labelRank = null; 
    //                 CudaPieceInt src2MatchIdx = null;
    //                 CudaPieceInt src2MatchElement = null;

    //                 int srcBatchSize = 0;
    //                 int rankBatchSize = 0;
    //                 switch(lossParameter.OutputDataType)
    //                 {
    //                     case DataSourceID.HiddenBatchData:
    //                         outputRank = ((HiddenBatchData)output).Output.Data;
    //                         derivRank = ((HiddenBatchData)output).Deriv.Data;
    //                         dimRank = ((HiddenBatchData)output).Dim;
    //                         break;
    //                 }
                    
    //                 switch(lossParameter.InputDataType)
    //                 {
    //                     case DataSourceID.GeneralBatchInputData:
    //                         rankBatchSize = srcBatchSize;
    //                         break;
    //                     case DataSourceID.JDSSMData:
    //                         throw new Exception("JDSSMData is not supported in LossObjFunction.cs");
    //                 }

    //                 switch (lossParameter.lossType)
    //                 {
    //                     case LossFunctionType.PairwiseRank:
    //                         loss += LossFunction.PairwiseLoss(src2MatchIdx, src2MatchElement, srcBatchSize, outputRank, labelRank, derivRank, rankBatchSize);
    //                         break;
    //                     case LossFunctionType.ListwiseRank:
    //                         loss += LossFunction.ListwiseLoss(src2MatchIdx, src2MatchElement, srcBatchSize, outputRank, labelRank, derivRank, rankBatchSize, lossParameter.Gamma);
    //                         break;
    //                     case LossFunctionType.BayesianRating:
    //                         loss += LossFunction.BayesianRatingLoss(src2MatchIdx, src2MatchElement, srcBatchSize,
    //                                                       outputRank, labelRank, derivRank, rankBatchSize, lossParameter.Gamma);
    //                         break;
    //                     case LossFunctionType.LambdaRankingLoss:
    //                         loss += LossFunction.LambdaRankingLoss(src2MatchIdx, src2MatchElement, srcBatchSize,
    //                                                     outputRank, labelRank, derivRank, rankBatchSize);
    //                         break;
    //                     case LossFunctionType.LogBayesianRating:
    //                         loss += LossFunction.LogBayesianRatingLoss(src2MatchIdx, src2MatchElement, srcBatchSize,
    //                                                       outputRank, labelRank, derivRank, rankBatchSize, lossParameter.Gamma);
    //                         break;
    //                 }
    //                 break;
    //             case LossFunctionType.SampleSoftmax:    //7
    //             case LossFunctionType.SampleLogisicalRegression:     //8
    //                 if (lossParameter.InputDataType == DataSourceID.SeqSparseDataSource && lossParameter.OutputDataType == DataSourceID.SeqDenseBatchData)
    //                 {
    //                     SeqSparseDataSource Input = (SeqSparseDataSource)input;
    //                     SeqDenseBatchData Output = (SeqDenseBatchData)output;

    //                     //Ouput.SentOutput.CopyOutFromCuda();
    //                     EmbeddingLossDerivParameter parameter = (EmbeddingLossDerivParameter)lossParameter.obj;

    //                     //parameter.NegativeIndex.CopyOutFromCuda();
    //                     Output.SentDeriv.Zero();

    //                     switch (lossParameter.lossType)
    //                     {
    //                         case LossFunctionType.SampleSoftmax:
    //                             Cudalib.UpdateSoftmaxEmbedding(Input.SequenceLabel.CudaPtr, Input.SeqSize,
    //                                 Output.SentOutput.CudaPtr, Output.SentDeriv.CudaPtr, Output.Dim, parameter.vSpace.CudaPtr, parameter.vBias.CudaPtr, parameter.vocabSize,
    //                                 parameter.stepSize, parameter.Simi.CudaPtr, parameter.Alpha.CudaPtr, parameter.NegativeNum,
    //                                 parameter.gamma, parameter.NegativeIndex.CudaPtr, Util.CudaRandom, Util.RandomNum, Util.URandom.Next(Util.RandomNum));
                                
    //                             parameter.Simi.CopyOutFromCuda();
    //                             parameter.Alpha.CopyOutFromCuda();
    //                             for (int i = 0; i < Input.SeqSize; i++)
    //                                 loss += parameter.Simi.MemPtr[i *(parameter.NegativeNum + 1) ] * parameter.gamma - parameter.Alpha.MemPtr[i];
    //                             loss = loss / Input.SeqSize;
    //                             break;
    //                         case LossFunctionType.SampleLogisicalRegression:
    //                             Cudalib.UpdateEmbedding(Input.SequenceLabel.CudaPtr, Input.SeqSize,
    //                                 Output.SentOutput.CudaPtr, Output.SentDeriv.CudaPtr, Output.Dim, parameter.vSpace.CudaPtr, parameter.vBias.CudaPtr, parameter.vocabSize,
    //                                 parameter.stepSize, parameter.Simi.CudaPtr, parameter.Alpha.CudaPtr, parameter.NegativeNum,
    //                                 parameter.gamma, parameter.NegativeIndex.CudaPtr, Util.CudaRandom, Util.RandomNum, Util.URandom.Next(Util.RandomNum));
                                
    //                             parameter.Simi.CopyOutFromCuda();
    //                             parameter.NegativeIndex.CopyOutFromCuda();

    //                             for (int i = 0; i < Input.SeqSize; i++)
    //                             {
    //                                 loss += -(float)Math.Log(1 + Math.Exp(-parameter.Simi.MemPtr[i * (parameter.NegativeNum + 1)] * parameter.gamma));
    //                                 for (int n = 0; n < parameter.NegativeNum; n++)
    //                                     loss += -parameter.Simi.MemPtr[i * (parameter.NegativeNum + 1) + n + 1] * parameter.gamma -
    //                                             (float)Math.Log(1 + Math.Exp(-parameter.Simi.MemPtr[i * (parameter.NegativeNum + 1) + n + 1] * parameter.gamma));
    //                             }
    //                             loss = loss / (Input.SeqSize * (parameter.NegativeNum + 1));
    //                             break;
    //                     }
    //                     if (loss == float.NaN)
    //                         Console.WriteLine("Something Bad Happen!!");
    //                 }
    //                 break;
    //             case LossFunctionType.SampleNCE:    //13
    //                 if (lossParameter.InputDataType == DataSourceID.SeqSparseDataSource && lossParameter.OutputDataType == DataSourceID.SeqDenseBatchData)
    //                 {
    //                     SeqSparseDataSource Input = (SeqSparseDataSource)input;
    //                     SeqDenseBatchData Output = (SeqDenseBatchData)output;

    //                     //Ouput.SentOutput.CopyOutFromCuda();
    //                     EmbeddingLossDerivParameter parameter = (EmbeddingLossDerivParameter)lossParameter.obj;

                        
    //                     Output.SentDeriv.Zero();

    //                     bool isSuccess1 = Cudalib.TestCuda();

    //                     int randomStart = (int)(Util.URandom.NextDouble() * Util.RandomNum);
    //                     float[] randomMemory = Util.CPURandom;

    //                     Parallel.For(0, ((SeqSparseDataSource)input).SeqSize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
    //                     {
    //                         for(int n=0;n<parameter.NegativeNum;n++)
    //                         {
    //                             int pos = (int)(randomMemory[(randomStart + i * parameter.NegativeNum + n) % Util.RandomNum] * parameter.VocabSampleSize) % parameter.VocabSampleSize;
    //                             parameter.NegativeIndex.MemPtr[i * parameter.NegativeNum + n] = parameter.VocabSampleTable.MemPtr[pos];
    //                             //int pos = (int)(randomMemory[ (randomStart + i * parameter.NegativeNum + n) % Util.RandomNum] * parameter.vocabSize) % parameter.vocabSize;
    //                             //parameter.NegativeIndex.MemPtr[i * parameter.NegativeNum + n] = pos; // parameter.VocabSampleTable.MemPtr[pos];
    //                         }
    //                     });

    //                     parameter.NegativeIndex.SyncFromCPU();

    //                     //Cudalib.UpdateBalanceEmbedding(Input.SequenceLabel.CudaPtr, Input.SeqSize,
    //                     //      Output.SentOutput.CudaPtr, Output.SentDeriv.CudaPtr, Output.Dim, parameter.vSpace.CudaPtr, parameter.vBias.CudaPtr, parameter.vocabSize,
    //                     //      parameter.stepSize, parameter.Simi.CudaPtr, parameter.Alpha.CudaPtr, parameter.NegativeNum,
    //                     //      parameter.gamma, parameter.NegativeIndex.CudaPtr, Util.CudaRandom, Util.RandomNum, Util.URandom.Next(Util.RandomNum),
    //                     //      parameter.VocabSampleTable.CudaPtr, parameter.VocabSampleSize);

    //                     Cudalib.UpdateBinaryEmbedding_NegIds(Input.SequenceLabel.CudaPtr, Input.SeqSize,
    //                           Output.SentOutput.CudaPtr, Output.SentDeriv.CudaPtr, Output.Dim, parameter.vSpace.CudaPtr, parameter.vBias.CudaPtr, parameter.vocabSize,
    //                           parameter.stepSize, parameter.Simi.CudaPtr, parameter.Alpha.CudaPtr, parameter.NegativeNum,
    //                           parameter.gamma, parameter.NegativeIndex.CudaPtr);

    //                     parameter.Simi.CopyOutFromCuda();
    //                     //parameter.Alpha.CopyOutFromCuda();
    //                     //parameter.NegativeIndex.CopyOutFromCuda();

    //                     bool isSuccess2 = Cudalib.TestCuda();

    //                     if (isSuccess1 && !isSuccess2)
    //                     {
    //                         Console.WriteLine("UpdataBalance Problem");
    //                         Console.ReadLine();
    //                     }

    //                     for (int i = 0; i < Input.SeqSize; i++)
    //                     {
    //                         loss += -(float)Math.Log(1 + Math.Exp(-parameter.Simi.MemPtr[i * (parameter.NegativeNum + 1)] * parameter.gamma));

    //                         for (int n = 0; n < parameter.NegativeNum; n++)
    //                         {
    //                             loss += -parameter.Simi.MemPtr[i * (parameter.NegativeNum + 1) + n + 1] * parameter.gamma -
    //                                     (float)Math.Log(1 + Math.Exp(-parameter.Simi.MemPtr[i * (parameter.NegativeNum + 1) + n + 1] * parameter.gamma));
    //                         }
    //                     }
    //                     Output.SentDeriv.CopyOutFromCuda();
    //                     if (loss == float.NaN)
    //                     {
    //                         Console.WriteLine("Something Bad Happen!!");
    //                     }
    //                     loss = loss / (Input.SeqSize * (parameter.NegativeNum + 1));

    //                 }
    //                 break;
    //             case LossFunctionType.HierarcialSoftmax:
    //                 if (lossParameter.InputDataType == DataSourceID.SeqSparseDataSource && lossParameter.OutputDataType == DataSourceID.SeqDenseBatchData)
    //                 {
    //                     SeqSparseDataSource Input = (SeqSparseDataSource)input;
    //                     SeqDenseBatchData Output = (SeqDenseBatchData)output;
    //                     //Ouput.SentOutput.CopyOutFromCuda();
    //                     EmbeddingLossDerivParameter parameter = (EmbeddingLossDerivParameter)lossParameter.obj;
    //                     //parameter.NegativeIndex.CopyOutFromCuda();
    //                     bool isSuccess1 = Cudalib.TestCuda();
    //                     Output.SentDeriv.Zero();
    //                     //Cudalib.HierarcialSoftmaxEmbedding(Input.SequenceLabel.CudaPtr, Input.SeqSize,
    //                     //      Output.SentOutput.CudaPtr, Output.SentDeriv.CudaPtr, Output.Dim, parameter.vSpace.CudaPtr, parameter.vBias.CudaPtr, parameter.vocabSize,
    //                     //      parameter.cSpace.CudaPtr, parameter.cBias.CudaPtr, parameter.classNum,
    //                     //      parameter.stepSize, parameter.vocab2class.CudaPtr, parameter.classIndex.CudaPtr, parameter.wordIndex.CudaPtr, parameter.wordClassIndex.CudaPtr,
    //                     //      parameter.gamma, parameter.classOutput.CudaPtr, parameter.wordOutput.CudaPtr, parameter.classVocabNum, parameter.Prob.CudaPtr);

    //                     Cudalib.HierarcialSoftmaxEmbeddingV2(Input.SequenceLabel.CudaPtr, Input.SeqSize,
    //                           Output.SentOutput.CudaPtr, Output.SentDeriv.CudaPtr, Output.Dim, parameter.vSpace.CudaPtr, parameter.vBias.CudaPtr, parameter.vocabSize,
    //                           parameter.cSpace.CudaPtr, parameter.cBias.CudaPtr, parameter.classNum,
    //                           parameter.stepSize, parameter.vocab2class.CudaPtr, parameter.classIndex.CudaPtr, parameter.wordIndex.CudaPtr, parameter.wordClassIndex.CudaPtr,
    //                           parameter.gamma, parameter.classOutput.CudaPtr, parameter.wordOutput.CudaPtr, 
    //                           parameter.wordAct1.CudaPtr, parameter.wordAct2.CudaPtr,
    //                           parameter.classVocabNum, parameter.Prob.CudaPtr);
                        

    //                     //parameter.Alpha.CopyOutFromCuda();
    //                     bool isSuccess2 = Cudalib.TestCuda();
    //                     if (isSuccess1 && !isSuccess2)
    //                     {
    //                         Console.WriteLine("UpdataBalance Problem");
    //                         Console.ReadLine();
    //                     }
    //                     parameter.Prob.CopyOutFromCuda();
    //                     float batchLoss = Enumerable.Range(0, Input.SeqSize).Select(i =>
    //                                  (float)Math.Log(Math.Max(float.Epsilon, parameter.Prob.MemPtr[i]),2)).Sum();
    //                     loss += batchLoss * 1.0f / Input.SeqSize;
    //                     if (loss == float.NaN)
    //                     {
    //                         Console.WriteLine("Something Bad Happen!!");
    //                     }
    //                 }
    //                 break;
    //             case LossFunctionType.SamplePairRank:               //9
    //                 break;
                
    //         }
    //         return loss;
    //     }

    //     public static double CrossEntropyLoss(CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int batchSize)
    //     {
    //         ///Compute Loss.
    //         double miniBatchLoss = 0;
    //         MathOperatorManager.GlobalInstance.DerivCrossEntropy(output, label, deriv, batchSize);

    //         output.SyncToCPU(batchSize);
    //         label.SyncToCPU(batchSize);

    //         for (int i = 0; i < batchSize; i++)
    //         {
    //             double score = (Math.Tanh(output.MemPtr[i] / 2) + 1) / 2;

    //             if (label.MemPtr[i] == ParameterSetting.MissingLabel) continue;
    //             else if (label.MemPtr[i] > 0) miniBatchLoss += score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(score);
    //             else miniBatchLoss += 1 - score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(1 - score);
    //         }
    //         return miniBatchLoss ;/// batchSize;
    //     }

    //     public static double MulticlassSoftmaxLoss(CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, CudaPieceFloat weight, int dim, float gamma, int batchSize)
    //     {
    //         output.SyncToCPU();
    //         int step = output.MemPtr.Length / batchSize;

    //         MathOperatorManager.GlobalInstance.SoftMax(output, deriv, dim, batchSize, gamma);

    //         if(label != null) label.SyncToCPU(batchSize);
    //         deriv.SyncToCPU(batchSize * dim);
    //         if (weight != null) weight.SyncToCPU(batchSize);

    //         double miniBatchLoss = 0;
    //         for (int i = 0; i < batchSize; i++)
    //         {
    //             int l = label == null ? 0 : (int)label.MemPtr[i];
    //             float wei = weight == null ? 1 : weight.MemPtr[i];
    //             miniBatchLoss += wei * Math.Log(Math.Max(deriv.MemPtr[i * dim + l], float.Epsilon));
                
    //             deriv.MemPtr[i * dim + l] -= 1;

    //             if (weight != null)
    //                 for (int d = 0; d < dim; d++)
    //                     deriv.MemPtr[i * dim + d] = deriv.MemPtr[i * dim + d] * wei;
    //         }
    //         deriv.SyncFromCPU(batchSize * dim);
    //         MathOperatorManager.GlobalInstance.Scale_Matrix(deriv, batchSize, dim, -gamma);
    //         deriv.SyncToCPU();
    //         return miniBatchLoss/batchSize;  
    //     }

    //     public static float RegressionLoss(CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int batchSize)
    //     {
    //         ///Compute Gradient.
    //         Cudalib.RegressionLoss(output.CudaPtr, label.CudaPtr, deriv.CudaPtr, batchSize);

    //         ///Calculate Loss.
    //         float miniBatchLoss = 0;
    //         deriv.CopyOutFromCuda(batchSize);
    //         for (int i = 0; i < batchSize; i++) miniBatchLoss += (float)(deriv.MemPtr[i] * deriv.MemPtr[i] / 4);
    //         if (float.IsNaN(miniBatchLoss) || batchSize == 0) Console.WriteLine("Regression Loss goes to NAN");
    //         return miniBatchLoss;
    //     }

    //     public static float RegressionLoss(CudaPieceFloat output, CudaPieceFloat index, CudaPieceFloat label, CudaPieceFloat deriv, int dim, int batchSize)
    //     {
    //         ///Compute Gradient.
    //         output.SyncToCPU();
    //         index.SyncToCPU();
    //         label.SyncToCPU();
    //         deriv.Zero();

    //         float miniBatchLoss = 0;
    //         for(int i=0;i<batchSize;i++)
    //         {
    //             float bias = (label.MemPtr[i] - output.MemPtr[i * dim + (int)index.MemPtr[i]]);
    //             deriv.MemPtr[i * dim + (int)index.MemPtr[i]] += (2 * bias) / batchSize;
    //             miniBatchLoss += bias * bias;
    //         }
    //         deriv.SyncFromCPU();
    //         return miniBatchLoss;
    //     }


    //     public static float RegressionSlotLoss(float[] output, float[] label, int[] mark, float[] deriv, int batchSize, bool isMark, bool isDeriv)
    //     {
    //         float miniBatchLoss = 0;
    //         int id = 0;
    //         for (int i = 0; i < batchSize; i++)
    //         {
    //             float derivValue = 0;
    //             if ((isMark && mark[i] > 0) || (!isMark && mark[i] <= 0))
    //             {
    //                 derivValue = 2 * (label[i] - output[i]);
    //                 id += 1;
    //             }
    //             if (isDeriv) deriv[i] = derivValue;
    //             miniBatchLoss += (float)(derivValue * derivValue / 2);
    //         }
    //         return miniBatchLoss / (id + float.Epsilon);
    //     }

    //     static CudaPieceFloat SrcBatchLoss = null;
    //     static CudaPieceFloat LogitParameter = null;
    //     static CudaPieceFloat DefaultLabel = null;
    //     public static float PairwiseLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize,
    //                                     CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int batchSize)
    //     {
    //         if (SrcBatchLoss == null) SrcBatchLoss = new CudaPieceFloat(srcBatchSize, true, true);

    //         if (SrcBatchLoss.Size < srcBatchSize) SrcBatchLoss.Resize(srcBatchSize);

    //         Cudalib.PairwiseRankingLoss(src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, srcBatchSize, SrcBatchLoss.CudaPtr,
    //                                     output.CudaPtr, label.CudaPtr, deriv.CudaPtr, batchSize, Util.GPUEpsilon);
    //         float miniBatchLoss = 0;
    //         SrcBatchLoss.CopyOutFromCuda(srcBatchSize);
    //         for (int i = 0; i < srcBatchSize; i++) miniBatchLoss += (float)SrcBatchLoss.MemPtr[i];
    //         return miniBatchLoss / srcBatchSize;
    //     }

    //     public static float LambdaRankingLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize,
    //                                     CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int batchSize)
    //     {
    //         if (SrcBatchLoss == null) SrcBatchLoss = new CudaPieceFloat(srcBatchSize, true, true);

    //         if (SrcBatchLoss.Size < srcBatchSize) SrcBatchLoss.Resize(srcBatchSize);

    //         Cudalib.LambdaRankingLoss(src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, srcBatchSize, SrcBatchLoss.CudaPtr,
    //                                     output.CudaPtr, label.CudaPtr, deriv.CudaPtr, batchSize, Util.GPUEpsilon, 10);
    //         float miniBatchLoss = 0;
    //         SrcBatchLoss.CopyOutFromCuda(srcBatchSize);
    //         for (int i = 0; i < srcBatchSize; i++) miniBatchLoss += (float)SrcBatchLoss.MemPtr[i];
    //         return miniBatchLoss / srcBatchSize;
    //     }
        
    //     public static float ListwiseLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize,
    //                                     CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int batchSize, float gamma)
    //     {
    //         if (SrcBatchLoss == null) SrcBatchLoss = new CudaPieceFloat(srcBatchSize, true, true);
    //         if (SrcBatchLoss.Size < srcBatchSize) SrcBatchLoss.Resize(srcBatchSize);
    //         Cudalib.ListwiseRankingLoss(src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, srcBatchSize, SrcBatchLoss.CudaPtr,
    //                                     output.CudaPtr, label.CudaPtr, deriv.CudaPtr, batchSize, Util.GPUEpsilon, gamma);
    //         float miniBatchLoss = 0;
    //         SrcBatchLoss.CopyOutFromCuda(srcBatchSize);
    //         for (int i = 0; i < srcBatchSize; i++) miniBatchLoss += (float)SrcBatchLoss.MemPtr[i];
    //         return miniBatchLoss / srcBatchSize;
    //     }

    //     public static float OrdinalLogisticRegressionLoss(CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, float labelMin, float labelMax, int batchSize)
    //     {
    //         ///Compute Gradient. ///Calculate Loss.
    //         output.CopyOutFromCuda();
    //         label.CopyOutFromCuda();

    //         float miniBatchLoss = 0;
    //         for (int i = 0; i < batchSize; i++)
    //         {
    //             float score = Util.Logistic(output.MemPtr[i]);
    //             float target = (label.MemPtr[i] - labelMin) / (labelMax - labelMin + float.Epsilon);
    //             deriv.MemPtr[i] = (target - score);
    //             miniBatchLoss += (float)(target * Math.Log(score + float.Epsilon) + (1 - target) * Math.Log(1 - score + float.Epsilon));
    //         }
    //         deriv.CopyIntoCuda();

    //         return miniBatchLoss / batchSize;
    //     }

    //     public static float OrderedLogitRegressionLoss(CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, float labelMin, float labelMax, float gamma, int batchSize)
    //     {
    //         if (LogitParameter == null)
    //         {
    //             LogitParameter = new CudaPieceFloat((int)(labelMax - labelMin), true, true);
    //             for (int i = 0; i < (int)(labelMax - labelMin); i++)
    //             {
    //                 LogitParameter.MemPtr[i] =  (float)Math.Log(2.0f * gamma / (labelMax - labelMin + 1)); //-gamma; // *1.0f / (labelMax - labelMin);
    //             }

    //             LogitParameter.MemPtr[0] = -gamma + (float)(2.0f * gamma / (labelMax - labelMin + 1));
    //         }

    //         ///Compute Gradient. ///Calculate Loss.
    //         output.CopyOutFromCuda();
    //         label.CopyOutFromCuda();

    //         float[] threshold = new float[(int)(labelMax - labelMin)];
    //         threshold[0] = LogitParameter.MemPtr[0];
    //         for (int r = 1; r < (int)(labelMax - labelMin); r++)
    //         {
    //             threshold[r] = threshold[r - 1] + (float)Math.Exp(LogitParameter.MemPtr[r]);
    //         }

    //         float[] score = new float[(int)(labelMax - labelMin)];
    //         float[] prob = new float[(int)(labelMax - labelMin + 1)];
    //         float[] derivLogit = new float[(int)(labelMax - labelMin)];
    //         float miniBatchLoss = 0;
    //         for (int i = 0; i < batchSize; i++)
    //         {
    //             deriv.MemPtr[i] = 0;
    //             for (int r = 0; r < (int)(labelMax - labelMin); r++)
    //             {
    //                 score[r] = Util.Logistic(1 * (output.MemPtr[i] - threshold[r]));
    //             }

    //             int targetId = (int)label.MemPtr[i];

    //             int rr = targetId - (int)labelMin;
    //             float up = rr == 0 ? 1 : score[rr - 1];
    //             float down = rr == (int)(labelMax - labelMin) ? 0 : score[rr];
    //             prob[rr] = up - down;

    //             miniBatchLoss += (float)Math.Log(prob[rr] + Util.GPUEpsilon);
    //             if (targetId == (int)labelMin)
    //             {
    //                 deriv.MemPtr[i] += -1 * score[rr];
    //                 derivLogit[rr] += 1 * score[rr];
    //             }
    //             else if (targetId == (int)labelMax)
    //             {
    //                 deriv.MemPtr[i] += 1 * (1 - score[rr - 1]);
    //                 derivLogit[rr - 1] -= 1 * (1 - score[rr - 1]);
    //             }
    //             else if (targetId < (int)(labelMax) && targetId > (int)labelMin)
    //             {
    //                 deriv.MemPtr[i] += 1 * (1 - (score[rr - 1] + score[rr]));
    //                 derivLogit[rr] += 1 * score[rr] * (1 - score[rr]) / (score[rr - 1] - score[rr] + Util.GPUEpsilon);
    //                 derivLogit[rr - 1] += -1 * score[rr - 1] * (1 - score[rr - 1]) / (score[rr - 1] - score[rr] + Util.GPUEpsilon);
    //             }
    //         }

    //         float[] derivAccum = new float[(int)(labelMax - labelMin)];
    //         for (int r = (int)(labelMax - labelMin - 1); r >= 0 ; r--)
    //         {
    //             for (int s = 1; s <= r; s++)
    //             {
    //                 derivAccum[s] += derivLogit[r] * (float)Math.Exp(LogitParameter.MemPtr[s]);
    //             }
    //             derivAccum[0] += derivLogit[r];
    //         }

    //         for(int r = 0; r < (int)(labelMax - labelMin); r++)
    //         {
    //             LogitParameter.MemPtr[r] += 0.00000001f * derivAccum[r];
    //         }
            
    //         deriv.CopyIntoCuda();

    //         return miniBatchLoss / (batchSize);
    //     }
    //     public static float BayesianRatingLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize,
    //                                     CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int batchSize, float gamma)
    //     {
    //         if (SrcBatchLoss == null) SrcBatchLoss = new CudaPieceFloat(srcBatchSize, true, true);
    //         if (SrcBatchLoss.Size < srcBatchSize) SrcBatchLoss.Resize(srcBatchSize);


    //         Cudalib.BayesianRatingLoss(src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, srcBatchSize, SrcBatchLoss.CudaPtr,
    //                                     output.CudaPtr, label.CudaPtr, deriv.CudaPtr, batchSize, Util.GPUEpsilon, gamma);
    //         float miniBatchLoss = 0;
    //         SrcBatchLoss.CopyOutFromCuda(srcBatchSize);
    //         //deriv.CopyOutFromCuda();
    //         for (int i = 0; i < srcBatchSize; i++) miniBatchLoss += (float)SrcBatchLoss.MemPtr[i];
    //         return miniBatchLoss / srcBatchSize;
    //     }


    //     public static float LogBayesianRatingLoss(CudaPieceInt src2MatchIdx, CudaPieceInt src2MatchElement, int srcBatchSize,
    //                                     CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int batchSize, float gamma)
    //     {
    //         if (SrcBatchLoss == null) SrcBatchLoss = new CudaPieceFloat(srcBatchSize, true, true);
    //         if (SrcBatchLoss.Size < srcBatchSize) SrcBatchLoss.Resize(srcBatchSize);

    //         //label.CopyOutFromCuda();
    //         //output.CopyOutFromCuda();

    //         Cudalib.LogBayesianRatingLoss(src2MatchIdx.CudaPtr, src2MatchElement.CudaPtr, srcBatchSize, SrcBatchLoss.CudaPtr,
    //                                     output.CudaPtr, label.CudaPtr, deriv.CudaPtr, (uint)batchSize, Util.GPUEpsilon, gamma);
    //         float miniBatchLoss = 0;
    //         SrcBatchLoss.CopyOutFromCuda(srcBatchSize);
    //         //deriv.CopyOutFromCuda();
    //         for (int i = 0; i < srcBatchSize; i++) miniBatchLoss += (float)SrcBatchLoss.MemPtr[i];
    //         return miniBatchLoss / srcBatchSize;
    //     }
    // }
}
