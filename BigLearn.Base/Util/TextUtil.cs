﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BigLearn
{
    
    /// <summary>
    /// Item Index & Freq Table.
    /// </summary>
    public class ItemFreqIndexDictionary
    {
        string ItemName { get; set; }
        public ItemFreqIndexDictionary(string name) { ItemName = name; }
        public ItemFreqIndexDictionary(StreamReader reader, bool isHeader = true) { Load(reader, isHeader); }

        public int ItemDictSize { get { return ItemIndex.NameCount; } }

        //itemIndex -> itemfreq.
        public Dictionary<int, int> ItemFreq = new Dictionary<int, int>();
        //itemName -> itemIndex.
        public NameIndex ItemIndex = new NameIndex();

        public int PushItem(string key)
        {
            int itemIdx = ItemIndex.Index(key, true);
            if (ItemFreq.ContainsKey(itemIdx)) { ItemFreq[itemIdx] += 1; }
            else { ItemFreq[itemIdx] = 1; }
            return itemIdx;
        }

        public void BuildIndex(IEnumerable<string[]> paragraphs)
        {
            foreach(string[] line in paragraphs)
            {
                foreach(string i in line)
                {
                    PushItem(i);
                }
            }
        }


        public int IndexItem(string key)
        {
            return ItemIndex.Index(key, false);
        }


        public void Filter(int freqThreshold, int maxItemNum)
        {
            IEnumerable<KeyValuePair<int,int>> itemList = ItemFreq.Where(entry => entry.Value >= freqThreshold).OrderByDescending(entry => entry.Value);
            if (maxItemNum > 0 && maxItemNum < int.MaxValue)
            {
                itemList = itemList.Take(maxItemNum);
            }
            NameIndex filterItemIndex = new NameIndex();
            Dictionary<int, int> filterItemFreq = new Dictionary<int, int>();
            foreach (KeyValuePair<int,int> kv in itemList)
            {
                int filterIdx = filterItemIndex.Index(ItemIndex.GetName(kv.Key), true);
                filterItemFreq.Add(filterIdx, kv.Value);
            }
            ItemIndex = filterItemIndex;
            ItemFreq = filterItemFreq;
        }

        /// <summary>
        /// in order to call load vocab, the header must be included.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="hasHeader"></param>
        public void Save(StreamWriter writer, bool hasHeader = true)
        {
            if(hasHeader) writer.WriteLine(string.Format("{0}\t{1}", ItemName, ItemDictSize));
            HashSet<string> savedkeys = new HashSet<string>();
            foreach (var item in ItemFreq)
            {
                string key = ItemIndex.GetName(item.Key);
                if (savedkeys.Contains(key) == false)
                {
                    savedkeys.Add(key);
                }
                else
                {
                    continue;
                }
                writer.WriteLine("{0}\t{1}\t{2}", key, item.Key, item.Value);
            }
            writer.Flush();
        }

        public void Load(StreamReader reader, bool hasHeader = true)
        {
            if (hasHeader)
            {
                string[] header = reader.ReadLine().Split('\t');
                ItemName = header[0];
                for (int i = 0; i < int.Parse(header[1]); i++)
                {
                    if (reader.EndOfStream)
                    {
                        Console.WriteLine("{0}", i);
                        break;
                    }
                    string[] item = reader.ReadLine().Split('\t');
                    ItemIndex.Index(item[0], int.Parse(item[1]));
                    ItemFreq[int.Parse(item[1])] = int.Parse(item[2]);
                }
            }
            else
            {
                ItemName = "EmptyName";
                while (!reader.EndOfStream)
                {
                    string[] item = reader.ReadLine().Split('\t');

                    string entity = item[0];
                    int idx = item.Length > 1 ? int.Parse(item[1]) : ItemIndex.NameCount;
                    ItemIndex.Index(entity, idx);
                    int freq = item.Length > 2 ? int.Parse(item[2]) : 1;
                    ItemFreq[idx] = freq;
                }
            }
        }
    }




    public class TextUtil
    {
        public static bool IsEnglishNumber(string word)
        {
            foreach (char c in word.ToLower())
            {
                if ( (c.CompareTo('a') >= 0 && c.CompareTo('z') <= 0) || ( c.CompareTo('0') >= 0 && c.CompareTo('9') <= 0 ) )
                    continue;
                else
                    return false;
            }
            return true;
        }

        public static bool IsEnglishNumber(char c)
        {
            if ((c.CompareTo('a') >= 0 && c.CompareTo('z') <= 0) || (c.CompareTo('0') >= 0 && c.CompareTo('9') <= 0)
                || (c.CompareTo('A') >= 0 && c.CompareTo('Z') <= 0))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string CleanHTMLText(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string[] AdvancedTextSplit(string text)
        {
            List<string> tokens = new List<string>();
            string token = string.Empty;
            foreach (char c in text)
            {
                if (IsEnglishNumber(c))
                {
                    token = token + c;
                }
                else if (!token.Equals(string.Empty))
                {
                    tokens.Add(token);
                    token = string.Empty;
                }
            }

            if (!token.Equals(string.Empty))
            {
                tokens.Add(token);
            }

            return tokens.ToArray();
        }

        public static string PrunningEnglishText(string text)
        {
            string[] tokens = text.Split(TextTokenizeParameters.WordTokens, StringSplitOptions.RemoveEmptyEntries);
            foreach (string token in tokens)
            {
                if (IsEnglishNumber(token))
                    continue;
                return string.Empty;
            }
            return string.Join(" ",tokens);
        }


        public static string Normalize(string str)
        {
            return str.Replace('\t', ' ').Replace('\r', ' ').Replace('\n', ' ');
        }

        public static string BriefText(string str, int head, int tail)
        {
            if (str.Length > head + tail) return str.Substring(0, Math.Min(head, str.Length) - 1) + str.Substring(str.Length - tail);
            else return str;
        }

        public static string Dict2Str<K,V>(Dictionary<K, V> fea)
        {
            return string.Join(" ", fea.Select(kv => kv.Key.ToString() + ":" + kv.Value.ToString()));
        }

        public static Dictionary<K, V> Str2Dict<K, V>(string feaStr)
        {
            return feaStr == null || feaStr.Trim().Equals("") ? new Dictionary<K, V>() : feaStr.Split(new string[]{" "},StringSplitOptions.RemoveEmptyEntries).
                    Select(item => item.Split(':')).ToDictionary(kv => (K)(Convert.ChangeType(kv[0], typeof(K))), kv => (V)(Convert.ChangeType(kv[1], typeof(V))));
        }

        public static string Dict2Str(Dictionary<int, float> fea)
        {
            return string.Join(" ", fea.Select(kv => kv.Key.ToString() + ":" + kv.Value.ToString()));
        }

        public static string Dicts2Str(List<Dictionary<int, float>> feas)
        {
            return string.Join("#", feas.Select(fea => Dict2Str(fea)));
        }

        public static Dictionary<int, float> Str2Dict(string feaStr)
        {
            return feaStr.Trim().Equals("") ? new Dictionary<int,float>() : feaStr.Split(' ').Select(item => item.Split(':')).ToDictionary(kv => int.Parse(kv[0]), kv => float.Parse(kv[1]));
        }

        public static List<Dictionary<int, float>> Str2Dicts(string feasStr)
        {
            return feasStr.Split('#').Select(feaStr => Str2Dict(feaStr)).ToList();
        }

        public static Dictionary<string, int> ReadHeader(string headerStr, char spliter)
        {
            string[] columns = headerStr.Split(spliter);
            return Enumerable.Range(0, columns.Count()).ToDictionary(i => columns[i], i => i);
        }


        public static Dictionary<int, float> TLCFeatureExtract(string[] items, ref int featureDim)
        {
            Dictionary<int, float> feaDict = new Dictionary<int, float>();
            for (int i = 0; i < items.Length; i++)
            {
                try {
                    //second collumn (index 1) can be also amount of features in TLC format
                    if (items[i].Contains(":"))
                    {
                        string[] kv = items[i].Trim().Split(':');
                        int k = int.Parse(kv[0]);
                        float v = float.Parse(kv[1]);
                        feaDict.Add(k, v);
                        if (k >= featureDim) featureDim = k + 1;
                    }
                    else if (i == 1 && featureDim < int.Parse(items[i]))
                    {
                        featureDim = int.Parse(items[i]);
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.ToString());
                    Console.WriteLine(string.Join(" ", items));
                    throw;
                }
            }
            return feaDict;
        }


        public static List<float> TLCDenseFeatureExtract(string[] items, ref int featureDim)
        {
            List<float> feaList = new List<float>();
            for (int i = 0; i < items.Length; i++)
            {
                //second collumn (index 1) can be also amount of features in TLC format
                if (items[i].Contains(":"))
                {
                    string[] kv = items[i].Trim().Split(':');
                    int k = int.Parse(kv[0]);
                    float v = float.Parse(kv[1]);
                    feaList.Add(v);
                }
                else if( i == 1 && featureDim < int.Parse(items[i]))
                {
                    featureDim = int.Parse(items[i]);
                }
            }
            featureDim = Math.Max(featureDim, feaList.Count);
            return feaList;
        }

        public static bool TermSetMatch(HashSet<string> text, HashSet<string> query)
        {
            foreach (string term in query)
            {
                bool isMatch = false;
                foreach (string textTerm in text)
                {
                    if (textTerm.StartsWith(term))
                    {
                        isMatch = true;
                        break;
                    }
                }
                if (!isMatch)
                    return false;
            }
            return true;
        }

        public static bool TermSetEqualMatch(HashSet<string> text, HashSet<string> query)
        {
            foreach (string term in query)
            {
                bool isMatch = false;
                foreach (string textTerm in text)
                {
                    if (textTerm.Equals(term))
                    {
                        isMatch = true;
                        break;
                    }
                }
                if (!isMatch)
                    return false;
            }
            return true;
        }


        public static bool TermContainMatch(string text, string query)
        {
            string[] terms = query.ToLower().Split(new string[] {" ", ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string term in terms)
            {
                if (text.ToLower().Contains(term))
                    continue;
                else
                    return false;
            }
            return true;
        }

        public static bool TermContainMatch(HashSet<string> text, string query)
        {
            string[] terms = query.ToLower().Split(new string[] { " ", ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string term in terms)
            {
                if (text.Contains(term))
                    continue;
                else
                    return false;
            }
            return true;
        }

        public static bool PhraseContainMatch(string text, string query)
        {
            if (text.ToLower().Contains(query)) return true;
            else return false;
        }

        public static string[] AdvancedTokens(string text)
        {
            return text.Trim().ToLower().Split(new string[] { " ", ",", ";", "'", "-", "_", "(", ")", ".", "/", "<", ">", "?", "[", "]", "&", "*", "`", "\\", "=", ":", "+", "\"", "{", "}", "|" }, 
                StringSplitOptions.RemoveEmptyEntries).Where(term => !term.Trim().Equals(string.Empty) && TextUtil.IsEnglishNumber(term)).ToArray();
            
        }
    }

    /// <summary>
    /// Helper classes for dealing with whitespace tokenization.
    /// </summary>
    public class WhitespaceTokenize 
    {
        /// <summary>
        /// Concatenate the given strings, putting something between each. (Should be moved!)
        /// </summary>
        /// <param name="rgs"></param>
        /// <param name="sBetween"></param>
        /// <returns></returns>
        public static string Join(IEnumerable<string> rgs, string sBetween)
        {
            StringBuilder sb = new StringBuilder();
            bool fFirst = true;
            foreach (string s in rgs)
            {
                if (fFirst)
                    fFirst = false;
                else
                    sb.Append(sBetween);
                sb.Append(s);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Find the number of whitespace delimited tokens in the string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static int TokenCount(string s)
        {
            int iCount = 0;
            int i = 0;
            while (true)
            {
                while (i < s.Length && char.IsWhiteSpace(s[i]))
                    ++i;
                if (i >= s.Length)
                    break;
                ++iCount;
                ++i;
                while (i < s.Length && !char.IsWhiteSpace(s[i]))
                    ++i;
            }
            return iCount;
        }
        /// <summary>
        /// Split the given string into an array of tokens, using whitespace to delimit each token.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string[] TokenizeToArray(string s)
        {
            string[] rgs = new string[TokenCount(s)];
            int iString = 0;
            int iPos = 0;
            int iStartPos = -1;
            while (true)
            {
                while (iPos < s.Length && char.IsWhiteSpace(s[iPos]))
                    ++iPos;
                if (iPos >= s.Length)
                    break;
                iStartPos = iPos++;
                while (iPos < s.Length && !char.IsWhiteSpace(s[iPos]))
                    ++iPos;
                rgs[iString++] = s.Substring(iStartPos, iPos - iStartPos);
            }
            return rgs;
        }
        
        /// <summary>
        /// Split the given string into an enumeration of tokens, whitespace delimited.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static IEnumerable<string> Tokens(string s)
        {
            State state = State.Pre;
            int iStart = -1;
            for (int i = 0; i < s.Length; ++i)
            {
                if (char.IsWhiteSpace(s[i]))
                {
                    if (state == State.InAlpha)
                    {
                        string sRet = s.Substring(iStart, i - iStart);
                        yield return sRet;
                    }
                    state = State.InSpace;
                }
                else
                {
                    if (state != State.InAlpha)
                    {
                        iStart = i;
                        state = State.InAlpha;
                    }
                }
            }
            if (state == State.InAlpha)
            {
                string sRet = s.Substring(iStart);
                yield return sRet;
            }
        }

        enum State
        {
            Pre,
            InAlpha,
            InSpace
        }
    }
}
