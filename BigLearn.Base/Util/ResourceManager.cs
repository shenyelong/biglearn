using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace BigLearn
{
		/// <summary>
        /// Sample Graph. 
        /// </summary>
        public class ResourceManager 
        {
            public CudaPieceFloat One { get; set; }
            public int MaxOneSize = 0;
            public void RegisteOneVectorFloat(int maxSize)
            {
                if(maxSize >= MaxOneSize) MaxOneSize = maxSize;
            }

            public CudaPieceInt Inc { get; set; }
            public int MaxIncSize = 0;
            public void RegisteIncVectorInt(int maxSize)
            {
                if(maxSize >= MaxIncSize) MaxIncSize = maxSize;
            }

            public Dictionary<string, CudaPieceInt> Mem = new Dictionary<string, CudaPieceInt>();
            public Dictionary<string, int> MemDictSize = new Dictionary<string, int>();
            public void RegisteIntVector(int maxSize, string name)
            {
                if(!MemDictSize.ContainsKey(name))
                {
                    MemDictSize.Add(name, maxSize);
                }
                else if(MemDictSize[name] < maxSize)
                {
                    MemDictSize[name] = maxSize;
                }
            }

            public Dictionary<string, CudaPieceFloat> MemF = new Dictionary<string, CudaPieceFloat>();
            public Dictionary<string, int> MemFDictSize = new Dictionary<string, int>();
            public void RegisteFloatVector(int maxSize, string name)
            {
                if(!MemFDictSize.ContainsKey(name))
                {
                    MemFDictSize.Add(name, maxSize);
                }
                else if(MemFDictSize[name] < maxSize)
                {
                    MemFDictSize[name] = maxSize;
                }
            }

            public List<IntPtr> ReduceStates = new List<IntPtr>();
            public void RegisteReduceState(IntPtr reduceState)
            {
                ReduceStates.Add(reduceState);
            }

            public RunnerBehavior Behavior { get; set; }

            public ResourceManager(RunnerBehavior behavior)
            {
                Behavior = behavior;
            }

            public bool IsInited = false;
            public void InitResource()
            {
                if(IsInited) return;
                if(MaxOneSize > 0)
                {
                    One = new CudaPieceFloat(MaxOneSize, Behavior.Device);
                    One.Init(1);
                }

                if(MaxIncSize > 0)
                {
                    Inc = new CudaPieceInt(MaxIncSize, Behavior.Device);
                    for(int i = 0; i < MaxIncSize; i++)
                    {
                        Inc.MemPtr[i] = i;
                    }
                    Inc.SyncFromCPU();
                }

                foreach(KeyValuePair<string, int> pair in MemDictSize)
                {
                    Mem.Add(pair.Key, new CudaPieceInt(pair.Value, Behavior.Device));
                }

                foreach(KeyValuePair<string, int> pair in MemFDictSize)
                {
                    MemF.Add(pair.Key, new CudaPieceFloat(pair.Value, Behavior.Device));
                }

                int maxIndicsSize = 0;
                int maxWorkSpaceSize = 0;
                for(int i = 0; i < ReduceStates.Count; i++)
                {
                    int indicsSize = Behavior.Computelib.GetReduceIndiceSize(ReduceStates[i]);
                    int workspaceSize = Behavior.Computelib.GetReduceWorkspaceSize(ReduceStates[i]);

                    if(indicsSize > maxIndicsSize) maxIndicsSize = indicsSize;
                    if(workspaceSize > maxWorkSpaceSize) maxWorkSpaceSize = workspaceSize;
                }

                IntPtr indicePtr = Behavior.Computelib.CudaAllocMem(maxIndicsSize);
                IntPtr workspacePtr = Behavior.Computelib.CudaAllocMem(maxWorkSpaceSize);

                for(int i = 0; i < ReduceStates.Count; i++)
                {
                    Behavior.Computelib.AssignReduceState(ReduceStates[i], indicePtr, maxIndicsSize, workspacePtr, maxWorkSpaceSize);
                }
                
                IsInited = true;
            }
        }
}