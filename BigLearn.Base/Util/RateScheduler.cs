using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Diagnostics;

namespace BigLearn
{
    // learning rate or epsilon greedy schedule.
        public class RateScheduler
        {
            List<Tuple<int, float>> mschedule { get; set; }
            IntArgument mcounter = null;

            /// <summary>
            /// match candidate, and match probability. randomizer should be in through outside.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public RateScheduler(List<Tuple<int, float>> schedule)  
            {
                mschedule = schedule;
                mcounter = new IntArgument("self_counter", 0);
            }

            public RateScheduler(IntArgument counter, List<Tuple<int, float>> schedule)  
            {
                mschedule = schedule;
                mcounter = counter;
            }

            public RateScheduler(IntArgument counter, string schedule)
            {
                mschedule = schedule.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                mcounter = counter;
            }

            public void Init()
            {
                mcounter.Value = 0;
            }
            
            public void Incr()
            {
                mcounter.Value += 1;
            }

            public float CurrentEps { get { return threshold(mcounter.Value); } }
            
            float threshold(int c)
            {
                int pI = 0;
                float pV = 1;
                for (int i = 0; i < mschedule.Count; i++)
                {
                    if(c == mschedule[i].Item1) { return mschedule[i].Item2; }
                    else if(c < mschedule[i].Item1)
                    {
                        float lambda =  (c - pI) * 1.0f / (mschedule[i].Item1 - pI);
                        return lambda * mschedule[i].Item2 + (1 - lambda) * pV;
                    }
                    pI = mschedule[i].Item1;
                    pV = mschedule[i].Item2;
                }
                return mschedule.Last().Item2;
            }

            
            public bool AmIin(float rand)
            {
                float t = threshold(mcounter.Value);
                //Interlocked.Increment(mcount);
                if(rand <= t) return true;
                return false;
            }
        }


}
