﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BigLearn
{
    public class Util
    {
        /// <summary>
        /// GPU computation only support single precise, So we can not use float.Epsilon;
        /// </summary>
        public static float GPUEpsilon = 1.0e-6f;
        public static float LargeEpsilon = 1.0e-4f;
        public static float SmallEpsilon = 1.0e-8f;
        public static float XEpsilon = 1.0e-12f;

        /// <summary>
        /// Global Random Variable.
        /// </summary>
        public static Random URandom = new Random(ParameterSetting.Random.Next());

        static double _b = 1.0 / Math.Sqrt(2 * Math.PI);

        public static int RandomNum = 1024 * 4096;

        static CudaPieceFloat RandomGPU = null;
        public static IntPtr CudaRandom
        {
            get
            {
                if (RandomGPU == null)
                {
                    RandomGPU = new CudaPieceFloat(RandomNum, false, true);
                    Cudalib.RandomNumber(RandomGPU.CudaPtr, RandomNum);
                }
                return RandomGPU.CudaPtr;
            }
        }

        static float[] RandomCPU = null;
        public static float[] CPURandom
        {
            get
            {
                if (RandomCPU == null)
                {
                    RandomCPU = new float[RandomNum];
                    for (int i = 0; i < RandomNum; i++)
                    {
                        RandomCPU[i] = (float)URandom.NextDouble();
                    }
                }
                return RandomCPU;
            }
        }
        
        


        public static CudaPieceInt VocabSampleTable(Dictionary<int, int> vocab, int SampleSize, float pow)
        {
            CudaPieceInt table = new CudaPieceInt(SampleSize, true, true);
            double sum = 0;
            double por = 0;
            double cur_sum = 0;

            List<int> keys = vocab.Keys.ToList();
            foreach (KeyValuePair<int, int> item in vocab) sum += Math.Pow(item.Value, pow);
            int vid = 0;
            for (int k = 0; k < SampleSize; k++)
            {
                if ((double)(k + 1) / SampleSize > por)
                {
                    cur_sum += Math.Pow(vocab[keys[vid]], pow);
                    por = cur_sum / sum;
                    vid++;
                }
                table.MemPtr[k] = keys[vid - 1];
            }
            //table.CopyIntoCuda();
            return table;
        }

        /// <summary>
        /// Item1 : vocab2class;
        /// Item2 : classIndex;
        /// Item3 : wordIndex;
        /// Item4 : wordClassIndex;
        /// Int : maxWordSeg in each Cluster.
        /// </summary>
        /// <param name="clusters"></param>
        /// <returns></returns>
        public static Tuple<int[], int[], int[], int[], int> ClusterIndex(List<List<int>> clusters, int vocabSize)
        {
            int[] classIndex = new int[clusters.Count];
            int[] wordIndex = new int[clusters.Select(i => i.Count).Sum()];
            int[] vocab2Class = new int[vocabSize];
            int[] wordSegIndex = new int[vocabSize];

            for (int i = 0; i < vocabSize; i++)
            {
                vocab2Class[i] = -1;
                wordSegIndex[i] = -1;
            }

            int maxWordSeg = clusters.Select(i => i.Count).Max();

            int c = 0;
            int s = 0;
            foreach (List<int> list in clusters)
            {
                classIndex[c] = c == 0 ? list.Count : classIndex[c - 1] + list.Count;
                int innerId = 0;
                foreach (int i in list)
                {
                    wordIndex[s] = i;
                    vocab2Class[i] = c;
                    wordSegIndex[i] = innerId;
                    s += 1;
                    innerId += 1;
                }
                c += 1;
            }
            return new Tuple<int[], int[], int[], int[], int>(vocab2Class, classIndex, wordIndex, wordSegIndex, maxWordSeg);
        }

        public static double Gaussian(double x, double m, double v)
        {
            return _b / Math.Sqrt(v) * Math.Exp(-Math.Pow(x - m, 2) / (2 * v));
        }

        public static double Gaussian_N(double z)
        {
            if (z > 6.0) { return 1.0; }
            if (z < -6.0) { return 0.0; }

            double b1 = 0.31938153;
            double b2 = -0.356563782;
            double b3 = 1.781477937;
            double b4 = -1.821255978;
            double b5 = 1.330274429;
            double p = 0.2316419;
            double c2 = 0.3989423;

            double a = Math.Abs(z);
            double t = 1.0 / (1.0 + a * p);
            double b = c2 * Math.Exp((-z) * (z / 2.0));
            double n = (((((b5 * t + b4) * t + b3) * t + b2) * t) + b1) * t;
            n = 1.0 - b * n;
            if (z < 0.0)
                n = 1.0 - n;
            return n;
        }

        public static double Func_V(double x)
        {
            double g = Gaussian(x, 0, 1);
            double phi = Gaussian_N(x);
            if (g > 0 && phi <= 0)
            {
                return 1;
            }
            return g * 1.0 / phi;
        }

        public static float LogLogistial(float f)
        {
            return (float)Math.Log(Logistic(f) + Util.SmallEpsilon);
            //return -(float)Math.Log(1 + Math.Exp(-f));
        }

        public static float LogNLogistial(float f)
        {
            return (float)Math.Log(1 - Logistic(f) + Util.SmallEpsilon);
            //return -f - (float)Math.Log(1 + Math.Exp(-f));
        }



        public static float[] InitRandomVec(int dim, float scale)
        {
            float[] x = new float[dim];
            for (int i = 0; i < dim; i++)
            {
                x[i] = (float)(URandom.NextDouble()) * scale;
            }
            return x;
        }


        public static int LogRandomSampling(float[] alpha, float sum)
        {
            float t = (float)Util.URandom.NextDouble();

            float asum = 0;
            for (int i = 0; i < alpha.Length; i++)
            {
                asum += (float)Math.Exp(alpha[i] - sum);
                if (asum >= t)
                {
                    return i;
                }
            }
            return alpha.Length - 1;
        }

        public static int RandomSamplingThreadSafe(float[] alpha, float sum)
        {
            float t = 0;
            lock (Util.URandom)
            {
                t = (float)Util.URandom.NextDouble();
            }
            float asum = 0;
            for (int i = 0; i < alpha.Length; i++)
            {
                asum += alpha[i];
                if (asum >= t * sum)
                {
                    return i;
                }
            }
            return alpha.Length - 1;
        }

        public static int LogRandomSamplingThreadSafe(float[] alpha, float sum)
        {
            float t = 0;
            lock (Util.URandom)
            {
                t = (float)Util.URandom.NextDouble();
            }
            float asum = 0;
            for (int i = 0; i < alpha.Length; i++)
            {
                asum += (float)Math.Exp(alpha[i] - sum);
                if (asum >= t)
                {
                    return i;
                }
            }
            return alpha.Length - 1;
        }


        /// <summary>
        /// push from to into the Dictionary.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void Push(Dictionary<int, HashSet<string>> data, int from, string to)
        {
            if (!data.ContainsKey(from))
            {
                data.Add(from, new HashSet<string>());
            }
            data[from].Add(to);
        }

        public static int RandomSampling(float[] alpha, float sum)
        {
            float t = (float)Util.URandom.NextDouble();

            float asum = 0;
            for (int i = 0; i < alpha.Length; i++)
            {
                asum += alpha[i];
                if (asum >= t * sum)
                {
                    return i;
                }
            }
            return alpha.Length - 1;
        }

        /// <summary>
        /// Random Split Hashset into two parts.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="split"></param>
        /// <returns></returns>
        public static Tuple<HashSet<T>, HashSet<T>> SplitRate<T>(HashSet<T> data, float testsplit)
        {
            HashSet<T> p1 = new HashSet<T>();
            HashSet<T> p2 = new HashSet<T>();
            foreach (T d in data)
            {
                if (URandom.NextDouble() < testsplit)
                {
                    p2.Add(d);
                }
                else
                {
                    p1.Add(d);
                }
            }
            return new Tuple<HashSet<T>, HashSet<T>>(p1, p2);
        }

        public static Dictionary<T, int> Enumable2Dict<T>(IEnumerable<T> list)
        {
            Dictionary<T, int> dict = new Dictionary<T, int>();
            foreach (T t in list)
            {
                if (dict.ContainsKey(t))
                    dict[t] += 1;
                else
                    dict.Add(t, 1);
            }
            return dict;
        }

        public static List<Dictionary<T, float>> Enumable2SeqDict<T>(IEnumerable<T> list)
        {
            List<Dictionary<T, float>> dict = new List<Dictionary<T, float>>();
            foreach (T t in list)
            {
                Dictionary<T, float> tmp = new Dictionary<T, float>();
                tmp.Add(t, 1);
                dict.Add(tmp);
            }
            return dict;
        }


        public static Tuple<HashSet<T>, HashSet<T>> SplitNumber<T>(HashSet<T> data, int testnumber)
        {
            HashSet<T> p1 = new HashSet<T>(data);
            HashSet<T> p2 = new HashSet<T>();

            for (int i = 0; i < testnumber; i++)
            {
                if (p1.Count == 0)
                    break;
                int n = URandom.Next(p1.Count);
                T e = p1.ElementAt(n);
                p2.Add(e);
                p1.Remove(e);
            }
            return new Tuple<HashSet<T>, HashSet<T>>(p1, p2);
        }

        public static IEnumerable<int> SplitCount(int num, int total)
        {
            List<int> RandomBatchIdx = Enumerable.Range(0, total).ToList();
            for (int i = 0; i < num; i++)
            {
                int randomPos = Util.URandom.Next(i, total);
                int id = RandomBatchIdx[randomPos];
                RandomBatchIdx[randomPos] = RandomBatchIdx[i];
                RandomBatchIdx[i] = id;
            }
            return RandomBatchIdx.Take(num);
        }

        /// <summary>
        /// Memory in idx : (Smp1, negId1) (Smp1, negId2) (Smp1, negId3) ...  (Smp2, negId1) (Smp2, negId2) 
        /// </summary>
        /// <param name="batchSize"></param>
        /// <param name="nTrail"></param>
        /// <param name="idx"></param>
        public static void MatchSampling(int batchSize, int nTrail, int[] srcIdx, int[] tgtIdx, float[] matchLabel, Random rnd)
        {
            for (int k = 0; k < batchSize; k++)
            {
                srcIdx[k * (nTrail + 1)] = k;
                matchLabel[k * (nTrail + 1)] = 1;
                for (int i = 0; i < nTrail; i++)
                    srcIdx[k * (nTrail + 1) + i + 1] = k;
                tgtIdx[k * (nTrail + 1)] = k;
            }

            for (int k = 0; k < batchSize; ++k)
            {
                for (int i = 0; i < nTrail; ++i)
                {
                    int negIdx = 0;
                    if (batchSize > 1)
                    {
                        // uniformly randomly choose a negative sample from (0,...,k-1,k+1,...batchSize-1)
                        negIdx = rnd.Next(batchSize - 1);
                        if (negIdx >= k) ++negIdx; 
                    }
                    tgtIdx[k * (nTrail + 1) + i + 1] = negIdx;
                    matchLabel[k * (nTrail + 1) + i + 1] = 0;
                }
            }
        }

        /// <summary>
        /// Memory in idx : (Smp1, negId1) (Smp2, negId1) (Smp3, negId1) ... ...  (Smp1, negId2) (Smp2, negId2) ... ; 
        /// </summary>
        /// <param name="batchSize"></param>
        /// <param name="nTrail"></param>
        /// <param name="BATCHSIZE"></param>
        /// <param name="idx"></param>
        public static void NegativeSampling(int batchSize, int nTrail, int BATCHSIZE, int[] idx)
        {
            for (int k = 0; k < batchSize; ++k)
            {
                for (int i = 0; i < nTrail; ++i)
                {
                    int negIdx = ParameterSetting.Random.Next(batchSize);
                    idx[i * BATCHSIZE + k] = negIdx;
                }
            }
        }

        public static void NegativeSamplingTranspose(int batchsize, int nTrail, int[] idx, int[] inverseIdx, int[] inverseValue)
        {
            List<List<int>> list = new List<List<int>>();
            for (int k = 0; k < batchsize; k++)
            {
                list.Add(new List<int>());
            }
            for (int i = 0; i < nTrail; i++)
            {
                for (int k = 0; k < batchsize; k++)
                {
                    int bs = idx[k * nTrail + i];
                    list[bs].Add(k);
                }
            }

            int ptotal = 0;
            int pindex = 0;
            for (int k = 0; k < batchsize; k++)
            {
                for (int m = 0; m < list[k].Count; m++)
                {
                    inverseValue[pindex] = list[k][m];
                    pindex++;
                }
                inverseIdx[k] = ptotal + list[k].Count;
                ptotal = inverseIdx[k];
            }
        }

        public static void NegativeSamplingTranspose(int batchsize, int nTrail, int BATCHSIZE, int[] idx, int[] inverseIdx, int[] inverseValue)
        {
            List<List<int>> list = new List<List<int>>();
            for (int k = 0; k < batchsize; k++)
            {
                list.Add(new List<int>());
            }
            for (int i = 0; i < nTrail; i++)
            {
                for (int k = 0; k < batchsize; k++)
                {
                    list[k].Clear();
                }

                for (int k = 0; k < batchsize; k++)
                {
                    int bs = idx[i * BATCHSIZE + k]; // ].MemPtr[k];
                    list[bs].Add(k);
                }

                int ptotal = 0;
                int pindex = 0;
                for (int k = 0; k < batchsize; k++)
                {
                    for (int m = 0; m < list[k].Count; m++)
                    {
                        inverseValue[i * BATCHSIZE + pindex] = list[k][m]; // i].MemPtr[pindex] = mlist[k][m];
                        pindex++;
                    }
                    inverseIdx[i * BATCHSIZE + k] = ptotal + list[k].Count;
                    ptotal = inverseIdx[i * BATCHSIZE + k];
                }
            }
        }


        public static HashSet<int> RandomNegative(int negNum, int tryTimes, int total, HashSet<int> input)
        {
            HashSet<int> negSet = new HashSet<int>();
            for (int i = 0; i < tryTimes; i++)
            {
                int tmp = Util.URandom.Next(total);
                if (input.Contains(tmp))
                    continue;
                negSet.Add(tmp);
                if (negSet.Count >= negNum)
                    break;
            }
            return negSet;
        }

        public static string Hashset2String<T>(HashSet<T> data)
        {
            StringBuilder sb = new StringBuilder();
            int id = 0;
            foreach (T d in data)
            {
                if (id == 0)
                    sb.Append(d.ToString());
                else
                    sb.Append("," + d.ToString());
                id += 1;
            }
            return sb.ToString();
        }
        
        unsafe public static float CosineSimilarity(float[] a, int aIdx, float[] b, int bIdx, int size)
        {

            fixed (float* aP = & a[aIdx])
            fixed(float * bP = & b[bIdx])
                {
                    //IntPtr aPtr = new IntPtr((void*)aP);
                    //IntPtr bPtr = new IntPtr((void*)bP);
                    
                    float L2ASquare = SSElib.SSE_SumSq(aP, size) + Util.GPUEpsilon;
                    float L2BSquare = SSElib.SSE_SumSq(bP, size) + Util.GPUEpsilon;
                    float dp = SSElib.SSE_DotProduct(aP, bP, size);
                    return dp / ( L2ASquare * L2BSquare); 
                }
            
            /*
            FastVector aVec = FastVector.Create(a, aIdx, size, false);
            FastVector bVec = FastVector.Create(b, bIdx, size, false);

            float L2ASquare = aVec.L2Norm + Util.GPUEpsilon;
            float L2BSquare = bVec.L2Norm + Util.GPUEpsilon;
            float dp = aVec.DotProduct(bVec);
            return dp / (L2ASquare * L2BSquare);
            */
        }

        unsafe public static void DerivCosineSimilarity(float deriv, float similarity, 
                                                float[] a, float[] aDeriv, float a_alpha, float a_beta, int aIdx, 
                                                float[] b, float[] bDeriv, float b_alpha, float b_beta, int bIdx, int size)
        {
            fixed (float* aP = & a[aIdx])
                fixed (float* bP = & b[bIdx])
                {
                    //IntPtr aPtr = new IntPtr((void*)aP);
                    //IntPtr bPtr = new IntPtr((void*)bP);
                    
                    float L2ASquare = SSElib.SSE_SumSq(aP, size) + Util.GPUEpsilon;
                    float L2BSquare = SSElib.SSE_SumSq(bP, size) + Util.GPUEpsilon;

                    float bc = L2ASquare * L2BSquare;
                    float bb = L2ASquare * L2ASquare;
                    float cc = L2BSquare * L2BSquare;

                    float m1 = a_beta * deriv;
                    float m2 = b_beta * deriv;

            float m3 = similarity / bb;
            float m4 = similarity / cc;

            for (int i=0;i<size;i++)
            {
                aDeriv[i + aIdx] = a_alpha * aDeriv[i + aIdx] + m1 * (b[bIdx + i] / bc - a[aIdx + i] * m3); 
                bDeriv[i + aIdx] = b_alpha * bDeriv[i + bIdx] + m2 * (a[aIdx + i] / bc - b[bIdx + i] * m4);
            }

             
                }
        }

        public static void RandomMatrix(float[,] matrix, int row, int col, float scale)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    matrix[i, j] = (float)((2 * URandom.NextDouble() - 1) * scale);
                }
            }
        }

        public static void VectorTanh(float[] hidden)
        {
            for (int i = 0; i < hidden.Length; i++)
            {
                hidden[i] = (float)Math.Tanh(hidden[i]);
            }
        }

        /// <summary>
        /// (Math.Tanh(value/2) + 1) / 2.0f;
        /// </summary>
        /// <param name="hidden"></param>
        public static void VectorSigmod(float[] hidden)
        {
            for (int i = 0; i < hidden.Length; i++)
            {
                hidden[i] = (float)Math.Tanh(hidden[i] / 2.0f);
                hidden[i] = (hidden[i] + 1.0f) / 2.0f;
            }
        }
        public static void VectorRectified(float[] hidden)
        {
            for (int i = 0; i < hidden.Length; i++)
            {
                if (hidden[i] < 0)
                    hidden[i] = 0;
            }
        }

        public static HashSet<int> SampleNeg(HashSet<int> input, int prodNum, int num, int tryNum)
        {
            HashSet<int> neg = new HashSet<int>();

            for (int i = 0; i < tryNum; i++)
            {
                int tmp = URandom.Next(prodNum);

                if (input.Contains(tmp))
                    continue;

                neg.Add(tmp);
                if (neg.Count >= num)
                    break;
            }
            return neg;
        }

        public static float LogAdd(float a, float b)
        {
            if (a > b)
                return a + (float)Math.Log(1 + Math.Exp(b - a));
            else
                return b + (float)Math.Log(1 + Math.Exp(a - b));
        }
        public static double LogAdd(double a, double b)
        {
            if (a > b) return a + Math.Log(1 + Math.Exp(b - a));
            else return b + Math.Log(1 + Math.Exp(a - b));
        }
        public static double LogSum(IEnumerable<double> a)
        {
            double r = double.MinValue;
            foreach(double o in a)
            {
                r = LogAdd(r, o);
            }
            return r;
        }

        public static float LogSum(IEnumerable<float> a, int len)
        {
            float r = float.MinValue;
            int idx = 0;
            foreach (float o in a)
            {
                r = LogAdd(r, o);
                idx += 1;
                if (idx >= len) break;
            }
            return r;
        }

        public static Tuple<int, int, float> ScanMaxValueSpan(float[] start, float[] end, int from, int to, int spanLimited)
        {
            float maxV = float.MinValue;
            int maxSIndex = -1;
            int maxEIndex = -1;
            for (int i = from; i < to; i++)
            {
                for (int t = i; t < Math.Min(to, i + spanLimited); t++)
                {
                    if (start[i] + end[t] > maxV)
                    {
                        maxV = start[i] + end[t];
                        maxSIndex = i;
                        maxEIndex = t;
                    }
                }
            }
            return new Tuple<int, int, float>(maxSIndex, maxEIndex, maxV);
        }

        public unsafe static Tuple<int, int, float> ScanMaxValueSpan(float * start, float * end, int from, int to, int spanLimited)
        {
            float maxV = float.MinValue;
            int maxSIndex = -1;
            int maxEIndex = -1;
            for (int i = from; i < to; i++)
            {
                for (int t = i; t < Math.Min(to, i + spanLimited); t++)
                {
                    if (start[i] + end[t] > maxV)
                    {
                        maxV = start[i] + end[t];
                        maxSIndex = i;
                        maxEIndex = t;
                    }
                }
            }
            return new Tuple<int, int, float>(maxSIndex, maxEIndex, maxV);
        }

        public static float[] Softmax(float[] x, double gamma)
        {
            float[] y = new float[x.Length];
            float sum = float.MinValue;
            for (int i = 0; i < x.Length; ++i)
            {
                y[i] = (float)gamma * x[i]; // (float)Math.Min(50, Math.Max(-50, gamma * x[i])); // for numerical stability                
                sum = LogAdd(y[i], sum);
            }
            for (int i = 0; i < y.Length; ++i)
            {
                y[i] = (float)Math.Exp(y[i] - sum);
            }
            return y;
        }

        public static void Softmax(float[] x, float[] y, double gamma)
        {
            Softmax(x, 0, y, 0, x.Length, gamma);
        }

        public static void Softmax(float[] x, int offsetX, float[] y, int offsetY, int len, double gamma)
        {
            float sum = float.MinValue;
            for (int i = 0; i < len; ++i)
            {
                y[offsetY + i] = (float)gamma * x[offsetX + i]; // (float)Math.Min(50, Math.Max(-50, gamma * x[i])); // for numerical stability                
                sum = LogAdd(y[offsetY + i], sum);
            }
            for (int i = 0; i < len; ++i)
            {
                y[offsetY + i] = (float)Math.Exp(y[offsetY + i] - sum);
            }
        }

        public static void Softmax(float[] x, int[] indices, float[] y, double gamma)
        {
            float sum = float.MinValue;
            for (int i = 0; i < indices.Length; ++i)
            {
                int idx = indices[i];
                y[idx] = (float)gamma * x[idx]; // (float)Math.Min(50, Math.Max(-50, gamma * x[i])); // for numerical stability                
                sum = LogAdd(y[idx], sum);
            }
            for (int i = 0; i < indices.Length; ++i)
            {
                int idx = indices[i];
                y[idx] = (float)Math.Exp(y[idx] - sum);
            }
        }

        public static float LogSoftmax(float[] x, int xStart, int length, int labelIndex, float gamma)
        {
            float sum = gamma * x[xStart];
            float target = gamma * x[labelIndex + xStart];
            for (int i = 1; i < length; ++i)
            {
                float v = (float)gamma * x[i + xStart]; // (float)Math.Min(50, Math.Max(-50, gamma * x[i])); // for numerical stability                
                sum = LogAdd(v, sum);
            }
            return target - sum;
        }

        public static float[] LogSoftmax(float[] x, double gamma)
        {
            float[] y = new float[x.Length];
            float sum = float.MinValue;

            for (int i = 0; i < x.Length; ++i)
            {
                y[i] = (float)gamma * x[i]; // (float)Math.Min(50, Math.Max(-50, gamma * x[i])); // for numerical stability                
                sum = LogAdd(y[i], sum);
            }
            for (int i = 0; i < y.Length; ++i)
                y[i] = y[i] - sum;
            return y;
        }

        public static float Logistic(float value)
        {
            return (float)(Math.Tanh(value / 2) + 1) / 2.0f;
        }

        public static float[] Logistic(float[] value)
        {
            return Enumerable.Range(0, value.Length).Select(i => Logistic(value[i])).ToArray();
        }

        public static int MinimumValue(float[] list)
        {
            float minValue = float.MaxValue;
            int idx = -1;
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i] < minValue)
                {
                    minValue = list[i];
                    idx = i;
                }
            }
            return idx;
        }

        public static int MaximumValue(float[] list)
        {
            float maxValue = float.MinValue;
            int idx = -1;
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i] > maxValue)
                {
                    maxValue = list[i];
                    idx = i;
                }
            }
            return idx;
        }

        public unsafe static int MaximumValue(float * list, int size)
        {
            float maxValue = float.MinValue;
            int idx = -1;
            for (int i = 0; i < size; i++)
            {
                if (list[i] > maxValue)
                {
                    maxValue = list[i];
                    idx = i;
                }
            }
            return idx;
        }

        public static int MaximumValue(float[] list, int s, int e)
        {
            float maxValue = float.MinValue;
            int idx = -1;
            for (int i = s; i < e; i++)
            {
                if (list[i] > maxValue)
                {
                    maxValue = list[i];
                    idx = i;
                }
            }
            return idx;
        }

        public unsafe static int MaximumValue(float * list, int s, int e)
        {
            float maxValue = float.MinValue;
            int idx = -1;
            for (int i = s; i < e; i++)
            {
                if (list[i] > maxValue)
                {
                    maxValue = list[i];
                    idx = i;
                }
            }
            return idx;
        }


        public static int Sample(float[] prob, Random random)
        {
            return Sample(prob, 0, prob.Length, random);
        }


        public static int Sample(float[] prob, int sidx, int offset, Random random)
        {
            float v = (float)random.NextDouble();
            float pSum = 0;
            for (int i = 0; i < offset; i++)
            {
                pSum += prob[sidx + i];
                if (pSum >= v) return i;
            }
            return offset - 1;
        }

        public unsafe static int Sample(float * prob, int sidx, int offset, Random random)
        {
            float v = (float)random.NextDouble();
            float pSum = 0;
            for (int i = 0; i < offset; i++)
            {
                pSum += prob[sidx + i];
                if (pSum >= v) return i;
            }
            return offset - 1;
        }

        public unsafe static int Sample(float * prob, int sidx, int offset, float * maskScale, int midx, Random random)
        {
            float v = (float)random.NextDouble();
            float pSum = 0;
            int defaultIdx = 0;
            for (int i = 0; i < offset; i++)
            {
                if(maskScale[midx + i] > 0)
                {
                    defaultIdx = i;
                    pSum += prob[sidx + i];
                    if (pSum >= v) return i;
                }
            }
            return defaultIdx;
        }

       

        public static float LogSoftMaxLossDeriv(Dictionary<int, float> predict, HashSet<int> target, Dictionary<int, float> deriv, float gamma)
        {
            List<int> posIds = new List<int>();
            List<int> negIds = new List<int>();

            deriv.Clear();

            foreach (KeyValuePair<int, float> item in predict)
            {
                if (target.Contains(item.Key))
                {
                    posIds.Add(item.Key);
                    deriv.Add(item.Key, 0); //.1f);
                }
                else
                {
                    negIds.Add(item.Key);
                    deriv.Add(item.Key, 0);
                }
                //deriv.Add(item.Key, 0);
            }
            //return 0;

            float[] scores = new float[negIds.Count + 1];
            for (int n = 0; n < negIds.Count; n++)
            {
                scores[n + 1] = predict[negIds[n]];
            }

            float loss = 0;
            for (int p = 0; p < posIds.Count; p++)
            {
                scores[0] = predict[posIds[p]];

                float[] softmaxProb = LogSoftmax(scores, gamma);
                loss += (float)(softmaxProb[0]);

                ///Get Derivative of Log-SoftMax Function.
                for (int x = 0; x < negIds.Count; ++x)
                    deriv[negIds[x]] += -gamma * (float)Math.Exp(softmaxProb[x + 1]) / posIds.Count;
                deriv[posIds[p]] += gamma * (1 - (float)Math.Exp(softmaxProb[0])) / posIds.Count;
            }
            return loss / (posIds.Count + float.Epsilon);
        }

        public static float LogSoftMaxLossDeriv(Dictionary<int, float> predict, int target, Dictionary<int, float> deriv, float gamma)
        {
            List<int> posIds = new List<int>();
            List<int> negIds = new List<int>();

            deriv.Clear();

            foreach (KeyValuePair<int, float> item in predict)
            {
                if (target == item.Key)
                {
                    posIds.Add(item.Key);
                    deriv.Add(item.Key, 0); //.1f);
                }
                else
                {
                    negIds.Add(item.Key);
                    deriv.Add(item.Key, 0);
                }
                //deriv.Add(item.Key, 0);
            }
            //return 0;

            float[] scores = new float[negIds.Count + 1];
            for (int n = 0; n < negIds.Count; n++)
            {
                scores[n + 1] = predict[negIds[n]];
            }

            float loss = 0;
            for (int p = 0; p < posIds.Count; p++)
            {
                scores[0] = predict[posIds[p]];

                float[] softmaxProb = LogSoftmax(scores, gamma);
                loss += (float)(softmaxProb[0]);

                ///Get Derivative of Log-SoftMax Function.
                for (int x = 0; x < negIds.Count; ++x)
                    deriv[negIds[x]] += -gamma * (float)Math.Exp(softmaxProb[x + 1]) / posIds.Count;
                deriv[posIds[p]] += gamma * (1 - (float)Math.Exp(softmaxProb[0])) / posIds.Count;
            }
            return loss / (posIds.Count + float.Epsilon);
        }


        public static void InverseMatchIdx(int[] matchIdx, int matchNum, int[] inverseSmpIdx, int[] inverseElementIdx, int inverseSmpNum)
        {
            List<int>[] indexMatchList = new List<int>[inverseSmpNum];
            foreach (int i in Enumerable.Range(0, inverseSmpNum)) { indexMatchList[i] = new List<int>(); }

            for (int i = 0; i < matchNum; i++)
                indexMatchList[matchIdx[i]].Add(i);

            int element = 0;
            foreach (int i in Enumerable.Range(0, inverseSmpNum))
            {
                inverseSmpIdx[i] = element + indexMatchList[i].Count;
                foreach (int m in indexMatchList[i])
                    inverseElementIdx[element++] = m;
            }
        }

        public static int InverseMatchIdx(int[] matchIdx, int matchNum, int[] inverseIdx, int[] inverseSmpIdx, int[] inverseElementIdx)
        {
            int maxIdx = matchIdx.Max() + 1;
            List<int>[] indexMatchList = new List<int>[maxIdx];
            //foreach (int i in Enumerable.Range(0, maxIdx)) { indexMatchList[i] = new List<int>(); }

            for (int i = 0; i < matchNum; i++)
            {
                if (indexMatchList[matchIdx[i]] == null) indexMatchList[matchIdx[i]] = new List<int>();
                indexMatchList[matchIdx[i]].Add(i);
            }

            int element = 0;
            int idx = 0;
            foreach (int i in Enumerable.Range(0, maxIdx))
            {
                if(indexMatchList[i] != null)
                {
                    inverseIdx[idx] = i;
                    inverseSmpIdx[idx] = element + indexMatchList[i].Count;
                    foreach (int m in indexMatchList[i])
                        inverseElementIdx[element++] = m;
                    idx += 1;
                }
            }
            return idx;
        }

        public static Dictionary<int, float> Str2Dict(string feaStr, char seperator)
        {
            return feaStr.Trim().Equals("") ? new Dictionary<int, float>() : feaStr.Split(seperator).Select(item => item.Split(':')).ToDictionary(kv => int.Parse(kv[0]), kv => float.Parse(kv[1]));
        }

        public static float Add(ref float location1, float value)
        {
            float newCurrentValue = 0;
            while (true)
            {
                float currentValue = newCurrentValue;
                float newValue = currentValue + value;
                newCurrentValue = Interlocked.CompareExchange(ref location1, newValue, currentValue);
                if (newCurrentValue == currentValue)
                    return newValue;
            }
        }

        public static void RandomShuffle<T>(T[] array, Random rnd)
        {
            int batIdx = 0;
            while (batIdx < array.Length)
            {
                int randomPos = rnd.Next(batIdx, array.Length);
                T temp = array[randomPos];
                array[randomPos] = array[batIdx];
                array[batIdx] = temp;
                batIdx++;
            }
        }

        public static void RandomShuffle<T>(T[] array, int seed = 0)
        {
            Random rnd = new Random(seed);
            RandomShuffle(array, rnd);
        }
    }

    public class CSVTable
    {
        public static List<object[]> LoadDataTable(string input, char sep)
        {
            List<object[]> data = new List<object[]>();
            using (StreamReader reader = new StreamReader(input))
            {
                string[] items = reader.ReadLine().Split(sep);
                data.Add(items);
            }
            return data;
        }
    }
}
