﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public abstract class IGradientAggregator
    {
        public abstract void Aggregate(params CudaPieceFloat[] grads);
    }

    public abstract class IGradientAggregatorFactory
    {
        public abstract IGradientAggregator CreateAggregator(int dimension);
    }
}
