﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MinMaxHeap<T>
    {
        public int Mode = -1;
        public List<KeyValuePair<T, float>> topK = new List<KeyValuePair<T, float>>();
        public int K = 0;

        public int Size { get { return topK.Count; } }

        /// <summary>
        /// 1 :  Min Heap for TopK Largest; -1 : Max Heap for TopK Minest;
        /// </summary>
        /// <param name="topk"></param>
        /// <param name="mode"></param>
        public MinMaxHeap(int topk, int mode)
        {
            K = topk;
            Mode = mode;
        }

        public MinMaxHeap(int mode, float[] value, T[] idx, int k)
        {
            Mode = mode;
            K = k;
            for (int i = 0; i < k; i++)
            {
                topK.Add(new KeyValuePair<T, float>(idx[i], value[i]));
            }
        }

        public void swap(int id1, int id2)
        {
            KeyValuePair<T, float> kv = topK[id1];
            topK[id1] = topK[id2];
            topK[id2] = kv;
        }

        public void down(int index)
        {
            if (index * 2 + 1 >= topK.Count)
                return;
            int min_index = index * 2 + 1;
            if (index * 2 + 2 < topK.Count && topK[index * 2 + 2].Value * Mode < topK[index * 2 + 1].Value * Mode)
                min_index = index * 2 + 2;
            if (topK[min_index].Value * Mode < topK[index].Value * Mode)
            {
                swap(index, min_index);
                down(min_index);
            }
        }

        public void up(int index)
        {
            if (index == 0)
                return;
            int pare_index = (index - 1) / 2;
            if (topK[index].Value * Mode < topK[pare_index].Value * Mode)
            {
                swap(index, pare_index);
                up(pare_index);
            }
        }
        

        public void clear()
        {
            topK.Clear();
        }

        public void push_pair(T key, float value)
        {
            if (topK.Count == K)
            {
                if (topK[0].Value * Mode < value * Mode)
                {
                    topK[0] = new KeyValuePair<T, float>(key, value);
                    down(0);
                    return;
                }
            }
            else
            {
                topK.Add(new KeyValuePair<T, float>(key, value));
                up(topK.Count - 1);
                return;
            }
        }

        public bool IsEmpty { get { return topK.Count == 0; } }
        public KeyValuePair<T,float> PopTop()
        {
            KeyValuePair<T, float> top = topK[0];
            topK[0] = topK[topK.Count - 1];
            topK.RemoveAt(topK.Count - 1);
            down(0);
            return top;
        }
    }
}
