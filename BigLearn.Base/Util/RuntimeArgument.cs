﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class Shape
    {
        public IntArgument[] Dims;

        public int DimNum { get { return Dims.Length; } }
        public int[] DimList { get { return Dims.Select(i => i.Default).ToArray(); } }
        public int[] RDimList { get { return Dims.Select(i => i.Default).Reverse().ToArray(); } }
        

        public int[] DimIndexes 
        {
            get
            {
                int[] dimIndexes = new int[DimNum];
                int s = 1;
                int m = 0;
                foreach(IntArgument i in Dims)
                {
                    dimIndexes[m] = s;
                    s = s * i.Default;
                    m += 1;
                }
                return dimIndexes;
            } 
        }

        public int[] DimOffsets
        {
            get
            {
                int[] dimOffsets = new int[DimNum];
                int s = 1;
                int m = 0;
                foreach(IntArgument i in Dims)
                {
                    s = s * i.Default;
                    dimOffsets[m] = s;
                    m += 1;
                }
                return dimOffsets;
            } 
        }

        public int DimSum
        {
            get
            {
                int s = 0;
                foreach(IntArgument i in Dims)
                {
                    s = s + i.Default;
                }
                return s;
            }
        }

        public int Size
        {
            get
            {
                int s = 1;
                foreach(IntArgument i in Dims)
                {
                    s = s * i.Value;
                }
                return s;
            }
        }

        public int DefaultSize
        {
            get
            {
                int s = 1;
                foreach(IntArgument i in Dims)
                {
                    s = s * i.Default;
                }
                return s;
            }
        }

        public Shape(params IntArgument[] dims)
        {
            Dims = dims;
        }

    }
    public class IntArgument
    {
        public string Name;
        int _Value;
        public int Value { get { return _Value; } set { _Value = value; if( _Value > Default) Default = _Value; } }
        public int Default;
        
        public IntArgument(string name, int defaultValue = 0)
        {
            Name = name;
            Value = defaultValue;
            Default = defaultValue;
        }
    }

    public class FloatArgument
    {
        public string Name;
        float _Value;
        public float Value { get { return _Value; } set { _Value = value; if( _Value > Default) Default = _Value; } }
        public float Default;

        public FloatArgument(string name, float defaultValue = 0)
        {
            Name = name;
            Value = defaultValue;
            Default = defaultValue;
        }
    }

    public static class ArgumentHelper
    {
        public static string ToStr(this IntArgument[] shape)
        {
            return string.Join(",", shape.Select(i => i.Default));
        }
    }

    public class HiddenDataBatchSizeRunner : StructRunner
    {
        HiddenBatchData Data;
        IntArgument Arg;
        public HiddenDataBatchSizeRunner(HiddenBatchData data, IntArgument arg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Data = data;
            Arg = arg;
        }
        public override void Forward() { Arg.Value = Data.BatchSize; }
    }

    public class ZeroMatrixRunner : StructRunner
    {
        CudaPieceFloat zeroMat =  null;
        IntArgument SizeArg { get; set; }
        
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public ZeroMatrixRunner(int maxSize, int dim, IntArgument sizeArg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            zeroMat = new CudaPieceFloat(maxSize * dim, Behavior.Device); 
            zeroMat.Zero();
            SizeArg = sizeArg;
            Output = new HiddenBatchData(maxSize, dim, zeroMat, null, Behavior.Device);
        }
        public override void Forward()
        {
            Output.BatchSize = SizeArg.Value;
        }
    }


}
