﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class FileUtil
    {
        public static int BUFFER_SIZE = 1 * 1024 * 1024;
        public unsafe static float[][] LoadEbdFile(string filename, int topK = -1)
        {
            const int megasize = 2040 * 1024 * 1024;
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            Console.WriteLine("Read from " + filename);            
            
            byte[] buffer = new byte[megasize];
            int bytesRead = fs.Read(buffer, 0, megasize);
            int linecount = BitConverter.ToInt32(new byte[] { buffer[0], buffer[1], buffer[2], buffer[3] }, 0);
            int dim = BitConverter.ToInt32(new byte[] { buffer[4], buffer[5], buffer[6], buffer[7] }, 0);
            int p = 8;

            if (topK > 0)
                linecount = Math.Min(topK, linecount);

            float[][] points = new float[linecount][];

            for (int lineid = 0; lineid < linecount; ++lineid)
            {
                if (lineid % 500000 == 0)
                {
                    Console.WriteLine("{0} lines... {1}", lineid, DateTime.Now);
                }
                points[lineid] = new float[dim];
                for (int j = 0; j < dim; ++j)
                {
                    fixed (byte* pThat = buffer)
                    {
                        points[lineid][j] = *(float*)&pThat[p];
                        p += 4;
                        if (p >= bytesRead)
                        {
                            bytesRead = fs.Read(buffer, 0, megasize);
                            p = 0;
                        }
                    }
                }
            }
            
            Console.WriteLine();
            
            fs.Close();
            return points;
        }

        public static FileStream CreateReadFS(string filename)
        {
            return new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, BUFFER_SIZE);
        }

        public static FileStream CreateWriteFS(string filename)
        {
            return new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None, BUFFER_SIZE);
        }

        public static BinaryReader CreateBinaryRead(string filename)
        {
            return new BinaryReader(CreateReadFS(filename));
        }

        public static StreamReader CreateStreamRead(string filename)
        {
            return new StreamReader(CreateReadFS(filename));
        }

        public static BinaryWriter CreateBinaryWrite(string filename)
        {
            if (string.IsNullOrEmpty(filename))
                return null;
            return new BinaryWriter(CreateWriteFS(filename));
        }

        public static StreamWriter CreateStreamWrite(string filename)
        {
            return new StreamWriter(CreateWriteFS(filename));
        }

        public static bool IsFileinUse(FileInfo file)
        {
            FileStream stream = null;

            if (!File.Exists(file.FullName)) return false;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

    }

    public class IOUtil
    {
        public static void WriteFloatArray(float[] array, BinaryWriter bw)
        {
            byte[] buf = new byte[array.Length * sizeof(float)];
            Buffer.BlockCopy(array, 0, buf, 0, array.Length * sizeof(float));
            bw.Write(buf);
        }
    }
}
