﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // composite struct runner.
    public class ComputationGraph : StructRunner
    {
        bool IsSetNoBackwardUpdate = false;

        public void StartNoBackwardZoom()
        {
            IsSetNoBackwardUpdate = true;
        }

        public void EndNoBackwardZoom()
        {
            IsSetNoBackwardUpdate = false;
        }

        /// <summary>
        /// Computation Flow.
        /// </summary>
        public List<StructRunner> DataRunners = new List<StructRunner>();
        public BatchData AddDataRunner(StructRunner runner)
        {
            DataRunners.Add(runner);
            return runner.Output;
        }

        public List<StructRunner> SequenceRunners = new List<StructRunner>();
        public List<StructRunner> Session { get { return SequenceRunners; } }
        public BatchData AddRunner(StructRunner runner)
        {
            SequenceRunners.Add(runner);

            if(IsSetNoBackwardUpdate)
            {
                runner.IsBackProp = false;
                runner.IsUpdate = false;
            }
            
            return runner.Output;
        }

        public List<ObjectiveRunner> ObjectiveRunners = new List<ObjectiveRunner>();
        public void AddObjective(ObjectiveRunner runner)
        {
            ObjectiveRunners.Add(runner);
        }

        public List<GradientOptimizer> OptimizerRunners = new List<GradientOptimizer>();

        public List<StructRunner> ParameterRunners = new List<StructRunner>();
        public BatchData AddParameterRunner(StructRunner runner)
        {
            ParameterRunners.Add(runner);
            return runner.Output;
        }

        //public RunnerBehavior Behavior = null;
        public void SetTrainMode()
        {
            Behavior.RunMode = DNNRunMode.Train;
        }

        public void SetPredictMode()
        {
            Behavior.RunMode = DNNRunMode.Predict;
        }

        public void AdjustLR(float lr)
        {
            foreach (GradientOptimizer runner in OptimizerRunners) runner.HyperParameter.LearnRate = lr; 
        }

        // register model over here. gradient should be allocated first.
        //public Structure mDeepNet = null;
        public Structure DeepNet = null;

        public CompositeNNStructure Model = null;

        public void SetDelegateModel(Structure deepNet)
        {
            DeepNet = deepNet;
            OptimizerRunners.Clear();
            OptimizerRunners.AddRange(deepNet.ModelOptimizers);    
            Logger.WriteLog("Delegate Optimizer number {0} ", OptimizerRunners.Count);
        }

        public void AddOptimizer(GradientOptimizer optimizer)
        {
            OptimizerRunners.Add(optimizer);
            Logger.WriteLog("Add Optimizer number {0} ", OptimizerRunners.Count);
        }

        public void AddOptimizer(GradientOptimizer[] optimizers)
        {
            OptimizerRunners.AddRange(optimizers);
            Logger.WriteLog("Add Optimizer number {0} ", OptimizerRunners.Count);

        }

        public void AddOptimizer(StructureLearner learner, NdArrayData data)
        {
            GradientOptimizer optimizer = GradientOptimizer.CreateLocalOptimizer(data.Output, data.Deriv, learner, Behavior);
            AddOptimizer(optimizer);
        }

        public void InitOptimizer(StructureLearner learner)
        {
            DeepNet.InitOptimizer(learner, Behavior);
            OptimizerRunners.Clear();
            OptimizerRunners.AddRange(DeepNet.ModelOptimizers);    
        }

        public void AllocateOptimizer(Structure deepNet, StructureLearner learner)
        {
            DeepNet = deepNet;
            AllocateOptimizer(learner);
        }

        public void AllocateOptimizer(StructureLearner learner)
        {
            //OptimizerRunners.Clear();
            foreach(NdArrayData p in DeepNet.Parameters)
            {
                if(p.Deriv == null)
                {
                    throw new Exception("throw an exception : grad is null.");
                }
                //if(StructureOptimizer.ContainsKey(p.Key))
                //{
                //    Console.WriteLine("WARNING : Structure {0} with gradient is already set to optimizer {1}. skip! ", p.Key, StructureOptimizer[p.Key].ToString());
                //    continue;
                //}
                GradientOptimizer Optimizer = GradientOptimizer.CreateLocalOptimizer(p.Output, p.Deriv, learner, Behavior);

                //Console.WriteLine("Structure {0} with gradient is set to optimizer {1} done! ", p.Key, Optimizer.ToString());
                //StructureOptimizer.Add(p.Key, Optimizer);
                OptimizerRunners.Add(Optimizer);    
            }

            Logger.WriteLog("Optimizer number {0} ", OptimizerRunners.Count);
            //DeepNet.AllocateOptimizer(learner, Behavior);
        }

        bool IsReportGradientRegular = false;
        public virtual void GradientRegular()
        {
            if(DeepNet == null || DeepNet.Learner == null) return;

            float clipNorm = DeepNet.Learner.GradNormClip;
            if(clipNorm > 0)
            {
                if(!IsReportGradientRegular)
                {
                    Logger.WriteLog("Gradient Clip at {0}", clipNorm);
                    IsReportGradientRegular = true;
                }

                float gradL2 = Behavior.Computelib.DotProduct(DeepNet.Grad, DeepNet.Grad, DeepNet.Grad.Size);
                float norm = (float)Math.Sqrt(gradL2);
                
                if(norm > clipNorm)
                {
                    Behavior.Computelib.Scale_Vector(DeepNet.Grad, 0, DeepNet.Grad, 0, DeepNet.Grad.Size, 0, clipNorm / norm);
                }
            }
            

        }
        
        
        /// <summary>
        /// P 
        /// </summary>
        //public bool IsTerminate = false;   /// Sign for end the outer loop.

        /// Break
        //public bool IsContinue = true;     /// Sign for end the inner loop.

        int StepNum = 0;
        double AvgLoss = 0;
        double totalLoss = 0;
        public double StepLoss { get; set; }
        
        public IntArgument GlobalUpdateStep = new IntArgument("GlobalStep", 0);

        
        
        public ComputationGraph(RunnerBehavior behavior) : base(Structure.Empty, behavior)
        { }

        /// <summary>
        /// Forward. 
        /// </summary>
        /// <returns></returns>
        public override void Forward()
        {
            IsTerminate = false;
            IsContinue = true;

            foreach (StructRunner runner in DataRunners)
            {
                runner.Forward();

                IsTerminate = IsTerminate || runner.IsTerminate;
                IsContinue = IsContinue && runner.IsContinue;
            }

            if (IsTerminate || !IsContinue) return;

            int idx = 0;
            foreach (StructRunner runner in SequenceRunners)
            {
                runner.Forward();
                IsTerminate = runner.IsTerminate;
                IsContinue = runner.IsContinue;
                if (IsTerminate || !IsContinue) return;
                idx += 1;
            }
            //return true;
        }

        // public void BeforeOptimization()
        // {
        //     foreach (GradientOptimizer runner in OptimizerRunners) runner.BeforeGradient();
        // }

        public void AfterOptimization()
        {
            foreach (GradientOptimizer runner in OptimizerRunners)
            {
                runner.PrepareAfterGradient();
                runner.AfterGradient();
                runner.ConfirmAfterGradient();
            }
            GlobalUpdateStep.Value += 1;
        }

        public override void CleanDeriv()
        {
            foreach (StructRunner runner in SequenceRunners) { runner.CleanDeriv(); }
            foreach (StructRunner runner in DataRunners) { runner.CleanDeriv(); }
        }

        public override void Backward(bool isClearDeriv)
        {
            foreach (ObjectiveRunner runner in Enumerable.Reverse(ObjectiveRunners)) { runner.Backward(false); }
            foreach (StructRunner runner in Enumerable.Reverse(SequenceRunners)) { if (runner.IsBackProp) runner.Backward(false); }
            foreach (StructRunner runner in Enumerable.Reverse(DataRunners)) { if (runner.IsBackProp) runner.Backward(false); }
        }

        public override void Update()
        {
            foreach (StructRunner runner in SequenceRunners) { if(runner.IsBackProp && runner.IsUpdate) runner.Update(); }
            foreach (StructRunner runner in DataRunners) { if (runner.IsBackProp && runner.IsUpdate) runner.Update(); }
        }

        public double Loss()
        {
            double loss = 0;
            foreach (ObjectiveRunner runner in ObjectiveRunners) { runner.Forward(); loss = loss + runner.ObjectiveScore; }
            return loss;
        }

        public void ParameterRegular()
        {
            foreach(StructRunner runner in ParameterRunners) { runner.Forward(); }
        }

       

        public Dictionary<string,double> MultiLoss()
        {
            Dictionary<string, double> loss = new Dictionary<string, double>();
            foreach (ObjectiveRunner runner in ObjectiveRunners)
            {
                runner.Forward();
                foreach (KeyValuePair<string, double> item in runner.ObjectiveDict)
                {
                    if (!loss.ContainsKey(item.Key))
                    {
                        loss[runner.name + "-" + item.Key] = item.Value;
                    }
                    else
                    {
                        loss[runner.name + "-" + item.Key] += item.Value;
                    }
                }
                loss[runner.name] = runner.ObjectiveScore;
            }
            return loss;
        }

        public override void Complete()
        {
            foreach (StructRunner runner in DataRunners) runner.Complete();
            foreach (StructRunner runner in SequenceRunners) runner.Complete();
            foreach (ObjectiveRunner runner in ObjectiveRunners) runner.Complete();
            
            if(Behavior == null || Behavior.RunMode == DNNRunMode.Train)
            {
                foreach (GradientOptimizer runner in OptimizerRunners) runner.Complete();
            }

            foreach (StructRunner runner in ParameterRunners) runner.Complete();
        }

        public override void Init()
        {
            // must assign behavior.
            Behavior.Resource.InitResource();

            foreach (StructRunner runner in DataRunners) runner.Init();
            foreach (StructRunner runner in SequenceRunners) runner.Init(); 
            foreach (ObjectiveRunner runner in ObjectiveRunners) runner.Init();

            if(Behavior == null || Behavior.RunMode == DNNRunMode.Train)
            {
                foreach (GradientOptimizer runner in OptimizerRunners) runner.Init();
            }

            foreach (StructRunner runner in ParameterRunners) runner.Init();
            IsTerminate = false;
            IsContinue = true;
            StepNum = 0;
            totalLoss = 0;
        }

        // step for training.
        public bool Step()
        {
            if (!IsTerminate)
            {
                //var forwardCounter = PerfCounter.Manager.Instance["Forward"].Begin();
                Forward();
                //PerfCounter.Manager.Instance["Forward"].TakeCount(forwardCounter);

                if (!IsTerminate && IsContinue)
                {
                    ++StepNum;
                    /// no objective function.
                    if (ObjectiveRunners.Count > 0)
                    {
                        CleanDeriv();
                        
                        // usually this function do nothing.
                        //BeforeOptimization();

                        StepLoss = Loss();
                        totalLoss += StepLoss;
                        AvgLoss = (StepNum - 1) * 1.0f / StepNum * AvgLoss + 1.0f / StepNum * StepLoss;

                        Backward(false);
                        
                        Update();
                        
                        GradientRegular();

                        AfterOptimization();

                        ParameterRegular();
                        
                        //if (StepNum % 50 == 0)
                        //    Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", StepNum, AvgLoss);
                    }
                    //else if (StepNum % 500 == 0) Console.WriteLine("MiniBatch {0}", StepNum);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool StepV2()
        {
            if (!IsTerminate)
            {
                //var forwardCounter = PerfCounter.Manager.Instance["Forward"].Begin();
                Forward();

                CleanDeriv();

                //PerfCounter.Manager.Instance["Forward"].TakeCount(forwardCounter);
                StepLoss = Loss();
                
                totalLoss += StepLoss;
                AvgLoss = (StepNum - 1) * 1.0f / StepNum * AvgLoss + 1.0f / StepNum * StepLoss;
                ++StepNum;
                
                if (!IsTerminate && IsContinue)
                {
                    /// no objective function.
                    if (Behavior.RunMode == DNNRunMode.Train)
                    {
                        // usually this function do nothing.
                        //BeforeOptimization();
                        //StepLoss = Loss();
                        Backward(false);
                        Update();
                        GradientRegular();
                        AfterOptimization();
                        ParameterRegular();
                        //if (StepNum % 50 == 0)
                        //    Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", StepNum, AvgLoss);
                    }
                    //else if (StepNum % 500 == 0) Console.WriteLine("MiniBatch {0}", StepNum);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ForwardV3()
        {
            Forward();
            CleanDeriv();
        }

        public void BackwardV3()
        {
            Backward(false);
            Update();
        }

        public float StepV3Run()
        {
            Forward();

            CleanDeriv();

            //PerfCounter.Manager.Instance["Forward"].TakeCount(forwardCounter);
            StepLoss = Loss();

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                //BeforeOptimization();
                Backward(false);
                Update();
                //AfterOptimization();
                //ParameterRegular();
            }
            return (float)StepLoss;
        }

        public void StepV3Update()
        {
            if (Behavior.RunMode == DNNRunMode.Train)
            {
                GradientRegular();
                AfterOptimization();
                
                //Meta Optimization();

                ParameterRegular();
            }
        }

        public int StatusReportSteps = 100;
        public bool IsReportObjectives = true;
        
        /// <summary>
        /// Execute Computation Graph without data source.
        /// or Data Source is another data runner.
        /// </summary>
        /// <returns></returns>
        public double Execute(bool withUpdate = true)
        {
            DateTime pre = DateTime.Now;
            //double loss = 0;

            Dictionary<string, double> multiObj = new Dictionary<string, double>();

            int batchIdx = 0;
            //var trainingCounter = PerfCounter.Manager.Instance["Running"].Begin();

            //int trackMiniBatch = -1;
            //if (ParameterSetting.DEBUG && DeepNet != null)
            //{
            //    DeepNet.TrackModelParameter();
            //    trackMiniBatch = 1;
            //}

            Init();
            
            while (!IsTerminate)
            {
                //PerfCounter.Manager.Instance["Forward"].CountOperation(() => Forward());
                Forward();

                if(!IsTerminate && IsContinue)
                {
                    /// no objective function.
                    if (ObjectiveRunners.Count > 0)
                    {
                        //PerfCounter.Manager.Instance["CleanDerivation"].CountOperation(() => CleanDeriv());
                        CleanDeriv();

                        //delegate the model optimizer in Computation Graph.
                        //if (withUpdate) { BeforeOptimization(); }
                        //foreach (ModelOptimizer optimizer in DeepNet.ModelOptimizers) optimizer.Optimizer.BeforeGradient();

                        //double tmpLoss = 0;
                        Dictionary<string, double> tmpLoss = null;
                        //PerfCounter.Manager.Instance["LossDerivFunction"].CountOperation(() => tmpLoss = MultiLoss());
                        tmpLoss = MultiLoss();

                        //loss = batchIdx * 1.0f / (batchIdx + 1) * loss + 1.0f / (batchIdx + 1) * tmpLoss["OBJ-LOSS"];
                        foreach (KeyValuePair<string, double> item in tmpLoss)
                        {
                            if(multiObj.ContainsKey(item.Key))
                            {
                                multiObj[item.Key] = batchIdx * 1.0f / (batchIdx + 1) * multiObj[item.Key] + 1.0f / (batchIdx + 1) * item.Value;
                            }
                            else
                            {
                                multiObj[item.Key] = 1.0f / (batchIdx + 1) * item.Value;
                            }
                        }

                        //totalLoss += tmpLoss["OBJ-LOSS"];
                        if (withUpdate)
                        {
                            //PerfCounter.Manager.Instance["Backward"].CountOperation(() => Backward(false));
                            Backward(false);

                            //PerfCounter.Manager.Instance["Update"].CountOperation(() => Update());
                            Update();
                        }
                        if( withUpdate ) { GradientRegular(); }
                        if (withUpdate) { AfterOptimization(); }
                        
                        if (withUpdate) { ParameterRegular(); }

                        if (++batchIdx % StatusReportSteps == 0)
                        {
                            Logger.WriteLog("Batch Num {0}:", batchIdx);
                            foreach(string key in multiObj.Keys)
                            {
                                Logger.WriteLog(" {0},  Avg  {1}, Current  {2}", key, multiObj[key], tmpLoss.ContainsKey(key) ? tmpLoss[key] : 0);
                            }
                        }
                    }
                    else if (++batchIdx % StatusReportSteps == 0) Console.WriteLine("MiniBatch {0} at {1}", batchIdx, DateTime.Now);
                }
            }

            if (ObjectiveRunners.Count > 0 && IsReportObjectives)
            {
                Logger.WriteLog("Running Batch:{0}, Time cost: {1}s", batchIdx, (DateTime.Now - pre).TotalSeconds);
                foreach (string key in multiObj.Keys)
                {
                    Logger.WriteLog("{0},  Avg  {1}", key, multiObj[key]);
                }
            }

            Complete();
            //PerfCounter.Manager.Instance["Running"].TakeCount(trainingCounter);
            return 0;
        }


    }
}
