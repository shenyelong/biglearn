﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class DataRandomShuffling
    {
        int Idx = 0;
        int Num = 0;
        //List<int> RandomIdxs = new List<int>();
        public int[] RandomIdxs = null;
        Random mRandom = null;
        public DataRandomShuffling(int num, Random random = null)
        {
            Num = num;
            RandomIdxs = new int[Num];
            
            Init();

            if (random == null) { mRandom = new Random(13); }
            else { mRandom = random; }
        }
        
        public void Init()
        {
            Idx = 0;
            for(int i=0; i < Num; i++) RandomIdxs[i] = i;
        }

        public void Shuffling()
        {
            Idx = 0;
            Shuffling(0, Num);
        }

        public IEnumerable<int> Idxs
        {
            get
            {
                while(!IsDone)
                {
                    yield return RandomIdxs[Idx];
                    Idx += 1;
                }
            }
        }

        public void SetCursor(int idx)
        {
            Idx = idx;
        }

        public int GetCursor() 
        {
            return Idx; 
        }
        public void Shuffling(int idx, int offset)
        {
            int e = Math.Min(Num, idx + offset);
            for( int i = idx; i < e; i++)
            {
                int randomPos = mRandom.Next(i, e);
                int id = RandomIdxs[randomPos];
                RandomIdxs[randomPos] = RandomIdxs[i];
                RandomIdxs[i] = id;
            }
        }
        

        public bool IsDone { get { return Idx >= Num; } }

        public bool TestRandomBatch(int batchSize)
        {
            if(Idx + batchSize > Num) return false;
            else return true;
        }

        public int RandomNext()
        {
            if (Idx >= Num) return -1;
            int randomPos = mRandom.Next(Idx, Num);

            int id = RandomIdxs[randomPos];
            RandomIdxs[randomPos] = RandomIdxs[Idx];
            RandomIdxs[Idx] = id;
            Idx++;
            return id;
        }

        public int Random()
        {
            int randomPos = mRandom.Next(0, Num);
            return RandomIdxs[randomPos];
        }
        
        public int OrderNext()
        {
            if (Idx >= Num) return -1;
            int id = RandomIdxs[Idx];
            Idx++;
            return id;
        }
    }

}
