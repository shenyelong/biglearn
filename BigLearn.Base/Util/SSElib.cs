﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public static class SSElib
    {
        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static float SSE_SumSq(float * x, int len);

        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static float SSE_SumAbs(float * x, int len);

        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static float SSE_DotProduct(float * a, float * b, int size);

        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void SSE_AddScale(float a, float * ps, float * pd, int size);

        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void SSE_AddScaleEx(float a, float b, float * pa, float * pb, float * pd, int c);

        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void SSE_Scale(float a, float * psd, int size);

        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void SSE_MatrixAggragateRowwise(float * A, float * B, int ARow, int AColumn_BLength, float w1, float w2);
    
        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void ApplyMatMul(float * pLeft, float * pRight, float * pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2);
    
        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void ApplyMatMulTR(float * pLeft, float * pRight, float * pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2);

        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void ApplyMatMulTL(float * pLeft, float * pRight, float * pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2);
        
        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]
        public extern unsafe static void ApplyMatMulTLStride(float * pLeft, float * pRight, float * pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2, int leftStride, int rightStride, int dstStride);

        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]        
        public extern unsafe static void ApplyMatMulTLTR(float * pLeft, float * pRight, float * pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2);
    
        [DllImport("Sselib", CallingConvention = CallingConvention.StdCall)]        
        public extern unsafe static void ApplyOutterProduct(float * pLeft, float * pRight, float * pDst, int leftLength, int rightLength, float weight);
    }    
}
