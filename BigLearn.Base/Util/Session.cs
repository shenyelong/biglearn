using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // Dynamic Computation Graph.
    // assume most of the data is defined as NdArrayData.
    public class Session : ComputationGraph
    {
        public Session(RunnerBehavior behavior) : base(behavior)
        {
            Model = new CompositeNNStructure();
            DeepNet = Model; 
        }

        public NdArrayData LookupEmbed(EmbedStructure embed, int[] input)
        {
            LookupEmbedRunner runner = new LookupEmbedRunner(new CudaPieceInt(input, Behavior.Device), input.Length, embed, Behavior);
            AddRunner(runner);
            return runner.Output.ToND();
        }

        public NdArrayData LookupEmbed(EmbedStructure embed, CudaPieceInt input)
        {
            LookupEmbedRunner runner = new LookupEmbedRunner(input, input.Size, embed, Behavior);
            AddRunner(runner);
            return runner.Output.ToND();
        }

        public NdArrayData LookupEmbed(NdArrayData data, CudaPieceInt input)
        {
            LookupEmbedRunner runner = new LookupEmbedRunner(input, input.Size, data.Output, data.Deriv, data.Dimensions[1].Default, data.Dimensions[0].Default, Behavior);
            AddRunner(runner);
            return runner.Output.ToND();
        }

        public void LookupEmbed(NdArrayData src, CudaPieceInt index, NdArrayData tgt)
        {
            LookupEmbedRunner runner = new LookupEmbedRunner(index, index.Size, src.Output, src.Deriv, src.Dimensions[1].Default, src.Dimensions[0].Default, tgt.Output, tgt.Deriv, Behavior);
            AddRunner(runner);
        }


        // activation of ndarray.
        public NdArrayData Act(NdArrayData data, A_Func af)
        {
            TensorActivateRunner runner = new TensorActivateRunner(data, af, Behavior);
            AddRunner(runner);
            return runner.Output;
        }

        public NdArrayData Add(NdArrayData data1, NdArrayData data2)
        {
            return data1.AddExpand(data2, Session, Behavior);
        }

        public NdArrayData Add(NdArrayData data1, float beta1, NdArrayData data2, float beta2)
        {
            TensorAddRunner tensorAddRunner = new TensorAddRunner(data1, beta1, data2, beta2, Behavior);
            AddRunner(tensorAddRunner);
            return tensorAddRunner.Output;
        }

        public NdArrayData Scale(NdArrayData data, float scale)
        {
            ScaleRunner runner = new ScaleRunner(data.Context, scale, Behavior);
            AddRunner(runner);
            return new NdArrayData(data.DeviceType, runner.Output.Output, runner.Output.Deriv, data.Shape);
        }

        public NdArrayData Dot(NdArrayData data1, NdArrayData data2)
        {
            TensorDotRunner tensorDotRunner = new TensorDotRunner(data1, data2, Behavior);
            AddRunner(tensorDotRunner);
            return tensorDotRunner.Output;
        }

        public CudaPieceInt Argmax(NdArrayData data, int dim)
        {
            TensorArgmaxRunner argmaxRunner = new TensorArgmaxRunner(data, dim, Behavior);
            AddRunner(argmaxRunner);
            return argmaxRunner.Indices;
        }

        public Tuple<NdArrayData, CudaPieceInt, CudaPieceInt> ArgmaxK(NdArrayData data, int topK)
        {
            TensorArgmaxKRunner argmaxKRunner = new TensorArgmaxKRunner(data, topK, Behavior);
            AddRunner(argmaxKRunner);
            return new Tuple<NdArrayData, CudaPieceInt, CudaPieceInt>(argmaxKRunner.Output, argmaxKRunner.Indices, argmaxKRunner.Indices_v2);
        }

        //        public TensorArgmaxKV2Runner(NdArrayData input, int topK, int max_dim, RunnerBehavior behavior) : base(Structure.Empty, behavior)

        public Tuple<NdArrayData, CudaPieceInt, CudaPieceInt> ArgmaxKV2(NdArrayData data, int topK, int max_dim)
        {
            TensorArgmaxKV2Runner argmaxKRunner = new TensorArgmaxKV2Runner(data, topK, max_dim, Behavior);
            AddRunner(argmaxKRunner);
            return new Tuple<NdArrayData, CudaPieceInt, CudaPieceInt>(argmaxKRunner.Output, argmaxKRunner.LocalIndices, argmaxKRunner.GlobalIndices);
        }

        public Tuple<NdArrayData, CudaPieceInt, CudaPieceInt> ArgmaxKV3(NdArrayData data, int topK)
        {
            TensorArgmaxKV3Runner argmaxKRunner = new TensorArgmaxKV3Runner(data, topK, Behavior);
            AddRunner(argmaxKRunner);
            return new Tuple<NdArrayData, CudaPieceInt, CudaPieceInt>(argmaxKRunner.Output, argmaxKRunner.Indices, argmaxKRunner.Indices_v2);
        }

        public NdArrayData Norm(NdArrayData data, int normDim)
        {
            return data.Norm(normDim, Session, Behavior);
        }

        public NdArrayData L2Norm(NdArrayData data, int normDim)
        {
            TensorL2NormRunner normRunner = new TensorL2NormRunner(data, normDim, Behavior);
            AddRunner(normRunner);
            return normRunner.Output;
        }

        public NdArrayData DotAndAdd(NdArrayData data, NdArrayData scale, NdArrayData bias)
        {   
            TensorDotAndAddRunner runner = new TensorDotAndAddRunner(data, scale, bias, Behavior);
            AddRunner(runner);
            return runner.Output;
            //return data.DotAndAdd(scale, bias, Session, Behavior);
        }

        public NdArrayData Transformer(TransformerStructure block, NdArrayData input, int head_num, CudaPieceFloat maskScale, CudaPieceFloat maskBias, float dropout)
        {
            TransformerRunner blockRunner = new TransformerRunner(block, head_num, input.Shape[1].Default, input, maskScale, maskBias, dropout, Behavior);
            AddRunner(blockRunner);
            return blockRunner.Output;
        }

        public NdArrayData Transformer(TransformerStructure block, NdArrayData input, int head_num, float dropout)
        {
            TransformerRunner blockRunner = new TransformerRunner(block, head_num, input.Shape[1].Default, input, null, null, dropout, Behavior);
            AddRunner(blockRunner);
            return blockRunner.Output;
        }
        
        public NdArrayData Transformer(TransformerRunner runner, NdArrayData input)
        {
            TensorCopyRunner copyRunner = new TensorCopyRunner(input, runner.Input, Behavior);
            AddRunner(copyRunner);
            AddRunner(runner);
            return runner.Output;
        }

        public NdArrayData AttentionTransformer(AttentionTransformer block, NdArrayData input, int head_num, NdArrayData memory, float dropout)
        {
            AttentionTransformerRunner blockRunner = new AttentionTransformerRunner(block, head_num, dropout, input, memory, Behavior);
            //int nhead, float dropout, NdArrayData input, NdArrayData memory, RunnerBehavior behavior) : base(behavior)
            AddRunner(blockRunner);
            return blockRunner.Output;

        }
        
        public NdArrayData MatMul(NdArrayData data1, int op1, NdArrayData data2, int op2)
        {
    	    if(data1.Shape.Length != data2.Shape.Length)
    	    {
    		  throw new Exception(string.Format("matmul shape dimension should match each other {0}, {1}", data1.Shape.Length, data2.Shape.Length));
    	    }
            if(data1.Shape.Length > 2)
	        {
            	//return data1.MatMul(op1, data2, op2, Session, Behavior);
                NdMultiplicationRunner matMulRunner = new NdMultiplicationRunner(data1, op1, data2, op2, Behavior);
                AddRunner(matMulRunner);
                return matMulRunner.Output;
            }
    	    else if(data1.Shape.Length == 2)
    	    {
                MatrixMultiplicationRunner matMulRunner = new MatrixMultiplicationRunner(data1.ToMD(), op1, data2.ToMD(), op2, Behavior);
                AddRunner(matMulRunner);
                return matMulRunner.Output.ToND();
    		      //return data1.ToMD().MatMul(op1, data2.ToMD(), op2, Session, Behavior).ToND();	
    	    }
	       return null;
	    }

        public NdArrayData Reshape(NdArrayData data, params IntArgument[] shape)
        {
            return data.Reshape(shape);
        }

        public NdArrayData Reshape(CudaPieceFloat data, params IntArgument[] shape)
        {
            return new NdArrayData(data.DeviceType, data, null, shape);
        }

        
        public NdArrayData Reshape(CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] shape)
        {
            return new NdArrayData(data.DeviceType, data, deriv, shape);
        }


        // mean square error on a tensor.
        public ObjectiveRunner MSE(NdArrayData data, float weight, float clipDelta)
        {
            MSERunnerV2 objRunner = new MSERunnerV2(data, weight, clipDelta, Behavior); 
            AddObjective(objRunner);
            return objRunner;
        }

        public ObjectiveRunner SoftmaxLoss(NdArrayData data, CudaPieceInt label)
        {
            SoftmaxObjRunner objRunner = new SoftmaxObjRunner(label, data.ToHDB(), Behavior);
            AddObjective(objRunner);
            return objRunner; 
        }

        public ObjectiveRunner SmoothSoftmaxLoss(NdArrayData data, CudaPieceInt label, float smooth)
        {
            SmoothSoftmaxObjRunner objRunner = new SmoothSoftmaxObjRunner(label, data.ToHDB(), smooth, Behavior);
            AddObjective(objRunner);
            return objRunner;
        }

        public ObjectiveRunner DotLoss(NdArrayData data, CudaPieceFloat label)
        {
            DotLossRunner objRunner = new DotLossRunner(data, label, Behavior);
            AddObjective(objRunner);
            return objRunner;
        }

        public StandardKLRunner StandardKLLoss(NdArrayData mean, NdArrayData logv, FloatArgument weight)
        {
            StandardKLRunner objRunner = new StandardKLRunner(mean, logv, weight, Behavior);
            AddObjective(objRunner);
            return objRunner;
        }


        public CrossEntropyRunner CrossEntropyLoss(NdArrayData logit, CudaPieceFloat label, float weight, bool isAvg)
        {
            CrossEntropyRunner ceRunner = new CrossEntropyRunner(label, logit.ToHDB(), Behavior) { IsReportBCE = true, IsAvg = isAvg, Weight = weight };
            AddObjective(ceRunner);
            return ceRunner;
        }

        public SeqDenseBatchData Tensor2SeqData(NdArrayData data, CudaPieceInt seqlen)
        {
            Tensor2SeqRunner runner = new Tensor2SeqRunner(data, seqlen, Behavior);
            AddRunner(runner);
            return runner.Output;
        }

        public NdArrayData SeqData2CompactTensor(SeqDenseBatchData data)
        {
            return data.ToHDB().ToND();
        }

        public NdArrayData LSTM(LSTMStructure model, SeqDenseBatchData data)
        {
            return LSTM(model, data, true);
        } 

        public NdArrayData LSTM(LSTMStructure model, SeqDenseBatchData data, bool isUpdate)
        {
            LSTMRunner runner = new LSTMRunner(model, data, Behavior, true);
            runner.IsUpdate = isUpdate;
            AddRunner(runner);
            return runner.LastStatus.ToND();
        } 

        public NdArrayData LSTM(LSTMStructure model, SeqDenseBatchData data, NdArrayData[] preO, NdArrayData[] preC)
        {
            LSTMRunner runner = new LSTMRunner(model, data, preO.Select(i => i.ToHDB()).ToList(), preC.Select(i => i.ToHDB()).ToList(), Behavior, true);
            AddRunner(runner);
            return runner.LastStatus.ToND();
        }

        public SeqDenseBatchData LSTMEncode(LSTMStructure model, SeqDenseBatchData data)
        {
            LSTMRunner runner = new LSTMRunner(model, data, Behavior, false);
            AddRunner(runner);
            return runner.Output;
        }

        public SeqDenseBatchData LSTMEncode(LSTMStructure model, SeqDenseBatchData data, NdArrayData[] preO, NdArrayData[] preC)
        {
            LSTMRunner runner = new LSTMRunner(model, data, preO.Select(i => i.ToHDB()).ToList(), preC.Select(i => i.ToHDB()).ToList(), Behavior, false);
            AddRunner(runner);
            return runner.Output;
        }


        public NdArrayData BiLSTM(LSTMStructure fwmodel, LSTMStructure bwmodel, SeqDenseBatchData data)
        {
            return BiLSTM(fwmodel, bwmodel, data, true);
        } 

        public NdArrayData BiLSTM(LSTMStructure fwmodel, LSTMStructure bwmodel, SeqDenseBatchData data, bool isUpdate)
        {
            BiLSTMRunner runner = new BiLSTMRunner(fwmodel, bwmodel, data, Behavior, true);
            runner.IsUpdate = isUpdate;
            AddRunner(runner);
            return runner.LastStatus.ToND();
        } 

        public NdArrayData FNN(NdArrayData dataA, LayerStructure model)
        {
            return dataA.FNN(model, Session, Behavior);
            // FNNRunner runner = new FNNRunner(dataA, model.NDWeight, model.NDBias, model.Af, Behavior);
            // AddRunner(runner);
            // return runner.Output; 

            // HiddenBatchData input = new HiddenBatchData(batch, dataA.Dimensions[0].Value, dataA.Output, dataA.Deriv, behavior.Device);
        
            // FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(model, input, behavior);
            // session.Add(fcRunner);

            // IntArgument outDim = new IntArgument("outDim", fcRunner.Output.Dim);
            // IntArgument[] newDims = new IntArgument[dataA.Dimensions.Length];
            // newDims[0] = outDim;
            // for(int i= 1; i<dataA.Dimensions.Length; i++)
            // {
            //     newDims[i] = dataA.Dimensions[i];
            // }
            // return new NdArrayData(newDims, fcRunner.Output.Output.Data, fcRunner.Output.Deriv.Data, behavior.Device);


            //return dataA.FNN(model, Session, Behavior);
        } 

        public NdArrayData FNN(NdArrayData data, NdArrayData weight, NdArrayData bias, A_Func af)
        {
            //IntArgument[] flatDims = new IntArgument[data.Dimensions.Length - 1];
            //for(int i = 1; i < data.Dimensions.Length; i++) flatDims[i - 1] = data.Dimensions[i];
            //IntArgument flatDim = Session.Flat(Behavior, flatDims);
            FNNRunner runner = new FNNRunner(data, weight, bias, af, Behavior);
            AddRunner(runner);
            return runner.Output; 
        }

        public Tuple<NdArrayData, NdArrayData> LSTMState(LSTMCell model, NdArrayData po, NdArrayData pc, NdArrayData input) 
        {
            LSTMStateRunner runner = new LSTMStateRunner(model, po.ToHDB(), pc.ToHDB(), input.ToHDB(), Behavior);
            AddRunner(runner);
            return new Tuple<NdArrayData, NdArrayData>(runner.o, runner.c);
        }

        public Tuple<NdArrayData[], NdArrayData[]> LSTMState(LSTMStructure model, NdArrayData[] po, NdArrayData[] pc, NdArrayData input)
        {
            NdArrayData[] O = new NdArrayData[model.LSTMCells.Count];
            NdArrayData[] C = new NdArrayData[model.LSTMCells.Count];

            HiddenBatchData emd = input.ToHDB();
            
            for(int l = 0; l < model.LSTMCells.Count; l++)
            {
                int r_i = model.LSTMCells.Count - po.Length;
                //r_i > i) ? null : lastO[i - r_i];
                LSTMStateRunner runner = new LSTMStateRunner(model.LSTMCells[l], r_i > l ? null : po[l - r_i].ToHDB(), r_i > l ? null : pc[l - r_i].ToHDB(), emd, Behavior);
                AddRunner(runner);
                emd = runner.O;

                O[l] = runner.O.ToND();
                C[l] = runner.C.ToND();
            }
            return new Tuple<NdArrayData[], NdArrayData[]>(O, C);
        }

        public Tuple<NdArrayData, CudaPieceInt> GumbelSoftmax(NdArrayData logits, RateScheduler temp, bool hard)
        {
            GumbelSoftmaxRunner runner = new GumbelSoftmaxRunner(logits.ToHDB(), temp, hard, Behavior);
            AddRunner(runner);
            return new Tuple<NdArrayData, CudaPieceInt>(runner.Output.ToND(), runner.maxIndex);
        }

        public Tuple<NdArrayData, CudaPieceInt> GumbelSoftmax(NdArrayData logits, RateScheduler temp, FloatArgument lambda, bool hard)
        {
            GumbelSoftmaxRunner runner = new GumbelSoftmaxRunner(logits.ToHDB(), temp, hard, lambda, Behavior);
            AddRunner(runner);
            return new Tuple<NdArrayData, CudaPieceInt>(runner.Output.ToND(), runner.maxIndex);
        }

        public Tuple<NdArrayData, CudaPieceInt> GumbelSoftmax(NdArrayData logits, bool hard)
        {
            return GumbelSoftmax(logits, null, hard);
        }


        public NdArrayData Softmax(NdArrayData logits)
        {
            NdArraySoftmaxRunner softmaxRunner = new NdArraySoftmaxRunner(logits, Behavior);
            AddRunner(softmaxRunner);
            return softmaxRunner.Output;
        }

        public NdArrayData Cat(NdArrayData[] data, int cat_dim)
        {
            TensorCatRunner runner = new TensorCatRunner(data.ToList(), cat_dim, Behavior);
            AddRunner(runner);
            return runner.Output;
        }

        public NdArrayData Expand(NdArrayData data, int expandIdx, IntArgument expandDim)
        {
            TensorExpansionRunner runner = new TensorExpansionRunner(data, expandIdx, expandDim, Behavior);
            AddRunner(runner);
            return runner.Output;
        }

        public Tuple<CudaPieceInt, CudaPieceInt> DivMod(CudaPieceInt data, int dim)
        {
            IntDivModRunner runner = new IntDivModRunner(data, dim, Behavior);
            AddRunner(runner);
            return new Tuple<CudaPieceInt, CudaPieceInt>(runner.Div_Output, runner.Mod_Output);
        }

        public CudaPieceInt Inc(CudaPieceInt data, int num)
        {
            IntIncRunner runner = new IntIncRunner(data, num, Behavior);
            AddRunner(runner);
            return runner._Output;
        }

        public Tuple<NdArrayData, NdArrayData, NdArrayData> BaseBertFeaturizer(BaseBertModel model, CudaPieceInt tok_ids, CudaPieceInt seg_ids, CudaPieceInt mask_ids, int batch_size, int max_seq_len)
        {
            BaseBertRunner bertRunner = new BaseBertRunner(model, tok_ids, seg_ids, mask_ids, batch_size, max_seq_len, Behavior);
            AddRunner(bertRunner);
            return new Tuple<NdArrayData, NdArrayData, NdArrayData>(bertRunner.Featurer, bertRunner.Slice.ToND(), bertRunner.Pool.ToND());
        }

        public Tuple<NdArrayData, NdArrayData, NdArrayData> BaseBertFeaturizer(BaseBertModel model, CudaPieceInt tok_ids, CudaPieceInt seg_ids, CudaPieceInt mask_ids, int batch_size, int max_seq_len, int nhead)
        {
            BaseBertRunner bertRunner = new BaseBertRunner(model, tok_ids, seg_ids, mask_ids, batch_size, max_seq_len, nhead, Behavior);
            AddRunner(bertRunner);
            return new Tuple<NdArrayData, NdArrayData, NdArrayData>(bertRunner.Featurer, bertRunner.Slice.ToND(), bertRunner.Pool.ToND());
        }

        public Tuple<NdArrayData, NdArrayData, NdArrayData> BaseBertFeaturizer(BaseBertModel model, NdArrayData token_embeds, CudaPieceInt seg_ids, CudaPieceInt mask_ids, int batch_size, int max_seq_len, int nhead)
        {
            BaseBertRunner bertRunner = new BaseBertRunner(model, token_embeds, seg_ids, mask_ids, batch_size, max_seq_len, nhead, Behavior);
            AddRunner(bertRunner);
            return new Tuple<NdArrayData, NdArrayData, NdArrayData>(bertRunner.Featurer, bertRunner.Slice.ToND(), bertRunner.Pool.ToND());
        }


        public NdArrayData[] BaseBertFeatureMaps(BaseBertModel model, CudaPieceInt tok_ids, CudaPieceInt seg_ids, CudaPieceInt mask_ids, int batch_size, int max_seq_len)
        {
            BaseBertRunner bertRunner = new BaseBertRunner(model, tok_ids, seg_ids, mask_ids, batch_size, max_seq_len, Behavior);
            AddRunner(bertRunner);
            return bertRunner.LayerMaps;
        }

        public NdArrayData[] BaseBertFeatureMaps(BaseBertModel model, CudaPieceInt tok_ids, CudaPieceInt seg_ids, CudaPieceInt mask_ids, int batch_size, int max_seq_len, int nhead)
        {
            BaseBertRunner bertRunner = new BaseBertRunner(model, tok_ids, seg_ids, mask_ids, batch_size, max_seq_len, nhead, Behavior);
            AddRunner(bertRunner);
            return bertRunner.LayerMaps;
        }


        public NdArrayData Dropout(NdArrayData data, float dropout)
        {
            TensorDropoutRunner dropRunner = new TensorDropoutRunner(data.ToT4D(), dropout, Behavior);
            AddRunner(dropRunner);
            return dropRunner.Output.ToND().Reshape(data.Shape);
        }

        public Tuple<NdArrayData, NdArrayData> Mask(CudaPieceInt mask)
        {
            CudaPieceFloat scaleVec = new CudaPieceFloat(2, Behavior.Device);
            scaleVec[0] = 0;
            scaleVec[1] = 1;
            scaleVec.SyncFromCPU();

            CudaPieceFloat biasVec = new CudaPieceFloat(2, Behavior.Device);
            biasVec[0] = (float)-1e9;
            biasVec[1] = 0;
            biasVec.SyncFromCPU();

            LookupEmbedRunner scaleLookupRunner = new LookupEmbedRunner(mask, mask.Size, new EmbedStructure(2, 1, scaleVec, Behavior.Device), Behavior);
            LookupEmbedRunner biasLookupRunner = new LookupEmbedRunner(mask, mask.Size, new EmbedStructure(2, 1, biasVec, Behavior.Device), Behavior);

            AddRunner(scaleLookupRunner);
            AddRunner(biasLookupRunner);

            return new Tuple<NdArrayData, NdArrayData>(scaleLookupRunner.Output.ToND().NoGrad(), biasLookupRunner.Output.ToND().NoGrad());
        }

        public NdArrayData Slice(NdArrayData data, int offset, int length)
        {
            IntArgument[] sliceShape = new IntArgument[data.Dimensions.Length];

            int cursor = 1;
            for(int i = 0; i < data.Dimensions.Length - 1; i++)
            { 
                sliceShape[i] = data.Dimensions[i];
                cursor = cursor * sliceShape[i].Default;
            }
            int cursor_start = cursor * offset;
            int cursor_length = cursor * length;

            sliceShape[data.Dimensions.Length - 1] = new IntArgument("slice length", length);

            return new NdArrayData(sliceShape, data.Output.SubArray(cursor_start, cursor_length), data.Deriv == null ? null : data.Deriv.SubArray(cursor_start, cursor_length), Behavior.Device);
        }

        public NdArrayData Slice(NdArrayData data, int[] offset, params IntArgument[] shape)
        {
            IntArgument[] offsetShape = new IntArgument[offset.Length];

            for(int i = 0; i < offset.Length; i++) offsetShape[i] = new IntArgument("offset dim", offset[i]);

            TensorSliceRunner runner = new TensorSliceRunner(data, offsetShape, shape, Behavior);
            AddRunner(runner);

            return runner.Output;
        }
        //         public FillupTensorRunner(CudaPieceInt idxes, NdArrayData memory, NdArrayData data, RunnerBehavior behavior) 
        public void Fillup3DTensor(NdArrayData memory, NdArrayData data, CudaPieceInt pos)
        {
            FillupTensorRunner runner = new FillupTensorRunner(pos, memory, data, Behavior);
            AddRunner(runner);
        }

        public NdArrayData Extract3DTensor(NdArrayData memory, CudaPieceInt pos)
        {
            ExtractupTensorRunner runner = new ExtractupTensorRunner(pos, memory, Behavior);
            AddRunner(runner);
            return runner.Data;
        }

        public NdArrayData L2Normalized(NdArrayData a, int reduceDim)
        {
            TensorL2NormRunner l2Runner = new TensorL2NormRunner(a, reduceDim, Behavior);  
            AddRunner(l2Runner);
            NdArrayData l2 = l2Runner.Output;

            TensorDivRunner divRunner = new TensorDivRunner(l2, Behavior);
            AddRunner(divRunner);
            NdArrayData _l2 = divRunner.Output;

            TensorDotRunner dotRunner = new TensorDotRunner(a, _l2, Behavior);
            AddRunner(dotRunner);
            
            return dotRunner.Output;
        }

    }
}
