﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    public enum NormalizerType
    {
        NONE = 0,
        MIN_MAX = 1,
        ZERO_MIN_MAX = 2,
    }

    public class Normalizer
    {
        int FeatureDim = 0;
        public virtual NormalizerType Type
        {
            get
            {
                return NormalizerType.NONE;
            }
        }

        public Normalizer(int featureDim)
        {
            FeatureDim = featureDim;
        }
        public virtual void AnalysisBatch(SeqSparseBatchData input)
        { }

        public virtual void AnalysisEnd()
        { }

        public virtual void ProcessBatch(SeqSparseBatchData input)
        { }

        public static Normalizer CreateFeatureNormalize(NormalizerType type, int featureSize)
        {
            Normalizer norm = null;
            switch (type)
            {
                case NormalizerType.NONE:
                    norm = new Normalizer(featureSize);
                    break;
                case NormalizerType.MIN_MAX:
                    norm = new MinMaxNormalizer(featureSize);
                    break;
                case NormalizerType.ZERO_MIN_MAX:
                    norm = new ZeroMinMaxNormalizer(featureSize);
                    break;
            }
            return norm;
        }
    }

    public class MinMaxNormalizer : Normalizer
    {
        public float[] FeaMin;
        public float[] FeaMax;
        Dictionary<int, int> FeaDict = new Dictionary<int, int>();
        int SampleNum = 0;
        public MinMaxNormalizer(int featureDim)
            : base(featureDim)
        {
            FeaMin = new float[featureDim];
            FeaMax = new float[featureDim];
            for (int i = 0; i < featureDim; i++)
            {
                FeaMin[i] = float.MaxValue;
                FeaMax[i] = float.MinValue;
            }
        }

        public override void AnalysisEnd()
        {
            foreach (int fid in FeaDict.Keys)
            {
                if (FeaDict[fid] < SampleNum)
                {
                    if (FeaMin[fid] > 0)
                        FeaMin[fid] = 0;
                    if (FeaMax[fid] < 0)
                        FeaMax[fid] = 0;
                }
            }
        }

        public override NormalizerType Type
        {
            get
            {
                return NormalizerType.MIN_MAX;
            }
        }

        public override void AnalysisBatch(SeqSparseBatchData input)
        {
            SampleNum += input.BatchSize;
            for (int sample = 0; sample < input.BatchSize; ++sample)
            {
                int seg_begin = sample >= 1 ? input.SampleIdx[sample - 1] : 0;
                int seg_end = input.SampleIdx[sample];

                for (int seg_id = seg_begin; seg_id < seg_end; ++seg_id)
                {
                    int fea_begin = seg_id >= 1 ? input.SequenceIdx[seg_id - 1] : 0;
                    int fea_end = input.SequenceIdx[seg_id];

                    for (int fea_id = fea_begin; fea_id < fea_end; ++fea_id)
                    {
                        int fea_key = input.FeaIdx[fea_id];
                        float fea_value = input.FeaValue[fea_id];
                        
                        if (FeaDict.ContainsKey(fea_key))
                            FeaDict[fea_key] += 1;
                        else
                            FeaDict[fea_key] = 1;

                        if (FeaMin[fea_key] > fea_value)
                            FeaMin[fea_key] = fea_value;
                        if (FeaMax[fea_key] < fea_value)
                            FeaMax[fea_key] = fea_value;
                    }
                }
            }
        }

        public override void ProcessBatch(SeqSparseBatchData input)
        {
            for (int sample = 0; sample < input.BatchSize; ++sample)
            {
                int seg_begin = sample >= 1 ? input.SampleIdx[sample - 1] : 0;
                int seg_end = input.SampleIdx[sample];

                for (int seg_id = seg_begin; seg_id < seg_end; ++seg_id)
                {
                    int fea_begin = seg_id >= 1 ? input.SequenceIdx[seg_id - 1] : 0;
                    int fea_end = input.SequenceIdx[seg_id];

                    for (int fea_id = fea_begin; fea_id < fea_end; ++fea_id)
                    {
                        int fea_key = input.FeaIdx[fea_id];
                        float fea_value = input.FeaValue[fea_id];
                        float new_fea_value = (fea_value - FeaMin[fea_key]) / (FeaMax[fea_key] - FeaMin[fea_key] + float.Epsilon);
                        input.FeaValue[fea_id] = new_fea_value;
                    }
                }
            }
        }
    }

    public class ZeroMinMaxNormalizer : Normalizer
    {
        public float[] FeaMin;
        public float[] FeaMax;
        public float[] FeaAvg;
        Dictionary<int, int> FeaDict = new Dictionary<int, int>();
        int SampleNum = 0;
        public ZeroMinMaxNormalizer(int featureDim)
            : base(featureDim)
        {
            FeaMin = new float[featureDim];
            FeaMax = new float[featureDim];
            FeaAvg = new float[featureDim];

            for (int i = 0; i < featureDim; i++)
            {
                FeaMin[i] = float.MaxValue;
                FeaMax[i] = float.MinValue;
                FeaAvg[i] = 0;
            }
        }

        public override void AnalysisEnd()
        {
            foreach (int fid in FeaDict.Keys)
            {
                if (FeaDict[fid] < SampleNum)
                {
                    if (FeaMin[fid] > 0)
                        FeaMin[fid] = 0;
                    if (FeaMax[fid] < 0)
                        FeaMax[fid] = 0;
                }
                FeaAvg[fid] = FeaAvg[fid] * 1.0f / SampleNum;
            }
        }

        public override NormalizerType Type
        {
            get
            {
                return NormalizerType.ZERO_MIN_MAX;
            }
        }

        public override void AnalysisBatch(SeqSparseBatchData input)
        {
            SampleNum += input.BatchSize;
            for (int sample = 0; sample < input.BatchSize; ++sample)
            {
                int seg_begin = sample >= 1 ? input.SampleIdx[sample - 1] : 0;
                int seg_end = input.SampleIdx[sample];

                for (int seg_id = seg_begin; seg_id < seg_end; ++seg_id)
                {
                    int fea_begin = seg_id >= 1 ? input.SequenceIdx[seg_id - 1] : 0;
                    int fea_end = input.SequenceIdx[seg_id];

                    for (int fea_id = fea_begin; fea_id < fea_end; ++fea_id)
                    {
                        int fea_key = input.FeaIdx[fea_id];
                        float fea_value = input.FeaValue[fea_id];
                        
                        if (FeaDict.ContainsKey(fea_key))
                            FeaDict[fea_key] += 1;
                        else
                            FeaDict[fea_key] = 1;

                        if (FeaMin[fea_key] > fea_value)
                            FeaMin[fea_key] = fea_value;
                        if (FeaMax[fea_key] < fea_value)
                            FeaMax[fea_key] = fea_value;
                        FeaAvg[fea_key] += fea_value;
                    }
                }
            }
        }

        public override void ProcessBatch(SeqSparseBatchData input)
        {
            for (int sample = 0; sample < input.BatchSize; ++sample)
            {
                int seg_begin = sample >= 1 ? input.SampleIdx[sample - 1] : 0;
                int seg_end = input.SampleIdx[sample];

                for (int seg_id = seg_begin; seg_id < seg_end; ++seg_id)
                {
                    int fea_begin = seg_id >= 1 ? input.SequenceIdx[seg_id - 1] : 0;
                    int fea_end = input.SequenceIdx[seg_id];

                    for (int fea_id = fea_begin; fea_id < fea_end; ++fea_id)
                    {
                        int fea_key = input.FeaIdx[fea_id];
                        float fea_value = input.FeaValue[fea_id];
                        
                        float new_fea_value = (fea_value - FeaAvg[fea_key]) / (FeaMax[fea_key] - FeaMin[fea_key] + float.Epsilon);
                        input.FeaValue[fea_id] = fea_value - FeaAvg[fea_key]; // new_fea_value;
                    }
                }
            }
        }
    }


}
