using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // Dynamic Computation Graph.
    // assume most of the data is defined as NdArrayData.
    public class NCCL 
    {
        int DeviceNum { get { return DeviceIds.Length; } }
        
        int[] DeviceIds { get; set; }

        IntPtr Comm { get; set; }

        public unsafe NCCL(int device_num) 
        {
            DeviceIds = new int[device_num];
            for(int i = 0; i < device_num; i++) DeviceIds[i] = i;

            fixed (int * pDevice = & DeviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, device_num);
            }
        }

        public void AllReduce(CudaPieceFloat data, int rank)
        {
            Cudalib.AllReduce(data.CudaPtr, data.Size, rank, Comm);
        }

        public void AllReduce(CudaPieceFloat[] grads, int device_num)
        {
            Parallel.For(0, device_num, i => 
            {
                Cudalib.CudaInit(i);
                Cudalib.AllReduce(grads[i].CudaPtr, grads[i].Size, i, Comm);
            });
        }

        public void AllReduceUpdate(Session[] sessions, CudaPieceFloat[] grads, int device_num)
        {
            Parallel.For(0, device_num, i => 
            {
                Cudalib.CudaInit(i);
                Cudalib.AllReduce(grads[i].CudaPtr, grads[i].Size, i, Comm);
                sessions[i].StepV3Update();
            });
        }

        public void Execute(Session[] sessions, CudaPieceFloat[] grads, int device_num)
        {
            Parallel.For(0, device_num, i => 
            {
                Cudalib.CudaInit(i);
                sessions[i].StepV3Run();
                Cudalib.AllReduce(grads[i].CudaPtr, grads[i].Size, i, Comm);
                sessions[i].StepV3Update();
            });
        }

        public void Predict(Session[] sessions, int device_num)
        {
            Parallel.For(0, device_num, i => 
            {
                Cudalib.CudaInit(i);
                sessions[i].StepV3Run();
            });
        }

        public void AllRun(Session[] sessions, int device_num)
        {
            Parallel.For(0, device_num, i => 
            {
                Cudalib.CudaInit(i);
                sessions[i].StepV3Run();
            });
        }

    }
}
