﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ActivationFuncLib
    {
        public static void ActivationFunc(IMathOperationManager computeLib, CudaPieceFloat output, int batchSize, int dim, A_Func Af, CudaPieceFloat bias)
        {
            switch (Af)
            {
                case A_Func.Tanh:
                    computeLib.Matrix_Add_Tanh(output, bias, batchSize, dim);
                    break;
                case A_Func.Linear:
                    computeLib.Matrix_Add_Linear(output, bias, batchSize, dim);
                    break;
                case A_Func.Rectified:
                    computeLib.Matrix_Add_Rectified(output, bias, batchSize, dim);
                    break;
                case A_Func.Sigmoid:
                    computeLib.Matrix_Add_Sigmoid(output, bias, batchSize, dim);
                    break;
            }
        }


        public static void DerivActivationFunc(IMathOperationManager computeLib, CudaPieceFloat deriv, CudaPieceFloat output, int batchSize, int dim, A_Func Af)
        {
            switch (Af)
            {
                case A_Func.Tanh:
                    computeLib.Deriv_Tanh(deriv, output, batchSize, dim);
                    break;
                case A_Func.Rectified:
                    computeLib.Deriv_Rectified(deriv, output, batchSize, dim);
                    break;
                case A_Func.Sigmoid:
                    computeLib.DerivLogistic(output, 0, deriv, 0, deriv, 0, batchSize * dim, 0, 1);
                    break;
            }
        }



        /// <summary>
        /// only support GPU.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="Bias"></param>
        /// <param name="Af"></param>
        /// <param name="batchSize"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="depth"></param>
        public static void Image_ActiveOutput(CudaPieceFloat output, CudaPieceFloat Bias, A_Func Af, int batchSize, int width, int height, int depth)
        {
            Cudalib.Matrix_Add_Image_FType(output.CudaPtr, Bias.CudaPtr, batchSize, width, height, depth, (int)Af);
        }

        /// <summary>
        /// Only support GPU.
        /// </summary>
        /// <param name="output"></param>
        /// <param name="deriv"></param>
        /// <param name="Af"></param>
        /// <param name="batchSize"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="depth"></param>
        public static void Image_DeactiveOutput(CudaPieceFloat output, CudaPieceFloat deriv, A_Func Af, int batchSize, int width, int height, int depth)
        {
            Cudalib.Deriv_Image_FType(deriv.CudaPtr, output.CudaPtr, batchSize, width, height, depth, (int)Af);
        }


    }
}
