﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class GRUDecodeComputeLib
    {
        /// <summary>
        /// Given InitO, InitC, and Words ,output newO and newC.
        /// </summary>
        /// <param name="Computelib"></param>
        /// <param name="?"></param>
        public static void GRNNForwardDecode(IMathOperationManager Computelib, int BatchSize, CudaPieceFloat Input, int InputDim,
            CudaPieceFloat InitO, CudaPieceInt LagInfo, GRUCell Model, CudaPieceFloat newO,
            CudaPieceFloat tmpO, IAttentionComputeLib attentLib, CudaPieceFloat C, int[] batchIdArray,
            CudaPieceFloat GateZ, CudaPieceFloat GateR, CudaPieceFloat rS, CudaPieceFloat sP)
        {
            Computelib.Matrix_AdditionMask(InitO, 0, LagInfo, 0,
                                           tmpO, 0, CudaPieceInt.Empty, 0,
                                           tmpO, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, BatchSize, 1, 0, 0);
            GRUComputeForwardLib.DenseForward(Computelib, tmpO, BatchSize, InputDim, Model, GateR, GateZ, sP);

            GRUComputeForwardLib.GRURecurrentForward(Computelib, BatchSize, tmpO, 0, Model, newO, 0, attentLib, C, 0, batchIdArray, 0, GateZ, GateR, rS, sP, 0);
        }
    }


    public class GRUComputeForwardLib
    {
        public static void DenseForward(IMathOperationManager Computelib, CudaPieceFloat SentOutput, int SentSize, int FeatureDim, GRUCell Model,
            CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat HHat)
        {
            /*Wi X -> GateI*/
            Computelib.Sgemm(SentOutput, 0, Model.Wr, 0, GateR, 0, SentSize, FeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(GateR, Model.Br, SentSize, Model.HiddenStateDim);

            /*Wc X -> MemoryC*/
            Computelib.Sgemm(SentOutput, 0, Model.Wz, 0, GateZ, 0, SentSize, FeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(GateZ, Model.Bz, SentSize, Model.HiddenStateDim);

            /*Wf X -> GateF*/
            Computelib.Sgemm(SentOutput, 0, Model.Wh, 0, HHat, 0, SentSize, FeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(HHat, Model.Bh, SentSize, Model.HiddenStateDim);
        }


        /// <summary>
        /// Given InitO, InitC, and Words ,output newO and newC.
        /// </summary>
        /// <param name="Computelib"></param>
        /// <param name="?"></param>
        public static void GRURecurrentForward(IMathOperationManager Computelib, int BatchSize,
            CudaPieceFloat InitO, int initOffset, GRUCell Model, CudaPieceFloat newO, int newOffset,
            IAttentionComputeLib attentLib, CudaPieceFloat C, int cOffset, int[] batchIdArray, int batchIdOffset,
            CudaPieceFloat GateZ, CudaPieceFloat GateR, CudaPieceFloat rS, CudaPieceFloat sP, int hopid = 0)
        {
            if (InitO != null && !InitO.IsEmpty)
            {
                Computelib.Sgemm(InitO, initOffset, Model.Ur, 0, GateR, newOffset, BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                Computelib.Sgemm(InitO, initOffset, Model.Uz, 0, GateZ, newOffset, BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            }

            if (attentLib != null && InitO != null && !InitO.IsEmpty)
            {
                // c = attention s(t-1). 
                attentLib.AttentionForward(InitO, initOffset, batchIdArray, batchIdOffset, C, cOffset, hopid, BatchSize);
                //attentLib.AttentionForward(tmpO, 0, batchIdArray, 0, C.Output.Data, 0, 0, BatchSize);
                Computelib.Sgemm(C, cOffset, Model.Cr, 0, GateR, newOffset, BatchSize, Model.AttentionDim, Model.HiddenStateDim, 1, 1, false, false);
                Computelib.Sgemm(C, cOffset, Model.Cz, 0, GateZ, newOffset, BatchSize, Model.AttentionDim, Model.HiddenStateDim, 1, 1, false, false);
                Computelib.Sgemm(C, cOffset, Model.Ch, 0, sP, newOffset, BatchSize, Model.AttentionDim, Model.HiddenStateDim, 1, 1, false, false);
            }
            else if(attentLib != null && C != null && !C.IsEmpty)
            {
                // C = 0;
                Computelib.Add_Vector(C, cOffset, C, cOffset, BatchSize * Model.AttentionDim, 0, 0);
            }
            //gate_r = sigmoid(gate_r)
            Computelib.Logistic(GateR, newOffset, GateR, newOffset, BatchSize * Model.HiddenStateDim, 1);
            //gate_z = sigmoid(gate_z)
            Computelib.Logistic(GateZ, newOffset, GateZ, newOffset, BatchSize * Model.HiddenStateDim, 1);


            if (InitO != null && !InitO.IsEmpty)
            {
                //rS = gate_r @ s(t-1)
                Computelib.ElementwiseProduct(GateR, newOffset, InitO, initOffset, rS, newOffset, BatchSize, Model.HiddenStateDim, 0);

                //sP += rS * Ux;
                Computelib.Sgemm(rS, newOffset, Model.Uh, 0, sP, newOffset, BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            }
            else
            {
                // rs = 0;
                Computelib.Add_Vector(rS, newOffset, rS, newOffset, BatchSize * Model.HiddenStateDim, 0, 0);
            }

            Computelib.Tanh(sP, newOffset, sP, newOffset, BatchSize * Model.HiddenStateDim);

            Computelib.ElementwiseProduct(sP, newOffset, GateZ, newOffset, newO, newOffset, BatchSize, Model.HiddenStateDim, 0);

            // s += (1-gate_z) @ s(t-1)
            if (InitO != null && !InitO.IsEmpty)
            {
                Computelib.Matrix_AdditionMask(InitO, initOffset, CudaPieceInt.Empty, 0,
                                               newO, newOffset, CudaPieceInt.Empty, 0,
                                               newO, newOffset, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, BatchSize, 1, 1, 0);
                Computelib.ElementwiseProductMask(GateZ, newOffset,
                                                  InitO, initOffset,
                                                  newO, newOffset,
                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  BatchSize, Model.HiddenStateDim, 1, -1);
            }
        }

        static void RecurrentForwardV2(IMathOperationManager Computelib, RecurrentSeqInfo RecurrentInfo, CudaPieceFloat InitO, bool isInit, GRUCell Model,
            CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat GateZ_N, CudaPieceFloat HHat, CudaPieceFloat RestH, CudaPieceFloat Output)
        {
            
            int preStartIdx = -1;
            for (int i = 0; i < RecurrentInfo.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    GRURecurrentForward(Computelib, Sum, Output, preStartIdx * Model.HiddenStateDim, Model, Output, startIdx * Model.HiddenStateDim, null, CudaPieceFloat.Empty, 0,
                        RecurrentInfo.LagSeqElement, startIdx, GateZ, GateR, RestH, HHat, i);
                }
                else if (isInit)
                {
                    GRURecurrentForward(Computelib, Sum, InitO, 0, Model, Output, 0, null, CudaPieceFloat.Empty, 0,
                        RecurrentInfo.LagSeqElement, 0, GateZ, GateR, RestH, HHat, i);
                }
                else
                {
                    GRURecurrentForward(Computelib, Sum, CudaPieceFloat.Empty, 0, Model, Output, 0, null, CudaPieceFloat.Empty, 0,
                        RecurrentInfo.LagSeqElement, 0, GateZ, GateR, RestH, HHat, i);
                }
                preStartIdx = startIdx;
            }

            GateZ_N.Init(1);
            Computelib.Matrix_AdditionMask(GateZ_N, 0, CudaPieceInt.Empty, 0,
                                           GateZ, 0, CudaPieceInt.Empty, 0,
                                           GateZ_N, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, RecurrentInfo.SentSize, 1, -1, 0);
        }

        static void RecurrentAttentionForwardv2(IMathOperationManager Computelib, RecurrentSeqInfo RecurrentInfo,
            CudaPieceFloat InitO, bool isInit, GRUCell Model, IAttentionComputeLib attentLib, CudaPieceFloat Z,
            CudaPieceFloat GateR, CudaPieceFloat GateZ, CudaPieceFloat GateZ_N, CudaPieceFloat HHat, CudaPieceFloat RestH, CudaPieceFloat Output)
        {
            int preStartIdx = -1;
            for (int i = 0; i < RecurrentInfo.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    GRURecurrentForward(Computelib, Sum, Output, preStartIdx * Model.HiddenStateDim, Model, Output, startIdx * Model.HiddenStateDim, attentLib, Z, startIdx * Model.AttentionDim,
                        RecurrentInfo.LagSeqElement, startIdx, GateZ, GateR, RestH, HHat, i);
                }
                else if (isInit)
                {
                    GRURecurrentForward(Computelib, Sum, InitO, 0, Model, Output, 0, attentLib, Z, 0,
                        RecurrentInfo.LagSeqElement, 0, GateZ, GateR, RestH, HHat, i);
                }
                else
                {
                    GRURecurrentForward(Computelib, Sum, CudaPieceFloat.Empty, 0, Model, Output, 0, attentLib, Z, 0, 
                        RecurrentInfo.LagSeqElement, 0, GateZ, GateR, RestH, HHat, i);
                }
                preStartIdx = startIdx;
            }

            GateZ_N.Init(1);
            Computelib.Matrix_AdditionMask(GateZ_N, 0, CudaPieceInt.Empty, 0,
                                           GateZ, 0, CudaPieceInt.Empty, 0,
                                           GateZ_N, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, RecurrentInfo.SentSize, 1, -1, 0);
        }

        public static void GRUForwardv2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, SeqDenseRecursiveData InitO, bool isInit, GRUCell Model,
            SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            DenseForward(Computelib, Input.SentOutput, Input.SentSize, Input.Dim, Model, GateR.SentOutput, GateZ.SentOutput, HHat.SentOutput);
            RecurrentForwardV2(Computelib, Input.RecurrentInfo, InitO.SentOutput, isInit, Model, GateR.SentOutput, GateZ.SentOutput, GateZ_N.SentOutput, HHat.SentOutput, RestH.SentOutput, Output.SentOutput);
        }


        public static void GRUForwardAttentionv2(IMathOperationManager Computelib, SeqDenseRecursiveData Input,
            SeqDenseRecursiveData InitO, bool isInit, GRUCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;
            DenseForward(Computelib, Input.SentOutput, Input.SentSize, Input.Dim, Model, GateR.SentOutput, GateZ.SentOutput, HHat.SentOutput);
            RecurrentAttentionForwardv2(Computelib, Input.RecurrentInfo, InitO.SentOutput, isInit, Model, attentLib, Z.SentOutput, GateR.SentOutput, GateZ.SentOutput, GateZ_N.SentOutput, HHat.SentOutput, RestH.SentOutput, Output.SentOutput);
        }
    }

    public class GRUComputeBackwardDataLib
    {
        public static void DenseBackwardData(IMathOperationManager Computelib, CudaPieceFloat SentDeriv, int SentSize, GRUCell Model,
            CudaPieceFloat GateRDeriv, CudaPieceFloat GateZDeriv, CudaPieceFloat HHatDeriv)
        {
            Computelib.Sgemm(GateRDeriv, 0, Model.Wr, 0, SentDeriv, 0, SentSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            Computelib.Sgemm(GateZDeriv, 0, Model.Wz, 0, SentDeriv, 0, SentSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            Computelib.Sgemm(HHatDeriv, 0, Model.Wh, 0, SentDeriv, 0, SentSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
        }

        public static void GRURecurrentBackwardData(IMathOperationManager Computelib, int BatchSize,
            CudaPieceFloat InitO, CudaPieceFloat InitODeriv, int initOffset, GRUCell Model, CudaPieceFloat newO, CudaPieceFloat newODeriv, int newOffset,
            IAttentionComputeLib attentLib, CudaPieceFloat C, CudaPieceFloat CDeriv, int cOffset, int[] batchIdArray, int batchIdOffset,
            CudaPieceFloat GateZ, CudaPieceFloat GateZDeriv, CudaPieceFloat GateZ_N, CudaPieceFloat GateR, CudaPieceFloat GateRDeriv,
            CudaPieceFloat rS, CudaPieceFloat rSDeriv, CudaPieceFloat sP, CudaPieceFloat sPDeriv, int hopid = 0)
        {

            // derivS --GateZ_N--> derivS_{i-1} 
            if (InitO != null && !InitO.IsEmpty)
            {
                Computelib.ElementwiseProduct(newODeriv, newOffset, GateZ_N, newOffset, InitODeriv, initOffset, BatchSize, Model.HiddenStateDim, 1);
            }

            // derivS --GateZ--> derivS_p
            Computelib.ElementwiseProduct(newODeriv, newOffset, GateZ, newOffset, sPDeriv, newOffset, BatchSize, Model.HiddenStateDim, 0);

            Computelib.DerivTanh(sP, newOffset, sPDeriv, newOffset, sPDeriv, newOffset, BatchSize * Model.HiddenStateDim, 0, 1);

            if (InitO != null && !InitO.IsEmpty)
            {
                Computelib.Sgemm(sPDeriv, newOffset, Model.Uh, 0, rSDeriv, newOffset, BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);
                Computelib.ElementwiseProduct(rSDeriv, newOffset, InitO, initOffset, GateRDeriv, newOffset, BatchSize, Model.HiddenStateDim, 0);
                Computelib.ElementwiseProduct(rSDeriv, newOffset, GateR, newOffset, InitODeriv, initOffset, BatchSize, Model.HiddenStateDim, 1);

                Computelib.DerivLogistic(GateR, newOffset, GateRDeriv, newOffset, GateRDeriv, newOffset, BatchSize * Model.HiddenStateDim, 0, 1);
                Computelib.Sgemm(GateRDeriv, newOffset, Model.Ur, 0, InitODeriv, initOffset, BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
            }
            else
            {
                Computelib.Add_Vector(GateRDeriv, newOffset, GateRDeriv, newOffset, BatchSize * Model.HiddenStateDim, 0, 0);
            }

            if (InitO != null && !InitO.IsEmpty)
            {
                Computelib.ElementwiseProduct(newODeriv, newOffset, InitO, initOffset, GateZDeriv, newOffset, BatchSize, Model.HiddenStateDim, 0);
                Computelib.ElementwiseProduct(newODeriv, newOffset, sP, newOffset, GateZDeriv, newOffset, BatchSize, Model.HiddenStateDim, -1);
            }
            else
            {
                Computelib.ElementwiseProduct(newODeriv, newOffset, sP, newOffset, GateZDeriv, newOffset, BatchSize, Model.HiddenStateDim, 0);
            }

            Computelib.DerivLogistic(GateZ, newOffset, GateZDeriv, newOffset, GateZDeriv, newOffset, BatchSize * Model.HiddenStateDim, 0, 1);

            if (InitO != null && !InitO.IsEmpty)
            {
                Computelib.Sgemm(GateZDeriv, newOffset, Model.Uz, 0, InitODeriv, initOffset, BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
            }

            if (attentLib != null)
            {
                Computelib.Sgemm(GateRDeriv, newOffset, Model.Cr, 0, CDeriv, cOffset, BatchSize, Model.HiddenStateDim, Model.AttentionDim, 0, 1, false, true);

                Computelib.Sgemm(GateZDeriv, newOffset, Model.Cz, 0, CDeriv, cOffset, BatchSize, Model.HiddenStateDim, Model.AttentionDim, 1, 1, false, true);

                Computelib.Sgemm(sPDeriv, newOffset, Model.Ch, 0, CDeriv, cOffset, BatchSize, Model.HiddenStateDim, Model.AttentionDim, 1, 1, false, true);

                if (InitO != null && !InitO.IsEmpty)
                {
                    attentLib.AttentionBackwardData(InitODeriv, initOffset, //CudaPieceInt.Empty, 0, 
                        batchIdArray, batchIdOffset, CDeriv, cOffset, hopid, BatchSize);
                        //RecurrentInfo.LagSeqElement, startIdx, Z.SentDeriv, startIdx * Model.AttentionDim, i, Sum);
                }
                //if (i > 0)
                //{
                //    attentLib.AttentionBackwardData(Output.SentDeriv, preStartIdx * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                //        RecurrentInfo.LagSeqElement, startIdx, Z.SentDeriv, startIdx * Model.AttentionDim, i, Sum);
                //}
                //else if (isInit)
                //{
                //    attentLib.AttentionBackwardData(Lag1Seq.SentDeriv, 0, CudaPieceInt.Empty, 0, RecurrentInfo.LagSeqElement, 0, Z.SentDeriv, 0, i, Sum);
                //}
            }
        }


        //public static void RecurrentBackwardDatav2(IMathOperationManager Computelib, 
        //    SeqDenseRecursiveData Lag1Seq, bool isInit, RecurrentSeqInfo RecurrentInfo,
        //    GRUCell Model, SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        //{
        //    Computelib.Zero(GateZ.SentDeriv, RecurrentInfo.SentSize * Model.HiddenStateDim);

        //    for (int i = RecurrentInfo.MaxLag - 1; i >= 0; i--)
        //    {
        //        int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
        //        int preStartIdx = i < 2 ? 0 : RecurrentInfo.LagSeqIdx[i - 2];
        //        int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
        //        if (Sum == 0) break;

        //        if (i > 0)
        //        {
        //            Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          GateZ_N.SentOutput, startIdx * Model.HiddenStateDim,
        //                                          Output.SentDeriv, preStartIdx * Model.HiddenStateDim,
        //                                          Sum, Model.HiddenStateDim, 1);
        //        }
        //        else if(isInit)
        //        {
        //            Computelib.ElementwiseProduct(Output.SentDeriv, 0,
        //                                          GateZ_N.SentOutput, 0,
        //                                          Lag1Seq.SentDeriv, 0,
        //                                          Sum, Model.HiddenStateDim, 1);
        //        }

        //        Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      GateZ.SentOutput, startIdx * Model.HiddenStateDim,
        //                                      HHat.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      Sum, Model.HiddenStateDim, 0);

        //        Computelib.DerivTanh(HHat.SentOutput, startIdx * Model.HiddenStateDim,
        //                                      HHat.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      HHat.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      Sum * Model.HiddenStateDim, 0, 1);

        //        Computelib.Sgemm(HHat.SentDeriv, startIdx * Model.HiddenStateDim, Model.Uh, 0,
        //                         RestH.SentDeriv, startIdx * Model.HiddenStateDim, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

        //        if (i > 0)
        //        {
        //            Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          Output.SentOutput, preStartIdx * Model.HiddenStateDim,
        //                                          GateZ.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          Sum, Model.HiddenStateDim, 0);
        //        }
        //        else if (isInit)
        //        {
        //            Computelib.ElementwiseProduct(Output.SentDeriv, 0,
        //                                          Lag1Seq.SentOutput, 0,
        //                                          GateZ.SentDeriv, 0,
        //                                          Sum, Model.HiddenStateDim, 0);
        //        }
        //        Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      HHat.SentOutput, startIdx * Model.HiddenStateDim,
        //                                      GateZ.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      Sum, Model.HiddenStateDim, -1);

        //        if (i > 0)
        //        {
        //            Computelib.ElementwiseProduct(RestH.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          Output.SentOutput, preStartIdx * Model.HiddenStateDim,
        //                                          GateR.SentDeriv, startIdx * Model.HiddenStateDim, 
        //                                          Sum, Model.HiddenStateDim, 0);

        //            Computelib.ElementwiseProduct(RestH.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          GateR.SentOutput, startIdx * Model.HiddenStateDim,
        //                                          Output.SentDeriv, preStartIdx * Model.HiddenStateDim,
        //                                          Sum, Model.HiddenStateDim, 1);
        //        }
        //        else if(isInit)
        //        {
        //            Computelib.ElementwiseProduct(RestH.SentDeriv, 0,
        //                                          Lag1Seq.SentOutput, 0,
        //                                          GateR.SentDeriv, 0,
        //                                          Sum, Model.HiddenStateDim, 0);

        //            Computelib.ElementwiseProduct(RestH.SentDeriv, 0,
        //                                          GateR.SentOutput, 0,
        //                                          Lag1Seq.SentDeriv, 0,
        //                                          Sum, Model.HiddenStateDim, 1);
        //        }

        //        Computelib.DerivLogistic(GateR.SentOutput, startIdx * Model.HiddenStateDim, GateR.SentDeriv, startIdx * Model.HiddenStateDim, 
        //            GateR.SentDeriv, startIdx * Model.HiddenStateDim, Sum * Model.HiddenStateDim, 0, 1);

        //        Computelib.DerivLogistic(GateZ.SentOutput, startIdx * Model.HiddenStateDim, GateZ.SentDeriv, startIdx * Model.HiddenStateDim, 
        //            GateZ.SentDeriv, startIdx * Model.HiddenStateDim, Sum * Model.HiddenStateDim, 0, 1);

        //        if (i > 0)
        //        {
        //            Computelib.Sgemm(GateR.SentDeriv, startIdx * Model.HiddenStateDim, Model.Ur, 0, Output.SentDeriv, preStartIdx * Model.HiddenStateDim, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
        //            Computelib.Sgemm(GateZ.SentDeriv, startIdx * Model.HiddenStateDim, Model.Uz, 0, Output.SentDeriv, preStartIdx * Model.HiddenStateDim, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
        //        }
        //        else if(isInit)
        //        {
        //            Computelib.Sgemm(GateR.SentDeriv, startIdx * Model.HiddenStateDim, Model.Ur, 0, Lag1Seq.SentDeriv, 0, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
        //            Computelib.Sgemm(GateZ.SentDeriv, startIdx * Model.HiddenStateDim, Model.Uz, 0, Lag1Seq.SentDeriv, 0, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
        //        }

        //    }
        //}

        public static void RecurrentBackwardDatav2(IMathOperationManager Computelib, SeqDenseRecursiveData Lag1Seq, bool isInit, RecurrentSeqInfo RecurrentInfo,
                GRUCell Model, SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            for (int i = RecurrentInfo.MaxLag - 1; i >= 0; i--)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int preStartIdx = i < 2 ? 0 : RecurrentInfo.LagSeqIdx[i - 2];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    GRURecurrentBackwardData(Computelib, Sum, Output.SentOutput, Output.SentDeriv, preStartIdx * Model.HiddenStateDim, Model,
                                                              Output.SentOutput, Output.SentDeriv, startIdx * Model.HiddenStateDim, null, CudaPieceFloat.Empty, CudaPieceFloat.Empty, 0,
                                                              RecurrentInfo.LagSeqElement, startIdx, GateZ.SentOutput, GateZ.SentDeriv, GateZ_N.SentOutput, GateR.SentOutput, GateR.SentDeriv,
                                                              RestH.SentOutput, RestH.SentDeriv, HHat.SentOutput, HHat.SentDeriv, i);
                }
                else if (isInit)
                {
                    GRURecurrentBackwardData(Computelib, Sum, Lag1Seq.SentOutput, Lag1Seq.SentDeriv, 0, Model,
                                                              Output.SentOutput, Output.SentDeriv, 0, null, CudaPieceFloat.Empty, CudaPieceFloat.Empty, 0,
                                                              RecurrentInfo.LagSeqElement, 0, GateZ.SentOutput, GateZ.SentDeriv, GateZ_N.SentOutput, GateR.SentOutput, GateR.SentDeriv,
                                                              RestH.SentOutput, RestH.SentDeriv, HHat.SentOutput, HHat.SentDeriv, i);
                }
                else
                {
                    GRURecurrentBackwardData(Computelib, Sum, CudaPieceFloat.Empty, CudaPieceFloat.Empty, 0, Model,
                                                              Output.SentOutput, Output.SentDeriv, 0, null, CudaPieceFloat.Empty, CudaPieceFloat.Empty, 0,
                                                              RecurrentInfo.LagSeqElement, 0, GateZ.SentOutput, GateZ.SentDeriv, GateZ_N.SentOutput, GateR.SentOutput, GateR.SentDeriv,
                                                              RestH.SentOutput, RestH.SentDeriv, HHat.SentOutput, HHat.SentDeriv, i);
                }
            }
        }

        public static void RecurrentAttentionBackwardDatav2(IMathOperationManager Computelib, SeqDenseRecursiveData Lag1Seq, bool isInit, RecurrentSeqInfo RecurrentInfo, GRUCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            for (int i = RecurrentInfo.MaxLag - 1; i >= 0; i--)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int preStartIdx = i < 2 ? 0 : RecurrentInfo.LagSeqIdx[i - 2];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                if (i > 0)
                {
                    GRURecurrentBackwardData(Computelib, Sum, Output.SentOutput, Output.SentDeriv, preStartIdx * Model.HiddenStateDim, Model,
                                                              Output.SentOutput, Output.SentDeriv, startIdx * Model.HiddenStateDim,
                                                              attentLib, Z.SentOutput, Z.SentDeriv, startIdx * Model.AttentionDim,
                                                              RecurrentInfo.LagSeqElement, startIdx, GateZ.SentOutput, GateZ.SentDeriv, GateZ_N.SentOutput, GateR.SentOutput, GateR.SentDeriv,
                                                              RestH.SentOutput, RestH.SentDeriv, HHat.SentOutput, HHat.SentDeriv, i);
                }
                else if (isInit)
                {
                    GRURecurrentBackwardData(Computelib, Sum, Lag1Seq.SentOutput, Lag1Seq.SentDeriv, 0, Model,
                                                              Output.SentOutput, Output.SentDeriv, 0, 
                                                              attentLib, Z.SentOutput, Z.SentDeriv, 0,
                                                              RecurrentInfo.LagSeqElement, 0, GateZ.SentOutput, GateZ.SentDeriv, GateZ_N.SentOutput, GateR.SentOutput, GateR.SentDeriv,
                                                              RestH.SentOutput, RestH.SentDeriv, HHat.SentOutput, HHat.SentDeriv, i);
                }
                else
                {
                    GRURecurrentBackwardData(Computelib, Sum, CudaPieceFloat.Empty, CudaPieceFloat.Empty, 0, Model,
                                                              Output.SentOutput, Output.SentDeriv, 0,
                                                              attentLib, Z.SentOutput, Z.SentDeriv, 0,
                                                              RecurrentInfo.LagSeqElement, 0, GateZ.SentOutput, GateZ.SentDeriv, GateZ_N.SentOutput, GateR.SentOutput, GateR.SentDeriv,
                                                              RestH.SentOutput, RestH.SentDeriv, HHat.SentOutput, HHat.SentDeriv, i);
                }
            }
        }

        //public static void RecurrentAttentionBackwardDatav2(IMathOperationManager Computelib, SeqDenseRecursiveData Lag1Seq, bool isInit, RecurrentSeqInfo RecurrentInfo, GRUCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
        //    SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        //{
        //    Computelib.Zero(GateZ.SentDeriv, RecurrentInfo.SentSize * Model.HiddenStateDim);

        //    for (int i = RecurrentInfo.MaxLag - 1; i >= 0; i--)
        //    {
        //        int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
        //        int preStartIdx = i < 2 ? 0 : RecurrentInfo.LagSeqIdx[i - 2];
        //        int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
        //        if (Sum == 0) break;

        //        if (i > 0)
        //        {
        //            Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          GateZ_N.SentOutput, startIdx * Model.HiddenStateDim,
        //                                          Output.SentDeriv, preStartIdx * Model.HiddenStateDim,
        //                                          Sum, Model.HiddenStateDim, 1);
        //        }
        //        else if (isInit)
        //        {
        //            Computelib.ElementwiseProduct(Output.SentDeriv, 0,
        //                                          GateZ_N.SentOutput, 0,
        //                                          Lag1Seq.SentDeriv, 0,
        //                                          Sum, Model.HiddenStateDim, 1);
        //        }

        //        Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      GateZ.SentOutput, startIdx * Model.HiddenStateDim,
        //                                      HHat.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      Sum, Model.HiddenStateDim, 0);

        //        Computelib.DerivTanh(HHat.SentOutput, startIdx * Model.HiddenStateDim,
        //                                      HHat.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      HHat.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      Sum * Model.HiddenStateDim, 0, 1);

        //        Computelib.Sgemm(HHat.SentDeriv, startIdx * Model.HiddenStateDim, Model.Uh, 0,
        //                         RestH.SentDeriv, startIdx * Model.HiddenStateDim, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

        //        if (i > 0)
        //        {
        //            Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          Output.SentOutput, preStartIdx * Model.HiddenStateDim,
        //                                          GateZ.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          Sum, Model.HiddenStateDim, 0);
        //        }
        //        else if (isInit)
        //        {
        //            Computelib.ElementwiseProduct(Output.SentDeriv, 0,
        //                                          Lag1Seq.SentOutput, 0,
        //                                          GateZ.SentDeriv, 0,
        //                                          Sum, Model.HiddenStateDim, 0);
        //        }
        //        Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      HHat.SentOutput, startIdx * Model.HiddenStateDim,
        //                                      GateZ.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                      Sum, Model.HiddenStateDim, -1);

        //        if (i > 0)
        //        {
        //            Computelib.ElementwiseProduct(RestH.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          Output.SentOutput, preStartIdx * Model.HiddenStateDim,
        //                                          GateR.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          Sum, Model.HiddenStateDim, 0);

        //            Computelib.ElementwiseProduct(RestH.SentDeriv, startIdx * Model.HiddenStateDim,
        //                                          GateR.SentOutput, startIdx * Model.HiddenStateDim,
        //                                          Output.SentDeriv, preStartIdx * Model.HiddenStateDim,
        //                                          Sum, Model.HiddenStateDim, 1);
        //        }
        //        else if (isInit)
        //        {
        //            Computelib.ElementwiseProduct(RestH.SentDeriv, 0,
        //                                          Lag1Seq.SentOutput, 0,
        //                                          GateR.SentDeriv, 0,
        //                                          Sum, Model.HiddenStateDim, 0);

        //            Computelib.ElementwiseProduct(RestH.SentDeriv, 0,
        //                                          GateR.SentOutput, 0,
        //                                          Lag1Seq.SentDeriv, 0,
        //                                          Sum, Model.HiddenStateDim, 1);
        //        }

        //        Computelib.DerivLogistic(GateR.SentOutput, startIdx * Model.HiddenStateDim, GateR.SentDeriv, startIdx * Model.HiddenStateDim,
        //            GateR.SentDeriv, startIdx * Model.HiddenStateDim, Sum * Model.HiddenStateDim, 0, 1);

        //        Computelib.DerivLogistic(GateZ.SentOutput, startIdx * Model.HiddenStateDim, GateZ.SentDeriv, startIdx * Model.HiddenStateDim,
        //            GateZ.SentDeriv, startIdx * Model.HiddenStateDim, Sum * Model.HiddenStateDim, 0, 1);

        //        if (i > 0)
        //        {
        //            Computelib.Sgemm(GateR.SentDeriv, startIdx * Model.HiddenStateDim, Model.Ur, 0, Output.SentDeriv, preStartIdx * Model.HiddenStateDim, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
        //            Computelib.Sgemm(GateZ.SentDeriv, startIdx * Model.HiddenStateDim, Model.Uz, 0, Output.SentDeriv, preStartIdx * Model.HiddenStateDim, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
        //        }
        //        else if (isInit)
        //        {
        //            Computelib.Sgemm(GateR.SentDeriv, 0, Model.Ur, 0, Lag1Seq.SentDeriv, 0, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
        //            Computelib.Sgemm(GateZ.SentDeriv, 0, Model.Uz, 0, Lag1Seq.SentDeriv, 0, Sum, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
        //        }

        //        Computelib.Sgemm(GateR.SentDeriv, startIdx * Model.HiddenStateDim,
        //                         Model.Cr, 0,
        //                         Z.SentDeriv, startIdx * Model.AttentionDim,
        //                         Sum, Model.HiddenStateDim, Model.AttentionDim,
        //                         0, 1, false, true);

        //        Computelib.Sgemm(GateZ.SentDeriv, startIdx * Model.HiddenStateDim,
        //                         Model.Cz, 0,
        //                         Z.SentDeriv, startIdx * Model.AttentionDim,
        //                         Sum, Model.HiddenStateDim, Model.AttentionDim,
        //                         1, 1, false, true);

        //        Computelib.Sgemm(HHat.SentDeriv, startIdx * Model.HiddenStateDim,
        //                           Model.Ch, 0,
        //                           Z.SentDeriv, startIdx * Model.AttentionDim,
        //                           Sum, Model.HiddenStateDim, Model.AttentionDim,
        //                           1, 1, false, true);

        //        if (i > 0)
        //        {
        //            attentLib.AttentionBackwardData(Output.SentDeriv, preStartIdx * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
        //                RecurrentInfo.LagSeqElement, startIdx, Z.SentDeriv, startIdx * Model.AttentionDim, i, Sum);
        //        }
        //        else if (isInit)
        //        {
        //            attentLib.AttentionBackwardData(Lag1Seq.SentDeriv, 0, CudaPieceInt.Empty, 0, RecurrentInfo.LagSeqElement, 0, Z.SentDeriv, 0, i, Sum);
        //        }
        //    }
        //}

        public static void GRUBackwardDatav2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, SeqDenseRecursiveData Lag1Seq, bool isInit, GRUCell Model,
             SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            RecurrentBackwardDatav2(Computelib, Lag1Seq, isInit, Input.RecurrentInfo, Model, GateR, GateZ, GateZ_N, HHat, RestH, Output);

            DenseBackwardData(Computelib, Input.SentDeriv, Input.SentSize, Model, GateR.SentDeriv, GateZ.SentDeriv, HHat.SentDeriv);
        }

        public static void GRUAttentionBackwardDatav2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, SeqDenseRecursiveData Lag1Seq, bool isInit, GRUCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            RecurrentAttentionBackwardDatav2(Computelib, Lag1Seq, isInit, Input.RecurrentInfo, Model, attentLib, Z, GateR, GateZ, GateZ_N, HHat, RestH, Output);

            DenseBackwardData(Computelib, Input.SentDeriv, Input.SentSize, Model, GateR.SentDeriv, GateZ.SentDeriv, HHat.SentDeriv);
        }
    }


    public class GRUComputeBackwardWeightLib
    {
        public static void DenseBackwardWeight(IMathOperationManager Computelib, CudaPieceFloat SentInput, int SentSize, GRUCell Model, SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData HHat)
        {
            Computelib.Sgemm(SentInput, 0, HHat.SentDeriv, 0, Model.WhGrad, 0, SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, 1, true, false);
            Computelib.Sgemm(SentInput, 0, GateR.SentDeriv, 0, Model.WrGrad, 0, SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, 1, true, false);
            Computelib.Sgemm(SentInput, 0, GateZ.SentDeriv, 0, Model.WzGrad, 0, SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, 1, true, false);

            Computelib.ColumnWiseSum(HHat.SentDeriv, Model.BhGrad, SentSize, Model.HiddenStateDim, 1);
            Computelib.ColumnWiseSum(GateR.SentDeriv, Model.BrGrad, SentSize, Model.HiddenStateDim, 1);
            Computelib.ColumnWiseSum(GateZ.SentDeriv, Model.BzGrad, SentSize, Model.HiddenStateDim, 1);
        }


        public static void RecurrentBackwardWeightv2(IMathOperationManager Computelib, SeqDenseRecursiveData Lag1Seq, bool isInit, RecurrentSeqInfo RecurrentInfo, GRUCell Model,
            SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            /// skip the first time step.
            int offset = RecurrentInfo.BatchSize * Model.HiddenStateDim;
            int seqSize = RecurrentInfo.SentSize - RecurrentInfo.BatchSize;

            /// add the first time step.
            if (isInit)
            {
                offset = 0;
                seqSize = RecurrentInfo.SentSize;
            }

            Computelib.Sgemm(RestH.SentOutput, offset, HHat.SentDeriv, offset,
                             Model.UhGrad, 0,
                             seqSize, Model.HiddenStateDim, Model.HiddenStateDim,
                             1, 1, true, false);

            Computelib.Sgemm(Lag1Seq.SentOutput, offset, GateR.SentDeriv, offset,
                             Model.UrGrad, 0,
                             seqSize, Model.HiddenStateDim, Model.HiddenStateDim,
                             1, 1, true, false);

            Computelib.Sgemm(Lag1Seq.SentOutput, offset, GateZ.SentDeriv, offset,
                             Model.UzGrad, 0,
                             seqSize, Model.HiddenStateDim, Model.HiddenStateDim,
                             1, 1, true, false);
        }

        public static void RecurrentAttentionBackwardWeightv2(IMathOperationManager Computelib, // CudaPieceInt SampleIdx, int BatchSize, int SentSize,
            SeqDenseBatchData Lag1Seq, bool isInit, RecurrentSeqInfo RecurrentInfo, GRUCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            /// skip the first time step.
            int offset = RecurrentInfo.BatchSize * Model.HiddenStateDim;
            int seqSize = RecurrentInfo.SentSize - RecurrentInfo.BatchSize;

            /// add the first time step.
            if (isInit)
            {
                offset = 0;
                seqSize = RecurrentInfo.SentSize;
            }

            Computelib.Sgemm(RestH.SentOutput, offset, HHat.SentDeriv, offset,
                             Model.UhGrad, 0,
                             seqSize, Model.HiddenStateDim, Model.HiddenStateDim,
                             1, 1, true, false);

            Computelib.Sgemm(Lag1Seq.SentOutput, offset, GateR.SentDeriv, offset,
                             Model.UrGrad, 0,
                             seqSize, Model.HiddenStateDim, Model.HiddenStateDim,
                             1, 1, true, false);

            Computelib.Sgemm(Lag1Seq.SentOutput, offset, GateZ.SentDeriv, offset,
                             Model.UzGrad, 0,
                             seqSize, Model.HiddenStateDim, Model.HiddenStateDim,
                             1, 1, true, false);

            Computelib.Sgemm(Z.SentOutput, 0, 
                             GateR.SentDeriv, 0, 
                             Model.CrGrad, 0,
                             RecurrentInfo.SentSize, Model.AttentionDim, Model.HiddenStateDim,
                             1, 1, true, false);

            Computelib.Sgemm(Z.SentOutput, 0,
                             GateZ.SentDeriv, 0,
                             Model.CzGrad, 0,
                             RecurrentInfo.SentSize, Model.AttentionDim, Model.HiddenStateDim,
                             1, 1, true, false);

            Computelib.Sgemm(Z.SentOutput, 0, 
                             HHat.SentDeriv, 0,
                             Model.ChGrad, 0,
                             RecurrentInfo.SentSize, Model.AttentionDim, Model.HiddenStateDim,
                             1, 1, true, false);

            attentLib.AttentionBackwardWeight(Lag1Seq.SentOutput, Output.SentOutput, RecurrentInfo, Output.Dim, Z.SentDeriv, Model.AttentionDim);
        }

        public static void GRUBackwardWeightv2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, SeqDenseRecursiveData Lag1Seq, bool isInit, GRUCell Model,
            SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            DenseBackwardWeight(Computelib, Input.SentOutput, Input.SentSize, Model, GateR, GateZ, HHat);
            RecurrentBackwardWeightv2(Computelib, Lag1Seq, isInit, Input.RecurrentInfo, Model, GateR, GateZ, GateZ_N, HHat, RestH, Output);
        }

        public static void GRUAttentionBackwardWeightv2(IMathOperationManager Computelib, SeqDenseRecursiveData Input,
            SeqDenseRecursiveData Lag1Seq, bool isInit, GRUCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseRecursiveData GateR, SeqDenseRecursiveData GateZ, SeqDenseRecursiveData GateZ_N, SeqDenseRecursiveData HHat, SeqDenseRecursiveData RestH, SeqDenseRecursiveData Output)
        {
            DenseBackwardWeight(Computelib, Input.SentOutput, Input.SentSize, Model, GateR, GateZ, HHat);
            RecurrentAttentionBackwardWeightv2(Computelib, Lag1Seq, isInit, Input.RecurrentInfo, Model, attentLib, Z, GateR, GateZ, GateZ_N, HHat, RestH, Output);
        }

    }
}
