﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LSTMComputeForwardLib
    {
        
        public static void RecurrentForwardV2(IMathOperationManager Computelib, RecurrentSeqInfo RecurrentInfo, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            CudaPieceFloat GateO, CudaPieceFloat GateI, CudaPieceFloat CHat, CudaPieceFloat GateF, CudaPieceFloat C, CudaPieceFloat TanhC,  CudaPieceFloat Output)
        {
            int preStartIdx = -1;
            for (int i = 0; i < RecurrentInfo.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;
                if (i > 0)
                {
                    /// obtain output(t-1)
                    //gate_i += U_{i} * o_{t-1}
                    Computelib.Sgemm(Output, preStartIdx * Model.CellDim, Model.Ui, 0, GateI, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                    //gate_f += U_{f} * o_{t-1}
                    Computelib.Sgemm(Output, preStartIdx * Model.CellDim, Model.Uf, 0, GateF, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                    //c_hat += U_{c} * o_{t-1}
                    Computelib.Sgemm(Output, preStartIdx * Model.CellDim, Model.Uc, 0, CHat, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                    //gate_o += U_{o} * o_{t-1}
                    Computelib.Sgemm(Output, preStartIdx * Model.CellDim, Model.Uo, 0, GateO, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
                }
                else if (InitO != null)
                {
                    Computelib.Sgemm(InitO.Output.Data, 0, Model.Ui, 0, GateI, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
                    Computelib.Sgemm(InitO.Output.Data, 0, Model.Uf, 0, GateF, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
                    Computelib.Sgemm(InitO.Output.Data, 0, Model.Uc, 0, CHat, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
                    Computelib.Sgemm(InitO.Output.Data, 0, Model.Uo, 0, GateO, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
                }

                //gate_i = sigmoid(gate_i)
                Computelib.Logistic(GateI, startIdx * Model.CellDim, GateI, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

                //c_hat = tanh(c_hat)
                Computelib.Tanh(CHat, startIdx * Model.CellDim, CHat, startIdx * Model.CellDim, Sum * Model.CellDim);

                //c = gate_i @ c_hat
                Computelib.ElementwiseProduct(GateI, startIdx * Model.CellDim, CHat, startIdx * Model.CellDim, C, startIdx * Model.CellDim, Sum, Model.CellDim, 0);

                //gate_f = sigmoid(gate_f)
                Computelib.Logistic(GateF, startIdx * Model.CellDim, GateF, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

                if (i > 0)
                {
                    // c(t) = c(t) + gate_f(t) @ c(t-1)
                    Computelib.ElementwiseProductMask(C, preStartIdx * Model.CellDim, GateF, startIdx * Model.CellDim,
                            C, startIdx * Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Sum, Model.CellDim, 1, 1);
                }
                else if (InitC != null)
                {
                    Computelib.ElementwiseProductMask(InitC.Output.Data, 0, GateF, 0,
                            C, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Sum, Model.CellDim, 1, 1);
                }

                //gate_o += Vo * c(t)
                Computelib.Sgemm(C, startIdx * Model.CellDim, Model.Vo, 0, GateO, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                //gate_o = simgoid(gate_o)
                Computelib.Logistic(GateO, startIdx * Model.CellDim, GateO, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

                //tanhc = tanh(c)
                Computelib.Tanh(C, startIdx * Model.CellDim, TanhC, startIdx * Model.CellDim, Sum * Model.CellDim);

                //o = gate_o @ tanhc
                Computelib.ElementwiseProduct(GateO, startIdx * Model.CellDim, TanhC, startIdx * Model.CellDim,
                    Output, startIdx * Model.CellDim, Sum, Model.CellDim, 0);

                preStartIdx = startIdx;
            }
        }

        //public static void SparseForward(IMathOperationManager Computelib, CudaPieceInt SequenceIdx, CudaPieceInt FeaIdx, CudaPieceFloat FeaValue, int SentSize, int FeatureDim, SeqHelpData HelpInput, LSTMCell Model,
        //    SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseBatchData C, SeqDenseBatchData TanhC,
        //    SeqDenseBatchData Output)
        //{
        //    /*Wi X -> GateI*/
        //    Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue, Model.Wi, 0, GateI.SentOutput, 0, SentSize,
        //            FeatureDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.TransSeqIndex, 0, 0, 1, false, false);
        //    Computelib.Matrix_Add_Linear(GateI.SentOutput, Model.Bi, SentSize, Model.CellDim);

        //    /*Wc X -> MemoryC*/
        //    Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue, Model.Wc, 0, CHat.SentOutput, 0, SentSize,
        //        FeatureDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.TransSeqIndex, 0, 0, 1, false, false);
        //    Computelib.Matrix_Add_Linear(CHat.SentOutput, Model.Bc, SentSize, Model.CellDim);

        //    /*Wf X -> GateF*/
        //    Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue, Model.Wf, 0, GateF.SentOutput, 0, SentSize,
        //        FeatureDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.TransSeqIndex, 0, 0, 1, false, false);
        //    Computelib.Matrix_Add_Linear(GateF.SentOutput, Model.Bf, SentSize, Model.CellDim);

        //    /*Wo x -> GateO*/
        //    Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue, Model.Wo, 0, GateO.SentOutput, 0, SentSize,
        //        FeatureDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.TransSeqIndex, 0, 0, 1, false, false);
        //    Computelib.Matrix_Add_Linear(GateO.SentOutput, Model.Bo, SentSize, Model.CellDim);
        //}

        public static void SparseForwardv2(IMathOperationManager Computelib, CudaPieceInt SequenceIdx, CudaPieceInt FeaIdx, CudaPieceFloat FeaValue, int SentSize, int FeatureDim, RecurrentSeqInfo RecurrentInfo, LSTMCell Model,
            CudaPieceFloat GateO, CudaPieceFloat GateI, CudaPieceFloat CHat, CudaPieceFloat GateF)
        {
            //SequenceIdx.SyncToCPU();
            //FeaIdx.SyncToCPU();
            //FeaValue.SyncToCPU();
            //Model.Wi.SyncToCPU();
            /*Wi X -> GateI*/
            Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue, Model.Wi, 0, GateI, 0, SentSize,
                    FeatureDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, RecurrentInfo.MapForward, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(GateI, Model.Bi, SentSize, Model.CellDim);

            //GateI.SyncToCPU();

            /*Wc X -> MemoryC*/
            Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue, Model.Wc, 0, CHat, 0, SentSize,
                FeatureDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, RecurrentInfo.MapForward, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(CHat, Model.Bc, SentSize, Model.CellDim);

            /*Wf X -> GateF*/
            Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue, Model.Wf, 0, GateF, 0, SentSize,
                FeatureDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, RecurrentInfo.MapForward, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(GateF, Model.Bf, SentSize, Model.CellDim);
            //Model.Bf.SyncToCPU();

            /*Wo x -> GateO*/
            Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue, Model.Wo, 0, GateO, 0, SentSize,
                FeatureDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, RecurrentInfo.MapForward, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(GateO, Model.Bo, SentSize, Model.CellDim);
        }

        public static void DenseForward(IMathOperationManager Computelib, CudaPieceFloat SentOutput, int SentSize, int FeatureDim, LSTMCell Model,
            CudaPieceFloat GateO, CudaPieceFloat GateI, CudaPieceFloat CHat, CudaPieceFloat GateF, CudaPieceFloat C, CudaPieceFloat TanhC, CudaPieceFloat Output)
        {
            /*Wi X -> GateI*/
            Computelib.Sgemm(SentOutput, 0, Model.Wi, 0, GateI, 0, SentSize, FeatureDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(GateI, Model.Bi, SentSize, Model.CellDim);

            /*Wc X -> MemoryC*/
            Computelib.Sgemm(SentOutput, 0, Model.Wc, 0, CHat, 0, SentSize, FeatureDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(CHat, Model.Bc, SentSize, Model.CellDim);

            /*Wf X -> GateF*/
            Computelib.Sgemm(SentOutput, 0, Model.Wf, 0, GateF, 0, SentSize, FeatureDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(GateF, Model.Bf, SentSize, Model.CellDim);

            /*Wo x -> GateO*/
            Computelib.Sgemm(SentOutput, 0, Model.Wo, 0, GateO, 0, SentSize, FeatureDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(GateO, Model.Bo, SentSize, Model.CellDim);
        }

        //public static void LSTMForward(IMathOperationManager Computelib, SeqSparseBatchData Input, SeqHelpData HelpInput, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
        //    SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseBatchData C, SeqDenseBatchData TanhC,
        //    SeqDenseBatchData Output)
        //{
        //    Output.BatchSize = Input.BatchSize;
        //    Output.SentSize = Input.SentSize;

        //    Output.SampleIdx = Input.SampleIdx;
        //    Output.SentMargin = Input.SentMargin;

        //    SparseForward(Computelib, Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Input.SentSize, Input.Stat.FEATURE_DIM, HelpInput, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);

        //    RecurrentForward(Computelib, HelpInput, InitO, InitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
        //}

        //public static void LSTMForward(IMathOperationManager Computelib, SeqDenseBatchData Input, SeqHelpData HelpInput, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
        //    SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseBatchData C, SeqDenseBatchData TanhC,
        //    SeqDenseBatchData Output)
        //{
        //    Output.BatchSize = Input.BatchSize;
        //    Output.SentSize = Input.SentSize;

        //    Output.SampleIdx = Input.SampleIdx;
        //    Output.SentMargin = Input.SentMargin;

        //    DenseForward(Computelib, Input.SentOutput, Input.SentSize, Input.Stat.FEATURE_DIM, Model, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
        //    RecurrentForward(Computelib, HelpInput, InitO, InitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
        //}

        public static void LSTMForwardv2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC, SeqDenseRecursiveData Output)
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            C.BatchSize = Input.BatchSize;
            C.SentSize = Input.SentSize;

            DenseForward(Computelib, Input.SentOutput, Input.SentSize, Input.Dim, Model, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
            RecurrentForwardV2(Computelib, Input.RecurrentInfo, InitO, InitC, Model, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
        }

        
        public static void LSTMForwardv2(IMathOperationManager Computelib, SeqSparseBatchData Input, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC, SeqDenseRecursiveData Output)
        {
            
            SparseForwardv2(Computelib, Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Input.SentSize, Input.FEATURE_DIM, Output.RecurrentInfo, Model, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput);
            //, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
            //CHat.SentOutput.SyncToCPU();
            //Output.SentOutput.SyncToCPU();
            RecurrentForwardV2(Computelib, Output.RecurrentInfo, InitO, InitC, Model, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
        }

        //public static void RecurrentAttentionForward(IMathOperationManager Computelib, SeqHelpData HelpInput,
        //    HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
        //    IAttentionComputeLib attentLib, SeqDenseBatchData Z,
        //    SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF,
        //    SeqDenseBatchData C, SeqDenseBatchData TanhC, SeqDenseBatchData Output)
        //{
        //    int preStartIdx = -1;
        //    for (int i = 0; i < HelpInput.MaxLag; i++)
        //    {
        //        int startIdx = i == 0 ? 0 : HelpInput.LagSeqIndex[i - 1];
        //        int Sum = HelpInput.LagSeqIndex[i] - startIdx;
        //        if (Sum == 0) break;
        //        if (i > 0)
        //        {
        //            // obtain attention from history memory.
        //            // o(t-1) -> attention Z
        //            attentLib.AttentionForward(Output.SentOutput, preStartIdx * Model.CellDim, // HelpInput.LagSeqPrevious, startIdx,
        //                                       HelpInput.LagSeqElementMem, startIdx,
        //                                       Z.SentOutput, startIdx * Model.AttentionDim, i, Sum);

        //            //gate_i += Z_{i} * z_{t}
        //            Computelib.Sgemm(Z.SentOutput, startIdx * Model.AttentionDim, Model.Zi, 0, GateI.SentOutput, startIdx * Model.CellDim,
        //                Sum, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

        //            //gate_f += Z_{f} * z_{t}
        //            Computelib.Sgemm(Z.SentOutput, startIdx * Model.AttentionDim, Model.Zf, 0, GateF.SentOutput, startIdx * Model.CellDim,
        //                Sum, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

        //            //c_hat += Z_{c} * z_{t}
        //            Computelib.Sgemm(Z.SentOutput, startIdx * Model.AttentionDim, Model.Zc, 0, CHat.SentOutput, startIdx * Model.CellDim,
        //                Sum, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

        //            //gate_o += Z_{o} * z_{t}
        //            Computelib.Sgemm(Z.SentOutput, startIdx * Model.AttentionDim, Model.Zo, 0, GateO.SentOutput, startIdx * Model.CellDim,
        //                Sum, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

        //            //gate_i += U_{i} * o_{t-1}
        //            //Computelib.SgemmMask(Output.SentOutput, preStartIdx * Model.CellDim, Model.Ui, 0, GateI.SentOutput, startIdx * Model.CellDim, 
        //            //    Sum, Model.CellDim, Model.CellDim,
        //            //        HelpInput.LagSeqPrevious, startIdx, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);
        //            Computelib.Sgemm(Output.SentOutput, preStartIdx * Model.CellDim, Model.Ui, 0, GateI.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

        //            //gate_f += U_{f} * o_{t-1}
        //            //Computelib.SgemmMask(Output.SentOutput, preStartIdx * Model.CellDim, Model.Uf, 0, GateF.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim,
        //            //        HelpInput.LagSeqPrevious, startIdx, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);
        //            Computelib.Sgemm(Output.SentOutput, preStartIdx * Model.CellDim, Model.Uf, 0, GateF.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

        //            //c_hat += U_{c} * o_{t-1}
        //            //Computelib.SgemmMask(Output.SentOutput, preStartIdx * Model.CellDim, Model.Uc, 0, CHat.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim,
        //            //        HelpInput.LagSeqPrevious, startIdx, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);
        //            Computelib.Sgemm(Output.SentOutput, preStartIdx * Model.CellDim, Model.Uc, 0, CHat.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

        //            //gate_o += U_{o} * o_{t-1}
        //            //Computelib.SgemmMask(Output.SentOutput, preStartIdx * Model.CellDim, Model.Uo, 0, GateO.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim,
        //            //        HelpInput.LagSeqPrevious, startIdx, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);
        //            Computelib.Sgemm(Output.SentOutput, preStartIdx * Model.CellDim, Model.Uo, 0, GateO.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
        //        }
        //        else if (InitO != null)
        //        {
        //            // obtain attention memory from history memory.
        //            //attentLib.AttentionForward(InitO.Output.Data, 0, CudaPieceInt.Empty, 0,
        //            //                           HelpInput.LagSeqElementMem, 0,
        //            //                           Z.SentOutput, 0, i, Sum);

        //            Computelib.Sgemm(InitO.Output.Data, 0, Model.Ui, 0, GateI.SentOutput, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
        //            Computelib.Sgemm(InitO.Output.Data, 0, Model.Uf, 0, GateF.SentOutput, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
        //            Computelib.Sgemm(InitO.Output.Data, 0, Model.Uc, 0, CHat.SentOutput, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
        //            Computelib.Sgemm(InitO.Output.Data, 0, Model.Uo, 0, GateO.SentOutput, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
        //        }



        //        //gate_i = sigmoid(gate_i)
        //        Computelib.Logistic(GateI.SentOutput, startIdx * Model.CellDim, GateI.SentOutput, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

        //        //c_hat = tanh(c_hat)
        //        Computelib.Tanh(CHat.SentOutput, startIdx * Model.CellDim, CHat.SentOutput, startIdx * Model.CellDim, Sum * Model.CellDim);

        //        //c = gate_i @ c_hat
        //        Computelib.ElementwiseProduct(GateI.SentOutput, startIdx * Model.CellDim, CHat.SentOutput, startIdx * Model.CellDim,
        //            C.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, 0);

        //        //gate_f = sigmoid(gate_f)
        //        Computelib.Logistic(GateF.SentOutput, startIdx * Model.CellDim, GateF.SentOutput, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

        //        if (i > 0)
        //        {
        //            /// c(t) = c(t) + gate_f(t) @ c(t-1)
        //            Computelib.ElementwiseProductMask(C.SentOutput, preStartIdx * Model.CellDim, GateF.SentOutput, startIdx * Model.CellDim,
        //                    C.SentOutput, startIdx * Model.CellDim, HelpInput.LagSeqPrevious, startIdx, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Sum, Model.CellDim, 1, 1);
        //        }
        //        else if (InitC != null)
        //        {
        //            Computelib.ElementwiseProductMask(InitC.Output.Data, 0, GateF.SentOutput, 0,
        //                    C.SentOutput, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Sum, Model.CellDim, 1, 1);
        //        }

        //        //gate_o += Vo * c(t)
        //        Computelib.Sgemm(C.SentOutput, startIdx * Model.CellDim, Model.Vo, 0, GateO.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

        //        //gate_o = simgoid(gate_o)
        //        Computelib.Logistic(GateO.SentOutput, startIdx * Model.CellDim, GateO.SentOutput, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

        //        //tanhc = tanh(c)
        //        Computelib.Tanh(C.SentOutput, startIdx * Model.CellDim, TanhC.SentOutput, startIdx * Model.CellDim, Sum * Model.CellDim);

        //        //o = gate_o @ tanhc
        //        Computelib.ElementwiseProduct(GateO.SentOutput, startIdx * Model.CellDim, TanhC.SentOutput, startIdx * Model.CellDim,
        //            Output.SentOutput, startIdx * Model.CellDim, Sum, Model.CellDim, 0);

        //        preStartIdx = startIdx;
        //    }
        //}

        public static void RecurrentAttentionForwardv2(IMathOperationManager Computelib, RecurrentSeqInfo RecurrentInfo,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, IAttentionComputeLib attentLib, CudaPieceFloat Z,
            CudaPieceFloat GateO, CudaPieceFloat GateI, CudaPieceFloat CHat, CudaPieceFloat GateF, CudaPieceFloat C, CudaPieceFloat TanhC, CudaPieceFloat Output)
        {
            int preStartIdx = -1;
            for (int i = 0; i < RecurrentInfo.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;
                if (i > 0)
                {
                    //gate_i += U_{i} * o_{t-1}
                    Computelib.Sgemm(Output, preStartIdx * Model.CellDim, Model.Ui, 0, GateI, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                    //gate_f += U_{f} * o_{t-1}
                    Computelib.Sgemm(Output, preStartIdx * Model.CellDim, Model.Uf, 0, GateF, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                    //c_hat += U_{c} * o_{t-1}
                    Computelib.Sgemm(Output, preStartIdx * Model.CellDim, Model.Uc, 0, CHat, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                    //gate_o += U_{o} * o_{t-1}
                    Computelib.Sgemm(Output, preStartIdx * Model.CellDim, Model.Uo, 0, GateO, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                    // obtain attention from history memory.
                    // o(t-1) -> attention Z.
                    attentLib.AttentionForward(Output, preStartIdx * Model.CellDim, RecurrentInfo.LagSeqElement, startIdx, Z, startIdx * Model.AttentionDim, i, Sum);
                }
                else if (InitO != null)
                {
                    // obtain attention memory from history memory.
                    Computelib.Sgemm(InitO.Output.Data, 0, Model.Ui, 0, GateI, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
                    Computelib.Sgemm(InitO.Output.Data, 0, Model.Uf, 0, GateF, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
                    Computelib.Sgemm(InitO.Output.Data, 0, Model.Uc, 0, CHat, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);
                    Computelib.Sgemm(InitO.Output.Data, 0, Model.Uo, 0, GateO, 0, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                    // obtain attention from history memory.
                    // o(t-1) -> attention Z.
                    attentLib.AttentionForward(InitO.Output.Data, 0, RecurrentInfo.LagSeqElement, 0, Z, startIdx * Model.AttentionDim, i, Sum);
                }
                
                /********************** Attention Gate!! ****************************/
                //gate_i += Z_{i} * z_{t}
                Computelib.Sgemm(Z, startIdx * Model.AttentionDim, Model.Zi, 0, GateI, startIdx * Model.CellDim, Sum, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

                //gate_f += Z_{f} * z_{t}
                Computelib.Sgemm(Z, startIdx * Model.AttentionDim, Model.Zf, 0, GateF, startIdx * Model.CellDim, Sum, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

                //c_hat += Z_{c} * z_{t}
                Computelib.Sgemm(Z, startIdx * Model.AttentionDim, Model.Zc, 0, CHat, startIdx * Model.CellDim, Sum, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

                //gate_o += Z_{o} * z_{t}
                Computelib.Sgemm(Z, startIdx * Model.AttentionDim, Model.Zo, 0, GateO, startIdx * Model.CellDim, Sum, Model.AttentionDim, Model.CellDim, 1, 1, false, false);
                /********************** Attention Gate!! ****************************/

                //gate_i = sigmoid(gate_i)
                Computelib.Logistic(GateI, startIdx * Model.CellDim, GateI, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

                //c_hat = tanh(c_hat)
                Computelib.Tanh(CHat, startIdx * Model.CellDim, CHat, startIdx * Model.CellDim, Sum * Model.CellDim);

                //c = gate_i @ c_hat
                Computelib.ElementwiseProduct(GateI, startIdx * Model.CellDim, CHat, startIdx * Model.CellDim, C, startIdx * Model.CellDim, Sum, Model.CellDim, 0);

                //gate_f = sigmoid(gate_f)
                Computelib.Logistic(GateF, startIdx * Model.CellDim, GateF, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

                if (i > 0)
                {
                    /// c(t) = c(t) + gate_f(t) @ c(t-1)
                    Computelib.ElementwiseProductMask(C, preStartIdx * Model.CellDim, GateF, startIdx * Model.CellDim, 
                        C, startIdx * Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Sum, Model.CellDim, 1, 1);
                }
                else if (InitC != null)
                {
                    Computelib.ElementwiseProductMask(InitC.Output.Data, 0, GateF, 0, 
                        C, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Sum, Model.CellDim, 1, 1);
                }

                //gate_o += Vo * c(t)
                Computelib.Sgemm(C, startIdx * Model.CellDim, Model.Vo, 0, GateO, startIdx * Model.CellDim, Sum, Model.CellDim, Model.CellDim, 1, 1, false, false);

                //gate_o = simgoid(gate_o)
                Computelib.Logistic(GateO, startIdx * Model.CellDim, GateO, startIdx * Model.CellDim, Sum * Model.CellDim, 1);

                //tanhc = tanh(c)
                Computelib.Tanh(C, startIdx * Model.CellDim, TanhC, startIdx * Model.CellDim, Sum * Model.CellDim);

                //o = gate_o @ tanhc
                Computelib.ElementwiseProduct(GateO, startIdx * Model.CellDim, TanhC, startIdx * Model.CellDim, Output, startIdx * Model.CellDim, Sum, Model.CellDim, 0);

                preStartIdx = startIdx;
            }
        }

        //public static void LSTMForwardAttention(IMathOperationManager Computelib, SeqSparseBatchData Input, SeqHelpData HelpInput,
        //    HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
        //    SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF,
        //    SeqDenseBatchData C, SeqDenseBatchData TanhC,
        //    SeqDenseBatchData Output)
        //{
        //    Output.BatchSize = Input.BatchSize;
        //    Output.SentSize = Input.SentSize;

        //    C.BatchSize = Input.BatchSize;
        //    C.SentSize = Input.SentSize;

        //    SparseForward(Computelib, Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Input.SentSize, Input.Stat.FEATURE_DIM, HelpInput, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
        //    RecurrentAttentionForward(Computelib, HelpInput, InitO, InitC, Model, attentLib, Z, GateO, GateI, CHat, GateF, C, TanhC, Output);
        //}


        //public static void LSTMForwardAttention(IMathOperationManager Computelib, SeqDenseBatchData Input, SeqHelpData HelpInput,
        //    HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
        //    SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF,
        //    SeqDenseBatchData C, SeqDenseBatchData TanhC,
        //    SeqDenseBatchData Output)
        //{
        //    Output.BatchSize = Input.BatchSize;
        //    Output.SentSize = Input.SentSize;

        //    Output.SampleIdx = Input.SampleIdx;
        //    Output.SentMargin = Input.SentMargin;

        //    DenseForward(Computelib, Input.SentOutput, Input.SentSize, Input.Stat.FEATURE_DIM,  Model, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
        //    RecurrentAttentionForward(Computelib, HelpInput, InitO, InitC, Model, attentLib, Z, GateO, GateI, CHat, GateF, C, TanhC, Output);
        //}


        public static void LSTMForwardAttentionv2(IMathOperationManager Computelib, SeqDenseRecursiveData Input,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF,
            SeqDenseRecursiveData C, SeqDenseBatchData TanhC, SeqDenseRecursiveData Output)
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            C.BatchSize = Input.BatchSize;
            C.SentSize = Input.SentSize;

            DenseForward(Computelib, Input.SentOutput, Input.SentSize, Input.Dim, Model, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
            RecurrentAttentionForwardv2(Computelib, Input.RecurrentInfo, InitO, InitC, Model, attentLib, Z.SentOutput, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
        }

        public static void LSTMForwardAttentionv2(IMathOperationManager Computelib, SeqSparseBatchData Input,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF,
            SeqDenseRecursiveData C, SeqDenseBatchData TanhC, SeqDenseRecursiveData Output)
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            C.BatchSize = Input.BatchSize;
            C.SentSize = Input.SentSize;

            SparseForwardv2(Computelib, Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Input.SentSize, Input.FEATURE_DIM, Output.RecurrentInfo, Model, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput);
            RecurrentAttentionForwardv2(Computelib, Output.RecurrentInfo, InitO, InitC, Model, attentLib, Z.SentOutput, GateO.SentOutput, GateI.SentOutput, CHat.SentOutput, GateF.SentOutput, C.SentOutput, TanhC.SentOutput, Output.SentOutput);
        }
    }

    public class LSTMComputeBackwardDataLib
    {
        //public static void RecurrentBackwardData(IMathOperationManager Computelib, CudaPieceInt SampleIdx, int BatchSize, int SentSize, HiddenBatchData InitO, HiddenBatchData InitC, SeqHelpData HelpInput,
        //    LSTMCell Model, SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseBatchData C, SeqDenseBatchData TanhC, SeqDenseBatchData Output)
        //{
        //    for (int i = HelpInput.MaxLag - 1; i >= 0; i--)
        //    {
        //        int startIdx = i == 0 ? 0 : HelpInput.LagSeqIndex[i - 1];
        //        int preStartIdx = i < 2 ? 0 : HelpInput.LagSeqIndex[i - 2];
        //        int Sum = HelpInput.LagSeqIndex[i] - startIdx;
        //        if (Sum == 0) break;

        //        // deriv_gate_o = deriv_o @ tanhc
        //        Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.CellDim,
        //                                               TanhC.SentOutput, startIdx * Model.CellDim,
        //                                               GateO.SentDeriv, startIdx * Model.CellDim,
        //                                               Sum, Model.CellDim, 0);

        //        // deriv_gate_o = deriv_gate_o *  gate_o * (1 - gate_o)
        //        Computelib.DerivLogistic(GateO.SentOutput, startIdx * Model.CellDim,
        //                                          GateO.SentDeriv, startIdx * Model.CellDim,
        //                                          GateO.SentDeriv, startIdx * Model.CellDim,
        //                                          Sum * Model.CellDim, 0, 1);
        //        // deriv_c += deriv_gate_o * Vo'
        //        Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
        //                                  Model.Vo, 0,
        //                                  C.SentDeriv, startIdx * Model.CellDim,
        //                                  Sum, Model.CellDim, Model.CellDim,
        //                                  1, 1, false, true);

        //        /// deriv_o(t-1) += deriv_gate_o(t) * Uo';
        //        if (i > 0)
        //        {
        //            Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
        //                             Model.Uo, 0,
        //                             Output.SentDeriv, preStartIdx * Model.CellDim,
        //                             Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);
        //            /*Computelib.SgemmMask(GateO.SentDeriv, startIdx * Model.CellDim,
        //                                          Model.Uo, 0,
        //                                          Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                          Sum, Model.CellDim, Model.CellDim,
        //                                          CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                          1, 1, false, true);*/
        //        }
        //        else if (InitO != null)
        //        {
        //            Computelib.Sgemm(GateO.SentDeriv, 0, Model.Uo, 0, InitO.Deriv.Data, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);
        //        }

        //        //deriv_tanhc = deriv_o @ gate_o
        //        Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.CellDim,
        //                                                   GateO.SentOutput, startIdx * Model.CellDim,
        //                                                   TanhC.SentDeriv, startIdx * Model.CellDim,
        //                                                   Sum, Model.CellDim, 0);

        //        // deriv_c += deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
        //        Computelib.DerivTanh(TanhC.SentOutput, startIdx * Model.CellDim,
        //                                      TanhC.SentDeriv, startIdx * Model.CellDim,
        //                                      C.SentDeriv, startIdx * Model.CellDim,
        //                                      Sum * Model.CellDim, 1, 1);

        //        // deriv_c_hat = deriv_c @ gate_i
        //        Computelib.ElementwiseProduct(C.SentDeriv, startIdx * Model.CellDim,
        //                                               GateI.SentOutput, startIdx * Model.CellDim,
        //                                               CHat.SentDeriv, startIdx * Model.CellDim,
        //                                               Sum, Model.CellDim, 0);

        //        //deriv_gate_i = deriv_c @ c_hat
        //        Computelib.ElementwiseProduct(C.SentDeriv, startIdx * Model.CellDim,
        //                                                   CHat.SentOutput, startIdx * Model.CellDim,
        //                                                   GateI.SentDeriv, startIdx * Model.CellDim,
        //                                                   Sum, Model.CellDim, 0); // 1);

        //        //deriv_gate_i = deriv_gate_i * gate_i * (1 - gate_i)
        //        Computelib.DerivLogistic(GateI.SentOutput, startIdx * Model.CellDim,
        //                                          GateI.SentDeriv, startIdx * Model.CellDim,
        //                                          GateI.SentDeriv, startIdx * Model.CellDim,
        //                                          Sum * Model.CellDim, 0, 1);

        //        // deriv_c_hat = deriv_c_hat * (1 + c_hat)*( 1- c_hat)
        //        Computelib.DerivTanh(CHat.SentOutput, startIdx * Model.CellDim,
        //                                      CHat.SentDeriv, startIdx * Model.CellDim,
        //                                      CHat.SentDeriv, startIdx * Model.CellDim,
        //                                      Sum * Model.CellDim, 0, 1);

        //        if (i > 0)
        //        {
        //            // deriv_o(t-1) += deriv_gate_i(t) * Ui';
        //            /*Computelib.SgemmMask(GateI.SentDeriv, startIdx * Model.CellDim,
        //                                          Model.Ui, 0,
        //                                          Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                          Sum, Model.CellDim, Model.CellDim,
        //                                          CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                          1, 1, false, true);*/
        //            Computelib.Sgemm(GateI.SentDeriv, startIdx * Model.CellDim,
        //                                          Model.Ui, 0,
        //                                          Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                          Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);


        //            //deriv_o(t-1) += deriv_cHat(t) * Uc'
        //            /*Computelib.SgemmMask(CHat.SentDeriv, startIdx * Model.CellDim,
        //                                          Model.Uc, 0,
        //                                          Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                          Sum, Model.CellDim, Model.CellDim,
        //                                          CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                          1, 1, false, true);*/
        //            Computelib.Sgemm(CHat.SentDeriv, startIdx * Model.CellDim,
        //                                          Model.Uc, 0,
        //                                          Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                          Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);


        //            //deriv_c(t-1) += deriv_c(t) @ gate_f
        //            Computelib.ElementwiseProductMask(C.SentDeriv, startIdx * Model.CellDim,
        //                                                       GateF.SentOutput, startIdx * Model.CellDim,
        //                                                       C.SentDeriv, preStartIdx * Model.CellDim,
        //                                                       CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                                       Sum, Model.CellDim, 1, 1);

        //            //deriv_gate_f = deriv_c @ c(t-1)
        //            Computelib.ElementwiseProductMask(C.SentOutput, preStartIdx * Model.CellDim,
        //                                                       C.SentDeriv, startIdx * Model.CellDim,
        //                                                       GateF.SentDeriv, startIdx * Model.CellDim,
        //                                                       HelpInput.LagSeqPrevious, startIdx, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
        //                                                       Sum, Model.CellDim, 0, 1);

        //            // deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
        //            Computelib.DerivLogistic(GateF.SentOutput, startIdx * Model.CellDim,
        //                                              GateF.SentDeriv, startIdx * Model.CellDim,
        //                                              GateF.SentDeriv, startIdx * Model.CellDim,
        //                                              Sum * Model.CellDim, 0, 1);

        //            //deriv_o(t -1) += deriv_gate_f(t) * Uf'
        //            /*Computelib.SgemmMask(GateF.SentDeriv, startIdx * Model.CellDim,
        //                                          Model.Uf, 0,
        //                                          Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                          Sum, Model.CellDim, Model.CellDim,
        //                                          CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                          1, 1, false, true);*/
        //            Computelib.Sgemm(GateF.SentDeriv, startIdx * Model.CellDim,
        //                                          Model.Uf, 0,
        //                                          Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                          Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);
        //        }
        //        else if (InitO != null)
        //        {
        //            // deriv_InitO += deriv_gate_i(t) * Ui;
        //            Computelib.Sgemm(GateI.SentDeriv, 0,
        //                             Model.Ui, 0,
        //                             InitO.Deriv.Data, 0,
        //                             BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //            // deriv_InitO += deriv_cHat(t) * Uc
        //            Computelib.Sgemm(CHat.SentDeriv, 0,
        //                             Model.Uc, 0,
        //                             InitO.Deriv.Data, 0,
        //                             BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //            if (InitC != null)
        //            {
        //                //deriv_InitC += deriv_c(t) @ gate_f
        //                Computelib.ElementwiseProductMask(C.SentDeriv, 0,
        //                                                  GateF.SentOutput, 0,
        //                                                  InitC.Deriv.Data, 0,
        //                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
        //                                                  BatchSize, Model.CellDim, 1, 1);

        //                //deriv_gate_f = deriv_c @ InitC
        //                Computelib.ElementwiseProductMask(InitC.Output.Data, 0,
        //                                                  C.SentDeriv, 0,
        //                                                  GateF.SentDeriv, 0,
        //                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
        //                                                  Sum, Model.CellDim, 0, 1);

        //                // deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
        //                Computelib.DerivLogistic(GateF.SentOutput, 0,
        //                                         GateF.SentDeriv, 0,
        //                                         GateF.SentDeriv, 0,
        //                                         Sum * Model.CellDim, 0, 1);

        //                //deriv_InitO += deriv_gate_f(t) * Uf
        //                /*Computelib.SgemmMask(GateF.SentDeriv, 0,
        //                                     Model.Uf, 0,
        //                                     InitO.Deriv.Data, 0,
        //                                     BatchSize, Model.CellDim, Model.CellDim,
        //                                     CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
        //                                     1, 1, false, true);*/
        //                Computelib.Sgemm(GateF.SentDeriv, 0,
        //                                     Model.Uf, 0,
        //                                     InitO.Deriv.Data, 0,
        //                                     BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //            }
        //            else
        //            {
        //                Computelib.Zero(GateF.SentDeriv, BatchSize * Model.CellDim);
        //            }
        //        }
        //    }
        //}

        public static void RecurrentBackwardDatav2(IMathOperationManager Computelib, //CudaPieceInt SampleIdx, int BatchSize, int SentSize, 
            HiddenBatchData InitO, HiddenBatchData InitC, RecurrentSeqInfo RecurrentInfo,
            LSTMCell Model, SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, 
            SeqDenseRecursiveData C, SeqDenseBatchData TanhC, SeqDenseRecursiveData Output)
        {
            for (int i = RecurrentInfo.MaxLag - 1; i >= 0; i--)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int preStartIdx = i < 2 ? 0 : RecurrentInfo.LagSeqIdx[i - 2];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                // deriv_gate_o = deriv_o @ tanhc
                Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.CellDim,
                                              TanhC.SentOutput, startIdx * Model.CellDim,
                                              GateO.SentDeriv, startIdx * Model.CellDim,
                                              Sum, Model.CellDim, 0);

                // deriv_gate_o = deriv_gate_o *  gate_o * (1 - gate_o)
                Computelib.DerivLogistic(GateO.SentOutput, startIdx * Model.CellDim,
                                         GateO.SentDeriv, startIdx * Model.CellDim,
                                         GateO.SentDeriv, startIdx * Model.CellDim,
                                         Sum * Model.CellDim, 0, 1);

                // deriv_c += deriv_gate_o * Vo'
                Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
                                 Model.Vo, 0,
                                 C.SentDeriv, startIdx * Model.CellDim,
                                 Sum, Model.CellDim, Model.CellDim,
                                 1, 1, false, true);

                // deriv_o(t-1) += deriv_gate_o(t) * Uo';
                if (i > 0)
                {
                    Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
                                     Model.Uo, 0,
                                     Output.SentDeriv, preStartIdx * Model.CellDim,
                                     Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);
                }
                else if (InitO != null && InitO.Deriv != null)
                {
                    Computelib.Sgemm(GateO.SentDeriv, 0, Model.Uo, 0, InitO.Deriv.Data, 0, RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);
                }

                //deriv_tanhc = deriv_o @ gate_o
                Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.CellDim,
                                                           GateO.SentOutput, startIdx * Model.CellDim,
                                                           TanhC.SentDeriv, startIdx * Model.CellDim,
                                                           Sum, Model.CellDim, 0);

                // deriv_c += deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
                Computelib.DerivTanh(TanhC.SentOutput, startIdx * Model.CellDim,
                                              TanhC.SentDeriv, startIdx * Model.CellDim,
                                              C.SentDeriv, startIdx * Model.CellDim,
                                              Sum * Model.CellDim, 1, 1);

                // deriv_c_hat = deriv_c @ gate_i
                Computelib.ElementwiseProduct(C.SentDeriv, startIdx * Model.CellDim,
                                                       GateI.SentOutput, startIdx * Model.CellDim,
                                                       CHat.SentDeriv, startIdx * Model.CellDim,
                                                       Sum, Model.CellDim, 0);

                //deriv_gate_i = deriv_c @ c_hat
                Computelib.ElementwiseProduct(C.SentDeriv, startIdx * Model.CellDim,
                                                           CHat.SentOutput, startIdx * Model.CellDim,
                                                           GateI.SentDeriv, startIdx * Model.CellDim,
                                                           Sum, Model.CellDim, 0); // 1);

                //deriv_gate_i = deriv_gate_i * gate_i * (1 - gate_i)
                Computelib.DerivLogistic(GateI.SentOutput, startIdx * Model.CellDim,
                                                  GateI.SentDeriv, startIdx * Model.CellDim,
                                                  GateI.SentDeriv, startIdx * Model.CellDim,
                                                  Sum * Model.CellDim, 0, 1);

                // deriv_c_hat = deriv_c_hat * (1 + c_hat)*( 1- c_hat)
                Computelib.DerivTanh(CHat.SentOutput, startIdx * Model.CellDim,
                                              CHat.SentDeriv, startIdx * Model.CellDim,
                                              CHat.SentDeriv, startIdx * Model.CellDim,
                                              Sum * Model.CellDim, 0, 1);

                if (i > 0)
                {
                    // deriv_o(t-1) += deriv_gate_i(t) * Ui';
                    Computelib.Sgemm(GateI.SentDeriv, startIdx * Model.CellDim,
                                                  Model.Ui, 0,
                                                  Output.SentDeriv, preStartIdx * Model.CellDim,
                                                  Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);

                    //deriv_o(t-1) += deriv_cHat(t) * Uc'
                    Computelib.Sgemm(CHat.SentDeriv, startIdx * Model.CellDim,
                                                  Model.Uc, 0,
                                                  Output.SentDeriv, preStartIdx * Model.CellDim,
                                                  Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);

                    //deriv_c(t-1) += deriv_c(t) @ gate_f
                    Computelib.ElementwiseProductMask(C.SentDeriv, startIdx * Model.CellDim,
                                                      GateF.SentOutput, startIdx * Model.CellDim,
                                                      C.SentDeriv, preStartIdx * Model.CellDim,
                                                      CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                      Sum, Model.CellDim, 1, 1);

                    //deriv_gate_f = deriv_c @ c(t-1)
                    Computelib.ElementwiseProductMask(C.SentOutput, preStartIdx * Model.CellDim,
                                                               C.SentDeriv, startIdx * Model.CellDim,
                                                               GateF.SentDeriv, startIdx * Model.CellDim,
                                                               CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                               Sum, Model.CellDim, 0, 1);

                    // deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
                    Computelib.DerivLogistic(GateF.SentOutput, startIdx * Model.CellDim,
                                                      GateF.SentDeriv, startIdx * Model.CellDim,
                                                      GateF.SentDeriv, startIdx * Model.CellDim,
                                                      Sum * Model.CellDim, 0, 1);

                    //deriv_o(t -1) += deriv_gate_f(t) * Uf'
                    Computelib.Sgemm(GateF.SentDeriv, startIdx * Model.CellDim,
                                                  Model.Uf, 0,
                                                  Output.SentDeriv, preStartIdx * Model.CellDim,
                                                  Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);
                }
                else 
                {
                    if (InitO != null && InitO.Deriv != null)
                    {
                        // deriv_InitO += deriv_gate_i(t) * Ui;
                        Computelib.Sgemm(GateI.SentDeriv, 0,
                                     Model.Ui, 0,
                                     InitO.Deriv.Data, 0,
                                     RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

                        // deriv_InitO += deriv_cHat(t) * Uc
                        Computelib.Sgemm(CHat.SentDeriv, 0,
                                         Model.Uc, 0,
                                         InitO.Deriv.Data, 0,
                                         RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);
                    }
                    if (InitC != null)
                    {
                        if(InitC.Deriv != null)
                            //deriv_InitC += deriv_c(t) @ gate_f
                            Computelib.ElementwiseProductMask(C.SentDeriv, 0,
                                                              GateF.SentOutput, 0,
                                                              InitC.Deriv.Data, 0,
                                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                              RecurrentInfo.BatchSize, Model.CellDim, 1, 1);

                        //deriv_gate_f = deriv_c @ InitC
                        Computelib.ElementwiseProductMask(InitC.Output.Data, 0,
                                                          C.SentDeriv, 0,
                                                          GateF.SentDeriv, 0,
                                                          CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                          Sum, Model.CellDim, 0, 1);

                        // deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
                        Computelib.DerivLogistic(GateF.SentOutput, 0,
                                                 GateF.SentDeriv, 0,
                                                 GateF.SentDeriv, 0,
                                                 Sum * Model.CellDim, 0, 1);

                        if(InitO.Deriv != null)
                            //deriv_InitO += deriv_gate_f(t) * Uf
                            Computelib.Sgemm(GateF.SentDeriv, 0,
                                                 Model.Uf, 0,
                                                 InitO.Deriv.Data, 0,
                                                 RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

                    }
                    else
                    {
                        Computelib.Zero(GateF.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim);
                    }
                }
            }
        }

        // public static void RecurrentAttentionBackwardData(IMathOperationManager Computelib, CudaPieceInt SampleIdx, int BatchSize, int SentSize,
        //     HiddenBatchData InitO, HiddenBatchData InitC, SeqHelpData HelpInput, LSTMCell Model,
        //     IAttentionComputeLib attentLib, SeqDenseBatchData Z,
        //     SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseBatchData C, SeqDenseBatchData TanhC, SeqDenseBatchData Output)
        // {
        //     for (int i = HelpInput.MaxLag - 1; i >= 0; i--)
        //     {
        //         int startIdx = i == 0 ? 0 : HelpInput.LagSeqIndex[i - 1];
        //         int preStartIdx = i < 2 ? 0 : HelpInput.LagSeqIndex[i - 2];
        //         int Sum = HelpInput.LagSeqIndex[i] - startIdx;
        //         if (Sum == 0) break;

        //         // deriv_gate_o = deriv_o @ tanhc
        //         Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.CellDim,
        //                                                TanhC.SentOutput, startIdx * Model.CellDim,
        //                                                GateO.SentDeriv, startIdx * Model.CellDim,
        //                                                Sum, Model.CellDim, 0);

        //         // deriv_gate_o = deriv_gate_o *  gate_o * (1 - gate_o)
        //         Computelib.DerivLogistic(GateO.SentOutput, startIdx * Model.CellDim,
        //                                           GateO.SentDeriv, startIdx * Model.CellDim,
        //                                           GateO.SentDeriv, startIdx * Model.CellDim,
        //                                           Sum * Model.CellDim, 0, 1);
        //         // deriv_c += deriv_gate_o * Vo'
        //         Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
        //                                   Model.Vo, 0,
        //                                   C.SentDeriv, startIdx * Model.CellDim,
        //                                   Sum, Model.CellDim, Model.CellDim,
        //                                   1, 1, false, true);

        //         /// deriv_o(t-1) += deriv_gate_o(t) * Uo';
        //         if (i > 0)
        //         {

        //             /*Computelib.SgemmMask(GateO.SentDeriv, startIdx * Model.CellDim,
        //                                           Model.Uo, 0,
        //                                           Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                           Sum, Model.CellDim, Model.CellDim,
        //                                           CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                           1, 1, false, true);*/
        //             Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
        //                                           Model.Uo, 0,
        //                                           Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                           Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //             // deriv_z(t) = deriv_gate_o(t) * Zo';
        //             Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
        //                                Model.Zo, 0,
        //                                Z.SentDeriv, startIdx * Model.AttentionDim,
        //                                Sum, Model.CellDim, Model.AttentionDim,
        //                                0, 1, false, true);
        //         }
        //         else if (InitO != null)
        //         {
        //             Computelib.Sgemm(GateO.SentDeriv, 0, Model.Uo, 0, InitO.Deriv.Data, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);
        //         }

        //         //deriv_tanhc = deriv_o @ gate_o
        //         Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.CellDim,
        //                                                    GateO.SentOutput, startIdx * Model.CellDim,
        //                                                    TanhC.SentDeriv, startIdx * Model.CellDim,
        //                                                    Sum, Model.CellDim, 0);

        //         // deriv_c += deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
        //         Computelib.DerivTanh(TanhC.SentOutput, startIdx * Model.CellDim,
        //                                       TanhC.SentDeriv, startIdx * Model.CellDim,
        //                                       C.SentDeriv, startIdx * Model.CellDim,
        //                                       Sum * Model.CellDim, 1, 1);

        //         // deriv_c_hat = deriv_c @ gate_i
        //         Computelib.ElementwiseProduct(C.SentDeriv, startIdx * Model.CellDim,
        //                                                GateI.SentOutput, startIdx * Model.CellDim,
        //                                                CHat.SentDeriv, startIdx * Model.CellDim,
        //                                                Sum, Model.CellDim, 0);

        //         //deriv_gate_i = deriv_c @ c_hat
        //         Computelib.ElementwiseProduct(C.SentDeriv, startIdx * Model.CellDim,
        //                                                    CHat.SentOutput, startIdx * Model.CellDim,
        //                                                    GateI.SentDeriv, startIdx * Model.CellDim,
        //                                                    Sum, Model.CellDim, 0); // 1);

        //         //deriv_gate_i = deriv_gate_i * gate_i * (1 - gate_i)
        //         Computelib.DerivLogistic(GateI.SentOutput, startIdx * Model.CellDim,
        //                                           GateI.SentDeriv, startIdx * Model.CellDim,
        //                                           GateI.SentDeriv, startIdx * Model.CellDim,
        //                                           Sum * Model.CellDim, 0, 1);

        //         // deriv_c_hat = deriv_c_hat * (1 + c_hat)*( 1- c_hat)
        //         Computelib.DerivTanh(CHat.SentOutput, startIdx * Model.CellDim,
        //                                       CHat.SentDeriv, startIdx * Model.CellDim,
        //                                       CHat.SentDeriv, startIdx * Model.CellDim,
        //                                       Sum * Model.CellDim, 0, 1);

        //         if (i > 0)
        //         {
        //             // deriv_o(t-1) += deriv_gate_i(t) * Ui';
        //             /*Computelib.SgemmMask(GateI.SentDeriv, startIdx * Model.CellDim,
        //                                           Model.Ui, 0,
        //                                           Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                           Sum, Model.CellDim, Model.CellDim,
        //                                           CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                           1, 1, false, true);*/
        //             Computelib.Sgemm(GateI.SentDeriv, startIdx * Model.CellDim,
        //                                           Model.Ui, 0,
        //                                           Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                           Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //             // deriv_z(t) += deriv_gate_i(t) * Zi';
        //             Computelib.Sgemm(GateI.SentDeriv, startIdx * Model.CellDim,
        //                                Model.Zi, 0,
        //                                Z.SentDeriv, startIdx * Model.AttentionDim,
        //                                Sum, Model.CellDim, Model.AttentionDim,
        //                                1, 1, false, true);

        //             //deriv_o(t-1) += deriv_cHat(t) * Uc'
        //             Computelib.SgemmMask(CHat.SentDeriv, startIdx * Model.CellDim,
        //                                           Model.Uc, 0,
        //                                           Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                           Sum, Model.CellDim, Model.CellDim,
        //                                           CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                           1, 1, false, true);
        //             Computelib.Sgemm(CHat.SentDeriv, startIdx * Model.CellDim,
        //                                           Model.Uc, 0,
        //                                           Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                           Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //             // deriv_z(t) += deriv_cHat(t) * Zc';
        //             Computelib.Sgemm(CHat.SentDeriv, startIdx * Model.CellDim,
        //                                Model.Zc, 0,
        //                                Z.SentDeriv, startIdx * Model.AttentionDim,
        //                                Sum, Model.CellDim, Model.AttentionDim,
        //                                1, 1, false, true);

        //             //deriv_c(t-1) += deriv_c(t) @ gate_f
        //             Computelib.ElementwiseProductMask(C.SentDeriv, startIdx * Model.CellDim,
        //                                                        GateF.SentOutput, startIdx * Model.CellDim,
        //                                                        C.SentDeriv, preStartIdx * Model.CellDim,
        //                                                        CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                                        Sum, Model.CellDim, 1, 1);

        //             //deriv_gate_f = deriv_c @ c(t-1)
        //             Computelib.ElementwiseProductMask(C.SentOutput, preStartIdx * Model.CellDim,
        //                                                        C.SentDeriv, startIdx * Model.CellDim,
        //                                                        GateF.SentDeriv, startIdx * Model.CellDim,
        //                                                        HelpInput.LagSeqPrevious, startIdx, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
        //                                                        Sum, Model.CellDim, 0, 1);

        //             // deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
        //             Computelib.DerivLogistic(GateF.SentOutput, startIdx * Model.CellDim,
        //                                               GateF.SentDeriv, startIdx * Model.CellDim,
        //                                               GateF.SentDeriv, startIdx * Model.CellDim,
        //                                               Sum * Model.CellDim, 0, 1);

        //             //deriv_o(t -1) += deriv_gate_f(t) * Uf'
        //             /*Computelib.SgemmMask(GateF.SentDeriv, startIdx * Model.CellDim,
        //                                           Model.Uf, 0,
        //                                           Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                           Sum, Model.CellDim, Model.CellDim,
        //                                           CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, HelpInput.LagSeqPrevious, startIdx,
        //                                           1, 1, false, true);*/
        //             Computelib.Sgemm(GateF.SentDeriv, startIdx * Model.CellDim,
        //                                           Model.Uf, 0,
        //                                           Output.SentDeriv, preStartIdx * Model.CellDim,
        //                                           Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //             // deriv_z(t) += deriv_gate_f(t) * Zf';
        //             Computelib.Sgemm(GateF.SentDeriv, startIdx * Model.CellDim,
        //                                Model.Zf, 0,
        //                                Z.SentDeriv, startIdx * Model.AttentionDim,
        //                                Sum, Model.CellDim, Model.AttentionDim,
        //                                1, 1, false, true);

        //         }
        //         else if (InitO != null)
        //         {
        //             // deriv_InitO += deriv_gate_i(t) * Ui;
        //             Computelib.Sgemm(GateI.SentDeriv, 0,
        //                              Model.Ui, 0,
        //                              InitO.Deriv.Data, 0,
        //                              BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //             // deriv_InitO += deriv_cHat(t) * Uc
        //             Computelib.Sgemm(CHat.SentDeriv, 0,
        //                              Model.Uc, 0,
        //                              InitO.Deriv.Data, 0,
        //                              BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

        //             if (InitC != null)
        //             {
        //                 //deriv_InitC += deriv_c(t) @ gate_f
        //                 Computelib.ElementwiseProductMask(C.SentDeriv, 0,
        //                                                   GateF.SentOutput, 0,
        //                                                   InitC.Deriv.Data, 0,
        //                                                   CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
        //                                                   BatchSize, Model.CellDim, 1, 1);

        //                 //deriv_gate_f = deriv_c @ InitC
        //                 Computelib.ElementwiseProductMask(InitC.Output.Data, 0,
        //                                                   C.SentDeriv, 0,
        //                                                   GateF.SentDeriv, 0,
        //                                                   CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
        //                                                   Sum, Model.CellDim, 0, 1);

        //                 // deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
        //                 Computelib.DerivLogistic(GateF.SentOutput, 0, // startIdx * Model.CellDim,
        //                                          GateF.SentDeriv, 0, //startIdx * Model.CellDim,
        //                                          GateF.SentDeriv, 0, //startIdx * Model.CellDim,
        //                                          Sum * Model.CellDim, 0, 1);

        //                 //deriv_InitO += deriv_gate_f(t) * Uf
        //                 /*Computelib.SgemmMask(GateF.SentDeriv, 0,
        //                                      Model.Uf, 0,
        //                                      InitO.Deriv.Data, 0,
        //                                      BatchSize, Model.CellDim, Model.CellDim,
        //                                      CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
        //                                      1, 1, false, true);*/
        //                 Computelib.Sgemm(GateF.SentDeriv, 0,
        //                                      Model.Uf, 0,
        //                                      InitO.Deriv.Data, 0,
        //                                      BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);
        //             }
        //             else
        //             {
        //                 Computelib.Zero(GateF.SentDeriv, BatchSize * Model.CellDim);
        //             }
        //         }

        //         if (i > 0)
        //         {
        //             attentLib.AttentionBackwardData(Output.SentDeriv, preStartIdx * Model.CellDim, //CudaPieceInt.Empty, 0, // HelpInput.LagSeqPrevious, startIdx,
        //                 HelpInput.LagSeqElementMem, startIdx,
        //                 Z.SentDeriv, startIdx * Model.AttentionDim, i, Sum);
        //         }
        //     }
        // }

        public static void RecurrentAttentionBackwardDatav2(IMathOperationManager Computelib, //CudaPieceInt SampleIdx, int BatchSize, int SentSize,
            HiddenBatchData InitO, HiddenBatchData InitC, RecurrentSeqInfo RecurrentInfo, LSTMCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC, SeqDenseRecursiveData Output)
        {
            for (int i = RecurrentInfo.MaxLag - 1; i >= 0; i--)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int preStartIdx = i < 2 ? 0 : RecurrentInfo.LagSeqIdx[i - 2];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                // deriv_gate_o = deriv_o @ tanhc
                Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.CellDim,
                                                       TanhC.SentOutput, startIdx * Model.CellDim,
                                                       GateO.SentDeriv, startIdx * Model.CellDim,
                                                       Sum, Model.CellDim, 0);

                // deriv_gate_o = deriv_gate_o *  gate_o * (1 - gate_o)
                Computelib.DerivLogistic(GateO.SentOutput, startIdx * Model.CellDim,
                                                  GateO.SentDeriv, startIdx * Model.CellDim,
                                                  GateO.SentDeriv, startIdx * Model.CellDim,
                                                  Sum * Model.CellDim, 0, 1);
                // deriv_c += deriv_gate_o * Vo'
                Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
                                          Model.Vo, 0,
                                          C.SentDeriv, startIdx * Model.CellDim,
                                          Sum, Model.CellDim, Model.CellDim,
                                          1, 1, false, true);

                // deriv_o(t-1) += deriv_gate_o(t) * Uo';
                if (i > 0)
                {
                    Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
                                     Model.Uo, 0,
                                     Output.SentDeriv, preStartIdx * Model.CellDim,
                                     Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);
                }
                else if (InitO != null)
                {
                    Computelib.Sgemm(GateO.SentDeriv, 0, Model.Uo, 0, InitO.Deriv.Data, 0, RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);
                }

                //deriv_tanhc = deriv_o @ gate_o
                Computelib.ElementwiseProduct(Output.SentDeriv, startIdx * Model.CellDim,
                                                           GateO.SentOutput, startIdx * Model.CellDim,
                                                           TanhC.SentDeriv, startIdx * Model.CellDim,
                                                           Sum, Model.CellDim, 0);

                // deriv_c += deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
                Computelib.DerivTanh(TanhC.SentOutput, startIdx * Model.CellDim,
                                              TanhC.SentDeriv, startIdx * Model.CellDim,
                                              C.SentDeriv, startIdx * Model.CellDim,
                                              Sum * Model.CellDim, 1, 1);

                // deriv_c_hat = deriv_c @ gate_i
                Computelib.ElementwiseProduct(C.SentDeriv, startIdx * Model.CellDim,
                                                       GateI.SentOutput, startIdx * Model.CellDim,
                                                       CHat.SentDeriv, startIdx * Model.CellDim,
                                                       Sum, Model.CellDim, 0);

                //deriv_gate_i = deriv_c @ c_hat
                Computelib.ElementwiseProduct(C.SentDeriv, startIdx * Model.CellDim,
                                                           CHat.SentOutput, startIdx * Model.CellDim,
                                                           GateI.SentDeriv, startIdx * Model.CellDim,
                                                           Sum, Model.CellDim, 0); // 1);

                //deriv_gate_i = deriv_gate_i * gate_i * (1 - gate_i)
                Computelib.DerivLogistic(GateI.SentOutput, startIdx * Model.CellDim,
                                                  GateI.SentDeriv, startIdx * Model.CellDim,
                                                  GateI.SentDeriv, startIdx * Model.CellDim,
                                                  Sum * Model.CellDim, 0, 1);

                // deriv_c_hat = deriv_c_hat * (1 + c_hat)*( 1- c_hat)
                Computelib.DerivTanh(CHat.SentOutput, startIdx * Model.CellDim,
                                              CHat.SentDeriv, startIdx * Model.CellDim,
                                              CHat.SentDeriv, startIdx * Model.CellDim,
                                              Sum * Model.CellDim, 0, 1);

                if (i > 0)
                {
                    // deriv_o(t-1) += deriv_gate_i(t) * Ui';
                    Computelib.Sgemm(GateI.SentDeriv, startIdx * Model.CellDim,
                                                  Model.Ui, 0,
                                                  Output.SentDeriv, preStartIdx * Model.CellDim,
                                                  Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);

                    //deriv_o(t-1) += deriv_cHat(t) * Uc'
                    Computelib.Sgemm(CHat.SentDeriv, startIdx * Model.CellDim,
                                                  Model.Uc, 0,
                                                  Output.SentDeriv, preStartIdx * Model.CellDim,
                                                  Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);

                    
                    //deriv_c(t-1) += deriv_c(t) @ gate_f
                    Computelib.ElementwiseProductMask(C.SentDeriv, startIdx * Model.CellDim,
                                                               GateF.SentOutput, startIdx * Model.CellDim,
                                                               C.SentDeriv, preStartIdx * Model.CellDim,
                                                               CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                               Sum, Model.CellDim, 1, 1);

                    //deriv_gate_f = deriv_c @ c(t-1)
                    Computelib.ElementwiseProductMask(C.SentOutput, preStartIdx * Model.CellDim,
                                                               C.SentDeriv, startIdx * Model.CellDim,
                                                               GateF.SentDeriv, startIdx * Model.CellDim,
                                                               CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                               Sum, Model.CellDim, 0, 1);

                    // deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
                    Computelib.DerivLogistic(GateF.SentOutput, startIdx * Model.CellDim,
                                                      GateF.SentDeriv, startIdx * Model.CellDim,
                                                      GateF.SentDeriv, startIdx * Model.CellDim,
                                                      Sum * Model.CellDim, 0, 1);

                    //deriv_o(t -1) += deriv_gate_f(t) * Uf'
                    Computelib.Sgemm(GateF.SentDeriv, startIdx * Model.CellDim,
                                                  Model.Uf, 0,
                                                  Output.SentDeriv, preStartIdx * Model.CellDim,
                                                  Sum, Model.CellDim, Model.CellDim, 1, 1, false, true);
                }
                else if (InitO != null)
                {
                    // deriv_InitO += deriv_gate_i(t) * Ui;
                    Computelib.Sgemm(GateI.SentDeriv, 0,
                                     Model.Ui, 0,
                                     InitO.Deriv.Data, 0,
                                     RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

                    // deriv_InitO += deriv_cHat(t) * Uc
                    Computelib.Sgemm(CHat.SentDeriv, 0,
                                     Model.Uc, 0,
                                     InitO.Deriv.Data, 0,
                                     RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);

                    if (InitC != null)
                    {
                        //deriv_InitC += deriv_c(t) @ gate_f
                        Computelib.ElementwiseProductMask(C.SentDeriv, 0,
                                                          GateF.SentOutput, 0,
                                                          InitC.Deriv.Data, 0,
                                                          CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                          RecurrentInfo.BatchSize, Model.CellDim, 1, 1);

                        //deriv_gate_f = deriv_c @ InitC
                        Computelib.ElementwiseProductMask(InitC.Output.Data, 0,
                                                          C.SentDeriv, 0,
                                                          GateF.SentDeriv, 0,
                                                          CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                          Sum, Model.CellDim, 0, 1);

                        // deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
                        Computelib.DerivLogistic(GateF.SentOutput, 0, // startIdx * Model.CellDim,
                                                 GateF.SentDeriv, 0, //startIdx * Model.CellDim,
                                                 GateF.SentDeriv, 0, //startIdx * Model.CellDim,
                                                 Sum * Model.CellDim, 0, 1);

                        //deriv_InitO += deriv_gate_f(t) * Uf
                        Computelib.Sgemm(GateF.SentDeriv, 0,
                                             Model.Uf, 0,
                                             InitO.Deriv.Data, 0,
                                             RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, true);
                    }
                    else
                    {
                        Computelib.Zero(GateF.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim);
                    }
                }


                // deriv_z(t) = deriv_gate_o(t) * Zo';
                Computelib.Sgemm(GateO.SentDeriv, startIdx * Model.CellDim,
                                 Model.Zo, 0,
                                 Z.SentDeriv, startIdx * Model.AttentionDim,
                                 Sum, Model.CellDim, Model.AttentionDim,
                                 0, 1, false, true);

                // deriv_z(t) += deriv_gate_i(t) * Zi';
                Computelib.Sgemm(GateI.SentDeriv, startIdx * Model.CellDim,
                                 Model.Zi, 0,
                                 Z.SentDeriv, startIdx * Model.AttentionDim,
                                 Sum, Model.CellDim, Model.AttentionDim,
                                 1, 1, false, true);

                // deriv_z(t) += deriv_cHat(t) * Zc';
                Computelib.Sgemm(CHat.SentDeriv, startIdx * Model.CellDim,
                                   Model.Zc, 0,
                                   Z.SentDeriv, startIdx * Model.AttentionDim,
                                   Sum, Model.CellDim, Model.AttentionDim,
                                   1, 1, false, true);

                // deriv_z(t) += deriv_gate_f(t) * Zf';
                Computelib.Sgemm(GateF.SentDeriv, startIdx * Model.CellDim,
                                   Model.Zf, 0,
                                   Z.SentDeriv, startIdx * Model.AttentionDim,
                                   Sum, Model.CellDim, Model.AttentionDim,
                                   1, 1, false, true);

                if (i > 0)
                { 
                    attentLib.AttentionBackwardData(Output.SentDeriv, preStartIdx * Model.CellDim, //CudaPieceInt.Empty, 0,
                        RecurrentInfo.LagSeqElement, startIdx, Z.SentDeriv, startIdx * Model.AttentionDim, i, Sum);
                }
                else if(InitO != null)
                {
                    attentLib.AttentionBackwardData(InitO.Deriv.Data, 0, //CudaPieceInt.Empty, 0,
                        RecurrentInfo.LagSeqElement, 0, Z.SentDeriv, 0, i, Sum);
                }
            }
        }

        //public static void LSTMBackwardData(IMathOperationManager Computelib, SeqDenseBatchData Input, HiddenBatchData InitO, HiddenBatchData InitC, SeqHelpData HelpInput, LSTMCell Model,
        //    SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseBatchData C, SeqDenseBatchData TanhC,
        //    SeqDenseBatchData Output)
        //{
        //    RecurrentBackwardData(Computelib, Input.SampleIdx, Input.BatchSize, Input.SentSize, InitO, InitC, HelpInput, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);

        //    Computelib.Sgemm(GateI.SentDeriv, 0, Model.Wi, 0, Input.SentDeriv, 0, Input.SentSize,
        //            Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);

        //    Computelib.Sgemm(CHat.SentDeriv, 0, Model.Wc, 0, Input.SentDeriv, 0, Input.SentSize,
        //            Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);

        //    Computelib.Sgemm(GateF.SentDeriv, 0, Model.Wf, 0, Input.SentDeriv, 0, Input.SentSize,
        //            Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);

        //    Computelib.Sgemm(GateO.SentDeriv, 0, Model.Wo, 0, Input.SentDeriv, 0, Input.SentSize,
        //            Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
        //}

        public static void LSTMBackwardDatav2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC,
            SeqDenseRecursiveData Output)
        {
            RecurrentBackwardDatav2(Computelib, InitO, InitC, Input.RecurrentInfo, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);

            if (Input.SentDeriv != null && !Input.SentDeriv.IsEmpty)
            {
                Computelib.Sgemm(GateI.SentDeriv, 0, Model.Wi, 0, Input.SentDeriv, 0, Input.SentSize, Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
                Computelib.Sgemm(CHat.SentDeriv, 0, Model.Wc, 0, Input.SentDeriv, 0, Input.SentSize, Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
                Computelib.Sgemm(GateF.SentDeriv, 0, Model.Wf, 0, Input.SentDeriv, 0, Input.SentSize, Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
                Computelib.Sgemm(GateO.SentDeriv, 0, Model.Wo, 0, Input.SentDeriv, 0, Input.SentSize, Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
            }
        }

        public static void LSTMBackwardDatav2(IMathOperationManager Computelib, SeqSparseBatchData Input, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC,
            SeqDenseRecursiveData Output)
        {
            RecurrentBackwardDatav2(Computelib, InitO, InitC, Output.RecurrentInfo, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
        }

        //public static void LSTMAttentionBackwardData(IMathOperationManager Computelib, SeqDenseBatchData Input, HiddenBatchData InitO, HiddenBatchData InitC,
        //    SeqHelpData HelpInput, LSTMCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z,
        //    SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseBatchData C, SeqDenseBatchData TanhC,
        //    SeqDenseBatchData Output)
        //{
        //    RecurrentAttentionBackwardData(Computelib, Input.SampleIdx, Input.BatchSize, Input.SentSize, InitO, InitC, HelpInput, Model,
        //        attentLib, Z, GateO, GateI, CHat, GateF, C, TanhC, Output);

        //    Computelib.Sgemm(GateI.SentDeriv, 0, Model.Wi, 0, Input.SentDeriv, 0, Input.SentSize,
        //            Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);

        //    Computelib.Sgemm(CHat.SentDeriv, 0, Model.Wc, 0, Input.SentDeriv, 0, Input.SentSize,
        //            Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);

        //    Computelib.Sgemm(GateF.SentDeriv, 0, Model.Wf, 0, Input.SentDeriv, 0, Input.SentSize,
        //            Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);

        //    Computelib.Sgemm(GateO.SentDeriv, 0, Model.Wo, 0, Input.SentDeriv, 0, Input.SentSize,
        //            Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
        //}

        public static void LSTMAttentionBackwardDatav2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, HiddenBatchData InitO, HiddenBatchData InitC,
            LSTMCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z, SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat,
            SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC, SeqDenseRecursiveData Output)
        {
            RecurrentAttentionBackwardDatav2(Computelib, InitO, InitC, Input.RecurrentInfo, Model, attentLib, Z, GateO, GateI, CHat, GateF, C, TanhC, Output);

            if (Input.SentDeriv != null && !Input.SentDeriv.IsEmpty)
            {
                Computelib.Sgemm(GateI.SentDeriv, 0, Model.Wi, 0, Input.SentDeriv, 0, Input.SentSize, Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
                Computelib.Sgemm(CHat.SentDeriv, 0, Model.Wc, 0, Input.SentDeriv, 0, Input.SentSize, Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
                Computelib.Sgemm(GateF.SentDeriv, 0, Model.Wf, 0, Input.SentDeriv, 0, Input.SentSize, Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
                Computelib.Sgemm(GateO.SentDeriv, 0, Model.Wo, 0, Input.SentDeriv, 0, Input.SentSize, Model.CellDim, Model.InputFeatureDim, 1, 1, false, true);
            }
        }
        public static void LSTMAttentionBackwardDatav2(IMathOperationManager Computelib, SeqSparseBatchData Input, HiddenBatchData InitO, HiddenBatchData InitC,
            LSTMCell Model, IAttentionComputeLib attentLib, SeqDenseBatchData Z, SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat,
            SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC, SeqDenseRecursiveData Output)
        {
            RecurrentAttentionBackwardDatav2(Computelib, InitO, InitC, Output.RecurrentInfo, Model, attentLib, Z, GateO, GateI, CHat, GateF, C, TanhC, Output);
        }
    }

    public class LSTMComputeBackwardWeightLib
    {
        
        public static void SparseBackwardWeightv2(IMathOperationManager Computelib, CudaPieceInt SequenceIdx, CudaPieceInt FeaIdx, CudaPieceFloat FeaValue, int SentSize,
            RecurrentSeqInfo RecurrentInfo, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF)
        {
            Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue,
                                                GateI.SentDeriv, 0,
                                                Model.WiGrad, 0,
                                                SentSize, Model.InputFeatureDim, Model.CellDim,
                                                CudaPieceInt.Empty, 0, RecurrentInfo.MapForward, 0, CudaPieceInt.Empty, 0,
                                                1, 1, true, false);

            
            Computelib.ColumnWiseSum(GateI.SentDeriv, Model.BiGrad, SentSize, Model.CellDim, 1);
            //Model.BiMatrixOptimizer.AfterGradient();

            //Model.BiGrad.SyncToCPU();

            /*Memory C*/
            //Model.WcMatrixOptimizer.BeforeGradient();
            Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue,
                                                CHat.SentDeriv, 0,
                                                Model.WcGrad, 0,
                                                SentSize, Model.InputFeatureDim, Model.CellDim,
                                                CudaPieceInt.Empty, 0, RecurrentInfo.MapForward, 0, CudaPieceInt.Empty, 0,
                                                1, 1, true, false);

            //Model.WcMatrixOptimizer.AfterGradient();
            //Model.BcMatrixOptimizer.BeforeGradient();
            Computelib.ColumnWiseSum(CHat.SentDeriv, Model.BcGrad, SentSize, Model.CellDim, 1);
            //Model.BcMatrixOptimizer.AfterGradient();

            /*Gate F*/
            //Model.WfMatrixOptimizer.BeforeGradient();
            Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue,
                                                GateF.SentDeriv, 0,
                                                Model.WfGrad, 0,
                                                SentSize, Model.InputFeatureDim, Model.CellDim,
                                                CudaPieceInt.Empty, 0, RecurrentInfo.MapForward, 0, CudaPieceInt.Empty, 0,
                                                1, 1, true, false);
            //Model.WfMatrixOptimizer.AfterGradient();
            //Model.BfMatrixOptimizer.BeforeGradient();
            Computelib.ColumnWiseSum(GateF.SentDeriv, Model.BfGrad, SentSize, Model.CellDim, 1);
            //Model.BfMatrixOptimizer.AfterGradient();

            //GateF.SentDeriv.SyncToCPU();
            //Model.BfGrad.SyncToCPU();


            /*Gate O*/
            //Model.WoMatrixOptimizer.BeforeGradient();
            Computelib.SparseSgemmMask(SequenceIdx, FeaIdx, FeaValue,
                                                GateO.SentDeriv, 0,
                                                Model.WoGrad, 0,
                                                SentSize, Model.InputFeatureDim, Model.CellDim,
                                                CudaPieceInt.Empty, 0, RecurrentInfo.MapForward, 0, CudaPieceInt.Empty, 0,
                                                1, 1, true, false);
            //Model.WoMatrixOptimizer.AfterGradient();
            //Model.BoMatrixOptimizer.BeforeGradient();
            Computelib.ColumnWiseSum(GateO.SentDeriv, Model.BoGrad, SentSize, Model.CellDim, 1);
            //Model.BoMatrixOptimizer.AfterGradient();
        }

        public static void DenseBackwardWeight(IMathOperationManager Computelib, CudaPieceFloat SentInput, int SentSize,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF)
        {
            /*Gate I*/
            Computelib.Sgemm(SentInput, 0, GateI.SentDeriv, 0, Model.WiGrad, 0,
                                    SentSize, Model.InputFeatureDim, Model.CellDim, 1, 1, true, false);
            Computelib.ColumnWiseSum(GateI.SentDeriv, Model.BiGrad, SentSize, Model.CellDim, 1);

            /*Memory C*/
            Computelib.Sgemm(SentInput, 0, CHat.SentDeriv, 0, Model.WcGrad, 0,
                                    SentSize, Model.InputFeatureDim, Model.CellDim, 1, 1, true, false);
            Computelib.ColumnWiseSum(CHat.SentDeriv, Model.BcGrad, SentSize, Model.CellDim, 1);

            /*Gate F*/
            Computelib.Sgemm(SentInput, 0, GateF.SentDeriv, 0, Model.WfGrad, 0,
                                    SentSize, Model.InputFeatureDim, Model.CellDim, 1, 1, true, false);
            Computelib.ColumnWiseSum(GateF.SentDeriv, Model.BfGrad, SentSize, Model.CellDim, 1);

            /*Gate O*/
            Computelib.Sgemm(SentInput, 0, GateO.SentDeriv, 0, Model.WoGrad, 0,
                                    SentSize, Model.InputFeatureDim, Model.CellDim, 1, 1, true, false);
            Computelib.ColumnWiseSum(GateO.SentDeriv, Model.BoGrad, SentSize, Model.CellDim, 1);
        }

        public static void RecurrentBackwardWeightv2(IMathOperationManager Computelib, HiddenBatchData InitO, HiddenBatchData InitC,
            RecurrentSeqInfo RecurrentInfo, LSTMCell Model, SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C,
            SeqDenseBatchData TanhC, SeqDenseRecursiveData Output, CudaPieceFloat lag1O)
        {
            /*Memory Cell 2 Output*/
            Computelib.Sgemm(C.SentOutput, 0,
                             GateO.SentDeriv, 0,
                             Model.VoGrad, 0,
                             RecurrentInfo.SentSize, Model.CellDim, Model.CellDim,
                             1, 1, true, false);

            //put o(t-1) -> lag1O,
            Computelib.Matrix_AdditionMask(Output.SentOutput, 0, RecurrentInfo.RecurrentBackward, RecurrentInfo.BatchSize,
                                           lag1O, 0, CudaPieceInt.Empty, 0,
                                           lag1O, 0, CudaPieceInt.Empty, 0,
                                           Model.CellDim, RecurrentInfo.SentSize - RecurrentInfo.BatchSize, 1, 0, 0);

            /*Recurrent GateI - H*/
            Computelib.Sgemm(lag1O, 0,
                             GateI.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim,
                             Model.UiGrad, 0,
                             RecurrentInfo.SentSize - RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            if (InitO != null)
            {
                Computelib.Sgemm(InitO.Output.Data, 0, GateI.SentDeriv, 0, Model.UiGrad, 0,
                    RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            }

            /*Recurrent MemoryC - H*/
            Computelib.Sgemm(lag1O, 0,
                             CHat.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim,
                             Model.UcGrad, 0,
                             RecurrentInfo.SentSize - RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            if (InitO != null)
            {
                Computelib.Sgemm(InitO.Output.Data, 0, CHat.SentDeriv, 0, Model.UcGrad, 0,
                    RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            }

            /*Recurrent GateF - H*/
            Computelib.Sgemm(lag1O, 0,
                             GateF.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim,
                             Model.UfGrad, 0,
                             RecurrentInfo.SentSize - RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            if (InitO != null && InitC != null)
            {
                Computelib.Sgemm(InitO.Output.Data, 0, GateF.SentDeriv, 0, Model.UfGrad, 0,
                    RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            }

            /*Recurrent GateO - H*/
            Computelib.Sgemm(lag1O, 0,
                             GateO.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim,
                             Model.UoGrad, 0,
                             RecurrentInfo.SentSize - RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            if (InitO != null)
            { 
                Computelib.Sgemm(InitO.Output.Data, 0, GateO.SentDeriv, 0, Model.UoGrad, 0,
                    RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            }
        }

        public static void RecurrentAttentionBackwardWeightv2(IMathOperationManager Computelib, // CudaPieceInt SampleIdx, int BatchSize, int SentSize,
            HiddenBatchData InitO, HiddenBatchData InitC, RecurrentSeqInfo RecurrentInfo, LSTMCell Model,
            IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C,
            SeqDenseBatchData TanhC, SeqDenseRecursiveData Output, CudaPieceFloat lag1O)
        {
            /*Memory Cell 2 Output*/
            Computelib.Sgemm(C.SentOutput, 0,
                             GateO.SentDeriv, 0,
                             Model.VoGrad, 0,
                             RecurrentInfo.SentSize, Model.CellDim, Model.CellDim,
                             1, 1, true, false);

            //put o(t-1) -> lag1O,
            Computelib.Matrix_AdditionMask(Output.SentOutput, 0, RecurrentInfo.RecurrentBackward, RecurrentInfo.BatchSize,
                                           lag1O, 0, CudaPieceInt.Empty, 0,
                                           lag1O, 0, CudaPieceInt.Empty, 0,
                                           Model.CellDim, RecurrentInfo.SentSize - RecurrentInfo.BatchSize, 1, 0, 0);

            /*Recurrent GateI - H*/
            Computelib.Sgemm(lag1O, 0,
                             GateI.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim,
                             Model.UiGrad, 0,
                             RecurrentInfo.SentSize - RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            if (InitO != null)
                Computelib.Sgemm(InitO.Output.Data, 0, GateI.SentDeriv, 0, Model.UiGrad, 0,
                                 RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);

            /*Recurrent MemoryC - H*/
            Computelib.Sgemm(lag1O, 0,
                             CHat.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim,
                             Model.UcGrad, 0,
                             RecurrentInfo.SentSize - RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            if (InitO != null)
                Computelib.Sgemm(InitO.Output.Data, 0, CHat.SentDeriv, 0, Model.UcGrad, 0,
                                 RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim,
                                 1, 1, true, false);

            /*Recurrent GateF - H*/
            Computelib.Sgemm(lag1O, 0,
                             GateF.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim,
                             Model.UfGrad, 0,
                             RecurrentInfo.SentSize - RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            if (InitO != null && InitC != null)
                Computelib.Sgemm(InitO.Output.Data, 0, GateF.SentDeriv, 0, Model.UfGrad, 0,
                                 RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);

            /*Recurrent GateO - H*/
            Computelib.Sgemm(lag1O, 0,
                             GateO.SentDeriv, RecurrentInfo.BatchSize * Model.CellDim,
                             Model.UoGrad, 0,
                             RecurrentInfo.SentSize - RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);
            if (InitO != null)
                Computelib.Sgemm(InitO.Output.Data, 0, GateO.SentDeriv, 0, Model.UoGrad, 0,
                                 RecurrentInfo.BatchSize, Model.CellDim, Model.CellDim, 1, 1, true, false);

            Computelib.Sgemm(Z.SentOutput, 0, // RecurrentInfo.BatchSize * Model.AttentionDim,
                             GateI.SentDeriv, 0, // RecurrentInfo.BatchSize * Model.CellDim,
                             Model.ZiGrad, 0,
                             RecurrentInfo.SentSize /*- RecurrentInfo.BatchSize*/, Model.AttentionDim, Model.CellDim,
                             1, 1, true, false);

            Computelib.Sgemm(Z.SentOutput, 0, // RecurrentInfo.BatchSize * Model.AttentionDim,
                             GateF.SentDeriv, 0, // RecurrentInfo.BatchSize * Model.CellDim,
                             Model.ZfGrad, 0,
                             RecurrentInfo.SentSize /*- RecurrentInfo.BatchSize*/, Model.AttentionDim, Model.CellDim,
                             1, 1, true, false);

            Computelib.Sgemm(Z.SentOutput, 0, // RecurrentInfo.BatchSize * Model.AttentionDim,
                             CHat.SentDeriv, 0, // RecurrentInfo.BatchSize * Model.CellDim,
                             Model.ZcGrad, 0,
                             RecurrentInfo.SentSize /*- RecurrentInfo.BatchSize*/, Model.AttentionDim, Model.CellDim,
                             1, 1, true, false);

            Computelib.Sgemm(Z.SentOutput, 0, //RecurrentInfo.BatchSize * Model.AttentionDim,
                             GateO.SentDeriv, 0, //RecurrentInfo.BatchSize * Model.CellDim,
                             Model.ZoGrad, 0,
                             RecurrentInfo.SentSize /*- RecurrentInfo.BatchSize*/, Model.AttentionDim, Model.CellDim,
                             1, 1, true, false);

            attentLib.AttentionBackwardWeight(InitO.Output.Data, Output.SentOutput, RecurrentInfo, Output.Dim, Z.SentDeriv, Model.AttentionDim);
        }

        public static void LSTMBackwardWeightv2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC,
            SeqDenseRecursiveData Output, CudaPieceFloat lag1O)
        {
            DenseBackwardWeight(Computelib, Input.SentOutput, Input.SentSize, InitO, InitC, Model, GateO, GateI, CHat, GateF);
            RecurrentBackwardWeightv2(Computelib, InitO, InitC, Input.RecurrentInfo, Model, GateO, GateI, CHat, GateF, C, TanhC, Output, lag1O);
        }

        public static void LSTMBackwardWeightv2(IMathOperationManager Computelib, SeqSparseBatchData Input, HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC,
            SeqDenseRecursiveData Output, CudaPieceFloat lag1O)
        {
            SparseBackwardWeightv2(Computelib, Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Input.SentSize, Output.RecurrentInfo, InitO, InitC, Model, GateO, GateI, CHat, GateF);

            RecurrentBackwardWeightv2(Computelib, InitO, InitC, Output.RecurrentInfo, Model, GateO, GateI, CHat, GateF, C, TanhC, Output, lag1O);
        }


        public static void LSTMAttentionBackwardWeightv2(IMathOperationManager Computelib, SeqDenseRecursiveData Input, 
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC,
            SeqDenseRecursiveData Output, CudaPieceFloat lag1O)
        {
            DenseBackwardWeight(Computelib, Input.SentOutput, Input.SentSize, InitO, InitC, Model, GateO, GateI, CHat, GateF);
            RecurrentAttentionBackwardWeightv2(Computelib, InitO, InitC, Input.RecurrentInfo, Model, attentLib, Z, GateO, GateI, CHat, GateF, C, TanhC, Output, lag1O);
        }

        public static void LSTMAttentionBackwardWeightv2(IMathOperationManager Computelib, SeqSparseBatchData Input,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model,
            IAttentionComputeLib attentLib, SeqDenseBatchData Z,
            SeqDenseBatchData GateO, SeqDenseBatchData GateI, SeqDenseBatchData CHat, SeqDenseBatchData GateF, SeqDenseRecursiveData C, SeqDenseBatchData TanhC,
            SeqDenseRecursiveData Output, CudaPieceFloat lag1O)
        {
            SparseBackwardWeightv2(Computelib, Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Input.SentSize, Output.RecurrentInfo, InitO, InitC, Model, GateO, GateI, CHat, GateF);
            RecurrentAttentionBackwardWeightv2(Computelib, InitO, InitC, Output.RecurrentInfo, Model, attentLib, Z, GateO, GateI, CHat, GateF, C, TanhC, Output, lag1O);
        }

    }

    public class LSTMComputeDecodeLib
    {

        /// <summary>
        /// Given InitO, InitC, and Words ,output newO and newC.
        /// </summary>
        /// <param name="Computelib"></param>
        /// <param name="?"></param>
        public static void LSTMForwardDecode(IMathOperationManager Computelib, int BatchSize, CudaPieceInt SmpIndex, CudaPieceInt WordIndex, CudaPieceInt LagInfo, int WordDim,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, HiddenBatchData newO, HiddenBatchData newC,
            SeqDenseBatchData tmpGateO, SeqDenseBatchData tmpGateI, SeqDenseBatchData tmpCHat, SeqDenseBatchData tmpGateF, SeqDenseBatchData tmpTanhC)
        {
            /*Wi X -> GateI*/
            Computelib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, Model.Wi, 0, tmpGateI.SentOutput, 0, BatchSize,
                    WordDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateI.SentOutput, Model.Bi, BatchSize, Model.CellDim);

            /*Wc X -> MemoryC*/
            Computelib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, Model.Wc, 0, tmpCHat.SentOutput, 0, BatchSize,
                WordDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpCHat.SentOutput, Model.Bc, BatchSize, Model.CellDim);

            /*Wf X -> GateF*/
            Computelib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, Model.Wf, 0, tmpGateF.SentOutput, 0, BatchSize,
                WordDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateF.SentOutput, Model.Bf, BatchSize, Model.CellDim);

            /*Wo x -> GateO*/
            Computelib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, Model.Wo, 0, tmpGateO.SentOutput, 0, BatchSize,
                WordDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateO.SentOutput, Model.Bo, BatchSize, Model.CellDim);

            /// obtain output(t-1)
            //gate_i += U_{i} * InitO
            Computelib.SgemmMask(InitO.Output.Data, 0, Model.Ui, 0, tmpGateI.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,
                    LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);

            //gate_f += U_{f} * InitO
            Computelib.SgemmMask(InitO.Output.Data, 0, Model.Uf, 0, tmpGateF.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,
                    LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);

            //c_hat += U_{c} * InitO
            Computelib.SgemmMask(InitO.Output.Data, 0, Model.Uc, 0, tmpCHat.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,
                    LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);

            //gate_o += U_{o} * InitO
            Computelib.SgemmMask(InitO.Output.Data, 0, Model.Uo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,
                    LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);

            //gate_i = sigmoid(gate_i)
            Computelib.Logistic(tmpGateI.SentOutput, 0, tmpGateI.SentOutput, 0, BatchSize * Model.CellDim, 1);

            //c_hat = tanh(c_hat)
            Computelib.Tanh(tmpCHat.SentOutput, 0, tmpCHat.SentOutput, 0, BatchSize * Model.CellDim);

            //c = gate_i @ c_hat
            Computelib.ElementwiseProduct(tmpGateI.SentOutput, 0, tmpCHat.SentOutput, 0,
                newC.Output.Data, 0, BatchSize, Model.CellDim, 0);

            //gate_f = sigmoid(gate_f)
            Computelib.Logistic(tmpGateF.SentOutput, 0, tmpGateF.SentOutput, 0, BatchSize * Model.CellDim, 1);

            /// c(t) = c(t) + gate_f(t) @ initC
            if (InitC != null)
                Computelib.ElementwiseProductMask(InitC.Output.Data, 0, tmpGateF.SentOutput, 0,
                    newC.Output.Data, 0, LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, BatchSize, Model.CellDim, 1, 1);

            //gate_o += Vo * c(t)
            Computelib.Sgemm(newC.Output.Data, 0, Model.Vo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_o = simgoid(gate_o)
            Computelib.Logistic(tmpGateO.SentOutput, 0, tmpGateO.SentOutput, 0, BatchSize * Model.CellDim, 1);

            //tanhc = tanh(c)
            Computelib.Tanh(newC.Output.Data, 0, tmpTanhC.SentOutput, 0, BatchSize * Model.CellDim);

            //o = gate_o @ tanhc
            Computelib.ElementwiseProduct(tmpGateO.SentOutput, 0, tmpTanhC.SentOutput, 0,
                newO.Output.Data, 0, BatchSize, Model.CellDim, 0);
        }

        /// <summary>
        /// Given InitO, InitC, and Words ,output newO and newC.
        /// </summary>
        /// <param name="Computelib"></param>
        /// <param name="?"></param>
        public static void LSTMForwardDecode(IMathOperationManager Computelib, int BatchSize, CudaPieceFloat Input, CudaPieceInt LagInfo, int InputDim,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, HiddenBatchData newO, HiddenBatchData newC,
            CudaPieceFloat tmpO, SeqDenseBatchData tmpGateO, SeqDenseBatchData tmpGateI, SeqDenseBatchData tmpCHat, SeqDenseBatchData tmpGateF, SeqDenseBatchData tmpTanhC)
        {
            /*Wi X -> GateI*/
            Computelib.Sgemm(Input, 0, Model.Wi, 0, tmpGateI.SentOutput, 0, BatchSize, InputDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateI.SentOutput, Model.Bi, BatchSize, Model.CellDim);

            /*Wc X -> MemoryC*/
            Computelib.Sgemm(Input, 0, Model.Wc, 0, tmpCHat.SentOutput, 0, BatchSize, InputDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpCHat.SentOutput, Model.Bc, BatchSize, Model.CellDim);

            /*Wf X -> GateF*/
            Computelib.Sgemm(Input, 0, Model.Wf, 0, tmpGateF.SentOutput, 0, BatchSize, InputDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateF.SentOutput, Model.Bf, BatchSize, Model.CellDim);

            /*Wo x -> GateO*/
            Computelib.Sgemm(Input, 0, Model.Wo, 0, tmpGateO.SentOutput, 0, BatchSize, InputDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateO.SentOutput, Model.Bo, BatchSize, Model.CellDim);

            Computelib.Matrix_AdditionMask(InitO.Output.Data, 0, LagInfo, 0,
                                           tmpO, 0, CudaPieceInt.Empty, 0,
                                           tmpO, 0, CudaPieceInt.Empty, 0,
                                           Model.CellDim, BatchSize, 1, 0, 0);

            /// obtain output(t-1)
            //gate_i += U_{i} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Ui, 0, tmpGateI.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);
            //Computelib.SgemmMask(tmpO, 0, Model.Ui, 0, tmpGateI.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,
            //        LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);

            //gate_f += U_{f} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uf, 0, tmpGateF.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);
            //Computelib.SgemmMask(tmpO, 0, Model.Uf, 0, tmpGateF.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,
            //        LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);

            //c_hat += U_{c} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uc, 0, tmpCHat.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);
            //Computelib.SgemmMask(tmpO, 0, Model.Uc, 0, tmpCHat.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,
            //        LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);

            //gate_o += U_{o} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);
            //Computelib.SgemmMask(tmpO, 0, Model.Uo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,
            //        LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 1, 1, false, false);

            //gate_i = sigmoid(gate_i)
            Computelib.Logistic(tmpGateI.SentOutput, 0, tmpGateI.SentOutput, 0, BatchSize * Model.CellDim, 1);

            //c_hat = tanh(c_hat)
            Computelib.Tanh(tmpCHat.SentOutput, 0, tmpCHat.SentOutput, 0, BatchSize * Model.CellDim);

            //c = gate_i @ c_hat
            Computelib.ElementwiseProduct(tmpGateI.SentOutput, 0, tmpCHat.SentOutput, 0,
                newC.Output.Data, 0, BatchSize, Model.CellDim, 0);

            //gate_f = sigmoid(gate_f)
            Computelib.Logistic(tmpGateF.SentOutput, 0, tmpGateF.SentOutput, 0, BatchSize * Model.CellDim, 1);

            /// c(t) = c(t) + gate_f(t) @ initC
            Computelib.ElementwiseProductMask(InitC.Output.Data, 0, tmpGateF.SentOutput, 0,
                    newC.Output.Data, 0, LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, BatchSize, Model.CellDim, 1, 1);

            //gate_o += Vo * c(t)
            Computelib.Sgemm(newC.Output.Data, 0, Model.Vo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_o = simgoid(gate_o)
            Computelib.Logistic(tmpGateO.SentOutput, 0, tmpGateO.SentOutput, 0, BatchSize * Model.CellDim, 1);

            //tanhc = tanh(c)
            Computelib.Tanh(newC.Output.Data, 0, tmpTanhC.SentOutput, 0, BatchSize * Model.CellDim);

            //o = gate_o @ tanhc
            Computelib.ElementwiseProduct(tmpGateO.SentOutput, 0, tmpTanhC.SentOutput, 0,
                newO.Output.Data, 0, BatchSize, Model.CellDim, 0);
        }

        /// <summary>
        /// Given InitO, InitC, and Words ,output newO and newC.
        /// </summary>
        /// <param name="Computelib"></param>
        /// <param name="?"></param>
        public static void LSTMForwardAttentionDecode(IMathOperationManager Computelib, int BatchSize, CudaPieceFloat Input, CudaPieceInt LagInfo, int InputDim,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, HiddenBatchData newO, HiddenBatchData newC,
            CudaPieceFloat tmpO, IAttentionComputeLib attentLib, HiddenBatchData tmpZ, int[] batchIdArray,
            SeqDenseBatchData tmpGateO, SeqDenseBatchData tmpGateI, SeqDenseBatchData tmpCHat, SeqDenseBatchData tmpGateF, SeqDenseBatchData tmpTanhC)
        {
            /*Wi X -> GateI*/
            Computelib.Sgemm(Input, 0, Model.Wi, 0, tmpGateI.SentOutput, 0, BatchSize, InputDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateI.SentOutput, Model.Bi, BatchSize, Model.CellDim);

            /*Wc X -> MemoryC*/
            Computelib.Sgemm(Input, 0, Model.Wc, 0, tmpCHat.SentOutput, 0, BatchSize, InputDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpCHat.SentOutput, Model.Bc, BatchSize, Model.CellDim);

            /*Wf X -> GateF*/
            Computelib.Sgemm(Input, 0, Model.Wf, 0, tmpGateF.SentOutput, 0, BatchSize, InputDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateF.SentOutput, Model.Bf, BatchSize, Model.CellDim);

            /*Wo x -> GateO*/
            Computelib.Sgemm(Input, 0, Model.Wo, 0, tmpGateO.SentOutput, 0, BatchSize, InputDim, Model.CellDim, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateO.SentOutput, Model.Bo, BatchSize, Model.CellDim);


            Computelib.Matrix_AdditionMask(InitO.Output.Data, 0, LagInfo, 0,
                                           tmpO, 0, CudaPieceInt.Empty, 0,
                                           tmpO, 0, CudaPieceInt.Empty, 0,
                                           Model.CellDim, BatchSize, 1, 0, 0);

            // obtain attention from history memory.
            // tmpO(t-1) -> attention Z
            attentLib.AttentionForward(tmpO, 0,
                                       batchIdArray, 0,
                                       tmpZ.Output.Data, 0, 0, BatchSize);

            //gate_i += Z_{i} * z_{t}
            Computelib.Sgemm(tmpZ.Output.Data, 0, Model.Zi, 0, tmpGateI.SentOutput, 0,
                BatchSize, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

            //gate_f += Z_{f} * z_{t}
            Computelib.Sgemm(tmpZ.Output.Data, 0, Model.Zf, 0, tmpGateF.SentOutput, 0,
                BatchSize, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

            //c_hat += Z_{c} * z_{t}
            Computelib.Sgemm(tmpZ.Output.Data, 0, Model.Zc, 0, tmpCHat.SentOutput, 0,
                BatchSize, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

            //gate_o += Z_{o} * z_{t}
            Computelib.Sgemm(tmpZ.Output.Data, 0, Model.Zo, 0, tmpGateO.SentOutput, 0,
                BatchSize, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

            /// obtain output(t-1)
            //gate_i += U_{i} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Ui, 0, tmpGateI.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim,  1, 1, false, false);

            //gate_f += U_{f} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uf, 0, tmpGateF.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //c_hat += U_{c} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uc, 0, tmpCHat.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_o += U_{o} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_i = sigmoid(gate_i)
            Computelib.Logistic(tmpGateI.SentOutput, 0, tmpGateI.SentOutput, 0, BatchSize * Model.CellDim, 1);

            //c_hat = tanh(c_hat)
            Computelib.Tanh(tmpCHat.SentOutput, 0, tmpCHat.SentOutput, 0, BatchSize * Model.CellDim);

            //c = gate_i @ c_hat
            Computelib.ElementwiseProduct(tmpGateI.SentOutput, 0, tmpCHat.SentOutput, 0,
                newC.Output.Data, 0, BatchSize, Model.CellDim, 0);

            //gate_f = sigmoid(gate_f)
            Computelib.Logistic(tmpGateF.SentOutput, 0, tmpGateF.SentOutput, 0, BatchSize * Model.CellDim, 1);

            /// c(t) = c(t) + gate_f(t) @ initC
            Computelib.ElementwiseProductMask(InitC.Output.Data, 0, tmpGateF.SentOutput, 0,
                    newC.Output.Data, 0, LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, BatchSize, Model.CellDim, 1, 1);

            //gate_o += Vo * c(t)
            Computelib.Sgemm(newC.Output.Data, 0, Model.Vo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_o = simgoid(gate_o)
            Computelib.Logistic(tmpGateO.SentOutput, 0, tmpGateO.SentOutput, 0, BatchSize * Model.CellDim, 1);

            //tanhc = tanh(c)
            Computelib.Tanh(newC.Output.Data, 0, tmpTanhC.SentOutput, 0, BatchSize * Model.CellDim);

            //o = gate_o @ tanhc
            Computelib.ElementwiseProduct(tmpGateO.SentOutput, 0, tmpTanhC.SentOutput, 0,
                newO.Output.Data, 0, BatchSize, Model.CellDim, 0);
        }


        /// <summary>
        /// Given InitO, InitC, and Words ,output newO and newC.
        /// </summary>
        /// <param name="Computelib"></param>
        /// <param name="?"></param>
        public static void LSTMForwardAttentionDecode(IMathOperationManager Computelib, int BatchSize, CudaPieceInt SmpIndex, CudaPieceInt WordIndex, CudaPieceInt LagInfo, int WordDim,
            HiddenBatchData InitO, HiddenBatchData InitC, LSTMCell Model, HiddenBatchData newO, HiddenBatchData newC,
            CudaPieceFloat tmpO, IAttentionComputeLib attentLib, HiddenBatchData tmpZ, int[] batchIdArray,
            SeqDenseBatchData tmpGateO, SeqDenseBatchData tmpGateI, SeqDenseBatchData tmpCHat, SeqDenseBatchData tmpGateF, SeqDenseBatchData tmpTanhC)
        {
            /*Wi X -> GateI*/
            Computelib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, Model.Wi, 0, tmpGateI.SentOutput, 0, BatchSize,
                    WordDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateI.SentOutput, Model.Bi, BatchSize, Model.CellDim);

            /*Wc X -> MemoryC*/
            Computelib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, Model.Wc, 0, tmpCHat.SentOutput, 0, BatchSize,
                WordDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpCHat.SentOutput, Model.Bc, BatchSize, Model.CellDim);

            /*Wf X -> GateF*/
            Computelib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, Model.Wf, 0, tmpGateF.SentOutput, 0, BatchSize,
                WordDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateF.SentOutput, Model.Bf, BatchSize, Model.CellDim);

            /*Wo x -> GateO*/
            Computelib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, Model.Wo, 0, tmpGateO.SentOutput, 0, BatchSize,
                WordDim, Model.CellDim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
            Computelib.Matrix_Add_Linear(tmpGateO.SentOutput, Model.Bo, BatchSize, Model.CellDim);


            Computelib.Matrix_AdditionMask(InitO.Output.Data, 0, LagInfo, 0,
                                           tmpO, 0, CudaPieceInt.Empty, 0,
                                           tmpO, 0, CudaPieceInt.Empty, 0,
                                           Model.CellDim, BatchSize, 1, 0, 0);

            // obtain attention from history memory.
            // o(t-1) -> attention Z
            attentLib.AttentionForward(tmpO, 0, batchIdArray, 0, tmpZ.Output.Data, 0, 0, BatchSize);

            //gate_i += Z_{i} * z_{t}
            Computelib.Sgemm(tmpZ.Output.Data, 0, Model.Zi, 0, tmpGateI.SentOutput, 0,
                BatchSize, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

            //gate_f += Z_{f} * z_{t}
            Computelib.Sgemm(tmpZ.Output.Data, 0, Model.Zf, 0, tmpGateF.SentOutput, 0,
                BatchSize, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

            //c_hat += Z_{c} * z_{t}
            Computelib.Sgemm(tmpZ.Output.Data, 0, Model.Zc, 0, tmpCHat.SentOutput, 0,
                BatchSize, Model.AttentionDim, Model.CellDim, 1, 1, false, false);

            //gate_o += Z_{o} * z_{t}
            Computelib.Sgemm(tmpZ.Output.Data, 0, Model.Zo, 0, tmpGateO.SentOutput, 0,
                BatchSize, Model.AttentionDim, Model.CellDim, 1, 1, false, false);


            /// obtain output(t-1)
            //gate_i += U_{i} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Ui, 0, tmpGateI.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_f += U_{f} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uf, 0, tmpGateF.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //c_hat += U_{c} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uc, 0, tmpCHat.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_o += U_{o} * InitO
            Computelib.Sgemm(tmpO, 0, Model.Uo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_i = sigmoid(gate_i)
            Computelib.Logistic(tmpGateI.SentOutput, 0, tmpGateI.SentOutput, 0, BatchSize * Model.CellDim, 1);

            //c_hat = tanh(c_hat)
            Computelib.Tanh(tmpCHat.SentOutput, 0, tmpCHat.SentOutput, 0, BatchSize * Model.CellDim);

            //c = gate_i @ c_hat
            Computelib.ElementwiseProduct(tmpGateI.SentOutput, 0, tmpCHat.SentOutput, 0,
                newC.Output.Data, 0, BatchSize, Model.CellDim, 0);

            //gate_f = sigmoid(gate_f)
            Computelib.Logistic(tmpGateF.SentOutput, 0, tmpGateF.SentOutput, 0, BatchSize * Model.CellDim, 1);

            /// c(t) = c(t) + gate_f(t) @ initC
            Computelib.ElementwiseProductMask(InitC.Output.Data, 0, tmpGateF.SentOutput, 0,
                    newC.Output.Data, 0, LagInfo, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, BatchSize, Model.CellDim, 1, 1);

            //gate_o += Vo * c(t)
            Computelib.Sgemm(newC.Output.Data, 0, Model.Vo, 0, tmpGateO.SentOutput, 0, BatchSize, Model.CellDim, Model.CellDim, 1, 1, false, false);

            //gate_o = simgoid(gate_o)
            Computelib.Logistic(tmpGateO.SentOutput, 0, tmpGateO.SentOutput, 0, BatchSize * Model.CellDim, 1);

            //tanhc = tanh(c)
            Computelib.Tanh(newC.Output.Data, 0, tmpTanhC.SentOutput, 0, BatchSize * Model.CellDim);

            //o = gate_o @ tanhc
            Computelib.ElementwiseProduct(tmpGateO.SentOutput, 0, tmpTanhC.SentOutput, 0,
                newO.Output.Data, 0, BatchSize, Model.CellDim, 0);
        }

    }
}
