using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // fill up the indices vectors into embed memory.
    public class FillupTensorRunner : StructRunner
    {
        public CudaPieceInt DimFillsup { get; set; }
            
        public NdArrayData Memory { get; set; }
        public NdArrayData Data { get; set; }
        
        CudaPieceInt MemDims { get; set; }


        public FillupTensorRunner(CudaPieceInt idxes, NdArrayData memory, NdArrayData data, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
            DimFillsup = idxes;
            
            Memory = memory;
            Data = data;
            
            MemDims = new CudaPieceInt(Memory.Shape.Length, behavior.Device);
        }

        public override void Forward()
        {
            for(int i = 0; i < Memory.Shape.Length; i++)
            {
                MemDims[i] = Memory.Shape[i].Value;
            }    
            MemDims.SyncFromCPU();

            ComputeLib.NDArrayScatter(Memory.Output, Memory.Shape.Length, MemDims, Data.Output, DimFillsup, Data.Length, 0.0f, 1.0f, 1.0f, 0.0f);
        }

        public override void CleanDeriv()
        { }

        public override void Backward(bool isClearDeriv)
        {
            if(Memory.Deriv != null && !Memory.Deriv.IsEmpty && Data.Deriv != null && !Data.Deriv.IsEmpty)
            {
                ComputeLib.NDArrayScatter(Memory.Deriv, Memory.Shape.Length, MemDims, Data.Deriv, DimFillsup, Data.Length, 0.0f, 0.0f, 1.0f, 1.0f);
                //ComputeLib.NDArrayScatterBackward(Memory.Deriv, Memory.Shape.Length, MemDims, Data.Deriv, 
                //        DimFillsup, CudaPieceInt.Empty, CudaPieceInt.Empty, Data.Length, 1.0f);
            }
        }

    }
}
