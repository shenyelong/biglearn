using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class IntDivModRunner : StructRunner
    {
        public CudaPieceInt _Input { get; set; }
        public CudaPieceInt Div_Output { get; set; } 
        public CudaPieceInt Mod_Output { get; set; }

        int Num { get; set; }
        public IntDivModRunner(CudaPieceInt input, int num, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            _Input = input;
            Div_Output = new CudaPieceInt(input.Size, behavior.Device);
            Mod_Output = new CudaPieceInt(input.Size, behavior.Device);
            
            Num = num;
        }

        public override void Forward()
        {
            Div_Output.EffectiveSize = _Input.EffectiveSize;
            Mod_Output.EffectiveSize = _Input.EffectiveSize;

            _Input.SyncToCPU();
            for(int i = 0; i < _Input.EffectiveSize; i++)
            {
                Mod_Output[i] = _Input[i] % Num;
                Div_Output[i] = _Input[i] / Num;
            }
            
            Div_Output.SyncFromCPU();
            Mod_Output.SyncFromCPU();
        }
    }
}
