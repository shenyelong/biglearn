﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RNNSparseRunner: StructRunner<RNNCell, SeqSparseBatchData>
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        public RNNSparseRunner(RNNCell model, SeqSparseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new SeqDenseBatchData(
                        data.SampleIdx, data.SentMargin, 
                        Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim,
                        Behavior.Device);    
        }
        
        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            ///// WI * Xi = Yi;
            ///*Wi X -> Hidden*/
            ComputeLib.Sparse_Matrix_Multiply_INTEX_Weight(
                                                    Input.SequenceIdx,
                                                    Input.FeaIdx,
                                                    Input.FeaValue,
                                                    Input.SentSize,
                                                    Input.ElementSize,
                                                    Model.IMatrix.Output,
                                                    Output.SentOutput,
                                                    Model.HiddenStateDim,
                                                    0);
            /*Hidden + Bias*/
            /// Yi = Yi + Bias;
            ComputeLib.Matrix_Add_Linear(Output.SentOutput, Model.Bias.Output, Input.SentSize, Model.HiddenStateDim);

            //MathOperatorManager.GlobalInstance.
            /// Y0 = Af(Y0)
            /// Y1 = Af(Y1 + W0 * Y0)
            /// Y2 = Af(Y2 + W0 Y1 + W1 Y0)
            /// Y3 = Af(Y3 + W0 Y2 + W1 Y1)
            /// Y(i+1) = Af( \sum_(0..T) Wt * Y(i-t))
            /// T = HistoryLag;
            ComputeLib.RecursiveForwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Output.SentOutput, Model.HiddenStateDim,
                                                            Model.WMatrix.Output, Model.HistoryLag, (int)Model.Af, Output.SentOutput);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.RecursiveBackwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Output.SentOutput, Model.HiddenStateDim,
                     Model.WMatrix.Output, Model.HistoryLag, (int)Model.Af, Output.SentDeriv);
        }

        public override void Update()
        {
            //if(!IsDelegateOptimizer) Model.IMatrixOptimizer.BeforeGradient();
            ComputeLib.MatrixMultiplicationSparseLeftTranspose(Input.SequenceIdx, Input.SentSize, Input.FeaIdx,
                                        Input.FeaValue, Input.ElementSize, Output.SentDeriv,  Model.IMatrix.Deriv,
                                        Model.InputFeatureDim, Model.HiddenStateDim,
                                        1);
            //if (!IsDelegateOptimizer) Model.IMatrixOptimizer.AfterGradient();

            //if (!IsDelegateOptimizer) Model.WMatrixOptimizer.BeforeGradient();
            ComputeLib.RecursiveUpdateBatch(Input.SampleIdx, Input.BatchSize, Input.SentSize, Output.SentOutput, Model.HiddenStateDim,
                                        Output.SentDeriv, Model.HistoryLag, Model.WMatrix.Deriv, 1);
            //if (!IsDelegateOptimizer) Model.WMatrixOptimizer.AfterGradient();

            if (Model.IsBias)
            {
                //if (!IsDelegateOptimizer) Model.BiasOptimizer.BeforeGradient();
                ComputeLib.ColumnWiseSum(Output.SentDeriv, Model.Bias.Deriv, Output.SentSize, Model.HiddenStateDim, 1);
                //if (!IsDelegateOptimizer) Model.BiasOptimizer.AfterGradient();
            }
        }

    }
}
