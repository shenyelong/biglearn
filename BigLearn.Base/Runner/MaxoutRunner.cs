﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MaxoutRunner<IN> : StructRunner<Structure, IN> where IN : HiddenBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        CudaPieceInt LayerMaxPoolingIndex = null;

        int MaxoutDim { get; set; }
        int OutDim { get; set; }
        int InputFormat { get; set; }
        public MaxoutRunner(HiddenBatchData data, int maxoutDim, RunnerBehavior behavior, int inputFormat = 0) : base(Structure.Empty, data, behavior)
        {
            MaxoutDim = maxoutDim;
            OutDim = Input.Dim / MaxoutDim;
            InputFormat = inputFormat;

            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, OutDim, Behavior.RunMode, Behavior.Device);
            LayerMaxPoolingIndex = new CudaPieceInt(Input.MAX_BATCHSIZE * OutDim, true, Behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            ComputeLib.MaxoutPooling(Input.Output.Data, Input.BatchSize, Input.Dim, LayerMaxPoolingIndex, MaxoutDim, OutDim, Output.Output.Data, InputFormat);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.DerivMaxoutPooling(Input.Deriv.Data, Input.BatchSize, Input.Dim, LayerMaxPoolingIndex, MaxoutDim, OutDim, Output.Deriv.Data);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

    }
}
