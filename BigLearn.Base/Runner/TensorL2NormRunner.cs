using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorL2NormRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        IntPtr varState;
        IntArgument[] inout_dims;
        IntArgument[] reduce_dims;

        public TensorL2NormRunner(NdArrayData input, int reduceDim, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            

            inout_dims = Input.Shape;
            if(inout_dims.Length < 4)
            {
                inout_dims = new IntArgument[4];
                for(int i = 0; i < 4; i++)
                {
                    if(i < Input.Shape.Length) inout_dims[i] = Input.Shape[i];
                    else inout_dims[i] = new IntArgument("default_dim", 1);
                }
            }

            reduce_dims = new IntArgument[inout_dims.Length];
            for (int i = 0; i < inout_dims.Length; i++)
            {
                if(i == reduceDim) 
                {
                    reduce_dims[i] = new IntArgument(inout_dims[i].Name, 1); 
                    Logger.WriteLog("L2 norm layer {0}", inout_dims[i].Default);
                }
                else reduce_dims[i] = inout_dims[i];
            }

            Output = new NdArrayData(behavior.Device, reduce_dims); 

            varState = ComputeLib.RegisteTensorLazyReduce(1, inout_dims.Length, new Shape(inout_dims).RDimList, new Shape(reduce_dims).RDimList);
            Behavior.Resource.RegisteReduceState(varState);

            Behavior.Resource.RegisteFloatVector(new Shape(reduce_dims).DefaultSize, "tmp1");
        }
        
        public override void Forward()
        {
            ComputeLib.TensorReduceOp(Input.Output, Output.Output, 0, 1, varState);
        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        CudaPieceFloat tmp1 { get { return Behavior.Resource.MemF["tmp1"]; } } 

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            if(Input.Deriv != null && Input.Deriv != CudaPieceFloat.Empty)
            {
                ComputeLib.Reciprocal(Output.Output, tmp1, 0, 1, new Shape(reduce_dims).DefaultSize);

                ComputeLib.TensorOp(1, inout_dims.Length, new Shape(reduce_dims).RDimList, tmp1,
                                                          new Shape(reduce_dims).RDimList, Output.Deriv, 
                                                          new Shape(reduce_dims).RDimList, tmp1,
                                                          1, 1, 0);

                ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Input.Output, 
                                                          new Shape(reduce_dims).RDimList, tmp1, 
                                                          new Shape(inout_dims).RDimList, Input.Deriv,
                                                          1, 1, 1);
            }
        }
    }
}
