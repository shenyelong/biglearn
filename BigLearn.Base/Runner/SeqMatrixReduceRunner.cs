using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqMatrixReduceRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        SeqMatrixData InputA { get; set; }
        SeqVectorData InputB { get; set; }

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public SeqMatrixReduceRunner(SeqMatrixData inputA, SeqVectorData inputB, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = new MatrixData(InputA.Column, InputA.MaxSegment, Behavior.Device);
        }

        public override void Forward()
        {
            iteration++;
            Output.Row = InputA.Segment;

            if (Output.Row == 0) { return; }
            if (InputA.Segment != InputB.Segment) { throw new Exception(string.Format("Input A {0} and Input B {1} Segment doesn't match ", InputA.Segment, InputB.Segment)); }
            if (InputA.Row != InputB.Length) { throw new Exception(string.Format("Input A {0} and Input B {1} Row doesn't match ", InputA.Row, InputB.Length)); }
            
            ComputeLib.ColumnWiseSumMask(InputA.Output, 0, CudaPieceInt.Empty, 0,
                InputB.Output, InputA.SegmentIdx, InputA.Segment,
                Output.Output, 0, CudaPieceInt.Empty, 0,
                InputA.Row, InputA.Column, 0, 1);
        }

        public override void CleanDeriv()
        {
            if (Output.Row == 0) { return; }
            ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Row == 0) { return; }

            if (InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {
                ComputeLib.Scale_MatrixMask(Output.Deriv, 0, InputA.SegmentMargin, 0,
                                        InputA.Deriv, 0, CudaPieceInt.Empty, 0,
                                        InputA.Column, InputA.Row, InputB.Output, 1);
                //ComputeLib.Sgemm(Output.Deriv, 0, InputB.Output, 0, InputA.Deriv, 0, Output.Row, Output.Column, InputB.Row, 1, 1, false, true);
            }

            if (InputB.Deriv != null && !InputB.Deriv.IsEmpty)
            {
                ComputeLib.Inner_Product_v1(Output.Deriv, InputA.Output, InputB.Deriv, 
                        InputA.SegmentMargin, CudaPieceInt.Empty, Output.Row, InputA.Row, InputA.Row, InputA.Column, 1, 1);

                //(Output.Deriv, 0, InputA.Output, 0, InputB.Deriv, 0,
                //        InputA.SegmentMargin, CudaPieceInt.Empty, Output.Row, InputA.Row, InputA.Row,
                //        Memory.Dim, Util.GPUEpsilon);
                //ComputeLib.Sgemm(InputA.Output, 0, Output.Deriv, 0, InputB.Deriv, 0, InputA.Row, InputB.Row, InputB.Column, 1, 1, true, false);
            }
        }
    }

}
