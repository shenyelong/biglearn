using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorDotRunner : StructRunner
    {
        public NdArrayData InputA { get; set; }
        public NdArrayData InputB { get; set; }

        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }

        IntArgument[] reduce_dims { get; set; }
        IntArgument[] inout_dims { get; set; }

        IntPtr sumState;
        CudaPieceFloat tmp;

        public TensorDotRunner(NdArrayData inputA, NdArrayData inputB, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;

            Output = new NdArrayData(InputA.Shape, behavior.Device); 
            

            inout_dims = InputA.Shape;
            if(inout_dims.Length < 4)
            {
                inout_dims = new IntArgument[4];
                for(int i = 0; i < 4; i++)
                {
                    if(i < InputA.Shape.Length) inout_dims[i] = InputA.Shape[i];
                    else inout_dims[i] = new IntArgument("default_dim", 1);
                }
            }

            int target_dim_num = inout_dims.Length;

            reduce_dims = InputB.Shape;
            if(reduce_dims.Length < target_dim_num)
            {
                reduce_dims = new IntArgument[target_dim_num];
                for(int i = 0; i < target_dim_num; i++)
                {
                    if(i < InputB.Shape.Length) reduce_dims[i] = InputB.Shape[i];
                    else reduce_dims[i] = new IntArgument("default_dim", 1);
                }
            }
            
            if(InputA.MaxLength > InputB.MaxLength)
            {
                tmp = new CudaPieceFloat(new Shape(InputA.Shape).DefaultSize, behavior.Device);
                sumState = ComputeLib.RegisteTensorReduce(2, inout_dims.Length, new Shape(inout_dims).RDimList, new Shape(reduce_dims).RDimList);
            }
        }

        public override void Forward()
        {
            ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, InputA.Output, 
                                                      new Shape(reduce_dims).RDimList, InputB.Output,
                                                      new Shape(inout_dims).RDimList, Output.Output,
                                                      1, 1, 0);
        }

        public override void CleanDeriv()
        {
            if (Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            /// b = bwei b + awei * a;
            if(InputA.Deriv != null )
            {
                ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Output.Deriv, 
                                                 new Shape(reduce_dims).RDimList, InputB.Output,
                                                 new Shape(inout_dims).RDimList, InputA.Deriv,
                                                 1, 1, 1);
            }
            if(InputB.Deriv != null)
            {
                if(InputA.Length == InputB.Length)
                {
                    ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Output.Deriv, 
                                                 new Shape(inout_dims).RDimList, InputA.Output,
                                                 new Shape(inout_dims).RDimList, InputB.Deriv,
                                                 1, 1, 1);
                }
                else
                {
                    ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Output.Deriv, 
                                                 new Shape(inout_dims).RDimList, InputA.Output,
                                                 new Shape(inout_dims).RDimList, tmp,
                                                 1, 1, 0);

                    ComputeLib.TensorReduceOp(tmp, InputB.Deriv, 1, 1, sumState);
                }
            }
        }

    }
}
