﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// This class will be staled, please use VectorDumpV2Runner instead.
    /// </summary>
    public class VectorDumpRunner : StructRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public string FileName { get; set; }

        public bool IsTextFormat { get; set;}

        StreamWriter dumpWriter;
        BinaryWriter bw;
        int counter = 0;
        public VectorDumpRunner(DenseBatchData output, string fileName, bool isTextFormat = false)
            : base(Structure.Empty, new RunnerBehavior())
        {
            Output = output;
            FileName = fileName;
            IsTextFormat = isTextFormat;
        }

        public override void Forward()
        {
            Output.Data.SyncToCPU();
            if (IsTextFormat)
            {
                //dumpWriter.WriteLine(string.Join(",", Output.Data.Skip(i * Output.Stat.Dim).Take(Output.Stat.Dim)));
                for (int i = 0; i < Output.BatchSize; i++)
                {
                    StringBuilder sb = new StringBuilder();
                    int pos = i * Output.Dim;
                    for (int j = 0; j < Output.Dim; j++)
                    {
                        sb.Append(Output.Data[pos + j]).Append(",");
                    }
                    string str = sb.ToString().Trim(','); //.Remove(sb.Length - 2, 1).ToString();
                    dumpWriter.WriteLine(str);
                }
                dumpWriter.Flush();
            }
            else
            {
                for (int i = 0; i < Output.BatchSize; i++)
                {
                    int pos = i * Output.Dim;
                    for (int j = 0; j < Output.Dim; j++)
                    {
                        bw.Write(Output.Data[pos + j]);
                    }
                    counter++;
                }
            }
            
        }
        public override void Init()
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None, FileUtil.BUFFER_SIZE);
            if (IsTextFormat) dumpWriter = new StreamWriter(fs);
            else
            {
                bw = new BinaryWriter(fs);
                bw.Write(0);
                bw.Write(Output.Dim);
            }
        }

        public override void Complete()
        {
            if (dumpWriter != null)
                dumpWriter.Close();
            if (bw != null)
            {
                bw.Flush();
                bw.Seek(0, SeekOrigin.Begin);
                bw.Write(counter);
                bw.Close();
            }
        }
    }

    public class VectorDumpV2Runner : StructRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        
        BinaryWriter BW = null;
        StreamWriter TW = null;
        List<float[]> LW = null;

        int counter = 0;
        public VectorDumpV2Runner(DenseBatchData output, BinaryWriter w)
            : base(Structure.Empty, new RunnerBehavior())
        {
            Output = output;
            BW = w;
        }
        public VectorDumpV2Runner(DenseBatchData output, StreamWriter w)
            : base(Structure.Empty, new RunnerBehavior())
        {
            Output = output;
            TW = w;
        }

        public VectorDumpV2Runner(DenseBatchData output, List<float[]> w)
            : base(Structure.Empty, new RunnerBehavior())
        {
            Output = output;
            LW = w;
        }


        public override void Forward()
        {
            Output.Data.SyncToCPU();
            for (int i = 0; i < Output.BatchSize; i++)
            {
                int pos = i * Output.Dim;
                float[] m = new float[Output.Dim];
                //Array.Copy(Output.Data, pos, m, 0, Output.Stat.Dim);
                Output.Data.SyncToCPU(pos, m, 0, Output.Dim);

                if (BW != null)
                    for (int j = 0; j < Output.Dim; j++) BW.Write(Output.Data[pos + j]);
                if (TW != null)
                    TW.WriteLine(string.Join(",", m));
                if (LW != null)
                    LW.Add(m);
                counter++;
            }

        }
    }


    public class ModelDiskDumpRunner : StructRunner
    {
        public new Structure Model { get { return (Structure)base.Model; } set { base.Model = value; } }
        public string FileName { get; set; }

        public int EpochNum = 0;
        public int MiniBatchNum = 0;
        public int MiniBatchPerSave = 0;

        public ModelDiskDumpRunner(Structure model, int miniBatchPerSave, string fileName)
            : base(Structure.Empty, new RunnerBehavior())
        {
            Model = model;
            FileName = fileName;
            MiniBatchPerSave = miniBatchPerSave;
        }

        public override void Forward()
        {
            if (++MiniBatchNum % MiniBatchPerSave == 0)
            {
                string dumpPath = string.Format("{0}.{1}.{2}.model", FileName,EpochNum, MiniBatchNum);
                using (BinaryWriter writer = new BinaryWriter(new FileStream(dumpPath, FileMode.Create, FileAccess.Write, FileShare.None)))
                {
                    Model.Serialize(writer);
                }
                Logger.WriteLog("Dump the model to {0}", dumpPath);
            }

        }

        public override void Complete()
        {
            EpochNum += 1;
        }
    }

    
    /// <summary>
    /// Dump Predict Score and Label into Disk.
    /// </summary>
    // public class AUCDiskDumpRunner : ObjectiveRunner
    // {
    //     public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
    //     public CudaPieceFloat Label { get; set; }

    //     public string FileName { get; set; }
    //     public string MetricFile { get; set; }

    //     List<float> groundTruth = new List<float>();
    //     List<float> pred = new List<float>();
    //     StreamWriter dumpWriter;
    //     public AUCDiskDumpRunner(CudaPieceFloat label, DenseBatchData output, string fileName, string metricFile)
    //         : base(Structure.Empty, new RunnerBehavior())
    //     {
    //         Label = label;
    //         Output = output;
    //         FileName = fileName;
    //         MetricFile = metricFile;
    //     }

    //     public override void Forward()
    //     {
    //         Label.SyncToCPU();
    //         Output.Data.SyncToCPU();
    //         for (int i = 0; i < Output.BatchSize; i++)
    //         {
    //             dumpWriter.WriteLine("{0}\t{1}", string.Join(",", Label[i]), string.Join(",", Output.Data[i]));
    //             if (Label[i] == ParameterSetting.MissingLabel) continue;
    //             groundTruth.Add(Label[i]);
    //             pred.Add(Output.Data[i]);
    //         }
    //     }
    //     public override void Init()
    //     {
    //         dumpWriter = new StreamWriter(FileName);
    //         groundTruth.Clear();
    //         pred.Clear();
    //     }

    //     public override void Complete()
    //     {
    //         dumpWriter.Close();
    //         //Tuple<List<string>, float> results = AUCEvaluationSet.EvaluateAUC(FileName, MetricFile);
    //         double value = AUCEvaluationSet.Auc(groundTruth, pred);
    //         double prValue = AUCEvaluationSet.AucForPR(groundTruth, pred);

    //         Logger.WriteLog(string.Format("\n best-select ROC AUC : {0} \t  PR AUC : {1}", value, prValue));
    //         ObjectiveScore = value;
    //     }
    // }

    /// <summary>
    /// Standard RMSE evaluation metric;
    /// </summary>
    // public class RMSEDiskDumpRunner : ObjectiveRunner
    // {
    //     public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
    //     public CudaPieceFloat Label { get; set; }
    //     public string FileName { get; set; }
    //     StreamWriter dumpWriter;
    //     double MSE { get; set; }
    //     int SmpCount { get; set; }
    //     float Scale { get; set; }
    //     bool IsApplyLogistic { get; set; }
    //     public RMSEDiskDumpRunner(CudaPieceFloat label, DenseBatchData output, string fileName)
    //         : base(Structure.Empty, new RunnerBehavior())
    //     {
    //         Label = label;
    //         Output = output;
    //         FileName = fileName;
    //         Scale = 1.0f;
    //         IsApplyLogistic = false;
    //     }

    //     public RMSEDiskDumpRunner(CudaPieceFloat label, DenseBatchData output, float scale, bool isApplyLogistic, string fileName)
    //         : base(Structure.Empty, new RunnerBehavior())
    //     {
    //         Label = label;
    //         Output = output;
    //         FileName = fileName;
    //         Scale = scale;
    //         IsApplyLogistic = isApplyLogistic;
    //     }

    //     public override void Forward()
    //     {
    //         Label.SyncToCPU();
    //         Output.Data.SyncToCPU();
    //         for (int i = 0; i < Output.BatchSize; i++)
    //         {
    //             float output = Output.Data[i] * Scale;
    //             if (IsApplyLogistic) output = Util.Logistic(output);
    //             dumpWriter.WriteLine("{0}\t{1}", Label[i], output);
    //             MSE = MSE + (Label[i] - output) * (Label[i] - output);
    //         }
    //         SmpCount += Output.BatchSize;
    //     }
    //     public override void Init()
    //     {
    //         dumpWriter = new StreamWriter(FileName);
    //         MSE = 0;
    //         SmpCount = 0;
    //     }

    //     public override void Complete()
    //     {
    //         dumpWriter.Close();
    //         ObjectiveScore = - Math.Sqrt(MSE / SmpCount);
    //         Logger.WriteLog("RMSE {0}", -ObjectiveScore);
    //     }
    // }

    // public class NDCGDiskDumpRunner : ObjectiveRunner
    // {
    //     public DenseBatchData Score { get; set; }

    //     List<string> QueryIds = new List<string>();
    //     List<string> DocIds = new List<string>();
    //     List<string> Ratings = new List<string>();
    //     List<float> Scores = new List<float>();

    //     public string OutputFileName { get; set; }
    //     public string MetricFileName { get; set; }
    //     StreamWriter dumpWriter;
    //     public NDCGDiskDumpRunner(DenseBatchData score, string mappingFileName, string outputFileName, string metricFileName)
    //         : base(Structure.Empty, new RunnerBehavior())
    //     {
    //         Score = score;
    //         OutputFileName = outputFileName;
    //         MetricFileName = metricFileName;
    //         using (StreamReader reader = new StreamReader(mappingFileName))
    //         {
    //             string header = reader.ReadLine();
    //             while (!reader.EndOfStream)
    //             {
    //                 string line = reader.ReadLine();
    //                 string[] columns = line.Split('\t');
    //                 QueryIds.Add(columns[0]);
    //                 DocIds.Add(columns[1]);
    //                 Ratings.Add(columns[2]);
    //             }
    //         }

    //     }

    //     public override void Forward()
    //     {
    //         Score.Data.SyncToCPU();
    //         for (int i = 0; i < Score.BatchSize; i++)
    //         {
    //             dumpWriter.WriteLine("{0}\t{1}\t{2}\t{3}", QueryIds[Scores.Count], DocIds[Scores.Count], Ratings[Scores.Count], Score.Data[i]);
    //             Scores.Add(Score.Data[i]);
    //         }

    //     }
    //     public override void Init()
    //     {
    //         dumpWriter = new StreamWriter(OutputFileName);
    //         dumpWriter.WriteLine("{0}\t{1}\t{2}\t{3}", "m:QueryId", "m:Url", "m:Rating", "DSSMHashing");
    //         Scores.Clear();
    //     }

    //     public override void Complete()
    //     {
    //         dumpWriter.Close();
    //         List<string> results = EvaluationSet.CallEvaluationTool(OutputFileName, MetricFileName,
    //                                     EvaluationToolkit.EvalToolExe(EvaluationType.NDCG));
    //         Logger.WriteLog(string.Join("\n", results));
    //         ObjectiveScore = float.Parse(results[0]);
    //     }
    // }

    // public class BLEUDiskDumpRunner : ObjectiveRunner
    // {
    //     List<List<BeamAction>> BatchResult;
    //     CudaPieceInt TgtSmpIdx;
    //     CudaPieceFloat TgtLabel;

    //     public string OutputFileName { get; set; }
    //     public string MetricFileName { get; set; }
    //     StreamWriter dumpWriter;
    //     int BatchNum = 0;
    //     public BLEUDiskDumpRunner(List<List<BeamAction>> beamResult, CudaPieceInt tgtSmpIdx, CudaPieceFloat tgtLabel, string outputFileName, string metricFileName)
    //         : base(Structure.Empty, new RunnerBehavior())
    //     {
    //         BatchResult = beamResult;
    //         TgtSmpIdx = tgtSmpIdx;
    //         TgtLabel = tgtLabel;
    //         OutputFileName = outputFileName;
    //         MetricFileName = metricFileName;
    //     }

    //     public override void Forward()
    //     {
    //         TgtSmpIdx.SyncToCPU();
    //         TgtLabel.SyncToCPU();

    //         for (int b = 0; b < BatchResult.Count; b++)
    //         {
    //             int start = b == 0 ? 0 : TgtSmpIdx[b - 1];
    //             int len = TgtSmpIdx[b] - start;
    //             string groundT = string.Join(" ", TgtLabel.TakeCPU(start, len));
    //             //BatchResult[b].Sort((a, c) => (c.Prob / c.Words.Length).CompareTo(a.Prob / a.Words.Length));

    //             BatchResult[b].Sort((a, c) => (c.Prob).CompareTo(a.Prob));
    //             foreach (BeamAction beam in BatchResult[b])
    //             {
    //                 string predT = string.Join(" ", beam.Words.Skip(1));
    //                 dumpWriter.WriteLine(string.Format("{0}\t{1}", groundT, predT));
    //                 break;
    //             }
    //         }
    //         BatchNum += BatchResult.Count;
    //         Logger.WriteLog(string.Format("Mini-Sample {0} Bleu", BatchNum));
    //     }
    //     public override void Init()
    //     {
    //         dumpWriter = new StreamWriter(OutputFileName);
    //     }

    //     public override void Complete()
    //     {
    //         dumpWriter.Close();
    //         Tuple<List<string>, float> results = BLEUEvaluationSet.EvaluateBLEU(OutputFileName, MetricFileName);
    //         ObjectiveScore = results.Item2;
    //         Logger.WriteLog(string.Format("\n BLEU Score : {0}\n", ObjectiveScore));

    //     }
    // }

    public class ROUGEDiskDumpRunner : ObjectiveRunner
    {
        List<List<BeamAction>> BatchResult;
        CudaPieceInt TgtSmpIdx;
        CudaPieceFloat TgtLabel;

        public string OutputFileName { get; set; }
        StreamWriter dumpWriter;
        List<Tuple<string, string>> dumpResult = new List<Tuple<string, string>>();
        int BatchNum = 0;
        public ROUGEDiskDumpRunner(List<List<BeamAction>> beamResult, CudaPieceInt tgtSmpIdx, CudaPieceFloat tgtLabel, 
            string outputFileName)
            : base(Structure.Empty, new RunnerBehavior())
        {
            BatchResult = beamResult;
            TgtSmpIdx = tgtSmpIdx;
            TgtLabel = tgtLabel;
            OutputFileName = outputFileName;
        }

        public override void Forward()
        {
            TgtSmpIdx.SyncToCPU();
            TgtLabel.SyncToCPU();

            for (int b = 0; b < BatchResult.Count; b++)
            {
                int start = b == 0 ? 0 : TgtSmpIdx[b - 1];
                int len = TgtSmpIdx[b] - start;
                string groundT = string.Join(" ", TgtLabel.TakeCPU(start, len));
                //BatchResult[b].Sort((a, c) => (c.Prob / c.Words.Length).CompareTo(a.Prob / a.Words.Length));

                BatchResult[b].Sort((a, c) => (c.Prob).CompareTo(a.Prob));
                foreach (BeamAction beam in BatchResult[b])
                {
                    string predT = string.Join(" ", beam.Words.Skip(1));
                    dumpWriter.WriteLine(string.Format("{0}\t{1}", groundT, predT));
                    dumpResult.Add(new Tuple<string, string>(groundT, predT));
                    break;
                }
            }
            BatchNum += BatchResult.Count;
            Logger.WriteLog(string.Format("Mini-Sample {0} Rouge", BatchNum));
        }

        public override void Init()
        {
            dumpWriter = new StreamWriter(OutputFileName);
            dumpResult.Clear();
        }

        public override void Complete()
        {
            dumpWriter.Close();

            //Tuple<List<string>, float> results = BLEUEvaluationSet.EvaluateBLEU(OutputFileName, MetricFileName);
            //MetricScore = results.Item2;
            
            var rouge1Scores = new List<RougeScore>();
            var rouge2Scores = new List<RougeScore>();
            var rouge3Scores = new List<RougeScore>();
            var rougeLScores = new List<RougeScore>();

            // Tokenize candidate summary text.C:\Users\v-phwan\Source\Repos\Hemingway\Rouge\Rouge\RougeHelper.cs
            foreach(Tuple<string, string> pair in dumpResult)
            {
                string[] candidate = RougeHelper.Tokenize(pair.Item2);

                var references = new List<string[]>();
                string[] reference = RougeHelper.Tokenize(pair.Item1);
                references.Add(reference);

                // Calculate ROUGE scores if references are found.
                var nextRouge1Score = RougeCalculator.EvalNGram(candidate, references, 1);
                var nextRouge2Score = RougeCalculator.EvalNGram(candidate, references, 2);
                var nextRouge3Score = RougeCalculator.EvalNGram(candidate, references, 3);
                //var nextRougeLScore = RougeCalculator.EvalLCS(candidate, references);

                rouge1Scores.Add(nextRouge1Score);
                rouge2Scores.Add(nextRouge2Score);
                rouge3Scores.Add(nextRouge3Score);
                //rougeLScores.Add(nextRougeLScore);
            }

            // Calculate ROUGE score averages.
            var avgRouge1Scores = RougeScore.Average(rouge1Scores);
            var avgRouge2Scores = RougeScore.Average(rouge2Scores);
            var avgRouge3Scores = RougeScore.Average(rouge3Scores);
            //var avgRougeLScores = RougeScore.Average(rougeLScores);

            Logger.WriteLog("------------------------------------------------");
            Logger.WriteLog("------------------------------------------------");
            Logger.WriteLog("            ROUGE1  ROUGE2  ROUGE3  ROUGE-L");
            Logger.WriteLog("Recall:    " + " " + avgRouge1Scores.Recall.ToString("n4") + "  "
                                                        + avgRouge2Scores.Recall.ToString("n4") + "  "
                                                        + avgRouge3Scores.Recall.ToString("n4") + "  ");
            //+ avgRougeLScores.Recall.ToString("n4"));

            Logger.WriteLog("Precision: " + " " + avgRouge1Scores.Precision.ToString("n4") + "  "
                                                        + avgRouge2Scores.Precision.ToString("n4") + "  "
                                                        + avgRouge3Scores.Precision.ToString("n4") + "  ");
                                                        //+ avgRougeLScores.Precision.ToString("n4"));

            Logger.WriteLog("F-Score:   " + " " + avgRouge1Scores.Fscore.ToString("n4") + "  "
                                                        + avgRouge2Scores.Fscore.ToString("n4") + "  "
                                                        + avgRouge3Scores.Fscore.ToString("n4") + "  ");
                                                        //+ avgRougeLScores.Fscore.ToString("n4"));

            ObjectiveScore = avgRouge1Scores.Fscore;
            Logger.WriteLog(string.Format("\n ROUGE-L Score : {0}\n", ObjectiveScore));

        }
    }

    public class AccuracyRunner : ObjectiveRunner
    {
        public HiddenBatchData Input { get; set; }

        public CudaPieceInt Label { get; set; }

        public int Dim { get { return Input.Dim; } }

        int TrueNum = 0;
        int TotalNum = 0;
        int[] ConfuseMatrix;
        
        //public string OutputFileName { get; set; }
        //StreamWriter dumpWriter;
        //float Gamma { get; set; }
        //bool IsSoftmax = true;

        CudaPieceFloat tmpOut;
        CudaPieceInt tmpIdx;

        public AccuracyRunner(CudaPieceInt label, HiddenBatchData input, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
            Label = label;
            Input = input;            
            tmpOut = new CudaPieceFloat(input.MAX_BATCHSIZE, behavior.Device);
            tmpIdx = new CudaPieceInt(input.MAX_BATCHSIZE, behavior.Device);
        }

        public override void Init()
        {
            TotalNum = 0;
            TrueNum = 0;
            ConfuseMatrix = new int[Dim * Dim];
        }

        public override void Complete()
        {
            Logger.WriteLog("Accuracy : {0}", TrueNum * 1.0f / TotalNum);
            // Logger.WriteLog("Confuse Matrix : ");
            // for (int i = 0; i < Dim ; i++)
            // {
            //     Logger.WriteLog(string.Join("\t", ConfuseMatrix.Skip(i* Dim).Take(Dim)));
            // }
            ObjectiveScore = TrueNum * 1.0f / TotalNum;
        }

        public override void Forward()
        {
            if(Input.BatchSize == 0) return;

            ComputeLib.Maxpooling1D(Input.Output.Data, Dim, 1, Input.BatchSize, tmpOut, tmpIdx);
            Label.SyncToCPU();
            tmpIdx.SyncToCPU();

            int hit = 0;
            int total = 0;
            for (int i = 0; i < Input.BatchSize; i++)
            {
                int target = Label[i] - i * Dim;
                int pred = tmpIdx[i];

                if (pred == target) hit += 1; 
                else ConfuseMatrix[target * Dim + pred] += 1;
                total += 1;
            }
            ObjectiveScore = hit * 1.0f / total;

            TrueNum += hit;
            TotalNum += total;
        }
    }

    public class TopKHitRunner : ObjectiveRunner
    {
        CudaPieceFloat Score;
        CudaPieceFloat Label;
        BiMatchBatchData Match;
        float Gamma;
        float HitScore;
        int SampleSize;
        public TopKHitRunner(CudaPieceFloat score, CudaPieceFloat label, BiMatchBatchData match, float gamma, RunnerBehavior rb) : base(Structure.Empty, rb)
        {
            Score = score;
            Label = label;
            Match = match;

            Gamma = gamma;
        }
        public override void Init()
        {
            HitScore = 0;
            SampleSize = 0;
        }

        public override void Forward()
        {
            Score.SyncToCPU();
            Label.SyncToCPU();
            Match.Src2MatchIdx.SyncToCPU();

            for (int i = 0; i < Match.SrcSize; i++)
            {
                int startIdx = i == 0 ? 0 : Match.Src2MatchIdx[i - 1];
                int endIdx = Match.Src2MatchIdx[i];

                float[] prob = Util.Softmax(Score.TakeCPU(startIdx, endIdx - startIdx).ToArray(), Gamma);
                int idx = Util.MaximumValue(prob);

                float score = Label[idx + startIdx];
                HitScore += score;
            }
            SampleSize += Match.SrcSize;
        }


        public override void Complete()
        {
            Logger.WriteLog("Average Hit Score {0}", HitScore / SampleSize);
        }
    }

    public class MultiOutputAccuracyDiskDumpRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat Label { get; set; }

        int TrueNum = 0;
        int TotalNum = 0;
        int[] ConfuseMatrix;
        public string OutputFileName { get; set; }

        StreamWriter dumpWriter;
        float Gamma { get; set; }
        int OutputNum { get; set; }
        int ClassNum { get; set; }
        public MultiOutputAccuracyDiskDumpRunner(CudaPieceFloat label, DenseBatchData output, float gamma, int outputNum, string outputFileName)
            : base(Structure.Empty, new RunnerBehavior())
        {
            Label = label;
            Output = output;
            Gamma = gamma;
            OutputNum = outputNum;
            ClassNum = Output.Dim / OutputNum;
            OutputFileName = outputFileName;
        }

        public override void Init()
        {
            TotalNum = 0;
            TrueNum = 0;
            ConfuseMatrix = new int[ClassNum * ClassNum];
            dumpWriter = new StreamWriter(OutputFileName);
        }

        public override void Complete()
        {
            dumpWriter.Close();
            Logger.WriteLog("Seg Accuracy : {0}", TrueNum * 1.0f / TotalNum);
            Logger.WriteLog("Confuse Matrix : ");
            for (int i = 0; i < ClassNum; i++)
            {
                Logger.WriteLog(string.Join("\t", ConfuseMatrix.Skip(i * ClassNum).Take(ClassNum)));
            }
            ObjectiveScore = TrueNum * 1.0f / TotalNum;
        }

        public override void Forward()
        {
            Label.SyncToCPU();
            Output.Data.SyncToCPU();

            for (int i = 0; i < Output.BatchSize; i++)
            {
                int segTrueNum = 0;
                string segPred = "";
                string segTarget = "";
                for (int k = 0; k < OutputNum; k++)
                {
                    int target = (int)(Label[i * OutputNum + k]);

                    float[] predArray = Output.Data.TakeCPU(i * Output.Dim + k * ClassNum, ClassNum).ToArray();
                    predArray = Util.Softmax(predArray, Gamma);
                    int maxIndex = Util.MaximumValue(predArray);
                    if (maxIndex == target) segTrueNum += 1; //TrueNum += 1;
                    if (target >= 0 && target < ClassNum)
                        ConfuseMatrix[target * ClassNum + maxIndex] += 1;
                    segPred += maxIndex.ToString() + " ";
                    segTarget += target.ToString() + " ";
                }

                dumpWriter.WriteLine("{0}\t{1}", segTarget, segPred);

                if (segTrueNum == OutputNum) TrueNum += 1;
                TotalNum += 1;
            }
        }
    }


    public class PrecisionKRunner : ObjectiveRunner
    {
        public DenseBatchData Score { get; set; }
        BiMatchBatchData NegMatch { get; set; }

        Dictionary<int, Dictionary<int, float>> PosScoreDict = new Dictionary<int, Dictionary<int, float>>();
        Dictionary<int, Dictionary<int, float>> NegScoreDict = new Dictionary<int, Dictionary<int, float>>();

        List<Tuple<int, int>> ScoreIdxList = new List<Tuple<int, int>>();
        int ScoreIndex = 0;

        public string OutputFileName { get; set; }
        StreamWriter dumpWriter;
        public PrecisionKRunner(DenseBatchData score, BiMatchBatchData negMatch, string mappingFileName, string outputFileName)
            : base(Structure.Empty, new RunnerBehavior())
        {
            Score = score;
            NegMatch = negMatch;

            OutputFileName = outputFileName;

            using (StreamReader reader = new StreamReader(mappingFileName))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] columns = line.Split('\t');

                    int qIdx = int.Parse(columns[0]);
                    int dIdx = int.Parse(columns[1]);

                    ScoreIdxList.Add(new Tuple<int, int>(qIdx, dIdx));

                    if (!PosScoreDict.ContainsKey(qIdx))
                    {
                        PosScoreDict[qIdx] = new Dictionary<int, float>();
                        NegScoreDict[qIdx] = new Dictionary<int, float>();
                    }

                    if (!PosScoreDict[qIdx].ContainsKey(dIdx))
                        PosScoreDict[qIdx].Add(dIdx, 0);
                }
            }

        }

        public override void Forward()
        {
            Score.Data.SyncToCPU();

            for (int i = 0; i < Score.BatchSize; i++)
            {
                float posScore = Score.Data[i * NegMatch.Dim];
                int qIdx = ScoreIdxList[ScoreIndex + i].Item1;
                int dIdx = ScoreIdxList[ScoreIndex + i].Item2;

                PosScoreDict[qIdx][dIdx] = posScore;

                for (int d = 1; d < NegMatch.Dim; d++)
                {
                    int negIdx = NegMatch.TgtIdx[i * NegMatch.Dim + d];
                    float negScore = Score.Data[i * NegMatch.Dim + d];
                    if (PosScoreDict[qIdx].ContainsKey(negIdx)) continue;

                    if (!NegScoreDict[qIdx].ContainsKey(negIdx))
                        NegScoreDict[qIdx].Add(negIdx, negScore);
                }
                //dumpWriter.WriteLine("{0}\t{1}\t{2}\t{3}", qIdx, dIdx, Score.Data[i]);
                //Scores.Add(Score.Data[i]);
            }
            ScoreIndex += Score.BatchSize;
        }
        public override void Init()
        {
            ScoreIndex = 0;
            //dumpWriter.WriteLine("{0}\t{1}\t{2}\t{3}", "m:QueryId", "m:Url", "m:Rating", "DSSMHashing");
            foreach(int qIdx in PosScoreDict.Keys) NegScoreDict[qIdx].Clear();
        }

        public override void Complete()
        {
            dumpWriter = new StreamWriter(OutputFileName);

            float macro_P = 0;
            int DocNum = 0;
            //macro precision K.
            foreach (int qIdx in PosScoreDict.Keys)
            {
                int K = PosScoreDict[qIdx].Count;
                List<Tuple<int, float>> pred = new List<Tuple<int, float>>();

                foreach (KeyValuePair<int, float> posIdx in PosScoreDict[qIdx])
                    pred.Add(new Tuple<int, float>(1, posIdx.Value));

                DataRandomShuffling shuffle = new DataRandomShuffling(NegScoreDict[qIdx].Count);
                for(int i=0;i<K;i++)
                {
                    int nid = shuffle.RandomNext();
                    if (nid < 0) break;
                    float negScore = NegScoreDict[qIdx].ElementAt(nid).Value;

                    pred.Add(new Tuple<int, float>(0, negScore));
                }

                int idx = 0;
                int hit = 0;
                foreach (Tuple<int, float> item in pred.OrderBy(o => o.Item2).Reverse())
                {
                    hit = hit + item.Item1;
                    if (++idx >= K) break;
                }
                macro_P += hit * 1.0f / K;
                DocNum += K;
            }
           
            int micro_Hit = 0;
            foreach (int qIdx in PosScoreDict.Keys)
            {
                int K = PosScoreDict[qIdx].Count;

                DataRandomShuffling shuffle = new DataRandomShuffling(NegScoreDict[qIdx].Count);
                foreach (KeyValuePair<int, float> posIdx in PosScoreDict[qIdx])
                {
                    float pScore = posIdx.Value;
                    int nid = shuffle.RandomNext();
                    if (nid < 0) break;

                    float negScore = NegScoreDict[qIdx].ElementAt(nid).Value;

                    if (pScore > negScore) micro_Hit += 1;
                }
            }
            Logger.WriteLog("Unique Query Num {0}, Average Doc Num {1}, Macro Precision @ K {2}, Micro Precision@1 {3}",
                       PosScoreDict.Count, DocNum * 1.0f / PosScoreDict.Count, macro_P * 1.0f / PosScoreDict.Count, micro_Hit * 1.0f / DocNum);

            dumpWriter.Close();
            ObjectiveScore = macro_P / PosScoreDict.Count;
            //List<string> results = EvaluationSet.CallEvaluationTool(OutputFileName, MetricFileName,
            //                            EvaluationToolkit.EvalToolExe(EvaluationType.NDCG));
            //Logger.WriteLog(string.Join("\n", results));
            //ObjectiveScore = float.Parse(results[0]);
        }
    }
}
