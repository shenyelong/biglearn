using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorSliceRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        IntArgument[] InShape { get { return Input.Shape; } }

        IntArgument[] FromCols { get; set; }
        IntArgument[] OutShape { get; set; }

        CudaPieceInt InDims { get; set; }
        CudaPieceInt Offsets { get; set; }
        CudaPieceInt OutDims { get; set; }

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public TensorSliceRunner(NdArrayData input, IntArgument[] from, IntArgument[] shape, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            FromCols = from;
            OutShape = shape;
            Output = new NdArrayData(behavior.Device, shape);
            
            InDims = new CudaPieceInt(InShape.Length, behavior.Device);
            Offsets = new CudaPieceInt(InShape.Length, behavior.Device);
            OutDims = new CudaPieceInt(InShape.Length, behavior.Device);
        }

        public override void Forward()
        {
            for(int i = 0; i < InShape.Length; i++)
            {
                InDims[i] = InShape[i].Value;
                Offsets[i] = FromCols[i].Value;
                OutDims[i] = OutShape[i].Value;
            }    
            InDims.SyncFromCPU();
            Offsets.SyncFromCPU();
            OutDims.SyncFromCPU();

            ComputeLib.NDArraySliceForward(Input.Output, InShape.Length, InDims, 
                                           Output.Output, Offsets, OutDims, new Shape(OutShape).Size, 0, 1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.NDArraySliceBackward(Output.Deriv, InShape.Length, Offsets, OutDims, Input.Deriv, InDims, new Shape(OutShape).Size, 1, 1);
        }

    }
}
