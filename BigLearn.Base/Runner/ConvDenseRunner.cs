﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Hideen Sequence of convolution nn.
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class ConvDenseRunner<IN> : StructRunner<LayerStructure, IN> where IN : SeqDenseBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        SeqDenseConvRunner<SeqDenseBatchData> ConvRunner = null;
        MaxPoolingRunner<SeqDenseBatchData> MaxPoolRunner = null;

        public ConvDenseRunner(LayerStructure model, SeqDenseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            ConvRunner = new SeqDenseConvRunner<SeqDenseBatchData>((model), data, behavior);
            MaxPoolRunner = new MaxPoolingRunner<SeqDenseBatchData>(ConvRunner.Output, behavior);
            Output = MaxPoolRunner.Output;
        }

        public override void CleanDeriv()
        {
            ConvRunner.CleanDeriv();
            MaxPoolRunner.CleanDeriv();
        }

        public override void Forward()
        {
            ConvRunner.Forward();
            MaxPoolRunner.Forward();
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            MaxPoolRunner.Backward(cleanDeriv);
            ConvRunner.Backward(cleanDeriv);

        }

        public override void Update()
        {
            ConvRunner.Update();
            MaxPoolRunner.Update();
        }


    }
}
