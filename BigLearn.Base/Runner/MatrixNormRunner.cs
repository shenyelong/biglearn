using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
        public class MatrixNormRunner : StructRunner
    {
        public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }
        int Norm { get; set; }
        public MatrixNormRunner(HiddenBatchData input, int norm, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Norm = norm;
            Output = new VectorData(Input.MAX_BATCHSIZE, behavior.Device);
        }

        public override void Forward()
        {
            Output.Length = Input.BatchSize;
            if(Norm == 1)
            {
                ComputeLib.MatrixL1Norm(Input.Output.Data, Input.Dim, Input.BatchSize, Output.Output);    
            }
            else if(Norm == 2)
            {
                ComputeLib.MatrixL2Norm(Input.Output.Data, Input.Dim, Input.BatchSize, Output.Output);    
            }
        }
        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Norm == 1)
                ComputeLib.DerivMatrixL1Norm(Input.Output.Data, Input.Dim, Input.BatchSize, Output.Output, Output.Deriv, Input.Deriv.Data);
            else if (Norm == 2)
                ComputeLib.DerivMatrixL2Norm(Input.Output.Data, Input.Dim, Input.BatchSize, Output.Output, Output.Deriv, Input.Deriv.Data);

        }

    }

    // public class MatrixNormRunner : StructRunner
    // {
    //     public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
    //     new HiddenBatchData Input { get; set; }

    //     CudaPieceFloat tmp = null;
    //     CudaPieceFloat tmp2 = null;
    //     CudaPieceFloat tmp3 = null;
    //     public MatrixNormRunner(HiddenBatchData data, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //     {
    //         Input = data;
    //         Output = data;
    //         tmp = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);
    //         tmp2 = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);
    //         tmp3 = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);
    //     }

    //     public override void Forward()
    //     {
    //         ComputeLib.MatrixL2Norm(Input.Output.Data, Input.Dim, Input.BatchSize, tmp);
    //         tmp.SyncToCPU(Output.BatchSize);
    //         for (int i = 0; i < Output.BatchSize; i++)
    //         {
    //             tmp[i] = 1.0f / (tmp[i] + Util.GPUEpsilon);
    //             tmp3[i] = -tmp[i];
    //         }
    //         tmp.SyncFromCPU(Output.BatchSize);
    //         tmp3.SyncFromCPU(Output.BatchSize);

    //         ComputeLib.Scale_MatrixMask(Input.Output.Data, 0, CudaPieceInt.Empty, 0,
    //                 Output.Output.Data, 0, CudaPieceInt.Empty, 0, Output.Dim, Output.BatchSize, tmp, 0);
    //     }

    //     public override void Backward(bool cleanDeriv)
    //     {
    //         ComputeLib.InnerProduct_Similarity(Output.Deriv.Data, Output.Output.Data, tmp2, Output.BatchSize, Output.Dim);
            
    //         ComputeLib.Scale_MatrixMask(Output.Output.Data, 0, CudaPieceInt.Empty, 0,
    //                                     Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
    //                                     Output.Dim, Output.BatchSize, tmp2, -1);

    //         ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
    //                                     Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
    //                                     Output.Dim, Output.BatchSize, tmp3, 0);

            
    //     }
    // }

}
