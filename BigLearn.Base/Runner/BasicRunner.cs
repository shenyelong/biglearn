﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum PlatformType : int
    {
        SingleBox = 0,
        AllReduce = 1,
        AllReduceRDMA = 2,
        AllReduceNCCL = 3
    }

    public enum DNNRunMode
    {
        Train = 0,
        Predict = 1,
        GradientCheck = 2,
        ExecuteOp1 = 3,
        ExecuteOp2 = 4,
    }

    public enum PoolingType
    {
        MAX = 0,
        LAST = 1,
        AVG = 2,
        ATT = 3,
        TREE = 4,
        RL = 5,
    }

    public class DeviceBehavior
    {
        public DeviceType Device = DeviceType.GPU;
        public IMathOperationManager Computelib;
        public int DeviceId { get; set; }
        
        protected DeviceBehavior() { }
        protected RunnerBehavior trainBehavior = null;
        protected RunnerBehavior predictBehavior = null;
        public DeviceBehavior(int gpuId)
        {
            Device = MathOperatorManager.SetDevice(gpuId);
            Computelib = MathOperatorManager.CreateInstance(Device);    
            DeviceId = gpuId;
        }
        public RunnerBehavior TrainMode { get { if(trainBehavior == null) trainBehavior = new RunnerBehavior(DNNRunMode.Train, this.Device, this.Computelib) { ID = DeviceId }; return trainBehavior; } }
        public RunnerBehavior PredictMode { get { if(predictBehavior == null) predictBehavior = new RunnerBehavior(DNNRunMode.Predict, this.Device, this.Computelib) { ID = DeviceId }; return predictBehavior; } }
    }

    public class RunnerBehavior
    {
        public DNNRunMode RunMode = DNNRunMode.Train;
        public DeviceType Device = DeviceType.GPU;
        public IMathOperationManager Computelib;
        public object BehaviorParameter;
        public int ID { get; set; }

        public RunnerBehavior() : this(DNNRunMode.Train, DeviceType.CPU, MathOperatorManager.CreateInstance(DeviceType.CPU)) { }

        public RunnerBehavior(DNNRunMode mode, DeviceType device, IMathOperationManager computeLib)
        {
            RunMode = mode;
            Device = device;
            Computelib = computeLib;
            Resource = new ResourceManager(this);
        }

        public void SetTrainMode() { RunMode = DNNRunMode.Train; }

        public void SetPredictMode() { RunMode = DNNRunMode.Predict; }

        public ResourceManager Resource { get; set; }

        public void Setup()
        {
            MathOperatorManager.SetDevice(ID);
            Resource.InitResource();
        }
    }

    /// <summary>
    /// Basic Runner.
    /// </summary>
    public class StructRunner : IDisposable
    {
        public Structure Model { get; set; }
        public BatchData Input { get; set; }
        public BatchData Output { get; set; }
        public RunnerBehavior Behavior { get; set; }

        public DeviceType Device { get { return Behavior == null ? DeviceType.GPU : Behavior.Device; } }
        public IMathOperationManager ComputeLib
        {
            get
            {
                if(Behavior == null) return null;
                if (Behavior.Computelib == null)
                {
                    Behavior.Computelib = MathOperatorManager.CreateInstance(Behavior.Device);
                }
                return Behavior.Computelib;
            }
        }
        public static Type[] RunnerList = null;

        public bool IsTerminate = false;
        public bool IsContinue = true;
        public bool IsBackProp = true;
        public bool IsUpdate = true;
        /// <summary>
        /// usually it will be set true in the future.
        /// </summary>
        //public bool IsDelegateOptimizer = false;

        public ComputationGraph CG = null;
        public virtual string name { get; set; }
        public int ReportPerEpoch = 1;
        public int iteration = 0;

        /// <summary>
        /// Get Runner Type According to Model and DataType.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="batchDataType"></param>
        /// <returns></returns>
        // static Type SearchRunnerType(Structure model, Type batchDataType)
        // {
        //     InitRunnerList();
        //     Type rootType = typeof(StructRunner<,>).MakeGenericType(new Type[] { model.GetType(), batchDataType });

        //     Type runnerGenericType = null;
        //     Type runnerData = null;
        //     foreach (Type runnerType in RunnerList)
        //     {
        //         try
        //         {
        //             Type mtry;
        //             if (!TryMakeGenericType(batchDataType, runnerType, out mtry))
        //             {
        //                 continue;
        //             }

        //             if (rootType.IsAssignableFrom(mtry))
        //             {
        //                 if (runnerGenericType == null)
        //                 {
        //                     runnerGenericType = mtry;
        //                     runnerData = runnerType.GetGenericArguments()[0].GetGenericParameterConstraints()[0];
        //                 }
        //                 else
        //                 {
        //                     Type tryData = runnerType.GetGenericArguments()[0].GetGenericParameterConstraints()[0];
        //                     if (runnerData.IsAssignableFrom(tryData))
        //                     {
        //                         runnerGenericType = mtry;
        //                         runnerData = tryData;
        //                     }
        //                 }
        //             }
        //         }
        //         catch
        //         {
        //             continue;
        //         }
        //     }
        //     return runnerGenericType;
        // }

        // static bool TryMakeGenericType(Type batchDataType, Type runnerGeneric, out Type runnerType)
        // {
        //     runnerType = null;
        //     Type[] genericArgs = runnerGeneric.GetGenericArguments();
        //     if (genericArgs.Length != 1)
        //     {
        //         return false;
        //     }

        //     if (genericArgs[0].IsGenericParameter)
        //     {
        //         Type[] constraints = genericArgs[0].GetGenericParameterConstraints();
        //         if (constraints.Length != 0)
        //         {
        //             if (!constraints.All(t => t.IsAssignableFrom(batchDataType)))
        //             {
        //                 return false;
        //             }
        //         }
        //     }

        //     runnerType = runnerGeneric.MakeGenericType(new Type[] { batchDataType });

        //     return true;
        // }

        /// <summary>
        /// Create the Runner which fit for the Structure and data.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        // public static StructRunner CreateRunner(Structure model, Type batchDataType, RunnerBehavior behavior)
        // {
        //     Type runnerType = SearchRunnerType(model, batchDataType);
        //     StructRunner neuralRunner = (StructRunner)Activator.CreateInstance(runnerType, new object[] { model, behavior });
        //     return neuralRunner;
        // }

        /// <summary>
        /// Enum All Runners.
        /// </summary>
        // static void InitRunnerList()
        // {
        //     if (RunnerList == null)
        //     {
        //         var domainAssemblies = (from domainAssembly in AppDomain.CurrentDomain.GetAssemblies() select domainAssembly);
        //         List<Type> domainTypes = new List<Type>();

        //         foreach (var domain in domainAssemblies)
        //         {
        //             try
        //             {
        //                 domainTypes.AddRange((from assemblyType in domain.GetTypes() select assemblyType));
        //             }
        //             catch
        //             {
        //                 continue;
        //             }
        //         }

        //         RunnerList = (from assemblyType in domainTypes
        //                       where assemblyType.IsGenericType
        //                              && assemblyType.IsClass
        //                              && typeof(StructRunner).IsAssignableFrom(assemblyType)
        //                              && !assemblyType.IsEquivalentTo(typeof(StructRunner<,>))
        //                       select assemblyType).ToArray();
        //     }
        // }

        protected StructRunner() : this(Structure.Empty, null)
        { }

        /// <summary>
        /// Construct Runner, Allocate Memory.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="input"></param>
        /// <param name="behavior"></param>
        public StructRunner(Structure model, RunnerBehavior behavior) : this(model, null, behavior)
        { }

        public StructRunner(Structure model, BatchData data, RunnerBehavior behavior) : this(null, model, data, behavior)
        { }

        public StructRunner(ComputationGraph cg, Structure model, BatchData data, RunnerBehavior behavior)
        {
            CG = cg;
            Model = model;
            Input = data;
            Behavior = behavior;
            //if (Behavior.RunMode == DNNRunMode.Predict) IsBackProp = false;
        }

        #region Dispose Function.
        /// <summary>
        /// Free GPU Memory.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~StructRunner()
        {
            this.Dispose(false);
        }

        bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;
        }

        #endregion.

        public delegate void DelegateFunc();
        public DelegateFunc ForwardDelegate = null;
        public DelegateFunc BackwardDelegate = null;
        public DelegateFunc CleanDelegate = null;
        public DelegateFunc InitDelegate = null;
        public DelegateFunc CompleteDelegate = null;


        //DelegateFunc ForwardDelegate = null;
        /// <summary>
        /// Generate Ouput from Input.
        /// </summary>
        public virtual void Forward()
        {
            if(ForwardDelegate != null) ForwardDelegate();
        }

        /// <summary>
        /// It will be called in Forward Function.
        /// </summary>
        public virtual void InitMemory() { throw new NotImplementedException(); }

        /// <summary>
        /// Clear Deriv.
        /// </summary>
        public virtual void CleanDeriv()
        {
            if(CleanDelegate != null) CleanDelegate(); 
        }

        /// <summary>
        /// Generate Deriv from output, Update Model.
        /// </summary>
        public virtual void Backward(bool cleanDeriv)
        {
            if(BackwardDelegate != null) BackwardDelegate(); 
        }

        /// <summary>
        /// Update Model Weight. (This function will be staled). 
        /// </summary>
        public virtual void Update()
        { }

        public virtual void Complete()
        {
            if(CompleteDelegate != null) CompleteDelegate(); 
        }

        public virtual void Init()
        {
            if(InitDelegate != null) InitDelegate(); 
        }
    }

    /// <summary>
    /// Basic Model with Generic Structure and Data.
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="IN"></typeparam>
    public class StructRunner<M, IN> : StructRunner where M : Structure where IN : BatchData
    {
        public new M Model
        {
            get { return (M)base.Model; }
            set
            {
                base.Model = value;
            }
        }
        public new virtual IN Input { get { return (IN)base.Input; } set { base.Input = value; } }

        //public LossDerivParameter LossParameter { get; set; }

        public StructRunner(Structure model, RunnerBehavior behavior) : base(model, behavior)
        { }

        public StructRunner(Structure model, BatchData data, RunnerBehavior behavior) : base(model, data, behavior)
        { }

        public StructRunner(ComputationGraph cg, Structure model, BatchData data, RunnerBehavior behavior) : base(cg, model, data, behavior)
        { }

        private bool disposed = false;
        ~StructRunner()
        {
            this.Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.disposed = true;

            base.Dispose(disposing);
        }
    }
}
