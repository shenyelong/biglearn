﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class FastLSTMDenseRunner<IN> : StructRunner<LSTMCell, IN> where IN : SeqDenseRecursiveData
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }
        public SeqDenseRecursiveData C;

        SeqDenseBatchData GateO;
        SeqDenseBatchData GateI;
        SeqDenseBatchData CHat;
        SeqDenseBatchData GateF;
        SeqDenseBatchData TanhC;

        CudaPieceFloat Lag1O;
        HiddenBatchData InitC = null;
        HiddenBatchData InitO = null;

        HiddenBatchData RemapInitC = null;
        HiddenBatchData RemapInitO = null;

        BasicMLPAttentionRunner AttentionRunner = null;

        public FastLSTMDenseRunner(LSTMCell model, SeqDenseRecursiveData input, RunnerBehavior behavior) : this(model, input, null, null, null, behavior)
        { }

        public FastLSTMDenseRunner(LSTMCell model, SeqDenseRecursiveData input, HiddenBatchData initO, HiddenBatchData initC, BasicMLPAttentionRunner attentionRunner, RunnerBehavior behavior) : base(model, input, behavior)
        {
            InitC = initC;
            InitO = initO;

            if (InitO != null) RemapInitO = new HiddenBatchData(InitO.MAX_BATCHSIZE, InitO.Dim, Behavior.RunMode, Behavior.Device);
            if (InitC != null) RemapInitC = new HiddenBatchData(InitC.MAX_BATCHSIZE, InitC.Dim, Behavior.RunMode, Behavior.Device);

            GateO = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);
            GateI = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);
            CHat = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);
            GateF = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);
            TanhC = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);

            Lag1O = new CudaPieceFloat(Input.MAX_SENTSIZE * Model.CellDim, true, Behavior.Device == DeviceType.GPU);

            Output = new SeqDenseRecursiveData(
                        Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo,
                        Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim,  
                        Behavior.Device);

            C = new SeqDenseRecursiveData(
                        Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo,
                        Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim,  
                        Behavior.Device);
            
            AttentionRunner = attentionRunner;
        }

        public override void Forward()
        {
            if (InitO != null)
            {
                RemapInitO.BatchSize = InitO.BatchSize;
                ComputeLib.Matrix_AdditionMask(InitO.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               RemapInitO.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               RemapInitO.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 0, 0);
            }

            if (InitC != null)
            {
                RemapInitC.BatchSize = InitC.BatchSize;
                ComputeLib.Matrix_AdditionMask(InitC.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               RemapInitC.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               RemapInitC.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               InitC.Dim, InitC.BatchSize, 1, 0, 0);
            }

            if (AttentionRunner == null)
            {
                LSTMComputeForwardLib.LSTMForwardv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
            else
            {
                if(AttentionRunner.IsDelegate) AttentionRunner.Forward();
                LSTMComputeForwardLib.LSTMForwardAttentionv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            ComputeLib.Zero(C.SentDeriv, C.SentSize * C.Dim);

            if (RemapInitO != null && RemapInitO.Deriv != null) { ComputeLib.Zero(RemapInitO.Deriv.Data, RemapInitO.BatchSize * RemapInitO.Dim); }
            if (RemapInitC != null && RemapInitC.Deriv != null) { ComputeLib.Zero(RemapInitC.Deriv.Data, RemapInitC.BatchSize * RemapInitC.Dim); }

            if (AttentionRunner != null && AttentionRunner.IsDelegate) { AttentionRunner.CleanDeriv(); }
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (AttentionRunner == null)
            {
                LSTMComputeBackwardDataLib.LSTMBackwardDatav2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
            else
            {
                LSTMComputeBackwardDataLib.LSTMAttentionBackwardDatav2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
                if(AttentionRunner.IsDelegate) AttentionRunner.Backward(cleanDeriv);
            }

            if (RemapInitO != null && InitO.Deriv != null)
            {
                ComputeLib.Matrix_AdditionMask(RemapInitO.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 1, 0);
            }

            if (RemapInitC != null && InitC.Deriv != null)
            {
                ComputeLib.Matrix_AdditionMask(RemapInitC.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                               InitC.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitC.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitC.Dim, InitC.BatchSize, 1, 1, 0);
            }

        }

        public override void Update()
        {            
            if (AttentionRunner == null)
            {
                LSTMComputeBackwardWeightLib.LSTMBackwardWeightv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
            }
            else
            {
                LSTMComputeBackwardWeightLib.LSTMAttentionBackwardWeightv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
                if(AttentionRunner.IsDelegate) AttentionRunner.Update();
            }

        }
    }
}
