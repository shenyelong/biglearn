﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

    public class BasicDecodingRunner : StructRunner
    {
        public virtual void InitMemory(int beamSize)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Init Status.
        /// </summary>
        public virtual void InitStatus()
        {
            throw new NotImplementedException();
        }

        public virtual void TakeStep(List<BeamAction> actions, int searchStep)
        {
            throw new NotImplementedException();
        }

        public virtual void TakeSwitch()
        {
            throw new NotImplementedException();
        }

        public virtual int BatchSize { get; set; }

        public virtual CudaPieceFloat WordProb { get; set; }

        public BasicDecodingRunner(Structure model, RunnerBehavior behavior): base(model, behavior)
        { }
    }

    public class LSTMDecodingRunner : BasicDecodingRunner
    {
        public override CudaPieceFloat WordProb { get; set; }
        public override int BatchSize { get { return Initstatus[0].Item1.BatchSize; } }


        List<Tuple<HiddenBatchData, HiddenBatchData>> Initstatus { get; set; }

        CudaPieceInt SmpIndex;
        CudaPieceInt WordIndex;
        CudaPieceInt LagInfo;
        int[] BatchIdx;

        SeqDenseBatchData GateO;
        SeqDenseBatchData GateI;
        SeqDenseBatchData CHat;
        SeqDenseBatchData GateF;
        SeqDenseBatchData TanhC;

        public List<BasicMLPAttentionRunner> AttentionLibs = null;

        //public CudaPieceFloat tmpWordProb;
        //public CudaPieceFloat tmpWordIndex;
        
        List<Tuple<HiddenBatchData, HiddenBatchData>> BufferStatus;
        List<Tuple<HiddenBatchData, HiddenBatchData>> BufferStatus2;
        CudaPieceFloat tmpO;

        public new LSTMStructure Model { get { return (LSTMStructure)base.Model; } set { base.Model = value; } }

        public EmbedStructure Embed { get; set; }

        //public int BeamSearchDepth { get; set; }
        //public int BeamSearchCandidate { get; set; }

        //int mbatch = 0;
        
        //public List<List<BeamAction>> BatchResult = new List<List<BeamAction>>();

        //public int BeginWordIndex = 0;
        //public int TerminalWordIndex = 9;
        bool firstAttention = true;

        public LSTMDecodingRunner(LSTMStructure model, EmbedStructure embed, List<Tuple<HiddenBatchData, HiddenBatchData>> initStatus,
            List<BasicMLPAttentionRunner> attentionLibs, RunnerBehavior behavior, bool firstAtt = false): base(model, behavior)
        {
            //BeginWordIndex = beginIndex;
            //TerminalWordIndex = terminalIndex;
            firstAttention = firstAtt;

            Embed = embed;
            Initstatus = initStatus;

            //BeamSearchDepth = beamSearchDepth;
            //BeamSearchCandidate = Math.Min(Embed.VocabSize, beamSearchCandidate);

            
            
            /// put attention here.
            AttentionLibs = attentionLibs;

            //tmpWordIndex = new CudaPieceFloat(BeamSearchCandidate * BeamSearchCandidate * maxBatchSize * BeamSearchCandidate * BeamSearchCandidate, true, Behavior.Device == DeviceType.GPU);
            //tmpWordProb = new CudaPieceFloat(BeamSearchCandidate * BeamSearchCandidate * maxBatchSize * BeamSearchCandidate * BeamSearchCandidate, true, Behavior.Device == DeviceType.GPU);

            //TgtSmpIdx = tgtSmpIdx;
            //TgtLabel = tgtLabel;
        }

        public override void InitMemory(int beamSize)
        {
            int maxBatchSize = Initstatus[0].Item1.MAX_BATCHSIZE;

            BufferStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
            BufferStatus2 = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
            int maxDim = 0;
            for (int l = 0; l < Initstatus.Count; l++)
            {
                int dim = Initstatus[l].Item1.Dim;

                BufferStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(
                    new HiddenBatchData(beamSize * beamSize * maxBatchSize, dim, Behavior.RunMode, Behavior.Device),
                    new HiddenBatchData(beamSize * beamSize * maxBatchSize, dim, Behavior.RunMode, Behavior.Device)));

                BufferStatus2.Add(new Tuple<HiddenBatchData, HiddenBatchData>(
                    new HiddenBatchData(beamSize * beamSize * maxBatchSize, dim, Behavior.RunMode, Behavior.Device),
                    new HiddenBatchData(beamSize * beamSize * maxBatchSize, dim, Behavior.RunMode, Behavior.Device)));

                if (maxDim < dim) maxDim = dim;
            }


            tmpO = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * maxDim, true, Behavior.Device == DeviceType.GPU);

            GateO = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);
            GateI = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);
            CHat = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);
            GateF = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);
            TanhC = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);

            LagInfo = new CudaPieceInt(beamSize * beamSize * maxBatchSize, true, Behavior.Device == DeviceType.GPU);
            WordIndex = new CudaPieceInt(beamSize * beamSize * maxBatchSize, true, Behavior.Device == DeviceType.GPU);
            SmpIndex = new CudaPieceInt(beamSize * beamSize * maxBatchSize, true, Behavior.Device == DeviceType.GPU);
            for (int i = 0; i < SmpIndex.Size; i++) SmpIndex[i] = i + 1; SmpIndex.SyncFromCPU();
            BatchIdx = new int[beamSize * beamSize * maxBatchSize];

            WordProb = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * Embed.VocabSize, true, Behavior.Device == DeviceType.GPU);
        }

        public override void InitStatus()
        {
            if (AttentionLibs != null)
            {
                for (int i = 0; i < AttentionLibs.Count; i++)
                {
                    if (AttentionLibs[i] != null) AttentionLibs[i].Forward();
                }
            }    
            input = Initstatus;
            output = BufferStatus;
            another = BufferStatus2;
        }
        List<Tuple<HiddenBatchData, HiddenBatchData>> input { get; set; }
        List<Tuple<HiddenBatchData, HiddenBatchData>> output { get; set; }
        List<Tuple<HiddenBatchData, HiddenBatchData>> another { get; set; }

        public override void TakeStep(List<BeamAction> actions, int searchStep)
        {
            StatusUpdate(input, actions, output, firstAttention ? true : searchStep > 0);

            Behavior.Computelib.Sgemm(output[output.Count - 1].Item1.Output.Data, 0, Embed.Embedding, 0, WordProb, 0, 
                actions.Count, Embed.Dim, Embed.VocabSize, 0, 1, false, true);
            //if (Embed.IsBias) Behavior.Computelib.Matrix_Add_Linear(WordProb, Embed.Bias, actions.Count, Embed.VocabSize);
            Behavior.Computelib.SoftMax(WordProb, WordProb, Embed.VocabSize, actions.Count, 1);
        }

        public void StatusUpdate(List<Tuple<HiddenBatchData, HiddenBatchData>> currentStatus, List<BeamAction> actions, List<Tuple<HiddenBatchData, HiddenBatchData>> newStatus, bool isAttention)
        {
            for (int i = 0; i < actions.Count; i++)
            {
                WordIndex[i] = actions[i].Words.Last();
                LagInfo[i] = actions[i].COId;
                BatchIdx[i] = actions[i].BatchId;
            }
            WordIndex.SyncFromCPU(actions.Count);
            LagInfo.SyncFromCPU(actions.Count);
            
            if(AttentionLibs == null || !isAttention || AttentionLibs[0] == null)
                LSTMComputeDecodeLib.LSTMForwardDecode(ComputeLib, actions.Count, SmpIndex, WordIndex, LagInfo, Embed.VocabSize,
                    currentStatus[0].Item1, currentStatus[0].Item2, Model.LSTMCells[0], newStatus[0].Item1, newStatus[0].Item2,
                    GateO, GateI, CHat, GateF, TanhC);
            else
                LSTMComputeDecodeLib.LSTMForwardAttentionDecode(ComputeLib, actions.Count, SmpIndex, WordIndex, LagInfo, Embed.VocabSize,
                    currentStatus[0].Item1, currentStatus[0].Item2, Model.LSTMCells[0], newStatus[0].Item1, newStatus[0].Item2,
                    tmpO, AttentionLibs[0], AttentionLibs[0].Z, BatchIdx,
                    GateO, GateI, CHat, GateF, TanhC);

            for (int i = 1; i < Model.LSTMCells.Count; i++)
            {
                if (AttentionLibs == null || !isAttention || AttentionLibs[i] == null)
                    LSTMComputeDecodeLib.LSTMForwardDecode(ComputeLib, actions.Count, newStatus[i - 1].Item1.Output.Data, LagInfo, newStatus[i-1].Item1.Dim,
                        currentStatus[i].Item1, currentStatus[i].Item2, Model.LSTMCells[i], newStatus[i].Item1, newStatus[i].Item2,
                        tmpO, GateO, GateI, CHat, GateF, TanhC);
                else
                    LSTMComputeDecodeLib.LSTMForwardAttentionDecode(ComputeLib, actions.Count, newStatus[i - 1].Item1.Output.Data, LagInfo, newStatus[i - 1].Item1.Dim,
                        currentStatus[i].Item1, currentStatus[i].Item2, Model.LSTMCells[i], newStatus[i].Item1, newStatus[i].Item2,
                        tmpO, AttentionLibs[i], AttentionLibs[i].Z, BatchIdx, 
                        GateO, GateI, CHat, GateF, TanhC);
            }
        }
        
        public override void TakeSwitch()
        {
            input = output;
            output = another;
            another = input;
        }

    }

    public class BeamAccuracyRunner : ObjectiveRunner
    {
        List<List<BeamAction>> BatchResult;
        CudaPieceInt TgtSmpIdx;
        CudaPieceFloat TgtLabel;

        int TotalBatchNum = 0;
        int TargetNum = 0;

        public BeamAccuracyRunner(List<List<BeamAction>> beamResult, CudaPieceInt tgtSmpIdx, CudaPieceFloat tgtLabel)
            : base(Structure.Empty, new RunnerBehavior())
        {
            BatchResult = beamResult;
            TgtSmpIdx = tgtSmpIdx;
            TgtLabel = tgtLabel;
        }


        public override void Init()
        {
            TotalBatchNum = 0;
            TargetNum = 0;
        }

        public override void Complete()
        {
            ObjectiveScore = TargetNum * 1.0 / TotalBatchNum;
            Logger.WriteLog(string.Format("\n Total Batch Num : {0} \t Target Num : {1} \t Accuracy : {2}", TotalBatchNum, TargetNum, ObjectiveScore));
        }


        public override void Forward()
        {
            TgtSmpIdx.SyncToCPU();
            TgtLabel.SyncToCPU();

            int target = 0;
            for (int b = 0; b < BatchResult.Count; b++)
            {
                int start = b == 0 ? 0 : TgtSmpIdx[b - 1];
                int len = TgtSmpIdx[b] - start;
                string groundT = string.Join(" ", TgtLabel.TakeCPU(start, len));
                //BatchResult[b].Sort((a, c) => (c.Prob / c.Words.Length).CompareTo(a.Prob / a.Words.Length));

                BatchResult[b].Sort((a, c) => (c.Prob).CompareTo(a.Prob));
                foreach (BeamAction beam in BatchResult[b])
                {
                    string predT = string.Join(" ", beam.Words.Skip(1));
                    if (groundT.Equals(predT))
                        target += 1;
                    break;
                }
            }

            TotalBatchNum += BatchResult.Count;
            TargetNum += target;
        }
    }

    public class TopKBeamRunner : ObjectiveRunner
    {
        List<List<BeamAction>> BatchResult;

        int TotalBatchNum = 0;
        int TopK = 0;
        string OutputFileName;
        StreamWriter dumpWriter;
        int Iteration = 0;

        public TopKBeamRunner(List<List<BeamAction>> beamResult, int topK, string dumpFile)
            : base(Structure.Empty, new RunnerBehavior())
        {
            BatchResult = beamResult;
            TopK = topK;
            OutputFileName = dumpFile;
        }

        public override void Init()
        {
            dumpWriter = new StreamWriter(string.Format("{0}.pred.{1}.model", OutputFileName, Iteration));
            TotalBatchNum = 0;
        }

        public override void Complete()
        {
            Iteration += 1;
            dumpWriter.Close();
            Logger.WriteLog(string.Format("\n Total Batch Num : {0}", TotalBatchNum));
        }


        public override void Forward()
        {
            for (int b = 0; b < BatchResult.Count; b++)
            {
                BatchResult[b].Sort((a, c) => (c.Prob).CompareTo(a.Prob));
                int t = 0;
                List<Tuple<string, string, float>> result = new List<Tuple<string, string, float>>(); 
                foreach (BeamAction beam in BatchResult[b])
                {
                    string predT = string.Join(" ", beam.Words);
                    string helpT = string.Join(" ", beam.HelpInfo);
                    result.Add(new Tuple<string, string, float>(predT, helpT, (float)beam.Prob));
                    if(++t >= TopK) break;
                }
                dumpWriter.WriteLine(string.Join("\t", result.Select(item => item.Item1 + "\t" + item.Item2 + "\t" + item.Item3)));
            }

            TotalBatchNum += BatchResult.Count;
        }
    }

    public class EnsembleTopKBeamRunner : ObjectiveRunner
    {
        List<List<BeamAction>>[] BatchResult;
        HiddenBatchData[] TreeProb;

        CudaPieceFloat TgtLabel;
        CudaPieceInt TgtSmpIdx;
        int TotalBatchNum = 0;
        int TopK = 0;
        int CorrectNum = 0;
        string OutputFileName;
        StreamWriter dumpWriter;

        public EnsembleTopKBeamRunner(List<List<BeamAction>>[] beamResults, CudaPieceInt tgtSmpIdx, CudaPieceFloat tgtLabel, HiddenBatchData[] treeprob, int topK, string dumpFile)
            : base(Structure.Empty, new RunnerBehavior())
        {
            BatchResult = beamResults;
            TgtSmpIdx = tgtSmpIdx;
            TgtLabel = tgtLabel;
            TreeProb = treeprob;
            TopK = topK;
            OutputFileName = dumpFile;
        }

        public override void Init()
        {
            dumpWriter = new StreamWriter(OutputFileName);
            TotalBatchNum = 0;
            CorrectNum = 0;
        }

        public override void Complete()
        {
            dumpWriter.Close();
            Logger.WriteLog(string.Format("\n Total Batch Num : {0}", TotalBatchNum));

            ObjectiveScore = CorrectNum * 1.0f / TotalBatchNum;
        }

        public override void Forward()
        {
            int BatchSize = BatchResult[0].Count;
            TgtSmpIdx.SyncToCPU();
            TgtLabel.SyncToCPU();

            for (int d = 0; d < BatchResult.Length; d++)
            {
                TreeProb[d].Output.Data.SyncToCPU(BatchSize);
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float maxP = 0;
                int maxD = -1;
                for (int d = 0; d < BatchResult.Length; d++)
                {
                    if(TreeProb[d].Output.Data[b] > maxP)
                    {
                        maxP = TreeProb[d].Output.Data[b];
                        maxD = d;
                    }
                }

                int start = b == 0 ? 0 : TgtSmpIdx[b - 1];
                int len = TgtSmpIdx[b] - start;
                string groundT = string.Join(" ", TgtLabel.TakeCPU(start,len));


                BatchResult[maxD][b].Sort((a, c) => (c.Prob).CompareTo(a.Prob));
                //int t = 0;
                List<Tuple<string, string, float>> result = new List<Tuple<string, string, float>>();
                string PredT = "";
                foreach (BeamAction beam in BatchResult[maxD][b])
                {
                    string predT = string.Join(" ", beam.Words.Skip(1));
                    //string helpT = string.Join(" ", beam.HelpInfo);
                    result.Add(new Tuple<string, string, float>(predT, "", (float)beam.Prob));
                    //if (++t >= TopK) break;
                    PredT = predT;
                    break;
                }

                if (groundT.Equals(PredT)) CorrectNum = CorrectNum + 1;
                dumpWriter.WriteLine(groundT + "\t" + string.Join("\t", result.Select(item => item.Item1 + "\t" + item.Item2 + "\t" + item.Item3)));
            }

            TotalBatchNum += BatchResult[0].Count;
        }
    }
}
