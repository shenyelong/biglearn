﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RNNDenseRunner : StructRunner<RNNCell, SeqDenseBatchData>
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        public RNNDenseRunner(RNNCell model, SeqDenseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new SeqDenseBatchData(data.SampleIdx, data.SentMargin,
                                            Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim, 
                                            Behavior.Device);
        }


        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            ComputeLib.Sgemm(Input.SentOutput, 0, Model.IMatrix.Output, 0, Output.SentOutput, 0, Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);

            ComputeLib.Matrix_Add_Linear(Output.SentOutput, Model.Bias.Output, Input.SentSize, Model.HiddenStateDim);

            ComputeLib.RecursiveForwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Output.SentOutput, Model.HiddenStateDim,
                                                          Model.WMatrix.Output, Model.HistoryLag, (int)Model.Af, Output.SentOutput);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.RecursiveBackwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Output.SentOutput, Model.HiddenStateDim,
                     Model.WMatrix.Output, Model.HistoryLag, (int)Model.Af, Output.SentDeriv);
            ComputeLib.MatrixMultiplicationTranspose(Output.SentDeriv, Model.IMatrix.Output, Input.SentDeriv, Input.SentSize, Model.HiddenStateDim, Model.InputFeatureDim, 1);
        }

        public override void Update()
        {
            ComputeLib.MatrixMultiplicationLeftTranspose(Input.SentOutput, Output.SentDeriv, Model.IMatrix.Deriv,
                                    Input.SentSize, Model.InputFeatureDim, Model.HiddenStateDim, 1);

            ComputeLib.RecursiveUpdateBatch(Input.SampleIdx, Input.BatchSize, Input.SentSize, Output.SentOutput, Model.HiddenStateDim,
                                        Output.SentDeriv, Model.HistoryLag, Model.WMatrix.Deriv, 1);

            if (Model.IsBias)
            {
                ComputeLib.ColumnWiseSum(Output.SentDeriv, Model.Bias.Deriv, Output.SentSize, Model.HiddenStateDim, 1);
            }
        }
    }
}
