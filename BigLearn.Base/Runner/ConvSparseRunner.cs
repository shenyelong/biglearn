﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ConvSparseRunner<IN> : StructRunner<LayerStructure, IN> where IN : SeqSparseBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat BiasOne;

        /// <summary>
        /// Used if convolutional
        /// </summary>
        CudaPieceFloat LayerPoolingOutput = null;

        /// <summary>
        /// Used if convolutional and maxpooling
        /// </summary>
        CudaPieceInt LayerMaxPoolingIndex = null;

        DropOutProcessor<HiddenBatchData> DropOutRegularized = null;

        ///// <summary>
        ///// will be removed.......
        ///// </summary>
        ///// <param name="model"></param>
        ///// <param name="behavior"></param>
        //public ConvSparseRunner(Structure model, RunnerBehavior behavior)
        //    : base(model, behavior)
        //{
        //    Output = new HiddenBatchData();
        //}

        public ConvSparseRunner(Structure model, SeqSparseBatchData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
            if (Model.Nt == N_Type.Convolution_layer)
            {
                LayerPoolingOutput = new CudaPieceFloat(Input.MAX_SENTSIZE * Model.Neural_Out, true, Behavior.Device == DeviceType.GPU);
                LayerMaxPoolingIndex = new CudaPieceInt(Input.MAX_BATCHSIZE * Model.Neural_Out, true, Behavior.Device == DeviceType.GPU);
            }

            if (Model.IsBias)
            {
                BiasOne = new CudaPieceFloat(Input.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                BiasOne.Init(1);
            }

            if (Model.DropOut > 0)
                DropOutRegularized = new DropOutProcessor<HiddenBatchData>(Output, Model.DropOut, Behavior);
        }


        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Forward()
        {
            //InitMemory();

            switch (Model.Nt)
            {
                case N_Type.Fully_Connected:
                    ComputeLib.SEQ_Sparse_Matrix_Multiply_INT(Input.SampleIdx, Input.BatchSize, Input.SequenceIdx, Input.SentSize, Input.FeaIdx,
                                                    Input.FeaValue, Input.ElementSize, Model.weight, Output.Output.Data,
                                                    Model.Neural_In, Model.Neural_Out);
                    break;
                case N_Type.Convolution_layer:
                    ComputeLib.Convolution_Sparse_Matrix_Multiply_INTEX(Input, Model.weight, LayerPoolingOutput,
                            Model.Neural_In, Model.Neural_Out, Model.N_Winsize);

                    ComputeLib.Max_Pooling(LayerPoolingOutput, Input, Output.Output.Data, LayerMaxPoolingIndex, Model.Neural_Out);
                    break;
            }

            Output.BatchSize = Input.BatchSize;

            if (Behavior.RunMode == DNNRunMode.Train && Model.DropOut > 0) DropOutRegularized.Forward();

            ActivationFuncLib.ActivationFunc(ComputeLib, Output.Output.Data, Input.BatchSize, Model.Neural_Out, Model.Af, Model.bias);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            // Backpropagate Deriv.
            //Model.DeactiveOutput(Output.Output, Output.Deriv, Behavior.RunMode);
            ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.Deriv.Data, Output.Output.Data, Input.BatchSize, Model.Neural_Out, Model.Af);

            if (Model.DropOut > 0) DropOutRegularized.Backward(false);
        }

        public override void Update()
        {
            
            switch (Model.Nt)
            {
                case N_Type.Fully_Connected:
                    /// even the 
                    ComputeLib.SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(Input.SampleIdx, Input.BatchSize, Input.SequenceIdx, Input.SentSize, Input.FeaIdx,
                        Input.FeaValue, Input.ElementSize, Model.weightGrad, Output.Deriv.Data, Model.Neural_In, Model.Neural_Out, 1);
                    break;
                case N_Type.Convolution_layer:
                    ComputeLib.Convolution_Sparse_Matrix_Product_INTEX_Weight(Output.Deriv.Data, LayerMaxPoolingIndex, Input.SequenceIdx, Input.SentMargin, Input.SentSize,
                        Model.N_Winsize, Input.BatchSize, Model.Neural_Out, Input.FeaIdx, Input.FeaValue, Model.weightGrad, Model.Neural_In, 1);
                    break;
            }

            if (Model.IsBias)
            {
                ComputeLib.Sgemv(Output.Deriv.Data, BiasOne, Input.BatchSize, Model.Neural_Out, Model.biasGrad, true, 1, 1);
                //MathOperatorManager.GlobalInstance.ColumnWiseSum(Output.Deriv.Data, Model.Bias.Grad, Input.BatchSize, Model.Neural_Out, 1);
            }
        }

        // public override bool GetActiveParameters(CudaPieceFloat parameters, out int[] fid_offsets, out int[] length)
        // {
        //     fid_offsets = null;
        //     length = null;
        //     if (this.Input is SeqSparseBatchData)
        //     {
        //         SeqSparseBatchData input = (SeqSparseBatchData)this.Input;

        //         if (this.Model.weight == parameters)
        //         {
        //             int[] inputIds = input.GetFeatureIds();

        //             int p = 0;
        //             switch (Model.Nt)
        //             {
        //                 case N_Type.Fully_Connected:
        //                     fid_offsets = new int[inputIds.Length];
        //                     length = new int[inputIds.Length];
        //                     for (int k = 0; k < inputIds.Length; k++)
        //                     {
        //                         length[p] = Model.Neural_Out;
        //                         fid_offsets[p++] = inputIds[k] * Model.Neural_Out;
        //                     }
        //                     break;
        //                 case N_Type.Convolution_layer:
        //                     fid_offsets = new int[inputIds.Length * Model.N_Winsize];
        //                     length = new int[inputIds.Length * Model.N_Winsize];
                            
        //                     p = 0;
        //                     for (int w = 0; w < Model.N_Winsize; w++)
        //                     {
        //                         int offset = w * Model.Neural_In * Model.Neural_Out;
        //                         for (int k = 0; k < inputIds.Length; k++)
        //                         {
        //                             int offset2 = offset + inputIds[k] * Model.Neural_Out;
        //                             length[p] = Model.Neural_Out;
        //                             fid_offsets[p++] = offset2;
        //                         }
        //                     }
        //                     break;
        //             }

        //             return true;
        //         }
        //     }

        //     return false;
        // }

    }

}
