using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // transpose convolutional runner.
    public class TensorTransConvRunner : StructRunner<TensorConvStructure, Tensor4DData>
    {
        public new Tensor4DData Output { get { return (Tensor4DData)base.Output; } set { base.Output = value; } }
        public new Tensor4DData Input { get { return (Tensor4DData)base.Input; } set { base.Input = value; } }

        public int PadHeight { get; set; }
        public int PadWidth { get; set; }
        public int StrideHeight { get; set; }
        public int StrideWidth { get; set; }

        //
        public TensorTransConvRunner(TensorConvStructure model, Tensor4DData input, int pad, int stride, RunnerBehavior behavior) 
            : this(model, input, pad, pad, stride, stride, behavior)
        { }

        public TensorTransConvRunner(TensorConvStructure model, Tensor4DData input, int padHeight, int padWidth, int strideHeight, int strideWidth, RunnerBehavior behavior) 
            : base(model, input, behavior)
        {
            PadHeight = padHeight;
            PadWidth = padWidth;

            StrideHeight = strideHeight;
            StrideWidth = strideWidth;

            int outputW = TensorConvUtil.TransFilterOutput(Input.Width, Model.FilterWidth, PadWidth, StrideWidth);  //FilterOutput(Input.Width, Model.FilterWidth, PadWidth, StrideWidth);
            int outputH = TensorConvUtil.TransFilterOutput(Input.Height, Model.FilterHeight, PadHeight, StrideHeight); //FilterOutput(Input.Height, Model.FilterHeight, PadHeight, StrideHeight);

            Output = new Tensor4DData(outputW, outputH, Model.InDepth, Input.MaxBatchSize, Behavior.Device);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;

            ComputeLib.CuDNNBackwardPropagateDataEx(Output.Output, Output.BatchSize, Output.Depth, Output.Height, Output.Width,
                        Model.Filter, Input.Depth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
                        Input.Output, Input.Height, Input.Width, 0, 1);

            //ComputeLib.CNNForwardPropagateEx(Input.Output, Input.BatchSize, Input.Depth, Input.Height, Input.Width,
            //    Model.Filter, Model.OutDepth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
            //    Output.Output, Output.Height, Output.Width, 0, 1);

            //ComputeLib.CuDNNBackwardPropagateDataEx(Input.Deriv, Output.BatchSize, Input.Depth, Input.Height, Input.Width,
            //            Model.Filter, Output.Depth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
            //            Output.Deriv, Output.Height, Output.Width, 1, 1);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if(Input.Deriv != null)
            {
                ComputeLib.CNNForwardPropagateEx(Output.Deriv, Output.BatchSize, Output.Depth, Output.Height, Output.Width,
                  Model.Filter, Input.Depth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
                  Input.Deriv, Input.Height, Input.Width, 1, 1);
                //ComputeLib.CuDNNBackwardPropagateDataEx(Input.Deriv, Output.BatchSize, Input.Depth, Input.Height, Input.Width,
                //        Model.Filter, Output.Depth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
                //        Output.Deriv, Output.Height, Output.Width, 1, 1);
            }

            ComputeLib.CuDNNBackwardPropagateFilterEx(Output.Deriv, Output.BatchSize, Output.Depth, Output.Height, Output.Width,
                    Model.FilterGrad, Input.Depth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
                    Input.Output, Input.Height, Input.Width, 1, 1);

            //ComputeLib.CuDNNBackwardPropagateFilterEx(Input.Output, Input.BatchSize, Input.Depth, Input.Height, Input.Width,
            //        Model.FilterGrad, Output.Depth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
            //        Output.Deriv, Output.Height, Output.Width, 1, 1);

        }
    }
}
