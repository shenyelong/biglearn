using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class VectorOpRunner : StructRunner 
    {
        public new VectorData Input { get { return (VectorData)base.Input; } set { base.Input = value; } }
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }
        public A_Func AFunc { get; private set; }

        public VectorOpRunner(VectorData input, A_Func aFunc, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Input = input;
            AFunc = aFunc;
            Output = new VectorData(input.MaxLength, behavior.Device);
        }

        public override void Forward()
        {
            Output.Length = Input.Length;
            if (Output.Length == 0) return;
            switch (AFunc)
            {
                case A_Func.Tanh:
                    ComputeLib.Tanh(Input.Output, 0, Output.Output, 0, Input.Length);
                    break;
                case A_Func.Rectified:
                    ComputeLib.ReLU(Input.Output, 0, Output.Output, 0, Input.Length);
                    break;
                case A_Func.Sigmoid:
                    ComputeLib.Logistic(Input.Output, 0, Output.Output, 0, Input.Length, 1.0f);
                    break;
            }

        }

        public override void CleanDeriv()
        {
            if (Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            switch (AFunc)
            {
                case A_Func.Tanh:
                    ComputeLib.DerivTanh(Output.Output, 0, Output.Deriv, 0, Input.Deriv, 0, Input.Length, 1, 1);
                    break;
                case A_Func.Rectified:
                    ComputeLib.DerivReLU(Output.Output, 0, Output.Deriv, 0, Input.Deriv, 0, Input.Length, 1, 1);
                    break;
                case A_Func.Sigmoid:
                    ComputeLib.DerivLogistic(Output.Output, 0, Output.Deriv, 0, Input.Deriv, 0, Input.Length, 1, 1);
                    break;
            }
        }

    }
}
