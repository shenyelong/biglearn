﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Obtain the order-kth matrix from SeqDenseBatchData.
    /// </summary>
    public class SeqOrderMatrixRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        new SeqDenseBatchData Input { get; set; }
        bool IsReverse { get; set; }
        int Order { get; set; }
        CudaPieceInt RecurrsiveMapForward = CudaPieceInt.Empty;

        public SeqOrderMatrixRunner(SeqDenseBatchData data, bool isReverse, int order, RunnerBehavior behavior)
            :this(data, isReverse, order, CudaPieceInt.Empty, behavior) { }

        public SeqOrderMatrixRunner(SeqDenseBatchData data, bool isReverse, int order, CudaPieceInt recurrsiveMapForward, RunnerBehavior behavior)
           : base(Structure.Empty, behavior)
        {
            Input = data;
            Order = order;
            IsReverse = isReverse;
            RecurrsiveMapForward = recurrsiveMapForward;
            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Input.Dim, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            ComputeLib.GetSeqOrderMatrixs(Input.SampleIdx, Input.BatchSize, Input.SentOutput, RecurrsiveMapForward, Input.Dim, IsReverse ? 1 : 0, Order, Output.Output.Data, 0, 1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.SetSeqOrderMatrixs(Input.SampleIdx, Input.BatchSize, Input.SentDeriv, RecurrsiveMapForward, Input.Dim, IsReverse ? 1 : 0, Order, Output.Deriv.Data, 1, 1);
        }
    }
}
