using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class GumbelSoftmaxRunner : StructRunner<Structure, BatchData>
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }

        RateScheduler Temp { get; set; }
        bool Hard { get; set; }
        CudaPieceFloat GumbelSamples = null;
        float CurTemp { get; set; }

        CudaPieceFloat maxProb { get; set; }
        public CudaPieceInt maxIndex { get; set; }

        IntPtr argmaxState;
        CudaPieceInt inDims = null;
        CudaPieceInt outDims = null;
        CudaPieceFloat ones = null;
        
        FloatArgument Lambda { get; set; }

        public GumbelSoftmaxRunner(HiddenBatchData logits, RateScheduler temp, bool hard, RunnerBehavior behavior) 
            : this(logits, temp, hard, new FloatArgument("lambda", 1.0f), behavior)
        { }

        public GumbelSoftmaxRunner(HiddenBatchData logits, RateScheduler temp, bool hard, FloatArgument lambda, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = logits;
            Temp = temp;
            Hard = hard;

            Lambda = lambda;

            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Input.Dim, Behavior.RunMode, Behavior.Device);
            GumbelSamples = new CudaPieceFloat(Input.MAX_BATCHSIZE * Input.Dim, Behavior.Device);

            maxProb = new CudaPieceFloat(Input.MAX_BATCHSIZE, Behavior.Device);
            maxIndex = new CudaPieceInt(Input.MAX_BATCHSIZE, Behavior.Device);

            argmaxState = ComputeLib.RegisteTensorLazyReduce(3, 4, new int[] {1, 1, Input.MAX_BATCHSIZE, Input.Dim }, new int[] {1, 1, Input.MAX_BATCHSIZE, 1 });
            Behavior.Resource.RegisteReduceState(argmaxState);

            inDims = new CudaPieceInt(2, Behavior.Device);
            outDims = new CudaPieceInt(2, Behavior.Device);
            ones = new CudaPieceFloat(Input.MAX_BATCHSIZE, Behavior.Device);
            
            inDims.Init(new int[] {1, Input.MAX_BATCHSIZE}); // new Shape(reduce_dims).DimList);
            outDims.Init(new int[] { 1, Input.Dim} ); // new Shape(in_dims).DimIndexes);
            ones.Init(1.0f);
        }

        void Sample_Gumbel(CudaPieceFloat x, int size, float eps)
        {
            x.EffectiveSize = size;
            
            ComputeLib.GenerateRand(x, size);
            //x.Print("test1", 100, true);

            ComputeLib.Gumbel(x, eps, size);
            //x.Print("test2", 100, true);
            
            /*
            generate gumbel softmax variable.
            */

            //for(int i = 0; i < size; i++)
            //{
            //    float v = (float)Util.URandom.NextDouble(); 
            //    x[i] = (float)-Math.Log(-Math.Log(v + eps) + eps);
            //}
            //x.SyncFromCPU(x.EffectiveSize);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            if (Input.BatchSize == 0) return;
            
            if(Behavior.RunMode == DNNRunMode.Train)
            {
                Sample_Gumbel(GumbelSamples, Output.BatchSize * Output.Dim, 1e-5f);
                ComputeLib.Add_Vector(GumbelSamples, Input.Output.Data, Output.BatchSize * Output.Dim, Lambda.Value, 1);
            }
            else
            {
                ComputeLib.Add_Vector(GumbelSamples, Input.Output.Data, Output.BatchSize * Output.Dim, 0, 1);
            }

            CurTemp = Temp == null ? 1 : Temp.CurrentEps;
            ComputeLib.Scale_Vector(GumbelSamples, 0, GumbelSamples, 0, Output.BatchSize * Output.Dim, 0, CurTemp);

             /// b = bwei b + awei * a;
            //void Scale_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei);

            if(Hard)
            {
                ComputeLib.SoftMax(GumbelSamples, GumbelSamples, Output.Dim, Output.BatchSize, 1);
               
                ComputeLib.TensorReduceOp(GumbelSamples, maxProb, 0, 1, argmaxState);
                ComputeLib.GetReduceIndices(argmaxState, maxIndex, Input.BatchSize);

                //ComputeLib.Maxpooling1D(GumbelSamples, Output.Dim, 1, Output.BatchSize, maxProb, maxIndex);
                
                ComputeLib.Zero(Output.Output.Data, Output.Dim * Output.BatchSize);
                
                //inDims.Init(new Shape(reduce_dims).DimList);
                //outDims.Init(new Shape(in_dims).DimIndexes);
                //ComputeLib.NDArrayMaxBackward(Output.Deriv, Indices, inDims, Input.Deriv, outDims, MaxDim, in_dims.Length, Output.Length, 1.0f);

                ComputeLib.NDArrayMaxBackward(ones, maxIndex, inDims, Output.Output.Data, outDims, 0, 2, Output.BatchSize, 1.0f);

                //maxIndex.SyncToCPU();
                //Output.Output.Data.SyncToCPU();
                //for(int i = 0; i < Output.BatchSize; i++)
                //{
                //    Output.Output.Data[i * Output.Dim + maxIndex[i]] = 1;
                //}
                //Output.Output.Data.SyncFromCPU();
                //maxIndex.SyncFromCPU();
            }
            else
            {
                ComputeLib.SoftMax(GumbelSamples, Output.Output.Data, Output.Dim, Output.BatchSize, 1);
                
                ComputeLib.TensorReduceOp(Output.Output.Data, maxProb, 0, 1, argmaxState);
                ComputeLib.GetReduceIndices(argmaxState, maxIndex, Input.BatchSize);
                
                //ComputeLib.Maxpooling1D(GumbelSamples, Output.Dim, 1, Output.BatchSize, maxProb, maxIndex);
            }
        }

        public override void CleanDeriv()
        {
            if (Input.BatchSize == 0) return;
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Input.BatchSize == 0) return;
            if(Hard)
            {
                ComputeLib.DerivSoftmax(GumbelSamples, Output.Deriv.Data, GumbelSamples, Input.Dim, Input.BatchSize, 0);
            }
            else
            {
                ComputeLib.DerivSoftmax(Output.Output.Data, Output.Deriv.Data, GumbelSamples, Input.Dim, Input.BatchSize, 0);
            }
            ComputeLib.Scale_Vector(GumbelSamples, 0, Input.Deriv.Data, 0, Output.BatchSize * Output.Dim, CurTemp, 1);
        }

    }

}
