using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LSTMStateRunner : CompositeNetRunner // StructRunner<LSTMCell, HiddenBatchData>
    {
        public HiddenBatchData pO { get; set; }
        public HiddenBatchData pC { get; set; }

        public new HiddenBatchData Input { get; set; }
        new LSTMCell Model { get; set; }

        public HiddenBatchData O { get; set; }
        public HiddenBatchData C { get; set; }

        HiddenBatchData GateF; 
        HiddenBatchData GateI; 
        HiddenBatchData GateO;
        HiddenBatchData CHat;
        HiddenBatchData TanhC;
        
        public NdArrayData o { get { return O.ToND(); } }
        public NdArrayData c { get { return C.ToND(); } }

        public LSTMStateRunner(LSTMCell model, HiddenBatchData po, HiddenBatchData pc, HiddenBatchData input, RunnerBehavior behavior) : base(behavior)
        {
            pO = po;
            pC = pc;
            Input = input;
            Model = model;

            O = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            C = new HiddenBatchData(pC.MAX_BATCHSIZE, pC.Dim, Behavior.RunMode, Behavior.Device);

            GateF = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            GateI = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            GateO = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            CHat = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);
            TanhC = new HiddenBatchData(po.MAX_BATCHSIZE, po.Dim, Behavior.RunMode, Behavior.Device);

            /*Wf X -> GateF*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(Input), new MatrixData(Model.CellDim, Model.InputFeatureDim, Model.Wf, Model.WfGrad, Behavior.Device),
                new MatrixData(GateF), 0, Behavior));
            /*Wi X -> GateI*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(Input), new MatrixData(Model.CellDim, Model.InputFeatureDim, Model.Wi, Model.WiGrad, Behavior.Device),
                new MatrixData(GateI), 0, Behavior));
            /*Wo X -> GateO*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(Input), new MatrixData(Model.CellDim, Model.InputFeatureDim, Model.Wo, Model.WoGrad, Behavior.Device),
                new MatrixData(GateO), 0, Behavior));
            /*Wc X -> C_hat*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(Input), new MatrixData(Model.CellDim, Model.InputFeatureDim, Model.Wc, Model.WcGrad, Behavior.Device),
                new MatrixData(CHat), 0, Behavior));

            /*Uf h_{t-1} -> GateF*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(pO), new MatrixData(Model.CellDim, Model.CellDim, Model.Uf, Model.UfGrad, Behavior.Device),
                new MatrixData(GateF), 1, Behavior));
            /*Ui h_{t-1} -> GateI*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(pO), new MatrixData(Model.CellDim, Model.CellDim, Model.Ui, Model.UiGrad, Behavior.Device),
                new MatrixData(GateI), 1, Behavior));
            /*Uo h_{t-1} -> GateO*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(pO), new MatrixData(Model.CellDim, Model.CellDim, Model.Uo, Model.UoGrad, Behavior.Device),
                new MatrixData(GateO), 1, Behavior));
            /*Uc h_{t-1} -> C_hat*/
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(pO), new MatrixData(Model.CellDim, Model.CellDim, Model.Uc, Model.UcGrad, Behavior.Device),
                new MatrixData(CHat), 1, Behavior));

            // GateF
            LinkRunners.Add(new BiasAdditionProcessor(new MatrixData(GateF), new VectorData(Model.CellDim, Model.Bf, Model.BfGrad, Behavior.Device), Behavior));
            LinkRunners.Add(new ActivationRunner(GateF, A_Func.Sigmoid, Behavior));
            // GateI
            LinkRunners.Add(new BiasAdditionProcessor(new MatrixData(GateI), new VectorData(Model.CellDim, Model.Bi, Model.BiGrad, Behavior.Device), Behavior));
            LinkRunners.Add(new ActivationRunner(GateI, A_Func.Sigmoid, Behavior));
            // C_hat
            LinkRunners.Add(new BiasAdditionProcessor(new MatrixData(CHat), new VectorData(Model.CellDim, Model.Bc, Model.BcGrad, Behavior.Device), Behavior));
            LinkRunners.Add(new ActivationRunner(CHat, A_Func.Tanh, Behavior));

            //c = gate_i @ c_hat
            LinkRunners.Add(new ProductRunner(new MatrixData(GateI), new MatrixData(CHat), new MatrixData(C), 0, 1, Behavior));
            //c = c + gate_f @ c_{t-1}
            LinkRunners.Add(new ProductRunner(new MatrixData(GateF), new MatrixData(pC), new MatrixData(C), 1, 1, Behavior));

            // GateO = GateO + Vo C
            LinkRunners.Add(new MatrixMultiplicationRunner(new MatrixData(C), new MatrixData(Model.CellDim, Model.CellDim, Model.Vo, Model.VoGrad, Behavior.Device),
                new MatrixData(GateO), 1, Behavior));

            // GateO
            LinkRunners.Add(new BiasAdditionProcessor(new MatrixData(GateO), new VectorData(Model.CellDim, Model.Bo, Model.BoGrad, Behavior.Device), Behavior));
            LinkRunners.Add(new ActivationRunner(GateO, A_Func.Sigmoid, Behavior));

            // tanhc = tanh(c)
            LinkRunners.Add(new ActivationRunner(C, A_Func.Tanh, TanhC, Behavior));

            // h = gate_o @ tanhc 
            LinkRunners.Add(new ProductRunner(new MatrixData(GateO), new MatrixData(TanhC), new MatrixData(O), 0, 1, Behavior));

            //ComputeLib.ElementwiseProduct(GateO.Output.Data, 0, TanhC.Output.Data, 0, O.Output.Data, 0, Input.BatchSize, Model.CellDim, 0);
        }
        
        public override void CleanDeriv()
        {
            if (O.BatchSize == 0) { return; }
            ComputeLib.Zero(O.Deriv.Data, O.Dim * Input.BatchSize);
            ComputeLib.Zero(C.Deriv.Data, C.Dim * Input.BatchSize);
            ComputeLib.Zero(GateF.Deriv.Data, O.Dim * O.BatchSize);
            ComputeLib.Zero(GateI.Deriv.Data, O.Dim * O.BatchSize);
            ComputeLib.Zero(GateO.Deriv.Data, O.Dim * O.BatchSize);
            ComputeLib.Zero(CHat.Deriv.Data, O.Dim * O.BatchSize);
            ComputeLib.Zero(TanhC.Deriv.Data, O.Dim * O.BatchSize);
        }
    }
}
