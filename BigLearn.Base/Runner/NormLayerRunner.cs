﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /*
    public class NormLayerRunner<IN>: StructRunner<NormLayerStructure, IN> where IN : HiddenBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        //BN special variable
        FastVector delta_beta = null;
        FastVector delta_gamma = null;
        FastVector delta_mu = null;
        FastVector delta_SigmaS = null;

        FastVector mu = null;
        FastVector sigma = null;

        //FastVector AdaGrad_Gamma = null;
        //FastVector AdaGrad_Beta = null;

        public int Dim = -1;
        //public NormLayerRunner(NormLayerStructure model, RunnerBehavior behavior)
        //    : base(model, behavior)
        //{
        //    Output = new HiddenBatchData();
        //}

        public NormLayerRunner(NormLayerStructure model, HiddenBatchData input, RunnerBehavior behavior)
            : base(model, input, behavior)
        {
            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.NeuronNum, Behavior.RunMode, Behavior.Device);
            Dim = Input.Dim;
            if (mu == null)
            {
                delta_beta = FastVector.Create(Dim);
                delta_gamma = FastVector.Create(Dim);
                delta_mu = FastVector.Create(Dim);
                delta_SigmaS = FastVector.Create(Dim);
                mu = FastVector.Create(Dim);
                sigma = FastVector.Create(Dim);
            }
            else
            {
                delta_beta.ZeroItems();
                delta_gamma.ZeroItems();
                delta_mu.ZeroItems();
                delta_SigmaS.ZeroItems();
                mu.ZeroItems();
                sigma.ZeroItems();
            }

        }

        public override void InitMemory()
        {
            //if (Output.MAX_BATCHSIZE < Input.MAX_BATCHSIZE)
            //{
            //    Output.Init(Input.MAX_BATCHSIZE, Model.NeuronNum, Behavior.RunMode, Behavior.Device);
            //}

            
        }

        ~NormLayerRunner()
        {
            this.Dispose(false);
        }

        private bool disposed = false;
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            if (disposing)
            {
                if (Output != null) Output.Dispose(); Output = null;
                delta_beta.Dispose();
                delta_gamma.Dispose();
                delta_SigmaS.Dispose();
                delta_mu.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Refer to the 4th page of http://arxiv.org/pdf/1502.03167v3.pdf
        /// </summary>
        /// <param></param>
        public override void Forward()
        {
            InitMemory();

            Input.Output.Data.SyncToCPU();
            
            int BatchSize = Input.BatchSize;
            int totalSize = Input.BatchSize * Input.Dim;

            for (int i = 0; i < Dim; i++)
            {
                for (int j = 0; j < totalSize; j += Dim)
                {
                    mu[i] += Input.Output.Data.MemPtr[j + i];
                    //Model.Mu.MemPtr[i] += Input.Output.Data.MemPtr[j + i];
                    //Model.Sigma.MemPtr[i] += Input.Output.Data.MemPtr[j + i] * Input.Output.Data.MemPtr[j + i];
                }
                mu[i] /= BatchSize;

                for (int j = 0; j < totalSize; j += Dim)
                {
                    float deviation = Input.Output.Data.MemPtr[j + i] - mu[i];
                    sigma[i] += deviation * deviation;
                }
                sigma[i] /= (BatchSize - 1);
                sigma[i] = (float)(Math.Sqrt(sigma[i] + 0.05));
                
                for (int j = 0; j < totalSize; j += Dim )
                {
                    //$\hat{x} = \frac{x-\mu}{\sigma}, y = \gamma * \hat{x} + \beta$
                    Output.Output.Data.MemPtr[j + i] = Model.Gamma.MemPtr[i] * (Input.Output.Data.MemPtr[j + i] - mu[i]) / sigma[i]
                        + Model.Beta.MemPtr[i];
                }
            }
            Output.BatchSize = Input.BatchSize;
            Output.Output.Data.CopyIntoCuda();
        }

        public override void CleanDeriv()
        {
            Output.Deriv.Data.Zero();
        }

        /// <summary>
        /// Refer to the 4th page of http://arxiv.org/pdf/1502.03167v3.pdf
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            // Backpropagate Deriv.
            if (cleanDeriv) Input.Deriv.Data.Zero();

            Output.Deriv.Data.CopyOutFromCuda();
            int BatchSize = Input.BatchSize;
            int totalSize = Input.BatchSize * Input.Dim;

            for (int i = 0; i < Dim; i++)
            {
                for (int j = 0; j < totalSize; j += Dim)
                {
                    float xHat = (Input.Output.Data.MemPtr[i + j] - mu[i]) / sigma[i];
                    delta_beta[i] += Output.Deriv.Data.MemPtr[i + j];
                    delta_gamma[i] += xHat * Output.Deriv.Data.MemPtr[i + j];

                    float gammaOverSigma = Model.Gamma.MemPtr[i] / sigma[i];
                    delta_mu[i] -= gammaOverSigma * Output.Deriv.Data.MemPtr[i + j];
                    delta_SigmaS[i] -= gammaOverSigma * xHat * Output.Deriv.Data.MemPtr[i + j];

                    Input.Deriv.Data.MemPtr[i + j] += gammaOverSigma * Output.Deriv.Data.MemPtr[i + j];
                }
                delta_SigmaS[i] = (float)(delta_SigmaS[i] /(2.0 * sigma[i]));

                for (int j = 0; j < totalSize; j+= Dim)
                {
                    Input.Deriv.Data.MemPtr[i + j] += 2 * (Input.Output.Data.MemPtr[i + j] - mu[i]) * delta_SigmaS[i] / (BatchSize - 1) + delta_mu[i] / BatchSize;
                }
            }

            Input.Deriv.Data.CopyIntoCuda();
        }

        /// <summary>
        /// we need to update \gamma,\beta based on delta_gamma and delta_beta here
        /// no bias here
        /// no need to update weight here
        /// </summary>
        public override void Update()
        {
            
            Model.BetaOptimizer.BeforeGradient();
            Model.BetaOptimizer.Gradient.CopyOutFromCuda();
            FastVector beta = FastVector.Create(Model.BetaOptimizer.Gradient.MemPtr);
            delta_beta.Scale(Model.BetaOptimizer.GradientStep);
            beta.Add(delta_beta);
            Model.BetaOptimizer.Gradient.CopyIntoCuda();
            Model.BetaOptimizer.AfterGradient();

            Model.GammaOptimizer.BeforeGradient();
            Model.GammaOptimizer.Gradient.CopyOutFromCuda();
            FastVector gamma = FastVector.Create(Model.GammaOptimizer.Gradient.MemPtr);
            delta_gamma.Scale(Model.GammaOptimizer.GradientStep);
            gamma.Add(delta_gamma);
            Model.GammaOptimizer.Gradient.CopyIntoCuda();
            Model.GammaOptimizer.AfterGradient();
             
        }
    }
    */
}
