using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // try to avoid in memory operation. it is very easy to cause the bug. I would rather waste the gpu memory. 
    public class VectorAddRunner : StructRunner<Structure, BatchData>
    {
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }

        public VectorData InputA { get; set; }
        public VectorData InputB { get; set; }

        float Alpha = 1;
        float Beta = 1;
        int Batch { get; set; }
        public VectorAddRunner(VectorData inputA, VectorData inputB, RunnerBehavior behavior)
            : this(inputA, inputB, 1, 1, behavior)
        { }

        public VectorAddRunner(VectorData inputA, VectorData inputB, float alpha, float beta, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Alpha = alpha;
            Beta = beta; 
            Output = new VectorData(inputA.MaxLength, behavior.Device);
        }

        public override void Forward()
        {
            if (InputA.Length  % InputB.Length != 0)
                Logger.WriteLog("In VectorAddRunner , the length of InputA and InputB does not match. InputA.length : {0} \t InputB.length : {1}", InputA.Length, InputB.Length);

            Output.Length = InputA.Length;
            if (Output.Length == 0) return; 

            if(InputA.Length == InputB.Length)
            {
                ComputeLib.Scale_Vector(InputA.Output, 0, Output.Output, 0, InputA.Length, Alpha, 0);
                ComputeLib.Scale_Vector(InputB.Output, 0, Output.Output, 0, InputB.Length, Beta, 1);
            }
            else
            {
                if(Beta != 1) 
                {
                    throw new Exception(string.Format("Only support beta = 1 at this time!!! beta = {0}", Beta));
                }

                Batch = InputA.Length / InputB.Length;

                ComputeLib.Scale_Vector(InputA.Output, 0, Output.Output, 0, InputA.Length, Alpha, 0);
                ComputeLib.Matrix_Add_Linear(Output.Output, InputB.Output, Batch, InputB.Length);
            }
        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            if(InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {
                ComputeLib.Scale_Vector(Output.Deriv, 0, InputA.Deriv, 0, Output.Length, Alpha, 1);
            }

            if(InputB.Deriv != null && !InputB.Deriv.IsEmpty)
            {
                if(InputB.Length >= InputA.Length)
                {
                    ComputeLib.Scale_Vector(Output.Deriv,  0, InputB.Deriv, 0, Output.Length, Beta, 1);
                }
                else
                {
                    RegisterWorkspace();
                    WorkSpaceB.EffectiveSize = InputA.Length / InputB.Length;
                    ComputeLib.Sgemv(Output.Deriv, WorkSpaceB, WorkSpaceB.EffectiveSize, InputB.Length, 
                                    InputB.Deriv, true, 1, Beta);
                }
            }
        }

        bool IsWorkSpace = false;
        CudaPieceFloat WorkSpaceB = null;
        void RegisterWorkspace()
        {   
            if(!IsWorkSpace)
            {
                WorkSpaceB = new CudaPieceFloat(InputA.MaxLength, Behavior.Device);
                WorkSpaceB.Init(1);
            }
            IsWorkSpace = true;
        }
    }
}
