﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum EnsemblePredType { RL_MAXITER, RL_AVGPROB }

    /// <summary>
    /// Consider the ReasoNet Agent will perform a sequence of actions based on the status.
    /// </summary>
    public class ReasoNetDecodingRunner : BasicDecodingRunner
    {
        List<Tuple<HiddenBatchData, HiddenBatchData>> Initstatus { get; set; }

        CudaPieceInt SmpIndex;
        CudaPieceInt WordIndex;
        CudaPieceInt LagInfo;
        int[] BatchIdx;

        CudaPieceFloat IEmd;
        SeqDenseBatchData GateO;
        SeqDenseBatchData GateI;
        SeqDenseBatchData CHat;
        SeqDenseBatchData GateF;
        SeqDenseBatchData TanhC;

        public List<BasicMLPAttentionRunner> AttentionLibs = null;

        List<Tuple<HiddenBatchData, HiddenBatchData>> BufferStatus;
        List<Tuple<HiddenBatchData, HiddenBatchData>> BufferStatus2;
        CudaPieceFloat tmpO;

        public new LSTMStructure Model { get { return (LSTMStructure)base.Model; } set { base.Model = value; } }

        public EmbedStructure OutputEmbed { get; set; }
        public EmbedStructure InputEmbed { get; set; }

        public override int BatchSize { get { return Initstatus[0].Item1.BatchSize; } }
        public override CudaPieceFloat WordProb { get; set; }
        CudaPieceFloat tmpWordProb { get; set; }

        HiddenBatchData Status = null;

        BasicReasoNetRunner ReasonetRunner = null;
         
        int T_MAX;
        EmbedStructure Mem;
        MLPAttentionStructure Att;
        float AttGamma;
        Att_Type AttType;
        GRUCell GRU;
        List<LayerStructure> TNets;
        List<LayerStructure> AnsNets;
        List<FullyConnectHiddenRunner<HiddenBatchData>> AnsRunners;

        EnsemblePredType PredType { get; set; }

        bool FirstAttention = true;
        public ReasoNetDecodingRunner(LSTMStructure model, EmbedStructure inputEmbed, EmbedStructure outputEmbed,
            List<Tuple<HiddenBatchData, HiddenBatchData>> initStatus, List<BasicMLPAttentionRunner> attentionLibs,
            int t_MAX, EmbedStructure mem, MLPAttentionStructure att, 
            float gamma, GRUCell gru, List<LayerStructure> t_nets, List<LayerStructure> ans_nets, Att_Type att_type,
            EnsemblePredType predType,
            RunnerBehavior behavior, bool firstAtt = false) : base(model, behavior)
        {
            FirstAttention = firstAtt;

            OutputEmbed = outputEmbed;
            InputEmbed = inputEmbed;
            Initstatus = initStatus;

            // put attention here.
            AttentionLibs = attentionLibs;

            T_MAX = t_MAX;
            Mem = mem;
            Att = att;
            AttGamma = gamma;
            AttType = att_type;
            GRU = gru;
            TNets = t_nets;
            AnsNets = ans_nets;

            PredType = predType;
        }

        public override void InitMemory(int beamSize)
        {
            int maxBatchSize = Initstatus[0].Item1.MAX_BATCHSIZE;

            BufferStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
            BufferStatus2 = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
            int maxDim = 0;
            for (int l = 0; l < Initstatus.Count; l++)
            {
                int dim = Initstatus[l].Item1.Dim;

                BufferStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(
                    new HiddenBatchData(beamSize * beamSize * maxBatchSize, dim, Behavior.RunMode, Behavior.Device),
                    new HiddenBatchData(beamSize * beamSize * maxBatchSize, dim, Behavior.RunMode, Behavior.Device)));

                BufferStatus2.Add(new Tuple<HiddenBatchData, HiddenBatchData>(
                    new HiddenBatchData(beamSize * beamSize * maxBatchSize, dim, Behavior.RunMode, Behavior.Device),
                    new HiddenBatchData(beamSize * beamSize * maxBatchSize, dim, Behavior.RunMode, Behavior.Device)));

                if (maxDim < dim) maxDim = dim;
            }
            tmpO = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * maxDim, Behavior.Device);

            GateO = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);
            GateI = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);
            CHat = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);
            GateF = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);
            TanhC = new SeqDenseBatchData(beamSize * beamSize * maxBatchSize, beamSize * beamSize * maxBatchSize, maxDim, Behavior.Device);

            IEmd = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * InputEmbed.Dim, Behavior.Device);

            LagInfo = new CudaPieceInt(beamSize * beamSize * maxBatchSize, Behavior.Device);
            WordIndex = new CudaPieceInt(beamSize * beamSize * maxBatchSize, Behavior.Device);
            SmpIndex = new CudaPieceInt(beamSize * beamSize * maxBatchSize, Behavior.Device);
            for (int i = 0; i < SmpIndex.Size; i++) SmpIndex.MemPtr[i] = i + 1; SmpIndex.SyncFromCPU();
            BatchIdx = new int[beamSize * beamSize * maxBatchSize];


            Status = new HiddenBatchData(beamSize * beamSize * maxBatchSize, maxDim, DNNRunMode.Predict, Behavior.Device);
            ReasonetRunner = new BasicReasoNetRunner(Status, T_MAX, Mem, Att, AttGamma, 1, GRU, TNets, Behavior, AttType);
            AnsRunners = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

            HiddenBatchData tmpStatus = Status;
            for (int i = 0; i < AnsNets.Count; i++)
            {
                FullyConnectHiddenRunner<HiddenBatchData> runner = new FullyConnectHiddenRunner<HiddenBatchData>(AnsNets[i], tmpStatus, Behavior);
                AnsRunners.Add(runner);
                tmpStatus = runner.Output;
            }

            tmpWordProb = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * OutputEmbed.VocabSize, Behavior.Device);
            WordProb = new CudaPieceFloat(beamSize * beamSize * maxBatchSize * OutputEmbed.VocabSize, Behavior.Device);
        }

        public void StatusUpdate(List<Tuple<HiddenBatchData, HiddenBatchData>> currentStatus, List<BeamAction> actions, 
            List<Tuple<HiddenBatchData, HiddenBatchData>> newStatus, bool isAttention)
        {
            for (int i = 0; i < actions.Count; i++)
            {
                WordIndex.MemPtr[i] = actions[i].Words.Last();
                LagInfo.MemPtr[i] = actions[i].COId;
                BatchIdx[i] = actions[i].BatchId;
            }
            WordIndex.SyncFromCPU();
            LagInfo.SyncFromCPU();

            /*Embeding Input -> Output*/
            ComputeLib.SparseSgemmMask(SmpIndex, WordIndex, CudaPieceFloat.Empty, InputEmbed.Embedding, 0, IEmd, 0, actions.Count,
                    InputEmbed.VocabSize, InputEmbed.Dim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);

            CudaPieceFloat input = IEmd;
            int inputDim = InputEmbed.Dim;

            for (int i = 0; i < Model.LSTMCells.Count; i++)
            {
                if (AttentionLibs == null || !isAttention || AttentionLibs[i] == null)
                    LSTMComputeDecodeLib.LSTMForwardDecode(ComputeLib, actions.Count, input, LagInfo, inputDim,
                        currentStatus[i].Item1, currentStatus[i].Item2, Model.LSTMCells[i], newStatus[i].Item1, newStatus[i].Item2,
                        tmpO, GateO, GateI, CHat, GateF, TanhC);
                else
                    LSTMComputeDecodeLib.LSTMForwardAttentionDecode(ComputeLib, actions.Count, input, LagInfo, inputDim,
                        currentStatus[i].Item1, currentStatus[i].Item2, Model.LSTMCells[i], newStatus[i].Item1, newStatus[i].Item2,
                        tmpO, AttentionLibs[i], AttentionLibs[i].Z, BatchIdx,
                        GateO, GateI, CHat, GateF, TanhC);

                input = newStatus[i].Item1.Output.Data;
                inputDim = newStatus[i].Item1.Dim;
                newStatus[i].Item1.BatchSize = actions.Count;
                newStatus[i].Item2.BatchSize = actions.Count;
            }
        }


        List<Tuple<HiddenBatchData, HiddenBatchData>> input { get; set; }
        List<Tuple<HiddenBatchData, HiddenBatchData>> output { get; set; }
        List<Tuple<HiddenBatchData, HiddenBatchData>> another { get; set; }

        public override void InitStatus()
        {
            if (AttentionLibs != null)
            {
                for (int i = 0; i < AttentionLibs.Count; i++)
                {
                    if (AttentionLibs[i] != null && AttentionLibs[i].IsDelegate)
                        AttentionLibs[i].Forward();
                }
            }
            input = Initstatus;
            output = BufferStatus;
            another = BufferStatus2;
        }

        /// <summary>
        /// List of BeamAction.
        /// </summary>
        /// <param name="action"></param>
        /// <param name="searchStep"></param>
        public override void TakeStep(List<BeamAction> action, int searchStep)
        {
            StatusUpdate(input, action, output, FirstAttention ? true : searchStep > 0);

            HiddenBatchData agentStatus = output[output.Count - 1].Item1;

            Status.Output.CopyFrom(agentStatus.Output); // .Data.Ref(agentStatus.Output.Data);
            //Status.Output = agentStatus.Output;
            ReasonetRunner.Forward();

            for (int i = 0; i < T_MAX; i++)
            {
                //Status.Output.Data.Ref(ReasonetRunner.StatusData[i].Output.Data);
                Status.Output.CopyFrom(ReasonetRunner.StatusData[i].Output);
                for (int t = 0; t < AnsRunners.Count; t++)
                {
                    AnsRunners[t].Forward();
                }
                HiddenBatchData hiddenData = AnsRunners.Last().Output;
                ComputeLib.Sgemm(hiddenData.Output.Data, 0, OutputEmbed.Embedding, 0, tmpWordProb, 0,
                    action.Count, OutputEmbed.Dim, OutputEmbed.VocabSize, 0, 1, false, true);
                //if (OutputEmbed.IsBias) Behavior.Computelib.Matrix_Add_Linear(tmpWordProb, OutputEmbed.Bias, action.Count, OutputEmbed.VocabSize);
                ComputeLib.SoftMax(tmpWordProb, tmpWordProb, OutputEmbed.VocabSize, action.Count, 1);

                HiddenBatchData probData = ReasonetRunner.AnsProb[i];

                ComputeLib.Scale_MatrixMask(tmpWordProb, 0, CudaPieceInt.Empty, 0, WordProb, 0, CudaPieceInt.Empty, 0, OutputEmbed.VocabSize, action.Count, probData.Output.Data, i == 0 ? 0 : 1);
            }
        }

        public override void TakeSwitch()
        {
            input = output;
            output = another;
            another = input;
        }

    }
}
