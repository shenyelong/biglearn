using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorCopyRunner : StructRunner
    {
        public NdArrayData Src { get; set; }
        public NdArrayData Tgt { get; set; }

        public TensorCopyRunner(NdArrayData src, NdArrayData tgt, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Src = src;
            Tgt = tgt;
        }
        

        public override void Forward()
        {
                    /// b = bwei b + awei * a;
                    //void Scale_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei);
            ComputeLib.Scale_Vector(Src.Output, 0, Tgt.Output, 0, Tgt.Length, 1.0f, 0.0f);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Scale_Vector(Tgt.Deriv, 0, Src.Deriv, 0, Tgt.Length, 1.0f, 1.0f);
        }

    }
}
