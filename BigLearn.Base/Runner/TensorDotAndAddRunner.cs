using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // try to avoid in memory operation. it is very easy to cause the bug. I would rather waste the gpu memory. 
    public class TensorDotAndAddRunner : StructRunner 
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }

        NdArrayData InputA { get; set; }
        NdArrayData Scale { get; set; }
        NdArrayData Bias { get; set; }


        IntArgument[] reduce_dims { get; set; }
        IntArgument[] inout_dims { get; set; }

        IntPtr sumState;

        //CudaPieceFloat tmp { get; set; }
        public TensorDotAndAddRunner(NdArrayData inputA, NdArrayData scale, NdArrayData bias, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            Scale = scale;
            Bias = bias;

            Output = new NdArrayData(InputA.Shape, behavior.Device); // new VectorData(inputA.MaxLength, behavior.Device);
            //tmp = new CudaPieceFloat(new Shape(InputA.Shape).DefaultSize, behavior.Device);

            inout_dims = InputA.Shape;
            if(inout_dims.Length < 4)
            {
                inout_dims = new IntArgument[4];
                for(int i = 0; i < 4; i++)
                {
                    if(i < InputA.Shape.Length) inout_dims[i] = InputA.Shape[i];
                    else inout_dims[i] = new IntArgument("default_dim", 1);
                }
            }

            int target_dim_num = inout_dims.Length;

            reduce_dims = scale.Shape;
            if(reduce_dims.Length < target_dim_num)
            {
                reduce_dims = new IntArgument[target_dim_num];
                for(int i = 0; i < target_dim_num; i++)
                {
                    if(i < scale.Shape.Length) reduce_dims[i] = scale.Shape[i];
                    else reduce_dims[i] = new IntArgument("default_dim", 1);
                }
            }

            sumState = ComputeLib.RegisteTensorLazyReduce(2, inout_dims.Length, new Shape(inout_dims).RDimList, new Shape(reduce_dims).RDimList);
            Behavior.Resource.RegisteReduceState(sumState);

            Behavior.Resource.RegisteFloatVector(new Shape(InputA.Shape).DefaultSize, "tmp1");
            //sumState = ComputeLib.RegisteTensorReduce(2, inout_dims.Length, new Shape(inout_dims).RDimList, new Shape(reduce_dims).RDimList);
        }

        public override void Forward()
        {
            ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, InputA.Output, 
                                                      new Shape(reduce_dims).RDimList, Scale.Output,
                                                      new Shape(inout_dims).RDimList, Behavior.Resource.MemF["tmp1"],
                                                      1, 1, 0);

            ComputeLib.TensorOp(0, inout_dims.Length, new Shape(inout_dims).RDimList, Behavior.Resource.MemF["tmp1"], 
                                                 new Shape(reduce_dims).RDimList, Bias.Output,
                                                 new Shape(inout_dims).RDimList, Output.Output,
                                                 1, 1, 0);

        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            if(InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {   
                ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Output.Deriv, 
                                                 new Shape(reduce_dims).RDimList, Scale.Output,
                                                 new Shape(inout_dims).RDimList, InputA.Deriv,
                                                 1, 1, 1);
            }

            if(Scale.Deriv != null && !Scale.Deriv.IsEmpty)
            {

                if(InputA.Length == Scale.Length)
                {
                    ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Output.Deriv, 
                                                 new Shape(inout_dims).RDimList, InputA.Output,
                                                 new Shape(inout_dims).RDimList, Scale.Deriv,
                                                 1, 1, 1);
                }
                else
                {
                    ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Output.Deriv, 
                                                     new Shape(inout_dims).RDimList, InputA.Output,
                                                     new Shape(inout_dims).RDimList, Behavior.Resource.MemF["tmp1"],
                                                     1, 1, 0);

                    ComputeLib.TensorReduceOp(Behavior.Resource.MemF["tmp1"], Scale.Deriv, 1, 1, sumState);
                }
            }

            if(Bias.Deriv != null && !Bias.Deriv.IsEmpty)
            {
                if(InputA.Length == Bias.Length)
                {
                    ComputeLib.Scale_Vector(Output.Deriv, 0, Bias.Deriv, 0, Output.Length, 1, 1);
                }
                else
                {
                    ComputeLib.TensorReduceOp(Output.Deriv, Bias.Deriv, 1, 1, sumState);
                }
            }
        }
    }
}
