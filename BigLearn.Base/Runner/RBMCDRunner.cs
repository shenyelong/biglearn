﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class RBMCDRunner<IN> : StructRunner<AutoEncoderLinkStructure, IN> where IN : BatchInputLabelData
    //{
    //    public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

    //    public DenseBatchData ph_sample;
    //    public DenseBatchData ph;

    //    public DenseBatchData pv_sample;
    //    public DenseBatchData pv;

    //    public DenseBatchData loss;

    //    //public RBMCDRunner(AutoEncoderLinkStructure model, RunnerBehavior behavior)
    //    //    : base(model, behavior)
    //    //{
    //    //    Output = new HiddenBatchData();
    //    //}


    //    //public override void InitMemory()
    //    //{
    //    //    if (Output.MAX_BATCHSIZE < Input.BatchSize)
    //    //    {
    //    //        Output.Init(Input.Stat.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
    //    //        ph_sample = new DenseBatchData(Input.Stat.MAX_BATCHSIZE, Model.Neural_Out, Behavior.Device);
    //    //        ph = new DenseBatchData(Input.Stat.MAX_BATCHSIZE, Model.Neural_Out, Behavior.Device);

    //    //        pv_sample = new DenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_FEATUREDIM, Behavior.Device);
    //    //        pv = new DenseBatchData(Input.Stat.MAX_BATCHSIZE, Input.Stat.MAX_FEATUREDIM, Behavior.Device);

    //    //        loss = new DenseBatchData(Input.Stat.MAX_BATCHSIZE, 1, Behavior.Device);  
    //    //    }
    //    //}

    //    //public RBMCDRunner(AutoEncoderLinkStructure model, BatchInputLabelData input, RunnerBehavior behavior)
    //    //    : base(model, input, behavior)
    //    //{
    //    //    Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, behavior.RunMode, behavior.Device);
    //    //    ph_sample = new DenseBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, behavior.Device);
    //    //    ph = new DenseBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, behavior.Device);

    //    //    pv_sample = new DenseBatchData(Input.MAX_BATCHSIZE, input.MAX_FEATUREDIM, behavior.Device);
    //    //    pv = new DenseBatchData(Input.MAX_BATCHSIZE, input.MAX_FEATUREDIM, behavior.Device);

    //    //    loss = new DenseBatchData(Input.MAX_BATCHSIZE, 1, behavior.Device);
    //    //}

    //    public int K = 3;

    //    ~RBMCDRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposed)
    //        {
    //            return;
    //        }

    //        disposed = true;
    //        if (disposing)
    //        {
    //            if (Output != null) Output.Dispose(); Output = null;
    //            if (ph_sample != null) ph_sample.Dispose(); ph_sample = null;
    //            if (ph != null) ph.Dispose(); ph = null;
    //            if (pv_sample != null) pv_sample.Dispose(); pv_sample = null;
    //            if (pv != null) pv.Dispose(); pv = null;
    //        }

    //        base.Dispose(disposing);
    //    }

    //    void SampleVGivenH(DenseBatchData h0_sample, DenseBatchData v1, DenseBatchData v1_sample)
    //    {
    //        Cudalib.Matrix_Multipy_Weight(h0_sample.Data.CudaPtr, Model.Weight, v1.Data.CudaPtr, Output.BatchSize, Output.Dim, Input.Stat.MAX_FEATUREDIM, 1, 0);
    //        MathOperatorManager.GlobalInstance.Matrix_Add_Sigmoid(v1.Data, Model.inBias, Output.BatchSize, Input.Stat.MAX_FEATUREDIM);
    //        Cudalib.BinomialSampling(v1.Data.CudaPtr, Output.BatchSize, Input.Stat.MAX_FEATUREDIM, v1_sample.Data.CudaPtr, (int)DateTime.Now.Ticks);

    //        v1.Data.CopyOutFromCuda();
    //        v1_sample.Data.CopyOutFromCuda();
    //    }

    //    void SampleHGivenV(DenseBatchData v0_sample, DenseBatchData h1, DenseBatchData h1_sample)
    //    {
    //        Cudalib.Matrix_Multipy_Weight(v0_sample.Data.CudaPtr, Model.Weight, h1.Data.CudaPtr, Output.BatchSize, Input.Stat.MAX_FEATUREDIM, Output.Dim, 0, 0);
    //        MathOperatorManager.GlobalInstance.Matrix_Add_Sigmoid(h1.Data, Model.hidBias, Output.BatchSize, Output.Dim);
    //        Cudalib.BinomialSampling(h1.Data.CudaPtr, Output.BatchSize, Output.Dim, h1_sample.Data.CudaPtr, (int)DateTime.Now.Ticks);
    //        h1.Data.CopyOutFromCuda();
    //        h1_sample.Data.CopyOutFromCuda();

    //    }

    //    public float ReconstructionLoss()
    //    {
    //        pv.Data.CopyOutFromCuda();
    //        Cudalib.ReconstructionLoss(pv.Data.CudaPtr, Input.SampleIdx.CudaPtr, Input.FeatureIdx.CudaPtr, Input.FeatureValue.CudaPtr, loss.Data.CudaPtr, Input.BatchSize, Input.Stat.MAX_FEATUREDIM, Util.GPUEpsilon);
    //        loss.Data.CopyOutFromCuda();
    //        return loss.Data.MemPtr.Take(Input.BatchSize).Sum();
    //    }

    //    public void Retrieve()
    //    {
    //        Cudalib.Matrix_Multipy_Weight(Output.Output.Data.CudaPtr, Model.Weight, pv.Data.CudaPtr, Output.BatchSize, Output.Dim, Input.Stat.MAX_FEATUREDIM, 1, 0);
    //        MathOperatorManager.GlobalInstance.Matrix_Add_Sigmoid(pv.Data, Model.inBias, Output.BatchSize, Input.Stat.MAX_FEATUREDIM);

    //        pv.Data.CopyOutFromCuda();
    //    }

    //    public override void Forward()
    //    {
    //        InitMemory();

    //        MathOperatorManager.MathDevice = Behavior.Device;
    //        Output.BatchSize = Input.BatchSize;
    //        MathOperatorManager.GlobalInstance.Sparse_Matrix_Multiply_INTEX_Weight(Input.SampleIdx, Input.FeatureIdx, Input.FeatureValue, Input.BatchSize, Input.Stat.MAX_FEATUREDIM,
    //                                Model.weight, Output.Output.Data, Output.Dim, 0);
    //        MathOperatorManager.GlobalInstance.Matrix_Add_Sigmoid(Output.Output.Data, Model.hidBias, Output.BatchSize, Output.Dim);

    //        Output.Output.Data.CopyOutFromCuda();
    //    }


    //    public override void Backward(bool cleanDeriv)
    //    {
    //        ph_sample.BatchSize = Output.BatchSize;
    //        pv_sample.BatchSize = Output.BatchSize;

    //        /// sample negative x. 
    //        Cudalib.BinomialSampling(Output.Output.Data.CudaPtr, Output.BatchSize, Output.Dim, ph_sample.Data.CudaPtr, (int)DateTime.Now.Ticks);
    //        Output.Output.Data.CopyOutFromCuda();
    //        ph_sample.Data.CopyOutFromCuda();

    //        for (int i = 0; i < K; i++)
    //        {
    //            SampleVGivenH(ph_sample, pv, pv_sample);
    //            SampleHGivenV(pv_sample, ph, ph_sample);
    //        }
    //        // Backpropagate Deriv.
    //    }

    //    public override void Update()
    //    {
    //        //Backpropagate Parameter Update.
    //        Model.InBiasOptimizer.BeforeGradient();
    //        Cudalib.Sparse_Matrix_Aggragate_Weight(Input.SampleIdx.CudaPtr, Input.FeatureIdx.CudaPtr, Input.FeatureValue.CudaPtr, Model.InBiasOptimizer.Gradient.CudaPtr, Input.BatchSize, Input.Stat.MAX_FEATUREDIM, Model.InBiasOptimizer.GradientStep);
    //        Cudalib.Matrix_Aggragate_Weight(pv.Data.CudaPtr, Model.InBiasOptimizer.Gradient.CudaPtr, Input.BatchSize, Input.Stat.MAX_FEATUREDIM, -Model.InBiasOptimizer.GradientStep);
    //        Model.InBiasOptimizer.AfterGradient();

    //        Model.HidBiasOptimizer.BeforeGradient();
    //        Cudalib.Matrix_Aggragate_Weight(Output.Output.Data.CudaPtr, Model.HidBiasOptimizer.Gradient.CudaPtr, Output.BatchSize, Output.Dim, Model.HidBiasOptimizer.GradientStep);
    //        Cudalib.Matrix_Aggragate_Weight(ph.Data.CudaPtr, Model.HidBiasOptimizer.Gradient.CudaPtr, Output.BatchSize, Output.Dim, -Model.HidBiasOptimizer.GradientStep);
    //        Model.HidBiasOptimizer.AfterGradient();

    //        Model.WeightOptimizer.BeforeGradient();
    //        Cudalib.Sparse_Matrix_Transpose_Multiply_INTEX_Weight(Input.SampleIdx.CudaPtr, Input.BatchSize, Input.FeatureIdx.CudaPtr, Input.FeatureValue.CudaPtr,
    //            Input.ElementSize, Model.WeightOptimizer.Gradient.CudaPtr, Output.Output.Data.CudaPtr, Input.Stat.MAX_FEATUREDIM, Output.Dim, Model.WeightOptimizer.GradientStep);
    //        Cudalib.Matrix_Product_Weight(pv_sample.Data.CudaPtr, ph.Data.CudaPtr, Model.WeightOptimizer.Gradient.CudaPtr, Input.BatchSize, Input.Stat.MAX_FEATUREDIM, Output.Dim, -Model.WeightOptimizer.GradientStep);
    //        Model.WeightOptimizer.AfterGradient();
    //    }
    //}
}
