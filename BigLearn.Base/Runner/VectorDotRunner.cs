using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // try to avoid in memory operation. it is very easy to cause the bug. I would rather waste the gpu memory. 
    public class VectorDotRunner : StructRunner<Structure, BatchData>
    {
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }

        VectorData InputA { get; set; }
        VectorData InputB { get; set; }

        float Beta = 1;
         public VectorDotRunner(VectorData inputA, VectorData inputB, RunnerBehavior behavior)
            : this(inputA, inputB, 1, behavior)
        { }

        public VectorDotRunner(VectorData inputA, VectorData inputB, float beta, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = new VectorData(inputA.MaxLength, behavior.Device);
 
            Beta = beta; 
        }

        public override void Forward()
        {
            if (InputA.Length  % InputB.Length != 0)
                Logger.WriteLog("In VectorDotRunner , the length of InputA and InputB does not match. InputA.length : {0} \t InputB.length : {1}", InputA.Length, InputB.Length);

            Output.Length = InputA.Length;
            if (Output.Length == 0) return; 

            ComputeLib.ElementwiseProductEx(InputA.Output, 1, InputA.Length, 
                                            InputB.Output, 1, InputB.Length, 
                                            Output.Output, 1, Output.Length, 0, Beta);
        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            if(InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {
                ComputeLib.ElementwiseProductEx(Output.Deriv, 1, Output.Length, 
                                                InputB.Output, 1, InputB.Length, 
                                                InputA.Deriv, 1, InputA.Length, 
                                                1, Beta);
            }

            if(InputB.Deriv != null && !InputB.Deriv.IsEmpty)
            {
                if(InputB.Length >= InputA.Length)
                {
                    ComputeLib.ElementwiseProductEx(Output.Deriv, 1, Output.Length, 
                                                InputA.Output, 1, InputA.Length, 
                                                InputB.Deriv, 1, InputB.Length, 1, Beta);
                }
                else
                {
                    RegisterWorkspace();
                    WorkSpaceA.EffectiveSize = Output.Length;
                    WorkSpaceB.EffectiveSize = InputA.Length / InputB.Length;

                    ComputeLib.ElementwiseProductEx(Output.Deriv, 1, Output.Length, 
                                                    InputA.Output, 1, InputA.Length, 
                                                    WorkSpaceA, 1, WorkSpaceA.EffectiveSize, 0, Beta);
                    ComputeLib.Sgemv(WorkSpaceA, WorkSpaceB, WorkSpaceB.EffectiveSize, InputB.Length, InputB.Deriv, true, 1, 1);
                }
            }
        }

        bool IsWorkSpace = false;
        CudaPieceFloat WorkSpaceA = null;
        CudaPieceFloat WorkSpaceB = null;
        void RegisterWorkspace()
        {   
            if(!IsWorkSpace)
            {
                WorkSpaceA = new CudaPieceFloat(InputA.MaxLength, Behavior.Device);
                WorkSpaceB = new CudaPieceFloat(InputA.MaxLength, Behavior.Device);
                WorkSpaceB.Init(1);
            }
            IsWorkSpace = true;
        }
    }
}
