using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorExpansionRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }

        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }


        int ExpanDimIndex { get; set; }
        IntArgument ExpanDim { get; set; }

        IntArgument[] in_dims;
        IntArgument[] out_dims;

        IntPtr sumState;

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public TensorExpansionRunner(NdArrayData input, int dimIdx, IntArgument expanDim, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            
            ExpanDimIndex = dimIdx;
            ExpanDim = expanDim;
            
            out_dims = new IntArgument[Math.Max(input.Shape.Length + 1, 4)]; //input.Shape.Length + 1];
            in_dims = new IntArgument[Math.Max(input.Shape.Length + 1, 4)]; //input.Shape.Length + 1];

            IntArgument[] stand_out_dims = new IntArgument[input.Shape.Length + 1];
            //Logger.WriteLog("shape length {0}, max shape length {1}", input.Shape.Length, Math.Max(input.Shape.Length + 1, 4));
            for(int i = 0; i < Math.Max(input.Shape.Length + 1, 4); i++)
            {
                if(i < input.Shape.Length + 1)
                {
                    if(i < dimIdx)
                    {
                        out_dims[i] = input.Shape[i];
                        in_dims[i] = input.Shape[i];
                    }
                    else if(i == dimIdx)
                    {
                        out_dims[i] = expanDim; 
                        in_dims[i] = new IntArgument("expan_dim", 1);
                    }
                    else if(i > dimIdx && i < input.Shape.Length + 1)
                    {
                        out_dims[i] = input.Shape[i - 1];
                        in_dims[i] = input.Shape[i - 1];
                    }
                    
                    stand_out_dims[i] = out_dims[i];
                }
                else //if(i >= input.Shape.Length + 1)
                {
                    out_dims[i] = new IntArgument("default_dim", 1);
                    in_dims[i] = new IntArgument("default_dim", 1);
                }
                //Logger.WriteLog("dim index {0}", i);
            }
            //Logger.WriteLog("yes it is");
            Output = new NdArrayData(stand_out_dims, behavior.Device); 

            sumState = ComputeLib.RegisteTensorReduce(2, out_dims.Length, new Shape(out_dims).RDimList, new Shape(in_dims).RDimList);
        }

        public override void Forward()
        {
            ComputeLib.TensorOp(0, out_dims.Length, new Shape(out_dims).RDimList, Output.Output, 
                                                 new Shape(in_dims).RDimList, Input.Output,
                                                 new Shape(out_dims).RDimList, Output.Output,
                                                 0, 1, 0);
        }

        public override void CleanDeriv()
        {
            if (Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.TensorReduceOp(Output.Deriv, Input.Deriv, 1, 1, sumState);
        }
    }
}
