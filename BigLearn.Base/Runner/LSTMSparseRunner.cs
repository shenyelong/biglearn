﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class FastLSTMSparseRunner<IN> : StructRunner<LSTMCell, IN> where IN : SeqSparseBatchData
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }
        public SeqDenseRecursiveData C;

        SeqDenseBatchData GateO;
        SeqDenseBatchData GateI;
        SeqDenseBatchData CHat;
        SeqDenseBatchData GateF;
        SeqDenseBatchData TanhC;

        CudaPieceFloat Lag1O;
        HiddenBatchData InitC = null;
        HiddenBatchData InitO = null;

        HiddenBatchData RemapInitC = null;
        HiddenBatchData RemapInitO = null;

        bool IsReverse { get; set; }
        MLPAttentionV2Runner AttentionRunner = null;

        public FastLSTMSparseRunner(LSTMCell model, SeqSparseBatchData input, bool isReverseOrder, RunnerBehavior behavior) : this(model, input, isReverseOrder, null, null, null, behavior)
        { }

        public FastLSTMSparseRunner(LSTMCell model, SeqSparseBatchData input, bool isReverseOrder, HiddenBatchData initO, HiddenBatchData initC, MLPAttentionV2Runner attentionRunner, RunnerBehavior behavior) : base(model, input, behavior)
        {
            InitC = initC;
            InitO = initO;

            if (InitO != null) RemapInitO = new HiddenBatchData(InitO.MAX_BATCHSIZE, InitO.Dim, Behavior.RunMode, Behavior.Device);
            if (InitC != null) RemapInitC = new HiddenBatchData(InitC.MAX_BATCHSIZE, InitC.Dim, Behavior.RunMode, Behavior.Device);

            IsReverse = isReverseOrder;
            GateO = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);
            GateI = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);
            CHat = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);
            GateF = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);
            TanhC = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, Behavior.Device);

            Lag1O = new CudaPieceFloat(Input.MAX_SENTSIZE * Model.CellDim, true, Behavior.Device == DeviceType.GPU);

            Output = new SeqDenseRecursiveData(
                            Input.SampleIdx, Input.SentMargin,
                            Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, 
                            Behavior.Device);

            C = new SeqDenseRecursiveData(
                            Input.SampleIdx, Input.SentMargin, Output.RecurrentInfo, 
                            Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.CellDim, 
                            Behavior.Device);
            AttentionRunner = attentionRunner;
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            C.BatchSize = Input.BatchSize;
            C.SentSize = Input.SentSize;

            if (!IsReverse) Output.RecurrentInfo.InitTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize);
            else Output.RecurrentInfo.InitReverseAndTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize);

            if (InitO != null)
            {
                RemapInitO.BatchSize = InitO.BatchSize;
                ComputeLib.Matrix_AdditionMask(InitO.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               RemapInitO.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               RemapInitO.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 0, 0);
            }

            if(InitC != null)
            {
                RemapInitC.BatchSize = InitC.BatchSize;
                ComputeLib.Matrix_AdditionMask(InitC.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               RemapInitC.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               RemapInitC.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               InitC.Dim, InitC.BatchSize, 1, 0, 0);
            }

            if (AttentionRunner == null)
            {
                LSTMComputeForwardLib.LSTMForwardv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
            else
            {
                AttentionRunner.Forward();
                LSTMComputeForwardLib.LSTMForwardAttentionv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            ComputeLib.Zero(C.SentDeriv, C.SentSize * C.Dim);
            
            if (RemapInitO != null) { ComputeLib.Zero(RemapInitO.Deriv.Data, RemapInitO.BatchSize * RemapInitO.Dim); }
            if (RemapInitC != null) { ComputeLib.Zero(RemapInitC.Deriv.Data, RemapInitC.BatchSize * RemapInitC.Dim); }

            if (AttentionRunner != null) { AttentionRunner.CleanDeriv(); }
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (AttentionRunner == null)
            {
                LSTMComputeBackwardDataLib.LSTMBackwardDatav2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output);
            }
            else
            {
                LSTMComputeBackwardDataLib.LSTMAttentionBackwardDatav2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output);
                AttentionRunner.Backward(cleanDeriv);
            }

            if (RemapInitO != null)
            {
                ComputeLib.Matrix_AdditionMask(RemapInitO.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 1, 0);
            }

            if (RemapInitC != null)
            {
                ComputeLib.Matrix_AdditionMask(RemapInitC.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                               InitC.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitC.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitC.Dim, InitC.BatchSize, 1, 1, 0);
            }
        }

        public override void Update()
        {
            //if (!IsDelegateOptimizer) { foreach (ModelOptimizer optimizer in Model.ModelOptimizers) optimizer.Optimizer.BeforeGradient(); }

            if (AttentionRunner == null)
            {
                LSTMComputeBackwardWeightLib.LSTMBackwardWeightv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
            }
            else
            {
                LSTMComputeBackwardWeightLib.LSTMAttentionBackwardWeightv2(ComputeLib, Input, RemapInitO, RemapInitC, Model, AttentionRunner, AttentionRunner.ZSeq, GateO, GateI, CHat, GateF, C, TanhC, Output, Lag1O);
                AttentionRunner.Update();
            }
            //if (!IsDelegateOptimizer) { foreach (ModelOptimizer optimizer in Model.ModelOptimizers) optimizer.Optimizer.AfterGradient(); }
        }
    }
}
