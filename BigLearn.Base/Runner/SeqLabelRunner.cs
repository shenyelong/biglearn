﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqLabelRunner<IN> : StructRunner<LayerStructure, IN> where IN : SeqDenseBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public int StackLen { get; set; }
        public SeqLabelRunner(LayerStructure model, int stackLen, SeqDenseBatchData inputData, RunnerBehavior behavior)
            : base(model, inputData, behavior)
        {
            StackLen = stackLen;
            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            ComputeLib.RNNLabelOutput(Input.SampleIdx, Input.BatchSize, Input.SentOutput, Input.SentSize, Model.weight, StackLen, Input.Dim, Model.Neural_Out, Output.Output.Data);
            ActivationFuncLib.ActivationFunc(ComputeLib, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af, Model.bias);
        }


        public override void Backward(bool cleanDeriv)
        {
            ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.Deriv.Data, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af);
            ComputeLib.DerivRNNLabelOutput(Input.SampleIdx, Input.BatchSize, Input.SentDeriv, Input.SentSize, Model.weight, StackLen, Input.Dim, Model.Neural_Out, Output.Deriv.Data);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Update()
        {
            //if(!IsDelegateOptimizer) Model.WeightOptimizer.BeforeGradient();
            ComputeLib.UpdateRNNLabelOutput(Input.SampleIdx, Input.BatchSize, Input.SentOutput, Input.SentSize,
                                    Model.weightGrad, StackLen, Input.Dim, Model.Neural_Out,
                                    Output.Deriv.Data, 1);
            //if (!IsDelegateOptimizer) Model.WeightOptimizer.AfterGradient();

            if (Model.IsBias)
            {
                //if (!IsDelegateOptimizer) Model.BiasOptimizer.BeforeGradient();
                ComputeLib.ColumnWiseSum(Output.Deriv.Data, Model.biasGrad, Input.BatchSize, Model.Neural_Out, 1);
                //if (!IsDelegateOptimizer) Model.BiasOptimizer.AfterGradient();
            }
        }
    }
}
