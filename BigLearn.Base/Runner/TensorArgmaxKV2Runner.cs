using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorArgmaxKV2Runner : StructRunner
    {
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        

        public CudaPieceInt LocalIndices { get; set; }
        public CudaPieceInt GlobalIndices { get; set; }

        IntArgument[] reduce_dims { get; set; }

        IntArgument[] in_dims { get; set; }

        IntPtr argmaxState;

        int MaxDim { get; set; }
        
        CudaPieceInt in_indexes { get; set; }
        CudaPieceInt out_dims { get; set; }

        int TopK { get; set; }
        
        public TensorArgmaxKV2Runner(NdArrayData input, int topK, int max_dim, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            TopK = topK;
            MaxDim = max_dim;


            in_dims = Input.Shape;
            if(in_dims.Length < 4)
            {
                in_dims = new IntArgument[4];
                for(int i = 0; i < 4; i++)
                {
                    if(i < Input.Shape.Length) in_dims[i] = Input.Shape[i];
                    else in_dims[i] = new IntArgument("default_dim", 1);
                }
            }

            reduce_dims = new IntArgument[in_dims.Length];
            for(int i = 0; i < reduce_dims.Length; i++)
            {
                if(i == max_dim) reduce_dims[i] = new IntArgument("reduce_dim", 1);
                else reduce_dims[i] = in_dims[i];
            }

            List<IntArgument> out_arg = reduce_dims.ToList();
            out_arg.Add(new IntArgument("topK", TopK));

            Output = new NdArrayData(out_arg.ToArray(), behavior.Device);

            LocalIndices = new CudaPieceInt(new Shape(out_arg.ToArray()).DefaultSize, behavior.Device);
            GlobalIndices = new CudaPieceInt(new Shape(out_arg.ToArray()).DefaultSize, behavior.Device);

            argmaxState = ComputeLib.RegisteTensorLazyReduce(3, in_dims.Length, new Shape(in_dims).RDimList, new Shape(reduce_dims).RDimList);
            Behavior.Resource.RegisteReduceState(argmaxState);

            out_dims = new CudaPieceInt(reduce_dims.Length, behavior.Device);
            in_indexes = new CudaPieceInt(in_dims.Length, behavior.Device);

            
            //Output = new NdArrayData(out_dims, behavior.Device);
            //Indices_f = new CudaPieceFloat(new Shape(out_dims).DefaultSize, behavior.Device);
            //Indices = new CudaPieceInt(new Shape(out_dims).DefaultSize, behavior.Device);
            //Indices_v2 = new CudaPieceInt(new Shape(out_dims).DefaultSize, behavior.Device);   
        }

        public override void Forward()
        {
            out_dims.Init(new Shape(reduce_dims).DimList);
            in_indexes.Init(new Shape(in_dims).DimIndexes);

            ComputeLib.CuDNNTensorArgmaxK(Input.Output, TopK, Output.Output, LocalIndices, GlobalIndices, new Shape(reduce_dims).Size, out_dims, in_indexes, MaxDim, in_dims.Length, argmaxState);
        }

        public override void CleanDeriv()
        {
            if (Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if(Input.Deriv != null)
            {
                ComputeLib.SparseVectorAdd(GlobalIndices, Output.Deriv, Input.Deriv, 1, Output.Length);
                //inDims.Init(new Shape(reduce_dims).DimList);
                //outDims.Init(new Shape(in_dims).DimIndexes);
                //ComputeLib.NDArrayMaxBackward(Output.Deriv, Indices, inDims, Input.Deriv, outDims, MaxDim, in_dims.Length, Output.Length, 1.0f);
            }
        }

    }
}
