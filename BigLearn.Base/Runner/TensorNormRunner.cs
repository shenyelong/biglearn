using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // try to avoid in memory operation. it is very easy to cause the bug. I would rather waste the gpu memory. 
    public class TensorNormRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        //IntArgument[] Dims { get; set; }
        CudaPieceFloat mean;
        IntPtr meanState;

        CudaPieceFloat var_1;
        CudaPieceFloat var;
        IntPtr varState;

        CudaPieceFloat w { get { return Behavior.Resource.MemF["tmp1"]; } } 
        CudaPieceFloat dw { get { return Behavior.Resource.MemF["tmp2"]; } } 

        IntArgument[] reduce_dims { get; set; }
        IntArgument[] inout_dims { get; set; }
        float Discount { get; set; }

        public TensorNormRunner(NdArrayData input, int reduceDim, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Output = new NdArrayData(behavior.Device, Input.Shape); 

            inout_dims = Input.Shape;
            if(inout_dims.Length < 4)
            {
                inout_dims = new IntArgument[4];
                for(int i = 0; i < 4; i++)
                {
                    if(i < Input.Shape.Length) inout_dims[i] = Input.Shape[i];
                    else inout_dims[i] = new IntArgument("default_dim", 1);
                }
            }

            reduce_dims = new IntArgument[inout_dims.Length];
            for (int i = 0; i < inout_dims.Length; i++)
            {
                if(i == reduceDim) 
                {
                    Discount = (float)Math.Sqrt(inout_dims[i].Default);
                    reduce_dims[i] = new IntArgument(inout_dims[i].Name, 1); 
                    Logger.WriteLog("norm layer {0} discount {1}", inout_dims[i].Default, Discount);
                }
                else reduce_dims[i] = inout_dims[i];
            }

            Behavior.Resource.RegisteFloatVector(new Shape(inout_dims).DefaultSize, "tmp1");
            Behavior.Resource.RegisteFloatVector(new Shape(inout_dims).DefaultSize, "tmp2");

            //w = new CudaPieceFloat(new Shape(inout_dims).DefaultSize, behavior.Device);
            //dw = new CudaPieceFloat(new Shape(inout_dims).DefaultSize, behavior.Device);

            mean = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            //RegisteTensorLazyReduce
            meanState = ComputeLib.RegisteTensorLazyReduce(0, inout_dims.Length, new Shape(inout_dims).RDimList, new Shape(reduce_dims).RDimList);
            Behavior.Resource.RegisteReduceState(meanState);
            //meanState = ComputeLib.RegisteTensorReduce(0, inout_dims.Length, new Shape(inout_dims).RDimList, new Shape(reduce_dims).RDimList);

            var = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            varState = ComputeLib.RegisteTensorLazyReduce(1, inout_dims.Length, new Shape(inout_dims).RDimList, new Shape(reduce_dims).RDimList);
            Behavior.Resource.RegisteReduceState(varState);
            //varState = ComputeLib.RegisteTensorReduce(1, inout_dims.Length, new Shape(inout_dims).RDimList, new Shape(reduce_dims).RDimList);

            var_1 = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);

        }
        
        public override void Forward()
        {
            ComputeLib.TensorReduceOp(Input.Output, mean, 0, 1, meanState);
            ComputeLib.TensorOp(0, reduce_dims.Length, new Shape(inout_dims).RDimList, Input.Output, 
                                                 new Shape(reduce_dims).RDimList, mean,
                                                 new Shape(inout_dims).RDimList, w,
                                                 1, -1, 0);

            ComputeLib.TensorReduceOp(w, var, 0, 1.0f / Discount, varState);

            //var.Print("stand var ",100, true);
            ComputeLib.Reciprocal(var, var_1, 0, 1, new Shape(reduce_dims).DefaultSize);

            ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(inout_dims).RDimList, w, 
                                                       new Shape(reduce_dims).RDimList, var_1,
                                                       new Shape(inout_dims).RDimList, Output.Output,
                                                       1, 1, 0);
        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;
            ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Output.Output, 
                                                      new Shape(inout_dims).RDimList, Output.Deriv,
                                                      new Shape(inout_dims).RDimList, w,
                                                      1, 1, 0);
            //w.Print("w 1", 100, true);

            ComputeLib.TensorReduceOp(w, mean, 0, 1, meanState);

            //mean.Print("mean", 100, true);

            ComputeLib.TensorOp(1, inout_dims.Length, new Shape(inout_dims).RDimList, Output.Output, 
                                                      new Shape(reduce_dims).RDimList, mean,
                                                      new Shape(inout_dims).RDimList, w,
                                                      1, 1, 0);

            //w.Print("w 2", 100, true);

            ComputeLib.Add_Vector(w, Output.Deriv, new Shape(inout_dims).DefaultSize, -1, 1); // CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei);

            //w.Print("w 3", 100, true);

            // //Console.WriteLine("tensor op 4");
            ComputeLib.TensorReduceOp(w, mean, 0, 1, meanState);
            
            //mean.Print("mean 2", 100, true);

            ComputeLib.TensorOp(0, reduce_dims.Length, new Shape(inout_dims).RDimList, w, 
                                                  new Shape(reduce_dims).RDimList, mean,
                                                  new Shape(inout_dims).RDimList, dw,
                                                  1, -1, 0);
            //dw.Print("dw", 100, true);

            // //Console.WriteLine("tensor op 5");
            ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(inout_dims).RDimList, dw, 
                                                 new Shape(reduce_dims).RDimList, var_1,
                                                 new Shape(inout_dims).RDimList, Input.Deriv,
                                                 1, 1, 1);
        }
    }
}
