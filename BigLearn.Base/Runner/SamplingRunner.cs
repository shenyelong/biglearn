using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Diagnostics;

namespace BigLearn
{
        public class SamplingRunner : StructRunner
        {
            Random mrandom { get; set; }
            RateScheduler Epsilon { get; set; }

            public CudaPieceInt ShotIndex { get; set; }
            public HiddenBatchData OneShot { get; set; }

            HiddenBatchData Prob { get; set; }

            int total_smp = 0;
            float total_smp_prob = 0;

            // epsilon greedy.
            public SamplingRunner(HiddenBatchData prob, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                : base(Structure.Empty, behavior)
            {
                mrandom = randomizer;
                Epsilon = epsilon;

                Prob = prob;
                                
                ShotIndex = new CudaPieceInt(prob.MAX_BATCHSIZE, behavior.Device);
                OneShot = new HiddenBatchData(prob.MAX_BATCHSIZE, prob.Dim, true, behavior.Device); 
            }

            public override void Init()
            {
                total_smp = 0;
                total_smp_prob = 0;
            }

            public override void Complete()
            {
                iteration += 1;
                //if(iteration % ReportPerEpoch == 0)
                //{
                Logger.WriteLog("Name {0} : Average ts sample Prob {1}", name, (total_smp_prob / total_smp));
                //}
            }

            public unsafe override void Forward()
            {
                //var perf_ts = PerfCounter.Manager.Instance["thompasSamplingRunner"].Begin();

                int batchSize = Prob.BatchSize;

                Prob.Output.Data.SyncToCPU();
                
                ShotIndex.EffectiveSize = batchSize;

                ComputeLib.Zero(OneShot.Output.Data, batchSize * OneShot.Dim);
                OneShot.Output.Data.SyncToCPU();

                float sum_prob = 0;
                for(int i = 0; i < batchSize; i++)
                {
                    float r = (float)mrandom.NextDouble();

                    int idx = 0;

                    if(Behavior.RunMode == DNNRunMode.Train)
                    {
                        idx = (Epsilon != null && Epsilon.AmIin(r)) ? 
                                   mrandom.Next(0, Prob.Dim) : Util.Sample(Prob.Output.Data.CpuPtr, i * Prob.Dim, Prob.Dim, mrandom);
                    }
                    else if(Behavior.RunMode == DNNRunMode.Predict)
                    {
                        idx = Util.MaximumValue(Prob.Output.Data.CpuPtr, i * Prob.Dim, (i + 1) * Prob.Dim) - i * Prob.Dim;
                    }

                    ShotIndex[i] = idx;
                    
                    OneShot.Output.Data[i * Prob.Dim + idx] = 1;

                    sum_prob += Prob.Output.Data[i * Prob.Dim + idx];
                }
                total_smp += batchSize;
                total_smp_prob += sum_prob;

                ShotIndex.SyncFromCPU();
                OneShot.Output.Data.SyncFromCPU();
                
                //PerfCounter.Manager.Instance["thompasSamplingRunner"].TakeCount(perf_ts);
            }
        }
}
