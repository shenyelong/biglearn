﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class FastGRUDenseRunner<IN> : StructRunner<GRUCell, IN> where IN : SeqDenseRecursiveData
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }
        public SeqDenseRecursiveData Lag1Seq; // Lag1 Sequence Output.
        //CudaPieceFloat Lag1O;

        SeqDenseRecursiveData GateR; //reset gate
        SeqDenseRecursiveData GateZ; //update gate
        SeqDenseRecursiveData GateZ_N; //update gate
        SeqDenseRecursiveData HHat; //new memory
        SeqDenseRecursiveData RestH; //new memory

        HiddenBatchData InitO = null;
        //HiddenBatchData RemapInitO = null;

        MLPAttentionV2Runner AttentionRunner = null;

        public FastGRUDenseRunner(GRUCell model, SeqDenseRecursiveData input, RunnerBehavior behavior) : this(model, input, null, null, behavior)
        { }

        public FastGRUDenseRunner(GRUCell model, SeqDenseRecursiveData input, HiddenBatchData initO,  MLPAttentionV2Runner attentionRunner, RunnerBehavior behavior) : base(model, input, behavior)
        {
            InitO = initO;
            
            GateR = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim, Behavior.Device);
            GateZ = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim, Behavior.Device);
            GateZ_N = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim, Behavior.Device);
            HHat = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim, Behavior.Device);
            RestH = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim, Behavior.Device);

            Lag1Seq = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim, Behavior.Device);
            //Lag1O = new CudaPieceFloat(Input.Stat.MAX_SEQUENCESIZE * Model.HiddenStateDim, true, Behavior.Device == DeviceType.GPU);

            Output = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, Input.RecurrentInfo, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim, Behavior.Device);
            AttentionRunner = attentionRunner;
        }
        


        public override void Forward()
        {
            if (InitO != null)
            {
                Lag1Seq.BatchSize = InitO.BatchSize;
                Lag1Seq.SentSize = Input.SentSize;

                ComputeLib.Matrix_AdditionMask(InitO.Output.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               Lag1Seq.SentOutput, 0, CudaPieceInt.Empty, 0,
                                               Lag1Seq.SentOutput, 0, CudaPieceInt.Empty, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 0, 0);
            }
            else
            {
                ComputeLib.Zero(Lag1Seq.SentOutput, Input.RecurrentInfo.BatchSize * Model.HiddenStateDim);
            }

            if (AttentionRunner == null)
            {
                GRUComputeForwardLib.GRUForwardv2(ComputeLib, Input, Lag1Seq, InitO != null, Model, GateR, GateZ, GateZ_N, HHat, RestH, Output);
            }
            else
            {
                AttentionRunner.Forward();
                //AttentionRunner.ZSeq.BatchSize = Input.BatchSize;
                AttentionRunner.ZSeq.SentSize = Input.SentSize;
                GRUComputeForwardLib.GRUForwardAttentionv2(ComputeLib, Input, Lag1Seq, InitO != null, Model, AttentionRunner, AttentionRunner.ZSeq, GateR, GateZ, GateZ_N, HHat, RestH, Output);
            }

            //put o(t-1) -> lag1O,
            ComputeLib.Matrix_AdditionMask(Output.SentOutput, 0, Output.RecurrentInfo.RecurrentBackward, Output.RecurrentInfo.BatchSize,
                                           Lag1Seq.SentOutput, Output.BatchSize * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                                           Lag1Seq.SentOutput, Output.BatchSize * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, Output.SentSize - Output.BatchSize, 1, 0, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            ComputeLib.Zero(Lag1Seq.SentDeriv, Output.SentSize * Output.Dim);
            if (AttentionRunner != null) { AttentionRunner.CleanDeriv(); }
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            /// apply lag1Seq to output.
            //put deriv_lag1 O -> deriv_o (i-1)
            ComputeLib.Matrix_AdditionMask(Lag1Seq.SentDeriv, Output.BatchSize * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                                           Lag1Seq.SentDeriv, Output.BatchSize * Model.HiddenStateDim, CudaPieceInt.Empty, 0,
                                           Output.SentDeriv, 0, Output.RecurrentInfo.RecurrentBackward, Output.RecurrentInfo.BatchSize,
                                           Model.HiddenStateDim, Output.SentSize - Output.BatchSize, 1, 0, 1);

            if (AttentionRunner == null)
            {
                GRUComputeBackwardDataLib.GRUBackwardDatav2(ComputeLib, Input, Lag1Seq, InitO != null , Model, GateR, GateZ, GateZ_N, HHat, RestH, Output);
            }
            else
            {
                GRUComputeBackwardDataLib.GRUAttentionBackwardDatav2(ComputeLib, Input, Lag1Seq, InitO != null, Model, AttentionRunner, AttentionRunner.ZSeq, GateR, GateZ, GateZ_N, HHat, RestH, Output);
                AttentionRunner.Backward(cleanDeriv);
            }

            if (InitO != null)
            {
                ComputeLib.Matrix_AdditionMask(Lag1Seq.SentDeriv, 0, CudaPieceInt.Empty, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Deriv.Data, 0, Output.RecurrentInfo.BatchShuffle, 0,
                                               InitO.Dim, InitO.BatchSize, 1, 1, 0);
            }
        }

        public override void Update()
        {
            if (AttentionRunner == null)
            {
                GRUComputeBackwardWeightLib.GRUBackwardWeightv2(ComputeLib, Input, Lag1Seq, InitO != null, Model, GateR, GateZ, GateZ_N, HHat, RestH, Output);
            }
            else
            {
                 GRUComputeBackwardWeightLib.GRUAttentionBackwardWeightv2(ComputeLib, Input, Lag1Seq, InitO != null, Model, AttentionRunner, AttentionRunner.ZSeq, GateR, GateZ, GateZ_N, HHat, RestH, Output);
                AttentionRunner.Update();
            }
        }
    }
    
    /// <summary>
    /// GRU State Runner.
    /// </summary>
    public class GRUStateRunner : StructRunner<GRUCell, HiddenBatchData>
    {
        public HiddenBatchData Query { get; set; }

        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        HiddenBatchData GateR; //reset gate
        HiddenBatchData GateZ; //update gate
        HiddenBatchData HHat; //new memory
        HiddenBatchData RestH; //new memory

        CudaPieceFloat Ones;
        CudaPieceFloat Tmp;
        public GRUStateRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
        {
            Query = query;

            Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

            GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
            RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

            Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            Ones.Init(1);

            Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);

            if(input.Dim != model.InputFeatureDim)
            {
                throw new Exception(string.Format("GRUStateRunner InputFeatureDim doesn't match {0} , {1}", input.Dim, model.InputFeatureDim)); 
            }

            if(query.Dim != model.HiddenStateDim)
            {
                throw new Exception(string.Format("GRUStateRunner HiddenStateDim doesn't match {0} , {1}", query.Dim, model.HiddenStateDim)); 
            }

        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            if (Output.BatchSize == 0) { return; }

            //var perf_gru = PerfCounter.Manager.Instance["gru"].Begin();

            //SubQuery.BatchSize = 1;
            /*Wr X -> GateR*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            
            /*h_t-1 -> GateR*/
            ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Input.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Input.BatchSize, Model.HiddenStateDim);

            /*Wz X -> GateZ*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            /*h_t-1 -> GateZ*/
            ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Input.BatchSize, Model.HiddenStateDim);

            /*Wh X -> HHat*/
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
            ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Input.BatchSize, Model.HiddenStateDim, 0);
            ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
            ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Input.BatchSize, Model.HiddenStateDim);

            //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
            // tmp = (1 - GateZ(t))
            ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                           GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Tmp, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

            //CudaPieceFloatDebug.Print("GRU Output step 1", Output.Output.Data, 300, false);
            //Logger.WriteLog("Output Batch Size {0}", Output.BatchSize);
            //Logger.WriteLog("Output Dim {0}", Output.Dim);

            //CudaPieceFloatDebug.Print("GRU HHat Output", HHat.Output.Data, 300, false);
            //CudaPieceFloatDebug.Print("GRU Gate Z Output", GateZ.Output.Data, 300, false);

            // NewQuery = GateZ(t) * HHat
            ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);


            //CudaPieceFloatDebug.Print("GRU Output step 2", Output.Output.Data, 300, false);
            //CudaPieceFloatDebug.Print("GRU GateR Output", GateR.Output.Data, 300, true);
            
            // NewQuery += tmp * h_t-1
            ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
        
            //CudaPieceFloatDebug.Print("GRU Input", Input.Output.Data, 300, false);
            //CudaPieceFloatDebug.Print("GRU Output Final", Output.Output.Data, 300, true);

           //PerfCounter.Manager.Instance["gru"].TakeCount(perf_gru);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) { return; }

            if (Query.Deriv != null)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Input.BatchSize, Model.HiddenStateDim, 1);
            }

            ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
            // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
            ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                           Tmp, 0, CudaPieceInt.Empty, 0,
                                           Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
            ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

            ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
            ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

            ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
            if (Query.Deriv != null)
            {
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);
            }

            ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
            ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

            if (Query.Deriv != null)
            {
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
            }
            ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);


            ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhGrad, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, 1, true, false);
            ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrGrad, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, 1, true, false);
            ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzGrad, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, 1, true, false);

            ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhGrad, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, true, false);
            ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrGrad, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, true, false);
            ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzGrad, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, true, false);

            ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhGrad, Output.BatchSize, Model.HiddenStateDim, 1);
            ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrGrad, Output.BatchSize, Model.HiddenStateDim, 1);
            ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzGrad, Output.BatchSize, Model.HiddenStateDim, 1);

        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) { return; }
            ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
        }
    }


    

}
