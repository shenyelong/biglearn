using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class NdArraySoftmaxRunner : StructRunner<Structure, BatchData>
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }

        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        public NdArraySoftmaxRunner(NdArrayData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Output = new NdArrayData(Input.Dimensions, behavior.Device);
        }

        public override void Forward()
        {
            Output.Length = Input.Length;
            if (Input.Length == 0) return;
            int Batch = Input.Length / Input.Dimensions[0].Value; 
            ComputeLib.SoftMax(Input.Output, Output.Output, Input.Dimensions[0].Value, Batch, 1);
        }

        public override void CleanDeriv()
        {
            if (Input.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Input.Length == 0) return;
            int Batch = Input.Length / Input.Dimensions[0].Value; 
            ComputeLib.DerivSoftmax(Output.Output, Output.Deriv, Input.Deriv, Input.Dimensions[0].Value, Batch, 1);
        }

    }

}
