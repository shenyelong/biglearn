using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ProductRunner : StructRunner<Structure, BatchData>
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        HiddenBatchData InputA { get; set; }
        HiddenBatchData InputB { get; set; }

        float Alpha = 0;
        float Beta = 1;
        public ProductRunner(HiddenBatchData inputA, HiddenBatchData inputB, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = new HiddenBatchData(InputA.MAX_BATCHSIZE, InputA.Dim, Behavior.RunMode, Behavior.Device);
            
            Alpha = 0;
            Beta = 1;
        }

        // 0, 0, 0, 3, 
        // 0, x, x, 0
        // 
        public ProductRunner(MatrixData inputA, MatrixData inputB, MatrixData output, float alpha, float beta, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = new HiddenBatchData(inputA);
            InputB = new HiddenBatchData(inputB);
            Output = new HiddenBatchData(output);
            
            Alpha = alpha;
            Beta = beta;
        }
        
        // public ProductRunner(MatrixData inputA, VectorData inputB, RunnerBehavior behavior)
        //     : base(Structure.Empty, behavior)
        // {
        // }

        public override void Forward()
        {
            if (InputA.BatchSize != InputB.BatchSize)
                Logger.WriteLog("In Product Runner , the BatchSize of InputA and InputB does not match. InputA.BatchSize : {0} \t InputB.BatchSize : {1}", InputA.BatchSize, InputB.BatchSize);

            if (InputA.Dim != InputB.Dim)
                Logger.WriteLog("In Product Runner , the Dim of InputA and InputB does not match. InputA.Dim : {0} \t InputB.Dim : {1}", InputA.Dim, InputB.Dim);

            Output.BatchSize = InputA.BatchSize;
            if (Output.BatchSize == 0) return; 

            ComputeLib.ElementwiseProductMask(InputA.Output.Data, 0,
                                              InputB.Output.Data, 0,
                                              Output.Output.Data, 0,
                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              InputA.BatchSize, InputA.Dim, Alpha, Beta);
        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) return;
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) return;

            if(InputB.Deriv != null && InputB.Deriv.Data != null && !InputB.Deriv.Data.IsEmpty)
            {
                ComputeLib.ElementwiseProductMask(Output.Deriv.Data, 0,
                                              InputA.Output.Data, 0,
                                              InputB.Deriv.Data, 0,
                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              Output.BatchSize, InputA.Dim, 1, Beta);
            }

            if(InputA.Deriv != null && InputA.Deriv.Data != null && !InputA.Deriv.Data.IsEmpty)
            {
                ComputeLib.ElementwiseProductMask(Output.Deriv.Data, 0,
                                             InputB.Output.Data, 0,
                                             InputA.Deriv.Data, 0,
                                             CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                             Output.BatchSize, InputB.Dim, 1, Beta);
            }
        }

    }
}
