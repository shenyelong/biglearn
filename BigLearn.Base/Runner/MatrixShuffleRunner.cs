﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixShuffleRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }

        CudaPieceInt OutShuffle;
        CudaPieceInt InShuffle;
        public MatrixShuffleRunner(HiddenBatchData data, CudaPieceInt inShuffle, CudaPieceInt outShuffle, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Input = data;
            InShuffle = inShuffle;
            OutShuffle = outShuffle;
            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Input.Dim, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            ComputeLib.Matrix_AdditionMask(Input.Output.Data, 0, InShuffle, 0,
                                           Output.Output.Data, 0, OutShuffle, 0,
                                           Output.Output.Data, 0, OutShuffle, 0,
                                           Input.Dim, Input.BatchSize, 1, 0, 0);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Matrix_AdditionMask(Output.Deriv.Data, 0, OutShuffle, 0,
                                           Input.Deriv.Data, 0, InShuffle, 0,
                                           Input.Deriv.Data, 0, InShuffle, 0,
                                           Output.Dim, Output.BatchSize, 1, 1, 0);
        }
        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }
    }

    

}
