using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
	public class GRUStateV2Runner : CompositeNetRunner // StructRunner<LSTMCell, HiddenBatchData>
    {
        public HiddenBatchData pO { get; set; }

        new GRUCellV2 Model { get; set; }

        public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public GRUStateV2Runner(GRUCellV2 model, HiddenBatchData po, HiddenBatchData input, RunnerBehavior behavior) : base(behavior)
        {
            pO = po;
            Input = input;
            Model = model;

            EnsembleMatrixRunner xoRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { Input, pO }, Behavior);
            LinkRunners.Add(xoRunner);

            LayerStructure zrLayer = new LayerStructure(Model.InputFeatureDim + Model.HiddenStateDim, Model.HiddenStateDim * 2, A_Func.Sigmoid, true, Model.Wzr, Model.WzrGrad, Model.Bzr, Model.BzrGrad, Behavior.Device);

            FullyConnectHiddenRunner<HiddenBatchData> zrRunner = new FullyConnectHiddenRunner<HiddenBatchData>(zrLayer, xoRunner.Output, Behavior);
            LinkRunners.Add(zrRunner);

            MatrixData zrData = new MatrixData(zrRunner.Output);
            SubcolMatrixRunner zRunner = new SubcolMatrixRunner(zrData, new IntArgument("z", 0), Model.HiddenStateDim, Behavior);
            LinkRunners.Add(zRunner);

            SubcolMatrixRunner rRunner = new SubcolMatrixRunner(zrData, new IntArgument("r", Model.HiddenStateDim), Model.HiddenStateDim, Behavior);
            LinkRunners.Add(rRunner);

            ProductRunner srRunner = new ProductRunner(pO, new HiddenBatchData(rRunner.Output), Behavior);
            LinkRunners.Add(srRunner);

            EnsembleMatrixRunner xsrRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { Input, srRunner.Output }, Behavior);
            LinkRunners.Add(xsrRunner);

            LayerStructure hLayer = new LayerStructure(Model.InputFeatureDim + Model.HiddenStateDim, Model.HiddenStateDim, A_Func.Tanh, true, 
                        Model.Wh, Model.WhGrad, Model.Bh, Model.BhGrad, Behavior.Device);

            FullyConnectHiddenRunner<HiddenBatchData> hRunner = new FullyConnectHiddenRunner<HiddenBatchData>(hLayer, xsrRunner.Output, Behavior);
            LinkRunners.Add(hRunner);

            ProductRunner zhRunner = new ProductRunner(hRunner.Output, new HiddenBatchData(zRunner.Output), Behavior);
            LinkRunners.Add(zhRunner);

            ProductRunner zsRunner = new ProductRunner(new HiddenBatchData(zRunner.Output), pO, Behavior);
            LinkRunners.Add(zsRunner);

            AdditionExRunner h_zhRunner = new AdditionExRunner(hRunner.Output, zhRunner.Output, null, 1, -1, Behavior);
            LinkRunners.Add(h_zhRunner);

            AdditionExRunner sRunner = new AdditionExRunner(h_zhRunner.Output, zsRunner.Output, null, 1, 1, Behavior);
            LinkRunners.Add(sRunner);

            Output = sRunner.Output;
        }
    }
}