﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class EnsembleAvgSeqRunner : StructRunner
    {
        public new  SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

        List<SeqDenseBatchData> InputList = new List<SeqDenseBatchData>();
        List<float> InputWeight = new List<float>();

        public EnsembleAvgSeqRunner(SeqDenseBatchData mainData, float mainWei,  RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputList.Add(mainData);
            InputWeight.Add(mainWei);
            Output = mainData;
        }

        public void EnsembleSample(SeqDenseBatchData sData, float sWei)
        {
            InputList.Add(sData);
            InputWeight.Add(sWei);
        }

        public override void Forward()
        {
            Behavior.Computelib.Scale_Matrix(InputList[0].SentOutput, Output.SentSize, Output.Dim, InputWeight[0]);

            for(int i=1;i<InputList.Count;i++)
            {
                Behavior.Computelib.Matrix_Add(Output.SentOutput, InputList[i].SentOutput, Output.SentSize, Output.Dim, InputWeight[i]);
            }
        }

        public override void CleanDeriv()
        {
            Output.SentDeriv.Zero();
        }

        public override void Backward(bool cleanDeriv)
        {
            for (int i = 1; i < InputList.Count; i++)
            {
                Behavior.Computelib.Matrix_Add(InputList[i].SentDeriv, Output.SentDeriv, Output.SentSize, Output.Dim, InputWeight[i]);
            }
            Behavior.Computelib.Scale_Matrix(InputList[0].SentDeriv, Output.SentSize, Output.Dim, InputWeight[0]);
        }
    }
    
    public class EnsembleMatrixRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        List<HiddenBatchData> InputList = new List<HiddenBatchData>();

        public EnsembleMatrixRunner(List<HiddenBatchData> dataList, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputList = dataList;

            int maxBatchSize = dataList[0].MAX_BATCHSIZE;
            int dim = dataList[0].Dim;
            int baseDim = dataList[0].Dim;
            for (int i = 1; i < dataList.Count; i++)
            {
                if (dataList[i].MAX_BATCHSIZE != maxBatchSize) {
                    throw new Exception(string.Format("Maximum BatchSize in EnsembleMatrixRunner Doesn't mmatch!, {0}, {1}", dataList[i].MAX_BATCHSIZE, maxBatchSize)); }
                dim += dataList[i].Dim;
            }
            Output = new HiddenBatchData(maxBatchSize, dim, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = InputList[0].BatchSize;
            int offsetDim = 0;
            //var perf_ensemble = PerfCounter.Manager.Instance["ensembleMatrix"].Begin();

            for (int i = 0; i < InputList.Count; i++)
            {
                if(InputList[i].BatchSize != Output.BatchSize) { throw new Exception(string.Format("BatchSize doesn't matach in EnsembleMatrixRunner {0}, {1}", InputList[i].BatchSize, Output.BatchSize)); }
                ComputeLib.Matrix_AdditionEx(InputList[i].Output.Data, 0, InputList[i].Dim,
                                             InputList[i].Output.Data, 0, InputList[i].Dim,
                                             Output.Output.Data, offsetDim, Output.Dim,
                                             InputList[i].Dim, InputList[i].BatchSize, 1, 0, 0);
                offsetDim += InputList[i].Dim;
            }
            //PerfCounter.Manager.Instance["ensembleMatrix"].TakeCount(perf_ensemble);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            int offsetDim = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                if(InputList[i].Deriv != null && ! InputList[i].Deriv.Data.IsEmpty)
                {
                    ComputeLib.Matrix_AdditionEx(Output.Deriv.Data, offsetDim, Output.Dim,
                                             InputList[i].Deriv.Data, 0, InputList[i].Dim,
                                             InputList[i].Deriv.Data, 0, InputList[i].Dim,
                                             InputList[i].Dim, InputList[i].BatchSize, 1, 0, 1);
                }
                offsetDim += InputList[i].Dim;
            }

        }
    }

    public class EnsembleSeqMatrixRunner : StructRunner
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
        //CudaPieceInt OutputMask;
        List<HiddenBatchData> InputList = new List<HiddenBatchData>();

        public EnsembleSeqMatrixRunner(List<HiddenBatchData> dataList, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputList = dataList;
            int maxBatchSize = dataList[0].MAX_BATCHSIZE;
            //int dim = dataList[0].Dim;
            int baseDim = dataList[0].Dim;
            for (int i = 1; i < dataList.Count; i++)
            {
                if (dataList[i].MAX_BATCHSIZE != maxBatchSize) { throw new Exception("Maximum BatchSize in EnsembleMatrixRunner Doesn't mmatch!"); }
                //dim += dataList[i].Dim;
                if(dataList[i].Dim != baseDim ) { throw new Exception(
                    string.Format("Data Dimension in EnsembleSeqMatrixRunner doesn't match! {0} \t {1}", baseDim, dataList[i].Dim)); }
            }

            Output = new SeqDenseBatchData(maxBatchSize, maxBatchSize * dataList.Count, baseDim, Behavior.Device);
            Output.SampleIdx = new CudaPieceInt(maxBatchSize, Behavior.Device);
            Output.SentMargin = new CudaPieceInt(maxBatchSize * dataList.Count, Behavior.Device);
            for (int i = 0; i < maxBatchSize; i++)
            {
                for (int t = 0; t < dataList.Count; t++)
                {
                    Output.SentMargin.MemPtr[i * dataList.Count + t] = i;
                }
                Output.SampleIdx.MemPtr[i] = dataList.Count * (i + 1);
            }
            Output.SampleIdx.SyncFromCPU(maxBatchSize);
            Output.SentMargin.SyncFromCPU(maxBatchSize * dataList.Count);
        }

        public override void Forward()
        {
            Output.BatchSize = InputList[0].BatchSize;
            Output.SentSize = Output.BatchSize * InputList.Count;
            int offsetDim = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                if (InputList[i].BatchSize != Output.BatchSize) { throw new Exception("BatchSize doesn't matach in EnsembleMatrixRunner"); }

                ComputeLib.Matrix_AdditionEx(InputList[i].Output.Data, 0, InputList[i].Dim,
                                             InputList[i].Output.Data, 0, InputList[i].Dim,
                                             Output.SentOutput, offsetDim, Output.Dim,
                                             InputList[i].Dim, InputList[i].BatchSize, 1, 0, 0);
                offsetDim += InputList[i].Dim;
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            int offsetDim = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, offsetDim, Output.Dim,
                                             InputList[i].Deriv.Data, 0, InputList[i].Dim,
                                             InputList[i].Deriv.Data, 0, InputList[i].Dim,
                                             InputList[i].Dim, InputList[i].BatchSize, 1, 0, 1);
                offsetDim += InputList[i].Dim;
            }

        }
    }



    public class EnsembleConcateSeqRunner : StructRunner
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
        //CudaPieceInt OutputMask;
        List<SeqDenseBatchData> InputList = new List<SeqDenseBatchData>();

        public EnsembleConcateSeqRunner(List<SeqDenseBatchData> dataList, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {

            InputList = dataList;
            int maxBatchSize = dataList[0].MAX_BATCHSIZE;
            int maxSeqSize = dataList[0].MAX_SENTSIZE;
            int dim = dataList[0].Dim;
            //int baseDim = dataList[0].Stat.FEATURE_DIM;
            for (int i = 1; i < dataList.Count; i++)
            {
                maxBatchSize = Math.Max(dataList[i].MAX_BATCHSIZE, maxBatchSize);
                maxSeqSize = Math.Max(dataList[i].MAX_SENTSIZE, maxSeqSize);
                dim += dataList[i].Dim;
            }
            Output = new SeqDenseBatchData(InputList[0].SampleIdx, InputList[0].SentMargin, maxBatchSize, maxSeqSize, dim, Behavior.Device);
        }
        
        public override void Forward()
        {
            iteration++;
            Output.BatchSize = InputList[0].BatchSize;
            Output.SentSize = InputList[0].SentSize;

            int offsetDim = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                ComputeLib.Matrix_AdditionEx(InputList[i].SentOutput, 0, InputList[i].Dim,
                                             InputList[i].SentOutput, 0, InputList[i].Dim,
                                             Output.SentOutput, offsetDim, Output.Dim,
                                             InputList[i].Dim, InputList[i].SentSize, 1, 0, 0);
                offsetDim += InputList[i].Dim;
            }
        }
        
        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            int offsetDim = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, offsetDim, Output.Dim,
                                             InputList[i].SentDeriv, 0, InputList[i].Dim,
                                             InputList[i].SentDeriv, 0, InputList[i].Dim,
                                             InputList[i].Dim, InputList[i].SentSize, 1, 0, 1);
                offsetDim += InputList[i].Dim;
            }

        }
    }

    
}
