using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // try to avoid in memory operation. it is very easy to cause the bug. I would rather waste the gpu memory. 
    public class VectorDotAndAddRunner : StructRunner<Structure, BatchData>
    {
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }

        VectorData InputA { get; set; }
        VectorData InputB { get; set; }
        VectorData InputC { get; set; }

        float Alpha = 1;
        float Beta = 1;
         public VectorDotAndAddRunner(VectorData inputA, VectorData inputB, VectorData inputC, float alpha, float beta, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            InputC = inputC;

            Output = new VectorData(inputA.MaxLength, behavior.Device);
            
            Alpha = alpha;
            Beta = beta; 
        }

        public override void Forward()
        {
            if (InputA.Length  % InputB.Length != 0)
                Logger.WriteLog("In VectorDotAndAddRunner , the length of InputA and InputB does not match. InputA.length : {0} \t InputB.length : {1}", InputA.Length, InputB.Length);

            Output.Length = InputA.Length;
            if (Output.Length == 0) return; 

            ComputeLib.ElementwiseProductEx(InputA.Output, 1, InputA.Length, 
                                            InputB.Output, 1, InputB.Length, 
                                            Output.Output, 1, Output.Length, 0, Alpha);
            if(Output.Length <= InputC.Length)
            {
                ComputeLib.Scale_Vector(InputC.Output, 0, Output.Output, 0, Output.Length, Beta, 1);
            }
            else
            {
                int Batch = Output.Length / InputC.Length;
                if(Beta != 1) 
                {
                    throw new Exception(string.Format("Only support beta = 1 at this time!!! beta = {0}", Beta));
                }
                ComputeLib.Matrix_Add_Linear(Output.Output, InputC.Output, Batch, InputC.Length);
            }

        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            if(InputA.Deriv != null && !InputA.Deriv.IsEmpty)
            {
                ComputeLib.ElementwiseProductEx(Output.Deriv, 1, Output.Length, 
                                                InputB.Output, 1, InputB.Length, 
                                                InputA.Deriv, 1, InputA.Length, 
                                                1, Alpha);
            }

            if(InputB.Deriv != null && !InputB.Deriv.IsEmpty)
            {
                if(InputB.Length >= InputA.Length)
                {
                    ComputeLib.ElementwiseProductEx(Output.Deriv, 1, Output.Length, 
                                                InputA.Output, 1, InputA.Length, 
                                                InputB.Deriv, 1, InputB.Length, 1, Alpha);
                }
                else
                {
                    RegisterWorkspace1();
                    WorkSpaceA.EffectiveSize = Output.Length;
                    WorkSpaceB.EffectiveSize = InputA.Length / InputB.Length;

                    ComputeLib.ElementwiseProductEx(Output.Deriv, 1, Output.Length, 
                                                    InputA.Output, 1, InputA.Length, 
                                                    WorkSpaceA, 1, WorkSpaceA.EffectiveSize, 0, Alpha);
                    ComputeLib.Sgemv(WorkSpaceA, WorkSpaceB, WorkSpaceB.EffectiveSize, InputB.Length, InputB.Deriv, true, 1, 1);
                }
            }

            if(InputC.Deriv != null && !InputC.Deriv.IsEmpty)
            {
                if(InputC.Length >= InputA.Length)
                {
                    ComputeLib.Scale_Vector(Output.Deriv,  0, InputC.Deriv, 0, Output.Length, Beta, 1);
                }
                else
                {
                    RegisterWorkspace2();
                    WorkSpaceB.EffectiveSize = InputA.Length / InputC.Length;
                    ComputeLib.Sgemv(Output.Deriv, WorkSpaceB, WorkSpaceB.EffectiveSize, InputC.Length, 
                                    InputC.Deriv, true, 1, Beta);
                }
            }
        }

        CudaPieceFloat WorkSpaceA = null;
        CudaPieceFloat WorkSpaceB = null;
        void RegisterWorkspace1()
        {   
            if(WorkSpaceA == null)
            {
                WorkSpaceA = new CudaPieceFloat(InputA.MaxLength, Behavior.Device);
            }
            if(WorkSpaceB == null)
            {
                WorkSpaceB = new CudaPieceFloat(InputA.MaxLength, Behavior.Device);
                WorkSpaceB.Init(1);
            }
        }

        void RegisterWorkspace2()
        {   
            if(WorkSpaceB == null)
            {
                WorkSpaceB = new CudaPieceFloat(InputA.MaxLength, Behavior.Device);
                WorkSpaceB.Init(1);
            }
        }

    }
}
