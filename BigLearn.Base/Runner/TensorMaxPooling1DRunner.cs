using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorMaxPooling1DRunner : StructRunner<Structure, Tensor3DData>  
    {
        public new MatrixData Output { get { return (MatrixData) base.Output; } set { base.Output = value; } }

        public CudaPieceInt LayerMaxPoolingIndex = null;

 
        public TensorMaxPooling1DRunner(Tensor3DData data, RunnerBehavior behavior) : 
            base(Structure.Empty, data, behavior)
        {
            Output = new MatrixData(Input.Width, Input.MaxBatchSize, Behavior.Device);
            LayerMaxPoolingIndex = new CudaPieceInt(Input.MaxBatchSize * Input.Width, Behavior.Device);
        }
        
        public override void Forward()
        {
            iteration++;
            Output.Row = Input.BatchSize;
            if (Output.Row == 0) return;
            ComputeLib.Maxpooling1D(Input.Output, Input.Height, Input.Width, Input.BatchSize, Output.Output, LayerMaxPoolingIndex);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Row == 0) return;
            throw new NotImplementedException();
        }

        public override void CleanDeriv()
        {
            if (Output.Row == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

    }
}
