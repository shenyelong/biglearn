using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    public class SelfAttPoolRunner : CompositeNetRunner 
    {
        //public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }
        
        public new HiddenBatchData Output  { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } } 

        new SelfAttPoolStructure Model { get; set; }

        public CudaPieceInt AttIndex { get; set; }
        
        public SelfAttPoolRunner(SelfAttPoolStructure model, NdArrayData input, int nhead, RateScheduler tempRate, bool hardGumbel, RunnerBehavior behavior) : base(behavior)
        {
            Model = model;
            Input = input;
            
            IntArgument emd_dim = new IntArgument("emd", Model.StateDim);

            IntArgument input_dim = Input.Dimensions[0];
            IntArgument active_dim = Input.Dimensions[1];  
            IntArgument batch_size = Input.Dimensions[2];  

            if(Model.StateDim % nhead != 0)
            {
                throw new Exception(string.Format("the model state dim {0}, nhead {1}, they are dividable", Model.StateDim, nhead));
            }

            // first build key-value pair from data.
            IntArgument head_dim = new IntArgument("head-dim", Model.StateDim / nhead);
            IntArgument head_num = new IntArgument("head-num", nhead);
            IntArgument head_batch = new IntArgument("head-batch", nhead * batch_size.Default);

            NdArrayData kfeature = Input.FNN(Model.KProject, Session, Behavior).Reshape(head_dim, head_num, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);
            NdArrayData vfeature = Input.FNN(Model.VProject, Session, Behavior).Reshape(head_dim, head_num, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);

            //AttIndex = new List<CudaPieceInt>();

            //List<HiddenBatchData> si_list = new List<HiddenBatchData>();
            //for(int l = 0; l < Model.RecurrentLayer; l++) si_list.Add(states[l]);

            NdArrayData initZero = new NdArrayData(Behavior.Device, false, emd_dim, batch_size);  
            initZero.Output.Init(0); 

            HiddenBatchData initState = new HiddenBatchData(1, Model.StateDim, Model.QProject.Embedding, Model.QProject.EmbeddingGrad, Model.QProject.DeviceType);
            NdArrayData q = initZero.AddExpand(initState.ToND(), Session, Behavior).Reshape(head_dim, new IntArgument("seq", 1), head_num, batch_size);

            NdArrayData attScore = q.MatMul(0, kfeature, 1, Session, Behavior);

            GumbelSoftmaxRunner smRunner = new GumbelSoftmaxRunner(attScore.Reshape(active_dim, head_batch).ToHDB(), tempRate, hardGumbel, Behavior);                
            Session.Add(smRunner);
            AttIndex = smRunner.maxIndex;

            attScore = smRunner.Output.ToND().Reshape(attScore.Shape);

            NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
            Output = aData.Reshape(emd_dim, batch_size).ToHDB();
        }
    }

}
