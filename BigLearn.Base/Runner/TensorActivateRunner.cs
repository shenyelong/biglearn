using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorActivateRunner : StructRunner
    {
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public A_Func AFunc { get; private set; }

        public TensorActivateRunner(NdArrayData input, A_Func aFunc, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Input = input;
            AFunc = aFunc;
            Output = new NdArrayData(behavior.Device, Input.Shape); 

            if(A_Func.Log == aFunc)
            {
                behavior.Resource.RegisteFloatVector(Input.MaxLength, "tmp1");
            }
        }

        public override void Forward()
        {
	    Output.Length = Input.Length;
            switch (AFunc)
            {
                case A_Func.Tanh:
                    ComputeLib.Tanh(Input.Output, 0, Output.Output, 0, Input.Length);
                    break;
                case A_Func.Rectified:
                    ComputeLib.ReLU(Input.Output, 0, Output.Output, 0, Input.Length);
                    break;
                case A_Func.Sigmoid:
                    ComputeLib.Logistic(Input.Output, 0, Output.Output, 0, Input.Length, 1.0f);
                    break;
                case A_Func.Gelu:
                    ComputeLib.Gelu(Input.Output, Output.Output, Input.Length);
                    break;
                case A_Func.Log:
                    ComputeLib.Log(Input.Output, 0, Output.Output, 0, 0, 1, Input.Length);
                    break;
                case A_Func.Exp:
                    ComputeLib.Exp(Input.Output, Output.Output, 0, 1, Input.Length);
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
        }

        public override void CleanDeriv()
        {
            if (Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            switch (AFunc)
            {
                case A_Func.Tanh:
                    ComputeLib.DerivTanh(Output.Output, 0, Output.Deriv, 0, Input.Deriv, 0, Input.Length, 1, 1);
                    break;
                case A_Func.Rectified:
                    ComputeLib.DerivReLU(Output.Output, 0, Output.Deriv, 0, Input.Deriv, 0, Input.Length, 1, 1);
                    break;
                case A_Func.Sigmoid:
                    ComputeLib.DerivLogistic(Output.Output, 0, Output.Deriv, 0, Input.Deriv, 0, Input.Length, 1, 1);
                    break;
                case A_Func.Gelu:
                    ComputeLib.DerivGelu(Input.Output, Output.Output, Output.Deriv, Input.Deriv, Input.Length);
                    break;
                case A_Func.Exp:
                    // c = alpah c + beta ( a \dot b );
                    ComputeLib.CuDNNVectorMul(Output.Output, Output.Deriv, Input.Deriv, 1, 1, Input.Length);
                    break;
                case A_Func.Log:
                    ComputeLib.Reciprocal(Input.Output, Behavior.Resource.MemF["tmp1"], 0, 1, Input.Length);
                    ComputeLib.CuDNNVectorMul(Behavior.Resource.MemF["tmp1"], Output.Deriv, Input.Deriv, 1, 1, Input.Length);
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            //float _in_l2 = ComputeLib.DotProduct(Input.Deriv, Input.Deriv, Input.Length); 
            //    Logger.WriteLog("check input deriv norm af {0}, length {1}", Math.Sqrt(_in_l2), Input.Length);
        }

    }
}
