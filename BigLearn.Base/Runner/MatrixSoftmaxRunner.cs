﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class MatrixSoftmaxProcessor : StructRunner<Structure, BatchData>
    // {
    //     public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

    //     public new MatrixData Input { get { return (MatrixData)base.Input; } set { base.Input = value; } }

    //     //CudaPieceFloat tmpDeriv;
    //     float Gamma;

    //     /// <summary>
    //     /// Gamma must be 1.
    //     /// </summary>
    //     /// <param name="input"></param>
    //     /// <param name="gamma"></param>
    //     /// <param name="behavior"></param>
    //     public MatrixSoftmaxProcessor(MatrixData input, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //     {
    //         Input = input;
    //         Gamma = gamma;
    //         Output = input;
    //     }

    //     public override void Forward()
    //     {
    //         Output.Row = Input.Row;
    //         if (Input.Row == 0) return;
    //         ComputeLib.SoftMax(Input.Output, Output.Output, Input.Column, Input.Row, 1);
    //     }

    //     public override void CleanDeriv()
    //     {
    //         if (Input.Row == 0) return;
    //     }

    //     /// <summary>
    //     /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
    //     /// </summary>
    //     /// <param name="cleanDeriv"></param>
    //     public override void Backward(bool cleanDeriv)
    //     {
    //         if (Input.Row == 0) return;
    //         ComputeLib.DerivSoftmax(Output.Output, Output.Deriv, Input.Deriv, Input.Column, Input.Row, 0);
    //     }
    //     public override void Update() { }
    // }

    public class MatrixSoftmaxRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        public new MatrixData Input { get { return (MatrixData)base.Input; } set { base.Input = value; } }

        bool IsInplace { get; set; }
        public MatrixSoftmaxRunner(MatrixData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Output = new MatrixData(Input.Column, Input.MaxRow, behavior.Device);
        }

        public MatrixSoftmaxRunner(MatrixData input, bool isInPlace, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            IsInplace = isInPlace;
            Input = input;

            if(IsInplace) Output = input;
            else Output = new MatrixData(Input.Column, Input.MaxRow, behavior.Device);
        }

        public override void Forward()
        {
            Output.Row = Input.Row;
            if (Input.Row == 0) return;
            ComputeLib.SoftMax(Input.Output, Output.Output, Input.Column, Input.Row, 1);
        }

        public override void CleanDeriv()
        {
            if (Input.Row == 0) return;
            if(!IsInplace) ComputeLib.Zero(Output.Deriv, Output.Row * Output.Column);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Input.Row == 0) return;
            ComputeLib.DerivSoftmax(Output.Output, Output.Deriv, Input.Deriv, Input.Column, Input.Row, IsInplace ? 0 : 1);
        }

    }

}
