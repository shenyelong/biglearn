using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class IntIncRunner : StructRunner
    {
        public CudaPieceInt _Input { get; set; }
        public CudaPieceInt _Output { get; set; } 

        int Num { get; set; }
        public IntIncRunner(CudaPieceInt input, int num, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            _Input = input;
            _Output = new CudaPieceInt(input.Size, behavior.Device);            
            Num = num;
        }

        public override void Forward()
        {
            _Output.EffectiveSize = _Input.EffectiveSize;
            
            _Input.SyncToCPU();
            for(int i = 0; i < _Input.EffectiveSize; i++)
            {
                _Output[i] = _Input[i] + Num;
            }
            _Output.SyncFromCPU();
        }
    }
}
