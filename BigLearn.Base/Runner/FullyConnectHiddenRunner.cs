﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Fully Connected Layer Runner.  (HiddenBatchData + LinkStructure)
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class FullyConnectHiddenRunner<IN> : StructRunner<LayerStructure, IN> where IN : HiddenBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat BiasOne;

        DropOutProcessor<HiddenBatchData> DropOutRegularized = null;

        public FullyConnectHiddenRunner(LayerStructure model, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
        {
            if(input.Dim != model.Neural_In)
            {
                throw new Exception(string.Format("FullyConnectHiddenRunner Input Feature doesn't match {0} , {1}", input.Dim, model.Neural_In)); 
            }

            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
            if (Model.IsBias)
            {
                BiasOne = new CudaPieceFloat(Input.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                BiasOne.Init(1);
            }
            if (Model.DropOut > 0) DropOutRegularized = new DropOutProcessor<HiddenBatchData>(Output, Model.DropOut, Behavior);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;

            if(Output.BatchSize == 0) { return; }

            if(Input.Dim != Model.Neural_In)
            {
                throw new Exception(string.Format("FullyConnectHiddenRunner Input Feature doesn't match {0} , {1}", Input.Dim, Model.Neural_In)); 
            }

            ComputeLib.Sgemm(Input.Output.Data, 0, Model.weight, 0, Output.Output.Data, 0, Input.BatchSize, Model.Neural_In, Model.Neural_Out, 0, 1, false, false);

            if (Behavior.RunMode == DNNRunMode.Train && Model.DropOut > 0) DropOutRegularized.Forward();
            
            //Output.Output.Data.Print("FC Output:", 256, true);
            ActivationFuncLib.ActivationFunc(ComputeLib, Output.Output.Data, Input.BatchSize, Model.Neural_Out, Model.Af, Model.bias);
            //Output.Output.Data.Print("FC Activate + bias", 256, true);
        }

        /// <summary>
        /// cleanDeriv: will be always false.
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) { return; }
             // Backpropagate Deriv.
            ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.Deriv.Data, Output.Output.Data, Input.BatchSize, Model.Neural_Out, Model.Af);
            if (Model.DropOut > 0) DropOutRegularized.Backward(false);
            if (Input.Deriv != null) ComputeLib.Sgemm(Output.Deriv.Data, 0, Model.weight, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.Neural_Out, Model.Neural_In, 1, 1, false, true);

            if (Model.weightGrad != null)
            {
                ComputeLib.Sgemm(Input.Output.Data, 0, Output.Deriv.Data, 0, Model.weightGrad, 0, Input.BatchSize, Model.Neural_In, Model.Neural_Out, 1, 1, true, false);
            }

            if (Model.IsBias && Model.biasGrad != null)
            {
                ComputeLib.Sgemv(Output.Deriv.Data, BiasOne, Input.BatchSize, Model.Neural_Out, Model.biasGrad, true, 1, 1);
            }
        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) { return; }

            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }
    }
}
