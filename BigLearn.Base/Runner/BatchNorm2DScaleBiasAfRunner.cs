using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // try to avoid in memory operation. it is very easy to cause the bug. I would rather waste the gpu memory. 
    public class BatchNorm2DScaleBiasAfRunner : StructRunner
    {
        public new Tensor4DData Output { get { return (Tensor4DData)base.Output; } set { base.Output = value; } }
        new Tensor4DData Input { get { return (Tensor4DData)base.Input; } set { base.Input = value; } }
        Tensor4DData Scale { get; set; }
        Tensor4DData Bias { get; set; }


        CudaPieceFloat mean;
        //IntPtr meanState;

        CudaPieceFloat var_1;
        CudaPieceFloat var;
        IntPtr varState;

        IntPtr sumState;

        //CudaPieceFloat w;
        CudaPieceFloat tmpO;
        //CudaPieceFloat tmpODeriv;

        IntArgument[] reduce_dims { get; set; }

        IntArgument[] input_dims { get; set; }
        
        float Discount = 1;
        float Discount1 = 1;
        CudaPieceFloat running_mean;

        CudaPieceFloat running_var;

        float Momentum = 0.1f;

        int ReduceSize = 0;

        public BatchNorm2DScaleBiasAfRunner(Tensor4DData input, Tensor4DData scale, Tensor4DData bias, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Scale = scale;
            Bias = bias;

            input_dims = Input.Shape;

            reduce_dims = new IntArgument[4];
            reduce_dims[0] = new IntArgument("reduce_width", 1);
            reduce_dims[1] = new IntArgument("reduce_height", 1);
            reduce_dims[2] = Input.Shape[2];
            reduce_dims[3] = new IntArgument("reduce_batch", 1);

            Logger.WriteLog("BNAF reduce size {0}, input size {1}",  new Shape(reduce_dims).DefaultSize, new Shape(input_dims).DefaultSize);
            ReduceSize = Input.Shape[0].Default * Input.Shape[1].Default * Input.Shape[3].Default;
            Discount = 1.0f / (float)Math.Sqrt(ReduceSize);
            Discount1 = 1.0f / ReduceSize;

            mean = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            //meanState = ComputeLib.RegisteTensorReduce(0, input_dims.Length, new Shape(input_dims).RDimList, new Shape(reduce_dims).RDimList);
            sumState = ComputeLib.RegisteTensorLazyReduce(2, input_dims.Length, new Shape(input_dims).RDimList, new Shape(reduce_dims).RDimList);
            Behavior.Resource.RegisteReduceState(sumState);

            var = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            var_1 = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            varState = ComputeLib.RegisteTensorLazyReduce(1, input_dims.Length, new Shape(input_dims).RDimList, new Shape(reduce_dims).RDimList);
            Behavior.Resource.RegisteReduceState(varState);

            
            tmpO = new CudaPieceFloat(new Shape(input_dims).DefaultSize, behavior.Device);

            Behavior.Resource.RegisteFloatVector(new Shape(input_dims).DefaultSize, "tmp0");
            Behavior.Resource.RegisteFloatVector(new Shape(input_dims).DefaultSize, "tmp1");
            Behavior.Resource.RegisteFloatVector(new Shape(reduce_dims).DefaultSize, "tmp2");

            //tmpODeriv = new CudaPieceFloat(new Shape(input_dims).DefaultSize, behavior.Device);            
            //w = new CudaPieceFloat(new Shape(input_dims).DefaultSize, behavior.Device);
            
            running_mean = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device); 
            running_mean.Init(0);

            running_var = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device); 
            running_var.Init(1);

            Output = new Tensor4DData(behavior.Device, Input.Shape); 
        }

        public override void Forward()
        {
            if(Behavior.RunMode == DNNRunMode.Train)
            {
                // mean
                ComputeLib.TensorReduceOp(Input.Output, mean, 0, Discount1, sumState);

                // a = a * awei + b * bwei;
                // CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei);
                ComputeLib.Add_Vector(running_mean, mean, new Shape(reduce_dims).DefaultSize, 0.9f, 0.1f);

                // w = x - mean.
                ComputeLib.TensorOp(0, reduce_dims.Length, new Shape(input_dims).RDimList, Input.Output, 
                                                     new Shape(reduce_dims).RDimList, mean,
                                                     new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"],
                                                     1, -1, 0);
                // w -> var
                ComputeLib.TensorReduceOp(Behavior.Resource.MemF["tmp0"], var, 0, Discount, varState);
                

                
                ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(reduce_dims).RDimList, var, 
                                                      new Shape(reduce_dims).RDimList, var,
                                                      new Shape(reduce_dims).RDimList, Behavior.Resource.MemF["tmp2"],
                                                      1, 1, 0);

                // a = a * awei + b * bwei;
                // CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei);
                ComputeLib.Add_Vector(running_var, Behavior.Resource.MemF["tmp2"], new Shape(reduce_dims).DefaultSize, 0.9f, 0.1f);

                // 1.0 / var
                ComputeLib.Reciprocal(var, var_1, 0, 1, new Shape(reduce_dims).DefaultSize);
                
                // (x - mean) / var
                ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"], 
                                                           new Shape(reduce_dims).RDimList, var_1,
                                                           new Shape(input_dims).RDimList, tmpO,
                                                           1, 1, 0);
                
                
            }
            else
            {
                // x - mean
                ComputeLib.TensorOp(0, reduce_dims.Length, new Shape(input_dims).RDimList, Input.Output, 
                                                     new Shape(reduce_dims).RDimList, running_mean,
                                                     new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"],
                                                     1, -1, 0);
                // 1.0 / var
                ComputeLib.Reciprocal(running_var, var_1, 0, 1, new Shape(reduce_dims).DefaultSize);      
                
                // (x - mean) / var
                ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"], 
                                                           new Shape(reduce_dims).RDimList, var_1,
                                                           new Shape(input_dims).RDimList, tmpO,
                                                           1, 1, 0);
            }

            ComputeLib.TensorOp(1, input_dims.Length, new Shape(input_dims).RDimList, tmpO, 
                                                      new Shape(reduce_dims).RDimList, Scale.Output,
                                                      new Shape(input_dims).RDimList, Output.Output,
                                                      1, 1, 0);

            ComputeLib.TensorOp(0, input_dims.Length, new Shape(input_dims).RDimList, Output.Output, 
                                                 new Shape(reduce_dims).RDimList, Bias.Output,
                                                 new Shape(input_dims).RDimList, Output.Output,
                                                 1, 1, 0);        

            ComputeLib.ReLU(Output.Output, 0, Output.Output, 0, Output.Length);    

        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            //ComputeLib.DerivReLU(Output.Output, 0, Output.Deriv, 0, Input.Deriv, 0, Input.Length, 1, 1);

            ComputeLib.DerivReLU(Output.Output, 0, Output.Deriv, 0, Output.Deriv, 0, Output.Length, 0, 1);

            if(Scale.Deriv != null && !Scale.Deriv.IsEmpty)
            {
                ComputeLib.TensorOp(1, input_dims.Length, new Shape(input_dims).RDimList, Output.Deriv, 
                                                 new Shape(input_dims).RDimList, tmpO,
                                                 new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"],
                                                 1, 1, 0);

                ComputeLib.TensorReduceOp(Behavior.Resource.MemF["tmp0"], Scale.Deriv, 1, 1, sumState);
            }

            if(Bias.Deriv != null && !Bias.Deriv.IsEmpty)
            {
                ComputeLib.TensorReduceOp(Output.Deriv, Bias.Deriv, 1, 1, sumState);
            }

            ComputeLib.TensorOp(1, input_dims.Length, new Shape(input_dims).RDimList, Output.Deriv, 
                                                 new Shape(reduce_dims).RDimList, Scale.Output,
                                                 new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp1"],
                                                 1, 1, 0);

            ComputeLib.TensorOp(1, input_dims.Length, new Shape(input_dims).RDimList, tmpO, 
                                                      new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp1"],
                                                      new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"],
                                                      1, 1, 0);

            ComputeLib.TensorReduceOp(Behavior.Resource.MemF["tmp0"], mean, 0, Discount1, sumState);

            ComputeLib.TensorOp(1, input_dims.Length, new Shape(input_dims).RDimList, tmpO, 
                                                      new Shape(reduce_dims).RDimList, mean,
                                                      new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"],
                                                      1, 1, 0);

            ComputeLib.Add_Vector(Behavior.Resource.MemF["tmp0"], Behavior.Resource.MemF["tmp1"], new Shape(input_dims).DefaultSize, -1, 1); 

            ComputeLib.TensorReduceOp(Behavior.Resource.MemF["tmp0"], mean, 0, Discount1, sumState);
            
            ComputeLib.TensorOp(0, reduce_dims.Length, new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"], 
                                                  new Shape(reduce_dims).RDimList, mean,
                                                  new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"],
                                                  1, -1, 0);

            ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(input_dims).RDimList, Behavior.Resource.MemF["tmp0"], 
                                                 new Shape(reduce_dims).RDimList, var_1,
                                                 new Shape(input_dims).RDimList, Input.Deriv,
                                                 1, 1, 1);
        }
    }
}
