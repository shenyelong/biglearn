﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqBiMatchRunner : StructRunner
    {
        public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }
        SeqDenseBatchData QueryMem { get; set; }

        public SeqBiMatchRunner(SeqDenseBatchData queryMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            QueryMem = queryMem;

            Output = new BiMatchBatchData(
                //new BiMatchBatchDataStat()
            //{
                QueryMem.MAX_SENTSIZE, //MAX_MATCH_BATCHSIZE = 
                QueryMem.MAX_BATCHSIZE, //MAX_SRC_BATCHSIZE = 
                QueryMem.MAX_SENTSIZE, //MAX_TGT_BATCHSIZE = 
             Behavior.Device);
        }

        public override void Forward()
        {
            QueryMem.SampleIdx.SyncToCPU(QueryMem.BatchSize);
            List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
            for (int i = 0; i < QueryMem.BatchSize; i++)
            {
                int memBgn = i == 0 ? 0 : QueryMem.SampleIdx[i - 1];
                int memEnd = QueryMem.SampleIdx[i];

                for (int f = memBgn; f < memEnd; f++)
                {
                    matchList.Add(new Tuple<int, int, float>(i, f, 1));
                }
            }
            Output.Clear();
            Output.PushMatch(matchList);
        }
    }

    public class CrossSeqBiMatchRunner : StructRunner
    {
        public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }
        SeqDenseBatchData SrcMem { get; set; }
        SeqDenseBatchData TgtMem { get; set; }
        
        public CrossSeqBiMatchRunner(SeqDenseBatchData srcMem, SeqDenseBatchData tgtMem, int maxSrcTgtCrossLen, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            SrcMem = srcMem;
            TgtMem = tgtMem;

            Output = new BiMatchBatchData( //new BiMatchBatchDataStat()
            //{
                maxSrcTgtCrossLen, //MAX_MATCH_BATCHSIZE = 
                SrcMem.MAX_SENTSIZE, //MAX_SRC_BATCHSIZE = 
                TgtMem.MAX_SENTSIZE, //MAX_TGT_BATCHSIZE = 
            //}, 
                Behavior.Device);
        }

        public override void Forward()
        {
            SrcMem.SampleIdx.SyncToCPU(SrcMem.BatchSize);
            TgtMem.SampleIdx.SyncToCPU(TgtMem.BatchSize);
            List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
            for (int i = 0; i < SrcMem.BatchSize; i++)
            {
                int qmemBgn = i == 0 ? 0 : SrcMem.SampleIdx[i - 1];
                int qmemEnd = SrcMem.SampleIdx[i];

                int dmemBgn = i == 0 ? 0 : TgtMem.SampleIdx[i - 1];
                int dmemEnd = TgtMem.SampleIdx[i];

                for (int q = qmemBgn; q < qmemEnd; q++)
                {
                    for(int d = dmemBgn; d < dmemEnd; d++)
                    {
                        matchList.Add(new Tuple<int, int, float>(q, d, 1));
                    }
                }
            }
            Output.SetMatch(matchList);
        }
    }
    public class RandomBiMatchRunner : StructRunner
    {
        public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }
        Random rnd = null;
        int MaxBatchSize { get; set; }
        int NTrial { get; set; }
        bool IsConstRandom { get; set; }
        IntArgument BatchSizeArg { get; set; }
        public RandomBiMatchRunner(int maxBatchSize, int nTrial, IntArgument batchSizeArg, RunnerBehavior behavior, bool constRandom = true) : base(Structure.Empty, behavior)
        {
            IsConstRandom = constRandom;
            NTrial = nTrial;
            MaxBatchSize = maxBatchSize;

            Output = new BiMatchBatchData(
                //new BiMatchBatchDataStat()
            //{
                (NTrial + 1) * maxBatchSize, // MAX_MATCH_BATCHSIZE = 
                maxBatchSize, //MAX_SRC_BATCHSIZE = 
                maxBatchSize, //MAX_TGT_BATCHSIZE =   
            //}, 
                Behavior.Device);

            Output.Dim = NTrial + 1;
            BatchSizeArg = batchSizeArg;
            if (constRandom)
            {
                rnd = new Random(13);
            }
            else
            {
                rnd = new Random(ParameterSetting.RANDOM_SEED);
            }
        }

        public override void Forward()
        {
            Output.SrcSize = BatchSizeArg.Value;
            Output.TgtSize = BatchSizeArg.Value;
            Output.MatchSize = BatchSizeArg.Value * (NTrial + 1);

            #region Generate Sample.
            Util.MatchSampling(BatchSizeArg.Value, NTrial, Output.SrcIdx.MemPtr, Output.TgtIdx.MemPtr, Output.MatchInfo.TakeCPU(0).ToArray(), rnd);
            Util.InverseMatchIdx(Output.SrcIdx.MemPtr, Output.MatchSize, Output.Src2Idx.MemPtr, Output.Src2MatchIdx.MemPtr, Output.Src2MatchElement.MemPtr);
            Util.InverseMatchIdx(Output.TgtIdx.MemPtr, Output.MatchSize, Output.Tgt2Idx.MemPtr, Output.Tgt2MatchIdx.MemPtr, Output.Tgt2MatchElement.MemPtr);
            #endregion
            Output.SyncFromCPU();
        }
    }

    public class RepeatBiMatchRunner : StructRunner
    {
        public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }

        int MaxBatchSize { get; set; }
        int TgtNum { get; set; }
        IntArgument BatchSizeArg { get; set; }
        public RepeatBiMatchRunner(int maxBatchSize, int tgtNum, IntArgument batchSizeArg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            TgtNum = tgtNum;
            MaxBatchSize = maxBatchSize;

            Output = new BiMatchBatchData(
                //new BiMatchBatchDataStat()
            //{
                TgtNum * maxBatchSize, //MAX_MATCH_BATCHSIZE = 
                maxBatchSize, //MAX_SRC_BATCHSIZE = 
                TgtNum, //MAX_TGT_BATCHSIZE =   
            //}, 
            Behavior.Device);
            Output.Dim = TgtNum;

            BatchSizeArg = batchSizeArg;
            
            List<Tuple<int, int, float>> matchData = new List<Tuple<int, int, float>>();
            for (int i = 0; i < MaxBatchSize; i++)
            {
                for (int t = 0; t < TgtNum; t++)
                {
                    matchData.Add(new Tuple<int, int, float>(i, t, 1));
                }
            }
            Output.SetMatch(matchData);
        }

        public override void Forward()
        {
            if(Output.MatchSize == BatchSizeArg.Value * TgtNum) return;

            Output.MatchSize = BatchSizeArg.Value * TgtNum;
            Output.SrcSize = BatchSizeArg.Value;
            Output.TgtSize = TgtNum;

            //public static int InverseMatchIdx(int[] matchIdx, int matchNum, int maxIdx, int[] inverseIdx, int[] inverseSmpIdx, int[] inverseElementIdx)
            Output.TgtSize = BiMatchBatchData.InverseMatchIdx(Output.TgtIdx.MemPtr, Output.MatchSize, Output.TgtSize, Output.Tgt2Idx.MemPtr, Output.Tgt2MatchIdx.MemPtr, Output.Tgt2MatchElement.MemPtr);
            Output.Tgt2Idx.SyncFromCPU(Output.TgtSize);
            Output.Tgt2MatchIdx.SyncFromCPU(Output.TgtSize);
            Output.Tgt2MatchElement.SyncFromCPU(Output.MatchSize);
        }
    }
}
