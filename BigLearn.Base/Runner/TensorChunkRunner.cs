using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorChunkRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        IntArgument[] InShape { get { return Input.Shape; } }
        IntArgument[] OutShape { get { return Output.Shape; } }

        int Dim { get; set; }
        CudaPieceInt Pos { get; set; }
        int Len { get; set; }

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public TensorChunkRunner(NdArrayData input, int dim, CudaPieceInt pos, int len, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
            Input = input;

            IntArgument[] newShape = new IntArgument[Input.Shape.Length];
            for(int i=0; i < newShape.Count; i++)
            {
                if(i == dim) newShape[i] = new IntArgument("slice", len);
                else newShape[i] = Input.Shape[i];
            }
            Output = new NdArrayData(behavior.Device, newShape); 
            Dim = dim;
            Pos = pos;
            Len = len;
        }

        public override void Forward()
        {
            //
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
        }

    }
}
