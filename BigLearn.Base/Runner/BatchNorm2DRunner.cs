﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // try to avoid in memory operation. it is very easy to cause the bug. I would rather waste the gpu memory. 
    public class BatchNorm2DRunner : StructRunner
    {
        public new Tensor4DData Output { get { return (Tensor4DData)base.Output; } set { base.Output = value; } }
        new Tensor4DData Input { get { return (Tensor4DData)base.Input; } set { base.Input = value; } }

        CudaPieceFloat mean;
        IntPtr meanState;

        CudaPieceFloat var_1;
        CudaPieceFloat var;
        IntPtr varState;

        CudaPieceFloat w;
        //CudaPieceFloat dw;

        IntArgument[] reduce_dims { get; set; }

        IntArgument[] input_dims { get; set; }
        
        float Discount = 1;

        CudaPieceFloat running_mean;

        CudaPieceFloat running_var;

        float Momentum = 0.1f;

        int ReduceSize = 0;

        public BatchNorm2DRunner(Tensor4DData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Output = new Tensor4DData(behavior.Device, Input.Shape); 

            input_dims = Input.Shape;

            reduce_dims = new IntArgument[4];
            reduce_dims[0] = new IntArgument("reduce_width", 1);
            reduce_dims[1] = new IntArgument("reduce_height", 1);
            reduce_dims[2] = Input.Shape[2];
            reduce_dims[3] = new IntArgument("reduce_batch", 1);

            ReduceSize = Input.Shape[0].Default * Input.Shape[1].Default * Input.Shape[3].Default;
            Discount = 1.0f / (float)Math.Sqrt(ReduceSize);

            mean = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            meanState = ComputeLib.RegisteTensorReduce(0, input_dims.Length, new Shape(input_dims).RDimList, new Shape(reduce_dims).RDimList);

            w = new CudaPieceFloat(new Shape(input_dims).DefaultSize, behavior.Device);
            //dw = new CudaPieceFloat(new Shape(input_dims).DefaultSize, behavior.Device);

            var = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            varState = ComputeLib.RegisteTensorReduce(1, input_dims.Length, new Shape(input_dims).RDimList, new Shape(reduce_dims).RDimList);
        
            var_1 = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);

            running_mean = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device); 
            running_mean.Init(0);

            running_var = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device); 
            running_var.Init(1);
        }

        public override void Forward()
        {
            if(Behavior.RunMode == DNNRunMode.Train)
            {
                // mean
                ComputeLib.TensorReduceOp(Input.Output, mean, 0, 1, meanState);

                // a = a * awei + b * bwei;
                // CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei);
                ComputeLib.Add_Vector(running_mean, mean, new Shape(reduce_dims).DefaultSize, 0.9f, 0.1f);

                // x - mean.
                ComputeLib.TensorOp(0, reduce_dims.Length, new Shape(input_dims).RDimList, Input.Output, 
                                                     new Shape(reduce_dims).RDimList, mean,
                                                     new Shape(input_dims).RDimList, w,
                                                     1, -1, 0);
                // var
                ComputeLib.TensorReduceOp(w, var, 0, Discount, varState);
                
                // a = a * awei + b * bwei;
                // CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei);
                ComputeLib.Add_Vector(running_var, var, new Shape(reduce_dims).DefaultSize, 0.9f, 0.1f);

                // 1.0 / var
                ComputeLib.Reciprocal(var, var_1, 0, 1, new Shape(reduce_dims).DefaultSize);
                // (x - mean) / var
                ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(input_dims).RDimList, w, 
                                                           new Shape(reduce_dims).RDimList, var_1,
                                                           new Shape(input_dims).RDimList, Output.Output,
                                                           1, 1, 0);
            }
            else
            {
                // x - mean
                ComputeLib.TensorOp(0, reduce_dims.Length, new Shape(input_dims).RDimList, Input.Output, 
                                                     new Shape(reduce_dims).RDimList, running_mean,
                                                     new Shape(input_dims).RDimList, w,
                                                     1, -1, 0);
                // 1.0 / var
                ComputeLib.Reciprocal(running_var, var_1, 0, 1, new Shape(reduce_dims).DefaultSize);      
                
                // (x - mean) / var
                ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(input_dims).RDimList, w, 
                                                           new Shape(reduce_dims).RDimList, var_1,
                                                           new Shape(input_dims).RDimList, Output.Output,
                                                           1, 1, 0);
            }
        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            ComputeLib.TensorOp(1, input_dims.Length, new Shape(input_dims).RDimList, Output.Output, 
                                                      new Shape(input_dims).RDimList, Output.Deriv,
                                                      new Shape(input_dims).RDimList, w,
                                                      1, 1, 0);

            ComputeLib.TensorReduceOp(w, mean, 0, 1, meanState);

            ComputeLib.TensorOp(1, input_dims.Length, new Shape(input_dims).RDimList, Output.Output, 
                                                      new Shape(reduce_dims).RDimList, mean,
                                                      new Shape(input_dims).RDimList, w,
                                                      1, 1, 0);

            ComputeLib.Add_Vector(w, Output.Deriv, new Shape(input_dims).DefaultSize, -1, 1); 

            ComputeLib.TensorReduceOp(w, mean, 0, 1, meanState);
            
            ComputeLib.TensorOp(0, reduce_dims.Length, new Shape(input_dims).RDimList, w, 
                                                  new Shape(reduce_dims).RDimList, mean,
                                                  new Shape(input_dims).RDimList, w,
                                                  1, -1, 0);

            ComputeLib.TensorOp(1, reduce_dims.Length, new Shape(input_dims).RDimList, w, 
                                                 new Shape(reduce_dims).RDimList, var_1,
                                                 new Shape(input_dims).RDimList, Input.Deriv,
                                                 1, 1, 1);
        }
    }
}
