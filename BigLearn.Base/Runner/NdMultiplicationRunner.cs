using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class NdMultiplicationRunner : StructRunner<Structure, BatchData>
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }

        NdArrayData InputA { get; set; }
        int OpA { get; set; }

        NdArrayData InputB { get; set; }
        int OpB { get; set; }

        int Batch { get; set; }
        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public NdMultiplicationRunner(NdArrayData inputA, int opA, NdArrayData inputB, int opB, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            OpA = opA;
            
            InputB = inputB;
            OpB = opB;

            if(InputA.Dimensions.Length != InputB.Dimensions.Length)
            {
                throw new Exception(string.Format("NdArrayData A Length {0} should be equals to NdArrayData B Length {1}", InputA.Dimensions.Length, InputB.Dimensions.Length)); 
            }
            
            IntArgument[] dims = new IntArgument[InputA.Dimensions.Length];
            
            if(opA == 0 && opB == 0)
            {
                dims[0] = InputB.Dimensions[0];
                dims[1] = InputA.Dimensions[1];
            }
            else if(opA == 0 && opB == 1)
            {
                dims[0] = InputB.Dimensions[1];
                dims[1] = InputA.Dimensions[1];   
            }
            else if(opA == 1 && opB == 0)
            {
                dims[0] = InputB.Dimensions[0];
                dims[1] = InputA.Dimensions[0];
            }
            else
            {
                throw new NotImplementedException();
            }

            for(int k = 2; k < InputA.Dimensions.Length; k++)
            {
                dims[k] = InputA.Dimensions[k];
            }
            Output = new NdArrayData(dims, Behavior.Device);
        }

        public override void Forward()
        {
            Output.Length = Output.NdDim;
            if(Output.Length == 0) return;

            if(OpA == 0 && OpB == 0)
            {
                if(InputB.Dimensions[1].Value != InputA.Dimensions[0].Value)
                {
                    throw new Exception(string.Format("NdArrayData A Dimension 0 {0} should be equals to NdArrayData B Dimension 1 {1}", InputA.Dimensions[0].Value, InputB.Dimensions[1].Value)); 
                }
            }
            else if(OpA == 0 && OpB == 1)
            {
                if(InputB.Dimensions[0].Value != InputA.Dimensions[0].Value)
                {
                    throw new Exception(string.Format("NdArrayData A Dimension 0 {0} should be equals to NdArrayData B Dimension 0 {1}", InputA.Dimensions[0].Value, InputB.Dimensions[0].Value)); 
                }
            }
            else if(OpA == 1 && OpB == 0)
            {
                if(InputB.Dimensions[1].Value != InputA.Dimensions[1].Value)
                {
                    throw new Exception(string.Format("NdArrayData A Dimension 1 {0} should be equals to NdArrayData B Dimension 1 {1}", InputA.Dimensions[1].Value, InputB.Dimensions[1].Value)); 
                }
            }

            Batch = 1;
            for(int k = 2; k < InputA.Dimensions.Length; k++)
            {
                if(InputA.Dimensions[k].Value != InputB.Dimensions[k].Value)
                {
                    throw new Exception(string.Format("NdArrayData A Dims {0} {1} should be equals to NdArrayData B Dims {0} {2}", 
                                                        k, InputA.Dimensions[k].Value,  InputB.Dimensions[k].Value)); 
                }
                Batch = Batch * InputA.Dimensions[k].Value;
            }

            ComputeLib.SgemmStrideBatched(InputA.Output, InputA.Dimensions[1].Value, InputA.Dimensions[0].Value, 
                                          InputB.Output, InputB.Dimensions[1].Value, InputB.Dimensions[0].Value,
                                          Batch, Output.Output, OpA, OpB, 0, 1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if(Output.Length == 0) { return; }

            if(OpA == 0 && OpB == 0)
            {
                if(InputA.Deriv != null && !InputA.Deriv.IsEmpty)
                {
                    ComputeLib.SgemmStrideBatched(Output.Deriv, Output.Dimensions[1].Value, Output.Dimensions[0].Value, 
                                          InputB.Output, InputB.Dimensions[1].Value, InputB.Dimensions[0].Value,
                                          Batch, InputA.Deriv, 0, 1, 1, 1);
                }

                if(InputB.Deriv != null && !InputB.Deriv.IsEmpty)
                {
                    ComputeLib.SgemmStrideBatched(InputA.Output, InputA.Dimensions[1].Value, InputA.Dimensions[0].Value, 
                                          Output.Deriv, Output.Dimensions[1].Value, Output.Dimensions[0].Value,
                                          Batch, InputB.Deriv, 1, 0, 1, 1);
                }
            }
            else if(OpA == 0 && OpB == 1)
            {
                if(InputA.Deriv != null && !InputA.Deriv.IsEmpty)
                {
                    ComputeLib.SgemmStrideBatched(Output.Deriv, Output.Dimensions[1].Value, Output.Dimensions[0].Value, 
                                          InputB.Output, InputB.Dimensions[1].Value, InputB.Dimensions[0].Value,
                                          Batch, InputA.Deriv, 0, 0, 1, 1);
                }

                if(InputB.Deriv != null && !InputB.Deriv.IsEmpty)
                {
                    ComputeLib.SgemmStrideBatched(Output.Deriv, Output.Dimensions[1].Value, Output.Dimensions[0].Value, 
                                          InputA.Output, InputA.Dimensions[1].Value, InputA.Dimensions[0].Value,
                                          Batch, InputB.Deriv, 1, 0, 1, 1);
                }
            }
            else if(OpA == 1 && OpB == 0)
            {
                if(InputA.Deriv != null && !InputA.Deriv.IsEmpty)
                {
                    ComputeLib.SgemmStrideBatched(InputB.Output, InputB.Dimensions[1].Value, InputB.Dimensions[0].Value, 
                                          Output.Deriv, Output.Dimensions[1].Value, Output.Dimensions[0].Value,
                                          Batch, InputA.Deriv, 0, 1, 1, 1);
                }

                if(InputB.Deriv != null && !InputB.Deriv.IsEmpty)
                {
                    ComputeLib.SgemmStrideBatched(InputA.Output, InputA.Dimensions[1].Value, InputA.Dimensions[0].Value, 
                                          Output.Deriv, Output.Dimensions[1].Value, Output.Dimensions[0].Value,
                                          Batch, InputB.Deriv, 0, 0, 1, 1);
                }
            }
        }
    }

}
