using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public static class RunHelper  
    {
        public static NdArrayData Dot(this NdArrayData data, VectorData mask, List<StructRunner> session, RunnerBehavior behavior)
        {
            VectorDotRunner vecDotRunner = new VectorDotRunner(data.Context, mask, behavior);
            session.Add(vecDotRunner);
            NdArrayData output = new NdArrayData(data.Dimensions, vecDotRunner.Output.Output, vecDotRunner.Output.Deriv, behavior.Device);
            return output;
        }

        public static NdArrayData Dot(this NdArrayData data, NdArrayData mask, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorDotRunner dotRunner = new TensorDotRunner(data, mask, behavior);
            session.Add(dotRunner);
            return dotRunner.Output;
        }

        public static MatrixData Dot(this MatrixData data, MatrixData mask, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorDotRunner dotRunner = new TensorDotRunner(data.ToND(), mask.ToND(), behavior);
            session.Add(dotRunner);
            return dotRunner.Output.ToMD();
        }

        public static NdArrayData Add(this NdArrayData data, VectorData mask, List<StructRunner> session, RunnerBehavior behavior)
        {
            VectorAddRunner vecAddRunner = new VectorAddRunner(data.Context, mask, behavior);
            session.Add(vecAddRunner);
            NdArrayData output = new NdArrayData(data.Dimensions, vecAddRunner.Output.Output, vecAddRunner.Output.Deriv, behavior.Device);
            return output;
        }

        public static NdArrayData Add(this NdArrayData data, NdArrayData mask, List<StructRunner> session, RunnerBehavior behavior)
        {
            VectorAddRunner vecAddRunner = new VectorAddRunner(data.Context, mask.Context, behavior);
            session.Add(vecAddRunner);
            NdArrayData output = new NdArrayData(data.Dimensions, vecAddRunner.Output.Output, vecAddRunner.Output.Deriv, behavior.Device);
            return output;
        }

        public static Tensor4DData Add(this Tensor4DData data, Tensor4DData mask, List<StructRunner> session, RunnerBehavior behavior)
        {
            return data.ToND().Add(mask.ToND(), session, behavior).ToT4D();
        }


        public static MatrixData Add(this MatrixData data, MatrixData mask, List<StructRunner> session, RunnerBehavior behavior)
        {
            VectorAddRunner vecAddRunner = new VectorAddRunner(data.Context, mask.Context, behavior);
            session.Add(vecAddRunner);
            
            return new MatrixData(data.Column, data.MaxRow, vecAddRunner.Output.Output, vecAddRunner.Output.Deriv, behavior.Device);
        }

        public static MatrixData Add(this MatrixData data, VectorData bias, List<StructRunner> session, RunnerBehavior behavior)
        {
            //DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions)
            NdArrayData biasData = new NdArrayData(bias.DeviceType, bias.Output, bias.Deriv, bias.VectorDim, new IntArgument("batchsize", 1));
            TensorAddRunner tensorAddRunner = new TensorAddRunner(data.ToND(), biasData, behavior);
            session.Add(tensorAddRunner);
            return tensorAddRunner.Output.ToMD();
        }

        public static NdArrayData AddExpand(this NdArrayData data, NdArrayData bias, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorAddRunner tensorAddRunner = new TensorAddRunner(data, bias, behavior);
            session.Add(tensorAddRunner);
            return tensorAddRunner.Output;
        }

        public static NdArrayData Scale(this NdArrayData data, float scale, List<StructRunner> session, RunnerBehavior behavior)
        {
            ScaleRunner scaleRunner = new ScaleRunner(data.Context, scale, behavior);
            session.Add(scaleRunner);
            NdArrayData output = new NdArrayData(data.Dimensions, scaleRunner.Output.Output, scaleRunner.Output.Deriv, behavior.Device);
            return output;
        }

        public static NdArrayData DotAndAdd(this NdArrayData data, VectorData maskA, VectorData maskB, List<StructRunner> session, RunnerBehavior behavior)
        {
            VectorDotAndAddRunner runner = new VectorDotAndAddRunner(data.Context, maskA, maskB, 1, 1, behavior);
            session.Add(runner);
            NdArrayData output = new NdArrayData(data.Dimensions, runner.Output.Output, runner.Output.Deriv, behavior.Device);
            return output;
        }

        public static NdArrayData DotAndAdd(this NdArrayData data, NdArrayData scale, NdArrayData bias, List<StructRunner> session, RunnerBehavior behavior)
        {
            //NdArrayData inputA, NdArrayData inputB, RunnerBehavior behavior)
            TensorDotAndAddRunner runner = new TensorDotAndAddRunner(data, scale, bias, behavior);
            session.Add(runner);
            return runner.Output;

            // TensorDotRunner dotRunner = new TensorDotRunner(data, scale, behavior);
            // session.Add(dotRunner);
            // NdArrayData dotData = dotRunner.Output;

            // TensorAddRunner biasRunner = new TensorAddRunner(dotData, bias, behavior);
            // session.Add(biasRunner);
            // NdArrayData o = biasRunner.Output;
            // return o;
        }

        public static Tensor4DData BatchNorm2D(this Tensor4DData data, Tensor4DData scale, Tensor4DData bias, List<StructRunner> session, RunnerBehavior behavior)
        {
            BatchNorm2DScaleBiasRunner runner = new BatchNorm2DScaleBiasRunner(data, scale, bias, behavior);
            session.Add(runner);
            return runner.Output;
        }

        public static Tensor4DData BatchNorm2DAF(this Tensor4DData data, Tensor4DData scale, Tensor4DData bias, List<StructRunner> session, RunnerBehavior behavior)
        {
            BatchNorm2DScaleBiasAfRunner runner = new BatchNorm2DScaleBiasAfRunner(data, scale, bias, behavior);
            session.Add(runner);
            return runner.Output;
        }

        public static Tensor4DData BatchNorm2DAFV2(this Tensor4DData data, Tensor4DData scale, Tensor4DData bias, List<StructRunner> session, RunnerBehavior behavior)
        {
            BatchNorm2DScaleBiasAfV2Runner runner = new BatchNorm2DScaleBiasAfV2Runner(data, scale, bias, behavior);
            session.Add(runner);
            return runner.Output;
        }
        //BatchNorm2DScaleBiasAfRunner


        public static NdArrayData Dropout(this NdArrayData data, float drop, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorDropoutRunner dropRunner = new TensorDropoutRunner(data.ToT4D(), drop, behavior);
            session.Add(dropRunner);
            return dropRunner.Output.ToND().Reshape(data.Shape);
        }

        public static Tensor4DData Dropout(this Tensor4DData data, float drop, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorDropoutRunner dropRunner = new TensorDropoutRunner(data, drop, behavior);
            session.Add(dropRunner);
            return dropRunner.Output;
        }


        public static HiddenBatchData Dropout(this HiddenBatchData data, float drop, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorDropoutRunner dropRunner = new TensorDropoutRunner(data.ToT4D(), drop, behavior);
            session.Add(dropRunner);
            return dropRunner.Output.ToHDB();
        }

        public static NdArrayData Softmax(this NdArrayData data, List<StructRunner> session, RunnerBehavior behavior)
        {
            NdArraySoftmaxRunner softmaxRunner = new NdArraySoftmaxRunner(data, behavior);
            session.Add(softmaxRunner);
            NdArrayData output = softmaxRunner.Output;
            return output;
        }

        public static NdArrayData MatMul(this NdArrayData dataA, int opA, NdArrayData dataB, int opB, List<StructRunner> session, RunnerBehavior behavior)
        {
            NdMultiplicationRunner matMulRunner = new NdMultiplicationRunner(dataA, opA, dataB, opB, behavior);
            session.Add(matMulRunner);
            NdArrayData output = matMulRunner.Output;
            return output;
        }

        public static MatrixData MatMul(this MatrixData dataA, int opA, MatrixData dataB, int opB, List<StructRunner> session, RunnerBehavior behavior)
        {
            MatrixMultiplicationRunner matMulRunner = new MatrixMultiplicationRunner(dataA, opA, dataB, opB, behavior);
            session.Add(matMulRunner);
            MatrixData output = matMulRunner.Output;
            return output;
        }

        public static MatrixData Act(this MatrixData data, A_Func afunc, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorActivateRunner actRunner = new TensorActivateRunner(data.ToND(), afunc, behavior);
            session.Add(actRunner);
            return actRunner.Output.ToMD();
        }

        public static HiddenBatchData Act(this HiddenBatchData data, A_Func afunc, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorActivateRunner actRunner = new TensorActivateRunner(data.ToND(), afunc, behavior);
            session.Add(actRunner);
            return actRunner.Output.ToHDB();
        }

        public static NdArrayData Act(this NdArrayData dataA, A_Func afunc, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorActivateRunner actRunner = new TensorActivateRunner(dataA, afunc, behavior);
            session.Add(actRunner);
            return actRunner.Output;
        }

        public static Tensor4DData Act(this Tensor4DData dataA, A_Func afunc, List<StructRunner> session, RunnerBehavior behavior)
        {
            //TensorActivateRunner(NdArrayData input, A_Func aFunc, RunnerBehavior behavior)
            TensorActivateRunner actRunner = new TensorActivateRunner(dataA.ToND(), afunc, behavior);
            session.Add(actRunner);
            return actRunner.Output.ToT4D();
        }

        public static HiddenBatchData FNN(this HiddenBatchData dataA, LayerStructure model, List<StructRunner> session, RunnerBehavior behavior)
        {   
            FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(model, dataA, behavior);
            session.Add(fcRunner);
            return fcRunner.Output;
        }

        public static NdArrayData FNN(this NdArrayData dataA, LayerStructure model, List<StructRunner> session, RunnerBehavior behavior)
        {
            //
            IntArgument[] flatDims = new IntArgument[dataA.Dimensions.Length - 1];
            for(int i = 1; i < dataA.Dimensions.Length; i++) flatDims[i - 1] = dataA.Dimensions[i];

            IntArgument flatDim = session.Flat(behavior, flatDims);

            HiddenBatchData input =  new HiddenBatchData(behavior.Device, dataA.Output, dataA.Deriv, dataA.Dimensions[0], flatDim);
            
            //if(dataA.Shape.Length > 2)
            //{   
            //    int batch = dataA.MaxLength / dataA.Dimensions[0].Value;
                //Logger.WriteLog("max batch {0}", batch);
            //    HiddenBatchData input = new HiddenBatchData(batch, dataA.Dimensions[0].Value, dataA.Output, dataA.Deriv, behavior.Device);
            //}
            //else
            //{
            //}

            FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(model, input, behavior);
            session.Add(fcRunner);

            IntArgument outDim = new IntArgument("outDim", fcRunner.Output.Dim);
            IntArgument[] newDims = new IntArgument[dataA.Dimensions.Length];
            newDims[0] = outDim;
            for(int i= 1; i<dataA.Dimensions.Length; i++)
            {
                newDims[i] = dataA.Dimensions[i];
            }
            return new NdArrayData(newDims, fcRunner.Output.Output.Data, fcRunner.Output.Deriv.Data, behavior.Device);
        }

        public static NdArrayData Transpose(this NdArrayData data, int[] transIdx, List<StructRunner> session, RunnerBehavior behavior)
        {
            NdArrayTransposeRunner transposeRunner = new NdArrayTransposeRunner(data, transIdx, behavior);
            session.Add(transposeRunner);
            NdArrayData nd_y = transposeRunner.Output;
            return nd_y;
        }

        public static NdArrayData Norm(this NdArrayData data, int normDim, List<StructRunner> session, RunnerBehavior behavior)
        {
            TensorNormRunner normRunner = new TensorNormRunner(data, normDim, behavior);
            session.Add(normRunner);
            return normRunner.Output;
        }

        public static NdArrayData Merge(List<NdArrayData> input, List<StructRunner> session, RunnerBehavior behavior)
        {  
            List<VectorData> vectors = new List<VectorData>();
            foreach(NdArrayData d in input) vectors.Add(d.Context);

            VectorConcateRunner concateRunner = new VectorConcateRunner(vectors, behavior);
            session.Add(concateRunner);

            NdArrayData o = new NdArrayData(behavior.Device, concateRunner.Output.Output, concateRunner.Output.Deriv, 
                                            input[0].Shape.Concate(new IntArgument("Merge_Num", input.Count)));
            return o;
        }
        
        public static NdArrayData Concate(this NdArrayData data1, NdArrayData data2, NdArrayData data3, List<StructRunner> session, RunnerBehavior behavior)
        {
            List<VectorData> vectors = new List<VectorData>();
            vectors.Add(data1.Context);
            vectors.Add(data2.Context);
            vectors.Add(data3.Context);

            VectorConcateRunner concateRunner = new VectorConcateRunner(vectors, behavior);
            session.Add(concateRunner);

            IntArgument[] newDims = new IntArgument[data1.Dimensions.Length];
            for(int i = 0; i < data1.Dimensions.Length - 1; i++) newDims[i] = data1.Dimensions[i];

            newDims[data1.Dimensions.Length - 1] = new IntArgument("concate-dim", data1.Dimensions.Last().Default + data2.Dimensions.Last().Default + data3.Dimensions.Last().Default);

            NdArrayData o = new NdArrayData(behavior.Device, concateRunner.Output.Output, concateRunner.Output.Deriv, newDims);
            return o;
        }


        public static MatrixData Concate(this MatrixData data1, MatrixData data2, List<StructRunner> session, RunnerBehavior behavior)
        {
            List<MatrixData> matrixes = new List<MatrixData>();
            matrixes.Add(data1);
            matrixes.Add(data2);

            MatrixConcateRunner matrixCateRunner = new MatrixConcateRunner(matrixes, behavior);
            session.Add(matrixCateRunner);
            
            return matrixCateRunner.Output;
        }

        public static HiddenBatchData Concate(this List<HiddenBatchData> data, List<StructRunner> session, RunnerBehavior behavior)
        {
            //MatrixConcateRunner(List<MatrixData> inputList, RunnerBehavior behavior)
            MatrixConcateRunner mcRunner = new MatrixConcateRunner(data.Select(i => i.ToMD()).ToList(), behavior);
            session.Add(mcRunner);
            return mcRunner.Output.ToHDB();
        }

        public static List<NdArrayData> Split(this NdArrayData data, IntArgument[] shapes, int count, List<StructRunner> session, RunnerBehavior behavior)
        {
            List<IntArgument[]> list_shapes = new List<IntArgument[]>();
            for(int i = 0; i < count; i++)
            {
                list_shapes.Add(shapes);
            }
            return data.Split(list_shapes, session, behavior);
        }
        public static List<NdArrayData> Split(this NdArrayData data, List<IntArgument[]> shapes, List<StructRunner> session, RunnerBehavior behavior)
        {
            List<NdArrayData> results = new List<NdArrayData>();

            int cursor = 0;
            for(int i=0; i<shapes.Count; i++)
            {
                Shape s = new Shape(shapes[i]);
                results.Add(new NdArrayData(s.Dims,
                                            data.Output.SubArray(cursor, s.DefaultSize), 
                                            data.Deriv.SubArray(cursor, s.DefaultSize),
                                            behavior.Device));
                cursor += s.DefaultSize;
            }
            // session.Add(new StructRunner(Structure.Empty, behavior) 
            //      {
            //          ForwardDelegate = () =>
            //          {
            //              int tmp_cursor = 0;
            //              for( int i = 0; i < results.Count; i++)
            //              {
            //                  Shape s = new Shape(shapes[i]);
            //                  results[i].Output.SubArray(data.Output, tmp_cursor, s.Size);
            //                  results[i].Deriv.SubArray(data.Deriv, tmp_cursor, s.Size);
            //                  tmp_cursor += s.Size;
            //              }
            //          },
            //      });
            return results;

        }


        
            
    }
}
