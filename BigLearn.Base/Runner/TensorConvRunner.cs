using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorConvRunner : StructRunner<TensorConvStructure, Tensor4DData>
    {
        public new Tensor4DData Output { get { return (Tensor4DData)base.Output; } set { base.Output = value; } }
        public new Tensor4DData Input { get { return (Tensor4DData)base.Input; } set { base.Input = value; } }

        public int PadHeight { get; set; }
        public int PadWidth { get; set; }
        public int StrideHeight { get; set; }
        public int StrideWidth { get; set; }
        public A_Func Af { get; set; }

        public bool IsAf = true;

        public TensorConvRunner(TensorConvStructure model, Tensor4DData input, int pad, int stride, RunnerBehavior behavior) 
            : this(model, input, pad, stride, A_Func.Linear, behavior)
        {
            IsAf = false; 
        }


        public TensorConvRunner(TensorConvStructure model, Tensor4DData input, int pad, int stride, A_Func af, RunnerBehavior behavior) 
            : this(model, input, pad, pad, stride, stride, af, behavior)
        { }

        public TensorConvRunner(TensorConvStructure model, Tensor4DData input, int padHeight, int padWidth, int strideHeight, int strideWidth, A_Func af, RunnerBehavior behavior) 
            : base(model, input, behavior)
        {
            PadHeight = padHeight;
            PadWidth = padWidth;

            StrideHeight = strideHeight;
            StrideWidth = strideWidth;

            Af = af;

            int outputW = TensorConvUtil.FilterOutput(Input.Width, Model.FilterWidth, PadWidth, StrideWidth);
            int outputH = TensorConvUtil.FilterOutput(Input.Height, Model.FilterHeight, PadHeight, StrideHeight);

            Output = new Tensor4DData(outputW, outputH, Model.OutDepth, Input.MaxBatchSize, Behavior.Device);
        }


        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;

            ComputeLib.CNNForwardPropagateEx(Input.Output, Input.BatchSize, Input.Depth, Input.Height, Input.Width,
                Model.Filter, Model.OutDepth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
                Output.Output, Output.Height, Output.Width, 0, 1);
            //Output.Output.Print("Conv results", 100, true);

            if(IsAf) ActivationFuncLib.Image_ActiveOutput(Output.Output, Model.Bias, Af, Output.BatchSize, Output.Width, Output.Height, Output.Depth);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            // Backpropagate Deriv.
            if(IsAf) ActivationFuncLib.Image_DeactiveOutput(Output.Output, Output.Deriv, Af, Output.BatchSize, Output.Width, Output.Height, Output.Depth);
            
            if(Input.Deriv != null)
            {
                //float _out_l2 = ComputeLib.DotProduct(Output.Deriv, Output.Deriv, Output.Length); 
                //Logger.WriteLog("check output deriv norm step {0}", Math.Sqrt(_out_l2));

                ComputeLib.CuDNNBackwardPropagateDataEx(Input.Deriv, Output.BatchSize, Input.Depth, Input.Height, Input.Width,
                        Model.Filter, Output.Depth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
                        Output.Deriv, Output.Height, Output.Width, 1, 1);

                //float _filter_l2 = ComputeLib.DotProduct(Model.Filter, Model.Filter, Model.Filter.Size); 
                //Logger.WriteLog("check filter norm step {0}", Math.Sqrt(_filter_l2));

                //float _l2 = ComputeLib.DotProduct(Input.Deriv, Input.Deriv, Input.Length); 
                //Logger.WriteLog("check input deriv norm step {0}", Math.Sqrt(_l2));
            }

            if(Model.FilterGrad != null)
            {
                ComputeLib.CuDNNBackwardPropagateFilterEx(Input.Output, Input.BatchSize, Input.Depth, Input.Height, Input.Width,
                    Model.FilterGrad, Output.Depth, Model.FilterHeight, Model.FilterWidth, PadHeight, PadWidth, StrideHeight, StrideWidth,
                    Output.Deriv, Output.Height, Output.Width, 1, 1);
            }

            if(Model.BiasGrad != null && Model.IsBias)
            {
                ComputeLib.CuDNNBackwardPropagateBias(Model.BiasGrad, Output.Deriv, Output.BatchSize, Output.Depth, Output.Height, Output.Width, 1, 1);
            }
        }

    }
}
