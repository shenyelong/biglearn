﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class ImageFullyConnectRunner<IN> : StructRunner<LayerStructure, IN> where IN : ImageDataSource
    // {
    //     public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
    //     public CudaPieceFloat BiasOne;

    //     public ImageFullyConnectRunner(Structure model, ImageDataSource input, RunnerBehavior behavior)
    //         : base(model, input, behavior)
    //     {
    //         Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
    //         if (Model.IsBias)
    //         {
    //             BiasOne = new CudaPieceFloat(Input.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //             BiasOne.Init(1);
    //         }
    //     }

    //     public override void CleanDeriv()
    //     {
    //         ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
    //         //Output.Deriv.Data.Zero();
    //     }

    //     public override void Forward()
    //     {
    //         //InitMemory();
    //         //MathOperatorManager.MathDevice = Behavior.Device;
    //         Output.BatchSize = Input.BatchSize;
    //         ComputeLib.Sgemm(Input.Data, 0, Model.weight, 0, Output.Output.Data, 0, Input.BatchSize, Model.Neural_In, Model.Neural_Out, 0, 1, false, false);
    //         //ComputeLib.Matrix_Multipy(Input.Data, Model.weight, Output.Output.Data, Input.BatchSize,
    //         //                Model.Neural_In, Model.Neural_Out, 0);

    //         //Model.bias.SyncToCPU();
    //         ActivationFuncLib.ActivationFunc(Behavior.Computelib, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af, Model.bias);

    //         //Output.Output.Data.SyncToCPU();
    //         //Model.ActiveOutput(Output.Output, Behavior.RunMode);
    //     }

    //     /// <summary>
    //     /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
    //     /// </summary>
    //     /// <param name="cleanDeriv"></param>
    //     public override void Backward(bool cleanDeriv)
    //     {
    //         // Backpropagate Deriv.
    //         //Model.DeactiveOutput(Output.Output, Output.Deriv, Behavior.RunMode);
    //         ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.Deriv.Data, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af);

    //         ComputeLib.Sgemm(Output.Deriv.Data, 0, Model.weight, 0, Input.Deriv, 0, Input.BatchSize, Model.Neural_Out, Model.Neural_In, 1, 1, false, true);
    //         //ComputeLib.MatrixMultiplicationTranspose(Output.Deriv.Data, Model.weight, Input.Deriv, Input.BatchSize,
    //         //        Model.Neural_Out, Model.Neural_In, cleanDeriv ? 0 : 1);
    //         //Cudalib.Matrix_Multipy_Weight(Output.Deriv.Data.CudaPtr, Model.weight.CudaPtr, Input.Deriv.CudaPtr, Input.BatchSize,
    //         //        Model.Neural_Out, Model.Neural_In, 1, cleanDeriv ? 0 : 1);
    //     }

    //     public override void Update()
    //     {
    //         //Backpropagate Parameter Update.
    //         //Cudalib.Matrix_Product_Weight(Input.Data.CudaPtr, Output.Deriv.Data.CudaPtr, Model.weight.Grad.CudaPtr,
    //         //            Input.BatchSize, Model.Neural_In, Model.Neural_Out, 1);

    //         ComputeLib.Sgemm(Input.Data, 0, Output.Deriv.Data, 0, Model.weight.Grad, 0,
    //                     Input.BatchSize, Model.Neural_In, Model.Neural_Out, 1, 1, true, false);

    //         //ComputeLib.Matrix_Product_Weight(Input.Data, Output.Deriv.Data, Model.weight.Grad,
    //         //            Input.BatchSize, Model.Neural_In, Model.Neural_Out, 1);

    //         if (Model.IsBias)
    //         {
    //             //Output.Deriv.Data.SyncToCPU();
    //             //Model.bias.Grad.SyncToCPU();
    //             //Cudalib.Matrix_Aggragate_Weight(Output.Deriv.Data.CudaPtr, Model.bias.Grad.CudaPtr, Input.BatchSize, Model.Neural_Out, 1);
    //             ComputeLib.Sgemv(Output.Deriv.Data, BiasOne, Input.BatchSize, Model.Neural_Out, Model.bias.Grad, true, 1, 1);
    //             //Model.bias.Grad.SyncToCPU();

    //         }
    //     }
    // }
}
