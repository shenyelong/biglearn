using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Fully Connected Layer Runner.  (HiddenBatchData + LinkStructure)
    /// </summary>
    /// <typeparam name="IN"></typeparam>
    public class FNNRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        NdArrayData Weight { get; set; }
        NdArrayData Bias { get; set; }
        A_Func Af { get; set; }

        public CudaPieceFloat BiasOne;

        public FNNRunner(NdArrayData input, NdArrayData weight, NdArrayData bias, A_Func af, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
            Input = input;
            Weight = weight;
            Bias = bias;
            Af = af;

            Output = new NdArrayData(behavior.Device, weight.Shape[0], Input.Shape[1]);
         
            BiasOne = new CudaPieceFloat(Input.Shape[1].Default, behavior.Device);
            BiasOne.Init(1);
        }

        public override void Forward()
        {
            if(Input.Shape[0].Value != Weight.Shape[1].Value)
            {
                throw new Exception(string.Format("FNN Runner Dim doesn't match {0} , {1}", Input.Shape[0].Value, Weight.Shape[1].Value)); 
            }
            //Logger.WriteLog("input shape {0}, {1}", Input.Shape[1].Value, Weight.Shape[0].Value );

            Output.Output.EffectiveSize = Input.Shape[1].Value * Weight.Shape[0].Value;

            ComputeLib.Sgemm(Input.Output, 0, Weight.Output, 0, Output.Output, 0, Input.Shape[1].Value, Input.Shape[0].Value, Weight.Shape[0].Value, 0, 1, false, false);

            switch (Af)
            {
                case A_Func.Tanh:
                    ComputeLib.Matrix_Add_Tanh(Output.Output, Bias.Output, Input.Shape[1].Value, Weight.Shape[0].Value);
                    break;
                case A_Func.Linear:
                    ComputeLib.Matrix_Add_Linear(Output.Output, Bias.Output, Input.Shape[1].Value, Weight.Shape[0].Value);
                    break;
                case A_Func.Rectified:
                    ComputeLib.Matrix_Add_Rectified(Output.Output, Bias.Output, Input.Shape[1].Value, Weight.Shape[0].Value);
                    break;
                case A_Func.Sigmoid:
                    ComputeLib.Matrix_Add_Sigmoid(Output.Output, Bias.Output, Input.Shape[1].Value, Weight.Shape[0].Value);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// cleanDeriv: will be always false.
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            switch (Af)
            {
                case A_Func.Tanh:
                    ComputeLib.Deriv_Tanh(Output.Deriv, Output.Output, Input.Shape[1].Value, Weight.Shape[0].Value);
                    break;
                case A_Func.Rectified:
                    ComputeLib.Deriv_Rectified(Output.Deriv, Output.Output, Input.Shape[1].Value, Weight.Shape[0].Value);
                    break;
                case A_Func.Sigmoid:
                    ComputeLib.DerivLogistic(Output.Output, 0, Output.Deriv, 0, Output.Deriv, 0, Input.Shape[1].Value * Weight.Shape[0].Value, 0, 1);
                    break;
                case A_Func.Linear:
                    break;
                default:
                    throw new NotImplementedException();
            }

            if (Input.Deriv != null) 
                ComputeLib.Sgemm(Output.Deriv, 0, Weight.Output, 0, Input.Deriv, 0, Input.Shape[1].Value, Weight.Shape[0].Value, Weight.Shape[1].Value, 1, 1, false, true);

            if (Weight.Deriv != null)
                ComputeLib.Sgemm(Input.Output, 0, Output.Deriv, 0, Weight.Deriv, 0, Input.Shape[1].Value, Weight.Shape[1].Value, Weight.Shape[0].Value, 1, 1, true, false);

            if (Bias.Deriv != null)
                ComputeLib.Sgemv(Output.Deriv, BiasOne, Input.Shape[1].Value, Weight.Shape[0].Value, Bias.Deriv, true, 1, 1);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }
    }
}
