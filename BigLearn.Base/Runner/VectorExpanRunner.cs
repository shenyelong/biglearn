using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class VectorExpanRunner : StructRunner
    {
        public CudaPieceInt Ints = null;

        IntArgument Len { get; set; }
        int Idx { get; set; }
        /// <summary>
        /// Select Matrix Given BiMatchData.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="matchData"></param>
        /// <param name="side">0 : map source to target; 1 : source side; 2 : target side; </param>
        /// <param name="behavior"></param>
        public VectorExpanRunner(int id, IntArgument len, int maxLength, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Ints = new CudaPieceInt(maxLength, behavior.Device);
            Len = len;
            Idx = id;
            for(int i=0; i < maxLength; i++)
            {
                Ints.MemPtr[i] = Idx;
            }
            Ints.SyncFromCPU();
        }

        public override void Forward()
        {
            Ints.EffectiveSize = Len.Value; 
        }
    }
}
