﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ConvertSparseMatrixRunner : StructRunner
    {
        public new SparseMatrixData Output { get; set; }
        new List<int> Input { get; set; }
        int MaxBatchSize { get; set; }
        public ConvertSparseMatrixRunner(List<int> idx, int maxBatchSize, int dim, RunnerBehavior behavior) : base(Structure.Empty,  behavior)
        {
            Input = idx;
            Output = new SparseMatrixData(dim, maxBatchSize, maxBatchSize, behavior.Device, false);
        }

        public override void Forward()
        {
            Output.Row = Input.Count;
            Output.Length = Input.Count;
            for (int i = 0; i < Input.Count; i++)
            {
                Output.SampleIdx[i] = i + 1;
                Output.FeatureIdx[i] = Input[i];
                Output.FeatureValue[i] = 1;
            }
            Output.SyncFromCPU();
        }
    }
}
