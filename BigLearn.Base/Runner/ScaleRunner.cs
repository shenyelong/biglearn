using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ScaleRunner : StructRunner<Structure, VectorData>
    {
        float Scale { get; set; }
        public new VectorData Input { get { return (VectorData)base.Input; } set { base.Input = value; } }
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }
        bool IsInPlace { get; set; }

        public ScaleRunner(VectorData input, float scale, RunnerBehavior behavior) 
            : this(input, scale, false, behavior) //base(Structure.Empty, behavior)
        { }

        public ScaleRunner(VectorData input, float scale, bool isInPlace, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Scale = scale;
            if(isInPlace) Output = Input; // (T)(VectorData)input;
            else Output = new VectorData(Input.MaxLength, behavior.Device); // (T)(VectorData)(new MatrixData(input.Column, input.MaxRow, behavior.Device));
        }

        public override void Forward()
        {
            Output.Length = Input.Length;
            ComputeLib.Scale_Vector(Input.Output, 0, Output.Output, 0, Input.Length, Scale, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Scale_Vector(Output.Deriv, 0, Input.Deriv, 0, Input.Length, Scale, 1);
        }
    }
}
