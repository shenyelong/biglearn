﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixMultiplicationRunner : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        MatrixData InputA { get; set; }
        MatrixData InputB { get; set; }

        //bool IsProcessor = false;
        float Alpha = 0;
        float Beta = 1;
        int OpA { get; set; }
        int OpB { get; set; }
        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public MatrixMultiplicationRunner(MatrixData inputA, MatrixData inputB, RunnerBehavior behavior) 
            : this(inputA, 0, inputB, 0, behavior)
        {  }

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public MatrixMultiplicationRunner(MatrixData inputA, VectorData inputB, RunnerBehavior behavior) 
            : this(inputA, 0, new MatrixData(inputB), 0, behavior)
        {  }

        public MatrixMultiplicationRunner(MatrixData inputA, MatrixData inputB, MatrixData output, float alpha, RunnerBehavior behavior) 
            : this(inputA, 0, inputB, 0, output, alpha, 1, behavior)
        {  }

        public MatrixMultiplicationRunner(MatrixData inputA, int opA, MatrixData inputB, int opB, RunnerBehavior behavior) 
            : this(inputA, opA, inputB, opB, 
                    (opA == 0 && opB == 0) ? new MatrixData(behavior.Device, inputB.IColumn, inputA.IRow) :
                        ((opA == 0 && opB == 1) ? new MatrixData(behavior.Device, inputB.IRow, inputA.IRow) : 
				((opA == 1 && opB == 0) ? new MatrixData(behavior.Device, inputB.IColumn, inputA.IColumn) :  null)), 0, 1, behavior)
        {  }

        public MatrixMultiplicationRunner(MatrixData inputA, int opA, MatrixData inputB, int opB, MatrixData output, float alpha, float beta, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = output;
            OpA = opA;
            OpB = opB;

            Alpha = alpha;
            Beta = beta;
        }

        public override void Forward()
        {
            iteration++;

            if(OpA == 0 && OpB == 0)
            {
                Output.Row = InputA.Row;
                //Output.Column = InputB.Column;
                if (InputA.Column != InputB.Row) { throw new Exception(string.Format("Matrix A Dim1 {0} should be equals to Matrix B Dim2 {1}", InputA.Column, InputB.Row)); }
                ComputeLib.Sgemm(InputA.Output, 0, InputB.Output, 0, Output.Output, 0, InputA.Row, InputA.Column, InputB.Column, Alpha, Beta, false, false);
            }
            else if(OpA == 0 && OpB == 1)
            {
                Output.Row = InputA.Row;
                //Output.Column = InputB.Row;
                if (InputA.Column != InputB.Column) { throw new Exception(string.Format("Matrix A Dim1 {0} should be equals to Matrix B Dim1 {1}", InputA.Column, InputB.Column)); }
                ComputeLib.Sgemm(InputA.Output, 0, InputB.Output, 0, Output.Output, 0, InputA.Row, InputA.Column, InputB.Row, Alpha, Beta, false, true);
            }
            else if(OpA == 1 && OpB == 0)
            {
		          Output.Row = InputA.Column;
                if(InputA.Row != InputB.Row) { throw new Exception(string.Format("Matrix A Row {0} should be equals to Matrix B Row {1}", InputA.Row, InputB.Row)); }
                ComputeLib.Sgemm(InputA.Output, 0, InputB.Output, 0, Output.Output, 0, InputA.Row, InputA.Column, InputB.Column, Alpha, Beta, true, false);
            }
        }

        public override void CleanDeriv()
        {
            if (Output.Row == 0) { return; }
            if (Output.Deriv != null && !Output.Deriv.IsEmpty) ComputeLib.Zero(Output.Deriv, Output.Column * Output.Row);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if(Output.Row == 0) { return; }
            if(OpA == 0 && OpB == 0)
            {
                if (InputA.Deriv != null && !InputA.Deriv.IsEmpty)
                {
                    ComputeLib.Sgemm(Output.Deriv, 0, InputB.Output, 0, InputA.Deriv, 0, Output.Row, Output.Column, InputB.Row, 1, Beta, false, true);
                }

                if (InputB.Deriv != null && !InputB.Deriv.IsEmpty)
                {
                    ComputeLib.Sgemm(InputA.Output, 0, Output.Deriv, 0, InputB.Deriv, 0, InputA.Row, InputB.Row, InputB.Column, 1, Beta, true, false);
                }
            }
            else if(OpA == 0 && OpB == 1)
            {
                if (InputA.Deriv != null && !InputA.Deriv.IsEmpty)
                {
                    ComputeLib.Sgemm(Output.Deriv, 0, InputB.Output, 0, InputA.Deriv, 0, Output.Row, Output.Column, InputB.Column, 1, Beta, false, false);
                }
                if (InputB.Deriv != null && !InputB.Deriv.IsEmpty)
                {
                    ComputeLib.Sgemm(Output.Deriv, 0, InputA.Output, 0, InputB.Deriv, 0, Output.Row, Output.Column, InputB.Column, 1, Beta, true, false);
                }
            }
            else if(OpA == 1 && OpB == 0)
            {
                // throw new NotImplementedException();
                if (InputA.Deriv != null && !InputA.Deriv.IsEmpty)
                {
                    ComputeLib.Sgemm(InputB.Output, 0, Output.Deriv, 0, InputA.Deriv, 0, InputB.Row, InputB.Column, Output.Row, 1, Beta, false, true);
                }
                if (InputB.Deriv != null && !InputB.Deriv.IsEmpty)
                {
                    ComputeLib.Sgemm(InputA.Output, 0, Output.Deriv, 0, InputB.Deriv, 0, InputA.Row, InputA.Column, Output.Column, 1, Beta, false, false); 
                }   
            }
        }
    }

}
