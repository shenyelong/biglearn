using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorDropoutRunner : StructRunner 
    {
        public new Tensor4DData Output { get { return (Tensor4DData)base.Output; } set { base.Output = value; } }
        public new Tensor4DData Input { get { return (Tensor4DData)base.Input; } set { base.Input = value; } }

        float DropoutRate { get; set; }
        IntPtr DropoutState { get; set; }

        public TensorDropoutRunner(Tensor4DData input, float dropout, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Input = input;
            DropoutRate = dropout;
            Output = new Tensor4DData(behavior.Device, Input.Shape);
            DropoutState = ComputeLib.RegisterDropout(Input.MaxBatchSize, Input.Depth, Input.Height, Input.Width, DropoutRate);

            if(Behavior.RunMode == DNNRunMode.Train && DropoutRate > 0)
                Behavior.Resource.RegisteFloatVector(new Shape(Input.Shape).DefaultSize, "tmp1");
        }

        ~TensorDropoutRunner()
        {
            ComputeLib.UnregisterDropout(DropoutState);
        }

        public override void Forward()
        {   
            Output.BatchSize = Input.BatchSize;
            if(Behavior.RunMode == DNNRunMode.Train && DropoutRate > 0)
            {
                ComputeLib.CuDNNDropoutForward(Input.Output, Input.BatchSize, Input.Depth, Input.Height, Input.Width, Output.Output, DropoutRate, DropoutState);
            }
            else
            {
                ComputeLib.Add_Vector(Output.Output, Input.Output, Input.Length, 0, 1);
            }
        }
        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        public override void Backward(bool cleanDeriv)
        {
            if(DropoutRate > 0)
            {
                ComputeLib.CuDNNDropoutBackward(Output.Deriv, Input.BatchSize, Input.Depth, Input.Height, Input.Width, Behavior.Resource.MemF["tmp1"], DropoutRate, DropoutState);
                ComputeLib.Add_Vector(Input.Deriv, Behavior.Resource.MemF["tmp1"],  Input.Length, 1, 1);
            }
            else
            {
                ComputeLib.Add_Vector(Input.Deriv, Output.Deriv,  Input.Length, 1, 1);
            }
        }
    }
}
