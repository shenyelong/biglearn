using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RRUStateRunner : CompositeNetRunner // StructRunner<LSTMCell, HiddenBatchData>
    {
        public HiddenBatchData pO { get; set; }

        new GRUCell Model { get; set; }

        public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public RRUStateRunner(GRUCell model, HiddenBatchData po, HiddenBatchData input, RunnerBehavior behavior) : base(behavior)
        {
            pO = po;
            Input = input;
            Model = model;

            if(model.InputFeatureDim != Input.Dim)
            {
                throw new Exception("Model input feature dim : " + model.InputFeatureDim.ToString() + "; input dim : "+ Input.Dim.ToString());
            }

            MatrixData Wr = new MatrixData(Input.Dim, Input.MAX_BATCHSIZE, model.Wr, model.WrGrad, behavior.Device);
            MatrixData Ur = new MatrixData(model.HiddenStateDim, Input.MAX_BATCHSIZE, model.Ur, model.UrGrad, behavior.Device);
            VectorData Br = new VectorData(model.HiddenStateDim, model.Br, model.BrGrad, behavior.Device);

            MatrixData h_t_1 = pO.ToMD();
            MatrixData x_t = Input.ToMD();

            MatrixData r1 = x_t.MatMul(0, Wr, 0, Session, Behavior);
            MatrixData r2 = h_t_1.MatMul(0, Ur, 0, Session, Behavior);

            MatrixData r = r1.Add(r2, Session, Behavior).Add(Br, Session, Behavior).Act(A_Func.Sigmoid, Session, Behavior);

            MatrixData rData = h_t_1.Dot(r, Session, Behavior);

            
            MatrixData Wh = new MatrixData(Input.Dim, Input.MAX_BATCHSIZE, model.Wr, model.WrGrad, behavior.Device);
            MatrixData Uh = new MatrixData(model.HiddenStateDim, Input.MAX_BATCHSIZE, model.Ur, model.UrGrad, behavior.Device);
            VectorData Bh = new VectorData(model.HiddenStateDim, model.Bh, model.BhGrad, behavior.Device);


            MatrixData h1 = x_t.MatMul(0, Wh, 0, Session, Behavior);
            MatrixData h2 = rData.MatMul(0, Uh, 0, Session, Behavior);

            MatrixData hhat = h1.Add(h2, Session, Behavior).Add(Bh, Session, Behavior).Act(A_Func.Tanh, Session, Behavior);

            MatrixData o = hhat.Add(h_t_1, Session, Behavior);

            Output = o.ToHDB();
        }
    }
}