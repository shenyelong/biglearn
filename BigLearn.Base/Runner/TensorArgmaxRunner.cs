using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorArgmaxRunner : StructRunner
    {
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public CudaPieceInt Indices { get; set; }

        IntArgument[] reduce_dims { get; set; }

        IntArgument[] in_dims { get; set; }

        IntPtr argmaxState;
        int MaxDim { get; set; }
        
        CudaPieceInt inDims { get; set; }
        CudaPieceInt outDims { get; set; }

        public TensorArgmaxRunner(NdArrayData input, int dim, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Input = input;
            
            in_dims = Input.Shape;
            if(in_dims.Length < 4)
            {
                in_dims = new IntArgument[4];
                for(int i = 0; i < 4; i++)
                {
                    if(i < Input.Shape.Length) in_dims[i] = Input.Shape[i];
                    else in_dims[i] = new IntArgument("default_dim", 1);
                }
            }

            reduce_dims = new IntArgument[in_dims.Length];
            for(int i = 0; i < reduce_dims.Length; i++)
            {
                if(i == dim) reduce_dims[i] = new IntArgument("reduce_dim", 1);
                else reduce_dims[i] = in_dims[i];
            }

            MaxDim = dim;

            Output = new NdArrayData(reduce_dims, behavior.Device);
            Indices = new CudaPieceInt(new Shape(reduce_dims).DefaultSize, behavior.Device);

            argmaxState = ComputeLib.RegisteTensorLazyReduce(3, in_dims.Length, new Shape(in_dims).RDimList, new Shape(reduce_dims).RDimList);
            Behavior.Resource.RegisteReduceState(argmaxState);

            inDims = new CudaPieceInt(in_dims.Length, behavior.Device);
            outDims = new CudaPieceInt(in_dims.Length, behavior.Device);
        }

        public override void Forward()
        {
            ComputeLib.TensorReduceOp(Input.Output, Output.Output, 0, 1, argmaxState);
            ComputeLib.GetReduceIndices(argmaxState, Indices, new Shape(reduce_dims).DefaultSize);

            
        }

        public override void CleanDeriv()
        {
            if (Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if(Input.Deriv != null)
            {
                inDims.Init(new Shape(reduce_dims).DimList);
                outDims.Init(new Shape(in_dims).DimIndexes);
                ComputeLib.NDArrayMaxBackward(Output.Deriv, Indices, inDims, Input.Deriv, outDims, MaxDim, in_dims.Length, Output.Length, 1.0f);
            }
        }

    }
}
