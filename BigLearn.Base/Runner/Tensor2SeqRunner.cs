using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class Tensor2SeqRunner : StructRunner
    {
        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        CudaPieceInt SeqLen { get; set; }

        CudaPieceInt SampleIdx { get; set; }
        CudaPieceInt SentMargin { get; set; }
        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public Tensor2SeqRunner(NdArrayData input, CudaPieceInt seqlen, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            SeqLen = seqlen;
            
            SampleIdx = new CudaPieceInt(input.Shape[2].Default, behavior.Device);
            SentMargin = new CudaPieceInt(input.Shape[2].Default * input.Shape[1].Default, behavior.Device);

            Output = new SeqDenseBatchData(SampleIdx, SentMargin, input.Shape[2].Default, input.Shape[2].Default * input.Shape[1].Default, input.Shape[0].Default, behavior.Device);
        }

        public override void Forward()
        {
            SeqLen.SyncToCPU();

            int cursor = 0;
            for(int batch = 0; batch < Input.Shape[2].Value; batch++)
            {
                for(int i = 0; i < SeqLen[batch]; i++)
                    SentMargin[cursor + i] = batch;
                
                cursor = cursor + SeqLen[batch];
                SampleIdx[batch] = cursor;
            }

            SampleIdx.EffectiveSize = Input.Shape[2].Value; 
            SampleIdx.SyncFromCPU();

            SentMargin.EffectiveSize = cursor;
            SentMargin.SyncFromCPU();

            Output.SentSize = cursor;
            ComputeLib.NDArray2SegForward(Input.Output, Input.Shape[0].Value, Input.Shape[1].Value, Input.Shape[2].Value, SampleIdx, SentMargin, Output.SentSize, Output.SentOutput);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.Dim * Output.SentSize);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.NDArray2SegBackward(Input.Deriv, Input.Shape[0].Value, Input.Shape[1].Value, Input.Shape[2].Value, SampleIdx, SentMargin, Output.SentSize, Output.SentDeriv, 1.0f);
        }

    }
}
