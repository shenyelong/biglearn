﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MatrixInnerProductRunner : StructRunner<Structure, BatchData>
    {
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }

        MatrixData InputA { get; set; }
        MatrixData InputB { get; set; }

        BiMatchBatchData MatchData = null;

        public MatrixInnerProductRunner(MatrixData inputA, MatrixData inputB, BiMatchBatchData matchData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            MatchData = matchData;

            Output = new VectorData(MatchData == null ? InputA.MaxRow : MatchData.MAX_MATCH_BATCHSIZE, Behavior.Device);
        
            if(InputA.Column != InputB.Column) 
            {
                throw new Exception(string.Format("Matrix Inner Product Dimension Doesn't match {0} , {1}", InputA.Column, InputB.Column)); 
            }
        }

        public override void Forward()
        {
            iteration++;

            //var perf_mip = PerfCounter.Manager.Instance["matrix_inner_product"].Begin();

            Output.Length = MatchData == null ? InputA.Row : MatchData.MatchSize;
            ComputeLib.Inner_Product_Matching(InputA.Output, InputB.Output, Output.Output,
                                              MatchData == null ? CudaPieceInt.Empty :  MatchData.SrcIdx, 
                                              MatchData == null ? CudaPieceInt.Empty : MatchData.TgtIdx,
                                              InputA.Row, InputB.Row, Output.Length, InputA.Column, 0);
            
            //PerfCounter.Manager.Instance["matrix_inner_product"].TakeCount(perf_mip);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (MatchData == null)
            {
                ComputeLib.Scale_MatrixMask(InputA.Output, 0, CudaPieceInt.Empty, 0,
                                            InputB.Deriv, 0, CudaPieceInt.Empty, 0,
                                            InputA.Column, InputA.Row, Output.Deriv, 1);

                ComputeLib.Scale_MatrixMask(InputB.Output, 0, CudaPieceInt.Empty, 0,
                                            InputA.Deriv, 0, CudaPieceInt.Empty, 0,
                                            InputB.Column, InputB.Row, Output.Deriv, 1);
            }
            else
            {
                ComputeLib.Deriv_InnerProduct_Matching(InputA.Output, InputB.Output, InputA.Column,
                                                       MatchData.Src2MatchIdx, MatchData.Src2MatchElement,
                                                       MatchData.Tgt2MatchIdx, MatchData.Tgt2MatchElement,
                                                       MatchData.SrcIdx, MatchData.TgtIdx,
                                                       MatchData.SrcSize, MatchData.TgtSize, MatchData.MatchSize,
                                                       Output.Deriv, InputA.Deriv, InputB.Deriv, 0);
            }
        }
    }
}
