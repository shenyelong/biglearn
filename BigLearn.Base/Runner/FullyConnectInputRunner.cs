﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class FullyConnectInputRunner<IN> : StructRunner<LayerStructure, IN> where IN : GeneralBatchInputData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat BiasOne;

        DropOutProcessor<HiddenBatchData> DropOutRegularized = null;

        public FullyConnectInputRunner(LayerStructure model, GeneralBatchInputData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
            if (Model.IsBias)
            {
                BiasOne = new CudaPieceFloat(Input.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                BiasOne.Init(1);
            }
            if (Model.DropOut > 0) DropOutRegularized = new DropOutProcessor<HiddenBatchData>(Output, Model.DropOut, Behavior);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }
        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            switch (Model.Nt)
            {
                case N_Type.Fully_Connected:
                    switch (Input.FeatureType)
                    {
                        case FeatureDataType.SparseFeature:
                            ComputeLib.SparseSgemmMask(Input.BatchIdx, Input.FeatureIdx, Input.FeatureValue,
                                        Model.weight, 0,
                                        Output.Output.Data, 0,
                                        Input.BatchSize, Model.Neural_In, Model.Neural_Out,
                                        CudaPieceInt.Empty, 0,
                                        CudaPieceInt.Empty, 0,
                                        CudaPieceInt.Empty, 0,
                                        0, 1, false, false);
                            break;
                        case FeatureDataType.DenseFeature:
                            ComputeLib.Sgemm(Input.DenseFeatureData, 0, Model.weight, 0, Output.Output.Data, 0, Input.BatchSize, Model.Neural_In, Model.Neural_Out, 0, 1, false, false);
                            break;
                    }
                    break;
                //case N_Type.Tensor_layer:
                    //switch (Input.FeatureType)
                    //{
                    //    case FeatureDataType.DenseFeature:
                    //        ComputeLib.Sgemm(Input.DenseFeatureData, 0, Model.weight, 0, Output.Output.Data, 0, Input.BatchSize, Model.Neural_In, Model.Neural_Out, 0, 1, false, false);
                    //        Cudalib.Tensor_Matrix_Multiply_Weight(Input.DenseFeatureData.CudaPtr, Model.tensorWeight.CudaPtr, Output.Output.Data.CudaPtr, Input.BatchSize, Model.Neural_In, Model.Neural_Out, 0, 1);
                    //        break;
                    //}
                //    break;
            }

            if (Behavior.RunMode == DNNRunMode.Train && Model.DropOut > 0) DropOutRegularized.Forward();
            ActivationFuncLib.ActivationFunc(ComputeLib, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af, Model.bias);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ActivationFuncLib.DerivActivationFunc(ComputeLib, Output.Deriv.Data, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af);
            if (Model.DropOut > 0) DropOutRegularized.Backward(false);
        }

        public override void Update()
        {
            switch (Model.Nt)
            {
                case N_Type.Fully_Connected:
                    switch (Input.FeatureType)
                    {
                        case FeatureDataType.SparseFeature:
                            ComputeLib.SparseSgemmMask(Input.BatchIdx, Input.FeatureIdx, Input.FeatureValue,
                                       Output.Deriv.Data, 0,
                                       Model.weightGrad, 0,
                                       Input.BatchSize, Model.Neural_In, Model.Neural_Out,
                                       CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                       1, 1, true, false);
                            break;
                        case FeatureDataType.DenseFeature:
                            ComputeLib.Sgemm(Input.DenseFeatureData, 0, Output.Deriv.Data, 0, Model.weightGrad, 0,
                                    Input.BatchSize, Model.Neural_In, Model.Neural_Out, 1, 1, true, false);
                            break;
                    }
                    break;
                //case N_Type.Tensor_layer:
                    // switch (Input.FeatureType)
                    // {
                    //     case FeatureDataType.DenseFeature:
                    //         Cudalib.Matrix_Product_Weight(Input.DenseFeatureData.CudaPtr, Output.Deriv.Data.CudaPtr, Model.WeightOptimizerGradient.CudaPtr,
                    //                 Input.BatchSize, Model.Neural_In, Model.Neural_Out, Model.WeightOptimizerGradientStep);

                    //         if (!IsDelegateOptimizer)  Model.TensorWeightOptimizer.BeforeGradient();
                    //         Cudalib.Matrix_Tensor_Product_Weight(Input.DenseFeatureData.CudaPtr, Output.Deriv.Data.CudaPtr, Model.TensorWeightOptimizerGradient.CudaPtr,
                    //                 Input.BatchSize, Model.Neural_In, Model.Neural_Out, Model.TensorWeightOptimizerGradientStep);
                    //         if (!IsDelegateOptimizer)  Model.TensorWeightOptimizer.AfterGradient();
                    //         break;
                    // }
                //    break;
            }
            if (Model.IsBias)
            {
                ComputeLib.Sgemv(Output.Deriv.Data, BiasOne, Input.BatchSize, Model.Neural_Out, Model.biasGrad, true, 1, 1);
            }
        }
    }

}
