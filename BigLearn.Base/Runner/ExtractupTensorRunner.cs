using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // fill up the indices vectors into embed memory.
    public class ExtractupTensorRunner : StructRunner
    {
        public CudaPieceInt DimExtractup { get; set; }
            
        public NdArrayData Memory { get; set; }
        public NdArrayData Data { get; set; }
        
        CudaPieceInt MemDims { get; set; }
        

        public ExtractupTensorRunner(CudaPieceInt idxes, NdArrayData memory, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
            DimExtractup = idxes;
            
            Memory = memory;

            Data = new NdArrayData(behavior.Device, Memory.Shape[0], Memory.Shape[2]);

            MemDims = new CudaPieceInt(Memory.Shape.Length, behavior.Device);
        }

        public override void Forward()
        {
            for(int i = 0; i < Memory.Shape.Length; i++)
            {
                MemDims[i] = Memory.Shape[i].Value;
            }    
            MemDims.SyncFromCPU();

            ComputeLib.NDArrayScatter(Memory.Output, Memory.Shape.Length, MemDims, Data.Output, DimExtractup, Data.Length, 1.0f, 0.0f, 0.0f, 1.0f);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Data.Deriv, Data.Length); 
        }

        public override void Backward(bool isClearDeriv)
        {
            if(Memory.Deriv != null && !Memory.Deriv.IsEmpty && Data.Deriv != null && !Data.Deriv.IsEmpty)
            {
                ComputeLib.NDArrayScatter(Memory.Deriv, Memory.Shape.Length, MemDims, Data.Deriv, DimExtractup, Data.Length, 1.0f, 1.0f, 1.0f, 0.0f);
            }
        }

    }
}
