﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class GRURecurrentRunner : StructRunner<GRUCell, BatchData>
    {
        RecurrentSeqInfo RecurrentInfo { get; set; }
        MatrixData GateR { get; set; }
        MatrixData GateZ { get; set; }
        MatrixData GateZ_N { get; set; }

        MatrixData HHat { get; set; }
        MatrixData RsetH { get; set; }
        public new MatrixData Output { get; set; }

        //MatrixData InitO { get; set; }
        MatrixData InputO { get; set; }
        List<StructRunner> ProcessFlow = new List<StructRunner>();

        public GRURecurrentRunner(RecurrentSeqInfo recurrentInfo, MatrixData inputO, GRUCell model,
            MatrixData gateR, MatrixData gateZ, MatrixData hHat, RunnerBehavior behavior) 
            :base(model, behavior)
        {
            RecurrentInfo = recurrentInfo;
            InputO = inputO;
            GateR = gateR;
            GateZ = gateZ;
            HHat = hHat;

            Output = new MatrixData(model.HiddenStateDim, hHat.MaxRow, behavior.Device);
            RsetH = new MatrixData(model.HiddenStateDim, hHat.MaxRow, behavior.Device);
        }

        public override void Forward()
        {
            ProcessFlow.Clear();
            RsetH.Row = RecurrentInfo.SentSize;
            Output.Row = RecurrentInfo.SentSize;
            MatrixData Ur = new MatrixData(Model.HiddenStateDim, Model.HiddenStateDim, Model.Ur, Model.UrGrad, Behavior.Device);
            MatrixData Uz = new MatrixData(Model.HiddenStateDim, Model.HiddenStateDim, Model.Uz, Model.UzGrad, Behavior.Device);
            MatrixData Uh = new MatrixData(Model.HiddenStateDim, Model.HiddenStateDim, Model.Uh, Model.UhGrad, Behavior.Device);

            int preStartIdx = -1;
            for (int i = 0; i < RecurrentInfo.MaxLag; i++)
            {
                int startIdx = i == 0 ? 0 : RecurrentInfo.LagSeqIdx[i - 1];
                int Sum = RecurrentInfo.LagSeqIdx[i] - startIdx;
                if (Sum == 0) break;

                MatrixData initO = null;
                int batchSize = Sum;
                if (i > 0)
                {
                    initO = new MatrixData(Model.HiddenStateDim, batchSize,
                        Output.Output.SubArray(preStartIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        Output.Deriv.SubArray(preStartIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim), 
                        Behavior.Device);
                }
                else if(InputO != null)
                {
                    initO = InputO;
                }

                MatrixData newGateR = new MatrixData(Model.HiddenStateDim, batchSize,
                        GateR.Output.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        GateR.Deriv.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        Behavior.Device);

                MatrixData newGateZ = new MatrixData(Model.HiddenStateDim, batchSize,
                        GateZ.Output.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        GateZ.Deriv.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        Behavior.Device);

                MatrixData newHHat = new MatrixData(Model.HiddenStateDim, batchSize,
                        HHat.Output.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        HHat.Deriv.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        Behavior.Device);

                MatrixData newO = new MatrixData(Model.HiddenStateDim, batchSize,
                        Output.Output.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        Output.Deriv.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        Behavior.Device);

                if (initO != null)
                {
                    ProcessFlow.Add(new MatrixMultiplicationRunner(initO, Ur, newGateR, 1, Behavior));
                    ProcessFlow.Add(new MatrixMultiplicationRunner(initO, Uz, newGateZ, 1, Behavior));
                }

                ProcessFlow.Add(new ActivationRunner(newGateR, A_Func.Sigmoid, Behavior));
                ProcessFlow.Add(new ActivationRunner(newGateZ, A_Func.Sigmoid, Behavior));

                if (initO != null)
                {
                    MatrixData rsetH = new MatrixData(Model.HiddenStateDim, batchSize,
                        RsetH.Output.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        RsetH.Deriv.SubArray(startIdx * Model.HiddenStateDim, batchSize * Model.HiddenStateDim),
                        Behavior.Device);

                    ProcessFlow.Add(new ProductRunner(initO, newGateR, rsetH, 0, 1, Behavior));
                    ProcessFlow.Add(new MatrixMultiplicationRunner(rsetH, Uh, newHHat, 1, Behavior));
                }

                ProcessFlow.Add(new ActivationRunner(newHHat, A_Func.Tanh, Behavior));
                ProcessFlow.Add(new ProductRunner(HHat, newGateZ, newO, 0, 1, Behavior));

                // s += (1-gate_z) @ s(t-1)
                if (initO != null)
                {
                    ProcessFlow.Add(new AdditionRunner(initO, null, newO, false, Behavior));
                    ProcessFlow.Add(new ProductRunner(initO, newGateZ, newO, 1, -1, Behavior));
                }
                preStartIdx = startIdx;
            }

            foreach(StructRunner subRunner in ProcessFlow) { subRunner.Forward(); }
        }

        public override void Backward(bool cleanDeriv)
        {
            foreach(StructRunner subRunner in Enumerable.Reverse(ProcessFlow)) { subRunner.Backward(cleanDeriv); }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(RsetH.Deriv, RsetH.Row * RsetH.Column);
            ComputeLib.Zero(Output.Deriv, Output.Row * Output.Column);
            foreach (StructRunner subRunner in ProcessFlow) { subRunner.CleanDeriv(); }
        }

    }

    /// <summary>
    /// This is not tested yet.
    /// </summary>
    public class FastGRUSparseRunner : StructRunner<GRUCell, SeqSparseBatchData>
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }

        SeqDenseBatchData GateR; //reset gate
        SeqDenseBatchData GateZ; //update gate
        SeqDenseBatchData HHat; //new memory
        SeqDenseBatchData RestH; //new memory

        //BatchData input,  input, 
        //public GRUSparseRunner(Structure model, RunnerBehavior behavior)
        //    : base(model, behavior)
        //{
        //    Output = new SeqDenseBatchData();
        //}

        public FastGRUSparseRunner(ComputationGraph cg, GRUCell model, HiddenBatchData inputO, SeqSparseBatchData data, bool isReverse, RunnerBehavior behavior)
            : base(cg, model, data, behavior)
        {
            MatrixData Wh = new MatrixData(model.HiddenStateDim, model.InputFeatureDim, model.Wh, model.WhGrad, Behavior.Device);
            MatrixData Wr = new MatrixData(model.HiddenStateDim, model.InputFeatureDim, model.Wr, model.WrGrad, Behavior.Device);
            MatrixData Wz = new MatrixData(model.HiddenStateDim, model.InputFeatureDim, model.Wz, model.WzGrad, Behavior.Device);

            VectorData Bh = new VectorData(model.HiddenStateDim, model.Bh, model.BhGrad, Behavior.Device);
            VectorData Br = new VectorData(model.HiddenStateDim, model.Br, model.BrGrad, Behavior.Device);
            VectorData Bz = new VectorData(model.HiddenStateDim, model.Bz, model.BzGrad, Behavior.Device);

            SparseMatrixData X = new SparseMatrixData(data.FEATURE_DIM, data.MAX_SENTSIZE, data.MAX_ELEMENTSIZE, 
                data.SequenceIdx, data.FeaIdx, data.FeaValue, CudaPieceFloat.Empty, Behavior.Device);
            RecurrentSeqInfo RecurrentInfo = (RecurrentSeqInfo)cg.AddRunner(new RecurrentSeqInfoRunner(data.MAX_BATCHSIZE, data.MAX_SENTSIZE, data.SampleIdx, isReverse, behavior));

            MatrixData h = (MatrixData)CG.AddRunner(new SparseMultiplicationRunner(X, Wh, RecurrentInfo.MapForward, behavior));
            MatrixData r = (MatrixData)CG.AddRunner(new SparseMultiplicationRunner(X, Wr, RecurrentInfo.MapForward, behavior));
            MatrixData z = (MatrixData)CG.AddRunner(new SparseMultiplicationRunner(X, Wz, RecurrentInfo.MapForward, behavior));

            CG.AddRunner(new VectorExpansionProcessor(Bh, h, behavior));
            CG.AddRunner(new VectorExpansionProcessor(Br, r, behavior));
            CG.AddRunner(new VectorExpansionProcessor(Bz, z, behavior));

            MatrixData InputO = null;
            if (inputO!= null)
            {
                InputO = new MatrixData((HiddenBatchData)CG.AddRunner(new MatrixShuffleRunner(inputO, RecurrentInfo.BatchShuffle, CudaPieceInt.Empty, Behavior)));
            }
            MatrixData o = (MatrixData)CG.AddRunner(new GRURecurrentRunner(RecurrentInfo, InputO, Model, r, z, h, Behavior));

            //SequenceDataStat stat = new SequenceDataStat(Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim);
            Output = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, RecurrentInfo, 
                                               Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.HiddenStateDim,
                                               o.Output, o.Deriv, Behavior.Device);
        }
    }

    // public class GRUSparseRunner<IN> : StructRunner<GRUCell, IN> where IN : SeqSparseBatchData
    // {
    //     public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }

    //     SeqDenseBatchData GateR; //reset gate
    //     SeqDenseBatchData GateZ; //update gate
    //     SeqDenseBatchData HHat; //new memory
    //     SeqDenseBatchData RestH; //new memory

    //     //BatchData input,  input, 
    //     //public GRUSparseRunner(Structure model, RunnerBehavior behavior)
    //     //    : base(model, behavior)
    //     //{
    //     //    Output = new SeqDenseBatchData();
    //     //}

    //     public GRUSparseRunner(Structure model, BatchData data, RunnerBehavior behavior)
    //         : base(model, data, behavior)
    //     {
    //         Output = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);

    //         GateR = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
    //         GateZ = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
    //         HHat = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
    //         RestH = new SeqDenseBatchData(Input.MAX_BATCHSIZE, Input.Stat.MAX_SEQUENCESIZE, Model.HiddenStateDim, Behavior.Device);
    //     }

    //     public override void Forward()
    //     {
    //         //MathOperatorManager.MathDevice = Behavior.Device;
    //         Output.SampleIdx = Input.SampleIdx;
    //         Output.SentMargin = Input.SentMargin;

    //         Output.BatchSize = Input.BatchSize;
    //         Output.SentSize = Input.SentSize;

    //         //InitMemory();

    //         /*Wr X -> GateR*/
    //         ComputeLib.SparseSgemmMask(Input.SequenceIdx, Input.FeaIdx, Input.FeaValue,
    //                                     Model.Wr, 0, GateR.SentOutput, 0, Input.SentSize,
    //                                     Input.Stat.FEATURE_DIM, Model.HiddenStateDim,
    //                                     CudaPieceInt.Empty, 0,
    //                                     CudaPieceInt.Empty, 0,
    //                                     CudaPieceInt.Empty, 0,
    //                                     0, 1, false, false);

    //         MathOperatorManager.GlobalInstance.Matrix_Add_Linear(GateR.SentOutput, Model.Br, Input.SentSize, Model.HiddenStateDim);

    //         /*Wz X -> GateZ*/
    //         MathOperatorManager.GlobalInstance.Sparse_Matrix_Multiply_INTEX_Weight(
    //                                                 Input.SequenceIdx,
    //                                                 Input.FeaIdx,
    //                                                 Input.FeaValue,
    //                                                 Input.SentSize,
    //                                                 Input.ElementSize,
    //                                                 Model.Wz,
    //                                                 GateZ.SentOutput,
    //                                                 Model.HiddenStateDim,
    //                                                 0);
    //         MathOperatorManager.GlobalInstance.Matrix_Add_Linear(GateZ.SentOutput, Model.Bz, Input.SentSize, Model.HiddenStateDim);

    //         /*Wh X -> HHat*/
    //         MathOperatorManager.GlobalInstance.Sparse_Matrix_Multiply_INTEX_Weight(
    //                                                 Input.SequenceIdx,
    //                                                 Input.FeaIdx,
    //                                                 Input.FeaValue,
    //                                                 Input.SentSize,
    //                                                 Input.ElementSize,
    //                                                 Model.Wh,
    //                                                 HHat.SentOutput,
    //                                                 Model.HiddenStateDim,
    //                                                 0);
    //         MathOperatorManager.GlobalInstance.Matrix_Add_Linear(HHat.SentOutput, Model.Bh, Input.SentSize, Model.HiddenStateDim);

    //         MathOperatorManager.GlobalInstance.GRUForwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Model.HiddenStateDim,
    //                                                Output.SentOutput, GateR.SentOutput, GateZ.SentOutput, HHat.SentOutput, RestH.SentOutput,
    //                                                Model.Ur, Model.Uz, Model.Uh);
    //     }

    //     public override void CleanDeriv()
    //     {
    //         Output.SentDeriv.Zero();
    //     }

    //     /// <summary>
    //     /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
    //     /// </summary>
    //     /// <param name="cleanDeriv"></param>
    //     public override void Backward(bool cleanDeriv)
    //     {

    //         MathOperatorManager.GlobalInstance.GRUBackwardPropagateBatch(Input.SampleIdx, Input.BatchSize, Model.HiddenStateDim,
    //             Output.SentOutput, GateR.SentOutput, GateZ.SentOutput, HHat.SentOutput, RestH.SentOutput,
    //             Output.SentDeriv, GateR.SentDeriv, GateZ.SentDeriv, HHat.SentDeriv, RestH.SentDeriv,
    //             Model.Ur, Model.Uz, Model.Uh);
    //     }

    //     public override void Update()
    //     {
    //         /*GateR*/
    //         Model.WrMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.MatrixMultiplicationSparseLeftTranspose(Input.SequenceIdx, Input.SentSize, Input.FeaIdx,
    //                                     Input.FeaValue, Input.ElementSize, GateR.SentDeriv, Model.WrMatrixOptimizer.Gradient,
    //                                     Model.InputFeatureDim, Model.HiddenStateDim,
    //                                     Model.WrMatrixOptimizer.GradientStep);
    //         Model.WrMatrixOptimizer.AfterGradient();
    //         Model.BrMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.ColumnWiseSum(GateR.SentDeriv, Model.BrMatrixOptimizer.Gradient, Output.SentSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
    //         Model.BrMatrixOptimizer.AfterGradient();

    //         /*GateZ*/
    //         Model.WzMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.MatrixMultiplicationSparseLeftTranspose(Input.SequenceIdx, Input.SentSize, Input.FeaIdx,
    //                                     Input.FeaValue, Input.ElementSize, GateZ.SentDeriv, Model.WzMatrixOptimizer.Gradient,
    //                                     Model.InputFeatureDim, Model.HiddenStateDim,
    //                                     Model.WzMatrixOptimizer.GradientStep);
    //         Model.WzMatrixOptimizer.AfterGradient();
    //         Model.BzMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.ColumnWiseSum(GateZ.SentDeriv, Model.BzMatrixOptimizer.Gradient, Output.SentSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);
    //         Model.BzMatrixOptimizer.AfterGradient();

    //         /*H*/
    //         Model.WhMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.MatrixMultiplicationSparseLeftTranspose(Input.SequenceIdx, Input.SentSize, Input.FeaIdx,
    //                                     Input.FeaValue, Input.ElementSize, HHat.SentDeriv, Model.WhMatrixOptimizer.Gradient,
    //                                     Model.InputFeatureDim, Model.HiddenStateDim,
    //                                     Model.WhMatrixOptimizer.GradientStep);
    //         Model.WhMatrixOptimizer.AfterGradient();
    //         Model.BhMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.ColumnWiseSum(HHat.SentDeriv, Model.BhMatrixOptimizer.Gradient, Output.SentSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
    //         Model.BhMatrixOptimizer.AfterGradient();

    //         /*Hiddent*/
    //         Model.UhMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.MatrixMultiplicationLeftTranspose(RestH.SentOutput, HHat.SentDeriv, Model.UhMatrixOptimizer.Gradient,
    //                                 Input.SentSize, Model.HiddenStateDim, Model.HiddenStateDim, Model.UhMatrixOptimizer.GradientStep);
    //         Model.UhMatrixOptimizer.AfterGradient();

    //         /*Recurrent GateZ */
    //         Model.UzMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.RecursiveUpdateBatch(Input.SampleIdx, Input.BatchSize, Input.SentSize, Output.SentOutput, Model.HiddenStateDim,
    //                                     GateZ.SentDeriv, 1, Model.UzMatrixOptimizer.Gradient, Model.UzMatrixOptimizer.GradientStep);
    //         Model.UzMatrixOptimizer.AfterGradient();

    //         /*Recurrent GateR*/
    //         Model.UrMatrixOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.RecursiveUpdateBatch(Input.SampleIdx, Input.BatchSize, Input.SentSize, Output.SentOutput, Model.HiddenStateDim,
    //                                     GateR.SentDeriv, 1, Model.UrMatrixOptimizer.Gradient, Model.UrMatrixOptimizer.GradientStep);
    //         Model.UrMatrixOptimizer.AfterGradient();
    //     }
    // }

}
