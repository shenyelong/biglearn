using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // try to avoid in memory operation. it is very easy to cause the bug. I would rather waste the gpu memory. 
    public class BatchNorm2DScaleBiasAfV2Runner : StructRunner
    {
        public new Tensor4DData Output { get { return (Tensor4DData)base.Output; } set { base.Output = value; } }
        new Tensor4DData Input { get { return (Tensor4DData)base.Input; } set { base.Input = value; } }
        Tensor4DData Scale { get; set; }
        Tensor4DData Bias { get; set; }

        //CudaPieceFloat mean;
        //IntPtr meanState;

        //CudaPieceFloat var_1;
        //CudaPieceFloat var;
        //IntPtr varState;

        //IntPtr sumState;

        //CudaPieceFloat w;
        //CudaPieceFloat tmpO;
        //CudaPieceFloat tmpODeriv;

        IntArgument[] reduce_dims { get; set; }

        IntArgument[] input_dims { get; set; }
        
        //float Discount = 1;
        //float Discount1 = 1;
        public CudaPieceFloat running_mean;

        public CudaPieceFloat running_var;

        public CudaPieceFloat save_mean;

        public CudaPieceFloat save_var;


        float Momentum = 0.1f;

        int ReduceSize = 0;

        public BatchNorm2DScaleBiasAfV2Runner(Tensor4DData input, Tensor4DData scale, Tensor4DData bias, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            Scale = scale;
            Bias = bias;

            input_dims = Input.Shape;

            reduce_dims = new IntArgument[4];
            reduce_dims[0] = new IntArgument("reduce_width", 1);
            reduce_dims[1] = new IntArgument("reduce_height", 1);
            reduce_dims[2] = Input.Shape[2];
            reduce_dims[3] = new IntArgument("reduce_batch", 1);

            Logger.WriteLog("BNAF reduce size {0}, input size {1}",  new Shape(reduce_dims).DefaultSize, new Shape(input_dims).DefaultSize);
            //ReduceSize = Input.Shape[0].Default * Input.Shape[1].Default * Input.Shape[3].Default;
            //Discount = 1.0f / (float)Math.Sqrt(ReduceSize);
            //Discount1 = 1.0f / ReduceSize;

            //mean = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            //meanState = ComputeLib.RegisteTensorReduce(0, input_dims.Length, new Shape(input_dims).RDimList, new Shape(reduce_dims).RDimList);
            //sumState = ComputeLib.RegisteTensorLazyReduce(2, input_dims.Length, new Shape(input_dims).RDimList, new Shape(reduce_dims).RDimList);
            //ResourceManager.RegisteReduceState(sumState);

            //var = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            //var_1 = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device);
            //varState = ComputeLib.RegisteTensorLazyReduce(1, input_dims.Length, new Shape(input_dims).RDimList, new Shape(reduce_dims).RDimList);
            //ResourceManager.RegisteReduceState(varState);

            
            //tmpO = new CudaPieceFloat(new Shape(input_dims).DefaultSize, behavior.Device);

            //ResourceManager.RegisteFloatVector(new Shape(input_dims).DefaultSize, "tmp0");
            //ResourceManager.RegisteFloatVector(new Shape(input_dims).DefaultSize, "tmp1");
            //ResourceManager.RegisteFloatVector(new Shape(reduce_dims).DefaultSize, "tmp2");

            //tmpODeriv = new CudaPieceFloat(new Shape(input_dims).DefaultSize, behavior.Device);            
            //w = new CudaPieceFloat(new Shape(input_dims).DefaultSize, behavior.Device);
            
            running_mean = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device); 
            running_mean.Init(0);

            running_var = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device); 
            running_var.Init(1);

            save_mean = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device); 
            save_mean.Init(0);

            save_var = new CudaPieceFloat(new Shape(reduce_dims).DefaultSize, behavior.Device); 
            save_var.Init(0);


            Output = new Tensor4DData(behavior.Device, Input.Shape); 
        }

        public override void Forward()
        {
            if(Behavior.RunMode == DNNRunMode.Train)
            {
                ComputeLib.CuDNNBatchNormalizationForwardTraining(Input.Output, input_dims[3].Default, input_dims[2].Default, input_dims[1].Default, input_dims[0].Default, Output.Output, 
                                                Scale.Output, Bias.Output, Momentum, 0, 1, running_mean, running_var, save_mean, save_var);
            }
            else
            {
                ComputeLib.CuDNNBatchNormalizationForwardInference(Input.Output, input_dims[3].Default, input_dims[2].Default, input_dims[1].Default, input_dims[0].Default, Output.Output, 
                                                Scale.Output, Bias.Output, 0, 1, running_mean, running_var);
            }
            ComputeLib.ReLU(Output.Output, 0, Output.Output, 0, Output.Length);    

        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            ComputeLib.DerivReLU(Output.Output, 0, Output.Deriv, 0, Output.Deriv, 0, Output.Length, 0, 1);
            ComputeLib.CuDNNBatchNormalizationBackward(Input.Output, input_dims[3].Default, input_dims[2].Default, input_dims[1].Default, input_dims[0].Default, Output.Deriv, 
                                                       Input.Deriv, Scale.Output, 1, 1, 1, 1, save_mean, save_var, Scale.Deriv, Bias.Deriv);
        }
    }
}
