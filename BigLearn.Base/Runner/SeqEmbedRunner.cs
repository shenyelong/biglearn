﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class AdvancedSeqEmbedRunner : StructRunner<EmbedStructure, SeqSparseBatchData>
    {
        public new SeqDenseRecursiveData Output { get { return (SeqDenseRecursiveData)base.Output; } set { base.Output = value; } }

        bool IsReverseOrder { get; set; }
        public AdvancedSeqEmbedRunner(EmbedStructure model, SeqSparseBatchData data, bool isReverseOrder, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            IsReverseOrder = isReverseOrder;
            Output = new SeqDenseRecursiveData(Input.SampleIdx, Input.SentMargin, Input.MAX_BATCHSIZE, Input.MAX_SENTSIZE, Model.Dim, behavior.Device);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            Output.SentSize = Input.SentSize;

            if (IsReverseOrder) { Output.RecurrentInfo.InitReverseAndTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize); }
            else { Output.RecurrentInfo.InitTransposeInfo(Input.SampleIdx, Input.BatchSize, Input.SentSize); }

            /*Embeding Input -> Output*/
            ComputeLib.SparseSgemmMask(Input.SequenceIdx, Input.FeaIdx, Input.FeaValue, Model.Embedding, 0, Output.SentOutput, 0, Input.SentSize,
                        Input.FEATURE_DIM, Model.Dim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Output.MapForward, 0, 0, 1, false, false);

            //Output.SentOutput.SyncToCPU();
        }

        public override void Backward(bool cleanDeriv) //override void Update()
        {
            //if(!IsDelegateOptimizer) Model.EmbeddingOptimizer.BeforeGradient();

            ComputeLib.SparseSgemmMask(Input.SequenceIdx, Input.FeaIdx, Input.FeaValue,
                                       Output.SentDeriv, 0,
                                       Model.EmbeddingGrad, 0,
                                       Input.SentSize, Input.FEATURE_DIM, Model.Dim,
                                       CudaPieceInt.Empty, 0, Output.MapForward, 0, CudaPieceInt.Empty, 0,
                                       1, 1, true, false);

            //if (!IsDelegateOptimizer) Model.EmbeddingOptimizer.AfterGradient();
        }
    }


    public class AdvancedEmbedRunner : StructRunner<EmbedStructure, GeneralBatchInputData>
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public AdvancedEmbedRunner(EmbedStructure model, GeneralBatchInputData data, RunnerBehavior behavior)
            : base(model, data, behavior)
        {
            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Dim, Behavior.RunMode, Behavior.Device);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            /*Embeding Input -> Output*/
            ComputeLib.SparseSgemmMask(Input.BatchIdx, Input.FeatureIdx, Input.FeatureValue, Model.Embedding, 0, Output.Output.Data, 0, Input.BatchSize,
                        Model.VocabSize, Model.Dim, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, 0, 1, false, false);
        }

        public override void Update()
        {

            ComputeLib.SparseSgemmMask(Input.BatchIdx, Input.FeatureIdx, Input.FeatureValue,
                                       Output.Deriv.Data, 0,
                                       Model.EmbeddingGrad, 0,
                                       Input.BatchSize, Model.VocabSize, Model.Dim,
                                       CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                       1, 1, true, false);

        }
    }
}
