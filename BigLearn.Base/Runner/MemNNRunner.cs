﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public class MemoryRunner : StructRunner //<MemoryStructure, BatchData>
    // {
    //     public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

    //     CudaPieceFloat AddressScore;
    //     CudaPieceFloat AddressProb;
    //     CudaPieceFloat TmpAddressProbDeriv_1;
    //     CudaPieceFloat TmpAddressProbDeriv_2;

    //     CudaPieceFloat AddressProbDeriv;
    //     CudaPieceFloat AddressScoreDeriv;

    //     /// <summary>
    //     /// 0 : GeneralBatchInputData; 1 : HiddenBatchData;
    //     /// </summary>
    //     int InputType { get; set; }
    //     int InputBatchSize { get { return InputType == 0 ? ((GeneralBatchInputData)Input).BatchSize : ((HiddenBatchData)Input).BatchSize; } }
    //     CudaPieceFloat InputData { get { return InputType == 0 ? ((GeneralBatchInputData)Input).DenseFeatureData : ((HiddenBatchData)Input).Output.Data; } }
    //     CudaPieceFloat InputDeriv { get { return InputType == 0 ? null : ((HiddenBatchData)Input).Deriv.Data; } }

    //     public MemoryRunner(MemoryStructure model, HiddenBatchData input, RunnerBehavior behavior)
    //         : base(model, input, behavior)
    //     {
    //         Output = new HiddenBatchData(input.MAX_BATCHSIZE, Model.OutputDim, Behavior.RunMode, Behavior.Device);
    //         AddressScore = new CudaPieceFloat(input.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);
    //         AddressProb = new CudaPieceFloat(input.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);
    //         AddressProbDeriv = new CudaPieceFloat(input.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);
    //         TmpAddressProbDeriv_1 = new CudaPieceFloat(input.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //         TmpAddressProbDeriv_2 = new CudaPieceFloat(input.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);
    //         AddressScoreDeriv = new CudaPieceFloat(input.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);

    //         InputType = 1;
    //     }

    //     public MemoryRunner(MemoryStructure model, GeneralBatchInputData input, RunnerBehavior behavior)
    //         : base(model, input, behavior)
    //     {
    //         Output = new HiddenBatchData(input.Stat.MAX_BATCHSIZE, Model.OutputDim, Behavior.RunMode, Behavior.Device);
    //         AddressScore = new CudaPieceFloat(input.Stat.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);
    //         AddressProb = new CudaPieceFloat(input.Stat.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);
    //         AddressProbDeriv = new CudaPieceFloat(input.Stat.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);
    //         TmpAddressProbDeriv_1 = new CudaPieceFloat(input.Stat.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //         TmpAddressProbDeriv_2 = new CudaPieceFloat(input.Stat.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);
    //         AddressScoreDeriv = new CudaPieceFloat(input.Stat.MAX_BATCHSIZE * Model.MemoryLen, true, Behavior.Device == DeviceType.GPU);

    //         InputType = 0;
    //     }

    //     public override void Forward()
    //     {
    //         Output.BatchSize = InputBatchSize;
    //         MathOperatorManager.GlobalInstance.MatrixMultiplicationTranspose(InputData, Model.Address, AddressScore, InputBatchSize, Model.InputDim, Model.MemoryLen, 0);
    //         MathOperatorManager.GlobalInstance.SoftMax(AddressScore, AddressProb, Model.MemoryLen, InputBatchSize, 1);
    //         MathOperatorManager.GlobalInstance.Matrix_Multipy(AddressProb, Model.Content, Output.Output.Data, InputBatchSize, Model.MemoryLen, Model.OutputDim, 0);
    //     }

    //     public override void CleanDeriv()
    //     {
    //         MathOperatorManager.GlobalInstance.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
    //     }

    //     public override void Backward(bool cleanDeriv)
    //     {
    //         MathOperatorManager.GlobalInstance.MatrixMultiplicationTranspose(Output.Deriv.Data, Model.Content, AddressProbDeriv, Output.BatchSize, Model.OutputDim, Model.MemoryLen, 0);

    //         //-------------> Deriv Softmax.
    //         MathOperatorManager.GlobalInstance.InnerProduct_Similarity(AddressProbDeriv, AddressProb, TmpAddressProbDeriv_1, Output.BatchSize, Model.MemoryLen);
    //         Cudalib.MatrixTran_AddVector(AddressProbDeriv.CudaPtr, TmpAddressProbDeriv_1.CudaPtr, TmpAddressProbDeriv_2.CudaPtr, Output.BatchSize, Model.MemoryLen, -1);
    //         MathOperatorManager.GlobalInstance.ElementwiseProduct(AddressProb, TmpAddressProbDeriv_2, AddressScoreDeriv, Output.BatchSize, Model.MemoryLen, 0);

    //         // Get Derivation of Input Layer.
    //         if (InputDeriv != null) MathOperatorManager.GlobalInstance.Matrix_Multipy(AddressScoreDeriv, Model.Address, InputDeriv, InputBatchSize, Model.MemoryLen, Model.InputDim, 0);
    //     }

    //     public override void Update()
    //     {
    //         Model.ContentOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.MatrixMultiplicationLeftTranspose(AddressProb, Output.Deriv.Data, Model.ContentOptimizer.Gradient, Output.BatchSize, Model.MemoryLen, Model.OutputDim, Model.ContentOptimizer.GradientStep);
    //         Model.ContentOptimizer.AfterGradient();

    //         Model.AddressOptimizer.BeforeGradient();
    //         MathOperatorManager.GlobalInstance.MatrixMultiplicationLeftTranspose(AddressScoreDeriv, InputData, Model.AddressOptimizer.Gradient, InputBatchSize, Model.MemoryLen, Model.InputDim, Model.AddressOptimizer.GradientStep);
    //         Model.AddressOptimizer.AfterGradient();
    //     }
    // }

    
    // public class ExternalMemoryRunner : StructRunner<Structure, BatchData>
    // {
    //     HiddenBatchData MemoryAddress;
    //     HiddenBatchData MemoryContent;
    //     HiddenBatchData Query;
    //     MemoryQAHelpData MemoryHelp;

    //     HiddenBatchData Address;
        
    //     public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        
    //     public ExternalMemoryRunner(HiddenBatchData query, MemoryQAHelpData memoryHelp, 
    //         HiddenBatchData memoryAddress, HiddenBatchData memoryContent,  RunnerBehavior behavior)
    //         : base(Structure.Empty, behavior)
    //     {
    //         MemoryAddress = memoryAddress; 
    //         MemoryContent = memoryContent;
    //         Query = query;

    //         MemoryHelp = memoryHelp;
    //         Address = new HiddenBatchData(memoryHelp.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);

    //         Output = new HiddenBatchData(memoryHelp.Stat.MAX_SRC_BATCHSIZE, MemoryContent.Dim, Behavior.RunMode, Behavior.Device);
    //     }

    //     public override void Forward()
    //     {
    //         Output.BatchSize = MemoryHelp.BatchSize;

    //         ComputeLib.Inner_Product_Matching(Query.Output.Data, MemoryAddress.Output.Data, Address.Output.Data, MemoryHelp.MatchSrc, MemoryHelp.MatchMem,
    //             Query.BatchSize, MemoryAddress.BatchSize, MemoryHelp.MemorySize, MemoryAddress.Dim, Util.GPUEpsilon);

    //         //Address.Output.Data.SyncToCPU();
    //         ComputeLib.SparseSoftmax(MemoryHelp.BatchMemIdx, Address.Output.Data, Address.Output.Data, 1, MemoryHelp.BatchSize);
            
    //         //Address.Output.Data.SyncToCPU();
    //         ComputeLib.ColumnWiseSumMask(MemoryContent.Output.Data, 0, MemoryHelp.MatchMem, 0,
    //             Address.Output.Data, MemoryHelp.BatchMemIdx, MemoryHelp.BatchSize,
    //             Output.Output.Data, 0, CudaPieceInt.Empty, 0,
    //             MemoryContent.BatchSize, MemoryContent.Dim, 0, 1);

    //         //Output.Output.Data.SyncToCPU();
    //     }

    //     public override void CleanDeriv()
    //     {
    //         ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
    //     }

    //     public override void Backward(bool cleanDeriv)
    //     {
    //         // outputDeriv -> memoryContentDeriv.
    //         if (MemoryHelp.MemorySize > MemoryContent.BatchSize)
    //         {
    //             Cudalib.Accurate_Scale_Matrix(Output.Deriv.Data.CudaPtr, MemoryHelp.MatchOut.CudaPtr,
    //                                         MemoryContent.Deriv.Data.CudaPtr, MemoryHelp.MatchMem.CudaPtr,
    //                                         MemoryContent.Dim, MemoryHelp.MemorySize, Address.Output.Data.CudaPtr);
    //         }
    //         else
    //         {
    //             ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, MemoryHelp.MatchOut, 0,
    //                                         MemoryContent.Deriv.Data, 0, MemoryHelp.MatchMem, 0,
    //                                         MemoryContent.Dim, MemoryContent.BatchSize, Address.Output.Data, 1);
    //         }
    //         // outputDeriv -> AddressDeriv.
    //         ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, MemoryContent.Output.Data, 0, Address.Deriv.Data, 0,
    //             MemoryHelp.MatchOut, MemoryHelp.MatchMem, Output.BatchSize, MemoryContent.BatchSize, MemoryHelp.MemorySize,
    //             MemoryContent.Dim, Util.GPUEpsilon);

    //         // AddressDeriv = AddressDeriv * SoftMax(Address)
    //         ComputeLib.DerivSparseMultiClassSoftmax(MemoryHelp.BatchMemIdx, Address.Output.Data, Address.Deriv.Data, Address.Deriv.Data, 1, MemoryHelp.BatchSize);

    //         // AddressDeriv -> MemoryAddressDeriv
    //         if (MemoryHelp.MemorySize > MemoryAddress.BatchSize)
    //         {
    //             Cudalib.Accurate_Scale_Matrix(Query.Output.Data.CudaPtr, MemoryHelp.MatchSrc.CudaPtr,
    //                                     MemoryAddress.Deriv.Data.CudaPtr, MemoryHelp.MatchMem.CudaPtr,
    //                                     MemoryAddress.Dim, MemoryHelp.MemorySize, Address.Deriv.Data.CudaPtr);
    //         }
    //         else
    //         { 
    //             ComputeLib.Scale_MatrixMask(Query.Output.Data, 0, MemoryHelp.MatchSrc, 0,
    //                                     MemoryAddress.Deriv.Data, 0, MemoryHelp.MatchMem, 0,
    //                                     MemoryAddress.Dim, MemoryAddress.BatchSize, Address.Deriv.Data, 1);
    //         }

    //         // AddressDeriv -> QueryDeriv
    //         Cudalib.Accurate_Scale_Matrix(MemoryAddress.Output.Data.CudaPtr, MemoryHelp.MatchMem.CudaPtr,
    //                                     Query.Deriv.Data.CudaPtr, MemoryHelp.MatchSrc.CudaPtr,
    //                                     MemoryAddress.Dim, MemoryHelp.MemorySize, Address.Deriv.Data.CudaPtr);

    //         //ComputeLib.Scale_MatrixMask(MemoryAddress.Output.Data, 0, MemoryHelp.MatchMem, 0,
    //         //                            Query.Deriv.Data, 0, MemoryHelp.MatchSrc, 0,
    //         //                            MemoryAddress.Dim, MemoryAddress.BatchSize, Address.Deriv.Data, 1);
    //     }
        
    // }

}
