using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LookupEmbedRunner : StructRunner
    {
        public CudaPieceInt CudaInts { get; set; }
            
        public int MaxBatchSize = 0;

        public List<int> InputList = null;

        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        //EmbedStructure Embed { get; set; }

        public CudaPieceFloat EmbedValue { get; set; }
        public CudaPieceFloat EmbedGrad { get; set; }
        public int EmbedVocabSize { get; set; }
        public int EmbedDim { get; set; }

        int Iteration = 0;
        int LookupSize = 0;

        public LookupEmbedRunner(List<int> input, int maxBatchSize, EmbedStructure embed, RunnerBehavior behavior) 
            : this(new CudaPieceInt(maxBatchSize, behavior.Device), maxBatchSize, embed, behavior)
        {
            InputList = input;
        }

        public LookupEmbedRunner(CudaPieceInt input, int maxBatchSize, EmbedStructure embed, RunnerBehavior behavior) 
            : this(input, maxBatchSize, embed.Embedding, embed.EmbeddingGrad, embed.VocabSize, embed.Dim, behavior)
        { }

        public LookupEmbedRunner(CudaPieceInt input, int maxBatchSize, CudaPieceFloat embed, CudaPieceFloat embedGrad, int vocabSize, int embedDim, CudaPieceFloat tgt, CudaPieceFloat tgtGrad, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            CudaInts = input;
            
            EmbedValue = embed;
            EmbedGrad = embedGrad;
            EmbedVocabSize = vocabSize;
            EmbedDim = embedDim;

            //DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] args)
            Output = new HiddenBatchData(behavior.Device, tgt, tgtGrad, new IntArgument("dim", EmbedDim), new IntArgument("batchSize", maxBatchSize));
                        
            MaxBatchSize = maxBatchSize;
        
            Behavior.Resource.RegisteOneVectorFloat(maxBatchSize);
            Behavior.Resource.RegisteIncVectorInt(maxBatchSize + 1);

            Behavior.Resource.RegisteIntVector(EmbedVocabSize + 1, "tmp_int_1");
            Behavior.Resource.RegisteIntVector(maxBatchSize,"tmp_int_2");
        }

        public LookupEmbedRunner(CudaPieceInt input, int maxBatchSize, CudaPieceFloat embed, CudaPieceFloat embedGrad, int vocabSize, int embedDim, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            CudaInts = input;
            
            EmbedValue = embed;
            EmbedGrad = embedGrad;
            EmbedVocabSize = vocabSize;
            EmbedDim = embedDim;

            Output = new HiddenBatchData(maxBatchSize, EmbedDim, DNNRunMode.Train, behavior.Device);
            MaxBatchSize = maxBatchSize;
        
            Behavior.Resource.RegisteOneVectorFloat(maxBatchSize);
            Behavior.Resource.RegisteIncVectorInt(maxBatchSize + 1);

            Behavior.Resource.RegisteIntVector(EmbedVocabSize + 1, "tmp_int_1");
            Behavior.Resource.RegisteIntVector(maxBatchSize,"tmp_int_2");
        }

        public override void Forward()
        {
            //var perf_lookup = PerfCounter.Manager.Instance["lookup_forward"].Begin();
            if(InputList != null)
            {
                if(InputList.Count > CudaInts.Size)
                {
                    throw new Exception(string.Format("input idx is larger than cuda ints {0}, {1}", InputList.Count, CudaInts.Size));
                }

                CudaInts.EffectiveSize = InputList.Count; 
                
                //var perf_lookup_prep = PerfCounter.Manager.Instance["lookup_prep_forward"].Begin();
                InputList.CopyTo(CudaInts.MemPtr);

                //for(int i=0; i < InputList.Count; i++)
                //{
                //    CudaInts.MemPtr[i] = InputList[i];
                //}
                //PerfCounter.Manager.Instance["lookup_prep_forward"].TakeCount(perf_lookup_prep);

                //var perf_lookup_sync = PerfCounter.Manager.Instance["lookup_sync_forward"].Begin();
                CudaInts.SyncFromCPU();
                //PerfCounter.Manager.Instance["lookup_sync_forward"].TakeCount(perf_lookup_sync);
            }


            Output.BatchSize = CudaInts.EffectiveSize;
            if(Output.BatchSize == 0) return;

            ComputeLib.LookupForward(CudaPieceInt.Empty, CudaInts, CudaInts.EffectiveSize, EmbedValue, Output.Output.Data, EmbedVocabSize,  EmbedDim);
            //PerfCounter.Manager.Instance["lookup_forward"].TakeCount(perf_lookup);

            Iteration += 1;
            LookupSize += Output.BatchSize;
        }

        public override void CleanDeriv()
        {
            if(Output.BatchSize == 0) return;
            ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
        }

        public override void Backward(bool isClearDeriv)
        {
            if(Output.BatchSize == 0) return;
            if(EmbedGrad != null )
            {
                //var perf_lookup = PerfCounter.Manager.Instance["lookup_update"].Begin();

                ComputeLib.CuCsr2Csc(Behavior.Resource.Inc, CudaInts, Behavior.Resource.One,
                                     Behavior.Resource.Mem["tmp_int_1"], Behavior.Resource.Mem["tmp_int_2"], Behavior.Resource.One,
                                     CudaInts.EffectiveSize, CudaInts.EffectiveSize, EmbedVocabSize);

                ComputeLib.CuSparseSgemm(Output.Deriv.Data, Behavior.Resource.Mem["tmp_int_1"], Behavior.Resource.Mem["tmp_int_2"], Behavior.Resource.One, 
                        CudaInts.EffectiveSize, EmbedGrad, Output.BatchSize, EmbedDim, EmbedVocabSize, 1, 1);

                //PerfCounter.Manager.Instance["lookup_update"].TakeCount(perf_lookup);
            }
        }
        public override void Init()
        {
            Iteration = 0;
            LookupSize = 0;
        }
        public override void Complete()
        {
            //Logger.WriteLog("LookupEmbed Times {0}, Avg Lookup Size {1}", Iteration, LookupSize * 1.0f / Iteration);
        }
    }
}
