using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorArgmaxKRunner : StructRunner
    {
        public new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat Indices_f { get; set; }

        // local indices.
        public CudaPieceInt Indices { get; set; }
        
        // global indices.
        public CudaPieceInt Indices_v2 { get; set; }

        int TopK { get; set; }
        public TensorArgmaxKRunner(NdArrayData input, int topK, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;
            
            IntArgument[] out_dims = new IntArgument[input.Shape.Length];
            for(int i = 0; i < out_dims.Length; i++)
            {
                if(i == 0) out_dims[i] = new IntArgument("topK", topK);
                else out_dims[i] = input.Shape[i];
            }

            TopK = topK;

            Output = new NdArrayData(out_dims, behavior.Device);
            Indices_f = new CudaPieceFloat(new Shape(out_dims).DefaultSize, behavior.Device);
            Indices = new CudaPieceInt(new Shape(out_dims).DefaultSize, behavior.Device);
            Indices_v2 = new CudaPieceInt(new Shape(out_dims).DefaultSize, behavior.Device);   
        }

        public override void Forward()
        {
            int dim = Input.Dimensions[0].Default;
            int batchSize = Input.Length / dim;
            ComputeLib.KLargestValueBatch(Input.Output, batchSize, dim, TopK, 1, Output.Output, Indices_f);
            
            Indices_f.SyncToCPU();
            for(int b = 0; b < batchSize; b++)
            {
                for(int t = 0; t < TopK; t++)
                {
                    int idx = b * TopK + t;

                    Indices[idx] = (int)Indices_f[idx];
                    Indices_v2[idx] = Indices[idx] + b * dim;
                }
            }
            Indices.SyncFromCPU();
            Indices_v2.SyncFromCPU();
        }

        public override void CleanDeriv()
        {
            if (Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if(Input.Deriv != null)
            {
                ComputeLib.SparseVectorAdd(Indices_v2, Output.Deriv, Input.Deriv, 1, Output.Length);
                //inDims.Init(new Shape(reduce_dims).DimList);
                //outDims.Init(new Shape(in_dims).DimIndexes);
                //ComputeLib.NDArrayMaxBackward(Output.Deriv, Indices, inDims, Input.Deriv, outDims, MaxDim, in_dims.Length, Output.Length, 1.0f);
            }
        }

    }
}
