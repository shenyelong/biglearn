using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SeqInnerProductRunner : StructRunner<Structure, BatchData>
    {
        public new VectorData Output { get { return (VectorData)base.Output; } set { base.Output = value; } }

        MatrixData InputA { get; set; }
        SeqMatrixData InputB { get; set; }

        public SeqInnerProductRunner(MatrixData inputA, SeqMatrixData inputB, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = new VectorData(InputB.MaxRow, Behavior.Device);

            if(InputA.Column != InputB.Column)
            {
                Logger.WriteLog("SeqInnerProductRunner InputA Column {0} doesnt match Input B Column {1}", InputA.Column, InputB.Column);
                throw new Exception(string.Format("SeqInnerProductRunner InputA Column {0} doesnt match Input B Column {1}", InputA.Column, InputB.Column));
            }
        }

        public override void Forward()
        {
            iteration++;
            Output.Length = InputB.Row;
            //var perf_mip = PerfCounter.Manager.Instance["seqmatrix_inner_product"].Begin();

            ComputeLib.Inner_Product_Matching(InputA.Output, InputB.Output, Output.Output,
                                              InputB.SegmentMargin, CudaPieceInt.Empty,
                                              InputA.Row, InputB.Row, Output.Length, InputA.Column, 0);
            //PerfCounter.Manager.Instance["seqmatrix_inner_product"].TakeCount(perf_mip);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            ComputeLib.Scale_MatrixMask(InputA.Output, 0, InputB.SegmentMargin, 0,
                                        InputB.Deriv, 0, CudaPieceInt.Empty, 0,
                                        InputA.Column, Output.Length, Output.Deriv, 1);

            ComputeLib.ColumnWiseSumMask(InputB.Output, 0, CudaPieceInt.Empty, 0,
                                         Output.Deriv, InputB.SegmentIdx, InputB.Segment, InputA.Deriv, 0, CudaPieceInt.Empty, 0,
                                         InputB.Row, InputB.Column, 1, 1);
        }
    }
}
