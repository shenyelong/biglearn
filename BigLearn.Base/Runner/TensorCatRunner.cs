using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorCatRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }

        public List<NdArrayData> InputList { get; set; }

        public int Cat { get; set; }

        /// <summary>
        /// O = A * B;
        /// </summary>
        /// <param name="inputA"></param>
        /// <param name="inputB"></param>
        /// <param name="behavior"></param>
        public TensorCatRunner(List<NdArrayData> inputList, int cat, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            InputList = inputList;
            Cat = cat;
            
            int catdim = 0;
            for(int i = 0; i < inputList.Count; i++)
            {
                catdim += inputList[i].Shape[cat].Default;
            }

            IntArgument[] shape = new IntArgument[inputList[0].Shape.Length];
            for(int i = 0; i < inputList[0].Shape.Length; i++)
            {
                if(i == cat)
                    shape[i] = new IntArgument("cat_dim", catdim); 
                else
                    shape[i] = inputList[0].Shape[i];
                
                //Logger.WriteLog("output shape {0}: {1}", i, shape[i].Default);
            }
            Output = new NdArrayData(behavior.Device, shape);
            //Logger.WriteLog("output size {0}", Output.Length);
        }

        public override void Forward()
        {
            //Logger.WriteLog("concate runner start ...concate :{0}", InputList.Count);

            //Shape tshape = new Shape(Output.Shape);
            //Logger.WriteLog("tshape {0}, {1}, {2}", tshape.DimNum, tshape.DimList[0], tshape.DimList[1]);

            int[] out_dim_offsets = new Shape(Output.Shape).DimOffsets;
            //Logger.WriteLog("out_dim_offsets {0}, {1}", out_dim_offsets[0], out_dim_offsets[1]);

            int out_col = out_dim_offsets[Cat];
            int out_row = out_dim_offsets.Last() / out_col;

            //Logger.WriteLog("output row {0}, output col {1}", out_row, out_col);
            int offset_col = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                int[] dim_offsets = new Shape(InputList[i].Shape).DimOffsets;

                int col = dim_offsets[Cat];
                int row = dim_offsets.Last() / col;
                if (row != out_row) { throw new Exception("Row doesn't matach in TensorCatRunner"); }
                
                ComputeLib.Matrix_AdditionEx(InputList[i].Output, 0, col,
                                             InputList[i].Output, 0, col,
                                             Output.Output, offset_col, out_col,
                                             col, row, 1, 0, 0);
                
                offset_col += col;
            }
            //Output.Output.Print("cat embedding", 100, true);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            int[] out_dim_offsets = new Shape(Output.Shape).DimOffsets;
            int out_col = out_dim_offsets[Cat];
            int out_row = out_dim_offsets.Last() / out_col;

            int offset_col = 0;
            for (int i = 0; i < InputList.Count; i++)
            {
                int[] dim_offsets = new Shape(InputList[i].Shape).DimOffsets;

                int col = dim_offsets[Cat];
                int row = dim_offsets.Last() / col;
                if (row != out_row) { throw new Exception("Row doesn't matach in TensorCatRunner"); }
                
                ComputeLib.Matrix_AdditionEx(Output.Deriv, offset_col, out_col,
                                             InputList[i].Deriv, 0, col,
                                             InputList[i].Deriv, 0, col,
                                             col, row, 1, 0, 1);
                
                offset_col += col;
            }
        }
    }
}
