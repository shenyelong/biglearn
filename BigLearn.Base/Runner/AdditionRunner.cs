﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class AdditionRunner : StructRunner<Structure, BatchData>
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        HiddenBatchData InputA { get; set; }
        HiddenBatchData InputB { get; set; }

        bool IsProcess = false;
        float Alpha = 0;

        public AdditionRunner(HiddenBatchData inputA, HiddenBatchData inputB, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            Output = new HiddenBatchData(InputA.MAX_BATCHSIZE, InputA.Dim, Behavior.RunMode, Behavior.Device);
        }

        public AdditionRunner(MatrixData inputA, MatrixData inputB, MatrixData output, bool overwrite, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = new HiddenBatchData(inputA);
            InputB = inputB == null ? null : new HiddenBatchData(inputB);
            Output = new HiddenBatchData(output);
            IsProcess = true;
            Alpha = overwrite ? 0 : 1;
        }

        public override void Forward()
        {
            Output.BatchSize = InputA.BatchSize;
            if (InputB == null)
            {
                ComputeLib.Matrix_AdditionEx(InputA.Output.Data, 0, InputA.Dim,
                                             Output.Output.Data, 0, Output.Dim,
                                             Output.Output.Data, 0, Output.Dim,
                                             Output.Dim, Output.BatchSize, 1, 0, IsProcess ? Alpha : 0);
            }
            else
            {
                ComputeLib.Matrix_AdditionEx(InputA.Output.Data, 0, InputA.Dim,
                                             InputB.Output.Data, 0, InputB.Dim,
                                             Output.Output.Data, 0, Output.Dim,
                                             Output.Dim, Output.BatchSize, 1, 1, IsProcess ? Alpha : 0);
            }
        }

        public override void CleanDeriv()
        {
            if(!IsProcess) ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (InputB != null)
            {
                ComputeLib.Matrix_AdditionEx(Output.Deriv.Data, 0, Output.Dim,
                                             InputB.Deriv.Data, 0, InputB.Dim,
                                             InputB.Deriv.Data, 0, InputB.Dim,
                                             Output.Dim, Output.BatchSize, 1, 0, 1);
            }
            ComputeLib.Matrix_AdditionEx(Output.Deriv.Data, 0, Output.Dim,
                                         InputA.Deriv.Data, 0, InputA.Dim,
                                         InputA.Deriv.Data, 0, InputA.Dim,
                                         Output.Dim, Output.BatchSize, 1, 0, 1);
        }
    }
    
    public class AdditionExRunner : StructRunner<Structure, BatchData>
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        HiddenBatchData InputA { get; set; }
        HiddenBatchData InputB { get; set; }
        BiMatchBatchData MatchData { get; set; }
        float Alpha { get; set; }
        float Beta { get; set; }
        public AdditionExRunner(HiddenBatchData inputA, HiddenBatchData inputB, BiMatchBatchData match, float alpha, float beta, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            InputB = inputB;
            MatchData = match;

            Alpha = alpha;
            Beta = beta;

            if(InputA.Dim != InputB.Dim)
            {
                throw new Exception(string.Format("Addition Runner InputA dim doesn't match InputB dim {0}, {1}", InputA.Dim, InputB.Dim));
            }
            int maxBatchSize = MatchData == null ? InputA.MAX_BATCHSIZE : MatchData.MAX_MATCH_BATCHSIZE;
            Output = new HiddenBatchData(maxBatchSize, InputA.Dim, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = MatchData == null ? InputA.BatchSize : MatchData.MatchSize;
            if(Output.BatchSize == 0) return;
            ComputeLib.Matrix_AdditionExMask(InputA.Output.Data, MatchData == null ? CudaPieceInt.Empty : MatchData.SrcIdx, InputA.Dim,
                                             InputB.Output.Data, MatchData == null ? CudaPieceInt.Empty : MatchData.TgtIdx, InputB.Dim,
                                             Output.Output.Data, CudaPieceInt.Empty, Output.Dim,
                                             Output.Dim, Output.BatchSize, Alpha, Beta, 0);
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) return;

            if(MatchData != null)
            {
                ComputeLib.ColumnWiseSumMaskEx(Output.Deriv.Data, 0, MatchData.Src2MatchElement, 0, CudaPieceFloat.Empty, Output.Dim,
                                        MatchData.Src2MatchIdx, MatchData.SrcSize, InputA.Deriv.Data, 0, MatchData.Src2Idx, 0, InputA.Dim,
                                        MatchData.MatchSize, InputA.Dim, 1, Alpha);
                
                ComputeLib.ColumnWiseSumMaskEx(Output.Deriv.Data, 0, MatchData.Tgt2MatchElement, 0, CudaPieceFloat.Empty, Output.Dim,
                                        MatchData.Tgt2MatchIdx, MatchData.TgtSize, InputB.Deriv.Data, 0, MatchData.Tgt2Idx, 0, InputB.Dim,
                                        MatchData.MatchSize, InputB.Dim, 1, Beta);
            }
            else
            {
                ComputeLib.Matrix_AdditionEx(Output.Deriv.Data, 0, Output.Dim,
                                             InputB.Deriv.Data, 0, InputB.Dim,
                                             InputB.Deriv.Data, 0, InputB.Dim,
                                             Output.Dim, Output.BatchSize, Beta, 0, 1);
                
                ComputeLib.Matrix_AdditionEx(Output.Deriv.Data, 0, Output.Dim,
                                         InputA.Deriv.Data, 0, InputA.Dim,
                                         InputA.Deriv.Data, 0, InputA.Dim,
                                         Output.Dim, Output.BatchSize, Alpha, 0, 1);
            }
        }
    }

    /// <summary>
    /// Append Bias to Matrix
    /// </summary>
    public class BiasAdditionProcessor : StructRunner<Structure, BatchData>
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }

        MatrixData InputA { get; set; }
        VectorData Bias { get; set; }
        CudaPieceFloat BiasOne;

        public BiasAdditionProcessor(MatrixData inputA, VectorData bias, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputA = inputA;
            Bias = bias;
            Output = InputA;
            BiasOne = new CudaPieceFloat(InputA.MaxRow, behavior.Device);
            BiasOne.Init(1);
        }


        public override void Forward()
        {
            Output.Row = InputA.Row;
            if (Output.Row == 0) return;
            ComputeLib.Matrix_Add_Linear(InputA.Output, Bias.Output, InputA.Row, InputA.Column);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Row == 0) return;
            ComputeLib.Sgemv(Output.Deriv, BiasOne, InputA.Row, InputA.Column, Bias.Deriv, true, 1, 1);
        }
    }

    public class AdditionMatrixRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        List<HiddenBatchData> InputList = new List<HiddenBatchData>();

        public AdditionMatrixRunner(List<HiddenBatchData> dataList, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputList = dataList;

            int maxBatchSize = dataList[0].MAX_BATCHSIZE;
            int dim = dataList[0].Dim;
            for (int i = 1; i < dataList.Count; i++)
            {
                if (dataList[i].MAX_BATCHSIZE != maxBatchSize) { throw new Exception("Maximum BatchSize in EnsembleMatrixRunner Doesn't mmatch!"); }
                if (dim != dataList[i].Dim) { throw new Exception("Data Dimension in EnsembleMatrixRunner Doesn't mmatch!"); }
            }
            Output = new HiddenBatchData(maxBatchSize, dim, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = InputList[0].BatchSize;
            for (int i = 0; i < InputList.Count; i++)
            {
                if (InputList[i].BatchSize != Output.BatchSize)
                {
                    throw new Exception(string.Format("BatchSize doesn't matach in EnsembleMatrixRunner {0], {1}", InputList[i].BatchSize, Output.BatchSize));
                }
                ComputeLib.Add_Vector(Output.Output.Data, InputList[i].Output.Data,
                                InputList[i].Dim * InputList[i].BatchSize, i == 0 ? 0 : 1, 1);
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            for (int i = 0; i < InputList.Count; i++)
            {
                ComputeLib.Add_Vector(InputList[i].Deriv.Data, Output.Deriv.Data, InputList[i].Dim * InputList[i].BatchSize, 1, 1);
            }
        }
    }


    public class WAdditionMatrixRunner : StructRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        List<HiddenBatchData> InputList = new List<HiddenBatchData>();
        List<HiddenBatchData> InputWeight = new List<HiddenBatchData>();

        public WAdditionMatrixRunner(List<HiddenBatchData> dataList, List<HiddenBatchData> weight, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            InputList = dataList;
            InputWeight = weight;

            int maxBatchSize = dataList[0].MAX_BATCHSIZE;
            int dim = dataList[0].Dim;
            for (int i = 1; i < dataList.Count; i++)
            {
                if (dataList[i].MAX_BATCHSIZE != maxBatchSize) { throw new Exception("Maximum BatchSize in EnsembleMatrixRunner Doesn't mmatch!"); }
                if (dim != dataList[i].Dim) { throw new Exception("Data Dimension in EnsembleMatrixRunner Doesn't mmatch!"); }
            }
            Output = new HiddenBatchData(maxBatchSize, dim, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            Output.BatchSize = InputList[0].BatchSize;
            for (int i = 0; i < InputList.Count; i++)
            {
                if (InputList[i].BatchSize != Output.BatchSize)
                {
                    throw new Exception(string.Format("BatchSize doesn't matach in EnsembleMatrixRunner {0], {1}", InputList[i].BatchSize, Output.BatchSize));
                }
                ComputeLib.Scale_MatrixMask(InputList[i].Output.Data, 0, CudaPieceInt.Empty, 0, Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                                            InputList[i].Dim, InputList[i].BatchSize, InputWeight[i].Output.Data, i == 0 ? 0 : 1);
            }
        }

        public override void CleanDeriv()
        {
            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
        }

        public override void Backward(bool cleanDeriv)
        {
            for (int i = 0; i < InputList.Count; i++)
            {
                ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                            InputList[i].Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                            InputList[i].Dim, InputList[i].BatchSize, InputWeight[i].Output.Data, 1);
                ComputeLib.InnerProduct_Similarity(Output.Deriv.Data, InputList[i].Output.Data, InputWeight[i].Deriv.Data, InputList[i].BatchSize, InputList[i].Dim);
                                                
            }
        }
    }
}
