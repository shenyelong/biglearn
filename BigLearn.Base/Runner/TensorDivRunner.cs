using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorDivRunner : StructRunner
    {
        public new NdArrayData Output { get { return (NdArrayData)base.Output; } set { base.Output = value; } }
        new NdArrayData Input { get { return (NdArrayData)base.Input; } set { base.Input = value; } }

        public TensorDivRunner(NdArrayData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Input = input;

            Output = new NdArrayData(behavior.Device, Input.Shape); 
            
            Behavior.Resource.RegisteFloatVector(new Shape(Input.Shape).DefaultSize, "tmp1");
        }
        
        public override void Forward()
        {
            ComputeLib.Reciprocal(Input.Output, Output.Output, 0, 1, Input.Length);
        }

        public override void CleanDeriv()
        {
            if(Output.Length == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.Length == 0) return;

            if(Input.Deriv != null && Input.Deriv != CudaPieceFloat.Empty)
            {
                
                ComputeLib.TensorOp(1, Input.Shape.Length, new Shape(Input.Shape).RDimList, Output.Output,
                                                          new Shape(Input.Shape).RDimList, Output.Output, 
                                                          new Shape(Input.Shape).RDimList, Behavior.Resource.MemF["tmp1"],
                                                          1, 1, 0);

                ComputeLib.TensorOp(1, Input.Shape.Length, new Shape(Input.Shape).RDimList, Behavior.Resource.MemF["tmp1"],
                                                          new Shape(Input.Shape).RDimList, Output.Deriv, 
                                                          new Shape(Input.Shape).RDimList, Input.Deriv,
                                                          -1, 1, 1);
            }
        }
    }
}
