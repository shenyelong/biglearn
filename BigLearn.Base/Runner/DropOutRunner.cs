﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class DropOutRunner : StructRunner<Structure, MatrixData> 
    {
        public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }
        public new MatrixData Input { get { return (MatrixData)base.Input; } set { base.Input = value; } }

        float DropOutRate { get; set; }
        public CudaPieceFloat DropoutMaskFloat = null;
        Random mRandom = new Random(ParameterSetting.Random.Next());
        bool isProcess = false;

        public DropOutRunner(MatrixData input, float dropOutRate, RunnerBehavior behavior)
            : base(Structure.Empty, input, behavior)
        {
            DropOutRate = dropOutRate;
            DropoutMaskFloat = new CudaPieceFloat(Input.Column * Input.MaxRow, Behavior.Device);
            Output = new MatrixData(Input.Column, Input.MaxRow, Behavior.Device);
        }

        public DropOutRunner(MatrixData input, float dropOutRate, MatrixData output, RunnerBehavior behavior)  : base(Structure.Empty, input, behavior)
        {
            DropOutRate = dropOutRate;
            DropoutMaskFloat = new CudaPieceFloat(Input.Column * Input.MaxRow, Behavior.Device);
            Output = output;
            isProcess = true;
        }


        public void InitDropOut()
        {
            if(DropOutRate > 0)
            {
                for (int b = 0; b < Input.Row; b++)
                {
                    List<int> seqList = new List<int>();
                    for (int i = 0; i < Input.Column; i++)
                    {
                        seqList.Add(i);
                        DropoutMaskFloat[b * Input.Column + i] = 1;
                    }
                    int expection = (int)(DropOutRate * Input.Column);
                    for (int h = 0; h < expection; h++)
                    {
                        int index = mRandom.Next(h, seqList.Count);
                        int selectedID = seqList[index];
                        seqList[index] = seqList[h];
                        seqList[h] = selectedID;
                        DropoutMaskFloat[b * Input.Column + selectedID] = 0;
                    }
                }
                DropoutMaskFloat.SyncFromCPU(Input.Column * Input.Row);
            }
        }

        public override void Forward()
        {
            if (DropOutRate > 0 && DropOutRate < 1)
            {
                InitDropOut();
                Output.Row = Input.Row;
                ComputeLib.ElementwiseProductMask(Input.Output, 0, DropoutMaskFloat, 0, Output.Output, 0, 
                      CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Input.Row, Input.Column, 0, 1.0f / (1 - DropOutRate));
            }
        }

        public override void CleanDeriv()
        {
            if (DropOutRate > 0 && DropOutRate < 1)
            {
                if(!isProcess) ComputeLib.Zero(Output.Deriv, Output.Row * Output.Column);
            }
        }
        public override void Backward(bool cleanDeriv)
        {
            if (DropOutRate > 0 && DropOutRate < 1)
            {
                ComputeLib.ElementwiseProductMask(Output.Deriv, 0, DropoutMaskFloat, 0, Input.Deriv, 0, 
                    CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, Output.Row, Output.Column, isProcess ? 0 : 1, 1.0f / (1 - DropOutRate));
            }
        }
    }


    


    public class DropOutProcessor<IN> : StructRunner<Structure, IN> where IN : HiddenBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        float DropOutRate { get; set; }
        public CudaPieceInt DropoutMask = null;
        public CudaPieceFloat DropoutMaskFloat = null;
        Random mRandom = new Random(ParameterSetting.Random.Next());
        bool ExternelMask = false;
        bool BatchWise = true;

        public DropOutProcessor(HiddenBatchData input, float dropOutRate, CudaPieceInt dropoutMask, RunnerBehavior behavior)
            : base(Structure.Empty, input, behavior)
        {
            Output = Input;
            DropOutRate = dropOutRate;
            DropoutMask = dropoutMask;
            ExternelMask = true;
            BatchWise = true;
        }

        public DropOutProcessor(HiddenBatchData input, float dropOutRate, RunnerBehavior behavior, bool batchWise = true)
            : base(Structure.Empty, input, behavior)
        {
            Output = Input;
            DropOutRate = dropOutRate;
            BatchWise = batchWise;

            if (batchWise)
            {
                DropoutMask = new CudaPieceInt(Input.Dim, true, Behavior.Device == DeviceType.GPU);
            }
            else
            {
                DropoutMaskFloat = new CudaPieceFloat(Input.Dim * Input.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
            }
        }

        public void InitDropOut()
        {
            if (BatchWise)
            {
                List<int> seqList = new List<int>();
                for (int i = 0; i < Input.Dim; i++)
                {
                    seqList.Add(i);
                    DropoutMask[i] = 1;
                }
                int expection = (int)(DropOutRate * Input.Dim);
                for (int h = 0; h < expection; h++)
                {
                    int index = mRandom.Next(h, seqList.Count);
                    int selectedID = seqList[index];
                    seqList[index] = seqList[h];
                    seqList[h] = selectedID;
                    DropoutMask[selectedID] = 0;
                }
                DropoutMask.SyncFromCPU();
            }
            else
            {
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    List<int> seqList = new List<int>();
                    for (int i = 0; i < Input.Dim; i++)
                    {
                        seqList.Add(i);
                        DropoutMaskFloat[b * Input.Dim + i] = 1;
                    }
                    int expection = (int)(DropOutRate * Input.Dim);
                    for (int h = 0; h < expection; h++)
                    {
                        int index = mRandom.Next(h, seqList.Count);
                        int selectedID = seqList[index];
                        seqList[index] = seqList[h];
                        seqList[h] = selectedID;
                        DropoutMaskFloat[b * Input.Dim + selectedID] = 0;
                    }
                }

                DropoutMaskFloat.SyncFromCPU(Input.Dim * Input.BatchSize);
            }
        }

        public override void Forward()
        {
            //var perf_dropout = PerfCounter.Manager.Instance["dropout_process_forward"].Begin();
            if (DropOutRate > 0 && DropOutRate < 1)
            {
                if (!ExternelMask) InitDropOut();

                if (BatchWise)
                {
                    ComputeLib.Matrix_Mask(Output.Output.Data, DropoutMask, Output.BatchSize, Output.Dim);
                }
                else
                {
                    ComputeLib.ElementwiseProduct(Input.Output.Data, DropoutMaskFloat, Output.Output.Data, Input.BatchSize, Input.Dim, 0);
                }
                ComputeLib.Scale_Matrix(Output.Output.Data, Output.BatchSize, Output.Dim, 1.0f / (1 - DropOutRate));
            }
            //PerfCounter.Manager.Instance["dropout_process_forward"].TakeCount(perf_dropout);
        }

        public override void Backward(bool cleanDeriv)
        {
            if (DropOutRate > 0 && DropOutRate < 1)
            {
                if (BatchWise)
                {
                    ComputeLib.Matrix_Mask(Output.Deriv.Data, DropoutMask, Output.BatchSize, Output.Dim);
                }
                else
                {
                    ComputeLib.ElementwiseProduct(Output.Deriv.Data, DropoutMaskFloat, Input.Deriv.Data, Output.BatchSize, Output.Dim, 0);
                }
                ComputeLib.Scale_Matrix(Input.Deriv.Data, Input.BatchSize, Input.Dim, 1.0f / (1 - DropOutRate));
            }
        }
    }
}
