using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorMeanPooling2DRunner : StructRunner<Structure, Tensor4DData>  
    {
        public new Tensor4DData Output { get { return (Tensor4DData) base.Output; } set { base.Output = value; } }
        CudaPieceInt LayerMaxPoolingIndex = null;

        public int SXWidth { get; set; }
        public int SXHeight { get; set; }
        
        public int StrideWidth { get; set; }
        public int StrideHeight { get; set; }
        
        public TensorMeanPooling2DRunner(Tensor4DData data, int sx, int stride, RunnerBehavior behavior) : 
            base(Structure.Empty, data, behavior)
        {

            SXWidth = sx;
            SXHeight = sx;
            StrideWidth = stride;
            StrideHeight = stride;

            Output = new Tensor4DData(Behavior.Device,
                                      new IntArgument("WIDTH",TensorConvUtil.PoolingOutput(Input.Width, sx, stride)),
                                      new IntArgument("HEIGHT", TensorConvUtil.PoolingOutput(Input.Height, sx, stride)),
                                      data.Shape[2],
                                      data.Shape[3]);
            LayerMaxPoolingIndex = new CudaPieceInt(Output.MaxLength, Behavior.Device);
        }
        
        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            if (Output.BatchSize == 0) return;
            
            ComputeLib.CuDNNMeanPooling2DForward(Input.Output, Input.Width, Input.Height, Input.Depth, Input.BatchSize, 
                                                SXWidth, SXHeight, 0, 0, StrideWidth, StrideHeight, 
                                                Output.Output, Output.Width, Output.Height, 0, 1);
            // int in_width, int in_height, int depth, int batch, 
            //                          int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
            //                          CudaPieceFloat output, int out_width, int out_height, float alpha, float beta);
            
            //Cudalib.MaxImagePooling(Input.Output.CudaPtr, Input.BatchSize, Input.Width, Input.Height, Input.Depth,
            //                           LayerMaxPoolingIndex.CudaPtr,
            //                           SXWidth, StrideWidth,
            //                           Output.Output.CudaPtr, Output.Width, Output.Height);

            //Output.Output.Print("Pooling results", 256, true);
            // ComputeLib.Maxpooling1D(Input.Output, Input.Height, Input.Width, Input.BatchSize, Output.Output, LayerMaxPoolingIndex);
        }

        /// <summary>
        /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
        /// </summary>
        /// <param name="cleanDeriv"></param>
        public override void Backward(bool cleanDeriv)
        {
            if (Output.BatchSize == 0) return;
            ComputeLib.CuDNNMeanPooling2DBackward(Input.Output, Input.Deriv, Input.Width, Input.Height, Input.Depth, Input.BatchSize,
                                                 SXWidth, SXHeight, 0, 0, StrideWidth, StrideHeight, 
                                                 Output.Output, Output.Deriv, Output.Width, Output.Height, 1, 1);
        }

        public override void CleanDeriv()
        {
            if (Output.BatchSize == 0) return;
            ComputeLib.Zero(Output.Deriv, Output.Length);
        }
    }
}
