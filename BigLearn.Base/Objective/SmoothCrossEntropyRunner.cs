
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{

	public class SmoothCrossEntropyRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public DenseBatchData Deriv { get; set; }

        public CudaPieceFloat Label { get; set; }
        
        public float Smooth = 0;

        public float Weight = 1.0f;
		public bool IsAvg = true;

        public CudaPieceFloat tmpOutput;
        
        //public float bceLoss = 0;
        //public float avgProb = 0;

        //public bool IsReportBCE = false;
        
        public SmoothCrossEntropyRunner(CudaPieceFloat label, HiddenBatchData output, float smooth, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
	        name = "cross entropy loss";
            Output = output.Output;
            Deriv = output.Deriv;

            tmpOutput = new CudaPieceFloat(Output.Dim * Output.MAX_BATCHSIZE, behavior.Device); 

            Label = label;
            Smooth = smooth;
        }



        public override void Forward()
        {
            ObjectiveScore = 0;
            if(Output.BatchSize == 0) return;

            int num = Output.BatchSize * Output.Dim;

            // Deriv.Data
            ComputeLib.Logistic(Output.Data, 0, tmpOutput, 0, num, 1);

            //if(IsReportAvgProb) avgProb = ComputeLib.VectorSum(tmpOutput, 0, num , 1) * 1.0f / num;
            
            //if(IsReportBCE)
            {
                tmpOutput.SyncToCPU();
                Label.SyncToCPU();
                float bceLoss = 0;
                for(int i = 0; i < num; i++)
                { 
                    float lambda = Label[i];

                    if(lambda >= 1) lambda = 1 - Smooth;
                    else if(lambda <= 0) lambda = Smooth;

                    bceLoss += lambda * (float)Math.Log(tmpOutput[i] + Util.LargeEpsilon) + (1 - lambda) * (float)Math.Log(1 - tmpOutput[i] + Util.LargeEpsilon); 
                    //(float)(tmpOutput[i] > 0 ? Math.Log(1 - tmpOutput[i]) : Math.Log(1 + tmpOutput[i]));
                    
                    tmpOutput[i] = lambda - tmpOutput[i];
                }
                ObjectiveScore = -bceLoss * Weight;
                
                tmpOutput.SyncFromCPU();
            }
            //ComputeLib.Add_Vector(tmpOutput, Label, num, -1, 1);

            if(Deriv != null)
            {
                ComputeLib.Add_Vector(Deriv.Data, tmpOutput, num, 1, Weight * (IsAvg ? 1.0f / num : 1) );
            }

            if(IsAvg) ObjectiveScore = ObjectiveScore / num;
        }
    }
}
