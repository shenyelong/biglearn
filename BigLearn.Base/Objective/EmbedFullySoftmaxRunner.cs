﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class EmbedFullySoftmaxRunner : ObjectiveRunner
    {
        public new EmbedStructure Model { get { return (EmbedStructure)base.Model; } set { base.Model = value; } }
        //new SeqDenseBatchData Input { get; set; }
        new HiddenBatchData Input { get; set; }
        CudaPieceInt LabelMask = null;
        CudaPieceFloat Label { get; set; }
        new CudaPieceFloat Output;
        CudaPieceFloat Deriv;
        CudaPieceFloat Prob;
        float Gamma;
        bool IsDeriv = true;
        bool IsUpdate = true;
        CudaPieceFloat biasOne;

        string OutputFilePath = string.Empty;
        StreamWriter dumpWriter;

        int batchNum = 0;
        float ppl = 0;
        public EmbedFullySoftmaxRunner(EmbedStructure model, SeqDenseBatchData input, CudaPieceInt labelMask, CudaPieceFloat label, float gamma,
            RunnerBehavior behavior, bool isDeriv = true, bool isUpdate = true)
            : base(model, behavior)
        {
            Input = new HiddenBatchData(input.MAX_SENTSIZE, input.Dim, input.SentOutput, input.SentDeriv, behavior.Device);
            //Input = input;
            LabelMask = labelMask;
            Label = label;
            Gamma = gamma;

            biasOne = new CudaPieceFloat(input.MAX_SENTSIZE, Behavior.Device);// true, Behavior.Device == DeviceType.GPU);
            biasOne.Init(1);

            Output = new CudaPieceFloat(input.MAX_SENTSIZE * Model.VocabSize, Behavior.Device); // true, Behavior.Device == DeviceType.GPU);

            Deriv = new CudaPieceFloat(input.MAX_SENTSIZE * Model.VocabSize, Behavior.Device); // true, Behavior.Device == DeviceType.GPU);

            Prob = new CudaPieceFloat(input.MAX_SENTSIZE, Behavior.Device); // true,  Behavior.Device == DeviceType.GPU);
            IsDeriv = isDeriv;
            IsUpdate = isUpdate;
        }

        public EmbedFullySoftmaxRunner(EmbedStructure model, HiddenBatchData input,  CudaPieceFloat label, float gamma,
            RunnerBehavior behavior, bool isDeriv = true, bool isUpdate = true)  : base(model, behavior)
        {
            Input = input;
            //Input = input;
            LabelMask = CudaPieceInt.Empty;
            Label = label;
            Gamma = gamma;

            biasOne = new CudaPieceFloat(input.MAX_BATCHSIZE, Behavior.Device);// true, Behavior.Device == DeviceType.GPU);
            biasOne.Init(1);

            Output = new CudaPieceFloat(input.MAX_BATCHSIZE * Model.VocabSize, Behavior.Device); // true, Behavior.Device == DeviceType.GPU);

            Deriv = new CudaPieceFloat(input.MAX_BATCHSIZE * Model.VocabSize, Behavior.Device); // true, Behavior.Device == DeviceType.GPU);

            Prob = new CudaPieceFloat(input.MAX_BATCHSIZE, Behavior.Device); // true,  Behavior.Device == DeviceType.GPU);
            IsDeriv = isDeriv;
            IsUpdate = isUpdate;
        }


        public EmbedFullySoftmaxRunner(EmbedStructure model, SeqDenseBatchData input, CudaPieceInt labelMask, CudaPieceFloat label, float gamma,
            string outputFile, RunnerBehavior behavior) : this(model, input, labelMask, label, gamma, behavior, false, false)
        {
            OutputFilePath = outputFile;
        }

        public override void Init()
        {
            if (OutputFilePath != string.Empty)
            {
                dumpWriter = new StreamWriter(OutputFilePath);
            }
            batchNum = 0;
            ppl = 0;
        }

        public override void Complete()
        {
            if (dumpWriter != null) dumpWriter.Close();
            Logger.WriteLog("PPL per word {0}", ppl / (batchNum + float.Epsilon));
        }

        public override void Forward()
        {
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Embedding, 0, Output, 0, Input.BatchSize, Model.Dim, Model.VocabSize, 0, 1, false, true);
            //if (Model.IsBias) ComputeLib.Matrix_Add_Linear(Output, Model.Bias, Input.BatchSize, Model.VocabSize);
            ComputeLib.SoftMax(Output, Deriv, Model.VocabSize, Input.BatchSize, Gamma);

            float prob = ComputeLib.LogLossDeriv(Deriv, Model.VocabSize, Input.BatchSize, Prob, Label,
                CudaPieceFloat.Empty, LabelMask, Gamma * 1.0f / Input.BatchSize, Util.GPUEpsilon);

            ObjectiveScore = prob / Input.BatchSize;

            if (IsDeriv)
            {
                ComputeLib.Sgemm(Deriv, 0, Model.Embedding, 0, Input.Deriv.Data, 0, Input.BatchSize, Model.VocabSize, Model.Dim, 0, 1, false, false);
            }
            if (IsUpdate)
            {
                /// update vocab embedding.
                //if (!IsDelegateOptimizer) { Model.EmbeddingOptimizer.BeforeGradient(); }
                ComputeLib.Sgemm(Deriv, 0, Input.Output.Data, 0, Model.EmbeddingGrad, 0, Input.BatchSize, Model.VocabSize, Model.Dim, 1, 1, true, false);
                //if (!IsDelegateOptimizer) { Model.EmbeddingOptimizer.AfterGradient(); }

                //if (Model.IsBias)
                //{
                    //if (!IsDelegateOptimizer) { Model.BiasOptimizer.BeforeGradient(); }
                //    ComputeLib.Sgemv(Deriv, biasOne, Input.BatchSize, Model.VocabSize, Model.BiasGrad, true, 1, 1);
                    //if (!IsDelegateOptimizer) { Model.BiasOptimizer.AfterGradient(); }
                //}
            }

            if (dumpWriter != null) dumpWriter.WriteLine(prob.ToString());

            ppl += prob;
            batchNum += Input.BatchSize;
        }
    }

}
