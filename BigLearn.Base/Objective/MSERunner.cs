using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MSERunnerV2 : ObjectiveRunner
    {
        public NdArrayData Data { get; set; }

        float Weight = 1.0f;
        float ClipDelta = 0;

        CudaPieceFloat tmp { get; set; }
        public MSERunnerV2(NdArrayData data, float weight, float clipDelta, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Data = data;

            Weight = weight;
            ClipDelta = 0;
            Weight = weight;
            
            tmp = new CudaPieceFloat(Data.MaxLength, Behavior.Device);
        }

        public override void Forward()
        {
            int length = Data.Length;
            ComputeLib.Add_Vector(tmp, Data.Output, length, 0, 1);

            float miniBatchLoss = 0;
            if (ClipDelta > 0)
            {
                tmp.SyncToCPU(length);
                for (int i = 0; i < length; i++)
                {
                    float bias = tmp[i];
                    float delta = bias;
                    float quadratic_part = Math.Min(Math.Abs(bias), ClipDelta);
                    float linear_part = Math.Abs(bias) - quadratic_part;

                    delta = bias > 0 ? quadratic_part : - quadratic_part;
                    tmp[i] = delta;
                    miniBatchLoss += quadratic_part * quadratic_part * 0.5f + linear_part;
                }
                tmp.SyncFromCPU(length);
            }
            else
            {
                miniBatchLoss = ComputeLib.DotProduct(tmp, tmp, length) * 0.5f;
            }

            ObjectiveScore = miniBatchLoss * Weight / length;
        }

        public override void Backward(bool clearDeriv)
        {
            ComputeLib.Scale_Vector(tmp, 0, tmp, 0, Data.Length, 0, Weight);
            if(Data.Deriv != null && Data.Deriv != CudaPieceFloat.Empty)
            {
                ComputeLib.Add_Vector(Data.Deriv, tmp, Data.Length, 1, -1.0f / Data.Length);
            }
        }   
    }

	public class MSERunner : ObjectiveRunner
    {
    	public List<float> LabelList = null;

    	public CudaPieceFloat Label { get; set; }

        public CudaPieceFloat Output { get; set; }        
        public CudaPieceFloat Deriv { get; set; }

        public CudaPieceFloat tmpDeriv = null;

        public bool IsAvgLoss = true;

        float Weight = 1.0f;

        float ClipDelta = 0;
        
        float Loss = 0;
        int BatchSize = 0;
        int Epoch = 0;

        // public MSERunner(DenseBatchData output, DenseBatchData label, CudaPieceFloat index, DenseBatchData deriv, RunnerBehavior behavior, bool isAvgLoss = true, float clip_delta = 0)
        //     : base(Structure.Empty, behavior)
        // {
        //     ClipDelta = clip_delta;
        //     Output = output;
        //     Label = label.Data;
        //     Index = index;
        //     Deriv = deriv;
        //     IsAvgLoss = isAvgLoss;
        //     if (Index == null || Index == CudaPieceFloat.Empty)
        //     {
        //         tmpDeriv = new CudaPieceFloat(Output.Dim * Output.MAX_BATCHSIZE, Behavior.Device);
        //     }
        // }
        public MSERunner(CudaPieceFloat label, CudaPieceFloat output, CudaPieceFloat deriv, int max_size, float clipDelta, bool isAvg, float weight, RunnerBehavior behavior)
        	: base(Structure.Empty, behavior)
        {
        	Label = label;
        	Output = output;
        	Deriv = deriv;
        	
        	ClipDelta = clipDelta;
        	IsAvgLoss = isAvg;
            Weight = weight;

        	tmpDeriv = new CudaPieceFloat(max_size, Behavior.Device);
        }

        public MSERunner(CudaPieceFloat label, CudaPieceFloat output, CudaPieceFloat deriv, int max_size, float weight, RunnerBehavior behavior)
            : this(label, output, deriv, max_size, 0.0f, true, weight, behavior)
        { }

        public MSERunner(List<float> label, HiddenBatchData output, float clipDelta, bool isAvg, RunnerBehavior behavior) 
            : this(new CudaPieceFloat(output.MAX_BATCHSIZE * output.Dim, behavior.Device), output.Output.Data, output.Deriv.Data, output.MAX_BATCHSIZE * output.Dim, clipDelta, isAvg, 1.0f, behavior)
        {
            LabelList = label;
        }

        public override void Complete()
            {
                Epoch += 1;
                //if(Epoch % 200 == 0)
                {
                    Logger.WriteLog("MSE Learner Epoch : {0}", Epoch);
                    {
                        Logger.WriteLog("Loss : {0}; weight : {1}", Loss * 1.0f / BatchSize, Weight / Output.EffectiveSize );
                    }
                    BatchSize = 0;
                    Loss = 0;
                }
            }

        public unsafe override void Forward()
        {
        	if(LabelList != null)
        	{
                Label.EffectiveSize = LabelList.Count;
                //int offset, int[] data, int offsetdata, int length)
                Label.SyncFromCPU(0, LabelList.ToArray(), 0, LabelList.Count);
        		//LabelList.CopyTo(Label.MemPtr);
                //Label.SyncFromCPU();
        	}
            
            //if (Index != null && Index != CudaPieceFloat.Empty)
            //    ObjectiveScore = RegressionLoss(Output.Data, Index, Label, Deriv.Data, Output.Dim, Output.BatchSize);
            //else
            ObjectiveScore = RegressionLoss(Output, Label, Deriv, Output.EffectiveSize);
            
            BatchSize += 1;
            Loss += (float)ObjectiveScore;
        }

        // unsafe float RegressionLoss(CudaPieceFloat output, CudaPieceFloat index, CudaPieceFloat label, CudaPieceFloat deriv, int dim, int batchSize)
        // {
        //     ///Compute Gradient.
        //     output.SyncToCPU();
        //     index.SyncToCPU();
        //     label.SyncToCPU();
        //     deriv.SyncToCPU();

        //     float miniBatchLoss = 0;
        //     for (int i = 0; i < batchSize; i++)
        //     {
        //         int regressIdx = (index == null || index == CudaPieceFloat.Empty) ? 0 : (int)index.CpuPtr[i];
        //         float bias = (label[i] - output[i * dim + regressIdx]);
        //         float delta = bias;
        //         if (ClipDelta > 0)
        //         {
        //             float quadratic_part = Math.Min(Math.Abs(bias), ClipDelta);
        //             float linear_part = Math.Abs(bias) - quadratic_part;
        //             miniBatchLoss += quadratic_part * quadratic_part * 0.5f + linear_part;
        //             delta = bias > 0 ? quadratic_part : -quadratic_part;
        //         }
        //         else
        //         {
        //             miniBatchLoss += bias * bias * 0.5f;
        //         }
        //         if (IsAvgLoss) { deriv[i * dim + regressIdx] += delta / batchSize; }
        //         else { deriv.CpuPtr[i * dim + regressIdx] += delta; }
        //     }
        //     deriv.SyncFromCPU();

        //     if (IsAvgLoss) return miniBatchLoss / batchSize;
        //     else return miniBatchLoss;
        // }

        unsafe float RegressionLoss(CudaPieceFloat output, CudaPieceFloat label, CudaPieceFloat deriv, int length)
        {
            if (length == 0) return 0;

            ComputeLib.Add_Vector(tmpDeriv, label, length, 0, 1);
            ComputeLib.Add_Vector(tmpDeriv, output, length, 1, -1);

            ///Compute Gradient.
            //Add_Vector(CudaPieceFloat a, CudaPieceFloat b, int m, float awei, float bwei);
            //if (IsAvgLoss) 
            //{   
            //    ComputeLib.Add_Vector(tmpDeriv, output, length, 2.0f / (batchSize * dim), -2.0f / (batchSize * dim));
            //}
            //ComputeLib.Matrix_AdditionEx(output, 0, dim, label, 0, dim, tmpDeriv, 0, dim, dim, batchSize, -2.0f / (batchSize * dim), 2.0f / (batchSize * dim), 0);
            //else 
            //{
                //ComputeLib.Matrix_AdditionEx(output, 0, dim, label, 0, dim, tmpDeriv, 0, dim, dim, batchSize, -1.0f, 1.0f, 0);
            //}
            ///Calculate Loss.
            float miniBatchLoss = 0;
            
            if (ClipDelta > 0)
            {
                tmpDeriv.SyncToCPU(length);
                for (int i = 0; i < length; i++)
                {
                    float bias = tmpDeriv[i];
                    float delta = bias;
                    float quadratic_part = Math.Min(Math.Abs(bias), ClipDelta);
                    float linear_part = Math.Abs(bias) - quadratic_part;

                    delta = bias > 0 ? quadratic_part : - quadratic_part;
                    tmpDeriv[i] = delta;

                    miniBatchLoss += quadratic_part * quadratic_part * 0.5f + linear_part;
                }
                tmpDeriv.SyncFromCPU(length);
            }
            else
            {
                //miniBatchLoss = ComputeLib.VectorSum(tmpDeriv, 0,  batchSize * dim , 1);
                miniBatchLoss = ComputeLib.DotProduct(tmpDeriv, tmpDeriv, length) * 0.5f;
                //for (int i = 0; i < batchSize * dim; i++) miniBatchLoss += (float)(tmpDeriv.CpuPtr[i] * tmpDeriv.CpuPtr[i] * 0.5);
            }

            /// b = bwei b + awei * a;
            ComputeLib.Scale_Vector(tmpDeriv, 0, tmpDeriv, 0, length, 0, Weight);
            
            ComputeLib.Add_Vector(deriv, tmpDeriv, length, 1, 1.0f / length);

            //Matrix_Add(deriv, tmpDeriv, dim, batchSize, 1);
            return miniBatchLoss * Weight / length ;
        }

    }
}
