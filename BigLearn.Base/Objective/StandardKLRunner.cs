using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class StandardKLRunner : ObjectiveRunner
    {
        public NdArrayData Mean { get; set; }

        public NdArrayData Logv { get; set; }

        public FloatArgument Weight { get; set; }

        CudaPieceFloat mean_v2 ;
        CudaPieceFloat exp_v;
        CudaPieceFloat tmp_v;

        CudaPieceFloat ones;

        public float KLValue { get; set; }
        public float Mu { get; set; }
        public float Delta { get; set;}

        public StandardKLRunner(NdArrayData mean, NdArrayData logv, FloatArgument weight, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Mean = mean;
            Logv = logv;
            Weight = weight;
            
            mean_v2 = new CudaPieceFloat(mean.MaxLength, behavior.Device);
            exp_v = new CudaPieceFloat(logv.MaxLength, behavior.Device);
            tmp_v = new CudaPieceFloat(logv.MaxLength, behavior.Device);
            
            ones = new CudaPieceFloat(logv.MaxLength, behavior.Device);
            ones.Init(1.0f);
        }

        public override void Backward(bool isclear)
        {
            ComputeLib.Add_Vector(Mean.Deriv, Mean.Output, Mean.Length, 1, - Weight.Value * 2.0f / Mean.Length);
            
            ComputeLib.Add_Vector(Logv.Deriv, ones, Logv.Length, 1,  Weight.Value * 1.0f / Logv.Length);
            ComputeLib.Add_Vector(Logv.Deriv, exp_v, Logv.Length, 1, - Weight.Value * 1.0f / Logv.Length);   
            //Logger.WriteLog("backward kl is called");
        }

        public override void Forward()
        {
            // c = alpah c + beta ( a \dot b );
            ComputeLib.CuDNNVectorMul(Mean.Output, Mean.Output, mean_v2, 0, 1, Mean.Length);

            float m_2 = ComputeLib.VectorSum(mean_v2, 0, Mean.Length, 1);

            // exp. b = alpha * b + beta * expf(a)
            //void Exp(CudaPieceFloat a, CudaPieceFloat b, float alpha, float beta, int size);
            ComputeLib.Exp(Logv.Output, exp_v, 0, 1, Logv.Length);

            ComputeLib.Add_Vector(tmp_v, exp_v, Logv.Length, 0, 1);
            ComputeLib.Add_Vector(tmp_v, Logv.Output, Logv.Length, 1, -1);

            float v_2 = ComputeLib.VectorSum(tmp_v, 0, Logv.Length, 1);

            KLValue = (m_2 + v_2) / Logv.Length - 1;

            Mu = m_2 / Logv.Length;
            Delta = v_2 / Logv.Length - 1;

            ObjectiveScore = KLValue * Weight.Value;
        }
    }
}
