using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class JointLoglikelihoodRunner : ObjectiveRunner
        {
            List<HiddenBatchData> TPs { get; set; }
            List<HiddenBatchData> TOs { get; set; }
            SparseVectorData Label { get; set; }

            float[] Probs = null;
            int BatchSize = 0;
            int Step = 0;
            public int StatusReport = 100;
            int Accuracy = 0;
            
            public float PredAccuracy { get { return Accuracy * 1.0f / BatchSize; } }

            public List<float[]> Pred { get; set; }

            public JointLoglikelihoodRunner(List<HiddenBatchData> tps, List<HiddenBatchData> tos, SparseVectorData label, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                TPs = tps;
                TOs = tos;
                Label = label;

                Probs = new float[TPs.Count];
                Pred = new List<float[]>();
            }

            public override void Init()
            {
                BatchSize = 0;
                Accuracy = 0;
                Probs = new float[TPs.Count];
                Pred.Clear();
            }

            public override void Complete()
            {
                for(int i=0; i < Probs.Length; i++)
                {
                    Logger.WriteLog("Average Step {0} Terminate Prob {1}", i, Probs[i] / BatchSize);
                }
                Logger.WriteLog("Average Accuracy {0}", Accuracy * 1.0f / BatchSize);
            }

            public override void Forward()
            {
                for(int i = 0; i < TPs.Count; i++)
                {
                    TPs[i].Output.Data.SyncToCPU();
                    TOs[i].Output.Data.SyncToCPU();

                    TPs[i].Deriv.Data.SyncToCPU();
                    TOs[i].Deriv.Data.SyncToCPU();
                }

                int batchSize = TPs[0].BatchSize;
                int dim = TOs[0].Dim;

                float np_score = 0;

                //Logger.WriteLog("forward 1");
                for(int b = 0; b < batchSize; b++)
                {
                    float np = -1e10f;
                    float _p = 0;

                    // i * Output.Dim + tmpMaxIndex[i] == Label.Idx[i]
                    int lidx = Label.Idx[b] - b * dim;
                    
                    float[] p1List = new float[TPs.Count];
                    float[] p2List = new float[TPs.Count];
                    float[] pList = new float[TPs.Count];
                    float[] npList = new float[TPs.Count];

                    float[] pred = new float[dim];

                    for(int i = 0; i < TPs.Count; i++)
                    {
                        float p1 = TPs[i].Output.Data[b * 2 + 1];
                        float _p1 = TPs[i].Output.Data[b * 2 + 0];

                        Probs[i] += p1;

                        float p2 = TOs[i].Output.Data[b * dim + lidx];
                        
                        p2List[i] = (float)Math.Log(p2 + Util.SmallEpsilon);

                        pList[i] = _p;

                        if(i == TPs.Count - 1) p1List[i] = _p;
                        else p1List[i] = _p + (float)Math.Log(p1 + Util.SmallEpsilon); 
                        
                        _p = _p + (float)Math.Log(_p1 + Util.SmallEpsilon);

                        np = Util.LogAdd(np, p1List[i] + p2List[i]); 

                        npList[i] = np;

                        for(int d = 0; d < dim; d++)
                        {
                            pred[d] += (float)Math.Exp(p1List[i]) * TOs[i].Output.Data[b * dim + d];
                        }

                    }

                    Pred.Add(pred);

                    if(Util.MaximumValue(pred) == lidx)
                    {
                        Accuracy += 1;
                    }

                    //Logger.WriteLog("forward 3");
                    // average score.
                    np_score += (float)Math.Exp(np);    

                    if(Behavior.RunMode == DNNRunMode.Train)
                    {
                        for(int i = 0; i < TPs.Count; i++)
                        {
                            TOs[i].Deriv.Data[b * dim + lidx] += (float)Math.Exp(p1List[i] - np);
                        }

                        for(int i = 0; i < TPs.Count - 1; i++)
                        {
                            TPs[i].Deriv.Data[b * 2 + 1] += (float) Math.Exp(p2List[i] + pList[i] - np);
                            TPs[i].Deriv.Data[b * 2] += (float)(1 - Math.Exp(npList[i] - np)) / (TPs[i].Output.Data[b * 2] + Util.SmallEpsilon);
                        }
                    }
                }

                //Logger.WriteLog("forward 4");
                if(Behavior.RunMode == DNNRunMode.Train)
                {
                    for(int i = 0; i < TPs.Count; i++)
                    {
                        TPs[i].Deriv.Data.SyncFromCPU();
                        TOs[i].Deriv.Data.SyncFromCPU();
                    }
                }

                ObjectiveScore = np_score / batchSize;
                BatchSize += batchSize;

                Step += 1;

                if(Step % StatusReport == 0)
                {
                    for(int i=0; i < Probs.Length; i++)
                    {
                        Logger.WriteLog("Average Step {0} Terminate Prob {1}", i, Probs[i] / BatchSize);
                    }
                    Logger.WriteLog("Average Accuracy {0}", Accuracy * 1.0f / BatchSize);
                }
            }
        }
}
