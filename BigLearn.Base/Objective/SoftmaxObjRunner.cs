using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SoftmaxObjRunner : ObjectiveRunner
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value;  } }
        
        public CudaPieceInt LabelIdx { get; set; }

        public SparseVectorData Label { get; set; }
        //public float Gamma { get; set; }
        public CudaPieceFloat tmpProb { get; set; }

        float Perplexity = 0;
        int Num = 0;

        //CudaPieceFloat tmpOut;
        //CudaPieceInt tmpIdx;

        bool IsReportAccuracy = false;

        public bool IsSavePred = false;

        CudaPieceFloat tmpMaxProb = null;
        CudaPieceInt tmpMaxIndex = null;
        public float Accuracy = 0;

        public int CurrentAcc = 0;
        public int CurrentSmp = 0;

        public List<float[]> Pred { get; set; }
        /// <summary>
        /// Label could be null, the first element will be viewed as positive, others are negative.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="output"></param>
        /// <param name="deriv"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public SoftmaxObjRunner(SparseVectorData label, HiddenBatchData output, RunnerBehavior behavior)
            : this(label, output, false, behavior)
        { }

        public SoftmaxObjRunner(CudaPieceInt idx, HiddenBatchData output, RunnerBehavior behavior) 
            : this(new SparseVectorData(idx.Size, behavior.Device, false), output, behavior)
        {
            LabelIdx = idx;
            Label.Value.Init(1.0f);
        }

        public SoftmaxObjRunner(SparseVectorData label, HiddenBatchData output, bool isReportAccuracy, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Label = label;
            Output = output;
            tmpProb = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);

            IsReportAccuracy = isReportAccuracy;
            if(IsReportAccuracy)
            {
                tmpMaxProb = new CudaPieceFloat(Output.MAX_BATCHSIZE, behavior.Device);
                tmpMaxIndex = new CudaPieceInt(Output.MAX_BATCHSIZE, behavior.Device);
            }

            name = "softmax-perplexity";

            Pred = new List<float[]>();
        }

        public override void Init()
        {
            Perplexity = 0;
            Num = 0;
            Accuracy = 0;
            
            if(IsSavePred) Pred.Clear();
        }

        public float AvgPerplexity { get { return Perplexity * 1.0f / Num; } }
        public float AvgAccuracy { get { return Accuracy * 1.0f / Num; } }

        public float CurrentPerplexity { get; set; }
        public override void Complete()
        {
            Logger.WriteLog("Average Perplexity {0}, Number {1}", Perplexity / Num, Num);

            if(IsReportAccuracy)
            {
                Logger.WriteLog("Average Accuracy {0}", AvgAccuracy);
            }
        }

        public override void Forward()
        {
            if(Output.BatchSize == 0) { ObjectiveScore = 0; return; }
            
            if(LabelIdx != null)
            {
        		if(Output.BatchSize != LabelIdx.EffectiveSize)
        		{
        		    throw new Exception(string.Format("output batchsize {0} doesn't match with the labelIdx size {1}", Output.BatchSize, LabelIdx.EffectiveSize));
        		}
                LabelIdx.SyncToCPU();
                
                for(int i = 0; i < Output.BatchSize; i++)
                {
                    Label.Idx[i] = LabelIdx[i] + i * Output.Dim;
                }
                Label.Length = Output.BatchSize;
                Label.Idx.SyncFromCPU();
            }

            ComputeLib.SoftMax(Output.Output.Data, Output.Deriv.Data, Output.Dim, Output.BatchSize, 1);

            // report top1 accuracy.
            if(IsReportAccuracy)
            {
                ComputeLib.Maxpooling1D(Output.Deriv.Data, Output.Dim, 1, Output.BatchSize, tmpMaxProb, tmpMaxIndex);
                tmpMaxIndex.SyncToCPU();

                CurrentAcc = 0;
                for(int i = 0; i < Output.BatchSize; i++)
                {
                    if(i * Output.Dim + tmpMaxIndex[i] == Label.Idx[i])
                    {
                        Accuracy += 1;
                        CurrentAcc += 1;
                    }
                }
                CurrentSmp = Output.BatchSize;
            }

            if(IsSavePred)
            {
                //SyncToCPU(int offset, float[] data, int offsetdata, int length) 
                float[] pV = new float[Output.BatchSize * Output.Dim];
                Output.Deriv.Data.SyncToCPU(0, pV, 0, Output.BatchSize * Output.Dim);

                for(int i=0; i < Output.BatchSize; i++)
                {
                    float[] p = new float[Output.Dim];
                    Array.Copy(pV, i * Output.Dim, p, 0, Output.Dim);
                    Pred.Add(p);
                }
            }

            // compute the probability of the target.
            ComputeLib.SparseGthr(Label.Idx, tmpProb, Output.Deriv.Data, Label.Length);

            ///// b = alpha * b + alpha * log(a);
            // ObjectiveScore = ComputeLib.VectorSum(tmpProb, 0, Label.Length, 1) * 1.0f / Label.Length;
                        
            ComputeLib.Log(tmpProb, 0, tmpProb, 0, 0, 1, Label.Length);
            
            // vector sum is abs
            ObjectiveScore = ComputeLib.VectorSum(tmpProb, 0, Label.Length, 1) * 1.0f / Label.Length;
            
            CurrentPerplexity = (float)ObjectiveScore;

            Perplexity += (float)ObjectiveScore * Label.Length; // ComputeLib.VectorSum(tmpProb, 0, Label.Length, 1);
            Num = Num + Label.Length;

            if(Behavior.RunMode == DNNRunMode.Train)
            {
                ComputeLib.Scale_Matrix(Output.Deriv.Data, Output.BatchSize, Output.Dim, -1.0f / Output.BatchSize);
                ComputeLib.SparseVectorAdd(Label.Idx, Label.Value, Output.Deriv.Data, 1.0f / Output.BatchSize, Label.Length);
            }

        }
    }
}
