﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ObjectiveRunner : StructRunner
    {
        public float Value { get { return (float)ObjectiveScore; } }
        public double ObjectiveScore { get; protected set; }
        public Dictionary<string, double> ObjectiveDict = new Dictionary<string, double>();
        public ObjectiveRunner(Structure model, RunnerBehavior behavior) : base(model, behavior)
        { }
    }

    public class BayesianRatingRunner : ObjectiveRunner
    {
        public new CudaPieceFloat Output { get; set; }
        public CudaPieceFloat Deriv { get; set; }
        public CudaPieceFloat Label { get; set; }

        public BiMatchBatchData MatchData { get; set; }

        public CudaPieceFloat SrcBatchLoss;

        public float Gamma { get; set; }
        bool IsLog { get; set; }
        public BayesianRatingRunner(CudaPieceFloat output, CudaPieceFloat deriv, CudaPieceFloat label,
            BiMatchBatchData matchData, float gamma, RunnerBehavior behavior, bool isLog = true)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Deriv = deriv;
            Label = label;

            MatchData = matchData;
            Gamma = gamma;
            IsLog = isLog;

            SrcBatchLoss = new CudaPieceFloat(MatchData.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            if (MatchData.SrcSize == 0) { ObjectiveScore = 0; return; }
            ComputeLib.LogBayesianRatingLoss(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                      SrcBatchLoss, Output, Label, Deriv, (uint)MatchData.MatchSize, Util.GPUEpsilon, Gamma);



            ComputeLib.Scale_Matrix(Deriv, MatchData.MatchSize, 1, 1.0f / MatchData.SrcSize);

            float miniBatchLoss = 0;
            SrcBatchLoss.SyncToCPU(MatchData.SrcSize);

            if (IsLog)
            {
                for (int i = 0; i < MatchData.SrcSize; i++) miniBatchLoss += (float)SrcBatchLoss[i];
            }
            else
            {
                Deriv.SyncToCPU(MatchData.MatchSize);
                MatchData.Src2MatchIdx.SyncToCPU(MatchData.SrcSize);
                for (int i = 0; i < MatchData.SrcSize; i++)
                {
                    float avgScore = (float)Math.Exp(SrcBatchLoss[i]);
                    miniBatchLoss += -avgScore;
                    int end = MatchData.Src2MatchIdx[i];
                    int bgn = i == 0 ? 0 : MatchData.Src2MatchIdx[i - 1];
                    for(int k = bgn; k <end; k++) Deriv[k] = Deriv[k] * avgScore;
                }
                Deriv.SyncFromCPU(MatchData.MatchSize);
            }
            ObjectiveScore = -miniBatchLoss / MatchData.SrcSize;
        }
    }

    public class MultiInstanceBayesianRatingRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public BiMatchBatchData MatchData { get; set; }
        public float Gamma { get; set; }

        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;


        public CudaPieceFloat SrcBatchLoss;


        public MultiInstanceBayesianRatingRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob,
            BiMatchBatchData matchData, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Label = label;
            MatchData = matchData;
            Gamma = gamma;

            Data = data;
            Prob = prob;

            Post = new float[MatchData.MAX_SRC_BATCHSIZE * Data.Length];
            SrcBatchLoss = new CudaPieceFloat(MatchData.MAX_SRC_BATCHSIZE, true, behavior.Device == DeviceType.GPU);
        }

        public override void Forward()
        {
            if (MatchData.SrcSize == 0) { ObjectiveScore = 0; return; }

            Label.SyncToCPU(MatchData.MatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                ComputeLib.LogBayesianRatingLoss(MatchData.Src2MatchIdx, MatchData.Src2MatchElement, MatchData.SrcSize,
                                      SrcBatchLoss, Data[d].Output.Data, Label, Data[d].Deriv.Data, (uint)MatchData.MatchSize, Util.GPUEpsilon, Gamma);

                Prob[d].Output.Data.SyncToCPU(MatchData.SrcSize);
                Data[d].Deriv.Data.SyncToCPU(MatchData.MatchSize);
                SrcBatchLoss.SyncToCPU(MatchData.SrcSize);

                double miniBatchLoss = 0;
                for (int b = 0; b < MatchData.SrcSize; b++)
                {
                    float loss = SrcBatchLoss[b];

                    int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx[b - 1];
                    int matchEnd = MatchData.Src2MatchIdx[b];
                    for (int m = matchBgn; m < matchEnd; m++)
                    {
                        Data[d].Deriv.Data[m] = Data[d].Deriv.Data[m] * Prob[d].Output.Data[b] / MatchData.SrcSize;
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data[b];
                    miniBatchLoss += loss * Prob[d].Output.Data[b];
                }
                ObjectiveScore += -miniBatchLoss / MatchData.SrcSize;
                Data[d].Deriv.Data.SyncFromCPU(MatchData.MatchSize);
            }

            for (int b = 0; b < MatchData.SrcSize; b++)
            {
                float sum = 0;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    Prob[d].Deriv.Data[b] = Post[b * Data.Length + d] - Prob[d].Output.Data[b];
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(MatchData.SrcSize);
            }
        }
    }

    public class MultiInstanceCrossEntropyRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        public CudaPieceFloat tmpOutput;

        public float Gamma { get; set; }

        public MultiInstanceCrossEntropyRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];

            Label = label;
            Gamma = gamma;

            if (Gamma != 1) { tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, true, Behavior.Device == DeviceType.GPU); }
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            Label.SyncToCPU(BatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                if (Gamma != 1) ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, Data[d].BatchSize * Data[d].Dim, 0, Gamma);
                else tmpOutput = Data[d].Output.Data;

                ComputeLib.DerivCrossEntropy(tmpOutput, Label, Data[d].Deriv.Data, Data[d].Output.BatchSize);

                tmpOutput.SyncToCPU(Data[d].BatchSize);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);

                double miniBatchLoss = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    double score = (Math.Tanh(tmpOutput[b] / 2) + 1) / 2;

                    double loss = 0;
                    if (Label[b] != ParameterSetting.MissingLabel)
                    {
                        if (Label[b] > 0) loss = score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(score);
                        else loss = 1 - score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(1 - score);
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data[b];
                    miniBatchLoss += loss * Prob[d].Output.Data[b];
                }
                ObjectiveScore += -miniBatchLoss / Data[d].Output.BatchSize; /// batchSize;

                ComputeLib.Scale_Matrix(Data[d].Deriv.Data, Data[d].BatchSize, Data[d].Dim, (float)1.0 / Data[d].BatchSize * Gamma);
                ComputeLib.ElementwiseProduct(Data[d].Deriv.Data, Prob[d].Output.Data, Data[d].Deriv.Data, Data[d].BatchSize, 1, 0);
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float sum = 0;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    Prob[d].Deriv.Data[b] = Post[b * Data.Length + d] - Prob[d].Output.Data[b];
                }
            }
        }
    }

    public class BinaryStandardRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        //public CudaPieceFloat tmpOutput;

        public float Gamma { get; set; }

        public BinaryStandardRewardRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];

            Label = label;
            Gamma = gamma;

            //tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, true, Behavior.Device == DeviceType.GPU);
            //if (Gamma != 1) { }
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            Label.SyncToCPU(BatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                //if (Gamma != 1) ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, Data[d].BatchSize * Data[d].Dim, 0, Gamma);
                //else tmpOutput = Data[d].Output.Data;

                ComputeLib.Logistic(Data[d].Output.Data, 0, Data[d].Deriv.Data, 0, Data[d].Output.BatchSize * Data[d].Dim, Gamma);
                ComputeLib.ClipVector(Data[d].Deriv.Data, Data[d].Output.BatchSize * Data[d].Dim, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                //ComputeLib.DerivCrossEntropy(tmpOutput, Label, Data[d].Deriv.Data, Data[d].Output.BatchSize);

                //tmpOutput.SyncToCPU(Data[d].BatchSize);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);
                Data[d].Deriv.Data.SyncToCPU(Data[d].Output.BatchSize * Data[d].Dim);

                double miniBatchLoss = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    double score = Data[d].Deriv.Data[b];  // (Math.Tanh(tmpOutput[b] / 2) + 1) / 2;

                    double loss = 0;
                    {
                        if (Label[b] > 0) loss = score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(score);
                        else loss = 1 - score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(1 - score);
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data[b];
                    miniBatchLoss += Post[b * Data.Length + d]; // loss * Prob[d].Output.Data[b];
                }
                ObjectiveScore += miniBatchLoss / Data[d].Output.BatchSize; /// batchSize;
                //ComputeLib.Scale_Matrix(Data[d].Deriv.Data, Data[d].BatchSize, Data[d].Dim, (float)1.0 / Data[d].BatchSize * Gamma);
                //ComputeLib.ElementwiseProduct(Data[d].Deriv.Data, Prob[d].Output.Data, Data[d].Deriv.Data, Data[d].BatchSize, 1, 0);
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float sum = float.Epsilon;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    //Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    //Prob[d].Deriv.Data[b] = (Post[b * Data.Length + d] - Prob[d].Output.Data[b]) / BatchSize;

                    Prob[d].Deriv.Data[b] = Post[b * Data.Length + d] * 1.0f / BatchSize;


                    if (Label[b] > 0)
                        Data[d].Deriv.Data[b] = Gamma * (1 - Data[d].Deriv.Data[b]) * Post[b * Data.Length + d] / BatchSize;
                    else
                        Data[d].Deriv.Data[b] = -Gamma * Data[d].Deriv.Data[b] * Post[b * Data.Length + d] / BatchSize;
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(BatchSize);
                Data[d].Deriv.Data.SyncFromCPU(BatchSize);
            }
        }
    }

    public class BinaryMovingAvgRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        //public CudaPieceFloat tmpOutput;

        public float Gamma { get; set; }

        float[] AvgB;
        float[] AvgA;
        int MovingBatchNum = 0;
        public BinaryMovingAvgRewardRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];

            Label = label;
            Gamma = gamma;

            MovingBatchNum = 0;
            AvgB = new float[Data.Length];
            AvgA = new float[Data.Length];
            //tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, true, Behavior.Device == DeviceType.GPU);
            //if (Gamma != 1) { }
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            Label.SyncToCPU(BatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                //if (Gamma != 1) ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, Data[d].BatchSize * Data[d].Dim, 0, Gamma);
                //else tmpOutput = Data[d].Output.Data;

                ComputeLib.Logistic(Data[d].Output.Data, 0, Data[d].Deriv.Data, 0, Data[d].Output.BatchSize * Data[d].Dim, Gamma);
                ComputeLib.ClipVector(Data[d].Deriv.Data, Data[d].Output.BatchSize * Data[d].Dim, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                //ComputeLib.DerivCrossEntropy(tmpOutput, Label, Data[d].Deriv.Data, Data[d].Output.BatchSize);

                //tmpOutput.SyncToCPU(Data[d].BatchSize);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);
                Data[d].Deriv.Data.SyncToCPU(Data[d].Output.BatchSize * Data[d].Dim);

                double miniBatchLoss = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    double score = Data[d].Deriv.Data[b];  // (Math.Tanh(tmpOutput[b] / 2) + 1) / 2;

                    double loss = 0;
                    {
                        if (Label[b] > 0) loss = score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(score);
                        else loss = 1 - score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(1 - score);
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data[b];
                    miniBatchLoss += Post[b * Data.Length + d]; // loss * Prob[d].Output.Data[b];
                }
                ObjectiveScore += miniBatchLoss / Data[d].Output.BatchSize; /// batchSize;
                //ComputeLib.Scale_Matrix(Data[d].Deriv.Data, Data[d].BatchSize, Data[d].Dim, (float)1.0 / Data[d].BatchSize * Gamma);
                //ComputeLib.ElementwiseProduct(Data[d].Deriv.Data, Prob[d].Output.Data, Data[d].Deriv.Data, Data[d].BatchSize, 1, 0);
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float sum = float.Epsilon;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    //Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    //Prob[d].Deriv.Data[b] = (Post[b * Data.Length + d] - Prob[d].Output.Data[b]) / BatchSize;

                    double score = Data[d].Deriv.Data[b];  // (Math.Tanh(tmpOutput[b] / 2) + 1) / 2;
                    double p_pos = Label[b] > 0 ? score : 1 - score;
                    
                    Prob[d].Deriv.Data[b] = Prob[d].Output.Data[b] * 1.0f / BatchSize * (float)(p_pos - AvgB[d]);

                    if (Label[b] > 0)
                        Data[d].Deriv.Data[b] = (float) ( Gamma * (1 - score) * Post[b * Data.Length + d] / BatchSize );
                    else
                        Data[d].Deriv.Data[b] = -(float) ( Gamma * score * Post[b * Data.Length + d] / BatchSize );
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                float totalReward = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    totalReward += Post[b * Data.Length + d];
                }
                float avgReward = totalReward / BatchSize;

                AvgB[d] = 0.995f * AvgB[d] + 0.005f * avgReward;
            }
            MovingBatchNum = MovingBatchNum + 1;


            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(BatchSize);
                Data[d].Deriv.Data.SyncFromCPU(BatchSize);
            }
        }
    }

    public class BinaryCrossEntropyRunner : ObjectiveRunner
    {
        public SparseVectorData PosLabel { get; set; }
        List<List<int>> PosLabelList = null;
        public HiddenBatchData Score { get; set; }
        public SparseVectorData Mask = null;

        public BinaryCrossEntropyRunner(HiddenBatchData score, SparseVectorData posLabel, SparseVectorData mask, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
            Score = score;
            PosLabel = posLabel;
            Mask = mask;
            name = "bceRunner";
        }


        public BinaryCrossEntropyRunner(HiddenBatchData score, SparseVectorData posLabel, RunnerBehavior behavior) 
            : this(score, posLabel, null, behavior)
        { }


        public BinaryCrossEntropyRunner(HiddenBatchData score, List<List<int>> labels, RunnerBehavior behavior)
            : this(score, new SparseVectorData(score.MAX_BATCHSIZE * score.Dim, behavior.Device), behavior)
        {
            PosLabelList = labels;
        }

        public override void Forward()
        {
            //var perf_bce = PerfCounter.Manager.Instance["binarycrossentory"].Begin();
            if(PosLabelList != null)
            {
                int cursor = 0;
                for(int b =0 ; b < PosLabelList.Count; b++)
                {
                    int labelBase = b * Score.Dim;
                    foreach(int t in PosLabelList[b].Distinct())
                    {
                        PosLabel.Idx[cursor] = labelBase + t;
                        PosLabel.Value[cursor] = 0.999f;
                        cursor += 1;
                    }
                }
                PosLabel.Length = cursor;
                PosLabel.SyncFromCPU();
            }
            ComputeLib.Logistic(Score.Output.Data, 0, Score.Deriv.Data, 0, Score.BatchSize * Score.Dim, 1);
            //ComputeLib.Scale_Vector
            ComputeLib.Add_Vector(Score.Deriv.Data, Score.Deriv.Data, Score.Dim * Score.BatchSize, 0, -1);

            if(Mask != null)
            {
                ComputeLib.SparseVectorGivens(Mask.Idx, Mask.Value, Score.Deriv.Data, 0, 0, Mask.Length);
            }

            ComputeLib.SparseVectorAdd(PosLabel.Idx, PosLabel.Value, Score.Deriv.Data, 1, PosLabel.Length);
            ObjectiveScore = ComputeLib.VectorSum(Score.Deriv.Data, 0,  Score.BatchSize * Score.Dim , 1) / (Score.BatchSize * Score.Dim);
            
            //PerfCounter.Manager.Instance["binarycrossentory"].TakeCount(perf_bce);
        }
    }

    public class CrossEntropyRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat Label { get; set; }
        
        public List<float> LabelList = null;

        public DenseBatchData Deriv { get; set; }

        public float Gamma { get; set; }
        public CudaPieceFloat tmpOutput;
        public SparseVectorData Mask { get; set; }

        int Epoch = 0;
        int BatchSize = 0;
        float Loss = 0;
        float Accuracy = 0;
        int SampleNum = 0;
        
        public float bceLoss = 0;
        public float avgProb = 0;

        public float AvgLoss { get { return Loss * 1.0f / BatchSize; } }
        public float AvgAccuracy { get { return Accuracy * 1.0f / SampleNum; } }
        
        public bool IsReportAccuracy = true;
        public bool IsReportBCE = false;
        public bool IsReportAvgProb = false;

        public float Weight = 1.0f;
        public bool IsAvg = true;

        public CrossEntropyRunner(CudaPieceFloat label, SparseVectorData mask, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            name = "cross entropy loss";
            Output = output;
            Label = label;
            Mask = mask;
            Deriv = deriv;

            tmpOutput = new CudaPieceFloat(Output.Dim * Output.MAX_BATCHSIZE, behavior.Device);
        }

        public CrossEntropyRunner(CudaPieceFloat label, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : this(label, null, output, deriv, gamma, behavior)
        { }

        public CrossEntropyRunner(CudaPieceFloat label,  SparseVectorData mask, HiddenBatchData output, RunnerBehavior behavior) 
            : this(label, mask, output.Output, output.Deriv, 1, behavior)
        { }

        public CrossEntropyRunner(CudaPieceFloat label, HiddenBatchData output, RunnerBehavior behavior) 
            : this(label, null, output.Output, output.Deriv, 1, behavior)
        { }

        public CrossEntropyRunner(List<float> label, HiddenBatchData output, RunnerBehavior behavior) 
            : this(new CudaPieceFloat(output.MAX_BATCHSIZE * output.Dim, behavior.Device), output.Output, output.Deriv, 1, behavior)
        {
            LabelList = label;
        }

        public override void Complete()
        {
            Epoch += 1;
        }

        public override void Init()
        {
            Accuracy = 0;
            SampleNum = 0;

            BatchSize = 0;
            Loss = 0;
        }


        public override void Forward()
        {
            ObjectiveScore = 0;
            if(Output.BatchSize == 0) return;

            int num = Output.BatchSize * Output.Dim;

            if(LabelList != null)
            {
                Label.EffectiveSize = LabelList.Count;
                Label.SyncFromCPU(0, LabelList.ToArray(), 0, LabelList.Count);
                //LabelList.CopyTo(Label);
                //Label.SyncFromCPU();
            }

            // Deriv.Data
            ComputeLib.Logistic(Output.Data, 0, tmpOutput, 0, num, 1);

            if(IsReportAvgProb)
            {
                avgProb = ComputeLib.VectorSum(tmpOutput, 0, num , 1) * 1.0f / num;
            }
            
            if(IsReportAccuracy)
            {
                tmpOutput.SyncToCPU();
                for(int i = 0; i < num; i++)
                {
                    if(Math.Abs(tmpOutput[i]) < 0.5f)
                    {
                        Accuracy += 1;
                    }
                }
                SampleNum += num;
            }

            if(IsReportBCE)
            {
                tmpOutput.SyncToCPU();
                Label.SyncToCPU();
                bceLoss = 0;
                for(int i = 0; i < num; i++)
                { 
                    if(Label[i] == 0) bceLoss += (float)Math.Log(1 - tmpOutput[i] + Util.LargeEpsilon);
                    else if(Label[i] == 1) bceLoss += (float)Math.Log(tmpOutput[i] + Util.LargeEpsilon); 
                    else bceLoss += Label[i] * (float)Math.Log(tmpOutput[i] + Util.LargeEpsilon) + (1 - Label[i]) * (float)Math.Log(1 - tmpOutput[i] + Util.LargeEpsilon); 
                    //(float)(tmpOutput[i] > 0 ? Math.Log(1 - tmpOutput[i]) : Math.Log(1 + tmpOutput[i]));
                }
                ObjectiveScore = -bceLoss * Weight;
            }

            ComputeLib.Add_Vector(tmpOutput, Label, num, -1, 1);

            if(Mask != null)
            {
                ComputeLib.SparseVectorGivens(Mask.Idx, Mask.Value, tmpOutput, 0, 0, Mask.Length);
            }
            
            if(! IsReportBCE) 
            { 
                ObjectiveScore = ComputeLib.VectorSum(tmpOutput, 0, num, 1) * Weight; 
            }

            if(Deriv != null)
            {
                ComputeLib.Add_Vector(Deriv.Data, tmpOutput, num, 1, Weight * (IsAvg ? 1.0f / num : 1) );
            }

            if(IsAvg)
            {
                ObjectiveScore = ObjectiveScore / num;
            }

            BatchSize += 1;
            Loss += (float)ObjectiveScore;
        }

    }
    
    /// <summary>
    /// Label values only work on the First Dim. For other Dimension, label values are zero by default.
    /// </summary>
    public class SampleCrossEntropyRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public DenseBatchData Label { get; set; }
        public DenseBatchData Deriv { get; set; }
        public float Gamma { get; set; }
        public SampleCrossEntropyRunner(DenseBatchData label, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
            Deriv = deriv;
            Gamma = gamma;
        }

        public override void Forward()
        {
            ComputeLib.Calculate_SampleCrossEntropyDeriv(Output.Data, Deriv.Data, Output.Dim, Label.Data, Output.BatchSize, Gamma);
            ComputeLib.Scale_Matrix(Deriv.Data, Output.Dim, Output.BatchSize, (float)1.0f / Output.BatchSize);
            ObjectiveScore = 0;
            Label.Data.SyncToCPU();
            Deriv.Data.SyncToCPU();
            for (int i = 0; i < Output.BatchSize * Output.Dim; i++)
            {
                float label = i % Output.Dim == 0 ? Label.Data[i / Output.Dim] : 0;
                double value = label - Deriv.Data[i] / Gamma;

                if (float.IsInfinity(Deriv.Data[i])) Console.WriteLine("SampleCrossEntropyRunner Infinity in Deriv.Data");
                if(value < 0) value = Util.GPUEpsilon;
                if(value >= 1) value = 1 - Util.GPUEpsilon;

                double l = (label * Math.Log(value + Util.GPUEpsilon) + (1 - label) * Math.Log(-value + 1.0 + Util.GPUEpsilon));
                if (double.IsNaN(l) || double.IsInfinity(l)) Console.ReadLine();

                ObjectiveScore -= l;
            }
            //Console.WriteLine("Loss {0}", Loss);
        }
    }

    public class MultiClassSoftmaxRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value;  } }
        public CudaPieceFloat Label { get; set; }
        public DenseBatchData Deriv { get; set; }
        public DenseBatchData Weight { get; set; }

        public float Gamma { get; set; }


        public double ScoreTotal = 0;
        public int BatchTotal = 0;

        /// <summary>
        /// Label could be null, the first element will be viewed as positive, others are negative.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="output"></param>
        /// <param name="deriv"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public MultiClassSoftmaxRunner(CudaPieceFloat label, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
            Deriv = deriv;
            Gamma = gamma;
            Weight = null;
        }


        /// <summary>
        /// Weighted Log Softmax
        /// </summary>
        /// <param name="label"></param>
        /// <param name="weight"></param>
        /// <param name="output"></param>
        /// <param name="deriv"></param>
        /// <param name="gamma"></param>
        /// <param name="behavior"></param>
        public MultiClassSoftmaxRunner(CudaPieceFloat label, DenseBatchData weight, DenseBatchData output, DenseBatchData deriv, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
            Deriv = deriv;
            Gamma = gamma;
            Weight = weight;
        }

        public override void Init()
        {
            BatchTotal = 0;
            ScoreTotal = 0;
        }

        public override void Complete()
        {
            Logger.WriteLog("Average Score {0},  Batch {1}", ScoreTotal / BatchTotal, BatchTotal);
        }

        public override void Forward()
        {
            if(Output.BatchSize == 0)
            {
                ObjectiveScore = 0; return;
            }

            if (Label != null) Label.SyncToCPU(Output.BatchSize);
            if (Weight != null) Weight.Data.SyncToCPU(Output.BatchSize);


            Output.Data.SyncToCPU();
            for (int i = 0; i < Output.BatchSize; i++)
            {
                int l = Label == null ? 0 : (int)Label[i];
                float weight = 1.0f;
                if (Weight != null)
                {
                    weight = Weight.Data[i];
                }
                //DSSMLossDerivParameter.COSINE_SIM_SCORE += weight * Output.Data[i * Output.Stat.Dim + l];
                ScoreTotal += Output.Data[i * Output.Dim + l];
            }
            BatchTotal += Output.BatchSize;

            //if(Gamma != 1) ComputeLib.Scale_Matrix(Output.Data, Output.BatchSize, Output.Stat.Dim, Gamma);
            ComputeLib.Add_Vector(Deriv.Data, Output.Data, Output.BatchSize * Output.Dim, 0, Gamma);
            ComputeLib.SoftMax(Deriv.Data, Deriv.Data, Output.Dim, Output.BatchSize, 1);
            //if (Gamma != 1) ComputeLib.Scale_Matrix(Output.Data, Output.BatchSize, Output.Stat.Dim, 1.0f / Gamma);

            Deriv.Data.SyncToCPU(Output.BatchSize * Output.Dim);
            
            double miniBatchLoss = 0;
            for (int i = 0; i < Output.BatchSize; i++)
            {
                int l = Label == null ? 0 : (int)Label[i];
                float wei = Weight == null ? 1 : Weight.Data[i];
                miniBatchLoss += wei * Math.Log(Math.Max(Deriv.Data[i * Output.Dim + l], Util.LargeEpsilon));
                Deriv.Data[i * Output.Dim + l] -= 1;
                if (Weight != null)
                    for (int d = 0; d < Output.Dim; d++)
                        Deriv.Data[i * Output.Dim + d] = Deriv.Data[i * Output.Dim + d] * wei;
            }
            Deriv.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
            ComputeLib.Scale_Matrix(Deriv.Data, Output.BatchSize, Output.Dim, -Gamma * 1.0f / Output.BatchSize);
            ObjectiveScore = - miniBatchLoss / Output.BatchSize;
        }
    }


    


    public class SumCosineSimilarityRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat Label { get; set; }
        public DenseBatchData Deriv { get; set; }
        public DenseBatchData Weight { get; set; }



        public double CosineTotal { get; set; }
        public double WeightedCosineTotal { get; set; }

        public double TotalCount{ get; set; }

        public SumCosineSimilarityRunner(CudaPieceFloat label, DenseBatchData output,  RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
        }

        public override void Init()
        {
            this.CosineTotal = 0;
            this.WeightedCosineTotal = 0;
            this.TotalCount = 0;
        }

        public override void Complete()
        {
            Logger.WriteLog("Cosine Score {0},  weighted cosine:{1},  TotalCount {2}, Overall Cosine score:{3}, Overall Weighted Cosine score:{4}", 
                CosineTotal / TotalCount, this.WeightedCosineTotal / TotalCount, TotalCount, CosineTotal, this.WeightedCosineTotal);
        }
        public override void Forward()
        {
            if (Output.BatchSize == 0)
            {
                ObjectiveScore = 0; return;
            }

            if (Label != null) Label.SyncToCPU(Output.BatchSize);
            if (Weight != null) Weight.Data.SyncToCPU(Output.BatchSize);


            Output.Data.SyncToCPU();
            for (int i = 0; i < Output.BatchSize; i++)
            {
                int l = Label == null ? 0 : (int)Label[i];
                float weight = 1.0f;
                if (Weight != null)
                {
                    weight = Weight.Data[i];
                }
                WeightedCosineTotal += weight * Output.Data[i * Output.Dim + l];
                CosineTotal += Output.Data[i * Output.Dim + l];
            }
            TotalCount += Output.BatchSize;

        }
    }

    public class MarginLossRunner : ObjectiveRunner
    {
        public float Margin { get; set; }
        public HiddenBatchData PosData { get; set; }
        public HiddenBatchData NegData { get; set; }
        public MarginLossRunner(float margin, HiddenBatchData pos, HiddenBatchData neg, RunnerBehavior behavior) : base (Structure.Empty, behavior)
        {
            Margin = margin;
            PosData = pos;
            NegData = neg;
        }

        public override void Forward()
        {
            PosData.Output.Data.SyncToCPU();
            NegData.Output.Data.SyncToCPU();
            ObjectiveScore = 0;
            for (int i = 0; i < PosData.BatchSize; i++)
            {
                if (PosData.Output.Data[i] + Margin > NegData.Output.Data[i])
                {
                    PosData.Deriv.Data[i] = -1;
                    NegData.Deriv.Data[i] = 1;
                    ObjectiveScore += Margin + PosData.Output.Data[i] - NegData.Output.Data[i];
                }
                else 
                {
                    PosData.Deriv.Data[i] = 0;
                    NegData.Deriv.Data[i] = 0;
                }
            }
            PosData.Deriv.Data.SyncFromCPU();
            NegData.Deriv.Data.SyncFromCPU();
        }
    }

    public class HingeLossRunner : ObjectiveRunner
    {
        public float Margin { get; set; }
        public HiddenBatchData Score { get; set; }
        public HingeLossRunner(float margin, HiddenBatchData score, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Margin = margin;
            Score = score;
        }

        public override void Forward()
        {
            ObjectiveScore = 0;
            if (Score.BatchSize == 0) return;

            Score.Output.Data.SyncToCPU(Score.BatchSize * Score.Dim);
            Score.Deriv.Data.SyncToCPU(Score.BatchSize * Score.Dim);

            for (int i = 0; i < Score.BatchSize; i++)
            {
                //Score.Deriv.Data[i * Score.Dim] = 0;
                for (int d = 1; d < Score.Dim; d++)
                {
                    float v = Score.Output.Data[i * Score.Dim] + Margin - Score.Output.Data[i * Score.Dim + d];
                    if (v > 0)
                    {
                        Score.Deriv.Data[i * Score.Dim] += -1.0f / (Score.Dim - 1) / Score.BatchSize;
                        Score.Deriv.Data[i * Score.Dim + d] += 1.0f / (Score.Dim - 1) / Score.BatchSize;
                        ObjectiveScore += v;
                    }
                }
            }
            ObjectiveScore = ObjectiveScore / (Score.BatchSize * (Score.Dim - 1));
            Score.Deriv.Data.SyncFromCPU(Score.BatchSize * Score.Dim);
        }
    }
}
