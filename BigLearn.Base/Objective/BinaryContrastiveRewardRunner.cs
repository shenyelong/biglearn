﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class BinaryContrastiveRewardRunner : ObjectiveRunner
    {
        public CudaPieceFloat Label { get; set; }
        public HiddenBatchData[] Data;
        public HiddenBatchData[] Prob;
        public float[] Post;

        //public CudaPieceFloat tmpOutput;

        public float Gamma { get; set; }

        public BinaryContrastiveRewardRunner(CudaPieceFloat label, HiddenBatchData[] data, HiddenBatchData[] prob, float gamma, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Prob = prob;
            Post = new float[Data[0].MAX_BATCHSIZE * Data.Length];

            Label = label;
            Gamma = gamma;

            //tmpOutput = new CudaPieceFloat(Data[0].MAX_BATCHSIZE * Data[0].Dim, true, Behavior.Device == DeviceType.GPU);
            //if (Gamma != 1) { }
        }

        public override void Forward()
        {
            int BatchSize = Data[0].BatchSize;
            Label.SyncToCPU(BatchSize);
            ObjectiveScore = 0;

            for (int d = 0; d < Data.Length; d++)
            {
                //if (Gamma != 1) ComputeLib.Add_Vector(tmpOutput, Data[d].Output.Data, Data[d].BatchSize * Data[d].Dim, 0, Gamma);
                //else tmpOutput = Data[d].Output.Data;

                ComputeLib.Logistic(Data[d].Output.Data, 0, Data[d].Deriv.Data, 0, Data[d].Output.BatchSize * Data[d].Dim, Gamma);
                ComputeLib.ClipVector(Data[d].Deriv.Data, Data[d].Output.BatchSize * Data[d].Dim, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                //ComputeLib.DerivCrossEntropy(tmpOutput, Label, Data[d].Deriv.Data, Data[d].Output.BatchSize);

                //tmpOutput.SyncToCPU(Data[d].BatchSize);
                Prob[d].Output.Data.SyncToCPU(Data[d].BatchSize);
                Data[d].Deriv.Data.SyncToCPU(Data[d].Output.BatchSize * Data[d].Dim);

                double miniBatchLoss = 0;
                for (int b = 0; b < BatchSize; b++)
                {
                    double score = Data[d].Deriv.Data[b];  // (Math.Tanh(tmpOutput[b] / 2) + 1) / 2;

                    double loss = 0;
                    {
                        if (Label[b] > 0) loss = score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(score);
                        else loss = 1 - score < float.Epsilon ? Math.Log(float.Epsilon) : Math.Log(1 - score);
                    }

                    Post[b * Data.Length + d] = (float)Math.Exp(loss) * Prob[d].Output.Data[b];
                    miniBatchLoss += Post[b * Data.Length + d]; // loss * Prob[d].Output.Data[b];
                }
                ObjectiveScore += miniBatchLoss / Data[d].Output.BatchSize; /// batchSize;
                //ComputeLib.Scale_Matrix(Data[d].Deriv.Data, Data[d].BatchSize, Data[d].Dim, (float)1.0 / Data[d].BatchSize * Gamma);
                //ComputeLib.ElementwiseProduct(Data[d].Deriv.Data, Prob[d].Output.Data, Data[d].Deriv.Data, Data[d].BatchSize, 1, 0);
            }

            for (int b = 0; b < BatchSize; b++)
            {
                float sum = float.Epsilon;
                for (int d = 0; d < Data.Length; d++) sum += Post[b * Data.Length + d];

                for (int d = 0; d < Data.Length; d++)
                {
                    Post[b * Data.Length + d] = Post[b * Data.Length + d] / sum;
                    Prob[d].Deriv.Data[b] = (Post[b * Data.Length + d] - Prob[d].Output.Data[b]) / BatchSize;

                    if (Label[b] > 0) Data[d].Deriv.Data[b] = Gamma * (1 - Data[d].Deriv.Data[b]) * Post[b * Data.Length + d] / BatchSize;
                    else Data[d].Deriv.Data[b] = -Gamma * Data[d].Deriv.Data[b] * Post[b * Data.Length + d] / BatchSize;
                }
            }

            for (int d = 0; d < Data.Length; d++)
            {
                Prob[d].Deriv.Data.SyncFromCPU(BatchSize);
                Data[d].Deriv.Data.SyncFromCPU(BatchSize);
            }
        }
    }
}
