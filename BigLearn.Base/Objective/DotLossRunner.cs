using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class DotLossRunner : ObjectiveRunner
    {
        public NdArrayData Data { get; set; }

        public CudaPieceFloat Label { get; set; }

        public DotLossRunner(NdArrayData data, CudaPieceFloat label, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Data = data;
            Label = label;
        }

        public override void Backward(bool isclear)
        {
            if(Data.Deriv != null)
            {
                ComputeLib.Add_Vector(Data.Deriv, Label, Data.Length, 1.0f / Data.Length, 1.0f / Data.Length);
            }
        }

        public override void Forward()
        {
            if(Label.EffectiveSize != Data.Length)
            {
                throw new Exception(string.Format("label EffectiveSize {0} is not matched with data length {1}", Label.EffectiveSize, Data.Length));
            }

            float loss = ComputeLib.DotProduct(Data.Output, Label, Data.Length) * 1.0f / Data.Length;

            ObjectiveScore = loss;
        }
    }
}
