﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// g <- gradient
    /// if ||g|| >= threshold 
    ///    g <- (threshold/||g||) * g
    /// eif
    /// Pascanu et al., On the difficulty of training recurrent neural networks, ICML13
    /// </summary>
    public class SoftClipSGDOptimizer : GradientOptimizer
    {
        float UpdateClip = 0; //threshold

        float WeightClip = 0;

        //public SoftClipSGDOptimizer(RunnerBehavior behavior) : base(behavior) { }
        //public SoftClipSGDOptimizer(float clipUpdate, float clipWeight, RunnerBehavior behavior) : base(behavior)
        //{
        //    UpdateClip = clipUpdate;
        //    WeightClip = clipWeight;
        //}

        public SoftClipSGDOptimizer(CudaPieceFloat weight, CudaPieceFloat gradient, StructureLearner learner, RunnerBehavior behavior) 
            : base(weight, gradient, learner, behavior)
        {
            UpdateClip = learner.ClipDelta;
            WeightClip = learner.ClipWeight;
        }

        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    GradientStep = 1;
        //    UpdateRate = learner.LearnRate;

        //    UpdateClip = learner.ClipDelta;
        //    WeightClip = learner.ClipWeight;
        //    Parameter = weight;
        //    Gradient = new CudaPieceFloat(weight.Size, true, true);
        //}

        //public override void BeforeGradient()
        //{ }

        public override void AfterGradient()
        {
            float norm = 0;
            if (UpdateClip > 0) { norm = ComputeLib.L2Norm(Gradient, Gradient.Size); }
            float rate = UpdateClip > 0 ? UpdateClip / Math.Max(UpdateClip, norm) : 1;
            ComputeLib.Add_Vector(Parameter, Gradient, Parameter.Size, 1, UpdateRate * rate);
            if (WeightClip > 0) { ComputeLib.ClipVector(Parameter, Parameter.Size, WeightClip, -WeightClip); }
            ComputeLib.Zero(Gradient, Gradient.Size);
        }

    }
}
