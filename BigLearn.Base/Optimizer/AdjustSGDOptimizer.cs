﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    //public class AdjustSGDOptimizer : GradientOptimizer
    //{
    //    public AdjustSGDOptimizer(RunnerBehavior behavior) : base(behavior) { }
    //    public static double[] PrecomputeLearningRateSchedule(int nBatch, int nEpoch, double learnRateStart, double learnRateEnd, double accuracy)
    //    {
    //        // Initialization
    //        double[] learningRatePool = new double[nEpoch];
    //        learningRatePool[nEpoch - 1] = learnRateEnd;
    //        double b_min = 0;
    //        double b_max = 0;
    //        int iter = 0;
    //        double b;
    //        bool upper_flag;
    //        bool lower_flag;

    //        if (learnRateEnd > learnRateStart)
    //        {
    //            throw new System.ArgumentException("LearnRateEnd should be smaller than LearnRateStart");
    //        }

    //        // Precompute the optimal b by bi-section
    //        while (Math.Abs(learningRatePool[0] - learnRateStart) > accuracy * learnRateStart)
    //        {
    //            // Upper value of b
    //            b = b_max;
    //            for (int k = (nEpoch - 1); k >= 1; k--)
    //            {
    //                learningRatePool[k - 1] = 0.5 * (1 + 1 / (Math.Pow((1 - learningRatePool[k] * b), 2 * nBatch))) * learningRatePool[k];
    //            }
    //            upper_flag = ((learningRatePool[0] > learnRateStart) || (b * learningRatePool.Max() >= 2)) ? true : false;

    //            // Lower value of b
    //            b = b_min;
    //            for (int k = (nEpoch - 1); k >= 1; k--)
    //            {
    //                learningRatePool[k - 1] = 0.5 * (1 + 1 / (Math.Pow((1 - learningRatePool[k] * b), 2 * nBatch))) * learningRatePool[k];
    //            }
    //            lower_flag = ((learningRatePool[0] <= learnRateStart) || (b * learningRatePool.Max() < 2)) ? true : false;
    //            if (!lower_flag)
    //            {
    //                throw new System.InvalidOperationException("lower_flag cannot be zero");
    //            }

    //            // Update
    //            if (!upper_flag)
    //            {
    //                b_max = b_max + 1;
    //            }
    //            else
    //            {
    //                b = (b_max + b_min) / 2;
    //                for (int k = (nEpoch - 1); k >= 1; k--)
    //                {
    //                    learningRatePool[k - 1] = 0.5 * (1 + 1 / (Math.Pow((1 - learningRatePool[k] * b), 2 * nBatch))) * learningRatePool[k];
    //                }
    //                if ((learningRatePool[0] > learnRateStart) || (b * learningRatePool.Max() > 2))
    //                {
    //                    b_max = b;
    //                }
    //                else
    //                {
    //                    b_min = b;
    //                }
    //            }
    //            iter++;
    //            if (iter > 1e10)
    //            {
    //                throw new System.InvalidOperationException("Maximum number of iterations has reached");
    //            }
    //        }

    //        return learningRatePool;
    //    }

    //    public int StepNum = 0;
    //    public int BatchNum = 0;
    //    public double[] LRList = null;
    //    public override void Init(CudaPieceFloat weight, StructureLearner learner)
    //    {
    //        LRList = PrecomputeLearningRateSchedule(learner.BatchNum, learner.EpochNum, learner.LearnRate, learner.LearnRate * learner.ShrinkLR, 1e-8);
    //        StepNum = 0;
    //        GradientStep = learner.LearnRate;
    //        Gradient = weight;
    //        BatchNum = learner.BatchNum;
    //    }

    //    public override void BeforeGradient()
    //    {
    //        GradientStep = (float)LRList[StepNum / BatchNum];
    //        StepNum++;
    //    }
    //}
}
