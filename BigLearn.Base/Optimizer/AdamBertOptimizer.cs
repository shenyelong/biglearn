using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace BigLearn
{
    /// <summary>
    /// Rf ADAM: A METHOD FOR STOCHASTIC OPTIMIZATION, Kingma et al., ICLR 2015
    /// </summary>
    public class AdamBertOptimizer : GradientOptimizer
    {
        float UpdateClip = 0;
        float WeightClip = 0;

        /// <summary>
        /// Default: 1e-2
        /// </summary>

        private float Beta2 { get; set; }

        private float Epsilon { get; set; }

        private float Beta1 { get; set; }

        private float Decay { get; set; }

        private CudaPieceFloat M = null;

        private CudaPieceFloat V = null;

        public int Count { get; set; }


        public AdamBertOptimizer(CudaPieceFloat weight, CudaPieceFloat gradient, StructureLearner learner, RunnerBehavior behavior) 
            : base(weight, gradient, learner, behavior)
        {
            //learner.Adam_Beta1, learner.Adam_Beta2, learner.Adam_Epsilon, learner.ClipDelta, learner.ClipWeight,
            //UpdateClip = 1.0f; // learner.ClipDelta; // clipDelta;
            
            Beta1 = 0.9f; // learner.Adam_Beta1; // beta1;
            Beta2 = 0.999f; // learner.Adam_Beta2; // beta2;
            Epsilon = 1e-6f; // learner.Adam_Epsilon; // epsilon;

            Decay = learner.WeightDecay; // 0.01f; 

            Count = 0;
            
            M = new CudaPieceFloat(weight.Size, behavior.Device);
            M.Zero();
            V = new CudaPieceFloat(weight.Size, behavior.Device);
            V.Zero();   
        }

        //public override void BeforeGradient()
        //{ }

        public override void AfterGradient()
        {
            Count += 1;

            //float updateRate = (float)(UpdateRate * Math.Sqrt(1 - Math.Pow(Beta2, Count)) / (1 - Math.Pow(Beta1, Count)));  
            ComputeLib.ClipAdamBertUpdate(M, V, Gradient, Parameter, Beta1, Beta2, Epsilon, UpdateRate, UpdateClip, WeightClip, Decay, Parameter.Size);
            // this.Count, this.UpdateRate, this.UpdateClip, this.WeightClip);
        }

    }
}
