﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // public abstract class OptimizerFlow
    // {
    //     public Structure Model { get; set; }

    //     public ComputationGraph CG { get; set; }

    //     public DeviceType Device { get; set; }

    //     public double GoOverEpoch()
    //     {
    //         double Loss = 0;
    //         int batchIdx = 0;
    //         CG.Init();
    //         // while (CG.FetchData())
    //         // {
    //         //     if (!CG.IsContinue) continue;
    //         //     CG.Forward();
    //         //     Loss = batchIdx * 1.0f / (batchIdx + 1) * Loss + 1.0f / (batchIdx + 1) * CG.Loss();
    //         //     if (++batchIdx % 100 == 0)
    //         //     {
    //         //         Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", batchIdx, Loss);
    //         //     }
    //         //     CG.CleanDeriv();
    //         //     CG.Backward();
    //         //     CG.Update();
    //         // }

    //         CG.Complete();
    //         return Loss;
    //     }

    //     public virtual double Run() { return 0; }

    //     public virtual void DecreaseLRonValidationDontImprove(float decreaseRate)
    //     {
    //         foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //             optimizer.Optimizer.GradientStep = optimizer.Optimizer.GradientStep * decreaseRate;
    //     }

    //     public abstract void Init(Structure model, RunnerBehavior behavior, ComputationGraph cg, StructureLearner learner);

    //     public virtual double EvaluateTranningLoss()
    //     {
    //         CG.Init();
    //         double batchLoss = 0;
    //         int cnt = 0;
    //         Random rnd = new Random(0);
    //         // while (CG.FetchData())
    //         // {
    //         //     if (!CG.IsContinue) continue;
    //         //     if (rnd.NextDouble() < 0.1)
    //         //     {
    //         //         batchLoss += CG.ForwardLoss();
    //         //         cnt++;
    //         //     }

    //         //     if (cnt > 200)
    //         //     {
    //         //         break;
    //         //     }
    //         // }

    //         CG.Complete();
    //         return batchLoss / cnt;
    //     }

    //     public virtual void Save(BinaryWriter writer)
    //     {

    //     }

    //     public virtual void Load(BinaryReader reader)
    //     {

    //     }

    //     public static OptimizerFlow CreateOptimizerFlow(Structure model, ComputationGraph cg, DNNOptimizerType optimizer, StructureLearner learner, RunnerBehavior behavior)
    //     {
    //         OptimizerFlow optimizerFlow = null;
    //         switch (optimizer)
    //         {
    //             case DNNOptimizerType.SGD:
    //             case DNNOptimizerType.AdaGrad:
    //             case DNNOptimizerType.Momentum:
    //             case DNNOptimizerType.NVMomentum:
    //             case DNNOptimizerType.RMSProp:
    //             case DNNOptimizerType.AdaDelta:
    //             case DNNOptimizerType.Adam:
    //             case DNNOptimizerType.AdaMax:
    //                 throw new Exception("Optimizer Flow is staled for local optimization method.");
    //                 //optimizerFlow = new SimpleOptimizerFlow();
    //                 //break;
    //             //case DNNOptimizerType.DistASGD:
    //             //    optimizerFlow = new DistASGDOptimizerFlow();
    //             //    break;
    //             //case DNNOptimizerType.DistMASGD:
    //             //    optimizerFlow = new DistMAOptimizerFlow();
    //             //    break;
    //             //case DNNOptimizerType.BlockMomentum:
    //             //    optimizerFlow = new BlockMomentumOptimizerFlow();
    //             //    break;
    //             //case DNNOptimizerType.BlockAdam:
    //             //    optimizerFlow = new BlockAdamOptimizerFlow();
    //             //    break;
    //             default:
    //                 Logger.WriteLog("The Optimizer does not support for optimizer flow! {0} ", optimizer);
    //                 break;
    //         }

    //         optimizerFlow.Init(model, behavior, cg, learner);
    //         return optimizerFlow;
    //     }
    // }
}
