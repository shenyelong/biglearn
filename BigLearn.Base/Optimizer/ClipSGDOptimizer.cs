﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ClipSGDOptimizer : GradientOptimizer
    {
        float UpdateClip = 0;
        float WeightClip = 0;
        
        // public ClipSGDOptimizer(CudaPieceFloat weight, float learnRate, float clipDelta, float clipWeight, RunnerBehavior behavior) : this(clipDelta, clipWeight, behavior)
        // {
        //     SetupConfig(weight, new CudaPieceFloat(weight.Size, behavior.Device), new StructureLearner() { LearnRate = learnRate, device = behavior.Device });
        // }
        // public ClipSGDOptimizer(float clipDelta, float clipWeight, RunnerBehavior behavior) : base(behavior)
        // {
        //     UpdateClip = clipDelta;
        //     WeightClip = clipWeight;
        // }

        public ClipSGDOptimizer(CudaPieceFloat weight, CudaPieceFloat gradient, StructureLearner learner, RunnerBehavior behavior) 
            : base(weight, gradient, learner, behavior)
        {
            UpdateClip = learner.ClipDelta; // clipDelta;
            WeightClip = learner.ClipWeight; // clipWeight;
        }

        //public override void BeforeGradient()
        //{
            //WeightUpdate.Zero();
        //}

        public override void AfterGradient()
        {
            if (UpdateClip > 0) ComputeLib.ClipVector(Gradient, Gradient.Size, UpdateClip, -UpdateClip);
            //Cudalib.Clip_Vector(WeightUpdate.CudaPtr, WeightUpdate.Size, UpdateClip, -UpdateClip);
            ComputeLib.Add_Vector(Parameter, Gradient, Parameter.Size, 1, UpdateRate);
            ComputeLib.Zero(Gradient, Gradient.Size);
            //Cudalib.Add_Vector(Parameter.CudaPtr, WeightUpdate.CudaPtr, (uint)Parameter.Size, 1, 1);
            if (WeightClip > 0) ComputeLib.ClipVector(Parameter, Parameter.Size, WeightClip, -WeightClip);
            //Cudalib.Clip_Vector(Parameter.CudaPtr, Parameter.Size, WeightClip, -WeightClip);
        }

    }
}
