﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Rf ADAM: A METHOD FOR STOCHASTIC OPTIMIZATION, Kingma et al., ICLR 2015
    /// </summary>
    public class AdaMaxOptimizer : GradientOptimizer
    {
        float UpdateClip = 0;
        float WeightClip = 0;
        public float Beta2 { get; set; }
        public float Epsilon { get; set; }
        //public float Alpha { get; set; }
        public float Beta1 { get; set; }
        public int Count { get; set; }

        public float Decay = 0;

        CudaPieceFloat M = null;
        CudaPieceFloat V = null;

        public AdaMaxOptimizer(CudaPieceFloat weight, CudaPieceFloat gradient, StructureLearner learner, RunnerBehavior behavior)
            : base(weight, gradient, learner, behavior)
        {
            //Weight.Size, learner.ClipDelta, learner.ClipWeight,

            UpdateClip = learner.ClipDelta; // clipDelta;
            WeightClip = learner.ClipWeight; // clipWeight;

            M = new CudaPieceFloat(weight.Size, behavior.Device);
            M.Zero();
            V = new CudaPieceFloat(weight.Size, behavior.Device);
            V.Zero();

            Decay = learner.WeightDecay;

            //Alpha = 1.0e-2f;
            Beta2 = 0.999f;
            Beta1 = 0.9f;
            Epsilon = 1.0e-8f;
            Count = 0;
        }

        //public AdaMaxOptimizer(int weightSize, float clipDelta, float clipWeight, RunnerBehavior behavior) : base(behavior)
        //{  
        //}

        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    UpdateClip = learner.ClipDelta;
        //    WeightClip = learner.ClipWeight;
        //    M = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    M.Zero();
        //    V = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    V.Zero();

        //    Parameter = weight;
        //    UpdateRate = learner.LearnRate;

        //    Gradient = new CudaPieceFloat(weight.Size, true, learner.device == DeviceType.GPU);
        //    Gradient.Zero();
        //    Alpha = 1.0e-2f;
        //    Beta2 = 0.999f;
        //    Beta1 = 0.95f;
        //    Epsilon = 1.0e-8f;
        //    Count = 0;
        //    GradientStep = 1;
        //    Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(weight.Size);
        //}

        public override void AfterGradient()
        {
            Count += 1;

            if (UpdateClip > 0) { ComputeLib.ClipVector(Gradient, Gradient.Size, UpdateClip, -UpdateClip); }

            //float Alpha = (float)(UpdateRate / (1 - Math.Pow(Beta1, Count)));

            //M = Beta1 * M + ( 1 - Beta1 ) * g
            ComputeLib.Add_Vector(M, Gradient, Parameter.Size, Beta1, 1 - Beta1);
            
            //V = max(beta2 * V, |g|)
            //Gradient =   M/V 

            ComputeLib.AdaMax(V, M, Gradient, 1.0f, Beta2, Epsilon, Parameter.Size);

            ComputeLib.Add_Vector(Parameter, Gradient, Parameter.Size, UpdateRate * (1 - Decay), UpdateRate);

            if (WeightClip > 0) { ComputeLib.ClipVector(Parameter, Parameter.Size, WeightClip, - WeightClip); }

            ComputeLib.Zero(Gradient, Gradient.Size);
        }

    }
}
