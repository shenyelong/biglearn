using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace BigLearn
{
    /// <summary>
    /// Meta Learning for optimization.
    /// </summary>
    public class MetaAdamOptimizer : GradientOptimizer
    {
        /// <summary>
        /// Default: 1e-2
        /// </summary>
        private float Beta2 { get; set; }
        private float Beta1 { get; set; }
        private float Epsilon { get; set; }
        public int Count { get; set; }

        private CudaPieceFloat M = null;
        private CudaPieceFloat V = null;
    
        // automatically schedule learning rate.          
        public MetaAdamOptimizer(CudaPieceFloat weight, CudaPieceFloat gradient, StructureLearner learner, RunnerBehavior behavior) 
            : base(weight, gradient, learner, behavior)
        {
            Beta1 = learner.Adam_Beta1; // beta1;
            Beta2 = learner.Adam_Beta2; // beta2;
            Epsilon = learner.Adam_Epsilon; // epsilon;
            Count = 0;
            
            M = new CudaPieceFloat(weight.Size, behavior.Device);
            M.Zero();
            V = new CudaPieceFloat(weight.Size, behavior.Device);
            V.Zero();   
        }

        public override void AfterGradient()
        {
            metaParam.Output.SyncToCPU();
            metaParam.Deriv.SyncToCPU();

            Count += 1;
            ComputeLib.ClipAdamDeltaUpdate(M, V, Gradient, Parameter, Beta1, Beta2, Count, UpdateRate, UpdateClip, WeightClip); 
            
            metaParam.Deriv.SyncFromCPU();
        }
    }
}
