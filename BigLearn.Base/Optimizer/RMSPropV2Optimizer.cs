﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// ada = gamma * ada + (1 - gamma) * g^2
    /// w = w - (alpha/(sqrt(ada) + Epsilon)) * g
    /// </summary>
    public class RMSPropV2Optimizer: GradientOptimizer
    {
        public float Gamma {get; set;}
        public float Epsilon {get; set;}
        CudaPieceFloat Ada = null;
        CudaPieceFloat G = null;

        float UpdateClip = 0;
        float WeightClip = 0;

        public RMSPropV2Optimizer(CudaPieceFloat weight, CudaPieceFloat gradient, StructureLearner learner, RunnerBehavior behavior) 
            : base(weight, gradient, learner, behavior)
        {
            //Weight.Size, learner.RmsProp_Decay, learner.RmsProp_Decay, learner.ClipDelta, learner.ClipWeight

            UpdateClip = learner.ClipDelta; // clipDelta;
            WeightClip = learner.ClipWeight; // clipWeight;

            Ada = new CudaPieceFloat(weight.Size, behavior.Device);
            Ada.Init(learner.RmsProp_Epsilon);

            G = new CudaPieceFloat(weight.Size, behavior.Device);
            G.Init(0);

            Gamma = learner.RmsProp_Decay; // decay; // 0.995f; //Gamma = 0, it is the same as AdaGrad
            Epsilon = learner.RmsProp_Epsilon;
        }

        //public RMSPropV2Optimizer(int weightSize, float decay, float epsilon, float clipDelta, float clipWeight, RunnerBehavior behavior) : base(behavior)
        //{ }
        
        /// <summary>
        /// https://groups.google.com/forum/#!topic/deep-q-learning/_RFrmUALBQo
        /// </summary>
        public override void AfterGradient()
        {
            if (UpdateClip > 0) ComputeLib.ClipVector(Gradient, Gradient.Size, UpdateClip, -UpdateClip);
            
            ComputeLib.RMSPropV2_Gradient(Ada, G, Gradient, Gamma, Epsilon, Gradient.Size);

            ComputeLib.Add_Vector(Parameter, Gradient, Parameter.Size, 1, UpdateRate);
            
            if (WeightClip > 0) ComputeLib.ClipVector(Parameter, Parameter.Size, WeightClip, -WeightClip);
            ComputeLib.Zero(Gradient, Gradient.Size);
        }
   }
}
