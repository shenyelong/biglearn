﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LBFGS
    {
        int M = 10;
        int IterM = 0;
        CudaPieceFloat[] s = null;
        CudaPieceFloat[] y = null;
        double[] rou = null;

        CudaPieceFloat preWeight = null;
        CudaPieceFloat preGradient = null;
        CudaPieceFloat Q = null;

        public CudaPieceFloat CurrentWeight { get { return preWeight; } }

        DeviceType Device = DeviceType.GPU;
        public void Init(int _M, int featurenum, DeviceType device)
        {
            Device = device;
            M = _M;
            s = new CudaPieceFloat[M];
            y = new CudaPieceFloat[M];
            rou = new double[M];
            for (int i = 0; i < M; i++)
            {
                s[i] = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);  //VectorFactory.Create(featurenum);
                y[i] = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
            }
            preWeight = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
            preGradient = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
            Q = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
        }

        public static double DotProduct(CudaPieceFloat s1, CudaPieceFloat s2)
        {
            CudaPieceFloat r = new CudaPieceFloat(1, true, true);
            Cudalib.InnerProduct_Similarity(s1.CudaPtr, s2.CudaPtr, r.CudaPtr, 1, s1.Size);
            r.CopyOutFromCuda();
            double result = r[0];
            r.Dispose();
            return result;
        }

        public CudaPieceFloat TwoLoop(CudaPieceFloat gradient, CudaPieceFloat[] s, CudaPieceFloat[] y, double[] rou, int history, int cursor, int _M)
        {
            //gradient.CopyOutFromCuda();
            Q.CopyFrom(gradient);
            //Cudalib.Scale_Vector(Q.CudaPtr, gradient.Size, -1.0f);

            if (history > 0)
            {
                int idx;

                double[] alpha = new double[history];
                for (int i = 0; i < history; i++)
                {
                    idx = (cursor - i + _M - 1) % _M;
                    alpha[i] = rou[idx] * DotProduct(Q, s[idx]);
                    Cudalib.Add_Vector(Q.CudaPtr, y[idx].CudaPtr, y[idx].Size, 1, (float)-alpha[i]);
                }

                idx = (cursor - 1 + _M) % _M;

                double hk0 = rou[idx] * DotProduct(y[idx], y[idx]); // y[lastrou].DotProduct(y[lastrou]);
                hk0 = 1.0 / hk0;
                //q.Scale((float)hk0);
                Cudalib.Scale_Vector(Q.CudaPtr, Q.Size, (float)hk0);

                for (int i = 0; i < history; i++)
                {
                    idx = (cursor - history + i + _M) % _M;
                    //if (rou[idx] <= 0) continue;
                    double beta = rou[idx] * DotProduct(Q, y[idx]);
                    Cudalib.Add_Vector(Q.CudaPtr, s[idx].CudaPtr, Q.Size, 1, (float)(alpha[history - i - 1] - beta));
                }
            }
            return Q;
        }

        public CudaPieceFloat CalculateDirection(CudaPieceFloat param, CudaPieceFloat gradient)
        {
            UpdateState(param, gradient);
            CudaPieceFloat direction = TwoLoop(gradient, s, y, rou, Math.Min(M, IterM), IterM % M, M);
            IterM += 1;
            return direction;
        }

        void UpdateState(CudaPieceFloat weight, CudaPieceFloat gradient)
        {
            //weight.CopyOutFromCuda();
            //gradient.CopyOutFromCuda();

            if (IterM == 0)
            {
                preWeight.CopyFrom(weight);
                preGradient.CopyFrom(gradient);
            }
            else
            {
                int cursor = (IterM - 1) % M;
                y[cursor].CopyFrom(gradient);
                Cudalib.Add_Vector(y[cursor].CudaPtr, preGradient.CudaPtr, gradient.Size, 1.0f, -1.0f);
                preGradient.CopyFrom(gradient);

                s[cursor].CopyFrom(weight);
                Cudalib.Add_Vector(s[cursor].CudaPtr, preWeight.CudaPtr, weight.Size, 1.0f, -1.0f);
                preWeight.CopyFrom(weight);

                double s0y0 = DotProduct(s[cursor], y[cursor]);
                if (s0y0 == 0)
                {
                    Console.WriteLine("s0y0 converged after :{1} iterations", IterM);
                    Console.WriteLine("###########################################################################");
                }
                rou[cursor] = 1.0 / s0y0;
            }
        }
    }
}
