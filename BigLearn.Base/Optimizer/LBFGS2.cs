﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LBFGS2 
    {
        int M = 10;
        public float CL1;
        public float CL2;
        int IterM = 0;
        CudaPieceFloat[] s = null;
        CudaPieceFloat[] y = null;
        double[] rou = null;

        CudaPieceFloat preWeight = null;
        CudaPieceFloat preGradient = null;
        CudaPieceFloat Q = null;
        CudaPieceFloat pseudoGrad = null;

        double gd = 0.0;
        public CudaPieceFloat CurrentWeight { get { return preWeight; } }

        DeviceType Device = DeviceType.GPU;
        public void Init(int _M, int featurenum, DeviceType device, float _CL1 = 0f, float _CL2 = 0f)
        {
            Device = device;
            M = _M;
            CL1 = _CL1;
            CL2 = _CL2;
            s = new CudaPieceFloat[M];
            y = new CudaPieceFloat[M];
            rou = new double[M];
            for (int i = 0; i < M; i++)
            {
                s[i] = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
                y[i] = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
            }
            preWeight = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
            preGradient = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
            Q = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
            if (CL1 > 0)
            {
                pseudoGrad = new CudaPieceFloat(featurenum, true, device == DeviceType.GPU);
            }
        }

        public static double DotProduct(CudaPieceFloat s1, CudaPieceFloat s2)
        {
            CudaPieceFloat r = new CudaPieceFloat(1, true, true);
            Cudalib.InnerProduct_Similarity(s1.CudaPtr, s2.CudaPtr, r.CudaPtr, 1, s1.Size);
            r.CopyOutFromCuda();
            double result = r[0];
            r.Dispose();
            return result;
        }

        public static double L1Norm(CudaPieceFloat s1)
        {
            CudaPieceFloat r = new CudaPieceFloat(1, true, true);
            Cudalib.L1Norm(s1.CudaPtr, r.CudaPtr, s1.Size);
            r.CopyOutFromCuda();
            double result = r[0];
            r.Dispose();
            return result;
        }

        public static double L2NormSquare(CudaPieceFloat s1)
        {
            return DotProduct(s1, s1);
        }

        public static int CountZero(CudaPieceFloat s1)
        {
            CudaPieceInt r = new CudaPieceInt(1, true, true);
            Cudalib.CountZero(s1.CudaPtr, r.CudaPtr, s1.Size);
            r.CopyOutFromCuda();
            int result = r[0];
            r.Dispose();
            return result;
        }

        public CudaPieceFloat TwoLoop(CudaPieceFloat gradient, CudaPieceFloat[] s, CudaPieceFloat[] y, double[] rou, int history)
        {
            Cudalib.L1Reg_CalcPseudoGradient(this.preWeight.CudaPtr, gradient.CudaPtr, Q.CudaPtr, this.CL1, this.CL2, gradient.Size);
            if (this.CL1 > 0)
            {
                Cudalib.CudaCopy(pseudoGrad.CudaPtr, Q.CudaPtr, Q.Size, 0, 0);                
            }

            Cudalib.Scale_Vector(Q.CudaPtr, Q.Size, -1);

            if (history > 0)
            {
                double[] alpha = new double[history];
                int lastrou = -1;
                for (int i = 0; i < history; i++)
                {
                    if (rou[i] > 0)
                    {
                        alpha[i] = rou[i] * DotProduct(Q, s[i]);
                        Cudalib.Add_Vector(Q.CudaPtr, y[i].CudaPtr, y[i].Size, 1, (float)-alpha[i]);
                        if (lastrou == -1)
                            lastrou = i;
                    }
                    else
                    {
                        Console.WriteLine("##########Invlaid rou[{0}]={1}############", i, rou[i]);
                    }
                }

                if (lastrou != -1)
                {
                    double hk0 = rou[lastrou] * DotProduct(y[lastrou], y[lastrou]); // y[lastrou].DotProduct(y[lastrou]);
                    hk0 = 1.0 / hk0;
                    Cudalib.Scale_Vector(Q.CudaPtr, Q.Size, (float)hk0);

                    for (int i = history - 1; i >= 0; i--)
                    {
                        if (rou[i] <= 0)
                        {
                            Console.WriteLine("##########Skip ill-condition############");
                            continue;
                        }

                        double beta = alpha[i] - rou[i] * DotProduct(Q, y[i]);
                        Cudalib.Add_Vector(Q.CudaPtr, s[i].CudaPtr, Q.Size, 1, (float)beta);
                    }

                    if (this.CL1 > 0)
                    {
                        //Check sign
                        Cudalib.L1Reg_CheckSign(this.pseudoGrad.CudaPtr, Q.CudaPtr, Q.Size, 1, 1);
                    }
                }
            }

            return Q;
        }

        public CudaPieceFloat CalculateDirection(CudaPieceFloat param, CudaPieceFloat gradient)
        {
            UpdateState(param, gradient);
            CudaPieceFloat direction = TwoLoop(gradient, s, y, rou, Math.Min(M, IterM));
            this.gd = DotProduct(direction, this.preGradient);
            System.Diagnostics.Debug.Assert(this.gd < 0, "GD must be negative");
            IterM += 1;
            return direction;
        }

        void UpdateState(CudaPieceFloat weight, CudaPieceFloat gradient)
        {
            weight.CopyOutFromCuda();
            gradient.CopyOutFromCuda();
            if (IterM == 0)
            {
                preWeight.CopyFrom(weight);
                preGradient.CopyFrom(gradient);
            }
            else
            {
                CudaPieceFloat sTmp = s[M - 1];
                CudaPieceFloat yTmp = y[M - 1];
                for (int i = M - 2; i >= 0; i--)
                {
                    s[i + 1] = s[i];
                    y[i + 1] = y[i];
                    rou[i + 1] = rou[i];
                }

                s[0] = sTmp;
                y[0] = yTmp;

                s[0].CopyFrom(weight);
                Cudalib.Add_Vector(s[0].CudaPtr, preWeight.CudaPtr, weight.Size, 1.0f, -1.0f);
                preWeight.CopyFrom(weight);

                y[0].CopyFrom(gradient);
                Cudalib.Add_Vector(y[0].CudaPtr, preGradient.CudaPtr, gradient.Size, 1.0f, -1.0f);
                preGradient.CopyFrom(gradient);

                double s0y0 = DotProduct(s[0], y[0]);
                if (s0y0 <= 0)
                {
                    Console.WriteLine("s0y0 converged after :{0} iterations", IterM);
                    Console.WriteLine("###########################################################################");
                }
                rou[0] = 1.0 / s0y0;
            }
        }

        public void RegulizeNewWeight(CudaPieceFloat newWeight)
        {
            if (this.CL1 > 0)
            {
                Cudalib.L1Reg_CheckSign(preWeight.CudaPtr, newWeight.CudaPtr, preWeight.Size, 0, 0);          
            }
        }

        public double GetRegLoss(CudaPieceFloat parameter)
        {
            double regLoss = 0;
            if (this.CL1 > 0)
            {
                regLoss += L1Norm(parameter) * this.CL1;
            }

            if(this.CL2>0)
            {
                regLoss += L2NormSquare(parameter) * this.CL2*0.5;
            }

            return regLoss;
        }

        public Tuple<float, float> LineSearch(CudaPieceFloat parameter, CudaPieceFloat direction, ObjectiveFunction func, object par, double currentV, float initStepSize,
    float discount, float threshouldStepSize, int batchNum)
        {
            preWeight.CopyOutFromCuda();
            if (CL1 > 0)
                pseudoGrad.CopyOutFromCuda();

            float stepSize = initStepSize;
            double currentf = currentV; //.currentBestloss;
            Cudalib.Add_Vector(parameter.CudaPtr, direction.CudaPtr, parameter.Size, 1, stepSize);
            RegulizeNewWeight(parameter);
            int idx = 0;
            Console.WriteLine("Start Linesearch at:{0}", DateTime.Now);
            DateTime pre = DateTime.Now;
            while (Math.Abs(stepSize) > threshouldStepSize)
            {
                double l1Norm = LBFGS2.L1Norm(parameter);
                double newf = func(par, parameter) / batchNum + this.GetRegLoss(parameter);
                double Armijo_threshold = currentf + this.gd * 0.00001 * stepSize;
                if (newf < Armijo_threshold)
                {
                    currentf = newf;
                    break;
                }
                else
                {
                    Cudalib.Add_Vector(parameter.CudaPtr, direction.CudaPtr, parameter.Size, 1, -stepSize + discount * stepSize);
                    RegulizeNewWeight(parameter);

                    stepSize *= discount;
                    idx++;
                }
            }
            
            if (Math.Abs(stepSize) <= threshouldStepSize)
            {
                Console.WriteLine("####Line search failed. {0}:{1}######", stepSize, threshouldStepSize);
                parameter.CopyFrom(this.preWeight);
            }

            int count = CountZero(parameter);
            Console.WriteLine("Finish linesearch take:{0} seconds, {1} times with step size:{2}, Zeror count:{3}", DateTime.Now.Subtract(pre).TotalSeconds, idx, stepSize, count);
            return Tuple.Create((float)currentf, stepSize);
        }
    }
}
