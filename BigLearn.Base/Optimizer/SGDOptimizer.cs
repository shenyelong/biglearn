﻿using BigLearn;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class SGDOptimizer : GradientOptimizer
    {
        float MomentumRate = 0;
        float WeightDecay = 0;

        CudaPieceFloat Delta = null;

        public SGDOptimizer(CudaPieceFloat weight, CudaPieceFloat grad, StructureLearner learner, RunnerBehavior behavior) : base(weight, grad, learner, behavior)
        {
            MomentumRate = learner.MomemtumRate; // momentum;
            WeightDecay = learner.WeightDecay;
            
            Delta = new CudaPieceFloat(weight.Size, behavior.Device);
            Delta.Init(0);
        }

        public override void AfterGradient()
        {   
            ComputeLib.Add_Vector(Gradient, Parameter, Parameter.Size, 1, -WeightDecay); 

            // a = a * awei + b * bwei;
            ComputeLib.Add_Vector(Delta, Gradient, Parameter.Size, MomentumRate, UpdateRate);
            //if(WeightDecay > 1)
            //{
            // ComputeLib.Add_Vector(Delta, Parameter, Parameter.Size, 1, -UpdateRate * WeightDecay);
            //}
            ComputeLib.Add_Vector(Parameter, Delta, Parameter.Size, 1, 1);
            ComputeLib.Zero(Gradient, Parameter.Size);
        }
    }
}
