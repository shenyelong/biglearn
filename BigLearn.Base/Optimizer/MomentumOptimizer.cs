﻿using BigLearn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MomentumOptimizer : GradientOptimizer
    {
        float MomentumRate = 0;

        //public MomentumOptimizer(RunnerBehavior behavior) : base(behavior) { }

        public MomentumOptimizer(CudaPieceFloat weight, CudaPieceFloat gradient, StructureLearner learner, RunnerBehavior behavior)
            : base(weight, gradient, learner, behavior)
             // float momentum, float lr, RunnerBehavior behavior) : base(behavior)
        {
            MomentumRate = learner.MomemtumRate; // momentum;
        }

        //public override void Init(CudaPieceFloat weight, StructureLearner learner)
        //{
        //    GradientStep = 1;
        //    UpdateRate = learner.LearnRate;
        //    Parameter = weight;
        //    Gradient = new CudaPieceFloat(weight.Size, true, this.Behavior.Device == DeviceType.GPU);
        //    this.SetArgs(learner.OptArgs);
        //}

        public override void AfterGradient()
        {
            ComputeLib.Add_Vector(Parameter, Gradient, Parameter.Size, 1, UpdateRate);

            //        /// b = bwei b + awei * a; 
            ComputeLib.Scale_Vector(Gradient, 0, Gradient, 0, Gradient.Size, 0, MomentumRate); 
            
            // CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei);
            //Scale_Matrix(Gradient, Gradient.Size, 1, MomentumRate);

            //float beta = this.MomentumRate;
            //ComputeLib.MomentumUpdate(Parameter, Gradient, beta);
        }


        //private void SetArgs(string args)
        //{
        //    args = args ?? string.Empty;
        //    string[] argPairs = args.Split(',');
        //    NamedArgsParser parser = new NamedArgsParser(argPairs);
        //    this.MomentumRate = parser.Get("MomentumRate", 0.99f);
        //}
    }
}
