using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    /// <summary>
    /// Basic Structure Parameters. 
    /// </summary>
    public class Structure : IData, IFlatOptimizer 
    {        
        public virtual DataSourceID Type { get; set; }
        public virtual DeviceType DeviceType { get ; set; }

        /// <summary>
        /// Sub Structure. 
        /// </summary>
        public virtual List<Structure> SubStructure { get { return new List<Structure>(); } }

        public Dictionary<string, NdArrayData> Parameter = new Dictionary<string, NdArrayData>();
        public void AddParameter(string name, NdArrayData param)
        {
            Parameter.Add(name, param);
        }
                
        public List<NdArrayData> Parameters { get {
            List<NdArrayData> mParameters = new List<NdArrayData>();
            mParameters.AddRange(Parameter.Select(i => i.Value));
            foreach(Structure sub in SubStructure) mParameters.AddRange(sub.Parameters);
            return mParameters;
            } }

        /// <summary>
        /// Current Structure Optimizer. (this should be staled)
        /// </summary>
        public Dictionary<string, GradientOptimizer> StructureOptimizer = new Dictionary<string, GradientOptimizer>();

        /// <summary>
        /// enum all optimizers in the model structure. (this should be staled)
        /// </summary>
        public List<GradientOptimizer> ModelOptimizers { get {
                List<GradientOptimizer> mOptimizers = new List<GradientOptimizer>();
                mOptimizers.AddRange(StructureOptimizer.Select(i => i.Value));
                foreach (Structure sub in SubStructure) mOptimizers.AddRange(sub.ModelOptimizers);
                return mOptimizers;
            } }
	
        public StructureLearner Learner { get; set; }         
    	
        // staled.
    	public void AllocateOptimizer(StructureLearner learner, RunnerBehavior behavior)
    	{
           Learner = learner;

    	   foreach(KeyValuePair<string, NdArrayData> p in Parameter)
    	   {
                if(p.Value.Deriv == null)
        		{
        		    throw new Exception(string.Format("{0} throw an exception : grad is null.", p.Key));
                }

                if(StructureOptimizer.ContainsKey(p.Key))
                {
                    Console.WriteLine("WARNING : Structure {0} with gradient is already set to optimizer {1}. skip! ", p.Key, StructureOptimizer[p.Key].ToString());
                    continue;
                }

                GradientOptimizer Optimizer = GradientOptimizer.CreateLocalOptimizer(p.Value.Output, p.Value.Deriv, learner, behavior);

        		Console.WriteLine("Structure {0} with gradient is set to optimizer {1} done! ", p.Key, Optimizer.ToString());
        	   	StructureOptimizer.Add(p.Key, Optimizer);
    	   }
    	   foreach(Structure sub in SubStructure) sub.AllocateOptimizer(learner, behavior);
    	}
    	
        

        //    get
        //    {
        //        int paramNum = 0;
        //        foreach (GradientOptimizer opt in ModelOptimizers) { paramNum += opt.Parameter.Size; }
        //        return paramNum;
        //    }
        //}

        public CudaPieceFloat Grad { get; set; }	
    	public CudaPieceFloat AllocateGradient(DeviceType device)
    	{
    	    List<NdArrayData> p = Parameters;
    	    int pNum = p.Select(i => i.Length).Sum(); // ParamNum;
    	    Console.WriteLine("allocate gradient size {0}",pNum);
    	   
            CudaPieceFloat grad = new CudaPieceFloat(pNum, device);
    	    Grad = grad;
            
    	    //grad.Init(0);
            int offset = 0;
            for(int i = 0; i < p.Count; i++)
    	    {
    		    p[i].Deriv = grad.SubArray(offset, p[i].Length); //  new CudaPieceFloat(p[i].Size, device); // grad.SubArray(offset, p[i].Size);
    		    if(p[i].Length == 0) throw new Exception("parameter size is zero");
    		    offset = offset + p[i].Length;
    	    }
    	    if(offset != pNum) throw new Exception("parameter number doesn't match");
            return grad;
    	}
	
        CudaPieceFloat AllocateGradient(CudaPieceFloat grad, DeviceType device)
        {
            List<NdArrayData> p = Parameters;
            int pNum = p.Select(i => i.Length).Sum(); // ParamNum;
            Console.WriteLine("allocate gradient size {0}", pNum);
           
            if(grad.Size != pNum) throw new Exception("gradient number doesn't match");
           
            //CudaPieceFloat grad = new CudaPieceFloat(pNum, device);
            Grad = grad;
         
           //grad.Init(0);
            int offset = 0;
            for(int i=0; i<p.Count; i++)
            {
                //p[i].Grad = grad.SubArray(offset, p[i].Size); //  new CudaPieceFloat(p[i].Size, device); // grad.SubArray(offset, p[i].Size);
                p[i].Deriv = grad.SubArray(offset, p[i].Length); //  new CudaPieceFloat(p[i].Size, device); // grad.SubArray(offset, p[i].Size);
                if(p[i].Length == 0) throw new Exception("parameter size is zero");
                offset = offset + p[i].Length;
            }
            if(offset != pNum) throw new Exception("parameter number doesn't match");
            return grad;
        }

        public void Dispose() { }

        /// <summary>
        /// Get Model Gradient.
        /// </summary>
        /// <param name="grad"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public int GetModelGradient(CudaPieceFloat grad, int index)
        {
            int start = index;
            foreach (NdArrayData opt in Parameters)
            {
                grad.CopyFrom(index, opt.Deriv, 0, opt.Deriv.Size);
                index += opt.Deriv.Size;
            }
            return index - start;
        }

        public int GetModelParameter(CudaPieceFloat param, int index)
        {
            int start = index;
            foreach(NdArrayData opt in Parameters)
            {
                param.CopyFrom(index, opt.Output, 0, opt.Output.Size);
                index += opt.Output.Size;
            }
            return index - start;
        }

        public int SetModelParameter(CudaPieceFloat param, int index)
        {
            int start = index;
            foreach (NdArrayData opt in Parameters)
            {
                opt.Output.CopyFrom(0, param, index, opt.Output.Size);
                index += opt.Output.Size;
            }
            return index - start;
        }

        public int SetModelGradient(CudaPieceFloat grad, int index)
        {
            int start = index;
            foreach (NdArrayData opt in Parameters)
            {
                opt.Deriv.CopyFrom(0, grad, index, opt.Deriv.Size);
                index += opt.Deriv.Size;
            }
            return index - start;
        }

        public int GetGradientSize()
        {
            return Parameters.Select(i => i.Output.Size).Sum();
        }

        /// <summary>
        /// Model Parameter Number.
        /// </summary>
        public int ParamNumber { get {
            return Parameters.Select(i => i.Output.Size).Sum();
            } }


        /// <summary>
        /// Set Model Gradient to Zero;
        /// </summary>
        /// <param name="learner"></param>
        public void ClearModelGradient()
        {
            foreach (NdArrayData data in Parameters)
            {
                data.Deriv.Init(0);
                //subOptimizer.ComputeLib.Zero(subOptimizer.Gradient, subOptimizer.Gradient.Size);
            }
        }              


        public Structure() { /*InitStructureOptimizer();*/ }
        public Structure(DeviceType device) { DeviceType = device; }

        public static Structure Empty { get { return new Structure(); } }

        //protected virtual void InitStructureOptimizer() { }
	
	    public CudaPieceFloat InitOptimizer(StructureLearner learner, CudaPieceFloat grad, RunnerBehavior behavior)
        {
            AllocateGradient(grad, behavior.Device);
            AllocateOptimizer(learner, behavior);
            return Grad;
        }

        public CudaPieceFloat InitOptimizer(StructureLearner learner, RunnerBehavior behavior)
        {
	        AllocateGradient(behavior.Device);
            AllocateOptimizer(learner, behavior);
            return Grad;
	    }

        public virtual IMetaInfo Meta { get; set; }
        public virtual IData CloneAs(DeviceType deviceType) { throw new NotImplementedException(); }
        
        public virtual void CopyFrom(IData other) 
        {
            Structure src = ((Structure)other);
            List<NdArrayData> src_params = src.Parameters;
            List<NdArrayData> self_params = Parameters;

            if(src_params.Count != self_params.Count)
            {
                throw new Exception(string.Format("the source parameter count {0} doesn't math the target parameter count {1}", src_params.Count, self_params.Count)); 
            }

            for(int i = 0; i < self_params.Count; i++)
            {
                self_params[i].Output.CopyFrom(src_params[i].Output);
            }
            
            //throw new NotImplementedException(); 
        }

        #region IOUnit Interface.
        //public virtual IOUnitHeader Head { get; set; }

        public virtual void Load(BinaryReader input) { throw new NotImplementedException(); }
        public virtual void Save(BinaryWriter output) { throw new NotImplementedException(); }

        public virtual void Save(string fileName)
        {
            using (BinaryWriter mwriter = new BinaryWriter(new FileStream(fileName, FileMode.Create, FileAccess.Write)))
            {
                Serialize(mwriter);
            }
        }
    
        public virtual void Load(string fileName)
        {
            using(BinaryReader mreader = new BinaryReader(new FileStream(fileName, FileMode.Open, FileAccess.Read)))
            {
                Deserialize(mreader);
            } 
        }   

        public virtual void Serialize(BinaryWriter writer) 
        {
            foreach(NdArrayData param in Parameters) 
            {
                param.Output.Serialize(writer);
            }
        }

        public virtual void Deserialize(BinaryReader reader) 
        {
            foreach(NdArrayData param in Parameters) 
            {
                param.Output.Deserialize(reader);
            }
        }

        public virtual void SyncFromCPU() 
        {
            foreach(NdArrayData param in Parameters) 
            {
                param.Output.SyncFromCPU();
            }
            //throw new NotImplementedException(); 
        }
        
        public virtual void SyncToCPU() 
        {
            foreach(NdArrayData param in Parameters) 
            {
                param.Output.SyncToCPU();
            }
            //throw new NotImplementedException(); 
        }

        #endregion

        #region Global Optimizer Toolkit.
        //public GlobalOptimizer GlobalOptimizer { get; set; }

        //public void InitGlobalOptimizer(StructureLearner learner)
        //{
        //    //1. Init Global Optimizer.
        //    GlobalOptimizer = GradientOptimizer.CreateGlobalOptimizer(ParamNumber, learner, new RunnerBehavior() { Device = learner.device });
        //    InitOptimizer(learner);
        //    //2. Obtain the model Parameter.
        //    GetModelParameter(GlobalOptimizer.Parameter, 0);
        //}
        //public void ResetGlobalOptimization()
        //{
        //    ClearModelGradient();
        //}

        
        //public void EndGlobalOptimization(double loss, object par, ObjectiveFunction func)
        //{
        //    //1. Obtain the model Gradient.
        //    GetModelGradient(GlobalOptimizer.Gradient, 0);

        //    //2. Update Global Optimization.
        //    GlobalOptimizer.UpdateGlobalOptimizer(loss, par, func);

        //    //3. Set Model Parameters.
        //    SetModelParameter(GlobalOptimizer.Parameter, 0);
        //}
        #endregion

    }
}
