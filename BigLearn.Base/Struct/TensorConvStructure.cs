using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TensorConvUtil 
    {
        public static int FilterOutput(int inx, int filterSize, int pad, int stride)
        {
            return (inx + 2 * pad - filterSize) / stride + 1;
        }

        public static int TransFilterOutput(int inx, int filterSize, int pad, int stride)
        {
            return (inx - 1) * stride - 2 * pad + filterSize;
        }

        public static int PoolingOutput(int inx, int sx, int stride)
        {
            return (inx - sx) / stride + 1;
        }
    }

    public class TensorConvStructure : Structure 
    {
        public int FilterWidth { get; set; }
        public int FilterHeight { get; set; }
        public int InDepth { get; set; }
        public int OutDepth { get; set; }

        public CudaPieceFloat Filter { get { return Parameter["filter"].Output; } }
        public CudaPieceFloat Bias { get { return Parameter["bias"].Output; } }

        public CudaPieceFloat FilterGrad { get { return Parameter["filter"].Deriv; } } //{ get { return Filter.Grad; } }
        public CudaPieceFloat BiasGrad { get { return Parameter["bias"].Deriv; } } //{ get { return Bias.Grad; } }

        public bool IsBias { get; set; }

        // enumate model parameters.
        // public override Dictionary<string, CudaPieceFloat> Parameter
        // {
        //     get
        //     {
        //         if(IsBias)
        //         {
        //             return new Dictionary<string, CudaPieceFloat>()
        //             {
        //                 { "TensorCNN_Filter", Filter },
        //                 { "TensorCNN_Bias", Bias },
        //             };
        //         }
        //         else
        //         {
        //             return new Dictionary<string, CudaPieceFloat>()
        //             {
        //                 { "TensorCNN_Filter", Filter }
        //             };
        //         }
        //     }
        // }

        public override DataSourceID Type  {  get  { return DataSourceID.TensorConvStructure; } }

        public static int FSize(int fw, int fh, int iD, int oD) 
        {
            return fw * fh * iD * oD;
        }

        public int FilterSize { get { return FilterWidth * FilterHeight * InDepth * OutDepth; } }

        public TensorConvStructure(int fw, int fh, int inDepth, int outDepth, DeviceType device) : this(fw, fh, inDepth, outDepth, true, device)
        { }

        public TensorConvStructure(int fw, int fh, int inDepth, int outDepth, DNNInitMethod initMethod, DeviceType device) : this(fw, fh, inDepth, outDepth, true, initMethod, device)
        { }

        public TensorConvStructure(int fw, int fh, int inDepth, int outDepth, bool isBias, DeviceType device) : this(fw, fh, inDepth, outDepth, isBias, DNNInitMethod.Xavior_Init, device)
        { }

        public TensorConvStructure(int fw, int fh, int inDepth, int outDepth, bool isBias, DNNInitMethod initMethod, DeviceType device) : base(device)
        {
            FilterWidth = fw;
            FilterHeight = fh;
            
            InDepth = inDepth;
            OutDepth = outDepth;
            
            IsBias = isBias;

            IntArgument w = new IntArgument("width", fw); 
            IntArgument h = new IntArgument("height", fh);
            IntArgument i = new IntArgument("inDepth", inDepth);
            IntArgument o = new IntArgument("outDepth", outDepth);

            AddParameter("filter", new NdArrayData(device, false, w, h, i, o));
            AddParameter("bias", new NdArrayData(device, false, o));

            //Filter = new CudaPieceFloat(FSize(fw, fh, inDepth, outDepth), device);
            //Bias = new CudaPieceFloat(outDepth, device);

            Init(initMethod);
        }

        //public TensorConvStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        //{ }

        // public override void CopyFrom(IData other)
        // {
        //     Filter.CopyFrom(((TensorConvStructure)other).Filter);
        //     Bias.CopyFrom(((TensorConvStructure)other).Bias);
        // }
        
        //public override void SyncFromCPU()
        //{
        //    Filter.SyncFromCPU();
        //    Bias.SyncFromCPU();
        //}

        //public override void SyncToCPU()
        //{
        //    Filter.SyncToCPU();
        //    Bias.SyncToCPU();
        //}

        // public override void Serialize(BinaryWriter writer)
        // {
        //     writer.Write(FilterWidth);
        //     writer.Write(FilterHeight);
        //     writer.Write(InDepth);
        //     writer.Write(OutDepth);

        //     Filter.Serialize(writer);
        //     Bias.Serialize(writer);
        // }

        // public override void Deserialize(BinaryReader reader, DeviceType device)
        // {
        //     FilterWidth = reader.ReadInt32();
        //     FilterHeight = reader.ReadInt32();
        //     InDepth = reader.ReadInt32();
        //     OutDepth = reader.ReadInt32();

        //     Filter = new CudaPieceFloat(reader, device);
        //     Bias = new CudaPieceFloat(reader, device);
        // }

        public void Init(DNNInitMethod initMethod)
        {
            int inputsize = FilterSize / OutDepth;
            int outputsize = FilterSize / InDepth;
            
            float base_range = 0;

            if(initMethod == DNNInitMethod.Xavior_Init)
            {
                base_range = (float)Math.Sqrt(6.0f / (inputsize + outputsize));
            }
            else if(initMethod == DNNInitMethod.Kaiming_Init)
            {
                base_range = (float)Math.Sqrt(12.0f / (inputsize + outputsize));
            }
            Logger.WriteLog("Conv filter is initalized with {0} in range {1}", initMethod, base_range);

            Filter.Init((float)(base_range * 2), (float)(-base_range));
            Bias.Init(0);
        }
    }
}
