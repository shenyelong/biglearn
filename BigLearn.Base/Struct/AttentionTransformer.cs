using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class AttentionTransformer : CompositeNNStructure 
    {
        public int Dim { get; set; } 

        DeviceType Device { get; set; }
        
        public LayerStructure QProject { get; set; }
        public LayerStructure KProject { get; set; }
        public LayerStructure VProject { get; set; }

        public LayerStructure Norm1 { get; set; }
        public LayerStructure Norm2 { get; set; }

        public VectorData Norm1Scale { get { return new VectorData(Dim, Norm1.weight, Norm1.weightGrad, Device); } }
        public VectorData Norm1Bias { get { return new VectorData(Dim, Norm1.bias, Norm1.biasGrad, Device); } }
        
        public VectorData Norm2Scale { get { return new VectorData(Dim, Norm2.weight, Norm2.weightGrad, Device); } }
        public VectorData Norm2Bias { get { return new VectorData(Dim, Norm2.bias, Norm1.biasGrad, Device); } }
        
        public LayerStructure AttProject { get; set; }

        public LayerStructure  MLP1 { get; set; }
        public LayerStructure  MLP2 { get; set; }

        // basicly, no useful at this moment.
        public override DataSourceID Type  {  get  { return DataSourceID.AttentionTransformer; } }

        public AttentionTransformer(int dim, DeviceType device)
        {
            Dim = dim;
            Device = device;
            
            QProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            KProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            VProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            
            AttProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
    
            MLP1 = AddLayer(new LayerStructure(dim, dim * 4, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            MLP2 = AddLayer(new LayerStructure(dim * 4, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

            Norm1 = AddLayer(new LayerStructure(1, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            Norm1.weight.Init(1);
            Norm1.bias.Init(0);

            Norm2 = AddLayer(new LayerStructure(1, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            Norm2.weight.Init(1);
            Norm2.bias.Init(0);
        }
    }
}
