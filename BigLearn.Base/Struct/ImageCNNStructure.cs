﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class ImageInputParameter
    {
        public int Width;
        public int Height;
        public int Depth;
    }

    public class ImagePoolingParameter
    {
        public int SX;
        public int Stride;
    }

    public class ImageFilterParameter
    {
        public int SX;
        public int Stride;

        public int C;
        public int O;
        public int Pad;
        public A_Func Af;

        public ImagePoolingParameter PoolingParameter = new ImagePoolingParameter();

        public ImageFilterParameter(int sx, int stride, int c, int o, int pad, A_Func af, int poolingSx, int poolingStride)
        {
            SX = sx;
            Stride = stride;
            C = c;
            O = o;
            Pad = pad;
            Af = af;
            PoolingParameter.SX = poolingSx;
            PoolingParameter.Stride = poolingStride;
        }

        public ImageFilterParameter(BinaryReader reader)
        {
            Deserialize(reader);
        }
        public void Serialize(BinaryWriter writer)
        {
            writer.Write(SX);
            writer.Write(Stride);
            writer.Write(C);
            writer.Write(O);
            writer.Write(Pad);
            writer.Write((int)Af);
            writer.Write(PoolingParameter.SX);
            writer.Write(PoolingParameter.Stride);
        }

        public void Deserialize(BinaryReader reader)
        {
            SX = reader.ReadInt32();
            Stride = reader.ReadInt32();
            C = reader.ReadInt32();
            O = reader.ReadInt32();
            Pad = reader.ReadInt32();
            Af = (A_Func)reader.ReadInt32();
            PoolingParameter.SX = reader.ReadInt32();
            PoolingParameter.Stride = reader.ReadInt32();
        }

        public int FilterSize { get { return SX * SX * C * O; } }

        public int PoolingX(int width)
        {
            return (width + 2 * Pad - SX) / Stride + 1;
        }

        public int PoolingY(int height)
        {
            return (height + 2 * Pad - SX) / Stride + 1;
        }

        public int OutputWidth(int width)
        {
            return (PoolingX(width) - PoolingParameter.SX) / PoolingParameter.Stride + 1;
        }

        public int OutputHeight(int height)
        {
            return (PoolingY(height) - PoolingParameter.SX) / PoolingParameter.Stride + 1;
        }
    }

    // public sealed class ImageLinkStructure : Structure 
    // {
    //     public ImageFilterParameter Param;

    //     public CudaPieceFloat Filter;
    //     public CudaPieceFloat Bias;

    //     public CudaPieceFloat FilterGrad { get { return Filter.Grad; } }
    //     public CudaPieceFloat BiasGrad { get { return Bias.Grad; } }

    //     //public GradientOptimizer FilterOptimizer { get { return StructureOptimizer["Filter"].Optimizer; } }
    //     //public GradientOptimizer BiasOptimizer { get { return StructureOptimizer["Bias"].Optimizer; } }

    //     public override DataSourceID Type  {  get  { return DataSourceID.ImageLinkStructure; } }

    //     public ImageLinkStructure(ImageFilterParameter param)  : this(param, DeviceType.GPU)
    //     { }

    //     public ImageLinkStructure(ImageFilterParameter param, DeviceType device)
    //     {
    //         Param = param;
    //         DeviceType = device;

    //         Filter = new CudaPieceFloat(Param.FilterSize, DeviceType);
    //         Bias = new CudaPieceFloat(Param.O, DeviceType);
    //         Init();
    //     }

    //     public ImageLinkStructure(BinaryReader reader, DeviceType device) : base(reader, device)
    //     { }

    //     public override void CopyFrom(IData other)
    //     {
    //         Filter.CopyFrom(((ImageLinkStructure)other).Filter);
    //         Bias.CopyFrom(((ImageLinkStructure)other).Bias);
    //     }

    //     public override Dictionary<string, CudaPieceFloat> Parameter
    //     {
    //         get
    //         {
    //             return new Dictionary<string, CudaPieceFloat>()
    //             {
    //                 { "Filter", Filter },
    //                 { "Bias", Bias },
    //             };
    //         }
    //     }

    //     //protected override void InitStructureOptimizer()
    //     //{
    //     //    FilterGrad = new CudaPieceFloat(Filter.Size, DeviceType);
    //     //    BiasGrad = new CudaPieceFloat(Bias.Size, DeviceType);

    //     //    StructureOptimizer.Add("Filter", new ModelOptimizer() { Parameter = Filter, Gradient = FilterGrad, Name = "Filter" });
    //     //    StructureOptimizer.Add("Bias", new ModelOptimizer() { Parameter = Bias, Gradient = BiasGrad, Name = "Bias" });
    //     //}

    //     public override void Serialize(BinaryWriter writer)
    //     {
    //         Param.Serialize(writer);
    //         Filter.Serialize(writer);
    //         Bias.Serialize(writer);
    //     }

    //     public override void Deserialize(BinaryReader reader, DeviceType device)
    //     {
    //         Param = new ImageFilterParameter(reader);
    //         Filter = new CudaPieceFloat(reader, device);
    //         Bias = new CudaPieceFloat(reader, device);
    //     }

    //     public void Init()
    //     {
    //         int inputsize = Param.FilterSize / Param.O;
    //         int outputsize = Param.O;
    //         Filter.Init((float)(Math.Sqrt(1.0 /(inputsize + 1)) * 2), (float)(-Math.Sqrt(1.0 / (inputsize + 1))));
    //         Bias.Init(0);
    //     }
    // }

    // public sealed class ImageCNNStructure : Structure 
    // {
    //     public override List<Structure> SubStructure { get { return CNNNeuralLinks.Select(i => (Structure)i).Concat(NNNeuralLinks.Select(i => (Structure)i)).ToList(); } }

    //     public List<ImageLinkStructure> CNNNeuralLinks = new List<ImageLinkStructure>();
    //     public List<LayerStructure> NNNeuralLinks = new List<LayerStructure>();

    //     public ImageCNNStructure(ImageInputParameter input, List<ImageFilterParameter> cnnParameters, int[] layerDim, A_Func[] af, float[] dropOut, bool[] isbias)
    //     {
    //         int outWidth = input.Width;
    //         int outHeight = input.Height;
    //         foreach (ImageFilterParameter filter in cnnParameters)
    //         {
    //             CNNNeuralLinks.Add(new ImageLinkStructure(filter));
    //             outWidth = filter.OutputWidth(outWidth);
    //             outHeight = filter.OutputHeight(outHeight);
    //         }
    //         int featureNum = cnnParameters.Last().O * outWidth * outHeight;
    //         for (int i = 0; i < layerDim.Length; i++)
    //         {
    //             LayerStructure link = new LayerStructure(featureNum, layerDim[i], af[i], N_Type.Fully_Connected, 1, dropOut[i], isbias[i]);
    //             NNNeuralLinks.Add(link);
    //             featureNum = layerDim[i];
    //         }
    //     }

    //     public ImageCNNStructure(BinaryReader reader, DeviceType device) : base(reader, device)
    //     { }

    //     public void Init()
    //     {
    //         for (int i = 0; i < CNNNeuralLinks.Count; i++)
    //         {
    //             CNNNeuralLinks[i].Init();
    //         }

    //         for (int i = 0; i < NNNeuralLinks.Count; i++)
    //         {
    //             NNNeuralLinks[i].Init(DNNInitMethod.Xavior_Init);
    //         }
    //     }

    //     public override void CopyFrom(IData other)
    //     {
    //         ImageCNNStructure otherCNN = (ImageCNNStructure)other;
    //         for (int i = 0; i < CNNNeuralLinks.Count; i++)
    //         {
    //             CNNNeuralLinks[i].CopyFrom(otherCNN.CNNNeuralLinks[i]);
    //         }
    //         for (int i = 0; i < NNNeuralLinks.Count; i++)
    //         {
    //             NNNeuralLinks[i].CopyFrom(otherCNN.NNNeuralLinks[i]);
    //         }
    //     }

    //     public override void Serialize(BinaryWriter writer)
    //     {
    //         writer.Write(CNNNeuralLinks.Count);
    //         for(int i=0;i<CNNNeuralLinks.Count;i++)
    //         {
    //             CNNNeuralLinks[i].Serialize(writer);
    //         }

    //         writer.Write(NNNeuralLinks.Count);
    //         for(int i=0;i<NNNeuralLinks.Count;i++)
    //         {
    //             NNNeuralLinks[i].Serialize(writer);
    //         }

    //     }

    //     public override void Deserialize(BinaryReader reader, DeviceType device)
    //     {
    //         int cCount = reader.ReadInt32();
    //         for(int i=0;i<cCount;i++)
    //         {
    //             CNNNeuralLinks.Add(new ImageLinkStructure(reader, device));
    //         }

    //         int nCount = reader.ReadInt32();
    //         for(int i=0;i<nCount;i++)
    //         {
    //             NNNeuralLinks.Add(new LayerStructure(reader, device));
    //         }
    //     }


    //     public string CNN_Descr()
    //     {
    //         string result = "";
    //         for (int i = 0; i < CNNNeuralLinks.Count; i++)
    //         {
    //             result += string.Format("Convolutional Neural Layer {0}, SX : {1}, C : {2}, Stride : {3}, O : {4}, Pad : {5}, AF : {6}, PoolingSX : {7}, PoolingStride : {8}",
    //                     i, CNNNeuralLinks[i].Param.SX, CNNNeuralLinks[i].Param.C, CNNNeuralLinks[i].Param.Stride,
    //                         CNNNeuralLinks[i].Param.O, CNNNeuralLinks[i].Param.Pad, CNNNeuralLinks[i].Param.Af, CNNNeuralLinks[i].Param.PoolingParameter.SX,
    //                         CNNNeuralLinks[i].Param.PoolingParameter.Stride);
    //         }

    //         for (int i = 0; i < NNNeuralLinks.Count; i++)
    //         {
    //             result += "layer " + i.ToString() + " to layer " + (i + 1).ToString() + ":" +
    //                     " AF Type : " + NNNeuralLinks[i].Af.ToString() + ";" +
    //                     " hid bias : " + NNNeuralLinks[i].IsBias.ToString() + ";" +
    //                     " Drop Out Rate : " + NNNeuralLinks[i].DropOut.ToString() + ";" + "\n";
    //         }
    //         return result;
    //     }
    // }
}
