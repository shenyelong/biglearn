﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class LSTMCell : Structure 
    {
        public int InputFeatureDim { get { return Parameter["wi"].Shape[1].Default; } }
        public int CellDim { get { return Parameter["wi"].Shape[0].Default; } }
        public bool AttentionGate { get{ return Parameter.ContainsKey("zi"); } }
        public int AttentionDim { get { return AttentionGate ? Parameter["zi"].Shape[1].Default : 0; } }

        public override DataSourceID Type { get { return DataSourceID.LSTMCell; } }

        /// <summary>
        /// Input to Hidden State Matrix.
        /// </summary>
        public CudaPieceFloat Wi { get { return Parameter["wi"].Output; } }
        public CudaPieceFloat WiGrad { get { return Parameter["wi"].Deriv; } }

        public CudaPieceFloat Ui { get { return Parameter["ui"].Output; } }
        public CudaPieceFloat UiGrad { get { return Parameter["ui"].Deriv; } }

        public CudaPieceFloat Bi { get { return Parameter["bi"].Output; } }
        public CudaPieceFloat BiGrad { get { return Parameter["bi"].Deriv; } }

        public CudaPieceFloat Wc { get { return Parameter["wc"].Output; } }
        public CudaPieceFloat WcGrad { get { return Parameter["wc"].Deriv; } }

        public CudaPieceFloat Uc { get { return Parameter["uc"].Output; } }
        public CudaPieceFloat UcGrad { get { return Parameter["uc"].Deriv; } }

        public CudaPieceFloat Bc { get { return Parameter["bc"].Output; } }
        public CudaPieceFloat BcGrad { get { return Parameter["bc"].Deriv; } }

        public CudaPieceFloat Wf { get { return Parameter["wf"].Output; } }
        public CudaPieceFloat WfGrad { get { return Parameter["wf"].Deriv; } }

        public CudaPieceFloat Uf { get { return Parameter["uf"].Output; } }
        public CudaPieceFloat UfGrad { get { return Parameter["uf"].Deriv; } }

        public CudaPieceFloat Bf { get { return Parameter["bf"].Output; } }
        public CudaPieceFloat BfGrad { get { return Parameter["bf"].Deriv; } }

        public CudaPieceFloat Wo { get { return Parameter["wo"].Output; } }
        public CudaPieceFloat WoGrad { get { return Parameter["wo"].Deriv; } }

        public CudaPieceFloat Uo { get { return Parameter["uo"].Output; } }
        public CudaPieceFloat UoGrad { get { return Parameter["uo"].Deriv; } }

        public CudaPieceFloat Vo { get { return Parameter["vo"].Output; } }
        public CudaPieceFloat VoGrad { get { return Parameter["vo"].Deriv; } }

        public CudaPieceFloat Bo { get { return Parameter["bo"].Output; } }
        public CudaPieceFloat BoGrad { get { return Parameter["bo"].Deriv; } }

        public CudaPieceFloat Zi { get { return Parameter["zi"].Output; } }
        public CudaPieceFloat ZiGrad { get { return Parameter["zi"].Deriv; } }

        public CudaPieceFloat Zf { get { return Parameter["zf"].Output; } }
        public CudaPieceFloat ZfGrad { get { return Parameter["zf"].Deriv; } }

        public CudaPieceFloat Zc { get { return Parameter["zc"].Output; } }
        public CudaPieceFloat ZcGrad { get { return Parameter["zc"].Deriv; } }

        public CudaPieceFloat Zo { get { return Parameter["zo"].Output; } }
        public CudaPieceFloat ZoGrad { get { return Parameter["zo"].Deriv; } }

        // public override Dictionary<string, NdArrayData> Parameter
        // {
        //     get
        //     {
        //         Dictionary<string, NdArrayData> tmp = new Dictionary<string, NdArrayData>()
        //         {
        //             { "Wi", NDWi },
        //             { "Ui", NDUi },
        //             { "Bi", NDBi },

        //             { "Wc", NDWc },
        //             { "Uc", NDUc },
        //             { "Bc", NDBc },

        //             { "Wf", NDWf },
        //             { "Uf", NDUf },
        //             { "Bf", NDBf },

        //             { "Wo", NDWo },
        //             { "Uo", NDUo },
        //             { "Vo", NDVo },
        //             { "Bo", NDBo },                
        //         };

        //         if (AttentionGate)
        //         {
        //             tmp.Add("Zi", NDZi);
        //             tmp.Add("Zf", NDZf);
        //             tmp.Add("Zc", NDZc);
        //             tmp.Add("Zo", NDZo);
        //         }
        //         return tmp;
        //     }
        // }

        public LSTMCell(int input, int cell, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm) : this(input, cell, DeviceType.GPU, rndInit) { }

        public LSTMCell(int input, int cell, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm) : this(input, cell, 0, device, rndInit) { }

        public LSTMCell(int input, int cell, int attentionDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm)
            : base(device)
        {
            IntArgument in_dim = new IntArgument("indim", input);
            IntArgument cell_dim = new IntArgument("cell", cell); 
            IntArgument att_dim = new IntArgument("attdim", attentionDim);

            AddParameter("wi", new NdArrayData(device, false, cell_dim, in_dim));
            AddParameter("wc", new NdArrayData(device, false, cell_dim, in_dim));
            AddParameter("wf", new NdArrayData(device, false, cell_dim, in_dim));
            AddParameter("wo", new NdArrayData(device, false, cell_dim, in_dim));

            AddParameter("ui", new NdArrayData(device, false, cell_dim, cell_dim));
            AddParameter("uc", new NdArrayData(device, false, cell_dim, cell_dim));
            AddParameter("uf", new NdArrayData(device, false, cell_dim, cell_dim));
            AddParameter("uo", new NdArrayData(device, false, cell_dim, cell_dim));

            AddParameter("vo", new NdArrayData(device, false, cell_dim, cell_dim));

            AddParameter("bi", new NdArrayData(device, false, cell_dim));
            AddParameter("bc", new NdArrayData(device, false, cell_dim));
            AddParameter("bf", new NdArrayData(device, false, cell_dim));
            AddParameter("bo", new NdArrayData(device, false, cell_dim));

            if(attentionDim > 0)
            {
                AddParameter("zi", new NdArrayData(device, false, cell_dim, att_dim));
                AddParameter("zc", new NdArrayData(device, false, cell_dim, att_dim));
                AddParameter("zf", new NdArrayData(device, false, cell_dim, att_dim));
                AddParameter("zo", new NdArrayData(device, false, cell_dim, att_dim));
            }

            //InputFeatureDim = input;
            //CellDim = cell;
            //RndInit = rndInit;
            //AttentionDim = attentionDim;
            //AttentionGate = AttentionDim == 0 ? false : true;
            //DeviceType = device;
            Init(rndInit);
        }

        //RndRecurrentInit RndInit = RndRecurrentInit.RndNorm;

        public void Init(RndRecurrentInit RndInit)
        {
            //Wi = new CudaPieceFloat(InputFeatureDim * CellDim, true, device == DeviceType.GPU);
            //Ui = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            //Bi = new CudaPieceFloat(CellDim, true, device == DeviceType.GPU);
            Wi.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + CellDim))));
            if(RndInit == RndRecurrentInit.RndOrthogonal) Ui.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Ui.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));
            Bi.Init(0);

            //Wc = new CudaPieceFloat(InputFeatureDim * CellDim, true, device == DeviceType.GPU);
            //Uc = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            //Bc = new CudaPieceFloat(CellDim, true, device == DeviceType.GPU);
            Wc.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + CellDim))));
            if (RndInit == RndRecurrentInit.RndOrthogonal) Uc.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Uc.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));
            Bc.Init(0);

            //Wf = new CudaPieceFloat(InputFeatureDim * CellDim, true, device == DeviceType.GPU);
            //Uf = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            //Bf = new CudaPieceFloat(CellDim, true, device == DeviceType.GPU);
            Wf.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + CellDim))));
            if (RndInit == RndRecurrentInit.RndOrthogonal) Uf.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Uf.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));

            // forget gate bias is very important.
            Bf.Init(1.5f);

            //Wo = new CudaPieceFloat(InputFeatureDim * CellDim, true, device == DeviceType.GPU);
            //Uo = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            //Vo = new CudaPieceFloat(CellDim * CellDim, true, device == DeviceType.GPU);
            //Bo = new CudaPieceFloat(CellDim, true, device == DeviceType.GPU);
            Wo.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + CellDim))));
            if (RndInit == RndRecurrentInit.RndOrthogonal) Uo.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Uo.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));

            if (RndInit == RndRecurrentInit.RndOrthogonal) Vo.InitRandomOrthogonalMatrix(CellDim);
            else if (RndInit == RndRecurrentInit.RndNorm) Vo.Init((float)(Math.Sqrt(6.0 / (CellDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (CellDim + CellDim))));
            Bo.Init(0);

            if (AttentionGate)
            {
                //Zi = new CudaPieceFloat(AttentionDim * CellDim, true, device == DeviceType.GPU);
                //Zf = new CudaPieceFloat(AttentionDim * CellDim, true, device == DeviceType.GPU);
                //Zc = new CudaPieceFloat(AttentionDim * CellDim, true, device == DeviceType.GPU);
                //Zo = new CudaPieceFloat(AttentionDim * CellDim, true, device == DeviceType.GPU);

                Zi.Init((float)(Math.Sqrt(6.0 / (AttentionDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + CellDim))));
                Zf.Init((float)(Math.Sqrt(6.0 / (AttentionDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + CellDim))));
                Zc.Init((float)(Math.Sqrt(6.0 / (AttentionDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + CellDim))));
                Zo.Init((float)(Math.Sqrt(6.0 / (AttentionDim + CellDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + CellDim))));
            }
        }

        
    }
}
