using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{   
    // + layer normalization.  
    public class RRUCell : Structure 
    {
        //RndRecurrentInit RndInit = RndRecurrentInit.RndNorm;

        public RRUCell(int input, int hiddenDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm)  : base(device) // : base(input, 0, hiddenDim, device, rndInit)
        {
            InputFeatureDim = input;
            HiddenStateDim = hiddenDim;

            //RndInit = rndInit;

            IntArgument in_dim = new IntArgument("in_dim", input); 
            IntArgument hid_dim = new IntArgument("hid_dim", hiddenDim);

            AddParameter("wr", new NdArrayData(device, false, hid_dim, in_dim));
            AddParameter("ur", new NdArrayData(device, false, hid_dim, hid_dim));
            AddParameter("br", new NdArrayData(device, false, hid_dim));

            AddParameter("wh", new NdArrayData(device, false, hid_dim, in_dim));
            AddParameter("uh", new NdArrayData(device, false, hid_dim, hid_dim));
            AddParameter("bh", new NdArrayData(device, false, hid_dim));

            //InitMemory(device);
            Init(rndInit); 
        }

        //public RRUCell(BinaryReader reader, DeviceType device) : base(reader, device)
        //{ }

        public override DataSourceID Type { get { return DataSourceID.RRUCell; } }

        public int InputFeatureDim;
        public int HiddenStateDim;

        public CudaPieceFloat Wr { get { return Parameter["wr"].Output; } }
        public CudaPieceFloat WrGrad { get { return Parameter["wr"].Deriv; } }
        
        public CudaPieceFloat Ur { get { return Parameter["ur"].Output; } }
        public CudaPieceFloat UrGrad { get { return Parameter["ur"].Deriv; } }
        
        public CudaPieceFloat Br { get { return Parameter["br"].Output; } }
        public CudaPieceFloat BrGrad { get { return Parameter["br"].Deriv; } }

        public CudaPieceFloat Wh { get { return Parameter["wh"].Output; } }
        public CudaPieceFloat WhGrad { get { return Parameter["wh"].Deriv; } }
        
        public CudaPieceFloat Uh { get { return Parameter["uh"].Output; } }
        public CudaPieceFloat UhGrad { get { return Parameter["uh"].Deriv; } }

        public CudaPieceFloat Bh { get { return Parameter["bh"].Output; } }
        public CudaPieceFloat BhGrad { get { return Parameter["bh"].Deriv; } }

        // public override Dictionary<string, CudaPieceFloat> Parameter
        // {
        //     get
        //     {
        //         Dictionary<string, CudaPieceFloat> tmp = new Dictionary<string, CudaPieceFloat>()
        //         {
        //             { "RRU_Wr", Wr },
        //             { "RRU_Ur", Ur },
        //             { "RRU_Br", Br },

        //             { "RRU_Wh", Wh },
        //             { "RRU_Uh", Uh },
        //             { "RRU_Bh", Bh },
        //         };
        //         return tmp;
        //     }
        // }

        // public void InitMemory(DeviceType device)
        // {
        //     Wr = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);
        //     Ur = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);
        //     Br = new CudaPieceFloat(HiddenStateDim, device);

        //     Wh = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);
        //     Uh = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);
        //     Bh = new CudaPieceFloat(HiddenStateDim, device);
        // }

        public void Init(RndRecurrentInit RndInit)
        {
            Wr.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            if(RndInit == RndRecurrentInit.RndNorm) Ur.Init((float)(Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim))));
            else if(RndInit == RndRecurrentInit.RndOrthogonal) Ur.InitRandomOrthogonalMatrix(HiddenStateDim);
            Br.Init(0);
            
            Wh.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            if (RndInit == RndRecurrentInit.RndNorm) Uh.Init((float)(Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim))));
            else if (RndInit == RndRecurrentInit.RndOrthogonal) Uh.InitRandomOrthogonalMatrix(HiddenStateDim);
            Bh.Init(0);
        }

    }
}