﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace BigLearn
{    
    public enum DNNModelVersion
    {
        /// <summary>
        /// Only support DSSM model format used in 2013. (Bing Team (Gu Xu) use this format).
        /// </summary>
        DSSM_GPU_V0_2013 = 0,

        /// <summary>
        /// Only support CDSSM model format used in 2013. (Ads Team (qiang lou, mirror xu) use this format).
        /// </summary>
        CDSSM_GPU_V1_2013 = 1,

        /// <summary>
        /// Support both DSSM and CDSSM model with the same format (Jianshu, Xiaodong currently use this format to train models for other teams).
        /// </summary>
        C_OR_DSSM_GPU_V2_2014_MAY = 2,

        /// <summary>
        /// Support different Activation functions in different layer.
        /// </summary>
        C_OR_DSSM_GPU_V3_2014_NOV = 3,

        /// <summary>
        /// Support configurable IsBias and Dropout Rate in different layer.
        /// </summary>
        C_OR_DSSM_GPU_V4_2015_APR = 4,
    }

    public enum DNNInitMethod
    {
        Xavior_Init = 0,
        Kaiming_Init = 1
    }

    /// <summary>
    /// Model related parameters and network structure
    /// </summary>
    public sealed class DNNStructure : Structure 
    {
        public override List<Structure> SubStructure { get { return NeuralLinks.Select(i => (Structure)i).ToList(); } }

        public List<LayerStructure> NeuralLinks = new List<LayerStructure>();
        public List<int> NeuralLayers = new List<int>();

        public List<int> neurallayers { get { return NeuralLayers; } set { NeuralLayers = value; } }
        public List<LayerStructure> neurallinks { get { return NeuralLinks; } set { NeuralLinks = value; } }

        public bool IsConvolution { get { return neurallinks.Select(i => i.Nt == N_Type.Convolution_layer ? 1 : 0).Sum() > 0; } }

        public override DataSourceID Type { get { return DataSourceID.DNNStructure; } }

        public int OutputLayerSize
        {
            get { return neurallayers.Last(); }
        }

        public int InputLayerSize
        {
            get { return neurallayers.First(); }
        }

        public string DNN_Descr()
        {
            string result = "";
            for (int i = 0; i < neurallayers.Count; i++)
            {
                result += "Neural Layer " + i.ToString() + ": " + neurallayers[i].ToString() + "\n";
            }

            for (int i = 0; i < neurallinks.Count; i++)
            {
                result += "layer " + i.ToString() + " to layer " + (i + 1).ToString() + ":" +
                        " AF Type : " + neurallinks[i].Af.ToString() + ";" +
                        " hid bias : " + neurallinks[i].IsBias.ToString() + ";" +
                        " Neural Type : " + neurallinks[i].Nt.ToString() + ";" +
                        " Drop Out Rate : " + neurallinks[i].DropOut.ToString() + ";" +
                        " Window Size : " + neurallinks[i].N_Winsize.ToString() + ";" + "\n";
            }
            return result;
        }


        // public override void Serialize(BinaryWriter writer)
        // {
        //     ///By Default, Serialize to the latest version.
        //     SerializeAs_C_OR_DSSM_GPU_V4_2015_APR(writer);
        // }

        // public override void Deserialize(BinaryReader reader, DeviceType device)
        // {
        //     ModelLoad_C_OR_DSSM_GPU_V4_2015_APR(reader.BaseStream, device);
        // }

        /// <param name="modelFile"></param>
        /// <param name="device"></param>
        public void ModelLoad_DSSM_GPU_V0_2013(string modelFile, DeviceType device)
        {
            using (FileStream mstream = new FileStream(modelFile, FileMode.Open, FileAccess.Read))
            {
                ModelLoad_DSSM_GPU_V0_2013(mstream, device);
            }
        }
        private void ModelLoad_DSSM_GPU_V0_2013(Stream mstream, DeviceType device)
        {
            using (BinaryReader mreader = new BinaryReader(mstream, Encoding.UTF8, true))
            {
                NeuralLayers = new List<int>();
                int mlayer_num = mreader.ReadInt32();
                for (int i = 0; i < mlayer_num; i++)
                {
                    NeuralLayers.Add(mreader.ReadInt32());
                }

                int mlink_num = mreader.ReadInt32();
                for (int i = 0; i < mlink_num; i++)
                {
                    int in_num = mreader.ReadInt32();
                    int out_num = mreader.ReadInt32();
                    float inithidbias = mreader.ReadSingle();
                    float initweightsigma = mreader.ReadSingle();

                    LayerStructure link = new LayerStructure(NeuralLayers[i], NeuralLayers[i + 1], A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false, device);
                    neurallinks.Add(link);
                }

                for (int i = 0; i < mlink_num; i++)
                {
                    int weight_len = mreader.ReadInt32();
                    if (weight_len != neurallinks[i].weight.Size)
                    {
                        Console.WriteLine("Loading Model Weight Error!  " + weight_len.ToString() + " " + neurallinks[i].weight.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < weight_len; m++)
                        neurallinks[i].weight[m] = mreader.ReadSingle();

                    int bias_len = mreader.ReadInt32();
                    if (bias_len != neurallinks[i].bias.Size)
                    {
                        Console.WriteLine("Loading Model Bias Error!  " + bias_len.ToString() + " " + neurallinks[i].bias.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < bias_len; m++)
                        neurallinks[i].bias[m] = mreader.ReadSingle();

                    if (device == DeviceType.GPU)
                    {
                        NeuralLinks[i].weight.CopyIntoCuda();
                        NeuralLinks[i].bias.CopyIntoCuda();
                    }
                }
            }
        }

        /// <param name="modelFile"></param>
        /// <param name="device"></param>
        public void ModelLoad_CDSSM_GPU_V1_2013(string modelFile, DeviceType device)
        {
            using (FileStream mstream = new FileStream(modelFile, FileMode.Open, FileAccess.Read))
            {
                ModelLoad_CDSSM_GPU_V1_2013(mstream, device);
            }
        }
        private void ModelLoad_CDSSM_GPU_V1_2013(Stream mstream, DeviceType device)
        {
            using (BinaryReader mreader = new BinaryReader(mstream, Encoding.UTF8, true))
            {
                NeuralLayers = new List<int>();
                int mlayer_num = mreader.ReadInt32();
                for (int i = 0; i < mlayer_num; i++)
                    NeuralLayers.Add(mreader.ReadInt32());

                int mlink_num = mreader.ReadInt32();
                for (int i = 0; i < mlink_num; i++)
                {
                    int in_num = mreader.ReadInt32();
                    int out_num = mreader.ReadInt32();
                    float inithidbias = mreader.ReadSingle();
                    float initweightsigma = mreader.ReadSingle();
                    int mws = mreader.ReadInt32();
                    N_Type mnt = (N_Type)mreader.ReadInt32();
                    P_Pooling mp = (P_Pooling)mreader.ReadInt32();
                    LayerStructure link = new LayerStructure(NeuralLayers[i], NeuralLayers[i + 1], A_Func.Tanh, mnt, mws, 0, false, device);
                    neurallinks.Add(link);
                }

                for (int i = 0; i < mlink_num; i++)
                {
                    int weight_len = mreader.ReadInt32();
                    if (weight_len != neurallinks[i].weight.Size)
                    {
                        Console.WriteLine("Loading Model Weight Error!  " + weight_len.ToString() + " " + neurallinks[i].weight.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < weight_len; m++)
                        neurallinks[i].weight[m] = mreader.ReadSingle();

                    int bias_len = mreader.ReadInt32();
                    if (bias_len != neurallinks[i].bias.Size)
                    {
                        Console.WriteLine("Loading Model Bias Error!  " + bias_len.ToString() + " " + neurallinks[i].bias.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < bias_len; m++)
                        neurallinks[i].bias[m] = mreader.ReadSingle();

                    if (device == DeviceType.GPU)
                    {
                        NeuralLinks[i].weight.CopyIntoCuda();
                        NeuralLinks[i].bias.CopyIntoCuda();
                    }
                }
            }
        }

        /// <param name="modelFile"></param>
        public void ModelLoad_C_OR_DSSM_GPU_V4_2015_APR(string modelFile, DeviceType device)
        {
            using (FileStream mstream = new FileStream(modelFile, FileMode.Open, FileAccess.Read))
            {
                ModelLoad_C_OR_DSSM_GPU_V4_2015_APR(mstream, device);
            }
        }
        private void ModelLoad_C_OR_DSSM_GPU_V4_2015_APR(Stream mstream, DeviceType device)
        {
            using (BinaryReader mreader = new BinaryReader(mstream, Encoding.UTF8, true))
            {
                NeuralLayers = new List<int>();
                int mlayer_num = mreader.ReadInt32();           // 1. 4 byte integer : The total Number of Layers. Including the input layer.  (Multiple input layer is not supported).
                for (int i = 0; i < mlayer_num; i++)
                {
                    NeuralLayers.Add(mreader.ReadInt32());        // 2. The dimension of each Layer. 
                }

                int mlink_num = mreader.ReadInt32();            // 3. 4 byte integer : The number of links. (Link Number  = Layer Number - 1).
                for (int i = 0; i < mlink_num; i++)
                {
                    int in_num = mreader.ReadInt32();          // 4. The number of neurons of input layer for the link i.  
                    int out_num = mreader.ReadInt32();         // 5. The number of neurons of output layer for the link i.
                    int af = mreader.ReadInt32();               // 6. 
                    bool isbias = mreader.ReadInt32() > 0 ? true : false;   //7.
                    // this is the eventually favorable loading format
                    N_Type mnt = (N_Type)mreader.ReadInt32();       // 8. the link-layer type - 0 : Fully-Connected; 1 : Convolutional Layer.
                    int mws = mreader.ReadInt32();                  // 9. the window size of the convolution layer. If the link layer is  fully-connected, then mws will always be 1.
                    P_Pooling mp = (P_Pooling)mreader.ReadInt32();  // 10. the pooling method - 0 : max-pooling. 
                    float dropOut = mreader.ReadSingle();           // 11. DropOut Rate.
                    LayerStructure link = new LayerStructure(NeuralLayers[i], NeuralLayers[i + 1], (A_Func)(af), mnt, mws, dropOut, isbias, device);
                    neurallinks.Add(link);
                }

                for (int i = 0; i < mlink_num; i++)                 // the weight of the DNN model.
                {
                    int weight_len = mreader.ReadInt32(); // Write(neurallinks[i].weight.Size);  // 11. The length of the weight. It shoulde equals to Input Dimension * Output Dimension of each link;
                    if (weight_len != neurallinks[i].weight.Size)
                    {
                        Console.WriteLine("Loading Model Weight Error!  " + weight_len.ToString() + " " + neurallinks[i].weight.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < weight_len; m++)                            // i.e., input dimension = 5; output dimension = 10; len of weights = 50;
                    {
                        neurallinks[i].weight[m] = mreader.ReadSingle();       // 12. Read float for each weight.  
                    }
                    int bias_len = mreader.ReadInt32();                             // 13. The len of bias. It should equals to output dimension of each link.
                    if (bias_len != neurallinks[i].bias.Size)
                    {
                        Console.WriteLine("Loading Model Bias Error!  " + bias_len.ToString() + " " + neurallinks[i].bias.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < bias_len; m++)
                    {
                        neurallinks[i].bias[m] = mreader.ReadSingle();         // 14. Read float for each bias.  
                    }

                    if (device == DeviceType.GPU)
                    {
                        neurallinks[i].weight.CopyIntoCuda();
                        neurallinks[i].bias.CopyIntoCuda();
                    }
                }
            }
        }


        /// <param name="modelFile"></param>
        public void ModelLoad_C_OR_DSSM_GPU_V2_2014_MAY(string modelFile, DeviceType device)
        {
            using (FileStream mstream = new FileStream(modelFile, FileMode.Open, FileAccess.Read))
            {
                ModelLoad_C_OR_DSSM_GPU_V2_2014_MAY(mstream, device);
            }
        }
        private void ModelLoad_C_OR_DSSM_GPU_V2_2014_MAY(Stream mstream, DeviceType device)
        {
            using (BinaryReader mreader = new BinaryReader(mstream, Encoding.UTF8, true))
            {
                NeuralLayers = new List<int>();
                int mlayer_num = mreader.ReadInt32();           // 1. 4 byte integer : The total Number of Layers. Including the input layer.  (Multiple input layer is not supported).
                for (int i = 0; i < mlayer_num; i++)
                {
                    NeuralLayers.Add(mreader.ReadInt32());        // 2. The dimension of each Layer. 
                }

                int mlink_num = mreader.ReadInt32();
                for (int i = 0; i < mlink_num; i++)
                {
                    int in_num = mreader.ReadInt32();
                    int out_num = mreader.ReadInt32();
                    float inithidbias = mreader.ReadSingle();
                    float initweightsigma = mreader.ReadSingle();

                    int mws = mreader.ReadInt32();
                    N_Type mnt = (N_Type)mreader.ReadInt32();

                    P_Pooling mp = (P_Pooling)mreader.ReadInt32();

                    if (mnt == N_Type.Convolution_layer)
                    {
                        LayerStructure link = new LayerStructure(NeuralLayers[i], NeuralLayers[i + 1], A_Func.Tanh, mnt, mws, 0, false, device);
                        neurallinks.Add(link);
                    }
                    else if (mnt == N_Type.Fully_Connected)
                    {
                        LayerStructure link = new LayerStructure(NeuralLayers[i], NeuralLayers[i + 1], A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false, device);
                        neurallinks.Add(link);
                    }
                }

                for (int i = 0; i < mlink_num; i++)
                {
                    int weight_len = mreader.ReadInt32();
                    if (weight_len != neurallinks[i].weight.Size)
                    {
                        Console.WriteLine("Loading Model Weight Error!  " + weight_len.ToString() + " " + neurallinks[i].weight.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < weight_len; m++)
                    {
                        neurallinks[i].weight[m] = mreader.ReadSingle();
                    }
                    int bias_len = mreader.ReadInt32();
                    if (bias_len != neurallinks[i].bias.Size)
                    {
                        Console.WriteLine("Loading Model Bias Error!  " + bias_len.ToString() + " " + neurallinks[i].bias.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < bias_len; m++)
                    {
                        neurallinks[i].bias[m] = mreader.ReadSingle();
                    }

                    if (device == DeviceType.GPU)
                    {
                        NeuralLinks[i].weight.CopyIntoCuda();
                        NeuralLinks[i].bias.CopyIntoCuda();
                    }
                }
            }
        }

        /// <param name="modelFile"></param>
        public void ModelLoad_C_OR_DSSM_GPU_V3_2014_NOV(string modelFile, DeviceType device)
        {
            using (FileStream mstream = new FileStream(modelFile, FileMode.Open, FileAccess.Read))
            {
                ModelLoad_C_OR_DSSM_GPU_V3_2014_NOV(mstream, device);
            }
        }
        private void ModelLoad_C_OR_DSSM_GPU_V3_2014_NOV(Stream mstream, DeviceType device)
        {
            using (BinaryReader mreader = new BinaryReader(mstream, Encoding.UTF8, true))
            {
                NeuralLayers = new List<int>();
                int mlayer_num = mreader.ReadInt32();           // 1. 4 byte integer : The total Number of Layers. Including the input layer.  (Multiple input layer is not supported).
                for (int i = 0; i < mlayer_num; i++)
                {
                    NeuralLayers.Add(mreader.ReadInt32());        // 2. The dimension of each Layer. 
                }

                int mlink_num = mreader.ReadInt32();
                for (int i = 0; i < mlink_num; i++)
                {
                    int in_num = mreader.ReadInt32();
                    int out_num = mreader.ReadInt32();
                    float inithidbias = mreader.ReadSingle();
                    float initweightsigma = mreader.ReadSingle();

                    int mws = mreader.ReadInt32();
                    int afAndNt = mreader.ReadInt32();
                    A_Func aF = A_Func.Tanh;
                    switch (afAndNt >> 16)
                    {
                        case 0: aF = A_Func.Tanh; break;
                        case 1: aF = A_Func.Linear; break;
                        case 2: aF = A_Func.Rectified; break;
                    }
                    N_Type mnt = (N_Type)(afAndNt & ((1 << 16) - 1));

                    P_Pooling mp = (P_Pooling)mreader.ReadInt32();

                    if (mnt == N_Type.Convolution_layer)
                    {
                        LayerStructure link = new LayerStructure(NeuralLayers[i], NeuralLayers[i + 1], aF, mnt, mws, 0, false, device);
                        neurallinks.Add(link);
                    }
                    else if (mnt == N_Type.Fully_Connected)
                    {
                        LayerStructure link = new LayerStructure(NeuralLayers[i], NeuralLayers[i + 1], aF, N_Type.Fully_Connected, 1, 0, false, device);
                        neurallinks.Add(link);
                    }
                }

                for (int i = 0; i < mlink_num; i++)
                {
                    int weight_len = mreader.ReadInt32();
                    if (weight_len != neurallinks[i].weight.Size)
                    {
                        Console.WriteLine("Loading Model Weight Error!  " + weight_len.ToString() + " " + neurallinks[i].weight.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < weight_len; m++)
                    {
                        neurallinks[i].weight[m] = mreader.ReadSingle();
                    }
                    int bias_len = mreader.ReadInt32();
                    if (bias_len != neurallinks[i].bias.Size)
                    {
                        Console.WriteLine("Loading Model Bias Error!  " + bias_len.ToString() + " " + neurallinks[i].bias.Size.ToString());
                        Console.ReadLine();
                    }
                    for (int m = 0; m < bias_len; m++)
                    {
                        neurallinks[i].bias[m] = mreader.ReadSingle();
                    }
                    if (device == DeviceType.GPU)
                    {
                        NeuralLinks[i].weight.CopyIntoCuda();
                        NeuralLinks[i].bias.CopyIntoCuda();
                    }
                }

            }
        }


        public void ModelSave_CDSSM_GPU_V1_2013(string modelFile)
        {
            FileStream stream = new FileStream(modelFile, FileMode.Create, FileAccess.Write);
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(neurallayers.Count);
            for (int i = 0; i < neurallayers.Count; i++)
            {
                writer.Write(neurallayers[i]);
            }
            writer.Write(neurallinks.Count);
            for (int i = 0; i < neurallinks.Count; i++)
            {
                writer.Write(neurallinks[i].Neural_In);
                writer.Write(neurallinks[i].Neural_Out);
                writer.Write(neurallinks[i].IsBias ? 1.0f : 0.0f);
                writer.Write(neurallinks[i].N_Winsize);
                writer.Write((int)neurallinks[i].Nt);
                writer.Write((int)neurallinks[i].pool_type);
            }

            for (int i = 0; i < neurallinks.Count; i++)
            {
                int mlen = neurallinks[i].N_Winsize * neurallinks[i].Neural_In * neurallinks[i].Neural_Out;
                writer.Write(mlen);
                for (int m = 0; m < mlen; m++)
                {
                    writer.Write(neurallinks[i].weight[m]);
                }

                writer.Write(neurallinks[i].Neural_Out);
                for (int m = 0; m < neurallinks[i].Neural_Out; m++)
                {
                    writer.Write(neurallinks[i].bias[m]);
                }
            }

            writer.Close();
            stream.Close();
        }
        public void ModelSave_C_OR_DSSM_GPU_V2_2014_MAY(string modelFile)
        {
            using (FileStream mstream = new FileStream(modelFile, FileMode.Create, FileAccess.Write))
            {
                using (BinaryWriter mwriter = new BinaryWriter(mstream))
                {
                    mwriter.Write(NeuralLayers.Count);                // 1. 4 byte integer : The total Number of Layers. Including the input layer.  (Multiple input layer is not supported).  
                    for (int i = 0; i < NeuralLayers.Count; i++)
                    {
                        mwriter.Write(NeuralLayers[i]);               // 2. The dimension of each Layer.   
                    }

                    mwriter.Write(NeuralLinks.Count);
                    for (int i = 0; i < NeuralLinks.Count; i++)
                    {
                        mwriter.Write(neurallinks[i].Neural_In);
                        mwriter.Write(neurallinks[i].Neural_Out);
                        mwriter.Write(0);
                        mwriter.Write(0);

                        mwriter.Write(neurallinks[i].N_Winsize);
                        mwriter.Write((int)neurallinks[i].Nt);
                        mwriter.Write((int)neurallinks[i].pool_type);
                    }

                    for (int i = 0; i < NeuralLinks.Count; i++)
                    {
                        neurallinks[i].weight.CopyOutFromCuda();
                        mwriter.Write(neurallinks[i].weight.Size);
                        for (int m = 0; m < neurallinks[i].weight.Size; m++)
                        {
                            mwriter.Write(neurallinks[i].weight[m]);
                        }

                        neurallinks[i].bias.CopyOutFromCuda();
                        mwriter.Write(neurallinks[i].bias.Size);
                        for (int m = 0; m < neurallinks[i].bias.Size; m++)
                        {
                            mwriter.Write(neurallinks[i].bias[m]);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Save C_OR_DSSM_MSR_GPU_V4_2015_APR.
        /// </summary>
        /// <param name="modelFile"></param>
        public void ModelSave_C_OR_DSSM_GPU_V4_2015_APR(string modelFile)
        {
            using (FileStream mstream = new FileStream(modelFile, FileMode.Create, FileAccess.Write))
            {
                using (BinaryWriter mwriter = new BinaryWriter(mstream))
                {
                    SerializeAs_C_OR_DSSM_GPU_V4_2015_APR(mwriter);
                }
            }
        }
        public void SerializeAs_C_OR_DSSM_GPU_V4_2015_APR(BinaryWriter mwriter)
        {
            mwriter.Write(neurallayers.Count);
            for (int i = 0; i < neurallayers.Count; i++)
            {
                mwriter.Write(neurallayers[i]);
            }

            mwriter.Write(neurallinks.Count);
            for (int i = 0; i < neurallinks.Count; i++)
            {
                mwriter.Write(neurallinks[i].Neural_In);
                mwriter.Write(neurallinks[i].Neural_Out);
                mwriter.Write((int)neurallinks[i].Af);
                mwriter.Write(neurallinks[i].IsBias ? 1 : 0);
                mwriter.Write((int)neurallinks[i].Nt);
                mwriter.Write(neurallinks[i].N_Winsize);
                mwriter.Write((int)neurallinks[i].pool_type);
                mwriter.Write(neurallinks[i].DropOut);
            }

            for (int i = 0; i < neurallinks.Count; i++)
            {
                neurallinks[i].weight.SyncToCPU();
                neurallinks[i].bias.SyncToCPU();

                mwriter.Write(neurallinks[i].weight.Size);
                for (int m = 0; m < neurallinks[i].weight.Size; m++)
                    mwriter.Write(neurallinks[i].weight[m]);

                mwriter.Write(neurallinks[i].Neural_Out);
                for (int m = 0; m < neurallinks[i].Neural_Out; m++)
                    mwriter.Write(neurallinks[i].bias[m]);
            }
        }


        public void ModelSave(string dnnModel, DNNModelVersion modelVersion)
        {
            switch (modelVersion)
            {
                case DNNModelVersion.DSSM_GPU_V0_2013:
                    throw new Exception("DSSM_GPU_V0_2013 Not Implemented!");

                case DNNModelVersion.CDSSM_GPU_V1_2013:
                    ModelSave_CDSSM_GPU_V1_2013(dnnModel);
                    break;
                case DNNModelVersion.C_OR_DSSM_GPU_V2_2014_MAY:
                    ModelSave_C_OR_DSSM_GPU_V2_2014_MAY(dnnModel);
                    break;
                case DNNModelVersion.C_OR_DSSM_GPU_V3_2014_NOV:
                    throw new Exception("C_OR_DSSM_GPU_V3_2014_NOV Not Implemented!");

                case DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR:
                    ModelSave_C_OR_DSSM_GPU_V4_2015_APR(dnnModel);
                    break;
            }
        }

        /// <summary>
        /// Load DNN Structure from File Stream.
        /// </summary>
        /// <param name="dnnModel"></param>
        public DNNStructure(string dnnModel, DNNModelVersion modelVersion, DeviceType device)
        {
            switch (modelVersion)
            {
                case DNNModelVersion.DSSM_GPU_V0_2013:
                    ModelLoad_DSSM_GPU_V0_2013(dnnModel, device);
                    break;
                case DNNModelVersion.CDSSM_GPU_V1_2013:
                    ModelLoad_CDSSM_GPU_V1_2013(dnnModel, device);
                    break;
                case DNNModelVersion.C_OR_DSSM_GPU_V2_2014_MAY:
                    ModelLoad_C_OR_DSSM_GPU_V2_2014_MAY(dnnModel, device);
                    break;
                case DNNModelVersion.C_OR_DSSM_GPU_V3_2014_NOV:
                    ModelLoad_C_OR_DSSM_GPU_V3_2014_NOV(dnnModel, device);
                    break;
                case DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR:
                    ModelLoad_C_OR_DSSM_GPU_V4_2015_APR(dnnModel, device);
                    break;
            }
        }

        public DNNStructure(Stream model, DNNModelVersion modelVersion, DeviceType device)
        {
            switch (modelVersion)
            {
                case DNNModelVersion.DSSM_GPU_V0_2013:
                    ModelLoad_DSSM_GPU_V0_2013(model, device);
                    break;
                case DNNModelVersion.CDSSM_GPU_V1_2013:
                    ModelLoad_CDSSM_GPU_V1_2013(model, device);
                    break;
                case DNNModelVersion.C_OR_DSSM_GPU_V2_2014_MAY:
                    ModelLoad_C_OR_DSSM_GPU_V2_2014_MAY(model, device);
                    break;
                case DNNModelVersion.C_OR_DSSM_GPU_V3_2014_NOV:
                    ModelLoad_C_OR_DSSM_GPU_V3_2014_NOV(model, device);
                    break;
                case DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR:
                    ModelLoad_C_OR_DSSM_GPU_V4_2015_APR(model, device);
                    break;
            }
        }

        public DNNStructure(Stream model, DeviceType device) : this(model, DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR, device)
        { }

        public DNNStructure(int featureSize, int[] layerDim, A_Func[] activation, bool[] isbias, N_Type[] arch, int[] wind, float[] dropOut, DeviceType device)
        {
            neurallayers.Add(featureSize);
            for (int i = 0; i < layerDim.Length; i++) { neurallayers.Add(layerDim[i]); }
            for (int i = 0; i < layerDim.Length; i++) { neurallinks.Add(new LayerStructure(neurallayers[i], neurallayers[i + 1], activation[i], arch[i], wind[i], dropOut[i], isbias[i], device)); }
        }

        /// <summary>
        /// Convolution DNN.
        /// </summary>
        /// <param name="featureSize"></param>
        /// <param name="layerDim"></param>
        /// <param name="activation"></param>
        /// <param name="isbias"></param>
        /// <param name="arch"></param>
        /// <param name="wind"></param>
        /// <param name="?"></param>
        public DNNStructure(int featureSize, int[] layerDim, A_Func[] activation, bool[] isbias, N_Type[] arch, int[] wind, float[] dropOut) :
            this(featureSize, layerDim, activation, isbias, arch, wind, dropOut, DeviceType.GPU)
        { }

        /// <summary>
        /// Fully Connected DNN.
        /// </summary>
        /// <param name="featureSize"></param>
        /// <param name="layerDim"></param>
        /// <param name="af"></param>
        /// <param name="isbias"></param>
        public DNNStructure(int featureSize, int[] layerDim, A_Func[] af, bool[] isbias, DeviceType device = DeviceType.GPU) :
            this(featureSize, layerDim, af, Enumerable.Range(0, layerDim.Length).Select(i => 0.0f).ToArray(), isbias, device)
        { }

        /// <summary>
        /// Fully Connected DNN.
        /// </summary>
        /// <param name="featureSize"></param>
        /// <param name="layerDim"></param>
        /// <param name="af"></param>
        /// <param name="isbias"></param>
        public DNNStructure(int featureSize, int[] layerDim, A_Func[] af, float[] dropOut, bool[] isbias, DeviceType device = DeviceType.GPU) :
            //int featureSize, int[] layerDim, A_Func[] activation, bool[] isbias, N_Type[] arch, int[] wind, float[] dropOut)
            this(featureSize, layerDim, af, isbias, Enumerable.Range(0, layerDim.Length).Select(i => N_Type.Fully_Connected).ToArray(),
               Enumerable.Range(0, layerDim.Length).Select(i => 1).ToArray(), dropOut, device)
        { }

        //public DNNStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        //{ }

        // public override void CopyFrom(IData other)
        // {
        //     DNNStructure model = (DNNStructure)other;
        //     for (int i = 0; i < neurallinks.Count; i++)
        //     {
        //         neurallinks[i].CopyFrom(model.neurallinks[i]);
        //     }
        // }

        public void Init()
        {
            for (int i = 0; i < neurallinks.Count; i++)
            {
                neurallinks[i].Init(DNNInitMethod.Xavior_Init);
            }
        }

        // public void Init(DNNStructure model)
        // {
        //     for (int i = 0; i < neurallinks.Count; i++)
        //     {
        //         neurallinks[i].CopyFrom(model.neurallinks[i]);
        //     }
        // }

        public void Init(float wei)
        {
            for (int i = 0; i < neurallinks.Count; i++)
            {
                neurallinks[i].Init(wei);
            }
        }

        public void Init(float wei_scale, float wei_bias)
        {
            for (int i = 0; i < neurallinks.Count; i++)
            {
                neurallinks[i].Init(wei_scale, wei_bias);
            }
        }
    }
}
