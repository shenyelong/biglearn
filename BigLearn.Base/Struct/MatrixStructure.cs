﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // same as embedding structure.
    // public class MatrixStructure : Structure
    // {
    //     public CudaPieceFloat Memory;

    //     public CudaPieceFloat MemoryGrad { get { return Memory.Grad; } }

    //     public int Dim { get; set; }
    //     public int Size { get; set; }

    //     //public GradientOptimizer MemoryOptimizer { get { return StructureOptimizer["GlobalMemory"].Optimizer; } set { StructureOptimizer["GlobalMemory"].Optimizer = value; } }

    //     public override Dictionary<string, CudaPieceFloat> Parameter
    //     {
    //         get
    //         {
    //             return new Dictionary<string, CudaPieceFloat>()
    //             {
    //                 { "GlobalMemory", Memory },
    //             };
    //         }
    //     } 

    //     public MatrixStructure(int dim, int size, DeviceType device) : base(device)
    //     {
    //         Dim = dim;
    //         Size = size;

    //         DeviceType = device;

    //         Memory = new CudaPieceFloat(Dim * Size, device);
    //         Memory.Init((float)(12.0f / Math.Sqrt(Dim + Size)), (float)(-6.0f / Math.Sqrt(Dim + Size)));
    //         //Memory.Norm(Dim, Size, VecNormType.L2Norm);
    //     }

    //     public override DataSourceID Type { get { return DataSourceID.MatrixStructure; } }
    // }
}
