using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // Recurrent Attention Network Structure.
    public class RANStructure : CompositeNNStructure 
    {
        public int StateDim { get { return QProject.Neural_In; } }
        public int RecurrentLayer { get { return GruCells.Count; } }

        DeviceType Device { get; set; }

        public LayerStructure QProject { get; set; }
        public LayerStructure KProject { get; set; }
        public LayerStructure VProject { get; set; }
        public List<GRUCell> GruCells { get; set; }

        //public LayerStructure Classifier { get; set; }
        // 
        // public LayerStructure T { get; set; }

        public override DataSourceID Type  {  get  { return DataSourceID.RANStructure; } }

        //public RANStructure(int state_dim, int recurrent_layer, int input_dim, DeviceType device)
        //{ }

        //(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropOut, bool isbias, DeviceType device)       
        public RANStructure(int state_dim, int recurrent_layer, DeviceType device)
        {
            Device = device;
 
            QProject = AddLayer(new LayerStructure(state_dim, state_dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            KProject = AddLayer(new LayerStructure(state_dim, state_dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            VProject = AddLayer(new LayerStructure(state_dim, state_dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

            GruCells = new List<GRUCell>();
            for(int i = 0; i < recurrent_layer; i++)
            {
                GruCells.Add(AddLayer(new GRUCell(state_dim, state_dim, device)));
            }
            // T = AddLayer(new LayerStructure(state_dim, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
        }

        // public override void Serialize(BinaryWriter mwriter)
        // {
        //     QProject.Serialize(mwriter);
        //     KProject.Serialize(mwriter);
        //     VProject.Serialize(mwriter);
            
        //     mwriter.Write(RecurrentLayer);
        //     for(int i = 0; i < RecurrentLayer; i++)
        //     {
        //         GruCells[i].Serialize(mwriter);
        //     }

        //     // T.Serialize(mwriter);
        // }

        // public RANStructure(BinaryReader reader, DeviceType device)
        // {
        //     Device = device;

        //     QProject = DeserializeModel<LayerStructure>(reader, device); 
        //     KProject = DeserializeModel<LayerStructure>(reader, device); 
        //     VProject = DeserializeModel<LayerStructure>(reader, device);

        //     //   int modelNum = CompositeNNStructure.DeserializeModelCount(reader)
        //     GruCells = new List<GRUCell>();
        //     int layer = reader.ReadInt32();
        //     for(int i = 0; i < layer; i++)
        //     {
        //         GruCells.Add(DeserializeModel<GRUCell>(reader, device));
        //     }

        //     // T = DeserializeModel<LayerStructure>(reader, device);
        // }
    }
}
