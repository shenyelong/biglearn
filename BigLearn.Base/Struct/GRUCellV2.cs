using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class GRUCellV2 : Structure
    {
        public int InputFeatureDim;
        public int HiddenStateDim;
        
        public override DataSourceID Type { get { return DataSourceID.GRUCellV2; } }

        public GRUCellV2(int input, int hiddenDim, DeviceType device) : base(device)
        {
            InputFeatureDim = input;
            HiddenStateDim = hiddenDim;

            IntArgument i = new IntArgument("in_dim", input + hiddenDim);
            IntArgument o = new IntArgument("out_dim", hiddenDim);
            IntArgument zr_o = new IntArgument("zr_dim", 2 * hiddenDim);

            AddParameter("wzr", new NdArrayData(device, false, zr_o, i));
            AddParameter("bzr", new NdArrayData(device, false, zr_o));

            AddParameter("wh", new NdArrayData(device, false, o, i));
            AddParameter("bh", new NdArrayData(device, false, o));

            //Wzr = new CudaPieceFloat((InputFeatureDim + HiddenStateDim) * HiddenStateDim * 2, device);
            //Bzr = new CudaPieceFloat(HiddenStateDim * 2, device);
            //Wh = new CudaPieceFloat((InputFeatureDim + HiddenStateDim) * HiddenStateDim, device);
            //Bh = new CudaPieceFloat(HiddenStateDim, device);
            Init();
        }

        public CudaPieceFloat Wzr { get { return Parameter["wzr"].Output; } }
        public CudaPieceFloat Bzr { get { return Parameter["bzr"].Output; } }
        public CudaPieceFloat BzrGrad { get { return Parameter["bzr"].Deriv; } }
        public CudaPieceFloat WzrGrad { get { return Parameter["wzr"].Deriv; } }

        public CudaPieceFloat Wh { get { return Parameter["wh"].Output; } }
        public CudaPieceFloat Bh { get { return Parameter["bh"].Output; } }
        public CudaPieceFloat WhGrad { get { return Parameter["wh"].Deriv; } }
        public CudaPieceFloat BhGrad { get { return Parameter["bh"].Deriv; } }

        // public override Dictionary<string, CudaPieceFloat> Parameter
        // {
        //     get
        //     {
        //         Dictionary<string, CudaPieceFloat> tmp = new Dictionary<string, CudaPieceFloat>()
        //         {
        //             { "GRU_Wzr", Wzr },
        //             { "GRU_Bzr", Bzr },
        //             { "GRU_Wh", Wh },
        //             { "GRU_Bh", Bh },
        //         };
        //         return tmp;
        //     }
        // }

        public void Init()
        {
            int input = (InputFeatureDim + HiddenStateDim);
            int output = HiddenStateDim;

            float scale = (float)Math.Sqrt(6.0 / (input + output)); 
            Wzr.Init(scale * 2, -scale); 
            Bzr.Init(0);
            Wh.Init(scale * 2, -scale);
            Bh.Init(0);
        }
    }
}