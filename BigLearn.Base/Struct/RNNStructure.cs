﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RNNCell : Structure
    {
        public int InputFeatureDim;
        public int HiddenStateDim;
        public int HistoryLag;
        public A_Func Af;
        public bool IsBias = true;

        /// <summary>
        /// Input to Hidden State Matrix.
        /// </summary>
        public NdArrayData IMatrix { get { return Parameter["im"]; } }
        
        /// <summary>
        /// Hidden State History Matrix.
        /// </summary>
        public NdArrayData WMatrix { get { return Parameter["wm"]; } }

        /// <summary>
        /// Hidden State Bias.
        /// </summary>
        public NdArrayData Bias { get { return Parameter["bias"]; } }

        //public GradientOptimizer IMatrixOptimizer { get { return StructureOptimizer["IMatrix"].Optimizer; } }
        //public GradientOptimizer WMatrixOptimizer { get { return StructureOptimizer["WMatrix"].Optimizer; } }
        //public GradientOptimizer BiasOptimizer { get { return StructureOptimizer["Bias"].Optimizer; } }
        
        public RNNCell(int input, int hidden, int lag, A_Func af, bool isBias) : this(input, hidden, lag, af, isBias, DeviceType.GPU)
        { }

        public RNNCell(int input, int hidden, int lag, A_Func af, bool isBias, DeviceType device) : base(device)
        {
            IntArgument in_dim = new IntArgument("indim", input);
            IntArgument cell_dim = new IntArgument("cell", hidden); 
            IntArgument lag_dim = new IntArgument("lag", hidden * lag);

            AddParameter("im", new NdArrayData(device, false, cell_dim, in_dim));
            AddParameter("wm", new NdArrayData(device, false, cell_dim, lag_dim));
            AddParameter("bias", new NdArrayData(device, false, cell_dim));

            InputFeatureDim = input;
            HiddenStateDim = hidden;
            HistoryLag = lag;
            Af = af;
            IsBias = isBias;

            Init();
        }


        //public RNNCell(BinaryReader reader, DeviceType device) : base(reader, device)
        //{ }

        // public override Dictionary<string, CudaPieceFloat> Parameter
        // {
        //     get
        //     {
        //         return new Dictionary<string, CudaPieceFloat>()
        //         {
        //             { "IMatrix", IMatrix },
        //             { "WMatrix", WMatrix },
        //             { "Bias", Bias },
        //         };
        //     }
        // }

        public void Init()
        {
            IMatrix.Output.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            WMatrix.Output.Init((float)(Math.Sqrt(0.6 / (HiddenStateDim * HistoryLag + HiddenStateDim)) * 2), (float)(-Math.Sqrt(0.6 / (HiddenStateDim * HistoryLag + HiddenStateDim))));
            Bias.Output.Init(0);
        }
    }

    public class RNNStructure : Structure 
    {
        ///Multi Layer RNN Model.
        public List<RNNCell> RNNCells = new List<RNNCell>();

        public override List<Structure> SubStructure { get { return RNNCells.Select(i => (Structure)i).ToList(); } }

        public RNNStructure(int inputFea, int[] hiddenStates, int[] lags, A_Func[] afs, bool[] isBiases) : this(inputFea, hiddenStates, lags, afs, isBiases, DeviceType.GPU)
        { }

        public RNNStructure(int inputFea, int[] hiddenStates, int[] lags, A_Func[] afs, bool[] isBiases, DeviceType device)
        {
            int input = inputFea;
            for (int i = 0; i < hiddenStates.Length; i++)
            {
                RNNCell cell = new RNNCell(input, hiddenStates[i], lags[i], afs[i], isBiases[i], device);
                cell.Init();
                RNNCells.Add(cell);
                input = hiddenStates[i];
            }
        }
    }
}
