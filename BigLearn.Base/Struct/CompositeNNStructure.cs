﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class CompositeNNStructure : Structure 
    {
        public override List<Structure> SubStructure { get { return CompositeLinks.Select(i => (Structure)i).ToList(); } }

        public List<Structure> CompositeLinks = new List<Structure>();

        /// <summary>
        /// It only support certain type of structure.
        /// </summary>
        public CompositeNNStructure() { }

        public CompositeNNStructure(DeviceType device) : base(device) { }

        public T AddLayer<T>(T structure) where T : Structure
        {
            CompositeLinks.Add(structure);
            return structure;
        }
    }
}
