﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class EmbedStructure : Structure
    {
        public CudaPieceFloat Embedding { get { return Parameter["embed"].Output; } }
        
        public CudaPieceFloat EmbeddingGrad { get { return Parameter["embed"].Deriv; } } // { get { return Embedding.Grad; } set { Embedding.Grad = value; } }

        public NdArrayData NDEmbed { get  { return Parameter["embed"]; } }

        public int VocabSize { get; private set; }
        public int Dim { get; private set; }

        public override DataSourceID Type { get { return DataSourceID.EmbedStructure; } }
        
        // public override Dictionary<string, CudaPieceFloat> Parameter { get { return new Dictionary<string, CudaPieceFloat>() { { "Embedding", Embedding } }; } }

        // public GradientOptimizer EmbeddingOptimizer { get { return StructureOptimizer["embed"]; } set { StructureOptimizer.Add("embed", value); } }
        
        public EmbedStructure(int vocabSize, int dimension, DeviceType device) : this(vocabSize, dimension, -(float)Math.Sqrt(6.0f / (vocabSize + dimension)), (float)Math.Sqrt(6.0f /(vocabSize + dimension)), device)	
        { }

        public EmbedStructure(int vocabSize, int dimension, float min, float max, DeviceType device) : this(vocabSize, dimension, new CudaPieceFloat(vocabSize * dimension, device), device)
        {
            Logger.WriteLog("embedding init {0}, {1}", min, max);
            Init(min, max);
        }

        public EmbedStructure(int vocabSize, int dimension, CudaPieceFloat embed, DeviceType device) : base(device)
        {
            VocabSize = vocabSize;
            Dim = dimension;
            
            IntArgument vocab = new IntArgument("vocab", vocabSize); 
            IntArgument dim = new IntArgument("dim", dimension);

            AddParameter("embed", new NdArrayData(device, embed, null, dim, vocab));
            //Embedding = embed;
        }

        public void Init(float min, float max)
        {
            Embedding.Init(max - min, min);
        }
    }
}
