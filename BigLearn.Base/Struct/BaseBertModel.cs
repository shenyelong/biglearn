using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
        public class BaseBertModel : CompositeNNStructure
        {
            public int vocabSize = 30522;
            public int pos = 512;
            public int segType = 2;
            public int embed = 768;
            public int layer = 12;
            public int category = 2;
            public EmbedStructure TokenEmbed { get; set; }
            public NdArrayData NdTokenEmbed { get { return TokenEmbed.ToND(); } }

            public EmbedStructure PosEmbed { get; set; }
            public EmbedStructure TokenTypeEmbed { get; set; }

            public LayerStructure InputNorm { get; set; }
            
            VectorData mScale = null;
            public VectorData NormScale { get { if(mScale == null) mScale = new VectorData(embed, InputNorm.weight, InputNorm.WeightGrad, InputNorm.DeviceType); return mScale; } }
            public NdArrayData NdNormScale { get { return NormScale.ToND(); } }
            
            VectorData mBias = null;
            public VectorData NormBias { get { if(mBias == null) mBias = new VectorData(embed, InputNorm.bias, InputNorm.BiasGrad, InputNorm.DeviceType); return mBias; } }
            public NdArrayData NdNormBias { get { return NormBias.ToND(); } }

            VectorData tBias = null;
            public VectorData TBias { get { if(tBias == null) tBias = new VectorData(vocabSize, TokenBias.bias, InputNorm.BiasGrad, InputNorm.DeviceType); return tBias; } }
            public NdArrayData NdTBias { get { return TBias.ToND(); } }

            VectorData oScale = null;
            public VectorData OutputNormScale { get { if(oScale == null) oScale = new VectorData(embed, OutputNorm.weight, OutputNorm.WeightGrad, OutputNorm.DeviceType); return oScale; } }
            public NdArrayData NdOutputNormScale { get { return OutputNormScale.ToND(); } }

            VectorData oBias = null;
            public VectorData OutputNormBias { get { if(oBias == null) oBias = new VectorData(embed, OutputNorm.bias, OutputNorm.BiasGrad, OutputNorm.DeviceType); return oBias; } }
            public NdArrayData NdOutputNormBias { get { return OutputNormBias.ToND(); } }

            public TransformerStructure[] Blocks { get; set; }

            public LayerStructure Pooler { get; set; }
            public LayerStructure Classifier { get; set; }            
            public LayerStructure OutputLayer { get; set; }
            public LayerStructure OutputNorm { get; set; }
            public LayerStructure TokenBias { get; set; }

            public BaseBertModel(int mlayer, DeviceType device) : this(2, mlayer, -1, device)
            { }

            public BaseBertModel(int mcate, int mlayer, DeviceType device) : this(mcate, mlayer, -1, device)
            { }

            public BaseBertModel(DeviceType device) : this(2, 12, -1, device)
            { }

            public BaseBertModel(int mcate, int mlayer, int frozenlayer, DeviceType device) : this(mcate, mlayer, frozenlayer, 30522, device)
            { }
            
            public BaseBertModel(int mcate, int mlayer, int frozenlayer, int mvocabSize, DeviceType device) : this(mcate, mlayer, frozenlayer, 768, mvocabSize, device)
            { }

            public BaseBertModel(int mcate, int mlayer, int frozenlayer, int dim, int mvocabSize, DeviceType device) : this(mcate, mlayer, frozenlayer, dim, 512, mvocabSize, device)
            { }

            public BaseBertModel(int mcate, int mlayer, int frozenlayer, int dim, int max_seq_len, int mvocabSize, DeviceType device)
            {
                layer = mlayer;
                category = mcate;
                vocabSize = mvocabSize;
                embed = dim;
                pos = max_seq_len;
                
                if(frozenlayer >= 0)
                {
                    TokenEmbed = new EmbedStructure(vocabSize, embed, -0.034f, 0.034f, device); 
                    PosEmbed = new EmbedStructure(pos, embed, -0.034f, 0.034f, device); 
                    TokenTypeEmbed = new EmbedStructure(segType, embed, -0.034f, 0.034f, device); 
                    InputNorm = new LayerStructure(1, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device);
                }
                else
                {
                    TokenEmbed = AddLayer(new EmbedStructure(vocabSize, embed, -0.034f, 0.034f, device)); 
                    PosEmbed = AddLayer(new EmbedStructure(pos, embed, -0.034f, 0.034f, device)); 
                    TokenTypeEmbed = AddLayer(new EmbedStructure(segType, embed, -0.034f, 0.034f, device)); 
                    InputNorm = AddLayer(new LayerStructure(1, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                }

                InputNorm.weight.Init(1);
                InputNorm.bias.Init(0);

                Blocks = new TransformerStructure[layer];
                for(int i=0; i < layer; i++)
                {
                    if(frozenlayer >= i + 1)
                    {
                        Blocks[i] = new TransformerStructure(embed, device);
                    }
                    else
                    {
                        Blocks[i] = AddLayer(new TransformerStructure(embed, device));
                    }
                }


                Pooler = AddLayer(new LayerStructure(embed, embed, A_Func.Tanh, N_Type.Convolution_layer, 1, 0, true, device));
                Pooler.bias.Init(0);
                Pooler.weight.Init(2 * 0.034f, -0.034f);

                Classifier = AddLayer(new LayerStructure(embed, category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                Classifier.weight.Init(2 * 0.034f, -0.034f);
                Classifier.bias.Init(0);

                OutputLayer = AddLayer(new LayerStructure(embed, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                OutputLayer.weight.Init(2 * 0.034f, -0.034f);

                OutputNorm = AddLayer(new LayerStructure(1, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device)); 
                OutputNorm.weight.Init(1);
                OutputNorm.bias.Init(0);

                TokenBias = AddLayer(new LayerStructure(1, vocabSize, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                TokenBias.bias.Init(0);
            }
            
            public unsafe BaseBertModel(string meta_file, string bin_file, DeviceType device) : this(-1, 30522, 2, meta_file, bin_file, device)
            { }

            public unsafe BaseBertModel(int mvocabSize, int mcate, string meta_file, string bin_file,  DeviceType device) : this(-1, mvocabSize, mcate, meta_file, bin_file, device) 
            { }

            public unsafe BaseBertModel(int mcate, string meta_file, string bin_file, DeviceType device) : this(-1, 30522, mcate, meta_file, bin_file, device)
            { }

            public unsafe BaseBertModel(int frozenlayer, int mvocabSize, int mcate, string meta_file, string bin_file, DeviceType device) : this(frozenlayer, mvocabSize, mcate, 12, 768, meta_file, bin_file, device)
            { }

            public unsafe BaseBertModel(int frozenlayer, int mvocabSize, int mcate, int mlayer, int mdim, string meta_file, string bin_file, DeviceType device) 
                : this(mcate, mlayer, frozenlayer, mdim, 512, mvocabSize, device)
            {
                Logger.WriteLog("reading from the bert pretrained model.");
                using(StreamReader metaReader = new StreamReader(new FileStream(meta_file, FileMode.Open, FileAccess.Read, FileShare.Read))) //meta_file, FileMode.Open, FileAccess.Read, FileShare.Read))
                using(BinaryReader binReader = new BinaryReader(new FileStream(bin_file, FileMode.Open, FileAccess.Read, FileShare.Read)))
                {
                    while(!metaReader.EndOfStream)
                    {
                        string[] items = metaReader.ReadLine().Split('\t');
                        string itemName = items[0];
                        int itemSize = int.Parse(items[1]);

                        if(itemName.Equals("bert/embeddings/word_embeddings:0"))
                        {
                            TokenEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize TokenEmbed from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/token_type_embeddings:0"))
                        {
                            TokenTypeEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize TokenTypeEmbed from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/position_embeddings:0"))
                        {
                            PosEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize PosEmbed from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/LayerNorm/gamma:0"))
                        {
                            InputNorm.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize embedding-gamma from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/LayerNorm/beta:0"))
                        {
                            InputNorm.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize embedding-beta from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/pooler/dense/kernel:0"))
                        {
                            Pooler.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize pooling-weight from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/pooler/dense/bias:0"))
                        {
                            Pooler.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize pooling-bias from checkpoint");
                            continue;
                        }

                        //Component is not found cls/predictions/output_bias:0 : size 30522, skip...
                        //Component is not found cls/predictions/transform/LayerNorm/beta:0 : size 1024, skip...
                        //Component is not found cls/predictions/transform/LayerNorm/gamma:0 : size 1024, skip...
                        //Component is not found cls/predictions/transform/dense/bias:0 : size 1024, skip...
                        //Component is not found cls/predictions/transform/dense/kernel:0 : size 1048576, skip...
                        //Component is not found cls/seq_relationship/output_bias:0 : size 2, skip...
                        //Component is not found cls/seq_relationship/output_weights:0 : size 2048, skip...

                        // skip.
                        if(itemName.Equals("cls/predictions/output_bias:0"))
                        {
                            TokenBias.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/output_bias:0");
                            continue;
                        }
                        // skip ZN661XUS7JX297447
                        if(itemName.Equals("cls/predictions/transform/LayerNorm/beta:0"))
                        {
                            OutputNorm.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/transform/LayerNorm/beta:0");
                            continue;
                        }
                        if(itemName.Equals("cls/predictions/transform/LayerNorm/gamma:0"))
                        {
                            OutputNorm.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/transform/LayerNorm/gamma:0");
                            continue;
                        }
                        if(itemName.Equals("cls/predictions/transform/dense/bias:0"))
                        {
                            OutputLayer.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/transform/dense/bias:0");
                            continue;
                        }
                        if(itemName.Equals("cls/predictions/transform/dense/kernel:0"))
                        {
                            OutputLayer.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/transform/dense/kernel:0");
                            continue;
                        }

                        if(category == 2)
                        {
                            // next sentence prediction weight.
                            if(itemName.Equals("cls/seq_relationship/output_bias:0"))
                            {
                                Classifier.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize output-bias from checkpoint {0}", itemSize);
                                continue;
                            }
                            // next sentence prediction weight.
                            if(itemName.Equals("cls/seq_relationship/output_weights:0"))
                            {
                               Classifier.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                               BasicMathlib.Transpose(Classifier.weight.CpuPtr, 2, embed);
                               Logger.WriteLog("Initialize output-weight from checkpoint {0}", itemSize);
                               continue;
                            }
                        }
                        
                        bool isFound = false;

                        for(int c = 0; c < layer; c++)
                        {
                            if(isFound) break;

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/query/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                //Blocks[c].QKVProject.weight.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                //Buffer.BlockCopy(src, offset, MemPtr, Offset * sizeof(float), length);
                                //Blocks[c].QProject.weight.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;

                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/query/bias:0"))
                            {
                                byte[] mem = binReader.ReadBytes(sizeof(float) * itemSize);
                                Blocks[c].QKVProject.bias.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                //Blocks[c].QProject.bias.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/key/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr + embed, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                //Blocks[c].QKVProject.weight.BlockCopy(sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/key/bias:0"))
                            {
                                Blocks[c].QKVProject.bias.BlockCopy(sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/value/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr + 2 * embed, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                //Blocks[c].QKVProject.weight.BlockCopy(2 * sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/value/bias:0"))
                            {
                                Blocks[c].QKVProject.bias.BlockCopy(2 * sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/dense/kernel:0"))
                            {
                                Blocks[c].AttProject.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/dense/bias:0"))
                            {
                                Blocks[c].AttProject.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/LayerNorm/gamma:0"))
                            {
                                Blocks[c].Norm1.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/LayerNorm/beta:0"))
                            {
                                Blocks[c].Norm1.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/intermediate/dense/kernel:0"))
                            {
                                Blocks[c].MLP1.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/intermediate/dense/bias:0"))
                            {
                                Blocks[c].MLP1.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/dense/kernel:0"))
                            {
                                Blocks[c].MLP2.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/dense/bias:0"))
                            {
                                Blocks[c].MLP2.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/LayerNorm/gamma:0"))
                            {
                                Blocks[c].Norm2.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/LayerNorm/beta:0"))
                            {
                                Blocks[c].Norm2.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                        }
                        if(!isFound)
                        {
                            Logger.WriteLog("Component is not found {0} : size {1}, skip...", itemName, itemSize);
                            binReader.ReadBytes(sizeof(float) * itemSize);
                        }
                    }
                }
                TokenEmbed.Embedding.SyncFromCPU();
                TokenTypeEmbed.Embedding.SyncFromCPU();
                PosEmbed.Embedding.SyncFromCPU();
                InputNorm.SyncFromCPU();
                
                Pooler.SyncFromCPU();
                Classifier.SyncFromCPU();

                OutputLayer.SyncFromCPU();
                OutputNorm.SyncFromCPU();
                TokenBias.SyncFromCPU();

                for(int c = 0; c < layer; c++) Blocks[c].SyncFromCPU();
            }
        }
}
