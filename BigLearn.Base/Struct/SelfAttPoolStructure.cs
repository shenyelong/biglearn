using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    // Recurrent Attention Network Structure.
    public class SelfAttPoolStructure : CompositeNNStructure 
    {
        DeviceType Device { get; set; }

        public int StateDim { get { return KProject.Neural_Out; } }
        public int InputDim { get { return KProject.Neural_In; }}
        
        public EmbedStructure QProject { get; set; }

        public LayerStructure KProject { get; set; }
        public LayerStructure VProject { get; set; }
        // public LayerStructure Classifier { get; set; }
        // 
        // public LayerStructure T { get; set; }

        public override DataSourceID Type  {  get  { return DataSourceID.SelfAttPool; } }

        //(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropOut, bool isbias, DeviceType device)       
        public SelfAttPoolStructure(int input_dim, int state_dim, DeviceType device)
        {
            Device = device;
            
            //StateDim = state_dim;
            //InputDim = input_dim;

            QProject = AddLayer(new EmbedStructure(1, state_dim, -0.034f, 0.034f, device)); // int vocabSize, int dimension, DeviceType device)(1, state_dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            KProject = AddLayer(new LayerStructure(input_dim, state_dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            VProject = AddLayer(new LayerStructure(input_dim, state_dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
        }

        // public SelfAttPoolStructure(BinaryReader reader, DeviceType device)
        // {
        //     Device = device;
        //     QProject = DeserializeModel<EmbedStructure>(reader, device); 
        //     KProject = DeserializeModel<LayerStructure>(reader, device); 
        //     VProject = DeserializeModel<LayerStructure>(reader, device);
        // }

        // public override void Serialize(BinaryWriter mwriter)
        // {
        //     QProject.Serialize(mwriter);

        //     KProject.Serialize(mwriter);
        //     VProject.Serialize(mwriter);
            
        //     // T.Serialize(mwriter);
        // }


    }
}
