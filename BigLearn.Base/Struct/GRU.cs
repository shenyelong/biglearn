﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum RndRecurrentInit { RndNorm = 0, RndOrthogonal = 1 }
    
    public class GRUCell : Structure 
    {
        //RndRecurrentInit RndInit = RndRecurrentInit.RndNorm;
        public GRUCell(int input, int hiddenDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm) : this(input, 0, hiddenDim, device, rndInit)
        { }

        public GRUCell(int input, int attDim, int hiddenDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm) : base(device)
        {
            IntArgument in_dim = new IntArgument("indim", input);
            IntArgument cell_dim = new IntArgument("cell", hiddenDim); 
            IntArgument att_dim = new IntArgument("attdim", attDim);

            AddParameter("wr", new NdArrayData(device, false, cell_dim, in_dim));
            AddParameter("wz", new NdArrayData(device, false, cell_dim, in_dim));
            AddParameter("wh", new NdArrayData(device, false, cell_dim, in_dim));

            AddParameter("ur", new NdArrayData(device, false, cell_dim, cell_dim));
            AddParameter("uz", new NdArrayData(device, false, cell_dim, cell_dim));
            AddParameter("uh", new NdArrayData(device, false, cell_dim, cell_dim));

            AddParameter("br", new NdArrayData(device, false, cell_dim));
            AddParameter("bz", new NdArrayData(device, false, cell_dim));
            AddParameter("bh", new NdArrayData(device, false, cell_dim));

            if(attDim > 0)
            {
                AddParameter("cr", new NdArrayData(device, false, cell_dim, att_dim));
                AddParameter("cz", new NdArrayData(device, false, cell_dim, att_dim));
                AddParameter("ch", new NdArrayData(device, false, cell_dim, att_dim));
            }
            //InputFeatureDim = input;
            //HiddenStateDim = hiddenDim;
            //AttentionDim = attDim;
            //RndInit = rndInit;
            //DeviceType = device;

            //InitMemory(device);
            Init(rndInit);
        }

        // public GRUCell(BinaryReader reader, DeviceType device) : base(reader, device)
        // { }
        public override DataSourceID Type { get { return DataSourceID.GRUCell; } }

        //public int InputFeatureDim { get { return Parameter["wi"].Shape[1].Default; } }
        //public int CellDim { get { return Parameter["wi"].Shape[0].Default; } }
        //public bool AttentionGate { get{ return Parameter.ContainsKey("zi"); } }
        //public int AttentionDim { get { return AttentionGate ? Parameter["zi"].Shape[1].Default : 0; } }
        public int InputFeatureDim { get { return Parameter["wr"].Shape[1].Default; } }
        public int HiddenStateDim { get { return Parameter["wr"].Shape[0].Default; } }
        public bool AttentionGate { get{ return Parameter.ContainsKey("cr"); } }
        public int AttentionDim { get { return AttentionGate ? Parameter["cr"].Shape[1].Default : 0; } }

        public CudaPieceFloat Wr { get { return Parameter["wr"].Output; } }
        public CudaPieceFloat WrGrad { get { return Parameter["wr"].Deriv; } }
        
        public CudaPieceFloat Ur { get { return Parameter["ur"].Output; } }
        public CudaPieceFloat UrGrad { get { return Parameter["ur"].Deriv; } }

        public CudaPieceFloat Br { get { return Parameter["br"].Output; } }
        public CudaPieceFloat BrGrad { get { return Parameter["br"].Deriv; } }
        
        public CudaPieceFloat Cr { get { return Parameter["cr"].Output; } }
        public CudaPieceFloat CrGrad { get { return Parameter["cr"].Deriv; } }

        public CudaPieceFloat Wz { get { return Parameter["wz"].Output; } }
        public CudaPieceFloat WzGrad { get { return Parameter["wz"].Deriv; } }
        
        public CudaPieceFloat Uz { get { return Parameter["uz"].Output; } }
        public CudaPieceFloat UzGrad { get { return Parameter["uz"].Deriv; } }

        public CudaPieceFloat Bz { get { return Parameter["bz"].Output; } }
        public CudaPieceFloat BzGrad { get { return Parameter["bz"].Deriv; } }
        
        public CudaPieceFloat Cz { get { return Parameter["cz"].Output; } }
        public CudaPieceFloat CzGrad { get { return Parameter["cz"].Deriv; } }

        public CudaPieceFloat Uh { get { return Parameter["uh"].Output; } }
        public CudaPieceFloat UhGrad { get { return Parameter["uh"].Deriv; } }

        public CudaPieceFloat Wh { get { return Parameter["wh"].Output; } }
        public CudaPieceFloat WhGrad { get { return Parameter["wh"].Deriv; } }

        public CudaPieceFloat Bh { get { return Parameter["bh"].Output; } }
        public CudaPieceFloat BhGrad { get { return Parameter["bh"].Deriv; } }
        
        public CudaPieceFloat Ch { get { return Parameter["ch"].Output; } }
        public CudaPieceFloat ChGrad { get { return Parameter["ch"].Deriv; } }

        // public override Dictionary<string, CudaPieceFloat> Parameter
        // {
        //     get
        //     {
        //         Dictionary<string, CudaPieceFloat> tmp = new Dictionary<string, CudaPieceFloat>()
        //         {
        //             { "GRU_Wr", Wr },
        //             { "GRU_Ur", Ur },
        //             { "GRU_Br", Br },

        //             { "GRU_Wz", Wz },
        //             { "GRU_Uz", Uz },
        //             { "GRU_Bz", Bz },

        //             { "GRU_Wh", Wh },
        //             { "GRU_Uh", Uh },
        //             { "GRU_Bh", Bh },
        //         };

        //         if(AttentionDim > 0)
        //         {
        //             tmp.Add("GRU_Cr", Cr);
        //             tmp.Add("GRU_Cz", Cz);
        //             tmp.Add("GRU_Ch", Ch);
        //         }
        //         return tmp;
        //     }
        // }

        // public void InitMemory(DeviceType device)
        // {
        //     Wr = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);
        //     //WrGrad = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);

        //     Ur = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);
        //     //UrGrad = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);

        //     Br = new CudaPieceFloat(HiddenStateDim, device);
        //     //BrGrad = new CudaPieceFloat(HiddenStateDim, device);

        //     Wz = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);
        //     //WzGrad = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);

        //     Uz = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);
        //     //UzGrad = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);

        //     Bz = new CudaPieceFloat(HiddenStateDim, device);
        //     //BzGrad = new CudaPieceFloat(HiddenStateDim, device);

        //     Wh = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);
        //     //WhGrad = new CudaPieceFloat(InputFeatureDim * HiddenStateDim, device);

        //     Uh = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);
        //     //UhGrad = new CudaPieceFloat(HiddenStateDim * HiddenStateDim, device);

        //     Bh = new CudaPieceFloat(HiddenStateDim, device);
        //     //BhGrad = new CudaPieceFloat(HiddenStateDim, device);

        //     if (AttentionDim > 0)
        //     {
        //         Cr = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);
        //         //CrGrad = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);

        //         Cz = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);
        //         //CzGrad = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);

        //         Ch = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);
        //         //ChGrad = new CudaPieceFloat(AttentionDim * HiddenStateDim, true, device == DeviceType.GPU);
        //     }
        // }

        public void Init(RndRecurrentInit RndInit)
        {
            Wr.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            //float[,] ur = RandomUtil.GetRandomOrthogonalMatrix(HiddenStateDim);
            if(RndInit == RndRecurrentInit.RndNorm) Ur.Init((float)(Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim))));
            else if(RndInit == RndRecurrentInit.RndOrthogonal) Ur.InitRandomOrthogonalMatrix(HiddenStateDim);
            Br.Init(0);

            Wz.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            if (RndInit == RndRecurrentInit.RndNorm) Uz.Init((float)(Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim))));
            else if (RndInit == RndRecurrentInit.RndOrthogonal) Uz.InitRandomOrthogonalMatrix(HiddenStateDim);
            Bz.Init(0);

            Wh.Init((float)(Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputFeatureDim + HiddenStateDim))));
            if (RndInit == RndRecurrentInit.RndNorm) Uh.Init((float)(Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenStateDim + HiddenStateDim))));
            else if (RndInit == RndRecurrentInit.RndOrthogonal) Uh.InitRandomOrthogonalMatrix(HiddenStateDim);
            Bh.Init(0);

            if(AttentionDim > 0)
            {
                Cr.Init((float)(Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim))));
                Cz.Init((float)(Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim))));
                Ch.Init((float)(Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim)) * 2), (float)(-Math.Sqrt(6.0 / (AttentionDim + HiddenStateDim))));
            }
        }

    }

    public class GRUStructure : Structure 
    {
        public List<GRUCell> GRUCells = new List<GRUCell>();

        public override DataSourceID Type { get { return DataSourceID.GRUStructure; } }

        public override List<Structure> SubStructure { get { return GRUCells.Select(i => (Structure)i).ToList(); } }

        public GRUStructure(int inputFea, int[] hiddenStates) : this(inputFea, hiddenStates, DeviceType.GPU) { }

        public GRUStructure(int inputFea, int[] hiddenStates, DeviceType device, RndRecurrentInit rInit = RndRecurrentInit.RndNorm) : this(inputFea, hiddenStates, Enumerable.Range(0, hiddenStates.Length).Select(i => 0).ToArray(), device, rInit)
        { }

        public GRUStructure(int inputFea, int[] hiddenStates, int[] attentionStates, DeviceType device, RndRecurrentInit rInit = RndRecurrentInit.RndNorm)
        {
            int input = inputFea;
            for (int i = 0; i < hiddenStates.Length; i++)
            {
                GRUCell cell = new GRUCell(input, attentionStates[i], hiddenStates[i], device, rInit);
                GRUCells.Add(cell);
                input = hiddenStates[i];
            }
        }

        public GRUStructure(List<GRUCell> cells) { GRUCells = cells; }

        public string Descr()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("GRU Layer Number {0}\n", GRUCells.Count));
            sb.Append(string.Join("\n", GRUCells.Select(i => string.Format("Input Dim {0}, Output Dim {1}, Attention Dim {2}", i.InputFeatureDim, i.HiddenStateDim, i.AttentionDim))));
            return sb.ToString();
        }
    }
}