﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BigLearn
{
    // public class NormLayerStructure : Structure 
    // {
    //     public int NeuronNum;
    //     public CudaPieceFloat Beta;
    //     public CudaPieceFloat Gamma;

    //     public CudaPieceFloat Mu;
    //     public CudaPieceFloat Sigma;

    //     //public GradientOptimizer BetaOptimizer { get { return StructureOptimizer["Beta"].Optimizer; } }
    //     //public GradientOptimizer GammaOptimizer { get { return StructureOptimizer["Gamma"].Optimizer; } }

    //     public NormLayerStructure(int dim)
    //     {
    //         NeuronNum = dim;
    //         Beta = new CudaPieceFloat(NeuronNum, true, true);
    //         Gamma = new CudaPieceFloat(NeuronNum, true, true);
            
    //         Gamma.Init(1.0f);
    //         Mu = new CudaPieceFloat(NeuronNum, true, true);
    //         Sigma = new CudaPieceFloat(NeuronNum, true, true);
    //     }
        
    //     //public NormLayerStructure(BinaryReader reader, DeviceType device) : base(reader, device)
    //     //{ }

    //     public override DataSourceID Type { get { return DataSourceID.NormLayerStructure; } }

    //     public void ReInit()
    //     {
    //         Mu.Zero();
    //         Sigma.Zero();
    //     }

    //     public override Dictionary<string, CudaPieceFloat> Parameter
    //     {
    //         get
    //         {
    //             return new Dictionary<string, CudaPieceFloat>()
    //             {
    //                 { "Beta", Beta },
    //                 { "Gamma", Gamma },
    //             };
    //         }
    //     }

    //     //protected override void InitStructureOptimizer()
    //     //{
    //     //    StructureOptimizer.Add("Beta", new ModelOptimizer() { Parameter = Beta });
    //     //    StructureOptimizer.Add("Gamma", new ModelOptimizer() { Parameter = Gamma });
    //     //}
    // }
}
