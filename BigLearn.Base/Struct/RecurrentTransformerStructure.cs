using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class RecurrentTransformerStructure : CompositeNNStructure 
    {
        public int Dim { get; set; } 

        DeviceType Device { get; set; }
        
        public LayerStructure QProject { get; set; }

        //public LayerStructure QProject { get; set; }

        public LayerStructure Norm1 { get; set; }
        public LayerStructure Norm2 { get; set; }

        public VectorData Norm1Scale { get { return new VectorData(Dim, Norm1.weight, Norm1.weightGrad, Device); } }
        public VectorData Norm1Bias { get { return new VectorData(Dim, Norm1.bias, Norm1.biasGrad, Device); } }
        
        public VectorData Norm2Scale { get { return new VectorData(Dim, Norm2.weight, Norm2.weightGrad, Device); } }
        public VectorData Norm2Bias { get { return new VectorData(Dim, Norm2.bias, Norm1.biasGrad, Device); } }
        
        public LayerStructure AttProject { get; set; }

        public LayerStructure  MLP1 { get; set; }
        public LayerStructure  MLP2 { get; set; }

        // basicly, no useful at this moment.
        public override DataSourceID Type  {  get  { return DataSourceID.ExternalTransformerStructure; } }

        // public override void SyncFromCPU()
        // {
        //     //QProject.SyncFromCPU();

        //     QProject.SyncFromCPU();
        //     AttProject.SyncFromCPU();

        //     MLP1.SyncFromCPU();
        //     MLP2.SyncFromCPU();
        //     Norm1.SyncFromCPU();
        //     Norm2.SyncFromCPU();
        //     Console.WriteLine("Transformer sync from CPU");
        // }
        //(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropOut, bool isbias, DeviceType device)       
        public RecurrentTransformerStructure(int dim, DeviceType device)
        {
            Dim = dim;
            Device = device;

            //QProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            
            QProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            AttProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
    
            MLP1 = AddLayer(new LayerStructure(dim, dim * 4, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            MLP2 = AddLayer(new LayerStructure(dim * 4, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

            Norm1 = AddLayer(new LayerStructure(1, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            Norm1.weight.Init(1);
            Norm1.bias.Init(0);

            Norm2 = AddLayer(new LayerStructure(1, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            Norm2.weight.Init(1);
            Norm2.bias.Init(0);

            //Norm1Scale = new VectorData(dim, Norm1.weight,  )
        }

        // public RecurrentTransformerStructure(BinaryReader reader, DeviceType device)
        // {
        //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
        //     QProject = DeserializeModel<LayerStructure>(reader, device); 
        //     AttProject = DeserializeModel<LayerStructure>(reader, device);
        //     MLP1 = DeserializeModel<LayerStructure>(reader, device);
        //     MLP2 = DeserializeModel<LayerStructure>(reader, device);

        //     Norm1 = DeserializeModel<LayerStructure>(reader, device);
        //     Norm2 = DeserializeModel<LayerStructure>(reader, device);
        // }
    }
}
