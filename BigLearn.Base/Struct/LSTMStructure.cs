﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    
    public class LSTMStructure : Structure 
    {
        ///Multi Layer LSTM Model.
        public List<LSTMCell> LSTMCells = new List<LSTMCell>();

        public override List<Structure> SubStructure { get { return LSTMCells.Select(i => (Structure)i).ToList(); } }

        public int Layer { get { return LSTMCells.Count; } }
        public LSTMStructure(int inputFea, int[] hiddenStates) : this(inputFea, hiddenStates, DeviceType.GPU)
        { }

        public LSTMStructure(int inputFea, int[] hiddenStates, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm)
        {
            int input = inputFea;
            for (int i = 0; i < hiddenStates.Length; i++)
            {
                LSTMCell cell = new LSTMCell(input, hiddenStates[i], device, rndInit);
                LSTMCells.Add(cell);
                input = hiddenStates[i];
            }
        }

        public LSTMStructure(int inputFea, int[] hiddenStates, int[] attentDim, DeviceType device, RndRecurrentInit rndInit = RndRecurrentInit.RndNorm)
        {
            int input = inputFea;
            for (int i = 0; i < hiddenStates.Length; i++)
            {
                LSTMCell cell = new LSTMCell(input, hiddenStates[i], attentDim[i], device, rndInit);
                LSTMCells.Add(cell);
                input = hiddenStates[i];
            }
        }

        //public LSTMStructure(BinaryReader reader, DeviceType device) : base(reader, device) { }

        public LSTMStructure(List<LSTMCell> cells) { LSTMCells = cells; }

        public override DataSourceID Type { get { return DataSourceID.LSTMStructure; } }

        public string Descr()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("LSTM Layer Number {0}\n", LSTMCells.Count));
            sb.Append(string.Join("\n", LSTMCells.Select(i => string.Format("Input Dim {0}, Output Dim {1} ", i.InputFeatureDim, i.CellDim))));
            return sb.ToString();
        }
    }
}
