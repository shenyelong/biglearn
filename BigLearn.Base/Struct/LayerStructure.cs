﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public enum SimilarityType { CosineSimilarity = 0, InnerProduct = 1 }
    
    public enum A_Func { Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3, Gelu = 4, Log = 5, Exp = 6, Reciprocal = 7 };

    public enum N_Type { Fully_Connected = 0, Convolution_layer = 1 };
    public enum P_Pooling { MAX_Pooling = 0 };

    public enum Recurrent_Unit { GRU = 0, LSTM = 1};
    
    /// <summary>
    /// NN Layer Parameters
    /// </summary>
    public sealed class LayerStructure : Structure
    {
        public CudaPieceFloat weight { get { return Parameter["weight"].Output; } }
        public CudaPieceFloat bias { get { return Parameter["bias"].Output; } }

        public CudaPieceFloat weightGrad { get { return Parameter["weight"].Deriv; } } //{ get { return weight.Grad; } set { weight.Grad = value; } }
        public CudaPieceFloat biasGrad { get { return Parameter["bias"].Deriv; } } //{ get { return bias.Grad; } set { bias.Grad = value; } }
        
        public CudaPieceFloat WeightGrad { get { return weightGrad; } }
        public CudaPieceFloat BiasGrad { get { return biasGrad; } }

        public NdArrayData NDWeight { get { return Parameter["weight"]; } }
        
        public NdArrayData NDBias { get { return Parameter["bias"]; } }

        public IntPtr Weight { get { return weight.CudaPtr; } }
        public IntPtr Bias { get { return bias.CudaPtr; } }

        public int Neural_In;
        public int Neural_Out;

        public A_Func Af;
        
        public N_Type Nt = N_Type.Fully_Connected;
        
        public int N_Winsize = 1;
        
        public P_Pooling pool_type = P_Pooling.MAX_Pooling;

        public bool IsBias = false;
        
        public float DropOut = 0;

        //public GradientOptimizer WeightOptimizer { get { return StructureOptimizer["weight"]; } set { StructureOptimizer.Add("weight", value); } }
        //public GradientOptimizer BiasOptimizer { get { return StructureOptimizer["bias"]; } set { StructureOptimizer.Add("bias", value); } } //["Bias"].Optimizer = value; } }
        
        //public override Dictionary<string, CudaPieceFloat> Parameter { get {
        //        return new Dictionary<string, CudaPieceFloat>() {
        //            {"Weight", weight },
        //            {"Bias", bias } 
        //            }; } }  

        public override DataSourceID Type { get { return DataSourceID.LayerStructure; } }

        
        public LayerStructure(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropout, bool isbias) : this(layer_in, layer_out, af, nt, win_size, dropout, isbias, DeviceType.GPU)
        { }

        public LayerStructure(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropout, bool isbias, DeviceType device) 
            : this(layer_in, layer_out, af, nt, win_size, dropout, isbias, 
                    new CudaPieceFloat(layer_in * layer_out * win_size, device), null,
                    new CudaPieceFloat(layer_out, device), null, device) 
        {
            Init(DNNInitMethod.Xavior_Init);
        } 

        public LayerStructure(int layer_in, int layer_out, A_Func af, bool isbias, DeviceType device) : this(layer_in, layer_out, af, isbias, DNNInitMethod.Xavior_Init, device)
        { }
        
        public LayerStructure(int layer_in, int layer_out, A_Func af, bool isbias, DNNInitMethod initMethod, DeviceType device) 
            : this(layer_in, layer_out, af, N_Type.Fully_Connected, 1, 0, isbias, 
                    new CudaPieceFloat(layer_in * layer_out, device), null,
                    new CudaPieceFloat(layer_out, device), null, device) 
        {
            Init(initMethod);
        }

        
        public LayerStructure(int layer_in, int layer_out, A_Func af, bool isbias, CudaPieceFloat pweight, CudaPieceFloat pweightGrad, CudaPieceFloat pbias, CudaPieceFloat pbiasGrad, DeviceType device)
            : this(layer_in, layer_out, af, N_Type.Fully_Connected, 1, 0, isbias, pweight, pweightGrad, pbias, pbiasGrad, device)
        { }

        public LayerStructure(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropout, bool isbias, CudaPieceFloat pweight, CudaPieceFloat pweightGrad, CudaPieceFloat pbias, CudaPieceFloat pbiasGrad, DeviceType device)
            : base(device)
        {
            Neural_In = layer_in;
            Neural_Out = layer_out;
            Af = af;
            Nt = nt;
            N_Winsize = win_size;

            DropOut = dropout;

            IsBias = isbias;

            IntArgument in_dim = new IntArgument("in_dim", layer_in * win_size); 
            IntArgument out_dim = new IntArgument("out_dim", layer_out);

            AddParameter("weight", new NdArrayData(device, pweight, pweightGrad, out_dim, in_dim));
            AddParameter("bias", new NdArrayData(device, pbias, pbiasGrad, out_dim));
            
            //weight = pweight;
            //bias = pbias;

            //WeightGrad = pweightGrad;
            //BiasGrad = pbiasGrad;
        }
        

        public void Init(DNNInitMethod initMethod)
        {
            int inputsize = Neural_In * N_Winsize;
            int outputsize = Neural_Out;

            bias.Init(0);

            {
		      float base_range = 0;

                if(initMethod == DNNInitMethod.Xavior_Init)
                {
                    base_range = (float)Math.Sqrt(6.0f / (inputsize + outputsize));
                }
                else if(initMethod == DNNInitMethod.Kaiming_Init)
                {
                    base_range = (float)Math.Sqrt(12.0f / (inputsize + outputsize));
                }

                Logger.WriteLog("LayerStructure is initalized with {0} in range {1}", initMethod, base_range);

                weight.Init(base_range * 2, -base_range);
            }
        }

        public void Init(float wei)
        {
            weight.Init(wei);
            bias.Init(0);
        }

        public void Init(float wei_scale, float wei_bias)
        {
            weight.Init(wei_scale, wei_bias);
            bias.Init(0);
        }

    }
}
