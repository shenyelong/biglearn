
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
	public class MLPAttentionStructure : Structure
    {
        public int InputDim;
        public int MemoryDim;
        public int HiddenDim;

        /// <summary>
        /// Input to Hidden State Matrix.
        /// </summary>
        public CudaPieceFloat Wi { get { return Parameter["wi"].Output; } }
        public CudaPieceFloat Wm { get { return Parameter["wm"].Output; } }
        public CudaPieceFloat B { get { return Parameter["b"].Output; } }
        public CudaPieceFloat Wa { get { return Parameter["wa"].Output; } }

        public CudaPieceFloat WiGrad { get { return Parameter["wi"].Deriv; } } //{ get { return Wi.Grad; } }
        public CudaPieceFloat WmGrad { get { return Parameter["wm"].Deriv; } } //{ get { return Wm.Grad; } }
        public CudaPieceFloat BGrad { get { return Parameter["b"].Deriv; } } //BGrad { get { return B.Grad; } }
        public CudaPieceFloat WaGrad { get { return Parameter["wa"].Deriv; } } //{ get { return Wa.Grad; } }

        public bool IsBias = false;

        public override DataSourceID Type { get { return DataSourceID.MLPAttentionStructure; } }

        public MLPAttentionStructure(int inputDim, int memoryDim, int hiddenDim, DeviceType device) : base(device)
        {
            InputDim = inputDim;
            MemoryDim = memoryDim;
            HiddenDim = hiddenDim;

            //DeviceType = device;
            IntArgument in_dim = new IntArgument("in_dim", inputDim);
            IntArgument mem_dim = new IntArgument("mem_dim", memoryDim);
            IntArgument hid_dim = new IntArgument("hid_dim", hiddenDim);

            AddParameter("wi", new NdArrayData(device, false, hid_dim, in_dim));
            AddParameter("wm", new NdArrayData(device, false, hid_dim, mem_dim));
            AddParameter("b", new NdArrayData(device, false, hid_dim));
            AddParameter("wa", new NdArrayData(device, false, hid_dim));

            Init();
        }

        //public MLPAttentionStructure(BinaryReader reader, DeviceType device) : base(reader, device) { }

        public void Init()
        {
            Wi.Init((float)(Math.Sqrt(6.0 / (InputDim + HiddenDim)) * 2), (float)(-Math.Sqrt(6.0 / (InputDim + HiddenDim))));
            Wm.Init((float)(Math.Sqrt(6.0 / (MemoryDim + HiddenDim)) * 2), (float)(-Math.Sqrt(6.0 / (MemoryDim + HiddenDim))));
            Wa.Init((float)(Math.Sqrt(6.0 / (HiddenDim + 1)) * 2), (float)(-Math.Sqrt(6.0 / (HiddenDim + 1))));
            B.Init(0);
        }

    }
}