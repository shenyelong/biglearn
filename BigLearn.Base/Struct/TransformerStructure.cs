using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class TransformerStructure : CompositeNNStructure 
    {
        public int Dim { get; set; } 

        DeviceType Device { get; set; }
        public LayerStructure QKVProject { get; set; }

        //public LayerStructure QProject { get; set; }

        public LayerStructure Norm1 { get; set; }
        public LayerStructure Norm2 { get; set; }

        public VectorData Norm1Scale { get { return new VectorData(Dim, Norm1.weight, Norm1.WeightGrad, Device); } }
        public VectorData Norm1Bias { get { return new VectorData(Dim, Norm1.bias, Norm1.BiasGrad, Device); } }
        
        public VectorData Norm2Scale { get { return new VectorData(Dim, Norm2.weight, Norm2.WeightGrad, Device); } }
        public VectorData Norm2Bias { get { return new VectorData(Dim, Norm2.bias, Norm1.BiasGrad, Device); } }
        
        public LayerStructure AttProject { get; set; }

        public LayerStructure  MLP1 { get; set; }
        public LayerStructure  MLP2 { get; set; }

        // basicly, no useful at this moment.
        public override DataSourceID Type  {  get  { return DataSourceID.TransformerStructure; } }

        // public override void SyncFromCPU()
        // {
        //     //QProject.SyncFromCPU();
        //     QKVProject.SyncFromCPU();
        //     AttProject.SyncFromCPU();

        //     MLP1.SyncFromCPU();
        //     MLP2.SyncFromCPU();
        //     Norm1.SyncFromCPU();
        //     Norm2.SyncFromCPU();
        // }
        
        //(int layer_in, int layer_out, A_Func af, N_Type nt, int win_size, float dropOut, bool isbias, DeviceType device)       
        public TransformerStructure(int dim, DeviceType device)
        {
            Dim = dim;
            Device = device;

            //QProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            
            QKVProject = AddLayer(new LayerStructure(dim, dim * 3, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            QKVProject.weight.Init(2 * 0.034f, -0.034f);

            AttProject = AddLayer(new LayerStructure(dim, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            AttProject.weight.Init(2 * 0.034f, -0.034f);

            MLP1 = AddLayer(new LayerStructure(dim, dim * 4, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            MLP1.weight.Init(2 * 0.034f, -0.034f);

            MLP2 = AddLayer(new LayerStructure(dim * 4, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            MLP2.weight.Init(2 * 0.034f, -0.034f);

            Norm1 = AddLayer(new LayerStructure(1, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            Norm1.weight.Init(1);
            Norm1.bias.Init(0);

            Norm2 = AddLayer(new LayerStructure(1, dim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            Norm2.weight.Init(1);
            Norm2.bias.Init(0);

            //Norm1Scale = new VectorData(dim, Norm1.weight,  )
        }

        // public TransformerStructure(BinaryReader reader, DeviceType device)
        // {
        //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
        //     QKVProject = DeserializeModel<LayerStructure>(reader, device); 
        //     AttProject = DeserializeModel<LayerStructure>(reader, device);
        //     MLP1 = DeserializeModel<LayerStructure>(reader, device);
        //     MLP2 = DeserializeModel<LayerStructure>(reader, device);

        //     Norm1 = DeserializeModel<LayerStructure>(reader, device);
        //     Norm2 = DeserializeModel<LayerStructure>(reader, device);
        // }
    }
}
