﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn
{
    public class MemoryStructure : Structure
    {
        public CudaPieceFloat Address { get { return Parameter["address"].Output; } }
        public CudaPieceFloat Content { get { return Parameter["content"].Output; } }

        public CudaPieceFloat AddressGrad { get { return Parameter["address"].Deriv; } } //{ get { return Address.Grad; } }
        public CudaPieceFloat ContentGrad { get { return Parameter["content"].Deriv; } } //{ get { return Content.Grad; } }

        public int InputDim { get; set; }
        public int OutputDim { get; set; }
        public int MemoryLen { get; set; }

        //public GradientOptimizer ContentOptimizer { get { return StructureOptimizer["Content"].Optimizer; } }
        //public GradientOptimizer AddressOptimizer { get { return StructureOptimizer["Address"].Optimizer; } }

        // public override Dictionary<string, CudaPieceFloat> Parameter
        // {
        //     get
        //     {
        //         return new Dictionary<string, CudaPieceFloat>()
        //         {
        //             { "Address", Address },
        //             { "Content", Content },
        //         };
        //     }
        // }

        public MemoryStructure(int memoryLength, int inputDim, int outputDim, DeviceType device) : base(device)
        {
            InputDim = inputDim;
            OutputDim = outputDim;
            MemoryLen = memoryLength;

            IntArgument in_dim = new IntArgument("in_dim", inputDim); 
            IntArgument mem_len = new IntArgument("mem_size", memoryLength);
            IntArgument out_dim = new IntArgument("out_dim", outputDim);

            AddParameter("address", new NdArrayData(device, false, in_dim, mem_len));
            AddParameter("content", new NdArrayData(device, false, out_dim, mem_len));

            //Address = new CudaPieceFloat(MemoryLen * InputDim, true, device == DeviceType.GPU);
            //Content = new CudaPieceFloat(MemoryLen * OutputDim, true, device == DeviceType.GPU);

            Address.Init((float)(12.0f / Math.Sqrt(InputDim + MemoryLen)), (float)(-6.0f / Math.Sqrt(InputDim + MemoryLen)));
            Content.Init((float)(12.0f / Math.Sqrt(OutputDim + MemoryLen)), (float)(-6.0f / Math.Sqrt(OutputDim + MemoryLen)));
        }

        //public MemoryStructure(BinaryReader reader, DeviceType device) : base(reader, device)
        //{ }

        //protected override void InitStructureOptimizer()
        //{
        //    AddressGrad = new CudaPieceFloat(Address.Size, DeviceType);
        //    ContentGrad = new CudaPieceFloat(Content.Size, DeviceType);
        //    StructureOptimizer.Add("Address", new ModelOptimizer() { Parameter = Address, Gradient = AddressGrad });
        //    StructureOptimizer.Add("Content", new ModelOptimizer() { Parameter = Content, Gradient = ContentGrad });
        //}

        public override DataSourceID Type { get { return DataSourceID.MemoryStructure; } }

        // public override void Serialize(BinaryWriter writer)
        // {
        //     writer.Write(MemoryLen);
        //     writer.Write(InputDim);
        //     writer.Write(OutputDim);
        //     Address.Serialize(writer);
        //     Content.Serialize(writer);
        // }

        // public override void Deserialize(BinaryReader reader, DeviceType device)
        // {
        //     MemoryLen = reader.ReadInt32();
        //     InputDim = reader.ReadInt32();
        //     OutputDim = reader.ReadInt32();
        //     Address = new CudaPieceFloat(reader, device);
        //     Content = new CudaPieceFloat(reader, device);
        // }
    }
}
