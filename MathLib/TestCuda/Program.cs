﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using BigLearn;
using System.Diagnostics;

namespace TestCuda
{
    class Program
    {
        static IMathOperationManager lib;
        static void InitLib()
        {
            DeviceType device = MathOperatorManager.SetDevice(0);
            lib = MathOperatorManager.CreateInstance(device);
        }

        static void TestRecursiveForwardPropagateBatch( )
        {
            int batchSize = 2;
            CudaPieceInt InstanceIndex = new CudaPieceInt(batchSize, true, true);
            InstanceIndex.MemPtr[0] = 2;
            InstanceIndex.MemPtr[1] = 5;

            InstanceIndex.SyncFromCPU();

            int dim = 3;
            CudaPieceFloat SeqInputMatrixs = new CudaPieceFloat(5 * 3, true, true);
            SeqInputMatrixs.MemPtr[0] = 0.1f;
            SeqInputMatrixs.MemPtr[1] = 0.2f;
            SeqInputMatrixs.MemPtr[2] = 0.1f;

            SeqInputMatrixs.MemPtr[3] = 0.2f;
            SeqInputMatrixs.MemPtr[4] = 0.4f;
            SeqInputMatrixs.MemPtr[5] = 0.2f;

            SeqInputMatrixs.MemPtr[6] = 0.3f;
            SeqInputMatrixs.MemPtr[7] = 0.6f;
            SeqInputMatrixs.MemPtr[8] = 0.3f;

            SeqInputMatrixs.MemPtr[9] = 0.4f;
            SeqInputMatrixs.MemPtr[10] = 0.8f;
            SeqInputMatrixs.MemPtr[11] = 0.4f;

            SeqInputMatrixs.MemPtr[12] = 0.1f;
            SeqInputMatrixs.MemPtr[13] = 0.2f;
            SeqInputMatrixs.MemPtr[14] = 0.1f;
            SeqInputMatrixs.SyncFromCPU();

            int lag = 2;
            int af = 1;
            CudaPieceFloat WMatrix = new CudaPieceFloat(2 * 3 * 3, true, true);
            WMatrix.Zero();
            WMatrix.MemPtr[0] = 1;
            WMatrix.MemPtr[4] = 1;
            WMatrix.MemPtr[8] = 1;

            WMatrix.MemPtr[9] = 1;
            WMatrix.MemPtr[13] = 1;
            WMatrix.MemPtr[17] = 1;
            WMatrix.SyncFromCPU();

            //Cudalib.Matrix_Add_Vector(SeqInputMatrixs.CudaPtr, WMatrix.CudaPtr, 2, 6);

            //SeqInputMatrixs.SyncToCPU();

            Cudalib.RecursiveForwardPropagateBatch(InstanceIndex.CudaPtr, batchSize, SeqInputMatrixs.CudaPtr,
                        dim, WMatrix.CudaPtr, lag, af, SeqInputMatrixs.CudaPtr);


            SeqInputMatrixs.SyncToCPU();
            for(int i=0;i<15;i++)
            {
                Console.WriteLine(SeqInputMatrixs.MemPtr[i]);
            }
            InstanceIndex.Dispose();
            SeqInputMatrixs.Dispose();
            WMatrix.Dispose();
        }

        static void TestMax()
        {
            int num = 10000;
            CudaPieceInt v = new CudaPieceInt(num, true, true);
            for (int m = 0; m < num; m++ )
            {
                v.MemPtr[m] = (m - 50) * (m - 50);
            }
            v.SyncFromCPU();
            Console.WriteLine("max number {0}", Cudalib.Max(v.CudaPtr, num));
            v.Dispose();
        }

        static void TestColumnWiseSum()
        {
            int row = 1000;
            int col = 30000;
            CudaPieceFloat matrix = new CudaPieceFloat(row * col, true, true);
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    matrix.MemPtr[i * col + j] = 1; // i * 0.00001f + j * 0.0001f;
                }
            }
            matrix.SyncFromCPU();

            CudaPieceFloat vec = new CudaPieceFloat(col, true, true);
            for (int j = 0; j < col; j++) vec.MemPtr[j] = 10;
            vec.SyncFromCPU();

            Stopwatch stopwatch = new Stopwatch();
            // Begin timing.
            stopwatch.Start();
            for (int i = 0; i < 10;i++ )
                Cudalib.ColumnWiseSum(matrix.CudaPtr, vec.CudaPtr, row, col, 0.2f);
            Cudalib.CudaDeviceSynchronize();
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("V1 ColumnWise Sum Time elapsed: {0}", stopwatch.Elapsed);

            CudaPieceFloat vec2 = new CudaPieceFloat(col, true, true);
            for (int j = 0; j < col; j++) vec2.MemPtr[j] = 10;
            vec2.SyncFromCPU();

            Stopwatch stopwatch2 = new Stopwatch();
            // Begin timing.
            stopwatch2.Start();
            for (int i = 0; i < 10; i++)
                Cudalib.Matrix_Aggragate_Weight(matrix.CudaPtr, vec2.CudaPtr, row, col, 0.2f);
            Cudalib.CudaDeviceSynchronize();
            stopwatch2.Stop();
            // Write result.
            Console.WriteLine("V2 ColumnWise Sum Time elapsed: {0}", stopwatch2.Elapsed);


            CudaPieceFloat vec3 = new CudaPieceFloat(col, true, true);
            for (int j = 0; j < col; j++) vec3.MemPtr[j] = 10;
            vec3.SyncFromCPU();

            CudaPieceFloat biasOne = new CudaPieceFloat(row, true, true);
            biasOne.Init(1);
            IntPtr cublasP = Cudalib.CudaCreateCuBlas();

            Stopwatch stopwatch3 = new Stopwatch();
            // Begin timing.
            stopwatch3.Start();
            for (int i = 0; i < 10; i++)
                Cudalib.CuBLAS_Sgemv(cublasP, matrix.CudaPtr, biasOne.CudaPtr, row, col, vec3.CudaPtr, true, 1 , 0.2f);
            Cudalib.CudaDeviceSynchronize();
            stopwatch3.Stop();
            // Write result.
            Console.WriteLine("V3 ColumnWise Sum Time elapsed: {0}", stopwatch3.Elapsed);


            CudaPieceFloat proj = new CudaPieceFloat(col, true, true);
            proj.Init(1);
            Stopwatch stopwatch4 = new Stopwatch();
            // Begin timing.
            stopwatch4.Start();
            for (int i = 0; i < 10; i++)
                Cudalib.CuBLAS_Sgemv(cublasP, matrix.CudaPtr, proj.CudaPtr, row, col, vec3.CudaPtr, false, 0, 0.2f);
            Cudalib.CudaDeviceSynchronize();
            stopwatch4.Stop();
            // Write result.
            Console.WriteLine("V3 ColumnWise Sum Time elapsed: {0}", stopwatch4.Elapsed);


            vec.SyncToCPU();
            vec3.SyncToCPU();


            for (int i = 0; i < col; i++)
            {
                if (Math.Abs(vec.MemPtr[i] - vec3.MemPtr[i]) > 0.1)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, vec.MemPtr[i], vec3.MemPtr[i]);
                }
            }

            vec2.Dispose();
            vec.Dispose();
            matrix.Dispose();

        }

        static void TestDerivSparseSoftmax()
        {
            int smpNum = 123;
            int colNum = 5;
            CudaPieceInt index = new CudaPieceInt(smpNum, true, true);
            for (int i = 0; i < smpNum; i++)
                index.MemPtr[i] = (i + 1) * colNum;
            index.SyncFromCPU();

            CudaPieceFloat word = new CudaPieceFloat(smpNum * colNum, true, true);
            for(int i=0;i<smpNum * colNum;i++)
                word.MemPtr[i] = 0.2f * i ;

            for (int i = 0; i < colNum; i++)
                word.MemPtr[i] = 9;
            word.SyncFromCPU();

            CudaPieceInt label = new CudaPieceInt(smpNum, true, true);
            for (int i = 0; i < smpNum; i++ )
                label.MemPtr[i] = i % colNum;
            label.SyncFromCPU();

            CudaPieceFloat prob = new CudaPieceFloat(smpNum, true, true);
            for (int i = 0; i < smpNum; i++)
                prob.MemPtr[i] = 1;
            prob.SyncFromCPU();

            Cudalib.DerivSparseSoftmax(index.CudaPtr, word.CudaPtr, word.CudaPtr, label.CudaPtr, 1.0f, smpNum, prob.CudaPtr);

            prob.SyncToCPU();
            for (int i = 0; i < smpNum; i++)
            {
                Console.WriteLine(prob.MemPtr[i]);
            }
            index.Dispose();
            word.Dispose();
            label.Dispose();
            prob.Dispose();
        }

        static void TestImageCNNForward()
        {
            int inputWidth = 84;
            int inputHeight = 84;
            int inputDepth = 4;
            int batch = 32;

            int filterSX = 8;
            int pad = 0;
            int stride = 4;
            int outputDepth = 16;
            int outputHeight = (inputHeight - filterSX) / stride + 1;
            int outputWidth = (inputWidth - filterSX) / stride + 1;

            CudaPieceFloat inImageData = new CudaPieceFloat(inputWidth * inputHeight * inputDepth * batch, true, true);
            inImageData.Init(1, 0);

            CudaPieceFloat filterData = new CudaPieceFloat(filterSX * filterSX * inputDepth * outputDepth, true, true);
            filterData.Init(1, 0);

            CudaPieceFloat outImageData1 = new CudaPieceFloat(outputWidth * outputHeight * outputDepth * batch, true, true);
            outImageData1.Init(0);

            Stopwatch stopwatch = new Stopwatch();

            // Begin timing.
            stopwatch.Start();
 
            for (int i = 0; i < 10000; i++)
            {
                Cudalib.ImageConvolution(inImageData.CudaPtr, batch, inputWidth, inputHeight, inputDepth, filterData.CudaPtr, filterSX, inputDepth,
                    outputDepth, pad, stride, outImageData1.CudaPtr, outputWidth, outputHeight);
            }
            outImageData1.SyncToCPU();
            stopwatch.Stop();

            // Write result.
            Console.WriteLine("V1 CNN Time elapsed: {0}", stopwatch.Elapsed);


            Stopwatch stopwatch2 = new Stopwatch();

            // Begin timing.
            stopwatch2.Start();
 
            CudaPieceFloat outImageData2 = new CudaPieceFloat(outputWidth * outputHeight * outputDepth * batch, true, true);
            outImageData2.Init(0);
            IntPtr cudnnHandle = Cudalib.CudaCreateCuDnn();
            for (int i = 0; i < 10000;i++ )
            { 
                Cudalib.CuDNNForwardPropagate(cudnnHandle, inImageData.CudaPtr, batch, inputDepth, inputHeight, inputWidth, filterData.CudaPtr,
                        outputDepth, filterSX, filterSX, pad, stride, outImageData2.CudaPtr, outputHeight, outputWidth);
            } 
            outImageData2.SyncToCPU();

            Cudalib.CudaDestroyCuDnn(cudnnHandle);
            stopwatch2.Stop();

            // Write result.
            Console.WriteLine("V2 CNN Time elapsed: {0}", stopwatch2.Elapsed);

            for (int i = 0; i < outImageData1.Size; i++)
            {
                if (Math.Abs(outImageData1.MemPtr[i] - outImageData2.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, outImageData1.MemPtr[i], outImageData2.MemPtr[i]);
                }
            }

            inImageData.Dispose();
            outImageData1.Dispose();
            outImageData2.Dispose();
            filterData.Dispose();
        }

        static void TestImageCNNBackwardData()
        {
            int inputWidth = 84;
            int inputHeight = 84;
            int inputDepth = 4;
            int batch = 32;

            int filterSX = 8;
            int pad = 0;
            int stride = 4;
            int outputDepth = 16;
            int outputHeight = (inputHeight - filterSX) / stride + 1;
            int outputWidth = (inputWidth - filterSX) / stride + 1;


            DeviceType device = MathOperatorManager.SetDevice(0);
            IMathOperationManager lib = MathOperatorManager.CreateInstance(device);

            CudaPieceFloat filterData = new CudaPieceFloat(filterSX * filterSX * inputDepth * outputDepth, true, true);
            filterData.Init(1, 0);

            CudaPieceFloat outImageData = new CudaPieceFloat(outputWidth * outputHeight * outputDepth * batch, true, true);
            outImageData.Init(1, -0.5f);

            CudaPieceFloat inImageData1 = new CudaPieceFloat(inputWidth * inputHeight * inputDepth * batch, true, true);
            inImageData1.Init(0);

            CudaPieceFloat inImageData2 = new CudaPieceFloat(inputWidth * inputHeight * inputDepth * batch, true, true);
            inImageData2.Init(0);


            Stopwatch stopwatch = new Stopwatch();


            // Begin timing.
            stopwatch.Start();

            for (int i = 0; i < 10000; i++)
            {
                Cudalib.ImageDenseConvMatrixMultipy(outImageData.CudaPtr,
                                                batch, outputWidth, outputHeight, outputDepth,
                                                filterData.CudaPtr, filterSX, inputDepth, pad,
                                                stride, inImageData1.CudaPtr, inputWidth, inputHeight, inputDepth);
            }
            inImageData1.SyncToCPU();
            stopwatch.Stop();
            Cudalib.Scale_Vector(inImageData1.CudaPtr, inImageData1.Size, 1.0f / 10000);
            inImageData1.SyncToCPU();
            
            // Write result.
            Console.WriteLine("V1 CNN Time elapsed: {0}", stopwatch.Elapsed);


            Stopwatch stopwatch2 = new Stopwatch();

            // Begin timing.
            stopwatch2.Start();
            for (int i = 0; i < 10000; i++)
            {
                lib.CuDNNBackwardPropagateData(inImageData2, batch, inputDepth, inputHeight, inputWidth ,
                   filterData, outputDepth, filterSX, filterSX, pad, stride,
                   outImageData, outputHeight, outputWidth);
            }
            inImageData2.SyncToCPU();

            stopwatch2.Stop();

            // Write result.
            Console.WriteLine("V2 CNN Time elapsed: {0}", stopwatch2.Elapsed);

            for (int i = 0; i < inImageData2.Size; i++)
            {
                if (Math.Abs(inImageData2.MemPtr[i] - inImageData1.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, inImageData1.MemPtr[i], inImageData2.MemPtr[i]);
                }
            }

            inImageData2.Dispose();
            inImageData1.Dispose();
            outImageData.Dispose();
            filterData.Dispose();
        }

        static void TestImageCNNBackwardFilter()
        {
            int inputWidth = 84;
            int inputHeight = 84;
            int inputDepth = 4;
            int batch = 32;

            int filterSX = 8;
            int pad = 0;
            int stride = 4;
            int outputDepth = 16;
            int outputHeight = (inputHeight - filterSX) / stride + 1;
            int outputWidth = (inputWidth - filterSX) / stride + 1;


            CudaPieceFloat filterData1 = new CudaPieceFloat(filterSX * filterSX * inputDepth * outputDepth, true, true);
            filterData1.Init(0);
            
            CudaPieceFloat filterData2 = new CudaPieceFloat(filterSX * filterSX * inputDepth * outputDepth, true, true);
            filterData2.Init(0);

            CudaPieceFloat outImageData = new CudaPieceFloat(outputWidth * outputHeight * outputDepth * batch, true, true);
            outImageData.Init(1, -0.5f);

            CudaPieceFloat inImageData = new CudaPieceFloat(inputWidth * inputHeight * inputDepth * batch, true, true);
            inImageData.Init(1, 0);


            Stopwatch stopwatch = new Stopwatch();

            // Begin timing.
            stopwatch.Start();

            for (int i = 0; i < 10000; i++)
            {
                Cudalib.Convolution_Dense_Image_Matrix_Product_INTEX_Weight(outImageData.CudaPtr,
                                           batch, outputWidth, outputHeight, outputDepth,
                                           filterData1.CudaPtr, filterSX, inputDepth, pad,
                                           stride, inImageData.CudaPtr, inputWidth, inputHeight, inputDepth, 1.0f); 
            }
            filterData1.SyncToCPU();
            stopwatch.Stop();
            Cudalib.Scale_Vector(filterData1.CudaPtr, filterData1.Size, 1.0f / 10000);
            filterData1.SyncToCPU();

            // Write result.
            Console.WriteLine("V1 CNN Time elapsed: {0}", stopwatch.Elapsed);


            Stopwatch stopwatch2 = new Stopwatch();
            // Begin timing.
            stopwatch2.Start();
            
            //IntPtr handle = Cudalib.CuDNNCreateCuDnnHandle();
            //IntPtr input = Cudalib.CuDNNCreateTensorDescriptor(batch, inputDepth, inputHeight, inputWidth);
            //IntPtr output = Cudalib.CuDNNCreateTensorDescriptor(batch, outputDepth, outputHeight, outputWidth);
            //IntPtr filter = Cudalib.CuDNNCreateFilterDescriptor(outputDepth, inputDepth, filterSX, filterSX);
            //IntPtr conv = Cudalib.CuDNNCreateConvDescriptor(pad, stride);

            //for (int i = 0; i < 10000; i++)
            //{
            //    Cudalib.CuDNNBackwardPropagateFilterV2(handle, inImageData.CudaPtr, input,
            //        filterData2.CudaPtr, filter, conv,
            //        outImageData.CudaPtr, output);
            //}
            for (int i = 0; i < 10000; i++)
            {
                lib.CuDNNBackwardPropagateFilter(inImageData, batch, inputDepth, inputHeight, inputWidth,
                    filterData2, outputDepth, filterSX, filterSX, pad, stride,
                    outImageData, outputHeight, outputWidth, 1, 0);
            }  
            filterData2.SyncToCPU();

            stopwatch2.Stop();

            // Write result.
            Console.WriteLine("V2 CNN Time elapsed: {0}", stopwatch2.Elapsed);

            for (int i = 0; i < filterData2.Size; i++)
            {
                if (Math.Abs(filterData2.MemPtr[i] - filterData1.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, filterData1.MemPtr[i], filterData2.MemPtr[i]);
                }
            }
            inImageData.Dispose();
            outImageData.Dispose();
            filterData1.Dispose();
            filterData2.Dispose();
        }


        static void TestImageCNNBackwardBias()
        {
            int inputWidth = 84;
            int inputHeight = 84;
            int inputDepth = 4;
            int batch = 32;

            int filterSX = 8;
            int pad = 0;
            int stride = 4;
            int outputDepth = 16;
            int outputHeight = (inputHeight - filterSX) / stride + 1;
            int outputWidth = (inputWidth - filterSX) / stride + 1;


            CudaPieceFloat biasData1 = new CudaPieceFloat(1 * 1 * 1 * outputDepth, true, true);
            biasData1.Init(0);

            CudaPieceFloat biasData2 = new CudaPieceFloat(1 * 1 * 1 * outputDepth, true, true);
            biasData2.Init(0);

            CudaPieceFloat outImageData = new CudaPieceFloat(outputWidth * outputHeight * outputDepth * batch, true, true);
            outImageData.Init(0.00001f);

            Stopwatch stopwatch = new Stopwatch();

            // Begin timing.
            stopwatch.Start();

            for (int i = 0; i < 1000; i++)
            {
                //biasData1.Init(10);
                Cudalib.Matrix_Image_Aggragate_Weight(outImageData.CudaPtr, biasData1.CudaPtr, batch,
                        outputWidth, outputHeight, outputDepth, 1.0f);
                
            }
            biasData1.SyncToCPU();
            stopwatch.Stop();
            //Cudalib.Scale_Vector(biasData1.CudaPtr, biasData1.Size, 1.0f / 10000);
            //biasData1.SyncToCPU();

            // Write result.
            Console.WriteLine("V1 CNN Time elapsed: {0}", stopwatch.Elapsed);


            Stopwatch stopwatch2 = new Stopwatch();
            // Begin timing.
            stopwatch2.Start();

            //IntPtr handle = Cudalib.CuDNNCreateCuDnnHandle();
            //IntPtr input = Cudalib.CuDNNCreateTensorDescriptor(batch, inputDepth, inputHeight, inputWidth);
            //IntPtr output = Cudalib.CuDNNCreateTensorDescriptor(batch, outputDepth, outputHeight, outputWidth);
            //IntPtr filter = Cudalib.CuDNNCreateFilterDescriptor(outputDepth, inputDepth, filterSX, filterSX);
            //IntPtr conv = Cudalib.CuDNNCreateConvDescriptor(pad, stride);

            //for (int i = 0; i < 10000; i++)
            //{
            //    Cudalib.CuDNNBackwardPropagateFilterV2(handle, inImageData.CudaPtr, input,
            //        filterData2.CudaPtr, filter, conv,
            //        outImageData.CudaPtr, output);
            //}
            IntPtr hCudnn = Cudalib.CudaCreateCuDnn();
            for (int i = 0; i < 500; i++)
            {
                //biasData2.Init(10);
                Cudalib.CuDNNBackwardPropagateBias(hCudnn, biasData2.CudaPtr, outImageData.CudaPtr, batch, outputDepth, outputHeight, outputWidth, 1, 1);
                
                //CuDNNBackwardPropagateFilter(inImageData.CudaPtr, batch, inputDepth, inputHeight, inputWidth,
                //    filterData2.CudaPtr, outputDepth, filterSX, filterSX, pad, stride,
                //    outImageData.CudaPtr, outputHeight, outputWidth, 1, 0);
            }
            biasData2.SyncToCPU();

            Cudalib.CudaDestroyCuDnn(hCudnn);
            stopwatch2.Stop();

            // Write result.
            Console.WriteLine("V2 CNN Time elapsed: {0}", stopwatch2.Elapsed);

            for (int i = 0; i < biasData2.Size; i++)
            {
                if (Math.Abs(biasData2.MemPtr[i] - biasData1.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, biasData1.MemPtr[i], biasData2.MemPtr[i]);
                }
            }
            outImageData.Dispose();
            biasData1.Dispose();
            biasData2.Dispose();
        }


        static void TestFullyConnectForward()
        {
            int batch =  128;
            int input =  1024;
            int output = 1024;

            Cudalib.CudaSetDevice(0);
            CudaPieceFloat inputX = new CudaPieceFloat(batch * input, true, true);
            inputX.Init(1, 0);

            CudaPieceFloat weight = new CudaPieceFloat(input * output, true, true);
            weight.Init(1, -05f);


            CudaPieceFloat outputY = new CudaPieceFloat(batch * output, true, true);
            outputY.Init(0);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < 50; i++)
            {
                //.Cudalib.Matrix_Multipy(inputX.CudaPtr, weight.CudaPtr, outputY.CudaPtr, batch, input, output, 0);
                Cudalib.SgemmMask(inputX.CudaPtr, weight.CudaPtr, outputY.CudaPtr, batch, input, output, CudaPieceInt.Empty.CudaPtr, CudaPieceInt.Empty.CudaPtr, CudaPieceInt.Empty.CudaPtr, 0, 1, false, false);
            }
            outputY.SyncToCPU();
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("V1 Matrix Multiplication Time elapsed: {0}", stopwatch.Elapsed);

            IntPtr handle = Cudalib.CudaCreateCuBlas();

            CudaPieceFloat outputY2 = new CudaPieceFloat(batch * output, true, true);
            outputY2.Init(0);

            Stopwatch stopwatch2 = new Stopwatch();
            stopwatch2.Start();

            for (int i = 0; i < 50; i++)
            {
                Cudalib.CuBLAS_Sgemm(handle, inputX.CudaPtr, weight.CudaPtr, outputY2.CudaPtr, batch, input, output, 0, 1, false, false);
            }
            outputY2.SyncToCPU();
            
            //for (int i = 0; i < 1000; i++)
            //{
            //    Cudalib.CUBLAS_Init();
            //    Cudalib.CUBLAS_Destroy();
            //}
            
            stopwatch2.Stop();
            Cudalib.CudaDestroyCuBlas(handle);
            // Write result.
            Console.WriteLine("V2 Matrix Multiplication Time elapsed: {0}", stopwatch2.Elapsed);


            for (int i = 0; i < outputY2.Size; i++)
            {
                if (Math.Abs(outputY2.MemPtr[i] - outputY.MemPtr[i]) > 0.1)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, outputY.MemPtr[i], outputY2.MemPtr[i]);
                }
            }
            outputY2.Dispose();
            outputY.Dispose();
            inputX.Dispose();
            weight.Dispose();
        }

        static void TestFullyConnectBackwardData()
        {
            int batch = 1024;
            int input = 300;
            int output = 128;

            Cudalib.CudaSetDevice(3);
            CudaPieceFloat inputX = new CudaPieceFloat(batch * input, true, true);
            inputX.Init(0);

            CudaPieceFloat weight = new CudaPieceFloat(input * output, true, true);
            weight.Init(1, -05f);


            CudaPieceFloat outputY = new CudaPieceFloat(batch * output, true, true);
            outputY.Init(1, 0);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < 1000; i++)
            {
                //Cudalib.Matrix_Multipy_Weight(outputY.CudaPtr, weight.CudaPtr, inputX.CudaPtr, batch,
                //    output, input, 1, 0);
                Cudalib.SgemmMask(outputY.CudaPtr, weight.CudaPtr, inputX.CudaPtr, batch, output, input, CudaPieceInt.Empty.CudaPtr, CudaPieceInt.Empty.CudaPtr, CudaPieceInt.Empty.CudaPtr, 0, 1, false, true);
                //Cudalib.Matrix_Multipy(outputY.CudaPtr, weight.CudaPtr, inputX.CudaPtr, batch,
                //    output, input, 1);
            }
            inputX.SyncToCPU();
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("V1 Matrix Multiplication Time elapsed: {0}", stopwatch.Elapsed);

            IntPtr handle = Cudalib.CudaCreateCuBlas();
            CudaPieceFloat inputX2 = new CudaPieceFloat(batch * input, true, true);
            inputX2.Init(0);

            Stopwatch stopwatch2 = new Stopwatch();
            stopwatch2.Start();

            for (int i = 0; i < 1000; i++)
            {
                Cudalib.CuBLAS_Sgemm(handle, outputY.CudaPtr, weight.CudaPtr, inputX2.CudaPtr, batch, output, input, 0, 1, false, true);
            }
            inputX2.SyncToCPU();
            stopwatch2.Stop();
            Cudalib.CudaDestroyCuBlas(handle);
            // Write result.
            Console.WriteLine("V2 Matrix Multiplication Time elapsed: {0}", stopwatch2.Elapsed);


            for (int i = 0; i < inputX2.Size; i++)
            {
                if (Math.Abs(inputX2.MemPtr[i] - inputX.MemPtr[i]) > 0.1)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, inputX2.MemPtr[i], inputX.MemPtr[i]);
                }
            }
            inputX2.Dispose();
            outputY.Dispose();
            inputX.Dispose();
            weight.Dispose();
        }

        static void TestFullyConnectBackwardWeight()
        {
            int batch = 1024;
            int input = 300;
            int output = 128;

            Cudalib.CudaSetDevice(3);
            CudaPieceFloat inputX = new CudaPieceFloat(batch * input, true, true);
            inputX.Init(1, 0);

            CudaPieceFloat outputY = new CudaPieceFloat(batch * output, true, true);
            outputY.Init(1, 0);

            CudaPieceFloat weight = new CudaPieceFloat(input * output, true, true);
            weight.Init(0);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < 1000; i++)
            {
                Cudalib.SgemmMask(inputX.CudaPtr, outputY.CudaPtr, weight.CudaPtr, batch, input, output, CudaPieceInt.Empty.CudaPtr, CudaPieceInt.Empty.CudaPtr, CudaPieceInt.Empty.CudaPtr, 0, 1, true, false);
            }
            weight.SyncToCPU();
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("V1 Matrix Multiplication Time elapsed: {0}", stopwatch.Elapsed);


            CudaPieceFloat weight2 = new CudaPieceFloat(input * output, true, true);
            weight2.Init(0);

            IntPtr GHandle = Cudalib.CudaCreateCuBlas();

            Stopwatch stopwatch2 = new Stopwatch();
            stopwatch2.Start();

            for (int i = 0; i < 1000; i++)
            {
                Cudalib.CuBLAS_Sgemm(GHandle, inputX.CudaPtr, outputY.CudaPtr, weight2.CudaPtr, batch, input, output, 0, 1, true, false);
                //Cudalib.CuBLAS_Sgemm(GHandle, inputX.CudaPtr, outputY.CudaPtr, weight2.CudaPtr, batch, input, output, 1, 1, true, false);
            }
            weight2.SyncToCPU();
            stopwatch2.Stop();

            Cudalib.CudaDestroyCuBlas(GHandle);
            
            // Write result.
            Console.WriteLine("V2 Matrix Multiplication Time elapsed: {0}", stopwatch2.Elapsed);


            for (int i = 0; i < weight2.Size; i++)
            {
                if (Math.Abs(weight.MemPtr[i] - weight2.MemPtr[i]) > 0.1)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, weight.MemPtr[i], weight2.MemPtr[i]);
                }
            }
            weight2.Dispose();
            outputY.Dispose();
            inputX.Dispose();
            weight.Dispose();
        }
        static void TestOffSet()
        {
            CudaPieceFloat a = new CudaPieceFloat(100, true, true);

            CudaPieceFloat b = new CudaPieceFloat(100, true, true);

            for (int i = 0; i < 100; i++)
            {
                a.MemPtr[i] = (i + 1) * 0.01f;
            }
            a.SyncFromCPU();

            Cudalib.Tanh(IntPtr.Add(a.CudaPtr, 10 * sizeof(float)), IntPtr.Add(b.CudaPtr, 1 * sizeof(float)), 90);

            b.SyncToCPU();
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(b.MemPtr[i]);
            }
        }

        static void TestElementWiseMultiplication()
        {
            IntPtr GHandle = Cudalib.CudaCreateCuBlas();

            int batch = 640;
            int input = 300;

            Cudalib.CudaSetDevice(3);
            CudaPieceFloat inputX = new CudaPieceFloat(batch * input, true, true);
            inputX.Init(1, 0);


            CudaPieceFloat inputY = new CudaPieceFloat(batch * input, true, true);
            inputY.Init(1, 0);



            CudaPieceFloat output0 = new CudaPieceFloat(batch * input, true, true);
            output0.Init(0);

            CudaPieceInt leftMask = new CudaPieceInt(batch, true, true);
            for(int mm = 0;mm<batch;mm++)
            {
                leftMask.MemPtr[mm] = mm;
            }
            leftMask.SyncFromCPU();

            Stopwatch stopwatch0 = new Stopwatch();
            stopwatch0.Start();
            for (int i = 0; i < 1000; i++)
            {
                Cudalib.ElementwiseProductMask(inputX.CudaPtr, inputY.CudaPtr, output0.CudaPtr, leftMask.CudaPtr, 
                    CudaPieceInt.Empty.CudaPtr, CudaPieceInt.Empty.CudaPtr, batch, input, 0, 1);
            }
            output0.SyncToCPU();
            stopwatch0.Stop();
            Console.WriteLine("V0 ElementwiseProduct Time elapsed: {0}", stopwatch0.Elapsed);


            
            CudaPieceFloat output1 = new CudaPieceFloat(batch * input, true, true);
            output1.Init(0);

            Stopwatch stopwatch1 = new Stopwatch();
            stopwatch1.Start();
            for (int i = 0; i < 1000; i++)
            {
                Cudalib.ElementwiseProduct(inputX.CudaPtr, inputY.CudaPtr, output1.CudaPtr, batch, input, 0);
            }
            output1.SyncToCPU();
            stopwatch1.Stop();
            Console.WriteLine("V1 ElementwiseProduct Time elapsed: {0}", stopwatch1.Elapsed);


            CudaPieceFloat output2 = new CudaPieceFloat(batch * input, true, true);
            output2.Init(0);
            Stopwatch stopwatch2 = new Stopwatch();
            stopwatch2.Start();
            for (int i = 0; i < 1000; i++)
            {
                Cudalib.ElementwiseProduct(inputX.CudaPtr, inputY.CudaPtr, output2.CudaPtr, 1, batch * input, 0);
            }
            output2.SyncToCPU();
            stopwatch2.Stop();
            Console.WriteLine("V2 ElementwiseProduct Time elapsed: {0}", stopwatch2.Elapsed);


            CudaPieceFloat output3 = new CudaPieceFloat(batch * input, true, true);
            output3.Init(0);
            Stopwatch stopwatch3 = new Stopwatch();
            stopwatch3.Start();
            for (int i = 0; i < 1000; i++)
            {
                for (int b = 0; b < batch; b++)
                {
                    Cudalib.ElementwiseProduct(IntPtr.Add(inputX.CudaPtr, b * input * sizeof(float)),
                        IntPtr.Add(inputY.CudaPtr, b * input * sizeof(float)),
                        IntPtr.Add(output3.CudaPtr, b * input * sizeof(float)), input, 1, 0);
                }
            }
            output3.SyncToCPU();
            stopwatch3.Stop();
            Console.WriteLine("V3 ElementwiseProduct Time elapsed: {0}", stopwatch3.Elapsed);


            CudaPieceFloat output4 = new CudaPieceFloat(batch * input, true, true);
            output4.Init(0);
            Stopwatch stopwatch4 = new Stopwatch();
            stopwatch4.Start();
            for (int i = 0; i < 1000; i++)
            {
                Cudalib.CuBLAS_SgbmvDiagonal(GHandle, inputX.CudaPtr, inputY.CudaPtr, output4.CudaPtr, batch * input, 1, 0);
            }
            output4.SyncToCPU();
            stopwatch4.Stop();
            Console.WriteLine("V4 ElementwiseProduct Time elapsed: {0}", stopwatch4.Elapsed);

            for (int i = 0; i < output2.Size; i++)
            {
                if (Math.Abs(output2.MemPtr[i] - output0.MemPtr[i]) > 0.1)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, output0.MemPtr[i], output2.MemPtr[i]);
                }
            }
            output2.Dispose();
            output1.Dispose();
            inputX.Dispose();
            inputY.Dispose();
            Cudalib.CudaDestroyCuBlas(GHandle);
        }

        static void TestVectorScale()
        {
            IntPtr GHandle = Cudalib.CudaCreateCuBlas();

            int batch = 2000;
            Cudalib.CudaSetDevice(3);
            CudaPieceFloat inputX = new CudaPieceFloat(batch, true, true);
            inputX.Init(1);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < 1; i++)
            {
                //Cudalib.Matrix_Product_Weight(inputX.CudaPtr, outputY.CudaPtr, weight.CudaPtr,
                //        batch, input, output, 1);
                Cudalib.CuBLAS_Scal(GHandle, inputX.CudaPtr, 10, batch);
            }
            inputX.SyncToCPU();
            stopwatch.Stop();
            
            inputX.Dispose();

            Cudalib.CudaDestroyCuBlas(GHandle);
        }

        static void TestSoftmax()
        {
            int row = 1000;
            int col = 30000;

            CudaPieceFloat matrix = new CudaPieceFloat(row * col, true, true);
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    matrix.MemPtr[i * col + j] = i * 0.00001f + j * 0.0001f;
                }
            }
            matrix.SyncFromCPU();

            CudaPieceFloat output1 = new CudaPieceFloat(row * col, true, true);
            Stopwatch stopwatch = new Stopwatch();
            // Begin timing.
            stopwatch.Start();
            for (int i = 0; i < 10; i++)
                Cudalib.MultiClassSoftmax(matrix.CudaPtr, output1.CudaPtr, col, 1, row);
            //(matrix.CudaPtr, vec.CudaPtr, row, col, 0.2f);
            Cudalib.CudaDeviceSynchronize();
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("V1 ColumnWise Sum Time elapsed: {0}", stopwatch.Elapsed);

            CudaPieceFloat output2 = new CudaPieceFloat(row * col, true, true);

            IntPtr cublasP = Cudalib.CudaCreateCuDnn();

            Stopwatch stopwatch3 = new Stopwatch();
            // Begin timing.
            stopwatch3.Start();
            for (int i = 0; i < 10; i++)
                Cudalib.CuDNNSoftmax(cublasP, matrix.CudaPtr, output2.CudaPtr, row, col);
            Cudalib.CudaDeviceSynchronize();
            stopwatch3.Stop();
            // Write result.
            Console.WriteLine("V3 ColumnWise Sum Time elapsed: {0}", stopwatch3.Elapsed);


            output2.SyncToCPU();
            output1.SyncToCPU();


            for (int i = 0; i < row * col; i++)
            {
                if (Math.Abs(output2.MemPtr[i] - output1.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, output1.MemPtr[i], output2.MemPtr[i]);
                }
            }

            output2.Dispose();
            output1.Dispose();
            matrix.Dispose();

        }

        static void TestDerivSoftmax()
        {
            int row = 1000;
            int col = 20;

            CudaPieceFloat matrix = new CudaPieceFloat(row * col, true, true);
            CudaPieceInt smpIdx = new CudaPieceInt(row, true, true);
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                    matrix.MemPtr[i * col + j] = 0.1f ;
                smpIdx.MemPtr[i] = col * (i + 1);
            }
            matrix.SyncFromCPU();
            smpIdx.SyncFromCPU();

            CudaPieceFloat output2 = new CudaPieceFloat(row * col, true, true);
            IntPtr cublasP = Cudalib.CudaCreateCuDnn();
            Cudalib.CuDNNSoftmax(cublasP, matrix.CudaPtr, output2.CudaPtr, row, col);
            Cudalib.CudaDeviceSynchronize();
            // Write result.
            output2.SyncToCPU();

            CudaPieceFloat deriv = new CudaPieceFloat(row * col, true, true);
            Cudalib.Zero(deriv.CudaPtr, row * col);
            for (int r = 0; r < row; r++)
            {
                deriv.MemPtr[r * col + r % col] = 1.0f;
            }
            deriv.SyncFromCPU();

            CudaPieceFloat grad1 = deriv; // new CudaPieceFloat(row * col, true, true);
            Cudalib.CuDNNDerivSoftmax(cublasP, output2.CudaPtr, deriv.CudaPtr, grad1.CudaPtr, row, col, 0);
            grad1.SyncToCPU();

            /*
            CudaPieceFloat grad2 = new CudaPieceFloat(row * col, true, true);
            Cudalib.DerivSparseMultiClassSoftmax(smpIdx.CudaPtr, output2.CudaPtr, deriv.CudaPtr, grad2.CudaPtr, 1.0f, row);
            grad2.SyncToCPU();
            for (int i = 0; i < row * col; i++)
            {
                if (Math.Abs(grad1.MemPtr[i] - grad2.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, grad1.MemPtr[i], grad2.MemPtr[i]);
                }
            }
            grad2.Dispose();
            */

            grad1.Dispose();
            deriv.Dispose();

            smpIdx.Dispose();
            output2.Dispose();
            matrix.Dispose();
        }

        static void TestColumnWiseSumMask()
        {
            int batchSize = 128;
            int col = 1024;

            CudaPieceInt smpIdx = new CudaPieceInt(batchSize, true, true);
            Random mrandom = new Random(13);
            for(int i=0;i<batchSize;i++)
            {
                smpIdx.MemPtr[i] = mrandom.Next(10, 100) + (i == 0 ? 0 : smpIdx.MemPtr[i - 1]); 
            }
            smpIdx.SyncFromCPU();
            
            int sentSize = smpIdx.MemPtr[batchSize-1];

            CudaPieceFloat matrix = new CudaPieceFloat(sentSize * col, true, true);
            CudaPieceFloat wei = new CudaPieceFloat(sentSize, true, true);

            for (int i = 0; i < sentSize; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    matrix.MemPtr[i * col + j] = i * 0.0001f + j * 0.001f;
                }
                wei.MemPtr[i] = 1.0f;
            }
            matrix.SyncFromCPU();
            wei.SyncFromCPU();

            CudaPieceFloat output1 = new CudaPieceFloat(batchSize * col, true, true);
            Stopwatch stopwatch = new Stopwatch();
            // Begin timing.
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
                Cudalib.ColumnWiseSumMask(matrix.CudaPtr, IntPtr.Zero, wei.CudaPtr, smpIdx.CudaPtr,
                    batchSize, output1.CudaPtr, IntPtr.Zero, sentSize, col, 0, 1);
            Cudalib.CudaDeviceSynchronize();
            stopwatch.Stop();
            Console.WriteLine("V1 ColumnWise Sum Time elapsed: {0}", stopwatch.Elapsed);

            CudaPieceFloat output2 = new CudaPieceFloat(batchSize * col, true, true);
            Stopwatch stopwatch2 = new Stopwatch();
            // Begin timing.
            stopwatch2.Start();
            for (int i = 0; i < 100; i++)
                Cudalib.ColumnWiseSumMaskV2(matrix.CudaPtr, IntPtr.Zero, wei.CudaPtr, smpIdx.CudaPtr,
                    batchSize, output2.CudaPtr, IntPtr.Zero, sentSize, col, 0, 1);
            Cudalib.CudaDeviceSynchronize();
            stopwatch2.Stop();
            Console.WriteLine("V2 ColumnWise Sum Time elapsed: {0}", stopwatch2.Elapsed);

            output2.SyncToCPU();
            output1.SyncToCPU();


            for (int i = 0; i < batchSize * col; i++)
            {
                if (Math.Abs(output2.MemPtr[i] - output1.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, output1.MemPtr[i], output2.MemPtr[i]);
                }
            }
            output2.Dispose();
            output1.Dispose();
            
        }

        static void TestSparseMutliClassSoftmax()
        {
            //SparseMutliClassSoftmax(IntPtr smpIdx, IntPtr outputScore, IntPtr outputProb, float gamma, int batchSize);
            int batchSize = 128;

            CudaPieceInt smpIdx = new CudaPieceInt(batchSize, true, true);
            Random mrandom = new Random(13);
            for (int i = 0; i < batchSize; i++)
            {
                smpIdx.MemPtr[i] = mrandom.Next(30, 200) + (i == 0 ? 0 : smpIdx.MemPtr[i - 1]);
            }
            smpIdx.SyncFromCPU();

            int sentSize = smpIdx.MemPtr[batchSize - 1];
            CudaPieceFloat outputScore = new CudaPieceFloat(sentSize, true, true);
            for (int i = 0; i < sentSize; i++) outputScore.MemPtr[i] = (mrandom.Next(1, 100) - 20) / 10.0f;
            outputScore.SyncFromCPU();


            CudaPieceFloat output1 = new CudaPieceFloat(sentSize, true, true);
            Stopwatch stopwatch = new Stopwatch();
            // Begin timing.
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
                Cudalib.SparseMutliClassSoftmax(smpIdx.CudaPtr, outputScore.CudaPtr, output1.CudaPtr, 1, batchSize);
            Cudalib.CudaDeviceSynchronize();
            stopwatch.Stop();
            Console.WriteLine("V1 ColumnWise Sum Time elapsed: {0}", stopwatch.Elapsed);


            CudaPieceFloat output2 = new CudaPieceFloat(sentSize, true, true);
            Stopwatch stopwatch2 = new Stopwatch();
            // Begin timing.
            stopwatch2.Start();
            for (int i = 0; i < 100; i++)
                Cudalib.SparseMutliClassSoftmax(smpIdx.CudaPtr, outputScore.CudaPtr, output2.CudaPtr, 1, batchSize);
            Cudalib.CudaDeviceSynchronize();
            stopwatch2.Stop();
            Console.WriteLine("V1 ColumnWise Sum Time elapsed: {0}", stopwatch2.Elapsed);



            output2.SyncToCPU();
            output1.SyncToCPU();

            for (int i = 0; i < sentSize; i++)
            {
                if (Math.Abs(output2.MemPtr[i] - output1.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, output1.MemPtr[i], output2.MemPtr[i]);
                }
            }
            output2.Dispose();
            output1.Dispose();

        }

        static void TestSger()
        {
            int batchSize = 2280;
            int hidden = 1280;

            CudaPieceFloat x = new CudaPieceFloat(batchSize, true, true);
            CudaPieceFloat y = new CudaPieceFloat(hidden, true, true);
            x.Init(1.0f, -0.5f);
            y.Init(1.0f, -0.5f);

            IntPtr GHandle = Cudalib.CudaCreateCuBlas();

            CudaPieceFloat output1 = new CudaPieceFloat(batchSize * hidden, true, true);
            Stopwatch stopwatch = new Stopwatch();
            // Begin timing.
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
                Cudalib.CuBLAS_Sgemm(GHandle, x.CudaPtr, y.CudaPtr, output1.CudaPtr, batchSize, 1, hidden, 0, 1, false, false);
            Cudalib.CudaDeviceSynchronize();
            stopwatch.Stop();
            Console.WriteLine("V1 rank-1 update Time elapsed: {0}", stopwatch.Elapsed);

            CudaPieceFloat output2 = new CudaPieceFloat(batchSize * hidden, true, true);
            Stopwatch stopwatch2 = new Stopwatch();
            // Begin timing.
            stopwatch2.Start();
            for (int i = 0; i < 100; i++)
            {
                //Cudalib.Zero(output2.CudaPtr, batchSize * hidden);
                Cudalib.CuBLAS_Sger(GHandle, x.CudaPtr, y.CudaPtr, output2.CudaPtr, batchSize, hidden, 1.0f);
            }
            Cudalib.CudaDeviceSynchronize();
            stopwatch2.Stop();
            Console.WriteLine("V2 rank-1 update Time elapsed: {0}", stopwatch2.Elapsed);


            output2.SyncToCPU();
            output1.SyncToCPU();

            for (int i = 0; i < batchSize * hidden; i++)
            {
                if (Math.Abs(output2.MemPtr[i] - output1.MemPtr[i]) > 0.001)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, output1.MemPtr[i], output2.MemPtr[i]);
                }
            }

            output2.Dispose();
            output1.Dispose();
        }

        static void TestRecurrentUpdate()
        {
            int batchSize = 128;

            int dim = 1024;
            CudaPieceInt smpIdx = new CudaPieceInt(batchSize, true, true);
            Random mrandom = new Random(13);
            for (int i = 0; i < batchSize; i++)
            {
                smpIdx.MemPtr[i] = 10 + 2 * (batchSize - i) + (i == 0 ? 0 : smpIdx.MemPtr[i - 1]);
            }
            smpIdx.SyncFromCPU();
            int sentSize = smpIdx.MemPtr[batchSize - 1];

            KeyValuePair<int, int>[] seqLen = new KeyValuePair<int, int>[batchSize];
            for (int i = 0; i < batchSize; i++)
            {
                seqLen[i] = new KeyValuePair<int, int>(i, i == 0 ?
                        smpIdx.MemPtr[i] : (smpIdx.MemPtr[i] - smpIdx.MemPtr[i - 1]));
            }
            int MaxLag = seqLen.Select(i => i.Value).Max();
            CudaPieceInt Lag1SourceIndex = new CudaPieceInt(sentSize, true, true);

            int elementCount = 0;
            int lag1Index = 0;
            int[] preTSet = null;
            for (int lag = 0; lag < MaxLag; lag++)
            {
                IEnumerable<KeyValuePair<int, int>> result = seqLen.Where(i => i.Value > lag);
                int[] tSet = result.Select(i => i.Value - lag - 1).ToArray();
                int[] avSet = result.Select(i => i.Key).ToArray();

                if (lag > 0)
                {
                    int melement = elementCount;
                    for (int i = 0; i < preTSet.Length; i++)
                    {
                        if (preTSet[i] > 0)
                        {
                            Lag1SourceIndex.MemPtr[lag1Index] = melement - preTSet.Length + i;
                            lag1Index++;
                            elementCount++;
                        }
                    }
                }
                else elementCount = avSet.Length;
                preTSet = tSet;
            }
            Lag1SourceIndex.SyncFromCPU(lag1Index);
            //lag1Index

            CudaPieceFloat prob = new CudaPieceFloat(dim * sentSize, true, true);
            prob.Init(-1.0f, -0.5f);

            IntPtr cublasP = Cudalib.CudaCreateCuBlas();


            CudaPieceFloat output1 = new CudaPieceFloat(dim * dim, true, true);
            output1.Zero();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 5;i++ )
                Cudalib.SgemmMask(prob.CudaPtr, IntPtr.Add(prob.CudaPtr, batchSize * dim * sizeof(float)),
                                output1.CudaPtr, lag1Index, dim, dim,
                                Lag1SourceIndex.CudaPtr, IntPtr.Zero, IntPtr.Zero,
                                0, 1, true, false);
            Cudalib.CudaDeviceSynchronize();
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("V1 Recurrent Update. SgemmMask elapsed: {0}", stopwatch.Elapsed);

            CudaPieceFloat mem = new CudaPieceFloat(dim * lag1Index, true, true);
            CudaPieceFloat output2 = new CudaPieceFloat(dim * dim, true, true);
            output2.Zero();
            Stopwatch stopwatch2 = new Stopwatch();
            stopwatch2.Start();
            for (int i = 0; i < 50; i++)
            {
                Cudalib.Matrix_AdditionMask(prob.CudaPtr, Lag1SourceIndex.CudaPtr,
                                            mem.CudaPtr, IntPtr.Zero,
                                            mem.CudaPtr, IntPtr.Zero,
                                            dim, lag1Index, 1, 0, 0);
                Cudalib.CuBLAS_Sgemm(cublasP, mem.CudaPtr, IntPtr.Add(prob.CudaPtr, batchSize * dim * sizeof(float)),
                                output2.CudaPtr, lag1Index, dim, dim, 0, 1, true, false);
            }
            Cudalib.CudaDeviceSynchronize();
            stopwatch2.Stop();
            // Write result.
            Console.WriteLine("V2 Recurrent Update. SgemmMask elapsed: {0}", stopwatch2.Elapsed);



            output1.SyncToCPU();
            output2.SyncToCPU();

            for (int i = 0; i < dim * dim; i++)
            {
                if (Math.Abs(output1.MemPtr[i] - output2.MemPtr[i]) > 0.1)
                {
                    Console.WriteLine("The index of {0} is different {1} and {2}", i, output2.MemPtr[i], output1.MemPtr[i]);
                }
            }
        }

        static void TestKNN()
        {
            int num = 500000;
            int dim = 256;
            int topK = 1;
            int qNum = 32;

            //Cudalib.CudaSetDevice(0);
            CudaPieceFloat matrix = new CudaPieceFloat(num * dim, DeviceType.GPU);
            matrix.Init(2, -1);
            matrix.Norm(dim, num, VecNormType.L2Norm);

            CudaPieceFloat query = new CudaPieceFloat(qNum * dim, DeviceType.GPU);
            query.Init(2, -1);
            query.Norm(dim, qNum, VecNormType.L2Norm);

            //IMathOperationManager lib = MathOperatorManager.CreateInstance(DeviceType.GPU);

            CudaPieceFloat score = new CudaPieceFloat( qNum * num, DeviceType.GPU);
            CudaPieceFloat bestValues = new CudaPieceFloat(qNum * topK, DeviceType.GPU);
            CudaPieceFloat bestIdxs = new CudaPieceFloat(qNum * topK, DeviceType.GPU);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            lib.Sgemm(matrix, 0, query, 0, score, 0, num, dim, qNum, 0, 1, false, true);

            //score.SyncToCPU();
            lib.KLargestValueBatch(score, qNum, num, topK, -1, bestValues, bestIdxs);

            bestValues.SyncToCPU();
            bestIdxs.SyncToCPU();

            stopwatch.Stop();
            Console.WriteLine("KNN Search: {0}", stopwatch.Elapsed);
            score.SyncToCPU();

            for (int q = 0; q < qNum; q++)
            {
                Console.WriteLine("Query {0}", q);
                for (int t = 0; t < topK; t++)
                {
                    Console.WriteLine("best {2} Idxs {0}, best Values {1} :", bestIdxs.MemPtr[q * topK + t], bestValues.MemPtr[q * topK + t], t);
                }
            }
        }

        static void Main(string[] args)
        {
            InitLib();

            //TestRecursiveForwardPropagateBatch();
            //TestMax();
            //TestColumnWiseSum();
            //TestDerivSparseSoftmax();
            //TestImageCNNForward();
            //TestImageCNNBackwardData();
            //TestImageCNNBackwardFilter();
            //TestImageCNNBackwardBias();
            //TestFullyConnectForward();
            //TestFullyConnectBackwardData();
            //TestFullyConnectBackwardWeight();
            //TestOffSet();
            //TestElementWiseMultiplication();

            //TestSoftmax();
            //TestVectorScale();
            //TestColumnWiseSumMask();
            //TestSparseMutliClassSoftmax();

            //TestSger();
            //TestRecurrentUpdate();

            //TestDerivSoftmax();

            TestKNN();
            Console.ReadLine();
        }
    }
}
