#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include "device_functions.h"
#include <fstream>
#include "cublas_v2.h"

#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

using namespace std;
#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#endif

/*********************** Cuda Util *********************************/
__global__ void cuda_Max(int * int_array, int * result, int len)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	__shared__ int max_value[256];
	max_value[idx] = 0;
	for (int i = idx; i<len; i += 256)
	{
		if (int_array[i] > max_value[idx])
		{
			max_value[idx] = int_array[i];

		}
	}
	__syncthreads();
	if (idx == 0)
	{
		int max = 0;
		for (int i = 0; i < 256; i++)
		{
			if(max_value[i] > max) max = max_value[i];
		}
		result[0] = max;
	}
}

int Cuda_Max(int * int_array, int len)
{
	int * result;
	cudaMalloc((void**)&result, sizeof(int)); // , cudaHostAllocMapped);
	cuda_Max<<<1, 256, 256 * 4>>>(int_array, result, len);
	int mr = 0;
	cudaMemcpy(&mr, result, sizeof(int), cudaMemcpyDeviceToHost);
	cudaFree(result);
	return mr;
}


__global__ void cuda_Init(float * float_array, int len, float initValue)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < len) float_array[idx] = initValue;
}
void Cuda_Init(float * float_array, int len, float initValue)
{
	if (initValue == 0) cudaMemset(float_array, 0, len * sizeof(float));
	else cuda_Init << < (len - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(float_array, len, initValue);
}

__device__ void __cuda_Swap(int id1, int id2, float* values)
{
	float f = values[id1];
	values[id1] = values[id2];
	values[id2] = f;
}

__device__ void __cuda_Swap(int id1, int id2, int* values)
{
	int f = values[id1];
	values[id1] = values[id2];
	values[id2] = f;
}


__device__ void __cuda_Down(int idx, int K, int mode, float * values, float * indexes)
{
	int min_index = idx * 2 + 1;
	if (min_index < K)
	{
		int mIdx = idx * 2 + 2;
		if (mIdx < K && values[mIdx] * mode < values[min_index] * mode)
			min_index = mIdx;

		if (values[min_index] * mode < values[idx] * mode)
		{
			__cuda_Swap(idx, min_index, values);
			__cuda_Swap(idx, min_index, indexes);
			__cuda_Down(min_index, K, mode, values, indexes);
		}
	}
}
__device__ void __cuda_Up(int idx, int K, int mode, float * values, float * indexes)
{
	if (idx > 0)
	{
		int pare_index = (idx - 1) / 2;
		if (values[idx] * mode < values[pare_index] * mode)
		{
			__cuda_Swap(idx, pare_index, values);
			__cuda_Swap(idx, pare_index, indexes);
			__cuda_Up(pare_index, K, mode, values, indexes);
		}
	}
}

__global__ void cuda_KBestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, float * bestIndexes)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		float * pArray = float_array + idx * dim;
		float * pValues = bestValues + idx * K;
		float * pIndexes = bestIndexes + idx * K;

		int kCount = 0;
		for (int i = 0; i < dim; i++)
		{
			if (kCount < K)
			{
				pValues[kCount] = pArray[i];
				pIndexes[kCount] = i;
				__cuda_Up(kCount, K, mode, pValues, pIndexes);
				kCount += 1;
			}
			else
			{
				if (pValues[0] * mode < pArray[i] * mode)
				{
					pValues[0] = pArray[i];
					pIndexes[0] = i;
					__cuda_Down(0, K, mode, pValues, pIndexes);
				}
			}
		}
	}
}
/// MinMax Heap for KBest Number Selection.
void Cuda_KLargestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, float * bestIndexes)
{
	cuda_KBestValueBatch << < (batchSize - 1) / 32 + 1, 32 >> >(float_array, batchSize, dim, K, mode, bestValues, bestIndexes);
}


__device__ void __cuda_Down_stride(int idx, int K, int mode, float * values, int * indexes, int stride)
{
	int min_index = idx * 2 + 1;
	if (min_index < K)
	{
		int mIdx = idx * 2 + 2;
		if (mIdx < K && values[mIdx * stride] * mode < values[min_index * stride] * mode)
			min_index = mIdx;

		if (values[min_index * stride] * mode < values[idx * stride] * mode)
		{
			__cuda_Swap(idx * stride, min_index * stride, values);
			__cuda_Swap(idx * stride, min_index * stride, indexes);
			__cuda_Down_stride(min_index, K, mode, values, indexes, stride);
		}
	}
}
__device__ void __cuda_Up_stride(int idx, int K, int mode, float * values, int * indexes, int stride)
{
	if (idx > 0)
	{
		int pare_index = (idx - 1) / 2;
		if (values[idx * stride] * mode < values[pare_index * stride] * mode)
		{
			__cuda_Swap(idx * stride, pare_index * stride, values);
			__cuda_Swap(idx * stride, pare_index * stride, indexes);
			__cuda_Up_stride(pare_index, K, mode, values, indexes, stride);
		}
	}
}

__global__ void cuda_Fast_KBestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, int * bestIndexes, float * _bestValues, int * _bestIndexes, int stride)
{
	int id_x = blockDim.x * blockIdx.x + threadIdx.x;
	int id_y = blockDim.y * blockIdx.y + threadIdx.y;

	if (id_x < batchSize && id_y < stride)
	{
		float * pArray = float_array + id_x * dim;

		float * pValues = bestValues + id_x * dim + id_y; // K;
		int * pIndexes = bestIndexes + id_x * dim + id_y; // K;

		int kCount = 0;
		for (int i = id_y; i < dim; i += stride)
		{
			if (kCount < K)
			{
				pValues[kCount * stride] = pArray[i];
				pIndexes[kCount * stride] = i;
				__cuda_Up_stride(kCount, K, mode, pValues, pIndexes, stride);
				kCount += 1;
			}
			else
			{
				if (pValues[0] * mode < pArray[i] * mode)
				{
					pValues[0] = pArray[i];
					pIndexes[0] = i;
					__cuda_Down_stride(0, K, mode, pValues, pIndexes, stride);
				}
			}
		}

		__syncthreads();
		if (id_y == 0)
		{
			float * _pArray = bestValues + id_x * dim;
			int * _pIndex = bestIndexes + id_x * dim;

			float * _pValues = _bestValues + id_x * K;
			int * _pIndexes = _bestIndexes + id_x * K;

			int _kCount = 0;
			for (int i = 0; i < stride * K; i++)
			{
				if (_kCount < K)
				{
					_pValues[_kCount] = _pArray[i];
					_pIndexes[_kCount] = _pIndex[i];

					__cuda_Up_stride(_kCount, K, mode, _pValues, _pIndexes, 1);
					_kCount += 1;
				}
				else
				{
					if (_pValues[0] * mode < _pArray[i] * mode)
					{
						_pValues[0] = _pArray[i];
						_pIndexes[0] = _pIndex[i];
						__cuda_Down_stride(0, K, mode, _pValues, _pIndexes, 1);
					}
				}
			}
		}

	}
}

/// MinMax Heap for KBest Number Selection.
void Cuda_Fast_KLargestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, int * bestIndexes, float * _bestValues, int * _bestIndexes)
{
	int stride = (int)sqrt(dim / K);

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (stride - 1) / DEFAULT_THREAD_PER_DIM + 1);

	cuda_Fast_KBestValueBatch << < block_tail, thread_tail >> >(float_array, batchSize, dim, K, mode, bestValues, bestIndexes, _bestValues, _bestIndexes, stride);
}


__global__ void cuda_clip_vector(float * gpu_floats_a, int m, float maxThreshold, float minThreshold)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		if (gpu_floats_a[id] > maxThreshold)
			gpu_floats_a[id] = maxThreshold;
		else if (gpu_floats_a[id] < minThreshold)
			gpu_floats_a[id] = minThreshold;
	}
}

void cuda_Clip_Vector(float * gpu_floats_a, int m, float maxThreshold, float minThreshold)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_clip_vector << <block_tail, thread_tail >> >(gpu_floats_a, m, maxThreshold, minThreshold);
}



__global__ void cuda_copy(float * gpu_floats_a, float * gpu_float_b, int m, int aIdx, int bIdx)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		gpu_floats_a[aIdx + id] = gpu_float_b[bIdx + id]; 
	}
}

void cuda_Copy(float * gpu_floats_a, float * gpu_float_b, int m, int aIdx, int bIdx)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_copy << <block_tail, thread_tail >> >(gpu_floats_a, gpu_float_b, m, aIdx, bIdx);
}


__global__ void cuda_matrix_add(float * gpu_floats_a, float * gpu_floats_b, int m, int n, float mweight)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < n && idy < m)
	{
		gpu_floats_a[idy*n + idx] = gpu_floats_a[idy*n + idx] + gpu_floats_b[idy*n + idx] * mweight;
	}
}

void cuda_Matrix_Add(float * gpu_floats_a, float * gpu_floats_b, int m, int n, float mweight)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((n + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_add << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, m, n, mweight);
}

__global__ void cuda_scale_vector(float * gpu_floats_a, int m, float mweight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		gpu_floats_a[id] = gpu_floats_a[id] * mweight; 
	}
}

void cuda_Scale_Vector(float * gpu_floats_a, int m, float mweight)
{

	int RowNum = (m - 1) / COL_FOLD_NUM + 1;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_scale_vector << <block_tail, thread_tail >> >(gpu_floats_a, m, mweight);
}



__global__ void cuda_ada_gradient(float * ada, float * grad, int m)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		ada[id] = ada[id] + grad[id] * grad[id];
		grad[id] = grad[id] / sqrtf(ada[id]);
	}
}

void cuda_Ada_Gradient(float * ada, float * grad, int m)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_ada_gradient << <block_tail, thread_tail >> >(ada, grad, m);
}



__global__ void cuda_scale_matrix(float * gpu_floats_a, int m, int n, float mweight)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < n && idy < m)
	{
		gpu_floats_a[idy * n + idx] = gpu_floats_a[idy * n + idx] * mweight; //(float)log( (float)gpu_floats_a[idx]);
	}
}
__global__ void cuda_scale_matrix(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei, float bwei)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dim && idy < batchSize)
	{
		int aP = (aMask == NULL ? idy : aMask[idy]) * dim + idx;
		int bP = (bMask == NULL ? idy : bMask[idy]) * dim + idx;
		b[bP] = b[bP] * bwei + a[aP] * awei[idy];
	}
}
__global__ void cuda_accurate_scale_matrix(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dim && idy < batchSize)
	{
		int aP = (aMask == NULL ? idy : aMask[idy]) * dim + idx;
		int bP = (bMask == NULL ? idy : bMask[idy]) * dim + idx;
		float aw = awei == NULL ? 1 : awei[idy];
		atomicAdd(b + bP, a[aP] * aw);
			//atomicAdd(inputDeriv + inputBatchIdx + z * width * height + (j + startIdy) * width + (i + startIdx), 
			//deriv * filter[o * sx * sx * c + z * sx * sx + j * sx + i]);
		//b[bP] = b[bP] * bwei + a[aP] * awei[idy];
	}
}
void cuda_Scale_Matrix(float * gpu_floats_a, int m, int n, float mweight)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((n + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_scale_matrix << <block_tail, thread_tail >> >(gpu_floats_a, m, n, mweight);
}
void cuda_Scale_Matrix(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei, float bwei)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_scale_matrix << <block_tail, thread_tail >> >(a, aMask, b, bMask, dim, batchSize, awei, bwei);
}
void cuda_Accurate_Scale_Matrix(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_accurate_scale_matrix << <block_tail, thread_tail >> >(a, aMask, b, bMask, dim, batchSize, awei);
}
__global__ void cuda_matrix_add_sigmoid(float * gpu_floats_a, float * gpu_floats_b, int m, int n)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < n && idy < m)
	{
		int col = idx; //% n;
		float t = gpu_floats_a[idy * n + idx] + gpu_floats_b[col];
		gpu_floats_a[idy * n + idx] = (tanhf(t / 2) + 1) / 2;
		//(Math.Tanh(x[i] * gamma / 2) + 1) / 2.0f;
	}
}

void cuda_Matrix_Add_Sigmoid(float * gpu_floats_a, float * gpu_floats_b, int m, int n)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((n + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_add_sigmoid << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, m, n);
}

__global__ void cuda_deriv_image_FType(float * delta, float * layer_output, int batchSize, int width, int height, int depth, int af)
{
	int idz = blockDim.z * blockIdx.z + threadIdx.z;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < width && idy < height && idz < batchSize * depth)
	{
		int id = idz * width * height + idy * width + idx;
		if (af == 1)
		{
			delta[id] = delta[id] * (1 - layer_output[id]) * (1 + layer_output[id]);
		}
		else if (af == 2)
		{
			if (layer_output[id] <= 0)
			{
				delta[id] = 0; // delta[idy * m +idx] ;
			}
		}
		else if (af == 3)
		{
			delta[id] = delta[id] * (1 - layer_output[id]) *  layer_output[id];
		}
	}
}

void cuda_Deriv_Image_FType(float * delta, float * layer_output, int batchSize, int width, int height, int depth, int af)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize * m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D);
	dim3 block_tail(
			(width + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D, 
			(height + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D, 
			(batchSize * depth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);

	cuda_deriv_image_FType << <block_tail, thread_tail >> >(delta, layer_output, batchSize, width, height, depth, af);
}


/// Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3
__global__ void cuda_matrix_add_image_FType(float * gpu_floats_a, float * gpu_floats_b, int batchSize, int width, int height, int depth, int af)
{
	int idz = blockDim.z * blockIdx.z + threadIdx.z;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < width && idy < height && idz < batchSize * depth)
	{
		int o = idz % depth; //% n;
		int id = idz * width * height + idy * width + idx;
		float t = gpu_floats_a[id] + gpu_floats_b[o];
		if (af == 1)
		{
			t = tanhf(t);
		}
		else if (af == 2)
		{
			if (t < 0) t = 0;
		}
		else if (af == 3)
		{
			t = (tanhf(t / 2) + 1) / 2;
		}
		gpu_floats_a[id] = t;
	}
}

void cuda_Matrix_Add_Image_FType(float * gpu_floats_a, float * gpu_floats_b, int batchSize, int width, int height, int depth, int af)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D);
	dim3 block_tail((width + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(height + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(batchSize * depth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);

	cuda_matrix_add_image_FType << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, batchSize, width, height, depth, af);
}


__global__ void cuda_matrix_add_tanh(float * gpu_floats_a, float * gpu_floats_b, int m, int n)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < n && idy < m)
	{
		int col = idx; //% n;
		float t = gpu_floats_a[idy * n + idx] + gpu_floats_b[col];
		gpu_floats_a[idy * n + idx] = tanhf(t);
	}
}

void cuda_Matrix_Add_Tanh(float * gpu_floats_a, float * gpu_floats_b, int m, int n)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((n + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_add_tanh << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, m, n);
}


__global__ void cuda_matrix_rectified_vector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dimension && idy < batchsize)
	{
		gpu_floats_a[idy * dimension + idx] = gpu_floats_a[idy * dimension + idx] + gpu_floats_b[idx];
		if (gpu_floats_a[idy * dimension + idx] < 0)
		{
			gpu_floats_a[idy * dimension + idx] = 0;
		}
	}
}

void cuda_Matrix_Rectified_Vector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_matrix_rectified_vector << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, batchsize, dimension);
}


__global__ void cuda_deriv_cosine(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float a = 0;
		float b = eps;
		float c = eps;
		for (int i = 0; i < m; i++)
		{
			a += q[idx * m + i] * d[idx * m + i];
			b += q[idx * m + i] * q[idx * m + i];
			c += d[idx * m + i] * d[idx * m + i];
		}
		b = sqrtf(b);
		c = sqrtf(c);
		for (int i = 0; i < m; i++)
		{
			dcq[idx * m + i] = (float)((1 - q[idx * m + i]) * (1 + q[idx * m + i]) * (d[idx*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			dcd[idx * m + i] = (float)((1 - d[idx * m + i]) * (1 + d[idx * m + i]) * (q[idx*m + i] * 1.0f / (b*c) - d[idx*m + i] * a * 1.0f / (b*c*c*c)));
			//dcq[idx * m + i] = dcq[idx * m + i] * 1.0f / batchsize;
			//dcd[idx * m + i] = dcd[idx * m + i] * 1.0f / batchsize;
		}
	}
}

void cuda_Deriv_Cosine(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_deriv_cosine << < nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(q, d, dcq, dcd, batchsize, m, eps);
}

__global__ void cuda_deriv_cosine_linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float a = 0;
		float b = eps;
		float c = eps;
		for (int i = 0; i < m; i++)
		{
			a += q[idx * m + i] * d[idx * m + i];
			b += q[idx * m + i] * q[idx * m + i];
			c += d[idx * m + i] * d[idx * m + i];
		}
		b = sqrtf(b);
		c = sqrtf(c);
		for (int i = 0; i < m; i++)
		{
			dcq[idx * m + i] = (float)((d[idx*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			dcd[idx * m + i] = (float)((q[idx*m + i] * 1.0f / (b*c) - d[idx*m + i] * a * 1.0f / (b*c*c*c)));
			//dcq[idx * m + i] = dcq[idx * m + i] * 1.0f ; // batchsize;
			//dcd[idx * m + i] = dcd[idx * m + i] * 1.0f ; // batchsize;
		}
	}
}

void cuda_Derive_Cosine_Linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_deriv_cosine_linear << < nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(q, d, dcq, dcd, batchsize, m, eps);
}

__global__ void cuda_deriv_innerproduct_linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		/*float a = 0;
			float b = eps;
			float c = eps;
			for (int i = 0; i < m; i++)
			{
			a += q[idx * m + i] * d[idx * m + i];
			b += q[idx * m + i] * q[idx * m + i];
			c += d[idx * m + i] * d[idx * m + i];
			}
			b = sqrtf(b);
			c = sqrtf(c);*/
		for (int i = 0; i < m; i++)
		{
			dcq[idx * m + i] = d[idx*m + i]; // (float)((d[idx*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			dcd[idx * m + i] = q[idx*m + i];// (float)((q[idx*m + i] * 1.0f / (b*c) - d[idx*m + i] * a * 1.0f / (b*c*c*c)));
			//dcq[idx * m + i] = dcq[idx * m + i] * 1.0f ; // batchsize;
			//dcd[idx * m + i] = dcd[idx * m + i] * 1.0f ; // batchsize;
		}
	}
}

void cuda_Deriv_InnerProduct_Linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_deriv_innerproduct_linear << < nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(q, d, dcq, dcd, batchsize, m, eps);
}


__global__ void cuda_deriv_cosine_rectified(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float a = 0;
		float b = eps;
		float c = eps;
		for (int i = 0; i < m; i++)
		{
			a += q[idx * m + i] * d[idx * m + i];
			b += q[idx * m + i] * q[idx * m + i];
			c += d[idx * m + i] * d[idx * m + i];
		}
		b = sqrtf(b);
		c = sqrtf(c);
		for (int i = 0; i < m; i++)
		{
			if (fabsf(q[idx * m + i]) < eps)
			{
				dcq[idx * m + i] = 0;
			}
			else
			{
				dcq[idx * m + i] = (float)((d[idx*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			}
			//dcq[idx * m + i] = dcq[idx * m + i] * 1.0f ; // batchsize;

			if (fabsf(d[idx * m + i]) < eps)
			{
				dcd[idx * m + i] = 0;
			}
			else
			{
				dcd[idx * m + i] = (float)((q[idx*m + i] * 1.0f / (b*c) - d[idx*m + i] * a * 1.0f / (b*c*c*c)));
			}
			//dcd[idx * m + i] = dcd[idx * m + i] * 1.0f ; // batchsize;
		}
	}
}
void cuda_Derive_Cosine_Rectified(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_deriv_cosine_rectified << < nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(q, d, dcq, dcd, batchsize, m, eps);
}

//optimized version -- hxd
__global__ void cuda_deriv_cosine_ex(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float a = 0;
		float b = 0;
		float c = 0;
		float bc, a_bbbc, a_bccc, batchsizenorm;
		float * q_iter = q + idx*m;
		float * d_iter = d + neg_list[idx] * m;
		float * q_iter_end = q_iter + m;
		while (q_iter < q_iter_end)
		{
			b += (*q_iter) * (*q_iter);
			c += (*d_iter) * (*d_iter);
			a += (*q_iter++) * (*d_iter++);
		}
		b = sqrtf(b);
		c = sqrtf(c);
		bc = b*c + eps;
		a_bbbc = a / (b*b*b*c + eps);
		a_bccc = a / (b*c*c*c + eps);
		batchsizenorm = 1.0f;// batchsize;

		q_iter = q + idx*m;
		d_iter = d + neg_list[idx] * m;
		q_iter_end = q_iter + m;
		float * dcq_iter = dcq + idx*m;
		float * dcd_iter = dcd + idx*m;

		while (q_iter < q_iter_end)
		{
			*dcq_iter++ = (1.0f - *q_iter) * (1.0f + *q_iter) * (*d_iter / bc - *q_iter * a_bbbc); // * batchsizenorm;
			*dcd_iter++ = (1.0f - *d_iter) * (1.0f + *d_iter) * (*q_iter / bc - *d_iter * a_bccc); // * batchsizenorm;
			++q_iter;
			++d_iter;
		}
	}
}

void cuda_Deriv_Cosine_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_deriv_cosine_ex << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(q, d, neg_list, dcq, dcd, batchsize, m, eps);
}

__global__ void cuda_deriv_cosine_linear_ex(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float a = 0;
		float b = eps;
		float c = eps;
		int mIndex = neg_list[idx];
		for (int i = 0; i < m; i++)
		{
			a += q[idx * m + i] * d[mIndex * m + i];
			b += q[idx * m + i] * q[idx * m + i];
			c += d[mIndex * m + i] * d[mIndex * m + i];
		}
		b = sqrtf(b);
		c = sqrtf(c);
		for (int i = 0; i < m; i++)
		{
			dcq[idx * m + i] = (float)((d[mIndex*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			dcd[idx * m + i] = (float)((q[idx*m + i] * 1.0f / (b*c) - d[mIndex*m + i] * a * 1.0f / (b*c*c*c)));
			//dcq[idx * m + i] = dcq[idx * m + i] * 1.0f ;// batchsize;
			//dcd[idx * m + i] = dcd[idx * m + i] * 1.0f ;// batchsize;
		}
	}
}

void cuda_Deriv_Cosine_Linear_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_deriv_cosine_linear_ex << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(q, d, neg_list, dcq, dcd, batchsize, m, eps);
}

__global__ void cuda_deriv_cosine_rectified_ex(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float a = 0;
		float b = eps;
		float c = eps;
		int mIndex = neg_list[idx];
		for (int i = 0; i < m; i++)
		{
			a += q[idx * m + i] * d[mIndex * m + i];
			b += q[idx * m + i] * q[idx * m + i];
			c += d[mIndex * m + i] * d[mIndex * m + i];
		}
		b = sqrtf(b);
		c = sqrtf(c);
		for (int i = 0; i < m; i++)
		{
			if (q[idx*m + i] == 0)
			{
				dcq[idx * m + i] = 0;
			}
			else
			{
				dcq[idx * m + i] = (float)((d[mIndex*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			}
			//dcq[idx * m + i] = dcq[idx * m + i] * 1.0f ; // batchsize;


			if (d[mIndex*m + i] == 0)
			{
				dcd[idx * m + i] = 0;
			}
			else
			{
				dcd[idx * m + i] = (float)((q[idx*m + i] * 1.0f / (b*c) - d[mIndex*m + i] * a * 1.0f / (b*c*c*c)));
			}
			//dcd[idx * m + i] = dcd[idx * m + i] * 1.0f ; // batchsize;
		}
	}
}

void cuda_Deriv_Cosine_Rectified_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_deriv_cosine_rectified_ex << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(q, d, neg_list, dcq, dcd, batchsize, m, eps);
}


__global__ void cuda_cosine_similarity(float * a, float * b, float * c, int nTrial, int BATCHSIZE, int mindex,
		int batchsize, int dimension, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float sumxx = 0;
		float sumyy = 0;
		float sumxy = 0;
		for (int i = 0; i < dimension; i++)
		{
			sumxx += a[idx * dimension + i] * a[idx * dimension + i];
			sumyy += b[idx * dimension + i] * b[idx * dimension + i];
			sumxy += a[idx * dimension + i] * b[idx * dimension + i];
		}
		c[mindex * BATCHSIZE + idx] = (float)(sumxy * 1.0f / (sqrtf((float)(sumxx * sumyy)) + eps));
	}

}
void cuda_Cosine_Similarity(float * a, float * b, float * c, int nTrial, int BATCHSIZE, int mindex,
		int batchsize, int dimension, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_cosine_similarity << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, c, nTrial, BATCHSIZE, mindex, batchsize, dimension, eps);
}


__global__ void cuda_innerproduct_similarity(float * a, float * b, float * c, int batchsize, int dimension)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float sumxy = 0;
		for (int i = 0; i < dimension; i++)
		{
			sumxy += a[idx * dimension + i] * b[idx * dimension + i];
		}
		c[idx] = (float)(sumxy * 1.0f);
	}

}

void cuda_InnerProduct_Similarity(float * a, float * b, float * c, int batchsize, int dimension)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_innerproduct_similarity << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, c, batchsize, dimension);
}

//optimized version -- hxd
__global__ void cuda_cosine_similarity_ex(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int mindex,
		int batchsize, int dimension, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float sumxx = 0;
		float sumyy = 0;
		float sumxy = 0;
		float * a_iter = a + (idx * dimension);
		float * b_iter = b + (neg_list[idx] * dimension);
		float * a_iter_end = a_iter + dimension;
		while (a_iter < a_iter_end)
		{
			sumxx += (*a_iter) * (*a_iter);
			sumyy += (*b_iter) * (*b_iter);
			sumxy += (*a_iter++) * (*b_iter++);
		}
		c[mindex * BATCHSIZE + idx] = (float)(sumxy / ((float)sqrtf(sumxx * sumyy) + eps));
	}
}

void cuda_Cosine_Similarity_EX(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int mindex,
		int batchsize, int dimension, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_cosine_similarity_ex << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, neg_list, c, nTrial, BATCHSIZE, mindex, batchsize, dimension, eps);
}

__global__ void cuda_cal_alpha(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < (nTrial - 1)*batchsize)
	{
		int row = idx / batchsize;
		int col = idx % batchsize;
		alpha[row * BATCHSIZE + col + BATCHSIZE] = expf((float)(-gamma * (alpha[col] - alpha[row * BATCHSIZE + col + BATCHSIZE])));
	}
}

__global__ void cuda_cal_alpha_sum(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma, int init)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float sum = init;
		for (int i = 1; i < nTrial; i++)
		{
			sum += alpha[i * BATCHSIZE + idx];
		}
		alpha[idx] = sum;
	}
}

__global__ void cuda_cal_alpha_norm(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < (nTrial - 1)*batchsize)
	{
		int row = idx / batchsize;
		int col = idx % batchsize;
		alpha[row * BATCHSIZE + col + BATCHSIZE] = (float)((gamma * alpha[row * BATCHSIZE + col + BATCHSIZE]) / alpha[col]);
		//expf( (float)(-gamma * (alpha[col] - alpha[row * BATCHSIZE + col + BATCHSIZE]))) ; 
	}
}


void  cuda_Calculate_Alpha(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;

	int nBlockPerGrid_1 = ((nTrial - 1)*batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_cal_alpha << <nBlockPerGrid_1, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma);

	int nBlockPerGrid_2 = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_cal_alpha_sum << <nBlockPerGrid_2, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma, 1);

	cuda_cal_alpha_norm << <nBlockPerGrid_1, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma);
	cuda_cal_alpha_sum << <nBlockPerGrid_2, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma, 0);
}

__global__ void cuda_Calculate_SampleLRDeriv(float * output, float * deriv, int trial, int BATCHSIZE, float * label, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < trial*batchsize)
	{
		int row = idx / batchsize;
		int col = idx % batchsize;
		if (row == 0)
		{
			deriv[row * BATCHSIZE + col] = gamma * (label[col] - 1.0f / (1.0f + expf(-gamma * output[row * BATCHSIZE + col])));
		}
		else
		{
			deriv[row * BATCHSIZE + col] = gamma * (1.0f / (1.0f + expf(-gamma * output[row * BATCHSIZE + col])));// / trial;
		}
	}
}
void Cuda_Calculate_SampleLRDeriv(float * output, float * deriv, int trial, int BATCHSIZE, float * label, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (trial*batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_Calculate_SampleLRDeriv << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(output, deriv, trial, BATCHSIZE, label, batchsize, gamma);
}


__global__ void cuda_Calculate_SampleCrossEntropyDeriv(float * output, float * deriv, int dim, float * label, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dim * batchsize)
	{
		deriv[idx] = -gamma * (tanhf(gamma * output[idx] / 2) + 1) / 2.0f;
		if (idx % dim == 0) deriv[idx] += gamma * label[idx / dim];
	}
}
void Cuda_Calculate_SampleCrossEntropyDeriv(float * output, float * deriv, int dim, float * label, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (dim * batchsize - 1) / DEFAULT_THREAD_PER_BLOCK + 1;
	cuda_Calculate_SampleCrossEntropyDeriv << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(output, deriv, dim, label, batchsize, gamma);
}



__global__ void cuda_cal_alpha_norm_MXE(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < (nTrial - 1)*batchsize)
	{
		int row = idx / batchsize;
		int col = idx % batchsize;
		alpha[row * BATCHSIZE + col + BATCHSIZE] = (float)((gamma * alpha[row * BATCHSIZE + col + BATCHSIZE]) / alpha[col] / alpha[col]);
		//expf( (float)(-gamma * (alpha[col] - alpha[row * BATCHSIZE + col + BATCHSIZE]))) ; 
	}
}

void  cuda_Calculate_Alpha_MXE(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;

	int nBlockPerGrid_1 = ((nTrial - 1)*batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_cal_alpha << <nBlockPerGrid_1, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma);

	int nBlockPerGrid_2 = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_cal_alpha_sum << <nBlockPerGrid_2, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma, 1);

	cuda_cal_alpha_norm_MXE << <nBlockPerGrid_1, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma);
	cuda_cal_alpha_sum << <nBlockPerGrid_2, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma, 0);
}

__global__ void cuda_cal_alpha_PAIRRANK(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float msum = 0;
		for (int n = 1; n < nTrial; n++)
		{
			float a = gamma * (1.0f - 1.0f / (1 + expf(-gamma * (alpha[idx] - alpha[n * BATCHSIZE + idx]))));
			alpha[n * BATCHSIZE + idx] = a;
			msum += a;
		}
		alpha[idx] = msum;
	}
}

void cuda_Calculate_Alpha_PAIRRANK(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_cal_alpha_PAIRRANK << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(alpha, nTrial, BATCHSIZE, batchsize, gamma);
}

__global__ void cuda_cal_alpha_nce(float * alpha, float* dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		alpha[idx] = gamma - gamma / (1.0f + (nTrial - 1) * expf(dist[idx] - gamma * alpha[idx] + gamma)); //+gamma is from hxd, sd doesn't have this
	}
	else if (idx < nTrial*batchsize)
	{
		int row = idx / batchsize;
		int col = idx % batchsize;
		alpha[row * BATCHSIZE + col] = gamma / (1.0f + (nTrial - 1) * expf(dist[row * BATCHSIZE + col] - gamma * alpha[row * BATCHSIZE + col] + gamma)); //+gamma is from hxd, sd doesn't have this
	}
}

void cuda_Calculate_Alpha_NCE(float* alpha, float* dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (nTrial*batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_cal_alpha_nce << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(alpha, dist, nTrial, BATCHSIZE, batchsize, gamma);
}

__global__ void cuda_cal_alpha_nce2(float * alpha, float* dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < nTrial*batchsize)
	{
		int row = idx / batchsize;
		int col = idx % batchsize;
		float s = 1.0f / (1.0f + (nTrial - 1) * expf(dist[row * BATCHSIZE + col] - gamma * alpha[row * BATCHSIZE + col] + gamma)); //+gamma is from hxd, sd doesn't have this
		alpha[row * BATCHSIZE + col] = gamma * s * (1.0f - s);
	}
}

void cuda_Calculate_Alpha_NCE2(float* alpha, float* dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (nTrial*batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_cal_alpha_nce2 << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(alpha, dist, nTrial, BATCHSIZE, batchsize, gamma);
}

__global__ void cuda_fillout_dist_nce(float* dist, int* neg_list, int nTrailPlus1, int BATCH_SIZE, int mindex, int batchsize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		int mtindex = neg_list[idx];
		dist[mindex * BATCH_SIZE + idx] = dist[mtindex];
	}
}

void cuda_FillOut_Dist_NCE(float* dist, int* neg_list, int nTrailPlus1, int BATCH_SIZE, int mindex, int batchsize)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_fillout_dist_nce << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(dist, neg_list, nTrailPlus1, BATCH_SIZE, mindex, batchsize);
}

//optimized version -- hxd
__global__ void cuda_matrix_product(float * a, float * b, float * c, int batchsize, int m, int n)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < n && idy < m)
	{
		int row = idy; // / n;
		int col = idx;// % n;
		float sum = 0;
		float *a_iter = a + row;
		float *b_iter = b + col;
		float *a_end_pt = a_iter + (m*batchsize);
		while (a_iter < a_end_pt)
		{
			sum += (*a_iter) * (*b_iter);
			a_iter += m;
			b_iter += n;
		}
		c[idy * n + idx] = sum;
	}
}


void cuda_Matrix_Product(float * a, float * b, float * c, int batchsize, int m, int n)
//, int kept, float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((n + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (m + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_product << <block_tail, thread_tail >> >(a, b, c, batchsize, m, n);
	//, kept, alpha, ntrial, BATCH_SIZE, alpha_index);
}


__global__ void cuda_convolution_sparse_matrix_product_INTEX(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output_dimension * win_size)
	{
		int output_idx = idx / win_size;
		int win_idx = idx % win_size;

		//float sum = 0;
		for (int b = 0; b < batchsize; b++)
		{
			int target_seg = maxpooling_index[b * output_dimension + output_idx];

			if (target_seg == -1)
			{
				continue;
			}
			int target_smp = SegMargin_Index[target_seg];
			//deriv[i * output_dimension + idx] *  
			int ws = win_size / 2;
			int w = win_idx - ws;
			int row = target_seg + w; // idx / n;
			if (row >= 0 && row < seg_size)
			{
				if (SegMargin_Index[row] == target_smp)
				{
					int col_end = Seg_Index[row];
					int col_begin = 0;
					if (row > 0)
					{
						col_begin = Seg_Index[row - 1];
					}
					//float sum = 0;
					for (int i = col_begin; i < col_end; i++)
					{
						int fea_idx = Fea_Index[i];
						if (fea_idx >= Feature_Dimension)
						{
							continue;
						}
						float m = Fea_Value[i] * deriv[b*output_dimension + output_idx];
						// con_weight[((w+ws) * Feature_dimension + fea_idx)*output_dimension+idx];
						grad[(win_idx*Feature_Dimension + fea_idx) * output_dimension + output_idx] += m;
					}
				}
			}
		}

	}
}

void cuda_Convolution_Sparse_Matrix_Product_INTEX(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((output_dimension * win_size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK);

	cuda_convolution_sparse_matrix_product_INTEX << <block_tail, thread_tail >> >(deriv, maxpooling_index, Seg_Index, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, Fea_Index, Fea_Value, grad, Feature_Dimension);
	//,alpha, ntrial, BATCH_SIZE, alpha_index); 
}



__global__ void cuda_convolution_sparse_matrix_multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
		int seg_size, int * Fea_Index, float * Fea_Value, 
		int elementsize, float * con_weight, float * output, int Feature_dimension, 
		int output_dimension, int win_size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < output_dimension && idy < seg_size)
	{
		output[idy * output_dimension + idx] = 0;
		int ws = win_size / 2;
		int mSmp_idx = Seg_Margin[idy];
		float sum = 0;
		for (int w = -ws; w < (int)win_size - ws; w++)
		{
			int row = idy + w;
			if (row >= 0 && row < seg_size && Seg_Margin[row] == mSmp_idx)
			{
				int col_end = Seg_Index[row];
				int col_begin = row == 0 ? 0 : Seg_Index[row - 1];
				float * pcon_w = con_weight + (w + ws) * Feature_dimension * output_dimension + idx;
				for (int i = col_begin; i < col_end; i++)
				{
					int fea_idx = Fea_Index[i];
					//if (fea_idx >= Feature_dimension) { continue; }	
					sum += Fea_Value[i] * pcon_w[fea_idx * output_dimension];  
					//sum += con_weight[((w + ws) * Feature_dimension + fea_idx) * output_dimension + idx];
				}
			}
		}
		output[idy * output_dimension + idx] = sum;
	}
}

void cuda_Convolution_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
		int seg_size, int * Fea_Index, float * Fea_Value, int elementsize,
		float * con_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (seg_size + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_convolution_sparse_matrix_multiply_INTEX << <block_tail, thread_tail >> >(Smp_Index, batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, Fea_Value, elementsize, con_weight, output, Feature_dimension, output_dimension, win_size);
}

__global__ void cuda_imageconvolution(float * inputImage, int batchSize, int width, int height, int depth, 
		float * filter, int sx, int c, int output, int pad, int stride,
		float * poolingOutput, int poolingWidth, int poolingHeight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idz = blockDim.z * blockIdx.z + threadIdx.z;

	if (idx < poolingWidth && idy < poolingHeight && idz < output * batchSize)
	{
		//poolingOutput[idz * poolingWidth * poolingHeight + idy * poolingWidth + idx] = 2;
		int startIdx = idx * stride - pad;
		int startIdy = idy * stride - pad;

		int batchIdx = idz / output;
		int o = idz % output;

		int inputBatchIdx = batchIdx * width * height * depth;
		float sum = 0;
		for (int i = 0; i < sx; i++)
		{
			for (int j = 0; j < sx; j++)
			{
				for (int z = 0; z < c; z++)
				{
					if (i + startIdx >= 0 && i + startIdx < width && j + startIdy >= 0 && j + startIdy < height && z < depth)
					{
						sum += inputImage[inputBatchIdx + z * width * height + (j + startIdy) * width + i + startIdx] *
							filter[o * sx * sx * c + z * sx * sx + j * sx + i];
					}
				}
			}
		}

		poolingOutput[idz * poolingWidth * poolingHeight + idy * poolingWidth + idx] = sum;
		//+ 20;
	}
}

///Depth == c;
void cuda_ImageConvolution(float * inputImage, int batchSize, int width, int height, int depth, 
		float * filter, int sx, int c, int output, int pad, int stride,
		float * poolingOutput, int poolingWidth, int poolingHeight)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D); //, 64); // output * batchSize);
	dim3 block_tail((poolingWidth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D, 
			(poolingHeight + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(output * batchSize + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);

	cuda_imageconvolution << <block_tail, thread_tail >> >(inputImage, batchSize, width, height, depth,
			filter, sx, c, output, pad, stride,
			poolingOutput, poolingWidth, poolingHeight);
}

__global__ void cuda_imageconvmatrixmultipy(float * outputDeriv, int * maxpoolingIndex,
		int batchSize, int outputWidth, int outputHeight, int outputDepth,
		int poolingSX, int poolingStride,
		float * filter, int sx, int c, int pad, int stride, 
		float * inputDeriv, int width, int height, int depth)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idz = blockDim.z * blockIdx.z + threadIdx.z;
	if (idx < outputWidth && idy < outputHeight && idz < outputDepth * batchSize)
	{
		int poolMaxId = maxpoolingIndex[idz * outputWidth * outputHeight + idy * outputWidth + idx];
		int maxIdx = poolMaxId % poolingSX;
		int maxIdy = poolMaxId / poolingSX;

		int poolIdx = idx * poolingStride + maxIdx;
		int poolIdy = idy * poolingStride + maxIdy;

		int startIdx = poolIdx * stride - pad;
		int startIdy = poolIdy * stride - pad;

		int batchIdx = idz / outputDepth;
		int o = idz % outputDepth;

		float deriv = outputDeriv[idz * outputWidth * outputHeight + idy * outputWidth + idx];

		int inputBatchIdx = batchIdx * width * height * depth;
		float sum = 0;
		for (int i = 0; i < sx; i++)
		{
			for (int j = 0; j < sx; j++)
			{
				for (int z = 0; z < c; z++)
				{
					if (i + startIdx >= 0 && i + startIdx < width && j + startIdy >= 0 && j + startIdy < height && z < depth)
					{
						//float score = deriv * filter[o * sx * sx * c + z * sx * sx + j * sx + i];
						atomicAdd(inputDeriv + inputBatchIdx + z * width * height + (j + startIdy) * width + (i + startIdx), 
								deriv * filter[o * sx * sx * c + z * sx * sx + j * sx + i]);
					}
				}
			}
		}
	}
}

void cuda_ImageConvMatrixMultipy(float * outputDeriv, int * maxpoolingIndex,
		int batchSize, int outputWidth, int outputHeight, int outputDepth,
		int poolingSX, int poolingStride,
		float * filter, int sx, int c, int pad, int stride, 
		float * inputDeriv, int width, int height, int depth)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D);
	dim3 block_tail((outputWidth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D, 
			(outputHeight + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(outputDepth * batchSize + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);

	cuda_imageconvmatrixmultipy << <block_tail, thread_tail >> >(outputDeriv, maxpoolingIndex, batchSize, outputWidth, outputHeight, outputDepth,
			poolingSX, poolingStride, filter, sx, c, pad, stride, inputDeriv, width, height, depth);
}

__global__ void cuda_maximagepooling(float * inputImage, int batchSize, int width, int height, int depth,
		int * maxpoolingIndex,
		int sx, int stride,
		float * output, int outputWidth, int outputHeight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idz = blockDim.z * blockIdx.z + threadIdx.z;

	if (idx < outputWidth && idy < outputHeight && idz < depth * batchSize)
	{
		int startIdx = idx * stride;
		int startIdy = idy * stride;

		int batchIdx = idz / depth;
		int o = idz % depth;

		int inputBatchIdx = (batchIdx * depth + o) * width * height;
		int outputBatchIdx = (batchIdx * depth + o) * outputWidth * outputHeight;
		float max = 0;
		int maxIndex = 0;
		int isFirst = 1;
		for (int i = 0; i < sx; i++)
		{
			for (int j = 0; j < sx; j++)
			{
				if (startIdx + i < width && startIdy + j < height)
				{
					float v = inputImage[inputBatchIdx + (startIdy + j) * width + (startIdx + i)];
					if (isFirst > 0 || v > max)
					{
						max = v;
						maxIndex = j * sx + i;
						isFirst = 0;
					}
				}
			}
		}
		output[outputBatchIdx + idy * outputWidth + idx] = max;
		maxpoolingIndex[outputBatchIdx + idy * outputWidth + idx] = maxIndex;
	}
}

void cuda_MaxImagePooling(float * inputImage, int batchSize, int width, int height, int depth,
		int * maxpoolingIndex,
		int sx, int stride,
		float * output, int outputWidth, int outputHeight)
	//LayerPoolingOutput.Data.CudaPtr, LayerPoolingOutput.BatchSize, LayerPoolingOutput.Width, LayerPoolingOutput.Height, LayerPoolingOutput.Depth,
	//LayerMaxPoolingIndex.CudaPtr,
	//Model.Param.PoolingParameter.SX, Model.Param.PoolingParameter.Stride,
	//Output.Data.CudaPtr, Output.Width, Output.Height)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D);
	dim3 block_tail((outputWidth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D, 
			(outputHeight + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(depth * batchSize + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);
	cuda_maximagepooling << <block_tail, thread_tail >> > (inputImage, batchSize, width, height, depth,
			maxpoolingIndex, sx, stride, output, outputWidth, outputHeight);
}


__global__ void cuda_maxoutPooling(float * input, int batchSize, int dim,
	int * maxpoolingIndex, int maxoutDim, int outDim, float * output, int format)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;

	if (idx < batchSize && idy < outDim)
	{
		int maxIndex = -1; // 
		float max = 0; // input[maxIndex];

		if (format == 0)
		{
			maxIndex = idx * dim + idy * maxoutDim;
			max = input[maxIndex];

			for (int i = 1; i < maxoutDim; i++)
			{
				int id = idx * dim + idy * maxoutDim + i;
				float v = input[id];
				if (v > max)
				{
					max = v;
					maxIndex = id;
				}
			}
		}
		else
		{
			maxIndex = idx * dim + idy;
			max = input[maxIndex];

			int id = idx * dim + idy;
			for (int i = 1; i < maxoutDim; i++)
			{
				id = id + outDim;
				float v = input[id];
				if (v > max)
				{
					max = v;
					maxIndex = id;
				}
			}
		}
		output[idx * outDim + idy] = max;
		maxpoolingIndex[idx * outDim + idy] = maxIndex;
	}
}

void cuda_MaxoutPooling(float * input, int batchSize, int dim,
	int * maxpoolingIndex, int maxoutDim, int outDim, float * output, int format)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (outDim - 1) / DEFAULT_THREAD_PER_DIM + 1);

	cuda_maxoutPooling << <block_tail, thread_tail >> > (input, batchSize, dim,
		maxpoolingIndex, maxoutDim, outDim, output, format);
}


__global__ void cuda_derivMaxoutPooling(float * input_deriv, int batchSize, int dim,
	int * maxpoolingIndex, int maxoutDim, int outDim, float * output_deriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;

	if (idx < batchSize && idy < outDim)
	{
		int maxIndex = maxpoolingIndex[idx * outDim + idy];
		float deriv = output_deriv[idx * outDim + idy];
		input_deriv[maxIndex] += deriv;
	}
}

void cuda_DerivMaxoutPooling(float * input_deriv, int batchSize, int dim,
	int * maxpoolingIndex, int maxoutDim, int outDim, float * output_deriv)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (outDim - 1) / DEFAULT_THREAD_PER_DIM + 1);

	cuda_derivMaxoutPooling << <block_tail, thread_tail >> > (input_deriv, batchSize, dim,
		maxpoolingIndex, maxoutDim, outDim, output_deriv);
}



__global__ void cuda_DerivGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx,
		float gamma, float * locationValue, float * locationForcusDeriv, float * locationDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < ensembleLen)
	{
		int smpEnd = smpIdx[idx];
		int smpBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int smpLen = smpEnd - smpBegin;


		int ensembleId = smpIdx[batchSize - 1] * idy + smpBegin;
		float sum = 0;
		for (int w = 0; w < smpLen; w++)
		{
			sum += locationDeriv[smpBegin + w] * locationValue[ensembleId + w]; 
		}

		int id = idx * ensembleLen + idy;
		float v = locationForcusValue[id];

		float deriv = 0;
		for (int w = 0; w < smpLen; w++)
		{
			float l = w * 1.0f / smpLen - v;

			deriv += (locationDeriv[smpBegin + w] - sum) * locationValue[ensembleId + w] * l;
		}
		locationForcusDeriv[id] = 2 * gamma * deriv / ensembleLen;
	}
}

void Cuda_DerivGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx,
		float gamma, float * locationValue, float * locationForcusDeriv, float * locationDeriv)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (ensembleLen - 1) / DEFAULT_THREAD_PER_DIM + 1);

	cuda_DerivGaussian << <block_tail, thread_tail >> >(locationForcusValue, ensembleLen, batchSize, smpIdx, 
			gamma, locationValue, locationForcusDeriv, locationDeriv);
}


__global__ void cuda_DerivRectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize,
		float gamma, int * locationIdx, float * locationValue, float * locationForcusDeriv, float * locationDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < ensembleLen)
	{
		int id = idx * ensembleLen + idy;
		float v = locationForcusValue[id];

		int smpEnd = smpIdx[idx];
		int smpBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int smpLen = smpEnd - smpBegin;

		float targetV = v * smpLen;

		float sum = 0;
		int lid = id * windowSize;
		for (int w = 0; w < windowSize; w++)
		{
			lid = lid + w;
			int tid = locationIdx[lid];
			if (tid >= 0 && tid < smpLen)
				sum += (tid - targetV) * locationDeriv[lid] * locationValue[lid];
		}
		locationForcusDeriv[id] = sum * smpLen * gamma * 2;
	}
}


void Cuda_DerivRectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize,
		float gamma, int * locationIdx, float * locationValue, float * locationForcusDeriv, float * locationDeriv)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (ensembleLen - 1) / DEFAULT_THREAD_PER_DIM + 1);

	cuda_DerivRectifiedGaussian << <block_tail, thread_tail >> >(locationForcusValue, ensembleLen, batchSize, smpIdx, windowSize,
			gamma, locationIdx, locationValue, locationForcusDeriv, locationDeriv);
}

__global__ void cuda_Gaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, float gamma, float * locationValue)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < ensembleLen)
	{
		int id = idx * ensembleLen + idy;
		float v = locationForcusValue[id];

		int smpEnd = smpIdx[idx];
		int smpBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int smpLen = smpEnd - smpBegin;

		int ensembleId = smpIdx[batchSize - 1] * idy + smpBegin;

		float sum = 0;
		for (int w = 0; w < smpLen; w++)
		{
			float l = w * 1.0f / smpLen - v;
			locationValue[ensembleId + w] = expf(-gamma * l * l);
			sum += locationValue[ensembleId + w];
		}

		for (int w = 0; w < smpLen; w++)
		{
			locationValue[ensembleId + w] = locationValue[ensembleId + w] / sum;
		}
	}
}

void Cuda_Gaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, float gamma, float * locationValue)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (ensembleLen - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Gaussian << <block_tail, thread_tail >> >(locationForcusValue, ensembleLen, batchSize, smpIdx, 
			gamma, locationValue);
}



__global__ void cuda_RectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize, 
		float gamma, int * locationIdx, float * locationValue)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < ensembleLen)
	{
		int id = idx * ensembleLen + idy;
		float v = locationForcusValue[id];

		int smpEnd = smpIdx[idx];
		int smpBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int smpLen = smpEnd - smpBegin;

		float targetV = v * smpLen;
		int target = (int)(targetV);
		int backLen = (windowSize - 1) / 2;

		///exp(gamma * (w - targetV)^2)
		//float sum = 0;
		for (int w = 0; w < windowSize; w++)
		{
			int lid = id * windowSize + w;

			int tid = target - backLen + w;
			if (tid >= 0 && tid < smpLen)
			{
				locationValue[lid] = expf( - gamma *(tid - targetV) * (tid - targetV));
				locationIdx[lid] = tid;
				//sum += locationValue[lid];
			}
			else
			{
				locationIdx[lid] = -1;
				locationValue[lid] = 0;
			}
		}

	}
}

void Cuda_RectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize, 
		float gamma, int * locationIdx, float * locationValue)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (ensembleLen + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_RectifiedGaussian << <block_tail, thread_tail >> >(locationForcusValue, ensembleLen, batchSize, smpIdx , windowSize,
			gamma, locationIdx, locationValue);

}

__global__ void cuda_DerivGaussianMemory(int * sentMargin, int sequenceSize, float * memoryInputDeriv, float * locationValue,
		int ensembleLen, int memoryVecDim, float * memoryOutputDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < sequenceSize && idy < memoryVecDim)
	{
		int batchId = idx == 0 ? 0 : sentMargin[idx - 1];

		float mDeriv = memoryOutputDeriv[batchId * memoryVecDim + idy];

		float sum = 0;
		for (int i = 0; i < ensembleLen; i++)
		{
			sum += mDeriv * locationValue[i * sequenceSize + idx];
		}

		memoryInputDeriv[idx * memoryVecDim + idy] += sum / ensembleLen;
	}
}

void Cuda_DerivGaussianMemory(int * sentMargin, int sequenceSize, float * memoryInputDeriv, float * locationValue,
		int ensembleLen, int memoryVecDim, float * memoryOutputDeriv)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((sequenceSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (memoryVecDim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivGaussianMemory << <block_tail, thread_tail >> >(sentMargin, sequenceSize, memoryInputDeriv, locationValue,
			ensembleLen, memoryVecDim, memoryOutputDeriv);
}

__global__ void cuda_DerivMemory(int * smpIdx, int batchSize, float * memoryInputDeriv, int * locationIdx, float * locationValue,
		int ensembleLen, int windowSize, int memoryVecDim, float * memoryOutputDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < memoryVecDim)
	{
		int smpBegin = idx == 0 ? 0 : smpIdx[idx - 1];

		float mDeriv = memoryOutputDeriv[idx * memoryVecDim + idy];
		float sum = 0;
		int lid = idx * ensembleLen;
		for (int i = 0; i < ensembleLen; i++)
		{
			lid = lid + i;
			for (int w = 0; w < windowSize; w++)
			{
				int id = lid * windowSize + w;
				int l = locationIdx[id];
				if (l >= 0)
				{
					int m = (smpBegin + l) * memoryVecDim;
					memoryInputDeriv[m + idy] += mDeriv * locationValue[id];
				}
			}
		}
	}
}

void Cuda_DerivMemory(int * smpIdx, int batchSize, float * memoryInputDeriv, int * locationIdx, float * locationValue,
		int ensembleLen, int windowSize, int memoryVecDim, float * memoryOutputDeriv)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (memoryVecDim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivMemory << <block_tail, thread_tail >> >(smpIdx, batchSize, memoryInputDeriv, locationIdx, locationValue, 
			ensembleLen, windowSize, memoryVecDim, memoryOutputDeriv);
}

__global__ void cuda_DerivGaussianMemoryAddressing(int * sentMargin, int sequenceSize, float * memoryInput, float * locationValueDeriv,
		int memoryVecDim, float * memoryOutputDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < sequenceSize)
	{
		//int id = idx * ensembleLen * windowSize + idy;

		int batchId = sentMargin[idx];

		int id_seq = idx * memoryVecDim;
		int id_bat = batchId * memoryVecDim;

		float sum = 0;
		for (int i = 0; i < memoryVecDim; i++)
		{
			sum += memoryInput[id_seq] * memoryOutputDeriv[id_bat];
			id_seq += 1;
			id_bat += 1;
		}
		locationValueDeriv[idx] = sum;
	}
}

void Cuda_DerivGaussianMemoryAddressing(int * sentMargin, int sequenceSize, float * memoryInput, float * locationValueDeriv,
		int memoryVecDim, float * memoryOutputDeriv)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((sequenceSize - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivGaussianMemoryAddressing << <block_tail, thread_tail >> >(sentMargin, sequenceSize, memoryInput, locationValueDeriv, memoryVecDim, memoryOutputDeriv);
}


__global__ void cuda_DerivMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValueDeriv, 
		int ensembleLen, int windowSize, int memoryVecDim, float * memoryOutputDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < ensembleLen * windowSize)
	{
		int id = idx * ensembleLen * windowSize + idy;
		int l = locationIdx[id];
		if (l >= 0)
		{
			int smpBegin = idx == 0 ? 0 : smpIdx[idx - 1];
			int m = (smpBegin + l) * memoryVecDim;
			int id_a = idx * memoryVecDim;

			float sum = 0;
			for (int i = 0; i < memoryVecDim; i++)
			{
				sum += memoryInput[m] * memoryOutputDeriv[id_a];
				id_a += 1;
				m += 1;
			}
			locationValueDeriv[id] = sum;
		}
	}
}

void Cuda_DerivMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValueDeriv,
		int ensembleLen, int windowSize, int memoryVecDim, float * memoryOutputDeriv)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (ensembleLen * windowSize - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivMemoryAddressing << <block_tail, thread_tail >> >(smpIdx, batchSize, memoryInput, locationIdx, locationValueDeriv, ensembleLen,
			windowSize, memoryVecDim, memoryOutputDeriv);
}

__global__ void cuda_GaussianMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, float * locationValue, int ensembleLen, int memoryVecDim, float * memoryOutput)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < memoryVecDim)
	{
		int smpBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int smpEnd = smpIdx[idx];
		int smpLen = smpEnd - smpBegin;
		float sum = 0;
		for (int i = 0; i < ensembleLen; i++)
		{
			int ensembleId = smpIdx[batchSize - 1] * i + smpBegin; 
			for (int w = 0; w < smpLen; w++)
			{
				float v = locationValue[ensembleId + w];
				int m = (smpBegin + w) * memoryVecDim + idy;
				sum += memoryInput[m] * v;
			}
		}
		memoryOutput[idx * memoryVecDim + idy] = sum / ensembleLen;
	}
}

void Cuda_GaussianMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, float * locationValue, int ensembleLen, int memoryVecDim, float * memoryOutput)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (memoryVecDim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_GaussianMemoryAddressing << <block_tail, thread_tail >> >(smpIdx, batchSize, memoryInput, locationValue, ensembleLen, memoryVecDim, memoryOutput);
}

__global__ void cuda_MemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValue, int ensembleLen,
		int windowSize, int memoryVecDim, float * memoryOutput)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < memoryVecDim)
	{
		int smpBegin = idx == 0 ? 0 : smpIdx[idx - 1];

		float sum = 0;
		for (int i = 0; i < ensembleLen; i++)
		{
			int lid = idx * ensembleLen + i;
			for (int w = 0; w < windowSize; w++)
			{
				int id = lid * windowSize + w;
				int l = locationIdx[id];
				if (l >= 0)
				{
					float v = locationValue[id];
					int m = (smpBegin + l) * memoryVecDim;
					sum += memoryInput[m + idy] * v;
				}
			}
		}
		memoryOutput[idx * memoryVecDim + idy] = sum / ensembleLen;
	}
}

void Cuda_MemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValue, int ensembleLen,
		int windowSize, int memoryVecDim, float * memoryOutput)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (memoryVecDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_MemoryAddressing << <block_tail, thread_tail >> >(smpIdx, batchSize, memoryInput, locationIdx, locationValue, ensembleLen,
			windowSize, memoryVecDim, memoryOutput);
}

__global__ void cuda_seq_sparse_matrix_multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
		int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < output_dimension && idy < batchsize)
	{
		int seg_end = Smp_Index[idy];
		int seg_begin = 0;
		if (idy > 0) seg_begin = Smp_Index[idy - 1];

		float sum = 0;
		int col_begin = seg_begin == 0 ? 0 : Seg_Index[seg_begin - 1];
		for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
		{
			int col_end = Seg_Index[word_idx];
			for (int i = col_begin; i < col_end; ++i)
			{
				int fea_idx = Fea_Index[i];
				sum += Fea_Value[i] * mul_weight[((word_idx - seg_begin) * Feature_dimension + fea_idx) * output_dimension + idx];
			}
			col_begin = col_end;
		}
		output[idy * output_dimension + idx] = sum;
	}
}

/*
	 Added by xinson, 2/17/2014
	 This version still computes sparse matrix (batch * input)  multiples a dense matrix. However, each rwo of the sparse matrix is more than just BOW; it is a sequence of BOW. Put it another way, the
	 sparse matrix has exactly the same structure as what is used in Convolutional_Sparse_Matrix_Multiply_INTEX. As a result, the dense matrix (mul_weight) is of size (Feature_dimension * win_size) * output_dimension,
	 where win_size is how many words per input sequence instance. Note that all input should have exactly the same number of words. One word is represented as an instance of BOW.
 */
void cuda_SEQ_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_seq_sparse_matrix_multiply_INTEX << <block_tail, thread_tail >> >(Smp_Index, batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension, win_size);
}



__global__ void cuda_seq_sparse_matrix_multiply_INT(int * Smp_Index, int batchsize, int * Seg_Index, 
		int seg_size, int * Fea_Index, float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < output_dimension && idy < batchsize)
	{
		int seg_end = Smp_Index[idy];
		int seg_begin = idy == 0 ? 0 : Smp_Index[idy - 1];

		float sum = 0;
		int col_begin = seg_begin == 0 ? 0 : Seg_Index[seg_begin - 1];
		for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
		{
			int col_end = Seg_Index[word_idx];
			for (int i = col_begin; i < col_end; ++i)
			{
				int fea_idx = Fea_Index[i];
				sum += Fea_Value[i] * mul_weight[fea_idx * output_dimension + idx];
			}
			col_begin = col_end;
		}
		output[idy * output_dimension + idx] = sum;
	}
}

void cuda_SEQ_Sparse_Matrix_Multiply_INT(int * Smp_Index, int batchsize, int * Seg_Index, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize, float * mul_weight, float * output, int Feature_dimension, int output_dimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_seq_sparse_matrix_multiply_INT << <block_tail, thread_tail >> >(Smp_Index, batchsize, Seg_Index, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension);
}


__global__ void cuda_seq_sparse_matrix_transpose_multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
		int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output_dimension)
	{
		int seg_begin = 0;
		for (int sample = 0; sample < batchsize; ++sample)
		{
			int seg_end = Smp_Index[sample];
			float sum = 0;
			for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
			{
				int col_end = Seg_Index[word_idx];
				int col_begin = 0;
				if (word_idx > 0)
				{
					col_begin = Seg_Index[word_idx - 1];
				}
				for (int i = col_begin; i < col_end; ++i)
				{
					int fea_idx = Fea_Index[i];

					mul_weight[((word_idx - seg_begin) * Feature_dimension + fea_idx) * output_dimension + idx] += Fea_Value[i] * output[sample * output_dimension + idx];
				}
			}
			seg_begin = seg_end;
		}
	}
}

/*
	 Added by xinson, 3/11/2015
	 Given the same two inputs of an sparse matrix A (indexed by rows, size: batch * X), and a dense matrix B (size: batch * Y), computing C = A^T * B (size: X * Y).
	 Although we compute the transpose of A multiplied by B, the code does not perform sparse transpose and indexing at all.
	 Instead, it partitioned along the columns of the result C matrix.
	 float * output is B.
	 float * mul_weight is C.
	 Zero initialization/clear on C is required in advance.
 */
void cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_seq_sparse_matrix_transpose_multiply_INTEX << <block_tail, thread_tail >> >(Smp_Index, batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension, win_size);
}

__global__ void cuda_seq_sparse_matrix_transpose_multiply_INTEX_weight(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
		int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output_dimension)
	{
		int seg_begin = 0;
		for (int sample = 0; sample < batchsize; ++sample)
		{
			int seg_end = Smp_Index[sample];
			float sum = 0;
			for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
			{
				int col_end = Seg_Index[word_idx];
				int col_begin = 0;
				if (word_idx > 0)
				{
					col_begin = Seg_Index[word_idx - 1];
				}
				for (int i = col_begin; i < col_end; ++i)
				{
					int fea_idx = Fea_Index[i];

					mul_weight[((word_idx - seg_begin) * Feature_dimension + fea_idx) * output_dimension + idx] += Fea_Value[i] * output[sample * output_dimension + idx] * weight;
				}
			}
			seg_begin = seg_end;
		}
	}
}

/*
	 Added by Yelong, 3/11/2015
	 Given the same two inputs of an sparse matrix A (indexed by rows, size: batch * X), and a dense matrix B (size: batch * Y), computing C = A^T * B (size: X * Y).
	 Although we compute the transpose of A multiplied by B, the code does not perform sparse transpose and indexing at all.
	 Instead, it partitioned along the columns of the result C matrix.
	 float * output is B.
	 float * mul_weight is C.
	 Zero initialization/clear on C is required in advance.
 */
void cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size, float weight)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_seq_sparse_matrix_transpose_multiply_INTEX_weight << <block_tail, thread_tail >> >(Smp_Index, batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension, win_size, weight);
}


__global__ void cuda_seq_sparse_matrix_transpose_multiply_INT_weight(int * Smp_Index, int batchsize, int * Seg_Index, 
		int seg_size, int * Fea_Index, float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output_dimension)
	{
		int seg_begin = 0;
		for (int sample = 0; sample < batchsize; ++sample)
		{
			int seg_end = Smp_Index[sample];
			float sum = 0;
			for (int word_idx = seg_begin; word_idx < seg_end; ++word_idx)
			{
				int col_end = Seg_Index[word_idx];
				int col_begin = 0;
				if (word_idx > 0) col_begin = Seg_Index[word_idx - 1];

				for (int i = col_begin; i < col_end; ++i)
				{
					int fea_idx = Fea_Index[i];

					mul_weight[fea_idx * output_dimension + idx] += Fea_Value[i] * output[sample * output_dimension + idx] * weight;
				}
			}
			seg_begin = seg_end;
		}
	}
}

/*
	 Added by Yelong, 3/11/2015
	 Given the same two inputs of an sparse matrix A (indexed by rows, size: batch * X), and a dense matrix B (size: batch * Y), computing C = A^T * B (size: X * Y).
	 Although we compute the transpose of A multiplied by B, the code does not perform sparse transpose and indexing at all.
	 Instead, it partitioned along the columns of the result C matrix.
	 float * output is B.
	 float * mul_weight is C.
	 Zero initialization/clear on C is required in advance.
 */
void cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(int * Smp_Index, int batchsize, int * Seg_Index, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize, float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_seq_sparse_matrix_transpose_multiply_INT_weight << <block_tail, thread_tail >> >(Smp_Index, batchsize, Seg_Index, seg_size, Fea_Index, Fea_Value, elementsize, 
			mul_weight, output, Feature_dimension, output_dimension, weight);
}



__global__ void cuda_matrix_weightadd(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension, float * mweight, int start, int keep)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dimension && idy < batchsize)
	{
		if (keep != 0)
		{
			gpu_floats_a[idy*dimension + idx] += keep * gpu_floats_b[idy*dimension + idx] * mweight[start + idy];
		}
		else
		{
			gpu_floats_a[idy*dimension + idx] = gpu_floats_b[idy*dimension + idx] * mweight[start + idy];
		}
	}
}

void cuda_Matrix_WeightAdd(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension, float * mweight, int start, int keep)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_weightadd << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, batchsize, dimension, mweight, start, keep);
}

__global__ void cuda_matrix_weightadd_ex(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int batchsize, int dimension, float * mweight, int start, int keep)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dimension && idy < batchsize)
	{
		int col_end = inver_neg_index[idy];
		int col_begin = 0;
		if (idy > 0)
		{
			col_begin = inver_neg_index[idy - 1];
		}
		float sum = 0;
		for (int i = col_begin; i < col_end; i++)
		{
			int row = inver_neg_value[i];
			sum += gpu_floats_b[row * dimension + idx] * mweight[start + row];
		}
		if (keep != 0)
		{
			gpu_floats_a[idy*dimension + idx] += keep * sum;
		}
		else
		{
			gpu_floats_a[idy*dimension + idx] = sum;
		}
	}
}

void cuda_Matrix_WeightAdd_EX(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int batchsize, int dimension, float * mweight, int start, int keep)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_weightadd_ex << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, inver_neg_index, inver_neg_value, batchsize, dimension, mweight, start, keep);
}

__global__ void cuda_sparse2dense_matrix(int * Smp_Idx, int * Fea_Idx, float * Fea_Value, float * matrix, int batchsize, int outputDimension)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		int end = Smp_Idx[idx];
		int begin = idx >= 1 ? Smp_Idx[idx - 1] : 0;
		for (int k = begin; k < end; k++)
		{
			matrix[idx * outputDimension + Fea_Idx[k]] = Fea_Value[k];
		}
	}
}

void cuda_Sparse2Dense_Matrix(int * Smp_Idx, int * Fea_Idx, float * Fea_Value, float * matrix, int batchsize, int outputDimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK);

	cuda_sparse2dense_matrix << <block_tail, thread_tail >> >(Smp_Idx, Fea_Idx, Fea_Value, matrix, batchsize, outputDimension);
}

__global__ void cuda_matrix_aggragate(float * a, float * b, int batchsize, int m)
//int kept, float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < m)
	{
		float sum = 0;
		for (int i = 0; i < batchsize; i++)
		{
			sum += a[i * m + idx]; //* alpha[alpha_index * BATCH_SIZE + i];
		}
		b[idx] = sum;
	}
}

void cuda_Matrix_Aggragate(float * a, float * b, int batchsize, int m)
	//, int kept, float * alpha, 
	//		  int ntrial, int BATCH_SIZE, int alpha_index)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_matrix_aggragate << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, batchsize, m); //,kept, alpha, ntrial, BATCH_SIZE, alpha_index);
}


__global__ void cuda_matrix_image_aggragate_weight(float * a, float * b, int batchSize, int width, int height, int depth, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idz = blockDim.z * blockIdx.z + threadIdx.z;

	if (idx < width && idy < height && idz < batchSize * depth)
	{
		int o = idz % depth;
		int id = idz * width * height + idy * width + idx;
		atomicAdd(b + o, weight * a[id]);
	}
}

void cuda_Matrix_Image_Aggragate_Weight(float * a, float * b, int batchSize, int width, int height, int depth, float weight)
	//, int kept, float * alpha, 
	//		  int ntrial, int BATCH_SIZE, int alpha_index)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D);
	dim3 block_tail(
			(width + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D, 
			(height + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D, 
			(batchSize * depth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);

	cuda_matrix_image_aggragate_weight << <block_tail, thread_tail >> >(a, b, batchSize, width, height, depth, weight); //,kept, alpha, ntrial, BATCH_SIZE, alpha_index);
}



__global__ void cuda_matrix_add_offset(float * a, int offset_a, float * b, int offset_b, int len, float mweight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < len)
	{
		a[offset_a + idx] += b[offset_b + idx] * mweight; //* alpha[alpha_index * BATCH_SIZE + i];
	}
}

void cuda_Matrix_Add_OFFSET(float * gpu_floats_a, int offset_a, float * gpu_floats_b, int offset_b, int len, float mweight)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (len + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_matrix_add_offset << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(gpu_floats_a, offset_a, gpu_floats_b, offset_b, len, mweight);
}

cublasHandle_t global_handle;

void cublas_Init()
{
	cublasCreate(&global_handle);
}

void cublas_Destroy()
{
	cublasDestroy(global_handle);
}

void cublas_Sasum(float *x, int len, int norm, float * result)
{
	cublasSasum(global_handle, len, x, norm, result);
}

void cublas_Matrix_Multipy(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	float al = 1.0f;
	float bet = 0;
	if (inverse == 0)
	{

		cublasSgemm(global_handle, CUBLAS_OP_N, CUBLAS_OP_N, n, batchsize, m, &al, weight, n, delta, m, &bet, delta_low, n);
	}
	else
	{
		cublasSgemm(global_handle, CUBLAS_OP_T, CUBLAS_OP_N, n, batchsize, m, &al, weight, m, delta, m, &bet, delta_low, n);
	}
}

__global__ void cuda_Cosine_Similarity_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize && idy < nTrialplus1)
	{
		float sumxx = 0;
		float sumyy = 0;
		float sumxy = 0;
		float * a_iter = a + (idx * dimension);
		float * b_iter = idy == 0 ? b + idx * dimension : b + (neg_list[idx * (nTrialplus1 - 1) + idy - 1] * dimension);
		float * a_iter_end = a_iter + dimension;
		while (a_iter < a_iter_end)
		{
			sumxx += (*a_iter) * (*a_iter);
			sumyy += (*b_iter) * (*b_iter);
			sumxy += (*a_iter++) * (*b_iter++);
		}
		c[idx * nTrialplus1 + idy] = (float)(sumxy / ((float)sqrtf(sumxx * sumyy) + eps));
	}
}
void Cuda_Cosine_Similarity_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize - 1) / DEFAULT_THREAD_PER_DIM + 1, (nTrialplus1 - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Cosine_Similarity_Full << <block_tail, thread_tail >> >(a, b, neg_list, c, nTrialplus1, batchsize, dimension, eps);
}

__global__ void cuda_Inner_Product_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize && idy < nTrialplus1)
	{
		float sumxx = 0;
		float sumyy = 0;
		float sumxy = 0;
		float * a_iter = a + (idx * dimension);
		float * b_iter = b + idy == 0 ? b + idx * dimension : b + (neg_list[idx * (nTrialplus1 - 1) + idy - 1] * dimension); //(neg_list[idy * BATCHSIZE + idx] * dimension);
		float * a_iter_end = a_iter + dimension;
		while (a_iter < a_iter_end)
		{
			sumxy += (*a_iter++) * (*b_iter++);
		}
		c[idx * nTrialplus1 + idy] = (float)(sumxy); 
	}
}
void Cuda_Inner_Product_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize  - 1) / DEFAULT_THREAD_PER_DIM + 1, (nTrialplus1 - 1) / DEFAULT_THREAD_PER_DIM + 1);

	cuda_Inner_Product_Full << <block_tail, thread_tail >> >(a, b, neg_list, c, nTrialplus1, batchsize, dimension, eps);
}


__global__ void cuda_cosine_similarity_ex_full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE,
		int batchsize, int dimension, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize && idy < nTrial)
	{
		float sumxx = 0;
		float sumyy = 0;
		float sumxy = 0;
		float * a_iter = a + (idx * dimension);
		float * b_iter = b + (neg_list[idy * BATCHSIZE + idx] * dimension);
		float * a_iter_end = a_iter + dimension;
		while (a_iter < a_iter_end)
		{
			sumxx += (*a_iter) * (*a_iter);
			sumyy += (*b_iter) * (*b_iter);
			sumxy += (*a_iter++) * (*b_iter++);
		}
		c[(idy + 1) * BATCHSIZE + idx] = (float)(sumxy / ((float)sqrtf(sumxx * sumyy) + eps));
	}
}
void cuda_Cosine_Similarity_EX_Full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (nTrial + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_cosine_similarity_ex_full << <block_tail, thread_tail >> >(a, b, neg_list, c, nTrial, BATCHSIZE, batchsize, dimension, eps);
}


__global__ void cuda_inner_product_ex_full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE,
		int batchsize, int dimension, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize && idy < nTrial)
	{
		float sumxx = 0;
		float sumyy = 0;
		float sumxy = 0;
		float * a_iter = a + (idx * dimension);
		float * b_iter = b + (neg_list[idy * BATCHSIZE + idx] * dimension);
		float * a_iter_end = a_iter + dimension;
		while (a_iter < a_iter_end)
		{
			//sumxx += (*a_iter) * (*a_iter);
			//sumyy += (*b_iter) * (*b_iter);
			sumxy += (*a_iter++) * (*b_iter++);
		}
		c[(idy + 1) * BATCHSIZE + idx] = (float)(sumxy); /// ((float)sqrtf(sumxx * sumyy) + eps));
	}
}
void cuda_Inner_Product_EX_Full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (nTrial + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_inner_product_ex_full << <block_tail, thread_tail >> >(a, b, neg_list, c, nTrial, BATCHSIZE, batchsize, dimension, eps);
}


__global__ void cuda_fillout_dist_nce_full(float* dist, int* neg_list, int nTrail, int BATCH_SIZE, int batchsize)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize && idy < nTrail)
	{
		int mtindex = neg_list[idy * BATCH_SIZE + idx];
		dist[BATCH_SIZE + idy * BATCH_SIZE + idx] = dist[mtindex];
	}
}
void cuda_FillOut_Dist_NCE_Full(float* dist, int* neg_list, int nTrail, int BATCH_SIZE, int batchsize)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (nTrail + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_fillout_dist_nce_full << <block_tail, thread_tail >> >(dist, neg_list, nTrail, BATCH_SIZE, batchsize);
}

__global__ void cuda_deriv_cosine_ex_full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchsize && idy < nTrail)
	{
		float a = 0;
		float b = 0;
		float c = 0;
		float bc, a_bbbc, a_bccc, batchsizenorm;
		float * q_iter = q + idx*m;
		float * d_iter = d + neg_list[idy * BATCHSIZE + idx] * m;
		float * q_iter_end = q_iter + m;

		float * q_iter_P = q_iter;
		float * d_iter_P = d_iter;
		float * q_iter_end_P = q_iter_end;

		while (q_iter < q_iter_end)
		{
			b += (*q_iter) * (*q_iter);
			c += (*d_iter) * (*d_iter);
			a += (*q_iter++) * (*d_iter++);
		}
		b = sqrtf(b);
		c = sqrtf(c);
		bc = b*c + eps;
		a_bbbc = a / (b*b*b*c + eps);
		a_bccc = a / (b*c*c*c + eps);

		batchsizenorm = 1; //1.0f / batchsize;

		q_iter = q_iter_P;
		d_iter = d_iter_P;
		q_iter_end = q_iter_end_P;

		float * dcq_iter = dcq + idy * (BATCHSIZE * m) + idx * m;
		float * dcd_iter = dcd + idy * (BATCHSIZE * m) + idx * m;

		while (q_iter < q_iter_end)
		{
			*dcq_iter++ = (1.0f - *q_iter) * (1.0f + *q_iter) * (*d_iter / bc - *q_iter * a_bbbc); //* batchsizenorm;
			*dcd_iter++ = (1.0f - *d_iter) * (1.0f + *d_iter) * (*q_iter / bc - *d_iter * a_bccc); //* batchsizenorm;
			++q_iter;
			++d_iter;
		}
	}
}
void cuda_Deriv_Cosine_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (nTrail + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_cosine_ex_full << <block_tail, thread_tail >> >(q, d, neg_list, dcq, dcd, nTrail, BATCHSIZE, batchsize, m, eps);
}

__global__ void cuda_Deriv_CosineSimilarity_partialMatching(float * src, float * tgt, float *srcSquare, float * tgtSquare, int dim,
		int * src2MatchIdx, int * src2MatchElement, int * tgtIdx, int srcSize, int matchSize, float * simi, float * derivSimi,
		float * dcSrc, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	//idx -> source/q Index.
	if (idx < srcSize && idy < dim)
	{
		int matchBeginIdx = idx == 0 ? 0 : src2MatchIdx[idx - 1];
		int matchEndIdx = src2MatchIdx[idx];

		float sum = 0;
		float qRoot = srcSquare[idx];
		float qv = src[idx * dim + idy];
		float qSquare_qv = qv / (qRoot * qRoot + eps);
		for (int match = matchBeginIdx; match < matchEndIdx; match++)
		{
			int mIdx = src2MatchElement[match];
			int dIdx = tgtIdx[mIdx];
			float dRoot = tgtSquare[dIdx];
			sum += derivSimi[mIdx] * (tgt[dIdx * dim + idy] / (qRoot * dRoot + eps) - qSquare_qv * simi[mIdx]); /// qSquare);
		}
		dcSrc[idx * dim + idy] += sum;
	}
}
void Cuda_Deriv_CosineSimilarity_Matching(float * q, float * d, float *qSquare, float * dSquare, int dim,
		int * src2MatchIdx, int * src2MatchElement, int * tgt2MatchIdx, int * tgt2MatchElement, int * srcIdx, int * tgtIdx,  int srcSize, int tgtSize, int matchSize, 
		float * simi, float * derivSimi, float * dcq, float * dcd, float eps)
{
	dim3 srcThread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 srcBlock_tail((srcSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);

	cuda_Deriv_CosineSimilarity_partialMatching << <srcBlock_tail, srcThread_tail >> >(q, d, qSquare, dSquare, dim,
			src2MatchIdx, src2MatchElement, tgtIdx, srcSize, matchSize, simi, derivSimi,
			dcq, eps);

	dim3 tgtThread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 tgtBlock_tail((tgtSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);

	cuda_Deriv_CosineSimilarity_partialMatching << <tgtBlock_tail, tgtThread_tail >> >(d, q, dSquare, qSquare, dim,
			tgt2MatchIdx, tgt2MatchElement, srcIdx, tgtSize, matchSize, simi, derivSimi,
			dcd, eps);
}


__global__ void cuda_Deriv_InnerProduct_partialMatching(float * d, int dim,
		int * src2MatchIdx, int * src2MatchElement, int * tgtIdx, int srcSize, int matchSize, float * derivSimi,
		float * dcq, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	//idx -> source/q Index.
	if (idx < srcSize && idy < dim)
	{
		int matchBeginIdx = idx == 0 ? 0 : src2MatchIdx[idx - 1];
		int matchEndIdx = src2MatchIdx[idx];

		float sum = 0;
		for (int match = matchBeginIdx; match < matchEndIdx; match++)
		{
			int mIdx = src2MatchElement[match];
			int dIdx = tgtIdx[mIdx];
			sum += derivSimi[mIdx] * d[dIdx * dim + idy];
		}
		dcq[idx * dim + idy] += sum;
	}
}
void Cuda_Deriv_InnerProduct_Matching(float * q, float * d, int dim,
		int * src2MatchIdx, int * src2MatchElement, int * tgt2MatchIdx, int * tgt2MatchElement, int * srcIdx, int * tgtIdx, int srcSize, int tgtSize, int matchSize,
		float * derivSimi, float * dcq, float * dcd, float eps)
{
	dim3 srcThread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 srcBlock_tail((srcSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Deriv_InnerProduct_partialMatching << <srcBlock_tail, srcThread_tail >> >(d, dim,
			src2MatchIdx, src2MatchElement, tgtIdx, srcSize, matchSize, derivSimi, dcq, eps);

	dim3 tgtThread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 tgtBlock_tail((tgtSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Deriv_InnerProduct_partialMatching << <tgtBlock_tail, tgtThread_tail >> >(q, dim,
			tgt2MatchIdx, tgt2MatchElement, srcIdx, tgtSize, matchSize, derivSimi, dcd, eps);
}


__global__ void cuda_deriv_cosine_linear_ex_full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchsize && idy < nTrail)
	{
		float a = 0;
		float b = eps;
		float c = eps;
		int mIndex = neg_list[idy * BATCHSIZE + idx];
		for (int i = 0; i < m; i++)
		{
			a += q[idx * m + i] * d[mIndex * m + i];
			b += q[idx * m + i] * q[idx * m + i];
			c += d[mIndex * m + i] * d[mIndex * m + i];
		}
		b = sqrtf(b);
		c = sqrtf(c);
		for (int i = 0; i < m; i++)
		{
			dcq[idy * BATCHSIZE * m + idx * m + i] = (float)((d[mIndex*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			dcd[idy * BATCHSIZE * m + idx * m + i] = (float)((q[idx*m + i] * 1.0f / (b*c) - d[mIndex*m + i] * a * 1.0f / (b*c*c*c)));
			//dcq[idy * BATCHSIZE * m + idx * m + i] = dcq[idy * BATCHSIZE * m + idx * m + i] * 1.0f / batchsize;
			//dcd[idy * BATCHSIZE * m + idx * m + i] = dcd[idy * BATCHSIZE * m + idx * m + i] * 1.0f / batchsize;
		}
	}
}

void cuda_Deriv_Cosine_Linear_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (nTrail + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_cosine_linear_ex_full << <block_tail, thread_tail >> >(q, d, neg_list, dcq, dcd, nTrail, BATCHSIZE, batchsize, m, eps);
}

__global__ void cuda_deriv_innerproduct_linear_ex_full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchsize && idy < nTrail)
	{
		int mIndex = neg_list[idy * BATCHSIZE + idx];
		for (int i = 0; i < m; i++)
		{
			dcq[idy * BATCHSIZE * m + idx * m + i] = d[mIndex*m + i]; // (float)((d[mIndex*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			dcd[idy * BATCHSIZE * m + idx * m + i] = q[idx*m + i]; // (float)((q[idx*m + i] * 1.0f / (b*c) - d[mIndex*m + i] * a * 1.0f / (b*c*c*c)));
		}
	}
}

void cuda_Deriv_InnerProduct_Linear_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (nTrail + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_innerproduct_linear_ex_full << <block_tail, thread_tail >> >(q, d, neg_list, dcq, dcd, nTrail, BATCHSIZE, batchsize, m, eps);
}



__global__ void cuda_deriv_cosine_rectified_ex_full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchsize && idy < nTrail)
	{
		float a = 0;
		float b = eps;
		float c = eps;
		int mIndex = neg_list[idy * BATCHSIZE + idx];
		for (int i = 0; i < m; i++)
		{
			a += q[idx * m + i] * d[mIndex * m + i];
			b += q[idx * m + i] * q[idx * m + i];
			c += d[mIndex * m + i] * d[mIndex * m + i];
		}
		b = sqrtf(b);
		c = sqrtf(c);
		for (int i = 0; i < m; i++)
		{
			if (q[idx*m + i] == 0)
			{
				dcq[idy * BATCHSIZE * m + idx * m + i] = 0;
			}
			else
			{
				dcq[idy * BATCHSIZE * m + idx * m + i] = (float)((d[mIndex*m + i] * 1.0f / (b*c) - q[idx*m + i] * a * 1.0f / (b*b*b*c)));
			}
			//dcq[idy * BATCHSIZE * m + idx * m + i] = dcq[idy * BATCHSIZE * m + idx * m + i] * 1.0f / batchsize;

			if (d[mIndex*m + i] == 0)
			{
				dcd[idy * BATCHSIZE * m + idx * m + i] = 0;
			}
			else
			{
				dcd[idy * BATCHSIZE * m + idx * m + i] = (float)((q[idx*m + i] * 1.0f / (b*c) - d[mIndex*m + i] * a * 1.0f / (b*c*c*c)));
			}
			//dcd[idy * BATCHSIZE * m + idx * m + i] = dcd[idy * BATCHSIZE * m + idx * m + i] * 1.0f / batchsize;
		}
	}
}

void cuda_Deriv_Cosine_Rectified_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (nTrail + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_cosine_rectified_ex_full << <block_tail, thread_tail >> >(q, d, neg_list, dcq, dcd, nTrail, BATCHSIZE, batchsize, m, eps);
}


__global__ void cuda_matrix_weightadd_full(float * gpu_floats_a, float * gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dimension && idy < batchsize)
	{
		for (int i = 0; i < nTrail; i++)
		{
			gpu_floats_a[idy*dimension + idx] += keep * gpu_floats_b[i * BATCHSIZE * dimension + idy * dimension + idx] * mweight[start + i * BATCHSIZE + idy];
		}

	}
}

/// b add to a.
void cuda_Matrix_WeightAdd_Full(float * gpu_floats_a, float * gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_weightadd_full << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, nTrail, BATCHSIZE, batchsize, dimension, mweight, start, keep);
}

__global__ void cuda_matrix_weightadd_ex_full(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dimension && idy < batchsize)
	{
		for (int n = 0; n<nTrial; n++)
		{
			int col_end = inver_neg_index[n * BATCHSIZE + idy];
			int col_begin = 0;
			if (idy > 0)
			{
				col_begin = inver_neg_index[n * BATCHSIZE + idy - 1];
			}

			float sum = 0;
			for (int i = col_begin; i < col_end; i++)
			{
				int row = inver_neg_value[n * BATCHSIZE + i];
				sum += gpu_floats_b[n * BATCHSIZE * dimension + row * dimension + idx] * mweight[start + n * BATCHSIZE + row];
			}

			gpu_floats_a[idy*dimension + idx] += keep * sum;
		}
	}
}

void cuda_Matrix_WeightAdd_EX_Full(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_weightadd_ex_full << <block_tail, thread_tail >> >(gpu_floats_a, gpu_floats_b, inver_neg_index, inver_neg_value, nTrial, BATCHSIZE, batchsize, dimension, mweight, start, keep);
}



__global__ void cuda_cosine_similarity_subspace(float * a, float * b, float * c, int labelDim, int BATCHSIZE,
		int batchsize, int subspaceDim, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize && idy < labelDim)
	{
		float sumxx = 0;
		float sumyy = 0;
		float sumxy = 0;
		int id_start = idx * (labelDim * subspaceDim) + idy * subspaceDim;
		for (int i = 0; i < subspaceDim; i++)
		{
			sumxx += a[id_start + i] * a[id_start + i];
			sumyy += b[id_start + i] * b[id_start + i];
			sumxy += a[id_start + i] * b[id_start + i];
		}
		c[idx * labelDim + idy] = (float)(sumxy * 1.0f / (sqrtf((float)(sumxx * sumyy)) + eps));
	}

}
void cuda_Cosine_Similarity_SubSpace(float * a, float * b, float * c, int labelDim, int BATCHSIZE,
		int batchsize, int subspaceDim, float eps)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (labelDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_cosine_similarity_subspace << <block_tail, thread_tail >> >(a, b, c, labelDim, BATCHSIZE, batchsize, subspaceDim, eps);
}




__global__ void cuda_deriv_cosine_subspace(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize && idy < labelDim)
	{
		float alpha_v = gamma * alpha[idx * labelDim + idy];
		int id_start = idx * labelDim * subspaceDim + idy * subspaceDim;
		float a = 0;
		float b = eps;
		float c = eps;
		for (int i = 0; i < subspaceDim; i++)
		{
			a += q[id_start + i] * d[id_start + i];
			b += q[id_start + i] * q[id_start + i];
			c += d[id_start + i] * d[id_start + i];
		}
		b = sqrtf(b);
		c = sqrtf(c);

		/// tanh function.
		if (act_type == 0)
		{
			for (int i = 0; i < subspaceDim; i++)
			{
				dcq[id_start + i] = (float)((1 - q[id_start + i]) * (1 + q[id_start + i]) * (d[id_start + i] * 1.0f / (b*c) - q[id_start + i] * a * 1.0f / (b*b*b*c)));
				dcd[id_start + i] = (float)((1 - d[id_start + i]) * (1 + d[id_start + i]) * (q[id_start + i] * 1.0f / (b*c) - d[id_start + i] * a * 1.0f / (b*c*c*c)));
				dcq[id_start + i] = alpha_v * dcq[id_start + i]; //* 1.0f / batchsize;
				dcd[id_start + i] = alpha_v * dcd[id_start + i]; //* 1.0f / batchsize;
			}
		}
		/// linear function.
		else if (act_type == 1)
		{
			for (int i = 0; i < subspaceDim; i++)
			{
				dcq[id_start + i] = (float)((d[id_start + i] * 1.0f / (b*c) - q[id_start + i] * a * 1.0f / (b*b*b*c)));
				dcd[id_start + i] = (float)((q[id_start + i] * 1.0f / (b*c) - d[id_start + i] * a * 1.0f / (b*c*c*c)));
				dcq[id_start + i] = alpha_v * dcq[id_start + i]; //* 1.0f / batchsize;
				dcd[id_start + i] = alpha_v * dcd[id_start + i]; //* 1.0f / batchsize;
			}
		}
		/// 
		else if (act_type == 2)
		{
			for (int i = 0; i < subspaceDim; i++)
			{
				if (fabsf(q[id_start + i]) < eps)
				{
					dcq[id_start + i] = 0;
				}
				else
				{
					dcq[id_start + i] = (float)((d[id_start + i] * 1.0f / (b*c) - q[id_start + i] * a * 1.0f / (b*b*b*c)));
				}
				dcq[id_start + i] = alpha_v * dcq[id_start + i] * 1.0f; // / batchsize;

				if (fabsf(d[id_start + i]) < eps)
				{
					dcd[id_start + i] = 0;
				}
				else
				{
					dcd[id_start + i] = (float)((q[id_start + i] * 1.0f / (b*c) - d[id_start + i] * a * 1.0f / (b*c*c*c)));
				}
				dcd[id_start + i] = alpha_v * dcd[id_start + i] * 1.0f; /// batchsize;
			}
		}
	}
}

void cuda_Deriv_Cosine_Subspace(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps)
{
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (labelDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_cosine_subspace << < block_tail, thread_tail >> >(q, d, dcq, dcd, alpha, act_type, batchsize, labelDim, subspaceDim, gamma, eps);
}



__global__ void cuda_deriv_innerproduct(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		float alpha_v = gamma * alpha[idx];
		int id_start = idx * Dim;

		/// tanh function.
		if (act_type == 0)
		{
			for (int i = 0; i < Dim; i++)
			{
				dcq[id_start + i] = (float)((1 - q[id_start + i]) * (1 + q[id_start + i]) * d[id_start + i] * alpha_v * 1.0f);
				dcd[id_start + i] = (float)((1 - d[id_start + i]) * (1 + d[id_start + i]) * q[id_start + i] * alpha_v * 1.0f);
				//dcq[id_start + i] = alpha_v * dcq[id_start + i] ;
				//dcd[id_start + i] = alpha_v * dcd[id_start + i] ;
			}
		}
		/// linear function.
		else if (act_type == 1)
		{
			for (int i = 0; i < Dim; i++)
			{
				dcq[id_start + i] = (float)(d[id_start + i] * alpha_v * 1.0f);
				dcd[id_start + i] = (float)(q[id_start + i] * alpha_v * 1.0f);
				// dcq[id_start + i] = alpha_v * dcq[id_start + i] * 1.0f / batchsize;
				// dcd[id_start + i] = alpha_v * dcd[id_start + i] * 1.0f / batchsize;
			}
		}
		/// 
		else if (act_type == 2)
		{
			for (int i = 0; i < Dim; i++)
			{
				if (fabsf(q[id_start + i]) < eps)
				{
					dcq[id_start + i] = 0;
				}
				else
				{
					dcq[id_start + i] = (float)(d[id_start + i] * alpha_v * 1.0f);
				}


				if (fabsf(d[id_start + i]) < eps)
				{
					dcd[id_start + i] = 0;
				}
				else
				{
					dcd[id_start + i] = (float)(q[id_start + i] * alpha_v * 1.0f);
				}

			}
		}
	}
}

void cuda_Deriv_InnerProduct(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	//dim3 thread_tail(DEFAULT_THREAD_PER_DIM,DEFAULT_THREAD_PER_DIM);
	//dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, ( labelDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_deriv_innerproduct << < nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(q, d, dcq, dcd, alpha, act_type, batchsize, Dim, gamma, eps);
}



__global__ void cuda_sgd_train(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	/*if(idx == 0)
		L2Bias[0] = 10;*/
	//atomicAdd(L2Bias, 1);
	if (idx < BatchSize)
	{
		float *hidden = new float[HiddenNum], *hiddenPar = new float[HiddenNum];

		for (int k = 0; k < HiddenNum; k++)
		{
			hidden[k] = L1Bias[k];
		}
		int end = SampleIdx[idx];
		int start = idx == 0 ? 0 : SampleIdx[idx - 1];
		for (int j = start; j < end; j++)
		{
			int fid = FeatureIdx[j];
			float fval = FeatureValue[j];
			for (int k = 0; k < HiddenNum; k++)
			{
				hidden[k] += L1Weight[fid * HiddenNum + k] * fval;
			}
		}

		float output = L2Bias[0];
		for (int k = 0; k < HiddenNum; k++)
		{
			hidden[k] = 1.0f / (1.0f + expf(hidden[k]));
			output += hidden[k] * L2Weight[k];
		}
		output = 1.0f / (1.0f + expf(output));

		float dJdx = 2.0f * (output - Label[idx]) * output * (output - 1.0f);


		atomicAdd(L2Bias, -learnRate * dJdx);

		for (int k = 0; k < HiddenNum; k++)
		{
			atomicAdd(L2Weight + k, -learnRate * dJdx * hidden[k]);
			hiddenPar[k] = dJdx * L2Weight[k] * hidden[k] * (hidden[k] - 1.0f);
			atomicAdd(L1Bias + k, -learnRate * hiddenPar[k]);
		}

		for (int j = start; j < end; j++)
		{
			int fid = FeatureIdx[j];
			float fval = FeatureValue[j];
			for (int k = 0; k < HiddenNum; k++)
			{
				atomicAdd(L1Weight + fid * HiddenNum + k, -learnRate * hiddenPar[k] * fval);
			}
		}

		delete[] hidden;
		delete[] hiddenPar;
	}
}
void cuda_SGD_Train(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (BatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	//dim3 thread_tail(DEFAULT_THREAD_PER_DIM,DEFAULT_THREAD_PER_DIM);
	//dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, ( labelDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_sgd_train << < nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(SampleIdx, FeatureIdx, FeatureValue, Label, BatchSize, L1Weight, L1Bias, L2Weight, L2Bias, HiddenNum, learnRate);
	cudaDeviceSynchronize();
}



__global__ void cuda_predict(int* SampleIdx, int* FeatureIdx, float* FeatureValue, float * result, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < BatchSize)
	{
		float *hidden = new float[HiddenNum], *hiddenPar = new float[HiddenNum];

		for (int k = 0; k < HiddenNum; k++)
		{
			hidden[k] = L1Bias[k];
		}
		int end = SampleIdx[idx];
		int start = idx == 0 ? 0 : SampleIdx[idx - 1];
		for (int j = start; j < end; j++)
		{
			int fid = FeatureIdx[j];
			float fval = FeatureValue[j];
			for (int k = 0; k < HiddenNum; k++)
			{
				hidden[k] += L1Weight[fid * HiddenNum + k] * fval;
			}
		}

		float output = L2Bias[0];
		for (int k = 0; k < HiddenNum; k++)
		{
			hidden[k] = (tanhf(hidden[k] / 2.0f) + 1.0f) / 2.0f; //1.0f / (1.0f + expf(hidden[k]));
			output += hidden[k] * L2Weight[k];
		}
		output = (tanhf(output / 2.0f) + 1.0f) / 2.0f; // 1.0f / (1.0f + expf(output));
		result[idx] = output;

		delete[] hidden;
		delete[] hiddenPar;
	}
}

void cuda_Predict(int* SampleIdx, int* FeatureIdx, float* FeatureValue, float * result, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (BatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	//dim3 thread_tail(DEFAULT_THREAD_PER_DIM,DEFAULT_THREAD_PER_DIM);
	//dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, ( labelDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_predict << < nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(SampleIdx, FeatureIdx, FeatureValue, result, BatchSize, L1Weight, L1Bias, L2Weight, L2Bias, HiddenNum, learnRate);
}


__global__ void cuda_sgd_train_new(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	/*if(idx == 0)
		L2Bias[0] = 10;*/
	//atomicAdd(L2Bias, 1);
	if (idx < BatchSize)
	{
		float *hidden = new float[HiddenNum], *hiddenPar = new float[HiddenNum];

		for (int k = 0; k < HiddenNum; k++)
		{
			hidden[k] = L1Bias[k];
		}

		int end = SampleIdx[idx];
		int start = idx == 0 ? 0 : SampleIdx[idx - 1];
		for (int j = start; j < end; j++)
		{
			int fid = FeatureIdx[j];
			float fval = FeatureValue[j];
			for (int k = 0; k < HiddenNum; k++)
			{
				hidden[k] += L1Weight[fid * HiddenNum + k] * fval;
			}
		}

		float output = L2Bias[0];
		for (int k = 0; k < HiddenNum; k++)
		{
			hidden[k] = (tanhf(hidden[k] / 2.0f) + 1.0f) / 2.0f;
			output += hidden[k] * L2Weight[k];
		}
		output = (tanhf(output / 2.0f) + 1.0f) / 2.0f; //1.0f / (1.0f + expf(output));

		float dJdx = 2.0f * (Label[idx] - output);

		//atomicAdd(L2Bias, 1);

		atomicAdd(L2Bias, learnRate * dJdx);

		for (int k = 0; k < HiddenNum; k++)
		{
			hiddenPar[k] = dJdx * L2Weight[k] * hidden[k] * (1.0f - hidden[k]);
			atomicAdd(L2Weight + k, learnRate * dJdx * hidden[k]);
			atomicAdd(L1Bias + k, learnRate * hiddenPar[k]);
		}

		for (int j = start; j < end; j++)
		{
			int fid = FeatureIdx[j];
			float fval = FeatureValue[j];
			for (int k = 0; k < HiddenNum; k++)
			{
				atomicAdd(L1Weight + fid * HiddenNum + k, learnRate * hiddenPar[k] * fval);
			}
		}
		delete[] hidden;
		delete[] hiddenPar;
	}
}
void cuda_SGD_Train_New(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (BatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	//dim3 thread_tail(DEFAULT_THREAD_PER_DIM,DEFAULT_THREAD_PER_DIM);
	//dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, ( labelDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_sgd_train_new << < nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(SampleIdx, FeatureIdx, FeatureValue, Label, BatchSize, L1Weight, L1Bias, L2Weight, L2Bias, HiddenNum, learnRate);
	cudaDeviceSynchronize();
}




__global__ void cuda_seq_sparse_RBF_Kernel(int * Seg_Index, int seg_size, int * Fea_Index, float * Fea_Value, int elementsize,
		float * weight, float * bias, float * output, int Feature_dimension, int output_dimension,
		float * l2norm)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < output_dimension && idy < seg_size)
	{
		int seg_end = Seg_Index[idy];
		int seg_begin = 0;
		if (idy > 0)
		{
			seg_begin = Seg_Index[idy - 1];
		}

		float sum = l2norm[idx];
		for (int i = seg_begin; i < seg_end; ++i)
		{
			int fea_idx = Fea_Index[i];
			float v = Fea_Value[i] - 2 * weight[fea_idx * output_dimension + idx];
			sum += v * Fea_Value[i];
		}

		output[idy * output_dimension + idx] = sum * (bias[idx] + 0.001f);
	}
}

__global__ void L2Normalization(float * weight, int input, int output, float * l2norm)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output)
	{
		float sum = 0;
		for (int i = 0; i < input; i++)
		{
			sum = sum + weight[i * output + idx] * weight[i * output + idx];
		}
		l2norm[idx] = sum;
	}
}

void cuda_SEQ_Sparse_RBF_Kernel(int * Seg_Index, int seg_size, int * Fea_Index, float * Fea_Value, int elementsize,
		float * weight, float * bias, float * output, int Feature_dimension, int output_dimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (seg_size + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (output_dimension + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	float * gpu_floats;
	cudaMalloc((void **)&gpu_floats, output_dimension * sizeof(float));
	L2Normalization << <nBlockPerGrid, nThreadPerBlock >> >(weight, Feature_dimension, output_dimension, gpu_floats);

	cuda_seq_sparse_RBF_Kernel << <block_tail, thread_tail >> >(Seg_Index, seg_size, Fea_Index, Fea_Value, elementsize,
			weight, bias, output, Feature_dimension, output_dimension, gpu_floats);

	cudaFree(gpu_floats);
}

__global__ void cuda_matrix_mask(float * gpu_floats_a, int * gpu_mask, int batchsize, int dimension)
{
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < dimension && idy < batchsize)
	{
		if (gpu_mask[idx] == 0)
			gpu_floats_a[idy * dimension + idx] = 0;
	}
}

void cuda_Matrix_Mask(float * gpu_floats_a, int * gpu_mask, int batchsize, int dimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_matrix_mask << <block_tail, thread_tail >> >(gpu_floats_a, gpu_mask, batchsize, dimension);
}


__global__ void cuda_convolution_sparse_matrix_product_INTEX_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output_dimension * win_size)
	{
		int output_idx = idx % output_dimension;
		int win_idx = idx / output_dimension;

		//float sum = 0;
		for (int b = 0; b < batchsize; b++)
		{
			int target_seg = maxpooling_index[b * output_dimension + output_idx];

			if (target_seg == -1) continue;

			int target_smp = SegMargin_Index[target_seg];
			//deriv[i * output_dimension + idx] *  
			int ws = win_size / 2;
			int w = win_idx - ws;
			int row = target_seg + w; // idx / n;
			if (row >= 0 && row < seg_size && SegMargin_Index[row] == target_smp)
			{
				int col_end = Seg_Index[row];
				int col_begin = row == 0 ? 0 : Seg_Index[row - 1];
				float dv = deriv[b * output_dimension + output_idx];
				float * pGrad = grad + win_idx*Feature_Dimension * output_dimension + output_idx;
				for (int i = col_begin; i < col_end; i++)
				{
					int fea_idx = Fea_Index[i];
					if (fea_idx >= Feature_Dimension) continue;
					float m = Fea_Value[i] * dv;
					pGrad[fea_idx * output_dimension] += m * learnRate;
				}
			}
		}

	}
}

void cuda_Convolution_Sparse_Matrix_Product_INTEX_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((output_dimension * win_size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK);

	cuda_convolution_sparse_matrix_product_INTEX_Weight << <block_tail, thread_tail >> >(deriv, maxpooling_index, Seg_Index, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, Fea_Index, Fea_Value, grad, Feature_Dimension, learnRate);
	//,alpha, ntrial, BATCH_SIZE, alpha_index); 
}




__global__ void cuda_cosine_similarity_matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float * aa, float * bb, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < matchSize)
	{
		int aID = matchIdxA[idx];
		int bID = matchIdxB[idx];
		float sumxx = aa[aID];
		float sumyy = bb[bID];
		float sumxy = 0;
		aID = aID * dimension;
		bID = bID * dimension;
		for (int i = 0; i < dimension; i++)
		{
			sumxy += a[aID + i] * b[bID + i];
		}
		c[idx] = (float)(sumxy * 1.0f / ((float)(sumxx * sumyy) + eps));
	}

}
void cuda_Cosine_Similarity_Matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float * aa, float * bb, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (matchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_cosine_similarity_matching << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, b, c, matchIdxA, matchIdxB, aSize, bSize, 
			matchSize, dimension, aa, bb, eps);
}



__global__ void cuda_inner_product_matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < matchSize)
	{
		int aID = matchIdxA == NULL ? idx : matchIdxA[idx];
		int bID = matchIdxB == NULL ? idx : matchIdxB[idx];
		float sumxy = 0;
		aID = aID * dimension;
		bID = bID * dimension;
		for (int i = 0; i < dimension; i++)
		{
			sumxy += a[aID + i] * b[bID + i];
		}
		c[idx] = (float)(sumxy * 1.0f);
	}
}

void cuda_Inner_Product_Matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (matchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_inner_product_matching << <nBlockPerGrid, nThreadPerBlock >> >(a, b, c, matchIdxA, matchIdxB, aSize, bSize, 
			matchSize, dimension, eps);
}


__global__ void cuda_inner_product_v1(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float alpha, float beta)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < matchSize)
	{
		int aID = matchIdxA == NULL ? idx : matchIdxA[idx];
		int bID = matchIdxB == NULL ? idx : matchIdxB[idx];
		float sumxy = 0;
		aID = aID * dimension;
		bID = bID * dimension;
		for (int i = 0; i < dimension; i++)
		{
			sumxy += a[aID + i] * b[bID + i];
		}
		c[idx] = alpha * c[idx] + beta * sumxy; 
	}
}


void cuda_Inner_Product_v1(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float alpha, float beta)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (matchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_inner_product_v1 << <nBlockPerGrid, nThreadPerBlock >> >(a, b, c, matchIdxA, matchIdxB, aSize, bSize, matchSize, dimension, alpha, beta);
}


__global__ void cuda_square_matrix(float * a, int batchSize, int dim, float * aSquare)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int aID = idx * dim;
		float sumxx = 0;
		for (int i = 0; i < dim; i++)
		{
			sumxx += a[aID + i] * a[aID + i];
		}
		aSquare[idx] = sqrt(sumxx);
	}
}

void cuda_Square_Matrix(float * a, int batchSize, int dim, float * aSquare)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_square_matrix << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(a, batchSize, dim, aSquare);
}


__global__ void cuda_joint_sparse_matrix_multiply_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		int * ConSmp_Idx, int * ConFea_Idx, float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * Weight, float * Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < OutputDimension && idy < MatchSize)
	{
		float sum = 0;
		int FeaDim = 0;

		if (IsSim > 0)
		{
			sum += Match_Score[idy] * Weight[idx];
			FeaDim += 1;
		}

		if (IsSrc > 0)
		{
			int aIdx = Src_Match_Idx[idy] * SrcDimension;
			for (int i = 0; i < SrcDimension; i++)
				sum += Src_Input[aIdx + i] * Weight[(i + FeaDim) * OutputDimension + idx];
			FeaDim += SrcDimension;
		}

		if (IsTgt > 0)
		{
			int bIdx = Tgt_Match_Idx[idy] * TgtDimension;
			for (int i = 0; i<TgtDimension; i++)
				sum += Tgt_Input[bIdx + i] * Weight[(i + FeaDim) * OutputDimension + idx];
			FeaDim += TgtDimension;
		}

		if (IsConScore > 0)
		{
			int fea_end = ConSmp_Idx[idy];
			int fea_begin = idy > 0 ? ConSmp_Idx[idy - 1] : 0;
			for (int i = fea_begin; i < fea_end; ++i)
			{
				int fea_idx = ConFea_Idx[i];
				sum += ConFea_Value[i] * Weight[(FeaDim + fea_idx) * OutputDimension + idx];
			}
			FeaDim += ConDimension;
		}

		Output[idy * OutputDimension + idx] = sum;
	}
}

void cuda_Joint_Sparse_Matrix_Multiply_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		int * ConSmp_Idx, int * ConFea_Idx, float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * Weight, float * Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((OutputDimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (MatchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_joint_sparse_matrix_multiply_INTEX << <block_tail, thread_tail >> >(Src_Input, SrcSize, Tgt_Input, TgtSize,
			Src_Match_Idx, Tgt_Match_Idx, Match_Score, MatchSize,
			ConSmp_Idx, ConFea_Idx, ConFea_Value, SrcDimension, TgtDimension, ConDimension, IsConScore,
			Weight, Output, OutputDimension, IsSrc, IsTgt, IsSim);
}



__global__ void cuda_joint_dense_matrix_multiply_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * Weight, float * Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < OutputDimension && idy < MatchSize)
	{
		float sum = 0;
		int FeaDim = 0;

		if (IsSim > 0)
		{
			sum += Match_Score[idy] * Weight[idx];
			FeaDim += 1;
		}

		if (IsSrc > 0)
		{
			int aIdx = Src_Match_Idx[idy] * SrcDimension;
			for (int i = 0; i < SrcDimension; i++)
				sum += Src_Input[aIdx + i] * Weight[(i + FeaDim) * OutputDimension + idx];
			FeaDim += SrcDimension;
		}

		if (IsTgt > 0)
		{
			int bIdx = Tgt_Match_Idx[idy] * TgtDimension;
			for (int i = 0; i<TgtDimension; i++)
				sum += Tgt_Input[bIdx + i] * Weight[(i + FeaDim) * OutputDimension + idx];
			FeaDim += TgtDimension;
		}

		if (IsConScore > 0)
		{
			int fea_begin = idy * ConDimension;
			for (int i = 0; i < ConDimension; i++)
			{
				sum += ConFea_Value[fea_begin + i] * Weight[(FeaDim + i) * OutputDimension + idx];
			}
			FeaDim += ConDimension;
		}

		Output[idy * OutputDimension + idx] = sum;
	}
}

void cuda_Joint_Dense_Matrix_Multiply_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * Weight, float * Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((OutputDimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (MatchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_joint_dense_matrix_multiply_INTEX << <block_tail, thread_tail >> >(Src_Input, SrcSize, Tgt_Input, TgtSize,
			Src_Match_Idx, Tgt_Match_Idx, Match_Score, MatchSize,
			ConFea_Value, 
			SrcDimension, TgtDimension, ConDimension, IsConScore,
			Weight, Output, OutputDimension, IsSrc, IsTgt, IsSim);
}


__global__ void cuda_joint_backward_sim(
		float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * simDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < MatchSize)
	{
		float sum = 0;
		for (int i = 0; i < outputDim; i++)
			sum += outputDeriv[idx * outputDim + i] * Weight[i];
		simDeriv[idx] = sum;
	}
}

void cuda_Joint_Backward_Sim(
		float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * simDeriv)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (MatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_joint_backward_sim << <nBlockPerGrid, nThreadPerBlock >> >(
			outputDeriv, MatchSize, outputDim, Weight, simDeriv);
}


__global__ void cuda_joint_backward_src(
		float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * SimDeriv, 
		int * matchSrc, int * matchTgt,
		float * Src_Input, int SrcSize, int SrcDim,
		float * Tgt_Input, int TgtSize, int TgtDim,
		float * Sim, float * aa, float * bb, 
		int IsSrc, int IsTgt, int IsSim,
		int * SrcMatchIndex, int * SrcMatchElement,
		int * TgtMatchIndex, int * TgtMatchElement,
		float * Src_Deriv, float * Tgt_Deriv, float eps, int SimType)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < SrcDim && idy < SrcSize)
	{
		int matchEnd = SrcMatchIndex[idy];
		int matchBegin = idy > 0 ? SrcMatchIndex[idy - 1] : 0;

		int srcIdx = idy * SrcDim; 

		for (int m = matchBegin; m < matchEnd; m++)
		{
			int mid = SrcMatchElement[m];

			int fid = 0;
			if (IsSim > 0)
			{
				int tid = matchTgt[mid];
				int tgtIdx = tid * TgtDim;

				if (SimType == 0)
				{
					float b = aa[idy];
					float c = bb[tid];
					float a = Sim[mid] * (b * c);

					float bc = b * c + eps;
					float bbbc = b*b*b*c + eps;

					Src_Deriv[srcIdx + idx] += (float)(SimDeriv[mid] * (Tgt_Input[tgtIdx + idx] * 1.0f / (bc)-Src_Input[srcIdx + idx] * a * 1.0f / (bbbc)));
				}
				else if (SimType == 1)
				{
					Src_Deriv[srcIdx + idx] += (float)(SimDeriv[mid] * Tgt_Input[tgtIdx + idx]);
				}
				fid += 1;
			}
			if (IsSrc > 0)
			{
				int mIdx = mid * outputDim;
				for (int i = 0; i < outputDim; i++)
				{
					Src_Deriv[srcIdx + idx] += outputDeriv[mIdx + i] * Weight[(fid + idx) * outputDim + i];
				}
			}
		}
	}
}

__global__ void cuda_joint_backward_tgt(
		float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * SimDeriv, 
		int * matchSrc, int * matchTgt,
		float * Src_Input, int SrcSize, int SrcDim,
		float * Tgt_Input, int TgtSize, int TgtDim,
		float * Sim, float * aa, float * bb, 
		int IsSrc, int IsTgt, int IsSim,
		int * SrcMatchIndex, int * SrcMatchElement,
		int * TgtMatchIndex, int * TgtMatchElement,
		float * Src_Deriv, float * Tgt_Deriv, float eps, int SimType)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < TgtDim && idy < TgtSize)
	{
		int matchEnd = TgtMatchIndex[idy];
		int matchBegin = idy > 0 ? TgtMatchIndex[idy - 1] : 0;

		int tgtIdx = idy * TgtDim; 
		for (int m = matchBegin; m < matchEnd; m++)
		{
			int mid = TgtMatchElement[m];

			int fid = 0;
			if (IsSim > 0)
			{
				int sid = matchSrc[mid];
				int srcIdx = sid * SrcDim;
				if (SimType == 0)
				{
					float b = aa[sid];
					float c = bb[idy];
					float a = Sim[mid] * (b * c);

					float bc = b * c + eps;
					float bccc = b*c*c*c + eps;

					Tgt_Deriv[tgtIdx + idx] += (float)(SimDeriv[mid] * (Src_Input[srcIdx + idx] * 1.0f / (bc)-Tgt_Input[tgtIdx + idx] * a * 1.0f / (bccc)));
				}
				else if (SimType == 1)
				{
					Tgt_Deriv[tgtIdx + idx] += (float)(SimDeriv[mid] * Src_Input[srcIdx + idx]);
				}
				fid += 1;
			}
			if (IsSrc > 0)
			{
				fid += SrcDim;
			}
			if (IsTgt > 0)
			{
				int mIdx = mid * outputDim;
				for (int i = 0; i < outputDim; i++)
				{
					Tgt_Deriv[tgtIdx + idx] += outputDeriv[mIdx + i] * Weight[(fid + idx) * outputDim + i];
				}
			}
		}
	}
}

void cuda_Joint_Backward_Src_Tgt(
		float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * SimDeriv, 
		int * matchSrc, int * matchTgt,
		float * Src_Input, int SrcSize, int SrcDim,
		float * Tgt_Input, int TgtSize, int TgtDim,
		float * Sim, float * aa, float * bb, 
		int IsSrc, int IsTgt, int IsSim,
		int * SrcMatchIndex, int * SrcMatchElement,
		int * TgtMatchIndex, int * TgtMatchElement,
		float * Src_Deriv, float * Tgt_Deriv, float eps, int SimType)
{
	if (IsSim > 0 || IsSrc > 0)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((SrcDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (SrcSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

		cuda_joint_backward_src << <block_tail, thread_tail >> >(
				outputDeriv, MatchSize, outputDim, 
				Weight, SimDeriv,
				matchSrc, matchTgt,
				Src_Input, SrcSize, SrcDim,
				Tgt_Input, TgtSize, TgtDim,
				Sim, aa, bb,
				IsSrc, IsTgt, IsSim,
				SrcMatchIndex, SrcMatchElement,
				TgtMatchIndex, TgtMatchElement,
				Src_Deriv, Tgt_Deriv, eps, SimType);
	}
	if (IsSim > 0 || IsTgt > 0)
	{
		dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
		dim3 block_tail((TgtDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (TgtSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
		cuda_joint_backward_tgt << <block_tail, thread_tail >> >(
				outputDeriv, MatchSize, outputDim, 
				Weight, SimDeriv,
				matchSrc, matchTgt,
				Src_Input, SrcSize, SrcDim,
				Tgt_Input, TgtSize, TgtDim,
				Sim, aa, bb,
				IsSrc, IsTgt, IsSim,
				SrcMatchIndex, SrcMatchElement,
				TgtMatchIndex, TgtMatchElement,
				Src_Deriv, Tgt_Deriv, eps, SimType);
	}
}




__global__ void cuda_sparseforward(int * inputSmpIdx, int * inputItemIdx, int batchSize, float * weight, float * output, int inputDim, int outputDim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < outputDim && idy < batchSize)
	{
		int input_end = inputSmpIdx == NULL ? (idy + 1) : inputSmpIdx[idy];
		int input_begin = inputSmpIdx == NULL ? idy : ( idy > 0 ? inputSmpIdx[idy - 1] : 0 );

		float sum = 0;
		for (int word_idx = input_begin; word_idx < input_end; ++word_idx)
		{
			int fea_idx = inputItemIdx[word_idx];
			sum += weight[fea_idx * outputDim + idx];
		}
		output[idy * outputDim + idx] = sum;
	}
}

void cuda_SparseForward(int * inputSmpIdx, int * inputItemIdx, int batchSize, float * weight, float * output, int inputDim, int outputDim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((outputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	
	cuda_sparseforward << <block_tail, thread_tail >> >(inputSmpIdx, inputItemIdx, batchSize, weight, output, inputDim, outputDim);
}



__global__ void cuda_sparseL0(int * outputSmpIdx, int * outputItemIdx, int batchSize, int itemNum, float * L0, float * output, int inputDim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < itemNum)
	{
		int id = outputItemIdx[idx];
		output[idx] = output[idx] + L0[id];
	}
}

void cuda_SparseL0(int * outputSmpIdx, int * outputItemIdx, int batchSize, int itemNum, float * L0, float * output, int inputDim)
{
	//int thread_tail = DEFAULT_THREAD_PER_BLOCK;
	//dim3 block_tail((outputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (itemNum + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparseL0 << <nBlockPerGrid, nThreadPerBlock >> >(outputSmpIdx, outputItemIdx, batchSize, itemNum, L0, output, inputDim);
}


__global__ void cuda_sparseL0full(int batchSize, float * L0, float * output, int inputDim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < inputDim)
	{
		output[idx * inputDim + idy] += L0[idy];
	}
}

//Data.BatchSize, L0.CudaPtr, Data.OutputScore.CudaPtr, ItemNum);
void cuda_SparseL0Full(int batchSize, float * L0, float * output, int inputDim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (inputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_sparseL0full << <block_tail, thread_tail >> >(batchSize, L0, output, inputDim);
}

//Data.OutputSampleIndex.CudaPtr, Data.OutputItemIndex.CudaPtr, Data.OutputScore.CudaPtr, 
//Data.BatchSize, Data.OutputItemNum, L0.CudaPtr, ItemNum

__global__ void cuda_sparseL0update(int * outputSmpIdx, int * outputItemIdx, float * outputScore, int batchSize, int outputItemNum, float * L0, int inputDim, float lr)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < outputItemNum)
	{
		int id = outputItemIdx[idx];
		atomicAdd(L0 + id, outputScore[idx] * lr);
	}
}

void cuda_SparseL0Update(int * outputSmpIdx, int * outputItemIdx, float * outputScore, int batchSize, int outputItemNum, float * L0, int inputDim, float lr)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputItemNum + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparseL0update << <nBlockPerGrid, nThreadPerBlock >> >(outputSmpIdx, outputItemIdx, outputScore, batchSize, outputItemNum, L0, inputDim, lr);
}

__global__ void cuda_sparseL1(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx, int * outputItemMapSmp, int batchSize, int inputItemNum, int outputItemNum,
		float * L1, float * output, int inputDim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < outputItemNum)
	{
		int smpId = outputItemMapSmp[idx];
		int outputId = outputItemIdx[idx];

		int input_end = inputSmpIdx[smpId];
		int input_begin = smpId > 0 ? inputSmpIdx[smpId - 1] : 0;

		float sum = 0;
		for (int word_idx = input_begin; word_idx < input_end; ++word_idx)
		{
			int inputId = inputItemIdx[word_idx];
			sum += L1[inputId * inputDim + outputId];
		}
		output[idx] = output[idx] + sum;
	}
}

void cuda_SparseL1(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx, int * outputItemMapSmp, int batchSize, int inputItemNum, int outputItemNum,
		float * L1, float * output, int inputDim)
{
	//int thread_tail = DEFAULT_THREAD_PER_BLOCK;
	//dim3 block_tail((outputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputItemNum + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparseL1 << <nBlockPerGrid, nThreadPerBlock >> >(inputSmpIdx, inputItemIdx, outputSmpIdx, outputItemIdx, outputItemMapSmp, batchSize, inputItemNum, outputItemNum, L1, output, inputDim);
}




__global__ void cuda_sparseL1update(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx,
		int * outputItemMapSmp, float * outputScore, int batchSize, int inputItemNum, int outputItemNum, float * L1, int inputDim, float lr)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < outputItemNum)
	{
		int smpId = outputItemMapSmp[idx];
		int outputId = outputItemIdx[idx];
		float deriv = outputScore[idx] * lr;

		int input_end = inputSmpIdx[smpId];
		int input_begin = smpId > 0 ? inputSmpIdx[smpId - 1] : 0;

		for (int word_idx = input_begin; word_idx < input_end; ++word_idx)
		{
			int inputId = inputItemIdx[word_idx];
			atomicAdd(L1 + inputId * inputDim + outputId, deriv);
		}
	}
}

void cuda_SparseL1Update(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx, 
		int * outputItemMapSmp, float * outputScore, int batchSize, int inputItemNum, int outputItemNum, float * L1, int inputDim, float lr)
{
	//int thread_tail = DEFAULT_THREAD_PER_BLOCK;
	//dim3 block_tail((outputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputItemNum + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparseL1update << <nBlockPerGrid, nThreadPerBlock >> >(inputSmpIdx, inputItemIdx, outputSmpIdx, outputItemIdx, 
			outputItemMapSmp, outputScore, batchSize, inputItemNum, outputItemNum, L1, inputDim, lr);
}

__global__ void cuda_sparseLk(float * hidden, int hiddenDim, int * outputSmpIdx, int * outputItemIdx, int batchSize, int outputItemNum, int * outputItemMapSmp,
		float * Lk, float * output)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < outputItemNum)
	{
		int smpId = outputItemMapSmp[idx] * hiddenDim;
		int outputId = outputItemIdx[idx] * hiddenDim;

		float sum = 0;
		for (int h = 0; h < hiddenDim; ++h)
		{
			sum += hidden[smpId + h] * Lk[outputId + h];
		}
		output[idx] = output[idx] + sum;
	}
}

void cuda_SparseLk(float * hidden, int hiddenDim, int * outputSmpIdx, int * outputItemIdx, int batchSize, int outputItemNum, int * outputItemMapSmp,
		float * Lk, float * output)
{
	//int thread_tail = DEFAULT_THREAD_PER_BLOCK;
	//dim3 block_tail((outputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputItemNum + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparseLk << <nBlockPerGrid, nThreadPerBlock >> >(hidden, hiddenDim, outputSmpIdx, outputItemIdx, batchSize, outputItemNum, outputItemMapSmp, Lk, output);
}




__global__ void cuda_regressionloss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < outputSize)
	{
		outputDeriv[idx] = 2 * (outputLabel[idx] - outputScore[idx]);
	}
}

void cuda_RegressionLoss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_regressionloss << <nBlockPerGrid, nThreadPerBlock >> >(outputScore, outputLabel, outputDeriv, outputSize);
}


__global__ void cuda_pairwiserankingloss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < srcBatchSize)
	{
		int eleEnd = src2MatchIdx[idx];
		int eleBegin = idx > 0 ? src2MatchIdx[idx - 1] : 0;

		for (int i = eleBegin; i < eleEnd; i++)
		{
			outputDeriv[src2MatchElement[i]] = 0;
		}
		srcBatchLoss[idx] = 0;

		int pairNum = 0;
		for (int i = eleBegin; i < eleEnd; i++)
		{
			for (int j = i + 1; j < eleEnd; j++)
			{
				int matchI = src2MatchElement[i];
				int matchJ = src2MatchElement[j];

				if (outputLabel[matchI] > outputLabel[matchJ])
				{
					float score = (tanhf((outputScore[matchI] - outputScore[matchJ]) / 2) + 1) / 2.0f;
					outputDeriv[matchI] += 1 - score;
					outputDeriv[matchJ] += score - 1;
					srcBatchLoss[idx] += logf(score + eplison);
					pairNum += 1;
				}
				else if (outputLabel[matchJ] > outputLabel[matchI])
				{
					float score = (tanhf((outputScore[matchJ] - outputScore[matchI]) / 2) + 1) / 2.0f;
					outputDeriv[matchJ] += 1 - score;
					outputDeriv[matchI] += score - 1;
					srcBatchLoss[idx] += logf(score + eplison);
					pairNum += 1;
				}
			}
		}

		/*if(pairNum > 0)
			{
			for(int i = eleBegin; i < eleEnd; i++)
			{
			int matchI = src2MatchElement[i];
			outputDeriv[matchI] = outputDeriv[matchI] / pairNum ;
			}
			srcBatchLoss[idx] = srcBatchLoss[idx] / pairNum;
			}*/

	}
}

void cuda_PairwiseRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (srcBatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_pairwiserankingloss << <nBlockPerGrid, nThreadPerBlock >> >(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison);
}


__global__ void cuda_listwiserankingloss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < srcBatchSize)
	{
		int eleEnd = src2MatchIdx[idx];
		int eleBegin = idx > 0 ? src2MatchIdx[idx - 1] : 0;

		for (int i = eleBegin; i < eleEnd; i++)
		{
			outputDeriv[src2MatchElement[i]] = 0;
		}
		srcBatchLoss[idx] = 0;

		int listNum = eleEnd - eleBegin;
		for (int i = eleBegin; i < eleEnd; i++)
		{
			int matchI = src2MatchElement[i];
			float logSum = outputScore[matchI] * gamma;
			for (int j = eleBegin; j < eleEnd; j++)
			{
				int matchJ = src2MatchElement[j];
				if (matchI == matchJ || outputLabel[matchJ] >= outputLabel[matchI])
					continue;
				if (outputLabel[matchJ] < outputLabel[matchI])
				{
					float tmpScore = outputScore[matchJ] * gamma;
					if (logSum >= tmpScore)
					{
						logSum = logSum + logf(1 + expf(tmpScore - logSum));
					}
					else
					{
						logSum = tmpScore + logf(1 + expf(logSum - tmpScore));
					}
				}
			}

			outputDeriv[matchI] += (1 - expf(outputScore[matchI] * gamma - logSum)) * gamma;

			for (int j = eleBegin; j < eleEnd; j++)
			{
				int matchJ = src2MatchElement[j];
				if (matchI == matchJ || outputLabel[matchJ] >= outputLabel[matchI])
					continue;

				if (outputLabel[matchJ] < outputLabel[matchI])
				{
					outputDeriv[matchJ] += -expf(outputScore[matchJ] * gamma - logSum) * gamma;
				}
			}
			srcBatchLoss[idx] += (outputScore[matchI] * gamma - logSum);
		}

	}
}

void cuda_ListwiseRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (srcBatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_listwiserankingloss << <nBlockPerGrid, nThreadPerBlock >> >(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison, gamma);
}


__global__ void cuda_bayesianratingloss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < srcBatchSize)
	{
		int eleEnd = src2MatchIdx[idx];
		int eleBegin = idx > 0 ? src2MatchIdx[idx - 1] : 0;

		for (int i = eleBegin; i < eleEnd; i++)
		{
			outputDeriv[src2MatchElement[i]] = 0;
		}

		int listNum = eleEnd - eleBegin;
		float pure_sum = 0;
		float rate_sum = 0;
		for (int i = eleBegin; i < eleEnd; i++)
		{
			int matchI = src2MatchElement[i];
			float tmpScore = expf(gamma * outputScore[matchI]);
			float rateTmpScore = outputLabel[matchI] * tmpScore;
			pure_sum += tmpScore;
			rate_sum += rateTmpScore;
		}
		float avgRating = rate_sum / (pure_sum + eplison);

		srcBatchLoss[idx] = avgRating;
		for (int i = eleBegin; i < eleEnd; i++)
		{
			int matchI = src2MatchElement[i];
			float tmpScore = expf(gamma * outputScore[matchI]);
			outputDeriv[matchI] = gamma * tmpScore / (pure_sum + eplison) * (outputLabel[matchI] - avgRating); 
		}
	}
}

void cuda_BayesianRatingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (srcBatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_bayesianratingloss << <nBlockPerGrid, nThreadPerBlock >> >(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison, gamma);
}





__global__ void cuda_lambdarankingloss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, int K)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < srcBatchSize)
	{
		int eleEnd = src2MatchIdx[idx];
		int eleBegin = idx > 0 ? src2MatchIdx[idx - 1] : 0;

		if (eleEnd > eleBegin)
		{
			int *labelSort = new int[eleEnd - eleBegin], *scoreSort = new int[eleEnd - eleBegin], *tmpArray = new int[eleEnd - eleBegin];
			for (int i = eleBegin; i < eleEnd; i++)
				outputDeriv[i] = 0;
			srcBatchLoss[idx] = 0;

			for (int i = 0; i < eleEnd - eleBegin; i++) tmpArray[i] = 0;
			for (int i = 0; i < eleEnd - eleBegin; i++)
			{
				float maxScore = 0;
				int maxIndex = -1;
				for (int j = eleBegin; j < eleEnd; j++)
				{
					if (tmpArray[j - eleBegin] > 0) continue;

					int matchJ = src2MatchElement[j];
					if (maxIndex == -1 || outputScore[matchJ] >= maxScore)
					{
						maxScore = outputScore[matchJ];
						maxIndex = j;
					}
				}
				scoreSort[maxIndex - eleBegin] = i + 1;
				tmpArray[maxIndex - eleBegin] = 1;
			}

			for (int i = 0; i < eleEnd - eleBegin; i++) tmpArray[i] = 0;
			for (int i = 0; i < eleEnd - eleBegin; i++)
			{
				float maxScore = 0;
				int maxIndex = -1;
				for (int j = eleBegin; j < eleEnd; j++)
				{
					if (tmpArray[j - eleBegin] > 0) continue;

					int matchJ = src2MatchElement[j];
					if (maxIndex == -1 || outputLabel[matchJ] >= maxScore)
					{
						maxScore = outputLabel[matchJ];
						maxIndex = j;
					}
				}
				labelSort[i] = maxScore;
				tmpArray[maxIndex - eleBegin] = 1;
			}

			float dcg = eplison;
			for (int i = 0; i < eleEnd - eleBegin; i++)
			{
				dcg += (powf(2, labelSort[i]) - 1) / log2f(2 + i);
				break;
				//if(i >= K -1) break;
			}

			int pairNum = 0;
			for (int i = eleBegin; i < eleEnd; i++)
			{
				for (int j = i + 1; j < eleEnd; j++)
				{
					int matchI = src2MatchElement[i];
					int matchJ = src2MatchElement[j];

					if (outputLabel[matchI] > outputLabel[matchJ] + eplison)
					{
						float score = (tanhf((outputScore[matchI] - outputScore[matchJ]) / 2) + 1) / 2.0f;
						float ndcgDelta = fabsf((powf(2, outputLabel[matchI]) - powf(2, outputLabel[matchJ])) *
								(1.0f / log2f(1 + scoreSort[i - eleBegin]) - 1.0f / log2f(1 + scoreSort[j - eleBegin]))) / dcg;
						outputDeriv[matchI] += (1 - score) *  ndcgDelta;
						outputDeriv[matchJ] += (score - 1) *  ndcgDelta;
						srcBatchLoss[idx] += logf(score + eplison);
						pairNum += 1;
					}
					else if (outputLabel[matchJ] > outputLabel[matchI] + eplison)
					{
						float score = (tanhf((outputScore[matchJ] - outputScore[matchI]) / 2) + 1) / 2.0f;
						float ndcgDelta = fabsf((powf(2, outputLabel[matchJ]) - powf(2, outputLabel[matchI])) *
								(1.0f / log2f(1 + scoreSort[j - eleBegin]) - 1.0f / log2f(1 + scoreSort[i - eleBegin]))) / dcg;
						outputDeriv[matchJ] += (1 - score) * ndcgDelta;
						outputDeriv[matchI] += (score - 1) * ndcgDelta;
						srcBatchLoss[idx] += logf(score + eplison);
						pairNum += 1;
					}
				}
			}

			delete[] labelSort;
			delete[] scoreSort;
			delete[] tmpArray;
		}
	}
}

void cuda_LambdaRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, int K)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (srcBatchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_lambdarankingloss << <nBlockPerGrid, nThreadPerBlock >> >(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison, K);
}


__global__ void cuda_sparse_matrix_multiply_INTEX_weight(int * outputSmpIndex, int * outputItemIndex, int batchsize,
		int outputItemNum, float * weight, float * hidden_deriv, int hiddenDim, float wei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchsize && idy < hiddenDim)
	{
		int col_end = outputSmpIndex[idx];
		int col_begin = idx > 0 ? outputSmpIndex[idx - 1] : 0;

		float sum = 0;
		for (int i = col_begin; i < col_end; ++i)
		{
			int inputIdx = outputItemIndex[i];
			sum += weight[inputIdx * hiddenDim + idy];
		}

		hidden_deriv[idx * hiddenDim + idy] = wei * hidden_deriv[idx * hiddenDim + idy] + sum;
	}
}

__global__ void cuda_sparse_matrix_multiply_INTEX_weight(int * outputSmpIndex, int * outputItemIndex, float * outputDeriv, int batchsize,
		int outputItemNum, float * weight, float * hidden_deriv, int hiddenDim, float wei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchsize && idy < hiddenDim)
	{
		int col_end = outputSmpIndex[idx];
		int col_begin = idx > 0 ? outputSmpIndex[idx - 1] : 0;
		float sum = 0;
		for (int i = col_begin; i < col_end; ++i)
		{
			int inputIdx = outputItemIndex[i];
			float deriv = outputDeriv[i];
			sum += deriv * weight[inputIdx * hiddenDim + idy];
		}
		hidden_deriv[idx * hiddenDim + idy] = wei * hidden_deriv[idx * hiddenDim + idy] + sum;
	}
}

//Data.OutputSampleIndex.CudaPtr, Data.OutputItemIndex.CudaPtr, Data.OutputScore.CudaPtr,
//                        Data.BatchSize, Data.OutputItemNum, Lk[i].CudaPtr, DnnRun.neurallayers[i + 1].ErrorDeriv.CudaPtr, DnnRun.neurallayers[i + 1].Number, 0

void cuda_Sparse_Matrix_Multiply_INTEX_Weight(int * outputSmpIndex, int * outputItemIndex, float * outputDeriv, int batchsize, 
		int outputItemNum, float * weight, float * hidden_deriv, int hiddenDim, float wei)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	if (outputDeriv == 0)
	{
		cuda_sparse_matrix_multiply_INTEX_weight << <block_tail, thread_tail >> >(outputSmpIndex, outputItemIndex, 
				batchsize, outputItemNum, weight, hidden_deriv, hiddenDim, wei);
	}
	else
	{
		cuda_sparse_matrix_multiply_INTEX_weight << <block_tail, thread_tail >> >(outputSmpIndex, outputItemIndex, outputDeriv,
				batchsize, outputItemNum, weight, hidden_deriv, hiddenDim, wei);
	}
}



__global__ void cuda_sparsebackward(int * inputSmpIdx, int * inputItemIdx, int batchSize, int inputItemNum,
		float * deriv, int inputDim, int outputDim, float * output, float lr)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < outputDim) 
	{
		for (int i = 0; i<batchSize; i++)
		{
			int input_end = inputSmpIdx == NULL ? i + 1 : inputSmpIdx[i];
			int input_begin = inputSmpIdx == NULL ? i : (i > 0 ? inputSmpIdx[i - 1] : 0);
			float value = lr * deriv[i * outputDim + idx];
			for (int word_idx = input_begin; word_idx < input_end; ++word_idx)
			{
				int fea_idx = inputItemIdx[word_idx];
				output[fea_idx * outputDim + idx] += value;
			}
		}
	}
}

void cuda_SparseBackward(int * inputSmpIdx, int * inputItemIdx, int batchSize, int inputItemNum,
		float * deriv, int inputDim, int outputDim, float * output, float lr)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (outputDim + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparsebackward << < nBlockPerGrid, nThreadPerBlock >> >(inputSmpIdx, inputItemIdx, batchSize, inputItemNum,
			deriv, inputDim, outputDim, output, lr);
}

__global__ void cuda_sparseL1full(int batchSize, float * L1, int * inputSmpIdx, int * inputItemIdx, float * outputScore, int inputDim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < inputDim)
	{
		int col_end = inputSmpIdx[idx];
		int col_begin = idx > 0 ? inputSmpIdx[idx - 1] : 0;
		float score = 0;
		for (int w = col_begin; w < col_end; w++)
		{
			int inputId = inputItemIdx[w];
			score += L1[inputId * inputDim + idy];
		}
		outputScore[idx * inputDim + idy] += score;
	}
}

void cuda_SparseL1Full(int batchSize, float * L1, int * inputSmpIdx, int * inputItemIdx, float * outputScore, int inputDim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (inputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_sparseL1full << <block_tail, thread_tail >> >(batchSize, L1, inputSmpIdx, inputItemIdx, outputScore, inputDim);
}

__global__ void cuda_sparseLkfull(float * hidden, int hiddenDim, int batchSize, float * Lk, float * outputScore, int inputDim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < inputDim)
	{
		float sum = 0;
		for (int h = 0; h < hiddenDim; h++)
		{
			sum += hidden[idx * hiddenDim + h] * Lk[idy * hiddenDim + h];
		}
		outputScore[idx * inputDim + idy] += sum;
	}
}

void cuda_SparseLkFull(float * hidden, int hiddenDim, int batchSize, float * Lk, float * outputScore, int inputDim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (inputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_sparseLkfull << <block_tail, thread_tail >> >(hidden, hiddenDim, batchSize, Lk, outputScore, inputDim);
}

//Data.OutputSampleIndex.CudaPtr, Data.OutputItemIndex.CudaPtr, Data.OutputMapSample.CudaPtr, Data.OutputScore.CudaPtr,
//                        Data.BatchSize, Data.OutputItemNum, DnnRun.neurallayers[i + 1].Output.CudaPtr, Lk[i].CudaPtr, DnnRun.neurallayers[i + 1].Number, learnRate

__global__ void cuda_sparseLkupdate(int * outputSmpIdx, int * outputItemIdx, int * outputMapSample, float * outputDeriv, int batchSize,
		int outputItemNum, float * hidden, float * Lk, int hiddenDim, float lr)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < outputItemNum && idy < hiddenDim)
	{
		int smpId = outputMapSample[idx];
		float deriv = outputDeriv[idx];
		int outputId = outputItemIdx[idx];

		atomicAdd(Lk + outputId * hiddenDim + idy, deriv * lr * hidden[smpId * hiddenDim + idy]);
	}
}

void cuda_SparseLkUpdate(int * outputSmpIdx, int * outputItemIdx, int * outputMapSample, float * outputDeriv, int batchSize,
		int outputItemNum, float * hidden, float * Lk, int hiddenDim, float lr)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((outputItemNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_sparseLkupdate << <block_tail, thread_tail >> >(outputSmpIdx, outputItemIdx, outputMapSample, outputDeriv, batchSize,
			outputItemNum, hidden, Lk, hiddenDim, lr);
}




//Data.OutputSampleIndex.CudaPtr, Data.OutputItemIndex.CudaPtr, Data.OutputScore.CudaPtr,
// Data.OutputLabel.CudaPtr, Data.BatchSize, Data.OutputItemNum, Data.OutputScore.CudaPtr, DEMParameters.Gamma, DEMParameters.IsLogObj ? 1 : 0, batchLoss.CudaPtr

__global__ void cuda_sparse_softmax(int * outputSmpIdx, int * outputItemIdx, float * outputScore, float * outputLabel, int batchSize,
		int itemNum, float * deriv, float gamma, int isLog, float * loss) 
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int itemEnd = outputSmpIdx[idx];
		int itemBegin = idx > 0 ? outputSmpIdx[idx - 1] : 0;

		float log_sum = -1000000;
		float weight_log_sum = -1000000;
		for (int id = itemBegin; id < itemEnd; id++)
		{
			float a = gamma * outputScore[id];

			int label = 1;
			float label_bias = -1000000;
			if (outputLabel[id] > 0.0000001)
				label_bias = logf(outputLabel[id]);
			else
				label = 0;

			if (id == itemBegin)
			{
				log_sum = a;
				if (label > 0)
					weight_log_sum = a + label_bias; 
				continue;
			}

			if (log_sum >= a)
				log_sum = log_sum + logf(1 + expf((a - log_sum)));
			else
				log_sum = a + logf(1 + expf((log_sum - a)));


			if (label > 0)
			{
				if (weight_log_sum >= a + label_bias)
					weight_log_sum = weight_log_sum + logf(1 + expf((a + label_bias - weight_log_sum)));
				else
					weight_log_sum = a + label_bias + logf(1 + expf((weight_log_sum - a - label_bias)));
			}
		}
		if (weight_log_sum > -1000000)
		{
			loss[idx] = weight_log_sum - log_sum;

			for (int id = itemBegin; id < itemEnd; id++)
			{
				float a = gamma * outputScore[id];

				int label = 1;
				float label_bias = -1000000;
				if (outputLabel[id] > 0.0000001)
					label_bias = logf(outputLabel[id]);
				else
					label = 0;

				deriv[id] = -expf(a - log_sum);
				if (label > 0)
					deriv[id] += expf(a + label_bias - weight_log_sum);

				if (isLog == 0)
					deriv[id] = deriv[id] * loss[idx];
			}

			if (isLog == 0)
				loss[idx] = expf(loss[idx]);
		}
		else
		{
			loss[idx] = 0;
			for (int id = itemBegin; id < itemEnd; id++)
			{
				deriv[id] = 0;
			}
		}


	}

}
void cuda_Sparse_SoftMax(int * outputSmpIdx, int * outputItemIdx, float * outputScore, float * outputLabel, int batchSize,
		int itemNum, float * deriv, float gamma, int isLog, float * loss) 
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_sparse_softmax << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(outputSmpIdx, outputItemIdx, outputScore, outputLabel, batchSize,
			itemNum, deriv, gamma, isLog, loss);
}

__global__ void cuda_RowNormSoftmax(int * smpIdx, int * rowIdx, int * colIdx, int batchSize, int * rowMargin, int rowCount, float * inWeight, float gamma, float * outWeight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < rowCount)
	{
		int smpId = rowMargin[idx];

		int smpEnd = smpIdx[smpId];
		int smpBgn = smpId == 0 ? 0 : smpIdx[smpId - 1];

		int rowEnd = rowIdx[smpId];
		int rowBgn = smpId == 0 ? 0 : rowIdx[smpId - 1];
		int row = rowEnd - rowBgn;

		int colEnd = colIdx[smpId];
		int colBgn = smpId == 0 ? 0 : colIdx[smpId - 1];
		int col = colEnd - colBgn;

		int midx = smpBgn + (idx - rowBgn) * col;

		float max_wei = gamma * inWeight[midx];
		for (int id = 1; id < col; id++)
		{
			float a = gamma * inWeight[midx + id];
			if (a > max_wei) max_wei = a;
		}

		float exp_sum = 0;
		for (int id = 0; id < col; id++)
		{
			float a = expf(gamma * (inWeight[midx + id] - max_wei));
			outWeight[midx + id] = a;
			exp_sum += a;
		}

		for (int id = 0; id < col; id++)
		{
			outWeight[midx + id] = outWeight[midx + id] / exp_sum;
		}
	}
}

void Cuda_RowNormSoftmax(int * smpIdx, int * rowIdx, int * colIdx, int batchSize, int * rowMargin, int rowCount, float * inWeight, float gamma, float * outWeight)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (rowCount + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_RowNormSoftmax << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> > (smpIdx, rowIdx, colIdx, batchSize, rowMargin, rowCount, inWeight, gamma, outWeight);
}

__global__ void cuda_ColNormSoftmax(int * smpIdx, int * rowIdx, int * colIdx, int batchSize, int * colMargin, int colCount, float * inWeight, float gamma, float * outWeight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < colCount)
	{
		int smpId = colMargin[idx];

		int smpEnd = smpIdx[smpId];
		int smpBgn = smpId == 0 ? 0 : smpIdx[smpId - 1];

		int rowEnd = rowIdx[smpId];
		int rowBgn = smpId == 0 ? 0 : rowIdx[smpId - 1];
		int row = rowEnd - rowBgn;

		int colEnd = colIdx[smpId];
		int colBgn = smpId == 0 ? 0 : colIdx[smpId - 1];
		int col = colEnd - colBgn;

		int colIdx = idx - colBgn;
		float max_wei = gamma * inWeight[smpBgn + colIdx];
		for (int id = 1; id < row; id++)
		{
			float a = gamma * inWeight[smpBgn + id * col + colIdx];
			if (a > max_wei) max_wei = a;
		}

		float exp_sum = 0;
		for (int id = 0; id < row; id++)
		{
			float a = expf(gamma * (inWeight[smpBgn + id * col + colIdx] - max_wei));
			outWeight[smpBgn + colIdx * row + id] = a;
			exp_sum += a;
		}

		for (int id = 0; id < row; id++)
		{
			outWeight[smpBgn + colIdx * row + id] = outWeight[smpBgn + colIdx * row + id] / exp_sum;
		}
	}
}

void Cuda_ColNormSoftmax(int * smpIdx, int * rowIdx, int * colIdx, int batchSize, int * colMargin, int colCount, float * inWeight, float gamma, float * outWeight)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (colCount + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_ColNormSoftmax << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> > (smpIdx, rowIdx, colIdx, batchSize, colMargin, colCount, inWeight, gamma, outWeight);
}


__global__ void cuda_joint_sparse_matrix_product_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		int * ConSmp_Idx, int * ConFea_Idx, float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * WeightUpdate, float * OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < OutputDimension && idy < MatchSize)
	{
		float sum = 0;
		int FeaDim = 0;

		float outputDerivValue = OutputDeriv[idy * OutputDimension + idx] * lr;
		if (IsSim > 0 && SrcDimension == TgtDimension)
		{
			atomicAdd(WeightUpdate + idx, outputDerivValue * Match_Score[idy]);
			FeaDim += 1;
		}

		if (IsSrc > 0)
		{
			int aIdx = Src_Match_Idx[idy] * SrcDimension;
			for (int i = 0; i < SrcDimension; i++)
				atomicAdd(WeightUpdate + (i + FeaDim) * OutputDimension + idx, outputDerivValue * Src_Input[aIdx + i]);
			FeaDim += SrcDimension;
		}

		if (IsTgt > 0)
		{
			int bIdx = Tgt_Match_Idx[idy] * TgtDimension;
			for (int i = 0; i<TgtDimension; i++)
				atomicAdd(WeightUpdate + (i + FeaDim) * OutputDimension + idx, outputDerivValue * Tgt_Input[bIdx + i]);
			FeaDim += TgtDimension;
		}

		if (IsConScore > 0)
		{
			int fea_end = ConSmp_Idx[idy];
			int fea_begin = idy > 0 ? ConSmp_Idx[idy - 1] : 0;

			for (int i = fea_begin; i < fea_end; ++i)
			{
				int fea_idx = ConFea_Idx[i];
				atomicAdd(WeightUpdate + (fea_idx + FeaDim) * OutputDimension + idx, outputDerivValue * ConFea_Value[i]);
			}
			FeaDim += ConDimension;
		}

	}
}

void cuda_Joint_Sparse_Matrix_Product_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		int * ConSmp_Idx, int * ConFea_Idx, float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * WeightUpdate, float * OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((OutputDimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (MatchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_joint_sparse_matrix_product_INTEX << <block_tail, thread_tail >> >(Src_Input, SrcSize, Tgt_Input, TgtSize,
			Src_Match_Idx, Tgt_Match_Idx, Match_Score, MatchSize,
			ConSmp_Idx, ConFea_Idx, ConFea_Value, SrcDimension, TgtDimension, ConDimension, IsConScore,
			WeightUpdate, OutputDeriv, OutputDimension, IsSrc, IsTgt, IsSim, lr);
}




__global__ void cuda_joint_dense_matrix_product_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * WeightUpdate, float * OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr, int inputFeaDim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < inputFeaDim && idy < MatchSize)
	{
		int FeaDim = 0;

		if (IsSim > 0 && SrcDimension == TgtDimension)
		{
			if (idx == 0)
			{
				float deriv = lr * Match_Score[idy];
				for (int i = 0; i< OutputDimension; i++)
				{
					WeightUpdate[i] += OutputDeriv[idy * OutputDimension + i] * deriv;
				}
			}
			FeaDim += 1;
		}

		if (IsSrc > 0)
		{
			if (idx >= FeaDim && idx < FeaDim + SrcDimension)
			{
				int aIdx = Src_Match_Idx[idy] * SrcDimension;
				float deriv = lr * Src_Input[aIdx + idx - FeaDim];
				int wIdx = idx * OutputDimension;
				for (int i = 0; i< OutputDimension; i++)
				{
					WeightUpdate[wIdx + i] += deriv * OutputDeriv[idy * OutputDimension + i];
				}
			}
			FeaDim += SrcDimension;
		}

		if (IsTgt > 0)
		{
			if (idx >= FeaDim && idx < FeaDim + TgtDimension)
			{
				int bIdx = Tgt_Match_Idx[idy] * TgtDimension;
				float deriv = lr * Tgt_Input[bIdx + idx - FeaDim];
				int wIdx = (idx)* OutputDimension;
				for (int i = 0; i<OutputDimension; i++)
				{
					WeightUpdate[wIdx + i] += deriv * OutputDeriv[idy * OutputDimension + i];
				}
			}
			FeaDim += TgtDimension;
		}

		if (IsConScore > 0)
		{
			if (idx >= FeaDim && idx < FeaDim + ConDimension)
			{
				float deriv = ConFea_Value[idy * ConDimension + idx - FeaDim] * lr;
				int wIdx = (idx)* OutputDimension;
				for (int i = 0; i < OutputDimension; i++)
				{
					WeightUpdate[wIdx + i] += deriv * OutputDeriv[idy * OutputDimension + i];
				}
			}
			FeaDim += ConDimension;
		}
	}
}

void cuda_Joint_Dense_Matrix_Product_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * WeightUpdate, float * OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr)
{
	int inputFeaDim = 0;
	if (IsSim > 0 && SrcDimension == TgtDimension)
	{
		inputFeaDim += 1;
	}
	if (IsSrc > 0)
	{
		inputFeaDim += SrcDimension;
	}
	if (IsTgt > 0)
	{
		inputFeaDim += TgtDimension;
	}
	if (IsConScore > 0)
	{
		inputFeaDim += ConDimension;
	}

	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((inputFeaDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (MatchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_joint_dense_matrix_product_INTEX << <block_tail, thread_tail >> >(Src_Input, SrcSize, Tgt_Input, TgtSize,
			Src_Match_Idx, Tgt_Match_Idx, Match_Score, MatchSize,
			ConFea_Value, 
			SrcDimension, TgtDimension, ConDimension, IsConScore,
			WeightUpdate, OutputDeriv, OutputDimension, IsSrc, IsTgt, IsSim, lr, inputFeaDim);
}



//__global__ void cuda_sparse_matrix_transpose_multiply_INTEX_weight(int * Smp_Index, int batchsize, int * Fea_Index,
//	float * Fea_Value, int elementsize,
//	float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
//{
//	int idx = blockDim.x * blockIdx.x + threadIdx.x;
//	if (idx < output_dimension)
//	{
//		for (int sample = 0; sample < batchsize; ++sample)
//		{
//			int fea_end = Smp_Index[sample];
//			int fea_begin = sample == 0 ? 0 : Smp_Index[sample - 1];
//			for (int i = fea_begin; i<fea_end; ++i)
//			{
//				int fea_idx = Fea_Index[i];
//				mul_weight[fea_idx * output_dimension + idx] += Fea_Value[i] * output[sample * output_dimension + idx] * weight;
//			}
//		}
//	}
//}



__global__ void cuda_binomialsampling(float* prob, int batchSize, int Dim, float * sample, int randomSeed)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		curandState_t state;
		/* we have to initialize the state */
		curand_init(randomSeed, /* the seed controls the sequence of random values that are produced */
				idx, /* the sequence number is only important with multiple cores */
				0, /* the offset is how much extra we advance in the sequence for each call, can be 0 */
				&state);
		for (int i = 0; i < Dim; i++)
		{
			float p = curand_normal(&state);
			if (p < prob[idx * Dim + i]) sample[idx * Dim + i] = 1;
			else sample[idx * Dim + i] = 0;
		}
	}
}
void cuda_BinomialSampling(float* prob, int batchSize, int Dim, float * sample, int randomSeed)
{
	/* CUDA's random number library uses curandState_t to keep track of the seed value
		 we will store a random state for every thread  */

	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_binomialsampling << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(prob, batchSize, Dim, sample, randomSeed);
}



__global__ void cuda_sparse_matrix_aggragate_weight(int * smpIdx, int * feaIdx, float * feaValue, float * b, int batchsize, int m, float weight)
//int kept, float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchsize)
	{
		int feaEnd = smpIdx[idx];
		int feaBeg = idx == 0 ? 0 : smpIdx[idx - 1];
		for (int i = feaBeg; i < feaEnd; i++)
		{
			int fid = feaIdx[i];
			atomicAdd(b + fid, weight * feaValue[i]);
		}
	}
}

void cuda_Sparse_Matrix_Aggragate_Weight(int * smpIdx, int * feaIdx, float * feaValue, float * b, int batchsize, int m, float weight)
	//, int kept, float * alpha, 
	//		  int ntrial, int BATCH_SIZE, int alpha_index)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchsize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparse_matrix_aggragate_weight << <nBlockPerGrid, DEFAULT_THREAD_PER_BLOCK >> >(smpIdx, feaIdx, feaValue, b, batchsize, m, weight); //,kept, alpha, ntrial, BATCH_SIZE, alpha_index);
}


__global__ void cuda_reconstructionloss(float * outputScore, int * smpIdx, int * feaIdx, float * feaValue, float * loss, int batchSize, int dimension, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		loss[idx] = 0;
		for (int i = 0; i < dimension; i++)
		{
			loss[idx] += logf(1 - outputScore[idx * dimension + i] + eps);
		}

		int feaEnd = smpIdx[idx];
		int feaBeg = idx == 0 ? 0 : smpIdx[idx - 1];
		for (int i = feaBeg; i < feaEnd; i++)
		{
			int fid = feaIdx[i];
			if (feaValue[i] > 0) loss[idx] += -logf(1 - outputScore[idx * dimension + fid] + eps) + logf(outputScore[idx * dimension + fid] + eps);
		}
	}
}

void cuda_ReconstructionLoss(float * outputScore, int * smpIdx, int * feaIdx, float * feaValue, float * loss, int batchSize, int dimension, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_reconstructionloss << <nBlockPerGrid, nThreadPerBlock >> >(outputScore, smpIdx, feaIdx, feaValue, loss, batchSize, dimension, eps);
}


__global__ void cuda_sparseupdaterRBM(int * outputSmpIdx, int * outputItemIdx, float * outputItemLabel, int batchSize, float * weight, 
		float * output, float * bias_v, int inputDim, int outputDim,
		int * inputSmpIdx, int * inputItemIdx,
		float * grad_bias_v, float * grad_bias_h, float * grad_vh, float vStep, float hStep, float vhStep,
		float * loss, float eps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int end = outputSmpIdx[idx];
		int begin = idx == 0 ? 0 : outputSmpIdx[idx - 1];

		int ee = inputSmpIdx[idx];
		int ss = idx == 0 ? 0 : inputSmpIdx[idx - 1];

		int idx_start = idx * outputDim;
		for (int word_idx = begin; word_idx < end; ++word_idx)
		{
			int fea_idx = outputItemIdx[word_idx];
			int fea_start = fea_idx * outputDim;

			float Ebias = bias_v[fea_idx];
			for (int i = 0; i < outputDim; i++)
			{
				float oldh = output[idx_start + i];
				float h = oldh + weight[fea_start + i];
				Ebias += logf(1 + expf(h)) - logf(1 + expf(oldh));	//(newhiddens[h]) - SoftPlus(oldhiddens[h]);	
			}

			float score = (tanhf(Ebias / 2.0) + 1.0) / 2.0;

			if (outputItemLabel[word_idx] > 0)
			{
				loss[word_idx] = logf(score + eps);
				atomicAdd(grad_bias_v + fea_idx, vStep * (1 - score));
			}
			else
			{
				loss[word_idx] = logf(1 - score + eps);
				atomicAdd(grad_bias_v + fea_idx, -vStep * score);
			}
			for (int i = 0; i < outputDim; i++)
			{
				float oldh = output[idx_start + i];
				float h = oldh + weight[fea_start + i];

				float logistic_newOutput = (tanhf(h / 2.0) + 1.0) / 2.0;
				float logistic_output = (tanhf(oldh / 2.0) + 1.0) / 2.0;

				if (outputItemLabel[word_idx] > 0)
				{
					float delta = (1 - score) * (logistic_newOutput - logistic_output);
					atomicAdd(grad_bias_h + i, hStep * delta);
					for (int mi = ss; mi < ee; mi++)
					{
						int mfid = inputItemIdx[mi];
						atomicAdd(grad_vh + mfid * outputDim + i, vhStep * delta);
					}
					atomicAdd(grad_vh + fea_start + i, vhStep * (1 - score) *  logistic_newOutput);
				}
				else
				{
					float delta = score * (logistic_newOutput - logistic_output);
					atomicAdd(grad_bias_h + i, -hStep * delta);
					for (int mi = ss; mi < ee; mi++)
					{
						int mfid = inputItemIdx[mi];
						atomicAdd(grad_vh + mfid * outputDim + i, -vhStep * delta);
					}
					atomicAdd(grad_vh + fea_start + i, -vhStep * score *  logistic_newOutput);
				}
			}
		}
	}
}

//cuda_sparseupdaterRBM


void cuda_SparseUpdateRBM(int * outputSmpIdx, int * outputItemIdx, float * outputItemLabel, int batchSize, float * weight, 
		float * output, float * bias_v, int inputDim, int outputDim,
		int * inputSmpIdx, int * inputItemIdx,
		float * grad_bias_v, float * grad_bias_h, float * grad_vh, float vStep, float hStep, float vhStep,
		float * loss, float eps)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparseupdaterRBM << <nBlockPerGrid, nThreadPerBlock >> >(outputSmpIdx, outputItemIdx, outputItemLabel, batchSize, weight,
			output, bias_v, inputDim, outputDim, inputSmpIdx, inputItemIdx,
			grad_bias_v, grad_bias_h, grad_vh, vStep, hStep, vhStep, loss, eps);
}

__global__ void cuda_sparsepredictRBM(float * outputScore, int batchSize, float * weight, 
		float * output, float * bias_v, int inputDim, int outputDim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;

	if (idx < inputDim && idy < batchSize)
	{
		int batch_start = idy * outputDim;
		int fea_start = idx * outputDim;

		float Ebias = bias_v[idx];
		for (int h = 0; h < outputDim; h++)
		{
			float hid = output[batch_start + h] + weight[fea_start + h];
			Ebias += logf(1 + expf(hid)) - logf(1 + expf(output[batch_start + h]));
		}
		float score = (tanhf(Ebias / 2.0) + 1.0) / 2.0;
		outputScore[idy * inputDim + idx] = score;
	}
}

//cuda_sparseupdaterRBM

void cuda_SparsePredictRBM(float * outputScore, int batchSize, float * weight, 
		float * output, float * bias_v, int inputDim, int outputDim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((inputDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_sparsepredictRBM << <block_tail, thread_tail >> >(outputScore, batchSize, weight,
			output, bias_v, inputDim, outputDim);
}



// int Elementsize, Elementsize = Seg_size * Feature_dimension;
__global__ void cuda_convolution_dense_matrix_multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Margin, 
															int Seg_size, float * Fea_Value, int Feature_dimension, 
															float * con_weight, float * output, int output_dimension, int win_size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < output_dimension && idy < Seg_size)
	{
		output[idy * output_dimension + idx] = 0;
		int ws = win_size / 2;
		int mSmp_idx = Seg_Margin[idy];
		float sum = 0;

		for (int w = -ws; w < (int)win_size - ws; w++)
		{
			int row = idy + w;
			if (row >= 0 && row < Seg_size && Seg_Margin[row] == mSmp_idx)
			{
				float * pfea = Fea_Value + row * Feature_dimension;
				float * pcon_w = con_weight + (w + ws) * Feature_dimension * output_dimension + idx; 
				for (int fid = 0; fid < Feature_dimension; fid++)
				{
					sum += pfea[fid] * pcon_w[fid * output_dimension]; 
				}
			}
		}
		output[idy * output_dimension + idx] = sum;
	}
}

void cuda_Convolution_Dense_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Margin, 
		int Seg_size, float * Fea_Value, int Feature_dimension, // int Elementsize, Elementsize = Seg_size * Feature_dimension;
		float * con_weight, float * output, int output_dimension, int win_size)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (Seg_size + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	//int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	//int nBlockPerGrid = ( m * n + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_convolution_dense_matrix_multiply_INTEX << <block_tail, thread_tail >> >(Smp_Index, batchsize, Seg_Margin, Seg_size, Fea_Value, Feature_dimension, con_weight, output, output_dimension, win_size);
}


//										Output.Deriv.CudaPtr, LayerMaxPoolingIndex.CudaPtr, 
//                                       Input.BatchSize, Output.Width, Output.Height, Output.Depth,
//                                       Model.Param.PoolingParameter.SX, Model.Param.PoolingParameter.Stride,
//                                       Model.FilterOptimizer.Gradient.CudaPtr, Model.Param.SX, Model.Param.C, Model.Param.Pad,
//                                       Model.Param.Stride,  Input.Data.CudaPtr, Input.Width, Input.Height, Input.Depth, Model.FilterOptimizer.GradientStep);

__global__ void cuda_convolution_image_matrix_product_INTEX_Weight(float * outputDeriv, int * maxpoolingIndex, 
		int batchsize, int outputWidth, int outputHeight, int outputDepth,
		int poolingSX, int poolingStride, 
		float * grad, int sx, int c, int pad, int stride,
		float * input, int width, int height, int depth, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idz = blockDim.z * blockIdx.z + threadIdx.z;
	if (idx < outputWidth && idy < outputHeight && idz < outputDepth * batchsize)
	{
		int poolMaxId = maxpoolingIndex[idz * outputWidth * outputHeight + idy * outputWidth + idx];
		int maxIdx = poolMaxId % poolingSX;
		int maxIdy = poolMaxId / poolingSX;

		int poolIdx = idx * poolingStride + maxIdx;
		int poolIdy = idy * poolingStride + maxIdy;

		int startIdx = poolIdx * stride - pad;
		int startIdy = poolIdy * stride - pad;

		int batchIdx = idz / outputDepth;
		int o = idz % outputDepth;

		float deriv = outputDeriv[idz * outputWidth * outputHeight + idy * outputWidth + idx];

		int inputBatchIdx = batchIdx * width * height * depth;
		float sum = 0;
		for (int i = 0; i < sx; i++)
		{
			for (int j = 0; j < sx; j++)
			{
				for (int z = 0; z < c; z++)
				{
					if (i + startIdx >= 0 && i + startIdx < width && j + startIdy >= 0 && j + startIdy < height && z < depth)
					{
						float score = deriv * input[inputBatchIdx + z * width * height + (j + startIdy) * width + (i + startIdx)];

						//float score = deriv * filter[o * sx * sx * c + z * sx * sx + j * sx + i];
						atomicAdd(grad + o * sx * sx * c + z * sx * sx + j * sx + i, learnRate * score); 
					}
				}
			}
		}
	}
}

void cuda_Convolution_Image_Matrix_Product_INTEX_Weight(float * outputDeriv, int * maxpoolingIndex, 
		int batchsize, int outputWidth, int outputHeight, int outputDepth,
		int poolingSX, int poolingStride, 
		float * grad, int sx, int c, int pad, int stride,
		float * input, int width, int height, int depth, float learnRate)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D);
	dim3 block_tail((outputWidth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(outputHeight + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(batchsize * outputDepth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);

	cuda_convolution_image_matrix_product_INTEX_Weight << <block_tail, thread_tail >> >(outputDeriv, maxpoolingIndex, batchsize, outputWidth, outputHeight,
			outputDepth, poolingSX, poolingStride, grad, sx, c, pad, stride, input, width, height, depth, learnRate);
	//,alpha, ntrial, BATCH_SIZE, alpha_index); 
}



__global__ void cuda_convolution_dense_matrix_product_INTEX_Weight(float * deriv, int * maxpooling_index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output_dimension * win_size)
	{
		int output_idx = idx / win_size;
		int win_idx = idx % win_size;

		//float sum = 0;
		for (int b = 0; b < batchsize; b++)
		{
			int target_seg = maxpooling_index[b * output_dimension + output_idx];

			if (target_seg == -1) continue;
			int target_smp = SegMargin_Index[target_seg];
			//deriv[i * output_dimension + idx] *  
			int ws = win_size / 2;
			int w = win_idx - ws;
			int row = target_seg + w; // idx / n;
			if (row >= 0 && row < seg_size)
			{
				if (SegMargin_Index[row] == target_smp)
				{
					for (int i = 0; i < Feature_Dimension; i++)
					{
						float m = Fea_Value[row * Feature_Dimension + i] * deriv[b*output_dimension + output_idx];
						// con_weight[((w+ws) * Feature_dimension + fea_idx)*output_dimension+idx];
						grad[(win_idx*Feature_Dimension + i) * output_dimension + output_idx] += m * learnRate;
					}
				}
			}
		}

	}
}

void cuda_Convolution_Dense_Matrix_Product_INTEX_Weight(float * deriv, int * maxpooling_index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((output_dimension * win_size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK);

	cuda_convolution_dense_matrix_product_INTEX_Weight << <block_tail, thread_tail >> >(deriv, maxpooling_index, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, Fea_Value, grad, Feature_Dimension, learnRate);
	//,alpha, ntrial, BATCH_SIZE, alpha_index); 
}

__global__ void cuda_conv_matrix_multipy(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
		int * sentMargin, int * layerPoolingIndex, int neuralOut, int neuralIn, int winSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < neuralOut)
	{
		int ws = winSize / 2;
		int oID = idx * neuralOut + idy;
		int target_seg = layerPoolingIndex[oID];
		if (target_seg == -1) return;
		int target_smp = sentMargin[target_seg];
		for (int w = -ws; w < winSize - ws; w++)
		{
			int row = target_seg + w; // idx / n;
			if (row >= 0 && row < sentSize)
			{
				if (sentMargin[row] == target_smp)
				{
					for (int i = 0; i < neuralIn; i++)
					{
						float m = upperDeriv[oID] * con_weight[((w + ws) * neuralIn + i) * neuralOut + idy];
						atomicAdd(deriv + row * neuralIn + i, m);
					}
				}
			}
		}

	}
}


void cuda_Conv_Matrix_Multipy(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
		int * sentMargin, int * layerPoolingIndex, int neuralOut, int neuralIn, int winSize)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (neuralOut + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_conv_matrix_multipy << <block_tail, thread_tail >> >(upperDeriv, con_weight, deriv, batchSize, sentSize, sentMargin, layerPoolingIndex, neuralOut, neuralIn, winSize);
}



__global__ void reduce1(float *g_idata, float *g_odata, int n) 
{
	extern __shared__ float sdata[];
	// each thread loads one element from global to shared mem
	int tid = threadIdx.x;
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	if (i < n)
	{
		sdata[tid] = g_idata[i];
	}
	else
	{
		sdata[tid] = 0;
	}

	__syncthreads();
	// do reduction in shared mem
	for (int s = 1; s < blockDim.x; s *= 2) {
		if (tid % (2 * s) == 0) {
			sdata[tid] += sdata[tid + s];
		}
		__syncthreads();
	}
	// write result for this block to global mem
	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}


__global__ void block_sum(float *input, float *per_block_results, int n)
{
	extern __shared__ float sdata[];
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	// load input into __shared__ memory
	float x = 0;
	if (i < n)
	{ 
		x = input[i];

		sdata[threadIdx.x] = x;
		__syncthreads();
		// contiguous range pattern
		for (int offset = blockDim.x / 2; offset > 0; offset >>= 1)
		{
			if (threadIdx.x < offset)
			{
				// add a partial sum upstream to our own
				sdata[threadIdx.x] += sdata[threadIdx.x + offset];
			}
			// wait until all threads in the block have
			// updated their partial sums
			__syncthreads();
		}
		// thread 0 writes the final result
		if (threadIdx.x == 0)
		{
			per_block_results[blockIdx.x] = sdata[0];
		}
	}
}

__global__ void cuda_reduce(float * float_array, float * result, int ridx, int len)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	__shared__ float sum_value[256];
	__shared__ float miss_value[256];

	sum_value[idx] = 0;
	miss_value[idx] = 0;
	for (int i = idx; i<len; i += 256)
	{
		if (float_array[i] > 0)
		{
			sum_value[idx] += float_array[i];
			miss_value[idx] += 1;
		}
	}
	__syncthreads();
	if (idx == 0)
	{
		float sum = 0;
		float sumIdx = 0;
		for (int i = 0; i < 256; i++)
		{
			sum += sum_value[i];
			sumIdx += miss_value[i];
		}
		result[ridx] += sum;
		result[ridx + 1] += sumIdx;
	}
}

//void cuda_Normailzed(float * float_array, int len)
//{
//	dim3 thread_tail(DEFAULT_THREAD_PER_DIM);
//	dim3 block_tail(1);
//	cuda_normailzed<<<block_tail,thread_tail>>>(float_array, len);
//}
//


__global__ void cuda_Calculate_mse(float * label, float * score, int batchNum, float * result, int resultIdx)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchNum)
	{
		if (label[idx] > -1)
		{
			score[idx] = (label[idx] - score[idx]) * (label[idx] - score[idx]);
		}
		else
		{
			score[idx] = -1;
		}
	}
}

void cuda_Calculate_MSE(float * label, float * score, int length, float * result, int resultIdx)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (length + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_Calculate_mse << < nBlockPerGrid, nThreadPerBlock >> > (label, score, length, result, resultIdx);

	cuda_reduce << <1, 256 >> >(score, result, resultIdx, length);
	/*float *d_partial_sums_and_total = 0;
		cudaMalloc((void**)&d_partial_sums_and_total, sizeof(float) * (nBlockPerGrid));
		reduce1<<<nBlockPerGrid ,nThreadPerBlock ,nThreadPerBlock * sizeof(float)>>>(score, d_partial_sums_and_total, length);
		reduce1<<<1,nBlockPerGrid,nBlockPerGrid * sizeof(float)>>>(d_partial_sums_and_total, result + resultIdx, nBlockPerGrid);
		cudaFree(d_partial_sums_and_total);*/
}

__global__ void cuda_convolution_sparse_matrix_gradient(float * deriv, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output_dimension * win_size)
	{
		int output_idx = idx % output_dimension;
		int win_idx = idx / output_dimension;
		float * g = grad + win_idx * Feature_Dimension * output_dimension + output_idx;

		for (int b = 0; b < seg_size; b++)
		{
			int ws = win_size / 2;
			int w = win_idx - ws;
			int target_smp = SegMargin_Index[b];
			int row = b + w;
			if (row >= 0 && row < seg_size && SegMargin_Index[row] == target_smp)
			{
				int col_end = Seg_Index[row];
				int col_begin = row == 0 ? 0 : Seg_Index[row - 1];
				float d = deriv[b * output_dimension + output_idx];
				for (int i = col_begin; i < col_end; i++)
				{
					int fea_idx = Fea_Index[i];
					g[fea_idx * output_dimension] += Fea_Value[i] * d * learnRate;
				}
			}
		}

	}
}

void Cuda_convolution_sparse_matrix_gradient(float * deriv, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((output_dimension * win_size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK);

	cuda_convolution_sparse_matrix_gradient << <block_tail, thread_tail >> >(deriv, Seg_Index, SegMargin_Index, seg_size, win_size,
		batchsize, output_dimension, Fea_Index, Fea_Value, grad, Feature_Dimension, learnRate);
}


__global__ void cuda_convolution_dense_matrix_gradient(float * deriv, int * SegMargin_Index, int seg_size, int win_size,
	int batchsize, int output_dimension, float * input, float * grad, int Feature_Dimension, float learnRate)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < output_dimension * win_size && idy < Feature_Dimension)
	{
		int output_idx = idx % output_dimension;
		int win_idx = idx / output_dimension;
		float * g = grad + win_idx*Feature_Dimension* output_dimension + output_idx;

		for (int b = 0; b < seg_size; b++)
		{
			int ws = win_size / 2;
			int w = win_idx - ws;
			int target_smp = SegMargin_Index[b];
			int row = b + w;
			if (row >= 0 && row < seg_size && SegMargin_Index[row] == target_smp)
			{
				float d = deriv[b * output_dimension + output_idx];
				float * pInput = input + row * Feature_Dimension;
				g[idy * output_dimension] += pInput[idy] * d * learnRate;

				//for (int i = 0; i < Feature_Dimension; i++)
				//{
				//	float m = pInput[i] * d;
				//	g[i * output_dimension] += m * learnRate;
				//}
			}
		}

	}
}

void Cuda_convolution_dense_matrix_gradient(float * deriv, int * SegMargin_Index, int seg_size, int win_size,
	int batchsize, int output_dimension, float * input, float * grad, int Feature_Dimension, float learnRate)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension * win_size - 1) / DEFAULT_THREAD_PER_DIM + 1, (Feature_Dimension - 1) / DEFAULT_THREAD_PER_DIM + 1 );
	cuda_convolution_dense_matrix_gradient << <block_tail, thread_tail >> >(deriv, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, input, grad, Feature_Dimension, learnRate);
}



__global__ void cuda_Convolution_Matrix_Transpose_Multiply(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
		int * sentMargin,  int neuralOut, int neuralIn, int winSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < sentSize && idy < neuralIn)
	{
		int ws = winSize / 2;
		int target_smp = sentMargin[idx];

		float sum = 0;
		for (int w = -ws; w < winSize - ws; w++)
		{
			int row = idx - w; 
			if (row >= 0 && row < sentSize && sentMargin[row] == target_smp)
			{
				float * pW = con_weight + ((w + ws) * neuralIn + idy) * neuralOut;
				float * puD = upperDeriv + row * neuralOut;
				for (int i = 0; i < neuralOut; i++) sum += puD[i] * pW[i];
			}
		}
		deriv[idx * neuralIn + idy] += sum;
	}
}


void Cuda_Convolution_Matrix_Transpose_Multiply(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
		int * sentMargin, int neuralOut, int neuralIn, int winSize)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((sentSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (neuralIn + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_Convolution_Matrix_Transpose_Multiply << <block_tail, thread_tail >> >(upperDeriv, con_weight, deriv, batchSize, sentSize, sentMargin, neuralOut, neuralIn, winSize);
}



__global__ void cuda_imageDenseconvmatrixmultipy(float * outputDeriv, //int * maxpoolingIndex,
		int batchSize, int outputWidth, int outputHeight, int outputDepth,
		//int poolingSX, int poolingStride,
		float * filter, int sx, int c, int pad, int stride,
		float * inputDeriv, int width, int height, int depth)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idz = blockDim.z * blockIdx.z + threadIdx.z;
	if (idx < width && idy < height && idz < depth * batchSize)
	{
		//int poolMaxId = maxpoolingIndex[idz * outputWidth * outputHeight + idy * outputWidth + idx];
		//int maxIdx = poolMaxId % poolingSX;
		//int maxIdy = poolMaxId / poolingSX;

		//int poolIdx = idx * poolingStride + maxIdx;
		//int poolIdy = idy * poolingStride + maxIdy;

		int affectedW = (sx - 1) / stride + 1;
		int bidx = idz / depth;
		int fiterz = idz % depth;

		int oidx = (idx + pad) / stride;
		int oidy = (idy + pad) / stride;

		int oSize = outputWidth * outputHeight;
		int sx2 = sx * sx;
		int sx2c = sx2 * c;
		float sum = 0;
		for (int i = 0; i < affectedW; i++)
		{
			for (int j = 0; j < affectedW; j++)
			{
				int x = oidx - i;
				int y = oidy - j;

				if (x >= 0 && y >= 0 && x < outputWidth && y < outputHeight)
				{
					///
					float * pOutputDeriv = outputDeriv + bidx * outputDepth * oSize + y * outputWidth + x;

					int filterx = idx + pad - x * stride;
					int filtery = idy + pad - y * stride;

					if (filterx >= 0 && filterx < sx && filtery >= 0 && filtery < sx)
					{
						float * pFilter = filter + fiterz * sx2 + filtery * sx + filterx;

						for (int oidz = 0; oidz < outputDepth; oidz++)
							sum += pOutputDeriv[oidz * oSize] * pFilter[oidz * sx2c];
					}
				}
			}
		}

		inputDeriv[idz * width * height + idy * width + idx] += sum;
	}
}

void cuda_ImageDenseConvMatrixMultipy(float * outputDeriv, //int * maxpoolingIndex,
		int batchSize, int outputWidth, int outputHeight, int outputDepth,
		//int poolingSX, int poolingStride,
		float * filter, int sx, int c, int pad, int stride,
		float * inputDeriv, int width, int height, int depth)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D);
	dim3 block_tail((width + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(height + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(batchSize * depth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);

	cuda_imageDenseconvmatrixmultipy << <block_tail, thread_tail >> >(outputDeriv, batchSize, outputWidth, outputHeight, outputDepth,
			filter, sx, c, pad, stride, inputDeriv, width, height, depth);
}


__global__ void cuda_convolution_dense_image_matrix_product_INTEX_Weight(float * outputDeriv, //int * maxpoolingIndex,
		int batchsize, int outputWidth, int outputHeight, int outputDepth,
		//int poolingSX, int poolingStride,
		float * grad, int sx, int c, int pad, int stride,
		float * input, int width, int height, int depth, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int idz = blockDim.z * blockIdx.z + threadIdx.z;
	if (idx < sx && idy < sx && idz < c * outputDepth)
	{
		int x = idx;
		int y = idy;
		int oz = idz / c;
		int iz = idz % c;

		//int poolMaxId = maxpoolingIndex[idz * outputWidth * outputHeight + idy * outputWidth + idx];
		//int maxIdx = poolMaxId % poolingSX;
		//int maxIdy = poolMaxId / poolingSX;

		//int poolIdx = idx * poolingStride + maxIdx;
		//int poolIdy = idy * poolingStride + maxIdy;

		float sum = 0;
		for (int b = 0; b < batchsize; b++)
		{
			float * pODerivMain = outputDeriv + (b * outputDepth + oz) * outputWidth * outputHeight;
			float * pIMain = input + ((b * depth) + iz) * width * height;

			for (int i = 0; i < outputWidth; i++)
			{
				for (int j = 0; j < outputHeight; j++)
				{
					int startx = i * stride - pad;
					int starty = j * stride - pad;

					int inX = startx + x;
					int inY = starty + y;

					if (inX >= 0 && inX < width && inY >= 0 && inY < height)
					{
						sum += pODerivMain[j * outputWidth + i] * pIMain[inY * width + inX];
					}
				}
			}
		}
		grad[oz * sx * sx * c + iz * sx * sx + y * sx + x] += sum * learnRate;
	}
}

void cuda_Convolution_Dense_Image_Matrix_Product_INTEX_Weight(float * outputDeriv, 
		int batchsize, int outputWidth, int outputHeight, int outputDepth,
		//int poolingSX, int poolingStride,
		float * grad, int sx, int c, int pad, int stride,
		float * input, int width, int height, int depth, float learnRate)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D, DEFAULT_THREAD_PER_DIM_3D);
	dim3 block_tail((sx + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(sx + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D,
			(depth * outputDepth + DEFAULT_THREAD_PER_DIM_3D - 1) / DEFAULT_THREAD_PER_DIM_3D);

	cuda_convolution_dense_image_matrix_product_INTEX_Weight << <block_tail, thread_tail >> >(outputDeriv, batchsize, outputWidth, outputHeight,
			outputDepth, grad, sx, c, pad, stride, input, width, height, depth, learnRate);
	//,alpha, ntrial, BATCH_SIZE, alpha_index); 
}


__global__ void cuda_convolution_index_matrix_multiply(int batchsize, int * Seg_Index, int * Seg_Margin,
		int seg_size, int * Fea_Index, int elementsize, float * con_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < output_dimension && idy < seg_size)
	{
		int ws = win_size / 2;
		int mSmp_idx = Seg_Margin[idy];
		float sum = 0;
		for (int w = -ws; w < (int)win_size - ws; w++)	//win_size - 
		{
			//output[idy * output_dimension + idx] +=  1; 
			//sum += 1;
			int row = idy + w; // idx / n;
			if (row >= 0 && row < seg_size && Seg_Margin[row] == mSmp_idx)
			{
				int col_end = Seg_Index[row];
				int col_begin = row == 0 ? 0 : Seg_Index[row - 1];

				float * pcon_weight = con_weight + (w + ws) * Feature_dimension * output_dimension + idx;

				for (int i = col_begin; i < col_end; i++)
				{
					//if (fea_idx >= Feature_dimension) continue;	
					sum += pcon_weight[Fea_Index[i] * output_dimension]; // * 1.0f / mlen
				}
			}
		}
		output[idy * output_dimension + idx] = sum;
	}
}

void cuda_Convolution_Index_Matrix_Multiply(int batchsize, int * Seg_Index, int * Seg_Margin,
		int seg_size, int * Fea_Index, int elementsize, float * con_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (seg_size + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_convolution_index_matrix_multiply << <block_tail, thread_tail >> >(batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, 
			elementsize, con_weight, output, Feature_dimension, output_dimension, win_size);
}


__global__ void cuda_convolution_index_matrix_product_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * grad, int Feature_Dimension, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < output_dimension * win_size)
	{
		int output_idx = idx % output_dimension;
		int win_idx = idx / output_dimension;

		//float sum = 0;
		for (int b = 0; b < batchsize; b++)
		{
			int target_seg = maxpooling_index[b * output_dimension + output_idx];
			if (target_seg == -1) continue;

			int target_smp = SegMargin_Index[target_seg];

			//deriv[i * output_dimension + idx] *  
			int ws = win_size / 2;
			int w = win_idx - ws;
			int row = target_seg + w; // idx / n;

			if (row >= 0 && row < seg_size && SegMargin_Index[row] == target_smp)
			{
				int col_end = Seg_Index[row];
				int col_begin = row == 0 ? 0 : Seg_Index[row - 1];

				float dv = deriv[b * output_dimension + output_idx];
				float * pGrad = grad + win_idx * Feature_Dimension * output_dimension + output_idx;
				for (int i = col_begin; i < col_end; i++)
				{
					int fea_idx = Fea_Index[i];
					//if (fea_idx >= Feature_Dimension) continue;
					//float m = Fea_Value[i] * dv;
					pGrad[fea_idx * output_dimension] += dv * learnRate;
				}
			}
		}

	}
}

void cuda_Convolution_Index_Matrix_Product_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * grad, int Feature_Dimension, float learnRate)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((output_dimension * win_size + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK);

	cuda_convolution_index_matrix_product_Weight << <block_tail, thread_tail >> >(deriv, maxpooling_index, Seg_Index, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, Fea_Index, grad, Feature_Dimension, learnRate);
	//,alpha, ntrial, BATCH_SIZE, alpha_index); 
}


///////////////////////////////////////////////////////////////////////////// Max Pooling Functions.
__global__ void cuda_max_pooling(float * pooling_feas, int * Smp_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idy < batchsize && idx < output_dimension)
	{
		//output[idy * output_dimension + idx] = 0;
		int col_end = Smp_Index[idy];
		int col_begin = idy == 0 ? 0 : Smp_Index[idy - 1];

		float max_value = 0;
		int max_index = -1;
		if (col_begin < col_end)
		{
			max_value = pooling_feas[col_begin * output_dimension + idx];
			max_index = col_begin;
		}

		for (int i = col_begin + 1; i<col_end; i++)
		{
			if (pooling_feas[i * output_dimension + idx] > max_value)
			{
				max_value = pooling_feas[i * output_dimension + idx];
				max_index = i;
			}
		}
		output[idy * output_dimension + idx] = max_value;
		maxpooling_index[idy * output_dimension + idx] = max_index;
	}
}
__global__ void cuda_max_pooling_mask(float * pooling_feas, int * Smp_Index, int * Seq_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idy < batchsize && idx < output_dimension)
	{
		//output[idy * output_dimension + idx] = 0;
		int col_end = Smp_Index[idy];
		int col_begin = idy == 0 ? 0 : Smp_Index[idy - 1];

		float max_value = 0;
		int max_index = -1;
		if (col_begin < col_end)
		{
			max_index = Seq_Index == NULL ? col_begin : Seq_Index[col_begin];
			max_value = pooling_feas[max_index * output_dimension + idx];
		}

		for (int i = col_begin + 1; i<col_end; i++)
		{
			int idm = Seq_Index == NULL ? i : Seq_Index[i];
			float idv = pooling_feas[idm * output_dimension + idx];
			if (idv > max_value)
			{
				max_value = idv;
				max_index = idm;
			}
		}
		output[idy * output_dimension + idx] = max_value;
		maxpooling_index[idy * output_dimension + idx] = max_index;
	}
}
__global__ void cuda_SpanMaxPool(float * inputSent, int * smpIdx, float * start, float * end, int * sentMargin, int matchSize, float * outputSent, int * outMaxIndex, int dim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idy < matchSize && idx < dim)
	{
		int smpid = sentMargin[idy];
		int smpOffset = smpid == 0 ? 0 : smpIdx[smpid - 1];

		int startid = (int)start[idy] + smpOffset;
		int endid = (int)end[idy] + smpOffset;

		float max_value = 0;
		int max_index = -1;
		if (startid <= endid)
		{
			max_index = startid;
			max_value = inputSent[max_index * dim + idx];
		}

		for (int i = startid + 1; i <= endid; i++)
		{
			int idm = i;
			float idv = inputSent[idm * dim + idx];
			if (idv > max_value)
			{
				max_value = idv;
				max_index = idm;
			}
		}
		outputSent[idy * dim + idx] = max_value;
		outMaxIndex[idy * dim + idx] = max_index;
	}
}
__global__ void cuda_SpanLastPool(float * inputSent, int * smpIdx, float * start, float * end, int * sentMargin, int matchSize, float * outputSent, int dim)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idy < matchSize && idx < dim)
	{
		int smpid = sentMargin[idy];
		int smpOffset = smpid == 0 ? 0 : smpIdx[smpid - 1];

		int startid = (int)start[idy] + smpOffset;
		int endid = (int)end[idy] + smpOffset;

		outputSent[idy * 2 * dim  + idx] = inputSent[startid * dim + idx];
		outputSent[idy * 2 * dim + dim + idx] = inputSent[endid * dim + idx];
	}
}
__global__ void cuda_maxpooling1D(float * values, int row, int column, int batchsize, float * output, int * maxpooling_index)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idy < batchsize && idx < column)
	{
		int idyIdx = idy * row * column;
		float max_value = values[idyIdx + idx];
		int max_index = 0;

		for (int i=1; i<row; i++)
		{
			if (values[idyIdx + i * column + idx] > max_value)
			{
				max_value = values[idyIdx + i * column + idx];
				max_index = i;
			}
		}
		output[idy * column + idx] = max_value;
		maxpooling_index[idy * column + idx] = max_index;
	}
}
__global__ void cuda_meanpooling1D(float * values, int row, int column, int batchsize, float * output)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idy < batchsize && idx < column)
	{
		int idyIdx = idy * row * column;

		float sum_value = 0;
		for (int i=0; i<row; i++)
		{
			sum_value += values[idyIdx + i * column + idx];
		}
		output[idy * column + idx] = sum_value / row;
	}
}

/// Max Pooling over pooling_feas.
void cuda_Max_Pooling(float * pooling_feas, int * Smp_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_max_pooling << <block_tail, thread_tail >> >(pooling_feas, Smp_Index, batchsize, output, maxpooling_index, output_dimension);
}
void Cuda_Max_Pooling_Mask(float * pooling_feas, int * Smp_Index, int * Seq_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_max_pooling_mask << <block_tail, thread_tail >> >(pooling_feas, Smp_Index, Seq_Index, batchsize, output, maxpooling_index, output_dimension);
}
void Cuda_SpanMaxPool(float * inputSent, int * smpIdx, float * start, float * end, int * sentMargin, int matchSize, float * outputSent, int * outMaxIndex, int dim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (matchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_SpanMaxPool << <block_tail, thread_tail >> > (inputSent, smpIdx, start, end, sentMargin, matchSize, outputSent, outMaxIndex, dim);
}
void Cuda_SpanLastPool(float * inputSent, int * smpIdx, float * start, float * end, int * sentMargin, int matchSize, float * outputSent, int dim)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (matchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_SpanLastPool << <block_tail, thread_tail >> > (inputSent, smpIdx, start, end, sentMargin, matchSize, outputSent, dim);
}
void Cuda_Maxpooling1D(float * values, int row, int column, int batchsize, float * output, int * maxpooling_index)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((column + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_maxpooling1D << <block_tail, thread_tail >> >(values, row, column, batchsize, output, maxpooling_index);
}
void Cuda_Meanpooling1D(float * values, int row, int column, int batchsize, float * output)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((column + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_meanpooling1D << <block_tail, thread_tail >> >(values, row, column, batchsize, output);
}


__global__ void cuda_DerivMaxPooling(float * deriv, int * maxpooling_index, float * pooling_deriv, int batchsize, int output_dimension, float alpha, float beta)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idy < batchsize && idx < output_dimension)
	{
		int oId = idy * output_dimension + idx;
		int target_seg = maxpooling_index[oId];
		if (target_seg > -1)
		{
			int iId = target_seg * output_dimension + idx;
			pooling_deriv[iId] = alpha * pooling_deriv[iId] + beta * deriv[oId];
		}
	}
}
__global__ void cuda_DerivSpanMaxPool(float * outputSentDeriv, int * smpIdx, int batchSize, float * inputSentDeriv, int * outMaxIndex, int dim, float beta)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < batchSize)
	{
		int startSpan = idy == 0 ? 0 : smpIdx[idy - 1];
		int endSpan = smpIdx[idy];

		for (int i = startSpan; i < endSpan; i++)
		{
			int oId = i * dim + idx;
			int target_seg = outMaxIndex[oId];
			if (target_seg > -1)
			{
				int iId = target_seg * dim + idx;
				inputSentDeriv[iId] = inputSentDeriv[iId] + beta * outputSentDeriv[oId];
			}
		}
	}
}
__global__ void cuda_DerivSpanLastPool(float * outputSentDeriv, int * outSmpIdx, float * start, float * end, int batchSize, float * inputSentDeriv, 
								int * inSmpIdx, int dim, float beta)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < batchSize)
	{
		int startSpan = idy == 0 ? 0 : outSmpIdx[idy - 1];
		int endSpan = outSmpIdx[idy];

		int smpOffset = idy == 0 ? 0 : inSmpIdx[idy - 1];

		for (int i = startSpan; i < endSpan; i++)
		{
			int startid = (int)start[i] + smpOffset;
			int endid = (int)end[i] + smpOffset;

			inputSentDeriv[startid * dim + idx] = inputSentDeriv[startid * dim + idx] + beta * outputSentDeriv[i * 2 * dim + idx];
			inputSentDeriv[endid * dim + idx] = inputSentDeriv[endid * dim + idx] + beta * outputSentDeriv[i * 2 * dim + dim + idx];
		}
	}
}
void Cuda_DerivMaxPooling(float * deriv, int * maxpooling_index, float * pooling_deriv, int batchsize, int output_dimension, float alpha, float beta)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((output_dimension + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchsize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_DerivMaxPooling << <block_tail, thread_tail >> >(deriv, maxpooling_index, pooling_deriv, batchsize, output_dimension, alpha, beta);
}
void Cuda_DerivSpanMaxPool(float * outputSentDeriv, int * smpIdx, int batchSize, float * inputSentDeriv, int * outMaxIndex, int dim, float beta)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_DerivSpanMaxPool << <block_tail, thread_tail >> > (outputSentDeriv, smpIdx, batchSize, inputSentDeriv, outMaxIndex, dim, beta);
}
void Cuda_DerivSpanLastPool(float * outputSentDeriv, int * outSmpIdx, float * start, float * end, int batchSize, float * inputSentDeriv, int * inSmpIdx, int dim, float beta)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((dim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_DerivSpanLastPool << <block_tail, thread_tail >> > (outputSentDeriv, outSmpIdx, start, end, batchSize, inputSentDeriv, inSmpIdx, dim, beta);
}

__global__ void cuda_ClipAdamUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float UpdateRate,
		float WeightClip,
		float GradClip,
		float decay,
		int iter,
		int CudaSize)
{
	//int idx = blockDim.x * blockIdx.x + threadIdx.x;
	//int idy = blockDim.y * blockIdx.y + threadIdx.y;
	//int id = idy * COL_FOLD_NUM + idx;

	int id = blockDim.x * blockIdx.x + threadIdx.x;

	if (id >= CudaSize)
		return;
	float sBeta2, sBeta1, sGradClip, sWeightClip, sUpdateRate, sEpsilon;

	sBeta2 = Beta2;
	sBeta1 = Beta1;
	sGradClip = GradClip;
	sWeightClip = WeightClip;
	sUpdateRate = UpdateRate;
	sEpsilon = Epsilon;


	float rG = gpu_floats_Gradient[id];
	float rM = gpu_floats_M[id];
	float rV = gpu_floats_V[id];
	float rP = gpu_floats_Parameter[id];

	if (sGradClip > 0.0)
		rG = (rG > sGradClip) ? sGradClip : (rG < (-1 * sGradClip) ? (-1 * sGradClip) : rG);
	rM = rM * Beta1 + rG  * (1 - Beta1);
	rV = rV * Beta2 + rG * rG * (1 - Beta2);
	rG =  rM / (sqrtf(rV) + sEpsilon);

	if(decay > 0) rG = rG - decay * rP; 

	rP += sUpdateRate * rG;

	if (sWeightClip > 0.0)
		rP = (rP > sWeightClip) ? sWeightClip : (rP < (-1 * sWeightClip) ? (-1 * sWeightClip) : rP);

	gpu_floats_Gradient[id] = 0;
	gpu_floats_Parameter[id] = rP;
	gpu_floats_M[id] = rM;
	gpu_floats_V[id] = rV;

}

void Cuda_ClipAdamUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,
		int iter,
		int CudaSize)
{

	//int RowNum = (CudaSize - 1) / COL_FOLD_NUM + 1;
	//dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	//dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	//cuda_ClipAdamUpdate << <block_tail, thread_tail >> >(gpu_floats_Gradient,gpu_floats_M,gpu_floats_V,gpu_floats_Parameter,Beta1,Beta2,Epsilon,updateRate,weightClip,gradClip,iter,CudaSize);

	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (CudaSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_ClipAdamUpdate << <nBlockPerGrid, nThreadPerBlock >> >(gpu_floats_Gradient,gpu_floats_M,gpu_floats_V,gpu_floats_Parameter,Beta1,Beta2,Epsilon,updateRate,weightClip,gradClip,decay,iter,CudaSize);
}



__global__ void cuda_ClipAdamBertUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float UpdateRate,
		float WeightClip,
		float GradClip,
		float decay,
		int iter,
		int CudaSize)
{
	int id = blockDim.x * blockIdx.x + threadIdx.x;

	//int idx = blockDim.x * blockIdx.x + threadIdx.x;
	//int idy = blockDim.y * blockIdx.y + threadIdx.y;
	//int id = idy * COL_FOLD_NUM + idx;
	
	if (id >= CudaSize)
		return;
	float sBeta2, sBeta1, sGradClip, sWeightClip, sUpdateRate, sEpsilon;

	sBeta2 = Beta2;
	sBeta1 = Beta1;
	sGradClip = GradClip;
	sWeightClip = WeightClip;
	sUpdateRate = UpdateRate;
	sEpsilon = Epsilon;

	float rG = gpu_floats_Gradient[id];
	float rM = gpu_floats_M[id];
	float rV = gpu_floats_V[id];
	float rP = gpu_floats_Parameter[id];

	if (sGradClip > 0.0)
		rG = (rG > sGradClip) ? sGradClip : (rG < (-1 * sGradClip) ? (-1 * sGradClip) : rG);
	
	rM = rM * Beta1 + rG  * (1 - Beta1);
	rV = rV * Beta2 + rG * rG * (1 - Beta2);

	rG = rM / (sqrtf(rV) + sEpsilon);
    
    if(decay > 0)
    	rG = rG - decay * rP; 

	rP += rG * sUpdateRate;

	if (sWeightClip > 0.0)
		rP = (rP > sWeightClip) ? sWeightClip : (rP < (-1 * sWeightClip) ? (-1 * sWeightClip) : rP);

	gpu_floats_Gradient[id] = 0;
	gpu_floats_Parameter[id] = rP;

	gpu_floats_M[id] = rM;
	gpu_floats_V[id] = rV;
}

void Cuda_ClipAdamBertUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,
		int iter,
		int CudaSize)
{
	
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (CudaSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_ClipAdamBertUpdate << <nBlockPerGrid, nThreadPerBlock >> >(gpu_floats_Gradient, gpu_floats_M, gpu_floats_V, gpu_floats_Parameter,Beta1,Beta2,Epsilon,updateRate,weightClip,gradClip, decay, iter, CudaSize);

	//int RowNum = (CudaSize - 1) / COL_FOLD_NUM + 1;
	//dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	//dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	//cuda_ClipAdamBertUpdate << <block_tail, thread_tail >> >(gpu_floats_Gradient, gpu_floats_M, gpu_floats_V, gpu_floats_Parameter,Beta1,Beta2,Epsilon,updateRate,weightClip,gradClip, decay, iter, CudaSize);
}


__global__ void cuda_ClipAdamDeltaUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float * gpu_orig_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float UpdateRate,
		float WeightClip,
		float GradClip,
		float decay,
		int iter,
		int CudaSize)
{
	int id = blockDim.x * blockIdx.x + threadIdx.x;
	if (id >= CudaSize) return;
	float sBeta2, sBeta1, sGradClip, sWeightClip, sUpdateRate, sEpsilon;

	sBeta2 = Beta2;
	sBeta1 = Beta1;
	sGradClip = GradClip;
	sWeightClip = WeightClip;
	sUpdateRate = UpdateRate;
	sEpsilon = Epsilon;

	float rG = gpu_floats_Gradient[id];
	float rM = gpu_floats_M[id];
	float rV = gpu_floats_V[id];
	float rP = gpu_floats_Parameter[id];
	float orig_p = gpu_orig_Parameter[id];

	if (sGradClip > 0.0)
		rG = (rG > sGradClip) ? sGradClip : (rG < (-1 * sGradClip) ? (-1 * sGradClip) : rG);
	
	rM = rM * Beta1 + rG  * (1 - Beta1);
	rV = rV * Beta2 + rG * rG * (1 - Beta2);

	rG = rM / (sqrtf(rV) + sEpsilon);
    
    if(decay > 0) rG = rG - decay * (rP - orig_p); 

	rP += rG * sUpdateRate;

	if (sWeightClip > 0.0)
		rP = (rP > sWeightClip) ? sWeightClip : (rP < (-1 * sWeightClip) ? (-1 * sWeightClip) : rP);

	gpu_floats_Gradient[id] = 0;
	gpu_floats_Parameter[id] = rP;

	gpu_floats_M[id] = rM;
	gpu_floats_V[id] = rV;
}

void Cuda_ClipAdamDeltaUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float * gpu_orig_Parameter,

		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,
		int iter,
		int CudaSize)
{
	
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (CudaSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;

	cuda_ClipAdamDeltaUpdate << <nBlockPerGrid, nThreadPerBlock >> >(gpu_floats_Gradient, gpu_floats_M, gpu_floats_V, gpu_floats_Parameter, gpu_orig_Parameter, Beta1, Beta2, Epsilon, updateRate, weightClip,gradClip, decay, iter, CudaSize);
}

__global__ void cuda_UCTStateSampling(float * count,
	float * success,
	float * status,
	int statusNum,
	float * weight,
	int weightLen,
	float C)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id >= weightLen)
		return;

	int posNum = id * statusNum;
	float sum_t = 0;
	for (int i = 0; i < statusNum; i++)
	{
		sum_t += count[posNum + i];
	}

	int maxIdx = 0;
	float maxV = 0;
	for (int i = 0; i < statusNum; i++)
	{
		// UCT Rule.
		float v = success[posNum + i] * 1.0 / count[posNum + i] + C * sqrtf(logf(sum_t) / count[posNum + i]);
		if (v > maxV)
		{
			maxV = v;
			maxIdx = i;
		}
	}
	weight[id] = status[maxIdx];
}


void Cuda_UCTStateSampling(float * count,
	float * success,
	float * status,
	int statusNum,
	float * weight,
	int weightLen,
	float C)
{
	int RowNum = (weightLen - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_UCTStateSampling << <block_tail, thread_tail >> > (count, success, status, statusNum, weight, weightLen, C);
}

__global__ void cuda_BanditStateUpdating(float * fail, float * success, int * status, int armNum, int banditNum, float f, float s, float alpha)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id >= banditNum) return;

	int posNum = id * armNum;
	for (int a = 0; a < armNum; a++)
	{
		fail[posNum + a] = alpha * fail[posNum + a];
		success[posNum + a] = alpha * success[posNum + a];
	}
	posNum = posNum + status[id];
	fail[posNum] = fail[posNum] + f;
	success[posNum] = success[posNum] + s;
}

void Cuda_BanditStateUpdating(float * fail, float * success, int * status, int armNum, int banditNum, float f, float s, float alpha)
{
	int RowNum = (banditNum - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_BanditStateUpdating << <block_tail, thread_tail >> > (fail, success, status, armNum, banditNum, f, s, alpha);
}

__global__ void cuda_GraphNeighborNum(int * srcIdx, int batchSize, int * graphIdx, int * graphNeiNodes, int * graphNeiRels, 
									  int * maskSrc, int * maskRel, int * maskInvRel, int * maskTgt, int * neiNum, int maskType)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int nidx = srcIdx[idx];
		int e = graphIdx[nidx];
		int s = nidx == 0 ? 0 : graphIdx[nidx - 1];

		int mask_src = maskSrc[idx];
		int mask_rel = maskRel[idx];
		int mask_inv_rel = maskInvRel[idx];
		int mask_tgt = maskTgt[idx];

		int num = 0;
		for(int m = s; m < e; m++)
		{
			int sn = graphNeiNodes[m];
			int sr = graphNeiRels[m];

			if(maskType == 0)
			{
				if(nidx == mask_src && sr == mask_rel && sn == mask_tgt) continue;
				if(sn == mask_src && sr == mask_inv_rel && nidx == mask_tgt) continue;
			}
			else if(maskType == 1)
			{
				if(nidx == mask_src && sr == mask_rel ) continue;
				if(sn == mask_src && sr == mask_inv_rel ) continue;
			}
			num += 1;
		}
		neiNum[idx] = num + 1;
	}
}

void Cuda_GraphNeighborNum(int * srcIdx, int batchSize, int * graphIdx, int * graphNeiNodes, int * graphNeiRels, 
							int * maskSrc, int * maskRel, int * maskInvRel, int * maskTgt, int * neiNum, int maskType)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK);
	cuda_GraphNeighborNum << <block_tail, thread_tail >> >(srcIdx, batchSize, graphIdx, graphNeiNodes, graphNeiRels, maskSrc, maskRel, maskInvRel, maskTgt, neiNum, maskType);
}

__global__ void cuda_GraphNeighborInfo(int * srcIdx, int batchSize, int * graphIdx, int * graphNeiNodes, int * graphNeiRels, 
							int * maskSrc, int * maskRel, int * maskInvRel, int * maskTgt, int * neiNum, 
							int * neiNodes, int * neiRels, int * neiMargin, int defaultR, int maskType)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int nidx = srcIdx[idx];
		int e = graphIdx[nidx];
		int s = nidx == 0 ? 0 : graphIdx[nidx - 1];

		int mask_src = maskSrc[idx];
		int mask_rel = maskRel[idx];
		int mask_inv_rel = maskInvRel[idx];
		int mask_tgt = maskTgt[idx];

		int num = 0;
		int b = idx == 0 ? 0 : neiNum[idx - 1];
		for(int m = s; m < e; m++)
		{
			int sn = graphNeiNodes[m];
			int sr = graphNeiRels[m];

			//if(nidx == mask_src && sr == mask_rel && (sn == mask_tgt || mask_tgt == -1)) continue;
			//if(sn == mask_src && sr == mask_inv_rel && (nidx == mask_tgt || mask_tgt == -1)) continue;

			if(maskType == 0)
			{
				if(nidx == mask_src && sr == mask_rel && sn == mask_tgt) continue;
				if(sn == mask_src && sr == mask_inv_rel && nidx == mask_tgt) continue;
			}
			else if(maskType == 1)
			{
				if(nidx == mask_src && sr == mask_rel ) continue;
				if(sn == mask_src && sr == mask_inv_rel ) continue;
			}

			neiNodes[b + num] = sn;
			neiRels[b + num] = sr;
			neiMargin[b + num] = idx;

			num += 1;
		}
		neiNodes[b + num] = nidx;
		neiRels[b + num] = defaultR;
		neiMargin[b + num] = idx;
	}
}

void Cuda_GraphNeighborInfo(int * srcIdx, int batchSize, int * graphIdx, int * graphNeiNodes, int * graphNeiRels, 
							int * maskSrc, int * maskRel, int * maskInvRel, int * maskTgt, int * neiNum, 
							int * neiNodes, int * neiRels, int * neiMargin, int defaultR, int maskType)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_BLOCK);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK);
	cuda_GraphNeighborInfo << <block_tail, thread_tail >> > (srcIdx, batchSize, graphIdx, graphNeiNodes, graphNeiRels, 
														maskSrc, maskRel, maskInvRel, maskTgt, neiNum,
														neiNodes, neiRels, neiMargin, defaultR, maskType);
}

