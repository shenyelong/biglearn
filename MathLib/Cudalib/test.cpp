#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main()
{
	void * dllPtr = dlopen("libCudalib.so", RTLD_LAZY);
	char* error;
	if(dllPtr == 0)
	{
		error = dlerror();
		printf("failed to load Cudalib: %s\r\n", error);
		return -1;
	}

	typedef int (*CudaDeviceCount)();
	CudaDeviceCount cudaDevCount = (CudaDeviceCount)dlsym(dllPtr, "CudaDeviceCount");
	if( (error = dlerror()) != NULL)
	{
		printf("Failed to load the specified method. %s\r\n", error);
	}
	int devCnt = (*cudaDevCount)();
	printf("hello %d gpus\r\n", devCnt);
}
