#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include <curand.h>
#include <curand_kernel.h>
#include "device_functions.h"
#include <fstream>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

using namespace std;

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#endif


// InputMatrix : (x1, x2, x3, ...xT); T : seqLen; x_i : a vector with dimenion : dim;
// WMatrix : Recurrent Matrixs; Column : dim; Row : lag * dim;
// lag : window size of past x_i been used for calculating x_t;
// OutputMatrix : (y1, y2, y3,...yT);
// af : Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
void Cuda_RecursiveForwardPropagate(float * InputMatrix, int seqLen, int dim,
		float * WMatrix, int lag, int af,  float * OutputMatrix)
{
	for(int i=0;i<seqLen;i++)
	{
		float * pOutput = OutputMatrix + i * dim;
		for(int h = 0; h<lag;h++)
		{
			int cursor = i - h - 1;
			if(cursor >= 0)
			{
				float * pInput = InputMatrix + cursor * dim;
				float * pW = WMatrix + h * dim * dim;
				Cuda_VectorProjection(pInput, pW, pOutput, dim, dim, 1);
			}
		}
		if(af == 1)
			Cuda_Tanh(pOutput, pOutput, dim);
		else if(af == 2)
			Cuda_ReLU(pOutput, pOutput, dim);
		else if(af == 3)
			Cuda_Logistic(pOutput, pOutput, dim, 1);
	}
}

__global__ void cuda_CalculateMask(int * MaskMatrix, int seqLen, int batchSize, int src, int tgt, int * mask)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int srcId = MaskMatrix[src * batchSize + idx];
		int tgtId = MaskMatrix[tgt * batchSize + idx];
		if(srcId == tgtId && tgtId >= 0) mask[idx] = 1;
		else mask[idx] = 0;
	}
}

__global__ void cuda_CalculateMask(int * InstanceIndex, int batchSize, int tgt, int * mask)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
		int instanceEnd = InstanceIndex[idx];

		if(instanceEnd - instanceBegin > tgt) mask[idx] = 1;
		else mask[idx] = 0;
	}
}


__global__ void cuda_CalculateLinkMatrix(int * InstanceIndex, int batchSize, float * SeqMatrix, int dim, int tgt, int * mask, int bSum, float **linkMatrix)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < bSum)
	{
		int b = mask[idx] - 1;
		int instanceBegin = b == 0 ? 0 : InstanceIndex[b - 1];
		linkMatrix[idx] = SeqMatrix + (instanceBegin + tgt) * dim;	

		//for (int i = 0; i < dim; i++)
		//	linkMatrix[idx][i] += 1;
	}
}


// MaskMatrix : Row : seqLen; Column : batchSize;
void Cuda_RecursiveForwardPropagateBatch(int * MaskMatrix, int seqLen, int batchSize,
		float * SeqInputMatrixs, int dim,
		float * WMatrix, int lag, int af, float * SeqOutputMatrix)
{
	int * mask;
	cudaMalloc((void **)&mask, batchSize * sizeof(int));
	cudaMemset(mask, 0, batchSize * sizeof(int));
	for(int i=0;i<seqLen;i++)
	{
		float * pOutputMatrix = SeqOutputMatrix + i * batchSize * dim;
		for(int h = 0; h<lag;h++)
		{
			int cursor = i - h - 1;
			if(cursor >= 0)
			{
				float * pInput = SeqInputMatrixs + cursor * batchSize * dim;
				float * pW = WMatrix + h * dim * dim;

				cuda_CalculateMask<<< (batchSize - 1)/ DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK>>>(MaskMatrix, seqLen, batchSize, cursor, i, mask);
				Cuda_MatrixMultiplication_LeftRowMask(pInput, mask, pW, pOutputMatrix, batchSize, dim, dim, 1);
			}
		}
		if(af == 1)
			Cuda_Tanh(pOutputMatrix, pOutputMatrix, dim * batchSize);
		else if(af == 2)
			Cuda_ReLU(pOutputMatrix, pOutputMatrix, dim* batchSize);
		else if(af == 3)
			Cuda_Logistic(pOutputMatrix, pOutputMatrix, dim * batchSize, 1);
	}
	cudaFree(mask);
}




/// InstanceIndex : Accumulate Sequence Length of Input Seqes. i.e., [len of seq1, len of seq1 + len of seq2, ...]
/// batchSize : number of sequences.
void Cuda_RecursiveForwardPropagateBatch(int * InstanceIndex, int batchSize,
		float * SeqInputMatrixs, int dim, 
		float * WMatrix, int lag, int af, float * SeqOutputMatrix)
{
	float ** outputLinkMatrix;
	float ** inputLinkMatrix;
	int * mask;
	int * dev_mask;

	mask = (int *)malloc(batchSize * sizeof(int));

	cudaMalloc((void **)&dev_mask, batchSize * sizeof(int));
	cudaMalloc((void **)&outputLinkMatrix, batchSize * sizeof(float *)); // , cudaHostAllocMapped);
	cudaMalloc((void **)&inputLinkMatrix, batchSize * sizeof(float *)); // , cudaHostAllocMapped);


	int i = 0;
	while(true)
	{
		cuda_CalculateMask<<<(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >>>(InstanceIndex, batchSize, i, dev_mask);

		cudaMemcpy(mask, dev_mask, batchSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		int bSum = 0;
		for (int b = 0; b < batchSize; b++)
		{
			if(mask[b] > 0) 
			{
				mask[bSum] = b+1;
				bSum += 1;
			}
		}
		if(bSum == 0) break;

		cudaMemcpy(dev_mask, mask, batchSize * sizeof(int), cudaMemcpyHostToDevice);

		cuda_CalculateLinkMatrix<<< (bSum - 1)/ DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >>>
			(InstanceIndex, batchSize, SeqOutputMatrix, dim, i, dev_mask, bSum, outputLinkMatrix);


		for (int h = 0; h<lag; h++)
		{
			int cursor = i - h - 1;
			if(cursor >= 0)
			{
				cuda_CalculateLinkMatrix<<<(bSum - 1)/ DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >>>
					(InstanceIndex, batchSize, SeqInputMatrixs, dim, cursor, dev_mask, bSum, inputLinkMatrix);
				float * pW = WMatrix + h * dim * dim;		
				Cuda_MatrixMultiplication(inputLinkMatrix, pW, outputLinkMatrix, bSum, dim, dim, 1);
			}
		}

		if (af == 1)
			Cuda_Tanh(outputLinkMatrix, outputLinkMatrix, bSum, dim);
		else if(af == 2)
			Cuda_ReLU(outputLinkMatrix, outputLinkMatrix, bSum, dim);
		else if(af == 3)
			Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, bSum, dim, 1);

		i = i + 1;
	}

	free(mask);
	cudaFree(dev_mask);
	cudaFree(outputLinkMatrix);
	cudaFree(inputLinkMatrix);
}



// OutputMatrix : (x1, x2, x3, ...xT); T : seqLen; x_i : a vector with dimenion : dim;
// WMatrix : Recurrent Matrixs; Column : dim; Row : lag * dim;
// lag : window size of past x_i been used for calculating x_t;
// DerivMatrix : (y1, y2, y3,...yT);
// af : Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
void Cuda_RecursiveBackwardPropagate(float * OutputMatrix, int seqLen, int dim,
		float * WMatrix, int lag, int af,  float * DerivMatrix)
{
	for(int i=seqLen-1;i>=0;i--)
	{
		float * pDeriv = DerivMatrix + i * dim;
		float * pOutput = OutputMatrix + i * dim;

		if(af == 1)
			Cuda_DerivTanh(pOutput, pDeriv, dim);
		else if(af == 2)
			Cuda_DerivReLU(pOutput, pDeriv, dim);
		else if(af == 3)
			Cuda_DerivLogistic(pOutput, pDeriv, dim, 1);

		///back propagate to history states.
		for(int h = 0; h<lag;h++)
		{
			int cursor = i - h - 1;
			if(cursor >= 0)
			{
				float * pHistoryDeriv = DerivMatrix + cursor * dim;
				float * pW = WMatrix + h * dim * dim;
				Cuda_VectorProjectionTranspose(pDeriv, pW, pHistoryDeriv, dim, dim, 1);
			}
		}
	}
}

__global__ void CalculateInstanceLength(int * InstanceIndex, int batchSize, int * InstanceLen)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		InstanceLen[idx] = idx == 0 ? InstanceIndex[idx] : InstanceIndex[idx] - InstanceIndex[idx-1];
	}
}

/// InstanceIndex : Accumulate Sequence Length of Input Seqes. i.e., [len of seq1, len of seq1 + len of seq2, ...]
/// batchSize : number of sequences.
void Cuda_RecursiveBackwardPropagateBatch(int * InstanceIndex, int batchSize, 
		float * SeqOutputMatrixs, int dim, 
		float * WMatrix, int lag, int af, float * SeqDerivMatrixs)
{
	float ** derivLinkMatrix;
	float ** outputLinkMatrix;

	int * mask;
	int * dev_mask;
	mask = (int *)malloc(batchSize * sizeof(int));
	cudaMalloc((void **)&dev_mask, batchSize * sizeof(int));

	int * instanceLen;
	cudaMalloc((void **)&instanceLen, batchSize * sizeof(int));

	CalculateInstanceLength<<<(batchSize - 1)/ DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >>>
		(InstanceIndex, batchSize, instanceLen);
	int maxSeqLen = Cuda_Max(instanceLen, batchSize);

	//printf("%d max SeqLen", maxSeqLen);

	//cudaHostAlloc((void**)&mask, batchSize * sizeof(int), cudaHostAllocMapped);


	cudaMalloc((void **)&outputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&derivLinkMatrix, batchSize * sizeof(float *));

	for(int i = maxSeqLen - 1; i >=0 ; i--)
	{
		cuda_CalculateMask <<< (batchSize - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize, i, dev_mask);

		cudaMemcpy(mask, dev_mask, batchSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		int bSum = 0;
		for(int b = 0; b < batchSize; b++) 
		{
			if(mask[b] > 0) 
			{
				mask[bSum] = b+1;
				bSum += 1;
			}
		}
		if(bSum == 0) continue;

		cudaMemcpy(dev_mask, mask, batchSize * sizeof(int), cudaMemcpyHostToDevice);

		cuda_CalculateLinkMatrix<<< (bSum - 1)/ 32 + 1, 32 >>>
			(InstanceIndex, batchSize, SeqDerivMatrixs, dim, i, dev_mask, bSum, derivLinkMatrix);

		cuda_CalculateLinkMatrix<<< (bSum - 1)/ 32 + 1, 32 >>>
			(InstanceIndex, batchSize, SeqOutputMatrixs, dim, i, dev_mask, bSum, outputLinkMatrix);


		if(af == 1)
			Cuda_DerivTanh(outputLinkMatrix, derivLinkMatrix, bSum, dim);
		else if(af == 2)
			Cuda_DerivReLU(outputLinkMatrix, derivLinkMatrix, bSum, dim);
		else if(af == 3)
			Cuda_DerivLogistic(outputLinkMatrix, derivLinkMatrix, bSum, dim, 1);

		for(int h = 0; h<lag;h++)
		{
			int cursor = i - h - 1;
			if(cursor >= 0)
			{
				float ** historyDerivMatrix = outputLinkMatrix;
				cuda_CalculateLinkMatrix<<< (bSum - 1)/ 32 + 1, 32 >>>
					(InstanceIndex, batchSize, SeqDerivMatrixs, dim, cursor, dev_mask, bSum, historyDerivMatrix);
				float * pW = WMatrix + h * dim * dim;				
				Cuda_MatrixMultiplicationTranspose(derivLinkMatrix, pW, historyDerivMatrix, bSum, dim, dim, 1);
			}
		}

	}
	free(mask);
	cudaFree(dev_mask);
	cudaFree(instanceLen);
	cudaFree(outputLinkMatrix);
	cudaFree(derivLinkMatrix);
}


__global__ void cuda_CalculateSeqMask(int * InstanceIndex, int batchSize, int lag, int * leftSeqMask, int * rightSeqMask)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
		int instanceEnd = InstanceIndex[idx];

		int lagEnd = min(instanceBegin + lag, instanceEnd); 
		for(int i = instanceBegin; i < lagEnd; i++)
			rightSeqMask[i] = 0;

		for(int i = lagEnd; i < instanceEnd; i++)
			rightSeqMask[i] = 1;

		int lagBegin = max(instanceBegin, instanceEnd - lag);
		for(int i = lagBegin; i < instanceEnd; i++)
			leftSeqMask[i] = 0;

		for(int i = instanceBegin; i < lagBegin; i++)
			leftSeqMask[i] = 1;
	}
}

__global__ void cuda_CalculateSeqLinkMatrixs(float * SeqMatrix, int dim, int * SeqMask, int bSum, float **linkMatrix)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < bSum)
	{
		linkMatrix[idx] = SeqMatrix + (SeqMask[idx] - 1) * dim;		
	}
}


void Cuda_RecursiveUpdateBatch(int * InstanceIndex, int batchSize, int seqSize, float * SeqOutputMatrixs, int dim, float * SeqDerivMatrixs, int lag, float * gradient, float weight)
{
	int * leftSeqMask;
	int * rightSeqMask;

	float ** derivLinkMatrix;
	float ** outputLinkMatrix;

	int * leftSeqMaskHost;
	int * rightSeqMaskHost;

	cudaMalloc((void **)&outputLinkMatrix, seqSize * sizeof(float *));
	cudaMalloc((void **)&derivLinkMatrix, seqSize * sizeof(float *));

	cudaMalloc((void**)&leftSeqMask, seqSize * sizeof(int)); // , cudaHostAllocMapped);
	cudaMalloc((void**)&rightSeqMask, seqSize * sizeof(int));// , cudaHostAllocMapped);


	leftSeqMaskHost = (int *)malloc(seqSize * sizeof(int));
	rightSeqMaskHost = (int *)malloc(seqSize * sizeof(int));


	for(int h = 0; h < lag; h++)
	{
		cuda_CalculateSeqMask<<< (batchSize + 32 - 1) / 32, 32 >>>(InstanceIndex, batchSize, h + 1, leftSeqMask, rightSeqMask);

		cudaMemcpy(leftSeqMaskHost, leftSeqMask, seqSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		cudaMemcpy(rightSeqMaskHost, rightSeqMask, seqSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);

		int bLeftSum = 0;
		int bRightSum = 0;
		for(int b = 0; b < seqSize; b++) 
		{
			if (leftSeqMaskHost[b] > 0)
			{
				leftSeqMaskHost[bLeftSum] = b + 1;
				bLeftSum += 1;
			}
			if (rightSeqMaskHost[b] > 0)
			{
				rightSeqMaskHost[bRightSum] = b + 1;
				bRightSum += 1;
			}
		}
		if(bLeftSum != bRightSum)
		{
			printf("Cuda_RecursiveUpdateBatch bLeftSum != bRightSum; Error! ");
			break;
		}
		if(bLeftSum == 0) break;

		cudaMemcpy(leftSeqMask, leftSeqMaskHost, seqSize * sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(rightSeqMask, rightSeqMaskHost, seqSize * sizeof(int), cudaMemcpyHostToDevice);

		cuda_CalculateSeqLinkMatrixs<<< (bLeftSum + 512 - 1) / 512, 512 >>> (SeqOutputMatrixs, dim, leftSeqMask, bLeftSum, outputLinkMatrix);
		cuda_CalculateSeqLinkMatrixs<<< (bRightSum + 512 - 1) / 512, 512 >>> (SeqDerivMatrixs, dim, rightSeqMask, bRightSum, derivLinkMatrix);
		Cuda_MatrixMultiplicationLeftTranspose(outputLinkMatrix, derivLinkMatrix, gradient + h * dim * dim, bLeftSum, dim, dim, 1, weight);
	}

	free(leftSeqMaskHost);
	free(rightSeqMaskHost);

	cudaFree(leftSeqMask);
	cudaFree(rightSeqMask);

	cudaFree(outputLinkMatrix);
	cudaFree(derivLinkMatrix);
}



__global__ void cuda_CalculateLastLinkMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float **linkMatrix)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
		int instanceEnd = InstanceIndex[idx];
		if (instanceEnd > instanceBegin)
			linkMatrix[idx] = SeqMatrixs + (instanceEnd - 1) * dim;
		else
			linkMatrix[idx] = 0;
	}
}

__global__ void cuda_CalculateFirstLinkMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float **linkMatrix)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int instanceBegin = idx == 0 ? 0 : InstanceIndex[idx - 1];
		int instanceEnd = InstanceIndex[idx];
		if (instanceEnd > instanceBegin)
			linkMatrix[idx] = SeqMatrixs + instanceBegin * dim;
		else
			linkMatrix[idx] = 0;
	}
}

void Cuda_RecursiveExtendUpdateBatch(int * InstanceIndex, int batchSize, int seqSize, float * SeqOutputMatrixs,
		int * pre_Idx, //-> previous output,
		int pre_batchSize,  //->x;
		float * pre_O,
		int dim, float * SeqDerivMatrixs, int lag, float * gradient, float weight)
{
	int * leftSeqMask;
	int * rightSeqMask;

	float ** derivLinkMatrix;
	float ** outputLinkMatrix;

	int * leftSeqMaskHost;
	int * rightSeqMaskHost;

	cudaMalloc((void **)&outputLinkMatrix, seqSize * sizeof(float *));
	cudaMalloc((void **)&derivLinkMatrix, seqSize * sizeof(float *));

	cudaMalloc((void**)&leftSeqMask, seqSize * sizeof(int)); // , cudaHostAllocMapped);
	cudaMalloc((void**)&rightSeqMask, seqSize * sizeof(int));// , cudaHostAllocMapped);


	leftSeqMaskHost = (int *)malloc(seqSize * sizeof(int));
	rightSeqMaskHost = (int *)malloc(seqSize * sizeof(int));


	for (int h = 0; h < lag; h++)
	{
		cuda_CalculateSeqMask << < (batchSize + 32 - 1) / 32, 32 >> >(InstanceIndex, batchSize, h + 1, leftSeqMask, rightSeqMask);

		cudaMemcpy(leftSeqMaskHost, leftSeqMask, seqSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		cudaMemcpy(rightSeqMaskHost, rightSeqMask, seqSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);

		int bLeftSum = 0;
		int bRightSum = 0;
		for (int b = 0; b < seqSize; b++)
		{
			if (leftSeqMaskHost[b] > 0)
			{
				leftSeqMaskHost[bLeftSum] = b + 1;
				bLeftSum += 1;
			}
			if (rightSeqMaskHost[b] > 0)
			{
				rightSeqMaskHost[bRightSum] = b + 1;
				bRightSum += 1;
			}
		}
		if (bLeftSum != bRightSum)
		{
			printf("Cuda_RecursiveUpdateBatch bLeftSum != bRightSum; Error! ");
			break;
		}
		if (bLeftSum == 0) break;

		cudaMemcpy(leftSeqMask, leftSeqMaskHost, seqSize * sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(rightSeqMask, rightSeqMaskHost, seqSize * sizeof(int), cudaMemcpyHostToDevice);

		cuda_CalculateSeqLinkMatrixs << < (bLeftSum + 512 - 1) / 512, 512 >> > (SeqOutputMatrixs, dim, leftSeqMask, bLeftSum, outputLinkMatrix);
		cuda_CalculateSeqLinkMatrixs << < (bRightSum + 512 - 1) / 512, 512 >> > (SeqDerivMatrixs, dim, rightSeqMask, bRightSum, derivLinkMatrix);
		Cuda_MatrixMultiplicationLeftTranspose(outputLinkMatrix, derivLinkMatrix, gradient + h * dim * dim, bLeftSum, dim, dim, 1, weight);

		if (h == 0)
		{
			cuda_CalculateLastLinkMatrixs << <(pre_batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(
					pre_Idx, pre_batchSize, pre_O, dim, outputLinkMatrix);

			cuda_CalculateFirstLinkMatrixs << < (pre_batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(
					pre_Idx, pre_batchSize, SeqDerivMatrixs, dim, derivLinkMatrix);

			Cuda_MatrixMultiplicationLeftTranspose(outputLinkMatrix, derivLinkMatrix, gradient, pre_batchSize, dim, dim, 1, weight);
		}
	}

	free(leftSeqMaskHost);
	free(rightSeqMaskHost);

	cudaFree(leftSeqMask);
	cudaFree(rightSeqMask);

	cudaFree(outputLinkMatrix);
	cudaFree(derivLinkMatrix);
}


void Cuda_LSTMForwardPropagateP1(int * InstanceIndex, int batchSize,
		float * o, float * gate_i, float * gate_f, float * gate_o, float * c_hat, int cell,
		float * Ui, float * Uf, float * Uo, float * Uc,
		int * dev_mask, int i, int row,
		float ** inputLinkMatrix, float ** outputLinkMatrix)
{

	/// obtain gate_i(t)
	cuda_CalculateLinkMatrix << < (row - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(InstanceIndex, batchSize, gate_i, cell, i, dev_mask, row, outputLinkMatrix);
	/// gate_i(t) = gate_i(t) + U_i * o_t-1
	Cuda_MatrixMultiplication(inputLinkMatrix, Ui, outputLinkMatrix, row, cell, cell, 1);
	/// sigmoid(gate_i(t))
	//Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, row, cell, 1);

	/// obtain gate_f(t)
	cuda_CalculateLinkMatrix << < (row - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(InstanceIndex, batchSize, gate_f, cell, i, dev_mask, row, outputLinkMatrix);
	/// gate_f(t) = gate_f(t) + U_f * o_t-1
	Cuda_MatrixMultiplication(inputLinkMatrix, Uf, outputLinkMatrix, row, cell, cell, 1);
	/// sigmoid(gate_f(t))
	//Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, row, cell, 1);

	/// obtain gate_o(t)
	cuda_CalculateLinkMatrix << < (row - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(InstanceIndex, batchSize, gate_o, cell, i, dev_mask, row, outputLinkMatrix);
	/// gate_o(t) = gate_o(t) + U_o * o_t-1
	Cuda_MatrixMultiplication(inputLinkMatrix, Uo, outputLinkMatrix, row, cell, cell, 1);

	/// obtain c_hat(t)
	cuda_CalculateLinkMatrix << < (row - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(InstanceIndex, batchSize, c_hat, cell, i, dev_mask, row, outputLinkMatrix);
	/// c_hat(t) = c_hat(t) + U_c * o_t-1
	Cuda_MatrixMultiplication(inputLinkMatrix, Uc, outputLinkMatrix, row, cell, cell, 1);
	/// tanh(c_hat(t))
	//Cuda_Tanh(outputLinkMatrix, outputLinkMatrix, srcSum, cell);

}

void Cuda_LSTMExtendForwardPropagateBatch(
		int * srcInstanceIdx, int srcBatchSize,  //->src_x;
		float * pre_o, //-> previous output,
		float * pre_c, //-> previous c,
		int * InstanceIndex, int batchSize,  //->x;
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell,
		float * Ui,
		float * Uc, 
		float * Uf,
		float * Uo, float * Vo)

{
	int * mask;
	int * dev_mask;

	mask = (int *)malloc(batchSize * sizeof(int));
	cudaMalloc((void **)&dev_mask, batchSize * sizeof(int));

	float ** inputLinkMatrix;
	float ** outputLinkMatrix;
	float ** targetLinkMatrix;

	cudaMalloc((void **)&inputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&outputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&targetLinkMatrix, batchSize * sizeof(float *));

	int i = 0;
	while (true)
	{
		cuda_CalculateMask <<<(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(InstanceIndex, batchSize, i, dev_mask);
		cudaMemcpy(mask, dev_mask, batchSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		int Sum = 0;
		for (int b = 0; b < batchSize; b++)
		{
			if (mask[b] > 0)
			{
				mask[Sum] = b + 1;
				Sum += 1;
			}
		}
		if (Sum == 0) break;
		if (i == 0 && Sum != batchSize)
		{
			printf("It contains empty instance in minibatch, which is not allowed!");
			break;
		}

		/// calculate gate_i, chat_t, gate_f, gate_o according to previous output o(t-1)
		cudaMemcpy(dev_mask, mask, batchSize * sizeof(int), cudaMemcpyHostToDevice);

		if (i == 0)
		{
			/// input = pre_o(last)
			cuda_CalculateLastLinkMatrixs << <(srcBatchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(
					srcInstanceIdx, srcBatchSize, pre_o, cell, inputLinkMatrix);
		}
		else if (i > 0)
		{
			/// input = o(t-1)
			cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
				(InstanceIndex, batchSize, o, cell, i - 1, dev_mask, Sum, inputLinkMatrix);
		}
		Cuda_LSTMForwardPropagateP1(InstanceIndex, batchSize,
				o, gate_i, gate_f, gate_o, c_hat, cell,
				Ui, Uf, Uo, Uc,
				dev_mask, i, Sum,
				inputLinkMatrix, outputLinkMatrix);

		/// gate_i = logistic(gate_i);
		/// output = gate_i(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_i, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		/// c_hat = tanh(c_hat);
		/// input = c_hat(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, c_hat, cell, i, dev_mask, Sum, inputLinkMatrix);
		Cuda_Tanh(inputLinkMatrix, inputLinkMatrix, Sum, cell);

		/// c = gate_i * c_hat
		/// target = c(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, c, cell, i, dev_mask, Sum, targetLinkMatrix);
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 0);

		/// gate_f = logistic(gate_f);
		/// output = gate_f(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_f, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		if (i == 0)
			/// input = pre_c(last)
			cuda_CalculateLastLinkMatrixs << < (batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
				(srcInstanceIdx, srcBatchSize, pre_c, cell, inputLinkMatrix);
		else
			/// input = c(t-1)
			cuda_CalculateLinkMatrix <<< (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >>>
				(InstanceIndex, batchSize, c, cell, i - 1, dev_mask, Sum, inputLinkMatrix);

		/// c(t) = c(t) + gate_f(t) * c(t-1)
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 1);

		/// gate_o(t) = Vo * c(t)
		/// output = gate_o(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_o, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_MatrixMultiplication(targetLinkMatrix, Vo, outputLinkMatrix, Sum, cell, cell, 1);
		/// gate_o = logistic(gate_o)
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		/// obtain tanhc(t)
		/// input = tanhc
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, tanhc, cell, i, dev_mask, Sum, inputLinkMatrix);
		/// tanhc = tanh(c)
		Cuda_Tanh(targetLinkMatrix, inputLinkMatrix, Sum, cell);

		/// obtain o(t)
		/// target = o(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, o, cell, i, dev_mask, Sum, targetLinkMatrix);
		/// o = gate_o * tanhc
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 0);

		i = i + 1;
	}
	free(mask);
	cudaFree(dev_mask);
	cudaFree(inputLinkMatrix);
	cudaFree(outputLinkMatrix);
	cudaFree(targetLinkMatrix);
}


void Cuda_LSTMLinkExtendForwardPropagateBatch(
		float * pre_o, //-> previous output,
		float * pre_c, //-> previous c,
		int * InstanceIndex, int batchSize,  //->x;
		int * pre_idx,
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell,
		float * Ui,
		float * Uc,
		float * Uf,
		float * Uo, float * Vo)
{
	int * mask;
	int * dev_mask;

	mask = (int *)malloc(batchSize * sizeof(int));
	cudaMalloc((void **)&dev_mask, batchSize * sizeof(int));

	float ** inputLinkMatrix;
	float ** outputLinkMatrix;
	float ** targetLinkMatrix;

	cudaMalloc((void **)&inputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&outputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&targetLinkMatrix, batchSize * sizeof(float *));

	int i = 0;
	while (true)
	{
		cuda_CalculateMask << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(InstanceIndex, batchSize, i, dev_mask);
		cudaMemcpy(mask, dev_mask, batchSize * sizeof(int), cudaMemcpyDeviceToHost); 
		int Sum = 0;
		for (int b = 0; b < batchSize; b++)
		{
			if (mask[b] > 0)
			{
				mask[Sum] = b + 1;
				Sum += 1;
			}
		}
		if (Sum == 0) break;
		if (i == 0 && Sum != batchSize)
		{
			printf("It contains empty instance in minibatch, which is not allowed!");
			break;
		}

		/// calculate gate_i, chat_t, gate_f, gate_o according to previous output o(t-1)
		cudaMemcpy(dev_mask, mask, batchSize * sizeof(int), cudaMemcpyHostToDevice);

		if (i == 0)
		{
			/// input = pre_o(last)
			cuda_CalculateSeqLinkMatrixs << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(
					pre_o, cell, pre_idx, batchSize, inputLinkMatrix);
			//cuda_CalculateLastLinkMatrixs << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(
			//	pre_idx, batchSize, pre_o, cell, inputLinkMatrix);
		}
		else if (i > 0)
		{
			/// input = o(t-1)
			cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
				(InstanceIndex, batchSize, o, cell, i - 1, dev_mask, Sum, inputLinkMatrix);
		}

		Cuda_LSTMForwardPropagateP1(InstanceIndex, batchSize,
				o, gate_i, gate_f, gate_o, c_hat, cell,
				Ui, Uf, Uo, Uc,
				dev_mask, i, Sum,
				inputLinkMatrix, outputLinkMatrix);


		/// gate_i = logistic(gate_i);
		/// output = gate_i(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_i, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		/// c_hat = tanh(c_hat);
		/// input = c_hat(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, c_hat, cell, i, dev_mask, Sum, inputLinkMatrix);
		Cuda_Tanh(inputLinkMatrix, inputLinkMatrix, Sum, cell);

		/// c = gate_i * c_hat
		/// target = c(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, c, cell, i, dev_mask, Sum, targetLinkMatrix);
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 0);

		/// gate_f = logistic(gate_f);
		/// output = gate_f(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_f, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		if (i == 0)
		{
			/// input = pre_c(last)
			cuda_CalculateSeqLinkMatrixs << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(
					pre_c, cell, pre_idx, batchSize, inputLinkMatrix);
			//cuda_CalculateLastLinkMatrixs << < (batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			//	(InstanceIndex, batchSize, pre_c, cell, inputLinkMatrix);
		}
		else
			/// input = c(t-1)
			cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
				(InstanceIndex, batchSize, c, cell, i - 1, dev_mask, Sum, inputLinkMatrix);

		/// c(t) = c(t) + gate_f(t) * c(t-1)
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 1);

		/// gate_o(t) = gate_o(t) + Vo * c(t)
		/// output = gate_o(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_o, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_MatrixMultiplication(targetLinkMatrix, Vo, outputLinkMatrix, Sum, cell, cell, 1);
		/// gate_o = logistic(gate_o)
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		/// input = tanhc
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, tanhc, cell, i, dev_mask, Sum, inputLinkMatrix);
		/// tanhc = tanh(c)
		Cuda_Tanh(targetLinkMatrix, inputLinkMatrix, Sum, cell);

		/// target = o(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, o, cell, i, dev_mask, Sum, targetLinkMatrix);
		/// o = gate_o * tanhc
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 0);

		i = i + 1;
	}
	free(mask);
	cudaFree(dev_mask);
	cudaFree(inputLinkMatrix);
	cudaFree(outputLinkMatrix);
	cudaFree(targetLinkMatrix);
}


void Cuda_LSTMForwardPropagateBatch(int * InstanceIndex, int batchSize,  //->x;
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell,
		float * Ui,
		float * Uc,
		float * Uf,
		float * Uo, float * Vo)

{
	int * mask;
	int * dev_mask;

	mask = (int *)malloc(batchSize * sizeof(int));
	cudaMalloc((void **)&dev_mask, batchSize * sizeof(int));

	float ** inputLinkMatrix;
	float ** outputLinkMatrix;
	float ** targetLinkMatrix;

	cudaMalloc((void **)&inputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&outputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&targetLinkMatrix, batchSize * sizeof(float *));

	int i = 0;
	while (true)
	{
		cuda_CalculateMask << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(InstanceIndex, batchSize, i, dev_mask);
		cudaMemcpy(mask, dev_mask, batchSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		int Sum = 0;
		for (int b = 0; b < batchSize; b++)
		{
			if (mask[b] > 0)
			{
				mask[Sum] = b + 1;
				Sum += 1;
			}
		}
		if (Sum == 0) break;

		/// calculate gate_i, chat_t, gate_f, gate_o according to previous output o(t-1)

		cudaMemcpy(dev_mask, mask, batchSize * sizeof(int), cudaMemcpyHostToDevice);

		if (i > 0)
		{
			/// obtain output(t-1)
			cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
				(InstanceIndex, batchSize, o, cell, i - 1, dev_mask, Sum, inputLinkMatrix);

			Cuda_LSTMForwardPropagateP1(InstanceIndex, batchSize,
					o, gate_i, gate_f, gate_o, c_hat, cell,
					Ui, Uf, Uo, Uc,
					dev_mask, i, Sum,
					inputLinkMatrix, outputLinkMatrix);
		}

		/// gate_i = logistic(gate_i);
		/// output = gate_i(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_i, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		/// c_hat = tanh(c_hat);
		/// input = c_hat(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, c_hat, cell, i, dev_mask, Sum, inputLinkMatrix);
		Cuda_Tanh(inputLinkMatrix, inputLinkMatrix, Sum, cell);

		/// c = gate_i * c_hat
		/// target = c(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, c, cell, i, dev_mask, Sum, targetLinkMatrix);
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 0);

		/// gate_f = logistic(gate_f);
		/// output = gate_f(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_f, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		if (i > 0)
		{
			/// c(t) = c(t) + gate_f(t) * c(t-1)
			/// input = c(t-1)
			cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
				(InstanceIndex, batchSize, c, cell, i - 1, dev_mask, Sum, inputLinkMatrix);
			Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 1);
		}

		/// gate_o(t) = Vo * c(t)
		/// output = gate_o(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, gate_o, cell, i, dev_mask, Sum, outputLinkMatrix);
		Cuda_MatrixMultiplication(targetLinkMatrix, Vo, outputLinkMatrix, Sum, cell, cell, 1);
		/// gate_o = logistic(gate_o)
		Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		/// obtain tanhc(t)
		/// input = tanhc
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, tanhc, cell, i, dev_mask, Sum, inputLinkMatrix);
		/// tanhc = tanh(c)
		Cuda_Tanh(targetLinkMatrix, inputLinkMatrix, Sum, cell);

		/// obtain o(t)
		/// target = o(t)
		cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			(InstanceIndex, batchSize, o, cell, i, dev_mask, Sum, targetLinkMatrix);
		/// o = gate_o * tanhc
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 0);

		i = i + 1;
	}
	free(mask);
	cudaFree(dev_mask);
	cudaFree(inputLinkMatrix);
	cudaFree(outputLinkMatrix);
	cudaFree(targetLinkMatrix);
}



void Cuda_LSTMBackwardPropagateBatch(int * InstanceIndex, int batchSize, int seqSize, int cell,
		float * o,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,
		float * tanhc,

		float * deriv_o,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * deiv_tanhc,

		float * Ui, float * Uc, float * Uf, float * Uo, float * Vo)
{
	int * instanceLen;
	cudaMalloc((void **)&instanceLen, batchSize * sizeof(int));
	CalculateInstanceLength << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(InstanceIndex, batchSize, instanceLen);
	int maxSeqLen = Cuda_Max(instanceLen, batchSize);

	int * mask;
	int * dev_mask;
	mask = (int *)malloc(batchSize * sizeof(int));
	cudaMalloc((void **)&dev_mask, batchSize * sizeof(int));

	float ** inputLinkMatrix;
	float ** outputLinkMatrix;
	float ** targetLinkMatrix;
	float ** midLinkMatrix;
	float ** leftLinkMatrix;
	float ** rightLinkMatrix;
	float ** reserveLinkMatrix;

	cudaMalloc((void **)&inputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&outputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&targetLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&midLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&leftLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&rightLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&reserveLinkMatrix, batchSize * sizeof(float *));

	for (int i = maxSeqLen - 1; i >= 0; i--)
	{
		cuda_CalculateMask << < (batchSize - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize, i, dev_mask);

		cudaMemcpy(mask, dev_mask, batchSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		int bSum = 0;
		for (int b = 0; b < batchSize; b++)
		{
			if (mask[b] > 0)
			{
				mask[bSum] = b + 1;
				bSum += 1;
			}
		}
		if (bSum == 0) continue;
		cudaMemcpy(dev_mask, mask, batchSize * sizeof(int), cudaMemcpyHostToDevice);

		/// input <--- deriv_o(t);
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_o, cell, i, dev_mask, bSum, inputLinkMatrix);

		/// output <--- tanhc(t);
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				tanhc, cell, i, dev_mask, bSum, outputLinkMatrix);

		/// target <--- deriv_gate_o(t);
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_gate_o, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_gate_o = deriv_o * tanhc;
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		/// mid <---- gate_o(t);
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				gate_o, cell, i, dev_mask, bSum, midLinkMatrix);

		/// deriv_gate_o = deriv_gate_o * gate_o * ( 1- gate_o);
		Cuda_DerivLogistic(midLinkMatrix, targetLinkMatrix, bSum, cell, 1);

		/// left <---- deriv_c(t).
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_c, cell, i, dev_mask, bSum, leftLinkMatrix);

		/// deriv_c(t) += deriv_gate_o(t) * Vo;
		Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Vo, leftLinkMatrix, bSum, cell, cell, 1);

		/// deriv_o(t-1) += deriv_gate_o(t) * Uo;
		if (i > 0)
		{
			/// reserve <---- deriv_o(t-1)
			cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
					deriv_o, cell, i - 1, dev_mask, bSum, reserveLinkMatrix);

			/// deriv_o(t-1) += deriv_gate_o(t) * Uo;
			Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Uo, reserveLinkMatrix, bSum, cell, cell, 1);
		}

		/// target <---- deiv_tanhc(t)
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deiv_tanhc, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_tanhc = deriv_o * gate_o;
		Cuda_ElementwiseProduct(inputLinkMatrix, midLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		/// deriv_tanhc = deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
		Cuda_DerivTanh(outputLinkMatrix, targetLinkMatrix, bSum, cell);

		/// deriv_c (t) += deriv_tanhc(t);
		Cuda_Matrix_Add(targetLinkMatrix, leftLinkMatrix, leftLinkMatrix, bSum, cell, 1, 1, 0);

		// from deriv_c -> deriv_gate_i, deriv_chat, deriv_gate_f, deriv_c(t-1)

		// input <---- gate_i
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				gate_i, cell, i, dev_mask, bSum, inputLinkMatrix);

		// output <--- deriv_cHat
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_cHat, cell, i, dev_mask, bSum, outputLinkMatrix);

		/// deriv_cHat = deriv_c * gate_i;
		Cuda_ElementwiseProduct(leftLinkMatrix, inputLinkMatrix, outputLinkMatrix, bSum, cell, 0);

		// right <----- cHat
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				c_hat, cell, i, dev_mask, bSum, rightLinkMatrix);

		// target <----- deriv_gate_i
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_gate_i, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_gate_i = deriv_c * cHat;
		Cuda_ElementwiseProduct(leftLinkMatrix, rightLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		/// deriv_gate_i = deriv_gate_i * gate_i * (1-gate_i)
		Cuda_DerivLogistic(inputLinkMatrix, targetLinkMatrix, bSum, cell, 1);

		/// deriv_cHat = deriv_cHat * (1 + cHat) * (1-cHat)
		Cuda_DerivTanh(rightLinkMatrix, outputLinkMatrix, bSum, cell);

		if (i > 0)
		{
			/// deriv_o(t-1) += deriv_gate_i(t) * Ui;
			Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Ui, reserveLinkMatrix, bSum, cell, cell, 1);

			// deriv_o(t-1) += deriv_cHat(t) * Uc;
			Cuda_MatrixMultiplicationTranspose(outputLinkMatrix, Uc, reserveLinkMatrix, bSum, cell, cell, 1);

			// input <----- deriv_c(t-1)
			cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
					deriv_c, cell, i - 1, dev_mask, bSum, inputLinkMatrix);

			// output <------- gate_f(t)
			cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
					gate_f, cell, i, dev_mask, bSum, outputLinkMatrix);

			/// deriv_c(t-1) += deriv_c * gate_f;
			Cuda_ElementwiseProduct(leftLinkMatrix, outputLinkMatrix, inputLinkMatrix, bSum, cell, 1);

			// target <------- c(t-1)
			cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
					c, cell, i - 1, dev_mask, bSum, targetLinkMatrix);

			// right <-------- deriv_gate_f;
			cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
					deriv_gate_f, cell, i, dev_mask, bSum, rightLinkMatrix);

			// deriv_gate_f = deriv_c * c(t-1);
			Cuda_ElementwiseProduct(leftLinkMatrix, targetLinkMatrix, rightLinkMatrix, bSum, cell, 0);

			// deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
			Cuda_DerivLogistic(outputLinkMatrix, rightLinkMatrix, bSum, cell, 1);

			/// deriv_o(t-1) += deriv_gate_f(t) * Uf;
			Cuda_MatrixMultiplicationTranspose(rightLinkMatrix, Uf, reserveLinkMatrix, bSum, cell, cell, 1);
		}
	}

	free(mask);
	cudaFree(instanceLen);
	cudaFree(dev_mask);
	cudaFree(inputLinkMatrix);
	cudaFree(outputLinkMatrix);
	cudaFree(targetLinkMatrix);
	cudaFree(midLinkMatrix);
	cudaFree(leftLinkMatrix);
	cudaFree(rightLinkMatrix);
	cudaFree(reserveLinkMatrix);
}



void Cuda_LSTMExtendBackwardPropagateBatch(
		int * preInstanceIdx, int preBatchSize,  //->src_x;
		float * pre_o, //-> previous output,
		float * pre_c, //-> previous c,
		float * deriv_pre_o,	// -> previous deriv_o
		float * deriv_pre_c,	// -> previous deriv_c

		int * InstanceIndex, int batchSize, int seqSize, int cell,
		float * o,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,
		float * tanhc,

		float * deriv_o,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * deiv_tanhc,

		float * Ui, float * Uc, float * Uf, float * Uo, float * Vo)
{
	int * instanceLen;
	cudaMalloc((void **)&instanceLen, batchSize * sizeof(int));
	CalculateInstanceLength << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(InstanceIndex, batchSize, instanceLen);
	int maxSeqLen = Cuda_Max(instanceLen, batchSize);

	int * mask;
	int * dev_mask;
	mask = (int *)malloc(batchSize * sizeof(int));
	cudaMalloc((void **)&dev_mask, batchSize * sizeof(int));

	float ** inputLinkMatrix;
	float ** outputLinkMatrix;
	float ** targetLinkMatrix;
	float ** midLinkMatrix;
	float ** leftLinkMatrix;
	float ** rightLinkMatrix;
	float ** reserveLinkMatrix;

	cudaMalloc((void **)&inputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&outputLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&targetLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&midLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&leftLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&rightLinkMatrix, batchSize * sizeof(float *));
	cudaMalloc((void **)&reserveLinkMatrix, batchSize * sizeof(float *));

	for (int i = maxSeqLen - 1; i >= 0; i--)
	{
		cuda_CalculateMask << < (batchSize - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize, i, dev_mask);

		cudaMemcpy(mask, dev_mask, batchSize * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		int bSum = 0;
		for (int b = 0; b < batchSize; b++)
		{
			if (mask[b] > 0)
			{
				mask[bSum] = b + 1;
				bSum += 1;
			}
		}
		if (bSum == 0) continue;
		cudaMemcpy(dev_mask, mask, batchSize * sizeof(int), cudaMemcpyHostToDevice);

		/// input <--- deriv_o(t);
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_o, cell, i, dev_mask, bSum, inputLinkMatrix);

		/// output <--- tanhc(t);
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				tanhc, cell, i, dev_mask, bSum, outputLinkMatrix);

		/// target <--- deriv_gate_o(t);
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_gate_o, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_gate_o = deriv_o * tanhc;
		Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		/// mid <---- gate_o(t);
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				gate_o, cell, i, dev_mask, bSum, midLinkMatrix);

		/// deriv_gate_o = deriv_gate_o * gate_o * ( 1- gate_o);
		Cuda_DerivLogistic(midLinkMatrix, targetLinkMatrix, bSum, cell, 1);

		/// left <---- deriv_c(t).
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_c, cell, i, dev_mask, bSum, leftLinkMatrix);

		/// deriv_c(t) += deriv_gate_o(t) * Vo;
		Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Vo, leftLinkMatrix, bSum, cell, cell, 1);

		/// deriv_o(t-1) += deriv_gate_o(t) * Uo;
		if (i > 0)
		{
			/// reserve <---- deriv_o(t-1)
			cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
					deriv_o, cell, i - 1, dev_mask, bSum, reserveLinkMatrix);
		}
		else
		{
			/// reserve <-------deriv_o(t-1) = pre_deriv_o
			cuda_CalculateLastLinkMatrixs << <(preBatchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(
					preInstanceIdx, preBatchSize, deriv_pre_o, cell, reserveLinkMatrix);
		}

		/// deriv_o(t-1) += deriv_gate_o(t) * Uo;
		Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Uo, reserveLinkMatrix, bSum, cell, cell, 1);

		/// target <---- deiv_tanhc(t)
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deiv_tanhc, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_tanhc = deriv_o * gate_o;
		Cuda_ElementwiseProduct(inputLinkMatrix, midLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		/// deriv_tanhc = deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
		Cuda_DerivTanh(outputLinkMatrix, targetLinkMatrix, bSum, cell);

		/// deriv_c (t) += deriv_tanhc(t);
		Cuda_Matrix_Add(targetLinkMatrix, leftLinkMatrix, leftLinkMatrix, bSum, cell, 1, 1, 0);

		// from deriv_c -> deriv_gate_i, deriv_chat, deriv_gate_f, deriv_c(t-1)

		// input <---- gate_i
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				gate_i, cell, i, dev_mask, bSum, inputLinkMatrix);

		// output <--- deriv_cHat
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_cHat, cell, i, dev_mask, bSum, outputLinkMatrix);

		/// deriv_cHat = deriv_c * gate_i;
		Cuda_ElementwiseProduct(leftLinkMatrix, inputLinkMatrix, outputLinkMatrix, bSum, cell, 0);

		// right <----- cHat
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				c_hat, cell, i, dev_mask, bSum, rightLinkMatrix);

		// target <----- deriv_gate_i
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_gate_i, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_gate_i = deriv_c * cHat;
		Cuda_ElementwiseProduct(leftLinkMatrix, rightLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		/// deriv_gate_i = deriv_gate_i * gate_i * (1-gate_i)
		Cuda_DerivLogistic(inputLinkMatrix, targetLinkMatrix, bSum, cell, 1);

		/// deriv_cHat = deriv_cHat * (1 + cHat) * (1-cHat)
		Cuda_DerivTanh(rightLinkMatrix, outputLinkMatrix, bSum, cell);


		/// deriv_o(t-1) += deriv_gate_i(t) * Ui;
		Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Ui, reserveLinkMatrix, bSum, cell, cell, 1);

		// deriv_o(t-1) += deriv_cHat(t) * Uc;
		Cuda_MatrixMultiplicationTranspose(outputLinkMatrix, Uc, reserveLinkMatrix, bSum, cell, cell, 1);

		if (i > 0)
		{
			// input <----- deriv_c(t-1)
			cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
					deriv_c, cell, i - 1, dev_mask, bSum, inputLinkMatrix);

			// target <------- c(t-1)
			cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
					c, cell, i - 1, dev_mask, bSum, targetLinkMatrix);
		}
		else
		{
			// input <----- deriv_pre_c(t-1)
			cuda_CalculateLastLinkMatrixs << <(bSum - 1) / 32 + 1, 32 >> >(preInstanceIdx, batchSize,
					deriv_pre_c, cell, inputLinkMatrix);

			// target <------- pre_c(t-1)
			cuda_CalculateLastLinkMatrixs << <(bSum - 1) / 32 + 1, 32 >> >(preInstanceIdx, batchSize,
					pre_c, cell, targetLinkMatrix);
		}
		// output <------- gate_f(t)
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				gate_f, cell, i, dev_mask, bSum, outputLinkMatrix);

		/// deriv_c(t-1) += deriv_c * gate_f;
		Cuda_ElementwiseProduct(leftLinkMatrix, outputLinkMatrix, inputLinkMatrix, bSum, cell, 1);

		// right <-------- deriv_gate_f;
		cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
				deriv_gate_f, cell, i, dev_mask, bSum, rightLinkMatrix);

		// deriv_gate_f = deriv_c * c(t-1);
		Cuda_ElementwiseProduct(leftLinkMatrix, targetLinkMatrix, rightLinkMatrix, bSum, cell, 0);

		// deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
		Cuda_DerivLogistic(outputLinkMatrix, rightLinkMatrix, bSum, cell, 1);

		/// deriv_o(t-1) += deriv_gate_f(t) * Uf;
		Cuda_MatrixMultiplicationTranspose(rightLinkMatrix, Uf, reserveLinkMatrix, bSum, cell, cell, 1);
	}

	free(mask);
	cudaFree(instanceLen);
	cudaFree(dev_mask);
	cudaFree(inputLinkMatrix);
	cudaFree(outputLinkMatrix);
	cudaFree(targetLinkMatrix);
	cudaFree(midLinkMatrix);
	cudaFree(leftLinkMatrix);
	cudaFree(rightLinkMatrix);
	cudaFree(reserveLinkMatrix);
}



__global__ void cuda_CalculateSrcTgtLabelMask(float * srcLabels, int srcLabelCount, float * tgtLabels, int tgtLabelCount, int * mask)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < srcLabelCount + tgtLabelCount)
	{
		if (idx < srcLabelCount) mask[idx] = srcLabels[idx] < 0 ? 0 : 1;
		else mask[idx] = tgtLabels[idx - srcLabelCount] < 0 ? 0 : 1;
	}
}

__global__ void cuda_CalculateSrcTgtLinkMatrix(int srcLabelCount, float * srcSeqMatrix, int tgtLabelCount, float * tgtSeqMatrix, int dim, int * mask, int bSum, float **linkMatrix)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < bSum)
	{
		int b = mask[idx] - 1;
		if (b < srcLabelCount) linkMatrix[idx] = srcSeqMatrix + b * dim;
		else linkMatrix[idx] = tgtSeqMatrix + (b - srcLabelCount) * dim;
	}
}

__global__ void cuda_CalculateWord2Index(int srcLabelCount, float * srcLabels, int tgtLabelCount, float * tgtLabels, int * v2idx, int * mask, int bSum, int *linkClass)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < bSum)
	{
		int b = mask[idx] - 1;
		if (b < srcLabelCount) linkClass[idx] = v2idx[int(srcLabels[b])]; 
		else linkClass[idx] = v2idx[int(tgtLabels[b - srcLabelCount])]; 
	}
}
__global__ void cuda_CalculateWord2Index(int labelCount, float * labels, int * v2idx, int * idxMask)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < labelCount)
	{
		idxMask[idx] = v2idx[(int)labels[idx]];
	}
}
__global__ void cuda_CalculateWord2Index(int srcLabelCount, float * srcLabels, int tgtLabelCount, float * tgtLabels, int * mask, int bSum, int *vocabLabel)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < bSum)
	{
		int b = mask[idx] - 1;
		if (b < srcLabelCount) vocabLabel[idx] = (int)srcLabels[b];
		else vocabLabel[idx] = (int)tgtLabels[b - srcLabelCount];
	}
}

__global__ void cuda_CalculateClassWordCount(int *linkClass, int bSum, int * classIdx, int * wordCountIndex)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < bSum)
	{
		int c = linkClass[idx];
		wordCountIndex[idx] = c == 0 ? classIdx[c] : classIdx[c] - classIdx[c - 1];
	}
}

__global__ void cuda_CalculateClassWordCount(int *linkClass, int * wordCountIndex, int bSum, int * classIdx, int * wordIdx, int * wact1, int * wact2)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < bSum)
	{
		int start = idx == 0 ? 0 : wordCountIndex[idx - 1];
		int end = wordCountIndex[idx];
		int c = linkClass[idx];
		int idc = c == 0 ? 0 : classIdx[c - 1];

		for (int i = start; i < end; i++)
		{
			wact1[i] = idx;
			wact2[i] = wordIdx[idc];
			idc++;
		}
	}
}

/// targets : label
void Cuda_HierarcialSoftmaxPairEmbedding(
		float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv, 
		float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv, 
		int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,
		float step, // step size for update vSpace.
		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classAct,
		float * wordAct,
		int * wordActIdx1,
		int * wordActIdx2,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb)
{
	int * mask;
	int * dev_mask;
	int * class_mask;
	int * word_candidate_count;

	mask = (int *)malloc((srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&dev_mask, (srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&class_mask, (srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&word_candidate_count, (srcLabelCount + tgtLabelCount) * sizeof(int));

	float ** embedLinkMatrix;
	float ** classLinkMatrix;
	float ** derivLinkMatrix;

	cudaMalloc((void **)&embedLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));
	cudaMalloc((void **)&classLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));
	cudaMalloc((void **)&derivLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));

	/// which label contains true label;
	cuda_CalculateSrcTgtLabelMask << < (srcLabelCount + tgtLabelCount - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(srcLabels, srcLabelCount, tgtLabels, tgtLabelCount, dev_mask);

	cudaMemcpy(mask, dev_mask, (srcLabelCount + tgtLabelCount) * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
	int bSum = 0;
	for (int b = 0; b < (srcLabelCount + tgtLabelCount); b++)
	{
		if (mask[b] > 0)
		{
			mask[bSum] = b + 1;
			bSum += 1;
			//printf("%d, %d\n", b, bSum);
		}
	}
	if (bSum > 0)
	{
		Cuda_Init(targetProb, bSum, 1);

		cudaMemcpy(dev_mask, mask, (srcLabelCount + tgtLabelCount) * sizeof(int), cudaMemcpyHostToDevice);

		/// find class idx for the label word.
		cuda_CalculateWord2Index << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcLabels, tgtLabelCount, tgtLabels, v2c, dev_mask, bSum, class_mask);

		/// embedding vectors.
		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcEmbedding, tgtLabelCount, tgtEmbedding, dim, dev_mask, bSum, embedLinkMatrix);

		/// class output vectors.
		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, classAct, tgtLabelCount, classAct + srcLabelCount * cSize, cSize, dev_mask, bSum, classLinkMatrix);

		/// derivation vector
		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcDeriv, tgtLabelCount, tgtDeriv, dim, dev_mask, bSum, derivLinkMatrix);

		/********Calculate candidate word per sample*****/
		cuda_CalculateClassWordCount << <(bSum - 1) / 32 + 1, 32 >> >(
				class_mask, bSum, classIdx, word_candidate_count);

		cudaMemcpy(mask, word_candidate_count, bSum * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		for (int b = 1; b < (bSum); b++) mask[b] += mask[b - 1];
		int wSum = mask[bSum - 1];

		cudaMemcpy(word_candidate_count, mask, bSum * sizeof(int), cudaMemcpyHostToDevice);

		//printf("bSum  %d wSum %d\n", bSum, wSum);
		cuda_CalculateClassWordCount << <(bSum - 1) / 32 + 1, 32 >> >(
				class_mask, word_candidate_count, bSum, classIdx, wordIdx, wordActIdx1, wordActIdx2);




		/**********Phase 1 softmax on classes.*****************/
		Cuda_MatrixMultiplicationTranspose(embedLinkMatrix, cSpace, classLinkMatrix, bSum, dim, cSize, 0);
		Cuda_Matrix_AddVector(classLinkMatrix, cBias, bSum, cSize);

		Cuda_DerivSoftmax(classLinkMatrix, class_mask, classLinkMatrix, cSize, gamma, bSum, targetProb);
		/**********Phase 1*****************/

		///backpropagate from class to embedding.
		Cuda_MatrixMultiplication(classLinkMatrix, cSpace, derivLinkMatrix, bSum, cSize, dim, 0);

		/**********Update Class Bias and Class Space*************/
		Cuda_ColumnWiseSum(classLinkMatrix, cBias, bSum, cSize, step);
		Cuda_MatrixMultiplicationLeftTranspose(classLinkMatrix, embedLinkMatrix, cSpace, bSum, cSize, dim, 1, step);



		/**********Phase 2 softmax on words*****************/
		Cuda_InnerProduct(embedLinkMatrix, vSpace, bSum, vocabSize, dim, wordActIdx1, wordActIdx2, wSum, wordAct);
		Cuda_VectorAddition(wordAct, wordActIdx2, wSum, vBias, wordAct);

		cuda_CalculateWord2Index << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcLabels, tgtLabelCount, tgtLabels, wordClassIdx, dev_mask, bSum, class_mask);

		Cuda_DerivSparseSoftmax(word_candidate_count, wordAct, wordAct, class_mask, gamma, bSum, targetProb);
		/***********Phase 2****************/

		//backpropagate from words to embedding.
		Cuda_VectorIndexMatrixMultiplication(wordAct, wordActIdx2, wordActIdx1, wSum, vSpace, derivLinkMatrix, dim, 1);

		/**********Update Word Bias and Word Space*************/
		Cuda_VectorSparseAgg(wordAct, wordActIdx2, wSum, vBias, step);
		Cuda_VectorIndexMatrixMultiplication(wordAct, wordActIdx1, wordActIdx2, wSum, embedLinkMatrix, vSpace, dim, step);
	}

	free(mask);
	cudaFree(dev_mask);
	cudaFree(class_mask);
	cudaFree(word_candidate_count);

	cudaFree(embedLinkMatrix);
	cudaFree(classLinkMatrix);
	cudaFree(derivLinkMatrix);
}

void Cuda_FullySoftmaxPairEmbedding(
		float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv,
		float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv,
		int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step, // step size for update vSpace.
		float gamma,
		float * wordAct,
		float * targetProb)
{
	int * mask;
	int * dev_mask;
	int * label_mask;

	mask = (int *)malloc((srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&dev_mask, (srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&label_mask, (srcLabelCount + tgtLabelCount) * sizeof(int));


	float ** embedLinkMatrix;
	float ** outputLinkMatrix;
	float ** derivLinkMatrix;

	cudaMalloc((void **)&embedLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));
	cudaMalloc((void **)&outputLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));
	cudaMalloc((void **)&derivLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));

	/// which label contains true label;
	cuda_CalculateSrcTgtLabelMask << < (srcLabelCount + tgtLabelCount - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(srcLabels, srcLabelCount, tgtLabels, tgtLabelCount, dev_mask);
	cudaMemcpy(mask, dev_mask, (srcLabelCount + tgtLabelCount) * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
	int bSum = 0;
	for (int b = 0; b < (srcLabelCount + tgtLabelCount); b++)
	{
		if (mask[b] > 0)
		{
			mask[bSum] = b + 1;
			bSum += 1;
		}
	}
	if (bSum > 0)
	{
		Cuda_Init(targetProb, bSum, 1);
		cudaMemcpy(dev_mask, mask, (srcLabelCount + tgtLabelCount) * sizeof(int), cudaMemcpyHostToDevice);

		/// find word idx for the label word.
		cuda_CalculateWord2Index << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcLabels, tgtLabelCount, tgtLabels, dev_mask, bSum, label_mask);

		/// embedding vectors.
		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcEmbedding, tgtLabelCount, tgtEmbedding, dim, dev_mask, bSum, embedLinkMatrix);

		/// output vectors.
		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, wordAct, tgtLabelCount, wordAct + srcLabelCount * vocabSize, vocabSize, dev_mask, bSum, outputLinkMatrix);

		/// derivation vector
		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcDeriv, tgtLabelCount, tgtDeriv, dim, dev_mask, bSum, derivLinkMatrix);

		/**********Phase 1 softmax on words.*****************/
		Cuda_MatrixMultiplicationTranspose(embedLinkMatrix, vSpace, outputLinkMatrix, bSum, dim, vocabSize, 0);
		//Cuda_Matrix_AddVector(outputLinkMatrix, vBias, bSum, vocabSize);

		Cuda_DerivSoftmax(outputLinkMatrix, label_mask, outputLinkMatrix, vocabSize, gamma, bSum, targetProb);
		/**********Phase 1*****************/

		///backpropagate from wordOutput to embedding.
		Cuda_MatrixMultiplication(outputLinkMatrix, vSpace, derivLinkMatrix, bSum, vocabSize, dim, 0);

		/**********Update Class Bias and Class Space*************/
		//Cuda_ColumnWiseSum(outputLinkMatrix, vBias, bSum, vocabSize, step);
		Cuda_MatrixMultiplicationLeftTranspose(outputLinkMatrix, embedLinkMatrix, vSpace, bSum, vocabSize, dim, 1, step);
	}

	free(mask);
	cudaFree(dev_mask);
	cudaFree(label_mask);

	cudaFree(embedLinkMatrix);
	cudaFree(outputLinkMatrix);
	cudaFree(derivLinkMatrix);
}

void Cuda_HierarcialSoftmaxPairProbability(
		float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv,
		float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv,
		int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,
		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classAct,
		float * wordAct,
		int * wordActIdx1,
		int * wordActIdx2,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb)
{
	int * mask;
	int * dev_mask;
	int * class_mask;
	int * word_candidate_count;

	mask = (int *)malloc((srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&dev_mask, (srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&class_mask, (srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&word_candidate_count, (srcLabelCount + tgtLabelCount) * sizeof(int));


	float ** embedLinkMatrix;
	float ** classLinkMatrix;

	cudaMalloc((void **)&embedLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));
	cudaMalloc((void **)&classLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));

	cuda_CalculateSrcTgtLabelMask << < (srcLabelCount + tgtLabelCount - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(srcLabels, srcLabelCount, tgtLabels, tgtLabelCount, dev_mask);

	cudaMemcpy(mask, dev_mask, (srcLabelCount + tgtLabelCount) * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
	int bSum = 0;
	for (int b = 0; b < (srcLabelCount + tgtLabelCount); b++)
	{
		if (mask[b] > 0)
		{
			mask[bSum] = b + 1;
			bSum += 1;
		}
	}
	if (bSum > 0)
	{
		Cuda_Init(targetProb, bSum, 1);

		//printf("bSum %d first mask %d\n", bSum, mask[0]);

		cudaMemcpy(dev_mask, mask, (srcLabelCount + tgtLabelCount) * sizeof(int), cudaMemcpyHostToDevice);
		cuda_CalculateWord2Index << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcLabels, tgtLabelCount, tgtLabels, v2c, dev_mask, bSum, class_mask);

		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcEmbedding, tgtLabelCount, tgtEmbedding, dim, dev_mask, bSum, embedLinkMatrix);

		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, classAct, tgtLabelCount, classAct + srcLabelCount * cSize, cSize, dev_mask, bSum, classLinkMatrix);

		/**********Phase 1 softmax on classes.*****************/
		Cuda_MatrixMultiplicationTranspose(embedLinkMatrix, cSpace, classLinkMatrix, bSum, dim, cSize, 0);
		Cuda_Matrix_AddVector(classLinkMatrix, cBias, bSum, cSize);

		//
		Cuda_DerivSoftmax(classLinkMatrix, class_mask, classLinkMatrix, cSize, gamma, bSum, targetProb);
		///**********Phase 1*****************/

		///********Calculate candidate word per sample*****/
		cuda_CalculateClassWordCount << <(bSum - 1) / 32 + 1, 32 >> >(
				class_mask, bSum, classIdx, word_candidate_count);

		cudaMemcpy(mask, word_candidate_count, bSum * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
		for (int b = 1; b < (bSum); b++) mask[b] += mask[b - 1];
		int wSum = mask[bSum - 1];
		cudaMemcpy(word_candidate_count, mask, bSum * sizeof(int), cudaMemcpyHostToDevice);

		cuda_CalculateClassWordCount << <(bSum - 1) / 32 + 1, 32 >> >(
				class_mask, word_candidate_count, bSum, classIdx, wordIdx, wordActIdx1, wordActIdx2);

		//printf("bSum  %d wSum %d\n", bSum, wSum);

		///**********Phase 2 softmax on words*****************/
		Cuda_InnerProduct(embedLinkMatrix, vSpace, bSum, vocabSize, dim, wordActIdx1, wordActIdx2, wSum, wordAct);
		Cuda_VectorAddition(wordAct, wordActIdx2, wSum, vBias, wordAct);

		cuda_CalculateWord2Index << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcLabels, tgtLabelCount, tgtLabels, wordClassIdx, dev_mask, bSum, class_mask);

		Cuda_DerivSparseSoftmax(word_candidate_count, wordAct, wordAct, class_mask, gamma, bSum, targetProb);
		///***********Phase 2****************/
	}

	free(mask);
	cudaFree(dev_mask);
	cudaFree(class_mask);
	cudaFree(word_candidate_count);

	cudaFree(embedLinkMatrix);
	cudaFree(classLinkMatrix);
}

void Cuda_FullySoftmaxPairProbability(
		float * srcLabels, int srcLabelCount, float * srcEmbedding,
		float * tgtLabels, int tgtLabelCount, float * tgtEmbedding,
		int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step, // step size for update vSpace.
		float gamma,
		float * wordAct,
		float * targetProb)
{
	int * mask;
	int * dev_mask;
	int * label_mask;

	mask = (int *)malloc((srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&dev_mask, (srcLabelCount + tgtLabelCount) * sizeof(int));
	cudaMalloc((void **)&label_mask, (srcLabelCount + tgtLabelCount) * sizeof(int));


	float ** embedLinkMatrix;
	float ** outputLinkMatrix;

	cudaMalloc((void **)&embedLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));
	cudaMalloc((void **)&outputLinkMatrix, (srcLabelCount + tgtLabelCount) * sizeof(float *));

	/// which label contains true label;
	cuda_CalculateSrcTgtLabelMask << < (srcLabelCount + tgtLabelCount - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		(srcLabels, srcLabelCount, tgtLabels, tgtLabelCount, dev_mask);
	cudaMemcpy(mask, dev_mask, (srcLabelCount + tgtLabelCount) * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
	int bSum = 0;
	for (int b = 0; b < (srcLabelCount + tgtLabelCount); b++)
	{
		if (mask[b] > 0)
		{
			mask[bSum] = b + 1;
			bSum += 1;
		}
	}
	if (bSum > 0)
	{
		Cuda_Init(targetProb, bSum, 1);
		cudaMemcpy(dev_mask, mask, (srcLabelCount + tgtLabelCount) * sizeof(int), cudaMemcpyHostToDevice);

		/// find word idx for the label word.
		cuda_CalculateWord2Index << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcLabels, tgtLabelCount, tgtLabels, dev_mask, bSum, label_mask);

		/// embedding vectors.
		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, srcEmbedding, tgtLabelCount, tgtEmbedding, dim, dev_mask, bSum, embedLinkMatrix);

		/// output vectors.
		cuda_CalculateSrcTgtLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(
				srcLabelCount, wordAct, tgtLabelCount, wordAct + srcLabelCount * vocabSize, vocabSize, dev_mask, bSum, outputLinkMatrix);

		/**********Phase 1 softmax on words.*****************/
		Cuda_MatrixMultiplicationTranspose(embedLinkMatrix, vSpace, outputLinkMatrix, bSum, dim, vocabSize, 0);
		//Cuda_Matrix_AddVector(outputLinkMatrix, vBias, bSum, vocabSize);

		Cuda_DerivSoftmax(outputLinkMatrix, label_mask, outputLinkMatrix, vocabSize, gamma, bSum, targetProb);
		/**********Phase 1*****************/
	}
	free(mask);
	cudaFree(dev_mask);
	cudaFree(label_mask);

	cudaFree(embedLinkMatrix);
	cudaFree(outputLinkMatrix);
}


void Cuda_HierarcicalSoftmaxProbability(float * targets, int targetNum,
		float * embedding, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,

		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classOutput,
		float * wordOutput,

		int * wordActIdx1,
		int * wordActIdx2,

		int wordSegSize, // word Max SegmentSize,
		float * targetProb)
{
	int * mask;
	int * class_mask;
	int * word_index_mask;
	int * word_candidate_count;

	mask = (int *)malloc(targetNum * sizeof(int));
	cudaMalloc((void **)&class_mask, (targetNum)* sizeof(int));
	cudaMalloc((void **)&word_candidate_count, (targetNum)* sizeof(int));

	int bSum = targetNum;

	if (bSum > 0)
	{
		Cuda_Init(targetProb, bSum, 1);

		cuda_CalculateWord2Index << <(targetNum - 1) / 32 + 1, 32 >> >(targetNum, targets, v2c, class_mask);

		/**********Phase 1 softmax on classes.*****************/
		Cuda_MatrixMultiplicationTranspose(embedding, cSpace, classOutput, targetNum, dim, cSize);
		Cuda_Matrix_AddVector(classOutput, cBias, targetNum, cSize);

		Cuda_DerivSoftmax(classOutput, class_mask, classOutput, cSize, gamma, targetNum, targetProb);
		///**********Phase 1*****************/

		///********Calculate candidate word per sample*****/
		cuda_CalculateClassWordCount << <(targetNum - 1) / 32 + 1, 32 >> >(class_mask, targetNum, classIdx, word_candidate_count);

		cudaMemcpy(mask, word_candidate_count, targetNum * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);

		for (int b = 1; b < targetNum; b++) mask[b] += mask[b - 1];
		int wSum = mask[targetNum - 1];

		cudaMemcpy(word_candidate_count, mask, targetNum * sizeof(int), cudaMemcpyHostToDevice);

		cuda_CalculateClassWordCount << <(targetNum - 1) / 32 + 1, 32 >> >(
				class_mask, word_candidate_count, targetNum, classIdx, wordIdx, wordActIdx1, wordActIdx2);

		/////**********Phase 2 softmax on words*****************/
		Cuda_InnerProduct(embedding, vSpace, targetNum, vocabSize, dim, wordActIdx1, wordActIdx2, wSum, wordOutput);
		Cuda_VectorAddition(wordOutput, wordActIdx2, wSum, vBias, wordOutput);

		cudaDeviceSynchronize();

		cuda_CalculateWord2Index << <(targetNum - 1) / 32 + 1, 32 >> >(targetNum, targets, wordClassIdx, class_mask);
		Cuda_DerivSparseSoftmax(word_candidate_count, wordOutput, wordOutput, class_mask, gamma, targetNum, targetProb);

		cudaDeviceSynchronize();
		///***********Phase 2****************/
	}

	free(mask);
	cudaFree(class_mask);
	cudaFree(word_candidate_count);
}


void Cuda_HierarcialSoftmaxEmbeddingV2(float * targets, int targetNum,
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,
		float step,
		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classOutput,
		float * wordOutput,
		int * wordActIdx1,
		int * wordActIdx2,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb)	// step size for update vSpace.
{
	int * mask;
	int * class_mask;
	int * word_candidate_count;

	mask = (int *)malloc((targetNum)* sizeof(int));
	cudaMalloc((void **)&class_mask, (targetNum)* sizeof(int));
	cudaMalloc((void **)&word_candidate_count, (targetNum)* sizeof(int));

	Cuda_Init(targetProb, targetNum, 1);

	cuda_CalculateWord2Index << <(targetNum - 1) / 32 + 1, 32 >> >(targetNum, targets, v2c, class_mask);

	/**********Phase 1 softmax on classes.*****************/
	Cuda_MatrixMultiplicationTranspose(embedding, cSpace, classOutput, targetNum, dim, cSize);
	Cuda_Matrix_AddVector(classOutput, cBias, targetNum, cSize);

	Cuda_DerivSoftmax(classOutput, class_mask, classOutput, cSize, gamma, targetNum, targetProb);
	///**********Phase 1*****************/

	///backpropagate from class to embedding.
	Cuda_MatrixMultiplication(classOutput, cSpace, deriv, targetNum, cSize, dim, 0);

	/**********Update Class Bias and Class Space*************/
	Cuda_ColumnWiseSum(classOutput, cBias, targetNum, cSize, step);
	Cuda_MatrixMultiplicationLeftTranspose(classOutput, embedding, cSpace, targetNum, cSize, dim, 1, step);


	///********Calculate candidate word per sample*****/
	cuda_CalculateClassWordCount << <(targetNum - 1) / 32 + 1, 32 >> >(class_mask, targetNum, classIdx, word_candidate_count);
	cudaMemcpy(mask, word_candidate_count, targetNum * sizeof(int), cudaMemcpyDeviceToHost); // cudaMemcpyHostToDevice);
	for (int b = 1; b < targetNum; b++) mask[b] += mask[b - 1];
	int wSum = mask[targetNum - 1];
	cudaMemcpy(word_candidate_count, mask, targetNum * sizeof(int), cudaMemcpyHostToDevice);

	cuda_CalculateClassWordCount << <(targetNum - 1) / 32 + 1, 32 >> >(
			class_mask, word_candidate_count, targetNum, classIdx, wordIdx, wordActIdx1, wordActIdx2);
	/////**********Phase 2 softmax on words*****************/
	Cuda_InnerProduct(embedding, vSpace, targetNum, vocabSize, dim, wordActIdx1, wordActIdx2, wSum, wordOutput);
	Cuda_VectorAddition(wordOutput, wordActIdx2, wSum, vBias, wordOutput);
	cudaDeviceSynchronize();
	cuda_CalculateWord2Index << <(targetNum - 1) / 32 + 1, 32 >> >(targetNum, targets, wordClassIdx, class_mask);
	Cuda_DerivSparseSoftmax(word_candidate_count, wordOutput, wordOutput, class_mask, gamma, targetNum, targetProb);
	cudaDeviceSynchronize();
	///***********Phase 2****************/

	//backpropagate from words to embedding.
	Cuda_VectorIndexMatrixMultiplication(wordOutput, wordActIdx2, wordActIdx1, wSum, vSpace, deriv, dim, 1);

	/**********Update Word Bias and Word Space*************/
	Cuda_VectorSparseAgg(wordOutput, wordActIdx2, wSum, vBias, step);
	Cuda_VectorIndexMatrixMultiplication(wordOutput, wordActIdx1, wordActIdx2, wSum, embedding, vSpace, dim, step);


	free(mask);
	cudaFree(class_mask);
	cudaFree(word_candidate_count);
}


void Cuda_HierarcialSoftmaxDecoding(float * embedding, int batchSize, int dim,
		float * vSpace, float * vBias, int vSize,
		float * cSpace, float * cBias, int cSize,
		int bestN, float gamma,
		int * v2c, 
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		float * tmpClassOutput, float * tmpWordOutput,
		float * wordSet, float * wordProb)
{
	if (batchSize > 0)
	{
		/**********Phase 1 softmax on classes.*****************/
		Cuda_MatrixMultiplicationTranspose(embedding, cSpace, tmpClassOutput, batchSize, dim, cSize);
		Cuda_Matrix_AddVector(tmpClassOutput, cBias, batchSize, cSize);
		Cuda_LogSoftMax(tmpClassOutput, tmpClassOutput, cSize, batchSize, gamma);
		///**********Phase 1*****************/

		/////**********Phase 2 softmax on words*****************/
		Cuda_MatrixMultiplicationTranspose(embedding, vSpace, tmpWordOutput, batchSize, dim, vSize);
		Cuda_Matrix_AddVector(tmpWordOutput, vBias, batchSize, vSize);
		//Cuda_LogSoftMax(tmpWordOutput, tmpWordOutput, vSize, batchSize, gamma);
		Cuda_LogSparseSoftmax(classIdx, wordIdx, cSize, tmpWordOutput, batchSize, vSize, tmpWordOutput, gamma);
		///***********Phase 2****************/

		///combine word & class probability. 
		Cuda_VectorAdditionBatch(tmpWordOutput, vSize, v2c, tmpClassOutput, cSize, batchSize, tmpWordOutput);
		//Cuda_VectorAddition(tmpWordOutput, v2c, vSize, tmpClassOutput, tmpWordOutput);

		///select the most-possibile words from set.
		Cuda_KLargestValueBatch(tmpWordOutput, batchSize, vSize, bestN, 1, wordProb, wordSet);
		//cudaDeviceSynchronize();
	}
}


void Cuda_FullySoftmaxDecoding(float * embedding, int batchSize, int dim,
		float * vSpace, float * vBias, int vSize,
		int bestN, float gamma, float * tmpWordOutput,
		float * wordSet, float * wordProb)
{
	if (batchSize > 0)
	{
		/**********Phase 1 softmax on classes.*****************/
		Cuda_MatrixMultiplicationTranspose(embedding, vSpace, tmpWordOutput, batchSize, dim, vSize);
		//Cuda_Matrix_AddVector(tmpWordOutput, vBias, batchSize, vSize);
		Cuda_LogSoftMax(tmpWordOutput, tmpWordOutput, vSize, batchSize, gamma);
		///**********Phase 1*****************/

		///select the most-possibile words from set.
		Cuda_KLargestValueBatch(tmpWordOutput, batchSize, vSize, bestN, 1, wordProb, wordSet);
		//cudaDeviceSynchronize();
	}
}


__global__ void cuda_RNNforwardpropagate(int * smpIdx, int batchSize, int seqSize, float * Wmatrix, int lag, int hiddenDim, int af, float * seqOut)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		for (int i = seqBegin; i < seqEnd; i++)
		{
			int outStart = i * hiddenDim;

			for (int l = 0; l < lag; l++)
			{
				int history = i - l - 1;
				int hIndex = history * hiddenDim;
				if (history >= seqBegin)
				{
					for (int h = 0; h < hiddenDim; h++)
					{
						int windex = l * hiddenDim * hiddenDim + h * hiddenDim;
						float historyS = seqOut[hIndex + h];
						for (int hIn = 0; hIn < hiddenDim; hIn++)
						{
							seqOut[outStart + hIn] += historyS * Wmatrix[windex + hIn];
						}
					}
				}
				else
					break;
			}

			// Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
			if (af == 0)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = seqOut[outStart + h] ;
				}
			}
			else if (af == 1)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = tanhf(seqOut[outStart + h]);
				}
			}
			else if (af == 2)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					float v = seqOut[outStart + h] ;
					seqOut[outStart + h] = v <= 0 ? 0 : v;
				}
			}
			else if (af == 3)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = (tanhf((seqOut[outStart + h] ) / 2) + 1) / 2;
				}
			}
	}
}
}

/// smpIdx : indicate the length of each sample (time series data).
/// batchSize : 1
/// seqInput : time series input Data.
/// seqSize : sequence length of the total sample.
/// IMatrix : input 2 hidden
/// WMatrix : hidden -> hidden
/// lag : history hidden length -> current hidden state.
/// feaDim = 22 (input feature dimension)
/// 
void cuda_RNNForwardPropagate(int * smpIdx, int batchSize, int seqSize, float * Wmatrix, int lag, int hiddenDim, int af, float * seqOut)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_RNNforwardpropagate << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize,  seqSize,  Wmatrix, lag, hiddenDim, af, seqOut);
}



__global__ void cuda_RNNforwardpropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix,
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		for (int i = seqBegin; i < seqEnd; i++)
		{
			int outStart = i * hiddenDim;

			for (int h = 0; h < hiddenDim; h++)
			{
				float sum = 0;
				int fbegin = i == 0 ? 0 : seqIdx[i - 1];
				int fend = seqIdx[i];
				for (int f = fbegin; f < fend; f++)
				{
					sum += Imatrix[feaIdx[f] * hiddenDim + h] * feaValue[f];
				}
				seqOut[outStart + h] = sum;
			}

			for (int l = 0; l < lag; l++)
			{
				int history = i - l - 1;
				int hIndex = history * hiddenDim;
				if (history >= seqBegin)
				{
					for (int h = 0; h < hiddenDim; h++)
					{
						int windex = l * hiddenDim * hiddenDim + h * hiddenDim;
						float historyS = seqOut[hIndex + h];
						for (int hIn = 0; hIn < hiddenDim; hIn++)
						{
							seqOut[outStart + hIn] += historyS * Wmatrix[windex + hIn];
						}
					}
				}
				else
					break;
			}

			// Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
			if (af == 0)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = seqOut[outStart + h] + bias[h];
				}
			}
			else if (af == 1)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = tanhf(seqOut[outStart + h] + bias[h]);
				}
			}
			else if (af == 2)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					float v = seqOut[outStart + h] + bias[h];
					seqOut[outStart + h] = v <= 0 ? 0 : v;
				}
			}
			else if (af == 3)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = (tanhf((seqOut[outStart + h] + bias[h]) / 2) + 1) / 2;
				}
			}
	}
}
}

void cuda_RNNForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix,
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_RNNforwardpropagate << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, seqIdx, seqSize, feaIdx, feaValue, Imatrix, Wmatrix, lag, feaDim, hiddenDim,
			bias, af, seqOut);
}


__global__ void cuda_RNNforwardpropagateslot(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix,
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut, float * groundTruth, int * earlyStopIdx)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		int earlyEnd = earlyStopIdx[idx];

		for (int i = seqBegin; i < seqEnd; i++)
		{
			int outStart = i * hiddenDim;

			for (int h = 0; h < hiddenDim; h++)
			{
				float sum = 0;
				int fbegin = i == 0 ? 0 : seqIdx[i - 1];
				int fend = seqIdx[i];
				for (int f = fbegin; f < fend; f++)
				{
					sum += Imatrix[feaIdx[f] * hiddenDim + h] * feaValue[f];
				}
				seqOut[outStart + h] = sum;
			}

			for (int l = 0; l < lag; l++)
			{
				int history = i - l - 1;
				int hIndex = history * hiddenDim;
				if (history >= seqBegin)
				{
					for (int h = 0; h < hiddenDim; h++)
					{
						int windex = l * hiddenDim * hiddenDim + h * hiddenDim;
						float historyS = groundTruth[hIndex + h];
						if (hIndex >= earlyEnd) historyS = seqOut[hIndex + h];
						for (int hIn = 0; hIn < hiddenDim; hIn++)
						{
							seqOut[outStart + hIn] += historyS * Wmatrix[windex + hIn];
						}
					}
				}
				else
					break;
			}

			// Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
			if (af == 0)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = seqOut[outStart + h] + bias[h];
				}
			}
			else if (af == 1)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = tanhf(seqOut[outStart + h] + bias[h]);
				}
			}
			else if (af == 2)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					float v = seqOut[outStart + h] + bias[h];
					seqOut[outStart + h] = v <= 0 ? 0 : v;
				}
			}
			else if (af == 3)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = (tanhf((seqOut[outStart + h] + bias[h]) / 2) + 1) / 2;
				}
			}
	}
}
}

void cuda_RNNForwardPropagateSlot(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix,
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut, float * groundTruth, int * earlyStopIdx)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_RNNforwardpropagateslot << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, seqIdx, seqSize, feaIdx, feaValue, Imatrix, Wmatrix, lag, feaDim, hiddenDim,
			bias, af, seqOut, groundTruth, earlyStopIdx);
}


__global__  void cuda_RNNbackwardpropagate(int * smpIdx, int batchSize, float * derivOutput, float * output,
		int seqSize, float * wMatrix, int lag, int hiddenDim, int af)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		for (int h = seqEnd - 1; h >= seqBegin; h--)
		{
			int cId = h * hiddenDim;
			if (af == 1)
			{
				for (int h1 = 0; h1 < hiddenDim; h1++)
				{
					derivOutput[cId + h1] = derivOutput[cId + h1] * (1 - output[cId + h1]) * (1 + output[cId + h1]);
				}
			}
			else if (af == 2)
			{
				for (int h1 = 0; h1 < hiddenDim; h1++)
				{
					derivOutput[cId + h1] = output[cId + h1] > 0 ? derivOutput[cId + h1] : 0;
				}
			}
			else if (af == 3)
			{
				for (int h1 = 0; h1 < hiddenDim; h1++)
				{
					derivOutput[cId + h1] = derivOutput[cId + h1] * (1 - output[cId + h1]) * (output[cId + h1]);
				}
			}

			for (int l = 0; l < lag; l++)
			{
				int history = h - l - 1;
				int hStart = l * hiddenDim * hiddenDim;
				int hId = history * hiddenDim;
				if (history >= seqBegin)
				{
					for (int h2 = 0; h2 < hiddenDim; h2++)
					{
						float o = derivOutput[cId + h2];
						for (int h1 = 0; h1 < hiddenDim; h1++)
						{
							derivOutput[hId + h1] += o * wMatrix[hStart + h1 * hiddenDim + h2];
						}
					}
				}
			}
		}
	}
}

void cuda_RNNBackwardPropagate(int * smpIdx, int batchSize, float * derivOutput, float * output, int seqSize, float * wMatrix, int lag, int hiddenDim, int af)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_RNNbackwardpropagate << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, derivOutput, output, seqSize, wMatrix, lag, hiddenDim, af);
}


__global__  void cuda_RNNbackwardpropagateslot(int * smpIdx, int batchSize, float * derivOutput, float * output,
		int seqSize, float * wMatrix, int hiddenDim, int af, int * earlyStop)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int hidIdx = blockDim.y * blockIdx.y + threadIdx.y;

	if (idx < batchSize && hidIdx < hiddenDim)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		int seqStop = earlyStop[idx];
		for (int h = seqStop - 1; h >= seqBegin; h--)
		{
			int cId = h * hiddenDim;
			if (af == 1)
			{
				derivOutput[cId + hidIdx] = derivOutput[cId + hidIdx] * (1 - output[cId + hidIdx]) * (1 + output[cId + hidIdx]);
			}
			else if (af == 2)
			{
				derivOutput[cId + hidIdx] = output[cId + hidIdx] > 0 ? derivOutput[cId + hidIdx] : 0;
			}
			else if (af == 3)
			{
				derivOutput[cId + hidIdx] = derivOutput[cId + hidIdx] * (1 - output[cId + hidIdx]) * (output[cId + hidIdx]);
			}
		}
	}
}

void cuda_RNNBackwardPropagateSlot(int * smpIdx, int batchSize, float * derivOutput, float * output, int seqSize, float * wMatrix,
		int hiddenDim, int af, int * earlyStop)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (hiddenDim + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_RNNbackwardpropagateslot << <block_tail, thread_tail >> >(smpIdx, batchSize, derivOutput, output, seqSize, wMatrix, hiddenDim, af, earlyStop);
}



__global__  void cuda_recurrentupdate(int * smpIdx, int batchSize, float * output, float * derivOutput, int hiddenDim, int seqSize, int lag, float * gradient, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		for (int h = seqEnd - 1; h >= seqBegin; h--)
		{
			int cId = h * hiddenDim;
			for (int l = 0; l < lag; l++)
			{
				int history = h - l - 1;
				int hStart = l * hiddenDim * hiddenDim;
				int hId = history * hiddenDim;
				if (history >= seqBegin)
				{
					for (int h2 = 0; h2 < hiddenDim; h2++)
					{
						float o = derivOutput[cId + h2];
						for (int h1 = 0; h1 < hiddenDim; h1++)
						{
							atomicAdd(gradient + hStart + h1 * hiddenDim + h2, o * output[hId + h1] * weight);
						}
					}
				}
			}
		}
	}
}

void cuda_RecurrentUpdate(int * smpIdx, int batchSize, float * output, float * derivOutput, int hiddenDim, int seqSize, int lag, float * gradient, float weight)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_recurrentupdate << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, output, derivOutput, hiddenDim, seqSize, lag, gradient, weight);
}



__global__  void cuda_recurrentupdateslot(int * smpIdx, int batchSize, int * earlyStop, float * output, float * derivOutput, int hiddenDim,
		int seqSize, int lag, float * gradient, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		int seqEarly = earlyStop[idx];

		for (int h = seqEarly - 1; h >= seqBegin; h--)
		{
			int cId = h * hiddenDim;
			for (int l = 0; l < lag; l++)
			{
				int history = h - l - 1;
				int hStart = l * hiddenDim * hiddenDim;
				int hId = history * hiddenDim;
				if (history >= seqBegin)
				{
					for (int h2 = 0; h2 < hiddenDim; h2++)
					{
						float o = derivOutput[cId + h2];
						for (int h1 = 0; h1 < hiddenDim; h1++)
						{
							atomicAdd(gradient + hStart + h1 * hiddenDim + h2, o * output[hId + h1] * weight);
						}
					}
				}
			}
		}
	}
}

void cuda_RecurrentUpdateSlot(int * smpIdx, int batchSize, int * earlyStop, float * output, float * derivOutput, int hiddenDim,
		int seqSize, int lag, float * gradient, float weight)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_recurrentupdateslot << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, earlyStop, output, derivOutput, hiddenDim, seqSize, lag, gradient, weight);
}


__global__ void cuda_RNNforwardpropagate_dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix,
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		for (int i = seqBegin; i < seqEnd; i++)
		{
			int outStart = i * hiddenDim;
			int inStart = i * feaDim;
			for (int h = 0; h < hiddenDim; h++)
			{
				float sum = 0;
				for (int f = 0; f < feaDim; f++)
				{
					sum += Imatrix[f * hiddenDim + h] * seqInput[i * feaDim + f];
				}
				seqOut[outStart + h] = sum;
			}

			for (int l = 0; l < lag; l++)
			{
				int history = i - l - 1;
				int hIndex = history * hiddenDim;
				if (history >= seqBegin)
				{
					for (int h = 0; h < hiddenDim; h++)
					{
						int windex = l * hiddenDim * hiddenDim + h * hiddenDim;
						float historyS = seqOut[hIndex + h];
						for (int hIn = 0; hIn < hiddenDim; hIn++)
						{
							seqOut[outStart + hIn] += historyS * Wmatrix[windex + hIn];
						}
					}
				}
				else
					break;
			}

			// Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
			if (af == 0)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = seqOut[outStart + h] + bias[h];
				}
			}
			else if (af == 1)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = tanhf(seqOut[outStart + h] + bias[h]);
				}
			}
			else if (af == 2)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					float v = seqOut[outStart + h] + bias[h];
					seqOut[outStart + h] = v <= 0 ? 0 : v;
				}
			}
			else if (af == 3)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = (tanhf((seqOut[outStart + h] + bias[h]) / 2) + 1) / 2;
				}
			}
	}
}
}

/// smpIdx : indicate the length of each sample (time series data).
/// batchSize : 1
/// seqInput : time series input Data.
/// seqSize : sequence length of the total sample.
/// IMatrix : input 2 hidden
/// WMatrix : hidden -> hidden
/// lag : history hidden length -> current hidden state.
/// feaDim = 22 (input feature dimension)
/// 
void cuda_RNNForwardPropagate_Dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix,
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_RNNforwardpropagate_dense << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, seqInput, seqSize, Imatrix, Wmatrix, lag, feaDim, hiddenDim, bias, af, seqOut);
}


__global__ void cuda_RNNforwardpropagateSlot_dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix,
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut, float * groundOut, int * sampleEnd)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx];
		int earlyEnd = sampleEnd[idx];
		for (int i = seqBegin; i < seqEnd; i++)
		{
			int outStart = i * hiddenDim;

			for (int h = 0; h < hiddenDim; h++)
			{
				float sum = 0;
				for (int f = 0; f < feaDim; f++)
				{
					sum += Imatrix[f * hiddenDim + h] * seqInput[i * feaDim + f];
				}
				seqOut[outStart + h] = sum;
			}

			for (int l = 0; l < lag; l++)
			{
				int history = i - l - 1;
				int hIndex = history * hiddenDim;
				if (history >= seqBegin)
				{
					for (int h = 0; h < hiddenDim; h++)
					{
						int windex = l * hiddenDim * hiddenDim + h * hiddenDim;
						float historyS = groundOut[hIndex + h];
						if (hIndex >= earlyEnd) historyS = seqOut[hIndex + h];
						for (int hIn = 0; hIn < hiddenDim; hIn++)
						{
							seqOut[outStart + hIn] += historyS * Wmatrix[windex + hIn];
						}
					}
				}
				else
					break;
			}

			// Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
			if (af == 0)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = seqOut[outStart + h] + bias[h];
				}
			}
			else if (af == 1)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = tanhf(seqOut[outStart + h] + bias[h]);
				}
			}
			else if (af == 2)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					float v = seqOut[outStart + h] + bias[h];
					seqOut[outStart + h] = v <= 0 ? 0 : v;
				}
			}
			else if (af == 3)
			{
				for (int h = 0; h < hiddenDim; h++)
				{
					seqOut[outStart + h] = (tanhf((seqOut[outStart + h] + bias[h]) / 2) + 1) / 2;
				}
			}
	}
}
}

/// smpIdx : indicate the length of each sample (time series data).
/// batchSize : 1
/// seqInput : time series input Data.
/// seqSize : sequence length of the total sample.
/// IMatrix : input 2 hidden
/// WMatrix : hidden -> hidden
/// lag : history hidden length -> current hidden state.
/// feaDim = 22 (input feature dimension)
/// 
void cuda_RNNForwardPropagateSlot_Dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix,
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut, float * groundOut, int * sampleEnd)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_RNNforwardpropagateSlot_dense << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, seqInput, seqSize, Imatrix, Wmatrix, lag, feaDim, hiddenDim, bias, af, seqOut, groundOut, sampleEnd);
}

__global__ void cuda_rnnlabeloutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOut)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx] - 1;
		int seqStart = (seqEnd - lag);
		if (seqStart < seqBegin) seqStart = seqBegin;

		int outStart = idx * hiddenDim;

		for (int h = 0; h < hiddenDim; h++)
		{
			float sum = 0;
			for (int i = seqEnd; i >= seqStart; i--)
			{
				float * pSeqInput = seqInput + i * feaDim;
				float * pW = Wmatrix + (seqEnd - i) * feaDim * hiddenDim + h;
				for (int f = 0; f < feaDim; f++)
				{
					sum += (*pSeqInput++) *  (*pW);
					pW = pW + hiddenDim;
				}
			}
			seqOut[outStart + h] = sum;
		}
	}
}

void cuda_RNNLabelOutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOut)
{
	int nThreadPerBlock = 32;
	int nBlockPerGrid = (batchSize + 32 - 1) / 32;
	cuda_rnnlabeloutput << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, seqInput, seqSize, Wmatrix, lag, feaDim, hiddenDim, seqOut);
}

__global__ void cuda_dernnlabeloutput(int * smpIdx, int batchSize, float * seqInputDeriv, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx] - 1;
		int seqStart = (seqEnd - lag);
		if (seqStart < seqBegin) seqStart = seqBegin;

		int outStart = idx * hiddenDim;

		float * pSeqOutput = seqOutDeriv + outStart;

		for (int i = seqEnd; i >= seqStart; i--)
		{
			float * pSeqInput = seqInputDeriv + i * feaDim;
			float * pW = Wmatrix + (seqEnd - i) * feaDim * hiddenDim;
			for (int f = 0; f < feaDim; f++)
			{
				float sum = 0;
				float * pSeqCursor = pSeqOutput;
				for (int h = 0; h < hiddenDim; h++)
				{
					sum += (*pSeqCursor) * (*pW);
					pW++;
					pSeqCursor++;
				}
				///Keep Previous Input Deriv.
				(*pSeqInput) += sum;
				pSeqInput++;
			}
			//seqOut[outStart + h] = sum;
		}
	}
}

void cuda_DeRNNLabelOutput(int * smpIdx, int batchSize, float * seqInputDeriv, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv)
{
	int nThreadPerBlock = 32;
	int nBlockPerGrid = (batchSize + 32 - 1) / 32;
	cuda_dernnlabeloutput << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, seqInputDeriv, seqSize, Wmatrix, lag, feaDim, hiddenDim, seqOutDeriv);
}

__global__ void cuda_updaternnlabeloutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv, float wei)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchSize)
	{
		int seqBegin = idx == 0 ? 0 : smpIdx[idx - 1];
		int seqEnd = smpIdx[idx] - 1;
		int seqStart = (seqEnd - lag);
		if (seqStart < seqBegin) seqStart = seqBegin;

		int outStart = idx * hiddenDim;

		float * pSeqOutput = seqOutDeriv + outStart;

		for (int i = seqEnd; i >= seqStart; i--)
		{
			float * pSeqInput = seqInput + i * feaDim;
			float * pW = Wmatrix + (seqEnd - i) * feaDim * hiddenDim;
			for (int f = 0; f < feaDim; f++)
			{
				float * pSeqCursor = pSeqOutput;
				float seqInputValue = (*pSeqInput);
				for (int h = 0; h < hiddenDim; h++)
				{
					atomicAdd(pW, (*pSeqCursor) * seqInputValue * wei);
					//(*pW) = (*pW) + (*pSeqCursor) * seqInputValue * wei;
					pW++;
					pSeqCursor++;
				}
				pSeqInput++;
			}
			//seqOut[outStart + h] = sum;
		}
	}
}

void cuda_UpdateRNNLabelOutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv, float wei)
{
	int nThreadPerBlock = DEFAULT_THREAD_PER_BLOCK;
	int nBlockPerGrid = (batchSize + DEFAULT_THREAD_PER_BLOCK - 1) / DEFAULT_THREAD_PER_BLOCK;
	cuda_updaternnlabeloutput << <nBlockPerGrid, nThreadPerBlock >> >(smpIdx, batchSize, seqInput, seqSize, Wmatrix, lag, feaDim, hiddenDim, seqOutDeriv, wei);
}


__global__ void cuda_LogAdd(float * simi, int negativeSample, float gamma, int batchSize, float * logSum)
{
	int id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < batchSize)
	{
		float * pSimi = simi + id * (negativeSample + 1);
		float pos = gamma * (*pSimi);
		float sum = pos;

		for(int i=0;i<negativeSample;i++)
		{
			pSimi++;
			float v = gamma * (*pSimi);
			if(sum > v) sum = sum + logf(1 + expf(v- sum));
			else sum = v + logf(1 + expf(sum - v));
		}
		logSum[id] = sum;
	}    
}

__global__ void cuda_softmaxUpdate(float * targets, int targetNum, 
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, int * negIds)
{
	int tId = blockIdx.x;
	int sId = threadIdx.x;

	int Idx = -1;
	float param1 = 0;
	if(sId == 0)
	{
		Idx = (int)targets[tId];
		param1 = ( 1 - expf(simi[tId * (negativeSample + 1)] * gamma - alpha[tId])) * gamma;	
	}
	else
	{
		Idx = negIds[tId * negativeSample + sId - 1];
		param1 = - (expf(simi[tId * (negativeSample + 1) + sId] * gamma - alpha[tId])) * gamma;
	}
	//__syncthreads();

	float param2 = step * param1;

	float * pVspace = vSpace + Idx * dim;
	float * pDeriv = deriv + tId * dim;

	for(int i=0;i<dim;i++)
	{
		atomicAdd(pDeriv + i, param1 * (*pVspace));
		pVspace++;
	}
	__syncthreads();

	pVspace = vSpace + Idx * dim;
	float * pEmbed = embedding + tId * dim;

	atomicAdd(vBias + Idx, param2);
	for(int i=0;i<dim;i++)
	{
		atomicAdd(pVspace, param2 * (*pEmbed));
		pVspace++;
		pEmbed++;
	}
}

__global__ void cuda_binaryUpdate(float * targets, int targetNum, 
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, int * negIds)
{
	int tId = blockIdx.x;
	int sId = threadIdx.x;
	//extern __shared__ float embeds[];

	int Idx = -1;
	float param1 = 0;
	if(sId == 0)
	{
		Idx = (int)targets[tId];
		if(simi[tId * (negativeSample + 1)] * gamma > 20)
			param1 = 0;
		else if(simi[tId * (negativeSample + 1)] * gamma < -20)
			param1 = gamma;
		else
			param1 = ( 1 - 1.0f / (1 + expf(- simi[tId * (negativeSample + 1)] * gamma))) * gamma;	
	}
	else
	{
		Idx = negIds[tId * negativeSample + sId - 1];

		if(simi[tId * (negativeSample + 1) + sId] * gamma > 20)
			param1 = -gamma;
		else if(simi[tId * (negativeSample + 1) + sId] * gamma < -20)
			param1 = 0;
		else
			param1 = - 1.0f / (1 + (expf(-simi[tId * (negativeSample + 1) + sId] * gamma))) * gamma;
	}
	//__syncthreads();

	float param2 = step * param1;

	float * pVspace = vSpace + Idx * dim;
	float * pDeriv = deriv + tId * dim;

	for(int i=0;i<dim;i++)
	{
		atomicAdd(pDeriv + i, param1 * (*pVspace));
		pVspace++;
	}

	__syncthreads();
	pVspace = vSpace + Idx * dim;
	float * pEmbed = embedding + tId * dim;

	atomicAdd(vBias + Idx, param2);
	for(int i=0;i<dim;i++)
	{
		atomicAdd(pVspace, param2 * (*pEmbed));	
		pVspace++;
		pEmbed++;
	}
}

__global__ void cuda_ComputeVSpaceInnerProduct(float * targets, int targetNum, float * embedding, float * deriv, int dim,
		float * vSpace,  float * vBias, int vocabSize, float step, float * simi, int negativeSample, float gamma, int * negIds)
{
	int tId = blockIdx.x;
	int sId = threadIdx.x;
	//extern __shared__ float embeds[];

	int Idx = -1;
	if(sId == 0) Idx = (int)targets[tId];
	else Idx = negIds[tId * negativeSample + sId - 1];

	/*
	   __syncthreads();
	 */
	float * vVector = vSpace + Idx * dim;
	float * pEmbed = embedding + tId * dim;
	float sum = 0;
	for(int i=0;i<dim; i++)
	{
		sum += (*pEmbed) * (*vVector);
		vVector++;
		pEmbed++;
	}
	simi[tId * (negativeSample + 1) + sId] = sum + vBias[Idx];
}



void Cuda_UpdateBinaryEmbedding_NegIds(float * targets, int targetNum, 
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds)
{
	int nThreadPerBlock = negativeSample + 1;
	int nBlockPerGrid = targetNum;

	/// compute the similarity of positive samples. , dim
	cuda_ComputeVSpaceInnerProduct<<<nBlockPerGrid, nThreadPerBlock>>>(targets, targetNum, embedding, deriv, dim, 
			vSpace, vBias, vocabSize, step, simi, negativeSample, gamma, negIds);

	cuda_binaryUpdate<<< nBlockPerGrid, nThreadPerBlock>>>
		(targets, targetNum, embedding, deriv, dim,
		 vSpace, vBias, vocabSize, step, simi, alpha, negativeSample,  gamma, negIds);
}

void Cuda_UpdateSoftmaxEmbedding_NegIds(float * targets, int targetNum, 
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds)
{
	int nThreadPerBlock = negativeSample + 1;
	int nBlockPerGrid = targetNum;

	/// compute the similarity of positive samples. , dim
	cuda_ComputeVSpaceInnerProduct<<<nBlockPerGrid, nThreadPerBlock>>>(targets, targetNum, embedding, deriv, dim, 
			vSpace, vBias, vocabSize, step, simi, negativeSample, gamma, negIds);

	cuda_LogAdd<<< (targetNum - 1)/DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK>>>
		(simi, negativeSample, gamma, targetNum, alpha);

	// 
	cuda_softmaxUpdate<<< nBlockPerGrid, nThreadPerBlock>>>
		(targets, targetNum, embedding, deriv, dim, vSpace, vBias, vocabSize, step, simi, alpha, negativeSample,  gamma, negIds);
}




__global__ void cuda_ComputeVSpaceRandomNegativeInnerProduct(float * targets, int targetNum, float * embedding, float * deriv, int dim,
		float * vSpace,  float * vBias, int vocabSize, float step, float * simi, int negativeSample, float gamma, int * negIds,
		float * randVar, int randLen, int randStart)
{
	int tId = blockIdx.x;
	int sId = threadIdx.x;
	//extern __shared__ float embeds[];

	int Idx = -1;
	if(sId == 0)
	{
		Idx = (int)targets[tId];
	}
	else
	{
		Idx =  (int)(randVar[(randStart + tId * negativeSample + sId * 50 + 1) % randLen] * (vocabSize)) % vocabSize;
		negIds[tId * negativeSample + sId - 1] = Idx;
	}
	/*
	   __syncthreads();
	 */
	float * vVector = vSpace + Idx * dim;
	float * pEmbed = embedding + tId * dim;
	float sum = 0;
	for(int i=0;i<dim; i++)
	{
		sum += (*pEmbed) * (*vVector);
		vVector++;
		pEmbed++;
	}
	simi[tId * (negativeSample + 1) + sId] = sum + vBias[Idx];
}


void Cuda_UpdateSoftmaxEmbedding(float * targets, int targetNum, 
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds,
		float * randVar, int randLen, int randStart)
{
	int nThreadPerBlock = negativeSample + 1;
	int nBlockPerGrid = targetNum;

	/// compute the similarity of positive samples. , dim
	cuda_ComputeVSpaceRandomNegativeInnerProduct<<<nBlockPerGrid, nThreadPerBlock>>>(targets, targetNum, embedding, deriv, dim, 
			vSpace, vBias, vocabSize, step, simi, negativeSample, gamma, negIds, randVar, randLen, randStart);

	cuda_LogAdd<<< (targetNum - 1)/DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK>>>
		(simi, negativeSample, gamma, targetNum, alpha);

	// 
	cuda_softmaxUpdate<<< nBlockPerGrid, nThreadPerBlock>>>
		(targets, targetNum, embedding, deriv, dim,
		 vSpace, vBias, vocabSize, step, simi, alpha, negativeSample,  gamma, negIds);
}


void Cuda_UpdateEmbedding(float * targets, int targetNum, 
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds,
		float * randVar, int randLen, int randStart)
{
	int nThreadPerBlock = negativeSample + 1;
	int nBlockPerGrid = targetNum;

	/// compute the similarity of positive samples. , dim
	cuda_ComputeVSpaceRandomNegativeInnerProduct<<<nBlockPerGrid, nThreadPerBlock>>>(targets, targetNum, embedding, deriv, dim, 
			vSpace, vBias, vocabSize, step, simi, negativeSample, gamma, negIds, randVar, randLen, randStart);

	//cuda_LogAdd<<< (targetNum - 1)/DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK>>>
	//	(simi, negativeSample, gamma, targetNum, alpha);
	// 
	cuda_binaryUpdate<<< nBlockPerGrid, nThreadPerBlock>>>
		(targets, targetNum, embedding, deriv, dim,
		 vSpace, vBias, vocabSize, step, simi, alpha, negativeSample,  gamma, negIds);
}

__global__ void cuda_ComputeVSpaceRandomBlanceInnerProduct(float * targets, int targetNum, float * embedding, float * deriv, int dim,
		float * vSpace,  float * vBias, int vocabSize, float step, float * simi, int negativeSample, float gamma, int * negIds,
		float * randVar, int randLen, int randStart, int * sampleTable, int tableSize)
{
	int tId = blockIdx.x;
	int sId = threadIdx.x;

	int Idx = -1;
	if(sId == 0)
	{
		Idx = (int)targets[tId];
	}
	else
	{
		//int m = ((int)(randVar[(randStart + tId * negativeSample + sId * 50 + 1) % randLen] * tableSize)) % tableSize;
		//Idx =  sampleTable[m];
		//negIds[tId * negativeSample + sId - 1] = Idx;

		//Idx =  (int)(randVar[(randStart + tId * negativeSample + sId * 50 + 1) % randLen] * (vocabSize)) % vocabSize;
		//negIds[tId * negativeSample + sId - 1] = Idx;

		int sm =  (int)(randVar[(randStart + tId * negativeSample + sId * 50 + 1) % randLen] * (tableSize)) % tableSize;
		Idx =  sampleTable[sm];
		negIds[tId * negativeSample + sId - 1] = Idx;


	}
	float * vVector = vSpace + Idx * dim;
	float * pEmbed = embedding + tId * dim;
	float sum = 0;
	for(int i=0;i<dim; i++)
	{
		sum += (*pEmbed) * (*vVector);
		vVector++;
		pEmbed++;
	}
	simi[tId * (negativeSample + 1) + sId] = sum + vBias[Idx];
}


void Cuda_UpdateBalanceEmbedding(float * targets, int targetNum, 
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds,
		float * randVar, int randLen, int randStart,
		int * sampleTable, int tableSize)
{
	int nThreadPerBlock = negativeSample + 1;
	int nBlockPerGrid = targetNum;

	/// compute the similarity of positive samples. , dim
	cuda_ComputeVSpaceRandomBlanceInnerProduct<<<nBlockPerGrid, nThreadPerBlock>>>(targets, targetNum, embedding, deriv, dim, 
			vSpace, vBias, vocabSize, step, simi, negativeSample, gamma, negIds, randVar, randLen, randStart, sampleTable, tableSize);

	//cuda_LogAdd<<< (targetNum - 1)/DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK>>>
	//	(simi, negativeSample, gamma, targetNum, alpha);
	// 
	cuda_binaryUpdate<<< nBlockPerGrid, nThreadPerBlock>>>
		(targets, targetNum, embedding, deriv, dim,
		 vSpace, vBias, vocabSize, step, simi, alpha, negativeSample,  gamma, negIds);
}

__global__ void init_stuff(curandState *state) 
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	curand_init(2015, idx, 0, &state[idx]);
}

__global__ void make_rand(curandState *state, float *randArray) 
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	randArray[idx] = curand_uniform(&state[idx]);
}

void Cuda_RandomNumber(float * value, int size)
{
	curandState *d_state;
	int threadNum = DEFAULT_THREAD_PER_BLOCK;
	int blockNum = (size - 1) / DEFAULT_THREAD_PER_BLOCK + 1;
	cudaMalloc(&d_state, threadNum * blockNum);
	init_stuff<<<blockNum, threadNum>>>(d_state);
	make_rand<<<blockNum, threadNum>>>(d_state, value);
	cudaFree(d_state);
}

/**LSTM Optimization**/
//InstanceIndex: #smp_1, #smp_1 + #smp_2, ...
//InstanceStep: #smp_1, #smp_2, ...
__global__ void cuda_CalculateTimeStep(int *InstanceIndex, int batchSize, int *InstanceSteps)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		if (idx > 0)
		{
			InstanceSteps[idx] = InstanceIndex[idx] - InstanceIndex[idx - 1];
		}
		else
		{
			InstanceSteps[idx] = InstanceIndex[idx];
		}
	}
}

void Cuda_CalculateTimeStep(int *InstanceIndex, int batchSize, int *InstanceSteps)
{
	cuda_CalculateTimeStep <<<(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >>>(InstanceIndex, batchSize, InstanceSteps);
}

__global__ void cuda_SeqTransMatrixProjection(float *Y, float* W, float *X, int *InstanceIndex, int row, int col, int tidx_y, int tidx_x, int batchSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < col && idy < batchSize)
	{
		int seqBegin = idy == 0 ? 0 : InstanceIndex[idy - 1];
		int seqEnd = InstanceIndex[idy];
		int length = seqEnd - seqBegin;
		if (tidx_x < length && tidx_y < length)
		{
			float sum = 0;
			float *wptr = W + idx;
			float *xptr = X + (seqBegin + tidx_x) * row;
			int iy = (seqBegin + tidx_y)*row + idx;
			for (int i = 0; i < row; i++)
			{
				sum += (*wptr) * (*xptr);
				wptr += col;
				xptr++;
			}
			Y[iy] = Y[iy] * weight + sum;
		}
	}
}

void Cuda_SeqTransMatrixProjectionByAx(float *Y, float* W, float *X, int *InstanceIndex, int row, int col, int tidx_y, int tidx_x, int batchSize, float weight)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((col + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_SeqTransMatrixProjection << <block_tail, thread_tail >> > (Y, W, X, InstanceIndex, row, col, tidx_y, tidx_x, batchSize, weight);
}

__global__ void cuda_SeqMatrixProjection(float *Y, float* W, float *X, int *InstanceIndex, int row, int col, int tidx_y, int tidx_x, int batchSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < row && idy < batchSize)
	{
		int seqBegin = idy == 0 ? 0 : InstanceIndex[idy - 1];
		int seqEnd = InstanceIndex[idy];
		int length = seqEnd - seqBegin;
		if (tidx_x < length && tidx_y < length)
		{
			float sum = 0;
			int iy = (seqBegin + tidx_y)*row + idx;
			float* wptr =W + idx * row;
			float* xptr = X + (seqBegin + tidx_x)*row;
			for (int i = 0; i < col; i++)
			{
				sum += (* wptr)* (*xptr);
				wptr++;
				xptr++;
			}
			Y[iy] = Y[iy] * weight + sum;
		}
	}
}

void Cuda_SeqMatrixProjectionByAx(float *Y, float* W, float *X, int *InstanceIndex, int row, int col, int tidx_y, int tidx_x, int batchSize, float weight)
{
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((row + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (batchSize + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);
	cuda_SeqMatrixProjection << <block_tail, thread_tail >> > (Y, W, X, InstanceIndex, row, col, tidx_y, tidx_x, batchSize, weight);
}


__global__ void cuda_Logistic(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < dim)
	{
		int seqBeg = idx == 0 ? 0 : InstanceIdx[idx - 1];
		int seqEnd = InstanceIdx[idx];
		int length = seqEnd - seqBeg;
		if (tidx_x < length && tidx_y < length)
		{
			float v = tanhf(X[(seqBeg + tidx_x) * dim + idy] * gamma / 2.0);
			Y[(seqBeg + tidx_y) * dim + idy] = (v + 1.0) / 2.0;
		}
	}
}

void Cuda_Logistic(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y,  int tidx_x, int batchSize, float gamma)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Logistic << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, tidx_y, tidx_x, batchSize, gamma);
}

__global__ void cuda_DerivLogistic(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < dim)
	{
		int seqBeg = idx == 0 ? 0 : InstanceIdx[idx - 1];
		int seqEnd = InstanceIdx[idx];
		int length = seqEnd - seqBeg;
		if (tidx_x < length && tidx_y < length)
		{
			int iy = (seqBeg + tidx_y) * dim + idy;
			int ix = (seqBeg + tidx_x) * dim + idy;
			Y[iy] = Y[iy] * X[ix] * (1 - X[ix]) * gamma;
		}
	}
}

void Cuda_DerivLogistic(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize, float gamma)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivLogistic << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, tidx_y, tidx_x, batchSize, gamma);
}

__global__ void cuda_Tanh(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < dim)
	{
		int seqEnd = InstanceIdx[idx];
		int seqBeg = idx == 0 ? 0 : InstanceIdx[idx - 1];
		int length = seqEnd - seqBeg;

		if (tidx_x < length && tidx_y < length)
		{
			Y[(seqBeg + tidx_y) * dim + idy] = tanhf(X[(seqBeg + tidx_x) * dim + idy]);
		}
	}
}

void Cuda_Tanh(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Tanh <<<nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, tidx_y, tidx_x, batchSize);
}

__global__ void cuda_DerivTanh(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < dim)
	{
		int seqEnd = InstanceIdx[idx];
		int seqBeg = idx == 0 ? 0 : InstanceIdx[idx - 1];
		int length = seqEnd - seqBeg;
		int iy = (seqBeg + tidx_y) * dim + idy;
		int ix = (seqBeg + tidx_x) * dim + idy;
		if (tidx_x < length && tidx_y < length)
		{
			Y[iy] = Y[iy] * (1 - X[ix] * X[ix]);
		}
	}
}

void Cuda_DerivTanh(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivTanh << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, tidx_y, tidx_x, batchSize);
}

__global__ void cuda_ElementWiseProduct(float *Z, float *Y, float *X, int *InstanceIdx, int dim, int tidx_z, int tidx_y, int tidx_x, int batchSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < dim)
	{
		int seqEnd = InstanceIdx[idx];
		int seqBeg = idx == 0 ? 0 : InstanceIdx[idx - 1];
		int length = seqEnd - seqBeg;
		int iz = (seqBeg + tidx_z) * dim + idy;
		if (tidx_x < length && tidx_y < length && tidx_z < length)
		{
			Z[iz] = Z[iz] * weight + Y[(seqBeg + tidx_y) * dim + idy] * X[(seqBeg + tidx_x) * dim + idy];
		}
	}
}

void Cuda_ElementWiseProduct(float *Z, float *Y, float *X, int *InstanceIdx, int dim, int tidx_z, int tidx_y, int tidx_x, int batchSize, float weight)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_ElementWiseProduct << <nBlockPerGrid, nThreadPerBlock >> >(Z, Y, X, InstanceIdx, dim, tidx_z, tidx_y, tidx_x, batchSize, weight);
}

__global__ void cuda_ElementWiseAdd(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < dim)
	{
		int seqEnd = InstanceIdx[idx];
		int seqBeg = idx == 0 ? 0 : InstanceIdx[idx - 1];
		int length = seqEnd - seqBeg;
		int iy = (seqBeg + tidx_y) * dim + idy;
		if (tidx_x < length && tidx_y < length)
		{
			Y[iy] = Y[iy] * weight + X[(seqBeg + tidx_x) * dim + idy];
		}
	}
}

// y = y * weight + x
void Cuda_ElementWiseAdd(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize, float weight)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_ElementWiseAdd << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, tidx_y, tidx_x, batchSize, weight);
}


void Cuda_LSTMForwardPropagateBatchOpt(int * InstanceIndex, int batchSize,  //->x;
		float *o, //-> output,
		float *gate_i, //->gate i,
		float *c_hat, //->candidate memory,
		float *gate_f, //->forget gate,
		float *c, //->memory,
		float *gate_o, //->gate o,
		float *tanhc, //->tanh memory
		int cell,
		float *Ui,
		float *Uc,
		float *Uf,
		float *Uo, float *Vo)
{
	int *steps;
	cudaMalloc((void **)&steps, batchSize * sizeof(int));
	Cuda_CalculateTimeStep(InstanceIndex, batchSize, steps);
	int maxSeq = Cuda_Max(steps, batchSize);

	for (int t = 0; t < maxSeq; t++)
	{
		if (t > 0)
		{
			//gate_i += U_{i} * o_{t-1}
			Cuda_SeqTransMatrixProjectionByAx(gate_i, Ui, o, InstanceIndex, cell, cell, t, (t - 1), batchSize, 1.0f);
			//gate_f += U_{f} * o_{t-1}
			Cuda_SeqTransMatrixProjectionByAx(gate_f, Uf, o, InstanceIndex, cell, cell, t, (t - 1), batchSize, 1.0f);
			//c_hat += U_{c} * o_{t-1}
			Cuda_SeqTransMatrixProjectionByAx(c_hat, Uc, o, InstanceIndex, cell, cell, t, (t - 1), batchSize, 1.0f);
			//gate_o += U_{o} * o_{t-1}
			Cuda_SeqTransMatrixProjectionByAx(gate_o, Uo, o, InstanceIndex, cell, cell, t, (t - 1), batchSize, 1.0f);
		}
		//gate_i = sigmoid(gate_i)
		Cuda_Logistic(gate_i, gate_i, InstanceIndex, cell, t, t, batchSize, 1);
		//gate_f = sigmoid(gate_f)
		Cuda_Logistic(gate_f, gate_f, InstanceIndex, cell, t, t, batchSize, 1);
		//c_hat = tanh(c_hat)
		Cuda_Tanh(c_hat, c_hat, InstanceIndex, cell, t, t, batchSize);
		//c = gate_i @ c_hat
		Cuda_ElementWiseProduct(c, c_hat, gate_i, InstanceIndex, cell, t, t, t, batchSize, 0);
		//c += gate_f @ c(t-1)
		if (t> 0)
		{
			Cuda_ElementWiseProduct(c, gate_f, c, InstanceIndex, cell, t, t, t - 1, batchSize, 1.0f);
		}

		//gate_o += Vo * c(t)
		Cuda_SeqTransMatrixProjectionByAx(gate_o, Vo, c, InstanceIndex, cell, cell, t, t, batchSize, 1.0f);
		//gate_o = simgoid(gate_o)
		Cuda_Logistic(gate_o, gate_o, InstanceIndex, cell, t, t, batchSize, 1);

		//tanhc = tanh(c)
		Cuda_Tanh(tanhc, c, InstanceIndex, cell, t, t, batchSize);

		////o = gate_o @ tanhc
		Cuda_ElementWiseProduct(o, tanhc, gate_o, InstanceIndex, cell, t, t, t, batchSize, 0);
	}
	cudaFree(steps);
}

void Cuda_LSTMBackwardPropagateBatchOpt(int *InstanceIndex, int batchSize, int seqSize, int cell,
		float *o,
		float *gate_i,
		float *c_hat,
		float *gate_f,
		float *c,
		float *gate_o,
		float *tanhc,

		float *deriv_o,
		float *deriv_gate_i,
		float *deriv_cHat,
		float *deriv_gate_f,
		float *deriv_c,
		float *deriv_gate_o,
		float *deriv_tanhc,
		float *Ui, float *Uc, float *Uf, float *Uo, float *Vo)
{
	int *steps;
	cudaMalloc((void **)&steps, batchSize * sizeof(int));
	Cuda_CalculateTimeStep(InstanceIndex, batchSize, steps);
	int maxSeq = Cuda_Max(steps, batchSize);

	for (int t = maxSeq - 1; t >= 0; t--)
	{
		// deriv_gate_o = deriv_o @ tanhc
		Cuda_ElementWiseProduct(deriv_gate_o, tanhc, deriv_o, InstanceIndex, cell, t, t, t, batchSize, 0);
		// deriv_gate_o = deriv_gate_o *  gate_o * (1 - gate_o)
		Cuda_DerivLogistic(deriv_gate_o, gate_o,InstanceIndex, cell, t, t, batchSize, 1.0f);

		//deriv_tanhc = deriv_o @ gate_o
		Cuda_ElementWiseProduct(deriv_tanhc, deriv_o, gate_o, InstanceIndex, cell, t, t, t, batchSize, 0);
		// deriv_tanhc = deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
		Cuda_DerivTanh(deriv_tanhc, tanhc, InstanceIndex, cell, t, t, batchSize);

		if (t > 0)
		{
			// deriv_o(t-1) += deriv_gate * Uo
			Cuda_SeqMatrixProjectionByAx(deriv_o, Uo, deriv_gate_o, InstanceIndex, cell, cell, t - 1, t, batchSize, 1.0f);
		}

		// deriv_c += deriv_gate_o * Vo
		Cuda_SeqMatrixProjectionByAx(deriv_c, Vo, deriv_gate_o, InstanceIndex, cell, cell, t, t, batchSize, 1.0f);
		//deriv_c += deriv_tanhc
		Cuda_ElementWiseAdd(deriv_c, deriv_tanhc, InstanceIndex, cell, t, t, batchSize, 1.0f);

		// deriv_c_hat = deriv_c @ gate_i
		Cuda_ElementWiseProduct(deriv_cHat, deriv_c, gate_i, InstanceIndex, cell, t, t, t, batchSize, 0);
		// deriv_c_hat = deriv_c_hat * (1 + c_hat)*( 1- c_hat)
		Cuda_DerivTanh(deriv_cHat, c_hat, InstanceIndex, cell, t, t, batchSize);

		//deriv_gate_i = deriv_c @ c_hat
		Cuda_ElementWiseProduct(deriv_gate_i, deriv_c, c_hat, InstanceIndex, cell, t, t, t, batchSize, 0);
		//deriv_gate_i = deriv_gate_i * gate_i * (1 - gate_i)
		Cuda_DerivLogistic(deriv_gate_i, gate_i, InstanceIndex, cell, t, t, batchSize, 1.0f);

		if (t > 0)
		{
			//deriv_gate_f = deriv_c * c(t-1)
			Cuda_ElementWiseProduct(deriv_gate_f, deriv_c, c, InstanceIndex, cell, t, t, t - 1, batchSize, 0);
			//deriv_gate_f = deriv_gate_f * (1 -gate_f) * gate_f
			Cuda_DerivLogistic(deriv_gate_f, gate_f, InstanceIndex, cell, t, t, batchSize, 1.0f);
			//deriv_o(t -1) += deriv_gate_f * Uf
			Cuda_SeqMatrixProjectionByAx(deriv_o, Uf, deriv_gate_f, InstanceIndex, cell, cell, t - 1, t, batchSize, 1.0f);

			//deriv_o(t-1) += deriv_gate_i * Ui
			Cuda_SeqMatrixProjectionByAx(deriv_o, Ui, deriv_gate_i, InstanceIndex, cell, cell, t - 1, t, batchSize, 1.0f);
			//deriv_o(t-1) += deriv_cHat(t) * Uc
			Cuda_SeqMatrixProjectionByAx(deriv_o, Uc, deriv_cHat, InstanceIndex, cell, cell, t - 1, t, batchSize, 1.0f);

			//deriv_c(t-1) += deriv_c @ gate_f
			Cuda_ElementWiseProduct(deriv_c, deriv_c, gate_f, InstanceIndex, cell, t - 1, t, t, batchSize, 1.0f);
		}
	}
	cudaFree(steps);
}

/***** GRU *****/
__global__ void cuda_ScalarVectorAdd(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize, float scalar, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < batchSize && idy < dim)
	{
		int seqEnd = InstanceIdx[idx];
		int seqBeg = idx == 0 ? 0 : InstanceIdx[idx - 1];
		int length = seqEnd - seqBeg;
		if (tidx_x < length && tidx_y < length)
		{
			Y[(seqBeg + tidx_y) * dim + idy] =scalar + weight  * X[(seqBeg + tidx_x) * dim + idy];
		}
	}
}

// y = x * weight + s
void Cuda_ScalarVectorAdd(float *Y, float *X, int *InstanceIdx, int dim, int tidx_y, int tidx_x, int batchSize, float scalar, float weight)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((batchSize - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_ScalarVectorAdd << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, tidx_y, tidx_x, batchSize, scalar, weight);
}

//float Cuda_L2Norm(float *x, int size)
//{
//	float rv = 0.0f;
//	cublasHandle_t handle = NULL;
//	cublasCreate(&handle);
//	cublasSnrm2(handle, size, x, 1, &rv);
//	cudaFree(&handle);
//	cublasDestroy(handle);
//	return rv;
//}

__global__ void cuda_L2Norm(float *X, float *result, int size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	__shared__ float sum_values[DEFAULT_THREAD_PER_DIM];
	sum_values[idx] = 0;
	for (int i = idx; i < size; i += DEFAULT_THREAD_PER_DIM)
	{
		sum_values[idx] += X[i] * X[i];
	}
	__syncthreads();
	if (idx == 0)
	{
		float sum = 0.0f;
		for (int i = 0; i < DEFAULT_THREAD_PER_DIM; i++)
		{
			sum += sum_values[i];
		}
		result[0] = sqrtf(sum);
	}
}

float Cuda_L2Norm(float *X, int size)
{
	float *result;
	cudaMalloc((void**)&result, sizeof(float));
	cuda_L2Norm << <1, DEFAULT_THREAD_PER_DIM >> >(X, result, size);
	float mr = 0;
	cudaMemcpy(&mr, result, sizeof(float), cudaMemcpyDeviceToHost);
	cudaFree(result);
	return mr;
}

__global__ void cuda_MatrixL1Norm(float *x, int dim, int batchSize, float * l1Score)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchSize)
	{
		float sum = 0;
		for (int i = 0; i < dim; i++) sum += fabsf(x[idx * dim + i]);
		l1Score[idx] = sum;
	}
}

__global__ void cuda_DerivMatrixL1Norm(float *x, int dim, int batchSize, float * l1Score, float * l1ScoreDeriv, float * xDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchSize)
	{
		for (int i = 0; i < dim; i++)
		{
			xDeriv[idx * dim + i] = x[idx * dim + i] >= 0 ? l1ScoreDeriv[idx] : -l1ScoreDeriv[idx];
		}
	}
}

__global__ void cuda_MatrixL2Norm(float *x, int dim, int batchSize, float * l2Score)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	if (idx < batchSize)
	{
		float sum = 0;
		for (int i = 0; i < dim; i++) sum += x[idx * dim + i] * x[idx * dim + i];
		l2Score[idx] = sqrtf(sum);
	}
}

__global__ void cuda_DerivMatrixL2Norm(float *x, int dim, int batchSize, float * l2Score, float * l2ScoreDeriv, float * xDeriv)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < batchSize)
	{
		for (int i = 0; i < dim; i++)
		{
			xDeriv[idx * dim + i] = l2ScoreDeriv[idx] / l2Score[idx] * x[idx * dim + i];
		}
	}
}


void Cuda_MatrixL1Norm(float *x, int dim, int batchSize, float * l1Score)
{
	cuda_MatrixL1Norm << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(x, dim, batchSize, l1Score);
}

void Cuda_DerivMatrixL1Norm(float *x, int dim, int batchSize, float * l1Score, float * l1ScoreDeriv, float * xDeriv)
{
	cuda_DerivMatrixL1Norm << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(x, dim, batchSize, l1Score, l1ScoreDeriv, xDeriv);
}


void Cuda_MatrixL2Norm(float *x, int dim, int batchSize, float * l2Score)
{
	cuda_MatrixL2Norm << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(x, dim, batchSize, l2Score);
}

void Cuda_DerivMatrixL2Norm(float *x, int dim, int batchSize, float * l2Score, float * l2ScoreDeriv, float * xDeriv)
{
	cuda_DerivMatrixL2Norm << <(batchSize - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >(x, dim, batchSize, l2Score, l2ScoreDeriv, xDeriv);
}

void Cuda_GRUForwardPropagateBatch(int * InstanceIndex, int batchSize, int Dim, //->x;
		float *H, //->hidden,
		float *GateR, //->reset gate,
		float *GateZ, //->update gate,
		float *HHat, //->chat
		float *RestH,
		float * Ur,
		float * Uz,
		float * Uh)

{
	int *steps;
	cudaMalloc((void **)&steps, batchSize * sizeof(int));
	Cuda_CalculateTimeStep(InstanceIndex, batchSize, steps);
	int maxSeq = Cuda_Max(steps, batchSize);


	for (int t = 0; t < maxSeq; t++)
	{
		if (t > 0)
		{
			// GateR += Ur * h(t-1)
			Cuda_SeqTransMatrixProjectionByAx(GateR, Ur, H, InstanceIndex, Dim, Dim, t, t - 1, batchSize, 1.0f);

			// GateZ += Uz * h(t-1)
			Cuda_SeqTransMatrixProjectionByAx(GateZ, Uz, H, InstanceIndex, Dim, Dim, t, t - 1, batchSize, 1.0f);
		}

		//GateR = sigmoid(GateR)
		Cuda_Logistic(GateR, GateR, InstanceIndex, Dim, t, t, batchSize, 1.0f);

		//GateZ = sigmoid(GateZ)
		Cuda_Logistic(GateZ, GateZ, InstanceIndex, Dim, t, t, batchSize, 1.0f);

		if (t > 0)
		{
			//RestH(t) = GateR(t) @ h(t-1)
			Cuda_ElementWiseProduct(RestH, GateR, H, InstanceIndex, Dim, t, t, t - 1, batchSize, 0);

			//HHat = Uh * RestH
			Cuda_SeqTransMatrixProjectionByAx(HHat, Uh, RestH, InstanceIndex, Dim, Dim, t, t, batchSize, 1.0f);
		}
		/// HHat = tanh(HHat);
		Cuda_Tanh(HHat, HHat, InstanceIndex, Dim, t, t, batchSize);

		//H = 1 - GateZ(t)
		Cuda_ScalarVectorAdd(H, GateZ, InstanceIndex, Dim, t, t, batchSize, 1.0f, -1.0f);
		//h(t) = HHat @ h(t)
		Cuda_ElementWiseProduct(H, H, HHat, InstanceIndex, Dim, t, t, t, batchSize, 0);

		if (t > 0)
		{
			/// h(t) += z(t) @ h(t-1)
			Cuda_ElementWiseProduct(H, GateZ, H, InstanceIndex, Dim, t, t, t - 1, batchSize, 1.0f);
		}
	}
	cudaFree(steps);
}

void Cuda_GRUBackwardPropagateBatch(int *InstanceIndex, int batchSize,  int Dim,//->x
		float *H, //->hidden
		float *GateR, //->reset gate,
		float *GateZ, //->update gate,
		float *HHat, //->hat
		float *RestH, //H @ R
		float *DerivH,
		float *DerivGateR,
		float *DerivGateZ,
		float *DerivHHat,
		float *DerivRestH,
		float * Ur,
		float * Uz,
		float * Uh)
{
	int *steps;
	cudaMalloc((void **)&steps, batchSize * sizeof(int));
	Cuda_CalculateTimeStep(InstanceIndex, batchSize, steps);
	int maxSeq = Cuda_Max(steps, batchSize);

	float *tempMatrix;
	cudaMalloc((void**)&tempMatrix, batchSize * maxSeq * Dim * sizeof(float));
	for (int t = maxSeq - 1; t >= 0; t--)
	{
		//tempMatrix =  1 - GateZ(t)
		Cuda_ScalarVectorAdd(tempMatrix, GateZ, InstanceIndex, Dim, t, t, batchSize, 1.0f, -1.0f);
		//DerivHHat = DerivH @ tempMatrix
		Cuda_ElementWiseProduct(DerivHHat, DerivH, tempMatrix, InstanceIndex, Dim, t, t, t, batchSize, 0);

		//DerivHHat = DerivHHat * (1 - HHat) * (1 + HHat)
		Cuda_DerivTanh(DerivHHat, HHat, InstanceIndex, Dim, t, t, batchSize);

		//tempMatrix = -HHat(t)
		Cuda_ScalarVectorAdd(tempMatrix, HHat, InstanceIndex, Dim, t, t, batchSize, 0, -1.0f);
		//DerivGateZ = tempMatrix @ DerivH
		Cuda_ElementWiseProduct(DerivGateZ, DerivH, tempMatrix, InstanceIndex, Dim, t, t, t, batchSize, 0);
		if (t > 0)
		{
			//DerivGateZ += DerivH @ H(t-1)
			Cuda_ElementWiseProduct(DerivGateZ, DerivH, H, InstanceIndex, Dim, t, t, t - 1, batchSize, 1.0f);
		}
		//DerivGateZ = DerivGateZ * ( 1 - GateZ ) * GateZ
		Cuda_DerivLogistic(DerivGateZ, GateZ, InstanceIndex, Dim, t, t, batchSize, 1.0f);

		//DerivRestH = Uh * HHat
		Cuda_SeqMatrixProjectionByAx(DerivRestH, Uh, DerivHHat, InstanceIndex, Dim, Dim, t, t, batchSize, 0);

		//DerivGateR = DerivRestH @ H(t-1)
		if (t > 0)
		{
			Cuda_ElementWiseProduct(DerivGateR, DerivRestH, H, InstanceIndex, Dim, t, t, t - 1, batchSize, 0);
		}
		//DerivGateR = DerivGateR*GateR * (1 - GateR)
		Cuda_DerivLogistic(DerivGateR, GateR, InstanceIndex, Dim, t, t, batchSize, 1.0f);

		/***DerivH(t-1)**/
		if (t > 0)
		{
			//DerivH(t-1) += GateZ @ DerivH(t)
			Cuda_ElementWiseProduct(DerivH, DerivH, GateZ, InstanceIndex, Dim, t - 1, t, t, batchSize, 1.0f);

			//DerivH(t-1) += GateR @ DerivRestH
			Cuda_ElementWiseProduct(DerivH, DerivRestH, GateR, InstanceIndex, Dim, t - 1, t, t, batchSize, 1.0f);

			//DerivH(t-1) += Uz * DerivZ
			Cuda_SeqMatrixProjectionByAx(DerivH, Uz, DerivGateZ, InstanceIndex, Dim, Dim, t -1, t, batchSize, 1.0f);

			//DerivH(t-1) += Ur * DerivR
			Cuda_SeqMatrixProjectionByAx(DerivH, Ur, DerivGateR, InstanceIndex, Dim, Dim, t - 1, t, batchSize, 1.0f);

		}
	}

	cudaFree(tempMatrix);
	cudaFree(steps);
}

__global__ void cuda_RMSPropGradient(float * ada, float * grad, float gamma, float epsilon, int m)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		float gi = grad[id];
		ada[id] = gamma * ada[id] + (1 - gamma) * gi * gi;
		grad[id] = gi / (sqrtf(ada[id] + epsilon));
	}
}

void Cuda_RMSPropGradient(float * ada, float * grad, float gamma, float epsilon, int m)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_RMSPropGradient << <block_tail, thread_tail >> >(ada, grad, gamma, epsilon, m);
}

__global__ void cuda_RMSPropV2_Gradient(float * ada, float * g, float * grad, float gamma, float epsilon, int m)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		float gi = grad[id];
		g[id] = gamma * g[id] + (1 - gamma) * gi;
 		ada[id] = gamma * ada[id] + (1 - gamma) * gi * gi;
		grad[id] = gi / (sqrtf(ada[id] - g[id] * g[id] + epsilon));
	}
}

void Cuda_RMSPropV2_Gradient(float * ada, float * g, float * grad, float gamma, float epsilon, int m)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_RMSPropV2_Gradient << <block_tail, thread_tail >> >(ada, g, grad, gamma, epsilon, m);
}


__global__ void cuda_AdaMax(float *V, float *M, float *G, float alpha, float beta, float epsilon, int m)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		float val = beta * V[id];
		float absG = G[id];
		if (G[id] < 0) absG = -absG;

		if (absG > val)
		{
			V[id] = absG;
		}
		else
		{
			V[id] = val;
		}
		
		G[id] = alpha * M[id] / (V[id] + epsilon);
	}
}

void Cuda_AdaMax(float *V, float *M,float *G, float alpha, float beta, float epsilon, int m)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_AdaMax << <block_tail, thread_tail >> >(V, M, G, alpha, beta, epsilon, m);
}

__global__ void cuda_VectorSquareAdd(float *Z, float *X, float *Y, float wz, float wx, float wy, int m)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < m)
	{
		Z[id] = Z[id] * wz + X[id] * wx + Y[id] * Y[id] * wy;
	}
}

//Z = Z * wz + X * wx + Y * Y * wy
void Cuda_VectorSquareAdd(float *Z, float *X, float *Y, float wz, float wx, float wy, int m)
{
	int RowNum = (m - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_VectorSquareAdd << <block_tail, thread_tail >> >(Z, X, Y, wz, wx, wy, m);
}

__global__ void cuda_AdaMGradient(float *M, float *V, float *G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < size)
	{
		G[id] = alpha * M[id] / (sqrtf(V[id]) + epsilon);
	}
}

void Cuda_AdaMGradient(float *M, float *V, float *G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
{
	int RowNum = (size - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_AdaMGradient << <block_tail, thread_tail >> >(M, V, G, alpha, beta1, beta2,epsilon, t, size);
}

__global__ void cuda_AdaDeltaGradient(float *deltaX, float *AccumGrad, float *AccumUpdate, float *Grad, float epsilon, int size)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	int id = idy * COL_FOLD_NUM + idx;
	if (id < size)
	{
		deltaX[id] = Grad[id] * (sqrtf(AccumUpdate[id]) + epsilon) / (sqrtf(AccumGrad[id]) + epsilon);
	}
}

void Cuda_AdaDeltaGradient(float *deltaX, float *AccumGrad, float *AccumUpdate, float *Grad, float epsilon, int size)
{
	int RowNum = (size - 1) / COL_FOLD_NUM + 1;
	dim3 thread_tail(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 block_tail((COL_FOLD_NUM + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM, (RowNum + DEFAULT_THREAD_PER_DIM - 1) / DEFAULT_THREAD_PER_DIM);

	cuda_AdaDeltaGradient << <block_tail, thread_tail >> >(deltaX, AccumGrad, AccumUpdate, Grad, epsilon, size);
}
