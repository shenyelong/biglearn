#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <curand.h>
#include <curand_kernel.h>
#include <curand.h>
#include <curand_kernel.h>
#include "device_functions.h"
#include <fstream>
#include "cublas_v2.h"

#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

using namespace std;

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#endif

__global__ void cuda_Logistic(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < Sum && idy < dim)
	{
		int smpIdx = dev_mask[idx + maskIdx] - 1;
		int seqBeg = smpIdx == 0 ? 0 : InstanceIdx[smpIdx - 1];
		float v = tanhf(X[(seqBeg + tidx_x) * dim + idy] * gamma / 2.0f);
		Y[(seqBeg + tidx_y) * dim + idy] = (v + 1.0f) / 2.0f;
	}
}

void Cuda_Logistic(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float gamma)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((Sum - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Logistic << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, dev_mask, maskIdx, Sum, tidx_y, tidx_x, batchSize, gamma);
}

__global__ void cuda_Tanh(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < Sum && idy < dim)
	{
		int smpIdx = dev_mask[idx + maskIdx] - 1;
		int seqBeg = smpIdx == 0 ? 0 : InstanceIdx[smpIdx - 1];
		Y[(seqBeg + tidx_y) * dim + idy] = tanhf(X[(seqBeg + tidx_x) * dim + idy]);
	}
}

void Cuda_Tanh(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((Sum - 1) / DEFAULT_THREAD_PER_DIM + 1, (dim - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_Tanh << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, dev_mask, maskIdx, Sum, tidx_y, tidx_x, batchSize);
}

__global__ void cuda_ElementWiseProduct(float *Z, float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_z, int tidx_y, int tidx_x, int batchSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < Sum)
	{
		int smpIdx = dev_mask[idy + maskIdx] - 1;
		int seqBeg = smpIdx == 0 ? 0 : InstanceIdx[smpIdx - 1];
		int zIdx = (seqBeg + tidx_z) * dim + idx;
		int yIdx = (seqBeg + tidx_y) * dim + idx;
		int xIdx = (seqBeg + tidx_x) * dim + idx;
		Z[zIdx] = Z[zIdx] * weight + Y[yIdx] * X[xIdx];
	}
}
void Cuda_ElementWiseProduct(float *Z, float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_z, int tidx_y, int tidx_x, int batchSize, float weight)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((dim - 1) / DEFAULT_THREAD_PER_DIM + 1, (Sum - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_ElementWiseProduct << <nBlockPerGrid, nThreadPerBlock >> >(Z, Y, X, InstanceIdx, dim, dev_mask, maskIdx, Sum, tidx_z, tidx_y, tidx_x, batchSize, weight);
}


__global__ void cuda_SeqTransMatrixProjection(float *Y, float* W, float *X, int *InstanceIndex, int row, int col, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;


	float tmp = 0;
	__shared__ float tileA[32][32], tileB[32][32];

	tileA[threadIdx.y][threadIdx.x] = 0;
	tileB[threadIdx.x][threadIdx.y] = 0;

	if (idx < col && idy < Sum)
	{
		int smpIdx = dev_mask[idy + maskIdx] - 1;
		int seqBeg = smpIdx == 0 ? 0 : InstanceIndex[smpIdx - 1];
		float * pleft = X + (seqBeg + tidx_x) * row + threadIdx.x;
		float * pright = W + threadIdx.y * col + idx;

		for (int i = 0; i < row; i += 32)
		{
			tileA[threadIdx.y][threadIdx.x] = 0;
			if (i + threadIdx.x < row) tileA[threadIdx.y][threadIdx.x] = pleft[i];

			tileB[threadIdx.x][threadIdx.y] = 0;
			if (i + threadIdx.y < row) tileB[threadIdx.x][threadIdx.y] = pright[i * col]; // W[(i + threadIdx.y) * col + idx];

			//__threadfence_block();
			__syncthreads();
			for (int j = 0; j < 32; j++) tmp += tileA[threadIdx.y][j] * tileB[threadIdx.x][j];
			__syncthreads();
		}

		int yIdx = (seqBeg + tidx_y) * col + idx;
		Y[yIdx] = Y[yIdx] * weight + tmp;
	}
}

void Cuda_SeqTransMatrixProjectionByAx(float *Y, float* W, float *X, int *InstanceIndex, int row, int col, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float weight)
{
	dim3 thread_tail(32, 32);
	dim3 block_tail((col + 32 - 1) / 32, (Sum + 32 - 1) / 32);
	cuda_SeqTransMatrixProjection << <block_tail, thread_tail, 32 * 32 * 4 * 2 >> > (Y, W, X, InstanceIndex, row, col, dev_mask, maskIdx, Sum, tidx_y, tidx_x, batchSize, weight);
}


void Cuda_LSTMForwardPropagateBatchV2(int * InstanceIndex, int batchSize,  //->x;
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell,
		float * Ui,
		float * Uc,
		float * Uf,
		float * Uo, float * Vo,
		int * RecursiveIdx, int * RecursiveMatrix, int MaxLag)

{
	for (int i = 0; i < MaxLag; i++)
	{
		int startIdx = i == 0 ? 0 : RecursiveIdx[i - 1];
		int Sum = RecursiveIdx[i] - startIdx;
		if (Sum == 0) break;

		if (i > 0)
		{
			/// obtain output(t-1)
			//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			//	(InstanceIndex, batchSize, o, cell, i - 1, dev_mask, Sum, inputLinkMatrix);
			//Cuda_LSTMForwardPropagateP1(InstanceIndex, batchSize,
			//		o, gate_i, gate_f, gate_o, c_hat, cell,
			//		Ui, Uf, Uo, Uc,
			//		dev_mask, i, Sum,
			//		inputLinkMatrix, outputLinkMatrix);

			//gate_i += U_{i} * o_{t-1}
			Cuda_SeqTransMatrixProjectionByAx(gate_i, Ui, o, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i, (i - 1), batchSize, 1);
			//gate_f += U_{f} * o_{t-1}
			Cuda_SeqTransMatrixProjectionByAx(gate_f, Uf, o, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i, (i - 1), batchSize, 1);
			//c_hat += U_{c} * o_{t-1}
			Cuda_SeqTransMatrixProjectionByAx(c_hat, Uc, o, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i, (i - 1), batchSize, 1);
			//gate_o += U_{o} * o_{t-1}
			Cuda_SeqTransMatrixProjectionByAx(gate_o, Uo, o, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i, (i - 1), batchSize, 1);
		}

		/// gate_i = logistic(gate_i);
		/// output = gate_i(t)
		//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		//	(InstanceIndex, batchSize, gate_i, cell, i, dev_mask, Sum, outputLinkMatrix);
		//Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		//gate_i = sigmoid(gate_i)
		Cuda_Logistic(gate_i, gate_i, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);

		/// c_hat = tanh(c_hat);
		/// input = c_hat(t)
		//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		//	(InstanceIndex, batchSize, c_hat, cell, i, dev_mask, Sum, inputLinkMatrix);
		//Cuda_Tanh(inputLinkMatrix, inputLinkMatrix, Sum, cell);

		//c_hat = tanh(c_hat)
		Cuda_Tanh(c_hat, c_hat, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize);

		/// c = gate_i * c_hat
		/// target = c(t)
		//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		//	(InstanceIndex, batchSize, c, cell, i, dev_mask, Sum, targetLinkMatrix);
		//Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 0);

		//c = gate_i @ c_hat
		Cuda_ElementWiseProduct(c, c_hat, gate_i, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, i, batchSize, 0);

		/// output = gate_f(t)
		/// gate_f = logistic(gate_f);
		//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		//	(InstanceIndex, batchSize, gate_f, cell, i, dev_mask, Sum, outputLinkMatrix);
		//Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		//gate_f = sigmoid(gate_f)
		Cuda_Logistic(gate_f, gate_f, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);

		if (i > 0)
		{
			/// c(t) = c(t) + gate_f(t) * c(t-1)
			/// input = c(t-1)
			//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
			//	(InstanceIndex, batchSize, c, cell, i - 1, dev_mask, Sum, inputLinkMatrix);
			//Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 1);

			/// c(t) = c(t) + gate_f(t) * c(t-1)
			Cuda_ElementWiseProduct(c, gate_f, c, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, i - 1, batchSize, 1);
		}

		/// gate_o(t) = Vo * c(t)
		/// output = gate_o(t)
		//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		//	(InstanceIndex, batchSize, gate_o, cell, i, dev_mask, Sum, outputLinkMatrix);
		//Cuda_MatrixMultiplication(targetLinkMatrix, Vo, outputLinkMatrix, Sum, cell, cell, 1);

		/// gate_o = logistic(gate_o)
		//Cuda_Logistic(outputLinkMatrix, outputLinkMatrix, Sum, cell, 1);

		//gate_o += Vo * c(t)
		Cuda_SeqTransMatrixProjectionByAx(gate_o, Vo, c, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);

		//gate_o = simgoid(gate_o)
		Cuda_Logistic(gate_o, gate_o, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);


		/// obtain tanhc(t)
		/// input = tanhc
		//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		//	(InstanceIndex, batchSize, tanhc, cell, i, dev_mask, Sum, inputLinkMatrix);
		/// tanhc = tanh(c)
		//Cuda_Tanh(targetLinkMatrix, inputLinkMatrix, Sum, cell);

		//tanhc = tanh(c)
		Cuda_Tanh(tanhc, c, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize);


		/// obtain o(t)
		/// target = o(t)
		//cuda_CalculateLinkMatrix << < (Sum - 1) / DEFAULT_THREAD_PER_BLOCK + 1, DEFAULT_THREAD_PER_BLOCK >> >
		//	(InstanceIndex, batchSize, o, cell, i, dev_mask, Sum, targetLinkMatrix);
		/// o = gate_o * tanhc
		//Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, Sum, cell, 0);

		////o = gate_o @ tanhc
		Cuda_ElementWiseProduct(o, tanhc, gate_o, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, i, batchSize, 0);
		//i = i + 1;
	}
}


__global__ void cuda_DerivLogistic(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float gamma)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < Sum)
	{
		int smpIdx = dev_mask[idy + maskIdx] - 1;
		int seqBeg = smpIdx == 0 ? 0 : InstanceIdx[smpIdx - 1];

		int yIdx = (seqBeg + tidx_y) * dim + idx;
		int xIdx = (seqBeg + tidx_x) * dim + idx;
		Y[yIdx] = Y[yIdx] * X[xIdx] * (1 - X[xIdx]) * gamma;
	}
}

void Cuda_DerivLogistic(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float gamma)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((dim - 1) / DEFAULT_THREAD_PER_DIM + 1, (Sum - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivLogistic << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, dev_mask, maskIdx, Sum, tidx_y, tidx_x, batchSize, gamma);
}


__global__ void cuda_SeqMatrixProjection(float *Y, float* W, float *X, int *InstanceIndex, int row, int col, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;

	__shared__ float tileA[32][32], tileB[32][32];

	tileA[threadIdx.y][threadIdx.x] = 0;
	tileB[threadIdx.x][threadIdx.y] = 0;
	if (idx < row && idy < Sum)
	{
		int smpIdx = dev_mask[idy + maskIdx] - 1;
		int seqBeg = smpIdx == 0 ? 0 : InstanceIndex[smpIdx - 1];
		float * pleft = X + (seqBeg + tidx_x)  * col; // y*rightColumnCnt;
		float * pright = W + idx * col;
		float tmp = 0;
		for (int i = 0; i < col; i += 32)
		{
			tileA[threadIdx.y][threadIdx.x] = 0;
			if (i + threadIdx.x < col) tileA[threadIdx.y][threadIdx.x] = pleft[i + threadIdx.x];

			tileB[threadIdx.x][threadIdx.y] = 0;
			if (i + threadIdx.y < col) tileB[threadIdx.x][threadIdx.y] = pright[i + threadIdx.y];

			__threadfence_block();
			__syncthreads();

			for (int j = 0; j < 32; j++) tmp += tileA[threadIdx.y][j] * tileB[threadIdx.x][j];
			__syncthreads();
		}
		int yIdx = (seqBeg + tidx_y)  * row + idx;
		Y[yIdx] = Y[yIdx] * weight + tmp;

	}
}

void Cuda_SeqMatrixProjectionByAx(float *Y, float* W, float *X, int *InstanceIndex, int row, int col, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float weight)
{
	dim3 thread_tail(32, 32);
	dim3 block_tail((row + 32 - 1) / 32, (Sum + 32 - 1) / 32);
	cuda_SeqMatrixProjection << <block_tail, thread_tail, 32 * 32 * 4 * 2 >> > (Y, W, X, InstanceIndex, row, col, dev_mask, maskIdx, Sum, tidx_y, tidx_x, batchSize, weight);
}


__global__ void cuda_DerivTanh(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < Sum)
	{
		int smpIdx = dev_mask[idy + maskIdx] - 1;
		int seqBeg = smpIdx == 0 ? 0 : InstanceIdx[smpIdx - 1];

		int yIdx = (seqBeg + tidx_y) * dim + idx;
		int xIdx = (seqBeg + tidx_x) * dim + idx;
		Y[yIdx] = Y[yIdx] * (1 - X[xIdx] * X[xIdx]);
	}
}

void Cuda_DerivTanh(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((dim - 1) / DEFAULT_THREAD_PER_DIM + 1, (Sum - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_DerivTanh << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, dev_mask, maskIdx, Sum, tidx_y, tidx_x, batchSize);
}

__global__ void cuda_ElementWiseAdd(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float weight)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	int idy = blockDim.y * blockIdx.y + threadIdx.y;
	if (idx < dim && idy < Sum)
	{
		int smpIdx = dev_mask[idy + maskIdx] - 1;
		int seqBeg = smpIdx == 0 ? 0 : InstanceIdx[smpIdx - 1];
		int yIdx = (seqBeg + tidx_y) * dim + idx;
		int xIdx = (seqBeg + tidx_x) * dim + idx;
		Y[yIdx] = Y[yIdx] * weight + X[xIdx];
	}
}

// y = y * weight + x
void Cuda_ElementWiseAdd(float *Y, float *X, int *InstanceIdx, int dim, int * dev_mask, int maskIdx, int Sum, int tidx_y, int tidx_x, int batchSize, float weight)
{
	dim3 nThreadPerBlock(DEFAULT_THREAD_PER_DIM, DEFAULT_THREAD_PER_DIM);
	dim3 nBlockPerGrid((dim - 1) / DEFAULT_THREAD_PER_DIM + 1, (Sum - 1) / DEFAULT_THREAD_PER_DIM + 1);
	cuda_ElementWiseAdd << <nBlockPerGrid, nThreadPerBlock >> >(Y, X, InstanceIdx, dim, dev_mask, maskIdx, Sum, tidx_y, tidx_x, batchSize, weight);
}

void Cuda_LSTMBackwardPropagateBatchV2(int * InstanceIndex, int batchSize, int seqSize, int cell,
		float * o,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,
		float * tanhc,

		float * deriv_o,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * deriv_tanhc,

		float * Ui, float * Uc, float * Uf, float * Uo, float * Vo,
		int * RecursiveIdx, int * RecursiveMatrix, int MaxLag)
{

	for (int i = MaxLag - 1; i >= 0; i--)
	{

		int startIdx = i == 0 ? 0 : RecursiveIdx[i - 1];
		int Sum = RecursiveIdx[i] - startIdx;
		if (Sum == 0) break;
		//int * dev_mask = RecursiveMatrix + startIdx;

		/// input <--- deriv_o(t);
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	deriv_o, cell, i, dev_mask, bSum, inputLinkMatrix);

		/// output <--- tanhc(t);
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	tanhc, cell, i, dev_mask, bSum, outputLinkMatrix);

		/// target <--- deriv_gate_o(t);
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	deriv_gate_o, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_gate_o = deriv_o * tanhc;
		//Cuda_ElementwiseProduct(inputLinkMatrix, outputLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		// deriv_gate_o = deriv_o @ tanhc
		Cuda_ElementWiseProduct(deriv_gate_o, tanhc, deriv_o, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, i, batchSize, 0);

		/// mid <---- gate_o(t);
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	gate_o, cell, i, dev_mask, bSum, midLinkMatrix);
		/// deriv_gate_o = deriv_gate_o * gate_o * ( 1- gate_o);
		//Cuda_DerivLogistic(midLinkMatrix, targetLinkMatrix, bSum, cell, 1);

		// deriv_gate_o = deriv_gate_o *  gate_o * (1 - gate_o)
		Cuda_DerivLogistic(deriv_gate_o, gate_o, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);


		/// left <---- deriv_c(t).
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	deriv_c, cell, i, dev_mask, bSum, leftLinkMatrix);

		/// deriv_c(t) += deriv_gate_o(t) * Vo;
		//Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Vo, leftLinkMatrix, bSum, cell, cell, 1);

		// deriv_c += deriv_gate_o * Vo
		Cuda_SeqMatrixProjectionByAx(deriv_c, Vo, deriv_gate_o, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);


		/// deriv_o(t-1) += deriv_gate_o(t) * Uo;
		if (i > 0)
		{
			/// reserve <---- deriv_o(t-1)
			//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
			//	deriv_o, cell, i - 1, dev_mask, bSum, reserveLinkMatrix);

			/// deriv_o(t-1) += deriv_gate_o(t) * Uo;
			//Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Uo, reserveLinkMatrix, bSum, cell, cell, 1);

			// deriv_o(t-1) += deriv_gate_o(t) * Uo
			Cuda_SeqMatrixProjectionByAx(deriv_o, Uo, deriv_gate_o, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i - 1, i, batchSize, 1);
		}

		/// target <---- deiv_tanhc(t)
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	deiv_tanhc, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_tanhc = deriv_o * gate_o;
		//Cuda_ElementwiseProduct(inputLinkMatrix, midLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		//deriv_tanhc = deriv_o @ gate_o
		Cuda_ElementWiseProduct(deriv_tanhc, deriv_o, gate_o, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, i, batchSize, 0);


		/// deriv_tanhc = deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
		//Cuda_DerivTanh(outputLinkMatrix, targetLinkMatrix, bSum, cell);

		// deriv_tanhc = deriv_tanhc * (1 + tanhc) * ( 1- tanhc);
		Cuda_DerivTanh(deriv_tanhc, tanhc, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize);


		/// deriv_c (t) += deriv_tanhc(t);
		//Cuda_Matrix_Add(targetLinkMatrix, leftLinkMatrix, leftLinkMatrix, bSum, cell, 1, 1, 0);
		//deriv_c += deriv_tanhc
		Cuda_ElementWiseAdd(deriv_c, deriv_tanhc, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);

		// from deriv_c -> deriv_gate_i, deriv_chat, deriv_gate_f, deriv_c(t-1)

		// input <---- gate_i
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	gate_i, cell, i, dev_mask, bSum, inputLinkMatrix);

		// output <--- deriv_cHat
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	deriv_cHat, cell, i, dev_mask, bSum, outputLinkMatrix);

		/// deriv_cHat = deriv_c * gate_i;
		//Cuda_ElementwiseProduct(leftLinkMatrix, inputLinkMatrix, outputLinkMatrix, bSum, cell, 0);
		// deriv_c_hat = deriv_c @ gate_i
		Cuda_ElementWiseProduct(deriv_cHat, deriv_c, gate_i, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, i, batchSize, 0);

		// right <----- cHat
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	c_hat, cell, i, dev_mask, bSum, rightLinkMatrix);

		// target <----- deriv_gate_i
		//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
		//	deriv_gate_i, cell, i, dev_mask, bSum, targetLinkMatrix);

		/// deriv_gate_i = deriv_c * cHat;
		//Cuda_ElementwiseProduct(leftLinkMatrix, rightLinkMatrix, targetLinkMatrix, bSum, cell, 0);

		//deriv_gate_i = deriv_c @ c_hat
		Cuda_ElementWiseProduct(deriv_gate_i, deriv_c, c_hat, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, i, batchSize, 0);

		/// deriv_gate_i = deriv_gate_i * gate_i * (1-gate_i)
		//Cuda_DerivLogistic(inputLinkMatrix, targetLinkMatrix, bSum, cell, 1);
		//deriv_gate_i = deriv_gate_i * gate_i * (1 - gate_i)
		Cuda_DerivLogistic(deriv_gate_i, gate_i, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);


		/// deriv_cHat = deriv_cHat * (1 + cHat) * (1-cHat)
		//Cuda_DerivTanh(rightLinkMatrix, outputLinkMatrix, bSum, cell);
		// deriv_c_hat = deriv_c_hat * (1 + c_hat)*( 1- c_hat)
		Cuda_DerivTanh(deriv_cHat, c_hat, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize);

		if (i > 0)
		{
			/// deriv_o(t-1) += deriv_gate_i(t) * Ui;
			//Cuda_MatrixMultiplicationTranspose(targetLinkMatrix, Ui, reserveLinkMatrix, bSum, cell, cell, 1);

			//deriv_o(t-1) += deriv_gate_i * Ui
			Cuda_SeqMatrixProjectionByAx(deriv_o, Ui, deriv_gate_i, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i - 1, i, batchSize, 1);


			// deriv_o(t-1) += deriv_cHat(t) * Uc;
			//Cuda_MatrixMultiplicationTranspose(outputLinkMatrix, Uc, reserveLinkMatrix, bSum, cell, cell, 1);

			//deriv_o(t-1) += deriv_cHat(t) * Uc
			Cuda_SeqMatrixProjectionByAx(deriv_o, Uc, deriv_cHat, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i - 1, i, batchSize, 1);


			// input <----- deriv_c(t-1)
			//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
			//	deriv_c, cell, i - 1, dev_mask, bSum, inputLinkMatrix);

			// output <------- gate_f(t)
			//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
			//	gate_f, cell, i, dev_mask, bSum, outputLinkMatrix);

			/// deriv_c(t-1) += deriv_c * gate_f;
			//Cuda_ElementwiseProduct(leftLinkMatrix, outputLinkMatrix, inputLinkMatrix, bSum, cell, 1);

			//deriv_c(t-1) += deriv_c @ gate_f
			Cuda_ElementWiseProduct(deriv_c, deriv_c, gate_f, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i - 1, i, i, batchSize, 1);

			// target <------- c(t-1)
			//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
			//	c, cell, i - 1, dev_mask, bSum, targetLinkMatrix);

			// right <-------- deriv_gate_f;
			//cuda_CalculateLinkMatrix << <(bSum - 1) / 32 + 1, 32 >> >(InstanceIndex, batchSize,
			//	deriv_gate_f, cell, i, dev_mask, bSum, rightLinkMatrix);

			// deriv_gate_f = deriv_c * c(t-1);
			//Cuda_ElementwiseProduct(leftLinkMatrix, targetLinkMatrix, rightLinkMatrix, bSum, cell, 0);
			//deriv_gate_f = deriv_c @ c(t-1)
			Cuda_ElementWiseProduct(deriv_gate_f, deriv_c, c, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, i - 1, batchSize, 0);

			// deriv_gate_f = deriv_gate_f * gate_f * ( 1 - gate_f)
			//Cuda_DerivLogistic(outputLinkMatrix, rightLinkMatrix, bSum, cell, 1);
			//deriv_gate_f = deriv_gate_f * (1 -gate_f) * gate_f
			Cuda_DerivLogistic(deriv_gate_f, gate_f, InstanceIndex, cell, RecursiveMatrix, startIdx, Sum, i, i, batchSize, 1);


			/// deriv_o(t-1) += deriv_gate_f(t) * Uf;
			//Cuda_MatrixMultiplicationTranspose(rightLinkMatrix, Uf, reserveLinkMatrix, bSum, cell, cell, 1);
			//deriv_o(t -1) += deriv_gate_f * Uf
			Cuda_SeqMatrixProjectionByAx(deriv_o, Uf, deriv_gate_f, InstanceIndex, cell, cell, RecursiveMatrix, startIdx, Sum, i - 1, i, batchSize, 1);

		}
	}
}

