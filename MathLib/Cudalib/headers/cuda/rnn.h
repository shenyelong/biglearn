#pragma once

#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
#endif

/// InstanceIndex : Accumulate Sequence Length of Input Seqes. i.e., [len of seq1, len of seq1 + len of seq2, ...]
/// batchSize : number of sequences.
void Cuda_RecursiveForwardPropagateBatch(int * InstanceIndex, int batchSize,
						float * SeqInputMatrixs, int dim,
						float * WMatrix, int lag, int af, float * SeqOutputMatrix);


/// InstanceIndex : Accumulate Sequence Length of Input Seqes. i.e., [len of seq1, len of seq1 + len of seq2, ...]
/// batchSize : number of sequences.
void Cuda_RecursiveBackwardPropagateBatch(int * InstanceIndex, int batchSize, 
									float * SeqOutputMatrixs, int dim, 
									float * WMatrix, int lag, int af, float * SeqDerivMatrixs);

/// InstanceIndex : Accumulate Sequence Length of Input Seqes. i.e., [len of seq1, len of seq1 + len of seq2, ...]
/// batchSize : number of sequences. 
void Cuda_RecursiveUpdateBatch(int * InstanceIndex, int batchSize, int seqSize, float * SeqOutputMatrixs, int dim, float * SeqDerivMatrixs, int lag, float * gradient, float weight);


/// Take preSection's last state for calculating the update.
void Cuda_RecursiveExtendUpdateBatch(int * InstanceIndex, int batchSize, int seqSize, float * SeqOutputMatrixs,
	int * pre_Idx, //-> previous output,
	int pre_batchSize,  //->x;
	float * pre_O,
	int dim, float * SeqDerivMatrixs, int lag, float * gradient, float weight);

void Cuda_LSTMLinkExtendForwardPropagateBatch(
	float * pre_o, //-> previous output,
	float * pre_c, //-> previous c,
	int * InstanceIndex, int batchSize,  //->x;
	int * pre_idx,
	float * o, //-> output,
	float * gate_i, //->gate i,
	float * c_hat, //->candidate memory,
	float * gate_f, //->forget gate,
	float * c, //->memory,
	float * gate_o, //->gate o,
	float * tanhc, //->tanh memory
	int cell,
	float * Ui,
	float * Uc,
	float * Uf,
	float * Uo, float * Vo);

/// srcInstanceIdx, srcBatchSize, pre_o, pre_c -> lstm status of prevous sequence. 
void Cuda_LSTMExtendForwardPropagateBatch( int * srcInstanceIdx, int srcBatchSize,  //->src_x;
	float * pre_o, //-> previous output,
	float * pre_c, //-> previous c,
	int * InstanceIndex, int batchSize,  //->x;
	float * o, //-> output,
	float * gate_i, //->gate i,
	float * c_hat, //->candidate memory,
	float * gate_f, //->forget gate,
	float * c, //->memory,
	float * gate_o, //->gate o,
	float * tanhc, //->tanh memory
	int cell, float * Ui, float * Uc, float * Uf, float * Uo, float * Vo);

/// InstanceIndex : Accumulate Sequence Length of Input Seqes. i.e., [len of seq1, len of seq1 + len of seq2, ...]
/// batchSize : number of sequences. 
void Cuda_LSTMForwardPropagateBatch(int * InstanceIndex, int batchSize,  //->x;
	float * o, //-> output,
	float * gate_i, //->gate i,
	float * c_hat, //->candidate memory,
	float * gate_f, //->forget gate,
	float * c, //->memory,
	float * gate_o, //->gate o,
	float * tanhc, //->tanh memory
	int cell, float * Ui, float * Uc, float * Uf, float * Uo, float * Vo);


void Cuda_LSTMExtendBackwardPropagateBatch(
	int * preInstanceIdx, int preBatchSize,  //->src_x;
	float * pre_o, //-> previous output,
	float * pre_c, //-> previous c,
	float * deriv_pre_o,	// -> previous deriv_o
	float * deriv_pre_c,	// -> previous deriv_c
	int * InstanceIndex, int batchSize, int seqSize, int cell,
	float * o,
	float * gate_i,
	float * c_hat,
	float * gate_f,
	float * c,
	float * gate_o,
	float * tanhc,
	float * deriv_o,
	float * deriv_gate_i,
	float * deriv_cHat,
	float * deriv_gate_f,
	float * deriv_c,
	float * deriv_gate_o,
	float * deiv_tanhc,
	float * Ui, float * Uc, float * Uf, float * Uo, float * Vo);

void Cuda_LSTMBackwardPropagateBatch(int * InstanceIndex, int batchSize, int seqSize, int cell,
	float * o,
	float * gate_i,
	float * c_hat,
	float * gate_f,
	float * c,
	float * gate_o,
	float * tanhc,

	float * deriv_o,
	float * deriv_gate_i,
	float * deriv_cHat,
	float * deriv_gate_f,
	float * deriv_c,
	float * deriv_gate_o,
	float * deiv_tanhc,

	float * Ui, float * Uc, float * Uf, float * Uo, float * Vo);


void Cuda_HierarcialSoftmaxPairEmbedding(
	float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv,
	float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv,
	int dim,
	float * vSpace, float * vBias, int vocabSize,
	float * cSpace, float * cBias, int cSize,
	float step, // step size for update vSpace.
	int * v2c, // word 2 class,
	int * classIdx, // class 2 word number
	int * wordIdx, // class 2 word index
	int * wordClassIdx, //word 2 class segment index.
	float gamma,
	float * classAct,
	float * wordAct,
	int * wordActIdx1,
	int * wordActIdx2,
	int wordSegSize, // word Max SegmentSize,
	float * targetProb
	);

void Cuda_FullySoftmaxPairEmbedding(
	float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv,
	float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv,
	int dim,
	float * vSpace, float * vBias, int vocabSize,
	float step, // step size for update vSpace.
	float gamma,
	float * wordAct,
	float * targetProb);

void Cuda_HierarcialSoftmaxPairProbability(
	float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv,
	float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv,
	int dim,
	float * vSpace, float * vBias, int vocabSize,
	float * cSpace, float * cBias, int cSize,
	int * v2c, // word 2 class,
	int * classIdx, // class 2 word number
	int * wordIdx, // class 2 word index
	int * wordClassIdx, //word 2 class segment index.
	float gamma,
	float * classAct,
	float * wordAct,
	int * wordActIdx1,
	int * wordActIdx2,
	int wordSegSize, // word Max SegmentSize,
	float * targetProb);

void Cuda_FullySoftmaxPairProbability(
	float * srcLabels, int srcLabelCount, float * srcEmbedding,
	float * tgtLabels, int tgtLabelCount, float * tgtEmbedding,
	int dim,
	float * vSpace, float * vBias, int vocabSize,
	float step, // step size for update vSpace.
	float gamma,
	float * wordAct,
	float * targetProb);

void Cuda_HierarcicalSoftmaxProbability(float * targets, int targetNum,
	float * embedding, int dim,
	float * vSpace, float * vBias, int vocabSize,
	float * cSpace, float * cBias, int cSize,

	int * v2c, // word 2 class,
	int * classIdx, // class 2 word number
	int * wordIdx, // class 2 word index
	int * wordClassIdx, //word 2 class segment index.
	float gamma,
	float * classOutput,
	float * wordOutput,
	int * wordActIdx1,
	int * wordActIdx2,
	int wordSegSize, // word Max SegmentSize,
	float * targetProb);

void Cuda_HierarcialSoftmaxEmbeddingV2(float * targets, int targetNum,
	float * embedding, float * deriv, int dim,
	float * vSpace, float * vBias, int vocabSize,
	float * cSpace, float * cBias, int cSize,
	float step,
	int * v2c, // word 2 class,
	int * classIdx, // class 2 word number
	int * wordIdx, // class 2 word index
	int * wordClassIdx, //word 2 class segment index.
	float gamma,
	float * classOutput,
	float * wordOutput,
	int * wordActIdx1,
	int * wordActIdx2,
	int wordSegSize, // word Max SegmentSize,
	float * targetProb);

void Cuda_FullySoftmaxDecoding(float * embedding, int batchSize, int dim,
	float * vSpace, float * vBias, int vSize,
	int bestN, float gamma, float * tmpWordOutput,
	float * wordSet, float * wordProb);

void Cuda_HierarcialSoftmaxDecoding(float * embedding, int batchSize, int dim,
	float * vSpace, float * vBias, int vSize,
	float * cSpace, float * cBias, int cSize,
	int bestN, float gamma,
	int * v2c,
	int * classIdx, // class 2 word number
	int * wordIdx, // class 2 word index
	float * tmpClassOutput, float * tmpWordOutput,
	float * wordSet, float * wordProb);

/************************/
void cuda_RNNForwardPropagate(int * smpIdx, int batchSize, int seqSize, float * Wmatrix, int lag, int hiddenDim, int af, float * seqOut);


void cuda_RNNForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix,
	float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut);

void cuda_RNNBackwardPropagate(int * smpIdx, int batchSize, float * derivOutput, float * output, int seqSize, float * wMatrix, int lag, int hiddenDim, int af);

void cuda_RecurrentUpdate(int * smpIdx, int batchSize, float * output, float * derivOutput, int hiddenDim, int seqSize, int lag, float * gradient, float weight);

void cuda_RNNForwardPropagate_Dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix,
	float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut);



void cuda_RNNForwardPropagateSlot_Dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix,
	float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut,
	float * groundOut, int * sampleEnd);

void cuda_RNNForwardPropagateSlot(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix,
	float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut,
	float * groundTruth, int * earlyStopIdx);



void cuda_RNNBackwardPropagateSlot(int * smpIdx, int batchSize, float * derivOutput, float * output, int seqSize, float * wMatrix,
	int hiddenDim, int af, int * earlyStop);


void cuda_RecurrentUpdateSlot(int * smpIdx, int batchSize, int * earlyStop, float * output, float * derivOutput, int hiddenDim,
	int seqSize, int lag, float * gradient, float weight);

void cuda_RNNLabelOutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOut);

void cuda_DeRNNLabelOutput(int * smpIdx, int batchSize, float * seqInputDeriv, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv);

void cuda_UpdateRNNLabelOutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv, float wei);

void Cuda_RandomNumber(float * value, int size);

void Cuda_UpdateSoftmaxEmbedding(float * targets, int targetNum, 
			float * embedding, float * deriv, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float step,	// step size for update vSpace.
			float * simi, // similarity for negatives.
			float * alpha,
			int negativeSample, float gamma, 
			int * negIds,
			float * randVar, int randLen, int randStart);

void Cuda_UpdateEmbedding(float * targets, int targetNum, 
			float * embedding, float * deriv, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float step,	// step size for update vSpace.
			float * simi, // similarity for negatives.
			float * alpha,
			int negativeSample, float gamma, 
			int * negIds,
			float * randVar, int randLen, int randStart);


void Cuda_UpdateBalanceEmbedding(float * targets, int targetNum, 
			float * embedding, float * deriv, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float step,	// step size for update vSpace.
			float * simi, // similarity for negatives.
			float * alpha,
			int negativeSample, float gamma, 
			int * negIds,
			float * randVar, int randLen, int randStart,
			int * sampleTable, int tableSize);


void Cuda_UpdateBinaryEmbedding_NegIds(float * targets, int targetNum, 
			float * embedding, float * deriv, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float step,	// step size for update vSpace.
			float * simi, // similarity for negatives.
			float * alpha,
			int negativeSample, float gamma, 
			int * negIds);


void Cuda_UpdateSoftmaxEmbedding_NegIds(float * targets, int targetNum, 
			float * embedding, float * deriv, int dim,
			float * vSpace, float * vBias, int vocabSize,
			float step,	// step size for update vSpace.
			float * simi, // similarity for negatives.
			float * alpha,
			int negativeSample, float gamma, 
			int * negIds);

/// InstanceIndex : Accumulate Sequence Length of Input Seqes. i.e., [len of seq1, len of seq1 + len of seq2, ...]
/// batchSize : number of sequences. 
void Cuda_LSTMForwardPropagateBatchOpt(int * InstanceIndex, int batchSize,  //->x;
	float * o, //-> output,
	float * gate_i, //->gate i,
	float * c_hat, //->candidate memory,
	float * gate_f, //->forget gate,
	float * c, //->memory,
	float * gate_o, //->gate o,
	float * tanhc, //->tanh memory
	int cell, float * Ui, float * Uc, float * Uf, float * Uo, float * Vo);

void Cuda_LSTMBackwardPropagateBatchOpt(int *InstanceIndex, int batchSize, int seqSize, int cell,
	float *o,
	float *gate_i,
	float *c_hat,
	float *gate_f,
	float *c,
	float *gate_o,
	float *tanhc,

	float *deriv_o,
	float *deriv_gate_i,
	float *deriv_cHat,
	float *deriv_gate_f,
	float *deriv_c,
	float *deriv_gate_o,
	float *deriv_tanhc,
	float *Ui, float *Uc, float *Uf, float *Uo, float *Vo);

void Cuda_GRUForwardPropagateBatch(int * InstanceIndex, int batchSize, int Dim, //->x;
	float * H, //->hidden,
	float * GateR, //->reset gate,
	float * GateZ, //->update gate,
	float * HHat, //->chat
	float * RestH,
	float * Ur,
	float * Uz,
	float * Uh);

void Cuda_GRUBackwardPropagateBatch(
	int *InstanceIndex, int batchSize, int Dim,//->x
	float *H, //->hidden
	float *GateR, //->reset gate,
	float *GateZ, //->update gate,
	float *HHat, //->hat
	float *RestH, //H @ R
	float *DerivH,
	float *DerivGateR,
	float *DerivGateZ,
	float *DerivHHat,
	float *DerivRestH,
	float * Ur,
	float * Uz,
	float * Uh);

float Cuda_L2Norm(float *x, int size);
void Cuda_RMSPropGradient(float * ada, float * grad, float gamma, float epsilon, int m);
void Cuda_RMSPropV2_Gradient(float * ada, float * g, float * grad, float gamma, float epsilon, int m);

void Cuda_VectorSquareAdd(float *Z, float *X, float *Y, float wz, float wx, float wy, int m);
void Cuda_AdaMGradient(float *M, float *V, float *G, float alpha, float beta1, float beta2, float epsilon, int t, int size);
void Cuda_AdaDeltaGradient(float *deltaX, float *AccumGrad, float *AccumUpdate, float *Grad, float epsilon, int size);
void Cuda_AdaMax(float *V, float *M, float *G, float alpha, float beta, float epsilon, int m);

void Cuda_MatrixL1Norm(float *x, int dim, int batchSize, float * l1Score);
void Cuda_MatrixL2Norm(float *x, int dim, int batchSize, float * l2Score);
void Cuda_DerivMatrixL1Norm(float *x, int dim, int batchSize, float * l1Score, float * l1ScoreDeriv, float * xDeriv);
void Cuda_DerivMatrixL2Norm(float *x, int dim, int batchSize, float * l2Score, float * l2ScoreDeriv, float * xDeriv);


