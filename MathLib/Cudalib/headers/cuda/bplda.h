#pragma once

#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
#endif

/*********************** BP LDA *************/
void cuda_Matrix_Normalize_By_Col(float *a, uint32_t col, uint32_t row);


void cuda_Bp_Lda_Forward(
	float *phi,
	float *theta,
	float *b,
	float *U,
	float *T,
	float *d_TLayerValue,
	float *d_MaxColValue,
	float *d_SumColValue,
	float *iterT,
	float * output,
	float *matchLabelValue,
	float *d_Loss_Per_Epoch,
	uint32_t * matchIdx,
	uint32_t * featureIdx,
	float * featureValue,
	uint32_t elementSize,
	float eta,
	uint32_t useAdaptivenHidLayer,
	uint32_t batchsize,
	uint32_t inputDim,
	uint32_t hiddenDim,
	uint32_t outputDim,
	uint32_t layerDim,
	float *d_Phitheta,
	float *d_tempSparseMat,
	float *d_tempSparseMatTemp1,
	float *d_tempDenseMatTemp,
	float *d_NegGradTemp,
	float *d_LogThetaTemp,
	float *d_loss_pre,
	float *d_loss_post,
	float *d_sum,
	float *d_sumGradProj,
	uint32_t *d_flagWhileBreak, // batchsize
	uint32_t *d_whileBreakCount, // 1
	uint32_t *whileBreakCount, //1
	float gamma,
	uint32_t calculateSoftMax,
	uint32_t MaxLineSearchIter
	);

void cuda_Bp_Lda_Backward(
	float *output,
	float *matchLabelValue,
	float *theta,
	float *phi,
	float *b,
	float *U,
	float *T,
	float *iterT,
	float *grad_Q_U,
	float *grad_Q_Phi,
	uint32_t *matchIdx,
	uint32_t *featureIdx,
	float *featureValue,
	float gamma,
	float negativeBetaMinusOneOverTotalSamples,
	uint32_t batchsize,
	uint32_t inputDim,
	uint32_t hiddenDim,
	uint32_t outputDim,
	uint32_t layerDim,
	uint32_t elementSize,
	float *d_xi,
	float *d_tempDenseMat,
	float *d_temp_theta_xi,
	float *d_temp_theta_xi_b_T_OVER_theta_lm1_2,
	float *d_thetaRatio,
	float *d_sumGrad_Q_U, //outputdim * hiddenDim * batchsize
	float *d_temp_Xt_OVER_Phitheta, // elementSize
	float *d_tempSparseMatValue, // elementSize
	float *d_sumTempDenseMat, // batchsize * hiddendim
	float *d_tempSparseMat, // elementSize
	float *d_tempResBatch, // batchsize
	uint32_t calculateSoftMax
	);

void cuda_Bp_Lda_Update(
	float *grad_Q_U,
	float *grad_Q_Phi,
	float *U,
	float *phi,
	float *adaGradSum,
	float *d_sumAdaGradSum,
	float *d_sumTempColSum,
	float *muPhiSearch,
	float mu_U,
	float mu_Phi,
	uint32_t cntModelUpdate,
	uint32_t inputDim,
	uint32_t hiddenDim,
	uint32_t outputDim,
	float *d_update,
	float *d_tempColSum,
	float *d_tempColMax
	);
