#pragma once

#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
#endif

void Cuda_L1Reg_CheckSign(float *src, float *dst, int len, int keepZero, int reverse);

void Cuda_L1Reg_CalcPseudoGradient(float* weight, float *grad, float *pseudoGradient, float cl1, float cl2, int len);

void Cuda_L1Norm(float* weight, float* norm, int len);

void Cuda_CountZero(float* weight, int* zcount, int len);
