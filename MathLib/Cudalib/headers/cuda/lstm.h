#pragma once

#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
#endif

void Cuda_LSTMForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
							   float * h, //-> hiddenState,
							   float * gate_i, //->gate i,
							   float * c_hat, //->candidate memory,
							   float * gate_f, //->forget gate,
							   float * c, //->memory,
							   float * gate_o, //->output,
							   int cell,
							   float * Wi, float * Ui, float * bi,
							   float * Wc, float * Uc, float * bc,
							   float * Wf, float * Uf, float * bf,
							   float * Wo, float * Uo, float * Vo, float * bo);

void Cuda_LSTMForwardPropagateStep2(int * smpIdx, int batchSize, //int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
							   float * h, //-> hiddenState,
							   float * gate_i, //->gate i,
							   float * c_hat, //->candidate memory,
							   float * gate_f, //->forget gate,
							   float * c, //->memory,
							   float * output, //->output,
							   int cell,
							   float * Wi, float * Ui, float * bi,
							   float * Wc, float * Uc, float * bc,
							   float * Wf, float * Uf, float * bf,
							   float * Wo, float * Uo, float * Vo, float * bo);


void Cuda_LSTMForwardPropagateDense(int * smpIdx, int batchSize, float * seqInput, int seqSize, int inputDim,
									float * h,
									float * gate_i,
									float * c_hat,
									float * gate_f,
									float * c,
									float * output,
									int cell,
									float * Wi, float * Ui, float * bi,
									float * Wc, float * Uc, float * bc,
									float * Wf, float * Uf, float * bf,
									float * Wo, float * Uo, float * Vo, float * bo);

void Cuda_LSTMBackwardPropagate(int * smpIdx, int batchSize, int seqSize, int cell,
									float * h,
									float * gate_i,
									float * c_hat,
									float * gate_f,
									float * c,
									float * gate_o,

									float * deriv_h,
									float * deriv_gate_i,
									float * deriv_cHat,
									float * deriv_gate_f,
									float * deriv_c,
									float * deriv_gate_o,
									float * Wi, float * Ui, float * bi,
									float * Wc, float * Uc, float * bc,
									float * Wf, float * Uf, float * bf,
									float * Wo, float * Uo, float * Vo, float * bo);
