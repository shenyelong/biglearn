// dllapp.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>
#include <cstring>
#include <stdlib.h>
#include <cudnn.h>
#include <cusparse.h>
#include <curand.h>

//#include <cub/block/block_load.cuh>
//#include <cub/block/block_store.cuh>
//#include <cub/block/block_radix_sort.cuh>

#pragma comment(lib, "cudart") 
#pragma comment(lib, "cuda") 
#pragma comment(lib, "cudnn")
using namespace std;

//#ifdef WIN32
//#include <comutil.h>
//#include <windows.h>
//using namespace _com_util;
//#else
#include <cfloat>
#include <dlfcn.h>
//--->#include "mpi.h"
#include <nccl.h>
//#endif
/* variables for MPI aggregation */
//#ifdef WIN32
//#else
#define CUDNN_VERSION_MIN(major, minor, patch) \
   		(CUDNN_VERSION >= (major * 1000 + minor * 100 + patch))

#define CUDACHECK(cmd) do {                         \
  cudaError_t e = cmd;                              \
  if( e != cudaSuccess ) {                          \
    printf("Failed: Cuda error %s:%d '%s'\n",       \
        __FILE__,__LINE__,cudaGetErrorString(e));   \
    exit(EXIT_FAILURE);                             \
  }                                                 \
} while(0)

#define NCCLCHECK(cmd) do {                         \
  ncclResult_t r = cmd;                             \
  if (r!= ncclSuccess) {                            \
    printf("Failed, NCCL error %s:%d '%s'\n",       \
        __FILE__,__LINE__,ncclGetErrorString(r));   \
    exit(EXIT_FAILURE);                             \
  }                                                 \
} while(0)

static int rank = -1;
//--->static ncclUniqueId commId;
//--->static ncclComm_t comm;
static int numprocs = -1;
int r_len = 44362800;
float *d_result;
int *d_id,*d_cnt;
float *d_R_gather;
int *d_id_gather;
int *h_cnt_gather,*d_cnt_gather;
float ratio = 1.0;
//#endif

/**************Cuda Basic Information************************/

#ifdef WIN32
DLLEXP int __stdcall CudaDeviceCount()
#else
extern "C" int CudaDeviceCount()
#endif
{
	int devCount = 0;
	cudaGetDeviceCount(&devCount);
	return devCount;
}

#ifdef WIN32
DLLEXP void*  __stdcall CudaCtxGetCurrent()
#else
extern "C" void*  CudaCtxGetCurrent()
#endif
{
	CUcontext ctx = NULL;
	if (cuCtxGetCurrent(&ctx) != 0)
	{
		return NULL;
	}

	return ctx;
}

#ifdef WIN32
DLLEXP int  __stdcall CudaCtxSetCurrent(void * ctx)
#else
extern "C" int  CudaCtxSetCurrent(void * ctx)
#endif
{
	return cuCtxSetCurrent((CUcontext)ctx);
}
#ifdef WIN32
DLLEXP int  __stdcall CudaSetDevice(int device)
#else
extern "C" int  CudaSetDevice(int device)
#endif
{
	CUDACHECK(cudaSetDevice(device));
	return 0;
}

#ifdef WIN32
DLLEXP void __stdcall CudaDeviceSynchronize()
#else
extern "C" void CudaDeviceSynchronize()
#endif
{
	cudaDeviceSynchronize();
}

#ifdef WIN32
DLLEXP string  __stdcall CudaDeviceProperties(int i)
#else
extern "C" string  CudaDeviceProperties(int i)
#endif
{
	cudaDeviceProp devProp;
	cudaGetDeviceProperties(&devProp, i);
	char* msg = (char*)malloc(sizeof(char) * 4096);
	//  char msg[4096]; //= {"fasdfsd"};
	sprintf(msg, "Major revision number : %d \n", devProp.major);
	sprintf(msg + strlen(msg), "Minor revision number : %d \n", devProp.minor);
	sprintf(msg + strlen(msg), "Minor revision number : %d \n", devProp.minor);

	//wstring msg = "fssagsdf";

	sprintf(msg + strlen(msg), "Name : %s \n", devProp.name);
	sprintf(msg + strlen(msg), "Total global memory : %u \n", devProp.totalGlobalMem);
	sprintf(msg + strlen(msg), "Total shared memory per block : %d \n", devProp.sharedMemPerBlock);
	sprintf(msg + strlen(msg), "Total registers per block : %d \n", devProp.regsPerBlock);
	sprintf(msg + strlen(msg), "Warp size : %d \n", devProp.warpSize);
	sprintf(msg + strlen(msg), "Maximum memory pitch : %d \n", devProp.memPitch);

	sprintf(msg + strlen(msg), "Maximum threads per block : %u \n", devProp.maxThreadsPerBlock);


	for (int t = 0; t < 3; ++t)
		sprintf(msg + strlen(msg), "Maximum dimension %d of block:  %d\n", t, devProp.maxThreadsDim[t]);
	for (int t = 0; t < 3; ++t)
		sprintf(msg + strlen(msg), "Maximum dimension %d of grid:   %d\n", t, devProp.maxGridSize[t]);
	sprintf(msg + strlen(msg), "Clock rate:                    %d\n", devProp.clockRate);
	sprintf(msg + strlen(msg), "Total constant memory:         %u\n", devProp.totalConstMem);
	sprintf(msg + strlen(msg), "Texture alignment:             %u\n", devProp.textureAlignment);
	sprintf(msg + strlen(msg), "Concurrent copy and execution: %s\n", (devProp.deviceOverlap ? "Yes" : "No"));
	sprintf(msg + strlen(msg), "Number of multiprocessors:     %d\n", devProp.multiProcessorCount);
	sprintf(msg + strlen(msg), "Kernel execution timeout:      %s\n", (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
	sprintf(msg + strlen(msg), "GPU Major - Minor:      %d\t%d\n", (devProp.major), devProp.minor);

	string s(msg);
	return s;

}


extern "C" int* CudaAllocInt(int e)
{
	int * gpu_ints;
	cudaMalloc((void **)&gpu_ints, e * sizeof(int));
	cudaMemset(gpu_ints, 0, e * sizeof(int));
	return gpu_ints;
}

extern "C" void CudaDeallocInt(int * gpu_ints)
{
	cudaFree(gpu_ints);
}

extern "C" void * CudaAllocMem(int size)
{
	void * mem;
	cudaMalloc(&mem, size);
	return mem;
}

extern "C" void CudaDeallocMem(void * mem)
{
	cudaFree(mem);
}

#ifdef WIN32
DLLEXP int  __stdcall CudaCopyInInt(int * gpu_ints, int * int_array, int len)
#else
extern "C" int  CudaCopyInInt(int * gpu_ints, int * int_array, int len)
#endif
{
	return cudaMemcpy(gpu_ints, int_array, len * sizeof(int), cudaMemcpyHostToDevice);
}

#ifdef WIN32
DLLEXP int  __stdcall CudaCopyOutInt(int * gpu_ints, int * int_array, int len)
#else
extern "C" int  CudaCopyOutInt(int * gpu_ints, int * int_array, int len)
#endif
{
	return cudaMemcpy(int_array, gpu_ints, len * sizeof(int), cudaMemcpyDeviceToHost);
}

#ifdef WIN32
DLLEXP int  __stdcall CudaMemGetInfo(long *free, long *total)
#else
extern "C" int  CudaMemGetInfo(long *free, long *total)
#endif
{
	size_t cFree, cTotal;
	int erro = cudaMemGetInfo(&cFree, &cTotal);
	*free = cFree;
	*total = cTotal;
	return erro;
}

#ifdef WIN32
DLLEXP float *  __stdcall CudaAllocFloat(int e)
#else
extern "C" float *  CudaAllocFloat(int e)
#endif
{
	float * gpu_floats;
	cudaMalloc((void **)&gpu_floats, e * sizeof(float));
	cudaMemset(gpu_floats, 0, e * sizeof(float));
	return gpu_floats;
}

#ifdef WIN32
DLLEXP void __stdcall CudaDeallocFloat(float * gpu_floats)
#else
extern "C" void CudaDeallocFloat(float * gpu_floats)
#endif
{
	cudaFree(gpu_floats);
}

#ifdef WIN32
DLLEXP int  __stdcall CudaCopyInFloat(float * gpu_floats, float * float_array, int len)
#else
extern "C" int  CudaCopyInFloat(float * gpu_floats, float * float_array, int len)
#endif
{
	return cudaMemcpy(gpu_floats, float_array, len * sizeof(float), cudaMemcpyHostToDevice);
}

#ifdef WIN32
DLLEXP int  __stdcall CudaCopyOutFloat(float * gpu_floats, float * float_array, int len)
#else
extern "C" int  CudaCopyOutFloat(float * gpu_floats, float * float_array, int len)
#endif
{
	return cudaMemcpy(float_array, gpu_floats, len * sizeof(float), cudaMemcpyDeviceToHost);
}

#ifdef WIN32
DLLEXP int  __stdcall Zero(float * gpu_floats, int len)
#else
extern "C" int  Zero(float * gpu_floats, int len)
#endif
{
	return cudaMemset(gpu_floats, 0, len * sizeof(float));
}

#ifdef WIN32
DLLEXP void __stdcall CudaCopy(float * gpu_floats_a, float * gpu_float_b, int m, int aIdx, int bIdx)
#else
extern "C" void CudaCopy(float * gpu_floats_a, float * gpu_float_b, int m, int aIdx, int bIdx)
#endif
{
	cudaMemcpy(gpu_floats_a + aIdx, gpu_float_b + bIdx, m * sizeof(float), cudaMemcpyDeviceToDevice);
	//cuda_Copy(gpu_floats_a, gpu_float_b, m, aIdx, bIdx);
}




/*******************CuBlas Interface *****************************************/
//#ifdef WIN32
//DLLEXP void __stdcall CUBLAS_Init()
//#else
//extern "C" void CUBLAS_Init()
//#endif
//{
//	cublas_Init();
//}

//#ifdef WIN32
//DLLEXP void __stdcall CUBLAS_Destroy()
//#else
//extern "C" void CUBLAS_Destroy()
//#endif
//{
//	cublas_Destroy();
//}

//#ifdef WIN32
//DLLEXP void __stdcall CUBLAS_Matrix_Multipy(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse)
//#else
//extern "C" void CUBLAS_Matrix_Multipy(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse)
//#endif
//{
//	cublas_Matrix_Multipy(delta, weight, delta_low, batchsize, m, n, inverse);
//}


/*******************RNN Interface *****************************************/
#ifdef WIN32
DLLEXP void __stdcall RecursiveForwardPropagateBatch(int * InstanceIndex, int batchSize,
#else
extern "C" void RecursiveForwardPropagateBatch(int * InstanceIndex, int batchSize,
#endif
		float * SeqInputMatrixs, int dim, 
		float * WMatrix, int lag, int af, float * SeqOutputMatrix)
{
	Cuda_RecursiveForwardPropagateBatch(InstanceIndex, batchSize, SeqInputMatrixs, dim, WMatrix, lag, af, SeqOutputMatrix);
}

#ifdef WIN32
DLLEXP void __stdcall RecursiveBackwardPropagateBatch(int * InstanceIndex, int batchSize, 
#else
extern "C" void RecursiveBackwardPropagateBatch(int * InstanceIndex, int batchSize, 
#endif
		float * SeqOutputMatrixs, int dim, 
		float * WMatrix, int lag, int af, float * SeqDerivMatrixs)
{
	Cuda_RecursiveBackwardPropagateBatch(InstanceIndex, batchSize, SeqOutputMatrixs, dim, WMatrix, lag, af, SeqDerivMatrixs);
}

#ifdef WIN32
DLLEXP void __stdcall RecursiveUpdateBatch(int * InstanceIndex, int batchSize, int seqSize, float * SeqOutputMatrixs, int dim, 
#else
extern "C" void RecursiveUpdateBatch(int * InstanceIndex, int batchSize, int seqSize, float * SeqOutputMatrixs, int dim, 
#endif
		float * SeqDerivMatrixs, int lag, float * gradient, float weight)
{
	Cuda_RecursiveUpdateBatch(InstanceIndex, batchSize, seqSize, SeqOutputMatrixs, dim, SeqDerivMatrixs, lag, gradient, weight);
}

#ifdef WIN32
DLLEXP void __stdcall RecursiveExtendUpdateBatch(int * InstanceIndex, int batchSize, int seqSize, float * SeqOutputMatrixs,
#else
extern "C" void RecursiveExtendUpdateBatch(int * InstanceIndex, int batchSize, int seqSize, float * SeqOutputMatrixs,
#endif
		int * pre_Idx, //-> previous output,
		int pre_batchSize,  //->x;
		float * pre_O,
		int dim, float * SeqDerivMatrixs, int lag, float * gradient, float weight)
{
	Cuda_RecursiveExtendUpdateBatch(InstanceIndex, batchSize, seqSize, SeqOutputMatrixs, 
			pre_Idx, pre_batchSize, pre_O, dim, SeqDerivMatrixs, lag, gradient, weight);
}
/*******************LSTM Interface *****************************************/
#ifdef WIN32
DLLEXP void __stdcall LSTMExtendForwardPropagateBatch(int * srcInstanceIdx, int srcBatchSize,  //->src_x;
#else
extern "C" void LSTMExtendForwardPropagateBatch(int * srcInstanceIdx, int srcBatchSize,  //->src_x;
#endif
		float * pre_o, //-> previous output,
		float * pre_c, //-> previous c,
		int * InstanceIndex, int batchSize,  //->x;
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell, float * Ui, float * Uc, float * Uf, float * Uo, float * Vo)
{
	Cuda_LSTMExtendForwardPropagateBatch(srcInstanceIdx, srcBatchSize, pre_o, pre_c, InstanceIndex, batchSize, 
			o, gate_i, c_hat, gate_f, c, gate_o, tanhc, cell, Ui, Uc, Uf, Uo, Vo);
}

#ifdef WIN32
DLLEXP void __stdcall LSTMForwardPropagateBatch(int * InstanceIndex, int batchSize,  //->x;
#else
extern "C" void LSTMForwardPropagateBatch(int * InstanceIndex, int batchSize,  //->x;
#endif
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell, float * Ui, float * Uc, float * Uf, float * Uo, float * Vo)
{
	Cuda_LSTMForwardPropagateBatch(InstanceIndex, batchSize, o, gate_i, c_hat, gate_f, c, gate_o, tanhc, cell, Ui, Uc, Uf, Uo, Vo);
}

#ifdef WIN32
DLLEXP void __stdcall LSTMExtendBackwardPropagateBatch(
#else
extern "C" void LSTMExtendBackwardPropagateBatch(
#endif
		int * preInstanceIdx, int preBatchSize,  //->src_x;
		float * pre_o, //-> previous output,
		float * pre_c, //-> previous c,
		float * deriv_pre_o,	// -> previous deriv_o
		float * deriv_pre_c,	// -> previous deriv_c
		int * InstanceIndex, int batchSize, int seqSize, int cell,
		float * o,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,
		float * tanhc,
		float * deriv_o,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * deriv_tanhc,
		float * Ui, float * Uc, float * Uf, float * Uo, float * Vo)
{
	Cuda_LSTMExtendBackwardPropagateBatch(preInstanceIdx, preBatchSize, pre_o, pre_c,
			deriv_pre_o, deriv_pre_c, InstanceIndex, batchSize, seqSize, cell, o, gate_i, c_hat, gate_f, c,
			gate_o, tanhc, deriv_o, deriv_gate_i, deriv_cHat, deriv_gate_f, deriv_c, deriv_gate_o, deriv_tanhc, Ui, Uc, Uf, Uo, Vo);
}


#ifdef WIN32
DLLEXP void __stdcall LSTMLinkExtendForwardPropagateBatch(
#else
extern "C" void LSTMLinkExtendForwardPropagateBatch(
#endif
		float * pre_o, //-> previous output,
		float * pre_c, //-> previous c,
		int * InstanceIndex, int batchSize,  //->x;
		int * pre_idx,
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell,
		float * Ui,
		float * Uc,
		float * Uf,
		float * Uo, float * Vo)
{
	Cuda_LSTMLinkExtendForwardPropagateBatch(pre_o, pre_c, InstanceIndex, batchSize, pre_idx,
			o, gate_i, c_hat, gate_f, c, gate_o, tanhc, cell, Ui, Uc, Uf, Uo, Vo);
}

#ifdef WIN32
DLLEXP void __stdcall LSTMBackwardPropagateBatch(int * InstanceIndex, int batchSize, int seqSize, int cell,
#else
extern "C" void LSTMBackwardPropagateBatch(int * InstanceIndex, int batchSize, int seqSize, int cell,
#endif
		float * o,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,
		float * tanhc,

		float * deriv_o,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * deiv_tanhc,

		float * Ui, float * Uc, float * Uf, float * Uo, float * Vo)
{
	Cuda_LSTMBackwardPropagateBatch(InstanceIndex, batchSize, seqSize, cell, o, gate_i, c_hat, gate_f, c,
			gate_o, tanhc, deriv_o, deriv_gate_i, deriv_cHat, deriv_gate_f, deriv_c, deriv_gate_o, deiv_tanhc, Ui, Uc, Uf, Uo, Vo);
}

/*************Embedding Learning Function*************************************/
#ifdef WIN32
DLLEXP void __stdcall HierarcialSoftmaxPairEmbedding(
#else
extern "C" void HierarcialSoftmaxPairEmbedding(
#endif
		float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv,
		float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv,
		int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,
		float step, // step size for update vSpace.
		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classAct,
		float * wordAct,
		int * wordActIdx1,
		int * wordActIdx2,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb)
{
	Cuda_HierarcialSoftmaxPairEmbedding(srcLabels, srcLabelCount, srcEmbedding, srcDeriv, tgtLabels, tgtLabelCount, tgtEmbedding, tgtDeriv,
			dim, vSpace, vBias, vocabSize, cSpace, cBias, cSize, step, v2c, classIdx, wordIdx, wordClassIdx, gamma, classAct, wordAct,
			wordActIdx1, wordActIdx2, wordSegSize, targetProb);
}

#ifdef WIN32
DLLEXP void __stdcall HierarcialSoftmaxPairProbability(
#else
extern "C" void HierarcialSoftmaxPairProbability(
#endif
		float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv,
		float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv,
		int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,
		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classAct,
		float * wordAct,
		int * wordActIdx1,
		int * wordActIdx2,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb)
{
	Cuda_HierarcialSoftmaxPairProbability(srcLabels, srcLabelCount, srcEmbedding, srcDeriv,
			tgtLabels, tgtLabelCount, tgtEmbedding, tgtDeriv, dim,
			vSpace, vBias, vocabSize, cSpace, cBias, cSize,
			v2c, classIdx, wordIdx, wordClassIdx, gamma, classAct, wordAct,
			wordActIdx1, wordActIdx2, wordSegSize, targetProb);
}


#ifdef WIN32
DLLEXP void __stdcall FullySoftmaxPairProbability(
#else
extern "C" void FullySoftmaxPairProbability(
#endif
		float * srcLabels, int srcLabelCount, float * srcEmbedding,
		float * tgtLabels, int tgtLabelCount, float * tgtEmbedding,
		int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step, // step size for update vSpace.
		float gamma,
		float * wordAct,
		float * targetProb)
{
	Cuda_FullySoftmaxPairProbability(srcLabels, srcLabelCount, srcEmbedding, tgtLabels, tgtLabelCount, tgtEmbedding,
			dim, vSpace, vBias, vocabSize, step, gamma, wordAct, targetProb);
}

#ifdef WIN32
DLLEXP void __stdcall HierarcicalSoftmaxProbability(float * targets, int targetNum,
#else
extern "C" void HierarcicalSoftmaxProbability(float * targets, int targetNum,
#endif
		float * embedding, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,

		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classOutput,
		float * wordOutput,
		int * wordActIdx1,
		int * wordActIdx2,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb)
{
	Cuda_HierarcicalSoftmaxProbability(targets, targetNum, embedding, dim, vSpace, vBias, vocabSize,
			cSpace, cBias, cSize, v2c, classIdx, wordIdx, wordClassIdx, gamma, classOutput, wordOutput, wordActIdx1, wordActIdx2, wordSegSize, targetProb);
}

#ifdef WIN32
DLLEXP void __stdcall HierarcialSoftmaxEmbeddingV2(float * targets, int targetNum,
#else
extern "C" void HierarcialSoftmaxEmbeddingV2(float * targets, int targetNum,
#endif
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,
		float step,
		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classOutput,
		float * wordOutput,
		int * wordActIdx1,
		int * wordActIdx2,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb)
{
	Cuda_HierarcialSoftmaxEmbeddingV2(targets, targetNum, embedding, deriv, dim,
			vSpace, vBias, vocabSize, cSpace, cBias, cSize, step, v2c, classIdx, wordIdx, wordClassIdx,
			gamma, classOutput, wordOutput, wordActIdx1, wordActIdx2, wordSegSize, targetProb);
}


#ifdef WIN32
DLLEXP void __stdcall HierarcialSoftmaxDecoding(float * embedding, int batchSize, int dim,
#else
extern "C" void HierarcialSoftmaxDecoding(float * embedding, int batchSize, int dim,
#endif
		float * vSpace, float * vBias, int vSize,
		float * cSpace, float * cBias, int cSize,
		int bestN, float gamma,
		int * v2c,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		float * tmpClassOutput, float * tmpWordOutput,
		float * wordSet, float * wordProb)
{
	Cuda_HierarcialSoftmaxDecoding(embedding, batchSize, dim, vSpace, vBias, vSize, cSpace, cBias, cSize, bestN, gamma,
			v2c, classIdx, wordIdx, tmpClassOutput, tmpWordOutput, wordSet, wordProb);
}

#ifdef WIN32
DLLEXP void __stdcall FullySoftmaxDecoding(float * embedding, int batchSize, int dim,
#else
extern "C" void FullySoftmaxDecoding(float * embedding, int batchSize, int dim,
#endif
		float * vSpace, float * vBias, int vSize,
		int bestN, float gamma, float * tmpWordOutput,
		float * wordSet, float * wordProb)
{
	Cuda_FullySoftmaxDecoding(embedding, batchSize, dim, vSpace, vBias, vSize, bestN, gamma, tmpWordOutput, wordSet, wordProb);
}

#ifdef WIN32
DLLEXP void __stdcall DerivSparseSoftmax(int * word_candidate_count, float * wordAct, float * wordDeriv, int * wordLabel,
#else
extern "C" void DerivSparseSoftmax(int * word_candidate_count, float * wordAct, float * wordDeriv, int * wordLabel,
#endif
		float gamma, int batchSize, float * targetProb)
{
	Cuda_DerivSparseSoftmax(word_candidate_count, wordAct, wordDeriv, wordLabel, gamma, batchSize, targetProb);
}

#ifdef WIN32
DLLEXP void __stdcall FullySoftmaxPairEmbedding(
#else
extern "C" void FullySoftmaxPairEmbedding(
#endif
		float * srcLabels, int srcLabelCount, float * srcEmbedding, float * srcDeriv,
		float * tgtLabels, int tgtLabelCount, float * tgtEmbedding, float * tgtDeriv,
		int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step, // step size for update vSpace.
		float gamma,
		float * wordAct,
		float * targetProb)
{
	Cuda_FullySoftmaxPairEmbedding(srcLabels, srcLabelCount, srcEmbedding, srcDeriv,
			tgtLabels, tgtLabelCount, tgtEmbedding, tgtDeriv,
			dim, vSpace, vBias, vocabSize, step, gamma, wordAct, targetProb);
}

#ifdef WIN32
DLLEXP void __stdcall DerivSoftmax_Test(float * outputScore, int * outputLabel, float * outputDeriv, int dim, float gamma, int batchSize, float * targetProb)
#else
extern "C" void DerivSoftmax_Test(float * outputScore, int * outputLabel, float * outputDeriv, int dim, float gamma, int batchSize, float * targetProb)
#endif
{
	Cuda_DerivSoftmax_Test(outputScore, outputLabel, outputDeriv, dim, gamma, batchSize, targetProb);
}

#ifdef WIN32
DLLEXP int  __stdcall Max(int * int_array, int len)
#else
extern "C" int  Max(int * int_array, int len)
#endif
{
	return Cuda_Max(int_array, len);
}



#ifdef WIN32
DLLEXP void __stdcall RNNForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix,
#else
extern "C" void RNNForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix,
#endif
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias, int af, float * seqOut)
{
	cuda_RNNForwardPropagate(smpIdx, batchSize, seqIdx, seqSize, feaIdx, feaValue, Imatrix, Wmatrix, lag, feaDim, hiddenDim,
			bias, af, seqOut);
}

#ifdef WIN32
DLLEXP void __stdcall RNNForwardPropagate_Simple(int * smpIdx, int batchSize, int seqSize, float * Wmatrix, int lag, int hiddenDim, int af, float * seqOut)
#else
extern "C" void RNNForwardPropagate_Simple(int * smpIdx, int batchSize, int seqSize, float * Wmatrix, int lag, int hiddenDim, int af, float * seqOut)
#endif
{
	cuda_RNNForwardPropagate(smpIdx, batchSize, seqSize, Wmatrix, lag, hiddenDim, af, seqOut);
}


#ifdef WIN32
DLLEXP void __stdcall RNNBackwardPropagate(int * smpIdx, int batchSize, float * derivOutput, float * output, int seqSize, float * wMatrix, int lag, int hiddenDim, int af)
#else
extern "C" void RNNBackwardPropagate(int * smpIdx, int batchSize, float * derivOutput, float * output, int seqSize, float * wMatrix, int lag, int hiddenDim, int af)
#endif
{
	cuda_RNNBackwardPropagate(smpIdx, batchSize,
			derivOutput, output, seqSize, wMatrix, lag, hiddenDim, af);
}

#ifdef WIN32
DLLEXP void __stdcall RNNBackwardPropagateSlot(int * smpIdx, int batchSize, float * derivOutput, float * output, int seqSize, float * wMatrix,
#else
extern "C" void RNNBackwardPropagateSlot(int * smpIdx, int batchSize, float * derivOutput, float * output, int seqSize, float * wMatrix,
#endif
		int hiddenDim, int af, int * earlyStop)
{
	cuda_RNNBackwardPropagateSlot(smpIdx, batchSize, derivOutput, output, seqSize, wMatrix, hiddenDim, af, earlyStop);
}

#ifdef WIN32
DLLEXP void __stdcall RecurrentUpdate(int * smpIdx, int batchSize, float * output, float * derivOutput, int hiddenDim, int seqSize, int lag, float * gradient, float weight)
#else
extern "C" void RecurrentUpdate(int * smpIdx, int batchSize, float * output, float * derivOutput, int hiddenDim, int seqSize, int lag, float * gradient, float weight)
#endif
{
	cuda_RecurrentUpdate(smpIdx, batchSize, output, derivOutput, hiddenDim, seqSize, lag, gradient, weight);
}

#ifdef WIN32
DLLEXP void __stdcall RecurrentUpdateSlot(int * smpIdx, int batchSize, int * earlyStop, float * output, float * derivOutput, int hiddenDim,
#else
extern "C" void RecurrentUpdateSlot(int * smpIdx, int batchSize, int * earlyStop, float * output, float * derivOutput, int hiddenDim,
#endif
		int seqSize, int lag, float * gradient, float weight)
{
	cuda_RecurrentUpdateSlot(smpIdx, batchSize, earlyStop, output, derivOutput, hiddenDim, seqSize, lag, gradient, weight);
}


/**************Cuda  Matrix Operation************************/

#ifdef WIN32
DLLEXP void __stdcall MatrixMultiplicationTranspose(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt)
#else
extern "C" void MatrixMultiplicationTranspose(float * pLeft, float * pRight, float * pDst, int leftRowCnt, int rightColumnCnt, int rightRowCnt)
#endif
{
	Cuda_MatrixMultiplicationTranspose(pLeft, pRight, pDst, leftRowCnt, rightColumnCnt, rightRowCnt);
}
/************************************************************/




#ifdef WIN32
DLLEXP void __stdcall Matrix_Add_Tanh(float * gpu_floats_a, float * gpu_floats_b, int m, int n)
#else
extern "C" void Matrix_Add_Tanh(float * gpu_floats_a, float * gpu_floats_b, int m, int n)
#endif
{
	cuda_Matrix_Add_Tanh(gpu_floats_a, gpu_floats_b, m, n);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_Cosine(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
#else
extern "C" void Deriv_Cosine(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_Cosine(q, d, dcq, dcd, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_Cosine_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
#else
extern "C" void Deriv_Cosine_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_Cosine_EX(q, d, neg_list, dcq, dcd, batchsize, m, eps);
}






#ifdef WIN32
DLLEXP void __stdcall Matrix_Multipy(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse)
#else
extern "C" void Matrix_Multipy(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse)
#endif
{
	cuda_Matrix_Multipy(delta, weight, delta_low, batchsize, m, n, inverse);
}


#ifdef WIN32
DLLEXP void __stdcall Cosine_Similarity(float * a, float * b, float * c, int nTrial, int BATCHSIZE, int mindex,int batchsize, int dimension, float eps)
#else
extern "C" void Cosine_Similarity(float * a, float * b, float * c, int nTrial, int BATCHSIZE, int mindex,int batchsize, int dimension, float eps)
#endif
{
	cuda_Cosine_Similarity(a, b, c, nTrial, BATCHSIZE, mindex, batchsize, dimension, eps);
}
#ifdef WIN32
DLLEXP void __stdcall Inner_Product_EX_Full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE,
#else
extern "C" void Inner_Product_EX_Full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE,
#endif
		int batchsize, int dimension, float eps)
{
	cuda_Inner_Product_EX_Full(a, b, neg_list, c, nTrial, BATCHSIZE, batchsize, dimension, eps);
}


#ifdef WIN32
DLLEXP void __stdcall Cosine_Similarity_EX(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int mindex,
#else
extern "C" void Cosine_Similarity_EX(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int mindex,
#endif
		int batchsize, int dimension, float eps)
{
	cuda_Cosine_Similarity_EX(a, b, neg_list, c, nTrial, BATCHSIZE, mindex, batchsize, dimension, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Calculate_Alpha(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#else
extern "C" void Calculate_Alpha(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#endif
{
	cuda_Calculate_Alpha(alpha, nTrial, BATCHSIZE, batchsize, gamma);
}

#ifdef WIN32
DLLEXP void __stdcall Calculate_Alpha_MXE(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#else
extern "C" void Calculate_Alpha_MXE(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#endif
{
	cuda_Calculate_Alpha_MXE(alpha, nTrial, BATCHSIZE, batchsize, gamma);
}

#ifdef WIN32
DLLEXP void __stdcall Calculate_Alpha_NCE(float * alpha, float * dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#else
extern "C" void Calculate_Alpha_NCE(float * alpha, float * dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#endif
{
	cuda_Calculate_Alpha_NCE(alpha, dist, nTrial, BATCHSIZE, batchsize, gamma);
}

#ifdef WIN32
DLLEXP void __stdcall Calculate_Alpha_NCE2(float * alpha, float * dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#else
extern "C" void Calculate_Alpha_NCE2(float * alpha, float * dist, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#endif
{
	cuda_Calculate_Alpha_NCE2(alpha, dist, nTrial, BATCHSIZE, batchsize, gamma);
}

#ifdef WIN32
DLLEXP void __stdcall Calculate_Alpha_PAIRRANK(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#else
extern "C" void Calculate_Alpha_PAIRRANK(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma)
#endif
{
	cuda_Calculate_Alpha_PAIRRANK(alpha, nTrial, BATCHSIZE, batchsize, gamma);
}

#ifdef WIN32
DLLEXP void __stdcall Calculate_SampleLRDeriv(float * output, float * deriv, int trial, int BATCHSIZE, float * label, int batchsize, float gamma)
#else
extern "C" void Calculate_SampleLRDeriv(float * output, float * deriv, int trial, int BATCHSIZE, float * label, int batchsize, float gamma)
#endif
{
	Cuda_Calculate_SampleLRDeriv(output, deriv, trial, BATCHSIZE, label, batchsize, gamma);
}


#ifdef WIN32
DLLEXP void __stdcall FillOut_Dist_NCE(float* dist, int* neg_list, int nTrailPlus1, int BATCH_SIZE, int mindex, int batchsize)
#else
extern "C" void FillOut_Dist_NCE(float* dist, int* neg_list, int nTrailPlus1, int BATCH_SIZE, int mindex, int batchsize)
#endif
{
	cuda_FillOut_Dist_NCE(dist, neg_list, nTrailPlus1, BATCH_SIZE, mindex, batchsize);
}



#ifdef WIN32
DLLEXP void __stdcall Matrix_Product(float * a, float * b, float * c, int batchsize, int m, int n)
#else
extern "C" void Matrix_Product(float * a, float * b, float * c, int batchsize, int m, int n)
#endif
//, int kept, float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	cuda_Matrix_Product(a, b, c, batchsize, m, n); //,kept, alpha, ntrial, BATCH_SIZE, alpha_index);
}


#ifdef WIN32
DLLEXP void __stdcall SEQ_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
#else
extern "C" void SEQ_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
#endif
		int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	cuda_SEQ_Sparse_Matrix_Multiply_INTEX(Smp_Index, batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension, win_size);
}

#ifdef WIN32
DLLEXP void __stdcall SEQ_Sparse_Matrix_Multiply_INT(int * Smp_Index, int batchsize, int * Seg_Index, int seg_size, int * Fea_Index,
#else
extern "C" void SEQ_Sparse_Matrix_Multiply_INT(int * Smp_Index, int batchsize, int * Seg_Index, int seg_size, int * Fea_Index,
#endif
		float * Fea_Value, int elementsize, float * mul_weight, float * output, int Feature_dimension, int output_dimension)
{
	cuda_SEQ_Sparse_Matrix_Multiply_INT(Smp_Index, batchsize, Seg_Index, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension);
}

#ifdef WIN32
DLLEXP void __stdcall SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
#else
extern "C" void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
#endif
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(Smp_Index, batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension, win_size);
}


#ifdef WIN32
DLLEXP void __stdcall Convolution_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
#else
extern "C" void Convolution_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
#endif
		int seg_size, int * Fea_Index, float * Fea_Value, int elementsize,
		float * con_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	cuda_Convolution_Sparse_Matrix_Multiply_INTEX(Smp_Index, batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, Fea_Value, elementsize, con_weight, output, Feature_dimension, output_dimension, win_size);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_Add_Vector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
#else
extern "C" void Matrix_Add_Vector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
#endif
{
	Cuda_Matrix_AddVector(gpu_floats_a, gpu_floats_b, batchsize, dimension);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_Rectified_Vector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
#else
extern "C" void Matrix_Rectified_Vector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension)
#endif
{
	cuda_Matrix_Rectified_Vector(gpu_floats_a, gpu_floats_b, batchsize, dimension);
}

#ifdef WIN32
DLLEXP void __stdcall Derive_Cosine_Linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
#else
extern "C" void Derive_Cosine_Linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
#endif
{
	cuda_Derive_Cosine_Linear(q, d, dcq, dcd, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Derive_Cosine_Linear_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
#else
extern "C" void Derive_Cosine_Linear_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_Cosine_Linear_EX(q, d, neg_list, dcq, dcd, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Derive_Cosine_Rectified(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
#else
extern "C" void Derive_Cosine_Rectified(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
#endif
{
	cuda_Derive_Cosine_Rectified(q, d, dcq, dcd, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Derive_Cosine_Rectified_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
#else
extern "C" void Derive_Cosine_Rectified_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_Cosine_Rectified_EX(q, d, neg_list, dcq, dcd, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall RectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize,
#else
extern "C" void RectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize,
#endif
		float gamma, int * locationIdx, float * locationValue)
{
	Cuda_RectifiedGaussian(locationForcusValue, ensembleLen, batchSize, smpIdx, windowSize, gamma, locationIdx, locationValue);
}

#ifdef WIN32
DLLEXP void __stdcall Gaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, float gamma, float * locationValue)
#else
extern "C" void Gaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, float gamma, float * locationValue)
#endif
{
	Cuda_Gaussian(locationForcusValue, ensembleLen, batchSize, smpIdx, gamma, locationValue);
}


#ifdef WIN32
DLLEXP void __stdcall DerivRectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize,
#else
extern "C" void DerivRectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize,
#endif
		float gamma, int * locationIdx, float * locationValue, float * locationForcusDeriv, float * locationDeriv)
{
	Cuda_DerivRectifiedGaussian(locationForcusValue, ensembleLen, batchSize, smpIdx, windowSize, gamma, 
			locationIdx, locationValue, locationForcusDeriv, locationDeriv);
}

#ifdef WIN32
DLLEXP void __stdcall DerivGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx,
#else
extern "C" void DerivGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx,
#endif
		float gamma, float * locationValue, float * locationForcusDeriv, float * locationDeriv)
{
	Cuda_DerivGaussian(locationForcusValue, ensembleLen, batchSize, smpIdx,
			gamma, locationValue, locationForcusDeriv, locationDeriv);
}

//#ifdef WIN32
//DLLEXP void __stdcall MemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValue, int ensembleLen,
//#else
//#endif
extern "C" void MemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValue, int ensembleLen,
		int windowSize, int memoryVecDim, float * memoryOutput)
{
	Cuda_MemoryAddressing(smpIdx, batchSize, memoryInput, locationIdx, locationValue, ensembleLen, windowSize,
			memoryVecDim, memoryOutput);
}

#ifdef WIN32
DLLEXP void __stdcall GaussianMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, float * locationValue, int ensembleLen, int memoryVecDim, float * memoryOutput)
#else
extern "C" void GaussianMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, float * locationValue, int ensembleLen, int memoryVecDim, float * memoryOutput)
#endif
{
	Cuda_GaussianMemoryAddressing(smpIdx, batchSize, memoryInput, locationValue, ensembleLen, memoryVecDim, memoryOutput);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivGaussianMemoryAddressing(int * sentMargin, int sequenceSize, float * memoryInput, float * locationValueDeriv,
//#else
//#endif

extern "C" void DerivGaussianMemoryAddressing(int * sentMargin, int sequenceSize, float * memoryInput, float * locationValueDeriv,
		int memoryVecDim, float * memoryOutputDeriv)
{
	Cuda_DerivGaussianMemoryAddressing(sentMargin, sequenceSize, memoryInput, locationValueDeriv, memoryVecDim, memoryOutputDeriv);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValueDeriv,
//#else
//	#endif

extern "C" void DerivMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValueDeriv,
		int ensembleLen, int windowSize, int memoryVecDim, float * memoryOutputDeriv)
{
	Cuda_DerivMemoryAddressing(smpIdx, batchSize, memoryInput, locationIdx, locationValueDeriv, 
			ensembleLen, windowSize, memoryVecDim, memoryOutputDeriv);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivGaussianMemory(int * sentMargin, int sequenceSize, float * memoryInputDeriv, float * locationValue,
//#else
//#endif
extern "C" void DerivGaussianMemory(int * sentMargin, int sequenceSize, float * memoryInputDeriv, float * locationValue,
		int ensembleLen, int memoryVecDim, float * memoryOutputDeriv)
{
	Cuda_DerivGaussianMemory(sentMargin, sequenceSize, memoryInputDeriv, locationValue,
			ensembleLen,  memoryVecDim, memoryOutputDeriv);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivMemory(int * smpIdx, int batchSize, float * memoryInputDeriv, int * locationIdx, float * locationValue,
//#else
//#endif

extern "C" void DerivMemory(int * smpIdx, int batchSize, float * memoryInputDeriv, int * locationIdx, float * locationValue,
		int ensembleLen, int windowSize, int memoryVecDim, float * memoryOutputDeriv)
{
	Cuda_DerivMemory(smpIdx, batchSize, memoryInputDeriv, locationIdx, locationValue,
			ensembleLen, windowSize, memoryVecDim, memoryOutputDeriv);
}

//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Sparse_Matrix_Product_INTEX(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
//#else
//#endif

extern "C" void Convolution_Sparse_Matrix_Product_INTEX(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension)
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index)
{
	cuda_Convolution_Sparse_Matrix_Product_INTEX(deriv, maxpooling_index, Seg_Index, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, Fea_Index, Fea_Value, grad, Feature_Dimension);
	//,alpha, ntrial, BATCH_SIZE, alpha_index);
}

//#ifdef WIN32
//DLLEXP void __stdcall Matrix_WeightAdd(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension, float * mweight, int start, int keep)
//#else
//#endif

extern "C" void Matrix_WeightAdd(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension, float * mweight, int start, int keep)
{
	cuda_Matrix_WeightAdd(gpu_floats_a, gpu_floats_b, batchsize, dimension, mweight, start, keep);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_WeightAdd_EX(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int batchsize, int dimension, float * mweight, int start, int keep)
#else
extern "C" void Matrix_WeightAdd_EX(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int batchsize, int dimension, float * mweight, int start, int keep)
#endif
{
	cuda_Matrix_WeightAdd_EX(gpu_floats_a, gpu_floats_b, inver_neg_index, inver_neg_value, batchsize, dimension, mweight, start, keep);
}

#ifdef WIN32
DLLEXP void __stdcall Sparse2Dense_Matrix(int * Smp_Idx, int * Fea_Idx, float * Fea_Value, float * matrix, int batchsize, int outputDimension)
#else
extern "C" void Sparse2Dense_Matrix(int * Smp_Idx, int * Fea_Idx, float * Fea_Value, float * matrix, int batchsize, int outputDimension)
#endif
{
	cuda_Sparse2Dense_Matrix(Smp_Idx, Fea_Idx, Fea_Value, matrix, batchsize, outputDimension);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_Aggragate(float * a, float * b, int batchsize, int m)
#else
extern "C" void Matrix_Aggragate(float * a, float * b, int batchsize, int m)
#endif
{
	cuda_Matrix_Aggragate(a, b, batchsize, m);
}


#ifdef WIN32
DLLEXP void __stdcall Matrix_Add_OFFSET(float * gpu_floats_a, int offset_a, float * gpu_floats_b, int offset_b, int len, float mweight)
#else
extern "C" void Matrix_Add_OFFSET(float * gpu_floats_a, int offset_a, float * gpu_floats_b, int offset_b, int len, float mweight)
#endif
{
	cuda_Matrix_Add_OFFSET(gpu_floats_a, offset_a, gpu_floats_b, offset_b, len, mweight);
}


#ifdef WIN32
DLLEXP void __stdcall Cosine_Similarity_EX_Full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps)
#else
extern "C" void Cosine_Similarity_EX_Full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps)
#endif
{
	cuda_Cosine_Similarity_EX_Full(a, b, neg_list, c, nTrial, BATCHSIZE, batchsize, dimension, eps);
}

#ifdef WIN32
DLLEXP void __stdcall FillOut_Dist_NCE_Full(float* dist, int* neg_list, int nTrail, int BATCH_SIZE, int batchsize)
#else
extern "C" void FillOut_Dist_NCE_Full(float* dist, int* neg_list, int nTrail, int BATCH_SIZE, int batchsize)
#endif
{
	cuda_FillOut_Dist_NCE_Full(dist, neg_list, nTrail, BATCH_SIZE, batchsize);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_Cosine_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
#else
extern "C" void Deriv_Cosine_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_Cosine_EX_Full(q, d, neg_list, dcq, dcd, nTrail, BATCHSIZE, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_Cosine_Linear_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
#else
extern "C" void Deriv_Cosine_Linear_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_Cosine_Linear_EX_Full(q, d, neg_list, dcq, dcd, nTrail, BATCHSIZE, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_InnerProduct_Linear_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
#else
extern "C" void Deriv_InnerProduct_Linear_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_InnerProduct_Linear_EX_Full(q, d, neg_list, dcq, dcd, nTrail, BATCHSIZE, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_InnerProduct_Linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
#else
extern "C" void Deriv_InnerProduct_Linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_InnerProduct_Linear(q, d, dcq, dcd, batchsize, m, eps);
}


#ifdef WIN32
DLLEXP void __stdcall Deriv_Cosine_Rectified_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
#else
extern "C" void Deriv_Cosine_Rectified_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps)
#endif
{
	cuda_Deriv_Cosine_Rectified_EX_Full(q, d, neg_list, dcq, dcd, nTrail, BATCHSIZE, batchsize, m, eps);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_WeightAdd_Full(float * gpu_floats_a, float * gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep)
#else
extern "C" void Matrix_WeightAdd_Full(float * gpu_floats_a, float * gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep)
#endif
{
	cuda_Matrix_WeightAdd_Full(gpu_floats_a, gpu_floats_b, nTrail, BATCHSIZE, batchsize, dimension, mweight, start, keep);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_WeightAdd_EX_Full(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep)
#else
extern "C" void Matrix_WeightAdd_EX_Full(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep)
#endif
{
	cuda_Matrix_WeightAdd_EX_Full(gpu_floats_a, gpu_floats_b, inver_neg_index, inver_neg_value, nTrial, BATCHSIZE, batchsize, dimension, mweight, start, keep);
}

#ifdef WIN32
DLLEXP void __stdcall Cosine_Similarity_SubSpace(float * a, float * b, float * c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps)
#else
extern "C" void Cosine_Similarity_SubSpace(float * a, float * b, float * c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps)
#endif
{
	cuda_Cosine_Similarity_SubSpace(a, b, c, labelDim, BATCHSIZE, batchsize, subspaceDim, eps);
}

#ifdef WIN32
DLLEXP void __stdcall SoftMax(float * a, float * b, int labelDim, int batchsize, float gamma)
#else
extern "C" void SoftMax(float * a, float * b, int labelDim, int batchsize, float gamma)
#endif
{
	Cuda_SoftMax(a, b, labelDim, batchsize, gamma);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_Cosine_Subspace(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps)
#else
extern "C" void Deriv_Cosine_Subspace(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps)
#endif
{
	cuda_Deriv_Cosine_Subspace(q, d, dcq, dcd, alpha, act_type, batchsize, labelDim, subspaceDim, gamma, eps);
}

#ifdef WIN32
DLLEXP void __stdcall InnerProduct_Similarity(float * a, float * b, float * c, int batchsize, int dimension)
#else
extern "C" void InnerProduct_Similarity(float * a, float * b, float * c, int batchsize, int dimension)
#endif
{
	cuda_InnerProduct_Similarity(a, b, c, batchsize, dimension);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_InnerProduct(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
#else
extern "C" void Deriv_InnerProduct(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int Dim, float gamma, float eps)
#endif
{
	cuda_Deriv_InnerProduct(q, d, dcq, dcd, alpha, act_type, batchsize, Dim, gamma, eps);
}



//#ifdef WIN32
//DLLEXP void __stdcall SEQ_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
//#else
//#endif
extern "C" void SEQ_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size, float weight)
{
	cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(Smp_Index, batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output, Feature_dimension, output_dimension, win_size, weight);
}


//#ifdef WIN32
//DLLEXP void __stdcall SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(int * Smp_Index, int batchsize, int * Seg_Index, int seg_size, int * Fea_Index,
//#else
//#endif

extern "C" void SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(int * Smp_Index, int batchsize, int * Seg_Index, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize, float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
{
	cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(Smp_Index, batchsize, Seg_Index, seg_size, Fea_Index, Fea_Value, elementsize, mul_weight, output,
			Feature_dimension, output_dimension, weight);
}

//#ifdef WIN32
//DLLEXP void __stdcall Matrix_Product_Weight(float * a, float * b, float * c, int batchsize, int m, int n, float weight)
//#else
extern "C" void Matrix_Product_Weight(float * a, float * b, float * c, int batchsize, int m, int n, float weight)
//#endif
{
	Cuda_MatrixMultiplicationLeftTranspose(a, b, c, batchsize, m, n, 1, weight);
	//cuda_Matrix_Product_Weight(a, b, c, batchsize, m, n, weight);
}

#ifdef WIN32
DLLEXP void __stdcall SGD_Train(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
#else
extern "C" void SGD_Train(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
#endif
{
	cuda_SGD_Train(SampleIdx, FeatureIdx, FeatureValue, Label, BatchSize, L1Weight, L1Bias, L2Weight, L2Bias, HiddenNum, learnRate);
}

#ifdef WIN32
DLLEXP void __stdcall Predict(int* SampleIdx, int* FeatureIdx, float* FeatureValue, float * result, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
#else
extern "C" void Predict(int* SampleIdx, int* FeatureIdx, float* FeatureValue, float * result, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
#endif
{
	cuda_Predict(SampleIdx, FeatureIdx, FeatureValue, result, BatchSize, L1Weight, L1Bias, L2Weight, L2Bias, HiddenNum, learnRate);
}

#ifdef WIN32
DLLEXP void __stdcall SGD_Train_New(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
#else
extern "C" void SGD_Train_New(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate)
#endif
{
	cuda_SGD_Train_New(SampleIdx, FeatureIdx, FeatureValue, Label, BatchSize, L1Weight, L1Bias, L2Weight, L2Bias, HiddenNum, learnRate);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_Mask(float * gpu_floats_a, int * gpu_mask, int batchsize, int dimension)
#else
extern "C" void Matrix_Mask(float * gpu_floats_a, int * gpu_mask, int batchsize, int dimension)
#endif
{
	cuda_Matrix_Mask(gpu_floats_a, gpu_mask, batchsize, dimension);
}


//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Sparse_Matrix_Product_INTEX_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
//#else
//#endif

extern "C" void Convolution_Sparse_Matrix_Product_INTEX_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
{
	cuda_Convolution_Sparse_Matrix_Product_INTEX_Weight(deriv, maxpooling_index, Seg_Index, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, Fea_Index, Fea_Value, grad, Feature_Dimension, learnRate);
}

//#ifdef WIN32
//DLLEXP void __stdcall Cosine_Similarity_Matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
//#else
//#endif
extern "C" void Cosine_Similarity_Matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float * aa, float * bb,  float eps)
{
	cuda_Cosine_Similarity_Matching(a, b, c, matchIdxA, matchIdxB, aSize, bSize, matchSize, dimension, aa, bb, eps);
}

//#ifdef WIN32
//DLLEXP void __stdcall Joint_Sparse_Matrix_Multiply_INTEX(
//#else
//#endif

extern "C" void Joint_Sparse_Matrix_Multiply_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		int * ConSmp_Idx, int * ConFea_Idx, float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * Weight, float * Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
{
	cuda_Joint_Sparse_Matrix_Multiply_INTEX(Src_Input, SrcSize, Tgt_Input, TgtSize,
			Src_Match_Idx, Tgt_Match_Idx, Match_Score, MatchSize,
			ConSmp_Idx, ConFea_Idx, ConFea_Value, SrcDimension, TgtDimension, ConDimension, IsConScore,
			Weight, Output, OutputDimension, IsSrc, IsTgt, IsSim);
}

//#ifdef WIN32
//DLLEXP void __stdcall Matrix_Add_Sigmoid(float * gpu_floats_a, float * gpu_floats_b, int m, int n)
//#else
extern "C" void Matrix_Add_Sigmoid(float * gpu_floats_a, float * gpu_floats_b, int m, int n)
//#endif
{
	cuda_Matrix_Add_Sigmoid(gpu_floats_a, gpu_floats_b, m, n);
}

extern "C" void SparseForward(int * inputSmpIdx, int * inputItemIdx, int batchSize, float * weight, float * output, int inputDim, int outputDim)
{
	cuda_SparseForward(inputSmpIdx, inputItemIdx, batchSize, weight, output, inputDim, outputDim);
}

extern "C" void SparseBackward(int * inputSmpIdx, int * inputItemIdx, int batchSize, int inputItemNum,
		float * deriv, int inputDim, int outputDim, float * output, float lr)
{
	cuda_SparseBackward(inputSmpIdx, inputItemIdx, batchSize, inputItemNum, deriv, inputDim, outputDim, output, lr);
}

#ifdef WIN32
DLLEXP void __stdcall SparseL0(int * outputSmpIdx, int * outputItemIdx, int batchSize, int itemNum, float * L0, float * output, int inputDim)
#else
extern "C" void SparseL0(int * outputSmpIdx, int * outputItemIdx, int batchSize, int itemNum, float * L0, float * output, int inputDim)
#endif
{
	cuda_SparseL0(outputSmpIdx, outputItemIdx, batchSize, itemNum, L0, output, inputDim);
}



//#ifdef WIN32
//DLLEXP void __stdcall SparseL1(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx, int * outputItemMapSmp, int batchSize, int inputItemNum, int outputItemNum,
//#else
//#endif

extern "C" void SparseL1(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx, int * outputItemMapSmp, int batchSize, int inputItemNum, int outputItemNum,
		float * L1, float * output, int inputDim)
{
	cuda_SparseL1(inputSmpIdx, inputItemIdx, outputSmpIdx, outputItemIdx, outputItemMapSmp, batchSize, inputItemNum, outputItemNum, L1, output, inputDim);
}


//#ifdef WIN32
//DLLEXP void __stdcall SparseLk(float * hidden, int hiddenDim, int * outputSmpIdx, int * outputItemIdx, int batchSize, int outputItemNum, int * outputItemMapSmp,
//#else
//#endif

extern "C" void SparseLk(float * hidden, int hiddenDim, int * outputSmpIdx, int * outputItemIdx, int batchSize, int outputItemNum, int * outputItemMapSmp,
		float * Lk, float * output)
{
	cuda_SparseLk(hidden, hiddenDim, outputSmpIdx, outputItemIdx, batchSize, outputItemNum, outputItemMapSmp, Lk, output);
}


#ifdef WIN32
DLLEXP void __stdcall Matrix_Multipy_Weight(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse, float low_wei)
#else
extern "C" void Matrix_Multipy_Weight(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse, float low_wei)
#endif
{
	cuda_Matrix_Multipy_Weight(delta, weight, delta_low, batchsize, m, n, inverse, low_wei);
}




//#ifdef WIN32
//DLLEXP void __stdcall Sparse_Matrix_Multiply_INTEX_Weight(int * outputSmpIndex, int * outputItemIndex, float * outputDeriv, int batchsize,
//#else
//#endif
extern "C" void Sparse_Matrix_Multiply_INTEX_Weight(int * outputSmpIndex, int * outputItemIndex, float * outputDeriv, int batchsize,
		int outputItemNum, float * weight, float * hidden_deriv, int hiddenDim, float wei)
{
	cuda_Sparse_Matrix_Multiply_INTEX_Weight(outputSmpIndex, outputItemIndex, outputDeriv,
			batchsize, outputItemNum, weight, hidden_deriv, hiddenDim, wei);
}


#ifdef WIN32
DLLEXP void __stdcall SparseL0Update(int * outputSmpIdx, int * outputItemIdx, float * outputScore, int batchSize, int outputItemNum, float * L0, int inputDim, float lr)
#else
extern "C" void SparseL0Update(int * outputSmpIdx, int * outputItemIdx, float * outputScore, int batchSize, int outputItemNum, float * L0, int inputDim, float lr)
#endif
{
	cuda_SparseL0Update(outputSmpIdx, outputItemIdx, outputScore, batchSize, outputItemNum, L0, inputDim, lr);
}


//#ifdef WIN32
//DLLEXP void __stdcall SparseL1Update(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx,
//#else
//#endif

extern "C" void SparseL1Update(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx,
		int * outputItemMapSmp, float * outputScore, int batchSize, int inputItemNum, int outputItemNum, float * L1, int inputDim, float lr)
{
	cuda_SparseL1Update(inputSmpIdx, inputItemIdx, outputSmpIdx, outputItemIdx, outputItemMapSmp, outputScore, batchSize, inputItemNum, outputItemNum, L1, inputDim, lr);
}

#ifdef WIN32
DLLEXP void __stdcall SparseL0Full(int batchSize, float * L0, float * output, int inputDim)
#else
extern "C" void SparseL0Full(int batchSize, float * L0, float * output, int inputDim)
#endif
{
	cuda_SparseL0Full(batchSize, L0, output, inputDim);
}

#ifdef WIN32
DLLEXP void __stdcall SparseL1Full(int batchSize, float * L1, int * inputSmpIdx, int * inputItemIdx, float * outputScore, int inputDim)
#else
extern "C" void SparseL1Full(int batchSize, float * L1, int * inputSmpIdx, int * inputItemIdx, float * outputScore, int inputDim)
#endif
{
	cuda_SparseL1Full(batchSize, L1, inputSmpIdx, inputItemIdx, outputScore, inputDim);
}

#ifdef WIN32
DLLEXP void __stdcall SparseLkFull(float * hidden, int hiddenDim, int batchSize, float * Lk, float * outputScore, int inputDim)
#else
extern "C" void SparseLkFull(float * hidden, int hiddenDim, int batchSize, float * Lk, float * outputScore, int inputDim)
#endif
{
	cuda_SparseLkFull(hidden, hiddenDim, batchSize, Lk, outputScore, inputDim);
}

//#ifdef WIN32
//DLLEXP void __stdcall SparseLkUpdate(int * outputSmpIdx, int * outputItemIdx, int * outputMapSample, float * outputDeriv, int batchSize,
//#else
//#endif
extern "C" void SparseLkUpdate(int * outputSmpIdx, int * outputItemIdx, int * outputMapSample, float * outputDeriv, int batchSize,
		int outputItemNum, float * hidden, float * Lk, int hiddenDim, float lr)
{
	cuda_SparseLkUpdate(outputSmpIdx, outputItemIdx, outputMapSample, outputDeriv, batchSize, outputItemNum, hidden, Lk, hiddenDim, lr);
}







#ifdef WIN32
DLLEXP void __stdcall Ada_Gradient(float * ada, float * grad, int m)
#else
extern "C" void Ada_Gradient(float * ada, float * grad, int m)
#endif
{
	cuda_Ada_Gradient(ada, grad, m);
}

//#ifdef WIN32
//DLLEXP void __stdcall Joint_Backward_Sim(float * outputDeriv, int MatchSize, int outputDim,
//#else
//#endif
extern "C" void Joint_Backward_Sim(float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * simDeriv)
{
	cuda_Joint_Backward_Sim(outputDeriv, MatchSize, outputDim, Weight, simDeriv);
}

#ifdef WIN32
DLLEXP void __stdcall Square_Matrix(float * a, int batchSize, int dim, float * aSquare)
#else
extern "C" void Square_Matrix(float * a, int batchSize, int dim, float * aSquare)
#endif
{
	cuda_Square_Matrix(a, batchSize, dim, aSquare);
}


//#ifdef WIN32
//DLLEXP void __stdcall Joint_Backward_Src_Tgt(
//#else
//#endif
extern "C" void Joint_Backward_Src_Tgt(
		float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * SimDeriv, 
		int * matchSrc, int * matchTgt,
		float * Src_Input, int SrcSize, int SrcDim,
		float * Tgt_Input, int TgtSize, int TgtDim,
		float * Sim, float * aa, float * bb, 
		int IsSrc, int IsTgt, int IsSim,
		int * SrcMatchIndex, int * SrcMatchElement,
		int * TgtMatchIndex, int * TgtMatchElement,
		float * Src_Deriv, float * Tgt_Deriv, float eps, int SimType)
{
	cuda_Joint_Backward_Src_Tgt(
			outputDeriv, MatchSize,outputDim, 
			Weight, SimDeriv,
			matchSrc, matchTgt,
			Src_Input, SrcSize, SrcDim,
			Tgt_Input, TgtSize, TgtDim,
			Sim, aa, bb,
			IsSrc, IsTgt, IsSim,
			SrcMatchIndex, SrcMatchElement,
			TgtMatchIndex, TgtMatchElement,
			Src_Deriv, Tgt_Deriv, eps, SimType);
}

//#ifdef WIN32
//DLLEXP void __stdcall Joint_Sparse_Matrix_Product_INTEX(
//#else
//#endif

extern "C" void Joint_Sparse_Matrix_Product_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		int * ConSmp_Idx, int * ConFea_Idx, float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * WeightUpdate, float * OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr)
{
	cuda_Joint_Sparse_Matrix_Product_INTEX(Src_Input, SrcSize, Tgt_Input, TgtSize,
			Src_Match_Idx, Tgt_Match_Idx, Match_Score, MatchSize,
			ConSmp_Idx, ConFea_Idx, ConFea_Value, SrcDimension, TgtDimension, ConDimension, IsConScore,
			WeightUpdate, OutputDeriv, OutputDimension, IsSrc, IsTgt, IsSim, lr);
}

//#ifdef WIN32
//DLLEXP void __stdcall Inner_Product_Matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
//#else
//#endif
extern "C" void Inner_Product_Matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float eps)
{
	cuda_Inner_Product_Matching(a, b, c, matchIdxA, matchIdxB, aSize, bSize, matchSize, dimension, eps);
}

// c = alpha * c + a \inp b
extern "C" void Inner_Product_v1(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float alpha, float beta)
{
	cuda_Inner_Product_v1(a, b, c, matchIdxA, matchIdxB, aSize, bSize, matchSize, dimension, alpha, beta);
}



//#ifdef WIN32
//DLLEXP void __stdcall Sparse_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Fea_Index,
//#else
//#endif
extern "C" void Sparse_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Fea_Index,
		float * Fea_Value, int elementsize, float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight)
{
	cuda_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(Smp_Index, batchsize, Fea_Index, Fea_Value, elementsize,
			mul_weight, output, Feature_dimension, output_dimension, weight);
}

//#ifdef WIN32
//DLLEXP void __stdcall Joint_Dense_Matrix_Multiply_INTEX(
//#else
//#endif
extern "C" void Joint_Dense_Matrix_Multiply_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * Weight, float * Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim)
{
	cuda_Joint_Dense_Matrix_Multiply_INTEX(Src_Input, SrcSize, Tgt_Input, TgtSize,
			Src_Match_Idx, Tgt_Match_Idx, Match_Score, MatchSize,
			ConFea_Value, 
			SrcDimension, TgtDimension, ConDimension, IsConScore,
			Weight, Output, OutputDimension, IsSrc, IsTgt, IsSim);
}

//#ifdef WIN32
//DLLEXP void __stdcall Joint_Dense_Matrix_Product_INTEX(
//#else
//#endif
extern "C" void Joint_Dense_Matrix_Product_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * WeightUpdate, float * OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr)
{
	cuda_Joint_Dense_Matrix_Product_INTEX(Src_Input, SrcSize, Tgt_Input, TgtSize,
			Src_Match_Idx, Tgt_Match_Idx, Match_Score, MatchSize,
			ConFea_Value, 
			SrcDimension, TgtDimension, ConDimension, IsConScore,
			WeightUpdate, OutputDeriv, OutputDimension, IsSrc, IsTgt, IsSim, lr);
}

#ifdef WIN32
DLLEXP void __stdcall RegressionLoss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize)
#else
extern "C" void RegressionLoss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize)
#endif
{
	cuda_RegressionLoss(outputScore, outputLabel, outputDeriv, outputSize);
}

//#ifdef WIN32
//DLLEXP void __stdcall PairwiseRankingLoss(
//#else
//#endif

extern "C" void PairwiseRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison)
{
	cuda_PairwiseRankingLoss(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison);
}

//#ifdef WIN32
//DLLEXP void __stdcall ListwiseRankingLoss(
//#else
//#endif

extern "C" void ListwiseRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	cuda_ListwiseRankingLoss(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison, gamma);
}


//#ifdef WIN32
//DLLEXP void __stdcall LambdaRankingLoss(
//#else
//#endif

extern "C" void LambdaRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, int K)
{
	cuda_LambdaRankingLoss(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss, 
			outputScore, outputLabel, outputDeriv, outputSize, eplison, K);
}

//#ifdef WIN32
//DLLEXP void __stdcall BayesianRatingLoss(
//#else
//#endif

extern "C" void BayesianRatingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	cuda_BayesianRatingLoss(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison, gamma);
}

#ifdef WIN32
DLLEXP void __stdcall BinomialSampling(float* prob, int batchSize, int Dim, float * sample, int randomSeed)
#else
extern "C" void BinomialSampling(float* prob, int batchSize, int Dim, float * sample, int randomSeed)
#endif
{
	cuda_BinomialSampling(prob, batchSize, Dim, sample, randomSeed);
}

#ifdef WIN32
DLLEXP void __stdcall Sparse_Matrix_Aggragate_Weight(int * smpIdx, int * feaIdx,  float * feaValue, float * b, int batchsize, int m, float weight)
#else
extern "C" void Sparse_Matrix_Aggragate_Weight(int * smpIdx, int * feaIdx,  float * feaValue, float * b, int batchsize, int m, float weight)
#endif
{
	cuda_Sparse_Matrix_Aggragate_Weight(smpIdx, feaIdx, feaValue, b, batchsize, m, weight);
}


#ifdef WIN32
DLLEXP void __stdcall ReconstructionLoss(float * outputScore, int * smpIdx, int * feaIdx, float * feaValue, float * loss, int batchSize, int dimension, float eps)
#else
extern "C" void ReconstructionLoss(float * outputScore, int * smpIdx, int * feaIdx, float * feaValue, float * loss, int batchSize, int dimension, float eps)
#endif
{
	cuda_ReconstructionLoss(outputScore, smpIdx, feaIdx, feaValue, loss, batchSize, dimension, eps);
}

//#ifdef WIN32
//DLLEXP void __stdcall SparseUpdateRBM(int * outputSmpIdx, int * outputItemIdx, float * outputItemLabel, int batchSize, float * weight, 
//#else
//#endif

extern "C" void SparseUpdateRBM(int * outputSmpIdx, int * outputItemIdx, float * outputItemLabel, int batchSize, float * weight, 
		float * output, float * bias_v, int inputDim, int outputDim,
		int * inputSmpIdx, int * inputItemIdx,
		float * grad_bias_v, float * grad_bias_h, float * grad_vh, float vStep, float hStep, float vhStep,
		float * loss, float eps)
{
	cuda_SparseUpdateRBM(outputSmpIdx, outputItemIdx, outputItemLabel, batchSize, weight,
			output, bias_v, inputDim, outputDim, inputSmpIdx, inputItemIdx,
			grad_bias_v, grad_bias_h, grad_vh, vStep, hStep, vhStep, loss, eps);
}

//#ifdef WIN32
//DLLEXP void __stdcall SparsePredictRBM(float * outputScore, int batchSize, float * weight, 
//#else
//#endif
extern "C" void SparsePredictRBM(float * outputScore, int batchSize, float * weight, 
		float * output,  float * bias_v, int inputDim, int outputDim)
{
	cuda_SparsePredictRBM(outputScore, batchSize, weight, output, bias_v, inputDim, outputDim);
}


//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Dense_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Margin, 
//#else
//#endif
extern "C" void Convolution_Dense_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Margin, 
		int Seg_size, float * Fea_Value, int Feature_dimension, 
		float * con_weight, float * output,  int output_dimension, int win_size)
{
	cuda_Convolution_Dense_Matrix_Multiply_INTEX(Smp_Index, batchsize, Seg_Margin, Seg_size, Fea_Value, Feature_dimension, con_weight, output,
			output_dimension, win_size);
}

//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Dense_Matrix_Product_INTEX_Weight(float * deriv, int * maxpooling_index, int * SegMargin_Index, int seg_size, int win_size,
//#else
//#endif

extern "C" void Convolution_Dense_Matrix_Product_INTEX_Weight(float * deriv, int * maxpooling_index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
{
	cuda_Convolution_Dense_Matrix_Product_INTEX_Weight(deriv, maxpooling_index, SegMargin_Index, seg_size, win_size, batchsize, output_dimension, 
			Fea_Value, grad, Feature_Dimension, learnRate);
}


//#ifdef WIN32
//DLLEXP void __stdcall Conv_Matrix_Multipy(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
//#else
//#endif
extern "C" void Conv_Matrix_Multipy(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
		int * sentMargin, int * layerPoolingIndex, int neuralOut, int neuralIn, int winSize)
{
	cuda_Conv_Matrix_Multipy(upperDeriv, con_weight, deriv, batchSize, sentSize, sentMargin, layerPoolingIndex, neuralOut, neuralIn, winSize);
}

//#ifdef WIN32
//DLLEXP void __stdcall ImageConvolution(float * inputImage, int batchSize, int width, int height, int depth, 
//#else
//#endif
extern "C" void ImageConvolution(float * inputImage, int batchSize, int width, int height, int depth, 
		float * filter, int sx, int c, int output, int pad, int stride,
		float * poolingOutput, int poolingWidth, int poolingHeight)
{
	cuda_ImageConvolution(inputImage, batchSize, width, height, depth, filter, sx, c, output, pad, stride, poolingOutput, poolingWidth, poolingHeight);
}

//#ifdef WIN32
//DLLEXP void __stdcall MaxImagePooling(float * inputImage, int batchSize, int width, int height, int depth,
//#else
//#endif
extern "C" void MaxImagePooling(float * inputImage, int batchSize, int width, int height, int depth,
		int * maxpoolingIndex,
		int sx, int stride,
		float * output, int outputWidth, int outputHeight)
{
	cuda_MaxImagePooling(inputImage, batchSize, width, height, depth, maxpoolingIndex, sx, stride, output, outputWidth, outputHeight);
}

//#ifdef WIN32
//DLLEXP void __stdcall MaxoutPooling(float * input, int batchSize, int dim,
//#else
//#endif
extern "C" void MaxoutPooling(float * input, int batchSize, int dim,
	int * maxpoolingIndex, int maxoutDim, int outDim, float * output, int format)
{
	cuda_MaxoutPooling(input, batchSize, dim, maxpoolingIndex, maxoutDim, outDim, output, format);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivMaxoutPooling(float * input_deriv, int batchSize, int dim,
//#else
//#endif

extern "C" void DerivMaxoutPooling(float * input_deriv, int batchSize, int dim,
	int * maxpoolingIndex, int maxoutDim, int outDim, float * output_deriv)
{
	cuda_DerivMaxoutPooling(input_deriv, batchSize, dim, maxpoolingIndex, maxoutDim, outDim, output_deriv);
}




//#ifdef WIN32
//DLLEXP void __stdcall ImageConvMatrixMultipy(float * outputDeriv, int * maxpoolingIndex,
//#else
//#endif

extern "C" void ImageConvMatrixMultipy(float * outputDeriv, int * maxpoolingIndex,
		int batchSize, int outputWidth, int outputHeight, int outputDepth,
		int poolingSX, int poolingStride,
		float * filter, int sx, int c, int pad, int stride, 
		float * inputDeriv, int width, int height, int depth)
{
	cuda_ImageConvMatrixMultipy(outputDeriv, maxpoolingIndex, batchSize, outputWidth, outputHeight, outputDepth,
			poolingSX, poolingStride, filter, sx, c, pad, stride, inputDeriv, width, height, depth);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_Add_Image_FType(float * gpu_floats_a, float * gpu_floats_b, int batchSize, int width, int height, int depth, int af)
#else
extern "C" void Matrix_Add_Image_FType(float * gpu_floats_a, float * gpu_floats_b, int batchSize, int width, int height, int depth, int af)
#endif
{
	cuda_Matrix_Add_Image_FType(gpu_floats_a, gpu_floats_b, batchSize, width, height, depth, af);
}

#ifdef WIN32
DLLEXP void __stdcall Deriv_Image_FType(float * delta, float * layer_output, int batchSize, int width, int height, int depth, int af)
#else
extern "C" void Deriv_Image_FType(float * delta, float * layer_output, int batchSize, int width, int height, int depth, int af)
#endif
{
	cuda_Deriv_Image_FType(delta, layer_output, batchSize, width, height, depth, af);
}

//#ifdef WIN32
//DLLEXP void __stdcall Matrix_Image_Aggragate_Weight(float * a, float * b, int batchSize, int width, 
//#else
//#endif
extern "C" void Matrix_Image_Aggragate_Weight(float * a, float * b, int batchSize, int width, 
		int height, int depth, float weight)
{
	cuda_Matrix_Image_Aggragate_Weight(a, b, batchSize, width, height, depth, weight);
}

//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Image_Matrix_Product_INTEX_Weight(float * outputDeriv, int * maxpoolingIndex, 
//#else
//#endif

extern "C" void Convolution_Image_Matrix_Product_INTEX_Weight(float * outputDeriv, int * maxpoolingIndex, 
		int batchsize, int outputWidth, int outputHeight, int outputDepth,
		int poolingSX, int poolingStride, 
		float * grad, int sx, int c, int pad, int stride,
		float * input, int width, int height, int depth, float learnRate)
{
	cuda_Convolution_Image_Matrix_Product_INTEX_Weight(outputDeriv, maxpoolingIndex, batchsize, outputWidth, outputHeight,
			outputDepth, poolingSX, poolingStride, grad, sx, c, pad, stride, input, width, height, depth, learnRate);
}



//#ifdef WIN32
//DLLEXP void __stdcall RNNForwardPropagate_Dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix, 
//#else
//#endif

extern "C" void RNNForwardPropagate_Dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix, 
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias,  int af, float * seqOut)
{
	cuda_RNNForwardPropagate_Dense(smpIdx, batchSize, seqInput, seqSize, Imatrix, Wmatrix, lag, feaDim, hiddenDim, bias, af, seqOut);
}

//#ifdef WIN32
//DLLEXP void __stdcall RNNForwardPropagateSlot_Dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix, 
//#else
//#endif
extern "C" void RNNForwardPropagateSlot_Dense(int * smpIdx, int batchSize, float * seqInput, int seqSize, float *Imatrix, 
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias,  int af, float * seqOut, float * groundOut, int * sampleEnd)
{
	cuda_RNNForwardPropagateSlot_Dense(smpIdx, batchSize, seqInput, seqSize, Imatrix, Wmatrix, lag, feaDim, hiddenDim, bias, af, seqOut, groundOut, sampleEnd);
}

//#ifdef WIN32
//DLLEXP void __stdcall RNNForwardPropagateSlot(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix, 
//#else
//#endif
extern "C" void RNNForwardPropagateSlot(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, float *Imatrix, 
		float * Wmatrix, int lag, int feaDim, int hiddenDim, float * bias,  int af, float * seqOut, float * groundTruth, int * earlyStopIdx)
{
	cuda_RNNForwardPropagateSlot(smpIdx, batchSize, seqIdx, seqSize, feaIdx, feaValue, Imatrix, Wmatrix, lag, feaDim, hiddenDim,
			bias, af, seqOut, groundTruth, earlyStopIdx);
}

#ifdef WIN32
DLLEXP void __stdcall Calculate_MSE(float * label, float * score, int length, float * result, int resultIdx)
#else
extern "C" void Calculate_MSE(float * label, float * score, int length, float * result, int resultIdx)
#endif
{
	cuda_Calculate_MSE(label, score, length, result, resultIdx);
}

#ifdef WIN32
DLLEXP void __stdcall Tensor_Matrix_Multiply_Weight(float * input, float * weight, float * output, int inputRow, int inputCol, int outputCol, int inverse, float wei)
#else
extern "C" void Tensor_Matrix_Multiply_Weight(float * input, float * weight, float * output, int inputRow, int inputCol, int outputCol, int inverse, float wei)
#endif
{
	cuda_Tensor_Matrix_Multiply_Weight(input, weight, output, inputRow, inputCol, outputCol, inverse, wei);
}

#ifdef WIN32
DLLEXP void __stdcall Matrix_Tensor_Product_Weight(float * input, float * deriv, float * gradient, int batchsize, int inputDim, int outputDim, float weight)
#else
extern "C" void Matrix_Tensor_Product_Weight(float * input, float * deriv, float * gradient, int batchsize, int inputDim, int outputDim, float weight)
#endif
{
	cuda_Matrix_Tensor_Product_Weight(input, deriv, gradient, batchsize, inputDim, outputDim, weight);
}

#ifdef WIN32
DLLEXP void __stdcall RNNLabelOutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOut)
#else
extern "C" void RNNLabelOutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOut)
#endif
{
	cuda_RNNLabelOutput(smpIdx, batchSize, seqInput, seqSize, Wmatrix, lag, feaDim, hiddenDim, seqOut);
}

#ifdef WIN32
DLLEXP void __stdcall DeRNNLabelOutput(int * smpIdx, int batchSize, float * seqInputDeriv, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv)
#else
extern "C" void DeRNNLabelOutput(int * smpIdx, int batchSize, float * seqInputDeriv, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv)
#endif
{
	cuda_DeRNNLabelOutput(smpIdx, batchSize, seqInputDeriv, seqSize, Wmatrix, lag, feaDim, hiddenDim, seqOutDeriv);
}

#ifdef WIN32
DLLEXP void __stdcall UpdateRNNLabelOutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv, float wei)
#else
extern "C" void UpdateRNNLabelOutput(int * smpIdx, int batchSize, float * seqInput, int seqSize, float * Wmatrix, int lag, int feaDim, int hiddenDim, float * seqOutDeriv, float wei)
#endif
{
	cuda_UpdateRNNLabelOutput(smpIdx, batchSize, seqInput, seqSize, Wmatrix, lag, feaDim, hiddenDim, seqOutDeriv, wei);
}

#ifdef WIN32
DLLEXP void __stdcall Clip_Vector(float * gpu_floats_a, int m, float maxThreshold, float minThreshold)
#else
extern "C" void Clip_Vector(float * gpu_floats_a, int m, float maxThreshold, float minThreshold)
#endif
{
	cuda_Clip_Vector(gpu_floats_a, m, maxThreshold, minThreshold);
}

//#ifdef WIN32
//DLLEXP void __stdcall LogBayesianRatingLoss(
//#else
//#endif

extern "C" void LogBayesianRatingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma)
{
	Cuda_LogBayesianRatingLoss(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
			outputScore, outputLabel, outputDeriv, outputSize, eplison, gamma);
}

//#ifdef WIN32
//DLLEXP void __stdcall BayesianRatingProb(
//#else
//#endif
extern "C" void BayesianRatingProb(
	int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
	float * outputScore, float * outputLabel, float * outputProb, int outputSize, float eplison, float gamma)
{
	Cuda_BayesianRatingProb(src2MatchIdx, src2MatchElement, srcBatchSize, srcBatchLoss,
		outputScore, outputLabel, outputProb, outputSize, eplison, gamma);
}


//#ifdef WIN32
//DLLEXP void __stdcall LSTMForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
//#else
//#endif

extern "C" void LSTMForwardPropagate(int * smpIdx, int batchSize, int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
		float * h, //-> hiddenState,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	Cuda_LSTMForwardPropagate(smpIdx, batchSize, seqIdx, seqSize, feaIdx, feaValue, 
			h, gate_i, c_hat, gate_f, c, gate_o, cell, Wi, Ui, bi, Wc, Uc, bc, Wf, Uf, bf, Wo, Uo, Vo, bo);
}

//#ifdef WIN32
//DLLEXP void __stdcall LSTMForwardPropagateStep2(int * smpIdx, int batchSize, //int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
//#else
//#endif

extern "C" void LSTMForwardPropagateStep2(int * smpIdx, int batchSize, //int * seqIdx, int seqSize, int * feaIdx, float * feaValue, //->x;
		float * h, //-> hiddenState,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * output, //->output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	Cuda_LSTMForwardPropagateStep2(smpIdx, batchSize, 
			h, gate_i, c_hat, gate_f, c, output, cell, Wi, Ui, bi, Wc, Uc, bc, Wf, Uf, bf, Wo, Uo, Vo, bo);
}

//#ifdef WIN32
//DLLEXP void __stdcall LSTMForwardPropagateDense(int * smpIdx, int batchSize, float * seqInput, int seqSize, int inputDim,
//#else
extern "C" void LSTMForwardPropagateDense(int * smpIdx, int batchSize, float * seqInput, int seqSize, int inputDim,
//#endif
		float * h,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * output,
		int cell,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	Cuda_LSTMForwardPropagateDense(smpIdx, batchSize, seqInput, seqSize, inputDim, 
			h, gate_i, c_hat, gate_f, c, output, cell, Wi, Ui, bi, Wc, Uc, bc, Wf, Uf, bf, Wo, Uo, Vo, bo);
}

//#ifdef WIN32
//DLLEXP void __stdcall LSTMBackwardPropagate(int * smpIdx, int batchSize, int seqSize, int cell,
//#else
extern "C" void LSTMBackwardPropagate(int * smpIdx, int batchSize, int seqSize, int cell,
//#endif
		float * h,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,

		float * deriv_h,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * Wi, float * Ui, float * bi,
		float * Wc, float * Uc, float * bc,
		float * Wf, float * Uf, float * bf,
		float * Wo, float * Uo, float * Vo, float * bo)
{
	Cuda_LSTMBackwardPropagate(smpIdx, batchSize, seqSize, cell, 
			h, gate_i, c_hat, gate_f, c, gate_o, deriv_h, deriv_gate_i, deriv_cHat, deriv_gate_f, deriv_c, deriv_gate_o,
			Wi, Ui, bi, Wc, Uc, bc, Wf, Uf, bf, Wo, Uo, Vo, bo);
}

/***************************************/
#ifdef WIN32
DLLEXP void __stdcall Matrix_Normalize_By_Col(float *a, uint32_t col, uint32_t row)
#else
extern "C" void Matrix_Normalize_By_Col(float *a, uint32_t col, uint32_t row)
#endif
{
	cuda_Matrix_Normalize_By_Col(a, col, row);
}

//#ifdef WIN32
//DLLEXP void __stdcall Bp_Lda_Forward(
//#else
extern "C" void Bp_Lda_Forward(
//#endif
		float *phi,
		float *theta,
		float *b,
		float *U,
		float *T,
		float *d_TLayerValue,
		float *d_MaxColValue,
		float *d_SumColValue,
		float *iterT,
		float * output,
		float *matchLabelValue,
		float *d_Loss_Per_Epoch,
		uint32_t * matchIdx,
		uint32_t * featureIdx,
		float * featureValue,
		uint32_t elementSize,
		float eta,
		uint32_t useAdaptivenHidLayer,
		uint32_t batchsize,
		uint32_t inputDim,
		uint32_t hiddenDim,
		uint32_t outputDim,
		uint32_t layerDim,
		float *d_Phitheta,
		float *d_tempSparseMat,
		float *d_tempSparseMatTemp1,
		float *d_tempDenseMatTemp,
		float *d_NegGradTemp,
		float *d_LogThetaTemp,
		float *d_loss_pre,
		float *d_loss_post,
		float *d_sum,
		float *d_sumGradProj,
		uint32_t *d_flagWhileBreak, // batchsize
		uint32_t *d_whileBreakCount, // 1
		uint32_t *whileBreakCount, //1
		float gamma,
		uint32_t calculateSoftMax,
		uint32_t MaxLineSearchIter
		)
{
	cuda_Bp_Lda_Forward(
			phi,
			theta,
			b,
			U,
			T,
			d_TLayerValue,
			d_MaxColValue,
			d_SumColValue,
			iterT,
			output,
			matchLabelValue,
			d_Loss_Per_Epoch,
			matchIdx,
			featureIdx,
			featureValue,
			elementSize,
			eta,
			useAdaptivenHidLayer,
			batchsize,
			inputDim,
			hiddenDim,
			outputDim,
			layerDim,
			d_Phitheta,
			d_tempSparseMat,
			d_tempSparseMatTemp1,
			d_tempDenseMatTemp,
			d_NegGradTemp,
			d_LogThetaTemp,
			d_loss_pre,
			d_loss_post,
			d_sum,
			d_sumGradProj,
			d_flagWhileBreak, // batchsize
			d_whileBreakCount, // 1
			whileBreakCount, //1
			gamma,
			calculateSoftMax,
			MaxLineSearchIter
				);
}


//#ifdef WIN32
//DLLEXP void __stdcall Bp_Lda_Backward(
//#else
extern "C" void Bp_Lda_Backward(
//#endif
		float *output,
		float *matchLabelValue,
		float *theta,
		float *phi,
		float *b,
		float *U,
		float *T,
		float *iterT,
		float *grad_Q_U,
		float *grad_Q_Phi,
		uint32_t *matchIdx,
		uint32_t *featureIdx,
		float *featureValue,
		float gamma,
		float negativeBetaMinusOneOverTotalSamples,
		uint32_t batchsize,
		uint32_t inputDim,
		uint32_t hiddenDim,
		uint32_t outputDim,
		uint32_t layerDim,
		uint32_t elementSize,
		float *d_xi,
		float *d_tempDenseMat,
		float *d_temp_theta_xi,
		float *d_temp_theta_xi_b_T_OVER_theta_lm1_2,
		float *d_thetaRatio,
		float *d_sumGrad_Q_U, //outputdim * hiddenDim * batchsize
		float *d_temp_Xt_OVER_Phitheta, // elementSize
		float *d_tempSparseMatValue, // elementSize
		float *d_sumTempDenseMat, // batchsize * hiddendim
		float *d_tempSparseMat, // elementSize
		float *d_tempResBatch, // batchsize
		uint32_t calculateSoftMax
		)
{
	cuda_Bp_Lda_Backward(
			output,
			matchLabelValue,
			theta,
			phi,
			b,
			U,
			T,
			iterT,
			grad_Q_U,
			grad_Q_Phi,
			matchIdx,
			featureIdx,
			featureValue,
			gamma,
			negativeBetaMinusOneOverTotalSamples,
			batchsize,
			inputDim,
			hiddenDim,
			outputDim,
			layerDim,
			elementSize,
			d_xi,
			d_tempDenseMat,
			d_temp_theta_xi,
			d_temp_theta_xi_b_T_OVER_theta_lm1_2,
			d_thetaRatio,
			d_sumGrad_Q_U, //outputdim * hiddenDim * batchsize
			d_temp_Xt_OVER_Phitheta, // elementSize
			d_tempSparseMatValue, // elementSize
			d_sumTempDenseMat, // batchsize * hiddendim
			d_tempSparseMat, // elementSize
			d_tempResBatch, // batchsize
			calculateSoftMax
				);
}


//#ifdef WIN32
//DLLEXP void __stdcall Bp_Lda_Update(
//#else
extern "C" void Bp_Lda_Update(
//#endif
		float *grad_Q_U,
		float *grad_Q_Phi,
		float *U,
		float *phi,
		float *adaGradSum,
		float *d_sumAdaGradSum,
		float *d_sumTempColSum,
		float *muPhiSearch,
		float mu_U,
		float mu_Phi,
		uint32_t cntModelUpdate,
		uint32_t inputDim,
		uint32_t hiddenDim,
		uint32_t outputDim,
		float *d_update,
		float *d_tempColSum,
		float *d_tempColMax
		)
{
	cuda_Bp_Lda_Update(
			grad_Q_U,
			grad_Q_Phi,
			U,
			phi,
			adaGradSum,
			d_sumAdaGradSum,
			d_sumTempColSum,
			muPhiSearch,
			mu_U,
			mu_Phi,
			cntModelUpdate,
			inputDim,
			hiddenDim,
			outputDim,
			d_update,
			d_tempColSum,
			d_tempColMax
			);
}


//#ifdef WIN32
//DLLEXP void __stdcall L1Reg_CheckSign(float *src, float *dst, int len, int keepZero, int reverse)
//#else
extern "C" void L1Reg_CheckSign(float *src, float *dst, int len, int keepZero, int reverse)
//#endif
{
	Cuda_L1Reg_CheckSign(src, dst, len, keepZero, reverse);
}

//#ifdef WIN32
//DLLEXP void __stdcall L1Reg_CalcPseudoGradient(float* weight, float *grad, float *pseudoGradient, float cl1, float cl2, int len)
//#else
extern "C" void L1Reg_CalcPseudoGradient(float* weight, float *grad, float *pseudoGradient, float cl1, float cl2, int len)
//#endif
{
	Cuda_L1Reg_CalcPseudoGradient(weight, grad, pseudoGradient, cl1, cl2, len);
}

//#ifdef WIN32
//DLLEXP void __stdcall L1Norm(float* weight, float* norm, int len)
//#else
extern "C" void L1Norm(float* weight, float* norm, int len)
//#endif
{
	Cuda_L1Norm(weight, norm, len);
}

//#ifdef WIN32
//DLLEXP void __stdcall CountZero(float* weight, int* zCount, int len)
//#else
extern "C" void CountZero(float* weight, int* zCount, int len)
//#endif
{
	Cuda_CountZero(weight, zCount, len);
}

//#ifdef WIN32
//DLLEXP void __stdcall RandomNumber(float * value, int size)
//#else
extern "C" void RandomNumber(float * value, int size)
//#endif
{
	Cuda_RandomNumber(value, size);
}

//#ifdef WIN32
//DLLEXP void __stdcall Sparse_SoftMax(int * outputSmpIdx, int * outputItemIdx, float * outputScore, float * outputLabel, int batchSize,
//#else
extern "C" void Sparse_SoftMax(int * outputSmpIdx, int * outputItemIdx, float * outputScore, float * outputLabel, int batchSize,
//#endif
		int itemNum, float * deriv, float gamma, int isLog, float * loss)
{
	cuda_Sparse_SoftMax(outputSmpIdx, outputItemIdx, outputScore, outputLabel, batchSize, itemNum, deriv, gamma, isLog, loss);
}

//#ifdef WIN32
//DLLEXP void __stdcall UpdateSoftmaxEmbedding(float * targets, int targetNum, 
//#else
extern "C" void UpdateSoftmaxEmbedding(float * targets, int targetNum, 
//#endif
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds,
		float * randVar, int randLen, int randStart)
{
	Cuda_UpdateSoftmaxEmbedding(targets, targetNum, embedding, deriv, dim, vSpace, vBias, vocabSize, step, simi, alpha, negativeSample, gamma, negIds, randVar, randLen, randStart);
}

//#ifdef WIN32
//DLLEXP void __stdcall UpdateEmbedding(float * targets, int targetNum, 
//#else
extern "C" void UpdateEmbedding(float * targets, int targetNum, 
//#endif
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds,
		float * randVar, int randLen, int randStart)
{
	Cuda_UpdateEmbedding(targets, targetNum, embedding, deriv, dim, vSpace, vBias, vocabSize, step, simi, alpha, negativeSample, gamma, negIds, randVar, randLen, randStart);
}


//#ifdef WIN32
//DLLEXP void __stdcall UpdateBalanceEmbedding(float * targets, int targetNum, 
//#else
extern "C" void UpdateBalanceEmbedding(float * targets, int targetNum, 
//#endif
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds,
		float * randVar, int randLen, int randStart,
		int * sampleTable, int tableSize)
{
	Cuda_UpdateBalanceEmbedding(targets, targetNum, embedding, deriv, dim, vSpace, vBias, vocabSize, 
			step, simi, alpha, negativeSample, gamma, negIds, randVar, randLen, randStart, sampleTable, tableSize);
}

//#ifdef WIN32
//DLLEXP void __stdcall HierarcialSoftmaxEmbedding(float * targets, int targetNum, 
//#else
extern "C" void HierarcialSoftmaxEmbedding(float * targets, int targetNum, 
//#endif
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,
		float step,
		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classOutput,
		float * wordOutput,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb
		)
{
	Cuda_HierarcialSoftmaxEmbedding(targets, targetNum, embedding, deriv, dim, vSpace, vBias, vocabSize,
			cSpace, cBias, cSize, step, v2c, classIdx, wordIdx, wordClassIdx, gamma, classOutput, wordOutput, wordSegSize, targetProb);
}

//#ifdef WIN32
//DLLEXP void __stdcall HierarcicalProbability(float * targets, int targetNum, 
//#else
extern "C" void HierarcicalProbability(float * targets, int targetNum, 
//#endif
		float * embedding, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float * cSpace, float * cBias, int cSize,

		int * v2c, // word 2 class,
		int * classIdx, // class 2 word number
		int * wordIdx, // class 2 word index
		int * wordClassIdx, //word 2 class segment index.
		float gamma,
		float * classOutput,
		float * wordOutput,
		int wordSegSize, // word Max SegmentSize,
		float * targetProb
		)
{
	Cuda_HierarcicalProbability(targets, targetNum, embedding, dim, vSpace, vBias, vocabSize, cSpace, cBias, cSize, v2c, classIdx, wordIdx, 
			wordClassIdx, gamma, classOutput, wordOutput, wordSegSize, targetProb);
}

//#ifdef WIN32
//DLLEXP void __stdcall UpdateBinaryEmbedding_NegIds(float * targets, int targetNum, 
//#else
extern "C" void UpdateBinaryEmbedding_NegIds(float * targets, int targetNum, 
//#endif
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds)
{
	Cuda_UpdateBinaryEmbedding_NegIds(targets, targetNum, embedding, deriv, dim, vSpace, vBias, vocabSize,
			step, simi, alpha, negativeSample, gamma, negIds);
}


//#ifdef WIN32
//DLLEXP void __stdcall UpdateSoftmaxEmbedding_NegIds(float * targets, int targetNum, 
//#else
extern "C" void UpdateSoftmaxEmbedding_NegIds(float * targets, int targetNum, 
//#endif
		float * embedding, float * deriv, int dim,
		float * vSpace, float * vBias, int vocabSize,
		float step,	// step size for update vSpace.
		float * simi, // similarity for negatives.
		float * alpha,
		int negativeSample, float gamma, 
		int * negIds)
{
	Cuda_UpdateSoftmaxEmbedding_NegIds(targets, targetNum, embedding, deriv, dim, vSpace, vBias, vocabSize,
			step, simi, alpha, negativeSample, gamma, negIds);
}


//#ifdef WIN32
//DLLEXP void __stdcall Inner_Product_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps)
//#else
extern "C" void Inner_Product_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps)
//#endif
{
	Cuda_Inner_Product_Full(a, b, neg_list, c, nTrialplus1, batchsize, dimension, eps);
}

//#ifdef WIN32
//DLLEXP void __stdcall Cosine_Similarity_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps)
//#else
extern "C" void Cosine_Similarity_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps)
//#endif
{
	Cuda_Cosine_Similarity_Full(a, b, neg_list, c, nTrialplus1, batchsize, dimension, eps);
}

//#ifdef WIN32
//DLLEXP void __stdcall Deriv_CosineSimilarity_Matching(float * q, float * d, float *qSquare, float * dSquare, int dim,
//#else
extern "C" void Deriv_CosineSimilarity_Matching(float * q, float * d, float *qSquare, float * dSquare, int dim,
//#endif
		int * src2MatchIdx, int * src2MatchElement, int * tgt2MatchIdx, int * tgt2MatchElement, int * srcIdx, int * tgtIdx, int srcSize, int tgtSize, int matchSize,
		float * simi, float * derivSimi, float * dcq, float * dcd, float eps)
{
	Cuda_Deriv_CosineSimilarity_Matching(q, d, qSquare, dSquare, dim, src2MatchIdx, src2MatchElement, tgt2MatchIdx, tgt2MatchElement, srcIdx, tgtIdx, 
			srcSize, tgtSize, matchSize, simi, derivSimi, dcq, dcd, eps);
}

//#ifdef WIN32
//DLLEXP void __stdcall Deriv_InnerProduct_Matching(float * q, float * d, int dim,
//#else
extern "C" void Deriv_InnerProduct_Matching(float * q, float * d, int dim,
//#endif
		int * src2MatchIdx, int * src2MatchElement, int * tgt2MatchIdx, int * tgt2MatchElement, int * srcIdx, int * tgtIdx, int srcSize, int tgtSize, int matchSize,
		float * derivSimi, float * dcq, float * dcd, float eps)
{
	Cuda_Deriv_InnerProduct_Matching(q, d, dim, src2MatchIdx, src2MatchElement, tgt2MatchIdx, tgt2MatchElement, srcIdx, tgtIdx, srcSize, tgtSize, matchSize, 
			derivSimi, dcq, dcd, eps);
}

//#ifdef WIN32
//DLLEXP void __stdcall Calculate_SampleCrossEntropyDeriv(float * output, float * deriv, int dim, float * label, int batchsize, float gamma)
//#else
extern "C" void Calculate_SampleCrossEntropyDeriv(float * output, float * deriv, int dim, float * label, int batchsize, float gamma)
//#endif
{
	Cuda_Calculate_SampleCrossEntropyDeriv(output, deriv, dim, label, batchsize, gamma);
}


//#ifdef WIN32
//DLLEXP void __stdcall LSTMForwardPropagateBatchV2(int * InstanceIndex, int batchSize,  //->x;
//#else
extern "C" void LSTMForwardPropagateBatchV2(int * InstanceIndex, int batchSize,  //->x;
//#endif
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell,
		float * Ui,
		float * Uc,
		float * Uf,
		float * Uo, float * Vo,
		int * RecursiveIdx, int * RecursiveMatrix, int MaxLag)
{
	Cuda_LSTMForwardPropagateBatchV2(InstanceIndex, batchSize, o, gate_i, c_hat, gate_f, c, gate_o, tanhc, cell, Ui, Uc, Uf, Uo, Vo,
			RecursiveIdx, RecursiveMatrix, MaxLag);
}

//#ifdef WIN32
//DLLEXP void __stdcall LSTMBackwardPropagateBatchV2(int * InstanceIndex, int batchSize, int seqSize, int cell,
//#else
extern "C" void LSTMBackwardPropagateBatchV2(int * InstanceIndex, int batchSize, int seqSize, int cell,
//#endif
		float * o,
		float * gate_i,
		float * c_hat,
		float * gate_f,
		float * c,
		float * gate_o,
		float * tanhc,

		float * deriv_o,
		float * deriv_gate_i,
		float * deriv_cHat,
		float * deriv_gate_f,
		float * deriv_c,
		float * deriv_gate_o,
		float * deriv_tanhc,

		float * Ui, float * Uc, float * Uf, float * Uo, float * Vo,
		int * RecursiveIdx, int * RecursiveMatrix, int MaxLag)
{
	Cuda_LSTMBackwardPropagateBatchV2(InstanceIndex, batchSize, seqSize, cell, o, gate_i, c_hat, gate_f, c, gate_o, tanhc, deriv_o, deriv_gate_i,
			deriv_cHat, deriv_gate_f, deriv_c, deriv_gate_o, deriv_tanhc, Ui, Uc, Uf, Uo, Vo, RecursiveIdx, RecursiveMatrix, MaxLag);
}
/***************************************/

//#ifdef WIN32
//DLLEXP void __stdcall LSTMForwardPropagateBatchOpt(int * InstanceIndex, int batchSize,  //->x;
//#else
extern "C" void LSTMForwardPropagateBatchOpt(int * InstanceIndex, int batchSize,  //->x;
//#endif
		float * o, //-> output,
		float * gate_i, //->gate i,
		float * c_hat, //->candidate memory,
		float * gate_f, //->forget gate,
		float * c, //->memory,
		float * gate_o, //->gate o,
		float * tanhc, //->tanh memory
		int cell, float * Ui, float * Uc, float * Uf, float * Uo, float * Vo)
{
	Cuda_LSTMForwardPropagateBatchOpt(InstanceIndex, batchSize, o, gate_i, c_hat, gate_f, c, gate_o, tanhc, cell, Ui, Uc, Uf, Uo, Vo);
}

//#ifdef WIN32
//DLLEXP void __stdcall LSTMBackwardPropagateBatchOpt(int *InstanceIndex, int batchSize, int seqSize, int cell,
//#else
extern "C" void LSTMBackwardPropagateBatchOpt(int *InstanceIndex, int batchSize, int seqSize, int cell,
//#endif
		float *o,
		float *gate_i,
		float *c_hat,
		float *gate_f,
		float *c,
		float *gate_o,
		float *tanhc,

		float *deriv_o,
		float *deriv_gate_i,
		float *deriv_cHat,
		float *deriv_gate_f,
		float *deriv_c,
		float *deriv_gate_o,
		float *deriv_tanhc,
		float *Ui, float *Uc, float *Uf, float *Uo, float *Vo)
{
	Cuda_LSTMBackwardPropagateBatchOpt(InstanceIndex, batchSize, seqSize, cell, o, gate_i, c_hat, gate_f, c,
			gate_o, tanhc, deriv_o, deriv_gate_i, deriv_cHat, deriv_gate_f, deriv_c, deriv_gate_o, deriv_tanhc, Ui, Uc, Uf, Uo, Vo);
}

//#ifdef WIN32
//DLLEXP void __stdcall GRUForwardPropagateBatch(int * InstanceIndex, int batchSize, int Dim, //->x;
//#else
extern "C" void GRUForwardPropagateBatch(int * InstanceIndex, int batchSize, int Dim, //->x;
//#endif
		float * H, //->hidden,
		float * GateR, //->reset gate,
		float * GateZ, //->update gate,
		float * HHat, //->chat
		float * RestH,
		float * Ur,
		float * Uz,
		float * Uh)
{
	Cuda_GRUForwardPropagateBatch(InstanceIndex, batchSize, Dim, H, GateR, GateZ, HHat, RestH, Ur, Uz, Uh);
}

//#ifdef WIN32
//DLLEXP void __stdcall GRUBackwardPropagateBatch(
//#else
extern "C" void GRUBackwardPropagateBatch(
//#endif
		int *InstanceIndex, int batchSize, int Dim,//->x
		float *H, //->hidden
		float *GateR, //->reset gate,
		float *GateZ, //->update gate,
		float *HHat, //->hat
		float *RestH, //H @ R
		float *DerivH,
		float *DerivGateR,
		float *DerivGateZ,
		float *DerivHHat,
		float *DerivRestH,
		float * Ur,
		float * Uz,
		float * Uh)
{
	Cuda_GRUBackwardPropagateBatch(InstanceIndex, batchSize, Dim, H, GateR, GateZ, HHat, RestH, DerivH, DerivGateR, DerivGateZ, DerivHHat, DerivRestH, Ur, Uz, Uh);
}

//#ifdef WIN32
//DLLEXP float  __stdcall L2Norm(float *x, int size)
//#else
extern "C" float  L2Norm(float *x, int size)
//#endif
{
	return Cuda_L2Norm(x, size);
}

//#ifdef WIN32
//DLLEXP void  __stdcall MatrixL1Norm(float *x, int dim, int batchSize, float * l1Score)
//#else
extern "C" void MatrixL1Norm(float *x, int dim, int batchSize, float * l1Score)
//#endif
{
	Cuda_MatrixL1Norm(x, dim, batchSize, l1Score);
}

//#ifdef WIN32
//DLLEXP void __stdcall MatrixL2Norm(float *x, int dim, int batchSize, float * l2Score)
//#else
extern "C" void MatrixL2Norm(float *x, int dim, int batchSize, float * l2Score)
//#endif
{
	Cuda_MatrixL2Norm(x, dim, batchSize, l2Score);
}

//#ifdef WIN32
//DLLEXP void  __stdcall DerivMatrixL1Norm(float *x, int dim, int batchSize, float * l1Score, float * l1ScoreDeriv, float * xDeriv)
//#else
extern "C" void DerivMatrixL1Norm(float *x, int dim, int batchSize, float * l1Score, float * l1ScoreDeriv, float * xDeriv)
//#endif
{
	Cuda_DerivMatrixL1Norm(x, dim, batchSize, l1Score, l1ScoreDeriv, xDeriv);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivMatrixL2Norm(float *x, int dim, int batchSize, float * l2Score, float * l2ScoreDeriv, float * xDeriv)
//#else
extern "C" void DerivMatrixL2Norm(float *x, int dim, int batchSize, float * l2Score, float * l2ScoreDeriv, float * xDeriv)
//#endif
{
	Cuda_DerivMatrixL2Norm(x, dim, batchSize, l2Score, l2ScoreDeriv, xDeriv);
}




//#ifdef WIN32
//DLLEXP void __stdcall RMSPropGradient(float * ada, float * grad, float gamma, float epsilon, int m)
//#else
extern "C" void RMSPropGradient(float * ada, float * grad, float gamma, float epsilon, int m)
//#endif
{
	Cuda_RMSPropGradient(ada, grad, gamma, epsilon, m);
}

//#ifdef WIN32
//DLLEXP void __stdcall RMSPropV2_Gradient(float * ada, float * g, float * grad, float gamma, float epsilon, int m)
//#else
extern "C" void RMSPropV2_Gradient(float * ada, float * g, float * grad, float gamma, float epsilon, int m)
//#endif
{
	Cuda_RMSPropV2_Gradient(ada, g, grad, gamma, epsilon, m);
}


//#ifdef WIN32
//DLLEXP void __stdcall VectorSquareAdd(float *Z, float *X, float *Y, float wz, float wx, float wy, int m)
//#else
extern "C" void VectorSquareAdd(float *Z, float *X, float *Y, float wz, float wx, float wy, int m)
//#endif
{
	Cuda_VectorSquareAdd(Z, X, Y, wz, wx, wy, m);
}

//#ifdef WIN32
//DLLEXP void __stdcall AdaMGradient(float *M, float *V, float *G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
//#else
extern "C" void AdaMGradient(float *M, float *V, float *G, float alpha, float beta1, float beta2, float epsilon, int t, int size)
//#endif
{
	Cuda_AdaMGradient(M, V, G, alpha, beta1, beta2, epsilon, t, size);
}

//#ifdef WIN32
//DLLEXP void __stdcall AdaDeltaGradient(float *deltaX, float *AccumGrad, float *AccumUpdate, float *Grad, float epsilon, int size)
//#else
extern "C" void AdaDeltaGradient(float *deltaX, float *AccumGrad, float *AccumUpdate, float *Grad, float epsilon, int size)
//#endif
{
	Cuda_AdaDeltaGradient(deltaX, AccumGrad, AccumUpdate, Grad, epsilon, size);
}

//#ifdef WIN32
//DLLEXP void __stdcall AdaMax(float *V, float *M, float *G, float alpha, float beta, float epsilon, int m)
//#else
extern "C" void AdaMax(float *V, float *M, float *G, float alpha, float beta, float epsilon, int m)
//#endif
{
	Cuda_AdaMax(V, M, G, alpha, beta, epsilon, m);
}

//#ifdef WIN32
//DLLEXP void __stdcall convolution_sparse_matrix_gradient(float * deriv, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
//#else
extern "C" void convolution_sparse_matrix_gradient(float * deriv, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
//#endif
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate)
{
	Cuda_convolution_sparse_matrix_gradient(deriv, Seg_Index, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, Fea_Index, Fea_Value, grad, Feature_Dimension, learnRate);
}

//#ifdef WIN32
//DLLEXP void __stdcall convolution_dense_matrix_gradient(float * deriv, int * SegMargin_Index, int seg_size, int win_size,
//#else
extern "C" void convolution_dense_matrix_gradient(float * deriv, int * SegMargin_Index, int seg_size, int win_size,
//#endif
		int batchsize, int output_dimension, float * input, float * grad, int Feature_Dimension, float learnRate)
{
	Cuda_convolution_dense_matrix_gradient(deriv, SegMargin_Index, seg_size, win_size,
			batchsize, output_dimension, input, grad, Feature_Dimension, learnRate);
}

//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Matrix_Transpose_Multiply(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
//#else
extern "C" void Convolution_Matrix_Transpose_Multiply(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
//#endif
		int * sentMargin, int neuralOut, int neuralIn, int winSize)
{
	Cuda_Convolution_Matrix_Transpose_Multiply(upperDeriv, con_weight, deriv, batchSize, sentSize, sentMargin, neuralOut, neuralIn, winSize);
}

/// c_column_i = b_columnm_i + a
//#ifdef WIN32
//DLLEXP void __stdcall MatrixTran_AddVector(float * gpu_floats_a, float * gpu_floats_b, float * gpu_floats_c, int batchsize, int dimension, float weight)
//#else
extern "C" void MatrixTran_AddVector(float * gpu_floats_a, float * gpu_floats_b, float * gpu_floats_c, int batchsize, int dimension, float weight)
//#endif
{
	Cuda_MatrixTran_AddVector(gpu_floats_a, gpu_floats_b, gpu_floats_c, batchsize, dimension, weight);
}

//#ifdef WIN32
//DLLEXP void __stdcall ImageDenseConvMatrixMultipy(float * outputDeriv, //int * maxpoolingIndex,
//#else
extern "C" void ImageDenseConvMatrixMultipy(float * outputDeriv, //int * maxpoolingIndex,
//#endif
		int batchSize, int outputWidth, int outputHeight, int outputDepth,
		//int poolingSX, int poolingStride,
		float * filter, int sx, int c, int pad, int stride,
		float * inputDeriv, int width, int height, int depth)
{
	cuda_ImageDenseConvMatrixMultipy(outputDeriv, batchSize, outputWidth, outputHeight, outputDepth,
			filter, sx, c, pad, stride, inputDeriv, width, height, depth);
}

//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Dense_Image_Matrix_Product_INTEX_Weight(float * outputDeriv,
//#else
extern "C" void Convolution_Dense_Image_Matrix_Product_INTEX_Weight(float * outputDeriv,
//#endif
		int batchsize, int outputWidth, int outputHeight, int outputDepth,
		//int poolingSX, int poolingStride,
		float * grad, int sx, int c, int pad, int stride,
		float * input, int width, int height, int depth, float learnRate)
{
	cuda_Convolution_Dense_Image_Matrix_Product_INTEX_Weight(outputDeriv, batchsize, outputWidth, outputHeight, outputDepth,
			grad, sx, c, pad, stride, input, width, height, depth, learnRate);
}

//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Index_Matrix_Multiply(int batchsize, int * Seg_Index, int * Seg_Margin,
//#else
extern "C" void Convolution_Index_Matrix_Multiply(int batchsize, int * Seg_Index, int * Seg_Margin,
//#endif
		int seg_size, int * Fea_Index, int elementsize, float * con_weight, float * output, int Feature_dimension, int output_dimension, int win_size)
{
	cuda_Convolution_Index_Matrix_Multiply(batchsize, Seg_Index, Seg_Margin, seg_size, Fea_Index, elementsize, con_weight, output, Feature_dimension, output_dimension, win_size);
}

//#ifdef WIN32
//DLLEXP void __stdcall Convolution_Index_Matrix_Product_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
//#else
extern "C" void Convolution_Index_Matrix_Product_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * grad, int Feature_Dimension, float learnRate)
{
	cuda_Convolution_Index_Matrix_Product_Weight(deriv, maxpooling_index, Seg_Index, SegMargin_Index, seg_size, win_size, batchsize, output_dimension, Fea_Index, grad, Feature_Dimension, learnRate);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Cuda DLL Functions: Begin.

///////////////////////////////////////////////////////////////////////////// CuDNN Handle Free Functions. 
class DropoutState
{
public:
	size_t state_size;
	void * state;

	size_t reserve_size;
	void * reserve;

	cudnnDropoutDescriptor_t dropout_descriptor;
	cudnnTensorDescriptor_t descr;
	
	DropoutState(cudnnHandle_t handle, int n, int c, int h, int w, float drop)
	{
		cudnnDropoutGetStatesSize(handle, &state_size);
		cudaMalloc(&state, state_size);

		cudnnCreateTensorDescriptor(&descr);
		cudnnSetTensor4dDescriptor(descr, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, n, c, h, w);

		cudnnDropoutGetReserveSpaceSize(descr, &reserve_size);
		cudaMalloc(&reserve, reserve_size);

		cudnnCreateDropoutDescriptor(&dropout_descriptor);
		cudnnSetDropoutDescriptor(dropout_descriptor, handle, drop, state, state_size, time(NULL));
	}

	~DropoutState()
	{
		cudaFree(state);
		cudaFree(reserve);

		cudnnDestroyTensorDescriptor(descr);
		cudnnDestroyDropoutDescriptor(dropout_descriptor);
	}
};

extern "C" void * RegisterDropout(cudnnHandle_t handle, int n, int c, int h, int w, float drop)
{
	DropoutState * p = new DropoutState(handle, n, c, h, w, drop);
	return (void *)(p);
	//return (void *)p;
}

extern "C" void UnregisterDropout(void * state)
{
	DropoutState * p = (DropoutState *)state;
	delete p;
}

extern "C" void CuDNNDropoutForward(cudnnHandle_t handle, float * x, int batch, int depth, int height, int width, float * y, float drop, 
									void * state)
{
	DropoutState * p = (DropoutState *)state;
	//DropoutState dropoutState = DropoutState(handle, batch, depth, height, width, drop);
	//DropoutState * p = &dropoutState;
	cudnnStatus_t t = cudnnDropoutForward(handle, p->dropout_descriptor, p->descr, x, p->descr, y, p->reserve, p->reserve_size);

	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn dropout forward failed\n");
	}
}

extern "C" void CuDNNDropoutBackward(cudnnHandle_t handle, float * dy, int batch, int depth, int height, int width, float * dx, float drop, void * state)
{
	DropoutState * p = (DropoutState *)state;
	cudnnStatus_t t = cudnnDropoutBackward(handle, p->dropout_descriptor, p->descr, dy, p->descr, dx, p->reserve, p->reserve_size);
	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn dropout backward failed\n");
	}
}

extern "C" void CuDNNBatchNormalizationForwardTraining(cudnnHandle_t handle, float * x, int batch, int depth, int height, int width, float * y, float * bnScale, float * bnBias, float momentum, float alpha, float beta, float * runningMean, float * runningVariance, float * saveMean, float * saveVariance)
{

	cudnnTensorDescriptor_t ioDesc;
	cudnnCreateTensorDescriptor(&ioDesc);
	cudnnSetTensor4dDescriptor(ioDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, height, width);

	cudnnTensorDescriptor_t pDesc;
	cudnnCreateTensorDescriptor(&pDesc);
	cudnnSetTensor4dDescriptor(pDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, 1, depth, 1, 1);

	cudnnBatchNormalizationForwardTraining(handle, CUDNN_BATCHNORM_SPATIAL, &beta, &alpha, ioDesc, x, ioDesc, y, pDesc, bnScale, bnBias, momentum, runningMean, runningVariance, CUDNN_BN_MIN_EPSILON, saveMean, saveVariance);


	cudnnDestroyTensorDescriptor(ioDesc);
	cudnnDestroyTensorDescriptor(pDesc);
}

extern "C" void CuDNNBatchNormalizationBackward(cudnnHandle_t handle, float * x, int batch, int depth, int height, int width, float * dy, float * dx, float * bnScale, float alpha, float beta, float param_alpha, float param_beta, float * saveMean, float * saveVariance, float * dbnScale, float *dbnBias)
{

	cudnnTensorDescriptor_t ioDesc;
	cudnnCreateTensorDescriptor(&ioDesc);
	cudnnSetTensor4dDescriptor(ioDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, height, width);

	cudnnTensorDescriptor_t pDesc;
	cudnnCreateTensorDescriptor(&pDesc);
	cudnnSetTensor4dDescriptor(pDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, 1, depth, 1, 1);

	cudnnBatchNormalizationBackward(handle, CUDNN_BATCHNORM_SPATIAL, &beta, &alpha, &param_beta, &param_alpha, ioDesc, x, ioDesc, dy, ioDesc, dx,  
						pDesc, bnScale, dbnScale, dbnBias, CUDNN_BN_MIN_EPSILON, saveMean, saveVariance);

	cudnnDestroyTensorDescriptor(ioDesc);
	cudnnDestroyTensorDescriptor(pDesc);
}

extern "C" void CuDNNBatchNormalizationForwardInference(cudnnHandle_t handle, float * x, int batch, int depth, int height, int width, float * y, float * bnScale, float * bnBias, float alpha, float beta, float * runningMean, float * runningVariance)
{

	cudnnTensorDescriptor_t ioDesc;
	cudnnCreateTensorDescriptor(&ioDesc);
	cudnnSetTensor4dDescriptor(ioDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, height, width);

	cudnnTensorDescriptor_t pDesc;
	cudnnCreateTensorDescriptor(&pDesc);
	cudnnSetTensor4dDescriptor(pDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, 1, depth, 1, 1);

	cudnnBatchNormalizationForwardInference(handle, CUDNN_BATCHNORM_SPATIAL, &beta, &alpha, ioDesc, x, ioDesc, y, 
						pDesc, bnScale, bnBias, runningMean, runningVariance, CUDNN_BN_MIN_EPSILON);

	cudnnDestroyTensorDescriptor(ioDesc);
	cudnnDestroyTensorDescriptor(pDesc);
}


class TensorReduceState
{
public:
	size_t indicesSizeInBytes;
	void * indices;

	size_t workspaceSizeInBytes;
	void * workspace;

	cudnnReduceTensorDescriptor_t reduceDesc;
	cudnnTensorDescriptor_t aDesc;
	cudnnTensorDescriptor_t cDesc;

	int Lazy = 0;
	TensorReduceState(cudnnHandle_t handle, cudnnReduceTensorOp_t reduceOp_t, int dimNum, int * aDims, int * cDims, int lazy = 0)
	{
		cudnnCreateReduceTensorDescriptor(&reduceDesc);
		cudnnSetReduceTensorDescriptor(reduceDesc, reduceOp_t, CUDNN_DATA_FLOAT, CUDNN_PROPAGATE_NAN, CUDNN_REDUCE_TENSOR_FLATTENED_INDICES, CUDNN_32BIT_INDICES);

		cudnnCreateTensorDescriptor(&aDesc);
		cudnnSetTensorNdDescriptorEx(aDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, dimNum, aDims);

		cudnnCreateTensorDescriptor(&cDesc);
		cudnnSetTensorNdDescriptorEx(cDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, dimNum, cDims);
		
		cudnnGetReductionIndicesSize(handle, reduceDesc, aDesc, cDesc, &indicesSizeInBytes);

		cudnnGetReductionWorkspaceSize(handle, reduceDesc, aDesc, cDesc, &workspaceSizeInBytes);

		Lazy = lazy;

		if(lazy == 0)
		{
		    cudaMalloc(&indices, indicesSizeInBytes);
		    cudaMalloc(&workspace, workspaceSizeInBytes);
        	printf("reduce workspaceSizeInBytes %d\n", workspaceSizeInBytes);
			printf("reduce indicesSizeInBytes %d\n", indicesSizeInBytes);
        }
	}

	~TensorReduceState()
	{
		if(Lazy == 0)
		{
			cudaFree(indices);
			cudaFree(workspace);
		}
		cudnnDestroyReduceTensorDescriptor(reduceDesc);

		cudnnDestroyTensorDescriptor(aDesc);
		cudnnDestroyTensorDescriptor(cDesc);
	}
};

extern "C" void * CuDNNRegisteReduce(cudnnHandle_t handle, int reduceOp, int dimNum, int * aDims, int * cDims)
{

	TensorReduceState * p;

	if(reduceOp == 0)
	{
	 	p = new TensorReduceState(handle, CUDNN_REDUCE_TENSOR_AVG, dimNum, aDims, cDims);
	}
	else if(reduceOp == 1)
	{
		p = new TensorReduceState(handle, CUDNN_REDUCE_TENSOR_NORM2, dimNum, aDims, cDims);
	}
	else if(reduceOp == 2)
	{
		p = new TensorReduceState(handle, CUDNN_REDUCE_TENSOR_ADD, dimNum, aDims, cDims);
	}
	else if(reduceOp == 3)
	{
		p = new TensorReduceState(handle, CUDNN_REDUCE_TENSOR_MAX, dimNum, aDims, cDims);
	}
	return (void *)(p);
}

extern "C" void * CuDNNRegisteLazyReduce(cudnnHandle_t handle, int reduceOp, int dimNum, int * aDims, int * cDims)
{
	TensorReduceState * p;

	if(reduceOp == 0)
	{
	 	p = new TensorReduceState(handle, CUDNN_REDUCE_TENSOR_AVG, dimNum, aDims, cDims, 1);
	}
	else if(reduceOp == 1)
	{
		p = new TensorReduceState(handle, CUDNN_REDUCE_TENSOR_NORM2, dimNum, aDims, cDims, 1);
	}
	else if(reduceOp == 2)
	{
		p = new TensorReduceState(handle, CUDNN_REDUCE_TENSOR_ADD, dimNum, aDims, cDims, 1);
	}
	else if(reduceOp == 3)
	{
		p = new TensorReduceState(handle, CUDNN_REDUCE_TENSOR_MAX, dimNum, aDims, cDims, 1);
	}
	return (void *)(p);
}

extern "C" int GetReduceIndices(void * state, int * indices, int length)
{
	TensorReduceState * p = (TensorReduceState *)state;
	
	//cudaMemcpy(gpu_floats_a + aIdx, gpu_float_b + bIdx, m * sizeof(float), cudaMemcpyDeviceToDevice);
	cudaMemcpy(indices, p->indices, sizeof(int) * length, cudaMemcpyDeviceToDevice);
	return length;
}

extern "C" int GetReduceStateIndiceSize(void * state)
{
	TensorReduceState * p = (TensorReduceState *)state;
	return p->indicesSizeInBytes;
}

extern "C" int GetReduceStateWorkspaceSize(void * state)
{
	TensorReduceState * p = (TensorReduceState *)state;
	return p->workspaceSizeInBytes;
}

extern "C" void AssignReduceState(void * state, void * indicePtr, int indiceSize, void * workspacePtr, int workspaceSize)
{
	TensorReduceState * p = (TensorReduceState *)state;

	p->indicesSizeInBytes = indiceSize;
	p->indices = indicePtr;
	//cudaMalloc(&p->indices, indiceSize);

	p->workspaceSizeInBytes = workspaceSize;
	p->workspace = workspacePtr;
	//cudaMalloc(&p->workspace, workspaceSize);
}



extern "C" void CuDNNUnRegisteReduce(void * state)
{
	TensorReduceState * p = (TensorReduceState *)state;
	delete p;
}

extern "C" void CuDNNReduceOp(cudnnHandle_t handle, float * a, float * c, float alpha, float beta, void * state)
{
	TensorReduceState * p = (TensorReduceState *)state;
	cudnnStatus_t t = cudnnReduceTensor(handle, p->reduceDesc, p->indices, p->indicesSizeInBytes,
											 p->workspace, p->workspaceSizeInBytes,
											 &beta,
											 p->aDesc,
											 a,
											 &alpha,
											 p->cDesc,
											 c);
	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("error in executing cudnnReduceTensor tensor op.\n");
	}
	if(t == CUDNN_STATUS_NOT_SUPPORTED)
	{
		printf("not supported error in cudnnReduceTensor.\n");
	}
	if(t == CUDNN_STATUS_BAD_PARAM)
	{
		printf("bad param error in cudnnReduceTensor.\n");	
	}
	if(t == CUDNN_STATUS_EXECUTION_FAILED)
	{
		printf("execution failed error in cudnnReduceTensor.\n");	
	}
}


// use tensor argmax for argmax K.
extern "C" void CuDNNTensorArgmaxK(cudnnHandle_t handle, cusparseHandle_t sparseHandle, float * input, int k, float * output, int * local_indices, int * global_indices, int olength, int * out_dim, int * in_index, int max_dim, int dim_num, void * state)
{
	TensorReduceState * p = (TensorReduceState *)state;

	float alpha = 0;
	float beta = 1;
	for(int i = 0; i < k; i++)
	{
		cudnnStatus_t t = cudnnReduceTensor(handle, p->reduceDesc, p->indices, p->indicesSizeInBytes,
											 p->workspace, p->workspaceSizeInBytes,
											 &beta,
											 p->aDesc,
											 input,
											 &alpha,
											 p->cDesc,
											 output + (i * olength));

		cudaMemcpy(local_indices + (i * olength), p->indices, sizeof(int) * olength, cudaMemcpyDeviceToDevice);


		Cuda_Local2GlobalIndices(input, local_indices + (i * olength), global_indices + (i * olength), out_dim, in_index, max_dim, dim_num, olength);
	}

	cusparseSsctr(sparseHandle, k * olength, output, global_indices, input, CUSPARSE_INDEX_BASE_ZERO);
}

extern "C" void CuDNNTensorOp(cudnnHandle_t handle, int opType, int dimNum, int * aDims, float * a, 
																		 int * bDims, float * b, 
																		 int * cDims, float * c, 
																		 float beta1, float beta2, float alpha)
{
	// opType = 0 : add ; 1: MUL;
	cudnnTensorDescriptor_t aDesc;
	cudnnCreateTensorDescriptor(&aDesc);
	cudnnSetTensorNdDescriptorEx(aDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, dimNum, aDims);

	cudnnTensorDescriptor_t bDesc;
	cudnnCreateTensorDescriptor(&bDesc);
	cudnnSetTensorNdDescriptorEx(bDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, dimNum, bDims);

	cudnnTensorDescriptor_t cDesc;
	cudnnCreateTensorDescriptor(&cDesc);
	cudnnSetTensorNdDescriptorEx(cDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, dimNum, cDims);

	cudnnOpTensorDescriptor_t pOp;
	cudnnCreateOpTensorDescriptor(&pOp);
	if(opType == 0)
	{
		cudnnSetOpTensorDescriptor(pOp, CUDNN_OP_TENSOR_ADD, CUDNN_DATA_FLOAT, CUDNN_NOT_PROPAGATE_NAN);
	}
	else if(opType == 1)
	{
		cudnnSetOpTensorDescriptor(pOp, CUDNN_OP_TENSOR_MUL, CUDNN_DATA_FLOAT, CUDNN_NOT_PROPAGATE_NAN);
	}

	cudnnStatus_t t = cudnnOpTensor(handle, pOp, &beta1, aDesc, a, &beta2, bDesc, b, &alpha, cDesc, c);
	
	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("error in executing cudnnOpTensor tensor op.\n");
	}
	if(t == CUDNN_STATUS_NOT_SUPPORTED)
	{
		printf("not supported error in cudnnOpTensor.\n");
	}
	if(t == CUDNN_STATUS_BAD_PARAM)
	{
		printf("bad param error in cudnnOpTensor.\n");	
	}

	cudnnDestroyTensorDescriptor(aDesc);
	cudnnDestroyTensorDescriptor(bDesc);
	cudnnDestroyTensorDescriptor(cDesc);
	
	cudnnDestroyOpTensorDescriptor(pOp);
}

extern "C" void Reciprocal(float * a, float * b, float alpha, float beta, int size)
{
	Cuda_Reciprocal(a, b, alpha, beta, size);
}


extern "C" void CuDNNMeanPooling2DForward(cudnnHandle_t handle, float * input, int in_width, int in_height, int depth, int batch, 
															   int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
															   float * output, int out_width, int out_height, float alpha, float beta)
{
	cudnnTensorDescriptor_t pInputDesc;
	cudnnCreateTensorDescriptor(&pInputDesc);
	cudnnSetTensor4dDescriptor(pInputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, in_height, in_width);

	cudnnTensorDescriptor_t pOutputDesc;
	cudnnCreateTensorDescriptor(&pOutputDesc);
	cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, out_height, out_width);

	cudnnPoolingDescriptor_t pPoolDesc;
	cudnnCreatePoolingDescriptor(&pPoolDesc);
	cudnnSetPooling2dDescriptor(pPoolDesc, CUDNN_POOLING_AVERAGE_COUNT_EXCLUDE_PADDING, CUDNN_NOT_PROPAGATE_NAN, 
								sxHeight, sxWidth, padHeight, padWidth, strideHeight, strideWidth);

	cudnnStatus_t t = cudnnPoolingForward(handle, pPoolDesc, &beta, pInputDesc, input, &alpha, pOutputDesc, output);

	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn mean pooling forward failed\n");
	}

	cudnnDestroyTensorDescriptor(pInputDesc);
	cudnnDestroyTensorDescriptor(pOutputDesc);
	cudnnDestroyPoolingDescriptor(pPoolDesc);

}


extern "C" void CuDNNMeanPooling2DBackward(cudnnHandle_t handle, float * input, float * dx, int in_width, int in_height, int depth, int batch, 
															   int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
															   float * output, float * dy, int out_width, int out_height, float alpha, float beta)
{
	cudnnTensorDescriptor_t pInputDesc;
	cudnnCreateTensorDescriptor(&pInputDesc);
	cudnnSetTensor4dDescriptor(pInputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, in_height, in_width);

	cudnnTensorDescriptor_t pOutputDesc;
	cudnnCreateTensorDescriptor(&pOutputDesc);
	cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, out_height, out_width);

	cudnnPoolingDescriptor_t pPoolDesc;
	cudnnCreatePoolingDescriptor(&pPoolDesc);
	cudnnSetPooling2dDescriptor(pPoolDesc, CUDNN_POOLING_AVERAGE_COUNT_EXCLUDE_PADDING, CUDNN_NOT_PROPAGATE_NAN, 
								sxHeight, sxWidth, padHeight, padWidth, strideHeight, strideWidth);

	cudnnStatus_t t = cudnnPoolingBackward(handle, pPoolDesc, &beta,  pOutputDesc, output, 
													pOutputDesc, dy, 
													pInputDesc, input, 
											&alpha, pInputDesc, dx);

	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn mean pooling backward failed\n");
	}

	cudnnDestroyTensorDescriptor(pInputDesc);
	cudnnDestroyTensorDescriptor(pOutputDesc);
	cudnnDestroyPoolingDescriptor(pPoolDesc);
}

extern "C" void CuDNNMaxPooling2DForward(cudnnHandle_t handle, float * input, int in_width, int in_height, int depth, int batch, 
															   int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
															   float * output, int out_width, int out_height, float alpha, float beta)
{
	cudnnTensorDescriptor_t pInputDesc;
	cudnnCreateTensorDescriptor(&pInputDesc);
	cudnnSetTensor4dDescriptor(pInputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, in_height, in_width);

	cudnnTensorDescriptor_t pOutputDesc;
	cudnnCreateTensorDescriptor(&pOutputDesc);
	cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, out_height, out_width);

	cudnnPoolingDescriptor_t pPoolDesc;
	cudnnCreatePoolingDescriptor(&pPoolDesc);
	cudnnSetPooling2dDescriptor(pPoolDesc, CUDNN_POOLING_MAX, CUDNN_NOT_PROPAGATE_NAN, 
								sxHeight, sxWidth, padHeight, padWidth, strideHeight, strideWidth);

	cudnnStatus_t t = cudnnPoolingForward(handle, pPoolDesc, &beta, pInputDesc, input, &alpha, pOutputDesc, output);

	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn pooling forward failed\n");
	}

	cudnnDestroyTensorDescriptor(pInputDesc);
	cudnnDestroyTensorDescriptor(pOutputDesc);
	cudnnDestroyPoolingDescriptor(pPoolDesc);

}

extern "C" void CuDNNMaxPooling2DBackward(cudnnHandle_t handle, float * input, float * dx, int in_width, int in_height, int depth, int batch, 
															   int sxWidth, int sxHeight, int padWidth, int padHeight, int strideWidth, int strideHeight,
															   float * output, float * dy, int out_width, int out_height, float alpha, float beta)
{
	cudnnTensorDescriptor_t pInputDesc;
	cudnnCreateTensorDescriptor(&pInputDesc);
	cudnnSetTensor4dDescriptor(pInputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, in_height, in_width);

	cudnnTensorDescriptor_t pOutputDesc;
	cudnnCreateTensorDescriptor(&pOutputDesc);
	cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batch, depth, out_height, out_width);

	cudnnPoolingDescriptor_t pPoolDesc;
	cudnnCreatePoolingDescriptor(&pPoolDesc);
	cudnnSetPooling2dDescriptor(pPoolDesc, CUDNN_POOLING_MAX, CUDNN_NOT_PROPAGATE_NAN, 
								sxHeight, sxWidth, padHeight, padWidth, strideHeight, strideWidth);

	cudnnStatus_t t = cudnnPoolingBackward(handle, pPoolDesc, &beta,  pOutputDesc, output, 
													pOutputDesc, dy, 
													pInputDesc, input, 
											&alpha, pInputDesc, dx);

	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn pooling backward failed\n");
	}

	cudnnDestroyTensorDescriptor(pInputDesc);
	cudnnDestroyTensorDescriptor(pOutputDesc);
	cudnnDestroyPoolingDescriptor(pPoolDesc);
}



extern "C" void CuDNNForwardPropagateEx(cudnnHandle_t handle, float * imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
		float * filterMap, int filterMapNum, int filterHeight, int filterWidth,
		int padHeight, int padWidth, int strideHeight, int strideWidth,
		float * imageOutBatch, int outHeight, int outWidth, float alpha, float beta)
{
	cudnnDataType_t dataType = CUDNN_DATA_FLOAT;

	cudnnTensorDescriptor_t pInputDesc;

	int n_in = nBatch;	// Number of images - originally 128
	int c_in = inFeatureMap;	// Number of feature maps per image - originally 96
	int h_in = inHeight;	// Height of each feature map - originally 221
	int w_in = inWidth;	// Width of each feature map - originally 221

	cudnnCreateTensorDescriptor(&pInputDesc);
	cudnnSetTensor4dDescriptor(pInputDesc, CUDNN_TENSOR_NCHW, dataType, n_in, c_in, h_in, w_in);

	//cudaMalloc((void**)&pImageInBatch, n_in*c_in*h_in*w_in * nDataTypeSize);

	cudnnFilterDescriptor_t pFilterDesc = NULL;
	int k_pFilter_in = filterMapNum;	// Number of output feature maps - originally 256
	int c_pFilter_in = c_in;	// Number of input feature maps - originally 96
	int h_pFilter_in = filterHeight;	// Height of each pFilter - originally 7
	int w_pFilter_in = filterWidth;	// Width of each pFilter - originally 7

	cudnnCreateFilterDescriptor(&pFilterDesc);
	//cudnnSetFilter4dDescriptor(pFilterDesc, dataType, k_pFilter_in, c_pFilter_in, h_pFilter_in, w_pFilter_in);

	cudnnSetFilter4dDescriptor(pFilterDesc, dataType, CUDNN_TENSOR_NCHW, k_pFilter_in, c_pFilter_in, h_pFilter_in, w_pFilter_in);

	cudnnConvolutionDescriptor_t pConvDesc = NULL;
	cudnnCreateConvolutionDescriptor(&pConvDesc);

	#if CUDNN_VERSION_MIN(6, 0, 0)
		cudnnSetConvolution2dDescriptor(pConvDesc, padHeight, padWidth, strideHeight, strideWidth, 1, 1, CUDNN_CROSS_CORRELATION, CUDNN_DATA_FLOAT);	//cudnnDataType_t::
	#else
		cudnnSetConvolution2dDescriptor(pConvDesc, padHeight, padWidth, strideHeight, strideWidth, 1, 1, CUDNN_CROSS_CORRELATION);
	#endif
	cudnnTensorDescriptor_t pOutputDesc = NULL;
	int n_out = nBatch;	// Number of output images.
	int c_out = k_pFilter_in;	// Number of output feature maps per image.
	int h_out = outHeight;	// Height of each output feature map.
	int w_out = outWidth;	// Width of each output feature map.
	cudnnCreateTensorDescriptor(&pOutputDesc);
	cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, dataType, n_out, c_out, h_out, w_out);

	cudnnConvolutionFwdAlgo_t algo;
	cudnnGetConvolutionForwardAlgorithm(handle,
			pInputDesc,
			pFilterDesc,
			pConvDesc,
			pOutputDesc,
			CUDNN_CONVOLUTION_FWD_NO_WORKSPACE,
			//CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
			0,
			&algo
			);

	size_t sizeInBytes = 0;
	/*cudnnGetConvolutionForwardWorkspaceSize(hCudNN,
	  pInputDesc,
	  pFilterDesc,
	  pConvDesc,
	  pOutputDesc,
	  algo,
	  &sizeInBytes);*/
	//cout << "workSpace Size" << sizeInBytes << endl;

	void* workSpace = NULL;
	//cudaMalloc(&workSpace, sizeInBytes);

	//float alpha = 1;
	//float beta = 0;
	cudnnStatus_t t = cudnnConvolutionForward(handle, &beta, pInputDesc, imageInBatch, pFilterDesc, filterMap, pConvDesc, algo, 
							workSpace, sizeInBytes, &alpha, pOutputDesc, imageOutBatch);

	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn conv forward failed\n");
	}
	//cudaFree(workSpace);
	cudnnDestroyTensorDescriptor(pInputDesc);
	cudnnDestroyTensorDescriptor(pOutputDesc);
	cudnnDestroyConvolutionDescriptor(pConvDesc);
	cudnnDestroyFilterDescriptor(pFilterDesc);
	//cudnnDestroy(hCudNN);
}

extern "C" void CuDNNForwardPropagate(cudnnHandle_t handle, float * imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
		float * filterMap, int filterMapNum, int filterHeight, int filterWidth,
		int pad, int stride,
		float * imageOutBatch, int outHeight, int outWidth)
{
	CuDNNForwardPropagateEx(handle, imageInBatch, nBatch, inFeatureMap, inHeight, inWidth, filterMap, filterMapNum, filterHeight, filterWidth,
							pad, pad, stride, stride, imageOutBatch, outHeight, outWidth, 0, 1);

}



extern "C" void CuDNNBackwardPropagateDataEx(cudnnHandle_t hCudNN, float * imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth,
		float * filterMap, int filterMapBum, int filterHeight, int filterWidth, int padHeight, int padWidth, int strideHeight, int strideWidth, 
		float * imageOutBatchDeriv, int outHeight, int outWidth, float alpha, float beta)
{
	cudnnDataType_t dataType = CUDNN_DATA_FLOAT;
	cudnnTensorDescriptor_t pInputDesc;
	int n_in = nBatch;	// Number of images - originally 128
	int c_in = inFeatureMap;	// Number of feature maps per image - originally 96
	int h_in = inHeight;	// Height of each feature map - originally 221
	int w_in = inWidth;	// Width of each feature map - originally 221

	cudnnCreateTensorDescriptor(&pInputDesc);
	cudnnSetTensor4dDescriptor(pInputDesc, CUDNN_TENSOR_NCHW, dataType, n_in, c_in, h_in, w_in);

	//cudaMalloc((void**)&pImageInBatch, n_in*c_in*h_in*w_in * nDataTypeSize);

	cudnnFilterDescriptor_t pFilterDesc = NULL;
	int k_pFilter_in = filterMapBum;	// Number of output feature maps - originally 256
	int c_pFilter_in = c_in;	// Number of input feature maps - originally 96
	int h_pFilter_in = filterHeight;	// Height of each pFilter - originally 7
	int w_pFilter_in = filterWidth;	// Width of each pFilter - originally 7

	cudnnCreateFilterDescriptor(&pFilterDesc);
	//cudnnSetFilter4dDescriptor(pFilterDesc, dataType, k_pFilter_in, c_pFilter_in, h_pFilter_in, w_pFilter_in);
	cudnnSetFilter4dDescriptor(pFilterDesc, dataType, CUDNN_TENSOR_NCHW, k_pFilter_in, c_pFilter_in, h_pFilter_in, w_pFilter_in);

	cudnnConvolutionDescriptor_t pConvDesc = NULL;
	cudnnCreateConvolutionDescriptor(&pConvDesc);
	#if CUDNN_VERSION_MIN(6, 0, 0)
	cudnnSetConvolution2dDescriptor(pConvDesc, padHeight, padWidth, strideHeight, strideWidth, 1, 1, CUDNN_CROSS_CORRELATION, CUDNN_DATA_FLOAT);
	#else
	cudnnSetConvolution2dDescriptor(pConvDesc, padHeight, padWidth, strideHeight, strideWidth, 1, 1, CUDNN_CROSS_CORRELATION);
	#endif


	cudnnTensorDescriptor_t pOutputDesc = NULL;
	int n_out = nBatch;	// Number of output images.
	int c_out = k_pFilter_in;	// Number of output feature maps per image.
	int h_out = outHeight;	// Height of each output feature map.
	int w_out = outWidth;	// Width of each output feature map.
	cudnnCreateTensorDescriptor(&pOutputDesc);
	cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, dataType, n_out, c_out, h_out, w_out);

	//float alpha = 1;
	//float beta = 0;
	//cudnnConvolutionBackwardData(hCudNN, &alpha, pFilterDesc, filterMap, pOutputDesc, imageOutBatchDeriv,
	//		pConvDesc, &beta, pInputDesc, imageInBatchDeriv);
	cudnnStatus_t t = cudnnConvolutionBackwardData(hCudNN, &beta, pFilterDesc, filterMap, pOutputDesc, imageOutBatchDeriv,
		pConvDesc, (cudnnConvolutionBwdDataAlgo_t)0, NULL, 0, &alpha, pInputDesc, imageInBatchDeriv);

 	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn conv backward data failed\n");
	}

	cudnnDestroyTensorDescriptor(pInputDesc);
	cudnnDestroyTensorDescriptor(pOutputDesc);
	cudnnDestroyConvolutionDescriptor(pConvDesc);
	cudnnDestroyFilterDescriptor(pFilterDesc);

	//cudnnDestroy(hCudNN);
}

extern "C" void CuDNNBackwardPropagateData(cudnnHandle_t hCudNN, float * imageInBatchDeriv, int nBatch, int inFeatureMap, int inHeight, int inWidth,
		float * filterMap, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, 
		float * imageOutBatchDeriv, int outHeight, int outWidth)
{
	CuDNNBackwardPropagateDataEx(hCudNN, imageInBatchDeriv, nBatch, inFeatureMap, inHeight, inWidth, filterMap, filterMapBum, filterHeight, filterWidth, 
								pad, pad, stride, stride, imageOutBatchDeriv, outHeight, outWidth, 1, 1);
}


extern "C" void CuDNNBackwardPropagateFilterEx(cudnnHandle_t hCudNN, float * imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
		float * filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int padHeight, int padWidth, int strideHeight, int strideWidth, 
		float * imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue)
{
	//cudnnHandle_t hCudNN = NULL;
	//cudnnCreate(&hCudNN);

	cudnnDataType_t dataType = CUDNN_DATA_FLOAT;

	cudnnTensorDescriptor_t pInputDesc;

	int n_in = nBatch;	// Number of images - originally 128
	int c_in = inFeatureMap;	// Number of feature maps per image - originally 96
	int h_in = inHeight;	// Height of each feature map - originally 221
	int w_in = inWidth;	// Width of each feature map - originally 221

	cudnnCreateTensorDescriptor(&pInputDesc);
	cudnnSetTensor4dDescriptor(pInputDesc, CUDNN_TENSOR_NCHW, dataType, n_in, c_in, h_in, w_in);

	//cudaMalloc((void**)&pImageInBatch, n_in*c_in*h_in*w_in * nDataTypeSize);

	cudnnFilterDescriptor_t pFilterDesc = NULL;
	int k_pFilter_in = filterMapBum;	// Number of output feature maps - originally 256
	int c_pFilter_in = c_in;	// Number of input feature maps - originally 96
	int h_pFilter_in = filterHeight;	// Height of each pFilter - originally 7
	int w_pFilter_in = filterWidth;	// Width of each pFilter - originally 7

	cudnnCreateFilterDescriptor(&pFilterDesc);
	//cudnnSetFilter4dDescriptor(pFilterDesc, dataType, k_pFilter_in, c_pFilter_in, h_pFilter_in, w_pFilter_in);
	cudnnSetFilter4dDescriptor(pFilterDesc, dataType, CUDNN_TENSOR_NCHW, k_pFilter_in, c_pFilter_in, h_pFilter_in, w_pFilter_in);


	cudnnConvolutionDescriptor_t pConvDesc = NULL;
	cudnnCreateConvolutionDescriptor(&pConvDesc);
	#if CUDNN_VERSION_MIN(6, 0, 0)
		cudnnSetConvolution2dDescriptor(pConvDesc, padHeight, padWidth, strideHeight, strideWidth, 1, 1, CUDNN_CROSS_CORRELATION, CUDNN_DATA_FLOAT);
	#else
		cudnnSetConvolution2dDescriptor(pConvDesc, padHeight, padWidth, strideHeight, strideWidth, 1, 1, CUDNN_CROSS_CORRELATION);
	#endif
	//cudnnDataType_t::

	cudnnTensorDescriptor_t pOutputDesc = NULL;
	int n_out = nBatch;	// Number of output images.
	int c_out = k_pFilter_in;	// Number of output feature maps per image.
	int h_out = outHeight;	// Height of each output feature map.
	int w_out = outWidth;	// Width of each output feature map.
	cudnnCreateTensorDescriptor(&pOutputDesc);
	cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, dataType, n_out, c_out, h_out, w_out);

	//float alpha = 1;
	//float beta = 0;

	//cudnnConvolutionBackwardFilter(hCudNN, &alphaValue, pInputDesc, imageInBatch, pOutputDesc, imageOutBatchDeriv, pConvDesc, &betaValue, pFilterDesc, filterMapDeriv);
	cudnnStatus_t t = cudnnConvolutionBackwardFilter(hCudNN, &alphaValue, pInputDesc, imageInBatch, pOutputDesc, imageOutBatchDeriv, pConvDesc,
		(cudnnConvolutionBwdFilterAlgo_t)0, NULL, 0, &betaValue, pFilterDesc, filterMapDeriv);

	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn conv backward filter failed\n");
	}

	cudnnDestroyTensorDescriptor(pInputDesc);
	cudnnDestroyTensorDescriptor(pOutputDesc);
	cudnnDestroyConvolutionDescriptor(pConvDesc);
	cudnnDestroyFilterDescriptor(pFilterDesc);
	//cudnnDestroy(hCudNN);
}


extern "C" void CuDNNBackwardPropagateFilter(cudnnHandle_t hCudNN, float * imageInBatch, int nBatch, int inFeatureMap, int inHeight, int inWidth,
		float * filterMapDeriv, int filterMapBum, int filterHeight, int filterWidth, int pad, int stride, 
		float * imageOutBatchDeriv, int outHeight, int outWidth, float alphaValue, float betaValue)
{
	CuDNNBackwardPropagateFilterEx(hCudNN, imageInBatch, nBatch, inFeatureMap, inHeight, inWidth,
								   filterMapDeriv, filterMapBum, filterHeight, filterWidth, pad, pad, stride, stride,
								   imageOutBatchDeriv, outHeight, outWidth, alphaValue, betaValue);
}

//#ifdef WIN32
//DLLEXP void __stdcall CuDNNBackwardPropagateBias(cudnnHandle_t hCudNN, float * biasDeriv, float * imageOutBatchDeriv, int batch, int outDepth, int outHeight, int outWidth, float alphaValue, float betaValue)
//#else
extern "C" void CuDNNBackwardPropagateBias(cudnnHandle_t hCudNN, float * biasDeriv, float * imageOutBatchDeriv, int batch, int outDepth, int outHeight, int outWidth, float alphaValue, float betaValue)
//#endif
{
	//cudnnHandle_t hCudNN = NULL;
	//cudnnCreate(&hCudNN);

	cudnnTensorDescriptor_t pOutputDesc = NULL;
	int n_out = batch;	// Number of output images.
	int c_out = outDepth;	// Number of output feature maps per image.
	int h_out = outHeight;	// Height of each output feature map.
	int w_out = outWidth;	// Width of each output feature map.
	cudnnCreateTensorDescriptor(&pOutputDesc);
	cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, n_out, c_out, h_out, w_out);

	cudnnTensorDescriptor_t pBiasDesc = NULL;
	//int n_bias = 1;	// Number of output images.
	//int c_bias = outDepth;	// Number of output feature maps per image.
	//int h_bias = 1;	// Height of each output feature map.
	//int w_bias = 1;	// Width of each output feature map.
	cudnnCreateTensorDescriptor(&pBiasDesc);
	cudnnSetTensor4dDescriptor(pBiasDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, 1, outDepth, 1, 1);

	cudnnStatus_t t = cudnnConvolutionBackwardBias(hCudNN, &alphaValue, pOutputDesc, imageOutBatchDeriv, &betaValue, pBiasDesc, biasDeriv);
	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("cudnn conv backward bias filter failed\n");
	}

	cudnnDestroyTensorDescriptor(pOutputDesc);
	cudnnDestroyTensorDescriptor(pBiasDesc);
	//cudnnDestroy(hCudNN);
}

extern "C" void CuDNNLogSoftmax(cudnnHandle_t handle, float * input, float * output, int batchSize, int dim)
//#endif
{
	cudnnTensorDescriptor_t pDesc;
	cudnnCreateTensorDescriptor(&pDesc);
	cudnnSetTensor4dDescriptor(pDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batchSize, dim, 1, 1);

	float alpha = 0;
	float beta = 1;


	//printf("version %d \n", CUDNN_VERSION);
	//CUDNN_SOFTMAX_LOG
	cudnnStatus_t t = cudnnSoftmaxForward(handle, CUDNN_SOFTMAX_LOG,
			CUDNN_SOFTMAX_MODE_INSTANCE, &beta, pDesc, input, &alpha, pDesc, output);

	if(t == CUDNN_STATUS_BAD_PARAM)
	{
		printf("log softmax bad param.\n");
	}
	if(t == CUDNN_STATUS_EXECUTION_FAILED)
	{
		printf("log softmax execute failed.\n");
	}
	cudnnDestroyTensorDescriptor(pDesc);
}


//#ifdef WIN32
//DLLEXP void __stdcall CuDNNSoftmax(cudnnHandle_t handle, float * input, float * output, int batchSize, int dim)
//#else
extern "C" void CuDNNSoftmax(cudnnHandle_t handle, float * input, float * output, int batchSize, int dim)
//#endif
{
	cudnnTensorDescriptor_t pDesc;
	cudnnCreateTensorDescriptor(&pDesc);
	cudnnSetTensor4dDescriptor(pDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batchSize, dim, 1, 1);

	//cudnnTensorDescriptor_t pOutputDesc;
	//cudnnCreateTensorDescriptor(&pOutputDesc);
	//cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batchSize, dim, 1, 1);

	float alpha = 0;
	float beta = 1;


	//printf("version %d \n", CUDNN_VERSION);

	cudnnStatus_t t = cudnnSoftmaxForward(handle, CUDNN_SOFTMAX_ACCURATE,
			CUDNN_SOFTMAX_MODE_INSTANCE, &beta, pDesc, input, &alpha, pDesc, output);

	if(t == CUDNN_STATUS_BAD_PARAM)
	{
		printf("softmax bad param.\n");
	}
	if(t == CUDNN_STATUS_EXECUTION_FAILED)
	{
		printf("softmax execute failed.\n");
	}
	cudnnDestroyTensorDescriptor(pDesc);
}


extern "C" void CuDNNDerivLogSoftmax(cudnnHandle_t handle, float * softmaxData, float * softmaxDeriv, float * gradData, int batchSize, int dim, float alpha)
//#endif
{
	cudnnTensorDescriptor_t pDesc;
	cudnnCreateTensorDescriptor(&pDesc);
	cudnnSetTensor4dDescriptor(pDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batchSize, dim, 1, 1);

	//cudnnTensorDescriptor_t pOutputDesc;
	//cudnnCreateTensorDescriptor(&pOutputDesc);
	//cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batchSize, dim, 1, 1);

	float beta = 1;
	cudnnStatus_t t = cudnnSoftmaxBackward(handle, CUDNN_SOFTMAX_LOG,
		CUDNN_SOFTMAX_MODE_INSTANCE, &beta, pDesc, softmaxData, pDesc, softmaxDeriv, &alpha, pDesc, gradData);

	if(t == CUDNN_STATUS_BAD_PARAM)
	{
		printf("backward log softmax bad param.\n");
	}
	else if(t == CUDNN_STATUS_EXECUTION_FAILED)
	{
		printf("backward log softmax execute failed.\n");
	}

	cudnnDestroyTensorDescriptor(pDesc);
	//cudnnDestroyTensorDescriptor(pOutputDesc);
}

//#ifdef WIN32
//DLLEXP void __stdcall CuDNNDerivSoftmax(cudnnHandle_t handle, float * softmaxData, float * softmaxDeriv, float * gradData, int batchSize, int dim, float alpha)
//#else
extern "C" void CuDNNDerivSoftmax(cudnnHandle_t handle, float * softmaxData, float * softmaxDeriv, float * gradData, int batchSize, int dim, float alpha)
//#endif
{
	cudnnTensorDescriptor_t pDesc;
	cudnnCreateTensorDescriptor(&pDesc);
	cudnnSetTensor4dDescriptor(pDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batchSize, dim, 1, 1);

	//cudnnTensorDescriptor_t pOutputDesc;
	//cudnnCreateTensorDescriptor(&pOutputDesc);
	//cudnnSetTensor4dDescriptor(pOutputDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, batchSize, dim, 1, 1);

	float beta = 1;
	cudnnStatus_t t = cudnnSoftmaxBackward(handle, CUDNN_SOFTMAX_ACCURATE,
		CUDNN_SOFTMAX_MODE_INSTANCE, &beta, pDesc, softmaxData, pDesc, softmaxDeriv, &alpha, pDesc, gradData);

	if(t == CUDNN_STATUS_BAD_PARAM)
	{
		printf("backward softmax bad param.\n");
	}
	else if(t == CUDNN_STATUS_EXECUTION_FAILED)
	{
		printf("backward softmax execute failed.\n");
	}

	cudnnDestroyTensorDescriptor(pDesc);
	//cudnnDestroyTensorDescriptor(pOutputDesc);
}

extern "C" void CuDNNVectorMul(cudnnHandle_t handle, float * a, float * b, float * c, float alpha, float beta, int length)
{
	//printf("entry point\n");

	cudnnTensorDescriptor_t pDes;
	cudnnCreateTensorDescriptor(&pDes);
	cudnnSetTensor4dDescriptor(pDes, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, length, 1, 1, 1);

	//printf("debug1\n");

	cudnnOpTensorDescriptor_t pOp;
	cudnnCreateOpTensorDescriptor(&pOp);

	//printf("version %d \n", CUDNN_VERSION);

	cudnnSetOpTensorDescriptor(pOp, CUDNN_OP_TENSOR_MUL, CUDNN_DATA_FLOAT, CUDNN_NOT_PROPAGATE_NAN);
	float a2 = 1;

	cudnnStatus_t t = cudnnOpTensor(handle, pOp, &beta, pDes, a, &a2, pDes, b, &alpha, pDes, c);
	
	if(t != CUDNN_STATUS_SUCCESS)
	{
		printf("error in executing cudnnOpTensor vector mul\n");
	}
	if(t == CUDNN_STATUS_NOT_SUPPORTED)
	{
		printf("not supported error.\n");
	}
	if(t == CUDNN_STATUS_BAD_PARAM)
	{
		printf("bad param error.\n");	
	}

	//printf("debug3\n");

	cudnnDestroyTensorDescriptor(pDes);
	cudnnDestroyOpTensorDescriptor(pOp);

	//printf("debug4\n");
}





//#ifdef WIN32
//DLLEXP void*  __stdcall CudaCreateCuDnn()
//#else
extern "C" void*  CudaCreateCuDnn()
//#endif
{
	cudnnHandle_t hCudNN = NULL;
	cudnnStatus_t t = cudnnCreate(&hCudNN);
    
   if(t != CUDNN_STATUS_SUCCESS) 
    {
    	printf("CUDNN Initialization failed\n");
        printf("error msg %d\n", t);
    }
    else
    {
    	printf("CUDNN Initialization success\n");
    }
	return hCudNN;
}

//#ifdef WIN32
//DLLEXP void __stdcall CudaDestroyCuDnn(cudnnHandle_t handle)
//#else
extern "C" void CudaDestroyCuDnn(cudnnHandle_t handle)
//#endif
{
	cudnnDestroy(handle);
}

extern "C" void * CudaCreateCuRand(int seed)
{
	curandGenerator_t gen;
	curandStatus_t t = curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT);

	curandSetPseudoRandomGeneratorSeed(gen, seed);
	
	if(t != CURAND_STATUS_SUCCESS) 
    {
    	printf("CuRand Initialization failed\n");
        printf("error msg %d\n", t);
    }
    else
    {
    	printf("CuRand Initialization success\n");
    }
	return gen;
}

extern "C" void CudaDestroyCuRand(curandGenerator_t gen)
{
	curandDestroyGenerator(gen);
}

extern "C" void CudaGenerateRand(curandGenerator_t gen, float * gpu_data, int len)
{
	curandStatus_t t = curandGenerateUniform(gen, gpu_data, len);

	if(t != CURAND_STATUS_SUCCESS) 
    {
    	printf("CuRand generate random number failed\n");
        printf("error msg %d\n", t);
    }
}

///////////////////////////////////////////////////////////////////////////// CuBLAS Handle Free Functions. 
//#ifdef WIN32
//DLLEXP void __stdcall CuBLAS_Matrix_Multiplication(float * A, float * B, float * C, int batchsize, int m, int n, float alpha, float beta, bool transA, bool transB)
//#else
//extern "C" void CuBLAS_Matrix_Multiplication(float * A, float * B, float * C, int batchsize, int m, int n, float alpha, float beta, bool transA, bool transB)
//#endif
//{
//	cublasHandle_t handle;
//	cublasCreate(&handle);

	/// row major --> column major

//	if (!transA && !transB)
//	{
		/// c = a A * B + beta C
//		cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, batchsize, m, &alpha, B, n, A, m, &beta, C, n);
//	}
//	else if (!transA && transB)
//	{
		/// c = a op(A) * B + beta C
//		cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, batchsize, m, &alpha, B, m, A, m, &beta, C, n);
//	}
//	else if (transA && !transB)
//	{
		/// c = a A * op(B) + beta C
//		cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, m, batchsize, &alpha, B, n, A, m, &beta, C, n);
//	}
//	cublasDestroy(handle);
//}

//#ifdef WIN32
//DLLEXP void*   __stdcall CudaCreateCuBlas()
//#else
extern "C" void* CudaCreateCuBlas()
//#endif
{
	cublasHandle_t cublasHandle;
	cublasCreate(&cublasHandle);
	return cublasHandle;
}

//#ifdef WIN32
//DLLEXP void __stdcall CudaDestroyCuBlas(cublasHandle_t handle)
//#else
extern "C" void CudaDestroyCuBlas(cublasHandle_t handle)
//#endif
{
	cublasDestroy(handle);
}



extern "C" float  CUBLAS_Sasum(cublasHandle_t handle, float *x, int len, int norm)
{
	float result = 0;
	cublasSasum(handle, len, x, norm, &result);
	//cublas_Sasum(handle, x, len, norm, &result);
	return result;
}

extern "C" float CUBLAS_Sdot(cublasHandle_t handle, float * x, float * y, int len)
{
	float result = 0;
	cublasSdot(handle, len, x, 1, y, 1, &result);
	return result;
}

///////////////////////////////////////////////////////////////////////////// CuBLAS Handle Needed Functions. 
//#ifdef WIN32
//DLLEXP void __stdcall CuBLAS_Sgemm(cublasHandle_t handle, float * A, float * B, float * C, int batchsize, int m, int n, float alpha, float beta, bool transA, bool transB)
//#else
extern "C" void CuBLAS_Sgemm(cublasHandle_t handle, float * A, float * B, float * C, int batchsize, int m, int n, float alpha, float beta, bool transA, bool transB)
//#endif
{
	// row major --> column major

	if (!transA && !transB)
	{
		/// c = beta A * B + alpha C
		cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, batchsize, m, &beta, B, n, A, m, &alpha, C, n);
	}
	else if (!transA && transB)
	{
		/// c = beta op(A) * B + alpha C
		cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, batchsize, m, &beta, B, m, A, m, &alpha, C, n);
	}
	else if (transA && !transB)
	{
		/// c = beta A * op(B) + alpha C
		cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, m, batchsize, &beta, B, n, A, m, &alpha, C, n);
	}
}

///It is very slow for computing elementwise multiplication.
//#ifdef WIN32
//DLLEXP void __stdcall CuBLAS_SgbmvDiagonal(cublasHandle_t handle, float * A, float * x, float * y, int batchSize, float alpha, float beta)
//#else
extern "C" void CuBLAS_SgbmvDiagonal(cublasHandle_t handle, float * A, float * x, float * y, int batchSize, float alpha, float beta)
//#endif
{
	cublasSgbmv(handle, CUBLAS_OP_N, batchSize, batchSize, 0, 0, &alpha, A, 1, x, 1, &beta, y, 1);
}

//#ifdef WIN32
//DLLEXP void __stdcall CuBLAS_Scal(cublasHandle_t handle, float * A, float alpha, int n)
//#else
extern "C" void CuBLAS_Scal(cublasHandle_t handle, float * A, float alpha, int n)
//#endif
{
	cublasSscal(handle, n, &alpha, A, 1);
}

extern "C" void CuBLAS_Saxpy(cublasHandle_t handle, float * src, float alpha, float * dst, int n)
{
	cublasSaxpy(handle, n, &alpha, src, 1, dst, 1);
}

extern "C" void CuBLAS_Scopy(cublasHandle_t handle, float * src, float * dst, int n)
{
	cublasScopy(handle, n, src, 1, dst, 1);
}


//#ifdef WIN32
//DLLEXP void __stdcall CuBLAS_Sgemv(cublasHandle_t handle, float * matrix, float * x, int row, int col, float * y, bool transM, float alpha, float beta)
//#else
extern "C" void CuBLAS_Sgemv(cublasHandle_t handle, float * matrix, float * x, int row, int col, float * y, bool transM, float alpha, float beta)
//#endif
{
	if (transM)
	{
		cublasSgemv(handle, CUBLAS_OP_N, col, row, &beta, matrix, col, x, 1, &alpha, y, 1);
	}
	else
	{
		cublasSgemv(handle, CUBLAS_OP_T, col, row, &beta, matrix, col, x, 1, &alpha, y, 1);
	}

}

extern "C" void CuBLAS_Sger(cublasHandle_t handle, float *x, float * y, float * matrix, int row, int col, float beta)
{
	cublasSger(handle, col, row, &beta, x, 1, y, 1, matrix, col);
}

extern "C" float  CuBLAS_LogLossDeriv(cublasHandle_t handle, float * outputProb, int dim, int batchSize, float * smpProb,
		float * label, float * labelwei, int * labelMask, float gamma, float eps)
{
	return Cublas_LogLossDeriv(handle, outputProb, dim, batchSize, smpProb, label, labelwei, labelMask, gamma, eps);
}
///////////////////////////////////////////////////////////////////////////// Customized Sgemm and SparseSgemmFunction. 
//#ifdef WIN32
//DLLEXP void __stdcall SgemmMask(float * A, float * B, float * C, int batchsize, int m, int n, 
//#else
extern "C" void SgemmMask(float * A, float * B, float * C, int batchsize, int m, int n, 
//#endif
		int * aMask, int * bMask, int * cMask, float alpha, float beta, bool transA, bool transB)
{
	Cuda_SgemmMask(A, B, C, batchsize, m, n, aMask, bMask, cMask, alpha, beta, transA, transB);
}

//#ifdef WIN32
//DLLEXP void __stdcall SparseSgemmMask(int * AIndex, int * AFeaIndex, float * AFeaValue, float * B, float * C, int batchsize, int m, int n,
//#else
extern "C" void SparseSgemmMask(int * AIndex, int * AFeaIndex, float * AFeaValue, float * B, float * C, int batchsize, int m, int n,
		int * aMask, int * bMask, int * cMask, float alpha, float beta, bool transA, bool transB)
{
	Cuda_SparseSgemmMask(AIndex, AFeaIndex, AFeaValue, B, C, batchsize, m, n, aMask, bMask, cMask, alpha, beta, transA, transB);
}

extern "C" void CuSparseSgemm(cusparseHandle_t handle, float * A, int * bIndex, int * bfeaIndex, float * bfeaValue, int elementSize, 
					float * C, int batchSize, int aDim, int bDim, float alpha, float beta)
{
	cusparseSgemmi(handle, 
               aDim, bDim, batchSize, elementSize, &beta, A, aDim, bfeaValue, bIndex, bfeaIndex, &alpha, C, aDim);
}

extern "C" void CuCsr2Csc(cusparseHandle_t handle, int * aIndex, int * afeaIndex, float * afeaValue,
												   int * bIndex, int * bfeaIndex, float * bfeaValue,
												   int elementSize, int aRow, int aCol)
{
	cusparseScsr2csc(handle, aRow, aCol, elementSize, afeaValue, aIndex, afeaIndex,
				  bfeaValue, bfeaIndex, bIndex,	
				  CUSPARSE_ACTION_NUMERIC,
				  CUSPARSE_INDEX_BASE_ZERO);
}



///////////////////////////////////////////////////////////////////////////// Activation Functions. 
//#ifdef WIN32
//DLLEXP void __stdcall Logistic(float * a, float * b, int size, float gamma)
//#else
extern "C" void Logistic(float * a, float * b, int size, float gamma)
//#endif
{
	Cuda_Logistic(a, b, size, gamma);
}

extern "C" void Log(float * a, float * b, float alpha, float beta, int size)
{
	Cuda_Log(a, b, alpha, beta, size);
}

extern "C" void Exp(float * a, float * b, float alpha, float beta, int size)
{
	Cuda_Exp(a, b, alpha, beta, size);
}


//#ifdef WIN32
//DLLEXP void __stdcall Tanh(float * a, float * b, int size)
//#else
extern "C" void Tanh(float * a, float * b, int size)
//#endif
{
	Cuda_Tanh(a, b, size);
}

extern "C" void Sqrt(float * a, float * b, float alpha, float beta, int length)
{
	Cuda_Sqrt(a, b, alpha, beta, length);
}

//#ifdef WIN32
//DLLEXP void __stdcall ReLU(float * a, float * b, int size)
//#else
extern "C" void ReLU(float * a, float * b, int size)
//#endif
{
	Cuda_ReLU(a, b, size);
}

//#ifdef WIN32
//DLLEXP void __stdcall Deriv_Sigmoid(float * delta, float * layer_output, int batchsize, int m)
//#else
extern "C" void Deriv_Sigmoid(float * delta, float * layer_output, int batchsize, int m)
//#endif
{
	cuda_Deriv_Sigmoid(delta, layer_output, batchsize, m);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivSigmoid(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
//#else
extern "C" void DerivSigmoid(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
//#endif
{
	Cuda_DerivSigmoid(inputDeriv, outputDeriv, output, batchsize, m, wei_a);
}

//#ifdef WIN32
//DLLEXP void __stdcall Deriv_Tanh(float * delta, float * layer_output, int batchsize, int m)
//#else
extern "C" void Deriv_Tanh(float * delta, float * layer_output, int batchsize, int m)
//#endif
{
	cuda_Deriv_Tanh(delta, layer_output, batchsize, m);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivTanh(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
//#else
extern "C" void DerivTanh(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
//#endif
{
	Cuda_DerivTanh(inputDeriv, outputDeriv, output, batchsize, m, wei_a);
}

//#ifdef WIN32
//DLLEXP void __stdcall Deriv_Rectified(float * delta, float * layer_output, int batchsize, int m)
//#else
extern "C" void Deriv_Rectified(float * delta, float * layer_output, int batchsize, int m)
//#endif
{
	cuda_Deriv_Rectified(delta, layer_output, batchsize, m);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivRectified(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
//#else
extern "C" void DerivRectified(float * inputDeriv, float * outputDeriv, float * output, int batchsize, int m, float wei_a)
//#endif
{
	Cuda_DerivRectified(inputDeriv, outputDeriv, output, batchsize, m, wei_a);
}

///////////////////////////////////////////////////////////////////////////// MaxPooling Functions. 
//#ifdef WIN32
//DLLEXP void __stdcall Max_Pooling(float * pooling_feas, int * Smp_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension)
//#else
extern "C" void Max_Pooling(float * pooling_feas, int * Smp_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension)
//#endif
{
	cuda_Max_Pooling(pooling_feas, Smp_Index, batchsize, output, maxpooling_index, output_dimension);
}

//#ifdef WIN32
//DLLEXP void __stdcall MaxPoolingMask(float * pooling_feas, int * Smp_Index, int * Seq_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension)
//#else
extern "C" void MaxPoolingMask(float * pooling_feas, int * Smp_Index, int * Seq_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension)
//#endif
{
	Cuda_Max_Pooling_Mask(pooling_feas, Smp_Index, Seq_Index, batchsize, output, maxpooling_index, output_dimension);
}

extern "C" void Maxpooling1D(float * values, int row, int column, int batchsize, float * output, int * maxpooling_index)
{
	Cuda_Maxpooling1D(values, row, column, batchsize, output, maxpooling_index);
}

extern "C" void Meanpooling1D(float * values, int row, int column, int batchsize, float * output)
{
	Cuda_Meanpooling1D(values, row, column, batchsize, output);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivMaxPooling(float * deriv, int * maxpooling_index, float * pooling_deriv, int batchsize, int output_dimension, float alpha, float beta)
//#else
extern "C" void DerivMaxPooling(float * deriv, int * maxpooling_index, float * pooling_deriv, int batchsize, int output_dimension, float alpha, float beta)
//#endif
{
	Cuda_DerivMaxPooling(deriv, maxpooling_index, pooling_deriv, batchsize, output_dimension, alpha, beta);
}

///////////////////////////////////////////////////////////////////////////// Last Matrix from Sequence Matrixs. 
//#ifdef WIN32
//DLLEXP void __stdcall CalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix)
//#else
extern "C" void CalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix)
//#endif
{
	Cuda_CalculateLastMatrixs(InstanceIndex, batchSize, SeqMatrixs, dim, matrix);
}

//#ifdef WIN32
//DLLEXP void __stdcall CalculateLastMatrixsMask(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask)
//#else
extern "C" void CalculateLastMatrixsMask(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask)
//#endif
{
	Cuda_CalculateLastMatrixs(InstanceIndex, seqTransIdx, batchSize, SeqMatrixs, dim, matrix, matrixMask);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivCalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix, float alpha)
//#else
extern "C" void DerivCalculateLastMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int dim, float * matrix, float alpha)
//#endif
{
	Cuda_DerivCalculateLastMatrixs(InstanceIndex, batchSize, SeqMatrixs, dim, matrix, alpha);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivCalculateLastMatrixsMask(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask, float alpha)
//#else
extern "C" void DerivCalculateLastMatrixsMask(int * InstanceIndex, int * seqTransIdx, int batchSize, float * SeqMatrixs, int dim, float * matrix, float * matrixMask, float alpha)
//#endif
{
	Cuda_DerivCalculateLastMatrixs(InstanceIndex, seqTransIdx, batchSize, SeqMatrixs, dim, matrix, matrixMask, alpha);
}

//#ifdef WIN32
//DLLEXP void __stdcall GetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
//#else
extern "C" void GetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
//#endif
{
	Cuda_GetSeqOrderMatrixs(InstanceIndex, batchSize, SeqMatrixs, mapForward, dim, isReverse, order, matrix, alpha, beta);
}


//#ifdef WIN32
//DLLEXP void __stdcall GetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim, int winsize, float * matrix, float alpha, float beta)
//#else
extern "C" void GetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim, int winsize, float * matrix, float alpha, float beta)
//#endif
{
	Cuda_GetWindowMatrixs(InstanceMargin, sentsize, SeqMatrixs, mapForward, dim, winsize, matrix, alpha, beta);
}

//#ifdef WIN32
//DLLEXP void __stdcall SetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim, int winsize, float * matrix, float alpha, float beta)
//#else
extern "C" void SetWindowMatrixs(int * InstanceMargin, int sentsize, float * SeqMatrixs, int * mapForward, int dim, int winsize, float * matrix, float alpha, float beta)
//#endif
{
	Cuda_SetWindowMatrixs(InstanceMargin, sentsize, SeqMatrixs, mapForward, dim, winsize, matrix, alpha, beta);
}


//#ifdef WIN32
//DLLEXP void __stdcall SetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
//#else
extern "C" void SetSeqOrderMatrixs(int * InstanceIndex, int batchSize, float * SeqMatrixs, int * mapForward, int dim, int isReverse, int order, float * matrix, float alpha, float beta)
//#endif
{
	Cuda_SetSeqOrderMatrixs(InstanceIndex, batchSize, SeqMatrixs, mapForward, dim, isReverse, order, matrix, alpha, beta);
}


///////////////////////////////////////////////////////////////////////////// Elementwise Product Functions. 
//#ifdef WIN32
//DLLEXP void __stdcall ElementwiseProductMask(float * pLeft, float * pRight, float * pDst,
//#else
extern "C" void ElementwiseProductMask(float * pLeft, float * pRight, float * pDst,
//#endif
		int * leftmask, int * rightmask, int * dstmask,  int row, int column, float alpha, float beta)
{
	Cuda_ElementwiseProduct(pLeft, pRight, pDst, leftmask, rightmask, dstmask, row, column, alpha, beta);
}

//#ifdef WIN32
//DLLEXP void __stdcall ElementwiseProduct(float * pLeft, float * pRight, float * pDst, int row, int column, float alpha)
//#else
extern "C" int ElementwiseProduct(float * pLeft, float * pRight, float * pDst, int row, int column, float alpha)
//#endif
{
	//printf("Hello jjjjjjjjj.adsfasdfasdfasfsd");
	return Cuda_ElementwiseProduct(pLeft, pRight, pDst, row, column, alpha);
}

extern "C" int ElementwiseProductEx(float * pLeft, int strideLeft, int leftLen, float * pRight, int strideRight, int rightLen, float * pDst, int strideDst, int dstLen, float alpha, float beta)
{
	return Cuda_ElementwiseProduct(pLeft, strideLeft, leftLen, pRight, strideRight, rightLen, pDst, strideDst, dstLen, alpha, beta);
}


///////////////////////////////////////////////////////////////////////////// Cross Entropy Loss. 
//#ifdef WIN32
//DLLEXP void __stdcall CrossEntropyLoss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize)
//#else
extern "C" void CrossEntropyLoss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize)
//#endif
{
	cuda_CrossEntropyLoss(outputScore, outputLabel, outputDeriv, outputSize);
}

//#ifdef WIN32
//DLLEXP void __stdcall MultiClassSoftmax(float * outputScore, float * outputDeriv, int dim, float gamma, int batchSize)
//#else
extern "C" void MultiClassSoftmax(float * outputScore, float * outputDeriv, int dim, float gamma, int batchSize)
//#endif
{
	Cuda_MultiClassSoftmax(outputScore, outputDeriv, dim, gamma, batchSize);
}

//#ifdef WIN32
//DLLEXP void __stdcall SparseMutliClassSoftmax(int * smpIdx, float * outputScore, float * outputProb, float gamma, int batchSize)
//#else
extern "C" void SparseMutliClassSoftmax(int * smpIdx, float * outputScore, float * outputProb, float gamma, int batchSize)
//#endif
{
	Cuda_SparseMultiClassSoftmax(smpIdx, outputScore, outputProb, gamma, batchSize);
}
//#ifdef WIN32
//DLLEXP void __stdcall DerivSparseMultiClassSoftmax(int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize)
//#else
extern "C" void DerivSparseMultiClassSoftmax(int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize)
//#endif
{
	Cuda_DerivSparseMultiClassSoftmax(smpIdx, outputProb, probDeriv, outputDeriv, gamma, alpha, beta, batchSize);
}

///////////////////////////////////////////////////////////////////////////// K-Largest/Smallest Value. 
//#ifdef WIN32
//DLLEXP void __stdcall KLargestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, float * bestIndexes)
//#else
extern "C" void KLargestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, float * bestIndexes)
//#endif
{
	Cuda_KLargestValueBatch(float_array, batchSize, dim, K, mode, bestValues, bestIndexes);
}

extern "C" void Fast_KLargestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, int * bestIndexes, float * _bestValues, int * _bestIndexes)
{
	Cuda_Fast_KLargestValueBatch(float_array, batchSize, dim, K, mode, bestValues, bestIndexes, _bestValues, _bestIndexes);	
}

//

///////////////////////////////////////////////////////////////////////////// Vector Addition and Vector Scale.
//#ifdef WIN32
//DLLEXP void __stdcall Scale_Vector(float * gpu_floats_a, int m, float mweight)
//#else
extern "C" void Scale_Vector(float * gpu_floats_a, int m, float mweight)
//#endif
{
	cuda_Scale_Vector(gpu_floats_a, m, mweight);
}


//#ifdef WIN32
//DLLEXP void __stdcall Add_Vector(float * gpu_floats_a, float * gpu_float_b, int m, float awei, float bwei)
//#else
extern "C" void Add_Vector(float * gpu_floats_a, float * gpu_float_b, int m, float awei, float bwei)
//#endif
{
	cuda_Add_Vector(gpu_floats_a, gpu_float_b, m, awei, bwei);
}

extern "C" void Update_Vector(float * src, float * delta, int size, float decay)
{
	cuda_Update_Vector(src, delta, size, decay);
}

extern "C" void Init_Vector(float * gpu_floats_a, float b, int m, float awei)
{
	cuda_Init_Vector(gpu_floats_a, b, m, awei);
}



//#ifdef WIN32
//DLLEXP void __stdcall Matrix_Add(float * gpu_floats_a, float * gpu_floats_b, int m, int n, float mweight)
//#else
extern "C" void Matrix_Add(float * gpu_floats_a, float * gpu_floats_b, int m, int n, float mweight)
//#endif
{
	cuda_Matrix_Add(gpu_floats_a, gpu_floats_b, m, n, mweight);
}

//#ifdef WIN32
//DLLEXP void __stdcall Matrix_Addition(float * pLeft, float * pRight, float * pDst, int row, int column, float weiLeft, float weiRight, float weiDst)
//#else
extern "C" void Matrix_Addition(float * pLeft, float * pRight, float * pDst, int row, int column, float weiLeft, float weiRight, float weiDst)
//#endif
{
	Cuda_Matrix_Add(pLeft, pRight, pDst, row, column, weiLeft, weiRight, weiDst);
}

//#ifdef WIN32
//DLLEXP void __stdcall Matrix_AdditionMask(float * a, int * aMask, float * b, int * bMask, float * c, int * cMask, int dim, int batchSize, float awei, float bwei, float cwei)
//#else
extern "C" void Matrix_AdditionMask(float * a, int * aMask, float * b, int * bMask, float * c, int * cMask, int dim, int batchSize, float awei, float bwei, float cwei)
//#endif
{
	Cuda_Matrix_Add(a, aMask, b, bMask, c, cMask, dim, batchSize, awei, bwei, cwei);
}

//#ifdef WIN32
//DLLEXP void __stdcall SoftAttention_Matching(float * a, int * aMask, float * b, int * bMask, int dim, int a_func, int op, float * vec, float * c, int * cMask, int batchSize, float alpha, float beta)
//#else
extern "C" void SoftAttention_Matching(float * a, int * aMask, float * b, int * bMask, int dim, int a_func, int op, float * vec, float * c, int * cMask, int batchSize, float alpha, float beta)
//#endif
{
	Cuda_SoftAttention_Matching(a, aMask, b, bMask, dim, a_func, op, vec, c, cMask, batchSize, alpha, beta);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivSoftAttention_Matching(float * a, float * aDeriv, int * aMask, float * b, float * bDeriv, int * bMask,
//	int dim, int a_func, int op, float * vec, float * vecDeriv, float * c, float * cDeriv, int * cMask,
//	int * aSmpIdx, int * aElementIdx, int aSize, int * bSmpIdx, int * bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei)
//#else
extern "C" void DerivSoftAttention_Matching(float * a, float * aDeriv, int * aMask, float * b, float * bDeriv, int * bMask,
	int dim, int a_func, int op, float * vec, float * vecDeriv, float * c, float * cDeriv, int * cMask,
	int * aSmpIdx, int * aElementIdx, int aSize, int * bSmpIdx, int * bElementIdx, int bSize, int batchSize, float awei, float bwei, float vwei)
//#endif
{
	Cuda_DerivSoftAttention_Matching(a, aDeriv, aMask, b, bDeriv, bMask, dim, a_func, op, vec, vecDeriv, c, cDeriv, cMask, 
		aSmpIdx, aElementIdx, aSize, bSmpIdx, bElementIdx, bSize, batchSize, awei, bwei, vwei);
}


//#ifdef WIN32
//DLLEXP void __stdcall SoftAttention(float * a, float * b, int dim, int a_func, int op, float * vec, float * c, int aBatchSize, int bBatchSize, float alpha, float beta)
//#else
extern "C" void SoftAttention(float * a, float * b, int dim, int a_func, int op, float * vec, float * c, int aBatchSize, int bBatchSize, float alpha, float beta)
//#endif
{
	Cuda_SoftAttention(a, b, dim, a_func, op, vec, c, aBatchSize, bBatchSize, alpha, beta);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivSoftAttention(float * a, float * aDeriv, float * b, float * bDeriv, int dim, int a_func, int op, float * vec, float * vecDeriv,
//	float * c, float * cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei)

//#else
extern "C" void DerivSoftAttention(float * a, float * aDeriv, float * b, float * bDeriv, int dim, int a_func, int op, float * vec, float * vecDeriv,
	float * c, float * cDeriv, int aBatchSize, int bBatchSize, float awei, float bwei, float vwei)
//#endif
{
	Cuda_DerivSoftAttention(a, aDeriv, b, bDeriv, dim, a_func, op, vec, vecDeriv, c, cDeriv, aBatchSize, bBatchSize, awei, bwei, vwei);
}


//#ifdef WIN32
//DLLEXP void __stdcall Matrix_AdditionEx(float * a, int skipa, float * b, int skipB, float * c, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
//#else
extern "C" void Matrix_AdditionEx(float * a, int skipa, float * b, int skipB, float * c, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
//#endif
{
	Cuda_Matrix_Add(a, skipa, b, skipB, c, skipC, dim, batchSize, awei, bwei, cwei);
}


//#ifdef WIN32
//DLLEXP void __stdcall Matrix_AdditionExMask(float * a, int * aMask, int skipA, float * b, int * bMask, int skipB, float * c, int * cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
//#else
extern "C" void Matrix_AdditionExMask(float * a, int * aMask, int skipA, float * b, int * bMask, int skipB, float * c, int * cMask, int skipC, int dim, int batchSize, float awei, float bwei, float cwei)
//#endif
{
	Cuda_Matrix_Add(a, aMask, skipA, b, bMask, skipB, c, cMask, skipC, dim, batchSize, awei, bwei, cwei);
}


//#ifdef WIN32
//DLLEXP void __stdcall Scale_Matrix(float * gpu_floats_a, int m, int n, float mweight)
//#else
extern "C" void Scale_Matrix(float * gpu_floats_a, int m, int n, float mweight)
//#endif
{
	cuda_Scale_Matrix(gpu_floats_a, m, n, mweight);
}

//#ifdef WIN32
//DLLEXP void __stdcall Scale_MatrixMask(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei, float bwei)
//#else
extern "C" void Scale_MatrixMask(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei, float bwei)
//#endif
{
	cuda_Scale_Matrix(a, aMask, b, bMask, dim, batchSize, awei, bwei);
}

//#ifdef WIN32
//DLLEXP void __stdcall Accurate_Scale_Matrix(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei)
//#else
extern "C" void Accurate_Scale_Matrix(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei)
//#endif
{
	cuda_Accurate_Scale_Matrix(a, aMask, b, bMask, dim, batchSize, awei);
}

//#ifdef WIN32
//DLLEXP void __stdcall AccurateElementwiseProduct(float * pLeft, float * pRight, float * pDst, int * leftmask, int * rightmask, int * dstmask, int row, int column, float beta)
//#else
extern "C" void AccurateElementwiseProduct(float * pLeft, float * pRight, float * pDst, int * leftmask, int * rightmask, int * dstmask, int row, int column, float beta)
//#endif
{
	Cuda_AccurateElementwiseProduct(pLeft, pRight, pDst, leftmask, rightmask, dstmask, row, column, beta);
}

///////////////////////////////////////////////////////////////////////////// Matrix Columnwise Sum.
//#ifdef WIN32
//DLLEXP void __stdcall ColumnWiseSum(float *matrix, float * result, int row, int col, float wei)
//#else
extern "C" void ColumnWiseSum(float *matrix, float * result, int row, int col, float wei)
//#endif
{
	Cuda_ColumnWiseSum(matrix, result, row, col, wei);
}

//#ifdef WIN32
//DLLEXP void __stdcall Matrix_Aggragate_Weight(float * a, float * b, int batchsize, int m, float weight)
//#else
extern "C" void Matrix_Aggragate_Weight(float * a, float * b, int batchsize, int m, float weight)
//#endif
{
	cuda_Matrix_Aggragate_Weight(a, b, batchsize, m, weight);
}

//#ifdef WIN32
//DLLEXP void __stdcall ColumnWiseSumMask(float * matrix, int * matrixMask, float * weight,
//#else
extern "C" void ColumnWiseSumMask(float * matrix, int * matrixMask, float * weight,
//#endif
		int * smpIdx, int batchSize,
		float * output, int * outputMask,
		int row, int col,
		float alpha, float beta)
{
	Cuda_ColumnWiseSum(matrix, matrixMask, weight, smpIdx, batchSize, output, outputMask, row, col, alpha, beta);
}

//#ifdef WIN32
//DLLEXP void __stdcall ColumnWiseSumMaskV2(float * matrix, int * matrixMask, float * weight,
//#else
extern "C" void ColumnWiseSumMaskV2(float * matrix, int * matrixMask, float * weight,
//#endif
		int * smpIdx, int batchSize,
		float * output, int * outputMask,
		int row, int col,
		float alpha, float beta)
{
	Cuda_ColumnWiseSumV2(matrix, matrixMask, weight, smpIdx, batchSize, output, outputMask, row, col, alpha, beta);
}

//#ifdef WIN32
//DLLEXP void __stdcall ColumnWiseSumMaskV3(float * matrix, int * matrixMask, float * weight,
//#else
extern "C" void ColumnWiseSumMaskV3(float * matrix, int * matrixMask, float * weight,
//#endif
	int skipMatrix, 
	int * smpIdx, int batchSize,
	float * output, int * outputMask,
	int skipOutput,
	int row, int col,
	float alpha, float beta)
{
	Cuda_ColumnWiseSumV3(matrix, matrixMask, weight, skipMatrix, smpIdx, batchSize, output, outputMask, skipOutput, row, col, alpha, beta);
}


//#ifdef WIN32
//DLLEXP void __stdcall CudaClipAdamUpdate(
//#else
extern "C" void CudaClipAdamUpdate(
//#endif
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,
		int iter,
		int CudaSize)
{
	Cuda_ClipAdamUpdate(gpu_floats_Gradient,gpu_floats_M,gpu_floats_V,gpu_floats_Parameter,Beta1,Beta2,Epsilon,updateRate,weightClip,gradClip,decay,iter,CudaSize);
}

extern "C" void ClipAdamBertUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,
		int iter,
		int CudaSize)
{
	Cuda_ClipAdamBertUpdate(gpu_floats_Gradient, gpu_floats_M, gpu_floats_V, gpu_floats_Parameter, Beta1, Beta2,Epsilon,updateRate,weightClip,gradClip,decay,iter,CudaSize);
}

extern "C" void ClipAdamDeltaUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float * gpu_orig_Parameter,

		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,

		int iter,
		int CudaSize)
{
	Cuda_ClipAdamDeltaUpdate(gpu_floats_Gradient, gpu_floats_M, gpu_floats_V, gpu_floats_Parameter, gpu_orig_Parameter, Beta1, Beta2, Epsilon, updateRate, weightClip, gradClip, decay, iter, CudaSize);
}

//#ifdef WIN32
//DLLEXP int __stdcall MPI_Initialization()
//{
	//To do
//	return 0;
//}
//#else
// extern "C" int MPI_Initialization(int deviceId)
// {
//   ncclResult_t ret;
//   struct timeval start,end;
//   int namelen;
//   char processor_name[32];
//   int a = 1;
//   char **s;
//   MPI_Init(&a,&s);
//   MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
//   MPI_Comm_rank(MPI_COMM_WORLD,&rank);
//   MPI_Get_processor_name(processor_name,&namelen);

//   cudaSetDevice(deviceId);

//   ncclGetUniqueId(&commId);
//   MPI_Bcast(&commId, NCCL_UNIQUE_ID_BYTES, MPI_CHAR, 0, MPI_COMM_WORLD);
//   ret = ncclCommInitRank(&comm, numprocs, commId, rank);

//   split_init(r_len);
//   cudaMalloc(&d_result,sizeof(float) * (int)(r_len * ratio));
//   cudaMalloc(&d_id,sizeof(int) * (int)(r_len * ratio));
//   cudaMalloc(&d_cnt,sizeof(int));
//   cudaMalloc(&d_R_gather,sizeof(float) * (int)(r_len * ratio * numprocs));
//   cudaMalloc(&d_id_gather,sizeof(int) * (int)(r_len * ratio * numprocs));
//   cudaMalloc(&d_cnt_gather,sizeof(int) * (int)(numprocs));
//   h_cnt_gather = (int*)malloc(sizeof(int) * (int)(numprocs));
//   if (ret != ncclSuccess)
//   {
// 	printf("NCCL Init failed (%d) '%s'\n", ret, ncclGetErrorString(ret));
// 	exit(1);
//   }
//   else
// 	printf("NCCL Initialization success\n");
//   return rank;
// }
//#endif

//#ifdef WIN32
//DLLEXP void __stdcall MPI_Aggregation2(float *gpu_floats_weight, int m)
//{
//}
//#else
// extern "C" void MPI_Aggregation2(float *gpu_floats_weight,int m)
// {
//   cudaDeviceSynchronize();
//   cudaStream_t stream;
//   cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);

//   ncclAllReduce((void*)(gpu_floats_weight), (void*)(gpu_floats_weight),m, ncclFloat, ncclSum, comm, stream);
//   cudaStreamSynchronize(stream);
// }
//#endif

//#ifdef WIN32
//DLLEXP void __stdcall MPI_Aggregation(float *gpu_floats_weight, int m)
//{

//}
//#else
// extern "C" void MPI_Aggregation(float *gpu_floats_weight,int m)
// {
//   cudaDeviceSynchronize();
//   split(gpu_floats_weight,d_result,d_id,m,d_cnt);

//   cudaStream_t stream;
//   cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);

//   cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);
//   ncclAllGather((void*)d_cnt,1,ncclInt,d_cnt_gather,comm,stream);
//   cudaStreamSynchronize(stream);

//   cudaMemcpy(h_cnt_gather,d_cnt_gather,sizeof(int) * numprocs,cudaMemcpyDeviceToHost);

//   int max_cnt = h_cnt_gather[0];
//   for(int i = 1; i < numprocs; ++i)
// 	max_cnt = max(max_cnt,h_cnt_gather[i]);

//   ncclAllGather((void*)d_result,max_cnt,ncclFloat,d_R_gather,comm,stream);
//   cudaStreamSynchronize(stream);

//   cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);
//   ncclAllGather((void*)d_id,max_cnt,ncclInt,d_id_gather,comm,stream);
//   cudaStreamSynchronize(stream);

//   update(gpu_floats_weight,d_id_gather,d_R_gather,d_cnt_gather,numprocs,max_cnt,rank);
// }
//#endif

//#ifdef WIN32
//DLLEXP void __stdcall UCTStateSampling(
//#else
extern "C" void UCTStateSampling(
//#endif
	float * count,
	float * success,
	float * status,
	int statusNum,
	float * weight,
	int weightLen,
	float C)
{
	Cuda_UCTStateSampling(count, success, status, statusNum, weight, weightLen, C);
}

//#ifdef WIN32
//DLLEXP void __stdcall BanditStateUpdating(
//#else
extern "C" void BanditStateUpdating(
//#endif
	float * fail, float * success, int * status, int armNum, int banditNum, float f, float s, float alpha)
{
	Cuda_BanditStateUpdating(fail, success, status, armNum, banditNum, f, s, alpha);
}

//#ifdef WIN32
//DLLEXP void __stdcall SpanMaxPool(
//#else
extern "C" void SpanMaxPool(
//#endif
	float* inputSent, int* smpIdx, float* start, float* end, int* sentMargin, int matchSize, float* outputSent, int* outMaxIndex, int dim)
{
	Cuda_SpanMaxPool(inputSent, smpIdx, start, end, sentMargin, matchSize, outputSent, outMaxIndex, dim);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivSpanMaxPool(
//#else
extern "C" void DerivSpanMaxPool(
//#endif 
		float * outputSentDeriv, int * smpIdx, int batchSize, float * inputSentDeriv, int * outMaxIndex, int dim, float beta)
{
	Cuda_DerivSpanMaxPool(outputSentDeriv, smpIdx, batchSize, inputSentDeriv, outMaxIndex, dim, beta);
}

//#ifdef WIN32
//DLLEXP void __stdcall SpanLastPool(
//#else
extern "C" void SpanLastPool(
//#endif 
	float * inputSent, int * smpIdx, float * start, float * end, int * sentMargin, int matchSize, float * outputSent, int dim)
{
	Cuda_SpanLastPool(inputSent, smpIdx, start, end, sentMargin, matchSize, outputSent, dim);
}
//#ifdef WIN32
//DLLEXP void __stdcall DerivSpanLastPool(
//#else
extern "C" void DerivSpanLastPool(
//#endif 
	float * outputSentDeriv, int * outSmpIdx, float * start, float * end, int batchSize, float * inputSentDeriv, int * inSmpIdx, int dim, float beta)
{
	Cuda_DerivSpanLastPool(outputSentDeriv, outSmpIdx, start, end, batchSize, inputSentDeriv, inSmpIdx, dim, beta);
}

//#ifdef WIN32
//DLLEXP void __stdcall NDArrayTranspose(
//#else
extern "C" void NDArrayTranspose(float * input, int * indim, int * transDim, float * output, int dimNum, int length, float alpha, float beta)
{
	Cuda_NDArrayTranspose(input, indim, transDim, output, dimNum, length, alpha, beta);
}

extern "C" void NDArraySliceBackward(float * outputDeriv, int dimNum, int * offset, int * outdim, float * inputDeriv, int * indim, int length, float alpha, float beta)
{
	Cuda_NDArraySliceBackward(outputDeriv, dimNum, offset, outdim, inputDeriv, indim, length, alpha, beta);
}

extern "C" void NDArraySliceForward(float * input, int dimNum, int * indim, float * output, int * offset, int * outdim, int length, float alpha, float beta)
{
	Cuda_NDArraySliceForward(input, dimNum, indim, output, offset, outdim, length, alpha, beta);
}

extern "C" void NDArray2SegForward(float * input, int dim, int max_seq_len, int batch_size, int * sampleIdx, int * sentMargin, int sent_size, float * output)
{
	Cuda_NDArray2SegForward(input, dim, max_seq_len, batch_size, sampleIdx, sentMargin, sent_size, output);
}

extern "C" void NDArray2SegBackward(float * inputDeriv, int dim, int max_seq_len, int batch_size, int * sampleIdx, int * sentMargin, int sent_size, float * outputDeriv, float beta)
{
	Cuda_NDArray2SegBackward(inputDeriv, dim, max_seq_len, batch_size, sampleIdx, sentMargin, sent_size, outputDeriv, beta);
}



extern "C" void NDArrayMaxBackward(float * input, int * sparse_index, int * indim, float * output, int * outDim, int max_dim, int dimNum, int length, float beta)
{
	Cuda_NDArrayMaxBackward(input, sparse_index, indim, output, outDim, max_dim, dimNum, length, beta);
}


//#ifdef WIN32
//DLLEXP void __stdcall DerivLogSparseSoftmax(
//#else
extern "C" void DerivLogSparseSoftmax(
//#endif 
	int * smpIdx, float * outputProb, float * probDeriv, float * outputDeriv, float gamma, float alpha, float beta, int batchSize)
{
	Cuda_DerivLogSparseMultiClassSoftmax(smpIdx, outputProb, probDeriv, outputDeriv, gamma, alpha, beta, batchSize);
}

//#ifdef WIN32
//DLLEXP void __stdcall DerivLogProbEntropy(
//#else
extern "C" void DerivLogProbEntropy(int * smpIdx, float * outputProb, float * logProbDeriv, float alpha, float beta, float epislon, int batchSize)
{
	Cuda_DerivLogProbEntropy(smpIdx, outputProb, logProbDeriv, alpha, beta, epislon, batchSize);
}

extern "C" void DerivProbEntropy(float * probDeriv, float * prob, float alpha, float beta, float epislon, int dim, int batchSize)
{
	Cuda_DerivProbEntropy(probDeriv, prob, alpha, beta, epislon, dim, batchSize);
}


extern "C" void* CudaCreateCuSparse()
//#endif
{
	cusparseHandle_t hCusparse = NULL;
	cusparseCreate(&hCusparse);
	return hCusparse;
}

//#ifdef WIN32
//DLLEXP void __stdcall CudaDestroyCuDnn(cudnnHandle_t handle)
//#else
extern "C" void CudaDestroyCuSparse(cusparseHandle_t handle)
//#endif
{
	cusparseDestroy(handle);
}

extern "C" void SparseVectorAdd(cusparseHandle_t handle, int * idx, float * value, float * dst, float beta, int length)
{
	cusparseSaxpyi(handle, length, &beta, value, idx, dst, CUSPARSE_INDEX_BASE_ZERO);
}

//      y[xInd[i]-idxBase] = c * y[xInd[i]-idxBase] - s*xVal[i]
//		x[i]               = c * xVal[i]            + s * y[xInd[i]-idxBase]
extern "C" void SparseVectorGivens(cusparseHandle_t handle, int * idx, float * value, float * dst, float alpha, float beta, int length)
{
	cusparseSroti(handle, length, value, idx, dst, &alpha, &beta, CUSPARSE_INDEX_BASE_ZERO);
}

extern "C" void SparseVectorScatter(cusparseHandle_t handle, int * idx, float * value, float * dst, int length)
{
 	cusparseSsctr(handle, length, value, idx, dst, CUSPARSE_INDEX_BASE_ZERO);
}

extern "C" void SparseGthr(cusparseHandle_t handle, int * idx, float * value, float * src, int length)
{
	cusparseSgthr(handle, length, src, value, idx, CUSPARSE_INDEX_BASE_ZERO);
}


extern "C" void GraphNeighborNum(int * srcIdx, int batchSize, int * graphIdx, int * graphNeiNodes, int * graphNeiRels, 
							int * maskSrc, int * maskRel, int * maskInvRel, int * maskTgt, int * neiNum, int maskType)
{
	Cuda_GraphNeighborNum(srcIdx, batchSize, graphIdx, graphNeiNodes, graphNeiRels, 
							maskSrc, maskRel, maskInvRel, maskTgt, neiNum, maskType);
}


extern "C" void GraphNeighborInfo(int * srcIdx, int batchSize, int * graphIdx, int * graphNeiNodes, int * graphNeiRels, 
							int * maskSrc, int * maskRel, int * maskInvRel, int * maskTgt, int * neiNum, 
							int * neiNodes, int * neiRels, int * neiMargin, int defaultR, int maskType)
{
	Cuda_GraphNeighborInfo(srcIdx, batchSize, graphIdx, graphNeiNodes, graphNeiRels, 
							maskSrc, maskRel, maskInvRel, maskTgt, neiNum,
							neiNodes, neiRels, neiMargin, defaultR, maskType);
}

extern "C" void SgemmStrideBatched(cublasHandle_t handle, float * tensorA, int rowA, int colA, float * tensorB, int rowB, int colB, int batch, 
					float * tensorC, int transA, int transB, float alpha, float beta)
{

	// if (!transA && !transB)
	// {
	// 	/// c = beta A * B + alpha C
	// 	cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, batchsize, m, &beta, B, n, A, m, &alpha, C, n);
	// }
	// else if (!transA && transB)
	// {
	// 	/// c = beta op(A) * B + alpha C
	// 	cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, n, batchsize, m, &beta, B, m, A, m, &alpha, C, n);
	// }
	// else if (transA && !transB)
	// {
	// 	/// c = beta A * op(B) + alpha C
	// 	cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, m, batchsize, &beta, B, n, A, m, &alpha, C, n);
	// }
    
    if(transA == 0 && transB == 0)
    {
  		cublasSgemmStridedBatched(handle, CUBLAS_OP_N, CUBLAS_OP_N, colB, rowA, colA, &beta, 
  								tensorB, colB, rowB * colB,
  								tensorA, colA, rowA * colA,
  								&alpha,  
  								tensorC, colB, rowA * colB,
  								batch);
  	}
  	else if(transA == 0 && transB == 1)
  	{
  		cublasSgemmStridedBatched(handle, CUBLAS_OP_T, CUBLAS_OP_N, rowB, rowA, colA, &beta, 
  								tensorB, colB, rowB * colB,
  								tensorA, colA, rowA * colA,
  								&alpha,  
  								tensorC, rowB, rowB * rowA,
  								batch);
  	}
  	else if(transA == 1 && transB == 0)
  	{
  		cublasSgemmStridedBatched(handle, CUBLAS_OP_N, CUBLAS_OP_T, colB, colA, rowA, &beta, 
  								tensorB, colB, rowB * colB,
  								tensorA, colA, rowA * colA,
  								&alpha,  
  								tensorC, colB, colA * colB,
  								batch);
  	}
}


class CommState
{
public:
	int DevNum;
    int * Devs;
    cudaStream_t * CommStream;
    ncclComm_t * Comms;
    
	CommState(int * devs, int num)
	{
		Devs = devs;
		DevNum = num;
		Comms = (ncclComm_t *) malloc(sizeof(ncclComm_t) * DevNum);
		CommStream = (cudaStream_t *) malloc(sizeof(cudaStream_t) * DevNum);

		NCCLCHECK(ncclCommInitAll(Comms, DevNum, Devs));

		for(int i = 0; i < DevNum; i++)
		{
			cudaSetDevice(Devs[i]);
			cudaStreamCreate(&CommStream[i]);
		}
	}

	~CommState()
	{
		free(Comms);
		free(CommStream);
	}
};


//extern "C" void * RegisterDropout(cudnnHandle_t handle, int n, int c, int h, int w, float drop)
//	DropoutState * p = new DropoutState(handle, n, c, h, w, drop);
//	return (void *)(p);

extern "C" void * CommInitAll(int * devIds, int devNum)
{
	CommState * c = new CommState(devIds, devNum);
	return (void *) c;
}

//extern "C" void UnregisterDropout(void * state)
//{
//	DropoutState * p = (DropoutState *)state;
//	delete p;
//}

extern "C" void AllReduce(float * data, int size, int idx, void * commState)
{
	CommState * c = (CommState *)commState; // new CommState(devIds, devNum);  //(DropoutState *)state;

	//cudaSetDevice(c->Devs[idx]);
	NCCLCHECK(ncclAllReduce((const void *) data, (void *)data, size, ncclFloat, ncclSum, c->Comms[idx], c->CommStream[idx]));
	//cudaSetDevice(c->Devs[idx]);
	cudaStreamSynchronize(c->CommStream[idx]);
}

extern "C" void Bcast(float * data, int size, int root, int idx, void * commState)
{
	CommState * c = (CommState *)commState;
	NCCLCHECK(ncclBcast((void *) data, size, ncclFloat, root, c->Comms[idx], c->CommStream[idx]));	
	cudaStreamSynchronize(c->CommStream[idx]);
}


extern "C" void Gelu(float * x, float * y, int size)
{
	Cuda_Gelu(x, y, size);
}

extern "C" void DerivGelu(float * x, float * y, float * dy, float * dx, int size)
{
	Cuda_deriv_Gelu(x, y, dy, dx, size);
}

extern "C" void Gumbel(float * x, float eps, int size)
{
	Cuda_Gumbel(x, eps, size);
}

extern "C" void TopK(float * input, int length, int k, float * output)
{
	CudaTopK(input, length, k, output);
}

extern "C" void NDArrayScatter(float * memory, int dimNum, int * indim, float * data, int * dimindex, int length, float mem_alpha, float mem_beta, float data_alpha, float data_beta)
{
	Cuda_NDArrayScatter(memory, dimNum, indim, data, dimindex, length, mem_alpha, mem_beta, data_alpha, data_beta);
}





