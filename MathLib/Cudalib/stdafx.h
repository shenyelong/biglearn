// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <iostream> 
#include <vector> 
#include <cuda_runtime.h> 
#include <cublas.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_surface_types.h>
#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>

#include "cublas_v2.h"
#pragma comment(lib, "cudart") 
#pragma comment(lib,"cublas.lib")

//typedef amp_tausworthe_hybrid_collection<2> amp_rng;
// TODO: reference additional headers your program requires here
#define DROPOUT_CONST (3e38f)

#ifdef WIN32
#define DLLEXP extern "C" __declspec(dllexport)
#endif
// TODO: reference additional headers your program requires here
#define DEFAULT_THREAD_PER_BLOCK    512     // default number of threads per block 
#define DEFAULT_THREAD_PER_DIM		32
#define DEFAULT_THREAD_PER_DIM_3D	8
#define MAX_BATCH_SIZE              256
#define MAX_THREAD_NUM			1024
#define MAX_BLOCK_NUM			65536
#define COL_FOLD_NUM			4096



void split_init(int m);
void split(float *d_R,float *d_result,int *d_id,int r_len,int *cnt);
void update(float *gpu_floats_weight,int *id,float *delta,int *cnt,int numprocs,int offset,int rank);

void Cuda_ClipAdamUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,
		int iter,
		int CudaSize);

void Cuda_ClipAdamBertUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,
		int iter,
		int CudaSize);

void Cuda_ClipAdamDeltaUpdate(
		float * gpu_floats_Gradient,
		float * gpu_floats_M,
		float * gpu_floats_V,
		float * gpu_floats_Parameter,
		float * gpu_orig_Parameter,

		float Beta1,
		float Beta2,
		float Epsilon,
		float updateRate,
		float weightClip,
		float gradClip,
		float decay,

		int iter,
		int CudaSize);


void cublas_Init();
void cublas_Destroy();
void cublas_Sasum(float *x, int len, int norm, float * result);
void cublas_Matrix_Multipy(float * delta, float * weight, float * delta_low, int batchsize, int m, int n, int inverse);


int Cuda_Max(int * int_array, int len);
void Cuda_Init(float * float_array, int len, float initValue);
void Cuda_KLargestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, float * bestIndexes);
void Cuda_Fast_KLargestValueBatch(float * float_array, int batchSize, int dim, int K, int mode, float * bestValues, int * bestIndexes, float * _bestValues, int * _bestIndexes);

void cuda_Matrix_Add(float * gpu_floats_a, float * gpu_floats_b, int m, int n, float weight);
void cuda_Scale_Matrix(float * gpu_floats_a, int m, int n, float mweight);
void cuda_Scale_Matrix(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei, float bwei);
void cuda_Accurate_Scale_Matrix(float * a, int * aMask, float * b, int * bMask, int dim, int batchSize, float *awei);

void cuda_Matrix_Add_Tanh(float * gpu_floats_a, float * gpu_floats_b, int m, int n);

void cuda_Deriv_Cosine(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps);
void cuda_Deriv_Cosine_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps);


void cuda_Derive_Cosine_Linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps);
void cuda_Deriv_Cosine_Linear_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps);

void cuda_Deriv_InnerProduct_Linear_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps);
void cuda_Deriv_InnerProduct_Linear(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps);

void cuda_Derive_Cosine_Rectified(float * q, float * d, float * dcq, float * dcd, int batchsize, int m, float eps);
void cuda_Deriv_Cosine_Rectified_EX(float * q, float * d, int * neg_list, float * dcq, float * dcd, int batchsize, int m, float eps);


void cuda_Cosine_Similarity(float * a, float * b, float * c, int nTrial, int BATCHSIZE, int mindex,
		int batchsize, int dimension, float eps);
void cuda_Cosine_Similarity_EX(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int mindex,
		int batchsize, int dimension, float eps);
void cuda_Inner_Product_EX_Full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, 
		int batchsize, int dimension, float eps);

void cuda_Calculate_Alpha(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma);
void cuda_Calculate_Alpha_MXE(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma);
void cuda_Calculate_Alpha_PAIRRANK(float * alpha, int nTrial, int BATCHSIZE, int batchsize, float gamma);
void cuda_Calculate_Alpha_NCE(float * alpha, float * dist, int nTrial, int BATCHSIZE, int batchsize, float gamma);
void cuda_Calculate_Alpha_NCE2(float * alpha, float * dist, int nTrial, int BATCHSIZE, int batchsize, float gamma);
void cuda_FillOut_Dist_NCE(float* dist, int* neg_list, int nTrailPlus1, int BATCH_SIZE, int mindex, int batchsize);
void Cuda_Calculate_SampleLRDeriv(float * output, float * deriv, int trial, int BATCHSIZE, float * label, int batchsize, float gamma);


void cuda_Matrix_Product(float * a, float * b, float * c, int batchsize, int m, int n); //, int kept, float * alpha, int ntrial, int BATCH_SIZE, int alpha_index);

void cuda_SEQ_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize, float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size);

void cuda_SEQ_Sparse_Matrix_Multiply_INT(int * Smp_Index, int batchsize, int * Seg_Index, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize, float * mul_weight, float * output, int Feature_dimension, int output_dimension);

void cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size);

void cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INT_Weight(int * Smp_Index, int batchsize, int * Seg_Index, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize, float * mul_weight, float * output, int Feature_dimension, int output_dimension, float weight);

void cuda_Matrix_Rectified_Vector(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension);

void cuda_Convolution_Sparse_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, 
		int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize, float * con_weight, float * output, int Feature_dimension, int output_dimension, int win_size);

void cuda_Max_Pooling(float * pooling_feas, int * Smp_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension);


/// Gaussian and Rectified Memory Function. 
void Cuda_RectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize,
		float gamma, int * locationIdx, float * locationValue);

void Cuda_Gaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, float gamma, float * locationValue);


void Cuda_DerivRectifiedGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx, int windowSize,
		float gamma, int * locationIdx, float * locationValue, float * locationForcusDeriv, float * locationDeriv);

void Cuda_DerivGaussian(float * locationForcusValue, int ensembleLen, int batchSize, int * smpIdx,
		float gamma, float * locationValue, float * locationForcusDeriv, float * locationDeriv);


/// Windows and Gaussian Based Memory Addressing.
void Cuda_MemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValue, int ensembleLen,
		int windowSize, int memoryVecDim, float * memoryOutput);

void Cuda_GaussianMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, float * locationValue, int ensembleLen, int memoryVecDim, float * memoryOutput);

void Cuda_DerivGaussianMemoryAddressing(int * sentMargin, int sequenceSize, float * memoryInput, float * locationValueDeriv,
		int memoryVecDim, float * memoryOutputDeriv);

void Cuda_DerivMemoryAddressing(int * smpIdx, int batchSize, float * memoryInput, int * locationIdx, float * locationValueDeriv,
		int ensembleLen, int windowSize, int memoryVecDim, float * memoryOutputDeriv);

void Cuda_DerivMemory(int * smpIdx, int batchSize, float * memoryInputDeriv, int * locationIdx, float * locationValue,
		int ensembleLen, int windowSize, int memoryVecDim, float * memoryOutputDeriv);

void Cuda_DerivGaussianMemory(int * sentMargin, int sequenceSize, float * memoryInputDeriv, float * locationValue,
		int ensembleLen, int memoryVecDim, float * memoryOutputDeriv);



void cuda_Convolution_Sparse_Matrix_Product_INTEX(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension);
//,float * alpha, int ntrial, int BATCH_SIZE, int alpha_index);

void cuda_Matrix_WeightAdd(float * gpu_floats_a, float * gpu_floats_b, int batchsize, int dimension, float * mweight, int start, int keep);

void cuda_Matrix_WeightAdd_EX(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int batchsize, int dimension, float * mweight, int start, int keep);

void cuda_Sparse2Dense_Matrix(int * Smp_Idx, int * Fea_Idx, float * Fea_Value, float * matrix, int batchsize, int outputDimension);

void cuda_Matrix_Aggragate(float * a, float * b, int batchsize, int m);

void cuda_Matrix_Add_OFFSET(float * gpu_floats_a, int offset_a, float * gpu_floats_b, int offset_b, int len, float mweight);

void cuda_Cosine_Similarity_EX_Full(float * a, float * b, int * neg_list, float * c, int nTrial, int BATCHSIZE, int batchsize, int dimension, float eps);

void cuda_FillOut_Dist_NCE_Full(float* dist, int* neg_list, int nTrail, int BATCH_SIZE, int batchsize);

void cuda_Deriv_Cosine_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

void cuda_Deriv_Cosine_Linear_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

void cuda_Deriv_Cosine_Rectified_EX_Full(float * q, float * d, int * neg_list, float * dcq, float * dcd, int nTrail, int BATCHSIZE, int batchsize, int m, float eps);

void cuda_Matrix_WeightAdd_Full(float * gpu_floats_a, float * gpu_floats_b, int nTrail, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep);

void cuda_Matrix_WeightAdd_EX_Full(float * gpu_floats_a, float * gpu_floats_b, int * inver_neg_index, int * inver_neg_value, int nTrial, int BATCHSIZE, int batchsize, int dimension, float * mweight, int start, int keep);

void cuda_Cosine_Similarity_SubSpace(float * a, float * b, float * c, int labelDim, int BATCHSIZE, int batchsize, int subspaceDim, float eps);



void cuda_Deriv_Cosine_Subspace(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int labelDim, int subspaceDim, float gamma, float eps);

void cuda_InnerProduct_Similarity(float * a, float * b, float * c, int batchsize, int dimension);

void cuda_Deriv_InnerProduct(float * q, float * d, float * dcq, float * dcd, float * alpha, int act_type, int batchsize, int Dim, float gamma, float eps);


void cuda_SEQ_Sparse_Matrix_Transpose_Multiply_INTEX_Weight(int * Smp_Index, int batchsize, int * Seg_Index, int * Seg_Margin, int seg_size, int * Fea_Index,
		float * Fea_Value, int elementsize,
		float * mul_weight, float * output, int Feature_dimension, int output_dimension, int win_size, float weight);


void cuda_Predict(int* SampleIdx, int* FeatureIdx, float* FeatureValue, float * result, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate);

void cuda_SGD_Train(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate);

void cuda_SGD_Train_New(int* SampleIdx, int* FeatureIdx, float* FeatureValue, int * Label, int BatchSize, float * L1Weight, float * L1Bias, float * L2Weight, float * L2Bias, int HiddenNum, float learnRate);

void cuda_Matrix_Mask(float * gpu_floats_a, int * gpu_mask, int batchsize, int dimension);

void cuda_Convolution_Sparse_Matrix_Product_INTEX_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate);

void cuda_Cosine_Similarity_Matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float * aa, float * bb, float eps);

void cuda_Joint_Sparse_Matrix_Multiply_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		int * ConSmp_Idx, int * ConFea_Idx, float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * Weight, float * Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim);

void cuda_Joint_Dense_Matrix_Multiply_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * Weight, float * Output, int OutputDimension, int IsSrc, int IsTgt, int IsSim);


void cuda_Matrix_Add_Sigmoid(float * gpu_floats_a, float * gpu_floats_b, int m, int n);


void cuda_SparseForward(int * inputSmpIdx, int * inputItemIdx, int batchSize, float * weight, float * output, int inputDim, int outputDim);

void cuda_SparseL0(int * outputSmpIdx, int * outputItemIdx, int batchSize, int itemNum, float * L0, float * output, int inputDim);

void cuda_SparseL1(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx, int * outputItemMapSmp, int batchSize, int inputItemNum, int outputItemNum,
		float * L1, float * output, int inputDim);

void cuda_SparseLk(float * hidden, int hiddenDim, int * outputSmpIdx, int * outputItemIdx, int batchSize, int outputItemNum, int * outputItemMapSmp,
		float * Lk, float * output);



void cuda_Sparse_Matrix_Multiply_INTEX_Weight(int * outputSmpIndex, int * outputItemIndex, float * outputDeriv, int batchsize,
		int outputItemNum, float * weight, float * hidden_deriv, int hiddenDim, float wei);


void cuda_SparseBackward(int * inputSmpIdx, int * inputItemIdx, int batchSize, int inputItemNum,
		float * deriv, int inputDim, int outputDim, float * output, float lr);

void cuda_SparseL0Update(int * outputSmpIdx, int * outputItemIdx, float * outputScore, int batchSize, int outputItemNum, float * L0, int inputDim, float lr);

void cuda_SparseL1Update(int * inputSmpIdx, int * inputItemIdx, int * outputSmpIdx, int * outputItemIdx,
		int * outputItemMapSmp, float * outputScore, int batchSize, int inputItemNum, int outputItemNum, float * L1, int inputDim, float lr);

void cuda_SparseL0Full(int batchSize, float * L0, float * output, int inputDim);

void cuda_SparseL1Full(int batchSize, float * L1, int * inputSmpIdx, int * inputItemIdx, float * outputScore, int inputDim);

void cuda_SparseLkFull(float * hidden, int hiddenDim, int batchSize, float * Lk, float * outputScore, int inputDim);

void cuda_SparseLkUpdate(int * outputSmpIdx, int * outputItemIdx, int * outputMapSample, float * outputDeriv, int batchSize,
		int outputItemNum, float * hidden, float * Lk, int hiddenDim, float lr);

void cuda_Sparse_SoftMax(int * outputSmpIdx, int * outputItemIdx, float * outputScore, float * outputLabel, int batchSize,
		int itemNum, float * deriv, float gamma, int isLog, float * loss);


void cuda_Scale_Vector(float * gpu_floats_a, int m, float mweight);
void cuda_Add_Vector(float * gpu_floats_a, float * gpu_float_b, int m, float awei, float bwei);
void cuda_Init_Vector(float * gpu_floats_a, float b, int m, float awei);
void cuda_Update_Vector(float * src, float * delta, int size, float decay);

void cuda_Ada_Gradient(float * ada, float * grad, int m);

void cuda_Joint_Backward_Sim(
		float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * simDeriv);

void cuda_Square_Matrix(float * a, int batchSize, int dim, float * aSquare);

void cuda_Joint_Backward_Src_Tgt(
		float * outputDeriv, int MatchSize, int outputDim,
		float * Weight, float * SimDeriv, 
		int * matchSrc, int * matchTgt,
		float * Src_Input, int SrcSize, int SrcDim,
		float * Tgt_Input, int TgtSize, int TgtDim,
		float * Sim, float * aa, float * bb, 
		int IsSrc, int IsTgt, int IsSim,
		int * SrcMatchIndex, int * SrcMatchElement,
		int * TgtMatchIndex, int * TgtMatchElement,
		float * Src_Deriv, float * Tgt_Deriv, float eps ,int SimType);

void cuda_Joint_Sparse_Matrix_Product_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		int * ConSmp_Idx, int * ConFea_Idx, float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * WeightUpdate, float * OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr);


void cuda_Inner_Product_Matching(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float eps);

void cuda_Inner_Product_v1(float * a, float * b, float * c, int * matchIdxA, int * matchIdxB,
		int aSize, int bSize, int matchSize, int dimension, float alpha, float beta);

void cuda_Joint_Dense_Matrix_Product_INTEX(
		float * Src_Input, int SrcSize, 
		float * Tgt_Input, int TgtSize, 
		int * Src_Match_Idx, int * Tgt_Match_Idx, float * Match_Score, int MatchSize,
		float * ConFea_Value,
		int SrcDimension, int TgtDimension, int ConDimension, int IsConScore,
		float * WeightUpdate, float * OutputDeriv, int OutputDimension, int IsSrc, int IsTgt, int IsSim, float lr);

void cuda_RegressionLoss(float * outputScore, float * outputLabel, float * outputDeriv, int outputSize);

void cuda_PairwiseRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison);

void cuda_ListwiseRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma);


void cuda_BayesianRatingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma);

void cuda_LambdaRankingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, int K);

void cuda_BinomialSampling(float* prob, int batchSize, int Dim, float * sample, int randomSeed);

void cuda_Sparse_Matrix_Aggragate_Weight(int * smpIdx, int * feaIdx,  float * feaValue, float * b, int batchsize, int m, float weight);

void cuda_ReconstructionLoss(float * outputScore, int * smpIdx, int * feaIdx, float * feaValue, float * loss, int batchSize, int dimension, float eps);

void cuda_SparseUpdateRBM(int * outputSmpIdx, int * outputItemIdx, float * outputItemLabel, int batchSize, float * weight, 
		float * output,  float * bias_v, int inputDim, int outputDim,
		int * inputSmpIdx, int * inputItemIdx,
		float * grad_bias_v, float * grad_bias_h, float * grad_vh, float vStep, float hStep, float vhStep,
		float * loss, float eps);


void cuda_SparsePredictRBM(float * outputScore, int batchSize, float * weight, 
		float * output,  float * bias_v, int inputDim, int outputDim);


void cuda_Convolution_Dense_Matrix_Multiply_INTEX(int * Smp_Index, int batchsize, int * Seg_Margin, 
		int Seg_size, float * Fea_Value, int Feature_dimension, 
		float * con_weight, float * output,  int output_dimension, int win_size);

void cuda_Convolution_Dense_Matrix_Product_INTEX_Weight(float * deriv, int * maxpooling_index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate);


void cuda_Conv_Matrix_Multipy(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
		int * sentMargin, int * layerPoolingIndex, int neuralOut, int neuralIn, int winSize);

void cuda_Copy(float * gpu_floats_a, float * gpu_float_b, int m, int aIdx, int bIdx);

void cuda_ImageConvolution(float * inputImage, int batchSize, int width, int height, int depth, 
		float * filter, int sx, int c, int output, int pad, int stride,
		float * poolingOutput, int poolingWidth, int poolingHeight);

void cuda_MaxImagePooling(float * inputImage, int batchSize, int width, int height, int depth,
		int * maxpoolingIndex,
		int sx, int stride,
		float * output, int outputWidth, int outputHeight);

void cuda_ImageConvMatrixMultipy(float * outputDeriv, int * maxpoolingIndex,
		int batchSize, int outputWidth, int outputHeight, int outputDepth,
		int poolingSX, int poolingStride,
		float * filter, int sx, int c, int pad, int stride, 
		float * inputDeriv, int width, int height, int depth);

void cuda_Matrix_Add_Image_FType(float * gpu_floats_a, float * gpu_floats_b, int batchSize, int width, int height, int depth, int af);

void cuda_Deriv_Image_FType(float * delta, float * layer_output, int batchSize, int width, int height, int depth, int af);

void cuda_Matrix_Image_Aggragate_Weight(float * a, float * b, int batchSize, int width, int height, int depth, float weight);

void cuda_Convolution_Image_Matrix_Product_INTEX_Weight(float * outputDeriv, int * maxpoolingIndex, 
		int batchsize, int outputWidth, int outputHeight, int outputDepth,
		int poolingSX, int poolingStride, 
		float * grad, int sx, int c, int pad, int stride,
		float * input, int width, int height, int depth, float learnRate);

void cuda_Calculate_MSE(float * label, float * score, int length, float * result, int resultIdx);

#include "headers/cuda/lbfgs.h"
#include "headers/cuda/matrix.h"
#include "headers/cuda/bplda.h"
#include "headers/cuda/rnn.h"
#include "headers/cuda/lstm.h"
#include "headers/cuda/activation.h"
#include "headers/cuda/optlstm.h"
void cuda_Clip_Vector(float * gpu_floats_a, int m, float maxThreshold, float minThreshold);

void Cuda_LogBayesianRatingLoss(
		int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
		float * outputScore, float * outputLabel, float * outputDeriv, int outputSize, float eplison, float gamma);

void Cuda_Inner_Product_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps);
void Cuda_Cosine_Similarity_Full(float * a, float * b, int * neg_list, float * c, int nTrialplus1, int batchsize, int dimension, float eps);

void Cuda_Deriv_CosineSimilarity_Matching(float * q, float * d, float *qSquare, float * dSquare, int dim,
		int * src2MatchIdx, int * src2MatchElement, int * tgt2MatchIdx, int * tgt2MatchElement, int * srcIdx, int * tgtIdx, int srcSize, int tgtSize, int matchSize,
		float * simi, float * derivSimi, float * dcq, float * dcd, float eps);

void Cuda_Deriv_InnerProduct_Matching(float * q, float * d, int dim,
		int * src2MatchIdx, int * src2MatchElement, int * tgt2MatchIdx, int * tgt2MatchElement, int * srcIdx, int * tgtIdx, int srcSize, int tgtSize, int matchSize,
		float * derivSimi, float * dcq, float * dcd, float eps);

void Cuda_Calculate_SampleCrossEntropyDeriv(float * output, float * deriv, int dim, float * label, int batchsize, float gamma);

void Cuda_convolution_sparse_matrix_gradient(float * deriv, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * Fea_Value, float * grad, int Feature_Dimension, float learnRate);

void Cuda_convolution_dense_matrix_gradient(float * deriv, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, float * input, float * grad, int Feature_Dimension, float learnRate);

void Cuda_Convolution_Matrix_Transpose_Multiply(float * upperDeriv, float * con_weight, float * deriv, int batchSize, int sentSize,
		int * sentMargin, int neuralOut, int neuralIn, int winSize);

void cuda_ImageDenseConvMatrixMultipy(float * outputDeriv, //int * maxpoolingIndex,
		int batchSize, int outputWidth, int outputHeight, int outputDepth,
		//int poolingSX, int poolingStride,
		float * filter, int sx, int c, int pad, int stride,
		float * inputDeriv, int width, int height, int depth);

void cuda_Convolution_Dense_Image_Matrix_Product_INTEX_Weight(float * outputDeriv,
		int batchsize, int outputWidth, int outputHeight, int outputDepth,
		//int poolingSX, int poolingStride,
		float * grad, int sx, int c, int pad, int stride,
		float * input, int width, int height, int depth, float learnRate);

void cuda_Convolution_Index_Matrix_Multiply(int batchsize, int * Seg_Index, int * Seg_Margin,
		int seg_size, int * Fea_Index, int elementsize, float * con_weight, float * output, int Feature_dimension, int output_dimension, int win_size);

void cuda_Convolution_Index_Matrix_Product_Weight(float * deriv, int * maxpooling_index, int * Seg_Index, int * SegMargin_Index, int seg_size, int win_size,
		int batchsize, int output_dimension, int * Fea_Index, float * grad, int Feature_Dimension, float learnRate);

void Cuda_Maxpooling1D(float * values, int row, int column, int batchsize, float * output, int * maxpooling_index);
void Cuda_Meanpooling1D(float * values, int row, int column, int batchsize, float * output);

void Cuda_Max_Pooling_Mask(float * pooling_feas, int * Smp_Index, int * Seq_Index, int batchsize, float * output, int * maxpooling_index, int output_dimension);
void Cuda_DerivMaxPooling(float * deriv, int * maxpooling_index, float * pooling_deriv, int batchsize, int output_dimension, float alpha, float beta);

void cuda_MaxoutPooling(float * input, int batchSize, int dim, int * maxpoolingIndex, int maxoutDim, int outDim, float * output, int format);

void cuda_DerivMaxoutPooling(float * input_deriv, int batchSize, int dim, int * maxpoolingIndex, int maxoutDim, int outDim, float * output_deriv);

void Cuda_UCTStateSampling(float * count, float * success, float * status, int statusNum, float * weight, int weightLen, float C);
//void Cuda_StateUpdating(float * count, float * success, float * status, int statusNum, float * weight, int weightLen, float sampleNum, float win);
void Cuda_BanditStateUpdating(float * fail, float * success, int * status, int armNum, int banditNum, float f, float s, float alpha);

void Cuda_BayesianRatingProb(int * src2MatchIdx, int * src2MatchElement, int srcBatchSize, float * srcBatchLoss,
	float * outputScore, float * outputLabel, float * outputProb, int outputSize, float eplison, float gamma);

void Cuda_SpanMaxPool(float * inputSent, int * smpIdx, float * start, float * end, int * sentMargin, int matchSize, float * outputSent, int * outMaxIndex, int dim);
void Cuda_DerivSpanMaxPool(float * outputSentDeriv, int * smpIdx, int batchSize, float * inputSentDeriv, int * outMaxIndex, int dim, float beta);

void Cuda_SpanLastPool(float * inputSent, int * smpIdx, float * start, float * end, int * sentMargin, int matchSize, float * outputSent, int dim);
void Cuda_DerivSpanLastPool(float * outputSentDeriv, int * outSmpIdx, float * start, float * end, int batchSize, float * inputSentDeriv, int * inSmpIdx, int dim, float beta);

void Cuda_DerivLogProbEntropy(int * smpIdx, float * outputProb, float * logProbDeriv, float alpha, float beta, float epislon, int batchSize);


void Cuda_GraphNeighborNum(int * srcIdx, int batchSize, int * graphIdx, int * graphNeiNodes, int * graphNeiRels, 
							int * maskSrc, int * maskRel, int * maskInvRel, int * maskTgt, int * neiNum, int maskType);


void Cuda_GraphNeighborInfo(int * srcIdx, int batchSize, int * graphIdx, int * graphNeiNodes, int * graphNeiRels, 
							int * maskSrc, int * maskRel, int * maskInvRel, int * maskTgt, int * neiNum, 
							int * neiNodes, int * neiRels, int * neiMargin, int defaultR, int maskType);

void CudaTopK(float * input, int length, int k, float * output);


