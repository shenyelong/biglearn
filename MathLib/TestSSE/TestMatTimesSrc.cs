﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;
namespace TestSSE
{
    class TestMatTimesSrc
    {
        public static void MatTimesSrcNative(float[] mat, float[] src, float[] dst)
        {
            for (int r = 0; r < dst.Length; r++)
            {
                for (int i = 0; i < src.Length; i++)
                {
                    dst[r] += src[i] * mat[i + r * src.Length];
                }
            }
        }

        public static void MatTimesSrcNativeTrans(float[] mat, float[] src, float[] dst)
        {
            for (int r = 0; r < dst.Length; r++)
            {
                double v = 0;
                for (int i = 0; i < src.Length; i++)
                {
                    v += src[i] * mat[r + i * dst.Length];
                }

                dst[r] = (float)v;
            }
        }

        public static void Run()
        {
            Random rand = new Random(0);
            int rows = 1024*10;
            int cols = 1024*10;
            float[] mat = new float[rows * cols];
            float[] src = new float[cols];
            float[] dst = new float[rows];

            for (int i = 0; i < cols; i++)
            {
                src[i] = (float)rand.NextDouble();
                for (int j = 0; j < rows; j++)
                {
                    mat[j * cols + i] = 0.5f - (float)rand.NextDouble();
                }
            }

            Stopwatch sw = new Stopwatch();

            sw.Restart();
            AlignedArray amat = new AlignedArray(mat);
            AlignedArray asrc = new AlignedArray(src);
            AlignedArray adst = new AlignedArray(dst);
            Console.WriteLine("Construct aligned array: {0}ms", sw.ElapsedMilliseconds);

            TestMatTimesSrcN(rows, cols, mat, src, dst, sw, amat, asrc, adst);
        }

        private static void TestMatTimesSrcTrans(int rows, int cols, float[] mat, float[] src, float[] dst, Stopwatch sw, AlignedArray amat, AlignedArray asrc, AlignedArray adst)
        {
            sw.Restart();
            MatTimesSrcNativeTrans(mat, src, dst);
            sw.Stop();
            Console.WriteLine("MatTimesSrcTrans, Native: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(dst));

            for (int k = 0; k < 5; k++)
            {
                Console.WriteLine();
                sw.Restart();
                SseUtils.MatTimesSrc(true, false, amat, asrc, adst, cols);
                sw.Stop();
                Console.WriteLine("MatTimesSrcTrans, SseUtils Aligned: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(adst.Items, adst.AlignedBase, adst.Size));

                sw.Restart();
                SseUtils.MatTimesSrc(true, false, mat, src, dst, cols);
                sw.Stop();
                Console.WriteLine("MatTimesSrc, SseUtils unaligned: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(dst));

                sw.Restart();
                unsafe
                {
                    SseUtils.MatTimesSrc(true, false, (float*)amat.PinnedBasePtr.ToPointer(), (float*)asrc.PinnedBasePtr.ToPointer(), cols, (float*)adst.PinnedBasePtr.ToPointer(), rows, cols);
                }
                sw.Stop();
                Console.WriteLine("MatTimesSrc, SseUtils unsafe aligned: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(adst.Items, adst.AlignedBase, adst.Size));
            }
        }

        private static void TestMatTimesSrcN(int rows, int cols, float[] mat, float[] src, float[] dst, Stopwatch sw, AlignedArray amat, AlignedArray asrc, AlignedArray adst)
        {
            sw.Restart();
            MatTimesSrcNative(mat, src, dst);
            sw.Stop();
            Console.WriteLine("MatTimesSrc, Native: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(dst));

            for (int k = 0; k < 3; k++)
            {
                Console.WriteLine();
                sw.Restart();
                SseUtils.MatTimesSrc(false, false, amat, asrc, adst, rows);
                sw.Stop();
                Console.WriteLine("MatTimesSrc, SseUtils Aligned: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(adst.Items, adst.AlignedBase, adst.Size));

                sw.Restart();
                AvxUtils.MatTimesSrc(false, false, amat, asrc, adst, rows);
                sw.Stop();
                Console.WriteLine("MatTimesSrc, AvxUtils Aligned: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(adst.Items, adst.AlignedBase, adst.Size));


                sw.Restart();
                SseUtils.MatTimesSrc(false, false, mat, src, dst, rows);
                sw.Stop();
                Console.WriteLine("MatTimesSrc, SseUtils unaligned: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(dst));

                sw.Restart();
                unsafe
                {
                    SseUtils.MatTimesSrc(false, false, (float*)amat.PinnedBasePtr.ToPointer(), (float*)asrc.PinnedBasePtr.ToPointer(), cols, (float*)adst.PinnedBasePtr.ToPointer(), rows, rows);
                }
                sw.Stop();
                Console.WriteLine("MatTimesSrc, SseUtils unsafe aligned: {0}ms, dst:{1}", sw.ElapsedMilliseconds, SseUtils.SumSq(adst.Items, adst.AlignedBase, adst.Size));
            }
        }
    }
}
