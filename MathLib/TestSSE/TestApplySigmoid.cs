﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;

namespace TestSSE
{
    public class TestApplySigmoid
    {
        public unsafe static void Run()
        {
            int size = 1024 * 1024 * 100;
            float[] a = new float[size];
            float[] b = new float[size];
            Random rnd = new Random(0);
            for (int i = 0; i < size; i++)
            {
                a[i] = (0.5f - (float)rnd.NextDouble()) * 2;
            }
            a[0] = -1f;
            AlignedArray aa = new AlignedArray(a);
            AlignedArray ab = new AlignedArray(b);

            double d = 0;
            Stopwatch sw = new Stopwatch();

            int loop = 5;
            do
            {
                //sw.Restart();
                //fixed (float* pa = &a[0])
                //{
                //    fixed (float* pb = &b[0])
                //    {
                //        SseUtils.ApplySigmoid(pa, pb, size);
                //    }
                //}
                //sw.Stop();

                //d = 0;
                //for (int i = 0; i < size; i++)
                //{
                //    d += b[i];
                //}
                //Console.WriteLine("SseApplySigmodU:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                //sw.Restart();
                //fixed (float* pa = &a[0])
                //{
                //    fixed (float* pb = &b[0])
                //    {
                //        AvxUtils.ApplySigmoid(pa, pb, size);
                //    }
                //}
                //sw.Stop();

                //d = 0;
                //for (int i = 0; i < size; i++)
                //{
                //    d += b[i];
                //}
                //Console.WriteLine("AvxApplySigmodU:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                AvxUtils.ApplySigmoid((float*)aa.PinnedBasePtr.ToPointer(), (float*)ab.PinnedBasePtr.ToPointer(), size);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += ab[i];
                }
                Console.WriteLine("AvxApplySigmodU-[A]:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                SseUtils.ApplySigmoid((float*)aa.PinnedBasePtr.ToPointer(), (float*)ab.PinnedBasePtr.ToPointer(), size);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += ab[i];
                }
                Console.WriteLine("SseApplySigmodU-[A]:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                AvxUtils.ApplySigmoid(aa, ab, size);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += ab[i];
                }
                Console.WriteLine("SseApplySigmodA:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);

                sw.Restart();
                SigmoidNative(a, b);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += b[i];
                }
                Console.WriteLine("SigmodNative-[A]:{0}, Cost:{1}ms", d, sw.ElapsedMilliseconds);
                Console.WriteLine();

            } while (loop-- > 0);
        }

        static public void SigmoidNative(float[] a, float[] b)
        {
            for (int i = 0; i < a.Length; i++)
            {
                b[i] = (float)(1 / (1 + Math.Exp(-a[i])));
            }
        }
    }
}
