﻿using Microsoft.MachineLearning.Internal.Sse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSSE
{
    class Program
    {
        static void Main(string[] args)
        {

            TestAVXFastVector.Test_AddScale();

            //TestMatMul.Run();
            //TestApplySigmoid.Run();
            //TestApplyTanh.Run();
            //TestApplyRecip.Run();

            //System.Threading.ThreadPool.SetMinThreads(24, 24);
            //System.Threading.ThreadPool.SetMaxThreads(128, 128);
            //TestAdaGrad.Run();
            //Console.ReadKey();
#if(false)
            Random r = new Random(0);
            int size = 256;
            float[] a = new float[size];
            float[] b = new float[size];
            for (int i = 0; i < size; i++)
            {
                a[i] = (float)(r.NextDouble());
                b[i] = (float)(r.NextDouble());
            }
            DateTime pre = DateTime.Now;
            double sum = computeDotProduct(a, b, 1000);
            double ts = DateTime.Now.Subtract(pre).TotalMilliseconds;
            Console.WriteLine("Sum:{0},  Time:{1}", sum, ts);

            r = new Random(0);
            int cbaligh = AvxUtils.GetSysCbAlign();
            int rowcount = 32;
            int colcount = 64;
            AlignedArray mat = new AlignedArray(rowcount * colcount, cbaligh);
            AlignedArray src = new AlignedArray(colcount, cbaligh);
            for (int i = 0; i < colcount;i++)
            {
                for (int j = 0; j < rowcount; j++)
                {
                    mat[j * colcount + i] = (float)r.NextDouble();
                }
                src[i] = (float)r.NextDouble();
            }
            AlignedArray dst = new AlignedArray(rowcount, cbaligh);
            AvxUtils.MatTimesSrc(false, true, mat, src, dst, rowcount);
            Console.WriteLine(AvxUtils.Sum(dst));
#endif
        }

        private static double computeDotProduct(float[] a, float[] b, int time)
        {
            double sum = 0;
            for (int i = 0; i < time; i++)
                sum += AvxUtils.DotProductDense(a, b);
            return sum;
        }
    }
}
