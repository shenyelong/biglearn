﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;

namespace TestSSE
{
    public delegate void WorkProc(object state);

    public class MathTask
    {
        protected ManualResetEvent wait;

        public MathTask(WorkProc proc, Object state)
        {
            this.Proc = proc;
            this.State = state;
            this.wait = new ManualResetEvent(false);
        }

        public WorkProc Proc { get; protected set; }

        public bool Scheduled { get; set; }

        public bool Finished { get; protected set; }

        public Exception Exception { get; protected set; }

        public object State { get; protected set; }

        public void Wait()
        {
            this.wait.WaitOne();
            if (this.Exception != null)
            {
                throw new AggregateException(this.Exception);
            }
        }

        public void Run()
        {
            try
            {
                this.Proc(this.State);
            }
            catch (Exception exp)
            {
                this.Exception = exp;
                Console.WriteLine(exp);
            }
            finally
            {
                this.Finished = true;
                this.wait.Set();
            }
        }

        public static void WaitAll(MathTask[] task)
        {
            var events = task.Select(t => t.wait);
            ManualResetEvent.WaitAll(events.ToArray());
        }
    }

    public class MathThreadPool
    {
        delegate int ThreadProc(IntPtr param);

        static BlockingCollection<MathTask> taskList;
        static int CoreCount = 0;

        static MathThreadPool()
        {
            taskList = new BlockingCollection<MathTask>();
        }

        public MathThreadPool()
        {
        }

        public MathTask QueueWorkItem(WorkProc work, object state)
        {
            MathTask entry = new MathTask(work, state);
            taskList.Add(entry);
            return entry;
        }

        protected static void WorkingThread(object state)
        {
            int workId = -1;

            Thread.BeginThreadAffinity();

            workId = (int)state;

                Process currentProcess = Process.GetCurrentProcess();
                int threadId = GetCurrentThreadId();
                int mul = Environment.ProcessorCount / CoreCount;
                int mask = 1 << (((workId * 12)+12) % Environment.ProcessorCount);

                Console.WriteLine("ProcessThreads: {0}", currentProcess.Threads.Count);
                ProcessThread thisThread = currentProcess.Threads.Cast<ProcessThread>()
                               .Where(t => t.Id == threadId).Single();

                if (thisThread != null)
                {
                    Console.WriteLine("Find thread {0}:{1}", workId, threadId);

                    thisThread.IdealProcessor = 0;
                    thisThread.ProcessorAffinity = (IntPtr)mask;
                }
                else
                {
                    Console.WriteLine("Failed to find thread: {0}", workId);
                }
            MathTask task;
            while (true)
            {
                if (!taskList.TryTake(out task))
                {
                    Thread.SpinWait(1);
                    continue;
                }

                task.Scheduled = true;
                task.Run();
            }

            Thread.EndThreadAffinity();

        }

        public void Setup(int threadNum)
        {
            //ThreadProc proc = new ThreadProc();
            // IntPtr fpProc = Marshal.GetFunctionPointerForDelegate(proc);
            foreach (var item in new System.Management.ManagementObjectSearcher("Select * from Win32_Processor").Get())
            {
                CoreCount += int.Parse(item["NumberOfCores"].ToString());
            }

            Console.WriteLine("Number Of Cores: {0}", CoreCount);

            for (int i = 0; i < threadNum; i++)
            {
                Thread t = new Thread(WorkingThread);
                t.IsBackground = true;
                t.Priority = ThreadPriority.AboveNormal;
                t.Start(i);
                /* foreach (ProcessThread t in)
                 {
                     if (t.Id == dwThreadId)
                     {
                         Console.WriteLine("Find thread {0}:{1}", i, dwThreadId);
                     }
                 }*/
                //UIntPtr rlt = SetThreadAffinityMask(hThread, new UIntPtr(mask));
                //               Console.WriteLine("Set affinity to 0x{0:x} : {1} : {2} : {3}", mask, rlt == UIntPtr.Zero, Environment.ProcessorCount, i);
            }
        }


        #region System dependency import
        [DllImport("kernel32")]
        private static extern IntPtr CreateThread(
           IntPtr lpThreadAttributes,
           UInt32 dwStackSize,
           IntPtr lpStartAddress,
           IntPtr param,
           UInt32 dwCreationFlags,
           out UInt32 lpThreadId
         );

        [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int TerminateThread(int hThread);

        [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetLastError();


        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success), DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern bool CloseHandle(IntPtr handle);

        [DllImport("kernel32")]
        public static extern int GetCurrentThreadId();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern SafeThreadHandle OpenThread(int access, bool inherit, int threadId);

        //[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //public static extern IntPtr SetThreadAffinityMask(SafeThreadHandle handle, HandleRef mask);

        [DllImport("kernel32.dll")]
        public static extern UIntPtr SetThreadAffinityMask(IntPtr hThread, UIntPtr dwThreadAffinityMask);

        [SuppressUnmanagedCodeSecurity]
        public class SafeThreadHandle : SafeHandleZeroOrMinusOneIsInvalid
        {
            public SafeThreadHandle()
                : base(true)
            {

            }

            protected override bool ReleaseHandle()
            {
                return CloseHandle(handle);
            }
        }
        #endregion
    }
}
