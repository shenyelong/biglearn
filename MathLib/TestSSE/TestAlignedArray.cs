﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;

namespace TestSSE
{
    class TestAlignedArray
    {
        public static void Run()
        {
            AlignedArray aa = new AlignedArray(1024 * 1024 * 100);
            float[] ab = new float[aa.Size];
            Random rand = new Random(0);

            for (int i = 0; i < aa.Size; i++)
            {
                ab[i] = (float)rand.NextDouble();
            }

            int sz = aa.Size;
            int abase = aa.AlignedBase;
            Stopwatch sw = new Stopwatch();
            sw.Reset();
            sw.Start();
            for (int i = 0; i < aa.Size; i++)
            {
                aa.Items[i + abase] = ab[i];
            }
            sw.Stop();

            Console.WriteLine("Access by Items[], {0}ms, AlignedBase:{1}, {2}, addr: 0x{3:x}", sw.ElapsedMilliseconds, aa.AlignedBase, aa[0], (long)aa.PinnedBasePtr);
            sw.Reset();
            sw.Start();
            float[] it = aa.Items;
            int end = aa.Size + aa.AlignedBase;
            for (int i = abase; i < end; i++)
            {
                it[i] = ab[i - abase];
            }
            sw.Stop();

            Console.WriteLine("Access by Items2[], {0}ms, AlignedBase:{1}, {2}, addr: 0x{3:x}", sw.ElapsedMilliseconds, aa.AlignedBase, aa[0], (long)aa.PinnedBasePtr);

            sw.Reset();
            sw.Start();
            aa.CopyFrom(ab, 0, ab.Length);
            sw.Stop();

            Console.WriteLine("Access by CopyFrom, {0}ms, AlignedBase:{1}, {2}, addr: 0x{3:x}", sw.ElapsedMilliseconds, aa.AlignedBase, aa[0], (long)aa.PinnedBasePtr);


            sw.Reset();
            sw.Start();

            Array.Copy(ab, 0, it, abase, sz);

            sw.Stop();

            Console.WriteLine("Access by pointer, {0}ms, AlignedBase:{1}, {2}, addr: 0x{3:x}", sw.ElapsedMilliseconds, aa.AlignedBase, aa[0], (long)aa.PinnedBasePtr);

            sw.Reset();
            sw.Start();
            for (int i = 0; i < sz; i++)
            {
                aa[i] = ab[i];
            }
            sw.Stop();

            Console.WriteLine("Access by this[], {0}ms, AlignedBase:{1}, {2}, addr: 0x{3:x}", sw.ElapsedMilliseconds, aa.AlignedBase, aa[0], (long)aa.PinnedBasePtr);
            // It's strange when use float, access with this[] will be much slower, about 1 times, than double:( Seems inline failed with float.
            double v = 1.0f;
            //double v2 = 0.0f;
            int k =0;
            float[] c = new float[ab.Length];
            while (k++ < 3)
            {
                sw.Restart();
                v = 1.0f;
                for (int i = abase; i < end; i++)
                {
                    v = v+it[i];
                }
                sw.Stop();

                Console.WriteLine("[{0}] Access by Add array[], {1}ms, AlignedBase:{2}, {3}, addr: {4}", k, sw.ElapsedMilliseconds, aa.AlignedBase, ab[0], v);

                sw.Restart();
                v = 1.0f;
                for (int i = 0; i < sz; i++)
                {
                    v += aa[i];
                }
                sw.Stop();

                Console.WriteLine("[{0}] Access by Add this[], {1}ms, AlignedBase:{2}, {3}, addr: {4}", k, sw.ElapsedMilliseconds, aa.AlignedBase, aa[0], v);

                sw.Restart();
                v = 1.0f;
                unsafe
                {
                    float* ptr = (float*)aa.PinnedBasePtr.ToPointer();
                    for (int i = 0; i < sz; i++)
                    {
                        v += ptr[i];
                    }
                }
                sw.Stop();

                Console.WriteLine("[{0}] Access by Add unsafe ptr, {1}ms, AlignedBase:{2}, {3}, addr: {4}", k, sw.ElapsedMilliseconds, aa.AlignedBase, aa[0], v);
            }

            aa.Dispose();
        }
    }
}
