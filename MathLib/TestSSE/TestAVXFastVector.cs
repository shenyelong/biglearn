﻿using ScopeML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSSE
{
    public class TestAVXFastVector
    {
        public static bool Test_AddScale()
        {
            int dim = 100;
            int batch = 10000;
            float[] m1 = new float[batch * dim];
            float[] m2 = new float[batch * dim];
            float[] m3 = new float[batch * dim];

            float[] lr = new float[batch];
            for (int i = 0; i<batch;i++)
            {
                lr[i] = (i + 1) * 0.001f;

                for (int d= 0; d<dim;d++)
                {
                    m1[i * dim + d] = 1 + i * 0.001f + d * 0.05f ;
                    m2[i * dim + d] = (i + 1) * 0.0001f + d * 1.3f;
                    m3[i * dim + d] = m1[i * dim + d] + lr[i] * m2[i * dim + d];
                }

                double x = 0;
                for (int d = 0; d < dim; d++)
                {
                    x = x + m3[i * dim + d] * m3[i * dim + d];
                }
                x = Math.Sqrt(x);
                if (x > 1)
                    for (int ii = 0; ii < dim; ii++)
                        m3[i * dim + ii] = (float)(m3[i * dim + ii] / x);
            }

            Parallel.For(0, batch, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, b =>
            {
                AVXFastVector p = new AVXFastVector(m1, b * dim, dim, false);
                AVXFastVector g = new AVXFastVector(m2, b * dim, dim, false);
                p.AddScale(lr[b], g);

                double x = p.L2Norm;
                if (x > 1) { p.Scale((float)(1.0 / x)); }
                //g.ZeroItems();

                //for (int s = 0; s < gIdx.Value; s++)
                //{
                //    Parameter.MemPtr[gIdx.Key + s] += UpdateRate * Gradient.MemPtr[gIdx.Key + s];
                //}

                //if (IsNorm)
                //{
                //    double x = 0;
                //    for (int s = 0; s < gIdx.Value; s++)
                //    {
                //        x = x + Parameter.MemPtr[gIdx.Key + s] * Parameter.MemPtr[gIdx.Key + s];
                //    }
                //    x = Math.Sqrt(x);
                //    if (x > 1)
                //        for (int ii = 0; ii < gIdx.Value; ii++)
                //            Parameter.MemPtr[gIdx.Key + ii] = (float)(Parameter.MemPtr[gIdx.Key + ii] / x);
                //}
            });

            for (int i = 0; i < batch; i++)
            {
                for (int d = 0; d < dim; d++)
                {
                    if( Math.Abs( m3[i * dim + d] - m1[i * dim + d]) >= 0.0000001f)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
