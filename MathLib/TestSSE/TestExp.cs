﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;

namespace TestSSE
{
    class TestExp
    {
        public static void Run()
        {
            int size = 1024*1024*10 ;
            float[] a = new float[size];
            float[] b = new float[size];
            Random rnd = new Random(0);
            for (int i = 0; i < size; i++)
            {
                a[i] = (0.5f - (float)rnd.NextDouble()) * 2;
            }
            a[0] = 4.990432f;//(float)Math.Log(3);
            float d = 0;
            Stopwatch sw = new Stopwatch();
            for (int k = 0; k < 1; k++)
            {
                sw.Restart();
                SseUtils.ApplyExp(a, b);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += b[i];
                }

                Console.WriteLine("Sse ExpV:{0}, {1}ms", b[0], sw.ElapsedMilliseconds);

                sw.Restart();
                AvxUtils.ApplyExp(a, b);
                sw.Stop();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    d += b[i];
                }

                Console.WriteLine("Avx ExpV:{0}, {1}ms", b[0], sw.ElapsedMilliseconds);


                sw.Restart();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                   b[i] = SseUtils.Exp(a[i]);
                   d += b[i];
                }
                sw.Stop();

                Console.WriteLine("Sse Exp:{0}, {1}ms", b[0], sw.ElapsedMilliseconds);

                sw.Restart();
                d = 0;
                for (int i = 0; i < size; i++)
                {
                    b[i] = (float)Math.Exp(a[i]);
                    d += b[i];
                }
                sw.Stop();

                Console.WriteLine("Math exp:{0}, {1}ms", b[0], sw.ElapsedMilliseconds);

                Console.WriteLine();
            }
        }
    }
}
