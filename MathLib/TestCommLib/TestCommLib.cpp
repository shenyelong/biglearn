// TestCommLib.cpp : main project file.

#include "stdafx.h"
#include <Windows.h>
#include <string>
#include <vector>
using namespace System;
using namespace CommLib;
using namespace System::Runtime::InteropServices;

#pragma managed
void broadcast_udf(void* bcastbuf, UInt64 size, void* args)
{
	double sum = 0;
	double *dbuf = (double *)bcastbuf;
	size_t dsize = size / sizeof(double);

	for (size_t i = 0; i < dsize; i++)
	{
		sum += dbuf[i];
	}

	Console::WriteLine("sum " + sum);
	AsyncCommunication::FreeBroadCastBuf(bcastbuf);
}

int main(array<System::String ^> ^args)
{	
	bool use_rdma = false;
	if (args[0]->ToLower() == "true") 
		use_rdma = true;	

	AsyncCommunication::Init("servers.txt", 2, use_rdma);
	AsyncCommunication::Barrier();

	unsigned int vecsize = 1024 * 1024;
	//unsigned int vecsize = 1024;
	std::vector<double> vec1(vecsize);
	std::vector<double> vec2(vecsize);

	for (unsigned int i = 0; i < vecsize; i++)
	{
		vec1[i] = i;
		vec2[i] = 2 * vec1[i];
	}

	AsyncCommunication::AllReduce(&vec1[0], vecsize, sizeof(double), AllReduceDataType::TpDouble, AllReduceOpType::OpSum);

	double sum1, sum2;
	sum1 = sum2 = 0;
	for (unsigned int i = 0; i < vecsize; i++)
	{
		sum1 += vec1[i];
		sum2 += vec2[i];
	}

	Console::WriteLine("sum1 " + sum1);
	Console::WriteLine("sum2 " + sum2);

	AsyncCommunication::BroadCastCallback^ cb = gcnew AsyncCommunication::BroadCastCallback(broadcast_udf);
	GCHandle gc = GCHandle::Alloc(cb);
	AsyncCommunication::RegisterAsyncBroadCastHandler(cb, nullptr);
	Console::WriteLine("after RegisterAsyncBroadCastHandler");

	AsyncCommunication::Barrier();

	Console::WriteLine("after Barrier");

	//// test broadcast

	AsyncCommunication::AsyncBroadCast(&vec1[0], vecsize * sizeof(double));

	Sleep(5000);

    return 0;
}
