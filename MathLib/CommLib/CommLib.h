// CommLib.h

#pragma once

#include <string>
#include <chana_ps.h>

using namespace System;
using namespace System::Runtime::InteropServices;
#pragma managed
namespace CommLib {

	public enum class AllReduceDataType :int
	{
		TpInt32 = ALLREDUCE_TYPE_INT32,
		TpUint32 = ALLREDUCE_TYPE_UINT32,
		TpInt64 = ALLREDUCE_TYPE_INT64,
		TpUint64 = ALLREDUCE_TYPE_UINT64,
		TpFloat = ALLREDUCE_TYPE_FLOAT,
		TpDouble = ALLREDUCE_TYPE_DOUBLE,
		TpNum = ALLREDUCE_TYPE_NUM
	};

	public enum class AllReduceOpType :int
	{
		OpSum = ALLREDUCE_OP_SUM,
		OpNum = ALLREDUCE_OP_NUM
	};

	struct BroadcastMsg {
		UInt32 SessionId;
		unsigned char msg[0];
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	public ref class AsyncCommunication
	{
	public:

		delegate void BroadCastCallback(void* pBuffer, size_t size, void*args);

		ref class BroadcastSessionContext {
		public:
			BroadCastCallback^ Callback;
			void *Args;
			char *pSeesionBuf;
			UInt32 BufSize;

			BroadcastSessionContext()
			{
				pSeesionBuf = nullptr;
				BufSize = 0;
			}
		};

	public:
		static AsyncCommunication()
		{
			AsyncCommunication::gSessions = gcnew array<AsyncCommunication::BroadcastSessionContext^>(8);
		}

		/// The API to initialize the system to enable communication engine	
		/// machine_list_file: 
		///		The path of the configuration file for the membership of the machines in the computation.
		///		The format is a text file with each row as ip:port. For example:
		///		10.0.0.15:5000
		///		10.0.0.16:5000
		///	comm_thrd_count:
		///		The number of threads in the engine to conduct the actual API operations		
		///	use_rdma:
		///		true : for using rdma
		///		false : for using tcp/ip
		static void Init(String^ machine_list_file, const int comm_thrd_count, bool use_rdma);

		static void Barrier();

		/// This interface does the in-place allreduce on dense array
		///	arbuf:
		///		The buffer holding the to-be-allreduced data, i.e., an array of elements. 
		///		The allreduced result is also put into the buffer.
		/// elemcount:
		///		The number of elements in the arbuf.
		///	elemsize:
		///		Ths size of each element in bytes.
		///	elemtype:
		///		The type of each element. /ref chana_ps.h
		///	op:
		///		The operation of the allreduce. /ref chana_ps.h
		static void AllReduce(void *arbuf, size_t elemcount, size_t elemsize, AllReduceDataType elemtype, AllReduceOpType op);

		///	This interface registers the user defined handler for processing the received broadcast message.
		///	bcast_cb:
		///		The user defined handler function
		///	args:
		///		The parameter passed to bcast_cb when invoked 
		[DllImport("chanalib.dll", EntryPoint = "RegisterAsyncBroadCastHandler")]
		static void RegisterAsyncBroadCastHandler(BroadCastCallback^ bcast_cb, void *args);

		///	This interface broadcasts a message to all other peer servers
		///	bcastbuf:
		///		The buffer to be broadcasted
		///	size:
		///		The size of bcastbuf in bytes
		[DllImport("chanalib.dll", EntryPoint = "AsyncBroadCast")]
		static void AsyncBroadCast(void *bcastbuf, size_t size);

		[DllImport("chanalib.dll")]
		static void FreeBroadCastBuf(void *buf);

		[DllImport("chanalib.dll")]
		static int GetMyRank();

		[DllImport("chanalib.dll")]
		static int GetMachineCount();

		[DllImport("chanalib.dll")]
		size_t GetPSThreadNumPerMachine();

		static UInt32 RegisterAyncBroadcastSession(BroadCastCallback^ cb, void *args);

		static void BroadcastSession(UInt32 sid, void *bcastBuf, UInt32 size);

		static void FreeSessionBuffer(void *pBuf);
	protected:
		static void BroadcastSessionCallback(void* pBuffer, size_t size, void*args);

	private:
		static array<BroadcastSessionContext^>^ gSessions;
		static UInt32 GlobalId = 0;
		static GCHandle callbackHandle;
		static int machineCount = 0;
	};
}
