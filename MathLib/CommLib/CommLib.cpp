// This is the main DLL file.

#include "stdafx.h"
#include < vcclr.h >
#include "CommLib.h"
#include "ParameterServer.h"
#include  <stdlib.h>
#pragma managed
namespace CommLib {
	ChaNaPSBase* PsCreate(void *args)
	{
		return new ParameterServerWrapper();
	}

	void AsyncCommunication::Init(String^ machine_list_file, const int iothrd_count_per_machine, bool use_rdma)
	{
		putenv("CHANA_PIN_CPU=false");
		pin_ptr<const wchar_t> wch = PtrToStringChars(machine_list_file);
		size_t convertedChars = 0;
		size_t  sizeInBytes = ((machine_list_file->Length + 1) * 2);
		errno_t err = 0;
		char    *ch = (char *)malloc(sizeInBytes);

		memset(ch, 0, sizeInBytes);
		err = wcstombs_s(&convertedChars, ch, sizeInBytes, wch, sizeInBytes);
		chana_initialize(0, nullptr);
		//CreateSyncEngine(ch, iothrd_count_per_machine, use_rdma);
		CreateParameterServerC(ch, iothrd_count_per_machine, use_rdma, PsCreate, nullptr);

		free(ch);

		BroadCastCallback^ cb = gcnew BroadCastCallback(AsyncCommunication::BroadcastSessionCallback);
		callbackHandle = GCHandle::Alloc(cb);
		RegisterAsyncBroadCastHandler(cb, nullptr);
	}

	void AsyncCommunication::Barrier()
	{
		BarrierEnter();
	}

	void AsyncCommunication::AllReduce(void *arbuf, size_t elemcount, size_t elemsize, AllReduceDataType elemtype, AllReduceOpType op)
	{
		ChaNa_AllReduce(ALLREDUCE_IN_PLACE, arbuf, elemcount, elemsize, (int)elemtype, (int)op);
	}

	UInt32 AsyncCommunication::RegisterAyncBroadcastSession(BroadCastCallback ^ cb, void * args)
	{
		BroadcastSessionContext^ ctx = gcnew BroadcastSessionContext();
		ctx->Callback = cb;
		ctx->Args = args;
		AsyncCommunication::gSessions[GlobalId] = ctx;
		return GlobalId++;
	}

	void AsyncCommunication::BroadcastSession(UInt32 sid, void * bcastBuf, UInt32 size)
	{
		BroadcastSessionContext^ ctx = gSessions[sid];
		if (ctx->pSeesionBuf == nullptr || ctx->BufSize == 0)
		{
			ctx->pSeesionBuf = new char[size + 4];
		}
		else if (ctx->BufSize < size)
		{
			delete[] ctx->pSeesionBuf;
			ctx->pSeesionBuf = new char[size + 4];
		}

		ctx->BufSize = size;
		BroadcastMsg *pMsg = (BroadcastMsg*)ctx->pSeesionBuf;
		pMsg->SessionId = sid;
		memcpy(pMsg->msg, bcastBuf, size);
		AsyncBroadCast(pMsg, size + 4);
	}

	void AsyncCommunication::FreeSessionBuffer(void * pBuf)
	{
		FreeBroadCastBuf((char*)pBuf - 4);
	}

	void AsyncCommunication::BroadcastSessionCallback(void * pBuffer, size_t size, void * args)
	{
		BroadcastMsg *pMsg = (BroadcastMsg*)pBuffer;
		gSessions[pMsg->SessionId]->Callback(pMsg->msg, size - 4, gSessions[pMsg->SessionId]->Args);
	}
}