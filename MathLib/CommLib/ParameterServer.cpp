#include "stdafx.h"
#include "ParameterServer.h"
#include <vcclr.h>

namespace CommLib
{
	gcroot<ParameterServerBase^> ParameterServerWrapper::m_Impl = nullptr;
	void ParameterServerWrapper::control(int cmd_id, void *data, const size_t len)
	{
		m_Impl->OnControl(cmd_id, data, len);
	}

	void ParameterServerWrapper::server_process_pull(
		uint64_t *keys,
		size_t key_count,
		void **vals,
		size_t *val_sizes,
		val_deallocator_t *dealloc,
		void **args,
		bool *fixed_val_size
	)
	{		
		ParameterServerBase::DeAlocator^ da = (ParameterServerBase::DeAlocator^)Marshal::GetDelegateForFunctionPointer((IntPtr)dealloc, ParameterServerBase::DeAlocator::typeid);
		m_Impl->OnPullProc(keys, key_count, vals, val_sizes, da, args, fixed_val_size);
	}

	void ParameterServerWrapper::server_process_push(size_t key_count, uint64_t *keys, void **vals, size_t *valsizes)
	{
		m_Impl->OnPushProc(key_count, keys, vals, valsizes);
	}

	void ParameterServerWrapper::worker_apply_pull(void *args)
	{
		m_Impl->OnPullAck(args);
	}

	void ParameterServerWrapper::AttachPS(ParameterServerBase^ imp)
	{
		m_Impl = imp;
	}
}