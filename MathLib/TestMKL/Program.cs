﻿using Microsoft.MachineLearning.Internal.Mkl;
using Microsoft.MachineLearning.Internal.Sse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMKL
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random(0);
            int cbaligh = AvxUtils.GetSysCbAlign();
            int rowcount = 32;
            int colcount = 64;
            AlignedArray mat = new AlignedArray(rowcount * colcount, cbaligh);
            AlignedArray src = new AlignedArray(colcount, cbaligh);
            for (int i = 0; i < colcount; i++)
            {
                for (int j = 0; j < rowcount; j++)
                {
                    mat[j * colcount + i] = (float)r.NextDouble();
                }
                src[i] = (float)r.NextDouble();
            }
            AlignedArray dst = new AlignedArray(rowcount, cbaligh);
            MklUtils.MatTimesSrc(false, 1, false, mat, src, dst, rowcount, colcount, 1);
            //AvxUtils.MatTimesSrc(false, true, mat, src, dst, rowcount);
            Console.WriteLine(AvxUtils.Sum(dst));
            Console.ReadKey();
        }
    }
}
