// dllapp.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <iostream> 
#include <vector> 
//#include <cuda_runtime.h> 
//#include <cublas.h>
//#include <cuda.h>
//#include <cuda_runtime_api.h>
//#include <cuda_surface_types.h>
//#include "device_launch_parameters.h" //device_launch_parameters.h"
#include <stdint.h>
#include <stdio.h>
#include <cstring>
#include <stdlib.h>
//#include <cudnn.h>
//#include <cusparse.h>

//#pragma comment(lib, "cudart") 
//#pragma comment(lib, "cuda") 
//#pragma comment(lib, "cudnn")
using namespace std;

#ifdef WIN32
#include <comutil.h>
#include <windows.h>
using namespace _com_util;
#else
#include <cfloat>
#include <dlfcn.h>
//#include "mpi.h"
//#include "nccl.h"
#endif
/* variables for MPI aggregation */
#ifdef WIN32
#else
static int rank = -1;
//static ncclUniqueId commId;
//static ncclComm_t comm;
static int numprocs = -1;
int r_len = 44362800;
float *d_result;
int *d_id,*d_cnt;
float *d_R_gather;
int *d_id_gather;
int *h_cnt_gather,*d_cnt_gather;
float ratio = 1.0;
#endif


#include <xmmintrin.h>
#include <pmmintrin.h>
#include <inttypes.h>
#include <emmintrin.h>
#include <limits>
#include <cmath>
#include <smmintrin.h>
#define UNUSED(x) (void)(x)


//Exp.cpp
// 1 / ln(2).
				const float RecipLn2 = (float)1.44269504088896340735992468100;
				// Used for computing a 4th degree polynomial approximation of e^x.
				const float Coef1 = (float)0.013555747234814917704030793;
				const float Coef2 = (float)0.065588116243247810171479524;
				const float Coef3 = (float)0.3069678791803394491901401;

				const float ExpInf = 128;
				const int ExpBias = 127;
				const int ExpShift = 23;

inline float BuiltInExp(const float &input)
				{
					bool neg = false;
					float arg = input;

					arg = arg*RecipLn2;
					arg = arg > 128.0f ? 128.0f : (arg < -127.0f ? -127.0f : arg);

					int exp = (int)(arg + 127);
					int q = exp - 127;
					arg = arg - q;
					exp <<= ExpShift;

					float res = (1 + arg) + (arg - 1) * arg * ((Coef1 * arg + Coef2) * arg + Coef3);
					res *= *(float *)&exp;
					return res;
				}

// SSE.cpp
#define _load1(ps, pi) \
	_mm_set_ss(ps[pi[0]])

#define _load4(ps, pi) \
	_mm_setr_ps(ps[pi[0]], ps[pi[1]], ps[pi[2]], ps[pi[3]])

#define _rotate(x) _mm_shuffle_ps(x, x, 0x39)

#define _rotate_reverse(x) _mm_shuffle_ps(x, x, 0x93)

#define _store1(x, pd, pi) \
	_mm_store_ss(pd + pi[0], x)

//Warning: this operation changes the value of x => do not reuse x
#define _store4(x, pd, pi) \
	_mm_store_ss(pd + pi[0], x); \
	x = _rotate(x); _mm_store_ss(pd + pi[1], x); \
	x = _rotate(x); _mm_store_ss(pd + pi[2], x); \
	x = _rotate(x); _mm_store_ss(pd + pi[3], x)


void Init()
{
					_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
					_MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);
}
bool ChkAvx()
				{
					int cpuInfo[4];
					return false;
					//__cpuid(cpuInfo, 1);

					// 28th bit of second integer of Cpu Info denotes whether the Avx is supported in CPU or not 
					// Reference http://msdn.microsoft.com/en-us/library/hskdteyh(v=vs.100).aspx
					//return cpuInfo[2] & (1 << 28) || false;
				}


void MulA(bool add, const float * pmat, const int * pposSrc, const float * psrc,
					int posMin, int iposMin, int iposLim, float * pdst, int crow, int ccol)
				{
					// REVIEW shonk: For extremely sparse inputs, interchanging the loops would
					// likely be more efficient.
					const int * pposMin = pposSrc + iposMin;
					const int * pposLim = pposSrc + iposLim;
					const float * pdLim = pdst + crow;
					const float * pm0 = pmat - posMin;
					const float * ps = psrc - posMin;
					for (float * pd = pdst; pd < pdLim; pd += 4, pm0 += 4 * ccol)
					{
						const float * pm1 = pm0 + ccol;
						const float * pm2 = pm1 + ccol;
						const float * pm3 = pm2 + ccol;
						__m128 res = _mm_setzero_ps();
						for (const int * ppos = pposMin; ppos < pposLim; ppos++)
						{
							int col = *ppos;
							__m128 x1 = _mm_setr_ps(pm0[col], pm1[col], pm2[col], pm3[col]);
							__m128 x2 = _mm_set1_ps(ps[col]);
							x2 = _mm_mul_ps(x2, x1);
							res = _mm_add_ps(res, x2);
						}

						if (add)
							res = _mm_add_ps(res, _mm_load_ps(pd));
						_mm_store_ps(pd, res);
					}
				}

				void MulU(bool add, const int * pstarts, const int * pindices, const float * pcoefs,
					const float * ps, float * pdst, int crow)
				{
					const int * pii = pstarts + 1;
					const int * pi = pindices;
					const float * pm = pcoefs;
					const float * pdLim = pdst + crow;
					for (float * pd = pdst; pd < pdLim; pd++)
					{
						const int * piLim = pindices + *pii++;

						__m128 res = _mm_setzero_ps();
						for (; pi + 4 <= piLim; pi += 4, pm += 4)
						{
							__m128 x = _mm_mul_ps(_load4(ps, pi), _mm_loadu_ps(pm));
							res = _mm_add_ps(res, x);
						}
						for (; pi < piLim; pi++, pm++)
						{
							__m128 x = _mm_mul_ss(_load1(ps, pi), _mm_set_ss(*pm));
							res = _mm_add_ss(res, x);
						}
						res = _mm_hadd_ps(res, res);
						res = _mm_hadd_ps(res, res);

						if (add)
							res = _mm_add_ss(res, _mm_set_ss(*pd));
						_mm_store_ss(pd, res);
					}
				}

				// Unpadded.
				void MulConvU(bool add, const int * pmprowiv, const int * pmprowcol,
					const int * pruns, const float * pcoefs, const float * psrc, float * pdst, int crow)
				{
					int size = pruns[1];
					const int * psupport = pruns + 2;
					const int * piv = pmprowiv;
					const int * pcol = pmprowcol;
					const int * piLim = psupport + size;
					const float * pdLim = pdst + crow;

					for (float * pd = pdst; pd < pdLim; pd++)
					{
						const float * pm = pcoefs + *piv++;
						const float * ps = psrc + *pcol++;
						const int * pi = psupport;

						__m128 res = _mm_setzero_ps();
						for (; pi + 4 <= piLim; pi += 4, pm += 4)
						{
							__m128 x = _mm_mul_ps(_load4(ps, pi), _mm_loadu_ps(pm));
							res = _mm_add_ps(res, x);
						}
						for (; pi < piLim; pi++, pm++)
						{
							__m128 x = _mm_mul_ss(_load1(ps, pi), _mm_set_ss(*pm));
							res = _mm_add_ss(res, x);
						}
						res = _mm_hadd_ps(res, res);
						res = _mm_hadd_ps(res, res);

						// Add the bias.
						res = _mm_add_ss(res, _mm_set_ss(*pm));

						if (add)
							res = _mm_add_ss(res, _mm_set_ss(*pd));
						_mm_store_ss(pd, res);
					}
				}

				// Padded.
				void MulConvU(bool add, const int * pmprowiv, const int * pmprowcol, const int * pmprowrun,
					const int * pruns, const float * pcoefs, const float * psrc, float * pdst, int crow)
				{
					const int * piv = pmprowiv;
					const int * pcol = pmprowcol;
					const float * pdLim = pdst + crow;
					int kernelSize = pruns[1];

					const int * pirun = pmprowrun;
					for (float * pd = pdst; pd < pdLim; pd++)
					{
						const float * pm = pcoefs + *piv++;
						const float * pmBias = pm + kernelSize;
						const float * ps = psrc + *pcol++;
						int irun = *pirun++;

						const int * pi = pruns + 2 + irun;
						const int * piLim = pi + pi[-1];
						__m128 res = _mm_setzero_ps();
						if (irun == 0)
						{
							// No masking needed.
							for (; pi + 4 <= piLim; pi += 4, pm += 4)
							{
								__m128 x = _mm_mul_ps(_load4(ps, pi), _mm_loadu_ps(pm));
								res = _mm_add_ps(res, x);
							}
							for (; pi < piLim; pi++, pm++)
							{
								__m128 x = _mm_mul_ss(_load1(ps, pi), _mm_set_ss(*pm));
								res = _mm_add_ss(res, x);
							}
						}
						else
						{
							// Need masking.
							pm += pi[-2];
							const float * pmask = reinterpret_cast<const float *>(piLim);
							for (; pi + 4 <= piLim; pi += 4, pm += 4, pmask += 4)
							{
								__m128 x = _mm_mul_ps(_load4(ps, pi), _mm_and_ps(_mm_loadu_ps(pmask), _mm_loadu_ps(pm)));
								res = _mm_add_ps(res, x);
							}
							for (; pi < piLim; pi++, pm++, pmask++)
							{
								__m128 x = _mm_mul_ss(_load1(ps, pi), _mm_and_ps(_mm_set_ss(*pmask), _mm_set_ss(*pm)));
								res = _mm_add_ss(res, x);
							}
						}
						res = _mm_hadd_ps(res, res);
						res = _mm_hadd_ps(res, res);

						res = _mm_add_ss(res, _mm_set_ss(*pmBias));
						if (add)
							res = _mm_add_ss(res, _mm_set_ss(*pd));
						_mm_store_ss(pd, res);
					}
				}

				void MeanU(bool add, const int * pmprowcol, const int * pmprowindices, const int * pindices,
					const float * psrc, float * pdst, int crow)
				{
					const int * pcol = pmprowcol;
					const float * pdLim = pdst + crow;

					if (pmprowindices == NULL)
					{
						int size = pindices[0];
						__m128 x0 = _mm_set_ss((float)size);
						const int * piLim = pindices + 1 + size;
						for (float * pd = pdst; pd < pdLim; pd++)
						{
							const float * ps = psrc + *pcol++;
							const int * pi = pindices + 1;

							__m128 res = _mm_setzero_ps();
							for (; pi + 4 <= piLim; pi += 4)
								res = _mm_add_ps(res, _load4(ps, pi));
							for (; pi < piLim; pi++)
								res = _mm_add_ss(res, _load1(ps, pi));
							res = _mm_hadd_ps(res, res);
							res = _mm_hadd_ps(res, res);

							res = _mm_div_ss(res, x0);
							if (add)
								res = _mm_add_ss(res, _mm_set_ss(*pd));
							_mm_store_ss(pd, res);
						}
					}
					else
					{
						const int * pii = pmprowindices;
						for (float * pd = pdst; pd < pdLim; pd++)
						{
							const float * ps = psrc + *pcol++;
							int ii = *pii++;

							const int * pi = pindices + ii;
							int size = *pi++;
							const int * piLim = pi + size;
							__m128 res = _mm_setzero_ps();
							for (; pi + 4 <= piLim; pi += 4)
								res = _mm_add_ps(res, _load4(ps, pi));
							for (; pi < piLim; pi++)
								res = _mm_add_ss(res, _load1(ps, pi));
							res = _mm_hadd_ps(res, res);
							res = _mm_hadd_ps(res, res);

							res = _mm_div_ss(res, _mm_set_ss((float)size));
							if (add)
								res = _mm_add_ss(res, _mm_set_ss(*pd));
							_mm_store_ss(pd, res);
						}
					}
				}

				void MaxU(bool add, const int * pmprowcol, const int * pmprowindices, const int * pindices,
					const float * psrc, float * pdst, int crow)
				{
					const int * pcol = pmprowcol;
					const float * pdLim = pdst + crow;
					__m128 min = _mm_set1_ps(-std::numeric_limits<float>::infinity());

					if (pmprowindices == NULL)
					{
						int size = pindices[0];
						const int * piLim = pindices + 1 + size;
						for (float * pd = pdst; pd < pdLim; pd++)
						{
							const float * ps = psrc + *pcol++;
							const int * pi = pindices + 1;

							__m128 res = min;
							for (; pi + 4 <= piLim; pi += 4)
								res = _mm_max_ps(res, _load4(ps, pi));
							for (; pi < piLim; pi++)
								res = _mm_max_ss(res, _load1(ps, pi));
							__m128 x1 = _mm_shuffle_ps(res, res, 0xB1);
							res = _mm_max_ps(res, x1);
							x1 = _mm_shuffle_ps(res, res, 0x02);
							res = _mm_max_ss(res, x1);

							if (add)
								res = _mm_add_ss(res, _mm_set_ss(*pd));
							_mm_store_ss(pd, res);
						}
					}
					else
					{
						const int * pii = pmprowindices;
						for (float * pd = pdst; pd < pdLim; pd++)
						{
							const float * ps = psrc + *pcol++;
							int ii = *pii++;

							const int * pi = pindices + ii;
							int size = *pi++;
							const int * piLim = pi + size;
							__m128 res = min;
							for (; pi + 4 <= piLim; pi += 4)
								res = _mm_max_ps(res, _load4(ps, pi));
							for (; pi < piLim; pi++)
								res = _mm_max_ss(res, _load1(ps, pi));
							__m128 x1 = _mm_shuffle_ps(res, res, 0xB1);
							res = _mm_max_ps(res, x1);
							x1 = _mm_shuffle_ps(res, res, 0x02);
							res = _mm_max_ss(res, x1);

							if (add)
								res = _mm_add_ss(res, _mm_set_ss(*pd));
							_mm_store_ss(pd, res);
						}
					}
				}

				// REVIEW vishaljo: Try out SSE/AVX after padding support is added. AVX math platform uses the same code below.
				void RespNormU(bool add, float alpha, float beta, bool avgOverFullKernel, float offset,
					const int * pmprowcol, const int * pmprowindices, const int * pindices,
					const float * psrc, float * pdst, int crow)
				{
					const int * pcol = pmprowcol;
					const float * pdLim = pdst + crow;

					if (pmprowindices == NULL)
					{
						int size = pindices[0];
						float scale = alpha / size;
						const int * piLim = pindices + 1 + size;
						for (float * pd = pdst; pd < pdLim; pd++)
						{
							const float * ps = psrc + *pcol++;
							const int * pi = pindices + 1;
							float res = 0;
							for (; pi < piLim; pi++)
							{
								float cur = ps[*pi];
								res += cur * cur;
							}
							res = ps[0] * powf(offset + scale * res, -beta);
							*pd = add ? *pd + res : res;
						}
					}
					else
					{
						int kernelSize = pindices[0];
						const int * pii = pmprowindices;
						for (float * pd = pdst; pd < pdLim; pd++)
						{
							const float * ps = psrc + *pcol++;
							int ii = *pii++;
							const int * pi = pindices + ii;
							int size = *pi++;
							const int * piLim = pi + size;
							float res = 0;
							for (; pi < piLim; pi++)
							{
								float cur = ps[*pi];
								res += cur * cur;
							}
							int avgDenom = avgOverFullKernel ? kernelSize : size;
							res = ps[0] * powf(offset + alpha / avgDenom * res, -beta);
							*pd = add ? *pd + res : res;
						}
					}
				}

				void MulTranA(bool add, const float * pmat, const int * pposSrc, const float * psrc,
					int posMin, int iposMin, int iposLim, float * pdst, int crow)
				{
					const int * ppos = pposSrc + iposMin;
					const int * pposLim = pposSrc + iposLim;
					const float * pdLim = pdst + crow;

					if (!add)
					{
						int col = *ppos++ - posMin;
						const float * pm = pmat + col * crow;
						__m128 x0 = _mm_set1_ps(psrc[col]);
						for (float * pd = pdst; pd < pdLim; pd += 4, pm += 4)
						{
							__m128 x1 = _mm_load_ps(pm);
							x1 = _mm_mul_ps(x1, x0);
							_mm_store_ps(pd, x1);
						}
					}

					// REVIEW shonk: Should we explore unrolling the outer loop?
					for (; ppos < pposLim; ppos++)
					{
						int col = *ppos - posMin;
						__m128 x0 = _mm_set1_ps(psrc[col]);
						const float * pm = pmat + col * crow;
						for (float * pd = pdst; pd < pdLim; pd += 4, pm += 4)
						{
							__m128 x1 = _mm_load_ps(pm);
							__m128 x2 = _mm_load_ps(pd);
							x1 = _mm_mul_ps(x1, x0);
							x2 = _mm_add_ps(x2, x1);
							_mm_store_ps(pd, x2);
						}
					}
				}

				void MulTranU(bool add, const int * pstarts, const int * pindices, const float * pcoefs,
					const float * psrc, float * pd, int crow, int ccol)
				{
					if (!add)
						memset(pd, 0, crow * sizeof(float));

					const int * pii = pstarts + 1;
					const int * pi = pindices;
					const float * pm = pcoefs;
					const float * psLim = psrc + ccol;
					for (const float * ps = psrc; ps < psLim; ps++)
					{
						float x = *ps;
						const int * piLim = pindices + *pii++;

						__m128 x1 = _mm_set1_ps(x);
						for (; pi + 4 <= piLim; pi += 4, pm += 4)
						{
							__m128 x2 = _mm_mul_ps(x1, _mm_loadu_ps(pm));
							x2 = _mm_add_ps(x2, _load4(pd, pi));
							_store4(x2, pd, pi);
						}
						for (; pi < piLim; pi++, pm++)
						{
							__m128 x2 = _mm_mul_ss(x1, _mm_set_ss(*pm));
							x2 = _mm_add_ss(x2, _load1(pd, pi));
							_store1(x2, pd, pi);
						}
					}
				}

				// Unpadded.
				void MulTranConvU(bool add, const int * pmpcoliv, const int * pmpcolrow,
					const int * pruns, const float * pcoefs, const float * psrc, float * pdst, int crow, int ccol)
				{
					if (!add)
						memset(pdst, 0, crow * sizeof(float));

					int size = pruns[1];
					const int * psupport = pruns + 2;
					const int * piv = pmpcoliv;
					const int * prow = pmpcolrow;
					const int * piLim = psupport + size;
					const float * psLim = psrc + ccol;
					for (const float * ps = psrc; ps < psLim; ps++)
					{
						const float * pm = pcoefs + *piv++;
						float * pd = pdst + *prow++;
						const int * pi = psupport;

						float x = *ps;
						__m128 x1 = _mm_set1_ps(x);
						for (; pi + 4 <= piLim; pm += 4, pi += 4)
						{
							__m128 x2 = _mm_mul_ps(x1, _mm_loadu_ps(pm));
							x2 = _mm_add_ps(x2, _load4(pd, pi));
							_store4(x2, pd, pi);
						}
						for (; pi < piLim; pi++, pm++)
						{
							__m128 x2 = _mm_mul_ss(x1, _mm_set_ss(*pm));
							x2 = _mm_add_ss(x2, _load1(pd, pi));
							_store1(x2, pd, pi);
						}
					}
				}

				// Padded.
				void MulTranConvU(bool add, const int * pmpcoliv, const int * pmpcolrow, const int * pmpcolrun,
					const int * pruns, const float * pcoefs, const float * psrc, float * pdst, int crow, int ccol)
				{
					if (!add)
						memset(pdst, 0, crow * sizeof(float));

					const int * piv = pmpcoliv;
					const int * prow = pmpcolrow;
					const float * psLim = psrc + ccol;

					const int * pirun = pmpcolrun;
					for (const float * ps = psrc; ps < psLim; ps++)
					{
						const float * pm = pcoefs + *piv++;
						float * pd = pdst + *prow++;
						int irun = *pirun++;
						const int * pi = pruns + 2 + irun;
						const int * piLim = pi + pi[-1];

						float x = *ps;
						__m128 x1 = _mm_set1_ps(x);
						if (irun == 0)
						{
							// No masking needed.
							for (; pi + 4 <= piLim; pi += 4, pm += 4)
							{
								__m128 x2 = _mm_mul_ps(x1, _mm_loadu_ps(pm));
								x2 = _mm_add_ps(x2, _load4(pd, pi));
								_store4(x2, pd, pi);
							}
							for (; pi < piLim; pi++, pm++)
							{
								__m128 x2 = _mm_mul_ss(x1, _mm_set_ss(*pm));
								x2 = _mm_add_ss(x2, _load1(pd, pi));
								_store1(x2, pd, pi);
							}
						}
						else
						{
							// Need masking.
							pm += pi[-2];
							const float * pmask = reinterpret_cast<const float *>(piLim);
							for (; pi + 4 <= piLim; pi += 4, pm += 4, pmask += 4)
							{
								__m128 x2 = _mm_mul_ps(_mm_and_ps(_mm_loadu_ps(pmask), x1), _mm_loadu_ps(pm));
								x2 = _mm_add_ps(x2, _load4(pd, pi));
								_store4(x2, pd, pi);
							}
							for (; pi < piLim; pi++, pm++, pmask++)
							{
								__m128 x2 = _mm_mul_ss(_mm_and_ps(_mm_set_ss(*pmask), x1), _mm_set_ss(*pm));
								x2 = _mm_add_ss(x2, _load1(pd, pi));
								_store1(x2, pd, pi);
							}
						}
					}
				}

				void MeanBackU(bool add, const int * pmpcolrow, const int * pmpcolindices, const int * pindices,
					const float * psrc, float * pdst, int crow, int ccol)
				{
					if (!add)
						memset(pdst, 0, crow * sizeof(float));

					const int * prow = pmpcolrow;
					const float * psLim = psrc + ccol;
					if (pmpcolindices == NULL)
					{
						int size = pindices[0];
						const int * piLim = pindices + 1 + size;
						for (const float * ps = psrc; ps < psLim; ps++)
						{
							float * pd = pdst + *prow++;
							const int * pi = pindices + 1;

							float x = *ps / size;
							__m128 x1 = _mm_set1_ps(x);
							for (; pi + 4 <= piLim; pi += 4)
							{
								__m128 x2 = _mm_add_ps(x1, _load4(pd, pi));
								_store4(x2, pd, pi);
							}
							for (; pi < piLim; pi++)
							{
								__m128 x2 = _mm_add_ss(x1, _load1(pd, pi));
								_store1(x2, pd, pi);
							}
						}
					}
					else
					{
						const int * pii = pmpcolindices;
						for (const float * ps = psrc; ps < psLim; ps++)
						{
							float * pd = pdst + *prow++;
							int ii = *pii++;

							const int * pi = pindices + ii;
							int size = *pi++;
							const int * piLim = pi + size;

							float x = *ps / size;
							__m128 x1 = _mm_set1_ps(x);
							for (; pi + 4 <= piLim; pi += 4)
							{
								__m128 x2 = _mm_add_ps(x1, _load4(pd, pi));
								_store4(x2, pd, pi);
							}
							for (; pi < piLim; pi++)
							{
								__m128 x2 = _mm_add_ss(x1, _load1(pd, pi));
								_store1(x2, pd, pi);
							}
						}
					}
				}

				void MaxBackU(bool add, const int * pmpcolrow, const int * pmpcolindices, const int * pindices,
					const float * psrc, float * pdst, const float * pval, int crow, int ccol)
				{
					if (!add)
						memset(pdst, 0, crow * sizeof(float));

					const int * prow = pmpcolrow;
					const float * psLim = psrc + ccol;
					if (pmpcolindices == NULL)
					{
						const int * piLim = pindices + 1 + pindices[0];
						for (const float * ps = psrc; ps < psLim; ps++)
						{
							int rowBase = *prow++;
							float * pd = pdst + rowBase;
							const float * pv = pval + rowBase;
							const int * pi = pindices + 1;

							int j = *pi++;
							float m = pv[j];
							for (; pi < piLim; pi++)
							{
								if (m < pv[*pi])
								{
									m = pv[*pi];
									j = *pi;
								}
							}
							pd[j] += *ps;
						}
					}
					else
					{
						const int * pii = pmpcolindices;
						for (const float * ps = psrc; ps < psLim; ps++)
						{
							int rowBase = *prow++;
							int ii = *pii++;
							float * pd = pdst + rowBase;
							const float * pv = pval + rowBase;
							const int * pi = pindices + ii + 1;
							const int * piLim = pi + pi[-1];

							int j = *pi++;
							float m = pv[j];
							for (; pi < piLim; pi++)
							{
								if (m < pv[*pi])
								{
									m = pv[*pi];
									j = *pi;
								}
							}
							pd[j] += *ps;
						}
					}
				}

				// REVIEW vishaljo: Try out SSE/AVX after padding support is added. AVX math platform uses the same code below.
				void RespNormBackU(bool add, float alpha, float beta, bool avgOverFullKernel, float offset,
					const int * pmpcolrow, const int * pmpcolindices, const int * pindices,
					const float * perrors, float * perrorsPrev, const float * pvaluesPrev, int crow, int ccol)
				{
					if (!add)
						memset(perrorsPrev, 0, crow * sizeof(float));

					const int * prow = pmpcolrow;
					const float * psLim = perrors + ccol;
					if (pmpcolindices == NULL)
					{
						int size = pindices[0];
						float scale = alpha / size;
						const int * piMin = pindices + 1;
						const int * piLim = piMin + size;
						for (const float * ps = perrors; ps < psLim; ps++)
						{
							int rowBase = *prow++;
							// First compute denominator: denom = offset + scale * Sum(Xj^2)
							float denom = 0;
							const float * pv = pvaluesPrev + rowBase;

							for (const int * pi = piMin; pi < piLim; pi++)
							{
								float cur = pv[*pi];
								denom += cur * cur;
							}
							denom = offset + scale * denom;
							float denomPow = powf(denom, -beta);
							// The output.
							float y = pv[0] * denomPow;

							// The update logic:
							//     srcError(*ps) X the derivative.
							//     derivative at i wrt center point = powf(denom, -beta) - 2* scale * beta * X[i] * y / denom.
							//     derivative at i wrt other points = - 2* scale * beta * X[i] * y / denom.
							float commonUpdate = *ps * (-2 * scale * beta * y) / denom;

							float * pd = perrorsPrev + rowBase;
							for (const int * pi = piMin; pi < piLim; pi++)
								pd[*pi] += pv[*pi] * commonUpdate;

							// Additional update for the center point.
							pd[0] += *ps * denomPow;
						}
					}
					else
					{
						int kernelSize = pindices[0];
						const int * pii = pmpcolindices;
						for (const float * ps = perrors; ps < psLim; ps++)
						{
							int rowBase = *prow++;
							// First compute denominator: denom = 1 + scale * Sum(Xj^2)
							float denom = 0;
							const float * pv = pvaluesPrev + rowBase;
							int ii = *pii++;

							const int * piMin = pindices + ii;
							int size = *piMin++;
							const int * piLim = piMin + size;

							for (const int * pi = piMin; pi < piLim; pi++)
							{
								float cur = pv[*pi];
								denom += cur * cur;
							}
							float scale = alpha / (avgOverFullKernel ? kernelSize : size);
							denom = offset + scale * denom;
							float denomPow = powf(denom, -beta);
							// The output.
							float y = pv[0] * denomPow;

							// The update logic:
							//     srcError(*ps) X the derivative.
							//     derivative at i wrt center point = powf(denom, -beta) - 2* scale * beta * X[i] * y / denom.
							//     derivative at i wrt other points = - 2* scale * beta * X[i] * y / denom.
							float commonUpdate = *ps * (-2 * scale * beta * y) / denom;

							float * pd = perrorsPrev + rowBase;
							for (const int * pi = piMin; pi < piLim; pi++)
								pd[*pi] += pv[*pi] * commonUpdate;

							// Additional update for the center point.
							pd[0] += *ps * denomPow;
						}
					}
				}

				template <bool useDecay>
				void AddXYTranA(float a, const float * px, const float * py, float * pmat, int crow, int ccol, float decay)
				{
					const float * pyBase = py;
					const float * pxLim = px + crow;
					const float * pyLim = py + ccol;
					float * pm = pmat;
					__m128 wd;
					if (useDecay)
						wd = _mm_set1_ps(1 - decay);
					for (; px < pxLim; px++)
					{
						float r = a * *px;
						py = pyBase;

						__m128 x1 = _mm_set1_ps(r);
						for (; py + 16 <= pyLim; py += 16, pm += 16)
						{
							__m128 x02 = _mm_load_ps(py);
							__m128 x12 = _mm_load_ps(py + 4);
							__m128 x22 = _mm_load_ps(py + 8);
							__m128 x32 = _mm_load_ps(py + 12);
							__m128 x03 = _mm_load_ps(pm);
							__m128 x13 = _mm_load_ps(pm + 4);
							__m128 x23 = _mm_load_ps(pm + 8);
							__m128 x33 = _mm_load_ps(pm + 12);
							x02 = _mm_mul_ps(x1, x02);
							x12 = _mm_mul_ps(x1, x12);
							x22 = _mm_mul_ps(x1, x22);
							x32 = _mm_mul_ps(x1, x32);
							if (useDecay)
							{
								x03 = _mm_mul_ps(wd, x03);
								x13 = _mm_mul_ps(wd, x13);
								x23 = _mm_mul_ps(wd, x23);
								x33 = _mm_mul_ps(wd, x33);
							}
							x03 = _mm_add_ps(x02, x03);
							x13 = _mm_add_ps(x12, x13);
							x23 = _mm_add_ps(x22, x23);
							x33 = _mm_add_ps(x32, x33);
							_mm_store_ps(pm, x03);
							_mm_store_ps(pm + 4, x13);
							_mm_store_ps(pm + 8, x23);
							_mm_store_ps(pm + 12, x33);
						}
						for (; py < pyLim; py += 4, pm += 4)
						{
							__m128 x02 = _mm_load_ps(py);
							__m128 x03 = _mm_load_ps(pm);
							x02 = _mm_mul_ps(x1, x02);
							if (useDecay)
								x03 = _mm_mul_ps(wd, x03);
							x03 = _mm_add_ps(x02, x03);
							_mm_store_ps(pm, x03);
						}
					}
				}

				void AddXYTranA(float a, const float * px, const int * pposY, const float * pvaluesY,
					int posMinY, int iposMinY, int iposLimY, float * pmat, int crow, int ccol)
				{
#if 1
					// REVIEW shonk: This is faster for MNIST, but the version below is faster for extremely sparse input.
					const int * pposMin = pposY + iposMinY;
					const int * pposLim = pposY + iposLimY;
					const float * pxLim = px + crow;
					float * pm0 = pmat - posMinY;
					const float * py = pvaluesY - posMinY;

					__m128 x0 = _mm_set1_ps(a);
					for (; px < pxLim; px += 4, pm0 += 4 * ccol)
					{
						float * pm1 = pm0 + ccol;
						float * pm2 = pm1 + ccol;
						float * pm3 = pm2 + ccol;

						__m128 x1 = _mm_load_ps(px);
						x1 = _mm_mul_ps(x1, x0);

						for (const int * ppos = pposMin; ppos < pposLim; ppos++)
						{
							int col = *ppos;
							__m128 x2 = _mm_set1_ps(py[col]);
							__m128 x3 = _mm_setr_ps(pm0[col], pm1[col], pm2[col], pm3[col]);
							x2 = _mm_mul_ps(x2, x1);
							x3 = _mm_add_ps(x3, x2);

							_mm_store_ss(pm0 + col, x3); x3 = _rotate(x3);
							_mm_store_ss(pm1 + col, x3); x3 = _rotate(x3);
							_mm_store_ss(pm2 + col, x3); x3 = _rotate(x3);
							_mm_store_ss(pm3 + col, x3);
						}
					}
#else
					const int * pposMin = pposY + iposMinY;
					const int * pposLim = pposY + iposLimY;
					const float * pxLim = px + crow;
					float * pm = pmat - posMinY;
					const float * py = pvaluesY - posMinY;

					__m128 x0 = _mm_set1_ps(a);
					int d1 = 1 * ccol;
					int d2 = 2 * ccol;
					int d3 = 3 * ccol;
					int d4 = 4 * ccol;
					for (const int * ppos = pposMin; ppos < pposLim; ppos++)
					{
						int col = *ppos;
						__m128 x2 = _mm_set1_ps(py[col]);
						x2 = _mm_mul_ps(x2, x0);

						float * pm0 = pm + col;
						for (const float * px0 = px; px0 < pxLim; px0 += 4, pm0 += d4)
						{
							__m128 x1 = _mm_load_ps(px0);
							__m128 x3 = _mm_setr_ps(pm0[0], pm0[d1], pm0[d2], pm0[d3]);
							x1 = _mm_mul_ps(x1, x2);
							x3 = _mm_add_ps(x3, x1);

							_mm_store_ss(pm0, x3); x3 = _rotate(x3);
							_mm_store_ss(pm0 + d1, x3); x3 = _rotate(x3);
							_mm_store_ss(pm0 + d2, x3); x3 = _rotate(x3);
							_mm_store_ss(pm0 + d3, x3);
						}
					}
#endif
				}

#pragma warning(push)
#pragma warning(disable: 4127) // conditional expression is constant

				template <bool useDecay>
				void AddXYTranU(float a, const float * px, const float * py,
					const int * pstarts, const int * pindices, float * pcoefs, int crow, float decay)
				{
					const int * pii = pstarts + 1;
					const int * pi = pindices;
					float * pm = pcoefs;
					const float * pxLim = px + crow;
					__m128 wd;
					if (useDecay)
						wd = _mm_set1_ps(1 - decay);
					for (; px < pxLim; px++)
					{
						const int * piLim = pindices + *pii++;
						float r = a * *px;

						__m128 x1 = _mm_set1_ps(r);
						for (; pi + 4 <= piLim; pi += 4, pm += 4)
						{
							__m128 x2 = _mm_mul_ps(x1, _load4(py, pi));
							__m128 x3 = _mm_loadu_ps(pm);
							if (useDecay)
								x3 = _mm_mul_ps(x3, wd);
							x2 = _mm_add_ps(x2, x3);
							_mm_storeu_ps(pm, x2);
						}
						for (; pi < piLim; pi++, pm++)
							*pm = (useDecay ? (*pm * (1 - decay)) : *pm) + py[*pi] * r;
					}
				}
#pragma warning(pop)

				// Unpadded.
				void AddXYTranConvU(float a, const float * px, const float * py, const int * pmprowiv,
					const int * pmprowcol, const int * pruns, float * pcoefs, int crow)
				{
					int size = pruns[1];
					const int * psupport = pruns + 2;
					const int * piv = pmprowiv;
					const int * pcol = pmprowcol;
					const float * pxLim = px + crow;
					const int * piLim = psupport + size;

					for (; px < pxLim; px++)
					{
						float * pm = pcoefs + *piv++;
						const float * ps = py + *pcol++;
						const int * pi = psupport;
						float r = a * *px;

						__m128 x1 = _mm_set1_ps(r);
						for (; pi + 4 <= piLim; pi += 4, pm += 4)
						{
							__m128 x2 = _mm_mul_ps(x1, _load4(ps, pi));
							x2 = _mm_add_ps(x2, _mm_loadu_ps(pm));
							_mm_storeu_ps(pm, x2);
						}
						for (; pi < piLim; pi++, pm++)
							*pm += ps[*pi] * r;
						// Update the bias.
						*pm += r;
					}
				}

				// Padded.
				void AddXYTranConvU(float a, const float * px, const float * py, const int * pmprowiv,
					const int * pmprowcol, const int * pmprowrun, const int * pruns, float * pcoefs, int crow)
				{
					const int * piv = pmprowiv;
					const int * pcol = pmprowcol;
					const float * pxLim = px + crow;
					int kernelSize = pruns[1];

					const int * pirun = pmprowrun;
					for (; px < pxLim; px++)
					{
						float * pm = pcoefs + *piv++;
						const float * ps = py + *pcol++;
						int irun = *pirun++;
						const int * pi = pruns + 2 + irun;
						const int * piLim = pi + pi[-1];

						float r = a * *px;

						// Update the bias.
						pm[kernelSize] += r;

						__m128 x1 = _mm_set1_ps(r);
						if (irun == 0)
						{
							// No masking needed.
							for (; pi + 4 <= piLim; pi += 4, pm += 4)
							{
								__m128 x2 = _mm_mul_ps(x1, _load4(ps, pi));
								x2 = _mm_add_ps(x2, _mm_loadu_ps(pm));
								_mm_storeu_ps(pm, x2);
							}
							for (; pi < piLim; pi++, pm++)
								*pm += ps[*pi] * r;
						}
						else
						{
							// Need masking.
							pm += pi[-2];
							const float * pmask = reinterpret_cast<const float *>(piLim);
							for (; pi + 4 <= piLim; pi += 4, pm += 4, pmask += 4)
							{
								__m128 x2 = _mm_mul_ps(_mm_and_ps(_mm_loadu_ps(pmask), x1), _load4(ps, pi));
								x2 = _mm_add_ps(x2, _mm_loadu_ps(pm));
								_mm_storeu_ps(pm, x2);
							}
							for (; pi < piLim; pi++, pm++, pmask++)
							{
								__m128 x2 = _mm_mul_ss(_mm_and_ps(_mm_set_ss(*pmask), x1), _load1(ps, pi));
								x2 = _mm_add_ss(x2, _mm_set_ss(*pm));
								_mm_store_ss(pm, x2);
							}
						}
					}
				}

				void AddXYTranA(float a, const float * px, const float * py, float * pmat, float momentum, float * pdel, int crow, int ccol)
				{
					const float * pyBase = py;
					const float * pxLim = px + crow;
					const float * pyLim = py + ccol;
					float * pm = pmat;
					float * pd = pdel;

					__m128 x0 = _mm_set1_ps(momentum);
					for (; px < pxLim; px++)
					{
						float r = a * *px;

						__m128 x1 = _mm_set1_ps(r);
						for (py = pyBase; py < pyLim; pm += 4, pd += 4, py += 4)
						{
							__m128 x2 = _mm_load_ps(py);
							__m128 x3 = _mm_load_ps(pd);
							__m128 x4 = _mm_load_ps(pm);
							x2 = _mm_mul_ps(x1, x2);
							x3 = _mm_mul_ps(x0, x3);
							x3 = _mm_add_ps(x2, x3);
							x4 = _mm_add_ps(x3, x4);

							_mm_store_ps(pd, x3);
							_mm_store_ps(pm, x4);
						}
					}
				}

				// coef: coefs to update, ag: accumulated grads, au: accumulated updates, g: cur grads.
				// Note: parameters coef, ag, au and g will be updated, do not reuse parameter g in calling code.
				inline void UpdateAdadelta(__m128& coef, __m128& ag, __m128& au, __m128& g, const __m128& dec, const __m128& decc, const __m128& c)
				{
					__m128 x4 = _mm_mul_ps(g, g);   // x4 == g * g
					x4 = _mm_mul_ps(decc, x4);      // x4 == (1 - decay) * g * g
					ag = _mm_mul_ps(dec, ag);       // ag == decay * accG
					ag = _mm_add_ps(ag, x4);        // ag == decay * accG + (1 - decay) * g * g
					__m128 x41 = _mm_add_ps(ag, c); // x41 == ag + cond
					__m128 x51 = _mm_add_ps(au, c); // x51 == accU + cond
#if 0
					// naive version:
					x51 = _mm_div_ps(x51, x41);
					x41 = _mm_sqrt_ps(x51);         // x41 == rate
#else
					// faster (approximate) version:
					x41 = _mm_rsqrt_ps(x41);
					__m128 x52 = _mm_rsqrt_ps(x51);
					x51 = _mm_mul_ps(x51, x52);
					x41 = _mm_mul_ps(x41, x51);     // x41 == rate
#endif
					g = _mm_mul_ps(g, x41);         // g - current update
					coef = _mm_add_ps(coef, g);

					g = _mm_mul_ps(g, g);           // g  == newU * newU
					g = _mm_mul_ps(decc, g);        // g  == (1 - decay) * newU * newU
					au = _mm_mul_ps(dec, au);       // au == decay * accU
					au = _mm_add_ps(au, g);         // au == decay * accU + (1 - decay) * newU * newU
				}

				void AddXYTranA(const float * px, const float * py, float * pmat, float * paccGrads, float * paccUpdates,
					float decay, float cond, int crow, int ccol)
				{
					const float * pyBase = py;
					const float * pxLim = px + crow;
					const float * pyLim = py + ccol;
					float * pm = pmat;
					float * pag = paccGrads;
					float * pau = paccUpdates;

					__m128 dec = _mm_set1_ps(decay);
					__m128 decc = _mm_set1_ps(1 - decay);
					__m128 c = _mm_set1_ps(cond);
					for (; px < pxLim; px++)
					{
						float r = *px;

						__m128 x1 = _mm_set1_ps(r);
						for (py = pyBase; py < pyLim; pm += 4, pag += 4, pau += 4, py += 4)
						{
							__m128 x2 = _mm_load_ps(py);
							__m128 ag = _mm_load_ps(pag);
							__m128 au = _mm_load_ps(pau);
							__m128 coef = _mm_load_ps(pm);
							x2 = _mm_mul_ps(x1, x2);        // x2 == g

							UpdateAdadelta(coef, ag, au, x2, dec, decc, c);

							_mm_store_ps(pm, coef);
							_mm_store_ps(pag, ag);
							_mm_store_ps(pau, au);
						}
					}
				}

				void AddXYTranU(const float * px, const float * py, const int * pstarts, const int * pindices,
					float * pcoefs, float * paccGrads, float * paccUpdates, float decay, float cond, int crow)
				{
					const int * pii = pstarts + 1;
					const int * pi = pindices;
					float * pm = pcoefs;
					const float * pxLim = px + crow;
					float * pag = paccGrads;
					float * pau = paccUpdates;

					__m128 dec = _mm_set1_ps(decay);
					__m128 decc = _mm_set1_ps(1 - decay);
					__m128 c = _mm_set1_ps(cond);

					for (; px < pxLim; px++)
					{
						const int * piLim = pindices + *pii++;
						float r = *px;

						__m128 x1 = _mm_set1_ps(r);
						for (; pi + 4 <= piLim; pi += 4, pm += 4, pag += 4, pau += 4)
						{
							__m128 g = _mm_mul_ps(x1, _load4(py, pi));
							__m128 ag = _mm_loadu_ps(pag);
							__m128 au = _mm_loadu_ps(pau);
							__m128 coef = _mm_loadu_ps(pm);

							UpdateAdadelta(coef, ag, au, g, dec, decc, c);

							_mm_storeu_ps(pm, coef);
							_mm_storeu_ps(pag, ag);
							_mm_storeu_ps(pau, au);
						}

						if (pi < piLim)
						{
							size_t ctail = piLim - pi;
							__m128 g = _mm_mul_ss(_load1(py, pi++), x1);
							__m128 ag = _mm_load_ss(pag++);
							__m128 au = _mm_load_ss(pau++);
							__m128 coef = _mm_load_ss(pm++);
							for (; pi < piLim; pi++, pm++, pag++, pau++)
							{
								g = _mm_or_ps(_mm_mul_ss(_load1(py, pi), x1), _rotate(g));
								ag = _mm_or_ps(_mm_load_ss(pag), _rotate(ag));
								au = _mm_or_ps(_mm_load_ss(pau), _rotate(au));
								coef = _mm_or_ps(_mm_load_ss(pm), _rotate(coef));
							}
							UpdateAdadelta(coef, ag, au, g, dec, decc, c);
							for (int i = 0; i < ctail; i++)
							{
								_mm_store_ss(pm - i - 1, coef);
								coef = _rotate_reverse(coef);
								_mm_store_ss(pag - i - 1, ag);
								ag = _rotate_reverse(ag);
								_mm_store_ss(pau - i - 1, au);
								au = _rotate_reverse(au);
							}
						}
					}
				}

				void AddXYTranA(const float * px, const int * pposY, const float * pvaluesY,
					int posMinY, int iposMinY, int iposLimY, float * pmat, float * paccGrads, float * paccUpdates,
					float decay, float cond, int crow, int ccol)
				{
					const int * pposMin = pposY + iposMinY;
					const int * pposLim = pposY + iposLimY;
					const float * pxLim = px + crow;
					const float * py = pvaluesY - posMinY;
					float * pm0 = pmat - posMinY;
					float * pag0 = paccGrads - posMinY;
					float * pau0 = paccUpdates - posMinY;

					__m128 dec = _mm_set1_ps(decay);
					__m128 decc = _mm_set1_ps(1 - decay);
					__m128 c = _mm_set1_ps(cond);
					for (; px < pxLim; px += 4, pm0 += 4 * ccol, pag0 += 4 * ccol, pau0 += 4 * ccol)
					{
						float * pm1 = pm0 + ccol;
						float * pm2 = pm1 + ccol;
						float * pm3 = pm2 + ccol;

						float * pag1 = pag0 + ccol;
						float * pag2 = pag1 + ccol;
						float * pag3 = pag2 + ccol;

						float * pau1 = pau0 + ccol;
						float * pau2 = pau1 + ccol;
						float * pau3 = pau2 + ccol;

						__m128 x1 = _mm_load_ps(px);

						for (const int * ppos = pposMin; ppos < pposLim; ppos++)
						{
							int col = *ppos;
							__m128 x2 = _mm_set1_ps(py[col]);
							__m128 ag = _mm_setr_ps(pag0[col], pag1[col], pag2[col], pag3[col]);
							__m128 au = _mm_setr_ps(pau0[col], pau1[col], pau2[col], pau3[col]);
							__m128 coef = _mm_setr_ps(pm0[col], pm1[col], pm2[col], pm3[col]);
							x2 = _mm_mul_ps(x2, x1);

							UpdateAdadelta(coef, ag, au, x2, dec, decc, c);

							_mm_store_ss(pm0 + col, coef); coef = _rotate(coef);
							_mm_store_ss(pm1 + col, coef); coef = _rotate(coef);
							_mm_store_ss(pm2 + col, coef); coef = _rotate(coef);
							_mm_store_ss(pm3 + col, coef);

							_mm_store_ss(pag0 + col, ag); ag = _rotate(ag);
							_mm_store_ss(pag1 + col, ag); ag = _rotate(ag);
							_mm_store_ss(pag2 + col, ag); ag = _rotate(ag);
							_mm_store_ss(pag3 + col, ag);

							_mm_store_ss(pau0 + col, au); au = _rotate(au);
							_mm_store_ss(pau1 + col, au); au = _rotate(au);
							_mm_store_ss(pau2 + col, au); au = _rotate(au);
							_mm_store_ss(pau3 + col, au);
						}
					}
				}

				void ScaleU(const float* pSrc, float * pd, float a, int c)
				{
					float * pdLim = pd + c;

					__m128 x1 = _mm_set1_ps(a);
					for (; pd + 4 <= pdLim; pSrc += 4, pd += 4)
					{
						__m128 x2 = _mm_loadu_ps(pSrc);
						x2 = _mm_mul_ps(x1, x2);
						_mm_storeu_ps(pd, x2);
					}

					for (; pd < pdLim; pd++, pSrc++)
					{
						*pd = *pSrc*a;
					}
				}

				void ScaleA(float a, float * pd, int c)
				{
					float * pdLim = pd + c;

					__m128 x1 = _mm_set1_ps(a);
					for (; pd < pdLim; pd += 4)
					{
						__m128 x2 = _mm_load_ps(pd);
						x2 = _mm_mul_ps(x1, x2);
						_mm_store_ps(pd, x2);
					}
				}

				void ScaleMaxNormU(float maxNorm, int kernCount, int kernSize, float * pmat)
				{
					float * pm = pmat;
					for (int irow = 0; irow < kernCount; irow++)
					{
						float rowNorm = 0;
						for (int icol = 0; icol < kernSize; icol++)
						{
							rowNorm += *pm * *pm;
							pm++;
						}
						if (rowNorm > maxNorm * maxNorm)
						{
							float scale = maxNorm / std::sqrt(rowNorm);
							pm -= kernSize;
							for (int icol = 0; icol < kernSize; icol++)
							{
								*pm *= scale;
								pm++;
							}
						}
						// Skip bias.
						pm++;
					}
				}

				void ScaleMaxNormA(float maxNorm, float * pmat, int crow, int ccol)
				{
					float * pm = pmat;
					float maxNormSq = maxNorm * maxNorm;
					__m128 m = _mm_set1_ps(maxNorm);
					for (int irow = 0; irow < crow; irow++)
					{
						__m128 rowNorm = _mm_set1_ps(0);
						float * pms = pm;
						float * pmLim = pm + ccol;
						for (; pm < pmLim; pm += 4)
						{
							__m128 x1 = _mm_load_ps(pm);
							x1 = _mm_mul_ps(x1, x1);
							rowNorm = _mm_add_ps(x1, rowNorm);
						}
						rowNorm = _mm_hadd_ps(rowNorm, rowNorm);
						rowNorm = _mm_hadd_ps(rowNorm, rowNorm);
						float rowNormRes = _mm_cvtss_f32(rowNorm);
						if (rowNormRes > maxNormSq)
						{
							__m128 scale = _mm_set1_ps(rowNormRes);
#if 0
							// REVIEW alexeyk: this is faster but it uses approximation so results differ significantly from CLR.
							scale = _mm_rsqrt_ps(scale);
							scale = _mm_mul_ps(scale, m);
#else
							scale = _mm_sqrt_ps(scale);
							scale = _mm_div_ps(m, scale);
#endif
							for (pm = pms; pm < pmLim; pm += 4)
							{
								__m128 x1 = _mm_load_ps(pm);
								x1 = _mm_mul_ps(x1, scale);
								_mm_store_ps(pm, x1);
							}
						}
					}
				}

				void ScaleMaxNormTranU(float maxNorm, float * pmat, int crow, int ccol)
				{
					for (int icol = 0; icol < ccol; icol++)
					{
						float * pm = pmat + icol;
						float rowNorm = 0;
						for (int irow = 0; irow < crow; irow++)
						{
							rowNorm += *pm * *pm;
							pm += ccol;
						}
						if (rowNorm > maxNorm * maxNorm)
						{
							float scale = maxNorm / std::sqrt(rowNorm);
							pm = pmat + icol;
							for (int irow = 0; irow < crow; irow++)
							{
								*pm *= scale;
								pm += ccol;
							}
						}
					}
				}

				void ScaleMaxNormU(float maxNorm, const int * pstarts, float * pmat, int crow)
				{
					for (int irow = 0; irow < crow; irow++)
					{
						float rowNorm = 0;
						for (int idx = pstarts[irow]; idx < pstarts[irow + 1]; idx++)
						{
							rowNorm += pmat[idx] * pmat[idx];
						}
						if (rowNorm > maxNorm * maxNorm)
						{
							float scale = maxNorm / std::sqrt(rowNorm);
							for (int idx = pstarts[irow]; idx < pstarts[irow + 1]; idx++)
							{
								pmat[idx] *= scale;
							}
						}
					}
				}

				void AddScaleA(float a, const float * ps, float * pd, int c)
				{
					float * pdLim = pd + c;

					__m128 x1 = _mm_set1_ps(a);
					for (; pd < pdLim; pd += 4, ps += 4)
					{
						__m128 x2 = _mm_load_ps(ps);
						__m128 x3 = _mm_load_ps(pd);
						x2 = _mm_mul_ps(x1, x2);
						x3 = _mm_add_ps(x2, x3);
						_mm_store_ps(pd, x3);
					}
				}

				void AddScaleU(float a, const float * ps, float * pd, int c)
				{
					float * pdLim = pd + c;

					__m128 x1 = _mm_set1_ps(a);
					for (; pd + 4 <= pdLim; pd += 4, ps += 4)
					{
						__m128 x2 = _mm_loadu_ps(ps);
						__m128 x3 = _mm_loadu_ps(pd);
						x2 = _mm_mul_ps(x2, x1);
						x3 = _mm_add_ps(x3, x2);
						_mm_storeu_ps(pd, x3);
					}

					for (; pd < pdLim; pd++, ps++)
					{
						__m128 x2 = _mm_load_ss(ps);
						__m128 x3 = _mm_load_ss(pd);
						x2 = _mm_mul_ss(x2, x1);
						x3 = _mm_add_ss(x3, x2);
						_mm_store_ss(pd, x3);
					}
				}

				void AddU(const float * left, const float * right, float* pd, int c)
				{
					float * pdLim = pd + c;

					for (; pd + 4 <= pdLim; pd += 4, left += 4, right += 4)
					{
						__m128 x1 = _mm_loadu_ps(left);
						__m128 x2 = _mm_loadu_ps(right);
						x2 = _mm_add_ps(x1, x2);
						_mm_storeu_ps(pd, x2);
					}

					for (; pd < pdLim; pd++, right++, left++)
					{
						*pd = (*left) + (*right);
					}
				}

				void AddScaleU(const float * pSrcLeft, const float* pSrcRight, float leftWeight, float rightWeight, float * pDst, int c)
				{
					if (leftWeight == 1.0f && rightWeight == 1.0f)
					{
						AddU(pSrcLeft, pSrcRight, pDst, c);
						return;
					}
					else if (leftWeight == 0.0f)
					{
						ScaleU(pSrcRight, pDst, rightWeight, c);
						return;
					}
					else if (leftWeight != 0)
					{
						rightWeight = rightWeight / leftWeight;
					}

					float * pdLim = pDst + c;
					float * pDstStart = pDst;
					__m128 w2 = _mm_set1_ps(rightWeight);
					for (; pDst + 4 <= pdLim; pDst += 4, pSrcLeft += 4, pSrcRight += 4)
					{
						__m128 x1 = _mm_loadu_ps(pSrcLeft);
						__m128 x2 = _mm_loadu_ps(pSrcRight);
						x2 = _mm_mul_ps(x2, w2);
						x2 = _mm_add_ps(x1, x2);
						_mm_storeu_ps(pDst, x2);
					}

					if (leftWeight != 1.0 && leftWeight != 0)
					{
						pDst = pDstStart;
						__m128 w1 = _mm_set1_ps(leftWeight);
						for (; pDst + 4 <= pdLim; pDst += 4)
						{
							__m128 x1 = _mm_loadu_ps(pDst);
							x1 = _mm_mul_ps(x1, w1);
							_mm_storeu_ps(pDst, x1);
						}
					}

					for (; pDst < pdLim; pDst++, pSrcLeft++, pSrcRight++)
					{
						*pDst = (*pSrcLeft * leftWeight + *pSrcRight *rightWeight);
					}
				}

				void AddScaleU(float a, const float * ps, const int * pi, float * pd, int c)
				{
					const int * piLim = pi + c;

					__m128 x1 = _mm_set1_ps(a);
					for (; pi + 4 <= piLim; pi += 4, ps += 4)
					{
						__m128 x2 = _mm_loadu_ps(ps);
						__m128 x3 = _load4(pd, pi);
						x2 = _mm_mul_ps(x2, x1);
						x3 = _mm_add_ps(x3, x2);
						_store4(x3, pd, pi);
					}

					for (; pi < piLim; pi++, ps++)
						pd[*pi] += a * *ps;
				}

				void AddScaleA(float a, const float * ps, float * pd, float momentum, float * pe, int c)
				{
					float * pdLim = pd + c;

					__m128 x0 = _mm_set1_ps(momentum);
					__m128 x1 = _mm_set1_ps(a);
					for (; pd < pdLim; pd += 4, pe += 4, ps += 4)
					{
						__m128 x2 = _mm_load_ps(ps);
						__m128 x3 = _mm_load_ps(pe);
						__m128 x4 = _mm_load_ps(pd);
						x2 = _mm_mul_ps(x1, x2);
						x3 = _mm_mul_ps(x0, x3);
						x3 = _mm_add_ps(x2, x3);
						x4 = _mm_add_ps(x3, x4);
						_mm_store_ps(pe, x3);
						_mm_store_ps(pd, x4);
					}
				}

				void AddScaleA(const float * ps, float * pd, float * paccGrads, float * paccUpdates,
					float decay, float cond, int c)
				{
					float * pdLim = pd + c;

					__m128 dec = _mm_set1_ps(decay);
					__m128 decc = _mm_set1_ps(1 - decay);
					__m128 cnd = _mm_set1_ps(cond);
					for (; pd < pdLim; pd += 4, ps += 4, paccGrads += 4, paccUpdates += 4)
					{
						__m128 g = _mm_load_ps(ps);
						__m128 ag = _mm_load_ps(paccGrads);
						__m128 au = _mm_load_ps(paccUpdates);
						__m128 coef = _mm_load_ps(pd);

						UpdateAdadelta(coef, ag, au, g, dec, decc, cnd);

						_mm_store_ps(pd, coef);
						_mm_store_ps(paccGrads, ag);
						_mm_store_ps(paccUpdates, au);
					}
				}

				void AddA(const float * ps, float * pd, int c)
				{
					float * pdLim = pd + c;

					for (; pd < pdLim; pd += 4, ps += 4)
					{
						__m128 x1 = _mm_load_ps(ps);
						__m128 x2 = _mm_load_ps(pd);
						x2 = _mm_add_ps(x1, x2);
						_mm_store_ps(pd, x2);
					}
				}

				void AddAndClearU(const float * left, const float * right, float* pd, float *pClear, int count)
				{
					float * pdLim = pd + count;
					__m128 cZero = _mm_set1_ps(0.0f);
					for (; pd + 4 <= pdLim; pd += 4, left += 4, right += 4, pClear += 4)
					{
						__m128 x1 = _mm_loadu_ps(left);
						__m128 x2 = _mm_loadu_ps(right);
						x2 = _mm_add_ps(x1, x2);
						_mm_storeu_ps(pd, x2);
						_mm_storeu_ps(pClear, cZero);
					}

					for (; pd < pdLim; pd++, right++, left++, pClear++)
					{
						*pd = (*left) + (*right);
						*pClear = 0;
					}
				}

				void AddAndClearU(const float * left, const float * right, float* pd, float *pClear, float rightWeight, int count)
				{
					if (rightWeight == 1)
					{
						AddAndClearU(left, right, pd, pClear, count);
					}
					else
					{
						float * pdLim = pd + count;
						__m128 cZero = _mm_set1_ps(0.0f);
						__m128 weight = _mm_set1_ps(rightWeight);
						for (; pd + 4 <= pdLim; pd += 4, left += 4, right += 4, pClear += 4)
						{
							__m128 x1 = _mm_loadu_ps(left);
							__m128 x2 = _mm_loadu_ps(right);
							x2 = _mm_add_ps(x1, _mm_mul_ps(weight, x2));
							_mm_storeu_ps(pd, x2);
							_mm_storeu_ps(pClear, cZero);
						}

						for (; pd < pdLim; pd++, right++, left++, pClear++)
						{
							*pd = (*left) + rightWeight*(*right);
							*pClear = 0;
						}
					}
				}

				void AddScaleU(float * left, float * right, int count)
				{
					float * pdLim = left + count;
					__m128 cZero = _mm_set1_ps(0.0f);
					for (; left + 4 <= pdLim; left += 4, right += 4)
					{
						while (*right == 0 && left + 4 < pdLim)
						{
							right++;
							left++;
						}

						__m128 x1 = _mm_loadu_ps(left);
						__m128 x2 = _mm_loadu_ps(right);
						x2 = _mm_add_ps(x1, x2);
						_mm_storeu_ps(left, x2);
					}

					for (; left < pdLim; right++, left++)
					{
						*left = (*left) + (*right);
					}
				}

				void AddScaleU(float * left, float * right, float weight, int count)
				{
					if (weight == 1)
					{
						AddScaleU(left, right, count);
					}
					else
					{
						float * pdLim = left + count;
						__m128 cweight = _mm_set1_ps(weight);
						for (; left + 4 <= pdLim; left += 4, right += 4)
						{
							while (*right == 0 && left + 4 < pdLim)
							{
								right++;
								left++;
							}

							__m128 x1 = _mm_loadu_ps(left);
							__m128 x2 = _mm_loadu_ps(right);
							x2 = _mm_add_ps(x1, _mm_mul_ps(x2, cweight));
							_mm_storeu_ps(left, x2);
						}

						for (; left < pdLim; right++, left++)
						{
							*left = (*left) + weight*(*right);
						}
					}
				}

				void AddAndClearU(float * left, float * right, int count)
				{
					float * pdLim = left + count;
					__m128 cZero = _mm_set1_ps(0.0f);
					for (; left + 4 <= pdLim; left += 4, right += 4)
					{
						while (*right == 0 && left + 4 < pdLim)
						{
							right++;
							left++;
						}

						__m128 x1 = _mm_loadu_ps(left);
						__m128 x2 = _mm_loadu_ps(right);
						x2 = _mm_add_ps(x1, x2);
						_mm_storeu_ps(left, x2);
						_mm_storeu_ps(right, cZero);
					}

					for (; left < pdLim; right++, left++)
					{
						*left = (*left) + (*right);
						*right = 0;
					}
				}

				void AddAndClearU(float * left, float * right, float rightWeight, int count)
				{
					if (rightWeight == 1)
					{
						AddAndClearU(left, right, count);
					}
					else
					{
						float * pdLim = left + count;
						__m128 cZero = _mm_set1_ps(0.0f);
						__m128 weight = _mm_set1_ps(rightWeight);
						for (; left + 4 <= pdLim; left += 4, right += 4)
						{
							while (*right == 0 && left + 4 < pdLim)
							{
								right++;
								left++;
							}

							__m128 x1 = _mm_loadu_ps(left);
							__m128 x2 = _mm_loadu_ps(right);
							x2 = _mm_add_ps(x1, _mm_mul_ps(weight, x2));
							_mm_storeu_ps(left, x2);
							_mm_storeu_ps(right, cZero);
						}

						for (; left < pdLim; right++, left++)
						{
							*left = (*left) + rightWeight*(*right);
							*right = 0;
						}
					}
				}

				void SubU(const float * left, const float * right, float* pd, int c)
				{
					float * pdLim = pd + c;

					for (; pd + 4 <= pdLim; pd += 4, left += 4, right += 4)
					{
						__m128 x1 = _mm_loadu_ps(left);
						__m128 x2 = _mm_loadu_ps(right);
						x2 = _mm_sub_ps(x1, x2);
						_mm_storeu_ps(pd, x2);
					}

					for (; pd < pdLim; pd++, right++, left++)
					{
						*pd = (*left) - (*right);
					}
				}

				void AddU(const float * ps, const int * pi, float * pd, int c)
				{
					const int * piLim = pi + c;

					for (; pi + 4 <= piLim; pi += 4, ps += 4)
					{
						__m128 x1 = _load4(pd, pi);
						__m128 x2 = _mm_loadu_ps(ps);
						x1 = _mm_add_ps(x1, x2);
						_store4(x1, pd, pi);
					}

					for (; pi < piLim; pi++, ps++)
						pd[*pi] += *ps;
				}

				float SumA(const float * ps, int c)
				{
					const float * psLim = ps + c;

					__m128 res = _mm_setzero_ps();
					for (; ps < psLim; ps += 4)
						res = _mm_add_ps(res, _mm_load_ps(ps));

					res = _mm_hadd_ps(res, res);
					res = _mm_hadd_ps(res, res);

					return _mm_cvtss_f32(res);
				}

				float SumU(const float * ps, int c)
				{
					const float * psLim = ps + c;

					__m128 res = _mm_setzero_ps();
					for (; ps + 4 <= psLim; ps += 4)
						res = _mm_add_ps(res, _mm_loadu_ps(ps));

					res = _mm_hadd_ps(res, res);
					res = _mm_hadd_ps(res, res);

					for (; ps < psLim; ps++)
						res = _mm_add_ss(res, _mm_load_ss(ps));

					return _mm_cvtss_f32(res);
				}

				float SumU(const float * ps, const int * pi, int c)
				{
					const int * piLim = pi + c;

					__m128 res = _mm_setzero_ps();
					for (; pi + 4 <= piLim; pi += 4)
					{
						__m128 x = _load4(ps, pi);
						res = _mm_add_ps(res, x);
					}

					res = _mm_hadd_ps(res, res);
					res = _mm_hadd_ps(res, res);

					for (; pi < piLim; pi++)
					{
						__m128 x = _load1(ps, pi);
						res = _mm_add_ss(res, x);
					}

					return _mm_cvtss_f32(res);
				}

				float SumSqU(const float * ps, int c)
				{
					const float * psLim = ps + c;

					__m128 res = _mm_setzero_ps();
					for (; ps + 4 <= psLim; ps += 4)
					{
						__m128 x = _mm_loadu_ps(ps);
						res = _mm_add_ps(res, _mm_mul_ps(x, x));
					}

					res = _mm_hadd_ps(res, res);
					res = _mm_hadd_ps(res, res);

					for (; ps < psLim; ps++)
					{
						__m128 x = _mm_load_ss(ps);
						res = _mm_add_ss(res, _mm_mul_ss(x, x));
					}

					return _mm_cvtss_f32(res);
				}

				float SumSqU(float mean, const float * ps, int c)
				{
					const float * psLim = ps + c;

					__m128 res = _mm_setzero_ps();
					__m128 m = _mm_set1_ps(mean);
					for (; ps + 4 <= psLim; ps += 4)
					{
						__m128 x = _mm_loadu_ps(ps);
						x = _mm_sub_ps(x, m);
						res = _mm_add_ps(res, _mm_mul_ps(x, x));
					}

					res = _mm_hadd_ps(res, res);
					res = _mm_hadd_ps(res, res);

					for (; ps < psLim; ps++)
					{
						__m128 x = _mm_load_ss(ps);
						x = _mm_sub_ss(x, m);
						res = _mm_add_ss(res, _mm_mul_ss(x, x));
					}

					return _mm_cvtss_f32(res);
				}

				float SumAbsU(const float * ps, int c)
				{
					const float * psLim = ps + c;

					__m128 mask = _mm_castsi128_ps(_mm_set1_epi32(0x7FFFFFFF));
					__m128 res = _mm_setzero_ps();
					for (; ps + 4 <= psLim; ps += 4)
						res = _mm_add_ps(res, _mm_and_ps(_mm_loadu_ps(ps), mask));

					res = _mm_hadd_ps(res, res);
					res = _mm_hadd_ps(res, res);

					for (; ps < psLim; ps++)
						res = _mm_add_ss(res, _mm_and_ps(_mm_load_ss(ps), mask));

					return _mm_cvtss_f32(res);
				}

				float MaxAbsU(const float * ps, int c)
				{
					const float * psLim = ps + c;

					__m128 mask = _mm_castsi128_ps(_mm_set1_epi32(0x7FFFFFFF));
					__m128 res = _mm_setzero_ps();
					for (; ps + 4 <= psLim; ps += 4)
						res = _mm_max_ps(res, _mm_and_ps(_mm_loadu_ps(ps), mask));

					__m128 x1 = _mm_shuffle_ps(res, res, 0xB1);
					res = _mm_max_ps(res, x1);
					x1 = _mm_shuffle_ps(res, res, 0x02);
					res = _mm_max_ss(res, x1);

					for (; ps < psLim; ps++)
						res = _mm_max_ss(res, _mm_and_ps(_mm_load_ss(ps), mask));

					return _mm_cvtss_f32(res);
				}

				float DotU(const float * pa, const float * pb, int c)
				{
					const float * paLim = pa + c;

					__m128 res = _mm_setzero_ps();
					for (; pa + 4 <= paLim; pa += 4, pb += 4)
						res = _mm_add_ps(res, _mm_mul_ps(_mm_loadu_ps(pa), _mm_loadu_ps(pb)));

					res = _mm_hadd_ps(res, res);
					res = _mm_hadd_ps(res, res);

					for (; pa < paLim; pa++, pb++)
						res = _mm_add_ss(res, _mm_mul_ss(_mm_load_ss(pa), _mm_load_ss(pb)));

					return _mm_cvtss_f32(res);
				}

				float DotU(const float * pa, const float * pb, const int * pi, int c)
				{
					const int * piLim = pi + c;

					__m128 res = _mm_setzero_ps();
					for (; pi + 4 <= piLim; pi += 4, pb += 4)
					{
						__m128 x = _mm_mul_ps(_load4(pa, pi), _mm_loadu_ps(pb));
						res = _mm_add_ps(res, x);
					}

					res = _mm_hadd_ps(res, res);
					res = _mm_hadd_ps(res, res);

					for (; pi < piLim; pi++, pb++)
					{
						__m128 x = _mm_mul_ss(_load1(pa, pi), _mm_load_ss(pb));
						res = _mm_add_ss(res, x);
					}

					return _mm_cvtss_f32(res);
				}

				float Dist2(const float* px, const float* py, int c)
				{
					const float * pxLim = px + c;
					__m128 norm2_4 = _mm_setzero_ps();
					for (; px + 4 <= pxLim; px += 4, py += 4)
					{
						__m128 d = _mm_sub_ps(_mm_loadu_ps(px), _mm_loadu_ps(py));
						norm2_4 = _mm_add_ps(norm2_4, _mm_mul_ps(d, d));
					}
					norm2_4 = _mm_hadd_ps(norm2_4, norm2_4);
					norm2_4 = _mm_hadd_ps(norm2_4, norm2_4);

					float norm2 = _mm_cvtss_f32(norm2_4);
					for (; px < pxLim; ++px, ++py)
					{
						float d = *px - *py;
						norm2 += d * d;
					}

					return norm2;
				}

				// This is modeled after double-based SSE code authored by Mikhail Parakhin (mikhailp).

				// 1 / ln(2).
				//const float RecipLn2 = (float)1.44269504088896340735992468100;

				// Used for computing a 4th degree polynomial approximation of e^x.
				//const float Coef1 = (float)0.013555747234814917704030793;
				//const float Coef2 = (float)0.065588116243247810171479524;
				//const float Coef3 = (float)0.3069678791803394491901401;

				//const float ExpInf = 128;
				//const int ExpBias = 127;
				//const int ExpShift = 23;

				float ExpFast(float arg)
				{
					bool neg = false;
					if (arg < 0)
					{
						arg = -arg;
						neg = true;
					}

					arg *= RecipLn2;
					if (arg >= ExpInf)
						return neg ? 0.0f : std::numeric_limits<float>::infinity();

					int exp = (int)arg;
					arg -= exp;
					exp += ExpBias;
					exp <<= ExpShift;

					float res = (1 + arg) + (arg - 1) * arg * ((Coef1 * arg + Coef2) * arg + Coef3);
					res *= *(float *)&exp;

					if (neg)
						res = 1 / res;
					return res;
				}

#pragma warning(push)
#pragma warning(disable: 4127) // conditional expression is constant

				// Implements a fast approximation of sigmoid/tanh.
				template<bool isTanh>
				void ApplySigmoidCoreA(const float *ps, float * pd, int c)
				{
					float * pdLim = pd + c;

					__m128 cSign = _mm_set1_ps(-0.0f);
					__m128 cZero = _mm_set1_ps(0.0f);
					__m128 cOne = _mm_set1_ps(1.0f);

					__m128 cMax = _mm_set1_ps(ExpInf);
					__m128i cBias = _mm_set1_epi32(ExpBias);
					__m128 c0 = _mm_set1_ps(RecipLn2);
					__m128 c1 = _mm_set1_ps(Coef1);
					__m128 c2 = _mm_set1_ps(Coef2);
					__m128 c3 = _mm_set1_ps(Coef3);

					if (isTanh)
						c0 = _mm_add_ps(c0, c0);

					for (; pd < pdLim; ps += 4, pd += 4)
					{
						// Get the argument, capture its sign and take its absolute value.
						__m128 xArg = _mm_load_ps(ps);
						// maskNaN is set to zero if xArg is not NaN and set equal to xArg otherwise.
						__m128 maskNaN = _mm_and_ps(_mm_cmpneq_ps(xArg, xArg), xArg);
						__m128 xSign = _mm_and_ps(xArg, cSign);
						xArg = _mm_xor_ps(xArg, xSign);

						// Multiply by 1/ln(2) and check for out of bounds.
						xArg = _mm_mul_ps(xArg, c0);
						__m128 xGood = _mm_cmplt_ps(xArg, cMax);
						xArg = _mm_and_ps(xArg, xGood);

						// Get the integer and fractional parts.
						__m128i xInt = _mm_cvttps_epi32(xArg);
						xArg = _mm_sub_ps(xArg, _mm_cvtepi32_ps(xInt));

						// Add the exponent bias to xInt, then convert to a floating point
						// power of two by shifting past the mantissa bits.
						xInt = _mm_add_epi32(xInt, cBias);
						xInt = _mm_slli_epi32(xInt, ExpShift);

						// Approximate 2 raised to the fractional part.
						// (1 + f) + (f - 1) * f * ((c1 * f + c2) * f + c3)

						// x1 = (c1 * f + c2) * f + c3
						__m128 x1 = _mm_mul_ps(c1, xArg);
						x1 = _mm_add_ps(x1, c2);
						x1 = _mm_mul_ps(x1, xArg);
						x1 = _mm_add_ps(x1, c3);

						// x2 = f * (f - 1)
						__m128 x2 = _mm_sub_ps(xArg, cOne);
						x2 = _mm_mul_ps(xArg, x2);

						// Add (1 + f). Note that for tanh, we only add f, so we are approximating
						// 2^f - 1. This is necessary to preserve precision near zero. In particular,
						// near zero, tanh(x) ~ x.
						x1 = _mm_mul_ps(x2, x1);
						if (!isTanh)
							xArg = _mm_add_ps(xArg, cOne);
						x1 = _mm_add_ps(xArg, x1);

						// Multiply by 2^n, where n is the integer part.
						__m128 x3 = _mm_castsi128_ps(xInt);
						x1 = _mm_mul_ps(x1, x3);

						if (!isTanh)
						{
							// Add 1, and take the reciprocal.
							x1 = _mm_add_ps(x1, cOne);
							x1 = _mm_div_ps(cOne, x1);

							// Deal with out of bounds.
							x1 = _mm_and_ps(x1, xGood);
							// If the input was NaN, xGood is zero, so x1 is zero. So can simply or in maskNaN.
							x1 = _mm_or_ps(x1, maskNaN);

							// Deal with the sign. Set:
							// * x2 =     x1 if xSign is -0 (0x80000000)
							// * x2 = 1 - x1 if xSign is +0 (0x00000000).
							x1 = _mm_or_ps(x1, xSign);
							x2 = _mm_or_ps(xSign, cOne);
							x2 = _mm_max_ps(x2, cZero);
							x2 = _mm_sub_ps(x2, x1);
						}
						else
						{
							// [2^n(2^f - 1) + (2^n - 1)] / [2^n(2^f - 1) + (2^n + 1)]
							x2 = _mm_add_ps(x1, _mm_sub_ps(x3, cOne));
							x1 = _mm_add_ps(x1, _mm_add_ps(x3, cOne));
							x2 = _mm_div_ps(x2, x1);

							// Deal with out of bounds: x2 = (x2 & xGood) | ((1 + maskNaN) & ~xGood)
							x2 = _mm_and_ps(x2, xGood);
							x1 = _mm_andnot_ps(xGood, _mm_add_ps(maskNaN, cOne));
							x2 = _mm_or_ps(x2, x1);

							// Deal with the sign.
							x2 = _mm_or_ps(x2, xSign);
						}

						_mm_store_ps(pd, x2);
					}

					// If we overshot, back fill with zero! Since tanh(0) = 0, we only need to do this for sigmoid.
					if (!isTanh)
					{
						while (pd > pdLim)
							*--pd = 0.0f;
					}
				}
#pragma warning(pop)

				void ApplySigmoidA(const float *ps, float * pd, int c)
				{
					ApplySigmoidCoreA<false>(ps, pd, c);
				}

				void ApplySoftMaxU(float * ps, float * pd, int c)
				{
					// REVIEW shonk: Use SSE - do 4 at a time.

					float * psLim = ps + c;

					// Compute max output.
					float maxOut = -std::numeric_limits<float>::infinity();
					for (float * p = ps; p < psLim; p++)
					{
						float v = *p;
						if (maxOut < v)
							maxOut = v;
					}

					// Compute exp and sum.
					float sum = 0;
					for (float * p = ps, *q = pd; p < psLim; p++, q++)
					{
						float v = ExpFast(*p - maxOut);
						*q = v;
						sum += v;
					}

					// Normalize.
					for (float * q = pd; q < pd + c; q++)
						*q /= sum;
				}

				void ApplyRectifiedLinearA(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					for (; ps < psLim; ps += 4, pd += 4)
					{
						__m128 x1 = _mm_load_ps(ps);
						x1 = _mm_max_ps(x1, cZero);
						_mm_store_ps(pd, x1);
					}
				}

				void ApplyRectifiedLinearU(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					for (; ps <= psLim - 4; ps += 4, pd += 4)
					{
						__m128 x1 = _mm_loadu_ps(ps);
						x1 = _mm_max_ps(x1, cZero);
						_mm_storeu_ps(pd, x1);
					}

					while (ps < psLim)
					{
						*pd = *ps > 0 ? *ps : 0;
						pd++;
						ps++;
					}
				}

				void ApplySquareA(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					for (; ps < psLim; ps += 4, pd += 4)
					{
						__m128 x1 = _mm_load_ps(ps);
						x1 = _mm_mul_ps(x1, x1);
						_mm_store_ps(pd, x1);
					}
				}

				void ApplySquareU(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					for (; ps < psLim; ps += 4, pd += 4)
					{
						__m128 x1 = _mm_loadu_ps(ps);
						x1 = _mm_mul_ps(x1, x1);
						_mm_storeu_ps(pd, x1);
					}
				}

				void ApplySqrtA(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					for (; ps < psLim; ps += 4, pd += 4)
					{
						__m128 x1 = _mm_load_ps(ps);
						x1 = _mm_max_ps(x1, cZero);
						x1 = _mm_sqrt_ps(x1);
						_mm_store_ps(pd, x1);
					}
				}

				void ApplySqrtU(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					for (; ps < psLim; ps += 4, pd += 4)
					{
						__m128 x1 = _mm_loadu_ps(ps);
						x1 = _mm_max_ps(x1, cZero);
						x1 = _mm_sqrt_ps(x1);
						_mm_storeu_ps(pd, x1);
					}
				}

				void ApplySoftRectifiedLinearU(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					// Apply: f(x) = log(1 + e^x). To avoid overflow for large x, we use the identity: f(x) = x + f(-x).
					// REVIEW shonk: Should we implement a "LogFast"?
					// REVIEW shonk: Do 4 at a time.
					for (float *p = ps, *q = pd; p < psLim; p++, q++)
					{
						float x = *p;
						if (x > 0)
							*q = x + log(1 + ExpFast(-x));
						else
							*q = log(1 + ExpFast(x));
					}
				}

				void ApplyAbsA(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					__m128 mask = _mm_castsi128_ps(_mm_set1_epi32(0x7FFFFFFF));
					for (; ps < psLim; ps += 4, pd += 4)
					{
						__m128 x1 = _mm_load_ps(ps);
						x1 = _mm_and_ps(x1, mask);
						_mm_store_ps(pd, x1);
					}
				}

				void ApplyTanhA(float * ps, float * pd, int c)
				{
					ApplySigmoidCoreA<true>(ps, pd, c);
				}

				void ApplyBoundedRectifiedLinearA(float * ps, float * pd, int c)
				{
					float * psLim = ps + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					__m128 cOne = _mm_set1_ps(1.0f);
					for (; ps < psLim; ps += 4, pd += 4)
					{
						__m128 x1 = _mm_load_ps(ps);
						x1 = _mm_max_ps(x1, cZero);
						x1 = _mm_min_ps(x1, cOne);
						_mm_store_ps(pd, x1);
					}
				}

				void ApplySigmoidDerivativeA(const float * pv, float * pg, int c)
				{
					float * pgLim = pg + c;

					// pg[i] *= pv[i] * (1 - pv[i])
					__m128 cOne = _mm_set1_ps(1.0f);
					for (; pg < pgLim; pg += 4, pv += 4)
					{
						__m128 x1 = _mm_load_ps(pv);
						__m128 x2 = _mm_load_ps(pg);
						__m128 x3 = _mm_sub_ps(cOne, x1);
						x1 = _mm_mul_ps(x1, x3);
						x2 = _mm_mul_ps(x2, x1);
						_mm_store_ps(pg, x2);
					}
				}

				void ApplySigmoidDerivativeU(const float * pv, float * pg, int c)
				{
					float * pgLim = pg + c;

					// pg[i] *= pv[i] * (1 - pv[i])
					__m128 cOne = _mm_set1_ps(1.0f);
					for (; pg <= pgLim - 4; pg += 4, pv += 4)
					{
						__m128 x1 = _mm_loadu_ps(pv);
						__m128 x2 = _mm_loadu_ps(pg);
						__m128 x3 = _mm_sub_ps(cOne, x1);
						x1 = _mm_mul_ps(x1, x3);
						x2 = _mm_mul_ps(x2, x1);
						_mm_storeu_ps(pg, x2);
					}

					while (pg < pgLim)
					{
						*pg *= *pv*(1 - *pv);
						pg++;
						pv++;
					}
				}

				void ApplyRectifiedLinearDerivativeA(const float * pv, float * pg, int c)
				{
					float * pgLim = pg + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					for (; pg < pgLim; pg += 4, pv += 4)
					{
						__m128 x1 = _mm_load_ps(pv);
						__m128 x2 = _mm_load_ps(pg);
						x1 = _mm_cmpgt_ps(x1, cZero);
						x2 = _mm_and_ps(x2, x1);
						_mm_store_ps(pg, x2);
					}
				}

				void ApplyRectifiedLinearDerivativeU(const float * pv, float * pg, int c)
				{
					float * pgLim = pg + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					for (; pg <= pgLim - 4; pg += 4, pv += 4)
					{
						__m128 x1 = _mm_loadu_ps(pv);
						__m128 x2 = _mm_loadu_ps(pg);
						x1 = _mm_cmpgt_ps(x1, cZero);
						x2 = _mm_and_ps(x2, x1);
						_mm_storeu_ps(pg, x2);
					}

					while (pg < pgLim)
					{
						*pg = *pv > 0 ? *pg : 0;
						pg++;
						pv++;
					}
				}

				void ApplySquareDerivativeA(const float * px, const float * py, float * pg, int c, bool drop)
				{
					float * pgLim = pg + c;

					if (drop)
					{
						__m128 cZero = _mm_set1_ps(0.0f);
						for (; pg < pgLim; pg += 4, px += 4, py += 4)
						{
							__m128 x0 = _mm_cmpgt_ps(_mm_load_ps(py), cZero);
							__m128 x1 = _mm_load_ps(px);
							__m128 x2 = _mm_load_ps(pg);
							x1 = _mm_add_ps(x1, x1);
							x2 = _mm_mul_ps(x2, x1);
							x2 = _mm_and_ps(x2, x0);
							_mm_store_ps(pg, x2);
						}
					}
					else
					{
						for (; pg < pgLim; pg += 4, px += 4)
						{
							__m128 x1 = _mm_load_ps(px);
							__m128 x2 = _mm_load_ps(pg);
							x1 = _mm_add_ps(x1, x1);
							x2 = _mm_mul_ps(x2, x1);
							_mm_store_ps(pg, x2);
						}
					}
				}

				void ApplyAdaGradA(float * pada, float * pgradient, int c)
				{
					float * pLimit = pada + c - 4;
					for (; pada <= pLimit; pada += 4, pgradient += 4)
					{
						__m128 ppada = _mm_load_ps(pada);
						__m128 ppgradient = _mm_load_ps(pgradient);

						__m128 gdSq = _mm_mul_ps(ppgradient, ppgradient);
						ppada = _mm_add_ps(gdSq, ppada);

						ppgradient = _mm_mul_ps(ppgradient, _mm_rsqrt_ps(ppada));

						_mm_store_ps(pada, ppada);
						_mm_store_ps(pgradient, ppgradient);
					}

					while (pada < pLimit + 4)
					{
						*pada += (*pgradient)*(*pgradient);
						*pgradient = *pgradient / sqrt(*pada);
						pada++;
						pgradient++;
					}
				}

				void ApplyAdaGradU(float * pada, float * pgradient, int c)
				{
					float * pLimit = pada + c - 4;
					for (; pada <= pLimit; pada += 4, pgradient += 4)
					{
						__m128 ppada = _mm_loadu_ps(pada);
						__m128 ppgradient = _mm_loadu_ps(pgradient);

						__m128 gdSq = _mm_mul_ps(ppgradient, ppgradient);
						ppada = _mm_add_ps(gdSq, ppada);

						ppgradient = _mm_mul_ps(ppgradient, _mm_rsqrt_ps(ppada));

						_mm_storeu_ps(pada, ppada);
						_mm_storeu_ps(pgradient, ppgradient);
					}

					while (pada < pLimit + 4)
					{
						*pada += (*pgradient)*(*pgradient);
						*pgradient = *pgradient / sqrt(*pada);
						pada++;
						pgradient++;
					}
				}

				void ApplySqrtDerivativeA(const float * pv, float * pg, int c)
				{
					float * pgLim = pg + c;
					static const float smallValue = 1e-10F;

					__m128 cZero = _mm_set1_ps(0.0f);
					__m128 cSmall = _mm_set1_ps(smallValue);
					for (; pg < pgLim; pg += 4, pv += 4)
					{
						__m128 x1 = _mm_load_ps(pv);
						__m128 x2 = _mm_load_ps(pg);
						__m128 x3 = _mm_cmpgt_ps(x1, cZero);
						x1 = _mm_max_ps(x1, cSmall);
						x1 = _mm_add_ps(x1, x1);
						x2 = _mm_and_ps(x2, x3);
						x2 = _mm_div_ps(x2, x1);
						_mm_store_ps(pg, x2);
					}
				}

				void ApplySoftRectifiedLinearDerivativeU(const float * px, const float * py, float * pg, int c)
				{
					UNUSED(px);

					float * pgLim = pg + c;

					// Use the identity: y' = 1 - e^(-y). This has a few nice properties:
					// * If x is large enough that x == y (after rounding), we'll compute y' as 1.
					// * If x is small enough that y == 0 (after rounding), we'll compute y' as 0.
					// * If y is zero because of drop out, we'll compute y' as 0.
					// REVIEW shonk: Do 4 at a time.
					for (; pg < pgLim; pg++, py++)
						*pg *= 1 - ExpFast(-*py);
				}

				void ApplyAbsDerivativeA(const float * px, const float * py, float * pg, int c, bool drop)
				{
					float * pgLim = pg + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					__m128 cSign = _mm_set1_ps(-0.0f);
					if (drop)
					{
						for (; pg < pgLim; pg += 4, px += 4, py += 4)
						{
							__m128 x1 = _mm_and_ps(_mm_load_ps(px), cSign);
							__m128 x2 = _mm_cmpgt_ps(_mm_load_ps(py), cZero);
							__m128 x3 = _mm_load_ps(pg);
							x3 = _mm_xor_ps(x3, x1);
							x3 = _mm_and_ps(x3, x2);
							_mm_store_ps(pg, x3);
						}
					}
					else
					{
						for (; pg < pgLim; pg += 4, px += 4)
						{
							__m128 x0 = _mm_load_ps(px);
							__m128 x1 = _mm_and_ps(x0, cSign);
							__m128 x2 = _mm_cmpneq_ps(x0, cZero);
							__m128 x3 = _mm_load_ps(pg);
							x3 = _mm_xor_ps(x3, x1);
							x3 = _mm_and_ps(x3, x2);
							_mm_store_ps(pg, x3);
						}
					}
				}

				void ApplyTanhDerivativeA(const float * pv, float * pg, int c)
				{
					float * pgLim = pg + c;

					// pg[i] *= 1 - pv[i] * pv[i]
					__m128 cOne = _mm_set1_ps(1.0f);
					for (; pg < pgLim; pg += 4, pv += 4)
					{
						__m128 x1 = _mm_load_ps(pv);
						__m128 x2 = _mm_load_ps(pg);
						x1 = _mm_mul_ps(x1, x1);
						x1 = _mm_sub_ps(cOne, x1);
						x2 = _mm_mul_ps(x2, x1);
						_mm_store_ps(pg, x2);
					}
				}

				void ApplyTanhDerivativeU(const float * pv, float * pg, int c)
				{
					float * pgLim = pg + c;

					// pg[i] *= 1 - pv[i] * pv[i]
					__m128 cOne = _mm_set1_ps(1.0f);
					for (; pg < pgLim; pg += 4, pv += 4)
					{
						__m128 x1 = _mm_loadu_ps(pv);
						__m128 x2 = _mm_loadu_ps(pg);
						x1 = _mm_mul_ps(x1, x1);
						x1 = _mm_sub_ps(cOne, x1);
						x2 = _mm_mul_ps(x2, x1);
						_mm_storeu_ps(pg, x2);
					}
				}

				void ApplyBoundedRectifiedLinearDerivativeA(const float * pv, float * pg, int c)
				{
					float * pgLim = pg + c;

					__m128 cZero = _mm_set1_ps(0.0f);
					__m128 cOne = _mm_set1_ps(1.0f);
					for (; pg < pgLim; pg += 4, pv += 4)
					{
						__m128 x1 = _mm_load_ps(pv);
						__m128 x2 = _mm_load_ps(pg);
						x2 = _mm_and_ps(x2, _mm_cmpgt_ps(x1, cZero));
						x2 = _mm_and_ps(x2, _mm_cmplt_ps(x1, cOne));
						_mm_store_ps(pg, x2);
					}
				}

				void ZeroItemsU(float * pd, int c, const int* pindices, int cindices)
				{
#ifndef _DEBUG
					UNUSED(c);
#endif
					for (int i = 0; i < cindices; ++i)
					{
						int iv = pindices[i];
						//assert(0 <= iv && iv < c);
						pd[iv] = 0;
					}
				}

				void ZeroItemsU(float * pd, int c)
				{
					float * pdLim = pd + c;
					__m128 zero = _mm_setzero_ps();
					for (; pd <= pdLim - 4; pd += 4)
					{
						_mm_storeu_ps(pd, zero);
					}

					for (; pd < pdLim; pd++)
					{
						*pd = 0;
					}
				}

				void ZeroMatrixItemsCore(float * pd, int c, int ccol, int cfltRow, const int* pindices, int cindices)
				{
#ifndef _DEBUG
					UNUSED(c);
#endif
					int ivLogMin = 0;
					int ivLogLim = ccol;
					int ivPhyMin = 0;
					for (int i = 0; i < cindices; ++i)
					{
						int iv = pindices[i];
						//assert(0 <= iv && iv < c);

						int col = iv - ivLogMin;
						if ((unsigned int)col >= (unsigned int)ccol)
						{
							//assert(ivLogMin > iv || iv >= ivLogLim);
							int row = iv / ccol;
							ivLogMin = row * ccol;
							ivLogLim = ivLogMin + ccol;
							ivPhyMin = row * cfltRow;
							//assert(ivLogMin <= iv && iv < ivLogLim);
							col = iv - ivLogMin;
						}
						pd[ivPhyMin + col] = 0;
					}
				}

				void SDCAL1UpdateU(float primalUpdate, const float * ps, float threshold, float *pd1, float * pd2, int c)
				{
					const float * psLim = ps + c;

					__m128 xPrimal = _mm_set1_ps(primalUpdate);

					__m128 signMask = _mm_set1_ps(-0.0f); // 1000 0000 ...
					__m128 xThreshold = _mm_set1_ps(threshold);
					for (; ps + 4 <= psLim; ps += 4, pd1 += 4, pd2 += 4)
					{
						__m128 xs = _mm_loadu_ps(ps);
						__m128 xd1 = _mm_loadu_ps(pd1);
						xd1 = _mm_add_ps(xd1, _mm_mul_ps(xs, xPrimal));
						_mm_storeu_ps(pd1, xd1);

						__m128 xSign = _mm_and_ps(xd1, signMask); // result = 10000... if xd1 is negative or 00000 otherwise
						__m128 xd1Abs = _mm_xor_ps(xd1, xSign);
						__m128 xCond = _mm_cmpgt_ps(xd1Abs, xThreshold); // all 1's if true                        
						__m128 x2 = _mm_xor_ps(xSign, xThreshold); // -threshold if xd1 is negative and +threshold otherwise
						__m128 xd2 = _mm_and_ps(_mm_sub_ps(xd1, x2), xCond);
						_mm_storeu_ps(pd2, xd2);
					}

					for (; ps < psLim; ps++, pd1++, pd2++)
					{
						*pd1 += *ps * primalUpdate;
						float d1 = *pd1;
						*pd2 = abs(d1) > threshold ? (d1 > 0 ? d1 - threshold : d1 + threshold) : 0;
					}
				}

				void SDCAL1UpdateU(float primalUpdate, const float * ps, const int *pi, float threshold, float *pd1, float * pd2, int c)
				{
					const int * piLim = pi + c;

					__m128 xPrimal = _mm_set1_ps(primalUpdate);

					__m128 signMask = _mm_set1_ps(-0.0f); // 1000 0000 ...
					__m128 xThreshold = _mm_set1_ps(threshold);
					for (; pi + 4 <= piLim; pi += 4, ps += 4)
					{
						__m128 xs = _mm_loadu_ps(ps);

						__m128 xd1 = _load4(pd1, pi);
						xd1 = _mm_add_ps(xd1, _mm_mul_ps(xs, xPrimal));

						__m128 xSign = _mm_and_ps(xd1, signMask); // result = 10000... if xd1 is negative or 00000 otherwise
						__m128 xd1Abs = _mm_xor_ps(xd1, xSign);
						__m128 xCond = _mm_cmpgt_ps(xd1Abs, xThreshold); // all 1's if true
						__m128 x2 = _mm_xor_ps(xSign, xThreshold); // -threshold if xd1 is negative and +threshold otherwise
						__m128 xd2 = _mm_and_ps(_mm_sub_ps(xd1, x2), xCond);

						_store4(xd1, pd1, pi);
						_store4(xd2, pd2, pi);
					}

					for (; pi < piLim; pi++, ps++)
					{
						int i = *pi;
						pd1[i] += *ps * primalUpdate;
						float d1 = pd1[i];
						pd2[i] = abs(d1) > threshold ? (d1 > 0 ? d1 - threshold : d1 + threshold) : 0;
					}
				}

				void ScaleAdadeltaU(float* mat, float* accGrads, float* accUpdates, float decay, float cond, float* grads, int size)
				{
					float* pm = mat;
					float* pmLim = pm + size;
					float* pag = accGrads;
					float* pau = accUpdates;
					float* pg = grads;

					__m128 dec = _mm_set1_ps(decay);
					__m128 decc = _mm_set1_ps(1 - decay);
					__m128 c = _mm_set1_ps(cond);

					for (; pm + 4 <= pmLim; pm += 4, pag += 4, pau += 4, pg += 4)
					{
						__m128 g = _mm_loadu_ps(pg);
						__m128 ag = _mm_loadu_ps(pag);
						__m128 au = _mm_loadu_ps(pau);
						__m128 coef = _mm_loadu_ps(pm);

						UpdateAdadelta(coef, ag, au, g, dec, decc, c);

						_mm_storeu_ps(pm, coef);
						_mm_storeu_ps(pag, ag);
						_mm_storeu_ps(pau, au);
					}

					for (; pm < pmLim; pm++, pag++, pau++, pg++)
					{
						float g = *pg;
						float accGrad = decay * *pag + (1 - decay) * g * g;
						float accUpd = *pau;
						float newUpd = sqrt((accUpd + cond) / (accGrad + cond)) * g;
						*pm += newUpd;
						*pag = accGrad;
						*pau = decay * accUpd + (1 - decay) * newUpd * newUpd;
					}
				}

				void ClipU(float* vec, int length, float min, float max)
				{
					float* vecEnd = vec + length;
					__m128 minVec = _mm_set1_ps(min);
					__m128 maxVec = _mm_set1_ps(max);
					for (; vec + 4 <= vecEnd; vec += 4)
					{
						__m128 src = _mm_loadu_ps(vec);
						src = _mm_max_ps(_mm_min_ps(src, maxVec), minVec);
						_mm_storeu_ps(vec, src);
					}

					for (; vec < vecEnd; vec++)
					{
						*vec = *vec <= min ? min : (*vec >= max ? max : *vec);
					}
				}

				void ApplyAdaGradUpdateU(float * pada, float * pgradient, float *pWeight, int c, float updateRate)
				{
					float * pLimit = pada + c - 4;
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 cz = _mm_set1_ps(0);
					for (; pada <= pLimit; pada += 4, pgradient += 4, pWeight += 4)
					{
						__m128 ppada = _mm_loadu_ps(pada);
						__m128 ppgradient = _mm_loadu_ps(pgradient);

						__m128 gdSq = _mm_mul_ps(ppgradient, ppgradient);
						ppada = _mm_add_ps(gdSq, ppada);

						ppgradient = _mm_mul_ps(ur, _mm_mul_ps(ppgradient, _mm_rsqrt_ps(ppada)));

						__m128 wei = _mm_loadu_ps(pWeight);
						wei = _mm_add_ps(wei, ppgradient);
						_mm_storeu_ps(pada, ppada);
						_mm_storeu_ps(pWeight, wei);
						_mm_storeu_ps(pgradient, cz);
					}

					while (pada < pLimit + 4)
					{
						float grad = *pgradient;
						*pada += grad*grad;
						grad = updateRate*grad / sqrt(*pada);
						float wei = *pWeight;
						wei += grad;

						*pWeight = wei;
						*pgradient = 0;
						pWeight++;
						pada++;
						pgradient++;
					}
				}

				void ApplyAdaGradClipGradUpdateU(float * pada, float * pgradient, float *pWeight, int c, float updateRate, float gradClip)
				{
					if (gradClip == 0)
					{
						ApplyAdaGradUpdateU(pada, pgradient, pWeight, c, updateRate);
						return;
					}

					float * pLimit = pada + c - 4;
					__m128 minGrad = _mm_set1_ps(-gradClip);
					__m128 maxGrad = _mm_set1_ps(gradClip);
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 cz = _mm_set1_ps(0);
					for (; pada <= pLimit; pada += 4, pgradient += 4, pWeight += 4)
					{
						__m128 ppada = _mm_loadu_ps(pada);
						__m128 ppgradient = _mm_max_ps(_mm_min_ps(_mm_loadu_ps(pgradient), maxGrad), minGrad);

						__m128 gdSq = _mm_mul_ps(ppgradient, ppgradient);
						ppada = _mm_add_ps(gdSq, ppada);

						ppgradient = _mm_mul_ps(ur, _mm_mul_ps(ppgradient, _mm_rsqrt_ps(ppada)));

						__m128 wei = _mm_loadu_ps(pWeight);
						wei = _mm_add_ps(wei, ppgradient);
						_mm_storeu_ps(pada, ppada);
						_mm_storeu_ps(pWeight, wei);
						_mm_storeu_ps(pgradient, cz);
					}

					while (pada < pLimit + 4)
					{
						float grad = *pgradient;
						grad = grad <= -gradClip ? -gradClip : (grad >= gradClip ? gradClip : grad);
						*pada += grad*grad;
						grad = updateRate*grad / sqrt(*pada);
						float wei = *pWeight;
						wei += grad;

						*pWeight = wei;
						*pgradient = 0;
						pWeight++;
						pada++;
						pgradient++;
					}
				}

				void ApplyAdaGradClipWeightUpdateU(float * pada, float * pgradient, float *pWeight, int c, float updateRate, float weightClip)
				{
					if (weightClip == 0)
					{
						ApplyAdaGradUpdateU(pada, pgradient, pWeight, c, updateRate);
						return;
					}

					float * pLimit = pada + c - 4;
					__m128 minWeight = _mm_set1_ps(-weightClip);
					__m128 maxWeight = _mm_set1_ps(weightClip);
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 cz = _mm_set1_ps(0);
					for (; pada <= pLimit; pada += 4, pgradient += 4, pWeight += 4)
					{
						__m128 ppada = _mm_loadu_ps(pada);
						__m128 ppgradient = _mm_loadu_ps(pgradient);

						__m128 gdSq = _mm_mul_ps(ppgradient, ppgradient);
						ppada = _mm_add_ps(gdSq, ppada);

						ppgradient = _mm_mul_ps(ur, _mm_mul_ps(ppgradient, _mm_rsqrt_ps(ppada)));

						__m128 wei = _mm_loadu_ps(pWeight);
						wei = _mm_add_ps(wei, ppgradient);
						wei = _mm_max_ps(_mm_min_ps(wei, maxWeight), minWeight);
						_mm_storeu_ps(pada, ppada);
						_mm_storeu_ps(pWeight, wei);
						_mm_storeu_ps(pgradient, cz);
					}

					while (pada < pLimit + 4)
					{
						float grad = *pgradient;
						*pada += grad*grad;
						grad = updateRate*grad / sqrt(*pada);
						float wei = *pWeight;
						wei += grad;

						*pWeight = wei <= -weightClip ? -weightClip : (wei >= weightClip ? weightClip : wei);
						*pgradient = 0;
						pWeight++;
						pada++;
						pgradient++;
					}
				}

				void ApplyAdaGradClipUpdateU(float * pada, float * pgradient, float *pWeight, int c, float updateRate, float gradClip, float weightClip)
				{
					if (gradClip == 0)
					{
						ApplyAdaGradClipWeightUpdateU(pada, pgradient, pWeight, c, updateRate, weightClip);
						return;
					}
					else if (weightClip == 0)
					{
						ApplyAdaGradClipGradUpdateU(pada, pgradient, pWeight, c, updateRate, gradClip);
						return;
					}

					float * pLimit = pada + c - 4;
					__m128 minGrad = _mm_set1_ps(-gradClip);
					__m128 maxGrad = _mm_set1_ps(gradClip);
					__m128 minWeight = _mm_set1_ps(-weightClip);
					__m128 maxWeight = _mm_set1_ps(weightClip);
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 cz = _mm_set1_ps(0);
					for (; pada <= pLimit; pada += 4, pgradient += 4, pWeight += 4)
					{
						__m128 ppada = _mm_loadu_ps(pada);
						__m128 ppgradient = _mm_max_ps(_mm_min_ps(_mm_loadu_ps(pgradient), maxGrad), minGrad);

						__m128 gdSq = _mm_mul_ps(ppgradient, ppgradient);
						ppada = _mm_add_ps(gdSq, ppada);

						ppgradient = _mm_mul_ps(ur, _mm_mul_ps(ppgradient, _mm_rsqrt_ps(ppada)));

						__m128 wei = _mm_loadu_ps(pWeight);
						wei = _mm_add_ps(wei, ppgradient);
						wei = _mm_max_ps(_mm_min_ps(wei, maxWeight), minWeight);
						_mm_storeu_ps(pada, ppada);
						_mm_storeu_ps(pWeight, wei);
						_mm_storeu_ps(pgradient, cz);
					}

					while (pada < pLimit + 4)
					{
						float grad = *pgradient;
						grad = grad <= -gradClip ? -gradClip : (grad >= gradClip ? gradClip : grad);
						*pada += grad*grad;
						grad = updateRate*grad / sqrt(*pada);
						float wei = *pWeight;
						wei += grad;

						*pWeight = wei <= -weightClip ? -weightClip : (wei >= weightClip ? weightClip : wei);
						*pgradient = 0;
						pWeight++;
						pada++;
						pgradient++;
					}
				}

				void ApplyAdamUpdateU(float * pM, float *pV, float *pgradient, float *pWeight, int dim, float beta1, float beta2, int iter, float updateRate)
				{
					updateRate = (float)(updateRate*sqrt(1 - pow((double)beta2, iter)) / (1 - pow((double)beta1, iter)));
					float epsilon = 1e-8;
					float * pLimit = pM + dim - 4;
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 vBeta1 = _mm_set1_ps(beta1);
					__m128 vCBeta1 = _mm_set1_ps((1 - beta1));
					__m128 vBeta2 = _mm_set1_ps(beta2);
					__m128 vCBeta2 = _mm_set1_ps(1 - beta2);
					__m128 vEps = _mm_set1_ps(epsilon);
					__m128 cz = _mm_set1_ps(0);
					for (; pM <= pLimit; pM += 4, pgradient += 4, pWeight += 4, pV += 4)
					{
						__m128 m;
						__m128 v;
						__m128 g;
						__m128 wei;

						g = _mm_loadu_ps(pgradient);
						_mm_storeu_ps(pgradient, cz);
						m = _mm_add_ps(_mm_mul_ps(vBeta1, _mm_loadu_ps(pM)), _mm_mul_ps(vCBeta1, g));
						_mm_storeu_ps(pM, m);

						v = _mm_add_ps(_mm_mul_ps(vBeta2, _mm_loadu_ps(pV)), _mm_mul_ps(vCBeta2, _mm_mul_ps(g, g)));
						_mm_storeu_ps(pV, v);

						g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rsqrt_ps(_mm_add_ps(v, vEps))));
						_mm_storeu_ps(pWeight, _mm_add_ps(_mm_loadu_ps(pWeight), g));
					}

					while (pM < pLimit + 4)
					{
						float grad = *pgradient;
						*pV = beta2*(*pV) + (1 - beta2)*grad*grad;
						*pM = beta1*(*pM) + (1 - beta1)*grad;
						grad = updateRate*(*pM) / (sqrt(*pV) + epsilon);

						*pWeight += grad;
						*pgradient = 0;
						pWeight++;
						pM++;
						pV++;
						pgradient++;
					}
				}

				void ApplyAdamUpdateU2(float * pM, float *pV, float *pgradient, float *pWeight, int dim, float beta1, float beta2, int iter, float updateRate)
				{
					updateRate = (float)(updateRate*sqrt(1 - pow((double)beta2, iter)) / (1 - pow((double)beta1, iter)));
					float epsilon = 1e-8;
					float * pLimit = pM + dim - 4;
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 vBeta1 = _mm_set1_ps(beta1);
					__m128 vCBeta1 = _mm_set1_ps(1 - beta1);
					__m128 vBeta2 = _mm_set1_ps(beta2);
					__m128 vCBeta2 = _mm_set1_ps(1 - beta2);
					__m128 vEps = _mm_set1_ps(epsilon);
					__m128 cz = _mm_set1_ps(0);
					for (; pM <= pLimit; pM += 4, pgradient += 4, pWeight += 4, pV += 4)
					{
						__m128 m = _mm_loadu_ps(pM);
						__m128 v = _mm_loadu_ps(pV);
						__m128 g = _mm_loadu_ps(pgradient);
						__m128 w = _mm_loadu_ps(pWeight);

						m = _mm_add_ps(_mm_mul_ps(vBeta1, m), _mm_mul_ps(vCBeta1, g));
						v = _mm_add_ps(_mm_mul_ps(vBeta2, v), _mm_mul_ps(vCBeta2, _mm_mul_ps(g, g)));
						//g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rcp_ps(_mm_add_ps(_mm_sqrt_ps(v), vEps))));
						g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rsqrt_ps(_mm_add_ps(v, vEps))));
						__m128 wei = _mm_loadu_ps(pWeight);
						wei = _mm_add_ps(wei, g);
						_mm_storeu_ps(pM, m);
						_mm_storeu_ps(pV, v);
						_mm_storeu_ps(pWeight, wei);
						_mm_storeu_ps(pgradient, cz);
					}

					while (pM < pLimit + 4)
					{
						float grad = *pgradient;
						*pV = beta2*(*pV) + (1 - beta2)*grad*grad;
						*pM = beta1*(*pM) + (1 - beta1)*grad;
						grad = updateRate*(*pM) / (sqrt(*pV) + epsilon);

						*pWeight += grad;
						*pgradient = 0;
						pWeight++;
						pM++;
						pV++;
						pgradient++;
					}
				}

				void ApplyAdamClipWeightUpdateU(float * pM, float *pV, float *pgradient, float *pWeight, int dim, float beta1, float beta2, int iter, float updateRate, float weightClip)
				{
					if (weightClip == 0)
					{
						ApplyAdamUpdateU(pM, pV, pgradient, pWeight, dim, beta1, beta2, iter, updateRate);
						return;
					}
					updateRate = (float)(updateRate * sqrt(1 - pow(beta2, iter)) / (1 - pow(beta1, iter)));
					__m128 minWeight = _mm_set1_ps(-weightClip);
					__m128 maxWeight = _mm_set1_ps(weightClip);

					float epsilon = 1e-8;
					float *pLimit = pM + dim - 4;
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 vBeta1 = _mm_set1_ps(beta1);
					__m128 vCBeta1 = _mm_set1_ps(1 - beta1);
					__m128 vBeta2 = _mm_set1_ps(beta2);
					__m128 vCBeta2 = _mm_set1_ps(1 - beta2);
					__m128 vEps = _mm_set1_ps(epsilon);
					__m128 cz = _mm_set1_ps(0);
					for (; pM <= pLimit; pM += 4, pgradient += 4, pWeight += 4, pV += 4)
					{
						__m128 m = _mm_loadu_ps(pM);
						__m128 v = _mm_loadu_ps(pV);
						__m128 w = _mm_loadu_ps(pWeight);
						__m128 g = _mm_loadu_ps(pgradient);
						m = _mm_add_ps(_mm_mul_ps(vBeta1, m), _mm_mul_ps(vCBeta1, g));
						v = _mm_add_ps(_mm_mul_ps(vBeta2, v), _mm_mul_ps(vCBeta2, _mm_mul_ps(g, g)));
						//g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rcp_ps(_mm_add_ps(_mm_sqrt_ps(v), vEps))));
						g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rsqrt_ps(_mm_add_ps(v, vEps))));
						__m128 wei = _mm_loadu_ps(pWeight);
						wei = _mm_add_ps(wei, g);
						wei = _mm_max_ps(_mm_min_ps(wei, maxWeight), minWeight);
						_mm_storeu_ps(pM, m);
						_mm_storeu_ps(pV, v);
						_mm_storeu_ps(pWeight, wei);
						_mm_storeu_ps(pgradient, cz);
					}

					while (pM < pLimit + 4)
					{
						float grad = *pgradient;
						*pV = beta2*(*pV) + (1 - beta2)*grad*grad;
						*pM = beta1*(*pM) + (1 - beta1)*grad;
						grad = updateRate*(*pM) / (sqrt(*pV) + epsilon);
						float wei = *pWeight;
						wei += grad;
						*pWeight = wei <= -weightClip ? -weightClip : (wei >= weightClip ? weightClip : wei);
						*pgradient = 0;
						pWeight++;
						pM++;
						pV++;
						pgradient++;
					}
				}

				void ApplyAdamClipGradUpdateU(float * pM, float *pV, float *pgradient, float *pWeight, int dim, float beta1, float beta2, int iter, float updateRate, float gradClip)
				{
					if (gradClip == 0)
					{
						ApplyAdamUpdateU(pM, pV, pgradient, pWeight, dim, beta1, beta2, iter, updateRate);
						return;
					}

					updateRate = (float)(updateRate * sqrt(1 - pow(beta2, iter)) / (1 - pow(beta1, iter)));
					__m128 minGrad = _mm_set1_ps(-gradClip);
					__m128 maxGrad = _mm_set1_ps(gradClip);;

					float epsilon = 1e-8;
					float *pLimit = pM + dim - 4;
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 vBeta1 = _mm_set1_ps(beta1);
					__m128 vCBeta1 = _mm_set1_ps(1 - beta1);
					__m128 vBeta2 = _mm_set1_ps(beta2);
					__m128 vCBeta2 = _mm_set1_ps(1 - beta2);
					__m128 vEps = _mm_set1_ps(epsilon);
					__m128 cz = _mm_set1_ps(0);
					for (; pM <= pLimit; pM += 4, pgradient += 4, pWeight += 4, pV += 4)
					{
						__m128 m = _mm_loadu_ps(pM);
						__m128 v = _mm_loadu_ps(pV);
						__m128 g = _mm_max_ps(_mm_min_ps(_mm_loadu_ps(pgradient), maxGrad), minGrad);
						__m128 w = _mm_loadu_ps(pWeight);

						m = _mm_add_ps(_mm_mul_ps(vBeta1, m), _mm_mul_ps(vCBeta1, g));
						v = _mm_add_ps(_mm_mul_ps(vBeta2, v), _mm_mul_ps(vCBeta2, _mm_mul_ps(g, g)));
						//g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rcp_ps(_mm_add_ps(_mm_sqrt_ps(v), vEps))));
						g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rsqrt_ps(_mm_add_ps(v, vEps))));
						__m128 wei = _mm_loadu_ps(pWeight);
						wei = _mm_add_ps(wei, g);
						_mm_storeu_ps(pM, m);
						_mm_storeu_ps(pV, v);
						_mm_storeu_ps(pWeight, wei);
						_mm_storeu_ps(pgradient, cz);
					}

					while (pM < pLimit + 4)
					{
						float grad = *pgradient;
						grad = grad <= -gradClip ? -gradClip : (grad >= gradClip ? gradClip : grad);
						*pV = beta2*(*pV) + (1 - beta2)*grad*grad;
						*pM = beta1*(*pM) + (1 - beta1)*grad;
						grad = updateRate*(*pM) / (sqrt(*pV) + epsilon);
						float wei = *pWeight;
						wei += grad;
						*pgradient = 0;
						pWeight++;
						pM++;
						pV++;
						pgradient++;
					}
				}

				void ApplyAdamClipUpdateU(float * pM, float *pV, float *pgradient, float *pWeight, int dim, float beta1, float beta2, int iter, float updateRate, float gradClip, float weightClip)
				{
					if (gradClip == 0)
					{
						ApplyAdamClipWeightUpdateU(pM, pV, pgradient, pWeight, dim, beta1, beta2, iter, updateRate, weightClip);
						return;
					}
					else if (weightClip == 0)
					{
						ApplyAdamClipGradUpdateU(pM, pV, pgradient, pWeight, dim, beta1, beta2, iter, updateRate, gradClip);
						return;
					}

					updateRate = (float)(updateRate * sqrt(1 - pow(beta2, iter)) / (1 - pow(beta1, iter)));
					__m128 minGrad = _mm_set1_ps(-gradClip);
					__m128 maxGrad = _mm_set1_ps(gradClip);
					__m128 minWeight = _mm_set1_ps(-weightClip);
					__m128 maxWeight = _mm_set1_ps(weightClip);

					float epsilon = 1e-8;
					float *pLimit = pM + dim - 4;
					__m128 ur = _mm_set1_ps(updateRate);
					__m128 vBeta1 = _mm_set1_ps(beta1);
					__m128 vCBeta1 = _mm_set1_ps(1 - beta1);
					__m128 vBeta2 = _mm_set1_ps(beta2);
					__m128 vCBeta2 = _mm_set1_ps(1 - beta2);
					__m128 vEps = _mm_set1_ps(epsilon);
					__m128 cz = _mm_set1_ps(0);
					for (; pM <= pLimit; pM += 4, pgradient += 4, pWeight += 4, pV += 4)
					{
						__m128 m = _mm_loadu_ps(pM);
						__m128 v = _mm_loadu_ps(pV);
						__m128 g = _mm_max_ps(_mm_min_ps(_mm_loadu_ps(pgradient), maxGrad), minGrad);
						__m128 w = _mm_loadu_ps(pWeight);

						m = _mm_add_ps(_mm_mul_ps(vBeta1, m), _mm_mul_ps(vCBeta1, g));
						v = _mm_add_ps(_mm_mul_ps(vBeta2, v), _mm_mul_ps(vCBeta2, _mm_mul_ps(g, g)));
						//g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rcp_ps(_mm_add_ps(_mm_sqrt_ps(v), vEps))));
						g = _mm_mul_ps(ur, _mm_mul_ps(m, _mm_rsqrt_ps(_mm_add_ps(v, vEps))));
						__m128 wei = _mm_loadu_ps(pWeight);
						wei = _mm_add_ps(wei, g);
						wei = _mm_max_ps(_mm_min_ps(wei, maxWeight), minWeight);
						_mm_storeu_ps(pM, m);
						_mm_storeu_ps(pV, v);
						_mm_storeu_ps(pWeight, wei);
						_mm_storeu_ps(pgradient, cz);
					}

					while (pM < pLimit + 4)
					{
						float grad = *pgradient;
						grad = grad <= -gradClip ? -gradClip : (grad >= gradClip ? gradClip : grad);
						*pV = beta2*(*pV) + (1 - beta2)*grad*grad;
						*pM = beta1*(*pM) + (1 - beta1)*grad;
						grad = updateRate*(*pM) / (sqrt(*pV) + epsilon);
						float wei = *pWeight;
						wei += grad;
						*pWeight = wei <= -weightClip ? -weightClip : (wei >= weightClip ? weightClip : wei);
						*pgradient = 0;
						pWeight++;
						pM++;
						pV++;
						pgradient++;
					}
				}

				void ApplyMomemtumUpdateU(float* weight, float* momentum, int length, float momemtumRate)
				{
					float* pEnd = weight + length;
					__m128 scale = _mm_set1_ps(momemtumRate);
					for (; weight <= pEnd - 4; weight += 4, momentum += 4)
					{
						__m128 m = _mm_loadu_ps(momentum);
						__m128 w = _mm_loadu_ps(weight);
						w = _mm_add_ps(m, w);
						m = _mm_mul_ps(m, scale);
						_mm_storeu_ps(weight, w);
						_mm_storeu_ps(momentum, m);
					}

					while (weight < pEnd)
					{
						*weight += *momentum;
						*momentum *= momemtumRate;
						weight++;
						momentum++;
					}
				}

				void ApplyNAGUpdateU(float* weight, float* momentum, float* trueWeight, int length, float momemtumRate)
				{
					float* pEnd = weight + length;
					__m128 scale = _mm_set1_ps(momemtumRate);
					for (; weight <= pEnd - 4; weight += 4, momentum += 4, trueWeight += 4)
					{
						__m128 m = _mm_loadu_ps(momentum);
						__m128 w = _mm_loadu_ps(trueWeight);
						w = _mm_add_ps(m, w);
						m = _mm_mul_ps(m, scale);
						_mm_storeu_ps(trueWeight, w);
						_mm_storeu_ps(momentum, m);
						w = _mm_add_ps(m, w);
						_mm_storeu_ps(weight, w);
					}

					while (weight < pEnd)
					{
						*trueWeight += *momentum;
						*momentum *= momemtumRate;
						*weight = *trueWeight + *momentum;
						weight++;
						momentum++;
						trueWeight++;
					}
				}


				void ScalarSubVector(float scalar, float *pVec, float *pDst, int cnt)
				{
					__m128 cS = _mm_set1_ps(scalar);
					int i = 0;
					for (; i <= cnt - 4; i += 4)
					{
						__m128 dst = _mm_loadu_ps(pVec);
						dst = _mm_sub_ps(cS, dst);
						_mm_storeu_ps(pDst, dst);
						pVec += 4;
						pDst += 4;
					}

					while (i < cnt)
					{
						*pDst = scalar - *pVec;
						i++;
						pDst++;
						pVec++;
					}
				}

				void ScalarSubVector(float scalar, float *pVec, float *pDst, int cnt, float weight)
				{
					if (weight == 1)
					{
						ScalarSubVector(scalar, pVec, pDst, cnt);
					}
					else
					{
						__m128 cW = _mm_set1_ps(weight);
						__m128 cS = _mm_set1_ps(scalar);
						int i = 0;
						for (; i <= cnt - 4; i += 4)
						{
							__m128 dst = _mm_loadu_ps(pVec);
							dst = _mm_sub_ps(cS, _mm_mul_ps(dst, cW));
							_mm_storeu_ps(pDst, dst);
							pVec += 4;
							pDst += 4;
						}

						while (i < cnt)
						{
							*pDst = scalar - weight* *pVec;
							i++;
							pDst++;
							pVec++;
						}
					}
				}

				void ScalarAddVector(float scalar, float *pVec, float *pDst, int cnt)
				{
					__m128 cS = _mm_set1_ps(scalar);
					int i = 0;
					for (; i <= cnt - 4; i += 4)
					{
						__m128 dst = _mm_loadu_ps(pVec);
						dst = _mm_add_ps(cS, dst);
						_mm_storeu_ps(pDst, dst);
						pVec += 4;
						pDst += 4;
					}

					while (i < cnt)
					{
						*pDst = scalar + *pVec;
						pDst++;
						pVec++;
						i++;
					}
				}

				void ScalarAddVector(float scalar, float *pVec, float *pDst, int cnt, float weight)
				{
					if (weight < 0)
					{
						ScalarSubVector(scalar, pVec, pDst, cnt, -weight);
					}
					else if (weight == 1)
					{
						ScalarAddVector(scalar, pVec, pDst, cnt);
					}
					else
					{
						__m128 cW = _mm_set1_ps(weight);
						__m128 cS = _mm_set1_ps(scalar);
						int i = 0;
						for (; i <= cnt - 4; i += 4)
						{
							__m128 dst = _mm_loadu_ps(pVec);
							dst = _mm_add_ps(cS, _mm_mul_ps(dst, cW));
							_mm_storeu_ps(pDst, dst);
							pVec += 4;
							pDst += 4;
						}

						while (i < cnt)
						{
							*pDst = scalar + weight* *pVec;
							pVec++;
							pDst++;
							i++;
						}
					}
				}

				void ScalarSubVectorMulVector(float scalar, float *pVec, float *pMul, float *pDst, int cnt)
				{
					__m128 cS = _mm_set1_ps(scalar);
					int i = 0;
					for (; i <= cnt - 4; i += 4)
					{
						__m128 dst = _mm_loadu_ps(pVec);
						dst = _mm_mul_ps(_mm_sub_ps(cS, dst), _mm_loadu_ps(pMul));
						_mm_storeu_ps(pDst, dst);
						pVec += 4;
						pDst += 4;
						pMul += 4;
					}

					while (i < cnt)
					{
						*pDst = (scalar - *pVec)* *pMul;
						pVec++;
						pDst++;
						pMul++;
						i++;
					}

				}

				void ScalarSubVectorMulVector(float scalar, float *pVec, float scalarAddWeight, float *pMul, float *pDst, int cnt)
				{
					if (scalarAddWeight == 1)
					{
						ScalarSubVectorMulVector(scalar, pVec, pMul, pDst, cnt);
					}
					else
					{
						__m128 cW = _mm_set1_ps(scalarAddWeight);
						__m128 cS = _mm_set1_ps(scalar);
						int i = 0;
						for (; i <= cnt - 4; i += 4)
						{
							__m128 dst = _mm_loadu_ps(pVec);
							dst = _mm_mul_ps(_mm_sub_ps(cS, _mm_mul_ps(dst, cW)), _mm_loadu_ps(pMul));
							_mm_storeu_ps(pDst, dst);
							pVec += 4;
							pDst += 4;
							pMul += 4;
						}

						while (i < cnt)
						{
							*pDst = (scalar - scalarAddWeight* *pVec)*  *pMul;
							pVec++;
							pDst++;
							pMul++;
							i++;
						}
					}
				}

				void ScalarAddVectorMulVector(float scalar, float *pVec, float *pMul, float *pDst, int cnt)
				{
					__m128 cS = _mm_set1_ps(scalar);
					int i = 0;
					for (; i <= cnt - 4; i += 4)
					{
						__m128 dst = _mm_loadu_ps(pVec);
						dst = _mm_mul_ps(_mm_add_ps(cS, dst), _mm_loadu_ps(pMul));
						_mm_storeu_ps(pDst, dst);
						pVec += 4;
						pDst += 4;
						pMul += 4;
					}

					while (i < cnt)
					{
						*pDst = (scalar + *pVec)* *pMul;
						pVec++;
						pDst++;
						pMul++;
						i++;
					}
				}

				void ScalarAddVectorMulVector(float scalar, float *pVec, float scalarAddWeight, float *pMul, float *pDst, int cnt)
				{
					if (scalarAddWeight < 0)
					{
						ScalarSubVectorMulVector(scalar, pVec, -scalarAddWeight, pMul, pDst, cnt);
					}
					else if (scalarAddWeight == 1)
					{
						ScalarAddVectorMulVector(scalar, pVec, pMul, pDst, cnt);
					}
					else
					{
						__m128 cW = _mm_set1_ps(scalarAddWeight);
						__m128 cS = _mm_set1_ps(scalar);
						int i = 0;
						for (; i <= cnt - 4; i += 4)
						{
							__m128 dst = _mm_loadu_ps(pVec);
							dst = _mm_mul_ps(_mm_add_ps(cS, _mm_mul_ps(dst, cW)), _mm_loadu_ps(pMul));
							_mm_storeu_ps(pDst, dst);
							pVec += 4;
							pDst += 4;
							pMul += 4;
						}

						while (i < cnt)
						{
							*pDst = (scalar + scalarAddWeight* *pVec) * *pMul;
							pVec++;
							pDst++;
							pMul++;
							i++;
						}
					}
				}

				void ScalarSubVectorMulVectorAdd(float scalar, float *pVec, float *pMul, float *pDst, int cnt)
				{
					__m128 cS = _mm_set1_ps(scalar);
					int i = 0;
					for (; i <= cnt - 4; i += 4)
					{
						__m128 dst = _mm_loadu_ps(pVec);
						dst = _mm_mul_ps(_mm_sub_ps(cS, dst), _mm_loadu_ps(pMul));
						_mm_storeu_ps(pDst, _mm_add_ps(_mm_loadu_ps(pDst), dst));
						pVec += 4;
						pDst += 4;
						pMul += 4;
					}

					while (i < cnt)
					{
						*pDst += (scalar - *pVec)* *pMul;
						pVec++;
						pDst++;
						pMul++;
						i++;
					}
				}

				void ScalarSubVectorMulVectorAdd(float scalar, float *pVec, float scalarAddWeight, float *pMul, float *pDst, int cnt)
				{
					if (scalarAddWeight == 1)
					{
						ScalarSubVectorMulVectorAdd(scalar, pVec, pMul, pDst, cnt);
					}
					else
					{
						__m128 cW = _mm_set1_ps(scalarAddWeight);
						__m128 cS = _mm_set1_ps(scalar);
						int i = 0;
						for (; i <= cnt - 4; i += 4)
						{
							__m128 dst = _mm_loadu_ps(pVec);
							dst = _mm_mul_ps(_mm_sub_ps(cS, _mm_mul_ps(dst, cW)), _mm_loadu_ps(pMul));
							_mm_storeu_ps(pDst, _mm_add_ps(_mm_loadu_ps(pDst), dst));
							pVec += 4;
							pDst += 4;
							pMul += 4;
						}

						while (i < cnt)
						{
							*pDst += (scalar - scalarAddWeight* *pVec)*  *pMul;
							pVec++;
							pDst++;
							pMul++;
							i++;
						}
					}
				}

				void ScalarAddVectorMulVectorAdd(float scalar, float *pVec, float *pMul, float *pDst, int cnt)
				{
					__m128 cS = _mm_set1_ps(scalar);
					int i = 0;
					for (; i <= cnt - 4; i += 4)
					{
						__m128 dst = _mm_loadu_ps(pVec);
						dst = _mm_mul_ps(_mm_add_ps(cS, dst), _mm_loadu_ps(pMul));
						_mm_storeu_ps(pDst, _mm_add_ps(_mm_loadu_ps(pDst), dst));
						pVec += 4;
						pDst += 4;
						pMul += 4;
					}

					while (i < cnt)
					{
						*pDst += (scalar + *pVec)* *pMul;
						pVec++;
						pDst++;
						pMul++;
						i++;
					}
				}

				void ScalarAddVectorMulVectorAdd(float scalar, float *pVec, float scalarAddWeight, float *pMul, float *pDst, int cnt)
				{
					if (scalarAddWeight < 0)
					{
						ScalarAddVectorMulVectorAdd(scalar, pVec, -scalarAddWeight, pMul, pDst, cnt);
					}
					else if (scalarAddWeight == 1)
					{
						ScalarAddVectorMulVectorAdd(scalar, pVec, pMul, pDst, cnt);
					}
					else
					{
						__m128 cW = _mm_set1_ps(scalarAddWeight);
						__m128 cS = _mm_set1_ps(scalar);
						int i = 0;
						for (; i <= cnt - 4; i += 4)
						{
							__m128 dst = _mm_loadu_ps(pVec);
							dst = _mm_mul_ps(_mm_add_ps(cS, _mm_mul_ps(dst, cW)), _mm_loadu_ps(pMul));
							_mm_storeu_ps(pDst, _mm_add_ps(_mm_loadu_ps(pDst), dst));
							pVec += 4;
							pDst += 4;
							pMul += 4;
						}

						while (i < cnt)
						{
							*pDst += (scalar + scalarAddWeight* *pVec) * *pMul;
							pVec++;
							pDst++;
							pMul++;
							i++;
						}
					}
				}

// Matrix.cpp
#define MM_Broadcast4A(ptr, mmOut0, mmOut1, mmOut2, mmOut3)			\
	__m128 mmIn = _mm_load_ps(ptr);				\
	mmOut1 = _mm_shuffle_ps(mmIn, mmIn, 0x55);	\
	mmOut2 = _mm_shuffle_ps(mmIn, mmIn, 0xAA);	\
	mmOut3 = _mm_shuffle_ps(mmIn, mmIn, 0xFF);	\
	mmOut0 = _mm_shuffle_ps(mmIn, mmIn, 0x00);

#define MM_Broadcast4U(ptr, mmOut0, mmOut1, mmOut2, mmOut3)			\
	__m128 mmIn = _mm_loadu_ps(ptr);				\
	mmOut1 = _mm_shuffle_ps(mmIn, mmIn, 0x55);	\
	mmOut2 = _mm_shuffle_ps(mmIn, mmIn, 0xAA);	\
	mmOut3 = _mm_shuffle_ps(mmIn, mmIn, 0xFF);	\
	mmOut0 = _mm_shuffle_ps(mmIn, mmIn, 0x00);

#define MM_Load4x4SubMatrixA(ptr, mm0, mm1, mm2, mm3, step)	\
	const float * pmTmp;				\
	mm0 = _mm_load_ps(pmTmp = ptr);		\
	mm1 = _mm_load_ps(pmTmp += step);	\
	mm2 = _mm_load_ps(pmTmp += step);	\
	mm3 = _mm_load_ps(pmTmp += step);

#define MM_Load4x4SubMatrixU(ptr, mm0, mm1, mm2, mm3, step)	\
	const float * pmTmp;				\
	mm0 = _mm_loadu_ps(pmTmp = ptr);	\
	mm1 = _mm_loadu_ps(pmTmp += step);	\
	mm2 = _mm_loadu_ps(pmTmp += step);	\
	mm3 = _mm_loadu_ps(pmTmp += step);

#define MM_Load4x4SubMatrixUScale(ptr, mmScale, mm0, mm1, mm2, mm3, step)	\
	const float * pmTmp;				\
	mm0 = _mm_mul_ps(mmScale, _mm_loadu_ps(pmTmp = ptr));	\
	mm1 = _mm_mul_ps(mmScale, _mm_loadu_ps(pmTmp += step));	\
	mm2 = _mm_mul_ps(mmScale, _mm_loadu_ps(pmTmp += step));	\
	mm3 = _mm_mul_ps(mmScale, _mm_loadu_ps(pmTmp += step));

#define BR_00 0x00
#define BR_01 0x55
#define BR_02 0xAA
#define BR_03 0xFF

#define MM_SelectBroadcast4(mmIn, mmOut, sel)			\
	mmOut = _mm_shuffle_ps(mmIn, mmIn, sel);

#define MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, c1, c2, c3, c4)								\
for (float const* pRowR = pMatR; pRowR <= pRowEnd - 4; pRowR += 4, pRowD += 4)	\
							{						\
							__m128 x02;			\
							__m128 x12;			\
							__m128 x22;			\
							__m128 x32;			\
							MM_Load4x4SubMatrixU(pRowR, x02, x12, x22, x32, rightStride); \
							__m128 xd = _mm_mul_ps(c1, x02);	\
							xd = _mm_add_ps(xd, _mm_mul_ps(c2, x12));	\
							xd = _mm_add_ps(xd, _mm_mul_ps(c3, x22));	\
							xd = _mm_add_ps(xd, _mm_mul_ps(c4, x32));	\
							xd = _mm_add_ps(_mm_loadu_ps(pRowD), xd);	\
							_mm_storeu_ps(pRowD, xd);	\
							}

#define MM_MatRowMulColDst(pMatR, pRowEnd, rightStride, c1, c2, c3, c4, xd)								\
for (float const* pRowR = pMatR; pRowR <= pRowEnd - 4; pRowR += 4)	\
							{						\
							__m128 x02;			\
							__m128 x12;			\
							__m128 x22;			\
							__m128 x32;			\
							MM_Load4x4SubMatrixU(pRowR, x02, x12, x22, x32, rightStride); \
							xd = _mm_mul_ps(c1, x02);	\
							xd = _mm_add_ps(xd, _mm_mul_ps(c2, x12));	\
							xd = _mm_add_ps(xd, _mm_mul_ps(c3, x22));	\
							xd = _mm_add_ps(xd, _mm_mul_ps(c4, x32));	\
							}


// MacroFunctions.h
#define MM128_Recip_NR(src, dst)	\
		__m128 dstFast = _mm_rcp_ps(src);	\
		dst = _mm_sub_ps(_mm_add_ps(dstFast, dstFast), _mm_mul_ps(src, _mm_mul_ps(dstFast, dstFast))); \
		__m128 maskNaNxx = _mm_cmpneq_ps(dst, dst);	\
		maskNaNxx = _mm_xor_ps(_mm_and_ps(dst, maskNaNxx), _mm_and_ps(maskNaNxx, dstFast)); \
		dst = _mm_xor_ps(maskNaNxx, dst);

#define MM256_Recip_NR(src, dst)	\
		dst = _mm256_rcp_ps(src);	\
		dst = _mm256_sub_ps(_mm256_add_ps(dst, dst), _mm256_mul_ps(src, _mm256_mul_ps(dst, dst))); 

#define MM128_Exp(xarg, dst, c0, c1, c2, c3, cBias, cMax, cMin, shift, cOne)	\
				xarg = _mm_mul_ps(xarg, c0);		\
				xarg = _mm_min_ps(xarg, cMax);		\
				xarg = _mm_max_ps(cMin, xarg);		\
				__m128 xexp = _mm_floor_ps(_mm_add_ps(xarg, cBias));	\
				xarg = _mm_sub_ps(xarg, _mm_sub_ps(xexp, cBias));		\
				xexp = _mm_castsi128_ps(_mm_cvtps_epi32(_mm_mul_ps(xexp, shift)));\
				dst = _mm_add_ps(_mm_mul_ps(c1, xarg), c2);	\
				dst = _mm_add_ps(_mm_mul_ps(dst, xarg), c3);	\
				dst = _mm_mul_ps(dst, xarg);					\
				dst = _mm_add_ps(_mm_mul_ps(dst, _mm_sub_ps(xarg, cOne)), _mm_add_ps(xarg, cOne));	\
				dst = _mm_mul_ps(dst, xexp);	

#define MM256_Exp(xarg, dst, c0, c1, c2, c3, cBias, cMax, cMin, shift, cOne)	\
				xarg = _mm256_max_ps(cMin, _mm256_min_ps(_mm256_mul_ps(xarg, c0), cMax)); \
				__m256 xexp = _mm256_floor_ps(_mm256_add_ps(cBias,xarg));	\
				xarg = _mm256_sub_ps(xarg, _mm256_sub_ps(xexp, cBias));	\
				xexp = _mm256_castsi256_ps(_mm256_cvtps_epi32(_mm256_mul_ps(xexp, shift))); \
				dst  = _mm256_add_ps(_mm256_mul_ps(c1, xarg), c2);	\
				dst = _mm256_add_ps(_mm256_mul_ps(dst, xarg), c3);	\
				dst = _mm256_mul_ps(dst, xarg);	\
				dst = _mm256_add_ps(_mm256_mul_ps(dst, _mm256_sub_ps(xarg, cOne)), _mm256_add_ps(xarg, cOne));	\
				dst = _mm256_mul_ps(dst, xexp);

// MatTimesSrc.cpp
#define MM_Broadcast4A(ptr, mmOut0, mmOut1, mmOut2, mmOut3)			\
						__m128 mmIn = _mm_load_ps(ptr);				\
						mmOut1 = _mm_shuffle_ps(mmIn, mmIn, 0x55);	\
						mmOut2 = _mm_shuffle_ps(mmIn, mmIn, 0xAA);	\
						mmOut3 = _mm_shuffle_ps(mmIn, mmIn, 0xFF);	\
						mmOut0 = _mm_shuffle_ps(mmIn, mmIn, 0x00);

#define MM_Broadcast4U(ptr, mmOut0, mmOut1, mmOut2, mmOut3)			\
						__m128 mmIn = _mm_loadu_ps(ptr);				\
						mmOut1 = _mm_shuffle_ps(mmIn, mmIn, 0x55);	\
						mmOut2 = _mm_shuffle_ps(mmIn, mmIn, 0xAA);	\
						mmOut3 = _mm_shuffle_ps(mmIn, mmIn, 0xFF);	\
						mmOut0 = _mm_shuffle_ps(mmIn, mmIn, 0x00);

#define MM_Load4x4SubMatrixA(ptr, mm0, mm1, mm2, mm3, step)	\
						const float * pmTmp;				\
						mm0 = _mm_load_ps(pmTmp = ptr);		\
						mm1 = _mm_load_ps(pmTmp += step);	\
						mm2 = _mm_load_ps(pmTmp += step);	\
						mm3 = _mm_load_ps(pmTmp += step);

#define MM_Load4x4SubMatrixU(ptr, mm0, mm1, mm2, mm3, step)	\
						const float * pmTmp;				\
						mm0 = _mm_loadu_ps(pmTmp = ptr);	\
						mm1 = _mm_loadu_ps(pmTmp += step);	\
						mm2 = _mm_loadu_ps(pmTmp += step);	\
						mm3 = _mm_loadu_ps(pmTmp += step);


//Exp.cpp
void ApplyExpV32fV32f(float* input, float* output, int len)
				{
					__m128 cBias = _mm_set1_ps(127);
					__m128 c0 = _mm_set1_ps(RecipLn2);
					__m128 c1 = _mm_set1_ps(Coef1);
					__m128 c2 = _mm_set1_ps(Coef2);
					__m128 c3 = _mm_set1_ps(Coef3);
					__m128 cOne = _mm_set1_ps(1.0f);
					__m128 cMax = _mm_set1_ps(128.0f);
					__m128 cMin = _mm_set1_ps(-127.0f);
					__m128 shift = _mm_set1_ps(1 << 23);
					float* inputLimt = input + len - 4;
					while (input <= inputLimt)
					{
						__m128 xarg = _mm_loadu_ps(input);
						__m128 dst;

						// maskNaN is set to zero if xArg is not NaN and set equal to xArg otherwise.
						__m128 maskNaN = _mm_and_ps(_mm_cmpneq_ps(xarg, xarg), xarg);
						MM128_Exp(xarg, dst, c0, c1, c2, c3, cBias, cMax, cMin, shift, cOne);

						dst = _mm_or_ps(maskNaN, dst);
						_mm_storeu_ps(output, dst);
						input += 4;
						output += 4;
					}

					while (input < inputLimt + 4)
					{
						*output = BuiltInExp(*input);;
						input++;
						output++;
					}
				}

				void ApplyTanhU(float * input, float * output, int len)
				{
					__m128 cSign = _mm_set1_ps(-0.0f);
					__m128 cBias = _mm_set1_ps(127);
					__m128 c0 = _mm_set1_ps(RecipLn2 * 2);
					__m128 c1 = _mm_set1_ps(Coef1);
					__m128 c2 = _mm_set1_ps(Coef2);
					__m128 c3 = _mm_set1_ps(Coef3);
					__m128 cOne = _mm_set1_ps(1.0f);
					__m128 cTwo = _mm_set1_ps(2.0f);
					__m128 cMax = _mm_set1_ps(128.0f);
					__m128 cMin = _mm_set1_ps(-127.0f);
					__m128 shift = _mm_set1_ps(1 << 23);
					float* inputLimt = input + len - 4;
					while (input <= inputLimt)
					{
						__m128 xarg = _mm_loadu_ps(input);
						__m128 maskNaN = _mm_and_ps(_mm_cmpneq_ps(xarg, xarg), xarg);
						__m128 sign = _mm_and_ps(cSign, xarg);
						xarg = _mm_xor_ps(xarg, sign);

						__m128 dst;
						MM128_Exp(xarg, dst, c0, c1, c2, c3, cBias, cMax, cMin, shift, cOne);
						dst = _mm_or_ps(maskNaN, dst);

						// (1-e^(-2x))/(1+e^(-2x))=>1-2/(1+e^(2x))
						dst = _mm_sub_ps(cOne, _mm_div_ps(cTwo, _mm_add_ps(dst, cOne)));
						dst = _mm_or_ps(sign, dst);
						_mm_storeu_ps(output, dst);
						input += 4;
						output += 4;
					}

					while (input < inputLimt + 4)
					{
						float p = BuiltInExp(*input*(-2.0f));
						// (1-e^(-2x))/(1+e^(-2x))=> 2/(1+e^(-2x))-1
						*output = 2.0f / (1 + p) - 1.0f;
						input++;
						output++;
					}
				}

void ApplyTanhUFast(float * input, float * output, int len)
				{
					__m128 cSign = _mm_set1_ps(-0.0f);
					__m128 cBias = _mm_set1_ps(127);
					__m128 c0 = _mm_set1_ps(-RecipLn2 * 2);
					__m128 c1 = _mm_set1_ps(Coef1);
					__m128 c2 = _mm_set1_ps(Coef2);
					__m128 c3 = _mm_set1_ps(Coef3);
					__m128 cOne = _mm_set1_ps(1.0f);
					__m128 cTwo = _mm_set1_ps(2.0f);
					__m128 cMax = _mm_set1_ps(128.0f);
					__m128 cMin = _mm_set1_ps(-127.0f);
					__m128 shift = _mm_set1_ps(1 << 23);
					float* inputLimt = input + len - 4;
					while (input <= inputLimt)
					{
						__m128 xarg = _mm_loadu_ps(input);
						__m128 maskNaN = _mm_and_ps(_mm_cmpneq_ps(xarg, xarg), xarg);

						__m128 dst;
						MM128_Exp(xarg, dst, c0, c1, c2, c3, cBias, cMax, cMin, shift, cOne);
						dst = _mm_or_ps(maskNaN, dst);

						// (1-e^(-2x))/(1+e^(-2x))=>1-2/(1+e^(2x))
						dst = _mm_sub_ps(_mm_div_ps(cTwo, _mm_add_ps(dst, cOne)), cOne);
						_mm_storeu_ps(output, dst);
						input += 4;
						output += 4;
					}

					while (input < inputLimt + 4)
					{
						float p = BuiltInExp(*input*(-2.0f));
						// (1-e^(-2x))/(1+e^(-2x))=> 2/(1+e^(-2x))-1
						*output = 2.0f / (1 + p) - 1.0f;
						input++;
						output++;
					}
				}


				void ApplySigmoidU(const float * input, float * output, int len)
				{
					__m128 cSign = _mm_set1_ps(-0.0f);
					__m128 cZero = _mm_set1_ps(0.0f);
					__m128 cBias = _mm_set1_ps(127);
					__m128 c0 = _mm_set1_ps(RecipLn2);
					__m128 c1 = _mm_set1_ps(Coef1);
					__m128 c2 = _mm_set1_ps(Coef2);
					__m128 c3 = _mm_set1_ps(Coef3);
					__m128 cOne = _mm_set1_ps(1.0f);
					__m128 cNOne = _mm_set1_ps(-1.0f);
					__m128 cMax = _mm_set1_ps(128.0f);
					__m128 cMin = _mm_set1_ps(-127.0f);
					__m128 shift = _mm_set1_ps(1 << 23);
					const float* inputLimt = input + len - 4;
					while (input <= inputLimt)
					{
						__m128 xarg = _mm_loadu_ps(input);
						__m128 maskNaN = _mm_and_ps(_mm_cmpneq_ps(xarg, xarg), xarg);

						__m128 sign = _mm_and_ps(cSign, xarg);
						xarg = _mm_xor_ps(xarg, sign);

						__m128 dst;
						MM128_Exp(xarg, dst, c0, c1, c2, c3, cBias, cMax, cMin, shift, cOne);
						dst = _mm_or_ps(maskNaN, dst);

						// 1/(1+e^(-x))
						dst = _mm_or_ps(_mm_div_ps(cOne, _mm_add_ps(dst, cOne)), sign);
						xarg = _mm_max_ps(_mm_or_ps(sign, cOne), cZero);

						// Deal with the sign. Set:
						// * x2 =     x1 if xSign is -0 (0x80000000)
						// * x2 = 1 - x1 if xSign is +0 (0x00000000).
						dst = _mm_sub_ps(xarg, dst);

						_mm_storeu_ps(output, dst);
						input += 4;
						output += 4;
					}

					while (input < inputLimt + 4)
					{
						float p = BuiltInExp(-*input);
						// (1/(1+e^(-x));
						*output = 1 / (1 + p);
						input++;
						output++;
					}
				}


void ApplySigmoidUFast(const float * input, float * output, int len)
				{
					__m128 cSign = _mm_set1_ps(-0.0f);
					__m128 cZero = _mm_set1_ps(0.0f);
					__m128 cBias = _mm_set1_ps(127);
					__m128 c0 = _mm_set1_ps(-RecipLn2);
					__m128 c1 = _mm_set1_ps(Coef1);
					__m128 c2 = _mm_set1_ps(Coef2);
					__m128 c3 = _mm_set1_ps(Coef3);
					__m128 cOne = _mm_set1_ps(1.0f);
					__m128 cNOne = _mm_set1_ps(-1.0f);
					__m128 cMax = _mm_set1_ps(128.0f);
					__m128 cMin = _mm_set1_ps(-127.0f);
					__m128 shift = _mm_set1_ps(1 << 23);
					const float* inputLimt = input + len - 4;
					while (input <= inputLimt)
					{
						__m128 xarg = _mm_loadu_ps(input);
						__m128 maskNaN = _mm_and_ps(_mm_cmpneq_ps(xarg, xarg), xarg);

						__m128 dst;
						MM128_Exp(xarg, dst, c0, c1, c2, c3, cBias, cMax, cMin, shift, cOne);
						dst = _mm_or_ps(maskNaN, dst);

						// 1/(1+e^(-x))
						dst = _mm_div_ps(cOne, _mm_add_ps(dst, cOne));

						_mm_storeu_ps(output, dst);
						input += 4;
						output += 4;
					}

					while (input < inputLimt + 4)
					{
						float p = BuiltInExp(-*input);
						// (1/(1+e^(-x));
						*output = 1 / (1 + p);
						input++;
						output++;
					}
				}
void DerivCrossEntropyU(float* pOutputScore, float* pOutputLabel, float* pOutputDeriv, int outputSize, float missingLableIndicateValue)
				{
					__m128 cZero = _mm_set1_ps(0.0f);
					__m128 cOne = _mm_set1_ps(1.0f);
					__m128 cMissingLabel = _mm_set1_ps(missingLableIndicateValue);
					ApplySigmoidUFast(pOutputScore, pOutputDeriv, outputSize);
					float* pScoreEnd = pOutputDeriv + outputSize;
					for (; pOutputDeriv <= pScoreEnd - 4; pOutputDeriv += 4, pOutputLabel += 4)
					{
						__m128 label = _mm_loadu_ps(pOutputLabel);
						__m128 mask = _mm_cmpneq_ps(label, cMissingLabel);
						label = _mm_cmpgt_ps(label, cZero);
						label = _mm_and_ps(label, cOne);

						__m128 sig = _mm_loadu_ps(pOutputDeriv);
						sig = _mm_sub_ps(label, sig);
						sig = _mm_and_ps(sig, mask);
						_mm_storeu_ps(pOutputDeriv, sig);
					}

					for (; pOutputDeriv < pScoreEnd; pOutputDeriv++, pOutputLabel++)
					{
						if (*pOutputLabel == missingLableIndicateValue)
						{
							*pOutputDeriv = 0;
						}
						else if (*pOutputLabel>0)
						{

							*pOutputDeriv = 1 - *pOutputDeriv;
						}
						else
						{
							*pOutputDeriv = -*pOutputDeriv;
						}
					}
				}
//Exp.cpp end.

void MulA(bool add, const float * pmat, const float * psrc, float * pdst, int crow, int ccol)
				{
					const float * psLim = psrc + ccol;
					const float * pdLim = pdst + crow;
					const float * pm = pmat;
					for (float * pd = pdst; pd < pdLim; pd += 4, pm += 3 * ccol)
					{
						__m128 res0 = _mm_setzero_ps();
						__m128 res1 = res0;
						__m128 res2 = res0;
						__m128 res3 = res0;
						for (const float * ps = psrc; ps < psLim; ps += 4, pm += 4)
						{
							const float * pmTmp;
							__m128 x01 = _mm_load_ps(pmTmp = pm);
							__m128 x11 = _mm_load_ps(pmTmp += ccol);
							__m128 x21 = _mm_load_ps(pmTmp += ccol);
							__m128 x31 = _mm_load_ps(pmTmp += ccol);
							__m128 x02 = _mm_load_ps(ps);
							x01 = _mm_mul_ps(x01, x02);
							x11 = _mm_mul_ps(x11, x02);
							x21 = _mm_mul_ps(x21, x02);
							x31 = _mm_mul_ps(x31, x02);
							res0 = _mm_add_ps(res0, x01);
							res1 = _mm_add_ps(res1, x11);
							res2 = _mm_add_ps(res2, x21);
							res3 = _mm_add_ps(res3, x31);
						}

						// Add up the entries of each, with the 4 results in res0
						res0 = _mm_hadd_ps(res0, res1);
						res2 = _mm_hadd_ps(res2, res3);
						res0 = _mm_hadd_ps(res0, res2);

						if (add)
						{
							res0 = _mm_add_ps(res0, _mm_load_ps(pd));
						}

						_mm_stream_ps(pd, res0);
					}
				}

				void MulU(bool add, const float * pmat, const float * psrc, float * pdst, int crow, int ccol)
				{
					const float * psLim = psrc + ccol;
					const float * pdLim = pdst + crow;
					const float * pm = pmat;
					const int row0 = 0;
					const int row1 = ccol;
					const int row2 = row1 + ccol;
					const int row3 = row2 + ccol;
					float * pd;
					for (pd = pdst; pd <= pdLim - 4; pd += 4, pm += row3)
					{
						__m128 res0 = _mm_setzero_ps();
						__m128 res1 = res0;
						__m128 res2 = res0;
						__m128 res3 = res0;
						const float * ps;
						for (ps = psrc; ps <= psLim - 4; ps += 4, pm += 4)
						{
							__m128 x02 = _mm_loadu_ps(ps);
							__m128 x01 = _mm_loadu_ps(&pm[row0]);
							__m128 x11 = _mm_loadu_ps(&pm[row1]);
							__m128 x21 = _mm_loadu_ps(&pm[row2]);
							__m128 x31 = _mm_loadu_ps(&pm[row3]);
							x01 = _mm_mul_ps(x01, x02);
							x11 = _mm_mul_ps(x11, x02);
							x21 = _mm_mul_ps(x21, x02);
							x31 = _mm_mul_ps(x31, x02);
							res0 = _mm_add_ps(res0, x01);
							res1 = _mm_add_ps(res1, x11);
							res2 = _mm_add_ps(res2, x21);
							res3 = _mm_add_ps(res3, x31);
						}

						// Add up the entries of each, with the 4 results in res0
						res0 = _mm_hadd_ps(res0, res1);
						res2 = _mm_hadd_ps(res2, res3);
						res0 = _mm_hadd_ps(res0, res2);

						while (ps < psLim)
						{
							res1 = _mm_load_ps1(ps);
							res2 = _mm_setr_ps(pm[row0], pm[row1], pm[row2], pm[row3]);
							res0 = _mm_add_ps(res0, _mm_mul_ps(res1, res2));
							ps++;
							pm++;
						}

						if (add)
						{
							res0 = _mm_add_ps(res0, _mm_loadu_ps(pd));
						}

						_mm_storeu_ps(pd, res0);
					}

					if (pd < pdLim)
					{
						long len = pdLim - pd;
						__m128 ll[4];
						const float * ps;
						for (int i = 0; i < len; i++)
						{
							ll[i] = _mm_setzero_ps();
							if (!add)
							{
								pd[i] = 0;
							}
						}

						for (ps = psrc; ps <= psLim - 4; ps += 4, pm += 4)
						{
							__m128 res = _mm_loadu_ps(ps);
							const float* pmTmp = pm;
							for (int i = 0; i < len; i++)
							{
								ll[i] = _mm_add_ps(ll[i], _mm_mul_ps(res, _mm_loadu_ps(pmTmp)));
								pmTmp += ccol;
							}
						}

						while (ps < psLim)
						{
							const float* pmTmp = pm;
							for (int i = 0; i < len; i++)
							{
								pd[i] += *ps*(*pmTmp);
								pmTmp += ccol;
							}

							ps++;
							pm++;
						}

						union {
    						__m128 v;    // SSE 4 x float vector
    						float a[4];  // scalar array of 4 floats
						} tmpll;
						for (int i = 0; i < len; i++)
						{
							tmpll.v = ll[i];

							pd[i] += tmpll.a[0] + tmpll.a[1] + tmpll.a[2] + tmpll.a[3];
							//ll[i].m128_f32[0] + ll[i].m128_f32[1] + ll[i].m128_f32[2] + ll[i].m128_f32[3];
						}
					}
				}

				void MulTranA(bool add, const float * pmat, const float * psrc, float * pdst, int dstLength, int srcLength)
				{
					const float * psLim = psrc + srcLength;
					const float * pdLim = pdst + dstLength;
					const float * pm = pmat;
					const float * ps = psrc;
					const int crow0 = 0;
					const int crow1 = crow0 + dstLength;
					const int crow2 = crow1 + dstLength;
					const int crow3 = crow2 + dstLength;
					__m128 x01;
					__m128 x11;
					__m128 x21;
					__m128 x31;

					__m128 x02;
					__m128 x12;
					__m128 x22;
					__m128 x32;

					if (!add)
					{
						// Replicate each slot of x01 into its own register.
						MM_Broadcast4A(ps, x01, x11, x21, x31);
						ps += 4;

						for (float * pd = pdst; pd < pdLim; pd += 4, pm += 4)
						{
							MM_Load4x4SubMatrixA(pm, x02, x12, x22, x32, dstLength);

							x02 = _mm_mul_ps(x01, x02);
							x12 = _mm_mul_ps(x11, x12);
							x22 = _mm_mul_ps(x21, x22);
							x32 = _mm_mul_ps(x31, x32);
							x02 = _mm_add_ps(x02, x12);
							x22 = _mm_add_ps(x22, x32);
							x02 = _mm_add_ps(x02, x22);
							_mm_store_ps(pd, x02);
						}

						pm += crow3;
					}

					for (; ps < psLim; ps += 4)
					{
						// Replicate each slot of x01 into its own register.
						MM_Broadcast4A(ps, x01, x11, x21, x31);

						for (float * pd = pdst; pd < pdLim; pd += 4, pm += 4)
						{
							MM_Load4x4SubMatrixA(pm, x02, x12, x22, x32, dstLength);

							__m128 x3 = _mm_load_ps(pd);
							x02 = _mm_mul_ps(x01, x02);
							x12 = _mm_mul_ps(x11, x12);
							x22 = _mm_mul_ps(x21, x22);
							x32 = _mm_mul_ps(x31, x32);
							x02 = _mm_add_ps(x02, x12);
							x22 = _mm_add_ps(x22, x32);
							x02 = _mm_add_ps(x02, x22);
							x3 = _mm_add_ps(x02, x3);
							_mm_store_ps(pd, x3);
						}

						pm += crow3;
					}
				}

				void MulTranU(bool add, const float * pmat, const float * psrc, float * pdst, int dstLength, int srcLength)
				{
					const float * psLim = psrc + srcLength;
					const float * pdLim = pdst + dstLength;
					const float * pm = pmat;
					const float * ps = psrc;
					const int crow0 = 0;
					const int crow1 = crow0 + dstLength;
					const int crow2 = crow1 + dstLength;
					const int crow3 = crow2 + dstLength;
					__m128 x01;
					__m128 x11;
					__m128 x21;
					__m128 x31;

					__m128 x02;
					__m128 x12;
					__m128 x22;
					__m128 x32;

					if (!add)
					{
						memset(pdst, 0, dstLength*sizeof(float));
						//eroMemory(pdst, dstLength*sizeof(float));
					}

					for (; ps <= psLim - 4; ps += 4)
					{
						// Replicate each slot of x01 into its own register.
						MM_Broadcast4U(ps, x01, x11, x21, x31);
						float * pd = pdst;
						for (; pd <= pdLim - 4; pd += 4, pm += 4)
						{
							MM_Load4x4SubMatrixU(pm, x02, x12, x22, x32, dstLength);

							__m128 x3 = _mm_loadu_ps(pd);
							x02 = _mm_mul_ps(x01, x02);
							x12 = _mm_mul_ps(x11, x12);
							x22 = _mm_mul_ps(x21, x22);
							x32 = _mm_mul_ps(x31, x32);
							x02 = _mm_add_ps(x02, x12);
							x22 = _mm_add_ps(x22, x32);
							x02 = _mm_add_ps(x02, x22);
							x3 = _mm_add_ps(x02, x3);
							_mm_storeu_ps(pd, x3);
						}

						x01 = _mm_loadu_ps(ps);
						while (pd < pdLim)
						{
							x02 = _mm_setr_ps(pm[crow0], pm[crow1], pm[crow2], pm[crow3]);
							x02 = _mm_mul_ps(x01, x02);
							union {
    						__m128 v;    // SSE 4 x float vector
    						float a[4];  // scalar array of 4 floats
							} tmpx02;
							tmpx02.v = x02;
							*pd += tmpx02.a[0] + tmpx02.a[1] + tmpx02.a[2] + tmpx02.a[3];
							//x02.m128_f32[0] + x02.m128_f32[1] + x02.m128_f32[2] + x02.m128_f32[3];
							pd++;
							pm++;
						}

						pm += crow3;
					}

					while (ps < psLim)
					{
						x01 = _mm_load_ps1(ps);
						float * pd = pdst;
						for (; pd <= pdLim - 4; pd += 4, pm += 4)
						{
							__m128 x3 = _mm_loadu_ps(pd);
							x3 = _mm_add_ps(_mm_mul_ps(x01, _mm_loadu_ps(pm)), x3);
							_mm_storeu_ps(pd, x3);
						}
						
						while (pd < pdLim)
						{	
							*pd += *ps * (*pm);
							pd++;
							pm++;
						}

						ps++;
					}
				}

				// Core.cpp

				void ApplyMultiplyV32fV32f(float* pLeft, float* pRight, float* pDst, int len)
				{
					float* pDstEnd = pDst + len - 4;
					while (pDst <= pDstEnd)
					{
						__m128 left = _mm_loadu_ps(pLeft);
						__m128 right = _mm_loadu_ps(pRight);

						__m128 dst = _mm_mul_ps(left, right);
						_mm_storeu_ps(pDst, dst);
						pLeft += 4;
						pRight += 4;
						pDst += 4;
					}

					while (pDst < pDstEnd + 4)
					{
						*pDst = *pLeft * (*pRight);
						pDst++;
						pLeft++;
						pRight++;
					}
				}

				void ApplyMultiplyV32fV32fAdd(float* pLeft, float* pRight, float* pDst, int len)
				{
					float* pDstEnd = pDst + len - 4;
					while (pDst <= pDstEnd)
					{
						__m128 left = _mm_loadu_ps(pLeft);
						__m128 right = _mm_loadu_ps(pRight);

						__m128 dst = _mm_add_ps(_mm_loadu_ps(pDst), _mm_mul_ps(left, right));
						_mm_storeu_ps(pDst, dst);
						pLeft += 4;
						pRight += 4;
						pDst += 4;
					}

					while (pDst < pDstEnd + 4)
					{
						*pDst = *pDst + *pLeft * (*pRight);
						pDst++;
						pLeft++;
						pRight++;
					}
				}

				void ApplyMultiplyV32fV32fAdd(float* pLeft, float* pRight, float* pDst, int len, float weight)
				{
					if (weight == 1.0f)
					{
						ApplyMultiplyV32fV32fAdd(pLeft, pRight, pDst, len);
					}
					else if (weight == 0)
					{
						ApplyMultiplyV32fV32f(pLeft, pRight, pDst, len);
					}
					else
					{
						__m128 cWeight = _mm_set1_ps(weight);
						float* pDstEnd = pDst + len - 4;
						while (pDst <= pDstEnd)
						{
							__m128 left = _mm_loadu_ps(pLeft);
							__m128 right = _mm_loadu_ps(pRight);

							__m128 dst = _mm_add_ps(_mm_mul_ps(_mm_loadu_ps(pDst), cWeight), _mm_mul_ps(left, right));
							_mm_storeu_ps(pDst, dst);
							pLeft += 4;
							pRight += 4;
							pDst += 4;
						}

						while (pDst < pDstEnd + 4)
						{
							*pDst = *pDst*weight + *pLeft * (*pRight);
							pDst++;
							pLeft++;
							pRight++;
						}
					}
				}

				void ApplyDivV32fV32f(float* pLeft, float* pRight, float* pDst, int len)
				{
					float* pDstEnd = pDst + len - 4;
					while (pDst <= pDstEnd)
					{
						__m128 left = _mm_loadu_ps(pLeft);
						__m128 right = _mm_loadu_ps(pRight);

						__m128 dst = _mm_div_ps(left, right);
						_mm_storeu_ps(pDst, dst);
						pLeft += 4;
						pRight += 4;
						pDst += 4;
					}

					while (pDst < pDstEnd + 4)
					{
						*pDst = *pLeft / (*pRight);
						pDst++;
						pLeft++;
						pRight++;
					}
				}

				void ApplyRecipNRV32f(float* pSrc, float* pDst, int count)
				{
					float* pDstEnd = pDst + count - 4;
					while (pDst <= pDstEnd)
					{
						__m128 src = _mm_loadu_ps(pSrc);

						__m128 dst;
						MM128_Recip_NR(src, dst);

						_mm_storeu_ps(pDst, dst);
						pSrc += 4;
						pDst += 4;
					}

					while (pDst < pDstEnd + 4)
					{
						*pDst = 1 / (*pSrc);
						pDst++;
						pSrc++;
					}
				}

				void ApplyRecipFastV32f(float* pSrc, float* pDst, int count)
				{
					float* pDstEnd = pDst + count - 4;
					while (pDst <= pDstEnd)
					{
						__m128 src = _mm_loadu_ps(pSrc);

						__m128 dst = _mm_rcp_ps(src);

						_mm_storeu_ps(pDst, dst);
						pSrc += 4;
						pDst += 4;
					}

					while (pDst < pDstEnd + 4)
					{
						*pDst = 1 / (*pSrc);
						pDst++;
						pSrc++;
					}
				}

				void ApplyRecipV32f(float* pSrc, float* pDst, int count)
				{
					__m128 cOne = _mm_set1_ps(1.0f);
					float* pDstEnd = pDst + count - 4;
					while (pDst <= pDstEnd)
					{
						__m128 src = _mm_loadu_ps(pSrc);

						__m128 dst = _mm_div_ps(cOne, src);

						_mm_storeu_ps(pDst, dst);
						pSrc += 4;
						pDst += 4;
					}

					while (pDst < pDstEnd + 4)
					{
						*pDst = 1 / (*pSrc);
						pDst++;
						pSrc++;
					}
				}



inline void MatMulAdd(float *pLeft, float* pRight, float* pDst, int leftRowCnt, int leftColumnCnt, int rightColumnCnt, int leftStride, int rightStride, int dstStride)
				{
					const float *pMatL = pLeft;
					const float *pMatR = pRight;
					float* pMatD = pDst;
					__m128 v01;
					__m128 v02;
					__m128 v03;
					__m128 v04;
					const int rightStep = 4 * rightStride;

					const float* pRow = pMatL;
					for (int r = 0; r <= leftRowCnt - 4; r += 4)
					{
						pRow = pMatL;
						int c = 0;
						for (; c <= leftColumnCnt - 4; c += 4, pRow += 4)
						{
							MM_Load4x4SubMatrixU(pRow, v01, v02, v03, v04, leftStride);
							const float *pRowEnd = pMatR + rightColumnCnt;
							float *pRowD = pMatD;
							__m128 x01;
							__m128 x11;
							__m128 x21;
							__m128 x31;
							MM_SelectBroadcast4(v01, x01, BR_00);
							MM_SelectBroadcast4(v01, x11, BR_01);
							MM_SelectBroadcast4(v01, x21, BR_02);
							MM_SelectBroadcast4(v01, x31, BR_03);

							MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, x01, x11, x21, x31);

							MM_SelectBroadcast4(v02, x01, BR_00);
							MM_SelectBroadcast4(v02, x11, BR_01);
							MM_SelectBroadcast4(v02, x21, BR_02);
							MM_SelectBroadcast4(v02, x31, BR_03);

							pRowD = pMatD + dstStride;
							MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, x01, x11, x21, x31);

							MM_SelectBroadcast4(v03, x01, BR_00);
							MM_SelectBroadcast4(v03, x11, BR_01);
							MM_SelectBroadcast4(v03, x21, BR_02);
							MM_SelectBroadcast4(v03, x31, BR_03);

							pRowD = pMatD + dstStride + dstStride;
							MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, x01, x11, x21, x31);

							MM_SelectBroadcast4(v04, x01, BR_00);
							MM_SelectBroadcast4(v04, x11, BR_01);
							MM_SelectBroadcast4(v04, x21, BR_02);
							MM_SelectBroadcast4(v04, x31, BR_03);
							pRowD = pMatD + dstStride + dstStride + dstStride;
							MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, x01, x11, x21, x31);

							pMatR += rightStep;
						}

						pMatL += 4 * leftStride;
						pMatD += rightStride * 4;
						pMatR = pRight;
					}
				}

				inline void MatMulAddNative(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride)
				{
					float* pRowL = pLeft;
					float* pRowD = pDst;
					float* pRowR = pRight;
					float* pDstEnd = pDst + dstRowCnt * dstStride;
					for (; pRowD < pDstEnd; pRowL += leftStride, pRowD += dstStride)
					{
						pRowR = pRight;
						float* pLEnd = pRowL + intermediateColumnCnt;
						float* pDEnd = pRowD + dstColumnCnt;
						for (float* pTmp = pRowL; pTmp < pLEnd; pTmp++, pRowR += rightStride)
						{
							float* pTmpDst = pRowD;
							float* pTmpRight = pRowR;
							__m128 lv = _mm_set1_ps(*pTmp);
							for (; pTmpDst <= pDEnd - 4; pTmpRight += 4, pTmpDst += 4)
							{
								__m128 dv = _mm_mul_ps(_mm_loadu_ps(pTmpRight), lv);
								_mm_storeu_ps(pTmpDst, _mm_add_ps(_mm_loadu_ps(pTmpDst), dv));
							}

							while (pTmpDst < pDEnd)
							{
								*pTmpDst += *pTmp * (*pTmpRight);
								pTmpRight++;
								pTmpDst++;
							}
						}
					}
				}

				void MatMulAdd(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2)
				{
					if (dstRowCnt == 1)
					{
						if (w2 == 1)
						{
							if (w1 == 0)
							{
								MulTranU(false, pRight, pLeft, pDst, dstColumnCnt, intermediateColumnCnt);
								return;
							}
							else if (w1 == 1)
							{
								MulTranU(true, pRight, pLeft, pDst, dstColumnCnt, intermediateColumnCnt);
								return;
							}
						}
					}

					__m128 scaleW2 = _mm_set_ps1(w2);
					float* dstEnd = pDst + dstRowCnt*dstColumnCnt;
					if (w1 != 0 && w1 != w2)
					{
						if (w2 != 0)
						{
							w1 = w1 / w2;
						}

						__m128 scaleW1 = _mm_set_ps1(w1);
						float *pd = pDst;
						for (; pd <= dstEnd - 4; pd += 4)
						{
							_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW1));
						}

						while (pd < dstEnd)
						{
							*pd *= w1;
							pd++;
						}
					}
					else if (w1 == 0)
					{
						__m128 zero = _mm_set_ps1(0);
						float *pd = pDst;
						for (; pd <= dstEnd - 4; pd += 4)
						{
							_mm_storeu_ps(pd, zero);
						}

						while (pd < dstEnd)
						{
							*pd = 0;
							pd++;
						}
					}

					if (w2 == 0)
					{
						return;
					}

					/*
						A00	| A01	 B00 | B01    A00xB00+A01xB10 | A00xB01+A01xB11
						--------- x ----------- = ---------------------------------
						A10	| A11	 B10 | B11	  A10xB00+A11xB10 | A10xB01+A11xB11
						*/
					int leftStride = intermediateColumnCnt;
					int rightStride = dstColumnCnt;
					int dstStride = dstColumnCnt;

					int dstRowCnt01 = dstRowCnt & 0x3;
					int dstRowCnt00 = (dstRowCnt&(~0x3));
					int interColCnt01 = intermediateColumnCnt & 0x3;
					int interColCnt00 = (intermediateColumnCnt & (~0x3));
					int dstColCnt01 = dstColumnCnt & 0x3;
					int dstColCnt00 = (dstColumnCnt&(~0x3));

					// A00xB00
					MatMulAdd(pLeft, pRight, pDst, dstRowCnt00, interColCnt00, dstColCnt00, leftStride, rightStride, dstStride);

					// A01xB10
					MatMulAddNative(pLeft + interColCnt00, pRight + interColCnt00*rightStride, pDst,
						dstRowCnt00, interColCnt01, dstColCnt00, leftStride, rightStride, dstStride);

					// A00xB01
					MatMulAddNative(pLeft, pRight + dstColCnt00, pDst + dstColCnt00,
						dstRowCnt00, interColCnt00, dstColCnt01, leftStride, rightStride, dstStride);

					// A01xB11
					MatMulAddNative(pLeft + interColCnt00, pRight + interColCnt00*rightStride + dstColCnt00, pDst + dstColCnt00, dstRowCnt00, interColCnt01, dstColCnt01, leftStride, rightStride, dstStride);

					// A10xB00
					MatMulAddNative(pLeft + dstRowCnt00*leftStride, pRight, pDst + dstRowCnt00*dstStride, dstRowCnt01, interColCnt00, dstColCnt00, leftStride, rightStride, dstStride);

					// A11xB10
					MatMulAddNative(pLeft + dstRowCnt00*leftStride + interColCnt00, pRight + interColCnt00*rightStride, pDst + dstRowCnt00*dstStride, dstRowCnt01, interColCnt01, dstColCnt00, leftStride, rightStride, dstStride);

					// A10xB01
					MatMulAddNative(pLeft + dstRowCnt00*leftStride, pRight + dstColCnt00, pDst + dstRowCnt00*dstStride + dstColCnt00, dstRowCnt01, interColCnt00, dstColCnt01, leftStride, rightStride, dstStride);

					// A11xB11
					MatMulAddNative(pLeft + dstRowCnt00*leftStride + interColCnt00, pRight + interColCnt00*rightStride + dstColCnt00,
						pDst + dstRowCnt00*dstStride + dstColCnt00, dstRowCnt01, interColCnt01, dstColCnt01, leftStride, rightStride, dstStride);
					//End

					if (w2 != 1)
					{
						float *pd = pDst;
						for (; pd <= dstEnd - 4; pd += 4)
						{
							_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW2));
						}

						for (; pd < dstEnd; pd++)
						{
							*pd *= w2;
						}
					}
				}

				inline void MatMulAddTL(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride, float weight)
				{
					const float *pMatL = pLeft;
					const float *pMatR = pRight;
					float* pMatD = pDst;
					__m128 v01;
					__m128 v02;
					__m128 v03;
					__m128 v04;
					const int rightStep = 4 * rightStride;
					__m128 cW = _mm_set1_ps(weight);
					for (int r = 0; r <= dstRowCnt - 4; r += 4)
					{
						const float* pRow = pMatL;
						for (int c = 0; c <= intermediateColumnCnt - 4; c += 4, pRow += 4 * leftStride)
						{
							MM_Load4x4SubMatrixUScale(pRow, cW, v01, v02, v03, v04, leftStride);
							const float *pRowEnd = pMatR + dstColumnCnt;
							float *pRowD = pMatD;
							__m128 x01;
							__m128 x11;
							__m128 x21;
							__m128 x31;
							MM_SelectBroadcast4(v01, x01, BR_00);
							MM_SelectBroadcast4(v02, x11, BR_00);
							MM_SelectBroadcast4(v03, x21, BR_00);
							MM_SelectBroadcast4(v04, x31, BR_00);

							MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, x01, x11, x21, x31);

							MM_SelectBroadcast4(v01, x01, BR_01);
							MM_SelectBroadcast4(v02, x11, BR_01);
							MM_SelectBroadcast4(v03, x21, BR_01);
							MM_SelectBroadcast4(v04, x31, BR_01);

							pRowD = pMatD + dstStride;
							MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, x01, x11, x21, x31);

							MM_SelectBroadcast4(v01, x01, BR_02);
							MM_SelectBroadcast4(v02, x11, BR_02);
							MM_SelectBroadcast4(v03, x21, BR_02);
							MM_SelectBroadcast4(v04, x31, BR_02);

							pRowD = pMatD + dstStride + dstStride;
							MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, x01, x11, x21, x31);

							MM_SelectBroadcast4(v01, x01, BR_03);
							MM_SelectBroadcast4(v02, x11, BR_03);
							MM_SelectBroadcast4(v03, x21, BR_03);
							MM_SelectBroadcast4(v04, x31, BR_03);
							pRowD = pMatD + dstStride + dstStride + dstStride;
							MM_MatRowMulCol(pMatR, pRowEnd, pRowD, rightStride, x01, x11, x21, x31);

							pMatR += rightStep;
						}

						pMatL += 4;
						pMatD += dstStride * 4;
						pMatR = pRight;
					}
				}

				inline void MatMulAddNativeTL(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride, float weight)
				{
					float* pRowL = pLeft;
					float* pRowD = pDst;
					float* pRowR = pRight;
					float* pDstEnd = pDst + dstRowCnt * dstStride;
					float* pMatLEnd = pLeft + intermediateColumnCnt * leftStride;
					for (; pRowL < pMatLEnd; pRowL += leftStride, pRowR += rightStride)
					{
						float* pLEnd = pRowL + dstRowCnt;
						pRowD = pDst;
						for (float* pTmp = pRowL; pTmp < pLEnd; pTmp++)
						{
							float* pTmpDst = pRowD;
							float* pTmpRight = pRowR;
							float* pDEnd = pRowD + dstColumnCnt;
							float wL = weight * *pTmp;
							for (; pTmpDst < pDEnd; pTmpRight++, pTmpDst++)
							{
								*pTmpDst += wL * (*pTmpRight);
							}

							pRowD += dstStride;
						}
					}
				}
				
				void MatMulAddTL(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2, int leftStride, int rightStride, int dstStride)
				{
					__m128 scaleW1 = _mm_set_ps1(w1);
					if (w1 != 0 && w1 != w2)
					{
						w2 = w2 / w1;
					}
					else if (w1 == 0)
					{
						__m128 zero = _mm_set_ps1(0);
						float *rowStart = pDst;
						for (int i = 0; i < dstRowCnt; i++)
						{
							float* off = rowStart;
							for (; off <= rowStart + dstColumnCnt-4; off += 4)
							{
								_mm_storeu_ps(off, zero);
							}

							for (; off < rowStart + dstColumnCnt; off++)
							{
								*off = 0;
							}	

							rowStart += dstStride;
						}						
					}

					if (w2 == 0)
					{
						return;
					}

					// TODO: Compute
					// TODO: do block wise computation
					/*
					A00	| A01|'	 B00 | B01    A00' | A10'	 B00 | B01   A00'xB00+A10'xB10 | A00'xB01+A10'xB11
					---------| x --------- = -----------  x  --------- = ------------------------------------
					A10	| A11|	 B10 | B11	  A01' | A11'	 B10 | B11   A01'xB00+A11'xB10 | A01'xB01+A11'xB11
					*/

					int dstRowCnt01 = dstRowCnt & 0x3;
					int dstRowCnt00 = (dstRowCnt&(~0x3));
					int interColCnt01 = intermediateColumnCnt & 0x3;
					int interColCnt00 = (intermediateColumnCnt & (~0x3));
					int dstColCnt01 = dstColumnCnt & 0x3;
					int dstColCnt00 = (dstColumnCnt&(~0x3));

					// A00'xB00
					MatMulAddTL(pLeft, pRight, pDst, dstRowCnt00, interColCnt00, dstColCnt00, leftStride, rightStride, dstStride, w2);

					// A10'xB10
					MatMulAddNativeTL(pLeft + interColCnt00*leftStride, pRight + interColCnt00*rightStride, pDst,
						dstRowCnt00, interColCnt01, dstColCnt00, leftStride, rightStride, dstStride, w2);

					// A00'xB01
					MatMulAddNativeTL(pLeft, pRight + dstColCnt00, pDst + dstColCnt00,
						dstRowCnt00, interColCnt00, dstColCnt01, leftStride, rightStride, dstStride, w2);

					// A10'xB11
					MatMulAddNativeTL(pLeft + interColCnt00*leftStride, pRight + interColCnt00*rightStride + dstColCnt00, pDst + dstColCnt00,
						dstRowCnt00, interColCnt01, dstColCnt01, leftStride, rightStride, dstStride, w2);

					// A01'xB00
					MatMulAddNativeTL(pLeft + dstRowCnt00, pRight, pDst + dstRowCnt00*dstStride, dstRowCnt01, interColCnt00, dstColCnt00, leftStride, rightStride, dstStride, w2);

					// A11'xB10
					MatMulAddNativeTL(pLeft + interColCnt00*leftStride + dstRowCnt00, pRight + interColCnt00*rightStride, pDst + dstRowCnt00*dstStride, dstRowCnt01, interColCnt01, dstColCnt00, leftStride, rightStride, dstStride, w2);

					// A01'xB01
					MatMulAddNativeTL(pLeft + dstRowCnt00, pRight + dstColCnt00, pDst + dstRowCnt00*dstStride + dstColCnt00, dstRowCnt01, interColCnt00, dstColCnt01, leftStride, rightStride, dstStride, w2);

					// A11'xB11
					MatMulAddNativeTL(pLeft + interColCnt00*leftStride + dstRowCnt00, pRight + interColCnt00*rightStride + dstColCnt00,
						pDst + dstRowCnt00*dstStride + dstColCnt00, dstRowCnt01, interColCnt01, dstColCnt01, leftStride, rightStride, dstStride, w2);

					// End

					if (w1 != 1 && w1 != 0)
					{
						float *rowStart = pDst;
						for (int i = 0; i < dstRowCnt; i++)
						{
							float* off = rowStart;
							for (; off <= rowStart + dstColumnCnt - 4; off += 4)
							{
								_mm_storeu_ps(off, _mm_mul_ps(_mm_loadu_ps(off), scaleW1));
							}

							for (; off < rowStart + dstColumnCnt; off++)
							{
								*off *= w1;
							}

							rowStart += dstStride;
						}
					}
				}

				inline void MatMulAddTR(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride)
				{
					const float *pMatL = pLeft;
					const float *pMatR = pRight;
					const float *pRowR = pRight;

					const int rightStep = 4 * rightStride;
					for (int r = 0; r < dstRowCnt; r++)
					{
						float*pRowDEnd = pDst + dstColumnCnt - 4;
						for (float* pRowD = pDst; pRowD <= pRowDEnd; pRowD += 4)
						{
							__m128 xd = _mm_setzero_ps();
							__m128 x00;
							__m128 x01;
							__m128 x11;
							__m128 x21;
							__m128 x31;

							pRowR = pMatR;
							x01 = _mm_setzero_ps();
							x11 = _mm_setzero_ps();
							x21 = _mm_setzero_ps();
							x31 = _mm_setzero_ps();
							const float *pRowEnd = pMatL + intermediateColumnCnt - 4;
							for (const float* pRow = pMatL; pRow <= pRowEnd; pRow += 4)
							{
								__m128 x02;
								__m128 x12;
								__m128 x22;
								__m128 x32;
								x00 = _mm_loadu_ps(pRow);
								MM_Load4x4SubMatrixU(pRowR, x02, x12, x22, x32, rightStride);
								pRowR += 4;
								x01 = _mm_add_ps(x01, _mm_mul_ps(x00, x02));
								x11 = _mm_add_ps(x11, _mm_mul_ps(x00, x12));
								x21 = _mm_add_ps(x21, _mm_mul_ps(x00, x22));
								x31 = _mm_add_ps(x31, _mm_mul_ps(x00, x32));
							}

							x01 = _mm_hadd_ps(x01, x11);
							x21 = _mm_hadd_ps(x21, x31);
							xd = _mm_hadd_ps(x01, x21);
							xd = _mm_add_ps(_mm_loadu_ps(pRowD), xd);
							_mm_storeu_ps(pRowD, xd);
							pMatR += rightStep;
						}

						pMatL += leftStride;
						pDst += dstStride;
						pMatR = pRight;
					}
				}

				inline void MatMulAddNativeTR(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride)
				{
					float* pRowL = pLeft;
					float* pRowD = pDst;
					float* pRowR = pRight;
					float* pDstEnd = pDst + dstRowCnt * dstStride;
					const int RightStride1 = rightStride;
					const int RightStride2 = rightStride * 2;
					const int RightStride3 = rightStride * 3;
					const int RightOffset = rightStride - intermediateColumnCnt;
					for (; pRowD < pDstEnd; pRowL += leftStride, pRowD += dstStride)
					{
						pRowR = pRight;
						float* pLEnd = pRowL + intermediateColumnCnt;
						float* pDEnd = pRowD + dstColumnCnt;
						float* pTmpDst = pRowD;
						for (; pTmpDst <= pDEnd - 4; pTmpDst += 4, pRowR += (RightStride3+ RightOffset))
						{
							float* pTmp = pRowL;
							__m128 res0 = _mm_setzero_ps();
							__m128 res1 = res0;
							__m128 res2 = res0;
							__m128 res3 = res0;
							for (; pTmp <= pLEnd - 4; pTmp += 4, pRowR += 4)
							{
								__m128 x02 = _mm_loadu_ps(pTmp);
								__m128 x01 = _mm_loadu_ps(&pRowR[0]);
								__m128 x11 = _mm_loadu_ps(&pRowR[RightStride1]);
								__m128 x21 = _mm_loadu_ps(&pRowR[RightStride2]);
								__m128 x31 = _mm_loadu_ps(&pRowR[RightStride3]);
								x01 = _mm_mul_ps(x01, x02);
								x11 = _mm_mul_ps(x11, x02);
								x21 = _mm_mul_ps(x21, x02);
								x31 = _mm_mul_ps(x31, x02);
								res0 = _mm_add_ps(res0, x01);
								res1 = _mm_add_ps(res1, x11);
								res2 = _mm_add_ps(res2, x21);
								res3 = _mm_add_ps(res3, x31);
							}

							// Add up the entries of each, with the 4 results in res0
							res0 = _mm_hadd_ps(res0, res1);
							res2 = _mm_hadd_ps(res2, res3);
							res0 = _mm_hadd_ps(res0, res2);

							while (pTmp < pLEnd)
							{
								res1 = _mm_load_ps1(pTmp);
								res2 = _mm_setr_ps(pRowR[0], pRowR[RightStride1], pRowR[RightStride2], pRowR[RightStride3]);
								res0 = _mm_add_ps(res0, _mm_mul_ps(res1, res2));
								pTmp++;
								pRowR++;
							}

							res0 = _mm_add_ps(res0, _mm_loadu_ps(pTmpDst));
							_mm_storeu_ps(pTmpDst, res0);
						}

						if (pTmpDst < pDEnd)
						{
							long len = pDEnd - pTmpDst;
							__m128 ll[4];
							const float * ps;
							for (int i = 0; i < len; i++)
							{
								ll[i] = _mm_setzero_ps();
							}

							for (ps = pRowL; ps <= pLEnd - 4; ps += 4, pRowR += 4)
							{
								__m128 res = _mm_loadu_ps(ps);
								const float* pmTmp = pRowR;
								for (int i = 0; i < len; i++)
								{
									ll[i] = _mm_add_ps(ll[i], _mm_mul_ps(res, _mm_loadu_ps(pmTmp)));
									pmTmp += RightStride1;
								}
							}

							while (ps < pLEnd)
							{
								const float* pmTmp = pRowR;
								for (int i = 0; i < len; i++)
								{
									pTmpDst[i] += *ps*(*pmTmp);
									pmTmp += RightStride1;
								}

								ps++;
								pRowR++;
							}
							union
							{
								__m128 v;
								float a[4];
							} tmpll;
							for (int i = 0; i < len; i++)
							{
								tmpll.v = ll[i];
								pTmpDst[i] += tmpll.a[0] + tmpll.a[1] + tmpll.a[2] + tmpll.a[3];
								//ll[i].m128_f32[0] + ll[i].m128_f32[1] + ll[i].m128_f32[2] + ll[i].m128_f32[3];
							}
						}
					}
				}

				void MatMulAddTR(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2)
				{
					if (dstRowCnt == 1)
					{
						if (w1 == 1 && w2 == 1)
						{
							MulU(true, pRight, pLeft, pDst, dstColumnCnt, intermediateColumnCnt);
							return;
						}
						else if (w1 == 0 && w2 == 1)
						{
							MulU(false, pRight, pLeft, pDst, dstColumnCnt, intermediateColumnCnt);
							return;
						}
					}

					__m128 scaleW2 = _mm_set_ps1(w2);
					float* dstEnd = pDst + dstRowCnt*dstColumnCnt;
					if (w1 != 0 && w1 != w2)
					{
						if (w2 != 0)
						{
							w1 = w1 / w2;
						}

						__m128 scaleW1 = _mm_set_ps1(w1);
						float *pd = pDst;
						for (; pd <= dstEnd - 4; pd += 4)
						{
							_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW1));
						}

						for (; pd < dstEnd; pd++)
						{
							*pd *= w1;
						}
					}
					else if (w1 == 0)
					{
						__m128 zero = _mm_set_ps1(0);
						float *pd = pDst;
						for (; pd <= dstEnd - 4; pd += 4)
						{
							_mm_storeu_ps(pd, zero);
						}

						for (; pd < dstEnd; pd++)
						{
							*pd = 0;
						}
					}

					if (w2 == 0)
					{
						return;
					}

					// TODO: do block wise computation
					/*
						A00	| A01	 B00 | B01|'  A00	| A01	 B00'| B10'   A00xB00'+A01xB01' | A00xB10'+A01xB11'
						--------- x ----------| = ----------- x ----------  = ------------------------------------
						A10	| A11	 B10 | B11|	  A10	| A11	 B01'| B11'   A10xB00'+A11xB01' | A10xB10'+A11xB11'
						*/
					int leftStride = intermediateColumnCnt;
					int rightStride = intermediateColumnCnt;
					int dstStride = dstColumnCnt;

					int dstRowCnt01 = dstRowCnt & 0x3;
					int dstRowCnt00 = (dstRowCnt&(~0x3));
					int interColCnt01 = intermediateColumnCnt & 0x3;
					int interColCnt00 = (intermediateColumnCnt & (~0x3));
					int dstColCnt01 = dstColumnCnt & 0x3;
					int dstColCnt00 = (dstColumnCnt&(~0x3));

					// A00xB00'
					MatMulAddTR(pLeft, pRight, pDst, dstRowCnt00, interColCnt00, dstColCnt00, leftStride, rightStride, dstStride);

					// A01xB01'
					MatMulAddNativeTR(pLeft + interColCnt00, pRight + interColCnt00, pDst,
						dstRowCnt00, interColCnt01, dstColCnt00, leftStride, rightStride, dstStride);

					// A00xB10'
					MatMulAddNativeTR(pLeft, pRight + dstColCnt00*rightStride, pDst + dstColCnt00,
						dstRowCnt00, interColCnt00, dstColCnt01, leftStride, rightStride, dstStride);

					// A01xB11'
					MatMulAddNativeTR(pLeft + interColCnt00, pRight + dstColCnt00*rightStride + interColCnt00, pDst + dstColCnt00, dstRowCnt00, interColCnt01, dstColCnt01, leftStride, rightStride, dstStride);

					// A10xB00'
					MatMulAddNativeTR(pLeft + dstRowCnt00*leftStride, pRight, pDst + dstRowCnt00*dstStride, dstRowCnt01, interColCnt00, dstColCnt00, leftStride, rightStride, dstStride);

					// A11xB01'
					MatMulAddNativeTR(pLeft + dstRowCnt00*leftStride + interColCnt00, pRight + interColCnt00, pDst + dstRowCnt00*dstStride, dstRowCnt01, interColCnt01, dstColCnt00, leftStride, rightStride, dstStride);

					// A10xB10'
					MatMulAddNativeTR(pLeft + dstRowCnt00*leftStride, pRight + dstColCnt00*rightStride, pDst + dstRowCnt00*dstStride + dstColCnt00, dstRowCnt01, interColCnt00, dstColCnt01, leftStride, rightStride, dstStride);

					// A11xB11'
					MatMulAddNativeTR(pLeft + dstRowCnt00*leftStride + interColCnt00, pRight + dstColCnt00*rightStride + interColCnt00,
						pDst + dstRowCnt00*dstStride + dstColCnt00, dstRowCnt01, interColCnt01, dstColCnt01, leftStride, rightStride, dstStride);
					//End

					if (w2 != 1)
					{
						float *pd = pDst;
						for (; pd <= dstEnd - 4; pd += 4)
						{
							_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW2));
						}

						for (; pd < dstEnd; pd++)
						{
							*pd *= w2;
						}
					}
				}

				inline void MatMulAddTLTR(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride)
				{
					const float *pMatL = pLeft;
					const float *pMatR = pRight;
					float* pMatD = pDst;// Row=leftColumnCnt, Col=rightRowCnt
					__m128 v01;
					__m128 v02;
					__m128 v03;
					__m128 v04;
					const int rightStep = 4 * rightStride;

					for (int r = 0; r <= dstColumnCnt - 4; r += 4)
					{
						const float* pRow = pMatR;
						for (int c = 0; c <= intermediateColumnCnt - 4; c += 4, pRow += 4)
						{
							MM_Load4x4SubMatrixU(pRow, v01, v02, v03, v04, rightStride);
							const float *pRowEnd = pMatL + dstRowCnt;
							float *pRowD = pMatD;
							__m128 x01;
							__m128 x11;
							__m128 x21;
							__m128 x31;
							MM_SelectBroadcast4(v01, x01, BR_00);
							MM_SelectBroadcast4(v01, x11, BR_01);
							MM_SelectBroadcast4(v01, x21, BR_02);
							MM_SelectBroadcast4(v01, x31, BR_03);

							__m128 y01;
							__m128 y11;
							__m128 y21;
							__m128 y31;
							MM_SelectBroadcast4(v02, y01, BR_00);
							MM_SelectBroadcast4(v02, y11, BR_01);
							MM_SelectBroadcast4(v02, y21, BR_02);
							MM_SelectBroadcast4(v02, y31, BR_03);

							__m128 z01;
							__m128 z11;
							__m128 z21;
							__m128 z31;
							MM_SelectBroadcast4(v03, z01, BR_00);
							MM_SelectBroadcast4(v03, z11, BR_01);
							MM_SelectBroadcast4(v03, z21, BR_02);
							MM_SelectBroadcast4(v03, z31, BR_03);

							__m128 w01;
							__m128 w11;
							__m128 w21;
							__m128 w31;
							MM_SelectBroadcast4(v04, w01, BR_00);
							MM_SelectBroadcast4(v04, w11, BR_01);
							MM_SelectBroadcast4(v04, w21, BR_02);
							MM_SelectBroadcast4(v04, w31, BR_03);

							for (float const* pRowL = pMatL; pRowL <= pRowEnd - 4; pRowL += 4)
							{
								__m128 x02;
								__m128 x12;
								__m128 x22;
								__m128 x32;
								MM_Load4x4SubMatrixU(pRowL, x02, x12, x22, x32, leftStride);

								__m128 xd0 = _mm_mul_ps(x01, x02);
								xd0 = _mm_add_ps(xd0, _mm_mul_ps(x11, x12));
								xd0 = _mm_add_ps(xd0, _mm_mul_ps(x21, x22));
								xd0 = _mm_add_ps(xd0, _mm_mul_ps(x31, x32));

								__m128 xd1 = _mm_mul_ps(y01, x02);
								xd1 = _mm_add_ps(xd1, _mm_mul_ps(y11, x12));
								xd1 = _mm_add_ps(xd1, _mm_mul_ps(y21, x22));
								xd1 = _mm_add_ps(xd1, _mm_mul_ps(y31, x32));

								__m128 xd2 = _mm_mul_ps(z01, x02);
								xd2 = _mm_add_ps(xd2, _mm_mul_ps(z11, x12));
								xd2 = _mm_add_ps(xd2, _mm_mul_ps(z21, x22));
								xd2 = _mm_add_ps(xd2, _mm_mul_ps(z31, x32));

								__m128 xd3 = _mm_mul_ps(w01, x02);
								xd3 = _mm_add_ps(xd3, _mm_mul_ps(w11, x12));
								xd3 = _mm_add_ps(xd3, _mm_mul_ps(w21, x22));
								xd3 = _mm_add_ps(xd3, _mm_mul_ps(w31, x32));

								_MM_TRANSPOSE4_PS(xd0, xd1, xd2, xd3);

								float* pTmp = pRowD;

								xd0 = _mm_add_ps(_mm_loadu_ps(pTmp), xd0);
								_mm_storeu_ps(pTmp, xd0);
								pTmp += dstStride;

								xd1 = _mm_add_ps(_mm_loadu_ps(pTmp), xd1);
								_mm_storeu_ps(pTmp, xd1);
								pTmp += dstStride;

								xd2 = _mm_add_ps(_mm_loadu_ps(pTmp), xd2);
								_mm_storeu_ps(pTmp, xd2);
								pTmp += dstStride;

								xd3 = _mm_add_ps(_mm_loadu_ps(pTmp), xd3);
								_mm_storeu_ps(pTmp, xd3);

								pRowD += 4 * dstStride;
							}

							pMatL += leftStride * 4;
						}

						pMatR += 4 * rightStride;
						pMatD += 4;
						pMatL = pLeft;
					}
				}

				inline void MatMulAddNativeTLTR(float* pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int leftStride, int rightStride, int dstStride)
				{
					float* pRowL = pLeft;
					float* pRowD = pDst;
					float* pRowR = pRight;
					float* pREnd = pRight + dstColumnCnt * rightStride;
					for (; pRowR < pREnd; pRowR += rightStride, pRowD++)
					{
						float* pRowREnd = pRowR + intermediateColumnCnt;
						pRowL = pLeft;
						for (float* pTmpRight = pRowR; pTmpRight < pRowREnd; pTmpRight++, pRowL += leftStride)
						{
							float* pRowLEnd = pRowL + dstRowCnt;
							float v = *pTmpRight;
							float* pTmpDst = pRowD;

							for (float* pTmp = pRowL; pTmp < pRowLEnd; pTmp++, pTmpDst += dstStride)
							{
								*pTmpDst += *pTmp * v;
							}
						}
					}
				}

				void MatMulAddTLTR(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2)
				{
					__m128 scaleW2 = _mm_set_ps1(w2);
					float* dstEnd = pDst + dstRowCnt*dstColumnCnt;
					if (w1 != 0 && w1 != w2)
					{
						if (w2 != 0)
						{
							w1 = w1 / w2;
						}

						__m128 scaleW1 = _mm_set_ps1(w1);
						float *pd = pDst;
						for (; pd <= dstEnd - 4; pd += 4)
						{
							_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW1));
						}

						for (; pd < dstEnd; pd++)
						{
							*pd *= w1;
						}
					}
					else if (w1 == 0)
					{
						__m128 zero = _mm_set_ps1(0);

						float *pd = pDst;
						for (; pd < dstEnd; pd += 4)
						{
							_mm_storeu_ps(pd, zero);
						}

						for (; pd < dstEnd; pd++)
						{
							*pd = 0;
						}
					}

					if (w2 == 0)
					{
						return;
					}

					// TODO: do block wise computation
					/*
					A00	| A01|'	  B00 | B01|'  A00'	| A10'	 B00'| B10'    A00'xB00'+A10'xB01' | A00'xB10'+A10'xB11'
					---------| x ----------| = ----------- x ----------  = -----------------------------------------
					A10	| A11|	  B10 | B11|   A01'	| A11'	 B01'| B11'    A01'xB00'+A11'xB01' | A01'xB10'+A11'xB11'
					*/
					int leftStride = dstRowCnt;
					int rightStride = intermediateColumnCnt;
					int dstStride = dstColumnCnt;

					int dstRowCnt01 = dstRowCnt & 0x3;
					int dstRowCnt00 = (dstRowCnt&(~0x3));
					int interColCnt01 = intermediateColumnCnt & 0x3;
					int interColCnt00 = (intermediateColumnCnt & (~0x3));
					int dstColCnt01 = dstColumnCnt & 0x3;
					int dstColCnt00 = (dstColumnCnt&(~0x3));

					// A00'xB00'
					MatMulAddTLTR(pLeft, pRight, pDst, dstRowCnt00, interColCnt00, dstColCnt00, leftStride, rightStride, dstStride);

					// A10'xB01'
					MatMulAddNativeTLTR(pLeft + interColCnt00*leftStride, pRight + interColCnt00, pDst,
						dstRowCnt00, interColCnt01, dstColCnt00, leftStride, rightStride, dstStride);

					// A00'xB10'
					MatMulAddNativeTLTR(pLeft, pRight + dstColCnt00*rightStride, pDst + dstColCnt00,
						dstRowCnt00, interColCnt00, dstColCnt01, leftStride, rightStride, dstStride);

					// A10'xB11'
					MatMulAddNativeTLTR(pLeft + interColCnt00*leftStride, pRight + dstColCnt00*rightStride + interColCnt00, pDst + dstColCnt00, dstRowCnt00, interColCnt01, dstColCnt01, leftStride, rightStride, dstStride);

					// A01'xB00'
					MatMulAddNativeTLTR(pLeft + dstRowCnt00, pRight, pDst + dstRowCnt00*dstStride, dstRowCnt01, interColCnt00, dstColCnt00, leftStride, rightStride, dstStride);

					// A11'xB01'
					MatMulAddNativeTLTR(pLeft + interColCnt00*leftStride + dstRowCnt00, pRight + interColCnt00, pDst + dstRowCnt00*dstStride, dstRowCnt01, interColCnt01, dstColCnt00, leftStride, rightStride, dstStride);

					// A01'xB10'
					MatMulAddNativeTLTR(pLeft + dstRowCnt00, pRight + dstColCnt00*rightStride, pDst + dstRowCnt00*dstStride + dstColCnt00, dstRowCnt01, interColCnt00, dstColCnt01, leftStride, rightStride, dstStride);

					// A11'xB11'
					MatMulAddNativeTLTR(pLeft + interColCnt00*leftStride + dstRowCnt00, pRight + dstColCnt00*rightStride + interColCnt00,
						pDst + dstRowCnt00*dstStride + dstColCnt00, dstRowCnt01, interColCnt01, dstColCnt01, leftStride, rightStride, dstStride);
					//End


					if (w2 != 1)
					{
						float *pd = pDst;
						for (; pd <= dstEnd - 4; pd += 4)
						{
							_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW2));
						}

						for (; pd < dstEnd; pd++)
						{
							*pd *= w2;
						}
					}
				}

				inline void MatMulAddSparseLeftTL(int *pLeftRowIndex, int *pLeftColumnIndex, float *pLeftValues, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int rightStride, int dstStride, float weight)
				{
					const float *pMatR = pRight;
					float* pMatD = pDst;

					int start = 0;
					if (intermediateColumnCnt < 0)
					{
						intermediateColumnCnt = -intermediateColumnCnt;
						start = pLeftRowIndex[-1];
					}

					float* pDstRow = pMatD;
					const float* pRightRow = pMatR;
					for (int i = 0; i < intermediateColumnCnt; i++)
					{
						int end = pLeftRowIndex[i];
						for (int k = start; k < end; k++)
						{
							float leftValue = pLeftValues[k] * weight;
							__m128 leftCol = _mm_set1_ps(leftValue);
							int rowId = pLeftColumnIndex[k];
							pDstRow = pMatD + rowId*dstStride;
							int c = 0;
							for (; c <= dstColumnCnt - 4; c += 4)
							{
								__m128 rightCol = _mm_loadu_ps(pRightRow + c);
								__m128 dstCol = _mm_loadu_ps(pDstRow + c);
								dstCol = _mm_add_ps(dstCol, _mm_mul_ps(rightCol, leftCol));
								_mm_storeu_ps(pDstRow + c, dstCol);
							}

							while (c < dstColumnCnt)
							{
								pDstRow[c] += pRightRow[c] * leftValue;
								c++;
							}
						}

						start = end;
						pRightRow += rightStride;
					}
				}

				inline void MatMulAddNativeSparseLeftTL(int *pLeftRowIndex, int *pLeftColumnIndex, float *pLeftValues, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int rightStride, int dstStride, float weight)
				{
					float* pRowD = pDst;
					float* pRowR = pRight;
					float* pDstEnd = pDst + dstRowCnt * dstStride;
					int start = 0;

					if (dstColumnCnt == 0)
						return;

					if (intermediateColumnCnt < 0)
					{
						intermediateColumnCnt = -intermediateColumnCnt;
						start = pLeftRowIndex[-1];
					}

					const float* pRightRow = pRowR;
					for (int i = 0; i < intermediateColumnCnt; i++)
					{
						int end = pLeftRowIndex[i];
						for (int k = start; k < end; k++)
						{
							float leftValue = pLeftValues[k] * weight;
							int rowId = pLeftColumnIndex[k];
							float* pDstRow = pRowD + rowId*dstStride;
							for (int c = 0; c < dstColumnCnt; c++)
							{
								pDstRow[c] += pRightRow[c] * leftValue;
							}
						}

						pRightRow += rightStride;
						start = end;
					}
				}

				void MatMulAddSparseLeftTL(int *pLeftRowIndex, int *pLeftColumnIndex, float *pLeftValues, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2, int rightStride, int dstStride)
				{
					__m128 scaleW1 = _mm_set_ps1(w1);

					if (w1 != 0 && w1 != w2)
					{
						w2 = w2 / w1;
					}
					else if (w1 == 0)
					{
						__m128 zero = _mm_set_ps1(0);
						float *pDstRow = pDst;
						float* dstEnd = pDst + dstColumnCnt;
						for (int i = 0; i < dstRowCnt; i++)
						{
							float *pd = pDstRow;
							for (; pd <= dstEnd - 4; pd += 4)
							{
								_mm_storeu_ps(pd, zero);
							}

							for (; pd < dstEnd; pd++)
							{
								*pd = 0;
							}

							pDstRow += dstStride;
							dstEnd += dstStride;
						}
					}

					if (w2 == 0)
					{
						return;
					}

					// TODO: Compute
					// TODO: do block wise computation
					/*
					A|'	* B00 | B01  =  A'*B00 | A'*B01
					*/

					int dstColCnt01 = dstColumnCnt & 0x3;
					int dstColCnt00 = (dstColumnCnt&(~0x3));

					// A'xB00
					MatMulAddSparseLeftTL(pLeftRowIndex, pLeftColumnIndex, pLeftValues, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColCnt00, rightStride, dstStride, w2);

					// A' x B01
					MatMulAddNativeSparseLeftTL(pLeftRowIndex, pLeftColumnIndex, pLeftValues, pRight+ dstColCnt00, pDst+ dstColCnt00, dstRowCnt, intermediateColumnCnt, dstColCnt01, rightStride, dstStride, w2);
					// End

					if (w1 != 1 && w1 != 0)
					{
						float *pDstRow = pDst;
						float* dstEnd = pDst + dstColumnCnt;
						for (int i = 0; i < dstRowCnt; i++)
						{
							float *pd = pDstRow;
							for (; pd <= dstEnd - 4; pd += 4)
							{
								_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW1));
							}

							for (; pd < dstEnd; pd++)
							{
								*pd *= w1;
							}

							pDstRow += dstStride;
							dstEnd += dstStride;
						}
					}
				}

				inline void MatMulAddSparseLeft(int leftRowIndexStart, int *pLeftRowIndex, int *pLeftColumnIndex, float *pLeftValues, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int rightStride, int dstStride)
				{
					const float *pMatR = pRight;
					float* pMatD = pDst;

					int start = leftRowIndexStart;
					float* pDstRow = pMatD;
					const float* pRightRow = pMatR;
					for (int i = 0; i < dstRowCnt; i++)
					{
						int end = pLeftRowIndex[i];
						for (int k = start; k < end; k++)
						{
							__m128 leftCol = _mm_set1_ps(pLeftValues[k]);
							int rowId = pLeftColumnIndex[k];
							pRightRow = pMatR + rowId*rightStride;
							for (int c = 0; c <= dstColumnCnt - 4; c += 4)
							{
								__m128 rightCol = _mm_loadu_ps(pRightRow + c);
								__m128 dstCol = _mm_loadu_ps(pDstRow + c);
								dstCol = _mm_add_ps(dstCol, _mm_mul_ps(rightCol, leftCol));
								_mm_storeu_ps(pDstRow + c, dstCol);
							}
						}

						start = end;
						pDstRow += dstStride;
					}
				}

				inline void MatMulAddNativeSparseLeft(int leftRowIndexStart, int *pLeftRowIndex, int *pLeftColumnIndex, float *pLeftValues, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, int rightStride, int dstStride)
				{
					float* pRowD = pDst;
					float* pRowR = pRight;
					float* pDstEnd = pDst + dstRowCnt * dstStride;
					int start = leftRowIndexStart;
					const float* pRightRow = pRowR;
					float* pDstRow = pRowD;
					if (dstColumnCnt == 0)
						return;

					for (int i = 0; i < dstRowCnt; i++)
					{
						int end = pLeftRowIndex[i];
						for (int k = start; k < end; k++)
						{
							float leftValue = pLeftValues[k];
							int rowId = pLeftColumnIndex[k];

							pRightRow = pRowR + rowId*rightStride;
							for (int c = 0; c < dstColumnCnt; c++)
							{
								pDstRow[c] += pRightRow[c] * leftValue;
							}
						}

						pDstRow += dstStride;
						start = end;
					}
				}

				void MatMulAddSparseLeft(int leftRowIndexStart, int *pLeftRowIndex, int *pLeftColumnIndex, float *pLeftValues, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2, int rightStride, int dstStride)
				{
					__m128 scaleW2 = _mm_set_ps1(w2);

					if (w1 != 0 && w1 != w2)
					{
						if (w2 != 0)
						{
							w1 = w1 / w2;
						}

						__m128 scaleW1 = _mm_set_ps1(w1);
						float *pDstRow = pDst;
						float* dstEnd = pDst + dstColumnCnt;
						for (int i = 0; i < dstRowCnt; i++)
						{
							float *pd = pDstRow;
							for (; pd <= dstEnd - 4; pd += 4)
							{
								_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW1));
							}

							for (; pd < dstEnd; pd++)
							{
								*pd *= w1;
							}

							pDstRow += dstStride;
							dstEnd += dstStride;
						}
					}
					else if (w1 == 0)
					{
						__m128 zero = _mm_set_ps1(0);
						float *pDstRow = pDst;
						float* dstEnd = pDst + dstColumnCnt;
						for (int i = 0; i < dstRowCnt; i++)
						{
							float *pd = pDstRow;
							for (; pd <= dstEnd - 4; pd += 4)
							{
								_mm_storeu_ps(pd, zero);
							}

							for (; pd < dstEnd; pd++)
							{
								*pd = 0;
							}

							pDstRow += dstStride;
							dstEnd += dstStride;
						}
					}

					if (w2 == 0)
					{
						return;
					}

					// TODO: Compute
					// TODO: do block wise computation
					/*
					A|'	* B00 | B01  =  A'*B00 | A'*B01
					*/

					int dstColCnt01 = dstColumnCnt & 0x3;
					int dstColCnt00 = (dstColumnCnt&(~0x3));

					// A'xB00
					MatMulAddSparseLeft(leftRowIndexStart, pLeftRowIndex, pLeftColumnIndex, pLeftValues, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColCnt00, rightStride, dstStride);

					// A' x B01
					MatMulAddNativeSparseLeft(leftRowIndexStart, pLeftRowIndex, pLeftColumnIndex, pLeftValues, pRight + dstColCnt00, pDst + dstColCnt00, dstRowCnt, intermediateColumnCnt, dstColCnt01, rightStride, dstStride);
					// End

					if (w2 != 1)
					{
						float *pDstRow = pDst;
						float* dstEnd = pDst + dstColumnCnt;
						for (int i = 0; i < dstRowCnt; i++)
						{
							float *pd = pDstRow;
							for (; pd <= dstEnd - 4; pd += 4)
							{
								_mm_storeu_ps(pd, _mm_mul_ps(_mm_loadu_ps(pd), scaleW2));
							}

							for (; pd < dstEnd; pd++)
							{
								*pd *= w2;
							}

							pDstRow += dstStride;
							dstEnd += dstStride;
						}
					}
				}

				void VectorOutterProductAdd(float *pLeft, float* pRight, float* pDst, int leftLength, int rightLength)
				{
					float* pDstRow = pDst;
					for (int i = 0; i < leftLength; i++)
					{
						__m128 cL = _mm_set1_ps(pLeft[i]);
						int j = 0;
						for (; j <= rightLength - 4; j += 4)
						{
							__m128 mR = _mm_loadu_ps(pRight + j);
							__m128 mD = _mm_loadu_ps(pDstRow + j);
							mD = _mm_add_ps(_mm_mul_ps(mR, cL), mD);
							_mm_storeu_ps(pDstRow + j, mD);
						}

						while (j < rightLength)
						{
							pDstRow[j] = pDstRow[j] + pLeft[i] * pRight[j];
							j++;
						}

						pDstRow += rightLength;
					}
				}

				void VectorOutterProductAdd(float *pLeft, float* pRight, float* pDst, int leftLength, int rightLength, float weight)
				{
					if (weight == 1)
					{
						VectorOutterProductAdd(pLeft, pRight, pDst, leftLength, rightLength);
					}
					else
					{
						__m128 cWeight = _mm_set1_ps(weight);
						float* pDstRow = pDst;
						int i = 0;
						for (; i <= leftLength - 4; i += 4)
						{
							__m128 cLeft = _mm_mul_ps(_mm_loadu_ps(pLeft + i), cWeight);
							__m128 x0;
							__m128 x1;
							__m128 x2;
							__m128 x3;
							MM_SelectBroadcast4(cLeft, x0, BR_00);
							MM_SelectBroadcast4(cLeft, x1, BR_01);
							MM_SelectBroadcast4(cLeft, x2, BR_02);
							MM_SelectBroadcast4(cLeft, x3, BR_03);
							// x0
							int j = 0;
							for (; j <= rightLength - 4; j += 4)
							{
								__m128 mR = _mm_loadu_ps(pRight + j);
								__m128 mD = _mm_loadu_ps(pDstRow + j);
								mD = _mm_add_ps(_mm_mul_ps(mR, x0), mD);
								_mm_storeu_ps(pDstRow + j, mD);
							}

							union{
								__m128 v;
								float a[4];
							} tmpCleft;

							tmpCleft.v = cLeft;

							while (j < rightLength)
							{
								pDstRow[j] = pDstRow[j] + /*cLeft.m128_f32[0]*/ tmpCleft.a[0] * pRight[j];
								j++;
							}

							pDstRow += rightLength;

							// x1
							j = 0;
							for (; j <= rightLength - 4; j += 4)
							{
								__m128 mR = _mm_loadu_ps(pRight + j);
								__m128 mD = _mm_loadu_ps(pDstRow + j);
								mD = _mm_add_ps(_mm_mul_ps(mR, x1), mD);
								_mm_storeu_ps(pDstRow + j, mD);
							}

							while (j < rightLength)
							{
								pDstRow[j] = pDstRow[j] + /*cLeft.m128_f32[1]*/ tmpCleft.a[1] * pRight[j];
								j++;
							}

							pDstRow += rightLength;

							// x2
							j = 0;
							for (; j <= rightLength - 4; j += 4)
							{
								__m128 mR = _mm_loadu_ps(pRight + j);
								__m128 mD = _mm_loadu_ps(pDstRow + j);
								mD = _mm_add_ps(_mm_mul_ps(mR, x2), mD);
								_mm_storeu_ps(pDstRow + j, mD);
							}

							while (j < rightLength)
							{
								pDstRow[j] = pDstRow[j] + /* cLeft.m128_f32[2]*/ tmpCleft.a[2] * pRight[j];
								j++;
							}

							pDstRow += rightLength;

							// x3
							j = 0;
							for (; j <= rightLength - 4; j += 4)
							{
								__m128 mR = _mm_loadu_ps(pRight + j);
								__m128 mD = _mm_loadu_ps(pDstRow + j);
								mD = _mm_add_ps(_mm_mul_ps(mR, x3), mD);
								_mm_storeu_ps(pDstRow + j, mD);
							}

							while (j < rightLength)
							{
								pDstRow[j] = pDstRow[j] + /*cLeft.m128_f32[3]*/ tmpCleft.a[3] * pRight[j];
								j++;
							}

							pDstRow += rightLength;
						}

						while (i < leftLength)
						{
							float wL = pLeft[i] * weight;
							__m128 cLeft = _mm_set1_ps(wL);
							int j = 0;
							for (; j <= rightLength - 4; j += 4)
							{
								__m128 mR = _mm_loadu_ps(pRight + j);
								__m128 mD = _mm_loadu_ps(pDstRow + j);
								mD = _mm_add_ps(_mm_mul_ps(mR, cLeft), mD);
								_mm_storeu_ps(pDstRow + j, mD);
							}

							while (j < rightLength)
							{
								pDstRow[j] = pDstRow[j] + wL * pRight[j];
								j++;
							}

							i++;
							pDstRow += rightLength;
						}
					}
				}
				
				void MatrixAddActivation(float * A, float * B, int ARow, int AColumn_BLength, int F)
				{
					float* pAEnd = A + ARow*AColumn_BLength;
					for (float *pA = A; pA < pAEnd; pA += AColumn_BLength)
					{
						AddU(pA, B, pA, AColumn_BLength);
					}
					__m128 cZero = _mm_set1_ps(0.0f);
					switch (F)
					{
					case 0://Tanh						
						ApplyTanhUFast(A, A, ARow*AColumn_BLength);
						break;
					case 1://Linear
						break;
					case 2:	//Rectified
						for (float *pA = A; pA < pAEnd; pA += 4)
						{
							_mm_storeu_ps(pA, _mm_max_ps(_mm_loadu_ps(pA), cZero));
						}
						break;
					case 3:	//Sigmoid
						ApplySigmoidUFast(A, A, ARow*AColumn_BLength);
						break;
					}
				}

				void MatrixAggragateRowwise(float *A, float * B, int ARow, int AColumn_BLength, float w1, float w2)
				{
					if (w2 == 0)
					{
						return;
					}

					ScaleU(B, B, w1 / w2, AColumn_BLength);
					float* pAEnd = A + ARow*AColumn_BLength;
					for (float *pA = A; pA < pAEnd; pA += AColumn_BLength)
					{
						AddU(pA, B, B, AColumn_BLength);
					}

					ScaleU(B, B, w2, AColumn_BLength);
				}


/**************Cuda Basic Information************************/

extern "C" int AddTest(int a, int b)
{
	return a + b;
}

extern "C" int MultiTest(int a, int b)
{
	return a * b;
}

extern "C" float SSE_SumSq(float * a, int len)
{
	return SumSqU(a, len);
}

extern "C" float SSE_SumAbs(float * a, int len)
{
	return SumAbsU(a, len);
}

extern "C" float SSE_DotProduct(float * a, float * b, int size)
{
	return DotU(a, b, size);
}

extern "C" void SSE_AddScale(float a, float * ps, float * pd, int c)
{
	AddScaleU(a, ps, pd, c);
}

// c= a * pa + b * pb;
extern "C" void SSE_AddScaleEx(float a, float b, float * pa, float * pb, float * pd, int c)
{
	AddScaleU(pa, pb, a, b, pd, c);
}

extern "C" void SSE_Scale(float a, float * psd, int size)
{
	ScaleU(psd, psd, a, size);
}

extern "C" void SSE_MatrixAggragateRowwise(float *A, float * B, int ARow, int AColumn_BLength, float w1, float w2)
{
	MatrixAggragateRowwise(A, B, ARow, AColumn_BLength, w1, w2);
}

extern "C" void ApplyMatMul(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2)
{
	MatMulAdd(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
}

extern "C" void ApplyMatMulTR(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2)
{
	MatMulAddTR(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
}

extern "C" void ApplyMatMulTL(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2)
{
	MatMulAddTL(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2, dstRowCnt, dstColumnCnt, dstColumnCnt);
}

extern "C" void ApplyMatMulTLStride(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2, int leftStride, int rightStride, int dstStride)
{
	MatMulAddTL(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2, leftStride, rightStride, dstStride);
}

extern "C" void ApplyMatMulTLTR(float *pLeft, float* pRight, float* pDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, float w1, float w2)
{
	MatMulAddTLTR(pLeft, pRight, pDst, dstRowCnt, intermediateColumnCnt, dstColumnCnt, w1, w2);
}

extern "C" void ApplyOutterProduct(float *pLeft, float *pRight, float *pDst, int leftLength, int rightLength, float weight)
{
	VectorOutterProductAdd(pLeft, pRight, pDst, leftLength, rightLength, weight);
}
