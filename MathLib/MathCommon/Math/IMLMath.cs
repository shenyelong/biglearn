﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScopeML
{
    public interface IMathVector
    {

    }

    /// <summary>
    /// The interface of machine learning math
    /// </summary>
    public interface IMLMath
    {
        /// <summary>
        /// Multiply two matrix and store the result to the result matrix
        /// transLeft = false; transRight = false --> matDst = dstWeight * matDst + mulWeight * matLeft * matRight;
        /// transLeft = true;  transRight = false --> matDst = dstWeight * matDst + mulWeight * matLeft' * matRight;
        /// transLeft = false; transRight = true  --> matDst = dstWeight * matDst + mulWeight * matLeft * matRight';
        /// transLeft = true;  transRight = true  --> matDst = dstWeight * matDst + mulWeight * matLeft' * matRight';
        /// </summary>
        /// <param name="matLeft">Left matrix. Stored as Row Major Order Matrix.
        ///     IF(!transLeft)
        ///         {dstRowCnt * intermediateColumnCnt} 
        ///     Else 
        ///         {intermediateColumnCnt * dstRowCnt}
        /// </param>
        /// <param name="matRight">Right matrix. Stored as Row Major Order Matrix.
        ///     If(!transRight)
        ///         {intermediateColumnCnt*dstColumnCnt} 
        ///     Else
        ///         {dstColumnCnt*intermediateColumnCnt}
        /// </param>
        /// <param name="matDst">Result matrix. Stored as (dstRowCnt * dstColumnCnt) Row Major Order Matrix.
        /// </param>
        /// <param name="dstRowCnt">The row count of the result matrix</param>
        /// <param name="intermediateColumnCnt">The intermediate column count of the multiply operation</param>
        /// <param name="dstColumnCnt">The column count of the result matrix</param>
        /// <param name="transLeft">If the left matrix is transposed.</param>
        /// <param name="transRight">If the right matrix is transposed.</param>
        /// <param name="dstWeight">The weight of the original result matrix</param>
        /// <param name="mulWeight">The weight of the multiply result</param>        
        void MatrixMultiply(IMathVector matLeft, IMathVector matRight, IMathVector matDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, bool transLeft=false, bool transRight=false, float dstWeight=0, float mulWeight=1);

        /// A = Row Major Order Matrix; (ARow * AColumn_BLength)
        /// B = Vector; (AColumn_BLength)
        /// A_i = F(A_i + B) (A_i is the i_th row of A);
        /// F = 0 : Tanh		--> y = tanh(x);
        /// F = 1 : Linear		--> y = x;
        /// F = 2 : Rectified	--> y = rectified(x);
        /// F = 3 : Sigmoid		--> y = sigmoid(x);
        void MatrixAddActivation(IMathVector A, IMathVector B, int ARow, int AColumn_BLength, int F);

        /// A = vector; (length)
        /// A = scale * A;
        void VectorScale(IMathVector A, int length, float scale);


        ///A = Row Major Order Matrix; (ARow * AColumn_BLength)
        ///B = Vector (AColumn_BLength);
        ///B = w1 * B + w2 * \Sum_i(A_i) (A_i is the i_th row of A);
        void MatrixAggragateRowwise(IMathVector A, IMathVector B, int ARow, int AColumn_BLength, float w1, float w2);

        IMathVector CreateVector(int length); 
    }
}
