﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;

namespace ScopeML
{
    public class SSEMath : IMLMath
    {
        public void MatrixMultiply(IMathVector matLeft, IMathVector matRight, IMathVector matDst, int dstRowCnt, int intermediateColumnCnt, int dstColumnCnt, bool transLeft = false, bool transRight = false, float dstWeight = 0, float mulWeight = 1.0f)
        {
            FastVector AF = matLeft as FastVector;
            FastVector BF = matRight as FastVector;
            FastVector CF = matDst as FastVector;

            FastVector.MatrixMulRowMajor(AF, BF, CF, dstRowCnt, intermediateColumnCnt, dstColumnCnt, transLeft, transRight, dstWeight, mulWeight);
        }

        public void MatrixAddActivation(IMathVector A, IMathVector B, int ARow, int AColumn_BLength, int F)
        {
            FastVector AF = A as FastVector;
            FastVector BF = B as FastVector;

            FastVector.MatrixAddActivation(AF, BF, ARow, AColumn_BLength, F);
        }

        public void VectorScale(IMathVector A, int length, float scale)
        {
            FastVector AF = A as FastVector;
            AF.Scale(scale);
        }

        public void MatrixAggragateRowwise(IMathVector A, IMathVector B, int ARow, int AColumn_BLength, float w1, float w2)
        {
            FastVector AF = A as FastVector;
            FastVector BF = B as FastVector;
            FastVector.MatrixAggragateRowwise(AF, BF, ARow, AColumn_BLength, w1, w2);
        }

        public IMathVector CreateVector(int length)
        {
            return FastVector.Create(length);
        }
    }
}
