
/// A = Row Major Order Matrix; (ARow * AColumn_BRow)
/// B = Row Major Order Matrix; (AColumn_BRow * BColumn)
/// C = Row Major Order Matrix; (ARow * BColumn)
/// AT = 0; BT = 0 --> C = w1 * C + w2 * A * B;
/// AT = 1; BT = 0 --> C = w1 * C + w2 * A^T * B;
/// AT = 0; BT = 1 --> C = w1 * C + w2 * A * B^T;
/// AT = 1; BT = 1 --> C = w1 * C + w2 * A^T * B^T;
void Matrix_Multiply(float *A, float *B, float *C, int ARow, int AColumn_BRow, int BColumn, int AT, int BT, float w1, float w2)
{
}

/// A = Row Major Order Matrix; (ARow * AColumn_BLength)
/// B = Vector; (AColumn_BLength)
/// A_i = F(A_i + B) (A_i is the i_th row of A);
/// F = 0 : Tanh		--> y = tanh(x);
/// F = 1 : Linear		--> y = x;
/// F = 2 : Rectified	--> y = rectified(x);
/// F = 3 : Sigmoid		--> y = sigmoid(x);
void Matrix_Add_Activation(float * A, float * B, int ARow, int AColumn_BLength, int F)
{
}

/// A = vector; (length)
/// A = scale * A;
void Vector_Scale(float * A, int length, float scale)
{
}


///A = Row Major Order Matrix; (ARow * AColumn_BLength)
///B = Vector (AColumn_BLength);
///B = w1 * B + w2 * \Sum_i(A_i) (A_i is the i_th row of A);
void Matrix_Aggragate_Rowwise(float *A, float * B, int ARow, int AColumn_BLength, float w1, float w2)
{
}