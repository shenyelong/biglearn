﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScopeML
{
    public class MathFactory
    {
        public enum DeviceType { 
            CPU = 0,
            GPU = 1
        }

        public static IMLMath Create(DeviceType devicetype)
        {
            switch (devicetype)
            {
                case DeviceType.CPU:
                    {
                        return new SSEMath();
                    }
                case DeviceType.GPU:
                    {
                        throw new NotSupportedException("GPU type is not supported yet!");
                    }
                default:
                    throw new ArgumentException(string.Format("Invalid device type: {0}", devicetype));
            }
        }
    }
}
