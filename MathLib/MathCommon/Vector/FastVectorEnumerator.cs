﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ScopeML
{
    public unsafe class FastVectorEnumerator : IEnumerator<float>
    {
        private int currentPos;
        private int length;
        private float* dataPtr;

        public FastVectorEnumerator(float* ptr, int length)
        {
            currentPos = -1;
            this.length = length;
            this.dataPtr = ptr;
        }

        object IEnumerator.Current
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]
            get
            {
                return dataPtr[currentPos];
            }
        }

        public float Current
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]
            get
            {
                return dataPtr[currentPos];
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]

        public bool MoveNext()
        {
            if (++currentPos < this.length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]

        public void Reset()
        {
            this.currentPos = -1;
        }

        public void Dispose()
        {
            this.dataPtr = null;
            this.currentPos = -1;
        }
    }
}
