﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime;

namespace ScopeML
{
    public unsafe partial class FastVector : IDisposable
    {
        private static bool? isAvx = null;

        public static bool IsAvx
        {
            get
            {
                if (!isAvx.HasValue)
                {
                    isAvx = AvxUtils.CheckAvx();
                }

                return isAvx.Value;
            }
        }

        public static FastVector Create(int length)
        {
            if (IsAvx)
            {
                return new AVXFastVector(length);
            }
            else
            {
                return new FastVector(length);
            }
        }

        public static FastVector Create(FastVector other, bool copyvalue = false)
        {
            return Create(other.values, other.alignedBase, other.length, copyvalue);
        }

        public static FastVector Create(float[] _values, bool copyvalue = false)
        {
            return Create(_values, 0, _values.Length, copyvalue);
        }

        public static FastVector Create(FastVector other, int offset, int length, bool copyvalue)
        {
            return Create(other.values, other.alignedBase + offset, length, copyvalue);
        }

        public static FastVector Create(float[] _values, int offset, int length, bool copyvalue)
        {
            if (IsAvx)
            {
                return new AVXFastVector(_values, offset, length, copyvalue);
            }
            else
            {
                return new FastVector(_values, offset, length, copyvalue);
            }
        }
    }
}
