﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace ScopeML
{
    public class AVXFastVector : FastVector
    {
        public AVXFastVector(int length)
            : base(length)
        {
        }

        public AVXFastVector(float[] _values, int offset, int length, bool copyvalue)
            : base(_values, offset, length, copyvalue)
        {
        }
    }
}
