﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime;

namespace ScopeML
{
    public unsafe partial class FastVector : IDisposable
    {
        public CompressionType compressionType;

        #region Serialization and Deserialization

        public byte[] Serialize()
        {
            return this.Serialize(compressionType);
        }

        public byte[] Serialize(CompressionType ct)
        {
            return SerializeInternal(this.values, this.alignedBase, this.length, ct);
        }

        private static byte[] SerializeInternal(float[] values, int index, int length, CompressionType ct = CompressionType.None)
        {
            if (index + length > values.Length)
            {
                throw new Exception(string.Format("index :{0} plus length:{1} is larger than values length:{2}", index, length, values.Length));
            }

            // Format:
            // [CompressionType[b:BitArray,g:general,n:none]:2bytes]
            // ----BitArray
            //   [All float vaules length:4B]
            //   [All none zero values length:4B]
            //   [BitArray to indicate which value is none zero]
            //   [Float array of the none zero values]
            // ----General
            //   [Length of raw content:4B]
            //   [Compressed bytes]
            // ----None
            //   [Raw content bytes]

            MemoryStream baseStream = new MemoryStream();
            using (BinaryWriter bw = new BinaryWriter(baseStream, Encoding.Default, true))
            {
                if (ct == CompressionType.BitArray)
                {
                    bw.Write('b');
                }
                if (ct == CompressionType.None)
                {
                    bw.Write('n');
                }
                else if (ct == CompressionType.General)
                {
                    bw.Write('g');
                }

                bw.Flush();

                if (ct == CompressionType.BitArray)
                {
                    BitArray bita = new BitArray(length);
                    List<float> vs = new List<float>();
                    for (int j = 0; j < length; j++)
                    {
                        if (values[index + j] != 0.0f)
                        {
                            bita.Set(j, true);
                            vs.Add(values[index + j]);
                        }
                    }

                    int bytelength = (length + 7) >> 3;
                    int vslength = vs.Count * sizeof(float);
                    baseStream.SetLength((int)baseStream.Length + bytelength + vslength + (sizeof(int) << 1));

                    bw.Write(length);
                    bw.Write(vslength);
                    bw.Flush();

                    byte[] buf = baseStream.GetBuffer();
                    bita.CopyTo(buf, (int)baseStream.Position);
                    baseStream.Position += bytelength;

                    Buffer.BlockCopy(vs.ToArray(), 0, buf, (int)baseStream.Position, vslength);
                    baseStream.Position += vslength;
                }
                else
                {
                    int basiclength = length * sizeof(float);
                    if (ct == CompressionType.General)
                    {
                        byte[] buffer = new byte[basiclength];
                        Buffer.BlockCopy(values, index * sizeof(float), buffer, 0, basiclength);
                        CompressUtils.Compress(buffer, 0, buffer.Length, baseStream);
                    }
                    else
                    {
                        baseStream.SetLength((int)baseStream.Length + basiclength);
                        byte[] buf = baseStream.GetBuffer();
                        Buffer.BlockCopy(values, index * sizeof(float), buf, (int)baseStream.Position, basiclength);
                        baseStream.Position += basiclength;
                    }
                }

                return baseStream.ToArray();
            }
        }

        public IEnumerable<Tuple<int, byte[]>> SerializeToPartitions(int parnum, CompressionType ct = CompressionType.None)
        {
            int parfeaturenum = (this.length + parnum - 1) / parnum;
            for (int i = 0; i < parnum; i++)
            {
                int index = this.alignedBase + i * parfeaturenum;
                int length = (i == parnum - 1 ? this.length - index : parfeaturenum);

                yield return Tuple.Create<int, byte[]>(i, SerializeInternal(this.values, index, length, ct));
            }
        }

        public static FastVector DeserializeSinglePartition(byte[] buffer, out CompressionType compressionType, int bufIndex = 0, int count = -1)
        {
            compressionType = CompressionType.None;
            CompressionType compressionTypeInStream = CompressionType.None;

            using (MemoryStream baseStream = new MemoryStream(buffer, bufIndex, count == -1 ? buffer.Length - bufIndex : count, false))
            using (BinaryReader br = new BinaryReader(baseStream, Encoding.Default, true))
            {
                char type = br.ReadChar();
                if (type == 'b')
                {
                    compressionTypeInStream = CompressionType.BitArray;
                }
                else if (type == 'g')
                {
                    compressionTypeInStream = CompressionType.General;
                }

                compressionType = compressionTypeInStream;

                FastVector _values = null;
                if (compressionTypeInStream == CompressionType.BitArray)
                {
                    int length = br.ReadInt32();
                    int valuelength = br.ReadInt32();
                    int bitBytes = (length + 7) >> 3;
                    byte[] bitbyte = br.ReadBytes(bitBytes);
                    float[] densevalues = new float[valuelength >> 2];
                    Buffer.BlockCopy(buffer, (int)baseStream.Position, densevalues, 0, valuelength);
                    BitArray ba = new BitArray(bitbyte);
                    _values = FastVector.Create(length);
                    int index = 0;
                    _values.UnsafeAccess((ptr, len) =>
                    {
                        for (int i = 0; i < length; i++)
                        {
                            if (ba.Get(i))
                            {
                                ptr[i] = densevalues[index];
                                index++;
                            }
                        }
                    });
                }
                else
                {
                    byte[] vbuffer = buffer;
                    int offset = (int)baseStream.Position;
                    if (compressionTypeInStream == CompressionType.General)
                    {
                        vbuffer = CompressUtils.Decompress(baseStream);
                        offset = 0;
                    }

                    _values = FastVector.Create((vbuffer.Length - offset) / sizeof(float));

                    _values.Access((vals, off, len) =>
                    {
                        Buffer.BlockCopy(vbuffer, offset, vals, off << 2, vbuffer.Length - offset);
                    });
                }

                return _values;
            }
        }

        public static FastVector Deserialize(byte[] buffer, int index = 0, int count = -1)
        {
            CompressionType compressionType = CompressionType.None;
            FastVector vector = DeserializeSinglePartition(buffer, out compressionType, index, count);
            vector.compressionType = compressionType;
            return vector;
        }

        public static FastVector DeserializeFromPartitions(Tuple<int, byte[]>[] idbuffers)
        {
            if (idbuffers.Length == 0)
                return null;
            if (idbuffers.Length > 1)
            {
                Array.Sort(idbuffers);
            }

            FastVector[] valuelist = new FastVector[idbuffers.Length];
            CompressionType compressionType = CompressionType.None;
            idbuffers.AsParallel().ForAll(t => valuelist[t.Item1] = DeserializeSinglePartition(t.Item2, out compressionType));

            int length = valuelist.Sum(v => v.Length);

            FastVector vector = FastVector.Create(length);
            vector.compressionType = compressionType;

            int index = 0;
            for (int i = 0; i < valuelist.Length; i++)
            {
                valuelist[i].CopyTo(0, vector, index, valuelist[i].length);
                index += valuelist[i].length;
            }
            valuelist = null;

            return vector;
        }

        public enum CompressionType
        {
            None,
            General
            , BitArray
        }
        #endregion
    }
}
