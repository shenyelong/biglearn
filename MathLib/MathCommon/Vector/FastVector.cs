﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MachineLearning.Internal.Sse;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime;

namespace ScopeML
{
    public unsafe partial class FastVector : IDisposable, IEnumerable<float>, IMathVector
    {
        private const int DefaultAlignmentInBytes = 32;

        protected bool disposed = false;
        protected float[] values = null;
        protected int length;

        protected int alignedBase;

        protected float* pinnedBasePtr;
        protected GCHandle pinHandle;

        // TODO: Change to protected so that it can only be created by factory method
        protected FastVector(int length)
        {
            this.CreatePinnedAlignedBuffer(length);
        }

        protected FastVector(float[] values, int offset, int length, bool copyvalue)
        {
            if (copyvalue)
            {
                this.CreatePinnedAlignedBuffer(length);
                Array.Copy(values, offset, this.values, this.alignedBase, length);
            }
            else
            {
                this.PinUnalignedBuffer(values, offset, length);
            }
        }

        public int Length
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]
            get { return length; }
        }

        /// <summary>
        /// This method is not efficient to access the elements in vector, 
        /// to access the inner data array, use Access method.
        /// TODO: Refactor the existing code using this to use Access method.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public float this[uint i]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]
            get { return this.pinnedBasePtr[i]; }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]
            set { this.pinnedBasePtr[i] = value; }
        }

        /// <summary>
        /// This method is not efficient to access the elements in vector, 
        /// to access the inner data array, use Access method.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public float this[int i]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]
            get { return this.pinnedBasePtr[i]; }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            [TargetedPatchingOptOut("Performance critical to inline across NGen image boundaries")]
            set { this.pinnedBasePtr[i] = value; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        IEnumerator<float> IEnumerable<float>.GetEnumerator()
        {
            return (IEnumerator<float>)GetEnumerator();
        }

        public FastVectorEnumerator GetEnumerator()
        {
            return new FastVectorEnumerator(this.pinnedBasePtr, this.length);
        }

        #region IDisposable
        ~FastVector()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                // Dispose managed resources.
                this.values = null;
                this.length = 0;
                this.alignedBase = 0;

                // Call the appropriate methods to clean up 
                // unmanaged resources here. 
                // If disposing is false, 
                // only the following code is executed.
                if (this.pinHandle.IsAllocated)
                {
                    this.pinHandle.Free();
                }

                if (this.pinnedBasePtr != null)
                {
                    this.pinnedBasePtr = null;
                }

                // Note disposing has been done.
                disposed = true;
            }
        }
        #endregion

        public int NonZeroCount
        {
            get
            {
                return values.Count(i => i != 0.0f);
            }
        }

        public void ZeroItems()
        {
            Array.Clear(this.values, 0, this.values.Length);
        }

        public void Access(VectorAccessor accessor)
        {
            accessor(this.values, this.alignedBase, this.length);
        }

        public void UnsafeAccess(VectorUnsafeAccessor accessor)
        {
            accessor(this.pinnedBasePtr, this.length);
        }

        #region data operations
        public void CopyTo(float[] target)
        {
            this.CopyTo(0, target, 0, target.Length);
        }

        public void CopyTo(FastVector target)
        {
            this.CopyTo(0, target, 0, target.Length);
        }

        public void CopyTo(int srcOffset, float[] target, int targetOffset, int count)
        {
            Array.Copy(this.values, this.alignedBase + srcOffset, target, targetOffset, count);
        }

        public void CopyTo(int srcOffset, FastVector target, int targetOffset, int length)
        {
            Array.Copy(this.values, this.alignedBase + srcOffset, target.values, target.alignedBase + targetOffset, length);
        }

        public void CopyTo(int[] srcIndices, float[] target)
        {
            this.CopyTo(srcIndices, 0, target, 0, srcIndices.Length);
        }

        public void CopyTo(int[] srcIndices, int idxOffset, float[] target, int targetOffset, int count)
        {
            if ((srcIndices.Length - idxOffset < count) || (target.Length - targetOffset < count) || count > this.length)
            {
                throw new ArgumentException(string.Format("Input arguments are invalid. indices:{0}:{1}, target:{2}:{3}, length:{4}, this:{5}",
                    srcIndices.Length, idxOffset, target.Length, targetOffset, count, this.length));
            }

            fixed (int* idxPtr = &srcIndices[idxOffset])
            fixed (float* targetPtr = &target[targetOffset])
            {
                this.CopyTo(idxPtr, targetPtr, count);
            }
        }

        public void CopyTo(int[] srcIndices, FastVector target)
        {
            this.CopyTo(srcIndices, 0, target, 0, srcIndices.Length);
        }

        public void CopyTo(int[] srcIndices, int idxOffset, FastVector target, int targetOffset, int count)
        {
            if ((srcIndices.Length - idxOffset < count) || (target.length - targetOffset < count) || count > this.length)
            {
                throw new ArgumentException(string.Format("Input arguments are invalid. indices:{0}:{1}, target:{2}:{3}, length:{4}, this:{5}",
                    srcIndices.Length, idxOffset, target.length, targetOffset, count, this.length));
            }

            float* targetPtr = target.pinnedBasePtr + targetOffset;
            fixed (int* idxPtr = &srcIndices[idxOffset])
            {
                this.CopyTo(idxPtr, targetPtr, count);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void CopyTo(int* srcIdxPtr, float* targetPtr, int count)
        {
            for (int i = 0; i < count; i++)
            {
                targetPtr[i] = this.pinnedBasePtr[srcIdxPtr[i]];
            }
        }
        #endregion

        protected void CreatePinnedAlignedBuffer(int length)
        {
            int align = DefaultAlignmentInBytes;
            if ((align & 0x3) != 0 || (align & (align - 1)) != 0)
            {
                throw new ArgumentException(string.Format("Alignment must be power of 2 and multiple of 4. Intput alignment: {0}", align));
            }

            this.values = new float[length + (align >> 2)];
            this.length = length;
            // Pin down the memory of items
            this.pinHandle = GCHandle.Alloc(this.values, GCHandleType.Pinned);

            long addru = this.pinHandle.AddrOfPinnedObject().ToInt64();
            this.alignedBase = ((align - (int)((addru & (align - 1)))) & (align - 1)) >> 2;

            this.pinnedBasePtr = (float*)(this.pinHandle.AddrOfPinnedObject() + (alignedBase << 2)).ToPointer();
        }

        protected void PinUnalignedBuffer(float[] values, int offset, int length)
        {
            this.values = values;
            this.length = length;
            // Pin down the memory of items
            this.pinHandle = GCHandle.Alloc(this.values, GCHandleType.Pinned);
            this.alignedBase = offset;

            this.pinnedBasePtr = (float*)(this.pinHandle.AddrOfPinnedObject() + (alignedBase << 2)).ToPointer();
        }

        internal float* GetPinnedBasePtr()
        {
            return this.pinnedBasePtr;
        }

        public delegate void VectorAccessor(float[] value, int offset, int length);

        public delegate void VectorUnsafeAccessor(float* valPtr, int length);

        private static int? maxDegreeOfParallelism;
        public static int MaxDegreeOfParallelism
        {
            get
            {
                if (!maxDegreeOfParallelism.HasValue || maxDegreeOfParallelism.Value <= 0)
                {
                   maxDegreeOfParallelism = Environment.ProcessorCount*2;                   
                }

                return maxDegreeOfParallelism.Value;
            }
            set
            {
                maxDegreeOfParallelism = value;
            }
        }
    }
}
