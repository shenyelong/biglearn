﻿//using CommLib;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
using BigLearn.Common;
namespace BigLearn.DeepNet
{
    // public unsafe class BlockMomentumOptimizerFlow : OptimizerFlow
    // {
    //     System.Action DoBM;
    //     private List<ModelOptimizer> localOpts;
    //     private IGradientAggregator Aggregator;
    //     private RunnerBehavior behavior;
    //     private DNNOptimizerType localOptimizer;
    //     private float[] globalWeight;
    //     private float[] globalMomemtum;
    //     private float eta = 0.4f;
    //     private float epsilon = 1.4f;
    //     private double gama = 1e-4;
    //     private int blockSize = 1000;
    //     private float localLearningRate = 0.4f;
    //     private bool resetLocalOpt = true;
    //     int iter = 0;
    //     public IMathOperationManager ComputeLib
    //     {
    //         get
    //         {
    //             if (behavior.Computelib == null)
    //             {
    //                 behavior.Computelib = MathOperatorManager.CreateInstance(behavior.Device);
    //             }
    //             return behavior.Computelib;
    //         }
    //     }

    //     public override double Run()
    //     {
    //         double Loss = 0;
    //         double batchLoss = 0;
    //         int batchIdx = 0;
    //         CG.Init();
    //         int blockCnt = 0;
    //         while (CG.FetchData())
    //         {
    //             if (!CG.IsContinue) continue;
    //             batchLoss = CG.ForwardLoss();
    //             Loss += batchLoss;

    //             if (++batchIdx % 100 == 0)
    //             {
    //                 Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}, BatchLoss: {2}", batchIdx, Loss / batchIdx, batchLoss);
    //             }

    //             CG.BackwardGrad();

    //             blockCnt++;
    //             if (blockCnt % blockSize == 0)
    //             {
    //                 DoBM();
    //                 blockCnt = 0;
    //             }
    //         }

    //         if (blockCnt > 0)
    //         {
    //             DoBM();
    //         }

    //         CG.Complete();
    //         return Loss / batchIdx;
    //     }

    //     public override void Init(Structure model, RunnerBehavior behavior, ComputationGraph cg, StructureLearner learner)
    //     {
    //         Model = model;
    //         CG = cg;
    //         Device = behavior.Device;
    //         this.behavior = behavior;
    //         this.localOpts = new List<ModelOptimizer>();
    //         Aggregator = learner.GradientAggFactory == null ? null : learner.GradientAggFactory.CreateAggregator(0);
    //         this.localLearningRate = learner.LearnRate;
    //         /// **************** Init Model-Weight Optimizer; ***************** ///
    //         foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
    //         {
    //             this.localOpts.Add(optimizer);
    //         }

    //         this.localOpts = this.localOpts.OrderBy(v => v.Parameter.Size).ToList();
    //         this.SetArgs(learner.OptArgs);
    //         CG.Init();

    //         StructureLearner subLearner = new StructureLearner
    //         {
    //             LearnRate = this.localLearningRate,
    //             Optimizer = this.localOptimizer,
    //             device = this.behavior.Device
    //         };

    //         int elements = this.localOpts.Sum(v => v.Parameter.EffectiveSize);

    //         this.globalWeight = new float[elements];
    //         this.globalMomemtum = new float[elements];
    //         int offset = 0;
    //         for (int i = 0; i < this.localOpts.Count; i++)
    //         {
    //             this.localOpts[i].Optimizer = GradientOptimizer.CreateLocalOptimizer(this.localOpts[i].Parameter, subLearner, this.behavior);
    //             Array.Copy(this.localOpts[i].Parameter.MemPtr, 0, this.globalWeight, offset, this.localOpts[i].Parameter.EffectiveSize);
    //             offset += this.localOpts[i].Parameter.EffectiveSize;
    //         }

    //         this.DoBM = BlockMomentumWithNAG;
    //         Console.WriteLine("Set BM with NAG, ResetLocalOpt: {0}.", this.resetLocalOpt);
    //     }
        
    //     private void SetArgs(string args)
    //     {
    //         args = args ?? string.Empty;

    //         string[] argPairs = args.Split(',');
    //         NamedArgsParser parser = new NamedArgsParser(argPairs);
    //         this.eta = parser.Get("ETA", 0.4f);
    //         this.epsilon = parser.Get("EPSILON", 1.4f);
    //         this.gama = parser.Get("GAMA", 1e-4);
    //         this.blockSize = parser.Get("BLOCK", 50);
    //         string localOptStr = parser.Get("LOCAL-OPTIMIZER", DNNOptimizerType.ClipSGD.ToString());
    //         this.localOptimizer = (DNNOptimizerType)Enum.Parse(typeof(DNNOptimizerType), localOptStr);
    //         this.resetLocalOpt = parser.Get("RESET-LOCAL", true);
    //     }

    //     private void BlockMomentumWithNAG()
    //     {
    //         var cnt = PerfCounter.Manager.Instance["AllReduce"].Begin();
    //         int n = AsyncCommunication.GetMachineCount();
    //         //float d = 1.0f / n;
    //         //int bucket = bmCnt++ / (n * 5);
    //         iter++;
    //         //float br = Math.Min(1 - 0.8f / (bucket + 1), eta);
    //         float et = eta;
    //         float eps = epsilon/n;
    //         int[] chunks = new int[this.localOpts.Count];
    //         for (int i = 1; i < chunks.Length; i++)
    //         {
    //             chunks[i] = chunks[i - 1] + this.localOpts[i - 1].Parameter.EffectiveSize;
    //         }

    //         for (int i = 0; i < chunks.Length; i++)
    //         {
    //             float[] param = this.localOpts[i].Parameter.MemPtr;
    //             int off = chunks[i];
    //             Parallel.For(0, this.localOpts[i].Parameter.EffectiveSize, j =>
    //             {
    //                 float delta = param[j] - this.globalWeight[off + j];
    //                 param[j] = this.globalWeight[off + j];
    //                 this.globalWeight[off + j] = delta;
    //             });
    //         };

    //         DistComputationUtils.Sum(this.globalWeight, globalWeight.Length, 120 * 1000);

    //         PerfCounter.Manager.Instance["BlockMomentum"].CountOperation(() =>
    //         {
    //             for (int i = 0; i < chunks.Length; i++)
    //             {
    //                 float[] subGlobalWeights = this.localOpts[i].Parameter.MemPtr;
    //                 int off = chunks[i];

    //                 Parallel.For(0, this.localOpts[i].Parameter.EffectiveSize, j =>
    //                 {
    //                     float gradient = this.globalWeight[off + j];
    //                     this.globalMomemtum[off + j] = et * this.globalMomemtum[off + j] + eps * gradient;
    //                     this.globalWeight[off + j] = subGlobalWeights[j] + this.globalMomemtum[off + j] - (float)(gama * subGlobalWeights[j]);
    //                     if (float.IsNaN(this.globalWeight[off + j]))
    //                     {
    //                         Console.WriteLine("Invlaide weight. Nan, {0}, {1}", this.globalMomemtum[off + j], subGlobalWeights[j]);
    //                         throw new InvalidDataException();
    //                     }

    //                     subGlobalWeights[j] = this.globalWeight[off + j] + et * this.globalMomemtum[off + j];
    //                 });

    //                 if (this.resetLocalOpt)
    //                 {
    //                     this.localOpts[i].Optimizer.Reset();
    //                 }
    //             }
    //         });

    //         PerfCounter.Manager.Instance["AllReduce"].TakeCount(cnt);
    //     }

    //     private void BolockMomentumWithCM()
    //     {
    //         var cnt = PerfCounter.Manager.Instance["AllReduce"].Begin();
    //         int n = AsyncCommunication.GetMachineCount();
    //         //float d = 1.0f / n;
    //         //int bucket = bmCnt++ / (n * 5);
    //         iter++;
    //         //float br = Math.Min(1 - 0.8f / (bucket + 1), eta);
    //         float et = eta;
    //         float eps = epsilon / n;
    //         int[] chunks = new int[this.localOpts.Count];
    //         for (int i = 1; i < chunks.Length; i++)
    //         {
    //             chunks[i] = chunks[i - 1] + this.localOpts[i - 1].Parameter.EffectiveSize;
    //         }

    //         for (int i = 0; i < chunks.Length; i++)
    //         {
    //             float[] param = this.localOpts[i].Parameter.MemPtr;
    //             int off = chunks[i];
    //             Parallel.For(0, this.localOpts[i].Parameter.EffectiveSize, j =>
    //             {
    //                 float delta = param[j] - this.globalWeight[off + j];
    //                 param[j] = this.globalWeight[off + j];
    //                 this.globalWeight[off + j] = delta;
    //             });
    //         };

    //         DistComputationUtils.Sum(this.globalWeight, globalWeight.Length, 120 * 1000);

    //         PerfCounter.Manager.Instance["BlockMomentum"].CountOperation(() =>
    //         {
    //             for (int i = 0; i < chunks.Length; i++)
    //             {
    //                 float[] subGlobalWeights = this.localOpts[i].Parameter.MemPtr;
    //                 int off = chunks[i];

    //                 Parallel.For(0, this.localOpts[i].Parameter.EffectiveSize, j =>
    //                 {
    //                     float gradient = this.globalWeight[off + j];
    //                     this.globalMomemtum[off + j] = et * this.globalMomemtum[off + j] + eps * gradient;
    //                     this.globalWeight[off + j] = subGlobalWeights[j] + this.globalMomemtum[off + j] - (float)(gama * subGlobalWeights[j]);
    //                     if (float.IsNaN(this.globalWeight[off + j]))
    //                     {
    //                         Console.WriteLine("Invlaide weight. Nan, {0}, {1}", this.globalMomemtum[off + j], subGlobalWeights[j]);
    //                         throw new InvalidDataException();
    //                     }

    //                     subGlobalWeights[j] = this.globalWeight[off + j];
    //                 });

    //                 if (this.resetLocalOpt)
    //                 {
    //                     this.localOpts[i].Optimizer.Reset();
    //                 }
    //             }
    //         });

    //         PerfCounter.Manager.Instance["AllReduce"].TakeCount(cnt);
    //     }

    //     public override void Save(BinaryWriter writer)
    //     {
    //         writer.Write(iter);
    //         int size = this.globalMomemtum.Length * sizeof(float);
    //         writer.Write(size);
    //         byte[] tempBuf = new byte[this.globalMomemtum.Length * sizeof(float)];
    //         Buffer.BlockCopy(this.globalMomemtum, 0, tempBuf, 0, tempBuf.Length);
    //         writer.Write(tempBuf, 0, tempBuf.Length);
    //     }

    //     public override void Load(BinaryReader reader)
    //     {
    //         this.iter = reader.ReadInt32();
    //         int size = reader.ReadInt32();
    //         byte[] tempBuf = new byte[size];
    //         reader.Read(tempBuf, 0, size);
    //         Buffer.BlockCopy(tempBuf, 0, this.globalMomemtum, 0, tempBuf.Length);
    //     }
    // }
}
