﻿//using CommLib;
//using ScopeML;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BigLearn;
#pragma warning disable IDE0003
namespace BigLearn.DeepNet
{
    // unsafe class SSPSharedAggregator : IGradientAggregator
    // {
    //     private static BlockingCollection<BufState> SendBuf;
    //     private static BlockingCollection<BufState> outFreeBuf;
    //     private static BlockingCollection<BufState> RecvBuf;
    //     private static BlockingCollection<BufState> inFreeBuf;
    //     private static ConcurrentQueue<IntPtr> RecvQueue;
    //     private static uint SessionId;
    //     private static bool Inited;
    //     private static uint AggCounts;
    //     private static int TotalDim;

    //     private static BufState pullBuf;
    //     private static BufState pushBuf;
    //     private static bool cancelled;
    //     private static int IterCnt;
    //     private static ManualResetEvent SyncEvent;
    //     protected static int delta;

    //     protected int Dim;
    //     protected int Offset;

    //     protected bool AggInited;

    //     static SSPSharedAggregator()
    //     {
    //         SendBuf = new BlockingCollection<BufState>(2);
    //         RecvBuf = new BlockingCollection<BufState>(2);
    //         outFreeBuf = new BlockingCollection<BufState>(2);
    //         inFreeBuf = new BlockingCollection<BufState>(2);
    //         RecvQueue = new ConcurrentQueue<IntPtr>();
    //         SessionId = AsyncCommunication.RegisterAyncBroadcastSession(OnBroadcastRecv, null);
    //         ThreadPool.QueueUserWorkItem(SendProc);
    //         ThreadPool.QueueUserWorkItem(RecvProc);
    //         Inited = false;
    //         AggCounts = 0;
    //         TotalDim = 1;
    //         pullBuf = null;
    //         pushBuf = null;
    //         SyncEvent = new ManualResetEvent(true);
    //         delta = 10;
    //     }

    //     ~SSPSharedAggregator()
    //     {
    //         cancelled = true;
    //     }

    //     public SSPSharedAggregator(int dim)
    //     {
    //         this.Dim = dim;
    //         this.AggInited = false;
    //     }

    //     public uint Id { get; private set; }

    //     public void InitAgg()
    //     {
    //         if (!this.AggInited)
    //         {
    //             this.AggInited = true;
    //             this.Id = SSPSharedAggregator.AggCounts;
    //             this.Offset = SSPSharedAggregator.TotalDim;
    //             SSPSharedAggregator.TotalDim += this.Dim;
    //             SSPSharedAggregator.AggCounts++;
    //         }
    //     }

    //     public override void Aggregate(params CudaPieceFloat[] grads)
    //     {
    //         if (this.Id == 0)
    //         {
    //             IterCnt++;
    //         }

    //         if (this.AggInited)
    //         {
    //             if (!SSPSharedAggregator.Inited)
    //             {
    //                 SSPSharedAggregator.Init();
    //             }

    //             foreach (var g in grads)
    //             {
    //                 PerfCounter.Manager.Instance["AsyncPush"].CountOperation(() => AsyncPush(g));
    //                 PerfCounter.Manager.Instance["AsyncPull"].CountOperation(() => AsyncPull(g));
    //             }
    //         }
    //         else
    //         {
    //             InitAgg();
    //         }
    //     }

    //     protected void AsyncPull(CudaPieceFloat grads)
    //     {
    //         //if (this.Id == 0)
    //         //{
    //         PerfCounter.Manager.Instance["SyncWait"].CountOperation(() => SyncEvent.WaitOne(delta));
    //         //}

    //         if (SSPSharedAggregator.pullBuf == null)
    //         {
    //             RecvBuf.TryTake(out pullBuf);
    //         }

    //         if (pullBuf != null)
    //         {
    //             fixed (float* pDst = grads.MemPtr)
    //             {
    //                 fixed (float* pSrc = &pullBuf.Buffer[this.Offset])
    //                 {
    //                     FastVector.Add_VectorAndClear(pDst, (float*)pSrc, grads.Size);
    //                 }
    //             }

    //             Console.WriteLine("Succeeded to get recv buf. {0}. This:{1}<>Remote:{2}", pullBuf.RefCount, IterCnt, pullBuf.Buffer[0]);

    //             pullBuf.RefCount--;

    //             if (pullBuf.RefCount == 0)
    //             {
    //                 pullBuf.RefCount = SSPSharedAggregator.AggCounts;
    //                 SSPSharedAggregator.inFreeBuf.Add(pullBuf);
    //                 pullBuf = null;
    //             }
    //         }
    //     }

    //     protected void AsyncPush(CudaPieceFloat grads)
    //     {
    //         // TODO: Need to further optimize the gap between send and fill
    //         if (SSPSharedAggregator.pushBuf == null)
    //         {
    //             if (!SendBuf.TryTake(out pushBuf))
    //             {
    //                 pushBuf = outFreeBuf.Take();
    //             }
    //         }

    //         fixed (float* pDst = &pushBuf.Buffer[this.Offset])
    //         {
    //             fixed (float* pSrc = grads.MemPtr)
    //             {
    //                 FastVector.Add_Vector((float*)pDst, pSrc, grads.Size);
    //             }
    //         }

    //         pushBuf.RefCount--;
    //         if (pushBuf.RefCount == 0)
    //         {
    //             pushBuf.RefCount = SSPSharedAggregator.AggCounts;
    //             pushBuf.Buffer[0] = IterCnt;
    //             SendBuf.Add(pushBuf);
    //             pushBuf = null;
    //         }
    //     }

    //     protected static void Init()
    //     {
    //         if (!SSPSharedAggregator.Inited)
    //         {
    //             BufState stat0 = new BufState() { Buffer = new float[TotalDim], RefCount = SSPSharedAggregator.AggCounts };
    //             BufState stat1 = new BufState() { Buffer = new float[TotalDim], RefCount = SSPSharedAggregator.AggCounts };
    //             outFreeBuf.Add(stat0);
    //             outFreeBuf.Add(stat1);

    //             BufState stat3 = new BufState() { Buffer = new float[TotalDim], RefCount = SSPSharedAggregator.AggCounts };
    //             BufState stat4 = new BufState() { Buffer = new float[TotalDim], RefCount = SSPSharedAggregator.AggCounts };
    //             inFreeBuf.Add(stat3);
    //             inFreeBuf.Add(stat4);
    //             SSPSharedAggregator.Inited = true;
    //         }
    //     }

    //     protected static void RecvProc(object state)
    //     {
    //         int recvCnt = 0;
    //         try
    //         {
    //             while (!cancelled)
    //             {
    //                 IntPtr recvPtr = IntPtr.Zero;
    //                 if (!RecvQueue.TryDequeue(out recvPtr))
    //                 {
    //                     continue;
    //                 }

    //                 Console.WriteLine("[{0}]Received {1}", SessionId, recvCnt++);

    //                 BufState accBuf = null;
    //                 if (!RecvBuf.TryTake(out accBuf))
    //                 {
    //                     accBuf = inFreeBuf.Take();
    //                 }

    //                 if (accBuf != null)
    //                 {
    //                     accBuf.Buffer[0] = 0;
    //                     fixed (float* pDst = accBuf.Buffer)
    //                     {
    //                         FastVector.Add_Vector(pDst, (float*)recvPtr.ToPointer(), accBuf.Buffer.Length);
    //                     }

    //                     RecvBuf.Add(accBuf);
    //                     if (IterCnt - accBuf.Buffer[0] > 10)
    //                     {
    //                         // TODO: Due to current code base, it's hard to enforce hard synchonorision. Instead, I use soft wait to slow down the faster runner.
    //                         if (IterCnt - accBuf.Buffer[0] > 100)
    //                         {
    //                             delta = (int)(IterCnt - accBuf.Buffer[0]) * 10;
    //                         }
    //                         else
    //                         {
    //                             delta = 10;
    //                         }

    //                         SyncEvent.Reset();
    //                     }
    //                     else
    //                     {
    //                         SyncEvent.Set();
    //                     }
    //                 }
    //                 else
    //                 {
    //                     Console.WriteLine("Failed to get recv buf.");
    //                 }

    //                 AsyncCommunication.FreeSessionBuffer(recvPtr.ToPointer());
    //             }
    //         }
    //         catch (Exception ex)
    //         {
    //             Console.WriteLine("Exception happenned in recv proc: {0}", ex);
    //         }
    //     }

    //     protected static void SendProc(object state)
    //     {
    //         try
    //         {
    //             while (!cancelled)
    //             {
    //                 BufState outBuf = SendBuf.Take();
    //                 if (outBuf == null)
    //                 {
    //                     continue;
    //                 }

    //                 fixed (float* pSend = outBuf.Buffer)
    //                 {
    //                     AsyncCommunication.BroadcastSession(SessionId, pSend, (uint)outBuf.Buffer.Length * sizeof(float));
    //                 }

    //                 FastVector.ZeroItems(outBuf.Buffer, outBuf.Buffer.Length);
    //                 outFreeBuf.Add(outBuf);
    //             }
    //         }
    //         catch (Exception ex)
    //         {
    //             Console.WriteLine("Exception happenned in send proc: {0}", ex);
    //         }
    //     }

    //     protected static void OnBroadcastRecv(void* pBuffer, ulong size, void* args)
    //     {
    //         RecvQueue.Enqueue(new IntPtr(pBuffer));
    //     }
    // }
#pragma warning restore IDE0003
}
