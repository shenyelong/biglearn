﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    class AllReduceGradAggFactory : IGradientAggregatorFactory
    {
        public AllReduceGradAggFactory(AggAlgorithmType algorithm = AggAlgorithmType.Native)
        {
            this.Algorithm = algorithm;
        }

        public AggAlgorithmType Algorithm {get; private set;}

        public override IGradientAggregator CreateAggregator(int dimension)
        {
            if (this.Algorithm == AggAlgorithmType.AllReduceNCCL)
            {
                return new MultiGPUAggregator();
            }
            else
            {
                return new MultiGPUAggregator();
                //return new NativeGradientAggregator();
            }
        }

        public enum AggAlgorithmType
        {
            Native,
            OneBit,
            AllReduceNCCL
        }
    }
}
