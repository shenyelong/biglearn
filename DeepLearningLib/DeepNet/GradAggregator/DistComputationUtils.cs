﻿//using CommLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    // public static unsafe class DistComputationUtils
    // {
    //     //[DllImport("msvcrt.dll")]
    //     //public static extern int _putenv_s(string e, string v);

    //     public static void SetupChaNa(string servers, bool useRdma, int ioThreadNum = 2)
    //     {
    //         //CHANA_PIN_CPU = False
    //         //_putenv_s("CHANA_PIN_CPU", "false");
    //         //Environment.SetEnvironmentVariable("CHANA_PIN_CPU", "false", EnvironmentVariableTarget.Process);
    //         AsyncCommunication.Init(servers, ioThreadNum, useRdma);
    //         AsyncCommunication.Barrier();
    //     }

    //     public static void Sum(CudaPieceFloat values, int length, int timeout)
    //     {
    //         Stopwatch sw = new Stopwatch();

    //         sw.Restart();
    //         var sumTask = Task.Factory.StartNew(() =>
    //             {
    //                 fixed (void* ptr = &values.MemPtr[0])
    //                 {
    //                     AsyncCommunication.AllReduce(ptr, (ulong)length, sizeof(float), AllReduceDataType.TpFloat, AllReduceOpType.OpSum);
    //                 }
    //             });

    //         if (!sumTask.Wait(timeout))
    //         {
    //             throw new TimeoutException(string.Format("Time out while try to aggregate values for vector: {0},{1},{2}, Has NAN: {3}", values.Size, length,
    //                 values.EffectiveSize, values.MemPtr.All(p => float.IsNaN(p))));
    //         }

    //         sw.Stop();
    //         Console.WriteLine("Finished to sum {0} floats with other nodes in {1}ms", length, sw.ElapsedMilliseconds);
    //     }

    //     public static void Sum(float[] values, int length, int timeout)
    //     {
    //         Stopwatch sw = new Stopwatch();
    //         sw.Restart();
    //         var sumTask = Task.Factory.StartNew(() =>
    //         {
    //             fixed (void* ptr = &values[0])
    //             {
    //                 AsyncCommunication.AllReduce(ptr, (ulong)length, sizeof(float), AllReduceDataType.TpFloat, AllReduceOpType.OpSum);
    //             }
    //         });

    //         if (!sumTask.Wait(timeout))
    //         {
    //             throw new TimeoutException(string.Format("Time out while try to aggregate values for vector: {0},{1}", values.Length, length));
    //         }

    //         sw.Stop();

    //         Console.WriteLine("Finished to sum {0} floats with other nodes in {1} ms, {2} Mbps", length, sw.ElapsedMilliseconds,
    //             (long)length * 32.0 * Stopwatch.Frequency / (1e6 * sw.ElapsedTicks));
    //     }
    // }
}
