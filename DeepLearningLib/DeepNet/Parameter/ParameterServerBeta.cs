﻿//using ScopeML;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    /// <summary>
    /// One Consumer + Multiple Producers.
    /// UnBlock Call; Send(); Multiple Producers;
    /// Block Call; Receive(float[] memory); One Consumer;
    /// </summary>
    // public class PushDataPipeline
    // {
    //     List<CudaPieceFloat> MemoryBlock = new List<CudaPieceFloat>();
    //     BlockingCollection<int> WritableBufferIndex;
    //     BlockingCollection<int> ReadableBufferIndex;

    //     int Dim { get; set; }
    //     int Cache { get; set; }

    //     //int l1Idx = dataBuffer.Take(); ///CPU buffer.
    //     /// <summary>
    //     /// 
    //     /// </summary>
    //     /// <param name="dim"></param>
    //     /// <param name="cache"></param>
    //     public PushDataPipeline(int dim, int cache)
    //     {
    //         for (int i = 0; i < cache; i++)
    //         {
    //             /// Parameter Server build on CPU.
    //             MemoryBlock.Add(new CudaPieceFloat(dim, true, false));
    //         }

    //         WritableBufferIndex = new BlockingCollection<int>(cache);
    //         ReadableBufferIndex = new BlockingCollection<int>(cache);
    //         for (int i = 0; i < cache; i++) WritableBufferIndex.Add(i);
    //         Dim = dim;
    //         Cache = cache;
    //     }

    //     /// <summary>
    //     /// UnBlock Call; Multiple Producers;
    //     /// </summary>
    //     /// <param name="memory"></param>
    //     public void Push(Structure model)
    //     {
    //         int location = 0;
    //         while (!WritableBufferIndex.TryTake(out location))
    //         {
    //             int nl = ReadableBufferIndex.Take();
    //             WritableBufferIndex.Add(nl);
    //         }
    //         model.GetModelGradient(MemoryBlock[location], 0);
    //         //location = WritableBufferIndex.Take();
    //         //Buffer.BlockCopy(memory, 0, MemoryBlock[location], 0, Dim * sizeof(float));
    //         ReadableBufferIndex.Add(location);
    //     }

    //     public void Push(CudaPieceFloat param)
    //     {
    //         int location = 0;
    //         while (!WritableBufferIndex.TryTake(out location))
    //         {
    //             int nl = ReadableBufferIndex.Take();
    //             WritableBufferIndex.Add(nl);
    //         }
    //         MemoryBlock[location].CopyFrom(param);
    //         //model.GetGlobalGradient(MemoryBlock[location], 0);
    //         //location = WritableBufferIndex.Take();
    //         //Buffer.BlockCopy(memory, 0, MemoryBlock[location], 0, Dim * sizeof(float));
    //         ReadableBufferIndex.Add(location);
    //     }


    //     /// <summary>
    //     /// Block Call; One Consumer;
    //     /// </summary>
    //     /// <param name="memory"></param>
    //     public void Get(CudaPieceFloat memory)
    //     {
    //         int l = ReadableBufferIndex.Take();
    //         memory.CopyFrom(MemoryBlock[l]);
    //         //Buffer.BlockCopy(MemoryBlock[l], 0, memory, 0, Dim * sizeof(float));
    //         WritableBufferIndex.Add(l);
    //     }
    // }

    // /// <summary>
    // /// One Writer + Multiple Reader.
    // /// </summary>
    // public class PullDataPipeline
    // {
    //     List<CudaPieceFloat> MemoryBlock = new List<CudaPieceFloat>();

    //     int[] MemoryStatus; // 0 : free; >= 0; number of readers; -1 : in writer;
    //     int Cursor = -1;
    //     object CursorLock = new object();

    //     int Dim { get; set; }
    //     int Cache { get; set; }
    //     public PullDataPipeline(int dim, int cache, CudaPieceFloat initMemory)
    //     {
    //         for (int i = 0; i < cache; i++) MemoryBlock.Add(new CudaPieceFloat(dim, true, false));
    //         MemoryStatus = new int[cache];
    //         Dim = dim;
    //         Cache = cache;
    //         Cursor = 0;
    //         MemoryBlock[Cursor].CopyFrom(initMemory);
    //     }


    //     public void Pull(Structure modelParameter)
    //     {
    //         int readCursor = -1;
    //         //Interlocked.Increment(ref CursorCount);
    //         lock (CursorLock)
    //         {
    //             readCursor = Cursor;
    //             Interlocked.Increment(ref MemoryStatus[readCursor]);
    //             //Interlocked.Decrement(ref CursorCount);
    //         }
    //         modelParameter.SetModelParameter(MemoryBlock[readCursor], 0);
    //         //Buffer.BlockCopy(MemoryBlock[readCursor], 0, memory, 0, Dim * sizeof(float));
    //         Interlocked.Decrement(ref MemoryStatus[readCursor]);
    //     }

    //     public void Set(CudaPieceFloat memory)
    //     {
    //         int writeCursor = Cursor;
    //         do
    //         {
    //             writeCursor = (writeCursor + 1) % Cache;
    //             if (MemoryStatus[writeCursor] > 0) continue;
    //         } while (writeCursor == Cursor);

    //         MemoryBlock[writeCursor].CopyFrom(memory);
    //         //Buffer.BlockCopy(memory, 0, MemoryBlock[writeCursor], 0, Dim * sizeof(float));
    //         lock (CursorLock)
    //         {
    //             Cursor = writeCursor;
    //         }
    //     }
    // }

    // /// <summary>
    // ///  Parameter Server on Structure.
    // /// </summary>
    // /// <typeparam name="S"></typeparam>
    // public class ParameterServer
    // {
    //     PushDataPipeline Pushline = null;// new PushDataPipeline();
    //     PullDataPipeline Pullline = null;// new PullDataPipeline();

    //     public CudaPieceFloat ModelParameter;
    //     CudaPieceFloat grad;

    //     object pushLock = new object();
    //     /// <summary>
    //     /// receive parameter cache and delte cache.
    //     /// </summary>
    //     /// <param name="memory"></param>
    //     /// <param name="parameterCache"></param>
    //     /// <param name="delteCache"></param>
    //     public ParameterServer(CudaPieceFloat initMemory, int parameterCache, int gradCache)
    //     {
    //         ModelParameter = initMemory;

    //         Pushline = new PushDataPipeline(ModelParameter.Size, gradCache);
    //         Pullline = new PullDataPipeline(ModelParameter.Size, parameterCache, initMemory);

    //         grad = new CudaPieceFloat(ModelParameter.Size, true, false);

            
    //         var modelAggTask = Task.Run(() =>
    //         {
    //             IsStop = false;
    //             while (!IsStop)
    //             {
    //                 Pushline.Get(grad);
    //                 /// get gradient.
    //                 /// apply addition to model cache.
    //                 //for (int i = 0; i < ModelCache.Length; i++) ModelCache[i] += memoryCache[i] * learnRate;

    //                 FastVector vec1 = FastVector.Create(ModelParameter.MemPtr);
    //                 FastVector vec2 = FastVector.Create(grad.MemPtr);
    //                 //vec2.Scale(learnRate);
    //                 vec1.Add(vec2);
                        
    //                 //BasicMathlib.Add_Vector(ModelParameter.MemPtr, grad.MemPtr, ModelParameter.MemPtr, ModelParameter.Size, 1, learnRate);
    //                 //.Add_Vector();
    //                 Pullline.Set(ModelParameter);
    //             }
    //         });
            
    //         ////////// receive gradient from cache.
    //     }

    //     public void Stop()
    //     {
    //     }

    //     public void PushSync(Structure model)
    //     {
    //         lock (pushLock)
    //         {
    //             model.GetModelGradient(grad, 0);

    //             FastVector.Add_Vector(ModelParameter.MemPtr, 0, grad.MemPtr, 0, ModelParameter.Size, 1, 1);

    //             //FastVector vec1 = FastVector.Create(ModelParameter.MemPtr);
    //             //FastVector vec2 = FastVector.Create(grad.MemPtr);
    //             //vec2.Scale(learnRate);
    //             //vec1.Add(vec2);
    //         }
    //     }

    //     public void PullSync(Structure model)
    //     {
    //         model.SetModelParameter(ModelParameter, 0);
    //     }

    //     public void Push(Structure model)
    //     {
    //         Pushline.Push(model);
    //     }

    //     public void Push(CudaPieceFloat param)
    //     {
    //         Pushline.Push(param);
    //     }

    //     public void Pull(Structure model)
    //     {
    //         Pullline.Pull(model);
    //     }


    //     int pullLoop = 1;
    //     int pushLoop = 1;
    //     public double ExecuteTrainMultipleComputationGraph(List<int> deviceList, List<ComputationGraph> cgs, List<Structure> structures)
    //     {
    //         double[] AvgLoss = new double[deviceList.Count];
    //         int[] StepNum = new int[deviceList.Count];

    //         Parallel.For(0, deviceList.Count, thread_idx =>
    //         {
    //             int gpuID = deviceList[thread_idx];
    //             Cudalib.CudaSetDevice(gpuID);
    //             cgs[thread_idx].Init();

    //             bool IsLoopEnd = false;
    //             int batchIdx = 0;

    //             PerfCounter.Manager instance = new PerfCounter.Manager();

    //             while (!IsLoopEnd)
    //             {
    //                 var pullCounter = instance["Pull"].Begin();
    //                 // pull model parameters.
    //                 if (batchIdx % pullLoop == 0) PullSync(structures[thread_idx]);
    //                 Cudalib.CudaDeviceSynchronize();
    //                 instance["Pull"].TakeCount(pullCounter);

    //                 //Console.WriteLine(PerfCounter.Manager.Instance["Pull"].ToString());

    //                 var stepCounter = instance["Step"].Begin();

    //                 var forwardCounter = instance["Forward"].Begin();
    //                 cgs[thread_idx].Forward();
    //                 if (cgs[thread_idx].IsTerminate)
    //                 {
    //                     IsLoopEnd = true;
    //                     break;
    //                 }
    //                 if (IsLoopEnd) break;
    //                 instance["Forward"].TakeCount(forwardCounter);

    //                 cgs[thread_idx].CleanDeriv();

    //                 var lossCounter = instance["Loss"].Begin();
    //                 StepNum[thread_idx]++;
    //                 AvgLoss[thread_idx] = (StepNum[thread_idx] - 1) * 1.0 / (StepNum[thread_idx]) * AvgLoss[thread_idx] + 1.0 / (StepNum[thread_idx]) * cgs[thread_idx].Loss();
    //                 //             if (StepNum[thread_idx] % 50 == 0) Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", StepNum, AvgLoss);
    //                 if (StepNum[thread_idx] % 50 == 0) Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", StepNum[thread_idx], AvgLoss[thread_idx]);
    //                 instance["Loss"].TakeCount(lossCounter);

    //                 var backCounter = instance["Backward"].Begin();
    //                 cgs[thread_idx].Backward();
    //                 instance["Backward"].TakeCount(backCounter);

    //                 var updateCounter = instance["Update"].Begin();
    //                 cgs[thread_idx].Update();
    //                 instance["Update"].TakeCount(updateCounter);
    //                 Cudalib.CudaDeviceSynchronize();
    //                 instance["Step"].TakeCount(stepCounter);

    //                 var pushCounter = instance["Push"].Begin();
    //                 if (batchIdx % pushLoop == 0 || IsLoopEnd)
    //                 {
    //                     // push model gradient.
    //                     PushSync(structures[thread_idx]);
    //                     structures[thread_idx].ClearModelGradient();
    //                 }

    //                 Cudalib.CudaDeviceSynchronize();
    //                 instance["Push"].TakeCount(pushCounter);
    //                 //Console.WriteLine(PerfCounter.Manager.Instance["Push"].ToString());

    //                 batchIdx++;
    //                 if (batchIdx % 20 == 0)
    //                 {
    //                     //Console.WriteLine(PerfCounter.Manager.Instance["Push"].ToString());
    //                     //Console.WriteLine(PerfCounter.Manager.Instance["Pull"].ToString());
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["Step"].ToString() + "\n");
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["Forward"].ToString() + "\n");
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["Loss"].ToString() + "\n");
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["Backward"].ToString() + "\n");
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["Update"].ToString() + "\n");
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["Data"].ToString() + "\n");
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["SparseMatrix"].ToString() + "\n");
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["LSTM"].ToString() + "\n");
    //                     Console.WriteLine("Device ID " + gpuID.ToString() + instance["Preprocess"].ToString() + "\n");
    //                 }
    //             }
    //             cgs[thread_idx].Complete();
    //         });

    //         double totalLoss = 0;
    //         int totalStepNum = 0;
    //         for (int i = 0; i < deviceList.Count; i++)
    //         {
    //             totalLoss += AvgLoss[i] * StepNum[i];
    //             totalStepNum += StepNum[i];
    //         }
    //         return totalLoss / totalStepNum;
    //     }

    // }
}
