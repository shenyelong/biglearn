﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using BigLearn;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace BigLearn.DeepNet
{
    unsafe class Test
    {
        //static IMathOperationManager lib;
        
        //static void InitLib()
        //{
        //    DeviceType device = MathOperatorManager.SetDevice(0);
        //    lib = MathOperatorManager.CreateInstance(device);
        //}

        static void TestCudaVectorScale()
        {
            IntPtr GHandle = Cudalib.CudaCreateCuBlas();
            int batch = 9;
            CudaPieceFloat inputX = new CudaPieceFloat(batch, true, true);
            inputX.Init(1);

            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();

            for (int i = 0; i < 1; i++)
            {
                //Cudalib.Matrix_Product_Weight(inputX.CudaPtr, outputY.CudaPtr, weight.CudaPtr,
                //        batch, input, output, 1);
                Cudalib.CuBLAS_Scal(GHandle, inputX.CudaPtr, 10, batch);
            }

            inputX.Print("VectorScale ", 300, false);

            //inputX.SyncToCPU();
            //stopwatch.Stop();
            
            //for(int i=0;i < batch;i ++)
            //{
            //    Console.WriteLine("num " + inputX.MemPtr[i].ToString());
            //}
            //TestCudaElementwiseProduct();

            Cudalib.CudaDestroyCuBlas(GHandle);
            inputX.Dispose();
        }

        static void TestCudaElementwiseProduct()
        {
            //Cudalib.CudaSetDevice(4);
            IntPtr cuDnnHandle = Cudalib.CudaCreateCuDnn();

            int row = 1024;
            int column = 512;
            CudaPieceFloat pLeft = new CudaPieceFloat(row * column, true, true);
            CudaPieceFloat pRight = new CudaPieceFloat(row * column, true, true);
            CudaPieceFloat pDst = new CudaPieceFloat(row * column, true, true);
            
            pLeft.Init(0.25f);
            pRight.Init(0.5f);

            int[] dims = new int[4] { 1, 1, row, column };
            
            fixed(int * pdim = &dims[0])
            {
                Cudalib.CuDNNTensorOp(cuDnnHandle, 1, 4, (IntPtr)pdim, pLeft.CudaPtr, 
                                                         (IntPtr)pdim, pRight.CudaPtr, 
                                                         (IntPtr)pdim, pDst.CudaPtr, 
                                                         1, 1, 0);
            }
            pDst.Print("element wise product cudnn ", 100, true);

            pDst.Init(98f);

            //for(int i = 0; i < row * column; i++)
            //{
            //    pLeft.MemPtr[i] = 0.25f;
            //    pRight.MemPtr[i] = 0.5f;
            //    pDst.MemPtr[i] = 98f;
            //}
            //pLeft.SyncFromCPU(row * column);
            //pRight.SyncFromCPU(row * column);
            //pDst.SyncFromCPU(row * column);

            Cudalib.Tanh(pDst.CudaPtr, pDst.CudaPtr, row * column);
            pDst.Print("Tanh ", 10, false);
            
            Stopwatch stopwatch = new Stopwatch();
            

            /******* method 1 : customerzied call ******/
            stopwatch.Restart();
            for(int m = 0; m<1000; m++)
            {
                Cudalib.ElementwiseProduct(pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, row, column, 1);
            }
            pDst.Print("Method 1: Element Wise Product ", 20, false);
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("Method 1 Time elapsed: {0}", stopwatch.Elapsed);


            /******* method 2 : cuDNN call ******/
            stopwatch.Restart();
            for(int m=0;m<1000; m++)
            {
                Cudalib.CuDNNVectorMul(cuDnnHandle, pLeft.CudaPtr, pRight.CudaPtr, pDst.CudaPtr, 1, 1, row * column);
            }
            pDst.Print("Method 2: Element Wise Product ", 20, false);
            stopwatch.Stop();
            // Write result.
            Console.WriteLine("Method 2 Time elapsed: {0}", stopwatch.Elapsed);


            Cudalib.CudaDestroyCuDnn(cuDnnHandle);

        }

        public unsafe static void TestAdd_Vector()
        {
            //float[] a, int a_idx, float[] b, int b_idx, 
             //   int size, float a_wei, float b_wei
            int size = 100;
            float[] a = new float[size];
            float[] b = new float[size];
            for(int i = 0; i < size; i++)
            {
                a[i] = i ;
                b[i] = i + 1;
            }
            fixed(float * pb = &b[0])
            fixed(float * pa = &a[0])
            {
                FastVector.Add_Vector(pa, 10, pb, 10, size - 10, 1, 1);
            }
            for(int i=0;i < size;i ++)
            {
                Console.WriteLine("num " + a[i].ToString() + " " + b[i].ToString());
            }
        }

        public static void TestDictionary()
        {
            Dictionary<string, Tuple<float[], float[]>> Mem = new Dictionary<string, Tuple<float[], float[]>>();

            for(int i=0;i<100000000;i++)
            {
                string mkey = "string" + i.ToString();
                Mem.Add(mkey, new Tuple<float[], float[]>(new float[100], new float[200]));

                if(i % 1000000 == 0)
                {
                    Console.WriteLine("push dictionary {0}", i);
                }
            }

            Random random = new Random();
            for(int i=0;i<100;i++)
            {
                int t = random.Next(100000000);
                Console.WriteLine(Mem["string"+t.ToString()].Item1.Length.ToString());
            }
        }

        public static void TestArray()
        {
            float[] m = new float[100];
            for(int i = 0; i< 100; i++)
            {
                m[i] = i;
            }

            IEnumerable<float> x = m.Take(10);
            foreach(float k in x)
            {
                Console.WriteLine(k.ToString());
            }
        }

        public static void TestSortArray()
        {
            Random random = new Random();
            float[] m = new float[5];
            for(int i = 0; i < 5; i++)
            {
                m[i] = random.Next(10000);
                Console.WriteLine(m[i]);
            }
            foreach(int k in m.Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index).Reverse())
            {
                Console.WriteLine(k);
            }
        }


        public static void TestCuSparse()
        {
            IntPtr cuSparseHandle = Cudalib.CudaCreateCuSparse();
            int batch = 100;
            CudaPieceFloat inputX = new CudaPieceFloat(batch, true, true);
            inputX.Init(100);

            SparseVectorData data = new SparseVectorData(5, DeviceType.GPU);

            data.Length = 5;
            data.Idx[0] = 1;
            data.Idx[1] = 3;
            data.Idx[2] = 5;
            data.Idx[3] = 7;
            data.Idx[4] = 9;

            data.Value[0] = 1;
            data.Value[1] = 2;
            data.Value[2] = 3;
            data.Value[3] = 4;
            data.Value[4] = 5;

            data.SyncFromCPU();

            Cudalib.SparseVectorAdd(cuSparseHandle, data.Idx.CudaPtr, data.Value.CudaPtr, inputX.CudaPtr, 1, 5);


            inputX.Print("data", 100, false);

            Cudalib.CudaDestroyCuSparse(cuSparseHandle);
        }


        public static void TestCuSum()
        {
            IntPtr cuBlasHandle = Cudalib.CudaCreateCuBlas();
            int batch = 10;
            CudaPieceFloat inputX = new CudaPieceFloat(batch, true, true);
            for(int i=0; i < batch;i++)
            {
                inputX[i] = -i ;
            }
            inputX.SyncFromCPU();
            float sum = Cudalib.CUBLAS_Sasum(cuBlasHandle, inputX.CudaPtr, batch, 1);
            Console.WriteLine(sum);
            Cudalib.CudaDestroyCuBlas(cuBlasHandle);
        }

        public static void TestMaxPooling()
        {
            int row = 5;
            int col = 8;
            int batch = 5;
            CudaPieceFloat tensor = new CudaPieceFloat(row * col * batch, true, true);
            for(int i = 0; i < batch; i++)
            {
                for(int r = 0; r < row; r++)
                {
                    for( int c = 0; c < col; c++)
                    {
                        tensor[i * row * col + r * col + c] = - i * 100 - 10 * r  + c;
                    }
                }
            }
            tensor.SyncFromCPU();
            CudaPieceFloat output = new CudaPieceFloat(col * batch, true, true);
            CudaPieceInt idx = new CudaPieceInt(col * batch, true, true);

            Cudalib.Meanpooling1D(tensor.CudaPtr, row, col, batch, output.CudaPtr); //, idx.CudaPtr);

            output.Print("output ", col * batch, false);
            //CudaPieceFloatDebug.Print("output idx ", idx, col * batch, false);
        }

        public class XRunner
        {
            public delegate void DelegateFunc();
            public DelegateFunc AddDelegate = null;

            public virtual void Add()
            {
                if(AddDelegate != null)
                {
                    AddDelegate();
                }
            }
        }

        public static XRunner TestFunc(List<int> mm)
        {
            List<List<int>> results = new List<List<int>>();
            results.Add(mm);

            XRunner x11 = new XRunner() ;//{ AddDelegate = () => return  };
            x11.AddDelegate = () =>
            {
                Console.WriteLine(results[0].Count);
            } ;
            return x11;
        }

        public static void TestTranspose()
        {
            DeviceType device = MathOperatorManager.SetDevice(5);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

            ComputationGraph cg = new ComputationGraph(rb);

            int dim1 = 10;
            int dim2 = 5;
            int dim3 = 3;
            int dim4 = 2;

            IntArgument[] shape = new IntArgument[4];
            shape[0] = new IntArgument("dim1", 10);
            shape[1] = new IntArgument("dim2", 5);
            shape[2] = new IntArgument("dim2", 3);
            shape[3] = new IntArgument("dim2", 2);

            NdArrayData data = new NdArrayData(shape, device);
            for(int i = 0; i < dim1 * dim2 * dim3 * dim4; i++)
            {
                    data.Output[i] = i ;
             
            }
            data.Output.SyncFromCPU();

            //data.Output.Print("original", 50, true);

            NdArrayData output = data.Transpose(new int[] { 0, 2, 3, 1}, cg.Session, rb);
            //cg.Step();

            NdArrayData back = output.Transpose(new int[] { 0, 3, 1, 2}, cg.Session, rb);
            cg.Step();

            output.Output.Print("transpose", dim1 * dim2 * dim3 * dim4, false);
            back.Output.Print("original", dim1 * dim2 * dim3 * dim4, false);
        }

        public static void TestMaxPooling2D()
        {
            int width = 8;
            int height = 8;

            int sx = 2;
            int stride = 2;

            int batch = 4;

            DeviceBehavior behavior = new DeviceBehavior(7);  
            CudaPieceFloat input = new CudaPieceFloat(width * height * batch, behavior.Device);
            for(int i = 0; i < width * height * batch; i++)
            {
                input[i] = i * 1.0f / 300.0f;
            }
            input.SyncFromCPU();

            CudaPieceFloat output1 = new CudaPieceFloat(4 * 4 * batch, behavior.Device);
            CudaPieceInt index1 = new CudaPieceInt(4 * 4 * batch, behavior.Device);

            Cudalib.MaxImagePooling(input.CudaPtr, batch, width, height, 1,
                                    index1.CudaPtr, sx, stride, output1.CudaPtr, 4, 4);
            output1.Print("pooling 1 results", 4 * 4 * batch, false);


            CudaPieceFloat output2 = new CudaPieceFloat(4 * 4 * batch, behavior.Device);
            behavior.Computelib.CuDNNMaxPooling2DForward(input, width, height, 1, batch, 
                                                sx, sx, 0, 0, stride, stride, 
                                                output2, 4, 4, 0, 1);

            output2.Print("pooling 2 results", 4 * 4 * batch, false);

        }

        public static void TestDotAndAdd()
        {
            DeviceType device = MathOperatorManager.SetDevice(5);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

            ComputationGraph cg = new ComputationGraph(rb);

            int dim1 = 10;
            int dim2 = 5;
            int dim3 = 3;
            int dim4 = 2;

            IntArgument[] shape = new IntArgument[4];
            shape[0] = new IntArgument("dim1", 10);
            shape[1] = new IntArgument("dim2", 5);
            shape[2] = new IntArgument("dim2", 3);
            shape[3] = new IntArgument("dim2", 2);

            NdArrayData data = new NdArrayData(shape, device);
            for(int i = 0; i < dim1 * dim2 * dim3 * dim4; i++)
            {
                    data.Output[i] = i ;
             
            }
            data.Output.SyncFromCPU();

            VectorData scale = new VectorData(dim1, device);
            VectorData bias = new VectorData(dim1, device);

            for(int i=0; i < dim1; i++)
            {
                scale.Output[i] =  i / 100.0f;
                bias.Output[i] = i * 100.0f;
            }

            scale.Output.SyncFromCPU();
            bias.Output.SyncFromCPU();
            //data.Output.Print("original", 50, true);

            NdArrayData output = data.DotAndAdd(scale, bias, cg.Session, rb);
            //cg.Step();

            //NdArrayData back = output.Transpose(new int[] { 0, 3, 1, 2}, cg.Session, rb);
            cg.Step();

            output.Output.Print("dotandadd", dim1 * dim2 * dim3 * dim4, false);
            //back.Output.Print("original", dim1 * dim2 * dim3 * dim4, false);
        }

        public interface IAdd
        {
            void Add(IAdd m);
        }

        public class Test_A : IAdd
        {
            int M { get; set; }
            public Test_A(int m)
            {
                M = m;
            }

            public void Add(Test_A mm)
            {
                Console.WriteLine("I am new test A");            
            }
            public void Add(IAdd m)
            {
                Console.WriteLine("I am test A");
            }

            public virtual void Print()
            {
                Console.WriteLine("IamtestA");
            }
        }

        public class Test_B : Test_A, IAdd
        {
            int N { get; set; }
            public Test_B(int m, int n) : base(m)
            {
                N = n;
            }

            public void Add(IAdd m)
            {
                Console.WriteLine("I am test B");
            }

            public override void Print()
            {
                Console.WriteLine("iamtestB");
            }
        }

        public class Test_C : Test_B
        {
            public Test_C(int m, int n) : base(m, n)
            {
            }

            public override void Print()
            {
                Console.WriteLine("iamtestC");
            }
        }

        public class Caller
        {
            public void Print<T>(T obj) where T : Test_A
            {
                obj.Print();
            }
        }

        // public class Caller<T> where T : Test_A
        // {
        //     public Caller()
        //     { }

        //     public void Test()
        //     {
        //         T.Add();
        //     }
        // } 

        public static int TestSum(params int[] p)
        {
            int sum = 0;
            foreach(int m in p)
            {
                sum = sum + m;
            }
            Console.WriteLine(sum);
            return sum;
        }

        public static void TestDropout()
        {
            DeviceType device = MathOperatorManager.SetDevice(1);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            int dim = 10;
            int H = 8;
            CudaPieceFloat x = new CudaPieceFloat(dim * H, device);
            for(int i = 0; i < dim * H; i++) x[i] = i + 1;
            x.SyncFromCPU();

            CudaPieceFloat y = new CudaPieceFloat(dim * H, device);
            CudaPieceFloat z = new CudaPieceFloat(dim * H, device);
            for(int i = 0; i < dim * H; i++) z[i] = i + 1;
            z.SyncFromCPU();

            IntPtr state = computeLib.RegisterDropout(1, 1, H, dim, 0.5f);
            Console.WriteLine(state);
            for(int i = 0; i < 1; i++)
            {
                x.Print("original", H * dim, true);
                //int reverseSize = computeLib.CuDNNDropoutSpaceSize(1, 1, H, dim);
                //Console.WriteLine("reverse size {0}", reverseSize);
                 // n, int c, int h, int w, float drop);(reverseSize);            
                computeLib.CuDNNDropoutForward(x, 1, 1, H, dim, y, 0.5f, state);
                y.Print("dropout forward", H * dim, true);

                z.Print("original z", H * dim, true);
                computeLib.CuDNNDropoutBackward(x, 1, 1, H, dim, z, 0.5f, state);
                z.Print("dropout backward", H * dim, true);
            }

        }

        public static void TestVecMul_Sqrt()
        {
            DeviceType device = MathOperatorManager.SetDevice(7);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            int dim = 10;
            int H = 8;
            CudaPieceFloat x = new CudaPieceFloat(dim * H, device);
            for(int i = 0; i < dim * H; i++) x[i] = i + 1;
            x.SyncFromCPU();

            CudaPieceFloat y = new CudaPieceFloat(dim * H, device);
            for(int i = 0; i < dim * H; i++) y[i] = i + 1;
            y.SyncFromCPU();

            CudaPieceFloat z = new CudaPieceFloat(dim * H, device);
            computeLib.CuDNNVectorMul(x, y, z, 0, 1, dim * H);
            z.Print("vec mul", dim * H, true);

            CudaPieceFloat q = new CudaPieceFloat(dim * H, device);
            computeLib.Sqrt(z, q, 0, 1, dim * H);
            q.Print("vec sqrt", dim * H, true);
        }

        public static void TestSger1()
        {   
            DeviceType device = MathOperatorManager.SetDevice(7);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            int dim = 10;
            int H = 8;
            CudaPieceFloat m = new CudaPieceFloat(dim * H, device);
            for(int i = 0; i < dim * H; i++) m[i] = i;
            m.SyncFromCPU();

            CudaPieceFloat x = new CudaPieceFloat(dim, device);
            x.Init(1);

            CudaPieceFloat y = new CudaPieceFloat(H, device);
            for(int i = 0; i < H; i++) y[i] = i + 1;
            y.SyncFromCPU();

            m.Print("original ", dim * H, true);
            computeLib.Sger(x, y, m, H, dim, 1);

            m.Print("update ", dim * H, true);
        }

        public static void TestSger2()
        {   
            DeviceType device = MathOperatorManager.SetDevice(7);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            int dim = 10;
            int H = 8;
            CudaPieceFloat m = new CudaPieceFloat(dim * H, device);
            for(int i = 0; i < dim * H; i++) m[i] = i;
            m.SyncFromCPU();

            CudaPieceFloat x = new CudaPieceFloat(dim, device);
            for(int i = 0; i < dim; i++) x[i] = i + 1;
            x.SyncFromCPU();

            CudaPieceFloat y = new CudaPieceFloat(H, device);
            y.Init(1);

            m.Print("original ", dim * H, true);
            computeLib.Sger(x, y, m, H, dim, 1);

            m.Print("update ", dim * H, true);
        }

        public static void TestBinaryRead()
        {
            using(BinaryReader reader = new BinaryReader(new FileStream(@"../text.bin", FileMode.Open, FileAccess.Read)))
            {
                //float m1 = reader.ReadSingle();
                //float m2 = reader.ReadSingle();
                //float m3 = reader.ReadSingle();
                //Console.WriteLine(m1);
                //Console.WriteLine(m2);
                //Console.WriteLine(m3);

                float[] mem = new float[3];

                Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * 3), 0, mem, 0, sizeof(float) * 3);
                for(int i=0;i<3;i++)
                {
                    Console.WriteLine(mem[i]);
                }
            }
        }

        public class VarBase
        {
            public int base1;
            public int base2;
        }

        public class VarBase2 : VarBase
        {
            public int base3;
            public int base4;
        }

        public interface VarAdd<DataType>
        {
           int FetchDataThreadSafe(DataType o1, DataType o2);
        } 

        public class VarAdd2 : VarAdd<VarBase2>
        {
            public int FetchDataThreadSafe(VarBase2 o1, VarBase2 o2)
            {
                Console.WriteLine(o1.base3 + o2.base4);
                return 100;
            }
        }

        public static void TestSoftmax()
        {
            DeviceType device = MathOperatorManager.SetDevice(2);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);


            CudaPieceFloat tmpC = new CudaPieceFloat(5, device);
            for(int i=0;i<5;i++)
            {
                tmpC[i] = (i + 1) * 0.1f;
            }
            tmpC.SyncFromCPU();
            
            computeLib.SoftMax(tmpC, tmpC, 5, 1, 1);

            tmpC.Print("softmax value ", 5, true);
            //InputImages.Output.SyncToCPU(0, tmp, 0, batchSize);
            //tmpC.SyncToCPU();
            //return tmpC.MemPtr;
        }


        public unsafe static void TestNCCLAllReduce()
        {
            int[] deviceIds = new int[] {1, 2, 3, 4};

            IntPtr Comm = IntPtr.Zero;
            fixed (int * pDevice = & deviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, 4);
            }
            
            int size = 150000000;
            CudaPieceFloat[] data = new CudaPieceFloat[4];
            Console.WriteLine("waiting here 0.");

            Parallel.For(0, 4, idx => 
            {
                DeviceType device = MathOperatorManager.SetDevice(deviceIds[idx]);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                data[idx] = new CudaPieceFloat(size, device);
                data[idx].Init(idx * 0.1f);
                Cudalib.AllReduce(data[idx].CudaPtr, size, idx, Comm);
            });

            // Thread[] ts = new Thread[4];
            // for(int i = 0; i < 4; i++)
            // {
            //     ts[i] = new Thread(() => Cudalib.AllReduce(data[i].CudaPtr, size, i, Comm)); 
            //     ts[i].Start();
            //     //ts[i] = t;
            // }
            // Console.WriteLine("waiting here 1.");
            // for(int i = 0; i < 4; i++)
            // {
            //     ts[i].Join();
            // }
            Console.WriteLine("waiting here 2.");

            for(int i = 0 ; i < 4; i++)
            {
                Cudalib.CudaSetDevice(deviceIds[i]);
                data[i].Print("device " + i.ToString(), 100, true);
            }
            //Console.ReadLine();
        }

        public unsafe static void TestNCCLAllReduce_SingleThread()
        {
            int[] deviceIds = new int[] {1, 2, 3, 4};

            IntPtr Comm = IntPtr.Zero;
            fixed (int * pDevice = & deviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, 4);
            }
            
            int size = 150000000;
            CudaPieceFloat[] data = new CudaPieceFloat[4];
            Console.WriteLine("waiting here 0.");

            Parallel.For(0, 4, idx => 
            {
                DeviceType device = MathOperatorManager.SetDevice(deviceIds[idx]);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                data[idx] = new CudaPieceFloat(size, device);
                data[idx].Init(idx * 0.1f);
                Cudalib.AllReduce(data[idx].CudaPtr, size, idx, Comm);
            });

            // Thread[] ts = new Thread[4];
            // for(int i = 0; i < 4; i++)
            // {
            //     ts[i] = new Thread(() => Cudalib.AllReduce(data[i].CudaPtr, size, i, Comm)); 
            //     ts[i].Start();
            //     //ts[i] = t;
            // }
            // Console.WriteLine("waiting here 1.");
            // for(int i = 0; i < 4; i++)
            // {
            //     ts[i].Join();
            // }
            Console.WriteLine("waiting here 2.");

            for(int i = 0 ; i < 4; i++)
            {
                Cudalib.CudaSetDevice(deviceIds[i]);
                data[i].Print("device " + i.ToString(), 100, true);
            }
            //Console.ReadLine();
        }

        public static void TestVectorNorm()
        {
            DeviceType device = MathOperatorManager.SetDevice(0);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            int batch = 2;
            int seq = 6;
            int len = 8;

            int dimNum = 4;
            float dis = (float)Math.Sqrt(len);

            int[] dims1 = new int[] {1, batch, seq, len };
            int[] dims2 = new int[] {1, batch, seq, 1 };

            CudaPieceFloat x = new CudaPieceFloat( len * seq * batch, device );
            for(int i = 0; i < len * seq * batch ; i ++)
            {
                x[i] = (i + 1) * 0.1f;
            }
            x.SyncFromCPU();

            CudaPieceFloat mean = new CudaPieceFloat(batch * seq, device);
            IntPtr meanState = computeLib.RegisteTensorReduce(0, dimNum, dims1, dims2);

            x.Print("x value", 100, true);
            computeLib.TensorReduceOp(x, mean, 0, 1, meanState);
            mean.Print("avg", 100, true);

            computeLib.TensorOp(0, dimNum, dims1, x, dims2, mean, dims1, x, 1, -1, 0);

            x.Print("b x", 100, true);

            CudaPieceFloat var = new CudaPieceFloat(batch * seq, device);
            IntPtr varState = computeLib.RegisteTensorReduce(1, dimNum, dims1, dims2);

            computeLib.TensorReduceOp(x, var, 0, 1, varState);

            var.Print("var x", 100, true);

            CudaPieceFloat var_1 = new CudaPieceFloat(batch * seq, device);
            computeLib.Reciprocal(var, var_1, 0, 1, batch * seq);

            var_1.Print("var_1 x", 100, true);            
            
            computeLib.TensorOp(1, dimNum, dims1, x, 
                                           dims2, var_1,
                                           dims1, x,
                                           1, dis, 0);
            x.Print("normalize x", 100, true);

            computeLib.UnRegisteTensorReduce(varState);
            computeLib.UnRegisteTensorReduce(meanState);
        }
	
	public static void TestSubArray()
	{
	    DeviceType device = MathOperatorManager.SetDevice(0);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
	    
            CudaPieceFloat x = new CudaPieceFloat(100, device);
	    x.Init(1);
	    
	    CudaPieceFloat y = x.SubArray(10, 5);
	    
	    x.Print("i am x", 100, true);
	}
	
    public static void TestTensorNorm()
    {
        DeviceType device = MathOperatorManager.SetDevice(0);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };


        int dimNum = 3;
        
        int batch = 16;
        int seq = 256;
        int len = 768;

        IntArgument[] dims = new IntArgument[dimNum];
        dims[0] = new IntArgument("len", len);
        dims[1] = new IntArgument("seq", seq);
        dims[2] = new IntArgument("batch", batch);

        NdArrayData input = new NdArrayData(device, dims);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            input.Output[i] = (i + 1) * 0.1f;
        }
        input.Output.SyncFromCPU();

        
        TensorNormRunner normRunner = new TensorNormRunner(input, 0, rb);
        NdArrayData output = normRunner.Output;
        
        input.Output.Print("raw vec", 100, true);
        normRunner.Forward();
        output.Output.Print("norm vec", 100, true);

        normRunner.CleanDeriv();
        Random random = new Random();
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            output.Deriv[i] = (float) (2.3f * i * Math.Sqrt(i) + Math.Tanh(i / 10.0f * 0.2f) * 0.1f * (i * 0.08f + Math.Sqrt(i) * 7.2f + 1));
                    //(new Shape(dims).DefaultSize - i) * 0.1f; // random.Next(20000) / 10000.0f - 1.0f;  //0.2f;// -(i + 1) * 0.1f;
        }
        output.Deriv.SyncFromCPU();

        output.Deriv.Print("deriv output vec",100, true);
        normRunner.Backward(false);
        input.Deriv.Print("deriv input vec", 100, true);
    }


    public static void TestTensorAdd()
    {
        DeviceType device = MathOperatorManager.SetDevice(2);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };


        int dimNum = 3;
        
        int batch = 3;
        int seq = 4;
        int len = 8;

        IntArgument[] dims = new IntArgument[dimNum];
        dims[0] = new IntArgument("len", len);
        dims[1] = new IntArgument("seq", seq);
        dims[2] = new IntArgument("batch", batch);

        NdArrayData inputA = new NdArrayData(device, dims);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            inputA.Output[i] = i + 1.0f; //(i + 1) * 0.1f;
        }
        inputA.Output.SyncFromCPU();

        IntArgument[] newDims = new IntArgument[dimNum];
        newDims[0] = new IntArgument("len", len);
        newDims[1] = new IntArgument("seq", 1);
        newDims[2] = new IntArgument("batch", batch);

        NdArrayData inputB = new NdArrayData(device, newDims);
        for(int i = 0; i < new Shape(newDims).DefaultSize; i++)
        {
            inputB.Output[i] = i + 1.0f; //(i + 1) * 0.1f;
        }
        inputB.Output.SyncFromCPU();


        TensorAddRunner addRunner = new TensorAddRunner(inputA, inputB, rb);
        NdArrayData output = addRunner.Output;
        
        inputA.Output.Print("raw input A", 100, true);
        inputB.Output.Print("raw input B", 100, true);
        addRunner.Forward();
        output.Output.Print("output vec", 100, true);

        addRunner.CleanDeriv();

        Random random = new Random();
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            output.Deriv[i] = i + 1.0f;
            //(float) (2.3f * i * Math.Sqrt(i) + Math.Tanh(i / 10.0f * 0.2f) * 0.1f * (i * 0.08f + Math.Sqrt(i) * 7.2f + 1));
                    //(new Shape(dims).DefaultSize - i) * 0.1f; // random.Next(20000) / 10000.0f - 1.0f;  //0.2f;// -(i + 1) * 0.1f;
        }
        output.Deriv.SyncFromCPU();

        output.Deriv.Print("deriv output vec",100, true);
        addRunner.Backward(false);
        inputA.Deriv.Print("deriv inputA vec", 100, true);
        inputB.Deriv.Print("deriv inputB vec", 100, true);
    }

    public static void TestTensorSlice()
    {
        DeviceType device = MathOperatorManager.SetDevice(3);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        int dimNum = 3;
        
        int batch = 3;
        int seq = 4;
        int len = 8;

        IntArgument[] dims = new IntArgument[dimNum];
        dims[0] = new IntArgument("len", len);
        dims[1] = new IntArgument("seq", seq);
        dims[2] = new IntArgument("batch", batch);

        NdArrayData input = new NdArrayData(device, dims);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            input.Output[i] = i + 1.0f; 
        }
        input.Output.SyncFromCPU();


        IntArgument[] offset = new IntArgument[dimNum];
        offset[0] = new IntArgument("len", 0);
        offset[1] = new IntArgument("seq", 0);
        offset[2] = new IntArgument("batch", 0);

        IntArgument[] newDims = new IntArgument[dimNum];
        newDims[0] = new IntArgument("len", 8);
        newDims[1] = new IntArgument("seq", 1);
        newDims[2] = new IntArgument("batch", 1);

        TensorSliceRunner sliceRunner = new TensorSliceRunner(input, offset, newDims, rb);
        NdArrayData output = sliceRunner.Output;

        input.Output.Print("raw input ", 100, true);
        sliceRunner.Forward();
        output.Output.Print("output vec", 100, true);

        sliceRunner.CleanDeriv();

        Random random = new Random();
        for(int i = 0; i < new Shape(newDims).DefaultSize; i++)
        {
            output.Deriv[i] = i + 1.0f;
            //(float) (2.3f * i * Math.Sqrt(i) + Math.Tanh(i / 10.0f * 0.2f) * 0.1f * (i * 0.08f + Math.Sqrt(i) * 7.2f + 1));
                    //(new Shape(dims).DefaultSize - i) * 0.1f; // random.Next(20000) / 10000.0f - 1.0f;  //0.2f;// -(i + 1) * 0.1f;
        }
        output.Deriv.SyncFromCPU();

        output.Deriv.Print("deriv output vec", 100, true);
        sliceRunner.Backward(false);
        input.Deriv.Print("deriv input vec", 100, true);
    }

    public static void TestBatchNorm2D()
    {
        DeviceType device = MathOperatorManager.SetDevice(0);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        Tensor4DData input = new Tensor4DData(1, 4, 2, 2, device);
        input.Output[0] = -1.2060f;
        input.Output[1] = -1.3660f;
        input.Output[2] =-1.3529f;
        input.Output[3] =-1.6523f;

        input.Output[4] = -1.2060f;
        input.Output[5] = -1.3660f;
        input.Output[6] =-1.3529f;
        input.Output[7] =-1.6523f;

        input.Output[8] = 1.5168f; 
        input.Output[9] =-1.2511f;
        input.Output[10] =0.7807f;
        input.Output[11] =2.3705f;

        input.Output[12] = 1.5168f; 
        input.Output[13] =-1.2511f;
        input.Output[14] =0.7807f;
        input.Output[15] =2.3705f;
        

        input.Output.SyncFromCPU();

        Tensor4DData scale = new Tensor4DData(1, 1, 2, 1, device);
        scale.Output.Init(1);

        Tensor4DData bias = new Tensor4DData(1, 1, 2, 1, device);
        bias.Output.Init(0);

        BatchNorm2DScaleBiasAfV2Runner v2Runner = new BatchNorm2DScaleBiasAfV2Runner(input, scale, bias, rb);
        v2Runner.Forward();

        v2Runner.running_mean.Print("running mean", 100, false);
        v2Runner.running_var.Print("running var", 100, false);

        v2Runner.Output.Output.Print("output", 100, false);
    }

    public unsafe static void TestSimpleTranspose()
    {
    	CudaPieceFloat data = new CudaPieceFloat(10, DeviceType.CPU);
    	for(int i = 0; i < 10; i++)
    		data[i] = i;
    	BasicMathlib.Transpose(data.CpuPtr, 5, 2);
    	data.Print("transpose", 10);
    }

    public static void TestTensorL2Norm()
    {
        DeviceType device = MathOperatorManager.SetDevice(5);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };


        int dimNum = 3;
        
        int batch = 2;
        int seq = 3;
        int len = 5;

        IntArgument[] dims = new IntArgument[dimNum];
        dims[0] = new IntArgument("len", len);
        dims[1] = new IntArgument("seq", seq);
        dims[2] = new IntArgument("batch", batch);

        NdArrayData input = new NdArrayData(device, dims);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            input.Output[i] = (i + 1) ;
        }
        input.Output.SyncFromCPU();

        
        TensorL2NormRunner normRunner = new TensorL2NormRunner(input, 0, rb);
        NdArrayData output = normRunner.Output;
        
        rb.Resource.InitResource();
        input.Output.Print("raw vec", 100, true);
        normRunner.Forward();
        output.Output.Print("norm vec", 100, true);

        normRunner.CleanDeriv();
        Random random = new Random();
        for(int i = 0; i < output.Length; i++)
        {
            output.Deriv[i] = (float) (2.3f * i * Math.Sqrt(i) + Math.Tanh(i / 10.0f * 0.2f) * 0.1f * (i * 0.08f + Math.Sqrt(i) * 7.2f + 1));
                    //(new Shape(dims).DefaultSize - i) * 0.1f; // random.Next(20000) / 10000.0f - 1.0f;  //0.2f;// -(i + 1) * 0.1f;
        }
        output.Deriv.SyncFromCPU();

        output.Deriv.Print("deriv output vec",100, true);
        normRunner.Backward(false);
        input.Deriv.Print("deriv input vec", 100, true);
    }

    public static void TestArgmax()
    {
        DeviceType device = MathOperatorManager.SetDevice(0);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        int dimNum = 2;
        
        //int batch = 2;
        int seq = 4;
        int len = 8;

        IntArgument[] dims = new IntArgument[dimNum];
        dims[0] = new IntArgument("len", len);
        dims[1] = new IntArgument("seq", seq);
        //dims[2] = new IntArgument("batch", batch);
        
        //IntArgument[] reduce_dims = new IntArgument[dimNum];
        //reduce_dims[0] = new IntArgument("len", len);
        //reduce_dims[1] = new IntArgument("seq", 1);
        //reduce_dims[2] = new IntArgument("batch", batch);
        
        Random random = new Random(110);
        NdArrayData input = new NdArrayData(device, dims);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            input.Output[i] = -(float)random.NextDouble();
        }
        input.Output.SyncFromCPU();


        TensorArgmaxRunner argmaxRunner = new TensorArgmaxRunner(input, 0, rb);
        NdArrayData output = argmaxRunner.Output;
        CudaPieceInt argmaxIndexes = argmaxRunner.Indices;
        
        rb.Resource.InitResource();


        //NdArrayData output = new NdArrayData(device, reduce_dims);

        //IntPtr argmaxState = computeLib.RegisteTensorReduce(3, dims.Length, new Shape(dims).RDimList, new Shape(reduce_dims).RDimList);

        //computeLib.TensorReduceOp(input.Output, output.Output, 0, 1, argmaxState);

        //CudaPieceInt argmaxIndexes = new CudaPieceInt(100, device);
        //computeLib.GetReduceIndices(argmaxState, argmaxIndexes);

        argmaxRunner.Forward();

        input.Output.Print("input", 100, true);
        output.Output.Print("output", 100, true);

        argmaxIndexes.Print("argmax index", 100, true);

        output.Deriv.Init(0.1f);

        //CudaPieceInt inDims = new CudaPieceInt(new Shape(reduce_dims).DimList, device);
        //CudaPieceInt outDims = new CudaPieceInt(new Shape(dims).DimIndexes, device);
        //computeLib.NDArrayMaxBackward(output.Deriv, argmaxIndexes, inDims, input.Deriv, outDims, 1, dims.Length, output.Length, 1.0f);

        argmaxRunner.Backward(false);

        input.Deriv.Print("input Deriv", 100, true);

    }

    public static void TestRandom()
    {
        DeviceType device = MathOperatorManager.SetDevice(7);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        
        int num = 32;
        CudaPieceFloat data = new CudaPieceFloat(num, device);

        for(int i = 0; i < 10; i++)
        {
            computeLib.GenerateRand(data, num);
            data.Print("random numbers", num, true);
        }
    }

    public static void TestGumbelSoftmax()
    {
        DeviceType device = MathOperatorManager.SetDevice(0);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        //, RateScheduler temp, bool hard, RunnerBehavior behavior)
        IntArgument[] dims = new IntArgument[2];
        dims[0] = new IntArgument("dim", 8);
        dims[1] = new IntArgument("batch", 4);
        //dims[2] = new IntArgument("batch", batch);
        
        //IntArgument[] reduce_dims = new IntArgument[dimNum];
        //reduce_dims[0] = new IntArgument("len", len);
        //reduce_dims[1] = new IntArgument("seq", 1);
        //reduce_dims[2] = new IntArgument("batch", batch);
        
        Random random = new Random(110);
        NdArrayData input = new NdArrayData(device, dims);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            input.Output[i] = -(float)random.NextDouble();
        }
        input.Output.SyncFromCPU();


        GumbelSoftmaxRunner runner = new GumbelSoftmaxRunner(input.ToHDB(), null, true, rb); 
        HiddenBatchData output = runner.Output;
        CudaPieceInt index = runner.maxIndex;

        rb.Resource.InitResource();
        rb.RunMode = DNNRunMode.Predict;
        
        runner.Forward();

        input.Output.Print("input", 100, true);
        output.Output.Data.Print("output", 100, true);

        index.Print("argmax index", 100, true);

    }

    public static void TestTensorExpansion()
    {
        DeviceType device = MathOperatorManager.SetDevice(6);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        //, RateScheduler temp, bool hard, RunnerBehavior behavior)
        IntArgument[] dims = new IntArgument[2];
        dims[0] = new IntArgument("dim", 5);
        dims[1] = new IntArgument("batch", 4);
        NdArrayData input = new NdArrayData(device, dims);

        Random random = new Random(110);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            input.Output[i] = -(float)random.NextDouble();
        }
        input.Output.SyncFromCPU();

        TensorExpansionRunner runner = new TensorExpansionRunner(input, 0, new IntArgument("beam", 3), rb);
        //GumbelSoftmaxRunner runner = new GumbelSoftmaxRunner(input.ToHDB(), null, true, rb); 
        //NdArrayData output = runner.Output;

        rb.Resource.InitResource();
        //rb.RunMode = DNNRunMode.Predict;
        
        runner.Forward();

        input.Output.Print("input", 100, true);
        runner.Output.Output.Print("output", 100, true);

        for(int i = 0; i < runner.Output.Length; i++)
        {
            runner.Output.Deriv[i] = -(float)random.NextDouble();
        }
        runner.Output.Deriv.SyncFromCPU();

        //index.Print("argmax index", 100, true);
        runner.Backward(true);

        runner.Output.Deriv.Print("output deriv", 100, true);
        input.Deriv.Print("input deriv", 100, true);
    }

    public static void TestTopK()
    {
        DeviceType device = MathOperatorManager.SetDevice(6);
        
        int array_size = 15;
        int k = 5;

        CudaPieceFloat data = new CudaPieceFloat(array_size, device);
        CudaPieceFloat output = new CudaPieceFloat(k, device);
        CudaPieceFloat indices = new CudaPieceFloat(k, device);

        Random random = new Random(110);
        for(int i = 0; i < array_size; i++)
        {
            data[i] = (float)(random.NextDouble() - 0.5f) * 2;
        }
        data.SyncFromCPU();

        //Cudalib.TopK(data.CudaPtr, array_size, k, output.CudaPtr);
        Cudalib.KLargestValueBatch(data.CudaPtr, 1, array_size, k, 1, output.CudaPtr, indices.CudaPtr);
        
        data.Print("input", 100, true);
        output.Print("output", 100, true);
    }


    public static void TestArgmaxK()
    {
        DeviceType device = MathOperatorManager.SetDevice(0);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        int dimNum = 2;
        
        //int batch = 2;
        int seq = 64; //64;
        int len = 1500000; //150000;

        int topk = 5; //5;
        IntArgument[] dims = new IntArgument[dimNum];
        dims[0] = new IntArgument("len", len);
        dims[1] = new IntArgument("seq", seq);
        //dims[2] = new IntArgument("batch", batch);
        
        //IntArgument[] reduce_dims = new IntArgument[dimNum];
        //reduce_dims[0] = new IntArgument("len", len);
        //reduce_dims[1] = new IntArgument("seq", 1);
        //reduce_dims[2] = new IntArgument("batch", batch);
        
        Random random = new Random(110);
        NdArrayData input = new NdArrayData(device, dims);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            input.Output[i] = (float)random.NextDouble();
        }
        input.Output.SyncFromCPU();
        
        //        public TensorArgmaxKV2Runner(NdArrayData input, int topK, int max_dim, RunnerBehavior behavior) : base(Structure.Empty, behavior)

        Stopwatch stopwatch = new Stopwatch();
        
        TensorArgmaxKV2Runner argmaxRunner = new TensorArgmaxKV2Runner(input, topk, 0, rb);
        NdArrayData output = argmaxRunner.Output;
        //CudaPieceInt argmaxIndexes = argmaxRunner.Indices;
        
        TensorArgmaxKRunner argmaxV1Runner = new TensorArgmaxKRunner(input, topk, rb);
        NdArrayData outputv1 = argmaxV1Runner.Output;
        //CudaPieceInt argmaxIndexes = argmaxV1Runner.Indices;
        
        TensorArgmaxKV3Runner argmaxV3Runner = new TensorArgmaxKV3Runner(input, topk, rb);
        NdArrayData outputv3 = argmaxV3Runner.Output;

        rb.Resource.InitResource();

        //NdArrayData output = new NdArrayData(device, reduce_dims);

        //IntPtr argmaxState = computeLib.RegisteTensorReduce(3, dims.Length, new Shape(dims).RDimList, new Shape(reduce_dims).RDimList);

        //computeLib.TensorReduceOp(input.Output, output.Output, 0, 1, argmaxState);

        //CudaPieceInt argmaxIndexes = new CudaPieceInt(100, device);
        //computeLib.GetReduceIndices(argmaxState, argmaxIndexes);

        input.Output.Print("input", 100, false);
        

        stopwatch.Restart();
        for(int i=0;i<10;i++)
        {
            argmaxRunner.Forward();
        }
        output.Output.Print("output v2", 100, false);
        //argmaxIndexes.Print("argmax index", 100, true);
        stopwatch.Stop();
        argmaxRunner.LocalIndices.Print("local indices", 100, false);
        argmaxRunner.GlobalIndices.Print("global indices", 100, false);
        // Write result.
        Console.WriteLine("\n\nargmax method v2 Time elapsed: {0}", stopwatch.Elapsed);

        stopwatch.Restart();
        for(int i=0;i<10;i++)
        {
            argmaxV1Runner.Forward();
        }
        outputv1.Output.Print("output v1", 100, false);
        stopwatch.Stop();
        argmaxV1Runner.Indices.Print("local indices", 100, false);
        argmaxV1Runner.Indices_v2.Print("global indices", 100, false);
        Console.WriteLine("\n\nargmax method v1 Time elapsed: {0}", stopwatch.Elapsed);

        stopwatch.Restart();
        for(int i=0;i<10;i++)
        {
            argmaxV3Runner.Forward();
        }
        outputv3.Output.Print("output v3", 100, false);
        stopwatch.Stop();
        argmaxV3Runner.Indices.Print("local indices", 100, false);
        argmaxV3Runner.Indices_v2.Print("global indices", 100, false);
        Console.WriteLine("\n\nargmax method v3 Time elapsed: {0}", stopwatch.Elapsed);

        output.Deriv.Init(0.1f);

        //CudaPieceInt inDims = new CudaPieceInt(new Shape(reduce_dims).DimList, device);
        //CudaPieceInt outDims = new CudaPieceInt(new Shape(dims).DimIndexes, device);
        //computeLib.NDArrayMaxBackward(output.Deriv, argmaxIndexes, inDims, input.Deriv, outDims, 1, dims.Length, output.Length, 1.0f);

        argmaxRunner.Backward(false);

        input.Deriv.Print("input Deriv", 100, false);
    }

    public static void TestTensorCat()
    {
        DeviceType device = MathOperatorManager.SetDevice(0);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        //, RateScheduler temp, bool hard, RunnerBehavior behavior)
        IntArgument[] dims = new IntArgument[3];
        dims[0] = new IntArgument("dim", 4);
        dims[1] = new IntArgument("chunk", 3);
        dims[2] = new IntArgument("batch", 2);

        NdArrayData input1 = new NdArrayData(device, dims);
        NdArrayData input2 = new NdArrayData(device, dims);

        Random random = new Random(110);
        for(int i = 0; i < new Shape(dims).DefaultSize; i++)
        {
            input1.Output[i] = i * 0.2f;
            input2.Output[i] = i * 0.2f + 0.1f;
        }
        input1.Output.SyncFromCPU();
        input2.Output.SyncFromCPU();

        TensorCatRunner runner = new TensorCatRunner((new NdArrayData[2] { input1, input2 }).ToList(), 1, rb);
        NdArrayData output = runner.Output;

        rb.Resource.InitResource();
        runner.Forward();

        input1.Output.Print("input1", 100, false);
        input2.Output.Print("input2", 100, false);

        output.Output.Print("output", 100, false);

        for(int i = 0; i < new Shape(dims).DefaultSize * 2; i++)
        {
            output.Deriv[i] = i * 0.1f;
        }
        output.Deriv.SyncFromCPU();

        runner.Backward(false);

        output.Deriv.Print("output deriv", 100, false);

        input1.Deriv.Print("input1 deriv", 100, false);
        input2.Deriv.Print("input2 deriv", 100, false);

    }

    public static void TestFillupTensor()
    {
        DeviceType device = MathOperatorManager.SetDevice(0);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        //, RateScheduler temp, bool hard, RunnerBehavior behavior)
        IntArgument _dim = new IntArgument("dim", 4);
        IntArgument _chunk = new IntArgument("chunk", 3);
        IntArgument _batch = new IntArgument("batch", 2);
        IntArgument _pad = new IntArgument("pad", 1);

        NdArrayData memory = new NdArrayData(device, _dim, _chunk, _batch);
        NdArrayData data = new NdArrayData(device, _dim, _batch);

        Random random = new Random(110);
        for(int i = 0; i < memory.Length; i++)
        {
            memory.Output[i] = (i + 1);
        }
        memory.Output.SyncFromCPU();

        for(int i = 0; i < data.Length; i++)
        {
            data.Output[i] = (i + 1);
        }
        data.Output.SyncFromCPU();

        CudaPieceInt pos = new CudaPieceInt(2, device);
        pos[0] = 1;
        pos[1] = 2;
        pos.SyncFromCPU();

        FillupTensorRunner runner = new FillupTensorRunner(pos, memory, data, rb);

        memory.Output.Print("org memory", 100, false);
        data.Output.Print("org data", 100, false);

        rb.Resource.InitResource();
        runner.Forward();

        memory.Output.Print("reset memory", 100, false);

        for(int i = 0; i < memory.Length; i++)
        {
            memory.Deriv[i] = (i + 1);
        }
        memory.Deriv.SyncFromCPU();

        memory.Deriv.Print("org memory deriv", 100, false);
        runner.Backward(false);

        memory.Deriv.Print("memory deriv", 100, false);
        data.Deriv.Print("data deriv",100, false);

    }


    public static void TestExtractTensor()
    {
        DeviceType device = MathOperatorManager.SetDevice(0);
        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
        RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

        //, RateScheduler temp, bool hard, RunnerBehavior behavior)
        IntArgument _dim = new IntArgument("dim", 4);
        IntArgument _chunk = new IntArgument("chunk", 3);
        IntArgument _batch = new IntArgument("batch", 2);
        IntArgument _pad = new IntArgument("pad", 1);

        NdArrayData memory = new NdArrayData(device, _dim, _chunk, _batch);
        
        Random random = new Random(110);
        for(int i = 0; i < memory.Length; i++)
        {
            memory.Output[i] = (i + 1);
        }
        memory.Output.SyncFromCPU();

        CudaPieceInt pos = new CudaPieceInt(2, device);
        pos[0] = 1;
        pos[1] = 2;
        pos.SyncFromCPU();

        //CudaPieceInt idxes, NdArrayData memory, RunnerBehavior behavior) 
        ExtractupTensorRunner runner = new ExtractupTensorRunner(pos, memory, rb);
        rb.Resource.InitResource();

        memory.Output.Print("org memory", 100, false);

        runner.Forward();
        NdArrayData data = runner.Data;

        data.Output.Print("extract data", 100, false);


        for(int i = 0; i < data.Length; i++)
        {
            data.Deriv[i] = (i + 1);
        }
        data.Deriv.SyncFromCPU();

        data.Deriv.Print("org data deriv", 100, false);
        memory.Deriv.Print("org memory deriv", 100, false);
        
        runner.Backward(false);

        memory.Deriv.Print("memory deriv", 100, false);
        data.Deriv.Print("data deriv",100, false);

    }

        public static void TestL2Norm()
        {
            DeviceType device = MathOperatorManager.SetDevice(5);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior rb = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

            Session cs = new Session(rb);

            int dimNum = 3;
        
            int batch = 2;
            int seq = 3;
            int len = 100;

            IntArgument[] dims = new IntArgument[dimNum];
            dims[0] = new IntArgument("len", len);
            dims[1] = new IntArgument("seq", seq);
            dims[2] = new IntArgument("batch", batch);

            NdArrayData input = new NdArrayData(device, dims);
            for(int i = 0; i < new Shape(dims).DefaultSize; i++)
            {
                input.Output[i] = (i + 1) - 3;
            }
            input.Output.SyncFromCPU();

            NdArrayData norm = cs.L2Normalized(input, 0);

            rb.Resource.InitResource();
            cs.Forward();

            input.Output.Print("input data", 100, false);
            norm.Output.Print("normalized data", 100, false);
        }


        public static void Main(string[] args)
        {
            TestL2Norm();
            return;

            TestTensorL2Norm();
            return;

            TestTensorSlice();
            return;
            
            TestExtractTensor();
            return;
            
            TestFillupTensor();
            return;

            


            TestDropout();
            return;
            
            TestTensorCat();
            return;

            TestArgmaxK();
            return;

            TestTopK();
            return;

            TestTensorExpansion();
            return;

            TestGumbelSoftmax();
            return;

            TestRandom();
            return;

            TestTensorL2Norm();
            return;

        	TestSimpleTranspose();
        	return;


//<<<<<<< Updated upstream
            TestBatchNorm2D();
            return;

            TestTensorSlice();
            return;
//=======
            //TestTensorSlice();
            //return;
//>>>>>>> Stashed changes

            //TestTensorAdd();
            //return;

            //TestBinaryRead();
            //return;
            
            //TestTensorNorm();
            //return;

            //TestCudaElementwiseProduct();
            //return;

            //TestVectorNorm();
            //return;

            TestNCCLAllReduce();
            return;

    	    TestSubArray();
    	    return;


            //TestCudaElementwiseProduct();
            //return;            

            TestSoftmax();
            return;

            int sizePerImg = 224 * 224 * 3 * sizeof(float);
            long pos = 0;
            for(int i = 0; i < 1281167; i++)
            {
                if(i % 10000 == 0)
                {
                    Console.WriteLine(pos);
                }
                pos = pos + sizePerImg;
            }  
            Console.WriteLine(pos);
            return;

            DataRandomShuffling shuffle = new DataRandomShuffling(100);
            shuffle.Shuffling();
            foreach(int i in shuffle.Idxs)
            {
                Console.WriteLine(i);
            }
            return;

            VarAdd2 madd2 = new VarAdd2();
            VarBase2 mbase2_1 = new VarBase2() { base3 = 10, base4 = 20};
            VarBase2 mbase2_2 = new VarBase2() { base3 = 30, base4 = 40};
            
            madd2.FetchDataThreadSafe(mbase2_1, mbase2_2);
            madd2.FetchDataThreadSafe(mbase2_2, mbase2_1);
            return;

            TestBinaryRead();
            return;

            TestSger1();
            TestSger2();
            return;

            TestVecMul_Sqrt();
            return;

            TestDropout();
            return;

            TestMaxPooling2D();
            return;

            int[] test_mmm = new int[3];
            int[] new_mmm = test_mmm.Concate(10);
            foreach(int mmm33 in new_mmm)
            {
                Console.WriteLine(mmm33);
            }
            return;

            // Test_C test__b1 = new Test_C(100, 10);
            // Caller test_caller = new Caller();
            // test_caller.Print(test__b1);
            // return;

            // TestSum(new int[]{ 10,20,30,40});
            // return;

            //Caller<Test_B> test_caller = new Caller<Test_B>();
            //test_caller.Test();
            IAdd test_a1 = new Test_A(100);
            test_a1.Add(test_a1);

            Test_A test_b1 = new Test_B(100,200);
            test_b1.Add(test_b1);

            IAdd test_a2 = new Test_B(100, 200);
            test_a2.Add(test_a2);
            return;

            TestDotAndAdd();
            return;


            TestTranspose();
            return;

            List<int> mmm = new List<int>();
            XRunner x111 = TestFunc(mmm);

            x111.Add();

            mmm.Add(100);
            x111.Add();
            mmm.Add(100);
            mmm.Add(100);
            mmm.Add(100);
            x111.Add();
            //int a11 = 10;
            //int a232 = 30;
            //XRunner x11 = new XRunner() ;//{ AddDelegate = () => return  };
            //x11.AddDelegate = () =>
            //{
            //    Console.WriteLine(a11 + a232);
            //} ;
            //x11.Add();

            //a11 = 203234;
            //x11.Add();
            return;

            TestMaxPooling();
            return;

            TestCudaElementwiseProduct();
            return;

            TestCuSum();
            return;

            TestCuSparse();
            return;

            TestSortArray();
            return;

            TestArray();

            //TestDictionary();
            
            Cudalib.CudaSetDevice(4);

            Console.WriteLine("hello world.");
            //int m = Sselib.AddTest(100, 100);
            //int n = Sselib.MultiTest(200, 200);

            //TestAdd_Vector();
            TestCudaVectorScale();
            TestCudaElementwiseProduct();
            

            float[] x = new float[10];
            for (int i = 0; i < 10;i++)
            {
                x[i] = i;
            }

            unsafe
            {

                fixed (float* pArray = &x[0])
                {
                    //IntPtr intPtr = new IntPtr((void*)pArray);
                    float m = SSElib.SSE_SumAbs(pArray, 5);
                    float n = SSElib.SSE_SumSq(pArray, 5);

                    Console.WriteLine(m.ToString());
                    Console.WriteLine(n.ToString());
                }
            }


        }
    }
}
