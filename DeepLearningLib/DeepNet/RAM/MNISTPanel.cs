using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;


namespace BigLearn.DeepNet.RAM
{
        public class MNISTUtil
        {
            static int ReadInt32(BinaryReader reader)
            {
                byte[] a = reader.ReadBytes(4);
                Array.Reverse(a);
                return BitConverter.ToInt32(a, 0);
            }

            public static byte[] ExtractLabels(string labelFile)
            {
                using (BinaryReader labelReader = new BinaryReader(new FileStream(labelFile, FileMode.Open, FileAccess.Read)))
                {
                    int magicNum = ReadInt32(labelReader);
                    int labelNumber = ReadInt32(labelReader);

                    byte[] labelArray = new byte[labelNumber];
                    for(int i=0;i<labelNumber;i++)
                    {
                        labelArray[i] = labelReader.ReadByte();
                    }
                    return labelArray;
                }
            }

            public static Tuple<int, int, int, int, byte[]> ExtractImages(string imageFile)
            {
                using (BinaryReader imageReader = new BinaryReader(new FileStream(imageFile, FileMode.Open, FileAccess.Read)))
                {
                    int magicNumber = ReadInt32(imageReader);
                    int imageNum = ReadInt32(imageReader);
                    int width = ReadInt32(imageReader);
                    int height = ReadInt32(imageReader); 
                    
                    byte[] imageData =  new byte[imageNum * width * height];
                    for(int i=0;i< imageNum * width * height;i++)
                    {
                        imageData[i] = imageReader.ReadByte();
                    }
                    return new Tuple<int, int, int, int, byte[]>(imageNum, width, height, 1, imageData);
                }
            }

            public static ImageFrame ExtractFrame(string imgfile, string labelfile)
            {
                byte[] label = ExtractLabels(labelfile);
                Tuple<int, int, int, int, byte[]> image = ExtractImages(imgfile);
                return new ImageFrame(image, label);
            }
        }


        public class MNISTPanel 
        {
            public static ImageFrame TrainFrame = null;
            public static ImageFrame DevFrame = null;
            public static ImageFrame TestFrame = null;

            public static void Init(string dataDir)
            {
                TestFrame = MNISTUtil.ExtractFrame(dataDir + @"/t10k-images-idx3-ubyte", dataDir + @"/t10k-labels-idx1-ubyte");
                ImageFrame allFrame = MNISTUtil.ExtractFrame(dataDir + @"/train-images-idx3-ubyte", dataDir + @"/train-labels-idx1-ubyte");
                Tuple<ImageFrame, ImageFrame> parts = allFrame.RandomSplit(0.9f);
                TrainFrame = parts.Item1;
                DevFrame = parts.Item2;
            }
        }

        
}
