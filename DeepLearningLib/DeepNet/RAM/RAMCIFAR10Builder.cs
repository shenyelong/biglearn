// design a recurrent attention model for binary classification task.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.RAM
{
	// build recurrent attention model.
    public class RAMCIFAR10Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("BERT-LAYER", new ParameterArgument("-1", "pre-trained bert layers."));

                Argument.Add("RECURRENT-LAYER",new ParameterArgument("1", "frozen layers."));

                // sometimes it is really hard to find the real interesting things.
                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "reward discount."));         
                Argument.Add("ENTROPY", new ParameterArgument("0:0.002", "entropy schedule."));   

                Argument.Add("STEP", new ParameterArgument("5", "reader step"));
                Argument.Add("K", new ParameterArgument("10", "reader k around"));
                Argument.Add("CATEGORY", new ParameterArgument("2", "classification category"));
                Argument.Add("BASELINE", new ParameterArgument("0","moving average baseline"));
                Argument.Add("ATTSCALE", new ParameterArgument("1", "attention scalar."));
                Argument.Add("TYPE", new ParameterArgument("0", "0: ram; 1 : reasonet; "));
                Argument.Add("RECURRENT", new ParameterArgument("0", "0:gru; 1:rru;"));

                Argument.Add("MAX-ACTIVE-FEATURE", new ParameterArgument("10", "maximum active feature."));
                Argument.Add("FEATURE-DIM", new ParameterArgument("100", "feature dimension."));
                Argument.Add("HEAD-NUM", new ParameterArgument("8","head number."));

                Argument.Add("TEMP", new ParameterArgument("0:0.2", "temp schedule."));   
                Argument.Add("HARD-GUMBEL", new ParameterArgument("0","0:soft gumbel; 1:hard gumbel;"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }

            public static int BERT_LAYER { get { return int.Parse(Argument["BERT-LAYER"].Value); } }

            public static int Recurrent_Layer { get { return int.Parse(Argument["RECURRENT-LAYER"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static List<Tuple<int, float>> Entropy
            {
                get
                {
                    return Argument["ENTROPY"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static List<Tuple<int, float>> Temp
            {
                get
                {
                    return Argument["TEMP"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static int Step { get { return int.Parse(Argument["STEP"].Value); } }
            public static int K { get { return int.Parse(Argument["K"].Value); } }

            public static int Category { get { return int.Parse(Argument["CATEGORY"].Value); } }

            public static float Baseline { get { return float.Parse(Argument["BASELINE"].Value); } }
            public static float AttScale { get { return float.Parse(Argument["ATTSCALE"].Value); } }
            
            public static int Type { get { return int.Parse(Argument["TYPE"].Value); } }

            public static int Recurrent { get { return int.Parse(Argument["RECURRENT"].Value); } }

            // maximum number of active feature.
            public static int MaxActiveFeature { get { return int.Parse(Argument["MAX-ACTIVE-FEATURE"].Value); } }
            // feature dimension.
            public static int FeatureDim { get { return int.Parse(Argument["FEATURE-DIM"].Value); } }
            public static int HeadNum { get { return int.Parse(Argument["HEAD-NUM"].Value); } }

            public static bool HardGumbel { get { return int.Parse(Argument["HARD-GUMBEL"].Value) > 0; }}
               
        }

        // ram classifier.
        public override BuilderType Type { get { return BuilderType.RAM_CIFAR10_CLASSIFIER; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class RAMImageClassifier : ComputationGraph
        {
        	public class RAMModel : CompositeNNStructure
	        {
                // feature image extraction 1.
                public TensorConvStructure Conv1 = null;

                // feature image extraction 2.
                public TensorConvStructure block1_1 = null;
                public TensorConvStructure block1_2 = null;

                public TensorConvStructure block2_1 = null;
                public TensorConvStructure block2_2 = null;

                public TensorConvStructure block3_1 = null;
                public TensorConvStructure block3_2 = null;

                public TensorConvStructure block4_1 = null;
                public TensorConvStructure block4_2 = null;
                public TensorConvStructure block4_3 = null;

                public TensorConvStructure block5_1 = null;
                public TensorConvStructure block5_2 = null;

                public TensorConvStructure block6_1 = null;
                public TensorConvStructure block6_2 = null;

                public TensorConvStructure block7_1 = null;
                public TensorConvStructure block7_2 = null;
                public TensorConvStructure block7_3 = null;

                public TensorConvStructure block8_1 = null;
                public TensorConvStructure block8_2 = null;

                public TensorConvStructure block9_1 = null;
                public TensorConvStructure block9_2 = null;


                public TensorConvStructure block10_1 = null;
                public TensorConvStructure block10_2 = null;
                public TensorConvStructure block10_3 = null;

                // position embedding for feature bias.
                public EmbedStructure PosEmbed = null;

                public List<EmbedStructure> InitStates { get; set; }

	        	public List<GRUCell> GruCells { get; set; }

	        	public LayerStructure Classifier { get; set; }
            	public LayerStructure T { get; set; }
				
                public LayerStructure QProject { get; set; }
                public LayerStructure KProject { get; set; }
                public LayerStructure VProject { get; set; }

                public int StateDim { get; set; }
	            public int Layer { get; set; }

                public RAMModel(int stateDim, int layer, DeviceType device)
	            {
                    StateDim = stateDim;
                    Layer = layer;

                    Conv1 = AddLayer(new TensorConvStructure(3, 3, 3, 16, device));

                    block1_1 = AddLayer(new TensorConvStructure(3, 3, 16, 16, device));
                    block1_2 = AddLayer(new TensorConvStructure(3, 3, 16, 16, device));

                    block2_1 = AddLayer(new TensorConvStructure(3, 3, 16, 16, device));
                    block2_2 = AddLayer(new TensorConvStructure(3, 3, 16, 16, device));

                    block3_1 = AddLayer(new TensorConvStructure(3, 3, 16, 16, device));
                    block3_2 = AddLayer(new TensorConvStructure(3, 3, 16, 16, device));

                    block4_1 = AddLayer(new TensorConvStructure(3, 3, 16, 32, device));
                    block4_2 = AddLayer(new TensorConvStructure(3, 3, 32, 32, device));
                    block4_3 = AddLayer(new TensorConvStructure(1, 1, 16, 32, device));

                    block5_1 = AddLayer(new TensorConvStructure(3, 3, 32, 32, device));
                    block5_2 = AddLayer(new TensorConvStructure(3, 3, 32, 32, device));

                    block6_1 = AddLayer(new TensorConvStructure(3, 3, 32, 32, device));
                    block6_2 = AddLayer(new TensorConvStructure(3, 3, 32, 32, device));

                    block7_1 = AddLayer(new TensorConvStructure(3, 3, 32, 64, device));
                    block7_2 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));
                    block7_3 = AddLayer(new TensorConvStructure(1, 1, 32, 64, device));

                    block8_1 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));
                    block8_2 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));

                    block9_1 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));
                    block9_2 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));


                    block10_1 = AddLayer(new TensorConvStructure(3, 3, 64, 128, device));
                    block10_2 = AddLayer(new TensorConvStructure(3, 3, 128, 128, device));
                    block10_3 = AddLayer(new TensorConvStructure(1, 1, 64, 128, device));

                    // 16 + 9 + 4 + 1 --> totally 30 positions.
                    int PosNum = 26;
                    PosEmbed = AddLayer(new EmbedStructure(PosNum, StateDim, -0.034f, 0.034f, device));

                    // better to be top layer.
                    QProject = AddLayer(new LayerStructure(StateDim, StateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                    KProject = AddLayer(new LayerStructure(StateDim, StateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                    VProject = AddLayer(new LayerStructure(StateDim, StateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

                    InitStates = new List<EmbedStructure>();
                    for(int i = 0; i < layer; i++)
                    {
                        InitStates.Add(AddLayer(new EmbedStructure(1, StateDim, -0.034f, 0.034f, device)));
                    }

                    GruCells = new List<GRUCell>();
                    for(int i = 0; i < layer; i++)
                    {
                        GruCells.Add(AddLayer(new GRUCell(StateDim, StateDim, device)));
                    }

	            	// two class classification. 
	                Classifier = AddLayer(new LayerStructure(StateDim, BuilderParameters.Category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

	                T = AddLayer(new LayerStructure(StateDim, 2, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));                    
                }
	        }

            public Tensor4DData Build3_3ResBlock(Tensor4DData data, TensorConvStructure conv1, TensorConvStructure conv2)
            {
                TensorConvRunner c1Runner = new TensorConvRunner(conv1, data, 1, 1, A_Func.Rectified, Behavior);
                AddRunner(c1Runner);

                TensorConvRunner c2Runner = new TensorConvRunner(conv2, c1Runner.Output, 1, 1, A_Func.Linear, Behavior);
                AddRunner(c2Runner);

                return data.Add(c2Runner.Output, Session, Behavior).Act(A_Func.Rectified, Session, Behavior);
            }

            public Tensor4DData Build3_3ResBlock_2(Tensor4DData data, TensorConvStructure conv1, TensorConvStructure conv2, TensorConvStructure conv3)
            {
                TensorConvRunner c1Runner = new TensorConvRunner(conv1, data, 1, 2, A_Func.Rectified, Behavior);
                AddRunner(c1Runner);

                TensorConvRunner c2Runner = new TensorConvRunner(conv2, c1Runner.Output, 1, 1, A_Func.Linear, Behavior);
                AddRunner(c2Runner);

                TensorConvRunner c3Runner = new TensorConvRunner(conv3, data, 0, 2, A_Func.Linear, Behavior);
                AddRunner(c3Runner);

                return c3Runner.Output.Add(c2Runner.Output, Session, Behavior).Act(A_Func.Rectified, Session, Behavior);
            }



            public void BuildRAM(int width, int height, int channel, int batchSize, int head, int recursiveStep)
            {
                BatchRunner = new ImageDataRunner(width, height, channel, batchSize, BuilderParameters.Category, Behavior);
                AddDataRunner(BatchRunner);

                SparseVectorData Labels = BatchRunner.Label;
                Tensor4DData image = BatchRunner.Image;
                
                // 32 * 32 * 3.
                TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, image, 1, 1, A_Func.Rectified, Behavior);
                AddRunner(conv1Runner);
                Tensor4DData b0 = conv1Runner.Output;
                // 32 * 32 * 16.
                Tensor4DData b1 = Build3_3ResBlock(b0, Model.block1_1, Model.block1_2);
                b1 = b1.Dropout(0.3f, Session, Behavior);

                // 32 * 32 * 16.
                Tensor4DData b2 = Build3_3ResBlock(b1, Model.block2_1, Model.block2_2);
                b2 = b2.Dropout(0.3f, Session, Behavior);
                // 32 * 32 * 16.
                //Tensor4DData b3 = Build3_3ResBlock(b2, Model.block3_1, Model.block3_2);
                // 32 * 32 * 16.
                Tensor4DData b4 = Build3_3ResBlock_2(b1, Model.block4_1, Model.block4_2, Model.block4_3);
                b4 = b4.Dropout(0.3f, Session, Behavior);
                // 16 * 16 * 32.
                Tensor4DData b5 = Build3_3ResBlock(b4, Model.block5_1, Model.block5_2);
                b5 = b5.Dropout(0.3f, Session, Behavior);
                // 16 * 16 * 32.
                //Tensor4DData b6 = Build3_3ResBlock(b5, Model.block6_1, Model.block6_2);
                // 16 * 16 * 32.
                Tensor4DData b7 = Build3_3ResBlock_2(b5, Model.block7_1, Model.block7_2, Model.block7_3);
                b7 = b7.Dropout(0.3f, Session, Behavior);
                // 8 * 8 * 64.
                //Tensor4DData b8 = Build3_3ResBlock(b7, Model.block8_1, Model.block8_2);
                // 8 * 8 * 64.
                //Tensor4DData b9 = Build3_3ResBlock(b8, Model.block9_1, Model.block9_2);
                // 8 * 8 * 64.
                Tensor4DData b10 = Build3_3ResBlock_2(b7, Model.block10_1, Model.block10_2, Model.block10_3);
                b10 = b10.Dropout(0.3f, Session, Behavior);
                // 4 * 4 * 128.
                
                // 4 * 4 * 128.
                Tensor4DData feaMap = b10;

                // build feature block (4*4).
                TensorMaxPooling2DRunner fea1_Runner = new TensorMaxPooling2DRunner(feaMap, 4, 4, Behavior);
                AddRunner(fea1_Runner);
                
                // build feature block (2*2).
                TensorMaxPooling2DRunner fea2_Runner = new TensorMaxPooling2DRunner(feaMap, 2, 1, Behavior);
                AddRunner(fea2_Runner);
                
                NdArrayData fea1block = fea1_Runner.Output.ToND().Transpose(new int[] { 2, 3, 0, 1 }, Session, Behavior).Reshape(feaMap.DepthArg, feaMap.BatchArg, new IntArgument("fea-dim", 1));
                NdArrayData fea2block = fea2_Runner.Output.ToND().Transpose(new int[] { 2, 3, 0, 1 }, Session, Behavior).Reshape(feaMap.DepthArg, feaMap.BatchArg, new IntArgument("fea-dim", 9));
                NdArrayData fea3block = feaMap.ToND().Transpose(new int[] { 2, 3, 0, 1 }, Session, Behavior).Reshape(feaMap.DepthArg, feaMap.BatchArg, new IntArgument("fea-dim", 16));

                NdArrayData feablock = fea1block.Concate(fea2block, fea3block, Session, Behavior).Transpose(new int[] {0, 2, 1}, Session, Behavior);
                
                //DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions) 
                NdArrayData posEmd = new NdArrayData(Behavior.Device, Model.PosEmbed.Embedding, Model.PosEmbed.EmbeddingGrad, feablock.Dimensions[0], feablock.Dimensions[1], new IntArgument("batch-size", 1));

                NdArrayData feas = feablock.AddExpand(posEmd, Session, Behavior);

                //CudaPieceInt TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);

                //CudaPieceFloat Label = BatchRunner.Labels;
                //Label = new CudaPieceFloat(batchSize, Behavior.Device);

                Logger.WriteLog("Build CG in data sampling.");

                // embedding dimension.
                IntArgument emd_dim = feas.Dimensions[0];
                IntArgument active_dim = feas.Dimensions[1];
                IntArgument batch_size = feas.Dimensions[2];

                IntArgument headdim = new IntArgument("head-dim", emd_dim.Default / head);
                IntArgument headnum = new IntArgument("head-num", head);

                IntArgument headbatch = new IntArgument("head-batch", head * batch_size.Default);

                //DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions) 
                //NdArrayData W_tensor = new NdArrayData(Behavior.Device, fea_embed.Output.Data, fea_embed.Deriv.Data, emd_dim, active_dim, batch_size);

                NdArrayData kfeature = feas.FNN(Model.KProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
                NdArrayData vfeature = feas.FNN(Model.VProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;


                NdArrayData initZero = new NdArrayData(Behavior.Device, false, emd_dim, batch_size);  
                initZero.Output.Init(0); 
                
                List<HiddenBatchData> si_list = new List<HiddenBatchData>();
                for(int i = 0; i < Model.Layer; i++)
                {
                    HiddenBatchData initState = new HiddenBatchData(1, Model.StateDim, Model.InitStates[i].Embedding, Model.InitStates[i].EmbeddingGrad, Model.InitStates[i].DeviceType);
                    HiddenBatchData si = initZero.AddExpand(initState.ToND(), Session, Behavior).ToHDB(); 
                    si_list.Add(si);
                }

                RateScheduler TempRate = new RateScheduler(GlobalUpdateStep, BuilderParameters.Temp);  

                for(int i = 0; i < recursiveStep; i++)
                {
                    //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                    //AddRunner(maskAttRunner);
                    
                    //MatrixConcateRunner concateRunner = new MatrixConcateRunner(si_list.Select(h => h.ToMD()).ToList(), Behavior);
                    //HiddenBatchData si_q = concateRunner.Output.ToHDB().FNN(Model.QProject, Session, Behavior);

                    HiddenBatchData si_q = si_list[Model.Layer - 1].FNN(Model.QProject, Session, Behavior);

                    NdArrayData qx = si_q.ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batch_size);

                    // softmax and sampling. 
                    NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);

                    //.Softmax(Session, Behavior);
                    HiddenBatchData attProb = attScore.Reshape(active_dim, headbatch).ToHDB();


                    GumbelSoftmaxRunner smRunner = new GumbelSoftmaxRunner(attProb, TempRate, BuilderParameters.HardGumbel, Behavior);
                    AddRunner(smRunner);
                    attScore = smRunner.Output.ToND().Reshape(attScore.Shape);

                    //SamplingRunner tsRunner = new SamplingRunner(attProb, Util.URandom, null, Behavior) { name = "smp_" + i.ToString() }; // HiddenBatchData prob, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                    //AddRunner(tsRunner);
                    //attScore = tsRunner.OneShot.ToND().Reshape(attScore.Shape); 
                    
                    // it is definitly worse than soft attention model.
                    //probs.Add(attProb);
                    //acts.Add(tsRunner.ShotIndex);


                    NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                    HiddenBatchData attData = aData.Reshape(emd_dim, batch_size).ToHDB();

                    List<HiddenBatchData> next_si_list = new List<HiddenBatchData>();
                    for(int l = 0; l < Model.Layer; l++)
                    {
                        GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCells[l], si_list[l], attData, Behavior);
                        AddRunner(stateRunner);
                        attData = stateRunner.Output;
                        next_si_list.Add(attData);
                    }
                    si_list = next_si_list;
                }

                FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si_list[Model.Layer - 1], Behavior); // fc2Runner.Output, Behavior);
                AddRunner(classifierRunner);
                HiddenBatchData o = classifierRunner.Output;
                
                SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
                AddObjective(CERunner); 
                CERunner.IsSavePred = true;

                Pred = CERunner.Pred;
            }


            public new RAMModel Model { get; set; }
            public ImageDataRunner BatchRunner { get; set; }
            public List<float[]> Pred { get; set; }

            public RAMImageClassifier(RAMModel model, RunnerBehavior behavior) : base(behavior)
            {
                Model = model;  
                SetDelegateModel(Model);
            }

            public void SetTrainSet(ImageFrame dataset)
            {
                SetTrainMode();
                BatchRunner.SetDataset(dataset);
            }

            public void SetPredictSet(ImageFrame dataset)
            {
                SetPredictMode();
                BatchRunner.SetDataset(dataset);
            }


        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            
            Logger.WriteLog("Loading MNIST Training/Testing Data.");
            CIFAR10Panel.Init(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");


            // create a model.
            //int stateDim, int layer, DeviceType device)
            //int featureDim, int stateDim, DeviceType device) 
            RAMImageClassifier.RAMModel model = new RAMImageClassifier.RAMModel(BuilderParameters.FeatureDim, BuilderParameters.Recurrent_Layer, device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }; 
            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);

            RAMImageClassifier classifier = new RAMImageClassifier(model, behavior);
            classifier.StatusReportSteps = 1000000;
            
            //int width, int height, int channel, int batchSize, int head, int recursiveStep)
            classifier.BuildRAM(CIFAR10Panel.TrainFrame.Width, CIFAR10Panel.TrainFrame.Height, CIFAR10Panel.TrainFrame.Channel, BuilderParameters.BATCH_SIZE, BuilderParameters.HeadNum, BuilderParameters.Recurrent);
            
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
                    Logger.WriteLog("iteration {0}", OptimizerParameters.Iteration);

                    double best_Dev_metric = 0;
                    double Test_metric = 0;
                    int iteration = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {   
                        classifier.SetTrainSet(CIFAR10Panel.TrainFrame);
                        classifier.Execute();
                        //Logger.WriteLog("Training avg Loss {0}, avg Accuracy {1}", reader.bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        //if(iter % 5 == 0)
                        {
                            classifier.SetPredictSet(CIFAR10Panel.DevFrame);
                            classifier.Execute(false);

                            //
                            double dev_metric = ACCURACYEvaluationSet.CalculateMultiAccuracy(CIFAR10Panel.DevFrame.Label.Select(i => (int)i).ToList(), classifier.Pred, BuilderParameters.Category, CIFAR10Panel.DevFrame.ImageNum);
                            Logger.WriteLog("Dev metric {0}", dev_metric);

                            if(dev_metric > best_Dev_metric)
                            {
                                best_Dev_metric = dev_metric;

                                classifier.SetPredictSet(CIFAR10Panel.TestFrame);
                                classifier.Execute(false);
                                double test_metric = ACCURACYEvaluationSet.CalculateMultiAccuracy(CIFAR10Panel.TestFrame.Label.Select(i => (int)i).ToList(), classifier.Pred, BuilderParameters.Category, CIFAR10Panel.TestFrame.ImageNum);
                                Logger.WriteLog("Test metric {0}", test_metric);
                                Test_metric = test_metric;
                                iteration = iter;
                            }
                            Logger.WriteLog("Best Dev-Metric {0}, Test-Metric {1}, iteration {2}", best_Dev_metric, Test_metric, iteration);
                            //Logger.WriteLog("Testing avg Loss {0}, avg Accuracy {1}", bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        }
                        //reader.EntropyRate.Incr();
                        //Logger.WriteLog("Entropy Term {0}", reader.EntropyRate.CurrentEps);
                    } 
                    break;
                case DNNRunMode.Predict:
                    break;
            }
        }
    }
}
