// design a recurrent attention model for binary classification task.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.RAM
{
	// build recurrent attention model.
    public class AlexCIFAR10Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("BERT-LAYER", new ParameterArgument("-1", "pre-trained bert layers."));

                Argument.Add("RECURRENT-LAYER",new ParameterArgument("1", "frozen layers."));

                // sometimes it is really hard to find the real interesting things.
                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "reward discount."));         
                Argument.Add("ENTROPY", new ParameterArgument("0:0.002", "entropy schedule."));   

                Argument.Add("STEP", new ParameterArgument("5", "reader step"));
                Argument.Add("K", new ParameterArgument("10", "reader k around"));
                Argument.Add("CATEGORY", new ParameterArgument("2", "classification category"));
                Argument.Add("BASELINE", new ParameterArgument("0","moving average baseline"));
                Argument.Add("ATTSCALE", new ParameterArgument("1", "attention scalar."));
                Argument.Add("TYPE", new ParameterArgument("0", "0: ram; 1 : reasonet; "));
                Argument.Add("RECURRENT", new ParameterArgument("0", "0:gru; 1:rru;"));

                Argument.Add("MAX-ACTIVE-FEATURE", new ParameterArgument("10", "maximum active feature."));
                Argument.Add("FEATURE-DIM", new ParameterArgument("100", "feature dimension."));
                Argument.Add("HEAD-NUM", new ParameterArgument("8","head number."));

                Argument.Add("TEMP", new ParameterArgument("0:0.2", "temp schedule."));   
                Argument.Add("HARD-GUMBEL", new ParameterArgument("0","0:soft gumbel; 1:hard gumbel;"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }

            public static int BERT_LAYER { get { return int.Parse(Argument["BERT-LAYER"].Value); } }

            public static int Recurrent_Layer { get { return int.Parse(Argument["RECURRENT-LAYER"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static List<Tuple<int, float>> Entropy
            {
                get
                {
                    return Argument["ENTROPY"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static List<Tuple<int, float>> Temp
            {
                get
                {
                    return Argument["TEMP"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static int Step { get { return int.Parse(Argument["STEP"].Value); } }
            public static int K { get { return int.Parse(Argument["K"].Value); } }

            public static int Category { get { return int.Parse(Argument["CATEGORY"].Value); } }

            public static float Baseline { get { return float.Parse(Argument["BASELINE"].Value); } }
            public static float AttScale { get { return float.Parse(Argument["ATTSCALE"].Value); } }
            
            public static int Type { get { return int.Parse(Argument["TYPE"].Value); } }

            public static int Recurrent { get { return int.Parse(Argument["RECURRENT"].Value); } }

            // maximum number of active feature.
            public static int MaxActiveFeature { get { return int.Parse(Argument["MAX-ACTIVE-FEATURE"].Value); } }
            // feature dimension.
            public static int FeatureDim { get { return int.Parse(Argument["FEATURE-DIM"].Value); } }
            public static int HeadNum { get { return int.Parse(Argument["HEAD-NUM"].Value); } }

            public static bool HardGumbel { get { return int.Parse(Argument["HARD-GUMBEL"].Value) > 0; }}
               
        }

        // ram classifier.
        public override BuilderType Type { get { return BuilderType.ALEX_CIFAR10_CLASSIFIER; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class AlexImageClassifier : ComputationGraph
        {
        	public class AlexModel : CompositeNNStructure
	        {
                // feature image extraction 1.
                public TensorConvStructure Conv1 = null;
                public TensorConvStructure Conv2 = null;
                public TensorConvStructure Conv3 = null;
                public TensorConvStructure Conv4 = null;
                public TensorConvStructure Conv5 = null;

	        	public LayerStructure Classifier { get; set; }

                public AlexModel(DeviceType device)
	            {

                    Conv1 = AddLayer(new TensorConvStructure(11, 11, 3, 64, device));
                    Conv2 = AddLayer(new TensorConvStructure(5, 5, 64, 192, device));
                    Conv3 = AddLayer(new TensorConvStructure(3, 3, 192, 384, device));
                    Conv4 = AddLayer(new TensorConvStructure(3, 3, 384, 256, device));
                    Conv5 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));

	            	// two class classification. 
	                Classifier = AddLayer(new LayerStructure(256, BuilderParameters.Category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                }
	        }


            public void BuildAlexNet(int width, int height, int channel, int batchSize)
            {
                BatchRunner = new ImageDataRunner(width, height, channel, batchSize, BuilderParameters.Category, Behavior);
                AddDataRunner(BatchRunner);

                SparseVectorData Labels = BatchRunner.Label;
                Tensor4DData image = BatchRunner.Image;
                
                // 32 * 32 * 3.
                TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, image, 5, 4, A_Func.Rectified, Behavior);
                AddRunner(conv1Runner);
                // 8 * 8 * 64
                TensorMaxPooling2DRunner pool1Runner = new TensorMaxPooling2DRunner(conv1Runner.Output, 2, 2, Behavior);
                AddRunner(pool1Runner);
                // 4 * 4 * 64
                TensorConvRunner conv2Runner = new TensorConvRunner(Model.Conv2, pool1Runner.Output, 2, 1, A_Func.Rectified, Behavior);
                AddRunner(conv2Runner);
                // 4 * 4 * 192
                TensorMaxPooling2DRunner pool2Runner = new TensorMaxPooling2DRunner(conv2Runner.Output, 2, 2, Behavior);
                AddRunner(pool2Runner);
                // 2 * 2 * 192
                TensorConvRunner conv3Runner = new TensorConvRunner(Model.Conv3, pool2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
                AddRunner(conv3Runner);
                // 2 * 2 * 384
                TensorConvRunner conv4Runner = new TensorConvRunner(Model.Conv4, conv3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
                AddRunner(conv4Runner);
                // 2 * 2 * 256
                TensorConvRunner conv5Runner = new TensorConvRunner(Model.Conv5, conv4Runner.Output, 1, 1, A_Func.Rectified, Behavior);
                AddRunner(conv5Runner);
                // 2 * 2 * 256.
                TensorMaxPooling2DRunner pool3Runner = new TensorMaxPooling2DRunner(conv5Runner.Output, 2, 2, Behavior);
                AddRunner(pool3Runner);
                // 1 * 1 * 256.

                NdArrayData feaMap = pool3Runner.Output.ToND();
                HiddenBatchData feaInput = feaMap.Reshape(feaMap.Dimensions[2], feaMap.Dimensions[3]).ToHDB();

                FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, feaInput, Behavior); // fc2Runner.Output, Behavior);
                AddRunner(classifierRunner);
                HiddenBatchData o = classifierRunner.Output;
                
                SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
                AddObjective(CERunner); 
                CERunner.IsSavePred = true;

                Pred = CERunner.Pred;
            }


            public new AlexModel Model { get; set; }
            public ImageDataRunner BatchRunner { get; set; }
            public List<float[]> Pred { get; set; }

            public AlexImageClassifier(AlexModel model, RunnerBehavior behavior) : base(behavior)
            {
                Behavior = behavior;  
                Model = model;  
                SetDelegateModel(Model);
            }

            public void SetTrainSet(ImageFrame dataset)
            {
                SetTrainMode();
                BatchRunner.SetDataset(dataset);
            }

            public void SetPredictSet(ImageFrame dataset)
            {
                SetPredictMode();
                BatchRunner.SetDataset(dataset);
            }
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            
            Logger.WriteLog("Loading MNIST Training/Testing Data.");
            CIFAR10Panel.Init(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");


            // create a model.
            //int stateDim, int layer, DeviceType device)
            //int featureDim, int stateDim, DeviceType device) 
            AlexImageClassifier.AlexModel model = new AlexImageClassifier.AlexModel(device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }; 
            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);

            AlexImageClassifier classifier = new AlexImageClassifier(model, behavior);
            classifier.StatusReportSteps = 1000000;
            
            //int width, int height, int channel, int batchSize, int head, int recursiveStep)
            classifier.BuildAlexNet(CIFAR10Panel.TrainFrame.Width, CIFAR10Panel.TrainFrame.Height, CIFAR10Panel.TrainFrame.Channel, BuilderParameters.BATCH_SIZE);
            
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
                    Logger.WriteLog("iteration {0}", OptimizerParameters.Iteration);

                    double best_Dev_metric = 0;
                    double Test_metric = 0;
                    int iteration = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {   
                        classifier.SetTrainSet(CIFAR10Panel.TrainFrame);
                        classifier.Execute();
                        //Logger.WriteLog("Training avg Loss {0}, avg Accuracy {1}", reader.bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        //if(iter % 5 == 0)
                        {
                            classifier.SetPredictSet(CIFAR10Panel.DevFrame);
                            classifier.Execute(false);

                            //
                            double dev_metric = ACCURACYEvaluationSet.CalculateMultiAccuracy(CIFAR10Panel.DevFrame.Label.Select(i => (int)i).ToList(), classifier.Pred, BuilderParameters.Category, CIFAR10Panel.DevFrame.ImageNum);
                            Logger.WriteLog("Dev metric {0}", dev_metric);

                            if(dev_metric > best_Dev_metric)
                            {
                                best_Dev_metric = dev_metric;

                                classifier.SetPredictSet(CIFAR10Panel.TestFrame);
                                classifier.Execute(false);
                                double test_metric = ACCURACYEvaluationSet.CalculateMultiAccuracy(CIFAR10Panel.TestFrame.Label.Select(i => (int)i).ToList(), classifier.Pred, BuilderParameters.Category, CIFAR10Panel.TestFrame.ImageNum);
                                Logger.WriteLog("Test metric {0}", test_metric);
                                Test_metric = test_metric;
                                iteration = iter;
                            }
                            Logger.WriteLog("Best Dev-Metric {0}, Test-Metric {1}, iteration {2}", best_Dev_metric, Test_metric, iteration);
                            //Logger.WriteLog("Testing avg Loss {0}, avg Accuracy {1}", bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        }
                        //reader.EntropyRate.Incr();
                        //Logger.WriteLog("Entropy Term {0}", reader.EntropyRate.CurrentEps);
                    } 
                    break;
                case DNNRunMode.Predict:
                    break;
            }
        }
    }
}
