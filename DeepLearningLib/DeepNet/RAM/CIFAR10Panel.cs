using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;


namespace BigLearn.DeepNet.RAM
{
        public class CIFAR10Util
        {
            public static ImageFrame ExtractFrame(List<string> binfiles, int width, int height, int channel, int size)
            {
                ImageFrame frame = new ImageFrame(size, width, height, channel); 

                int b = 0;
                int imgsize = width * height * channel;
                foreach(string mf in binfiles)
                {
                    using (BinaryReader reader = new BinaryReader(new FileStream(mf, FileMode.Open, FileAccess.Read)))
                    {
                        for(int i =0; i < 10000; i++)
                        {
                            frame.Label[b] = reader.ReadByte();
                            Buffer.BlockCopy(reader.ReadBytes(imgsize), 0, frame.Data, b * imgsize, imgsize);
                            b += 1;
                        }
                    }
                }
                return frame;
            }
        }

        public class CIFAR10Panel 
        {
            public static ImageFrame TrainFrame = null;
            public static ImageFrame DevFrame = null;
            public static ImageFrame TestFrame = null;

            public static void Init(string dataDir)
            {
                TestFrame = CIFAR10Util.ExtractFrame(new List<string> () { dataDir + @"/test_batch.bin" }, 32, 32, 3, 10000);
                ImageFrame allFrame = CIFAR10Util.ExtractFrame(new List<string> () { dataDir + @"/data_batch_1.bin", dataDir + @"/data_batch_2.bin", dataDir + @"/data_batch_3.bin", dataDir + @"/data_batch_4.bin", dataDir + @"/data_batch_5.bin" }, 32, 32, 3, 50000);
                Tuple<ImageFrame, ImageFrame> parts = allFrame.RandomSplit(0.9f);
                TrainFrame = parts.Item1;
                DevFrame = parts.Item2;

                TrainFrame.PrintLabelDistr();
                DevFrame.PrintLabelDistr();
                TestFrame.PrintLabelDistr();
            }
        }
}
