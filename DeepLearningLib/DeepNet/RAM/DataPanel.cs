using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;

namespace BigLearn.DeepNet.RAM
{
        public class DataFrame
        {
            public List<int[]> X = new List<int[]>();
            public List<int> Y = new List<int>();

            public int Row { get { return Y.Count; } }
        }

        public class DataPanel 
        {
            public static DataFrame Train { get; set; }
            public static DataFrame Dev { get; set; }
            public static DataFrame Test { get; set; }

            public static int FeatureNumber { get; set; }
            public static int MaxActiveFeature { get; set; }

            public static DataFrame LoadData(string fileName)
            {
                DataFrame data = new DataFrame();
                using(StreamReader reader = new StreamReader(fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Trim().Split(' ');
                        
                        int l = int.Parse(items[0]);
                        int[] ids = new int[items.Length - 1];

                        for(int i = 1; i < items.Length; i++)
                        {
                            string[] kv = items[i].Split(':');
                            int mk = int.Parse(kv[0]);
                            int mv = int.Parse(kv[1]);
                            ids[i - 1] = mk;
                        }
                        
                        data.X.Add(ids);
                        data.Y.Add(l);
                    }
                }

                return data;
            }

            // build device graph.
            public static void Init(string dataDir)
            {
                FeatureNumber = 0;
                MaxActiveFeature = 0;

                Train = LoadData(dataDir + @"/train.index");
                Dev = LoadData(dataDir + @"/dev.index");
                Test = LoadData(dataDir + @"/test.index");
                
                FeatureNumber = new int[] { Train.X.Select(i => i.Max()).Max(),  Test.X.Select(i => i.Max()).Max(),  Dev.X.Select(i => i.Max()).Max() }.Max() + 1;
                MaxActiveFeature = new int[] { Train.X.Select(i => i.Length).Max(), Test.X.Select(i => i.Length).Max(), Dev.X.Select(i => i.Length).Max() }.Max();

                Logger.WriteLog("Feature Dimension {0}", FeatureNumber);
                Logger.WriteLog("Maximum Active Feature {0}", MaxActiveFeature);
            }

        }


    public class DataRunner : StructRunner
    {
        DataFrame DataSet { get; set; }

        public SparseVectorData Labels { get; set; }

        public CudaPieceInt TokenIdxs { get; set; }

        public DataRandomShuffling Shuffle = null;

        Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);
        
        int MaxActiveNum { get; set; }
        int MaxBatchSize { get; set; }
        int Category { get; set; }
        public int DebugBatch = 0;
        public int BatchCursor = 0;

        public DataRunner(int category, int maxActNum, int maxBatchSize, RunnerBehavior behavior) 
            : base(Structure.Empty, behavior)
        {
            
            MaxActiveNum = maxActNum;
            MaxBatchSize = maxBatchSize;
            Category = category;

            Labels = new SparseVectorData(MaxBatchSize, behavior.Device, false);
            Labels.Value.Init(1);

            TokenIdxs = new CudaPieceInt(MaxBatchSize * MaxActiveNum, behavior.Device); // new SparseMatrixData(vocabSize, maxBatchSize, maxBatchSize * maxSeqLen, behavior.Device, false);
        }

        public void SetDataset(DataFrame dataSet)
        {
            DataSet = dataSet;
            Shuffle = new DataRandomShuffling(DataSet.Row, random);
        }

        public override void Init()
        {
            IsTerminate = false;
            IsContinue = true;
            BatchCursor = 0;
            Logger.WriteLog("Data sampling initialize.********************************");
            if(Behavior.RunMode == DNNRunMode.Train) Shuffle.Shuffling();
        }

        public override void Forward()
        {
            var perf_dataSample = PerfCounter.Manager.Instance["dataSampling"].Begin();

            if(Shuffle.IsDone) { IsTerminate = true; return; }
            
            //if(DebugBatch > 0 && BatchCursor >= DebugBatch) { IsTerminate = true; return; }

            int groupIdx = 0; 

            Labels.Length = MaxBatchSize;
            TokenIdxs.EffectiveSize = MaxBatchSize * MaxActiveNum;

            while (groupIdx < MaxBatchSize)
            {
                int idx = Shuffle.OrderNext();

                if( idx == -1) { idx = Shuffle.Random(); } 

                Labels.Idx[groupIdx] = Category * groupIdx + DataSet.Y[idx];

                if(DataSet.X[idx].Length != MaxActiveNum)
                {
                    throw new Exception(string.Format("the active feature number should be {0}, but it is {1}", MaxActiveNum, DataSet.X[idx].Length));
                }
 
                TokenIdxs.BlockCopy(groupIdx * MaxActiveNum * sizeof(int), DataSet.X[idx], 0,  MaxActiveNum * sizeof(int));

                groupIdx += 1;
            }

            BatchCursor += 1;
            Labels.Idx.SyncFromCPU();
            TokenIdxs.SyncFromCPU();
            PerfCounter.Manager.Instance["corpusSampling"].TakeCount(perf_dataSample);
        }   
            
    }


        
}
