// design a recurrent attention model for binary classification task.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.RAM
{
	// build recurrent attention model.
    public class RAMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("BERT-LAYER", new ParameterArgument("-1", "pre-trained bert layers."));

                Argument.Add("RECURRENT-LAYER",new ParameterArgument("1", "frozen layers."));

                // sometimes it is really hard to find the real interesting things.
                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "reward discount."));         
                Argument.Add("ENTROPY", new ParameterArgument("0:0.002", "entropy schedule."));   

                Argument.Add("STEP", new ParameterArgument("5", "reader step"));
                Argument.Add("K", new ParameterArgument("10", "reader k around"));
                Argument.Add("CATEGORY", new ParameterArgument("2", "classification category"));
                Argument.Add("BASELINE", new ParameterArgument("0","moving average baseline"));
                Argument.Add("ATTSCALE", new ParameterArgument("1", "attention scalar."));
                Argument.Add("TYPE", new ParameterArgument("0", "0: ram; 1 : reasonet; "));
                Argument.Add("RECURRENT", new ParameterArgument("0", "0:gru; 1:rru;"));


                Argument.Add("MAX-ACTIVE-FEATURE", new ParameterArgument("10", "maximum active feature."));
                Argument.Add("FEATURE-DIM", new ParameterArgument("100", "feature dimension."));
                Argument.Add("HEAD-NUM", new ParameterArgument("8","head number."));

                Argument.Add("TEMP", new ParameterArgument("0:0.2", "temp schedule."));   
                Argument.Add("HARD-GUMBEL", new ParameterArgument("0","0:soft gumbel; 1:hard gumbel;"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }

            public static int BERT_LAYER { get { return int.Parse(Argument["BERT-LAYER"].Value); } }

            public static int Recurrent_Layer { get { return int.Parse(Argument["RECURRENT-LAYER"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static List<Tuple<int, float>> Entropy
            {
                get
                {
                    return Argument["ENTROPY"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static List<Tuple<int, float>> Temp
            {
                get
                {
                    return Argument["TEMP"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static int Step { get { return int.Parse(Argument["STEP"].Value); } }
            public static int K { get { return int.Parse(Argument["K"].Value); } }

            public static int Category { get { return int.Parse(Argument["CATEGORY"].Value); } }

            public static float Baseline { get { return float.Parse(Argument["BASELINE"].Value); } }
            public static float AttScale { get { return float.Parse(Argument["ATTSCALE"].Value); } }
            
            public static int Type { get { return int.Parse(Argument["TYPE"].Value); } }

            public static int Recurrent { get { return int.Parse(Argument["RECURRENT"].Value); } }

            // maximum number of active feature.
            public static int MaxActiveFeature { get { return int.Parse(Argument["MAX-ACTIVE-FEATURE"].Value); } }
            // feature dimension.
            public static int FeatureDim { get { return int.Parse(Argument["FEATURE-DIM"].Value); } }
            public static int HeadNum { get { return int.Parse(Argument["HEAD-NUM"].Value); } }

            public static bool HardGumbel { get { return int.Parse(Argument["HARD-GUMBEL"].Value) > 0; }}
               
        }

        // ram classifier.
        public override BuilderType Type { get { return BuilderType.RAM_CLASSIFIER; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class RAMClassifier : ComputationGraph
        {
        	public class RAMModel : CompositeNNStructure
	        {

	        	// feature embedding.
	        	public EmbedStructure FeatureEmbed { get; set; } 

	        	public List<GRUCell> GruCells { get; set; }

	        	public LayerStructure Classifier { get; set; }
            	public LayerStructure T { get; set; }
				
                public LayerStructure QProject { get; set; }
                public LayerStructure KProject { get; set; }
                public LayerStructure VProject { get; set; }

                public List<EmbedStructure> InitStates { get; set; }

				public int FeatureEmdDim { get; set; }

                public int Layer { get; set; }

	            public RAMModel(int featureDim, int stateDim, int layer, DeviceType device)
	            {

	            	FeatureEmdDim = stateDim;

                    Layer = layer;

	            	FeatureEmbed = AddLayer(new EmbedStructure(featureDim, stateDim, -0.034f, 0.034f, device));

                    GruCells = new List<GRUCell>();
                    for(int i = 0; i < layer; i++)
                    {
                        GruCells.Add(AddLayer(new GRUCell(stateDim, stateDim, device)));
                    }

	            	// two class classification. 
	                Classifier = AddLayer(new LayerStructure(stateDim, BuilderParameters.Category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

	                T = AddLayer(new LayerStructure(stateDim, 2, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

                    // better to be top layer.
                    QProject = AddLayer(new LayerStructure(stateDim, stateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                    KProject = AddLayer(new LayerStructure(stateDim, stateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                    VProject = AddLayer(new LayerStructure(stateDim, stateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
				    
                    InitStates = new List<EmbedStructure>();
                    for(int i = 0; i < layer; i++)
                    {
                        InitStates.Add(AddLayer(new EmbedStructure(1, stateDim, -0.034f, 0.034f, device)));
                    }
                }

	        }

	        public new RAMModel Model { get; set; }

	        public DataRunner BatchRunner { get; set; }
	        
	        public List<float[]> Pred { get; set; }
	        

	        public RAMClassifier(RAMModel model, RunnerBehavior behavior) : base(behavior)
	        {
	            Behavior = behavior;  
	            Model = model;  
	            SetDelegateModel(Model);
	        }

            public void SetTrainSet(DataFrame dataset)
            {
                SetTrainMode();
                BatchRunner.SetDataset(dataset);
            }

            public void SetPredictSet(DataFrame dataset)
            {
                SetPredictMode();
                BatchRunner.SetDataset(dataset);
            }

	        public void BuildRAMV1(int maxActiveNum, int batchSize, int head, int recursiveStep)
	        {
	        	// int category, int maxActNum, int maxBatchSize, RunnerBehavior behavior) 
	            BatchRunner = new DataRunner(BuilderParameters.Category, maxActiveNum, batchSize, Behavior);
	            AddDataRunner(BatchRunner);

	            CudaPieceInt TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
	            SparseVectorData Labels = BatchRunner.Labels;

	            //CudaPieceFloat Label = BatchRunner.Labels;
	            //Label = new CudaPieceFloat(batchSize, Behavior.Device);

	            Logger.WriteLog("Build CG in data sampling.");
	            HiddenBatchData fea_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.FeatureEmbed, Behavior));

	            // embedding dimension.
	            IntArgument emd_dim = new IntArgument("emd", Model.FeatureEmdDim);
	            IntArgument active_dim = new IntArgument("active-size", maxActiveNum);
	            IntArgument batch_size = new IntArgument("batch_size", batchSize);

                IntArgument headdim = new IntArgument("head-dim", Model.FeatureEmdDim / head);
                IntArgument headnum = new IntArgument("head-num", head);

				//DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions) 
	            NdArrayData W_tensor = new NdArrayData(Behavior.Device, fea_embed.Output.Data, fea_embed.Deriv.Data, emd_dim, active_dim, batch_size);

                NdArrayData kfeature = W_tensor.FNN(Model.KProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
                NdArrayData vfeature = W_tensor.FNN(Model.VProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;


                NdArrayData initZero = new NdArrayData(Behavior.Device, false, emd_dim, batch_size);  
                initZero.Output.Init(0); 
                
                List<HiddenBatchData> si_list = new List<HiddenBatchData>();
                for(int i = 0; i < Model.Layer; i++)
                {
                    HiddenBatchData initState = new HiddenBatchData(1, Model.FeatureEmdDim, Model.InitStates[i].Embedding, Model.InitStates[i].EmbeddingGrad, Model.InitStates[i].DeviceType);
                    HiddenBatchData si = initZero.AddExpand(initState.ToND(), Session, Behavior).ToHDB(); 
                    si_list.Add(si);
                }


                for(int i = 0; i < recursiveStep; i++)
                {
                    //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                    //AddRunner(maskAttRunner);
                    
                    // top layer si;
                    //MatrixConcateRunner concateRunner = new MatrixConcateRunner(si_list.Select(h => h.ToMD()).ToList(), Behavior);
                    //HiddenBatchData si_q = concateRunner.Output.ToHDB().FNN(Model.QProject, Session, Behavior);

                    HiddenBatchData si_q = si_list[Model.Layer - 1].FNN(Model.QProject, Session, Behavior);

                    NdArrayData qx = si_q.ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batch_size);

                    NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior).Softmax(Session, Behavior);
                    //attScore = attScore.Reshape(attScore.Shape[0], attScore.Shape[2]);

                    // mask with empty. 
                    // attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior).Softmax(Session, Behavior);
                    //HiddenBatchData attSoftmax = attScore.ToHDB();
                    //NdArrayData attScoreX = attScore.Reshape(attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                    NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                    HiddenBatchData attData = aData.Reshape(emd_dim, batch_size).ToHDB();

                    List<HiddenBatchData> next_si_list = new List<HiddenBatchData>();
                    for(int l = 0; l < Model.Layer; l++)
                    {
                        GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCells[l], si_list[l], attData, Behavior);
                        AddRunner(stateRunner);
                        attData = stateRunner.Output;
                        next_si_list.Add(attData);
                    }
                    si_list = next_si_list;
                }

                FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si_list[Model.Layer - 1], Behavior); // fc2Runner.Output, Behavior);
                AddRunner(classifierRunner);
                HiddenBatchData o = classifierRunner.Output;
                
                SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
                AddObjective(CERunner); 
                CERunner.IsSavePred = true;

                Pred = CERunner.Pred;
	        }

            // ram v2.
            public void BuildRAMV2(int maxActiveNum, int batchSize, int head, int recursiveStep)
            {
                // int category, int maxActNum, int maxBatchSize, RunnerBehavior behavior) 
                BatchRunner = new DataRunner(BuilderParameters.Category, maxActiveNum, batchSize, Behavior);
                AddDataRunner(BatchRunner);

                CudaPieceInt TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
                SparseVectorData Labels = BatchRunner.Labels;

                //CudaPieceFloat Label = BatchRunner.Labels;
                //Label = new CudaPieceFloat(batchSize, Behavior.Device);

                Logger.WriteLog("Build CG in data sampling.");
                HiddenBatchData fea_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.FeatureEmbed, Behavior));

                // embedding dimension.
                IntArgument emd_dim = new IntArgument("emd", Model.FeatureEmdDim);
                IntArgument active_dim = new IntArgument("active-size", maxActiveNum);
                IntArgument batch_size = new IntArgument("batch_size", batchSize);

                IntArgument headdim = new IntArgument("head-dim", Model.FeatureEmdDim / head);
                IntArgument headnum = new IntArgument("head-num", head);

                //DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions) 
                NdArrayData W_tensor = new NdArrayData(Behavior.Device, fea_embed.Output.Data, fea_embed.Deriv.Data, emd_dim, active_dim, batch_size);

                NdArrayData kfeature = W_tensor.FNN(Model.KProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
                NdArrayData vfeature = W_tensor.FNN(Model.VProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;


                NdArrayData initZero = new NdArrayData(Behavior.Device, false, emd_dim, batch_size);  
                initZero.Output.Init(0); 
                
                List<HiddenBatchData> si_list = new List<HiddenBatchData>();
                for(int i = 0; i < Model.Layer; i++)
                {
                    HiddenBatchData initState = new HiddenBatchData(1, Model.FeatureEmdDim, Model.InitStates[i].Embedding, Model.InitStates[i].EmbeddingGrad, Model.InitStates[i].DeviceType);
                    HiddenBatchData si = initZero.AddExpand(initState.ToND(), Session, Behavior).ToHDB(); 
                    si_list.Add(si);
                }

                //HiddenBatchData initState = new HiddenBatchData(1, Model.FeatureEmdDim, Model.InitState.Embedding, Model.InitState.EmbeddingGrad, Model.InitState.DeviceType);
                //HiddenBatchData si = initZero.AddExpand(initState.ToND(), Session, Behavior).ToHDB();

                List<HiddenBatchData> TPs = new List<HiddenBatchData>();
                List<HiddenBatchData> TOs = new List<HiddenBatchData>();

                for(int i = 0; i < recursiveStep; i++)
                {
                    //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                    //AddRunner(maskAttRunner);
                    
                    //MatrixConcateRunner concateRunner = new MatrixConcateRunner(si_list.Select(h => h.ToMD()).ToList(), Behavior);
                    //HiddenBatchData si_q = concateRunner.Output.ToHDB().FNN(Model.QProject, Session, Behavior);

                    HiddenBatchData si_q = si_list[Model.Layer - 1].FNN(Model.QProject, Session, Behavior);

                    NdArrayData qx = si_q.ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batch_size);

                    NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior).Softmax(Session, Behavior);
                    //attScore = attScore.Reshape(attScore.Shape[0], attScore.Shape[2]);

                    // mask with empty. 
                    // attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior).Softmax(Session, Behavior);
                    //HiddenBatchData attSoftmax = attScore.ToHDB();
                    //NdArrayData attScoreX = attScore.Reshape(attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                    NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                    HiddenBatchData attData = aData.Reshape(emd_dim, batch_size).ToHDB();

                    List<HiddenBatchData> next_si_list = new List<HiddenBatchData>();
                    for(int l = 0; l < Model.Layer; l++)
                    {
                        GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCells[l], si_list[l], attData, Behavior);
                        AddRunner(stateRunner);
                        attData = stateRunner.Output;
                        next_si_list.Add(attData);
                    }
                    si_list = next_si_list;

                    HiddenBatchData tp = si_list[Model.Layer - 1].FNN(Model.T, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();
                    HiddenBatchData to = si_list[Model.Layer - 1].FNN(Model.Classifier, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();

                    TPs.Add(tp);
                    TOs.Add(to);
                }

                JointLoglikelihoodRunner ceRunner = new JointLoglikelihoodRunner(TPs, TOs, Labels, Behavior);
                AddObjective(ceRunner); 
                Pred = ceRunner.Pred;
                ceRunner.StatusReport = 100000;
            }

            // recurrent attention module with hard attention.
            public void BuildRAMV3(int maxActiveNum, int batchSize, int head, int recursiveStep)
            {
                // int category, int maxActNum, int maxBatchSize, RunnerBehavior behavior) 
                BatchRunner = new DataRunner(BuilderParameters.Category, maxActiveNum, batchSize, Behavior);
                AddDataRunner(BatchRunner);

                CudaPieceInt TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
                SparseVectorData Labels = BatchRunner.Labels;

                //CudaPieceFloat Label = BatchRunner.Labels;
                //Label = new CudaPieceFloat(batchSize, Behavior.Device);

                Logger.WriteLog("Build CG in data sampling.");
                HiddenBatchData fea_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.FeatureEmbed, Behavior));

                // embedding dimension.
                IntArgument emd_dim = new IntArgument("emd", Model.FeatureEmdDim);
                IntArgument active_dim = new IntArgument("active-size", maxActiveNum);
                IntArgument batch_size = new IntArgument("batch_size", batchSize);

                IntArgument headdim = new IntArgument("head-dim", Model.FeatureEmdDim / head);
                IntArgument headnum = new IntArgument("head-num", head);

                IntArgument headbatch = new IntArgument("head-batch", head * batchSize);

                //DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions) 
                NdArrayData W_tensor = new NdArrayData(Behavior.Device, fea_embed.Output.Data, fea_embed.Deriv.Data, emd_dim, active_dim, batch_size);

                NdArrayData kfeature = W_tensor.FNN(Model.KProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
                NdArrayData vfeature = W_tensor.FNN(Model.VProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;


                NdArrayData initZero = new NdArrayData(Behavior.Device, false, emd_dim, batch_size);  
                initZero.Output.Init(0); 
                
                List<HiddenBatchData> si_list = new List<HiddenBatchData>();
                for(int i = 0; i < Model.Layer; i++)
                {
                    HiddenBatchData initState = new HiddenBatchData(1, Model.FeatureEmdDim, Model.InitStates[i].Embedding, Model.InitStates[i].EmbeddingGrad, Model.InitStates[i].DeviceType);
                    HiddenBatchData si = initZero.AddExpand(initState.ToND(), Session, Behavior).ToHDB(); 
                    si_list.Add(si);
                }

                List<HiddenBatchData> probs = new List<HiddenBatchData>();
                List<CudaPieceInt> acts = new List<CudaPieceInt>();

                for(int i = 0; i < recursiveStep; i++)
                {
                    //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                    //AddRunner(maskAttRunner);
                    
                    //MatrixConcateRunner concateRunner = new MatrixConcateRunner(si_list.Select(h => h.ToMD()).ToList(), Behavior);
                    //HiddenBatchData si_q = concateRunner.Output.ToHDB().FNN(Model.QProject, Session, Behavior);

                    HiddenBatchData si_q = si_list[Model.Layer - 1].FNN(Model.QProject, Session, Behavior);

                    NdArrayData qx = si_q.ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batch_size);

                    // softmax and sampling. 
                    NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior).Softmax(Session, Behavior);
                    

                    HiddenBatchData attProb = attScore.Reshape(active_dim, headbatch).ToHDB();
                    SamplingRunner tsRunner = new SamplingRunner(attProb, Util.URandom, null, Behavior) { name = "smp_" + i.ToString() }; // HiddenBatchData prob, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                    AddRunner(tsRunner);
                    attScore = tsRunner.OneShot.ToND().Reshape(attScore.Shape); 
                    
                    // it is definitly worse than soft attention model.
                    probs.Add(attProb);
                    acts.Add(tsRunner.ShotIndex);


                    NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                    HiddenBatchData attData = aData.Reshape(emd_dim, batch_size).ToHDB();

                    List<HiddenBatchData> next_si_list = new List<HiddenBatchData>();
                    for(int l = 0; l < Model.Layer; l++)
                    {
                        GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCells[l], si_list[l], attData, Behavior);
                        AddRunner(stateRunner);
                        attData = stateRunner.Output;
                        next_si_list.Add(attData);
                    }
                    si_list = next_si_list;
                }

                FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si_list[Model.Layer - 1], Behavior); // fc2Runner.Output, Behavior);
                AddRunner(classifierRunner);
                HiddenBatchData o = classifierRunner.Output;
                
                SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
                AddObjective(CERunner); 
                CERunner.IsSavePred = true;

                Pred = CERunner.Pred;

                RateScheduler entropyRate = new RateScheduler(GlobalUpdateStep, BuilderParameters.Entropy);  

                // REINFORCE algoritm.
                for(int i = 0; i < recursiveStep; i++)
                {
                    float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, recursiveStep - i);

                    PGRunner pgRunner = new PGRunner(probs[i], acts[i], CERunner.tmpProb, head, BuilderParameters.Baseline, discount, entropyRate, Behavior) { name = "pg_" + i.ToString() };

                    AddObjective(pgRunner);
                }

            }

            public void BuildRAMV4(int maxActiveNum, int batchSize, int head, int recursiveStep)
            {
                // int category, int maxActNum, int maxBatchSize, RunnerBehavior behavior) 
                BatchRunner = new DataRunner(BuilderParameters.Category, maxActiveNum, batchSize, Behavior);
                AddDataRunner(BatchRunner);

                CudaPieceInt TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
                SparseVectorData Labels = BatchRunner.Labels;

                //CudaPieceFloat Label = BatchRunner.Labels;
                //Label = new CudaPieceFloat(batchSize, Behavior.Device);

                Logger.WriteLog("Build CG in data sampling.");
                HiddenBatchData fea_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.FeatureEmbed, Behavior));

                // embedding dimension.
                IntArgument emd_dim = new IntArgument("emd", Model.FeatureEmdDim);
                IntArgument active_dim = new IntArgument("active-size", maxActiveNum);
                IntArgument batch_size = new IntArgument("batch_size", batchSize);

                IntArgument headdim = new IntArgument("head-dim", Model.FeatureEmdDim / head);
                IntArgument headnum = new IntArgument("head-num", head);

                IntArgument headbatch = new IntArgument("head-batch", head * batchSize);

                //DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions) 
                NdArrayData W_tensor = new NdArrayData(Behavior.Device, fea_embed.Output.Data, fea_embed.Deriv.Data, emd_dim, active_dim, batch_size);

                NdArrayData kfeature = W_tensor.FNN(Model.KProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
                NdArrayData vfeature = W_tensor.FNN(Model.VProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;


                NdArrayData initZero = new NdArrayData(Behavior.Device, false, emd_dim, batch_size);  
                initZero.Output.Init(0); 
                
                List<HiddenBatchData> si_list = new List<HiddenBatchData>();
                for(int i = 0; i < Model.Layer; i++)
                {
                    HiddenBatchData initState = new HiddenBatchData(1, Model.FeatureEmdDim, Model.InitStates[i].Embedding, Model.InitStates[i].EmbeddingGrad, Model.InitStates[i].DeviceType);
                    HiddenBatchData si = initZero.AddExpand(initState.ToND(), Session, Behavior).ToHDB(); 
                    si_list.Add(si);
                }

                //List<HiddenBatchData> probs = new List<HiddenBatchData>();
                //List<CudaPieceInt> acts = new List<CudaPieceInt>();

                RateScheduler TempRate = new RateScheduler(GlobalUpdateStep, BuilderParameters.Temp);  


                for(int i = 0; i < recursiveStep; i++)
                {
                    //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                    //AddRunner(maskAttRunner);
                    
                    //MatrixConcateRunner concateRunner = new MatrixConcateRunner(si_list.Select(h => h.ToMD()).ToList(), Behavior);
                    //HiddenBatchData si_q = concateRunner.Output.ToHDB().FNN(Model.QProject, Session, Behavior);

                    HiddenBatchData si_q = si_list[Model.Layer - 1].FNN(Model.QProject, Session, Behavior);

                    NdArrayData qx = si_q.ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batch_size);

                    // softmax and sampling. 
                    NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);




                    //.Softmax(Session, Behavior);
                    HiddenBatchData attProb = attScore.Reshape(active_dim, headbatch).ToHDB();


                    GumbelSoftmaxRunner smRunner = new GumbelSoftmaxRunner(attProb, TempRate, BuilderParameters.HardGumbel, Behavior);
                    AddRunner(smRunner);
                    attScore = smRunner.Output.ToND().Reshape(attScore.Shape);

                    //SamplingRunner tsRunner = new SamplingRunner(attProb, Util.URandom, null, Behavior) { name = "smp_" + i.ToString() }; // HiddenBatchData prob, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                    //AddRunner(tsRunner);
                    //attScore = tsRunner.OneShot.ToND().Reshape(attScore.Shape); 
                    
                    // it is definitly worse than soft attention model.
                    //probs.Add(attProb);
                    //acts.Add(tsRunner.ShotIndex);


                    NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                    HiddenBatchData attData = aData.Reshape(emd_dim, batch_size).ToHDB();

                    List<HiddenBatchData> next_si_list = new List<HiddenBatchData>();
                    for(int l = 0; l < Model.Layer; l++)
                    {
                        GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCells[l], si_list[l], attData, Behavior);
                        AddRunner(stateRunner);
                        attData = stateRunner.Output;
                        next_si_list.Add(attData);
                    }
                    si_list = next_si_list;
                }

                FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si_list[Model.Layer - 1], Behavior); // fc2Runner.Output, Behavior);
                AddRunner(classifierRunner);
                HiddenBatchData o = classifierRunner.Output;
                
                SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
                AddObjective(CERunner); 
                CERunner.IsSavePred = true;

                Pred = CERunner.Pred;

                
                

            }


        }



        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data ...");
            DataPanel.Init(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");
            
            // create a model.
            //int featureDim, int stateDim, DeviceType device) 
            RAMClassifier.RAMModel model = new RAMClassifier.RAMModel(DataPanel.FeatureNumber, BuilderParameters.FeatureDim, BuilderParameters.Recurrent_Layer, device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }; 
            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);

            RAMClassifier classifier = new RAMClassifier(model, behavior);
            classifier.StatusReportSteps = 1000000;
            if(BuilderParameters.Type  == 0)
            {
                classifier.BuildRAMV1(DataPanel.MaxActiveFeature, BuilderParameters.BATCH_SIZE, BuilderParameters.HeadNum, BuilderParameters.Recurrent);
            }
            else if(BuilderParameters.Type == 1)
            {
                classifier.BuildRAMV2(DataPanel.MaxActiveFeature, BuilderParameters.BATCH_SIZE, BuilderParameters.HeadNum, BuilderParameters.Recurrent);
            }
            else if(BuilderParameters.Type == 2)
            {
                classifier.BuildRAMV3(DataPanel.MaxActiveFeature, BuilderParameters.BATCH_SIZE, BuilderParameters.HeadNum, BuilderParameters.Recurrent);
            }
            else if(BuilderParameters.Type == 3)
            {
                classifier.BuildRAMV4(DataPanel.MaxActiveFeature, BuilderParameters.BATCH_SIZE, BuilderParameters.HeadNum, BuilderParameters.Recurrent);
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
                    Logger.WriteLog("iteration {0}", OptimizerParameters.Iteration);

                    double best_Dev_metric = 0;
                    double Test_metric = 0;
                    int iteration = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {   
                        classifier.SetTrainSet(DataPanel.Train);
                        classifier.Execute();
                        //Logger.WriteLog("Training avg Loss {0}, avg Accuracy {1}", reader.bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        //if(iter % 5 == 0)
                        {
                            classifier.SetPredictSet(DataPanel.Dev);
                            classifier.Execute(false);

                            //
                            double dev_metric = 0;

                            if(BuilderParameters.Category == 2) dev_metric = AUCEvaluationSet.Auc(DataPanel.Dev.Y.Select(i => i * 1.0f), classifier.Pred.Select(i => i[1]));
                            else dev_metric = ACCURACYEvaluationSet.CalculateMultiAccuracy(DataPanel.Dev.Y, classifier.Pred, BuilderParameters.Category, DataPanel.Dev.Y.Count);

                            Logger.WriteLog("Dev metric {0}", dev_metric);

                            if(dev_metric > best_Dev_metric)
                            {
                                best_Dev_metric = dev_metric;

                                classifier.SetPredictSet(DataPanel.Test);
                                classifier.Execute(false);
                                double test_metric = 0;

                                if(BuilderParameters.Category == 2) test_metric = AUCEvaluationSet.Auc(DataPanel.Test.Y.Select(i => i * 1.0f), classifier.Pred.Select(i => i[1]));
                                else test_metric =  ACCURACYEvaluationSet.CalculateMultiAccuracy(DataPanel.Test.Y, classifier.Pred, BuilderParameters.Category, DataPanel.Test.Y.Count);
                                Logger.WriteLog("Test metric {0}", test_metric);
                                Test_metric = test_metric;
                                iteration = iter;
                            }
                            Logger.WriteLog("Best Dev-Metric {0}, Test-Metric {1}, iteration {2}", best_Dev_metric, Test_metric, iteration);
                            //Logger.WriteLog("Testing avg Loss {0}, avg Accuracy {1}", bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        }
                        //reader.EntropyRate.Incr();
                        //Logger.WriteLog("Entropy Term {0}", reader.EntropyRate.CurrentEps);
                    } 
                    break;
                case DNNRunMode.Predict:
                    break;
            }
        }
    }
}
