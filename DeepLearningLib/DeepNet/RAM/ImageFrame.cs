using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;


namespace BigLearn.DeepNet.RAM
{
        public class ImageFrame
        {            
            public Tuple<int, int, int, int, byte[]> Image = null;
            public byte[] Label = null;

            public int ImageNum { get { return Image.Item1; } }
            public int Width { get { return Image.Item2; } }
            public int Height { get { return Image.Item3; } }
            public int Channel { get { return Image.Item4; } }
            public byte[] Data { get { return Image.Item5; } }

            public void PrintLabelDistr()
            {
                Dictionary<int, int> dict = new Dictionary<int, int>();
                for(int i = 0; i < ImageNum; i++)
                {
                    int l = (int)Label[i];
                    if(dict.ContainsKey(l))
                    {
                        dict[l] += 1;
                    }
                    else
                    {
                        dict[l] = 1;
                    }
                }

                foreach(KeyValuePair<int, int> item in dict)
                {
                    Logger.WriteLog("label {0}, distr {1}", item.Key, item.Value);
                }
            }

            public ImageFrame(int size, int width, int height, int channel) 
            {
                Label = new byte[size];
                Image = new Tuple<int, int, int, int, byte[]>(size, width, height, channel, new byte[size * width * height * channel]);
            }

            // mnist load
            public ImageFrame(Tuple<int, int, int, int, byte[]> image, byte[] label)
            {
                Image = image;
                Label = label;
            }

            public Tuple<ImageFrame, ImageFrame> RandomSplit(float ratio)
            {
                int imgNum = ImageNum;
                
                DataRandomShuffling shuffle = new DataRandomShuffling(imgNum, Util.URandom);

                int part1Num = (int)(imgNum * ratio);
                int part2Num = imgNum - part1Num;

                ImageFrame part1 = new ImageFrame(part1Num, Width, Height, Channel);
                ImageFrame part2 = new ImageFrame(part2Num, Width, Height, Channel);

                int part1Idx = 0;
                int part2Idx = 0;

                int imgsize = Width * Height * Channel;

                for(int i = 0; i < imgNum; i++)
                {
                    int idx = shuffle.RandomNext();

                    if(i < part1Num)
                    {
                        part1.Label[part1Idx] = Label[idx];
                        Buffer.BlockCopy(Data, idx * imgsize, part1.Data, part1Idx * imgsize, imgsize);
                        part1Idx += 1;
                    }
                    else
                    {
                        part2.Label[part2Idx] = Label[idx];
                        Buffer.BlockCopy(Data, idx * imgsize, part2.Data, part2Idx * imgsize, imgsize);
                        part2Idx += 1;
                    }
                }

                return new Tuple<ImageFrame, ImageFrame>(part1, part2);
            }

            
        }

        public class ImageDataRunner : StructRunner
        {
            public Tensor4DData Image { get; set; }
            public SparseVectorData Label { get; set; }

            ImageFrame DataSet { get; set; }

            DataRandomShuffling Shuffle { get; set; }
            int BatchSize { get; set; }
            int CategoryNum { get; set; }

            public ImageDataRunner(int width, int height, int channel, int batchSize, int categoryNum, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                Image = new Tensor4DData(width, height, channel, batchSize, false, behavior.Device);
                Label = new SparseVectorData(batchSize, behavior.Device, false);
                Label.Value.Init(1);

                //Shuffle = new DataRandomShuffling(image.Item1, DeepNet.BuilderParameters.Randomer);
                BatchSize = batchSize;
                CategoryNum = categoryNum;

                Logger.WriteLog("image width {0}, height {1}, channel {2}, batchSize {3}", width, height, channel, batchSize);
            }

             public void SetDataset(ImageFrame dataSet)
            {
                DataSet = dataSet;
                Shuffle = new DataRandomShuffling(DataSet.ImageNum, Util.URandom);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                if(Behavior.RunMode == DNNRunMode.Train) Shuffle.Shuffling();
            }

            public override void Forward()
            {
                if(Shuffle.IsDone) { IsTerminate = true; return; }

                int width = DataSet.Width;
                int height = DataSet.Height;
                int channel = DataSet.Channel;

                int imgsize = width * height * channel;

                for(int i = 0; i < BatchSize; i++)
                {
                    int idx = Shuffle.OrderNext();
                    if( idx == -1) { idx = Shuffle.Random(); } 

                    Label.Idx[i] = i * CategoryNum + DataSet.Label[idx];
                    for (int x = 0; x < imgsize; x++)
                    {
                        Image.Output[i * imgsize + x] = DataSet.Data[idx * imgsize + x] / 255.0f;
                        //Console.WriteLine("pixel {0} {1}", x, Image.Output[i * (width * height) + x]);
                        
                        if(Image.Output[i * imgsize + x] > 1)
                        {
                            throw new Exception("Image data is wrong");
                        }
                    }
                }
                Label.Length = BatchSize;
                Label.Idx.SyncFromCPU();

                Image.BatchSize = BatchSize;
                Image.Output.SyncFromCPU();
            }

        }

        
}
