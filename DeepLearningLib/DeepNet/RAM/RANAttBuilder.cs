// design a recurrent attention model for binary classification task.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.RAM
{
	// build recurrent attention model.
    public class RANAttBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("BERT-LAYER", new ParameterArgument("-1", "pre-trained bert layers."));

                Argument.Add("RECURRENT-LAYER",new ParameterArgument("1", "frozen layers."));

                // sometimes it is really hard to find the real interesting things.
                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "reward discount."));         
                Argument.Add("ENTROPY", new ParameterArgument("0:0.002", "entropy schedule."));   

                Argument.Add("STEP", new ParameterArgument("5", "reader step"));
                Argument.Add("K", new ParameterArgument("10", "reader k around"));
                Argument.Add("CATEGORY", new ParameterArgument("2", "classification category"));
                Argument.Add("BASELINE", new ParameterArgument("0","moving average baseline"));
                Argument.Add("ATTSCALE", new ParameterArgument("1", "attention scalar."));
                Argument.Add("TYPE", new ParameterArgument("0", "0: ram; 1 : reasonet; "));
                Argument.Add("RECURRENT", new ParameterArgument("0", "0:gru; 1:rru;"));


                Argument.Add("MAX-ACTIVE-FEATURE", new ParameterArgument("10", "maximum active feature."));
                Argument.Add("FEATURE-DIM", new ParameterArgument("100", "feature dimension."));
                Argument.Add("HEAD-NUM", new ParameterArgument("8","head number."));

                Argument.Add("TEMP", new ParameterArgument("0:0.2", "temp schedule."));   
                Argument.Add("HARD-GUMBEL", new ParameterArgument("0","0:soft gumbel; 1:hard gumbel;"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }

            public static int BERT_LAYER { get { return int.Parse(Argument["BERT-LAYER"].Value); } }

            public static int Recurrent_Layer { get { return int.Parse(Argument["RECURRENT-LAYER"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static List<Tuple<int, float>> Entropy
            {
                get
                {
                    return Argument["ENTROPY"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static List<Tuple<int, float>> Temp
            {
                get
                {
                    return Argument["TEMP"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static int Step { get { return int.Parse(Argument["STEP"].Value); } }
            public static int K { get { return int.Parse(Argument["K"].Value); } }

            public static int Category { get { return int.Parse(Argument["CATEGORY"].Value); } }

            public static float Baseline { get { return float.Parse(Argument["BASELINE"].Value); } }
            public static float AttScale { get { return float.Parse(Argument["ATTSCALE"].Value); } }
            
            public static int Type { get { return int.Parse(Argument["TYPE"].Value); } }

            public static int Recurrent { get { return int.Parse(Argument["RECURRENT"].Value); } }

            // maximum number of active feature.
            public static int MaxActiveFeature { get { return int.Parse(Argument["MAX-ACTIVE-FEATURE"].Value); } }
            // feature dimension.
            public static int FeatureDim { get { return int.Parse(Argument["FEATURE-DIM"].Value); } }
            public static int HeadNum { get { return int.Parse(Argument["HEAD-NUM"].Value); } }

            public static bool HardGumbel { get { return int.Parse(Argument["HARD-GUMBEL"].Value) > 0; }}
               
        }

        // ram classifier.
        public override BuilderType Type { get { return BuilderType.RAN_ATT_CLASSIFIER; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class RANClassifier : ComputationGraph
        {
        	public class RANModel : CompositeNNStructure
	        {
                public int FeatureEmdDim { get { return FeatureEmbed.Dim; } }
                public int Layer { get { return InitStates.Count; } }

	        	// feature embedding.
	        	public EmbedStructure FeatureEmbed { get; set; } 
                public List<EmbedStructure> InitStates { get; set; }
                public RANStructure RAN { get; set; }
                public SelfAttPoolStructure SelfPool { get; set; }
                public LayerStructure Classifier { get; set; }

                //ran layer.
	            public RANModel(int featureDim, int stateDim, int layer, int category, DeviceType device)
	            {
                    DeviceType = device;

	            	FeatureEmbed = AddLayer(new EmbedStructure(featureDim, stateDim, -0.034f, 0.034f, device));
                    InitStates = new List<EmbedStructure>();
                    for(int i = 0; i < layer; i++)
                    {
                        InitStates.Add(AddLayer(new EmbedStructure(1, stateDim, -0.034f, 0.034f, device)));
                    }
                    RAN = AddLayer(new RANStructure(stateDim, layer, device)); // int state_dim, int recurrent_layer, DeviceType device)
                    // int input_dim, int state_dim, DeviceType device)
                    SelfPool = AddLayer(new SelfAttPoolStructure(stateDim, stateDim, device));
                    Classifier = AddLayer(new LayerStructure(stateDim, category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                }

                // public override void Serialize(BinaryWriter mwriter)
                // {
                //     FeatureEmbed.Serialize(mwriter);
                //     mwriter.Write(Layer);
                //     for(int i = 0; i < Layer; i++)
                //     {
                //         InitStates[i].Serialize(mwriter);
                //     }
                //     RAN.Serialize(mwriter); 
                //     SelfPool.Serialize(mwriter);
                //     Classifier.Serialize(mwriter);                         
                // }

                // public RANModel(BinaryReader reader, DeviceType device)
                // {
                //     DeviceType = device;

                //     FeatureEmbed = DeserializeModel<EmbedStructure>(reader, device); 
                //     int layer = reader.ReadInt32();
                //     InitStates = new List<EmbedStructure>();
                //     for(int i = 0; i < layer; i++)
                //     {
                //         InitStates.Add(DeserializeModel<EmbedStructure>(reader, device));
                //     }
                //     RAN = DeserializeModel<RANStructure>(reader, device);
                //     SelfPool = DeserializeModel<SelfAttPoolStructure>(reader, device);
                //     Classifier = DeserializeModel<LayerStructure>(reader, device);
                // }

	        }

	        public new RANModel Model { get; set; }

	        public DataRunner BatchRunner { get; set; }
	        
	        public List<float[]> Pred { get; set; }
	        
            public HiddenBatchData RAN_Pool { get { return RAN.Output; } }
            public List<HiddenBatchData> RAN_Att { get { return RAN.AttData; } }
            public List<CudaPieceInt> RAN_Att_Index { get { return RAN.AttIndex; } }
            public RANRunner RAN { get; set; }

	        public RANClassifier(RANModel model, RunnerBehavior behavior) : base(behavior)
	        {
	            Behavior = behavior;  
	            Model = model;  
	            SetDelegateModel(Model);
	        }

            public void SetTrainSet(DataFrame dataset)
            {
                SetTrainMode();
                BatchRunner.SetDataset(dataset);
            }

            public void SetPredictSet(DataFrame dataset)
            {
                SetPredictMode();
                BatchRunner.SetDataset(dataset);
            }

            
            DumpAttentionRunner AttIdxRunner { get; set; }
            public string DumpAttPath 
            {
                get { return AttIdxRunner.DumpPath; }
                set { AttIdxRunner.DumpPath = value; }
            }

            public void BuildRAN(int maxActiveNum, int batchSize, int head, int recursiveStep)
            {
                // int category, int maxActNum, int maxBatchSize, RunnerBehavior behavior) 
                BatchRunner = new DataRunner(BuilderParameters.Category, maxActiveNum, batchSize, Behavior);
                AddDataRunner(BatchRunner);

                CudaPieceInt TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
                SparseVectorData Labels = BatchRunner.Labels;

                //CudaPieceFloat Label = BatchRunner.Labels;
                //Label = new CudaPieceFloat(batchSize, Behavior.Device);

                Logger.WriteLog("Build CG in data sampling.");
                HiddenBatchData fea_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.FeatureEmbed, Behavior));

                // embedding dimension.
                IntArgument emd_dim = new IntArgument("emd", Model.FeatureEmdDim);
                IntArgument active_dim = new IntArgument("active-size", maxActiveNum);
                IntArgument batch_size = new IntArgument("batch_size", batchSize);
                NdArrayData W_tensor = new NdArrayData(Behavior.Device, fea_embed.Output.Data, fea_embed.Deriv.Data, emd_dim, active_dim, batch_size);

                RateScheduler TempRate = new RateScheduler(GlobalUpdateStep, BuilderParameters.Temp);  

                NdArrayData initZero = new NdArrayData(Behavior.Device, false, emd_dim, batch_size);  
                initZero.Output.Init(0); 
                
                List<HiddenBatchData> si_list = new List<HiddenBatchData>();
                for(int i = 0; i < Model.InitStates.Count; i++)
                {
                    HiddenBatchData initState = new HiddenBatchData(1, Model.FeatureEmdDim, Model.InitStates[i].Embedding, Model.InitStates[i].EmbeddingGrad, Model.InitStates[i].DeviceType);
                    HiddenBatchData si = initZero.AddExpand(initState.ToND(), Session, Behavior).ToHDB(); 
                    si_list.Add(si);
                }

                // RANStructure model, NdArrayData input, int nhead, RateScheduler tempRate, bool hardGumbel, List<HiddenBatchData> states, int step, RunnerBehavior behavior
                RAN = new RANRunner(Model.RAN, W_tensor, head, TempRate, BuilderParameters.HardGumbel, si_list, recursiveStep, Behavior);
                AddRunner(RAN);
                
                //List<CudaPieceInt> attIdx, IntArgument batch, int head, string dumpFile, RunnerBehavior behavior)
                AttIdxRunner = new DumpAttentionRunner(RAN.AttIndex, batch_size, head, Behavior);
                AddRunner(AttIdxRunner);
                
                IntArgument state_dim = new IntArgument("state", Model.RAN.StateDim);
                IntArgument recursive_dim = new IntArgument("recursive", recursiveStep);
                NdArrayData recOutput = RAN.Outputs.Concate(Session, Behavior).ToND().Reshape(state_dim, recursive_dim, batch_size);

                SelfAttPoolRunner selfPoolRunner = new SelfAttPoolRunner(Model.SelfPool, recOutput, head, TempRate, BuilderParameters.HardGumbel, Behavior);
                AddRunner(selfPoolRunner);
                
                // selfPoolRunner.Output // RAN.Output
                FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, selfPoolRunner.Output, Behavior); // fc2Runner.Output, Behavior);
                AddRunner(classifierRunner);
                HiddenBatchData o = classifierRunner.Output;
                
                SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
                AddObjective(CERunner); 
                CERunner.IsSavePred = true;

                Pred = CERunner.Pred;
            }

        }



        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data ...");
            DataPanel.Init(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");
            
            // create a model.
            //int featureDim, int stateDim, DeviceType device) int featureDim, int stateDim, int layer, DeviceType device)
            RANClassifier.RANModel model = new RANClassifier.RANModel(DataPanel.FeatureNumber, BuilderParameters.FeatureDim, BuilderParameters.Recurrent_Layer, BuilderParameters.Category, device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }; 
            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);

            RANClassifier classifier = new RANClassifier(model, behavior);
            classifier.StatusReportSteps = 1000000;
             
            classifier.BuildRAN(DataPanel.MaxActiveFeature, BuilderParameters.BATCH_SIZE, BuilderParameters.HeadNum, BuilderParameters.Recurrent);
            

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
                    Logger.WriteLog("iteration {0}", OptimizerParameters.Iteration);

                    double best_Dev_metric = 0;
                    double Test_metric = 0;
                    int iteration = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {   
                        classifier.SetTrainSet(DataPanel.Train);
                        classifier.DumpAttPath = string.Empty;
                        classifier.Execute();
                        //Logger.WriteLog("Training avg Loss {0}, avg Accuracy {1}", reader.bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        //if(iter % 5 == 0)
                        {
                            classifier.SetPredictSet(DataPanel.Dev);
                            classifier.DumpAttPath = BuilderParameters.ModelOutputPath + @"/att_dev_"+iter.ToString()+".dump";
                            classifier.Execute(false);

                            //
                            double dev_metric = 0;

                            if(BuilderParameters.Category == 2) dev_metric = AUCEvaluationSet.Auc(DataPanel.Dev.Y.Select(i => i * 1.0f), classifier.Pred.Select(i => i[1]));
                            else 
                            {
                                dev_metric = ACCURACYEvaluationSet.CalculateMultiAccuracy(DataPanel.Dev.Y, classifier.Pred, BuilderParameters.Category, DataPanel.Dev.Y.Count);
                                int[] dev_confusion = ACCURACYEvaluationSet.CalculateConfuseMatrix(DataPanel.Dev.Y, classifier.Pred, BuilderParameters.Category, DataPanel.Dev.Y.Count);
                                Logger.WriteLog("Dev Confusion Matrix:");
                                for(int d = 0; d < BuilderParameters.Category; d++)
                                {
                                    Logger.WriteLog(string.Join("\t", dev_confusion.Skip(d * BuilderParameters.Category).Take(BuilderParameters.Category)));   
                                }
                            }
                            
                            Logger.WriteLog("Dev metric {0}", dev_metric);


                            if(dev_metric > best_Dev_metric)
                            {
                                best_Dev_metric = dev_metric;

                                classifier.SetPredictSet(DataPanel.Test);
                                classifier.DumpAttPath = BuilderParameters.ModelOutputPath + @"/att_test_"+iter.ToString()+".dump"; // string.Empty;
                                classifier.Execute(false);
                                double test_metric = 0;

                                if(BuilderParameters.Category == 2) test_metric = AUCEvaluationSet.Auc(DataPanel.Test.Y.Select(i => i * 1.0f), classifier.Pred.Select(i => i[1]));
                                else
                                {
                                    test_metric =  ACCURACYEvaluationSet.CalculateMultiAccuracy(DataPanel.Test.Y, classifier.Pred, BuilderParameters.Category, DataPanel.Test.Y.Count);
                                    int[] test_confusion = ACCURACYEvaluationSet.CalculateConfuseMatrix(DataPanel.Test.Y, classifier.Pred, BuilderParameters.Category, DataPanel.Test.Y.Count);


                                    //Logger.WriteLog("Test Prediction Score:");
                                    //for(int pi = 0; pi < Math.Min(4, classifier.Pred.Count); pi++)
                                    //{
                                    //    Logger.WriteLog(string.Join("\t",classifier.Pred[pi]));
                                    //}

                                    //Logger.WriteLog("RAN Pooling Vectors:");
                                    //classifier.RAN_Pool.Output.Data.SyncToCPU();
                                    //classifier.RAN_Att.Last().Output.Data.SyncToCPU();
                                    //for(int pi = 0; pi < Math.Min(4, classifier.Pred.Count); pi++)
                                    //{
                                    //    for(int s = 0; s < classifier.RAN_Att_Index.Count; s++)
                                    //    {
                                    //        Logger.WriteLog(string.Join(",", classifier.RAN_Att_Index[s].TakeCPU(pi * BuilderParameters.HeadNum, BuilderParameters.HeadNum)) + "\t");
                                    //    }
                                    //    Logger.WriteLog(string.Join("\t", classifier.RAN_Pool.Output.Data.TakeCPU(pi * BuilderParameters.FeatureDim, BuilderParameters.FeatureDim )));
                                    //    Logger.WriteLog("**********************");
                                    //    Logger.WriteLog(string.Join("\t", classifier.RAN_Att.Last().Output.Data.TakeCPU(pi * BuilderParameters.FeatureDim, BuilderParameters.FeatureDim )));  
                                    //}

                                    Logger.WriteLog("Test Confusion Matrix:");
                                    for(int d = 0; d < BuilderParameters.Category; d++)
                                    {
                                        Logger.WriteLog(string.Join("\t", test_confusion.Skip(d * BuilderParameters.Category).Take(BuilderParameters.Category)));   
                                    }
                                }

                                Logger.WriteLog("Test metric {0}", test_metric);
                                Test_metric = test_metric;
                                iteration = iter;
                            }
                            Logger.WriteLog("Best Dev-Metric {0}, Test-Metric {1}, iteration {2}", best_Dev_metric, Test_metric, iteration);
                            //Logger.WriteLog("Testing avg Loss {0}, avg Accuracy {1}", bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        }
                        //reader.EntropyRate.Incr();
                        //Logger.WriteLog("Entropy Term {0}", reader.EntropyRate.CurrentEps);
                    } 
                    break;
                case DNNRunMode.Predict:
                    break;
            }
        }
    }
}
