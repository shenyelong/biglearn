using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    public class NeuralReaderBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("BERT-LAYER", new ParameterArgument("-1", "pre-trained bert layers."));

                Argument.Add("FROZEN-LAYER",new ParameterArgument("-1", "frozen layers."));

                // sometimes it is really hard to find the real interesting things.
                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "reward discount."));         
                Argument.Add("ENTROPY", new ParameterArgument("0:0.002", "entropy schedule."));   
                Argument.Add("STEP", new ParameterArgument("5", "reader step"));
                Argument.Add("K", new ParameterArgument("10", "reader k around"));
                Argument.Add("CATEGORY", new ParameterArgument("2", "classification category"));
                Argument.Add("BASELINE", new ParameterArgument("0","moving average baseline"));
                Argument.Add("ATTSCALE", new ParameterArgument("1", "attention scalar."));
                Argument.Add("TYPE", new ParameterArgument("NR", "default neural reader model."));
                Argument.Add("RECURRENT", new ParameterArgument("0", "0:gru; 1:rru;"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }

            public static int BERT_LAYER { get { return int.Parse(Argument["BERT-LAYER"].Value); } }

            public static int FROZEN_LAYER { get { return int.Parse(Argument["FROZEN-LAYER"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static List<Tuple<int, float>> Entropy
            {
                get
                {
                    return Argument["ENTROPY"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static int Step { get { return int.Parse(Argument["STEP"].Value); } }
            public static int K { get { return int.Parse(Argument["K"].Value); } }

            public static int Category { get { return int.Parse(Argument["CATEGORY"].Value); } }

            public static float Baseline { get { return float.Parse(Argument["BASELINE"].Value); } }
            public static float AttScale { get { return float.Parse(Argument["ATTSCALE"].Value); } }
            
            public static string Type { get { return Argument["TYPE"].Value; } }

            public static int Recurrent { get { return int.Parse(Argument["RECURRENT"].Value); } }
        }


        public override BuilderType Type { get { return BuilderType.NEURAL_READER; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data ...");

            TextDataSet trainSet = new TextDataSet(BuilderParameters.Data + @"/train.index");
            TextDataSet testSet = new TextDataSet(BuilderParameters.Data + @"/dev.index");

            Logger.WriteLog("Load Data Finished.");
            
            // create a model.
            NeuralReader.NeuralReaderModel model = new NeuralReader.NeuralReaderModel(BuilderParameters.FROZEN_LAYER, BuilderParameters.Category, BuilderParameters.BERT_SEED + @"/exp.label", BuilderParameters.BERT_SEED + @"/exp.bin", device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }; 
            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);

            NeuralReader reader = new NeuralReader(model, behavior);

            // initialize reinforce algorithm.
            reader.Discount = BuilderParameters.REWARD_DISCOUNT;
            reader.EntropyRate = new RateScheduler(reader.GlobalUpdateStep, BuilderParameters.Entropy);  

            //int batchSize, int max_seq_len, int bertLayer, int recursiveStep, int Karound
            if(BuilderParameters.Type.Equals("NR"))
            {
                Logger.WriteLog("Build NR CG.");
                reader.BuildCG(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ, BuilderParameters.BERT_LAYER, BuilderParameters.Step, BuilderParameters.K, BuilderParameters.Baseline, 
                               BuilderParameters.AttScale, BuilderParameters.Recurrent);
            }
            else if(BuilderParameters.Type.Equals("RAM"))
            {

                Logger.WriteLog("Build Ram CG.");
                reader.BuildCG_RAM(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ, BuilderParameters.BERT_LAYER, BuilderParameters.Step, BuilderParameters.K, BuilderParameters.Baseline,
                                   BuilderParameters.AttScale, BuilderParameters.Recurrent);
                               //int batchSize, int max_seq_len, int bertLayer, int recursiveStep, int Karound, float moving_avg, float attScalef)
            }
            else if(BuilderParameters.Type.Equals("REASONET"))
            {
                Logger.WriteLog("Build Reasonet CG.");
                reader.BuildCG_REASONET(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ, BuilderParameters.BERT_LAYER, BuilderParameters.Step, BuilderParameters.K, BuilderParameters.Baseline,
                                   BuilderParameters.AttScale, BuilderParameters.Recurrent);
            }
            else if(BuilderParameters.Type.Equals("RAM-MH"))
            {
                Logger.WriteLog("RAM Multi Head.");
                reader.BuildCG_RAM_MultiHead(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ, BuilderParameters.BERT_LAYER, BuilderParameters.Step, BuilderParameters.K, BuilderParameters.Baseline,
                                   BuilderParameters.AttScale, BuilderParameters.Recurrent);
            }
            else if(BuilderParameters.Type.Equals("REASONET-MH"))
            {
                Logger.WriteLog("REASONET Multi Head.");
                reader.BuildCG_Reasonet_MultiHead(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ, BuilderParameters.BERT_LAYER, BuilderParameters.Step, BuilderParameters.K, BuilderParameters.Baseline,
                                   BuilderParameters.AttScale, BuilderParameters.Recurrent);
            }
            else if(BuilderParameters.Type.Equals("RAM-HARD"))
            {
                Logger.WriteLog("Build Ram Hard Attention.");
                reader.BuildCG_Hard_RAM(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ, BuilderParameters.BERT_LAYER, BuilderParameters.Step, BuilderParameters.K, BuilderParameters.Baseline,
                                   BuilderParameters.AttScale, BuilderParameters.Recurrent);
            }
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
                    Logger.WriteLog("iteration {0}", OptimizerParameters.Iteration);
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {   
                        reader.SetTrainSet(trainSet);
                        reader.Execute();
                        //Logger.WriteLog("Training avg Loss {0}, avg Accuracy {1}", reader.bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        //if(iter % 5 == 0)
                        {
                            reader.SetPredictSet(testSet);
                            reader.Execute(false);
                            //Logger.WriteLog("Testing avg Loss {0}, avg Accuracy {1}", bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        }
                        //reader.EntropyRate.Incr();
                        //Logger.WriteLog("Entropy Term {0}", reader.EntropyRate.CurrentEps);
                    } 
                    break;
                case DNNRunMode.Predict:
                    break;
            }
        }
    }
}
