using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Language
{
		public class PolicyGradientRunner : ObjectiveRunner
        {
            //Random random = new Random();
            HiddenBatchData Prob { get; set; }
            CudaPieceInt Act { get; set; }
            CudaPieceFloat LogReward { get; set; }

            float Discount { get; set; }
            RateScheduler Entropy { get; set; }

            int globalStep = 0;
            float baseline = 0;

            float moving_avg = 0;

            public override void Init()
            {}

            public override void Complete()
            {
                // Epoch += 1;
                // int sc = Success.Where(i => i.Value > 0).Count();
                // int t = Success.Count;
                // Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
                // Logger.WriteLog("unique leaf number {0}", uniqueLeafNum * 1.0f / uniqueNum);

                // float alpha_entropy = Entropy * (float)Math.Pow(0.995f, totalBatchNum / 200);
                // Logger.WriteLog("Entropy Term {0}", alpha_entropy);
            }

            public PolicyGradientRunner(HiddenBatchData prob, CudaPieceInt act, CudaPieceFloat log_reward, float mae,
                                        float discount, RateScheduler entropy, RunnerBehavior behavior) 
                : base(Structure.Empty, behavior)
            {
                Prob = prob;
                Act = act;

                LogReward = log_reward;
                Discount = discount;
                Entropy = entropy;

                moving_avg = mae;
            }

            public override void Forward()
            {
                Prob.Deriv.Data.SyncToCPU();
                Prob.Output.Data.SyncToCPU();
                LogReward.SyncToCPU();
                Act.SyncToCPU();
                
                // heart break. 
                // for lgd loss the game. 
                // one question is : why emotion is important ?
                // how emotion is developped for most anamials.
                // take an example : when we train a classifier, we never think this classifier has any emotion. 
                // some low level anamials also don't have any emotions.
                int actDim = Prob.Dim;
                float sum_reward = 0;

                for(int i = 0; i < Prob.BatchSize; i++)
                {
                    int idx = Act[i];
                    float reward = (float)Math.Exp(LogReward[i]);

                    float mp = Prob.Output.Data[i * actDim + idx] < Util.LargeEpsilon ? Util.LargeEpsilon : Prob.Output.Data[i * actDim + idx];
                    
                    Prob.Deriv.Data[i * actDim + idx] += Discount * (reward - baseline) * 1.0f / mp;

                    sum_reward += reward;
                }

                ObjectiveScore = sum_reward / Prob.BatchSize;

                if(Behavior.RunMode == DNNRunMode.Train)
                {
                    baseline = (float)((1- moving_avg) * baseline + moving_avg * ObjectiveScore);
                }
                globalStep += 1;

                if(globalStep % 100 == 0)
                {
                    Logger.WriteLog("step {0}, sampling action {1}, baseline {2}, entropy {3} ", name, Act[0], baseline, Entropy.CurrentEps);
                }

                Prob.Deriv.Data.SyncFromCPU();
                ComputeLib.DerivProbEntropy(Prob.Deriv.Data, Prob.Output.Data, 1, Entropy.CurrentEps, Util.LargeEpsilon, actDim, Prob.BatchSize);
                //DerivProbEntropy(Prob.Deriv.Data, Prob.Output.Data, 1, alpha_entropy, Util.LargeEpsilon, actDim, Prob.BatchSize);
                //float alpha_entropy = Entropy * (float)Math.Pow(0.90f, totalBatchNum / 200);
                //{
                //    for (int i = Input.StatusPath.Count - 1; i >= 0; i--)
                //    {
                //        StatusData st = Input.StatusPath[i];
                //        if (st.MatchCandidateProb != null)
                //        {          
                //        }
                //    }
                //}
            }
        }
}
