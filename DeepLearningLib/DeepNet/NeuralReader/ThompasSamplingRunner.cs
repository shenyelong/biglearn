using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Language
{
        public class ThompasSamplingRunner : StructRunner
        {
            Random mrandom { get; set; }
            RateScheduler Epsilon { get; set; }

            public CudaPieceInt ProbIndex { get; set; }

            public CudaPieceFloat MaskProb { get; set; }

            HiddenBatchData Prob { get; set; }

            CudaPieceFloat MaskScale { get; set; }

            int total_smp = 0;
            float total_smp_prob = 0;

            int globalStep = 0;

            // epsilon greedy.
            public ThompasSamplingRunner(HiddenBatchData prob, CudaPieceFloat maskScale, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                : base(Structure.Empty, behavior)
            {
                mrandom = randomizer;
                Epsilon = epsilon;

                Prob = prob;
                
                MaskScale = maskScale;
                
                ProbIndex = new CudaPieceInt(prob.MAX_BATCHSIZE, behavior.Device);
                MaskProb = new CudaPieceFloat(prob.Dim * prob.MAX_BATCHSIZE, behavior.Device);
            }

            public override void Init()
            {
                total_smp = 0;
                total_smp_prob = 0;
            }

            public override void Complete()
            {
                iteration += 1;
                //if(iteration % ReportPerEpoch == 0)
                //{
                //    Logger.WriteLog("Average ts sample Prob {0}", (total_smp_prob / total_smp));
                //}
            }

            public unsafe override void Forward()
            {
                var perf_ts = PerfCounter.Manager.Instance["thompasSamplingRunner"].Begin();
                int batchSize = Prob.BatchSize;

                Prob.Output.Data.SyncToCPU();
                ProbIndex.EffectiveSize = batchSize;

                ComputeLib.Zero(MaskProb, MaskProb.Size);
                MaskProb.SyncToCPU();

                float sum_prob = 0;
                for(int i = 0; i < batchSize; i++)
                {
                    float r = (float)mrandom.NextDouble();

                    int idx = 0;

                    if(Behavior.RunMode == DNNRunMode.Train)
                    {
                        idx = (Epsilon != null && Epsilon.AmIin(r)) ? 
                                   mrandom.Next(0, Prob.Dim) : Util.Sample(Prob.Output.Data.CpuPtr, i * Prob.Dim, Prob.Dim, MaskScale.CpuPtr, i * Prob.Dim, mrandom);
                    }
                    else if(Behavior.RunMode == DNNRunMode.Predict)
                    {
                        idx = Util.MaximumValue(Prob.Output.Data.CpuPtr, i * Prob.Dim, (i + 1) * Prob.Dim) - i * Prob.Dim;
                    }
                    ProbIndex[i] = idx;
                    
                    MaskProb[i * Prob.Dim + idx] = 1;

                    sum_prob += Prob.Output.Data[i * Prob.Dim + idx];
                }
                total_smp += batchSize;
                total_smp_prob += sum_prob;

                globalStep += 1;
                if(globalStep % 100 == 0)
                {
                    Logger.WriteLog("{0} : Average samplling prob {1},  current sampling prob {2} ", name, total_smp_prob / total_smp, sum_prob / batchSize );
                }

                ProbIndex.SyncFromCPU();
                MaskProb.SyncFromCPU();
                
                PerfCounter.Manager.Instance["thompasSamplingRunner"].TakeCount(perf_ts);
            }
        }
}
