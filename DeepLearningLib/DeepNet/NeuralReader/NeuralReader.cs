using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    // it is an on-policy runner.
    public class NeuralReader : ComputationGraph
    {
        public class NeuralReaderModel : CompositeNNStructure
        {
            public Bert.BertBaseModel BertModel { get; set; }
            public LayerStructure QProject { get; set; }
            public LayerStructure KProject { get; set; }
            public LayerStructure VProject { get; set; }
            public GRUCell GruCell { get; set; }

            public LayerStructure Classifier { get; set; }
            public LayerStructure T { get; set; }
            public NeuralReaderModel(string meta_file, string bin_file, DeviceType device) 
                : this(-1, 2, meta_file, bin_file, device)
            { }

            public NeuralReaderModel(int frozenlayer, int category, string meta_file, string bin_file, DeviceType device)
            {
                BertModel = AddLayer(new Bert.BertBaseModel(frozenlayer, category, meta_file, bin_file, device));

                QProject = AddLayer(new LayerStructure(BertModel.embed, BertModel.embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                KProject = AddLayer(new LayerStructure(BertModel.embed, BertModel.embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                VProject = AddLayer(new LayerStructure(BertModel.embed, BertModel.embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

                GruCell = AddLayer(new GRUCell(BertModel.embed, BertModel.embed, device));

                Classifier = AddLayer(new LayerStructure(BertModel.embed, category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

                T = AddLayer(new LayerStructure(BertModel.embed, 2, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                //Classifier.weight.Init(2 * 0.034f, -0.034f);
                // generate coarse graph.
            }
        }

        public override void GradientRegular()
        {
            float clipNorm = OptimizerParameters.StructureOptimizer.GradNormClip;
            if(clipNorm > 0)
            {
                float gradL2 = Behavior.Computelib.DotProduct(Model.Grad, Model.Grad, Model.Grad.Size);
                float norm = (float)Math.Sqrt(gradL2);
                
                if(norm > clipNorm)
                {
                    /// b = bwei b + awei * a;
                    Behavior.Computelib.Scale_Vector(Model.Grad, 0, Model.Grad, 0, Model.Grad.Size, 0, clipNorm / norm);
                }
            }
        }

        public new NeuralReaderModel Model { get; set; }
        public RateScheduler EntropyRate { get; set; }
        public float Discount { get; set; }
        Bert BertRunner { get; set; } 
        
        public NeuralReader(NeuralReaderModel model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior;
            Model = model;
            SetDelegateModel(Model);
        }

        

        class MaskAttRunner : StructRunner
        {
            public NdArrayData MaskScale { get; set; }
            public NdArrayData MaskBias { get; set; }

            float _Scale { get; set; }
            float _Bias { get; set; }

            CudaPieceInt Pos { get; set; }  
            int Karound { get; set; }
            int BatchSize { get; set; }
            CudaPieceInt MaskIds { get; set; }
            int MaxSeqLen { get; set; }

            // all the data has been already in cpu.
            public MaskAttRunner(CudaPieceInt pos, int karound, CudaPieceInt maskIds, int batchSize, int max_seq_len, float maskScale, float maskBias, RunnerBehavior behavior) 
                : base(Structure.Empty, behavior)
            {
                Pos = pos;
                Karound = karound;
                MaskIds = maskIds;
                BatchSize = batchSize;
                MaxSeqLen = max_seq_len;

                _Scale = maskScale;
                _Bias = maskBias;

                MaskScale = new NdArrayData(behavior.Device, false, new IntArgument("MAX_SEQ_LEN", max_seq_len), new IntArgument("BatchSize", batchSize));
                MaskBias = new NdArrayData(behavior.Device, false, new IntArgument("MAX_SEQ_LEN", max_seq_len), new IntArgument("BatchSize", batchSize));
            }

            public override void Forward()
            {
                for(int i = 0; i < BatchSize; i++)
                {
                    for(int w = 0; w < MaxSeqLen; w++)
                    {
                        if((MaskIds[i * MaxSeqLen + w] == 1) && (w >= Pos[i] - Karound && w <= Pos[i] + Karound))
                        {
                            MaskScale.Output[i * MaxSeqLen + w] = _Scale;
                            MaskBias.Output[i * MaxSeqLen + w] = 0;
                        }
                        else
                        {
                            MaskScale.Output[i * MaxSeqLen + w] = 0;
                            MaskBias.Output[i * MaxSeqLen + w] = _Bias; 
                        }
                    }
                }
                MaskScale.Output.SyncFromCPU();
                MaskBias.Output.SyncFromCPU();
            }
        }

        public void BuildCG_REASONET(int batchSize, int max_seq_len, int bertLayer, int recursiveStep, int Karound, float moving_avg, float attScalef, int recurrentType)
        {
            Logger.WriteLog("start Building bert feature layer....");

            BertRunner = new Bert(Model.BertModel, Behavior);
            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, bertLayer);
            AddRunner(BertRunner);
            Logger.WriteLog("Build bert feature layer.");

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = BertRunner.Slice;
            SparseVectorData Label = BertRunner.BatchRunner.Labels;

            CudaPieceFloat maskScale = BertRunner.MaskScale;
            CudaPieceFloat maskBias = BertRunner.MaskBias; 

            NdArrayData MaskScale = new NdArrayData(Behavior.Device, maskScale, null, new IntArgument("MAX_SEQ_LEN", max_seq_len), new IntArgument("BatchSize", batchSize) );
            NdArrayData MaskBias = new NdArrayData(Behavior.Device, maskBias, null, new IntArgument("MAX_SEQ_LEN", max_seq_len), new IntArgument("BatchSize", batchSize));

            NdArrayData kfeature = feature.FNN(Model.KProject, Session, Behavior);
            NdArrayData vfeature = feature.FNN(Model.VProject, Session, Behavior);
            
            float attScale = (float)(attScalef / Math.Sqrt(pool.Dim));

            HiddenBatchData initZeroH0 = new HiddenBatchData(batchSize, pool.Dim, false, Behavior.Device);
            initZeroH0.Output.Data.Init(0);

            HiddenBatchData si = null;

            if(recurrentType == 0)
            {
                GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(initStateRunner);
                si = initStateRunner.Output;
            }
            else if(recurrentType == 1)
            {
                RRUStateRunner rruInitStateRunner = new RRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(rruInitStateRunner);
                si = rruInitStateRunner.Output;
            }

            Logger.WriteLog("Build init state runner.");

            HiddenBatchData tp = si.FNN(Model.T, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();
            // to softmax.
            HiddenBatchData to = si.FNN(Model.Classifier, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();

            List<HiddenBatchData> TPs = new List<HiddenBatchData>();
            List<HiddenBatchData> TOs = new List<HiddenBatchData>();
            TPs.Add(tp);
            TOs.Add(to);

            for(int i = 0; i < recursiveStep; i++)
            {
                //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                //AddRunner(maskAttRunner);

                HiddenBatchData si_q = si.FNN(Model.QProject, Session, Behavior);
                NdArrayData qx = si_q.ToND().Reshape(si_q.Shape[0], new IntArgument("seq", 1), si_q.Shape[1]);
                NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);
                attScore = attScore.Reshape(attScore.Shape[0], attScore.Shape[2]);

                // mask with empty. 
                attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior).Softmax(Session, Behavior);
                HiddenBatchData attSoftmax = attScore.ToHDB();

                NdArrayData attScoreX = attScore.Reshape(attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                NdArrayData aData = attScoreX.MatMul(0, vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(aData.Shape[0], aData.Shape[2]).ToHDB();


                if(recurrentType == 0)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(stateRunner);
                    si = stateRunner.Output;
                }
                else if(recurrentType == 1)
                {
                    RRUStateRunner rruStateRunner = new RRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(rruStateRunner);
                    si = rruStateRunner.Output;
                }

                //HiddenBatchData prob, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                //ThompasSamplingRunner tsRunner = new ThompasSamplingRunner(attSoftmax, maskAttRunner.MaskScale.Output, Util.URandom, null, Behavior) { name = "ts_" + i.ToString() };
                //AddRunner(tsRunner);

                //CudaPieceInt action = tsRunner.ProbIndex;
                //pos = action;

                //probs.Add(attSoftmax);
                //acts.Add(action);
                tp = si.FNN(Model.T, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();
                to = si.FNN(Model.Classifier, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();

                TPs.Add(tp);
                TOs.Add(to);
            }            
            
            JointLoglikelihoodRunner ceRunner = new JointLoglikelihoodRunner(TPs, TOs, Label, Behavior);
            AddObjective(ceRunner); 
        }



        //hard attention vs soft attention, see which one is better. 
        // build another CG second version.
        public void BuildCG_RAM(int batchSize, int max_seq_len, int bertLayer, int recursiveStep, int Karound, float moving_avg, float attScalef, int recurrentType)
        {
            Logger.WriteLog("start Building bert feature layer....");

            BertRunner = new Bert(Model.BertModel, Behavior);
            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, bertLayer);
            AddRunner(BertRunner);
            Logger.WriteLog("Build bert feature layer.");

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = BertRunner.Slice;
            SparseVectorData Label = BertRunner.BatchRunner.Labels;

            CudaPieceFloat maskScale = BertRunner.MaskScale;
            CudaPieceFloat maskBias = BertRunner.MaskBias; 

            NdArrayData MaskScale = new NdArrayData(Behavior.Device, maskScale, null, new IntArgument("MAX_SEQ_LEN", max_seq_len), new IntArgument("BatchSize", batchSize) );
            NdArrayData MaskBias = new NdArrayData(Behavior.Device, maskBias, null, new IntArgument("MAX_SEQ_LEN", max_seq_len), new IntArgument("BatchSize", batchSize));

            NdArrayData kfeature = feature.FNN(Model.KProject, Session, Behavior);
            NdArrayData vfeature = feature.FNN(Model.VProject, Session, Behavior);
                
            float attScale = (float)(attScalef / Math.Sqrt(pool.Dim));

            HiddenBatchData initZeroH0 = new HiddenBatchData(batchSize, pool.Dim, false, Behavior.Device);
            initZeroH0.Output.Data.Init(0);

            //GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
            //AddRunner(initStateRunner);
            //HiddenBatchData si = initStateRunner.Output;

            HiddenBatchData si = null;

            if(recurrentType == 0)
            {
                GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(initStateRunner);
                si = initStateRunner.Output;
            }
            else if(recurrentType == 1)
            {
                RRUStateRunner rruInitStateRunner = new RRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(rruInitStateRunner);
                si = rruInitStateRunner.Output;
            }


            Logger.WriteLog("Build init state runner.");

            // 心态很关键。心态不能崩。
            // 说实话，搞research这么多年，受的全是别人的负面评价。这里没做好，那里没做好。
            for(int i = 0; i < recursiveStep; i++)
            {
                //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                //AddRunner(maskAttRunner);
                HiddenBatchData si_q = si.FNN(Model.QProject, Session, Behavior);
                NdArrayData qx = si_q.ToND().Reshape(si_q.Shape[0], new IntArgument("seq", 1), si_q.Shape[1]);
                NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);
                attScore = attScore.Reshape(attScore.Shape[0], attScore.Shape[2]);

                // mask with empty. 
                attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior).Softmax(Session, Behavior);
                HiddenBatchData attSoftmax = attScore.ToHDB();

                NdArrayData attScoreX = attScore.Reshape(attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                NdArrayData aData = attScoreX.MatMul(0, vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(aData.Shape[0], aData.Shape[2]).ToHDB();



                if(recurrentType == 0)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(stateRunner);
                    si = stateRunner.Output;
                }
                
                else if(recurrentType == 1)
                {
                    RRUStateRunner rruStateRunner = new RRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(rruStateRunner);
                    si = rruStateRunner.Output;
                }

                //GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                //AddRunner(stateRunner);
                //si = stateRunner.Output;

            }

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            HiddenBatchData o = classifierRunner.Output;
            
            SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Label, o, true, Behavior); 
            AddObjective(CERunner); 
            // not ensemble.
        }

        // hard attention for ram.
        public void BuildCG_Hard_RAM(int batchSize, int max_seq_len, int bertLayer, int recursiveStep, int Karound, float moving_avg, float attScalef, int recurrentType)
        {
            Logger.WriteLog("start Building bert feature layer....");

            BertRunner = new Bert(Model.BertModel, Behavior);
            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, bertLayer);
            AddRunner(BertRunner);
            Logger.WriteLog("Build bert feature layer.");

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = BertRunner.Slice;
            SparseVectorData Label = BertRunner.BatchRunner.Labels;

            CudaPieceFloat maskScale = BertRunner.MaskScale;
            CudaPieceFloat maskBias = BertRunner.MaskBias; 

            NdArrayData MaskScale = new NdArrayData(Behavior.Device, maskScale, null, new IntArgument("MAX_SEQ_LEN", max_seq_len), new IntArgument("BatchSize", batchSize) );
            NdArrayData MaskBias = new NdArrayData(Behavior.Device, maskBias, null, new IntArgument("MAX_SEQ_LEN", max_seq_len), new IntArgument("BatchSize", batchSize));

            NdArrayData kfeature = feature.FNN(Model.KProject, Session, Behavior);
            NdArrayData vfeature = feature.FNN(Model.VProject, Session, Behavior);
                
            float attScale = (float)(attScalef / Math.Sqrt(pool.Dim));

            HiddenBatchData initZeroH0 = new HiddenBatchData(batchSize, pool.Dim, false, Behavior.Device);
            initZeroH0.Output.Data.Init(0);


            HiddenBatchData si = null;
            if(recurrentType == 0)
            {
                GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(initStateRunner);
                si = initStateRunner.Output;
            }
            else if(recurrentType == 1)
            {
                RRUStateRunner rruInitStateRunner = new RRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(rruInitStateRunner);
                si = rruInitStateRunner.Output;
            }

            //GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
            //AddRunner(initStateRunner);
            //HiddenBatchData si = initStateRunner.Output;
            Logger.WriteLog("Build init state runner.");

            // 心态很关键。心态不能崩。
            // 说实话，搞research这么多年，受的全是别人的负面评价。这里没做好，那里没做好。
            // 不论重复别人，还是重复自己，都是重复。

            List<HiddenBatchData> probs = new List<HiddenBatchData>();
            List<CudaPieceInt> acts = new List<CudaPieceInt>();

            for(int i = 0; i < recursiveStep; i++)
            {
                //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                //AddRunner(maskAttRunner);
                HiddenBatchData si_q = si.FNN(Model.QProject, Session, Behavior);
                NdArrayData qx = si_q.ToND().Reshape(si_q.Shape[0], new IntArgument("seq", 1), si_q.Shape[1]);
                NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);
                attScore = attScore.Reshape(attScore.Shape[0], attScore.Shape[2]);

                // mask with empty. 
                attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior).Softmax(Session, Behavior);
                HiddenBatchData attSoftmax = attScore.ToHDB();

                // thompas sampling method. sampling one word. 
                ThompasSamplingRunner tsRunner = new ThompasSamplingRunner(attSoftmax, maskScale, Util.URandom, null, Behavior) { name = "ts_" + i.ToString() };
                AddRunner(tsRunner);

                NdArrayData hardAttData = new NdArrayData(Behavior.Device, tsRunner.MaskProb, null, attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                //int maxBatchSize, int dim, CudaPieceFloat data, CudaPieceFloat deriv, DeviceType device) 
                //HiddenBatchData hardAttData = new HiddenBatchData(attSoftmax.MAX_BATCHSIZE, attSoftmax.Dim, tsRunner.MaskProb, null, Behavior.Device); 

                //NdArrayData attScoreX = hardAttData.ToND().Reshape(attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                NdArrayData aData = hardAttData.MatMul(0, vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(aData.Shape[0], aData.Shape[2]).ToHDB();

                if(recurrentType == 0)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(stateRunner);
                    si = stateRunner.Output;
                }
                else if(recurrentType == 1)
                {
                    RRUStateRunner rruStateRunner = new RRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(rruStateRunner);
                    si = rruStateRunner.Output;
                }

                //GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                //AddRunner(stateRunner);
                //si = stateRunner.Output;

                //HiddenBatchData prob, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                //ThompasSamplingRunner tsRunner = new ThompasSamplingRunner(attSoftmax, maskAttRunner.MaskScale.Output, Util.URandom, null, Behavior) { name = "ts_" + i.ToString() };
                //AddRunner(tsRunner);

                //CudaPieceInt action = tsRunner.ProbIndex;
                //pos = action;

                probs.Add(attSoftmax);
                acts.Add(tsRunner.ProbIndex);
            }

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            HiddenBatchData o = classifierRunner.Output;
            
            SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Label, o, true, Behavior); 
            AddObjective(CERunner); 

            // REINFORCE algoritm.
            for(int i = 0; i < recursiveStep; i++)
            {
                float discount = (float)Math.Pow(Discount, recursiveStep - i);

                PolicyGradientRunner pgRunner = new PolicyGradientRunner(probs[i], acts[i], CERunner.tmpProb, moving_avg, discount, EntropyRate, Behavior) { name = "pg_" + i.ToString() };
                //PolicyGradientRunner pgRunner = new PolicyGradientRunner(probs[i], acts[i], logProb, moving_avg, discount, EntropyRate, Behavior) { name = "pg_" + i.ToString() };
                AddObjective(pgRunner);
            }

            // not ensemble.
        }

        public void BuildCG_RAM_MultiHead(int batchSize, int max_seq_len, int bertLayer, int recursiveStep, int Karound, float moving_avg, float attScalef, int recurrentType)
        {
            int HeadNum = 12;

            Logger.WriteLog("start Building bert feature layer....");

            BertRunner = new Bert(Model.BertModel, Behavior);
            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, bertLayer);
            AddRunner(BertRunner);
            Logger.WriteLog("Build bert feature layer.");

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = BertRunner.Slice;
            SparseVectorData Label = BertRunner.BatchRunner.Labels;


            IntArgument headdim = new IntArgument("head-dim", pool.Dim / HeadNum);
            IntArgument headnum = new IntArgument("head-num", HeadNum);

            float attScale = (float)(attScalef / Math.Sqrt(pool.Dim / HeadNum));

            IntArgument batchsize = new IntArgument("batchSize", batchSize);
            IntArgument statedim = new IntArgument("stateDim", pool.Dim);
            IntArgument seqlen = new IntArgument("MAX_SEQ_LEN", max_seq_len);
            
            NdArrayData MaskScale = new NdArrayData(Behavior.Device, BertRunner.MaskScale, null, seqlen, new IntArgument("seq", 1), new IntArgument("head", 1), batchsize );
            NdArrayData MaskBias = new NdArrayData(Behavior.Device, BertRunner.MaskBias, null, seqlen, new IntArgument("seq", 1), new IntArgument("head", 1), batchsize );

            NdArrayData kfeature = feature.FNN(Model.KProject, Session, Behavior).Reshape(headdim, headnum, seqlen, batchsize).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
            NdArrayData vfeature = feature.FNN(Model.VProject, Session, Behavior).Reshape(headdim, headnum, seqlen, batchsize).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
                

            HiddenBatchData initZeroH0 = new HiddenBatchData(batchSize, pool.Dim, false, Behavior.Device);
            initZeroH0.Output.Data.Init(0);

            HiddenBatchData si = null;
            if(recurrentType == 0)
            {
                GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(initStateRunner);
                si = initStateRunner.Output;
            }
            else if(recurrentType == 1)
            {
                RRUStateRunner rruInitStateRunner = new RRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(rruInitStateRunner);
                si = rruInitStateRunner.Output;
            }

            //GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
            //AddRunner(initStateRunner);
            //HiddenBatchData si = initStateRunner.Output;
            Logger.WriteLog("Build init state runner.");
            
            // 心态很关键。心态不能崩。
            // 说实话，搞research这么多年，受的全是别人的负面评价。这里没做好，那里没做好。
            for(int i = 0; i < recursiveStep; i++)
            {
                //MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                //AddRunner(maskAttRunner);
                
                HiddenBatchData si_q = si.FNN(Model.QProject, Session, Behavior);

                NdArrayData qx = si_q.ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batchsize);

                NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);
                //attScore = attScore.Reshape(attScore.Shape[0], attScore.Shape[2]);

                // mask with empty. 
                attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior).Softmax(Session, Behavior);
                //HiddenBatchData attSoftmax = attScore.ToHDB();
                //NdArrayData attScoreX = attScore.Reshape(attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(statedim, batchsize).ToHDB();

                if(recurrentType == 0)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(stateRunner);
                    si = stateRunner.Output;
                }
                else if(recurrentType == 1)
                {
                    RRUStateRunner rruStateRunner = new RRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(rruStateRunner);
                    si = rruStateRunner.Output;
                }

                //GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                //AddRunner(stateRunner);
                //si = stateRunner.Output;

                //HiddenBatchData prob, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                //ThompasSamplingRunner tsRunner = new ThompasSamplingRunner(attSoftmax, maskAttRunner.MaskScale.Output, Util.URandom, null, Behavior) { name = "ts_" + i.ToString() };
                //AddRunner(tsRunner);

                //CudaPieceInt action = tsRunner.ProbIndex;
                //pos = action;

                //probs.Add(attSoftmax);
                //acts.Add(action);
            }
            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            HiddenBatchData o = classifierRunner.Output;
            
            SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Label, o, true, Behavior); 
            AddObjective(CERunner); 
            // not ensemble.
        }

        public void BuildCG_Reasonet_MultiHead(int batchSize, int max_seq_len, int bertLayer, int recursiveStep, int Karound, float moving_avg, float attScalef, int recurrentType)
        {
            int HeadNum = 12;

            Logger.WriteLog("start Building bert feature layer....");

            BertRunner = new Bert(Model.BertModel, Behavior);
            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, bertLayer);
            AddRunner(BertRunner);
            Logger.WriteLog("Build bert feature layer.");

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = BertRunner.Slice;
            SparseVectorData Label = BertRunner.BatchRunner.Labels;

            IntArgument headdim = new IntArgument("head-dim", pool.Dim / HeadNum);
            IntArgument headnum = new IntArgument("head-num", HeadNum);

            float attScale = (float)(attScalef / Math.Sqrt(pool.Dim / HeadNum));

            IntArgument batchsize = new IntArgument("batchSize", batchSize);
            IntArgument statedim = new IntArgument("stateDim", pool.Dim);
            IntArgument seqlen = new IntArgument("MAX_SEQ_LEN", max_seq_len);
            
            NdArrayData MaskScale = new NdArrayData(Behavior.Device, BertRunner.MaskScale, null, seqlen, new IntArgument("seq", 1), new IntArgument("head", 1), batchsize );
            NdArrayData MaskBias = new NdArrayData(Behavior.Device, BertRunner.MaskBias, null, seqlen, new IntArgument("seq", 1), new IntArgument("head", 1), batchsize );

            NdArrayData kfeature = feature.FNN(Model.KProject, Session, Behavior).Reshape(headdim, headnum, seqlen, batchsize).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
            NdArrayData vfeature = feature.FNN(Model.VProject, Session, Behavior).Reshape(headdim, headnum, seqlen, batchsize).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
                
            HiddenBatchData initZeroH0 = new HiddenBatchData(batchSize, pool.Dim, false, Behavior.Device);
            initZeroH0.Output.Data.Init(0);

            HiddenBatchData si = null;
            if(recurrentType == 0)
            {
                GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(initStateRunner);
                si = initStateRunner.Output;
            }
            else if(recurrentType == 1)
            {
                RRUStateRunner rruInitStateRunner = new RRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(rruInitStateRunner);
                si = rruInitStateRunner.Output;
            }

            //GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
            //AddRunner(initStateRunner);
            //HiddenBatchData si = initStateRunner.Output;
            Logger.WriteLog("Build init state runner.");
            
            HiddenBatchData tp = si.FNN(Model.T, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();
            HiddenBatchData to = si.FNN(Model.Classifier, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();

            List<HiddenBatchData> TPs = new List<HiddenBatchData>();
            List<HiddenBatchData> TOs = new List<HiddenBatchData>();

            TPs.Add(tp);
            TOs.Add(to);

            for(int i = 0; i < recursiveStep; i++)
            {
                HiddenBatchData si_q = si.FNN(Model.QProject, Session, Behavior);

                NdArrayData qx = si_q.ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batchsize);

                NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);
                //attScore = attScore.Reshape(attScore.Shape[0], attScore.Shape[2]);

                // mask with empty. 
                attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior).Softmax(Session, Behavior);
                //HiddenBatchData attSoftmax = attScore.ToHDB();
                //NdArrayData attScoreX = attScore.Reshape(attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(statedim, batchsize).ToHDB();

                //GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                //AddRunner(stateRunner);
                //si = stateRunner.Output;

                if(recurrentType == 0)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(stateRunner);
                    si = stateRunner.Output;
                }
                else if(recurrentType == 1)
                {
                    RRUStateRunner rruStateRunner = new RRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(rruStateRunner);
                    si = rruStateRunner.Output;
                }

                tp = si.FNN(Model.T, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();
                to = si.FNN(Model.Classifier, Session, Behavior).ToND().Softmax(Session, Behavior).ToHDB();

                TPs.Add(tp);
                TOs.Add(to);
            }
            
            JointLoglikelihoodRunner ceRunner = new JointLoglikelihoodRunner(TPs, TOs, Label, Behavior);
            AddObjective(ceRunner); 
            //FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si, Behavior); // fc2Runner.Output, Behavior);
            //AddRunner(classifierRunner);
            //HiddenBatchData o = classifierRunner.Output;
            
            //SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Label, o, true, Behavior); 
            //AddObjective(CERunner); 
            // not ensemble.

            // 好像这些都没什么效果，应该也是正常现象，本来模型就不会有很明显的效果。
            // 如果把dropout 去掉会不会好点呢，我觉得差别也不会很大。
            // 第三个问题，就是把attention 改成hard attention 看效果会不会炸裂。（超好，或者，超烂）.

        }

        // controller component.
        public void BuildCG(int batchSize, int max_seq_len, int bertLayer, int recursiveStep, int Karound, float moving_avg, float attScalef, int recurrentType)
        {
            Logger.WriteLog("start Building bert feature layer....");

            BertRunner = new Bert(Model.BertModel, Behavior);
            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, bertLayer);
            AddRunner(BertRunner);
            Logger.WriteLog("Build bert feature layer.");

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = BertRunner.Slice;
            SparseVectorData Label = BertRunner.BatchRunner.Labels;
            
            //FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.BertModel.Pooler, pool, Behavior); // fc2Runner.Output, Behavior);
            //AddRunner(fcRunner);
            //HiddenBatchData interm = fcRunner.Output;

            //PoolFeaturer = interm.ToT4D().ToND();
            //interm = interm.Dropout(0.1f, Session, Behavior);

            //FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, interm, Behavior); // fc2Runner.Output, Behavior);
            //AddRunner(classifierRunner);
            //HiddenBatchData o = classifierRunner.Output;

            NdArrayData kfeature = feature.FNN(Model.KProject, Session, Behavior);
            NdArrayData vfeature = feature.FNN(Model.VProject, Session, Behavior);
                
            float attScale = (float)(attScalef / Math.Sqrt(pool.Dim));

            HiddenBatchData initZeroH0 = new HiddenBatchData(batchSize, pool.Dim, false, Behavior.Device);
            initZeroH0.Output.Data.Init(0);

            HiddenBatchData si = null;
            if(recurrentType == 0)
            {
                GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(initStateRunner);
                si = initStateRunner.Output;
            }
            else if(recurrentType == 1)
            {
                RRUStateRunner rruInitStateRunner = new RRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
                AddRunner(rruInitStateRunner);
                si = rruInitStateRunner.Output;
            }

            //GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCell, initZeroH0, pool, Behavior);
            //AddRunner(initStateRunner);
            //HiddenBatchData si = initStateRunner.Output;
            

            CudaPieceInt pos = new CudaPieceInt(batchSize, Behavior.Device);
            pos.Init(0);

            Logger.WriteLog("Build init state runner.");
            
            List<HiddenBatchData> probs = new List<HiddenBatchData>();
            List<CudaPieceInt> acts = new List<CudaPieceInt>();

            // model an reasonable network structure.
            for(int i = 0; i < recursiveStep; i++)
            {
                MaskAttRunner maskAttRunner = new MaskAttRunner(pos, Karound, BertRunner.BatchRunner.MaskIds, batchSize, max_seq_len, attScale, (float)-1e9, Behavior);
                AddRunner(maskAttRunner);

                HiddenBatchData si_q = si.FNN(Model.QProject, Session, Behavior);
                NdArrayData qx = si_q.ToND().Reshape(si_q.Shape[0], new IntArgument("seq", 1), si_q.Shape[1]);
                NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);
                attScore = attScore.Reshape(attScore.Shape[0], attScore.Shape[2]);

                // mask with empty 
                attScore = attScore.DotAndAdd(maskAttRunner.MaskScale, maskAttRunner.MaskBias, Session, Behavior).Softmax(Session, Behavior);
                HiddenBatchData attSoftmax = attScore.ToHDB();

                NdArrayData attScoreX = attScore.Reshape(attScore.Shape[0], new IntArgument("seq", 1), attScore.Shape[1]);

                NdArrayData aData = attScoreX.MatMul(0, vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(aData.Shape[0], aData.Shape[2]).ToHDB();

                //GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                //AddRunner(stateRunner);
                //si = stateRunner.Output;

                if(recurrentType == 0)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(stateRunner);
                    si = stateRunner.Output;
                }
                else if(recurrentType == 1)
                {
                    RRUStateRunner rruStateRunner = new RRUStateRunner(Model.GruCell, si, attData, Behavior);
                    AddRunner(rruStateRunner);
                    si = rruStateRunner.Output;
                }

                //HiddenBatchData prob, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) 
                ThompasSamplingRunner tsRunner = new ThompasSamplingRunner(attSoftmax, maskAttRunner.MaskScale.Output, Util.URandom, null, Behavior) { name = "ts_" + i.ToString() };
                AddRunner(tsRunner);

                CudaPieceInt action = tsRunner.ProbIndex;
                pos = action;

                probs.Add(attSoftmax);
                acts.Add(action);
            }

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, si, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            HiddenBatchData o = classifierRunner.Output;
            
            SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Label, o, true, Behavior); 
            AddObjective(CERunner); 


            // REINFORCE algoritm.
            for(int i = 0; i < recursiveStep; i++)
            {
                float discount = (float)Math.Pow(Discount, recursiveStep - i);

                PolicyGradientRunner pgRunner = new PolicyGradientRunner(probs[i], acts[i], CERunner.tmpProb, moving_avg, discount, EntropyRate, Behavior) { name = "pg_" + i.ToString() };
                AddObjective(pgRunner);
            }
        }

        public void SetTrainSet(TextDataSet dataset)
        {
            SetTrainMode();
            BertRunner.BatchRunner.SetDataset(dataset);
        }

        public void SetPredictSet(TextDataSet dataset)
        {
            SetPredictMode();
            BertRunner.BatchRunner.SetDataset(dataset);
        }
    }
}
