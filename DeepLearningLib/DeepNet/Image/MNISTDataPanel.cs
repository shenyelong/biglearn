using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.Image
{
        public class MNISTDataPanel 
        {
            public static Tuple<int, int, int, byte[]> TrainImage = null;
            public static byte[] TrainLabel = null;

            public static Tuple<int, int, int, byte[]> ValidImage = null;
            public static byte[] ValidLabel = null;

            static int ReadInt32(BinaryReader reader)
            {
                byte[] a = reader.ReadBytes(4);
                Array.Reverse(a);
                return BitConverter.ToInt32(a, 0);
            }

            static byte[] ExtractLabels(string labelFile)
            {
                using (BinaryReader labelReader = new BinaryReader(new FileStream(labelFile, FileMode.Open, FileAccess.Read)))
                {
                    int magicNum = ReadInt32(labelReader);
                    int labelNumber = ReadInt32(labelReader);

                    byte[] labelArray = new byte[labelNumber];
                    for(int i=0;i<labelNumber;i++)
                    {
                        labelArray[i] = labelReader.ReadByte();
                    }
                    return labelArray;
                }
            }

            static Tuple<int, int, int, byte[]> ExtractImages(string imageFile)
            {
                using (BinaryReader imageReader = new BinaryReader(new FileStream(imageFile, FileMode.Open, FileAccess.Read)))
                {
                    int magicNumber = ReadInt32(imageReader);
                    int imageNum = ReadInt32(imageReader);
                    int width = ReadInt32(imageReader);
                    int height = ReadInt32(imageReader); 
                    
                    byte[] imageData =  new byte[imageNum * width * height];
                    for(int i=0;i< imageNum * width * height;i++)
                    {
                        imageData[i] = imageReader.ReadByte();
                    }
                    return new Tuple<int, int, int, byte[]>(imageNum, width, height, imageData);
                }
            }

            public static void Init(string dataDir)
            {
                TrainLabel = ExtractLabels(dataDir + @"/train-labels-idx1-ubyte");
                TrainImage = ExtractImages(dataDir + @"/train-images-idx3-ubyte");
                
                ValidLabel = ExtractLabels(dataDir + @"/t10k-labels-idx1-ubyte");
                ValidImage = ExtractImages(dataDir + @"/t10k-images-idx3-ubyte");
                
            }

        }

        
}
