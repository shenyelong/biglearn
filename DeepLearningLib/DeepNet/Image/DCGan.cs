using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.Image
{
    /// <summary>
    /// CNN for Image.
    /// </summary>
    public class DCGan : ComputationGraph
    {
        public class GModel : CompositeNNStructure
        {
            public int nz = 100;

            public TensorConvStructure Conv1 = null;
            public TensorConvStructure Conv2 = null;
            public TensorConvStructure Conv3 = null;
            public TensorConvStructure Conv4 = null;
            public TensorConvStructure Conv5 = null;
            public GModel(DeviceType device)
            {
                int ngf = 64;
                int nc = 1;
                //TensorConvStructure(int fw, int fh, int inDepth, int outDepth, bool isBias, DeviceType device)
                // 1 * 1 * nz
                Conv1 = AddLayer(new TensorConvStructure(4, 4, ngf * 8, nz, false, device));
                // 4 * 4 * ngf * 8
                Conv2 = AddLayer(new TensorConvStructure(4, 4, ngf * 4, ngf * 8, false, device));
                // 8 * 8 * ngf * 4    
                Conv3 = AddLayer(new TensorConvStructure(4, 4, ngf * 2, ngf * 4, false, device));
                // 16 * 16 * ngf * 2
                Conv4 = AddLayer(new TensorConvStructure(4, 4, ngf, ngf * 2, false, device));
                // 32 * 32 * ngf 
                Conv5 = AddLayer(new TensorConvStructure(4, 4, nc, ngf, false, device));
                // 64 * 64 * 1
            }
            public void InitOptimizer(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);  
            }
        }

        public class Generator : ComputationGraph
        {
            public new GModel Model { get; set; }

            public Tensor4DData Input { get; set; }
            public Tensor4DData Output { get; set; }

            public Generator(GModel model, RunnerBehavior behavior) : base(behavior)
            {
                Behavior = behavior;  
                Model = model;  
            }

            public void BuildCG(int batchSize)
            {
                Input = new Tensor4DData(1, 1, Model.nz, batchSize, false, Behavior.Device);

                Logger.WriteLog("GNet input shape {0}", Input.Shape.ToStr());
                //public TensorTransConvRunner(TensorConvStructure model, Tensor4DData input, int pad, int stride, RunnerBehavior behavior) 
                TensorTransConvRunner transConv1Runner = new TensorTransConvRunner(Model.Conv1, Input, 0, 1, Behavior);
                AddRunner(transConv1Runner);
                Tensor4DData o1 = transConv1Runner.Output.Act(A_Func.Rectified, Session, Behavior);

                Logger.WriteLog("GNet o1 shape {0}", o1.Shape.ToStr());

                TensorTransConvRunner transConv2Runner = new TensorTransConvRunner(Model.Conv2, o1, 1, 2, Behavior);
                AddRunner(transConv2Runner);
                Tensor4DData o2 = transConv2Runner.Output.Act(A_Func.Rectified, Session, Behavior);

                Logger.WriteLog("GNet o2 shape {0}", o2.Shape.ToStr());

                TensorTransConvRunner transConv3Runner = new TensorTransConvRunner(Model.Conv3, o2, 1, 2, Behavior);
                AddRunner(transConv3Runner);
                Tensor4DData o3 = transConv3Runner.Output.Act(A_Func.Rectified, Session, Behavior);

                Logger.WriteLog("GNet o3 shape {0}", o3.Shape.ToStr());


                TensorTransConvRunner transConv4Runner = new TensorTransConvRunner(Model.Conv4, o3, 1, 2, Behavior);
                AddRunner(transConv4Runner);
                Tensor4DData o4 = transConv4Runner.Output.Act(A_Func.Rectified, Session, Behavior);
                
                Logger.WriteLog("GNet o4 shape {0}", o4.Shape.ToStr());


                TensorTransConvRunner transConv5Runner = new TensorTransConvRunner(Model.Conv5, o4, 1, 2, Behavior);
                AddRunner(transConv5Runner);
                Tensor4DData o5 = transConv5Runner.Output.Act(A_Func.Tanh, Session, Behavior);
                Output = o5;

                Logger.WriteLog("GNet output shape {0}", Output.Shape.ToStr());


                SetDelegateModel(Model);
            }
        }

        public class DModel : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            public TensorConvStructure Conv2 = null;
            public TensorConvStructure Conv3 = null;
            public TensorConvStructure Conv4 = null;
            public TensorConvStructure Conv5 = null;
            
            public DModel(DeviceType device)
            {
                int nc = 1;
                int ndf = 64;
                
                //TensorConvStructure(int fw, int fh, int inDepth, int outDepth, bool isBias, DeviceType device)
                Conv1 = AddLayer(new TensorConvStructure(4, 4, nc, ndf, false, device));
                Conv2 = AddLayer(new TensorConvStructure(4, 4, ndf, ndf * 2, false, device));
                Conv3 = AddLayer(new TensorConvStructure(4, 4, ndf * 2, ndf * 4, false, device));
                Conv4 = AddLayer(new TensorConvStructure(4, 4, ndf * 4, ndf * 8, false, device));
                Conv5 = AddLayer(new TensorConvStructure(4, 4, ndf * 8, 1, false, device));
            }

            public void InitOptimizer(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);  
            }
        }

        public class Discriminator : ComputationGraph
        {
            public new DModel Model { get; set; }

            public Tensor4DData Input { get; set; }
            public Tensor4DData Output { get; set; }

            public Discriminator(DModel model, RunnerBehavior behavior) : base(behavior)
            {
                Behavior = behavior;  
                Model = model;  
            }

            public void BuildCG(int batchSize)
            {
                //(int width, int height, int depth, int maxBatchSize, DeviceType device) 
                Input = new Tensor4DData(64, 64, 1, batchSize, Behavior.Device);

                Logger.WriteLog("DNet input shape {0}", Input.Shape.ToStr());

                //public TensorTransConvRunner(TensorConvStructure model, Tensor4DData input, int pad, int stride, RunnerBehavior behavior) 
                TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, Input, 1, 2, Behavior);
                AddRunner(conv1Runner);
                Tensor4DData o1 = conv1Runner.Output.Act(A_Func.Rectified, Session, Behavior);

                Logger.WriteLog("DNet o1 shape {0}", o1.Shape.ToStr());

                TensorConvRunner conv2Runner = new TensorConvRunner(Model.Conv2, o1, 1, 2, Behavior);
                AddRunner(conv2Runner);
                Tensor4DData o2 = conv2Runner.Output.Act(A_Func.Rectified, Session, Behavior);

                Logger.WriteLog("DNet o2 shape {0}", o2.Shape.ToStr());

                TensorConvRunner conv3Runner = new TensorConvRunner(Model.Conv3, o2, 1, 2, Behavior);
                AddRunner(conv3Runner);
                Tensor4DData o3 = conv3Runner.Output.Act(A_Func.Rectified, Session, Behavior);

                Logger.WriteLog("DNet o3 shape {0}", o3.Shape.ToStr());

                TensorConvRunner conv4Runner = new TensorConvRunner(Model.Conv4, o3, 1, 2, Behavior);
                AddRunner(conv4Runner);
                Tensor4DData o4 = conv4Runner.Output.Act(A_Func.Rectified, Session, Behavior);

                Logger.WriteLog("DNet o4 shape {0}", o4.Shape.ToStr());
                
                TensorConvRunner conv5Runner = new TensorConvRunner(Model.Conv5, o4, 0, 1, Behavior);
                AddRunner(conv5Runner);
                Tensor4DData o5 = conv5Runner.Output; //.Act(A_Func.Sigmoid, Session, Behavior);
                Output = o5;

                Logger.WriteLog("DNet Output shape {0}", Output.Shape.ToStr());

                SetDelegateModel(Model);
            }
        }

         
        Discriminator DNet { get; set; }
        Generator GNet { get; set; }
        
        public DCGan(GModel gmodel, DModel dmodel, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior;
            DNet = new Discriminator(dmodel, behavior);
            GNet = new Generator(gmodel, behavior);
        }

        CrossEntropyRunner PosCERunner { get; set; }
        CrossEntropyRunner NegCERunner { get; set; }
        
        CudaPieceFloat PosLabel { get; set; }
        CudaPieceFloat NegLabel { get; set; }
        HiddenBatchData flat { get; set; }
        //CrossEntropyRunner(CudaPieceFloat label, HiddenBatchData output, RunnerBehavior behavior)  
        public void BuildCG(int batchSize)
        {
            DNet.BuildCG(batchSize);
            GNet.BuildCG(batchSize);

            PosLabel = new CudaPieceFloat(batchSize, Behavior.Device);
            PosLabel.Init(1);

            NegLabel = new CudaPieceFloat(batchSize, Behavior.Device);
            NegLabel.Init(0);

            flat = DNet.Output.ToND().Reshape(new IntArgument("FLAT", 1), DNet.Output.Shape[3]).ToHDB();

            PosCERunner = new CrossEntropyRunner(PosLabel, flat, Behavior);
            PosCERunner.IsReportAccuracy = false;
            PosCERunner.IsReportBCE = true;
            PosCERunner.IsReportAvgProb = true;

            NegCERunner = new CrossEntropyRunner(NegLabel, flat, Behavior);
            NegCERunner.IsReportAccuracy = false;
            NegCERunner.IsReportBCE = true;
            NegCERunner.IsReportAvgProb = true;
        }
        
        public override void Init()
        {
            DNet.Init();
            PosCERunner.Init();
            NegCERunner.Init();
            GNet.Init();
        }

        public override void Complete()
        {
            DNet.Complete();
            PosCERunner.Complete();
            NegCERunner.Complete();
            GNet.Complete();
        }

        public float[] Run(IntPtr data, IntPtr noise, int batchSize)
        {
            float[] results = new float[6];
            int memsize = 64 * 64 * 1 * batchSize;
            int noisesize = 1 * 1 * 100 * batchSize;
            Cudalib.CudaCopyInFloat(DNet.Input.Output.CudaPtr, data, memsize);
            Cudalib.CudaCopyInFloat(GNet.Input.Output.CudaPtr, noise, noisesize);

            GNet.Forward();

            // update discrimator network. 
            DNet.Forward();
            DNet.CleanDeriv();
            PosCERunner.Forward();
            float errD_real = PosCERunner.bceLoss;
            float D_x = PosCERunner.avgProb;

            DNet.Backward(false);
            DNet.Update();
            
            //float errD_real = 0;
            //float D_x = 0;


            //float _l2_1 = Behavior.Computelib.DotProduct(GNet.Output.Output, GNet.Output.Output, memsize); 
            //Logger.WriteLog("check 1 step {0}", Math.Sqrt(_l2_1));

            Cudalib.CudaCopy(DNet.Input.Output.CudaPtr, GNet.Output.Output.CudaPtr, memsize, 0, 0); 
            DNet.Forward();
            DNet.CleanDeriv();
            NegCERunner.Forward();
            float errD_fake = NegCERunner.bceLoss;
            float D_G_z1 = NegCERunner.avgProb;
            DNet.Backward(false);
            DNet.Update();

            //float errD_fake = 0;
            //float D_G_z1 = 0;

            //DNet.BeforeOptimization();
            DNet.GradientRegular();
            DNet.AfterOptimization();
            DNet.ParameterRegular();

            // update generator network.
            

            DNet.Forward();
            
            DNet.CleanDeriv();
            GNet.CleanDeriv();
            Behavior.Computelib.Zero(DNet.Input.Deriv, DNet.Input.Deriv.Size);
            //Behavior.Computelib.Zero(GNet.Input.Deriv, GNet.Input.Deriv.Size);
            
            PosCERunner.Forward();
            float errG = PosCERunner.bceLoss;
            float D_G_z2 = PosCERunner.avgProb;
            
            //float _l2_1_1 = Behavior.Computelib.DotProduct(flat.Output.Data, flat.Output.Data, batchSize); 
            //Logger.WriteLog("check 1.1 step {0}", Math.Sqrt(_l2_1_1));


            //float _l2_1_2 = Behavior.Computelib.DotProduct(flat.Deriv.Data, flat.Deriv.Data, batchSize); 
            //Logger.WriteLog("check 1.2 step {0}", Math.Sqrt(_l2_1_2));
            
            DNet.Backward(false);
            DNet.Update();
            

            //float _l2_2 = Behavior.Computelib.DotProduct(DNet.Input.Output, DNet.Input.Output, memsize); 
            //Logger.WriteLog("check 2 step {0}", Math.Sqrt(_l2_2));


            //float _l2_3 = Behavior.Computelib.DotProduct(DNet.Input.Deriv, DNet.Input.Deriv, memsize); 
            //Logger.WriteLog("check 3 step {0}", Math.Sqrt(_l2_3));

            Behavior.Computelib.Zero(DNet.Model.Grad, DNet.Model.Grad.Size);
            Cudalib.CudaCopy(GNet.Output.Deriv.CudaPtr, DNet.Input.Deriv.CudaPtr, memsize, 0, 0); 
            
            GNet.Backward(false);
            GNet.Update();

            //float gradL2 = Behavior.Computelib.DotProduct(Model.Grad, Model.Grad, Model.Grad.Size);
            //float norm = (float)Math.Sqrt(gradL2);
            //float gradL2 = Behavior.Computelib.DotProduct(GNet.Model.Grad, GNet.Model.Grad, GNet.Model.Grad.Size); 
            //float norm = (float)Math.Sqrt(gradL2);
            //Logger.WriteLog("grad norm {0}", norm);

            //GNet.BeforeOptimization();
            GNet.GradientRegular();
            GNet.AfterOptimization();
            GNet.ParameterRegular();

            //float errG = 0;
            //float D_G_z2 = 0;

            results[0] = errD_real;
            results[1] = errD_fake;
            results[2] = errG;
            
            results[3] = D_x;
            results[4] = D_G_z1;
            results[5] = D_G_z2;

            return results;
        }

        public void Generate(IntPtr noise, IntPtr o, int batchSize)
        {
            int memsize = 64 * 64 * 1 * batchSize;
            int noisesize = 1 * 1 * 100 * batchSize;
            Cudalib.CudaCopyInFloat(GNet.Input.Output.CudaPtr, noise, noisesize);
            GNet.Forward();
            Cudalib.CudaCopyOutFloat(GNet.Output.Output.CudaPtr, o, memsize);
        }
    }
}
