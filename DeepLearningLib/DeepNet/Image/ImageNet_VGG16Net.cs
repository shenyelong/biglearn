using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Image
{
    unsafe public class ImageNet_VGG16Net : ComputationGraph
    {
        public string name = "ImageNet_VGG16Net";

        public class VGG16Model : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            public TensorConvStructure Conv2 = null;
            
            public TensorConvStructure Conv3 = null;
            public TensorConvStructure Conv4 = null;
            
            public TensorConvStructure Conv5 = null;
            public TensorConvStructure Conv6 = null;
            public TensorConvStructure Conv7 = null;
            
            public TensorConvStructure Conv8 = null;
            public TensorConvStructure Conv9 = null;
            public TensorConvStructure Conv10 = null;
            
            public TensorConvStructure Conv11 = null;
            public TensorConvStructure Conv12 = null;
            public TensorConvStructure Conv13 = null;
            
            public LayerStructure FC6 = null;
            public LayerStructure FC7 = null;
            public LayerStructure FC8 = null;
            
            public int Category { get; set; }

            // how to make sure the model parameters allocated into a flat array.
            public VGG16Model(int category, DeviceType device)
            {
                Category = category;

                Conv1 = AddLayer(new TensorConvStructure(3, 3, 3, 64, DNNInitMethod.Kaiming_Init, device));
                Conv2 = AddLayer(new TensorConvStructure(3, 3, 64, 64, DNNInitMethod.Kaiming_Init, device));

                Conv3 = AddLayer(new TensorConvStructure(3, 3, 64, 128, DNNInitMethod.Kaiming_Init, device));
                Conv4 = AddLayer(new TensorConvStructure(3, 3, 128, 128, DNNInitMethod.Kaiming_Init, device));

                Conv5 = AddLayer(new TensorConvStructure(3, 3, 128, 256, DNNInitMethod.Kaiming_Init, device));
                Conv6 = AddLayer(new TensorConvStructure(3, 3, 256, 256, DNNInitMethod.Kaiming_Init, device));
                Conv7 = AddLayer(new TensorConvStructure(3, 3, 256, 256, DNNInitMethod.Kaiming_Init, device));

                Conv8 = AddLayer(new TensorConvStructure(3, 3, 256, 512, DNNInitMethod.Kaiming_Init, device));
                Conv9 = AddLayer(new TensorConvStructure(3, 3, 512, 512, DNNInitMethod.Kaiming_Init, device));
                Conv10 = AddLayer(new TensorConvStructure(3, 3, 512, 512, DNNInitMethod.Kaiming_Init, device));

                Conv11 = AddLayer(new TensorConvStructure(3, 3, 512, 512, DNNInitMethod.Kaiming_Init, device));
                Conv12 = AddLayer(new TensorConvStructure(3, 3, 512, 512, DNNInitMethod.Kaiming_Init, device));
                Conv13 = AddLayer(new TensorConvStructure(3, 3, 512, 512, DNNInitMethod.Kaiming_Init, device));
                
                FC6 = AddLayer(new LayerStructure(512 * 7 * 7, 4096, A_Func.Rectified, true, DNNInitMethod.Kaiming_Init, device));
                FC7 = AddLayer(new LayerStructure(4096, 4096, A_Func.Rectified, true, DNNInitMethod.Kaiming_Init, device));
                FC8 = AddLayer(new LayerStructure(4096, category, A_Func.Linear, true, DNNInitMethod.Kaiming_Init, device));
            }

            public VGG16Model(int category, string meta_file, string bin_file, DeviceType device) : this(category, device)
            {
                using(StreamReader metaReader = new StreamReader(meta_file))
                using(BinaryReader binReader = new BinaryReader(new FileStream(bin_file, FileMode.Open, FileAccess.Read)))
                {
                    while(!metaReader.EndOfStream)
                    {
                        string[] items = metaReader.ReadLine().Split('\t');
                        string itemName = items[0];
                        int itemSize = int.Parse(items[1]);

                        // layer 1-1.
                        if(itemName.Equals("vgg_16/conv1/conv1_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv1.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv1.Filter.CpuPtr, new int[] { 64, 3, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            
                            BasicMathlib.Scale_Vector(Conv1.Filter.CpuPtr, itemSize, 255);

                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv1/conv1_1/biases"))
                        {
                            Conv1.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);                            
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 1-2
                        if(itemName.Equals("vgg_16/conv1/conv1_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv2.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv2.Filter.CpuPtr, new int[] { 64, 64, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv1/conv1_2/biases"))
                        {
                            Conv2.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        //layer 2-1
                        if(itemName.Equals("vgg_16/conv2/conv2_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv3.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv3.Filter.CpuPtr, new int[] { 128, 64, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv2/conv2_1/biases"))
                        {
                            Conv3.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 2-2
                        if(itemName.Equals("vgg_16/conv2/conv2_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv4.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv4.Filter.CpuPtr, new int[] { 128, 128, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv2/conv2_2/biases"))
                        {
                            Conv4.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        //layer 3-1
                        if(itemName.Equals("vgg_16/conv3/conv3_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv5.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv5.Filter.CpuPtr, new int[] { 256, 128, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv3/conv3_1/biases"))
                        {
                            Conv5.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 3-2
                        if(itemName.Equals("vgg_16/conv3/conv3_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv6.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv6.Filter.CpuPtr, new int[] { 256, 256, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv3/conv3_2/biases"))
                        {
                            Conv6.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 3-3
                        if(itemName.Equals("vgg_16/conv3/conv3_3/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv7.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv7.Filter.CpuPtr, new int[] { 256, 256, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv3/conv3_3/biases"))
                        {
                            Conv7.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        //layer 4-1
                        if(itemName.Equals("vgg_16/conv4/conv4_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv8.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv8.Filter.CpuPtr, new int[] { 512, 256, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv4/conv4_1/biases"))
                        {
                            Conv8.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 4-2
                        if(itemName.Equals("vgg_16/conv4/conv4_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv9.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv9.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv4/conv4_2/biases"))
                        {
                            Conv9.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 4-3
                        if(itemName.Equals("vgg_16/conv4/conv4_3/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv10.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv10.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv4/conv4_3/biases"))
                        {
                            Conv10.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        //layer 5-1
                        if(itemName.Equals("vgg_16/conv5/conv5_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv11.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv11.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv5/conv5_1/biases"))
                        {
                            Conv11.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 5-2
                        if(itemName.Equals("vgg_16/conv5/conv5_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv12.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv12.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv5/conv5_2/biases"))
                        {
                            Conv12.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 5-3
                        if(itemName.Equals("vgg_16/conv5/conv5_3/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv13.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv13.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/conv5/conv5_3/biases"))
                        {
                            Conv13.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        // fc6
                        if(itemName.Equals("vgg_16/fc6/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            FC6.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(FC6.weight.CpuPtr, new int[] { 4096, 512, 7, 7 }, new int[] { 0, 2, 3, 1 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/fc6/biases"))
                        {
                            FC6.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        // fc7
                        if(itemName.Equals("vgg_16/fc7/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            FC7.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/fc7/biases"))
                        {
                            FC7.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        // fc8
                        if(itemName.Equals("vgg_16/fc8/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            FC8.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_16/fc8/biases"))
                        {
                            FC8.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        Logger.WriteLog("Component is not found {0} : size {1}, skip...", itemName, itemSize);
                        binReader.ReadBytes(sizeof(float) * itemSize);

                    }
                }
                Conv1.SyncFromCPU();
                Conv2.SyncFromCPU();
                Conv3.SyncFromCPU();
                Conv4.SyncFromCPU();
                Conv5.SyncFromCPU();
                Conv6.SyncFromCPU();
                Conv7.SyncFromCPU();
                Conv8.SyncFromCPU();
                Conv9.SyncFromCPU();
                Conv10.SyncFromCPU();
                Conv11.SyncFromCPU();
                Conv12.SyncFromCPU();
                Conv13.SyncFromCPU();

                FC6.SyncFromCPU();
                FC7.SyncFromCPU();
                FC8.SyncFromCPU();

            }
        }

        public Tensor4DData InputImages { get; set; }
        public SparseVectorData Label { get; set; }

        public new VGG16Model Model { get; set; }
        public HiddenBatchData O { get; set; }

        public int Width { get { return InputImages.Width; } }
        public int Height { get { return InputImages.Height; } }
        public int Channel { get { return InputImages.Depth; } }

        public ImageNet_VGG16Net(VGG16Model model, RunnerBehavior behavior) : base(behavior)
        {
            Model = model; // Model = new VGG11Model(Behavior); 
            SetDelegateModel(Model);
        }

        public ImageNet_VGG16Net(VGG16Model model, StructureLearner learner, RunnerBehavior behavior) : base(behavior)
        {
            Model = model;
            AllocateOptimizer(model, learner);
        }

        //"""Select conv1_1 ~ conv5_1 activation maps.""" 
        Dictionary<string, Tensor4DData> LayerFeatuers = new Dictionary<string, Tensor4DData>();

        public HiddenBatchData BuildFeaturizer()
        {
            // x --> 3 * 224 * 224
            TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, InputImages, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv1Runner);

            LayerFeatuers.Add("conv-1-1", conv1Runner.Output);

            // x --> 64 * 224 * 224
            TensorConvRunner conv2Runner = new TensorConvRunner(Model.Conv2, conv1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv2Runner);

            LayerFeatuers.Add("conv-1-2", conv2Runner.Output);

            // x --> 64 * 224 * 224
            TensorMaxPooling2DRunner pool1Runner = new TensorMaxPooling2DRunner(conv2Runner.Output, 2, 2, Behavior);
            AddRunner(pool1Runner);

            // x --> 64 * 112 * 112
            TensorConvRunner conv3Runner = new TensorConvRunner(Model.Conv3, pool1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv3Runner);
            
            LayerFeatuers.Add("conv-2-1", conv3Runner.Output);    

            // x --> 128 * 112 * 112
            TensorConvRunner conv4Runner = new TensorConvRunner(Model.Conv4, conv3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv4Runner);

            LayerFeatuers.Add("conv-2-2", conv4Runner.Output);    

            // x --> 128 * 112 * 112
            TensorMaxPooling2DRunner pool2Runner = new TensorMaxPooling2DRunner(conv4Runner.Output, 2, 2, Behavior);
            AddRunner(pool2Runner);
            // x --> 128 * 56 * 56
            TensorConvRunner conv5Runner = new TensorConvRunner(Model.Conv5, pool2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv5Runner);

            LayerFeatuers.Add("conv-3-1", conv5Runner.Output);    

            // x --> 256 * 56 * 56
            TensorConvRunner conv6Runner = new TensorConvRunner(Model.Conv6, conv5Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv6Runner);

            LayerFeatuers.Add("conv-3-2", conv6Runner.Output);    

            // x --> 256 * 56 * 56
            TensorConvRunner conv7Runner = new TensorConvRunner(Model.Conv7, conv6Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv7Runner);

            LayerFeatuers.Add("conv-3-3", conv7Runner.Output);    

            // x --> 256 * 56 * 56
            TensorMaxPooling2DRunner pool3Runner = new TensorMaxPooling2DRunner(conv7Runner.Output, 2, 2, Behavior);
            AddRunner(pool3Runner);
            // x --> 256 * 28 * 28
            TensorConvRunner conv8Runner = new TensorConvRunner(Model.Conv8, pool3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv8Runner);

            LayerFeatuers.Add("conv-4-1", conv8Runner.Output);    

            // x --> 512 * 28 * 28
            TensorConvRunner conv9Runner = new TensorConvRunner(Model.Conv9, conv8Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv9Runner);
            
            LayerFeatuers.Add("conv-4-2", conv9Runner.Output);    
            
            // x --> 512 * 28 * 28
            TensorConvRunner conv10Runner = new TensorConvRunner(Model.Conv10, conv9Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv10Runner);
            
            LayerFeatuers.Add("conv-4-3", conv10Runner.Output);    

            // x --> 512 * 28 * 28
            TensorMaxPooling2DRunner pool4Runner = new TensorMaxPooling2DRunner(conv10Runner.Output, 2, 2, Behavior);
            AddRunner(pool4Runner);
            
            // x --> 512 * 14 * 14
            TensorConvRunner conv11Runner = new TensorConvRunner(Model.Conv11, pool4Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv11Runner);
            
            LayerFeatuers.Add("conv-5-1", conv11Runner.Output);    

            // x --> 512 * 14 * 14
            TensorConvRunner conv12Runner = new TensorConvRunner(Model.Conv12, conv11Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv12Runner);
            
            LayerFeatuers.Add("conv-5-2", conv12Runner.Output);    

            // x --> 512 * 14 * 14
            TensorConvRunner conv13Runner = new TensorConvRunner(Model.Conv13, conv12Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv13Runner);
            
            LayerFeatuers.Add("conv-5-3", conv13Runner.Output);    

            // x --> 512 * 14 * 14 
            TensorMaxPooling2DRunner pool5Runner = new TensorMaxPooling2DRunner(conv13Runner.Output, 2, 2, Behavior);
            AddRunner(pool5Runner);
            
            // x --> 512 * 7 * 7 
            NdArrayData poolOut = pool5Runner.Output.ToND();

            int fc_flat_dim = poolOut.Shape[0].Default * poolOut.Shape[1].Default * poolOut.Shape[2].Default;

            HiddenBatchData flatEmbed = poolOut.Reshape(new IntArgument("FLAT_DIM", fc_flat_dim), poolOut.Shape[3]).ToHDB();

            FullyConnectHiddenRunner<HiddenBatchData> fc6Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FC6, flatEmbed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fc6Runner);

            HiddenBatchData o1 = fc6Runner.Output;

            //dropout.
            {
                TensorDropoutRunner drop1 = new TensorDropoutRunner(o1.ToT4D(), 0.5f, Behavior);
                AddRunner(drop1);
                o1 = drop1.Output.ToHDB();
            }

            FullyConnectHiddenRunner<HiddenBatchData> fc7Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FC7, o1, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fc7Runner);

            HiddenBatchData o2 = fc7Runner.Output;
            //dropout.
            {
                TensorDropoutRunner drop2 = new TensorDropoutRunner(o2.ToT4D(), 0.5f, Behavior);
                AddRunner(drop2);
                o2 = drop2.Output.ToHDB();
            }

            FullyConnectHiddenRunner<HiddenBatchData> fc8Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FC8, o2, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fc8Runner);            

            O = fc8Runner.Output;

            return O;
        }
        
        HiddenBatchData[] StyleFeatures { get; set; }
        Tensor4DData[] ContentFeatures { get; set; }
        MSERunner[] ContentLoss { get; set; }
        MSERunner[] StyleLoss { get; set; }

        public void BuildFeatureGraph(int width, int height, int channel, string[] layerNames, int featureNum)
        {
            InputImages = new Tensor4DData(width, height, channel, 1, false, Behavior.Device);
            BuildFeaturizer();
            
            StyleFeatures = new HiddenBatchData[featureNum];
            ContentFeatures = new Tensor4DData[featureNum];

            for(int i = 0; i < featureNum; i++)
            {
                ContentFeatures[i] = LayerFeatuers[layerNames[i]];
                StyleFeatures[i] = new HiddenBatchData(ContentFeatures[i].Depth, ContentFeatures[i].Depth, true, Behavior.Device);
            }
        }

        public void BuildTransferGraph(int width, int height, int channel, string[] layerNames, Tensor4DData[] conFeas, HiddenBatchData[] styleFeas, float style_weight, int featureNum)
        {
            InputImages = new Tensor4DData(width, height, channel, 1, true, Behavior.Device);
            BuildFeaturizer();

            StyleFeatures = new HiddenBatchData[featureNum];
            ContentFeatures = new Tensor4DData[featureNum];

            ContentLoss = new MSERunner[featureNum];
            StyleLoss = new MSERunner[featureNum];

            for(int i = 0; i < featureNum; i++)
            {
                ContentFeatures[i] = LayerFeatuers[layerNames[i]];
                ContentLoss[i] = new MSERunner(conFeas[i].Output, ContentFeatures[i].Output, ContentFeatures[i].Deriv, ContentFeatures[i].MaxLength, 1.0f, Behavior); 
                AddObjective(ContentLoss[i]); 
                
                IntArgument depth = ContentFeatures[i].DepthArg;
                IntArgument wh = new IntArgument("WH", ContentFeatures[i].Width * ContentFeatures[i].Height);
                Logger.WriteLog("Feature Map : {0}, Depth : {1}, Height : {2}, Width : {3}, style weight : {4} ", layerNames[i], depth.Default, ContentFeatures[i].Height, ContentFeatures[i].Width, style_weight * 1.0f / (depth.Default * wh.Default));

                MatrixData content_matrix = ContentFeatures[i].ToND().Reshape(new IntArgument[] { wh, depth }).ToMD();

                StyleFeatures[i] = content_matrix.MatMul(0, content_matrix, 1, Session, Behavior).ToHDB();
                StyleLoss[i] = new MSERunner(styleFeas[i].Output.Data, StyleFeatures[i].Output.Data, StyleFeatures[i].Deriv.Data, depth.Default * depth.Default, style_weight * 1.0f / (depth.Default * wh.Default), Behavior); 
                AddObjective(StyleLoss[i]);
            }
        }

        public void SetStyleOptimizer(StructureLearner learner)
        {
            AddOptimizer(GradientOptimizer.CreateLocalOptimizer(InputImages.Output, InputImages.Deriv, learner, Behavior));
        }

        public float[] StyleTransfer(IntPtr target)
        {
            InputImages.BatchSize = 1;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, target, memsize);
            // ComputeLib.Zero(InputImages.Deriv, memsize);
            
            //for(int step = 0; step < totalStep; step++)
            //{
            StepV3Run();
            StepV3Update();
            //}

            //InputImages.Output.Init(1.0f);
            Cudalib.CudaCopyOutFloat(InputImages.Output.CudaPtr, target, memsize);

            //(float)ContentLoss.Select(c => c.ObjectiveScore).Sum()
            return new float[] { (float)ContentLoss.Select(c => c.ObjectiveScore).Sum(), (float)StyleLoss.Select(s => s.ObjectiveScore).Sum() };

        }

        public Tensor4DData[] ExtractContentFeature(IntPtr input)
        {
            InputImages.BatchSize = 1;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, input, memsize);

            Forward();

            return ContentFeatures;
        }


        public HiddenBatchData[] ExtractStyleFeature(IntPtr input)
        {
            InputImages.BatchSize = 1;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, input, memsize);

            Forward();

            for(int i = 0; i < ContentFeatures.Length; i++)
            {
                ComputeLib.Sgemm(ContentFeatures[i].Output, 0, ContentFeatures[i].Output, 0, StyleFeatures[i].Output.Data, 0, 
                                ContentFeatures[i].Depth, ContentFeatures[i].Width * ContentFeatures[i].Height, ContentFeatures[i].Depth,
                                0, 1, false, true);
            }
            return StyleFeatures;
        }


        // classifier.
        public void BuildCG(int width, int height, int channel, int batchSize)
        {
            InputImages = new Tensor4DData(width, height, channel, batchSize, false, Behavior.Device);
            
            Label = new SparseVectorData(batchSize, Behavior.Device, false);
            Label.Value.Init(1);

            BuildFeaturizer();

            AddObjective(new SoftmaxObjRunner(Label, O, Behavior)); 
            
            //SetDelegateModel(Model);
        }

        // predict image.
        public unsafe void Predict(IntPtr batch, IntPtr o, int batchSize)
        {
            InputImages.BatchSize = batchSize;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, batch, memsize);

            StepV3Run();

            Cudalib.CudaCopyOutFloat(O.Output.Data.CudaPtr, o, batchSize * Model.Category);
        }

        public float Train(IntPtr batch, IntPtr target, IntPtr o, int batchSize, bool isUpdate)
        {
            InputImages.BatchSize = batchSize;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, batch, memsize);

            long * p = (long * )target.ToPointer();
            {
                for(int i = 0; i < batchSize; i++)
                {
                    Label.Idx[i] = i * Model.Category + (int)p[i];
                    //Logger.WriteLog("label {0}, idx {1}", (int)p[i], Label.Idx[i]);
                }
            }
            Label.Length = batchSize;
            Label.Idx.SyncFromCPU();

            float loss = StepV3Run();

            if(isUpdate) StepV3Update();
            
            if(o != IntPtr.Zero)
            {
                Cudalib.CudaCopyOutFloat(O.Output.Data.CudaPtr, o, batchSize * Model.Category);
            }

            return loss;
        }

    }

    public class DistImageNet_VGG16Net
    {
        public ImageNet_VGG16Net[] Nets { get; set; }
        int DeviceNum { get { return DeviceIds.Length; } }
        int[] DeviceIds { get; set; }

        IntPtr Comm { get; set; }
        CudaPieceFloat[] Grads { get; set; }

        ImageNet_VGG16Net.VGG16Model Model { get; set; }

        public unsafe DistImageNet_VGG16Net(ImageNet_VGG16Net.VGG16Model model, StructureLearner learner, int deviceNum)
        {
            Model = model;
            DeviceIds = new int[deviceNum];
            for(int i = 0; i < deviceNum; i++) DeviceIds[i] = i;

            fixed (int * pDevice = & DeviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, deviceNum);
            }
            Grads = new CudaPieceFloat[deviceNum];
            Nets = new ImageNet_VGG16Net[deviceNum];

            Parallel.For(0, DeviceNum, i =>  //foreach(int devId in DeviceIds)
            {
                int devId = DeviceIds[i];

                Logger.WriteLog("Create Device {0}", devId);
                DeviceType device = MathOperatorManager.SetDevice(devId);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

                ImageNet_VGG16Net.VGG16Model device_model = new ImageNet_VGG16Net.VGG16Model(model.Category, device);
                device_model.CopyFrom(model);

                Grads[i] = device_model.AllocateGradient(behavior.Device); // DeviceType device) 
                //InitOptimizer(learner, behavior);

                Nets[i] = new ImageNet_VGG16Net(device_model, learner, behavior);
            });
        }

        public void BuildCG(int width, int height, int channel, int batchSize)
        {
            int subBatchSize = batchSize / DeviceNum;
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].BuildCG(width, height, channel, subBatchSize);
            });
        }

        public void Init()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Init();
            });
        }

        public void Complete()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Complete();
            });
        }

        public void SetTrainMode()
        {
            foreach(ImageNet_VGG16Net net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Train;
            }
        }

        public void SetPredictMode()
        {
            foreach(ImageNet_VGG16Net net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Predict;
            }
        } 

        public void AdjustLR(float lr)
        {
            foreach(ImageNet_VGG16Net net in Nets)
            {
                net.AdjustLR(lr);
            }
        }

        public void SaveModel(string path)
        {
            Nets[0].Model.Save(path);
        }

        //10.93.128.213
        //Predict(IntPtr batch, IntPtr o, int batchSize)
        public unsafe void Predict(IntPtr batch, IntPtr o, int batchSize)
        {
            int subBatchSize = batchSize / DeviceNum;
            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                Nets[i].Predict(IntPtr.Add(batch, sizeof(float) * i * subBatchSize * Nets[i].Width * Nets[i].Height * Nets[i].Channel),  
                                IntPtr.Add(o, sizeof(float) * i * subBatchSize * Nets[i].Model.Category), 
                                subBatchSize);
            });

        }

        public unsafe float Train(IntPtr batch, IntPtr target, IntPtr o, int batchSize)
        {
            float[] losses = new float[DeviceNum];
            
            int subBatchSize = batchSize / DeviceNum;

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                float loss = Nets[i].Train(IntPtr.Add(batch, sizeof(float) * i * subBatchSize * Nets[i].Width * Nets[i].Height * Nets[i].Channel),  
                                           IntPtr.Add(target, sizeof(long) * i * subBatchSize),  
                                           IntPtr.Add(o, sizeof(float) * i * subBatchSize * Nets[i].Model.Category), 
                                           subBatchSize, false);

                losses[i] = loss;

                Cudalib.AllReduce(Grads[i].CudaPtr, Grads[i].Size, i, Comm);

                Nets[i].StepV3Update();
            });

            return losses.Average();
        }


    }





}
