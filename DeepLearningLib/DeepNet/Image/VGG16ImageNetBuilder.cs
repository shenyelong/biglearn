using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Image
{
    /// <summary>
    /// CNN for Image.
    /// </summary>
    public class VGG16ImageNetBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }

            public static string TrainData { get { return Argument["TRAIN-DATA"].Value; } }
            public static string TrainLabel { get { return Argument["TRAIN-LABEL"].Value; } }

            public static string ValidData { get { return Argument["VALID-DATA"].Value; } }
            public static string ValidLabel { get { return Argument["VALID-LABEL"].Value; } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }

            public static int BatchSize { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("TRAIN-DATA", new ParameterArgument(string.Empty, "Train Image Data"));
                Argument.Add("TRAIN-LABEL", new ParameterArgument(string.Empty, "Train Image Label"));

                Argument.Add("VALID-DATA", new ParameterArgument(string.Empty, "Valid Image Data"));
                Argument.Add("VALID-LABEL", new ParameterArgument(string.Empty, "Valid Image Label"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                
                Argument.Add("BATCH-SIZE", new ParameterArgument("32", "Mini batch size."));
            }
        }
        
        public override BuilderType Type { get { return BuilderType.VGG16_IMAGENET; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class VGG16Model : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            public TensorConvStructure Conv2 = null;
            public TensorConvStructure Conv3 = null;
            public TensorConvStructure Conv4 = null;
            public TensorConvStructure Conv5 = null;
            public TensorConvStructure Conv6 = null;
            public TensorConvStructure Conv7 = null;
            public TensorConvStructure Conv8 = null;
            public TensorConvStructure Conv9 = null;
            public TensorConvStructure Conv10 = null;
            public TensorConvStructure Conv11 = null;
            public TensorConvStructure Conv12 = null;
            public TensorConvStructure Conv13 = null;
            public LayerStructure FClayer1 = null;
            public LayerStructure FClayer2 = null;
            public LayerStructure FClayer3 = null;
            //
            public VGG16Model(DeviceType device)
            {
                Conv1 = AddLayer(new TensorConvStructure(3, 3, 3, 64, device));
                Conv2 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));
                Conv3 = AddLayer(new TensorConvStructure(3, 3, 64, 128, device));
                Conv4 = AddLayer(new TensorConvStructure(3, 3, 128, 128, device));
                Conv5 = AddLayer(new TensorConvStructure(3, 3, 128, 256, device));
                Conv6 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));
                Conv7 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));
                Conv8 = AddLayer(new TensorConvStructure(3, 3, 256, 512, device));
                Conv9 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv10 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv11 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv12 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv13 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));

                FClayer1 = AddLayer(new LayerStructure(25088, 4096, A_Func.Rectified, N_Type.Fully_Connected, 1, 0, true, device));
                FClayer2 = AddLayer(new LayerStructure(4096, 4096, A_Func.Rectified, N_Type.Fully_Connected, 1, 0, true, device));
                FClayer3 = AddLayer(new LayerStructure(4096, 1000, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device));
            }

            // public VGG16Model(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
            //     Conv1 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv2 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv3 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv4 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv5 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv6 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv7 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv8 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv9 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv10 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv11 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv12 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv13 = DeserializeModel<TensorConvStructure>(reader, device); 
                
            //     FClayer1 = DeserializeModel<LayerStructure>(reader, device); 
            //     FClayer2 = DeserializeModel<LayerStructure>(reader, device); 
            //     FClayer3 = DeserializeModel<LayerStructure>(reader, device); 
            // }
        }

        // class LabelConvertRunner : StructRunner
        // {
        //     public DenseBatchData Inlabel { get; set; }
        //     public SparseVectorData OutLabel { get; set; }
        //     int CategoryNum { get; set; }
        //     public LabelConvertRunner(DenseBatchData inLabel, int categoryNum, RunnerBehavior behavior)
        //         : base(Structure.Empty, behavior)
        //     {
        //         InLabel = inLabel;
        //         OutLabel = new SparseVectorData(InLabel.MAX_BATCHSIZE, behavior.Device, false);
        //         OutLabel.Value.Init(1);
        //         CategoryNum = categoryNum;
        //     }
        //     public override void Forward()
        //     {
        //         InLabel.Data.SyncToCPU();
        //         for(int i = 0; i < InLabel.BatchSize; i++)
        //         {
        //             OutLabel.Idx[i] = i * CategoryNum + (int)InLabel.Data[i];
        //         }
        //         OutLabel.Length = BatchSize;
        //         OutLabel.Idx.SyncFromCPU();
        //     }
        // }


        public static ComputationGraph BuildComputationGraph(string imageFile, string labelFile, VGG16Model model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph(Behavior);

            ImageNetDataCashier imgCashier = new ImageNetDataCashier(new BinaryReader(new FileStream(imageFile, FileMode.Open, FileAccess.Read)), 
                                                                    1281167, BuilderParameters.BatchSize, 3, 224, 224, 9823, 128, "imgCashier");

            /**************** Get Data from DataCashier *********/
            Tensor4DData image = (Tensor4DData)cg.AddDataRunner(new DataRunner<Tensor4DData>(imgCashier, Behavior));

            ImageNetLabelRunner labelRunner = new ImageNetLabelRunner(labelFile, BuilderParameters.BatchSize, imgCashier.DataShuffle, 1281167, 1000, Behavior);
            cg.AddRunner(labelRunner);
            SparseVectorData label = labelRunner.Label;

            //DenseBatchData label = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData>(labelData, Behavior));

            // x --> 3 * 224 * 224,
            TensorConvRunner conv1Runner = new TensorConvRunner(model.Conv1, image, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv1Runner);

            // x --> 64 * 224 * 224
            TensorConvRunner conv2Runner = new TensorConvRunner(model.Conv2, conv1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv2Runner);
            
            // x --> 64 * 224 * 224
            TensorMaxPooling2DRunner pool1Runner = new TensorMaxPooling2DRunner(conv2Runner.Output, 2, 2, Behavior);
            cg.AddRunner(pool1Runner);

            // x --> 64 * 112 * 112
            TensorConvRunner conv3Runner = new TensorConvRunner(model.Conv3, pool1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv3Runner);

            // x --> 128 * 112 * 112
            TensorConvRunner conv4Runner = new TensorConvRunner(model.Conv4, conv3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv4Runner);

            // x --> 128 * 112 * 112
            TensorMaxPooling2DRunner pool2Runner = new TensorMaxPooling2DRunner(conv4Runner.Output, 2, 2, Behavior);
            cg.AddRunner(pool2Runner);

            // x --> 128 * 56 * 56
            TensorConvRunner conv5Runner = new TensorConvRunner(model.Conv5, pool2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv5Runner);

            // x --> 256 * 56 * 56
            TensorConvRunner conv6Runner = new TensorConvRunner(model.Conv6, conv5Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv6Runner);

            // x --> 256 * 56 * 56
            TensorConvRunner conv7Runner = new TensorConvRunner(model.Conv7, conv6Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv7Runner);
            
            // x --> 256 * 56 * 56
            TensorMaxPooling2DRunner pool3Runner = new TensorMaxPooling2DRunner(conv7Runner.Output, 2, 2, Behavior);
            cg.AddRunner(pool3Runner);

            // x --> 256 * 28 * 28
            TensorConvRunner conv8Runner = new TensorConvRunner(model.Conv8, pool3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv8Runner);

            // x --> 512 * 28 * 28
            TensorConvRunner conv9Runner = new TensorConvRunner(model.Conv9, conv8Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv9Runner);

            // x --> 512 * 28 * 28
            TensorConvRunner conv10Runner = new TensorConvRunner(model.Conv10, conv9Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv10Runner);

            // x --> 512 * 28 * 28
            TensorMaxPooling2DRunner pool4Runner = new TensorMaxPooling2DRunner(conv10Runner.Output, 2, 2, Behavior);
            cg.AddRunner(pool4Runner);

            // x --> 512 * 14 * 14
            TensorConvRunner conv11Runner = new TensorConvRunner(model.Conv11, pool4Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv11Runner);

            // x --> 512 * 14 * 14
            TensorConvRunner conv12Runner = new TensorConvRunner(model.Conv12, conv11Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv12Runner);

            // x --> 512 * 14 * 14
            TensorConvRunner conv13Runner = new TensorConvRunner(model.Conv13, conv12Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv13Runner);

            // x --> 512 * 14 * 14 
            TensorMaxPooling2DRunner pool5Runner = new TensorMaxPooling2DRunner(conv13Runner.Output, 2, 2, Behavior);
            cg.AddRunner(pool5Runner);

            // x --> 512 * 7 * 7 
            NdArrayData poolOut = pool5Runner.Output.ToND();

            int fc_flat_dim = poolOut.Shape[0].Default * poolOut.Shape[1].Default * poolOut.Shape[2].Default;
            Logger.WriteLog("pool width {0}, pool height {1}, pool depth {2}, FC input {3}, batch size {4}", 
                        poolOut.Shape[0].Default, poolOut.Shape[1].Default, poolOut.Shape[2].Default,
                        fc_flat_dim, poolOut.Shape[3].Default);

            HiddenBatchData flatEmbed = poolOut.Reshape(new IntArgument("FLAT_DIM", fc_flat_dim), poolOut.Shape[3]).ToHDB();

            FullyConnectHiddenRunner<HiddenBatchData> fc1Runner = new FullyConnectHiddenRunner<HiddenBatchData>(model.FClayer1, flatEmbed, Behavior);
            cg.AddRunner(fc1Runner);
            HiddenBatchData o1 = fc1Runner.Output; 

            if(Behavior.RunMode == DNNRunMode.Train)
            {
                TensorDropoutRunner drop1 = new TensorDropoutRunner(o1.ToT4D(), 0.5f, Behavior);
                cg.AddRunner(drop1);
                o1 = drop1.Output.ToHDB();
            }

            //DropoutRunner  
            FullyConnectHiddenRunner<HiddenBatchData> fc2Runner = new FullyConnectHiddenRunner<HiddenBatchData>(model.FClayer2, o1, Behavior); // fc1Runner.Output, Behavior);
            cg.AddRunner(fc2Runner);
            HiddenBatchData o2 = fc2Runner.Output;

            if(Behavior.RunMode == DNNRunMode.Train)
            {
                TensorDropoutRunner drop2 = new TensorDropoutRunner(o2.ToT4D(), 0.5f, Behavior);
                cg.AddRunner(drop2);
                o2 = drop2.Output.ToHDB();
            }

            FullyConnectHiddenRunner<HiddenBatchData> fc3Runner = new FullyConnectHiddenRunner<HiddenBatchData>(model.FClayer3, o2, Behavior); // fc2Runner.Output, Behavior);
            cg.AddRunner(fc3Runner);
            HiddenBatchData o3 = fc3Runner.Output;

            //LabelConvertRunner labelRunner = new LabelConvertRunner(label, 1000, Behavior);
            //cg.AddRunner(labelRunner);
            
            if(Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddObjective(new SoftmaxObjRunner(label, o3, Behavior)); 
            }
            else
            {
                cg.AddRunner(new AccuracyRunner(label.Idx, o3, Behavior));
            }
 
            cg.SetDelegateModel(model);
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceBehavior behavior = new DeviceBehavior(BuilderParameters.GPUID);

            Logger.WriteLog("Loading Training/Validation Data.");
            Logger.WriteLog("Load Data Finished.");

            VGG16Model model = new VGG16Model(behavior.Device);
            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior.TrainMode);

            ComputationGraph train_cg = BuildComputationGraph(
                    BuilderParameters.TrainData, BuilderParameters.TrainLabel, model, behavior.TrainMode);

            ComputationGraph valid_cg = BuildComputationGraph(
                    BuilderParameters.ValidData, BuilderParameters.ValidLabel, model, behavior.PredictMode);

            //double best_score = 0;
            //double best_iter = -1;
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                double loss = train_cg.Execute();
                Logger.WriteLog("Train Loss {0}", loss);

                valid_cg.Execute();

                //double score = valid_cg.Execute();
                //Logger.WriteLog("Prediction Accuracy {0}, at Iteration {1}", score, iter);
                if(iter % 20 == 0)
                {
                    using(BinaryWriter modelWriter = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "vgg16." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                    {
                        model.Serialize(modelWriter);
                    }
                }
                //if (score > best_score)
                //{
                //    best_score = score;
                //    best_iter = iter;
                //}
                //Logger.WriteLog("Best Accuracy {0}, at Iteration {1}", best_score, best_iter);

            }
            Logger.CloseLog();
        }
    }
}
