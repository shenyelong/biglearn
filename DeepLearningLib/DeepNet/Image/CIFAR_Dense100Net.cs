using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Image
{
    unsafe public class CIFAR_Dense100Net : ComputationGraph
    {
        public string name = "CIFAR_Dense100Net";

        public class Dense100Model : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            
            public List<TensorConvStructure> Block1_1 = new List<TensorConvStructure>();
            public List<TensorConvStructure> Block1_2 = new List<TensorConvStructure>();
            
            public TensorConvStructure Conv2 = null;
            
            public List<TensorConvStructure> Block2_1 = new List<TensorConvStructure>();
            public List<TensorConvStructure> Block2_2 = new List<TensorConvStructure>();
            
            public TensorConvStructure Conv3 = null;

            public List<TensorConvStructure> Block3_1 = new List<TensorConvStructure>();
            public List<TensorConvStructure> Block3_2 = new List<TensorConvStructure>();
            
            public LayerStructure FClayer = null;

            public int Category { get; set; }

            // how to make sure the model parameters allocated into a flat array.
            public Dense100Model(int category, DeviceType device)
            {
                Category = category;

                int growthRate = 12;

                int inplanes = 2 * growthRate;

                int depth = 100;

                int block_num = (100 - 4) / 6;

                int expansion = 4;

                Conv1 = AddLayer(new TensorConvStructure(3, 3, 3, inplanes, false, device));

                for(int i = 0; i < block_num; i++)
                {
                    Block1_1.Add(  AddLayer(new TensorConvStructure(1, 1, inplanes, expansion * growthRate, false, device)) );
                    Block1_2.Add(  AddLayer(new TensorConvStructure(3, 3, expansion * growthRate, growthRate, false, device)) );
                    inplanes += growthRate;
                }

                Conv2 = AddLayer(new TensorConvStructure(1, 1, inplanes, inplanes / 2, false, device));
                inplanes = inplanes / 2;

                for(int i = 0; i < block_num; i++)
                {
                    Block2_1.Add(  AddLayer(new TensorConvStructure(1, 1, inplanes, expansion * growthRate, false, device)) );
                    Block2_2.Add(  AddLayer(new TensorConvStructure(3, 3, expansion * growthRate, growthRate, false, device)) );
                    inplanes += growthRate;
                }

                Conv3 = AddLayer(new TensorConvStructure(1, 1, inplanes, inplanes / 2, false, device));
                inplanes = inplanes / 2;

                for(int i = 0; i < block_num; i++)
                {
                    Block3_1.Add(  AddLayer(new TensorConvStructure(1, 1, inplanes, expansion * growthRate, false, device)) );
                    Block3_2.Add(  AddLayer(new TensorConvStructure(3, 3, expansion * growthRate, growthRate, false, device)) );
                    inplanes += growthRate;
                }
                
                FClayer = AddLayer(new LayerStructure(inplanes, category, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device));
            }
        }

        public Tensor4DData InputImages { get; set; }
        public SparseVectorData Label { get; set; }

        public new Dense100Model Model { get; set; }
        public HiddenBatchData O { get; set; }
        
        public CIFAR_Dense100Net(Dense100Model model, RunnerBehavior behavior) : base(behavior)
        {
            Model = model; // Model = new VGG11Model(Behavior); 
        }

        public Tensor4DData BuildBottleneckBlock(Tensor4DData x, TensorConvStructure conv1, TensorConvStructure conv2)
        {
            Tensor4DData o = x.Act(A_Func.Rectified, Session, Behavior);

            TensorConvRunner conv1Runner = new TensorConvRunner(conv1, o, 0, 1, A_Func.Rectified, Behavior);
            AddRunner(conv1Runner);
            o = conv1Runner.Output;

            TensorConvRunner conv2Runner = new TensorConvRunner(conv2, o, 1, 1, A_Func.Linear, Behavior);
            AddRunner(conv2Runner);
            o = conv2Runner.Output;

            int input_dim = x.Shape[0].Default * x.Shape[1].Default * x.Shape[2].Default;
            MatrixData inputEmbed = x.ToND().Reshape(new IntArgument("FLAT_DIM", input_dim), x.Shape[3]).ToMD();

            int output_dim = o.Shape[0].Default * o.Shape[1].Default * o.Shape[2].Default;
            MatrixData outputEmbed = o.ToND().Reshape(new IntArgument("FLAT_DIM", output_dim), o.Shape[3]).ToMD();
            
            MatrixData y = inputEmbed.Concate(outputEmbed, Session, Behavior);

            IntArgument new_depth = new IntArgument("new_depth", x.Shape[2].Default + o.Shape[2].Default);
            
            return y.ToND().Reshape(x.Shape[0], x.Shape[1], new_depth, x.Shape[3]).ToT4D();
        }
        //python3.5 cifar_biglearn.py -a vgg19_bn --lib /10T/yelongshen/storm --epochs 164 --schedule -1 --gamma 0.1 --checkpoint checkpoints/cifar10/vgg19_bn  --workers 0 --lr 0.1 --train-batch 100 --gpu-id 2 
        //python3.5 cifar_biglearn.py -a vgg16_bn --lib /10T/yelongshen/storm --epochs 164 --schedule -1 --gamma 0.1 --checkpoint checkpoints/cifar10/vgg16_bn  --workers 0 --lr 0.1 --train-batch 100 --gpu-id 3 
        //python3.5 cifar_biglearn.py -a densenet --depth 100 --growthRate 12 --train-batch 100 --epochs 300 --schedule -1 --wd 1e-4 --gamma 0.1 --checkpoint checkpoints/cifar10/densenet-bc-100-12-nobn --workers 0 --lib /10T/yelongshen/storm --lr 0.01 --gpu-id 3
        public void Build(int width, int height, int channel, int batchSize)
        {
            InputImages = new Tensor4DData(width, height, channel, batchSize, false, Behavior.Device);
            Label = new SparseVectorData(batchSize, Behavior.Device, false);
            Label.Value.Init(1);

            // 32 * 32 * 3 * batch
            TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, InputImages, 1, 1, A_Func.Linear, Behavior);
            AddRunner(conv1Runner);
            // 32 * 32 * 24 * batch
            Tensor4DData x = conv1Runner.Output;

            Logger.WriteLog("block 1 in x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);
            for(int i = 0; i < Model.Block1_1.Count; i++)
            {
                x = BuildBottleneckBlock(x, Model.Block1_1[i], Model.Block1_2[i]);
            }
            Logger.WriteLog("block 1 out x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);

            // 216 * 32 * 32
            x = x.Act(A_Func.Rectified, Session, Behavior);
            TensorConvRunner conv2Runner = new TensorConvRunner(Model.Conv2, x, 0, 1, A_Func.Linear, Behavior);
            AddRunner(conv2Runner);
            x = conv2Runner.Output;
            Logger.WriteLog("conv 2 out x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);
            // 108 * 32 * 32
            TensorMaxPooling2DRunner p1Runner = new TensorMaxPooling2DRunner(x, 2, 2, Behavior);
            AddRunner(p1Runner);
            // 108 * 16 * 16
            x = p1Runner.Output;
            Logger.WriteLog("block 2 in x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);
            for(int i = 0; i < Model.Block2_1.Count; i++)
            {
                x = BuildBottleneckBlock(x, Model.Block2_1[i], Model.Block2_2[i]);
            }

            Logger.WriteLog("block 2 out x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);

            // 300 * 16 * 16
            x = x.Act(A_Func.Rectified, Session, Behavior);
            TensorConvRunner conv3Runner = new TensorConvRunner(Model.Conv3, x, 0, 1, A_Func.Linear, Behavior);
            AddRunner(conv3Runner);
            // 150 * 16 * 16
            TensorMaxPooling2DRunner p2Runner = new TensorMaxPooling2DRunner(conv3Runner.Output, 2, 2, Behavior);
            AddRunner(p2Runner);
            // 150 * 8 * 8
            x = p2Runner.Output;

            for(int i = 0; i < Model.Block3_1.Count; i++)
            {
                x = BuildBottleneckBlock(x, Model.Block3_1[i], Model.Block3_2[i]);
            }
            Logger.WriteLog("block 3 x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);

            // 342 * 8 * 8
            x.Act(A_Func.Rectified, Session, Behavior);
            TensorMaxPooling2DRunner p3Runner = new TensorMaxPooling2DRunner(x, 8, 8, Behavior);
            AddRunner(p3Runner);

            // 342
            NdArrayData poolOut = p3Runner.Output.ToND();

            int fc_flat_dim = poolOut.Shape[0].Default * poolOut.Shape[1].Default * poolOut.Shape[2].Default;

            HiddenBatchData flatEmbed = poolOut.Reshape(new IntArgument("FLAT_DIM", fc_flat_dim), poolOut.Shape[3]).ToHDB();

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FClayer, flatEmbed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            O = classifierRunner.Output;
            
            AddObjective(new SoftmaxObjRunner(Label, O, Behavior)); 
            
            SetDelegateModel(Model);
        } 

        public float Run(IntPtr batch, IntPtr target, IntPtr o, int batchSize)
        {
            InputImages.BatchSize = batchSize;
            int memsize = InputImages.Length;

            //IntPtr.Add(cudaPiecePointer, sizeof(float) * offset)
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, batch, memsize);
            //(cudaP, (IntPtr)p, len);
            //InputImages.Output.SyncFromCPU(0, batch, 0, 224 * 224 * 3 * batchSize);
            long * p = (long * )target.ToPointer();
            {
                for(int i = 0; i < batchSize; i++)
                {
                    Label.Idx[i] = i * Model.Category + (int)p[i];
                }
            }

            Label.Length = batchSize;
            Label.Idx.SyncFromCPU();

            StepV2();
            
            if(o != IntPtr.Zero)
            {
                Cudalib.CudaCopyOutFloat(O.Output.Data.CudaPtr, o, batchSize * Model.Category);
            }

            return (float)StepLoss;
        }


        // call complete after training on every epoch.
        public void SaveModel(int epoch)
        {
            using(BinaryWriter modelWriter = new BinaryWriter(new FileStream("storm_dense100." + epoch.ToString(), FileMode.Create, FileAccess.Write)))
            {
                Model.Serialize(modelWriter);
            }
        }

    }
}
