using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Image
{
    unsafe public class AlexNet : ComputationGraph
    {
        public string name = "Alex";

        public class AlexModel : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            public TensorConvStructure Conv2 = null;
            public TensorConvStructure Conv3 = null;
            public TensorConvStructure Conv4 = null;
            public TensorConvStructure Conv5 = null;
            
            public LayerStructure Classifier { get; set; }

            public int Category { get; set; }

            public AlexModel(int category, DeviceType device)
            {
                Conv1 = AddLayer(new TensorConvStructure(11, 11, 3, 64, device));
                Conv2 = AddLayer(new TensorConvStructure(5, 5, 64, 192, device));
                Conv3 = AddLayer(new TensorConvStructure(3, 3, 192, 384, device));
                Conv4 = AddLayer(new TensorConvStructure(3, 3, 384, 256, device));
                Conv5 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));

                // two class classification. 
                Classifier = AddLayer(new LayerStructure(256, category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                
                Category = category;
            }
        }

        public Tensor4DData InputImages { get; set; }
        public SparseVectorData Label { get; set; }

        public new AlexModel Model { get; set; }
        public HiddenBatchData O { get; set; }

        public AlexNet(AlexModel model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior; // new RunnerBehavior(DNNRunMode.Train, device, computeLib); 
            Model = model; // Model = new VGG11Model(Behavior); 
            
        }

        public void Build(int width, int height, int channel, int batchSize)
        {
            InputImages = new Tensor4DData(width, height, channel, batchSize, false, Behavior.Device);
            Label = new SparseVectorData(batchSize, Behavior.Device, false);
            Label.Value.Init(1);

            // 32 * 32 * 3.
            TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, InputImages, 5, 4, A_Func.Rectified, Behavior);
            AddRunner(conv1Runner);
            // 8 * 8 * 64
            TensorMaxPooling2DRunner pool1Runner = new TensorMaxPooling2DRunner(conv1Runner.Output, 2, 2, Behavior);
            AddRunner(pool1Runner);
            // 4 * 4 * 64
            TensorConvRunner conv2Runner = new TensorConvRunner(Model.Conv2, pool1Runner.Output, 2, 1, A_Func.Rectified, Behavior);
            AddRunner(conv2Runner);
            // 4 * 4 * 192
            TensorMaxPooling2DRunner pool2Runner = new TensorMaxPooling2DRunner(conv2Runner.Output, 2, 2, Behavior);
            AddRunner(pool2Runner);
            // 2 * 2 * 192
            TensorConvRunner conv3Runner = new TensorConvRunner(Model.Conv3, pool2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv3Runner);
            // 2 * 2 * 384
            TensorConvRunner conv4Runner = new TensorConvRunner(Model.Conv4, conv3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv4Runner);
            // 2 * 2 * 256
            TensorConvRunner conv5Runner = new TensorConvRunner(Model.Conv5, conv4Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv5Runner);
            // 2 * 2 * 256.
            TensorMaxPooling2DRunner pool3Runner = new TensorMaxPooling2DRunner(conv5Runner.Output, 2, 2, Behavior);
            AddRunner(pool3Runner);
            // 1 * 1 * 256.
            
            NdArrayData feaMap = pool3Runner.Output.ToND();
            HiddenBatchData feaInput = feaMap.Reshape(feaMap.Dimensions[2], feaMap.Dimensions[3]).ToHDB();

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, feaInput, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            O = classifierRunner.Output;
            
            SoftmaxObjRunner ceRunner = new SoftmaxObjRunner(Label, O, true, Behavior); 
            AddObjective(ceRunner); 

            SetDelegateModel(Model);
        } 

        public float Run(IntPtr batch, IntPtr target, IntPtr o, int batchSize)
        {
            InputImages.BatchSize = batchSize;
            int memsize = InputImages.Length;

            //IntPtr.Add(cudaPiecePointer, sizeof(float) * offset)
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, batch, memsize);
            //(cudaP, (IntPtr)p, len);
            //InputImages.Output.SyncFromCPU(0, batch, 0, 224 * 224 * 3 * batchSize);
            long * p = (long * )target.ToPointer();
            {
                for(int i = 0; i < batchSize; i++)
                {
                    Label.Idx[i] = i * Model.Category + (int)p[i];
                }
            }

            Label.Length = batchSize;
            Label.Idx.SyncFromCPU();

            StepV2();
            
            if(o != IntPtr.Zero)
            {
                Cudalib.CudaCopyOutFloat(O.Output.Data.CudaPtr, o, batchSize * Model.Category);
            }

            return (float)StepLoss;
        }


        // call complete after training on every epoch.
        public void SaveModel(int epoch)
        {
            using(BinaryWriter modelWriter = new BinaryWriter(new FileStream("storm_alex." + epoch.ToString(), FileMode.Create, FileAccess.Write)))
            {
                Model.Serialize(modelWriter);
            }
        }

    }
}
