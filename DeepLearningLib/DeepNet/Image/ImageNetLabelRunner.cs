using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.Image
{
        public class ImageNetLabelRunner : StructRunner
        {
            public SparseVectorData Label { get; set; }
            int CategoryNum { get; set; }
            int[] LabelInfo { get; set; }
            
            int BatchSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            public ImageNetLabelRunner(string labelStream, int batchSize, DataRandomShuffling shuffle, int totalSampleNum, int categoryNum, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                CategoryNum = categoryNum;
                LabelInfo = new int[totalSampleNum];

                BatchSize = batchSize;
                Shuffle = shuffle;

                Dictionary<int, int> label_distr = new Dictionary<int, int>();
                using(StreamReader reader = new StreamReader(labelStream))
                {
                    int idx = 0;
                    while(!reader.EndOfStream)
                    {
                        int label = int.Parse(reader.ReadLine());
                        LabelInfo[idx] = label;
                        idx += 1;

                        if(!label_distr.ContainsKey(label))
                        {
                            label_distr[label] = 0;
                        }
                        label_distr[label] += 1;
                    }
                }

                foreach(KeyValuePair<int, int> label_d in label_distr)
                {
                    Console.WriteLine("Label {0}, count {1}", label_d.Key, label_d.Value);
                }

                Label = new SparseVectorData(batchSize, behavior.Device, false);
                Label.Value.Init(1);
            }

            public override void Forward()
            {
                for(int i = 0; i < BatchSize; i++)
                {
                    int lidx = Shuffle.OrderNext();
                    int label = LabelInfo[lidx];
                    Label.Idx[i] = i * CategoryNum + label;
                }
                Label.Length = BatchSize;
                Label.Idx.SyncFromCPU();
            }

        }
        
}
