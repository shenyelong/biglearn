using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Image
{
    unsafe public class CIFAR_Dense100Net_BN2 : ComputationGraph
    {
        public string name = "CIFAR_Dense100Net";

        public class Dense100Model : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            
            public List<TensorConvStructure> Block1_1 = new List<TensorConvStructure>();
            public List<TensorConvStructure> Block1_2 = new List<TensorConvStructure>();
            public List<LayerStructure> Block1_BN1 = new List<LayerStructure>();
            public List<LayerStructure> Block1_BN2 = new List<LayerStructure>();

            public NdArrayData Block1_BN1Scale(int i)
            {
                return new NdArrayData(DeviceType, Block1_BN1[i].weight, Block1_BN1[i].WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block1_BN1[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            public NdArrayData Block1_BN1Bias(int i)
            {
                return new NdArrayData(DeviceType, Block1_BN1[i].bias, Block1_BN1[i].BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block1_BN1[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            
            public NdArrayData Block1_BN2Scale(int i)
            {
                return new NdArrayData(DeviceType, Block1_BN2[i].weight, Block1_BN2[i].WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block1_BN2[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            public NdArrayData Block1_BN2Bias(int i)
            {
                return new NdArrayData(DeviceType, Block1_BN2[i].bias, Block1_BN2[i].BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block1_BN2[i].Neural_Out), new IntArgument("default 1", 1)); 
            }

            public LayerStructure BN1 { get; set; }
            public NdArrayData BN1Scale { get { return new NdArrayData(DeviceType, BN1.weight, BN1.WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", BN1.Neural_Out), new IntArgument("default 1", 1)); } }
            public NdArrayData BN1Bias { get { return new NdArrayData(DeviceType, BN1.bias, BN1.BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", BN1.Neural_Out), new IntArgument("default 1", 1)); } }

            public TensorConvStructure Conv2 = null;
            

            
            public List<TensorConvStructure> Block2_1 = new List<TensorConvStructure>();
            public List<TensorConvStructure> Block2_2 = new List<TensorConvStructure>();
            public List<LayerStructure> Block2_BN1 = new List<LayerStructure>();
            public List<LayerStructure> Block2_BN2 = new List<LayerStructure>();

            public NdArrayData Block2_BN1Scale(int i)
            {
                return new NdArrayData(DeviceType, Block2_BN1[i].weight, Block2_BN1[i].WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block2_BN1[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            public NdArrayData Block2_BN1Bias(int i)
            {
                return new NdArrayData(DeviceType, Block2_BN1[i].bias, Block2_BN1[i].BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block2_BN1[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            
            public NdArrayData Block2_BN2Scale(int i)
            {
                return new NdArrayData(DeviceType, Block2_BN2[i].weight, Block2_BN2[i].WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block2_BN2[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            public NdArrayData Block2_BN2Bias(int i)
            {
                return new NdArrayData(DeviceType, Block2_BN2[i].bias, Block2_BN2[i].BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block2_BN2[i].Neural_Out), new IntArgument("default 1", 1)); 
            }


            public LayerStructure BN2 { get; set; }
            public NdArrayData BN2Scale { get { return new NdArrayData(DeviceType, BN2.weight, BN2.WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", BN2.Neural_Out), new IntArgument("default 1", 1)); } }
            public NdArrayData BN2Bias { get { return new NdArrayData(DeviceType, BN2.bias, BN2.BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", BN2.Neural_Out), new IntArgument("default 1", 1)); } }
            
            public TensorConvStructure Conv3 = null;

            
            public List<TensorConvStructure> Block3_1 = new List<TensorConvStructure>();
            public List<TensorConvStructure> Block3_2 = new List<TensorConvStructure>();
            public List<LayerStructure> Block3_BN1 = new List<LayerStructure>();
            public List<LayerStructure> Block3_BN2 = new List<LayerStructure>();

            public NdArrayData Block3_BN1Scale(int i)
            {
                return new NdArrayData(DeviceType, Block3_BN1[i].weight, Block3_BN1[i].WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block3_BN1[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            public NdArrayData Block3_BN1Bias(int i)
            {
                return new NdArrayData(DeviceType, Block3_BN1[i].bias, Block3_BN1[i].BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block3_BN1[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            
            public NdArrayData Block3_BN2Scale(int i)
            {
                return new NdArrayData(DeviceType, Block3_BN2[i].weight, Block3_BN2[i].WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block3_BN2[i].Neural_Out), new IntArgument("default 1", 1)); 
            }
            public NdArrayData Block3_BN2Bias(int i)
            {
                return new NdArrayData(DeviceType, Block3_BN2[i].bias, Block3_BN2[i].BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", Block3_BN2[i].Neural_Out), new IntArgument("default 1", 1)); 
            }

            public LayerStructure BN3 { get; set; }
            public NdArrayData BN3Scale { get { return new NdArrayData(DeviceType, BN3.weight, BN3.WeightGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", BN3.Neural_Out), new IntArgument("default 1", 1)); } }
            public NdArrayData BN3Bias { get { return new NdArrayData(DeviceType, BN3.bias, BN3.BiasGrad, new IntArgument("default 1", 1), new IntArgument("default 1", 1), new IntArgument("depth", BN3.Neural_Out), new IntArgument("default 1", 1)); } }


            
            public LayerStructure FClayer = null;

            public int Category { get; set; }


            // how to make sure the model parameters allocated into a flat array.
            public Dense100Model(int category, DeviceType device)
            {
                Category = category;
                DeviceType = device;

                int growthRate = 12;

                int inplanes = 2 * growthRate;

                int depth = 100;

                int block_num = (100 - 4) / 6;

                int expansion = 4;

                Conv1 = AddLayer(new TensorConvStructure(3, 3, 3, inplanes, false, device));

                for(int i = 0; i < block_num; i++)
                {
                    Block1_1.Add(  AddLayer(new TensorConvStructure(1, 1, inplanes, expansion * growthRate, false, device)) );
                    Block1_2.Add(  AddLayer(new TensorConvStructure(3, 3, expansion * growthRate, growthRate, false, device)) );

                    Block1_BN1.Add( AddLayer(new LayerStructure(1, inplanes, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device)));
                    Block1_BN1[i].weight.Init(1);
                    Block1_BN1[i].bias.Init(0);
                    
                    Block1_BN2.Add( AddLayer(new LayerStructure(1, expansion * growthRate, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device)));
                    Block1_BN2[i].weight.Init(1);
                    Block1_BN2[i].bias.Init(0);

                    inplanes += growthRate;
                }
                // batch normalization.
                BN1 = AddLayer(new LayerStructure(1, inplanes, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                BN1.weight.Init(1);
                BN1.bias.Init(0);

                Conv2 = AddLayer(new TensorConvStructure(1, 1, inplanes, inplanes / 2, false, device));
                inplanes = inplanes / 2;


                for(int i = 0; i < block_num; i++)
                {
                    Block2_1.Add(  AddLayer(new TensorConvStructure(1, 1, inplanes, expansion * growthRate, false, device)) );
                    Block2_2.Add(  AddLayer(new TensorConvStructure(3, 3, expansion * growthRate, growthRate, false, device)) );

                    Block2_BN1.Add( AddLayer(new LayerStructure(1, inplanes, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device)));
                    Block2_BN1[i].weight.Init(1);
                    Block2_BN1[i].bias.Init(0);
                    
                    Block2_BN2.Add( AddLayer(new LayerStructure(1, expansion * growthRate, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device)));
                    Block2_BN2[i].weight.Init(1);
                    Block2_BN2[i].bias.Init(0);

                    inplanes += growthRate;
                }
                // batch normalization.
                BN2 = AddLayer(new LayerStructure(1, inplanes, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                BN2.weight.Init(1);
                BN2.bias.Init(0);


                Conv3 = AddLayer(new TensorConvStructure(1, 1, inplanes, inplanes / 2, false, device));
                inplanes = inplanes / 2;

                for(int i = 0; i < block_num; i++)
                {
                    Block3_1.Add(  AddLayer(new TensorConvStructure(1, 1, inplanes, expansion * growthRate, false, device)) );
                    Block3_2.Add(  AddLayer(new TensorConvStructure(3, 3, expansion * growthRate, growthRate, false, device)) );

                    Block3_BN1.Add( AddLayer(new LayerStructure(1, inplanes, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device)));
                    Block3_BN1[i].weight.Init(1);
                    Block3_BN1[i].bias.Init(0);
                    
                    Block3_BN2.Add( AddLayer(new LayerStructure(1, expansion * growthRate, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device)));
                    Block3_BN2[i].weight.Init(1);
                    Block3_BN2[i].bias.Init(0);

                    inplanes += growthRate;
                }
                // batch normalization.
                BN3 = AddLayer(new LayerStructure(1, inplanes, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                BN3.weight.Init(1);
                BN3.bias.Init(0);

                FClayer = AddLayer(new LayerStructure(inplanes, category, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device));
            }
        }

        public Tensor4DData InputImages { get; set; }
        public SparseVectorData Label { get; set; }

        public new Dense100Model Model { get; set; }
        public HiddenBatchData O { get; set; }
        
        public CIFAR_Dense100Net_BN2(Dense100Model model, RunnerBehavior behavior) : base(behavior)
        {
            Model = model; // Model = new VGG11Model(Behavior); 
        }

        // batch normalization.
        public Tensor4DData BuildBottleneckBlock(Tensor4DData x, TensorConvStructure conv1, TensorConvStructure conv2, NdArrayData bn1Scale, NdArrayData bn1Bias, NdArrayData bn2Scale, NdArrayData bn2Bias)
        {
            //BatchNorm2DRunner bn1Runner = new BatchNorm2DRunner(x, Behavior);
            //AddRunner(bn1Runner);
            //Tensor4DData o = bn1Runner.Output.ToND().DotAndAdd(bn1Scale, bn1Bias, Session, Behavior).ToT4D();

            Tensor4DData o = x;

            o = o.BatchNorm2DAFV2(bn1Scale.ToT4D(), bn1Bias.ToT4D(), Session, Behavior);
            //o = o.Act(A_Func.Rectified, Session, Behavior);

            TensorConvRunner conv1Runner = new TensorConvRunner(conv1, o, 0, 1, Behavior);
            AddRunner(conv1Runner);
            o = conv1Runner.Output;

            //BatchNorm2DRunner bn2Runner = new BatchNorm2DRunner(o, Behavior);
            //AddRunner(bn2Runner);
            //o = bn2Runner.Output.ToND().DotAndAdd(bn2Scale, bn2Bias, Session, Behavior).ToT4D();

            //o = o.Act(A_Func.Rectified, Session, Behavior);
            o = o.BatchNorm2DAFV2(bn2Scale.ToT4D(), bn2Bias.ToT4D(), Session, Behavior);

            TensorConvRunner conv2Runner = new TensorConvRunner(conv2, o, 1, 1, Behavior);
            AddRunner(conv2Runner);
            o = conv2Runner.Output;

            int input_dim = x.Shape[0].Default * x.Shape[1].Default * x.Shape[2].Default;
            MatrixData inputEmbed = x.ToND().Reshape(new IntArgument("FLAT_DIM", input_dim), x.Shape[3]).ToMD();

            int output_dim = o.Shape[0].Default * o.Shape[1].Default * o.Shape[2].Default;
            MatrixData outputEmbed = o.ToND().Reshape(new IntArgument("FLAT_DIM", output_dim), o.Shape[3]).ToMD();
            
            MatrixData y = inputEmbed.Concate(outputEmbed, Session, Behavior);

            IntArgument new_depth = new IntArgument("new_depth", x.Shape[2].Default + o.Shape[2].Default);
            
            return y.ToND().Reshape(x.Shape[0], x.Shape[1], new_depth, x.Shape[3]).ToT4D();
        }
        //python3.5 cifar_biglearn.py -a vgg19_bn --lib /10T/yelongshen/storm --epochs 164 --schedule -1 --gamma 0.1 --checkpoint checkpoints/cifar10/vgg19_bn  --workers 0 --lr 0.1 --train-batch 100 --gpu-id 2 
        //python3.5 cifar_biglearn.py -a vgg16_bn --lib /10T/yelongshen/storm --epochs 164 --schedule -1 --gamma 0.1 --checkpoint checkpoints/cifar10/vgg16_bn  --workers 0 --lr 0.1 --train-batch 100 --gpu-id 3 
        //python3.5 cifar_biglearn.py -a densenet --depth 100 --growthRate 12 --train-batch 100 --epochs 300 --schedule -1 --wd 1e-4 --gamma 0.1 --checkpoint checkpoints/cifar10/densenet-bc-100-12-nobn --workers 0 --lib /10T/yelongshen/storm --lr 0.01 --gpu-id 3
        public void Build(int width, int height, int channel, int batchSize)
        {
            InputImages = new Tensor4DData(width, height, channel, batchSize, false, Behavior.Device);
            Label = new SparseVectorData(batchSize, Behavior.Device, false);
            Label.Value.Init(1);

            // 32 * 32 * 3 * batch
            TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, InputImages, 1, 1, Behavior);
            AddRunner(conv1Runner);
            // 32 * 32 * 24 * batch
            Tensor4DData x = conv1Runner.Output;

            Logger.WriteLog("block 1 in x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);
            for(int i = 0; i < Model.Block1_1.Count; i++)
            {
                x = BuildBottleneckBlock(x, Model.Block1_1[i], Model.Block1_2[i], Model.Block1_BN1Scale(i), Model.Block1_BN1Bias(i), Model.Block1_BN2Scale(i), Model.Block1_BN2Bias(i));
            }
            Logger.WriteLog("block 1 out x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);

            // 216 * 32 * 32
            //bn
            
            x = x.BatchNorm2DAFV2(Model.BN1Scale.ToT4D(), Model.BN1Bias.ToT4D(), Session, Behavior);

            //Tensor4DData BatchNorm2D(this Tensor4DData data, Tensor4DData scale, Tensor4DData bias, List<StructRunner> session, RunnerBehavior behavior)
            //BatchNorm2DRunner bn1Runner = new BatchNorm2DRunner(x, Behavior);
            //AddRunner(bn1Runner);
            //x = bn1Runner.Output.ToND().DotAndAdd(Model.BN1Scale, Model.BN1Bias, Session, Behavior).ToT4D();


            //x = x.Act(A_Func.Rectified, Session, Behavior);
            TensorConvRunner conv2Runner = new TensorConvRunner(Model.Conv2, x, 0, 1, Behavior);
            AddRunner(conv2Runner);
            x = conv2Runner.Output;
            Logger.WriteLog("conv 2 out x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);
            // 108 * 32 * 32
            TensorMeanPooling2DRunner p1Runner = new TensorMeanPooling2DRunner(x, 2, 2, Behavior);
            AddRunner(p1Runner);
            // 108 * 16 * 16
            x = p1Runner.Output;
            Logger.WriteLog("block 2 in x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);
            for(int i = 0; i < Model.Block2_1.Count; i++)
            {
                x = BuildBottleneckBlock(x, Model.Block2_1[i], Model.Block2_2[i], Model.Block2_BN1Scale(i), Model.Block2_BN1Bias(i), Model.Block2_BN2Scale(i), Model.Block2_BN2Bias(i));
            }

            Logger.WriteLog("block 2 out x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);
            
            // 300 * 16 * 16
            //bn
            //BatchNorm2DRunner bn2Runner = new BatchNorm2DRunner(x, Behavior);
            //AddRunner(bn2Runner);
            //x = bn2Runner.Output.ToND().DotAndAdd(Model.BN2Scale, Model.BN2Bias, Session, Behavior).ToT4D();

            x = x.BatchNorm2DAFV2(Model.BN2Scale.ToT4D(), Model.BN2Bias.ToT4D(), Session, Behavior);
            //x = x.Act(A_Func.Rectified, Session, Behavior);

            TensorConvRunner conv3Runner = new TensorConvRunner(Model.Conv3, x, 0, 1, Behavior);
            AddRunner(conv3Runner);
            // 150 * 16 * 16
            TensorMeanPooling2DRunner p2Runner = new TensorMeanPooling2DRunner(conv3Runner.Output, 2, 2, Behavior);
            AddRunner(p2Runner);
            // 150 * 8 * 8
            x = p2Runner.Output;

            for(int i = 0; i < Model.Block3_1.Count; i++)
            {
                x = BuildBottleneckBlock(x, Model.Block3_1[i], Model.Block3_2[i], Model.Block3_BN1Scale(i), Model.Block3_BN1Bias(i), Model.Block3_BN2Scale(i), Model.Block3_BN2Bias(i));
            }
            Logger.WriteLog("block 3 x.shape : {0}, {1}, {2}, {3} ", x.Shape[0].Default, x.Shape[1].Default, x.Shape[2].Default, x.Shape[3].Default);

            // batch normalization.
            //BatchNorm2DRunner bn3Runner = new BatchNorm2DRunner(x, Behavior);
            //AddRunner(bn3Runner);
            //x = bn3Runner.Output.ToND().DotAndAdd(Model.BN3Scale, Model.BN3Bias, Session, Behavior).ToT4D();
            // 342 * 8 * 8
            x = x.BatchNorm2DAFV2(Model.BN3Scale.ToT4D(), Model.BN3Bias.ToT4D(), Session, Behavior);

            
            //x.Act(A_Func.Rectified, Session, Behavior);
            TensorMeanPooling2DRunner p3Runner = new TensorMeanPooling2DRunner(x, 8, 8, Behavior);
            AddRunner(p3Runner);

            // 342
            NdArrayData poolOut = p3Runner.Output.ToND();

            int fc_flat_dim = poolOut.Shape[0].Default * poolOut.Shape[1].Default * poolOut.Shape[2].Default;

            HiddenBatchData flatEmbed = poolOut.Reshape(new IntArgument("FLAT_DIM", fc_flat_dim), poolOut.Shape[3]).ToHDB();

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FClayer, flatEmbed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            O = classifierRunner.Output;
            
            AddObjective(new SoftmaxObjRunner(Label, O, Behavior)); 
            
            SetDelegateModel(Model);
        } 

        public float Run(IntPtr batch, IntPtr target, IntPtr o, int batchSize)
        {
            InputImages.BatchSize = batchSize;
            int memsize = InputImages.Length;

            //IntPtr.Add(cudaPiecePointer, sizeof(float) * offset)
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, batch, memsize);
            //(cudaP, (IntPtr)p, len);
            //InputImages.Output.SyncFromCPU(0, batch, 0, 224 * 224 * 3 * batchSize);
            long * p = (long * )target.ToPointer();
            {
                for(int i = 0; i < batchSize; i++)
                {
                    Label.Idx[i] = i * Model.Category + (int)p[i];
                }
            }

            Label.Length = batchSize;
            Label.Idx.SyncFromCPU();

            StepV2();
            
            if(o != IntPtr.Zero)
            {
                Cudalib.CudaCopyOutFloat(O.Output.Data.CudaPtr, o, batchSize * Model.Category);
            }

            return (float)StepLoss;
        }


        // call complete after training on every epoch.
        public void SaveModel(int epoch)
        {
            using(BinaryWriter modelWriter = new BinaryWriter(new FileStream("storm_dense100." + epoch.ToString(), FileMode.Create, FileAccess.Write)))
            {
                Model.Serialize(modelWriter);
            }
        }

    }
}
