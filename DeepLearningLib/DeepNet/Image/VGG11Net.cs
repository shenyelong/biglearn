using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Image
{
    unsafe public class VGG11Net : ComputationGraph
    {
        public string name = "VGG11";

        public class VGG11Model : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            //public TensorConvStructure Conv2 = null;
            public TensorConvStructure Conv3 = null;
            //public TensorConvStructure Conv4 = null;
            public TensorConvStructure Conv5 = null;
            public TensorConvStructure Conv6 = null;
            //public TensorConvStructure Conv7 = null;
            public TensorConvStructure Conv8 = null;
            public TensorConvStructure Conv9 = null;
            //public TensorConvStructure Conv10 = null;
            public TensorConvStructure Conv11 = null;
            public TensorConvStructure Conv12 = null;
            //public TensorConvStructure Conv13 = null;
            public LayerStructure FClayer1 = null;
            public LayerStructure FClayer2 = null;
            public LayerStructure FClayer3 = null;
            
            public VGG11Model(): this(DeviceType.CPU)
            { }

            // how to make sure the model parameters allocated into a flat array.
            public VGG11Model(DeviceType device)
            {
                // int conv1Num = TensorConvStructure.ParamNum(3, 3, 3, 64);
                // int conv3Num = TensorConvStructure.ParamNum(3, 3, 64, 128);
                // int conv5Num = TensorConvStructure.ParamNum(3, 3, 128, 256);
                // int conv6Num = TensorConvStructure.ParamNum(3, 3, 256, 256);
                // int conv8Num = TensorConvStructure.ParamNum(3, 3, 256, 512);
                // int conv9Num = TensorConvStructure.ParamNum(3, 3, 512, 512);
                // int conv11Num = TensorConvStructure.ParamNum(3, 3, 512, 512);
                // int conv12Num = TensorConvStructure.ParamNum(3, 3, 512, 512);
                // int fc1Num = 25088 * 4096 + 4096;
                // int fc2Num = 4096 * 4096 + 4096;
                // int fc3Num = 4096 * 1000 + 1000;
                // int paramNum = conv1Num + conv3Num + conv5Num + conv6Num + conv8Num + conv9Num + conv11Num + conv12Num + fc1Num + fc2Num + fc3Num;
                //DeviceBehavior
                Conv1 = AddLayer(new TensorConvStructure(3, 3, 3, 64, device));
                
                //Conv2 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));
                Conv3 = AddLayer(new TensorConvStructure(3, 3, 64, 128, device));
                //Conv4 = AddLayer(new TensorConvStructure(3, 3, 128, 128, device));
                Conv5 = AddLayer(new TensorConvStructure(3, 3, 128, 256, device));
                Conv6 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));
                //Conv7 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));
                Conv8 = AddLayer(new TensorConvStructure(3, 3, 256, 512, device));
                Conv9 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                //Conv10 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv11 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv12 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                //Conv13 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                FClayer1 = AddLayer(new LayerStructure(25088, 4096, A_Func.Rectified, N_Type.Fully_Connected, 1, 0, true, device));
                FClayer2 = AddLayer(new LayerStructure(4096, 4096, A_Func.Rectified, N_Type.Fully_Connected, 1, 0, true, device));
                FClayer3 = AddLayer(new LayerStructure(4096, 1000, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device));
            }

            public void InitOptimizer(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);  
            }

            // public override void CopyFrom(IData other)
            // {
            //     VGG11Model src = (VGG11Model)other;
            //     Conv1.CopyFrom(src.Conv1);
            //     Conv3.CopyFrom(src.Conv3);
            //     Conv5.CopyFrom(src.Conv5);
            //     Conv6.CopyFrom(src.Conv6);
            //     Conv8.CopyFrom(src.Conv8);
            //     Conv9.CopyFrom(src.Conv9);
            //     Conv11.CopyFrom(src.Conv11);
            //     Conv12.CopyFrom(src.Conv12);
            //     FClayer1.CopyFrom(src.FClayer1);
            //     FClayer2.CopyFrom(src.FClayer2);
            //     FClayer3.CopyFrom(src.FClayer3);
            // }

            // public VGG11Model(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
            //     Conv1 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     //Conv2 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv3 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     //Conv4 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv5 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv6 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     //Conv7 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv8 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv9 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     //Conv10 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv11 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv12 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     //Conv13 = DeserializeModel<TensorConvStructure>(reader, device); 
                
            //     FClayer1 = DeserializeModel<LayerStructure>(reader, device); 
            //     FClayer2 = DeserializeModel<LayerStructure>(reader, device); 
            //     FClayer3 = DeserializeModel<LayerStructure>(reader, device); 
            //     //if(behavior.RunMode == DNNRunMode.Train)
            //     //{
            //     //    InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);  
            //     //}
            // }
        }

        public Tensor4DData InputImages { get; set; }
        public SparseVectorData Label { get; set; }

        public new VGG11Model Model { get; set; }
        public HiddenBatchData O { get; set; }
        // for test purpose.
        //public VGG11Net()
        //{ }

        public VGG11Net(VGG11Model model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior; // new RunnerBehavior(DNNRunMode.Train, device, computeLib); 
            Model = model; // Model = new VGG11Model(Behavior); 
        }

        public void BuildCG(int batchSize)
        {
            InputImages = new Tensor4DData(224, 224, 3, batchSize, false, Behavior.Device);
            Label = new SparseVectorData(batchSize, Behavior.Device, false);
            Label.Value.Init(1);

            // x --> 3 * 224 * 224,
            TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, InputImages, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv1Runner);
            // x --> 64 * 224 * 224
            //TensorConvRunner conv2Runner = new TensorConvRunner(Model.Conv2, conv1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv2Runner);
            // x --> 64 * 224 * 224
            TensorMaxPooling2DRunner pool1Runner = new TensorMaxPooling2DRunner(conv1Runner.Output, 2, 2, Behavior);
            AddRunner(pool1Runner);
            // x --> 64 * 112 * 112
            TensorConvRunner conv3Runner = new TensorConvRunner(Model.Conv3, pool1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv3Runner);
            // x --> 128 * 112 * 112
            //TensorConvRunner conv4Runner = new TensorConvRunner(Model.Conv4, conv3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv4Runner);
            // x --> 128 * 112 * 112
            TensorMaxPooling2DRunner pool2Runner = new TensorMaxPooling2DRunner(conv3Runner.Output, 2, 2, Behavior);
            AddRunner(pool2Runner);
            // x --> 128 * 56 * 56
            TensorConvRunner conv5Runner = new TensorConvRunner(Model.Conv5, pool2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv5Runner);
            // x --> 256 * 56 * 56
            TensorConvRunner conv6Runner = new TensorConvRunner(Model.Conv6, conv5Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv6Runner);
            // x --> 256 * 56 * 56
            //TensorConvRunner conv7Runner = new TensorConvRunner(Model.Conv7, conv6Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv7Runner);
            // x --> 256 * 56 * 56
            TensorMaxPooling2DRunner pool3Runner = new TensorMaxPooling2DRunner(conv6Runner.Output, 2, 2, Behavior);
            AddRunner(pool3Runner);
            // x --> 256 * 28 * 28
            TensorConvRunner conv8Runner = new TensorConvRunner(Model.Conv8, pool3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv8Runner);
            // x --> 512 * 28 * 28
            TensorConvRunner conv9Runner = new TensorConvRunner(Model.Conv9, conv8Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv9Runner);
            // x --> 512 * 28 * 28
            //TensorConvRunner conv10Runner = new TensorConvRunner(Model.Conv10, conv9Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv10Runner);
            // x --> 512 * 28 * 28
            TensorMaxPooling2DRunner pool4Runner = new TensorMaxPooling2DRunner(conv9Runner.Output, 2, 2, Behavior);
            AddRunner(pool4Runner);
            // x --> 512 * 14 * 14
            TensorConvRunner conv11Runner = new TensorConvRunner(Model.Conv11, pool4Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv11Runner);
            // x --> 512 * 14 * 14
            TensorConvRunner conv12Runner = new TensorConvRunner(Model.Conv12, conv11Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv12Runner);
            // x --> 512 * 14 * 14
            //TensorConvRunner conv13Runner = new TensorConvRunner(Model.Conv13, conv12Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv13Runner);
            // x --> 512 * 14 * 14 
            TensorMaxPooling2DRunner pool5Runner = new TensorMaxPooling2DRunner(conv12Runner.Output, 2, 2, Behavior);
            AddRunner(pool5Runner);

            // x --> 512 * 7 * 7 
            NdArrayData poolOut = pool5Runner.Output.ToND();

            int fc_flat_dim = poolOut.Shape[0].Default * poolOut.Shape[1].Default * poolOut.Shape[2].Default;
            //Logger.WriteLog("pool width {0}, pool height {1}, pool depth {2}, FC input {3}, batch size {4}", 
            //            poolOut.Shape[0].Default, poolOut.Shape[1].Default, poolOut.Shape[2].Default,
            //            fc_flat_dim, poolOut.Shape[3].Default);

            HiddenBatchData flatEmbed = poolOut.Reshape(new IntArgument("FLAT_DIM", fc_flat_dim), poolOut.Shape[3]).ToHDB();



            FullyConnectHiddenRunner<HiddenBatchData> fc1Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FClayer1, flatEmbed, Behavior);
            AddRunner(fc1Runner);
            HiddenBatchData o1 = fc1Runner.Output; 
            {
                TensorDropoutRunner drop1 = new TensorDropoutRunner(o1.ToT4D(), 0.5f, Behavior);
                AddRunner(drop1);
                o1 = drop1.Output.ToHDB();
            }

            //DropoutRunner  
            FullyConnectHiddenRunner<HiddenBatchData> fc2Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FClayer2, o1, Behavior); // fc1Runner.Output, Behavior);
            AddRunner(fc2Runner);
            HiddenBatchData o2 = fc2Runner.Output;
            //if(Behavior.RunMode == DNNRunMode.Train)
            {
                TensorDropoutRunner drop2 = new TensorDropoutRunner(o2.ToT4D(), 0.5f, Behavior);
                AddRunner(drop2);
                o2 = drop2.Output.ToHDB();
            }

            FullyConnectHiddenRunner<HiddenBatchData> fc3Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FClayer3, o2, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fc3Runner);
            HiddenBatchData o3 = fc3Runner.Output;
            //LabelConvertRunner labelRunner = new LabelConvertRunner(label, 1000, Behavior);
            //cg.AddRunner(labelRunner);
            O = o3;
            
            //if(Behavior.RunMode == DNNRunMode.Train)
            //{
            AddObjective(new SoftmaxObjRunner(Label, o3, Behavior)); 
            //}
            //else
            //{
            //    AddRunner(new AccuracyRunner(Label.Idx, o3, Behavior));
            //}

            SetDelegateModel(Model);
        } 


        // multi-gpu running.
        public class VGG11Runner
        {
            public List<VGG11Net> Nets = new List<VGG11Net>();
            int DeviceNum { get { return DeviceIds.Length; } }
            int[] DeviceIds { get; set; }

            IntPtr Comm { get; set; }
            CudaPieceFloat[] Grads { get; set; }
            public VGG11Runner(VGG11Model model, int batchSize, int[] deviceIds)
            {
                DeviceIds = deviceIds;
                int subBatchSize = batchSize / DeviceNum;

                fixed (int * pDevice = & deviceIds[0])
                {
                    Comm = Cudalib.CommInitAll((IntPtr)pDevice, DeviceNum);
                }

                Grads = new CudaPieceFloat[DeviceNum];

                int idx = 0;
                foreach(int devId in deviceIds)
                {
                    Logger.WriteLog("Create Device {0}", devId);
                    DeviceType device = MathOperatorManager.SetDevice(devId);
                    IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                    VGG11Model vgg11 = new VGG11Model(device);
                    vgg11.CopyFrom(model);

                    //StructureLearner learner, RunnerBehavior behavior)
                    // initoptimizer;
                    RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };
                    
                    Grads[idx] = vgg11.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
                    Logger.WriteLog("gradient size {0}", Grads[idx].Size);

                    VGG11Net net = new VGG11Net(vgg11, behavior);
                    net.BuildCG(subBatchSize);
                    
                    Nets.Add(net);
                    idx += 1;
                }
            }


            public float Run(IntPtr batch, IntPtr target, int idx, int batchSize)
            {
                return Run(batch, target, IntPtr.Zero, idx, batchSize);
            }

            public void Init()
            {
                foreach(VGG11Net net in Nets)
                {
                    net.Init();
                }
            }

            public void Complete()
            {
                foreach(VGG11Net net in Nets)
                {
                    net.Complete();
                }
            }

            public unsafe void TestNCCLAllReduce()
            {
                int size = 1000;
                Parallel.For(0, DeviceNum, i => 
                {
                    Cudalib.CudaInit(DeviceIds[i]);

                    Grads[i].Init(i * 0.1f);

                    Grads[i].Print("all reduce before " + i.ToString(), 100, true);
    
                    Cudalib.AllReduce(Grads[i].CudaPtr, 100, i, Comm);
                    
                    Grads[i].Print("all reduce after " + i.ToString(), 100, true);

                    //Cudalib.AllReduce(Grads[i].CudaPtr, 100, i, Comm);
                });
            }


            public float Run(IntPtr batch, IntPtr target, IntPtr o, int idx, int batchSize)
            {
                int memsize = 224 * 224 * 3 * batchSize;
                int subBatchSize = batchSize / DeviceNum;
                int subSize = 224 * 224 * 3 * subBatchSize;

                float[] stepLosses = new float[DeviceNum];

                //Logger.WriteLog("batch size {0}", subBatchSize);

                //IntPtr.Add(cudaPiecePointer, sizeof(float) * offset)
                Parallel.For(0, DeviceNum, i => 
                {
                    Cudalib.CudaInit(DeviceIds[i]);
                    Nets[i].InputImages.BatchSize = subBatchSize;
                    Cudalib.CudaCopyInFloat(Nets[i].InputImages.Output.CudaPtr, IntPtr.Add(batch, sizeof(float) * (idx * memsize + i * subSize)), subSize);
                    //(cudaP, (IntPtr)p, len);
                    //InputImages.Output.SyncFromCPU(0, batch, 0, 224 * 224 * 3 * batchSize);
                    long * p = (long * )target.ToPointer();
                    {
                        for(int iter = 0; iter < subBatchSize; iter ++)
                        {
                            Nets[i].Label.Idx[iter] = iter * 1000 + (int)p[iter + idx * batchSize + i * subBatchSize];
                        }
                    }
                    //for(int i = 0; i < batchSize; i++)
                    //{
                    //   Label.Idx[i] = i * 1000 + target[i];
                    //}
                    Nets[i].Label.Length = subBatchSize;
                    Nets[i].Label.Idx.SyncFromCPU();

                    Nets[i].StepV3Run();
                    //Grads[i].Print("all reduce before " + i.ToString(), 100, true);
                    if(Nets[i].Behavior.RunMode == DNNRunMode.Train)
                    {    
                        Cudalib.AllReduce(Grads[i].CudaPtr, Grads[i].Size, i, Comm);
                        Nets[i].StepV3Update();
                    }

                    //Cudalib.Zero(Grads[i].CudaPtr, Grads[i].Size);
                    stepLosses[i] = (float)Nets[i].StepLoss;
                    // all reduce happen here.
                    if(o != IntPtr.Zero)
                    {
                        Cudalib.CudaCopyOutFloat(Nets[i].O.Output.Data.CudaPtr, IntPtr.Add(o, sizeof(float) * subBatchSize * i * 1000), subBatchSize * 1000);
                    }
                });
                return (float)stepLosses.Sum() / DeviceNum;
            }

            public void SetTrainMode()
            {
                foreach(VGG11Net net in Nets)
                {
                    net.Behavior.RunMode = DNNRunMode.Train;
                }
            }

            public void SetPredictMode()
            {
                foreach(VGG11Net net in Nets)
                {
                    net.Behavior.RunMode = DNNRunMode.Predict;
                }
            } 
        }


        public float Run(IntPtr batch, IntPtr target, int idx, int batchSize)
        {
            return Run(batch, target, IntPtr.Zero, idx, batchSize);
        }



        public float Run(IntPtr batch, IntPtr target, IntPtr o, int idx, int batchSize)
        {
            InputImages.BatchSize = batchSize;
            int memsize = 224 * 224 * 3 * batchSize;
            //IntPtr.Add(cudaPiecePointer, sizeof(float) * offset)
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, IntPtr.Add(batch, sizeof(float) * idx * memsize), memsize);
            //(cudaP, (IntPtr)p, len);
            //InputImages.Output.SyncFromCPU(0, batch, 0, 224 * 224 * 3 * batchSize);
            long * p = (long * )target.ToPointer();
            {
                for(int i=0; i < batchSize; i++)
                {
                    Label.Idx[i] = i * 1000 + (int)p[i + idx * batchSize];
                }
            }
            //for(int i = 0; i < batchSize; i++)
            //{
            //   Label.Idx[i] = i * 1000 + target[i];
            //}
            Label.Length = batchSize;
            Label.Idx.SyncFromCPU();
            StepV2();
            if(o != IntPtr.Zero)
            {
                Cudalib.CudaCopyOutFloat(O.Output.Data.CudaPtr, o, batchSize * 1000);
            }
            return (float)StepLoss;
        }

        // call Init() before training on every epoch
        //public float Run(float[] batch, int[] target, int batchSize)
        //{
            //InputImages.BatchSize = batchSize;
            //InputImages.Output.SyncFromCPU(0, batch, 0, 224 * 224 * 3 * batchSize);

            //for(int i = 0; i < batchSize; i++)
            //{
            //    Label.Idx[i] = i * 1000 + target[i];
            //}
            //Label.Length = batchSize;
            //Label.Idx.SyncFromCPU();
            //Step();
        //    return (float)StepLoss;
        //}

        public float testSum(float[] batch, int batchSize)
        {
            InputImages.Output.SyncFromCPU(0, batch, 0, batchSize);
            float x = Behavior.Computelib.VectorSum(InputImages.Output, 0, batchSize, 1);
            return x;
        }

        public float[] testSoftmax(float[] batch, int batchSize)
        {
            //float[] tmp = new float[batchSize];
            CudaPieceFloat tmpC = new CudaPieceFloat(batchSize, Behavior.Device);
            tmpC.SyncFromCPU(0, batch, 0, batchSize);
            
            Behavior.Computelib.SoftMax(tmpC, tmpC, batchSize, 1, 1);
            tmpC.Print("softmax value ", batchSize, false);
            
            //InputImages.Output.SyncToCPU(0, tmp, 0, batchSize);
            tmpC.SyncToCPU();
            return null ;//tmpC.MemPtr;
            //float x = Behavior.Computelib.VectorSum(InputImages.Output, 0, batchSize, 1);
        }

        public void testPointer(IntPtr cudaP, int len)
        {
            float * p = (float * )cudaP.ToPointer();
            {
                for(int i=0; i < len; i++)
                {
                    Console.WriteLine(p[i]);
                    p[i] = i * 100 + 1;
                }
            }
            // float[] tmp = new float[len];
            // fixed(float * p = &tmp[0])
            // {
            //     Cudalib.CudaCopyOutFloat(cudaP, (IntPtr)p, len);
            // }
            // for(int i=0; i < len; i++)
            // {
            //     Console.WriteLine(tmp[i]);
            // }
        }
        // call complete after training on every epoch.
        public void SaveModel(int epoch)
        {
            using(BinaryWriter modelWriter = new BinaryWriter(new FileStream("vgg11." + epoch.ToString(), FileMode.Create, FileAccess.Write)))
            {
                Model.Serialize(modelWriter);
            }
        }



        public float Sum(float[] x, int offset, int length)
        {
           float result = 0;
           for(int i = 0; i < length; i ++ )
           {
               result += x[offset + i];
           }
           return result;
        }

        public int SumInt(int[] x, int offset, int length)
        {
           int result = 0;
           for(int i = 0; i < length; i ++ )
           {
               result += x[offset + i];
           }
           return result;
        }
    }
}
