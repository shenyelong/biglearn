using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.Image
{
    /// <summary>
    /// CNN for Image.
    /// </summary>
    public class CNNMNISTBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }

            public static string DataDir { get { return Argument["DATA"].Value; } }
            public static int BatchSize { get { return int.Parse(Argument["BATCH"].Value); } }
                        
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Image DataDir"));
                Argument.Add("BATCH", new ParameterArgument("20", "BatchSize"));
            }
        }

        public override BuilderType Type { get { return BuilderType.CNN_MNIST; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        class ImageDataRunner : StructRunner
        {
            public Tensor4DData Image { get; set; }
            public SparseVectorData Label { get; set; }

            byte[] Labels { get; set; }
            Tuple<int, int, int, byte[]> Images { get; set; }

            DataRandomShuffling Shuffle { get; set; }
            int BatchSize { get; set; }
            int CategoryNum { get; set; }

            public ImageDataRunner(Tuple<int, int, int, byte[]> image, byte[] label, int batchSize, int categoryNum, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                Images = image;
                Labels = label;

                Image = new Tensor4DData(image.Item2, image.Item3, 1, batchSize, false, behavior.Device);
                Label = new SparseVectorData(batchSize, behavior.Device, false);
                Label.Value.Init(1);

                Shuffle = new DataRandomShuffling(image.Item1, ParameterSetting.Random);
                BatchSize = batchSize;
                CategoryNum = categoryNum;

                Logger.WriteLog("image width {0}, height {1}, size {2}, batchSize {3}", image.Item2, image.Item3, image.Item1, batchSize);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                if(!Shuffle.TestRandomBatch(BatchSize))
                {
                    IsTerminate = true;
                    IsContinue = false;
                    return;
                }
                int width = Images.Item2;
                int height = Images.Item3;

                for(int i = 0; i < BatchSize; i++)
                {
                    int idx = Shuffle.RandomNext();
                    Label.Idx[i] = i * CategoryNum + Labels[idx];
                    for (int x = 0; x < width * height; x++)
                    {
                        Image.Output[i * (width * height) + x] = Images.Item4[idx * width * height + x] / 255.0f;
                        //Console.WriteLine("pixel {0} {1}", x, Image.Output[i * (width * height) + x]);
                        
                        if(Image.Output[i * (width * height) + x] > 1)
                        {
                            throw new Exception("Image data is wrong");
                        }
                    }
                    //Console.WriteLine("label {0}", Label.Idx[i]);
                    //Console.ReadLine();
                }
                Label.Length = BatchSize;
                Label.Idx.SyncFromCPU();

                Image.BatchSize = BatchSize;
                //Logger.WriteLog("Image Length {0}", Image.Length);
                Image.Output.SyncFromCPU();
            }

        }

        public class CNNModel : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            public TensorConvStructure Conv2 = null;
            public LayerStructure FClayer = null;

            public CNNModel(DeviceType device)
            {
                Conv1 = AddLayer(new TensorConvStructure(5, 5, 1, 8, device));
                Conv2 = AddLayer(new TensorConvStructure(5, 5, 8, 16, device));
                FClayer = AddLayer(new LayerStructure(256, 10, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device));
            }

            // public CNNModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
            //     Conv1 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     Conv2 = DeserializeModel<TensorConvStructure>(reader, device); 
            //     FClayer = DeserializeModel<LayerStructure>(reader, device); 
            // }

            //public void InitOptimization(RunnerBehavior behavior)
            //{
            //    InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            //}
        }

        public static ComputationGraph BuildComputationGraph(Tuple<int, int, int, byte[]> imgData,
            byte[] labelData,  int batchSize, CNNModel model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph(Behavior);

            ImageDataRunner imgRunner = new ImageDataRunner(imgData, labelData, batchSize, 10, Behavior);
            cg.AddDataRunner(imgRunner);

            Tensor4DData image = imgRunner.Image;
            SparseVectorData label = imgRunner.Label;

            TensorConvRunner conv1Runner = new TensorConvRunner(model.Conv1, image, 2, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv1Runner);

            TensorMaxPooling2DRunner pool1Runner = new TensorMaxPooling2DRunner(conv1Runner.Output, 2, 2, Behavior);
            cg.AddRunner(pool1Runner);

            TensorConvRunner conv2Runner = new TensorConvRunner(model.Conv2, pool1Runner.Output, 2, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(conv2Runner);

            TensorMaxPooling2DRunner pool2Runner = new TensorMaxPooling2DRunner(conv2Runner.Output, 3, 3, Behavior);
            cg.AddRunner(pool2Runner);
            NdArrayData poolOut = pool2Runner.Output.ToND();

            int fc_flat_dim = poolOut.Shape[0].Default * poolOut.Shape[1].Default * poolOut.Shape[2].Default;
            Logger.WriteLog("pool width {0}, pool height {1}, pool depth {2}, FC input {3}, batch size {4}", 
                        poolOut.Shape[0].Default, poolOut.Shape[1].Default, poolOut.Shape[2].Default,
                        fc_flat_dim, poolOut.Shape[3].Default);

            HiddenBatchData flatEmbed = poolOut.Reshape(new IntArgument("FLAT_DIM", fc_flat_dim), poolOut.Shape[3]).ToHDB();

            
            if(Behavior.RunMode == DNNRunMode.Train)
            {
                TensorDropoutRunner drop = new TensorDropoutRunner(flatEmbed.ToT4D(), 0.5f, Behavior);
                cg.AddRunner(drop);
                flatEmbed = drop.Output.ToHDB();
            }

            FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(model.FClayer, flatEmbed, Behavior);
            cg.AddRunner(fcRunner);
            HiddenBatchData output = fcRunner.Output;

            if(Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddObjective(new SoftmaxObjRunner(label, output, Behavior)); 
            }
            else
            {
                cg.AddRunner(new AccuracyRunner(label.Idx, output, Behavior));
            }

            cg.SetDelegateModel(model);
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceBehavior behavior = new DeviceBehavior(BuilderParameters.GPUID);

            //DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            //IMathOperationManager ComputeLib = MathOperatorManager.CreateInstance(device);
            //new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = ComputeLib });
            Logger.WriteLog("Loading Training/Validation Data.");
            MNISTDataPanel.Init(BuilderParameters.DataDir);
            Logger.WriteLog("Load Data Finished.");


            CNNModel cnnModel = new CNNModel(behavior.Device);
            cnnModel.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior.TrainMode);
            Logger.WriteLog("Learning rate {0}", OptimizerParameters.LearnRate);

            ComputationGraph train_cg = BuildComputationGraph(MNISTDataPanel.TrainImage, MNISTDataPanel.TrainLabel, BuilderParameters.BatchSize, cnnModel, behavior.TrainMode);
            ComputationGraph valid_cg = BuildComputationGraph(MNISTDataPanel.ValidImage, MNISTDataPanel.ValidLabel, BuilderParameters.BatchSize, cnnModel, behavior.PredictMode);

            double best_score = 0;
            double best_iter = -1;
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                double loss = train_cg.Execute();
                Logger.WriteLog("Train Loss {0}", loss);
                
                double score = valid_cg.Execute();
                Logger.WriteLog("Prediction Accuracy {0}, at Iteration {1}", score, iter);

                if (score > best_score)
                {
                    best_score = score;
                    best_iter = iter;
                }
                Logger.WriteLog("Best Accuracy {0}, at Iteration {1}", best_score, best_iter);
            }
            Logger.CloseLog();
        }
    }
}
