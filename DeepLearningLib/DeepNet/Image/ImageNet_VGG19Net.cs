using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Image
{
    unsafe public class ImageNet_VGG19Net : ComputationGraph
    {
        public string name = "ImageNet_VGG19Net";

        public class VGG19Model : CompositeNNStructure
        {
            public TensorConvStructure Conv1_1 = null;
            public TensorConvStructure Conv1_2 = null;
            
            public TensorConvStructure Conv2_1 = null;
            public TensorConvStructure Conv2_2 = null;
            
            public TensorConvStructure Conv3_1 = null;
            public TensorConvStructure Conv3_2 = null;
            public TensorConvStructure Conv3_3 = null;
            public TensorConvStructure Conv3_4 = null;
            
            public TensorConvStructure Conv4_1 = null;
            public TensorConvStructure Conv4_2 = null;
            public TensorConvStructure Conv4_3 = null;
            public TensorConvStructure Conv4_4 = null;
            
            public TensorConvStructure Conv5_1 = null;
            public TensorConvStructure Conv5_2 = null;
            public TensorConvStructure Conv5_3 = null;
            public TensorConvStructure Conv5_4 = null;
            
            public LayerStructure FC6 = null;
            public LayerStructure FC7 = null;
            public LayerStructure FC8 = null;
            
            public int Category { get; set; }

            // how to make sure the model parameters allocated into a flat array.
            public VGG19Model(int category, DeviceType device)
            {
                Category = category;

                Conv1_1 = AddLayer(new TensorConvStructure(3, 3, 3, 64, device));
                Conv1_2 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));

                Conv2_1 = AddLayer(new TensorConvStructure(3, 3, 64, 128, device));
                Conv2_2 = AddLayer(new TensorConvStructure(3, 3, 128, 128, device));

                Conv3_1 = AddLayer(new TensorConvStructure(3, 3, 128, 256, device));
                Conv3_2 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));
                Conv3_3 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));
                Conv3_4 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));

                Conv4_1 = AddLayer(new TensorConvStructure(3, 3, 256, 512, device));
                Conv4_2 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv4_3 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv4_4 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));

                Conv5_1 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv5_2 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv5_3 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv5_4 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                
                FC6 = AddLayer(new LayerStructure(512 * 7 * 7, 4096, A_Func.Rectified, N_Type.Fully_Connected, 1, 0, true, device));
                FC7 = AddLayer(new LayerStructure(4096, 4096, A_Func.Rectified, N_Type.Fully_Connected, 1, 0, true, device));
                FC8 = AddLayer(new LayerStructure(4096, category, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device));
            }

            public VGG19Model(int category, string meta_file, string bin_file, DeviceType device) : this(category, device)
            {
                using(StreamReader metaReader = new StreamReader(meta_file))
                using(BinaryReader binReader = new BinaryReader(new FileStream(bin_file, FileMode.Open, FileAccess.Read)))
                {
                    while(!metaReader.EndOfStream)
                    {
                        string[] items = metaReader.ReadLine().Split('\t');
                        string itemName = items[0];
                        int itemSize = int.Parse(items[1]);

                        // layer 1-1.
                        if(itemName.Equals("vgg_19/conv1/conv1_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv1_1.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv1_1.Filter.CpuPtr, new int[] { 64, 3, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            BasicMathlib.Scale_Vector(Conv1_1.Filter.CpuPtr, itemSize, 255);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv1/conv1_1/biases"))
                        {
                            Conv1_1.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);                            
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 1-2
                        if(itemName.Equals("vgg_19/conv1/conv1_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv1_2.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv1_2.Filter.CpuPtr, new int[] { 64, 64, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv1/conv1_2/biases"))
                        {
                            Conv1_2.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        //layer 2-1
                        if(itemName.Equals("vgg_19/conv2/conv2_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv2_1.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv2_1.Filter.CpuPtr, new int[] { 128, 64, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv2/conv2_1/biases"))
                        {
                            Conv2_1.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 2-2
                        if(itemName.Equals("vgg_19/conv2/conv2_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv2_2.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv2_2.Filter.CpuPtr, new int[] { 128, 128, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv2/conv2_2/biases"))
                        {
                            Conv2_2.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        //layer 3-1
                        if(itemName.Equals("vgg_19/conv3/conv3_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv3_1.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv3_1.Filter.CpuPtr, new int[] { 256, 128, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv3/conv3_1/biases"))
                        {
                            Conv3_1.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 3-2
                        if(itemName.Equals("vgg_19/conv3/conv3_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv3_2.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv3_2.Filter.CpuPtr, new int[] { 256, 256, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv3/conv3_2/biases"))
                        {
                            Conv3_2.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 3-3
                        if(itemName.Equals("vgg_19/conv3/conv3_3/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv3_3.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv3_3.Filter.CpuPtr, new int[] { 256, 256, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv3/conv3_3/biases"))
                        {
                            Conv3_3.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 3-4
                        if(itemName.Equals("vgg_19/conv3/conv3_4/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv3_4.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv3_4.Filter.CpuPtr, new int[] { 256, 256, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv3/conv3_4/biases"))
                        {
                            Conv3_4.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        //layer 4-1
                        if(itemName.Equals("vgg_19/conv4/conv4_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv4_1.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv4_1.Filter.CpuPtr, new int[] { 512, 256, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv4/conv4_1/biases"))
                        {
                            Conv4_1.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 4-2
                        if(itemName.Equals("vgg_19/conv4/conv4_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv4_2.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv4_2.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv4/conv4_2/biases"))
                        {
                            Conv4_2.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 4-3
                        if(itemName.Equals("vgg_19/conv4/conv4_3/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv4_3.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv4_3.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv4/conv4_3/biases"))
                        {
                            Conv4_3.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 4-4
                        if(itemName.Equals("vgg_19/conv4/conv4_4/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv4_4.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv4_4.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv4/conv4_4/biases"))
                        {
                            Conv4_4.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        //layer 5-1
                        if(itemName.Equals("vgg_19/conv5/conv5_1/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv5_1.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv5_1.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv5/conv5_1/biases"))
                        {
                            Conv5_1.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 5-2
                        if(itemName.Equals("vgg_19/conv5/conv5_2/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv5_2.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv5_2.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv5/conv5_2/biases"))
                        {
                            Conv5_2.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 5-3
                        if(itemName.Equals("vgg_19/conv5/conv5_3/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv5_3.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv5_3.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv5/conv5_3/biases"))
                        {
                            Conv5_3.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        //layer 5-4
                        if(itemName.Equals("vgg_19/conv5/conv5_4/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            Conv5_4.Filter.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(Conv5_4.Filter.CpuPtr, new int[] { 512, 512, 3, 3 }, new int[] { 2, 3, 1, 0 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/conv5/conv5_4/biases"))
                        {
                            Conv5_4.Bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        

                        // fc6
                        if(itemName.Equals("vgg_19/fc6/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            FC6.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            BasicMathlib.Transpose(FC6.weight.CpuPtr, new int[] { 4096, 512, 7, 7 }, new int[] { 0, 2, 3, 1 } );
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/fc6/biases"))
                        {
                            FC6.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        // fc7
                        if(itemName.Equals("vgg_19/fc7/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            FC7.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/fc7/biases"))
                        {
                            FC7.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        // fc8
                        if(itemName.Equals("vgg_19/fc8/weights"))
                        {
                            //[filter_height, filter_width, in_channels, out_channels]
                            FC8.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }
                        if(itemName.Equals("vgg_19/fc8/biases"))
                        {
                            FC8.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize {0} from checkpoint", itemName);
                            continue;
                        }

                        Logger.WriteLog("Component is not found {0} : size {1}, skip....................", itemName, itemSize);
                        binReader.ReadBytes(sizeof(float) * itemSize);

                    }
                }
                Conv1_1.SyncFromCPU();
                Conv1_2.SyncFromCPU();
                Conv2_1.SyncFromCPU();
                Conv2_2.SyncFromCPU();
                Conv3_1.SyncFromCPU();
                Conv3_2.SyncFromCPU();
                Conv3_3.SyncFromCPU();
                Conv3_4.SyncFromCPU();
                Conv4_1.SyncFromCPU();
                Conv4_2.SyncFromCPU();
                Conv4_3.SyncFromCPU();
                Conv4_4.SyncFromCPU();
                Conv5_1.SyncFromCPU();
                Conv5_2.SyncFromCPU();
                Conv5_3.SyncFromCPU();
                Conv5_4.SyncFromCPU();
                FC6.SyncFromCPU();
                FC7.SyncFromCPU();
                FC8.SyncFromCPU();

            }
        }

        public Tensor4DData InputImages { get; set; }
        public SparseVectorData Label { get; set; }

        public new VGG19Model Model { get; set; }
        public HiddenBatchData O { get; set; }

        public int Width { get { return InputImages.Width; } }
        public int Height { get { return InputImages.Height; } }
        public int Channel { get { return InputImages.Depth; } }

        public ImageNet_VGG19Net(VGG19Model model, RunnerBehavior behavior) : base(behavior)
        {
            Model = model; // Model = new VGG11Model(Behavior); 
            SetDelegateModel(Model);
        }

        //"""Select conv1_1 ~ conv5_1 activation maps.""" 
        Dictionary<string, Tensor4DData> LayerFeatuers = new Dictionary<string, Tensor4DData>();

        public HiddenBatchData BuildFeaturizer()
        {
            // x --> 3 * 224 * 224
            TensorConvRunner conv1_1Runner = new TensorConvRunner(Model.Conv1_1, InputImages, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv1_1Runner);

            LayerFeatuers.Add("conv-1-1", conv1_1Runner.Output);

            // x --> 64 * 224 * 224
            TensorConvRunner conv1_2Runner = new TensorConvRunner(Model.Conv1_2, conv1_1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv1_2Runner);

            LayerFeatuers.Add("conv-1-2", conv1_2Runner.Output);

            // x --> 64 * 224 * 224
            TensorMaxPooling2DRunner pool1Runner = new TensorMaxPooling2DRunner(conv1_2Runner.Output, 2, 2, Behavior);
            AddRunner(pool1Runner);

            // x --> 64 * 112 * 112
            TensorConvRunner conv2_1Runner = new TensorConvRunner(Model.Conv2_1, pool1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv2_1Runner);
            
            LayerFeatuers.Add("conv-2-1", conv2_1Runner.Output);    

            // x --> 128 * 112 * 112
            TensorConvRunner conv2_2Runner = new TensorConvRunner(Model.Conv2_2, conv2_1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv2_2Runner);

            LayerFeatuers.Add("conv-2-2", conv2_2Runner.Output);    

            // x --> 128 * 112 * 112
            TensorMaxPooling2DRunner pool2Runner = new TensorMaxPooling2DRunner(conv2_2Runner.Output, 2, 2, Behavior);
            AddRunner(pool2Runner);
            // x --> 128 * 56 * 56
            TensorConvRunner conv3_1Runner = new TensorConvRunner(Model.Conv3_1, pool2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv3_1Runner);

            LayerFeatuers.Add("conv-3-1", conv3_1Runner.Output);    

            // x --> 256 * 56 * 56
            TensorConvRunner conv3_2Runner = new TensorConvRunner(Model.Conv3_2, conv3_1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv3_2Runner);

            LayerFeatuers.Add("conv-3-2", conv3_2Runner.Output);    

            // x --> 256 * 56 * 56
            TensorConvRunner conv3_3Runner = new TensorConvRunner(Model.Conv3_3, conv3_2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv3_3Runner);

            LayerFeatuers.Add("conv-3-3", conv3_3Runner.Output);    

            // x --> 256 * 56 * 56
            TensorConvRunner conv3_4Runner = new TensorConvRunner(Model.Conv3_4, conv3_3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv3_4Runner);

            LayerFeatuers.Add("conv-3-4", conv3_4Runner.Output);    

            // x --> 256 * 56 * 56
            TensorMaxPooling2DRunner pool3Runner = new TensorMaxPooling2DRunner(conv3_4Runner.Output, 2, 2, Behavior);
            AddRunner(pool3Runner);
            // x --> 256 * 28 * 28
            TensorConvRunner conv4_1Runner = new TensorConvRunner(Model.Conv4_1, pool3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv4_1Runner);

            LayerFeatuers.Add("conv-4-1", conv4_1Runner.Output);    

            // x --> 512 * 28 * 28
            TensorConvRunner conv4_2Runner = new TensorConvRunner(Model.Conv4_2, conv4_1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv4_2Runner);
            
            LayerFeatuers.Add("conv-4-2", conv4_2Runner.Output);    
            
            // x --> 512 * 28 * 28
            TensorConvRunner conv4_3Runner = new TensorConvRunner(Model.Conv4_3, conv4_2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv4_3Runner);
            
            LayerFeatuers.Add("conv-4-3", conv4_3Runner.Output);    

            // x --> 512 * 28 * 28
            TensorConvRunner conv4_4Runner = new TensorConvRunner(Model.Conv4_4, conv4_3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv4_4Runner);
            
            LayerFeatuers.Add("conv-4-4", conv4_4Runner.Output);    

            // x --> 512 * 28 * 28
            TensorMaxPooling2DRunner pool4Runner = new TensorMaxPooling2DRunner(conv4_4Runner.Output, 2, 2, Behavior);
            AddRunner(pool4Runner);
            
            // x --> 512 * 14 * 14
            TensorConvRunner conv5_1Runner = new TensorConvRunner(Model.Conv5_1, pool4Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv5_1Runner);
            
            LayerFeatuers.Add("conv-5-1", conv5_1Runner.Output);    

            // x --> 512 * 14 * 14
            TensorConvRunner conv5_2Runner = new TensorConvRunner(Model.Conv5_2, conv5_1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv5_2Runner);
            
            LayerFeatuers.Add("conv-5-2", conv5_2Runner.Output);    

            // x --> 512 * 14 * 14
            TensorConvRunner conv5_3Runner = new TensorConvRunner(Model.Conv5_3, conv5_2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv5_3Runner);
            
            LayerFeatuers.Add("conv-5-3", conv5_3Runner.Output);    

            // x --> 512 * 14 * 14
            TensorConvRunner conv5_4Runner = new TensorConvRunner(Model.Conv5_4, conv5_3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv5_4Runner);
            
            LayerFeatuers.Add("conv-5-4", conv5_4Runner.Output);    
            
            // x --> 512 * 14 * 14 
            TensorMaxPooling2DRunner pool5Runner = new TensorMaxPooling2DRunner(conv5_4Runner.Output, 2, 2, Behavior);
            AddRunner(pool5Runner);
            
            // x --> 512 * 7 * 7 
            NdArrayData poolOut = pool5Runner.Output.ToND();

            int fc_flat_dim = poolOut.Shape[0].Default * poolOut.Shape[1].Default * poolOut.Shape[2].Default;

            HiddenBatchData flatEmbed = poolOut.Reshape(new IntArgument("FLAT_DIM", fc_flat_dim), poolOut.Shape[3]).ToHDB();

            FullyConnectHiddenRunner<HiddenBatchData> fc6Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FC6, flatEmbed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fc6Runner);

            FullyConnectHiddenRunner<HiddenBatchData> fc7Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FC7, fc6Runner.Output, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fc7Runner);

            FullyConnectHiddenRunner<HiddenBatchData> fc8Runner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FC8, fc7Runner.Output, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fc8Runner);            

            O = fc8Runner.Output;

            return O;
        }
        
        HiddenBatchData[] StyleFeatures { get; set; }
        Tensor4DData[] ContentFeatures { get; set; }

        MSERunner[] ContentLoss { get; set; }
        MSERunner[] StyleLoss { get; set; }

        public void BuildFeatureGraph(int width, int height, int channel, string[] layerNames, int featureNum)
        {
            InputImages = new Tensor4DData(width, height, channel, 1, false, Behavior.Device);
            BuildFeaturizer();
            
            StyleFeatures = new HiddenBatchData[featureNum];
            ContentFeatures = new Tensor4DData[featureNum];

            for(int i = 0; i < featureNum; i++)
            {
                ContentFeatures[i] = LayerFeatuers[layerNames[i]];
                StyleFeatures[i] = new HiddenBatchData(ContentFeatures[i].Depth, ContentFeatures[i].Depth, true, Behavior.Device);
            }
        }

        public void BuildTransferGraph(int width, int height, int channel, string[] layerNames, Tensor4DData[] conFeas, HiddenBatchData[] styleFeas, float style_weight, int featureNum)
        {
            InputImages = new Tensor4DData(width, height, channel, 1, true, Behavior.Device);
            BuildFeaturizer();

            StyleFeatures = new HiddenBatchData[featureNum];
            ContentFeatures = new Tensor4DData[featureNum];

            ContentLoss = new MSERunner[featureNum];
            StyleLoss = new MSERunner[featureNum];

            for(int i = 0; i < featureNum; i++)
            {
                ContentFeatures[i] = LayerFeatuers[layerNames[i]];
                ContentLoss[i] = new MSERunner(conFeas[i].Output, ContentFeatures[i].Output, ContentFeatures[i].Deriv, ContentFeatures[i].MaxLength, 1.0f, Behavior); 
                AddObjective(ContentLoss[i]); 
                
                IntArgument depth = ContentFeatures[i].DepthArg;
                IntArgument wh = new IntArgument("WH", ContentFeatures[i].Width * ContentFeatures[i].Height);
                Logger.WriteLog("Feature Map : {0}, Depth : {1}, Height : {2}, Width : {3}, style weight : {4} ", layerNames[i], depth.Default, ContentFeatures[i].Height, ContentFeatures[i].Width, style_weight * 1.0f / (depth.Default * wh.Default));

                MatrixData content_matrix = ContentFeatures[i].ToND().Reshape(new IntArgument[] { wh, depth }).ToMD();

                StyleFeatures[i] = content_matrix.MatMul(0, content_matrix, 1, Session, Behavior).ToHDB();
                StyleLoss[i] = new MSERunner(styleFeas[i].Output.Data, StyleFeatures[i].Output.Data, StyleFeatures[i].Deriv.Data, depth.Default * depth.Default, style_weight * 1.0f / (depth.Default * wh.Default), Behavior); 
                AddObjective(StyleLoss[i]);
            }
        }

        public void SetStyleOptimizer(StructureLearner learner)
        {
            AddOptimizer(GradientOptimizer.CreateLocalOptimizer(InputImages.Output, InputImages.Deriv, learner, Behavior));
        }

        public float[] StyleTransfer(IntPtr target)
        {
            InputImages.BatchSize = 1;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, target, memsize);
            // ComputeLib.Zero(InputImages.Deriv, memsize);
            
            //for(int step = 0; step < totalStep; step++)
            //{
            StepV3Run();
            StepV3Update();
            //}

            //InputImages.Output.Init(1.0f);
            Cudalib.CudaCopyOutFloat(InputImages.Output.CudaPtr, target, memsize);

            //(float)ContentLoss.Select(c => c.ObjectiveScore).Sum()
            return new float[] { (float)ContentLoss.Select(c => c.ObjectiveScore).Sum(), (float)StyleLoss.Select(s => s.ObjectiveScore).Sum() };

        }

        public Tensor4DData[] ExtractContentFeature(IntPtr input)
        {
            InputImages.BatchSize = 1;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, input, memsize);

            Forward();

            return ContentFeatures;
        }


        public HiddenBatchData[] ExtractStyleFeature(IntPtr input)
        {
            InputImages.BatchSize = 1;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, input, memsize);

            Forward();

            for(int i = 0; i < ContentFeatures.Length; i++)
            {
                //ComputeLib.Sgemm(InputA.Output, 0, InputB.Output, 0, Output.Output, 0, InputA.Row, InputA.Column, InputB.Row, Alpha, Beta, false, true);
                ComputeLib.Sgemm(ContentFeatures[i].Output, 0, ContentFeatures[i].Output, 0, StyleFeatures[i].Output.Data, 0, 
                                ContentFeatures[i].Depth, ContentFeatures[i].Width * ContentFeatures[i].Height, ContentFeatures[i].Depth,
                                0, 1, false, true);
            }
            return StyleFeatures;
        }


        // classifier.
        public void BuildCG(int width, int height, int channel, int batchSize)
        {
            InputImages = new Tensor4DData(width, height, channel, batchSize, false, Behavior.Device);
            
            Label = new SparseVectorData(batchSize, Behavior.Device, false);
            Label.Value.Init(1);

            BuildFeaturizer();

            AddObjective(new SoftmaxObjRunner(Label, O, Behavior)); 
            
            SetDelegateModel(Model);
        }

        // predict image.
        public unsafe void Predict(IntPtr batch, IntPtr o, int batchSize)
        {
            InputImages.BatchSize = batchSize;
            int memsize = InputImages.Length;
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, batch, memsize);

            StepV3Run();

            Cudalib.CudaCopyOutFloat(O.Output.Data.CudaPtr, o, batchSize * Model.Category);
        }
    }

    public class DistImageNet_VGG19Net
    {
        public ImageNet_VGG19Net[] Nets { get; set; }
        int DeviceNum { get { return DeviceIds.Length; } }
        int[] DeviceIds { get; set; }

        IntPtr Comm { get; set; }
        CudaPieceFloat[] Grads { get; set; }

        ImageNet_VGG19Net.VGG19Model Model { get; set; }

        public unsafe DistImageNet_VGG19Net(ImageNet_VGG19Net.VGG19Model model, StructureLearner learner, int deviceNum)
        {
            Model = model;
            DeviceIds = new int[deviceNum];
            for(int i = 0; i < deviceNum; i++) DeviceIds[i] = i;

            fixed (int * pDevice = & DeviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, deviceNum);
            }
            Grads = new CudaPieceFloat[deviceNum];
            Nets = new ImageNet_VGG19Net[deviceNum];

            Parallel.For(0, DeviceNum, i =>  //foreach(int devId in DeviceIds)
            {
                int devId = DeviceIds[i];

                Logger.WriteLog("Create Device {0}", devId);
                DeviceType device = MathOperatorManager.SetDevice(devId);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

                ImageNet_VGG19Net.VGG19Model device_model = new ImageNet_VGG19Net.VGG19Model(model.Category, device);
                device_model.CopyFrom(model);

                Grads[i] = device_model.InitOptimizer(learner, behavior);

                Nets[i] = new ImageNet_VGG19Net(device_model, behavior);
            });
        }

        public void BuildCG(int width, int height, int channel, int batchSize)
        {
            int subBatchSize = batchSize / DeviceNum;
            //int idx = 0;
            //foreach(Bert net in Nets)
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].BuildCG(width, height, channel, subBatchSize);
            });
        }

        public void Init()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Init();
            });
        }

        public void Complete()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Complete();
            });
        }

        public void SetTrainMode()
        {
            foreach(ImageNet_VGG19Net net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Train;
            }
        }

        public void SetPredictMode()
        {
            foreach(ImageNet_VGG19Net net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Predict;
            }
        } 

        public void SaveModel(string path)
        {
            Nets[0].Model.Save(path);
        }

        //10.93.128.213
        //Predict(IntPtr batch, IntPtr o, int batchSize)
        public unsafe void Predict(IntPtr batch, IntPtr o, int batchSize)
        {
            int subBatchSize = batchSize / DeviceNum;

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                Nets[i].Predict(IntPtr.Add(batch, sizeof(float) * i * subBatchSize * Nets[i].Width * Nets[i].Height * Nets[i].Channel),  
                                IntPtr.Add(o, sizeof(float) * i * subBatchSize * Nets[i].Model.Category), 
                                subBatchSize);
            });

        }

    }





}
