using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Image
{
    unsafe public class CIFAR_VGG11Net : ComputationGraph
    {
        public string name = "CIFAR_VGG11Net";

        public class VGG11Model : CompositeNNStructure
        {
            public TensorConvStructure Conv1 = null;
            //public TensorConvStructure Conv2 = null;
            public TensorConvStructure Conv3 = null;
            //public TensorConvStructure Conv4 = null;
            public TensorConvStructure Conv5 = null;
            public TensorConvStructure Conv6 = null;
            //public TensorConvStructure Conv7 = null;
            public TensorConvStructure Conv8 = null;
            public TensorConvStructure Conv9 = null;
            //public TensorConvStructure Conv10 = null;
            public TensorConvStructure Conv11 = null;
            public TensorConvStructure Conv12 = null;
            //public TensorConvStructure Conv13 = null;
            public LayerStructure FClayer = null;
            
            public int Category { get; set; }

            // how to make sure the model parameters allocated into a flat array.
            public VGG11Model(int category, DeviceType device)
            {
                Category = category;

                Conv1 = AddLayer(new TensorConvStructure(3, 3, 3, 64, device));
                
                //Conv2 = AddLayer(new TensorConvStructure(3, 3, 64, 64, device));
                Conv3 = AddLayer(new TensorConvStructure(3, 3, 64, 128, device));
                //Conv4 = AddLayer(new TensorConvStructure(3, 3, 128, 128, device));
                Conv5 = AddLayer(new TensorConvStructure(3, 3, 128, 256, device));
                Conv6 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));
                //Conv7 = AddLayer(new TensorConvStructure(3, 3, 256, 256, device));
                Conv8 = AddLayer(new TensorConvStructure(3, 3, 256, 512, device));
                Conv9 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                //Conv10 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv11 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                Conv12 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                //Conv13 = AddLayer(new TensorConvStructure(3, 3, 512, 512, device));
                FClayer = AddLayer(new LayerStructure(512, category, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device));
                //FClayer2 = AddLayer(new LayerStructure(4096, 4096, A_Func.Rectified, N_Type.Fully_Connected, 1, 0, true, device));
                //FClayer3 = AddLayer(new LayerStructure(4096, 1000, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device));
            }
        }

        public Tensor4DData InputImages { get; set; }
        public SparseVectorData Label { get; set; }

        public new VGG11Model Model { get; set; }
        public HiddenBatchData O { get; set; }
        // for test purpose.
        //public VGG11Net()
        //{ }
        public CIFAR_VGG11Net(VGG11Model model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior; // new RunnerBehavior(DNNRunMode.Train, device, computeLib); 
            Model = model; // Model = new VGG11Model(Behavior); 
            SetDelegateModel(Model);
        }

        public void Build(int width, int height, int channel, int batchSize)
        {
            InputImages = new Tensor4DData(width, height, channel, batchSize, false, Behavior.Device);
            Label = new SparseVectorData(batchSize, Behavior.Device, false);
            Label.Value.Init(1);

            // x --> 3 * 224 * 224,
            TensorConvRunner conv1Runner = new TensorConvRunner(Model.Conv1, InputImages, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv1Runner);
            // x --> 64 * 224 * 224
            //TensorConvRunner conv2Runner = new TensorConvRunner(Model.Conv2, conv1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv2Runner);
            // x --> 64 * 224 * 224
            TensorMaxPooling2DRunner pool1Runner = new TensorMaxPooling2DRunner(conv1Runner.Output, 2, 2, Behavior);
            AddRunner(pool1Runner);
            // x --> 64 * 112 * 112
            TensorConvRunner conv3Runner = new TensorConvRunner(Model.Conv3, pool1Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv3Runner);
            // x --> 128 * 112 * 112
            //TensorConvRunner conv4Runner = new TensorConvRunner(Model.Conv4, conv3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv4Runner);
            // x --> 128 * 112 * 112
            TensorMaxPooling2DRunner pool2Runner = new TensorMaxPooling2DRunner(conv3Runner.Output, 2, 2, Behavior);
            AddRunner(pool2Runner);
            // x --> 128 * 56 * 56
            TensorConvRunner conv5Runner = new TensorConvRunner(Model.Conv5, pool2Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv5Runner);
            // x --> 256 * 56 * 56
            TensorConvRunner conv6Runner = new TensorConvRunner(Model.Conv6, conv5Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv6Runner);
            // x --> 256 * 56 * 56
            //TensorConvRunner conv7Runner = new TensorConvRunner(Model.Conv7, conv6Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv7Runner);
            // x --> 256 * 56 * 56
            TensorMaxPooling2DRunner pool3Runner = new TensorMaxPooling2DRunner(conv6Runner.Output, 2, 2, Behavior);
            AddRunner(pool3Runner);
            // x --> 256 * 28 * 28
            TensorConvRunner conv8Runner = new TensorConvRunner(Model.Conv8, pool3Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv8Runner);
            // x --> 512 * 28 * 28
            TensorConvRunner conv9Runner = new TensorConvRunner(Model.Conv9, conv8Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv9Runner);
            // x --> 512 * 28 * 28
            //TensorConvRunner conv10Runner = new TensorConvRunner(Model.Conv10, conv9Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv10Runner);
            // x --> 512 * 28 * 28
            TensorMaxPooling2DRunner pool4Runner = new TensorMaxPooling2DRunner(conv9Runner.Output, 2, 2, Behavior);
            AddRunner(pool4Runner);
            // x --> 512 * 14 * 14
            TensorConvRunner conv11Runner = new TensorConvRunner(Model.Conv11, pool4Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv11Runner);
            // x --> 512 * 14 * 14
            TensorConvRunner conv12Runner = new TensorConvRunner(Model.Conv12, conv11Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            AddRunner(conv12Runner);
            // x --> 512 * 14 * 14
            //TensorConvRunner conv13Runner = new TensorConvRunner(Model.Conv13, conv12Runner.Output, 1, 1, A_Func.Rectified, Behavior);
            //cg.AddRunner(conv13Runner);
            // x --> 512 * 14 * 14 
            TensorMaxPooling2DRunner pool5Runner = new TensorMaxPooling2DRunner(conv12Runner.Output, 2, 2, Behavior);
            AddRunner(pool5Runner);

            // x --> 512 * 7 * 7 
            NdArrayData poolOut = pool5Runner.Output.ToND();

            int fc_flat_dim = poolOut.Shape[0].Default * poolOut.Shape[1].Default * poolOut.Shape[2].Default;
            //Logger.WriteLog("pool width {0}, pool height {1}, pool depth {2}, FC input {3}, batch size {4}", 
            //            poolOut.Shape[0].Default, poolOut.Shape[1].Default, poolOut.Shape[2].Default,
            //            fc_flat_dim, poolOut.Shape[3].Default);

            HiddenBatchData flatEmbed = poolOut.Reshape(new IntArgument("FLAT_DIM", fc_flat_dim), poolOut.Shape[3]).ToHDB();


            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.FClayer, flatEmbed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            O = classifierRunner.Output;
            
            //if(Behavior.RunMode == DNNRunMode.Train)
            //{
            AddObjective(new SoftmaxObjRunner(Label, O, Behavior)); 
            //}
            //else
            //{
            //    AddRunner(new AccuracyRunner(Label.Idx, o3, Behavior));
            //}
            
        } 

        public float Run(IntPtr batch, IntPtr target, IntPtr o, int batchSize)
        {
            InputImages.BatchSize = batchSize;
            int memsize = InputImages.Length;

            //IntPtr.Add(cudaPiecePointer, sizeof(float) * offset)
            Cudalib.CudaCopyInFloat(InputImages.Output.CudaPtr, batch, memsize);
            //(cudaP, (IntPtr)p, len);
            //InputImages.Output.SyncFromCPU(0, batch, 0, 224 * 224 * 3 * batchSize);
            long * p = (long * )target.ToPointer();
            {
                for(int i = 0; i < batchSize; i++)
                {
                    Label.Idx[i] = i * Model.Category + (int)p[i];
                }
            }

            Label.Length = batchSize;
            Label.Idx.SyncFromCPU();

            StepV2();
            
            if(o != IntPtr.Zero)
            {
                Cudalib.CudaCopyOutFloat(O.Output.Data.CudaPtr, o, batchSize * Model.Category);
            }

            return (float)StepLoss;
        }


        // call complete after training on every epoch.
        public void SaveModel(int epoch)
        {
            using(BinaryWriter modelWriter = new BinaryWriter(new FileStream("storm_vgg11." + epoch.ToString(), FileMode.Create, FileAccess.Write)))
            {
                Model.Serialize(modelWriter);
            }
        }

    }
}
