using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;

using BigLearn;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.Image
{
    /// <summary>
    /// Data Type mush Support IData.
    /// </summary>
    /// <typeparam name="DataType"></typeparam>
    /// <typeparam name="MetaType"></typeparam>
    public class ImageNetDataCashier : IDataCashier<Tensor4DData> //where DataType : Tensor4DData
    {
        public IntArgument[] Shape { get; private set; }

        List<Tensor4DData> L1Caches = new List<Tensor4DData>();

        Stream IStream { get { return Reader.BaseStream; } }
        BinaryReader Reader = null;

        long[] SeekPosition = null;

        public string Name { get; set; }

        int BatchSize { get; set; }
        public int TotalSampleNum { get; private set; }
        public int TotalBatchNum { get; private set; }
        public int SizePerImg { get; private set; }

        IEnumerator<Tensor4DData> DataFlow { get; set; }

        object DataFlowLock = new object();
        Random DataRandom;

        bool IsReseted = false;
        
        public DataRandomShuffling DataShuffle = null;

        public ImageNetDataCashier(BinaryReader binReader, int totalSampleNum, int batchSize, int depth, int height, int width, int randomSeed,
                                                           int l1Cache, string name = "")
        {
            Reader = binReader;
            Name = name;

            DataRandom = new Random(randomSeed);

            TotalSampleNum = totalSampleNum;
            BatchSize = batchSize;
            TotalBatchNum = totalSampleNum / batchSize;

            Shape = new IntArgument[4];
            Shape[0] = new IntArgument("WIDTH", width);
            Shape[1] = new IntArgument("HEIGHT", height);
            Shape[2] = new IntArgument("DEPTH", depth);
            Shape[3] = new IntArgument("MAX_BATCHSIZE", batchSize);

            for (int i = 0; i < l1Cache; i++)
            {
                L1Caches.Add(new Tensor4DData(DeviceType.CPU, false, Shape));
            }

            SeekPosition = new long[TotalSampleNum];
            SizePerImg = (width * height * depth * sizeof(float));
            long offset = 0;
            for(int idx = 0; idx < TotalSampleNum; idx ++)
            {
                SeekPosition[idx] = offset;
                offset += SizePerImg;
            }

            DataShuffle = new DataRandomShuffling(TotalSampleNum, DataRandom);
        }

        public IEnumerable<Tensor4DData> GetInstances(Random randomizer)
        {
            {
                BlockingCollection<int> dataBuffer = new BlockingCollection<int>(L1Caches.Count - 2);
                var disk2CPUParallel = Task.Run(() =>
                {
                    {
                        //int[] RandomBatchIdx = Enumerable.Range(0, TotalSampleNum).ToArray();
                        int batIdx = 0;
                        int cursorIdx = 0;

                        while (batIdx < TotalBatchNum)
                        {
                            for(int b = 0; b < BatchSize; b++)
                            {
                                //int randomPos = randomizer.Next(cursorIdx, TotalSampleNum);
                                //int id = RandomBatchIdx[randomPos];
                                //RandomBatchIdx[randomPos] = RandomBatchIdx[cursorIdx];
                                //RandomBatchIdx[cursorIdx] = id;

                                int id = DataShuffle.RandomIdxs[cursorIdx];
                                cursorIdx += 1;
                                IStream.Position = SeekPosition[id];
                                
                                Buffer.BlockCopy(Reader.ReadBytes(SizePerImg), 0, 
                                                 L1Caches[batIdx % L1Caches.Count].Output.MemPtr, b * SizePerImg, SizePerImg);
                            }

                            //.Deserialize(Reader);
                            dataBuffer.Add(batIdx % L1Caches.Count);
                            batIdx += 1;
                        }
                    }
                    
                    // Let consumer know we are done.
                    dataBuffer.CompleteAdding();
                });

                {
                    while (!dataBuffer.IsCompleted)
                    {
                        int cpuId = -1;
                        try
                        {
                            cpuId = dataBuffer.Take();
                        }
                        catch (InvalidOperationException)
                        { }

                        if (cpuId != -1)
                        {
                            yield return L1Caches[cpuId];
                        }
                    }
                }
            }
        }
        

        /// <summary>
        /// Only need to start once. 
        /// </summary>
        public void ResetDataFlow()
        {
            if (!IsReseted)
            {
                IsReseted = true;
                DataShuffle.Shuffling();
                DataFlow = GetInstances(DataRandom).GetEnumerator();
            }
        }

        /// <summary>
        /// ThreadSafe for Fetching Training Data.
        /// </summary>
        /// <param name="output"> DataType must have IData Interface.</param>
        /// <returns></returns>
        public bool FetchDataThreadSafe(Tensor4DData output)
        {
            lock (DataFlowLock)
            {
                if (DataFlow.MoveNext())
                {
                    output.CopyFrom(DataFlow.Current);
                    return true;
                }
                else
                {
                    //output.Clear();
                    IsReseted = false;
                    return false;
                }
            }
        }

        public void CloseDataFlow()
        {
            Reader.Close();
            IStream.Close();
        }
        
    }
}
