using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.NL2SQL
{
        public class WikiSQLDataPanel 
        {
            public static List<WikiSQLData> TrainData { get; set; }
            public static List<WikiSQLData> TestData {get; set; }
            public static List<WikiSQLData> DevData { get; set; }

            public static List<WikiSQLData> LoadJsonFile(string fileName)
            {
                List<WikiSQLData> dataset = new List<WikiSQLData>();
                Logger.WriteLog("Load file {0}", fileName);
                foreach(string mline in File.ReadAllLines(fileName))
                {
                    WikiSQLData data = JsonConvert.DeserializeObject<WikiSQLData>(mline);
                    dataset.Add(data);
                }
                //TestData = JsonConvert.DeserializeObject<WikiSql>("{ \"data\" : [" + File.ReadAllText(dataDir + "test2.jsonl") + "] }").data;
                Logger.WriteLog("Load file  line {0}", dataset.Count);
                return dataset;

            }
            public static void Init(string dataDir)
            {
                TrainData = LoadJsonFile(dataDir + "train.jsonl");
                TestData = LoadJsonFile(dataDir + "test.jsonl");
                DevData = LoadJsonFile(dataDir + "dev.jsonl");
                
            }

        }

        
}
