using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.UnitTest
{
    public class AllReduceTest
    {
        int DeviceNum { get { return DeviceIds.Length; } }
        int[] DeviceIds { get; set; }

        IntPtr Comm { get; set; }
        CudaPieceFloat[] Data { get; set; }

        int size = 67609718;
        public unsafe AllReduceTest(int deviceNum)
        {
            DeviceIds = new int[deviceNum];
            for(int i = 0; i < deviceNum; i++) DeviceIds[i] = i;

            fixed (int * pDevice = & DeviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, deviceNum);
            }
            Data = new CudaPieceFloat[deviceNum];

            // int subBatchSize = batchSize / DeviceNum;

            int idx = 0;
            Parallel.For(0, DeviceNum, i => 
            {
                int devId = DeviceIds[i];

                Logger.WriteLog("Create Device {0}", devId);
                DeviceType device = MathOperatorManager.SetDevice(devId);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

                //StructureLearner learner, RunnerBehavior behavior)
                // initoptimizer;

                // InitOptimizer(StructureLearner learner, RunnerBehavior behavior) 

                Data[i] = new CudaPieceFloat(size, device);
                for(int t = 0; t < Data[i].Size; t++)
                {
                    Data[i][t] = (i + 1) * 0.2f;
                }
                Data[i].SyncFromCPU();

                //idx += 1;
            });
        }

        public unsafe void Run()
        {
            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Cudalib.AllReduce(Data[i].CudaPtr, Data[i].Size, i, Comm);
            });

            for(int i = 0; i < DeviceNum; i++)
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Data[i].SyncToCPU();
                Data[i].Print("device "+ i.ToString(), 10, false);
            }
        }

        public unsafe void RunBcast()
        {
            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Cudalib.Bcast(Data[i].CudaPtr, Data[i].Size, 0, i, Comm); // IntPtr data, int size, int root, int idx, IntPtr commState);
                //Cudalib.AllReduce(Data[i].CudaPtr, Data[i].Size, i, Comm);
            });

            for(int i = 0; i < DeviceNum; i++)
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Data[i].SyncToCPU();
                Data[i].Print("device "+ i.ToString(), 10, false);
            }
        }

    }

}
