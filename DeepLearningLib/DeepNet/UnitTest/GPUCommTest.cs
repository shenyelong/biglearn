using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.UnitTest
{
    public class GPUCommTest
    {
        public static CudaPieceFloat RunAssginCuda(int length, IntPtr data, float v, int deviceId)
        {
            //int length, IntPtr gPointer, DeviceType device)
            DeviceType device = MathOperatorManager.SetDevice(deviceId);
            CudaPieceFloat d = new CudaPieceFloat(length,  data, device);
            for(int i = 0; i < length; i++) d[i] = v;
            d.SyncFromCPU();
            return d;
        }

        public static void Print(CudaPieceFloat data)
        {
            data.Print("data", 100);
        }

    }

}
