﻿using BigLearn;
using System;
using System.Linq;
using BigLearn.Common;

namespace BigLearn.DeepNet
{
    public class DNNAppArgs : CmdArgumentsBase
    {
        #region command line arguments
        [Argument("Path of the server list file.")]
        public string Servers { get; set; }

        [Argument("The arguements of the application.")]
        public string AppArgs { get; set; }

        [Argument("Maxium Degree of Parallesim of managed thread pool.")]
        public int? MaxDop { get; set; }

        [Argument("-f", "Load arguemnts from configure file.")]
        public string ConfigureFile { get; set; }

        [Argument("-v|-verbose", "Run program in verbose mode.", IsPair = false)]
        public bool Verbose { get; set; }

        [Argument("-chk|-checkpoint", "The path of check point file.")]
        public string CheckPoint { get; set; }

        [Argument("-w|-workerid", "The id of the worker.", Default=null)]
        public int? WorkerId { get; set; }
        #endregion

        public string LegacyConfigureFile
        {
            get
            {
                if (this.UnrecognizedArgs != null)
                {
                    var conf = this.UnrecognizedArgs.Where(p => !p.Value.StartsWith("-")).FirstOrDefault();
                    return conf.Value;
                }

                return null;
            }
        }
    }    
}
