using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    /// <summary>
    /// Parameters for Optimizer.
    /// </summary>
    public class OptimizerParameters : BaseModelArgument<OptimizerParameters>
    {
        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

        /// <summary>
        /// AutoAdjust Learning Rate.
        /// </summary>
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }

        /// <summary>
        /// Initial Learning Rate.
        /// </summary>
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }

        /// <summary>
        /// Training Iterations.
        /// </summary>
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }

        /// <summary>
        /// Optimization Method.
        /// </summary>
        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

        public static string OptimizerArgs { get { return Argument["OPTIMIZER-ARGS"].Value; } }

        public static IGradientAggregatorFactory GradientAgg { get; set; }

        public static List<Tuple<int, float>> ScheduleLR
        {
            get
            {
                if (Argument["SCHEDULE-LR"].Value.Equals(string.Empty)) return new List<Tuple<int, float>>();
                return Argument["SCHEDULE-LR"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
            }
            set
            {
                string mv = string.Join(",", value.Select(i => string.Format("{0}:{1}", i.Item1, i.Item2)));

                if (Argument.ContainsKey("SCHEDULE-LR"))
                    Argument["SCHEDULE-LR"].Value = mv;
                else
                    Argument.Add("SCHEDULE-LR", new ParameterArgument(mv, ""));

            }
        }

        public static int ScheduleLRTyp { get { return int.Parse(Argument["SCHEDULE-LR-TYPE"].Value); } }


        static StructureLearner mStructureOptimizer = null;
        /// <summary>
        /// call OptimizerParameters.Parse(config) before using StructureOptimizer;
        /// </summary>
        public static StructureLearner StructureOptimizer
        {
            get
            {
                if (mStructureOptimizer == null)
                {
                    mStructureOptimizer = new StructureLearner()
                    {
                        LearnRate = LearnRate,
                        Optimizer = Optimizer,
                        ShrinkLR = ShrinkLR,
                        EpochNum = Iteration,
                        
                        ClipDelta = float.Parse(Argument["DELTA-CLIP"].Value),
                        ClipWeight = float.Parse(Argument["WEIGHT-CLIP"].Value),
                        GradNormClip = float.Parse(Argument["GRAD-NORM-CLIP"].Value),
                        EMA_Decay = float.Parse(Argument["EMA-DECAY"].Value),

                        AdaDelta_Rho = float.Parse(Argument["ADADELTA-RHO"].Value),
                        AdaDelta_Epsilon = float.Parse(Argument["ADADELTA-EPSILON"].Value),

                        Adam_Beta1 = float.Parse(Argument["ADAM-BETA1"].Value),
                        Adam_Beta2 = float.Parse(Argument["ADAM-BETA2"].Value),
                        Adam_Epsilon = float.Parse(Argument["ADAM-EPSILON"].Value),

                        RmsProp_Decay = float.Parse(Argument["RMSPROP-DECAY"].Value),
                        RmsProp_Epsilon = float.Parse(Argument["RMSPROP-EPSILON"].Value),

                        OptArgs = OptimizerArgs,

                        WeightDecay = float.Parse(Argument["WEIGHT-DECAY"].Value),
                        MomemtumRate = float.Parse(Argument["MOMEMTUM"].Value),

                        ScheduleLR = ScheduleLR,
                        ScheduleLRTyp = ScheduleLRTyp,

                        device = MathOperatorManager.Device(GPUID),
                    };
                }
                return mStructureOptimizer;
            }
        }

        static OptimizerParameters()
        {
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));

            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
            
            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
            Argument.Add("ITERATION", new ParameterArgument("200", "Train Iteration."));
            
            Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
            Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

            Argument.Add("GRAD-NORM-CLIP", new ParameterArgument("0", "gradient norm clip."));
            
            Argument.Add("MOMEMTUM", new ParameterArgument("0", "momemtum rate."));
            Argument.Add("WEIGHT-DECAY", new ParameterArgument("0.01", "weight decay."));
            
            Argument.Add("EMA-DECAY", new ParameterArgument("1.0", "Expontenial Moving Average Decay."));

            Argument.Add("ADADELTA-RHO", new ParameterArgument("0.9995", "AdaDelta-Rho"));
            Argument.Add("ADADELTA-EPSILON", new ParameterArgument("1.0e-6", "AdaDelta-Epsilon"));

            Argument.Add("ADAM-BETA1", new ParameterArgument("0.95", "AdaDelta-Epsilon"));
            Argument.Add("ADAM-BETA2", new ParameterArgument("0.999", "AdaDelta-Rho"));
            Argument.Add("ADAM-EPSILON", new ParameterArgument("1.0e-8", "AdaDelta-Epsilon"));

            Argument.Add("RMSPROP-DECAY", new ParameterArgument("0.95", "RmsProp-Decay"));
            Argument.Add("RMSPROP-EPSILON", new ParameterArgument("1.0e-6", "RmsProp-Epsilon"));

            Argument.Add("OPTIMIZER-ARGS", new ParameterArgument("", "The optimizer arguments."));

            Argument.Add("SCHEDULE-LR", new ParameterArgument(string.Empty, "Learning Rate Schedule."));
            Argument.Add("SCHEDULE-LR-TYPE", new ParameterArgument("0", "0:LearningRateLRJump; 1:LearningRateLRSmooth"));

        }
    }
}
