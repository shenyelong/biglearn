using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class BuilderParameters : BaseModelArgument<BuilderParameters>
    {
        public static BuilderType Type { get { return (BuilderType)Enum.Parse(typeof(BuilderType), Argument["BUILDER"].Value.ToUpper()); } }

        public static string EvalToolPath { get { return (Argument["EVAL-TOOL-PATH"].Value); } }

        public static int ModelSaveIteration { get { return int.Parse(Argument["MODEL-PER-ITERATION"].Value); } }

        public static bool ModelSavePerIteration { get { return int.Parse(Argument["MODEL-PER-ITERATION"].Value) > 0; } }

        public static int RandomSeed { get { return int.Parse(Argument["RANDOM-SEED"].Value); } }

        public static int ModelUpdatePerSave { get { return int.Parse(Argument["MODEL-PER-UPDATE"].Value); } }

        public static bool ParameterDebug { get { return int.Parse(Argument["PARAMETER-DEBUG"].Value) > 0; } }
        
        public static bool Verbose { get { return int.Parse(Argument["VB"].Value) > 0; } }
        public static int? gpuid;
        public static int GPUID
        {
            get
            {
                if (!gpuid.HasValue)
                {
                    gpuid = int.Parse(Argument["GPUID"].Value);
                }

                return gpuid.Value;
            }
            set
            {
                gpuid = value;
            }
        }

        public static int? WorkerId { get { return string.IsNullOrEmpty(Argument["WORKER-ID"].Value) ? (int?)null : int.Parse(Argument["WORKER-ID"].Value); } }

        public static string CheckPoint { get { return Argument["CHECK-POINT"].Value; } }

        public static PlatformType DistMode { get { return (PlatformType)Enum.Parse(typeof(PlatformType), Argument["DIST-MODE"].Value, true); } }

        public static int IoThreadNum { get { return int.Parse(Argument["IO-THREAD-NUM"].Value); } }

        public static bool IsLogFile { get { return int.Parse(Argument["IS-LOG-FILE"].Value) > 0; } }

        public static DeviceType Device
        {
            get
            {
                int gpuid = int.Parse(Argument["GPUID"].Value);
                if (gpuid >= 0)
                    return DeviceType.GPU;
                if (gpuid == -1)
                    return DeviceType.CPU;
                else
                    return DeviceType.CPU_FAST_VECTOR;
            }
        }

        // static Random mRandomer = null;
        // public static Random Randomer
        // {
        //     get
        //     {
        //         if(mRandomer == null) mRandomer = new Random(RandomSeed);
        //         return mRandomer;
        //     }
        // }

        static BuilderParameters()
        {
            Argument.Add("BUILDER", new ParameterArgument((BuilderType.JDSSM).ToString(), ParameterUtil.EnumValues(typeof(BuilderType))));
            Argument.Add("EVAL-TOOL-PATH", new ParameterArgument(@"\\deep-05\EvaluationToolkit\", "Evaluation Tool Folder"));
            Argument.Add("MODEL-PER-ITERATION", new ParameterArgument("1", "0 : Don't save model per iteration; 1 : Save Model Per iteration"));
            Argument.Add("MODEL-PER-UPDATE", new ParameterArgument("0", "0 : Don't save model per update; Save Model Per update"));
            Argument.Add("IS-LOG-FILE", new ParameterArgument("1", "0 : No Log File; 1 : Log File;"));
            Argument.Add("RANDOM-SEED", new ParameterArgument("13", "Model Random Seed."));
            Argument.Add("PARAMETER-DEBUG", new ParameterArgument("0", "Is Inspect model Parameter?"));

            Argument.Add("DIST-MODE", new ParameterArgument(@"0", "Distribute mode"));
            Argument.Add("IO-THREAD-NUM", new ParameterArgument(@"2", "AllReduce IO thread number"));
            Argument.Add("WORKER-ID", new ParameterArgument("", "The id of this worker."));
            Argument.Add("CHECK-POINT", new ParameterArgument("", "The check point path."));
            Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
            Argument.Add("VB", new ParameterArgument("0", "verbose profiling."));
        }

    }
}
