using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        /// <summary>
        /// episodic memory structure. it is a global structure.
        /// </summary>
        public class EpisodicMemory 
        {
            //decaying memory for visiting and success rate.
            Dictionary<string, Tuple<float[], float[]>> Mem = null;
            Dictionary<string, int> MemTimer = null; //new Dictionary<string, int>();

            int ReplayBufferSize { get; set; } //= BuilderParameters.ReplayBufferSize; //  1000000;
            float ReplayDecay { get; set; } //= BuilderParameters.ReplayDecay; // 0.9999f;
            int MiniReplaySize { get; set; } // = BuilderParameters.TReplaySize;
            float RewardDiscount { get; set; }

            int ReplayCursor = 0;
            int ReplayLength = 0;
            int Timer { get; set; }
            
            float[] ReplayRewards = null;
            int[] ReplayIdx = null;
            int[] ReplayTgt = null;
            int[] ReplaySrc = null;
            int[] ReplayRel = null;
            //Tuple<int, int, int, int>[][] ReplayPath = null;
            //Tuple<int[], int[]>[] ReplaySimplePath = null;
            int[][] ReplaySimplePath = null;
            public bool IsReplayBufferReady { get { if(ReplayLength < MiniReplaySize) return false; return true; } }
            
            public Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 998);

            public EpisodicMemory(bool isReplay, int replayBufSize, float replayDecay, float rewardDiscount, int replayThreshold) 
                : this(isReplay, true, replayBufSize, replayDecay, rewardDiscount, replayThreshold)
            { }

            public EpisodicMemory(bool isReplay, bool isDict, int replayBufSize, float replayDecay, float rewardDiscount, int replayThreshold)
            {
                ReplayBufferSize = replayBufSize;
                ReplayDecay = replayDecay;
                RewardDiscount = rewardDiscount;
                MiniReplaySize = replayThreshold;
                
                if(isDict)
                {
                    Mem = new Dictionary<string, Tuple<float[], float[]>>( ReplayBufferSize );
                    MemTimer = new Dictionary<string, int> ( ReplayBufferSize );
                }

                if(isReplay)
                {
                    ReplayRewards = new float[ReplayBufferSize];
                    ReplayIdx = new int[ReplayBufferSize];
                    ReplayTgt = new int[ReplayBufferSize];
                    ReplaySrc = new int[ReplayBufferSize];
                    ReplayRel = new int[ReplayBufferSize];
                    //ReplayPath = new Tuple<int, int, int, int>[ReplayBufferSize][];
                    //ReplaySimplePath = new Tuple<int[], int[]>[ReplayBufferSize];
                    ReplaySimplePath = new int[ReplayBufferSize][];
                }

                Timer = 0;
                ReplayCursor = 0;
                ReplayLength = 0;
            }

            public void Clear()
            {
                if(Mem != null) Mem.Clear();
                if(MemTimer != null) MemTimer.Clear();
            }

            /// <summary>
            /// memory index.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="actionCount"></param>
            /// <param name="topK"></param>
            /// <returns></returns>
            public Tuple<float[], float[], float> Search(string key)
            {
                if (Mem.ContainsKey(key))
                {
                    float decay = (float)Math.Pow(ReplayDecay, Timer - MemTimer[key]);
                    return new Tuple<float[], float[], float>(Mem[key].Item1, Mem[key].Item2, decay);
                }
                else
                {
                    return null;
                }
            }

            // public void AddReplayBuff(int dIdx, int srcId, int relId, int tgtId, Tuple<int, int, int, int>[] path, float reward)
            // {
            //     ReplaySrc[ReplayCursor % ReplayBufferSize] = srcId;
            //     ReplayRel[ReplayCursor % ReplayBufferSize] = relId;
            //     ReplayPath[ReplayCursor % ReplayBufferSize] = path;

            //     ReplayIdx[ReplayCursor % ReplayBufferSize] = dIdx;
            //     ReplayRewards[ReplayCursor % ReplayBufferSize] = reward;
            //     ReplayTgt[ReplayCursor % ReplayBufferSize] = tgtId;

            //     ReplayCursor += 1;
            //     ReplayLength += 1;
            // }

            public void AddReplayBuff(int dIdx, int srcId, int relId, int tgtId, int[] path, float reward)
            {
                ReplaySrc[ReplayCursor % ReplayBufferSize] = srcId;
                ReplayRel[ReplayCursor % ReplayBufferSize] = relId;
                ReplaySimplePath[ReplayCursor % ReplayBufferSize] = path;

                ReplayIdx[ReplayCursor % ReplayBufferSize] = dIdx;
                ReplayRewards[ReplayCursor % ReplayBufferSize] = reward;
                ReplayTgt[ReplayCursor % ReplayBufferSize] = tgtId;

                ReplayCursor += 1;
                ReplayLength += 1;
            }


            // public void AddReplayBuff(int dIdx, int srcId, int relId, int tgtId, int pos, Tuple<int, int, int, int>[] path, float reward)
            // {
            //     ReplaySrc[(ReplayCursor + pos) % ReplayBufferSize] = srcId;
            //     ReplayRel[(ReplayCursor + pos) % ReplayBufferSize] = relId;
            //     ReplayPath[(ReplayCursor + pos) % ReplayBufferSize] = path;

            //     ReplayIdx[(ReplayCursor + pos) % ReplayBufferSize] = dIdx;
            //     ReplayRewards[(ReplayCursor + pos) % ReplayBufferSize] = reward;
            //     ReplayTgt[(ReplayCursor + pos) % ReplayBufferSize] = tgtId;
            // }
            public void MoveCursor(int len)
            {
                ReplayCursor += len;
                ReplayLength += len;
            }

            // public Tuple<int, int, int, int, Tuple<int, int, int, int>[], float> SampleReplayData()
            // {
            //     int idx = random.Next(ReplayLength < ReplayBufferSize ? ReplayLength : ReplayBufferSize);
            //     return new Tuple<int, int, int, int, Tuple<int, int, int, int>[], float>(
            //             ReplayIdx[idx], 
            //             ReplaySrc[idx], 
            //             ReplayRel[idx], 
            //             ReplayTgt[idx],
            //             ReplayPath[idx],
            //             ReplayRewards[idx]
            //         );
            // }

            public Tuple<int, int, int, int, int[], float> SampleReplayData()
            {
                int idx = random.Next(ReplayLength < ReplayBufferSize ? ReplayLength : ReplayBufferSize);
                return new Tuple<int, int, int, int, int[], float>(
                        ReplayIdx[idx], 
                        ReplaySrc[idx], 
                        ReplayRel[idx], 
                        ReplayTgt[idx],
                        ReplaySimplePath[idx],
                        ReplayRewards[idx]
                    );
            }


            public void UpdateMem(int rawIdx, int srcId, int relId, Tuple<int, int, int, int>[] path, float reward)
            {
                string mkey = string.Format("Idx:{0}--N:{1}?R:{2}", rawIdx, srcId, relId);

                for(int i = 0; i < path.Length; i++)
                {
                    float r = reward * (float)Math.Pow(RewardDiscount, path.Length - 1 - i);
                    Update(mkey, path[i].Item1, path[i].Item2, r);
                    mkey = string.Format("{0}-R:{1}->N:{2}", mkey, path[i].Item4, path[i].Item3);
                }
            }


            public unsafe void Update(string key, int actid, int totalAct, float reward, float v = 1.0f)
            {
                Tuple<float[], float[]> arms = null;

                try
                {
                    float new_r = 0;
                    float new_v = 0;
                    if(actid >= totalAct)
                    {
                        throw new Exception(string.Format("action idx {0} doesnt exceed total action {1}", actid, totalAct ));
                    }
                    // update memory.
                    if (Mem.ContainsKey(key))
                    {
                        //Logger.WriteLog("step1");
                        int memTime = MemTimer[key];
                        float decay = (float)Math.Pow(ReplayDecay, Timer - memTime);

                        //Logger.WriteLog("step2");
                        arms = Mem[key];

                        if(arms.Item1.Length != totalAct || arms.Item2.Length != totalAct)
                        {
                            throw new Exception(string.Format("arms number doesnt match {0}, {1}", arms.Item1.Length, totalAct ));
                        }

                    
                        fixed(float * pr = &arms.Item1[0])
                        fixed(float * pv = &arms.Item2[0])
                        {
                            SSElib.SSE_Scale(decay, pr, totalAct);
                            SSElib.SSE_Scale(decay, pv, totalAct);
                        }
                    
                        float or = arms.Item1[actid];
                        float ov = arms.Item2[actid];

                        //Logger.WriteLog("step3");
                        if(actid >= arms.Item1.Length || actid >= arms.Item2.Length)
                        {
                            throw new Exception(string.Format("action idx {0} doesnt exceed total action {1}, {2}", actid, arms.Item1.Length, arms.Item2.Length ));
                        }

                        new_r = or + reward; // c / (c + v) * u + v / (c + v) * reward;
                        new_v = ov + v;
                    
                    }
                    else
                    {
                        //Logger.WriteLog("new key {0}, actid {1}. totalAct {2}");
                        arms = new Tuple<float[], float[]>(new float[totalAct], new float[totalAct]);
                        Mem.Add(key, arms);
                        MemTimer.Add(key, Timer);
                        new_r = reward;
                        new_v = v;

                        if(Mem.Count % 1000000 == 0)
                        {
                            Logger.WriteLog("MCTS Memory Size over {0}", Mem.Count);
                        }
                    }
                    arms.Item1[actid] = new_r;
                    arms.Item2[actid] = new_v;
                    MemTimer[key] = Timer;
                }
                catch(Exception e)
                {
                    Logger.WriteLog("actid {0}, visit len {1}, q len {2}", actid, arms.Item1.Length, arms.Item2.Length);
                    throw new Exception(e.ToString());
                }
            }

            public void UpdateTiming()
            {
                Timer += 1;
            }
        }

}