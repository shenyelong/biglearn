using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
    public class MCTSNode : IDisposable
    {
        public float[] arm_q = null;
        public float[] arm_v = null;
        public MCTSNode[] Children = null;
        public int arm_N { get; set; }
        public float V { get; set; }
        public float Q { get; set; }
        public MCTSNode(int arm_num)
        {
            arm_N = arm_num;
            arm_q = new float[arm_num];
            arm_v = new float[arm_num];
            Children = new MCTSNode[arm_num];
            V = 1;
            Q = 0;
        }

        public void Dispose()
        {
            for(int i= 0; i < arm_N; i++)
            {
                if(Children[i] != null) Children[i].Dispose();
                Children[i] = null;
            }
            arm_q = null;
            arm_v = null;
        }

        public MCTSNode Search(int[] paths, int depth)
        {
            if(depth == paths.Length)
            {
                return this;
            }
            int cidx = paths[depth];
            int arm_num = paths[depth + 1];
            if(cidx >= arm_N) { throw new Exception(string.Format("access children node {0} with arm number {1}", cidx, arm_N)); }
            if(arm_N != arm_num) { throw new Exception(string.Format("the arm number doesnt match {0}, {1}", arm_N, arm_num)); }
            else if(Children[cidx] != null)
            {
                return Children[cidx].Search(paths, depth + 2);
            }
            else return null;
        }

        public void Expand(int[] paths, int depth, float q, float v)
        {
            V = V + v;
            Q = Q + q;
            int cidx = paths[depth];
            int arm_num = paths[depth + 1];

            if(cidx >= arm_N) { throw new Exception(string.Format("access children node {0} with arm number {1}", cidx, arm_N)); }
            if(arm_N != arm_num) { throw new Exception(string.Format("the arm number doesnt match {0}, {1}", arm_N, arm_num)); }
            
            arm_q[cidx] = arm_q[cidx] + q;
            arm_v[cidx] = arm_v[cidx] + v;

            if(paths.Length > depth + 3)
            {
                int c_arm_num = paths[depth + 3];
                //continue;
                if(Children[cidx] == null) Children[cidx] = new MCTSNode(c_arm_num);
                Children[cidx].Expand(paths, depth + 2, q, v);
            }

        }
    }            
}