using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class GraphQueryData : BatchData
        {
            public List<int> RawSource = new List<int>();
            public List<int> RawRel = new List<int>();
            public List<int> RawTarget = new List<int>();
            public List<int> RawIndex = new List<int>();
            public List<int> RawStart = new List<int>();

            public int RawIdx = 0;
            public CudaPieceInt RawSourceInts { get; set; }
            public CudaPieceInt RawRelInts { get; set; }
            public CudaPieceInt RawTargetInts { get; set; }
            public CudaPieceInt RawInvRelInts { get; set; }

             // the edge already exists in KB.
            public List<List<int>> BlackTargets = new List<List<int>>();
            
            /// suppose this is a good implementation.
            /// public SparseVectorData BlackLabels = null;

            public List<int[]> GuidPath = null;
            public List<float> GuidReward = null;

            public List<StatusData> StatusPath = new List<StatusData>();

            public int BatchSize { get { return RawSource.Count; } }
            public int MaxBatchSize { get; set; }
            public int Roll = 1;
            
            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public GraphQueryData(int maxBatchSize, DeviceType device) : base(device, new IntArgument("MAX_BATCHSIZE", maxBatchSize))
            {
                MaxBatchSize = maxBatchSize;

                RawSourceInts = new CudaPieceInt(maxBatchSize, device);
                RawRelInts = new CudaPieceInt(maxBatchSize, device);
                RawTargetInts = new CudaPieceInt(maxBatchSize, device);
                RawInvRelInts = new CudaPieceInt(maxBatchSize, device);
            }

            public List<int> GetBatchIdxs(int b, int step)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < StatusPath[step].BatchSize; i++)
                {
                    if (StatusPath[step].GetOriginalStatsIndex(i) == b)
                        r.Add(i);
                }
                return r;
            }

            public void Clear()
            {
                RawSource.Clear();
                RawRel.Clear();
                RawTarget.Clear();
                RawIndex.Clear();
                RawStart.Clear();
                BlackTargets.Clear();

                if(GuidPath != null) GuidPath.Clear();
                if(GuidReward != null) GuidReward.Clear();

                RawIdx = 0;
            }

            public void Push(int idx, int srcId, int linkId, int tgtId, List<int> blacks, int start)
            {
                Push(idx, srcId, linkId, -1, tgtId, blacks, start);
             }

            public void Push(int idx, int srcId, int linkId, int invLinkIdx, int tgtId, List<int> blacks, int start)
            {
                RawIndex.Add(idx);
                RawSource.Add(srcId);
                RawRel.Add(linkId);
                RawTarget.Add(tgtId);
                BlackTargets.Add(blacks);
                RawStart.Add(start);

                RawSourceInts.MemPtr[RawIdx] = srcId;
                RawRelInts.MemPtr[RawIdx] = linkId;
                RawTargetInts.MemPtr[RawIdx] = tgtId;
                RawInvRelInts.MemPtr[RawIdx] = invLinkIdx;

                RawIdx += 1;
             }

             // please dont forget to call this.
             public void PushDone()
             {
                RawSourceInts.EffectiveSize = RawIdx;
                RawSourceInts.SyncFromCPU();

                RawRelInts.EffectiveSize = RawIdx;
                RawRelInts.SyncFromCPU();

                RawTargetInts.EffectiveSize = RawIdx;
                RawTargetInts.SyncFromCPU();

                RawInvRelInts.EffectiveSize = RawIdx;
                RawInvRelInts.SyncFromCPU();
             }
        }
}
