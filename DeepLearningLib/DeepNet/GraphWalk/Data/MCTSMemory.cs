using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
    public class MCTSMemory
    {
        Dictionary<int, int> mctsDicts = null;
        public Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 998);
        List<MCTSNode> mctsRoots = null;

        public MCTSMemory()
        { 
            mctsRoots = new List<MCTSNode>(); // Dictionary<int, MCTSNode>(); 
            //DistCount = distCount;
            mctsDicts = new Dictionary<int, int>();
        }

        public Tuple<float[], float[], float> Search(int idx, int[] paths)
        {
            var perf_mctsSearch = PerfCounter.Manager.Instance["mctsSearch"].Begin();

            Tuple<float[], float[], float> r = null;
            if(mctsDicts.ContainsKey(idx))
            {
                int id_pos = mctsDicts[idx];
                MCTSNode node = mctsRoots[id_pos].Search(paths, 0);
                if(node != null) 
                {
                    //float decay = (float)Math.Pow(Decay, Timer - node.Time);
                    r = new Tuple<float[], float[], float>(node.arm_q, node.arm_v, 1);
                }
            }
            PerfCounter.Manager.Instance["mctsSearch"].TakeCount(perf_mctsSearch);
            return r;
        }

        public Tuple<float[], float[], float> Search(int idx, int[] paths, int thread_idx)
        {
            var perf_mctsSearch = PerfCounter.Manager.Instance["mctsSearch_"+thread_idx.ToString()].Begin();

            Tuple<float[], float[], float> r = null;
            if(mctsDicts.ContainsKey(idx))
            {
                int id_pos = mctsDicts[idx];
                MCTSNode node = mctsRoots[id_pos].Search(paths, 0);
                if(node != null) 
                {
                    //float decay = (float)Math.Pow(Decay, Timer - node.Time);
                    r = new Tuple<float[], float[], float>(node.arm_q, node.arm_v, 1);
                }
            }
            PerfCounter.Manager.Instance["mctsSearch_"+thread_idx.ToString()].TakeCount(perf_mctsSearch);
            return r;
        }

        public MCTSNode SearchMCTSNode(int idx, int[] paths, int thread_idx)
        {
            //var perf_mctsSearch = PerfCounter.Manager.Instance["mctsSearch_"+thread_idx.ToString()].Begin();

            MCTSNode r = null;
            if(mctsDicts.ContainsKey(idx))
            {
                int id_pos = mctsDicts[idx];
                MCTSNode node = mctsRoots[id_pos].Search(paths, 0);
                if(node != null) 
                {
                    //float decay = (float)Math.Pow(Decay, Timer - node.Time);
                    r = node;  //new Tuple<float[], float[], float, float>(node.arm_q, node.arm_v, node.Q, node.V);
                }
            }
            //PerfCounter.Manager.Instance["mctsSearch_"+thread_idx.ToString()].TakeCount(perf_mctsSearch);
            return r;
        }


        public void Clear()
        {
            foreach(MCTSNode node in mctsRoots)
            {
                node.Dispose();
            }
            mctsRoots.Clear();
            mctsDicts.Clear();
        }

        public void PreUpdateDicts(int idx)
        {
            if(! mctsDicts.ContainsKey(idx))
            {
                mctsDicts.Add(idx, mctsRoots.Count);
                mctsRoots.Add(null);
            }
        }

        public void PostUpdateMem(int idx, int[] paths, float q, float v)
        {
            int newIdx = mctsDicts[idx];
            if(mctsRoots[newIdx] == null)
            {
                mctsRoots[newIdx] = new MCTSNode(paths[1]);
            }
            mctsRoots[newIdx].Expand(paths, 0, q, v);
        }

        public void UpdateMem(int idx, int[] paths, float q, float v)
        {
            var perf_mctsUpdate = PerfCounter.Manager.Instance["mctsUpdate"].Begin();

            if(!mctsDicts.ContainsKey(idx))
            {
                mctsDicts.Add(idx, mctsRoots.Count);                
                mctsRoots.Add(new MCTSNode(paths[1]));
            }
            int newIdx = mctsDicts[idx];
            mctsRoots[newIdx].Expand(paths, 0, q, v);

            PerfCounter.Manager.Instance["mctsUpdate"].TakeCount(perf_mctsUpdate);
        }

    }
        

}