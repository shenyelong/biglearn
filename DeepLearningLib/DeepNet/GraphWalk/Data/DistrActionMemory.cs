using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        /// <summary>
        /// episodic memory structure. it is a global structure.
        /// </summary>
        public class DistrActionMemory 
        {
            //decaying memory for visiting and success rate.
            public Dictionary<string, Tuple<float[], float[]>>[] Mem = null;
            public Dictionary<string, int>[] MemTimer = null; //new Dictionary<string, int>();

            int Timer { get; set; }
            int DistCount { get; set; } 
            public Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 998);
            float Decay { get; set; }
            float RDiscount { get; set; }
            public DistrActionMemory(int distCount, float decay, float rDiscount, int initMemCount)
            { 
                Mem = new Dictionary<string, Tuple<float[], float[]>>[ distCount ];
                MemTimer = new Dictionary<string, int> [distCount];
                DistCount = distCount;
                Decay = decay;
                RDiscount = rDiscount;

                for(int i=0; i < distCount; i++)
                {
                    Mem[i] = new Dictionary<string, Tuple<float[], float[]>>(initMemCount);
                    MemTimer[i] = new Dictionary<string, int>(initMemCount);
                }
                Timer = 0; 
            }

            public void Clear()
            {
                for(int i=0; i < DistCount; i++)
                {
                    Mem[i].Clear();
                    MemTimer[i].Clear();
                }
            }

            /// <summary>
            /// memory index.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="actionCount"></param>
            /// <param name="topK"></param>
            /// <returns></returns>
            public Tuple<float[], float[], float> Search(int idx, string key)
            {
                Tuple<float[], float[]> arms = null;
                if (Mem[idx].ContainsKey(key)) //TryGetValue(key, out arms))
                {
                    arms = Mem[idx][key];
                    float decay = (float)Math.Pow(Decay, Timer - MemTimer[idx][key]);
                    return new Tuple<float[], float[], float>(arms.Item1, arms.Item2, decay);
                }
                else return null;
            }
  
            public void UpdateMem(int rawIdx, int srcId, int relId, Tuple<int, int, int, int>[] path, float reward)
            {
                //string mkey = "p:"; //string.Format("N:{0}?R:{1}", srcId, relId);

                for(int i = 0; i < path.Length; i++)
                {
                    string mkey = "p:"+string.Join("-", path.Take(i).Select(m => m.Item1));
                   //if(rawIdx == 26632)
                    //{
                    //    Logger.WriteLog("key {0}, i {1}, action idx {2}, total {3}", mkey, i, path[i].Item1, path[i].Item2);
                    //}
                    float r = reward * (float)Math.Pow(RDiscount, path.Length - 1 - i);
                    Update(rawIdx, mkey, path[i].Item1, path[i].Item2, r);
                    //mkey = string.Format("{0}-R:{1}->N:{2}", mkey, path[i].Item4, path[i].Item3);
                }
            }


            unsafe void Update(int idx, string key, int actid, int totalAct, float reward, float v = 1.0f)
            {
                Tuple<float[], float[]> arms = null;

                try
                {
                    float new_r = 0;
                    float new_v = 0;
                    if(actid >= totalAct || actid < 0)
                    {
                        throw new Exception(string.Format("smp Idx {0}, key {1}, action idx {2} doesnt exceed total action {3}", idx, key, actid, totalAct ));
                    }
                    // update memory.
                    if (Mem[idx].ContainsKey(key)) // out arms))
                    {
                        arms = Mem[idx][key];
                        //Logger.WriteLog("try modify exist entry in mem");
                        int memTime = MemTimer[idx][key];
                        float decay = (float)Math.Pow(Decay, Timer - memTime);

                        if(arms.Item1.Length != totalAct || arms.Item2.Length != totalAct)
                        {
                            throw new Exception(string.Format("arms number doesnt match {0}, {1}", arms.Item1.Length, totalAct ));
                        }
 
                        fixed(float * pr = &arms.Item1[0])
                        fixed(float * pv = &arms.Item2[0])
                        {
                            SSElib.SSE_Scale(decay, pr, totalAct);
                            SSElib.SSE_Scale(decay, pv, totalAct);
                        }
                    
                        float or = arms.Item1[actid];
                        float ov = arms.Item2[actid];

                        //Logger.WriteLog("step3");
                        if(actid >= arms.Item1.Length || actid >= arms.Item2.Length || actid < 0)
                        {
                            throw new Exception(string.Format("action idx {0} doesnt exceed total action {1}, {2}", actid, arms.Item1.Length, arms.Item2.Length ));
                        }

                        new_r = or + reward; // c / (c + v) * u + v / (c + v) * reward;
                        new_v = ov + v;
                        MemTimer[idx][key] = Timer;
                    }
                    else
                    {
                        //Logger.WriteLog("try add new entry in mem");
                        //Logger.WriteLog("new key {0}, actid {1}. totalAct {2}");
                        arms = new Tuple<float[], float[]>(new float[totalAct], new float[totalAct]);
                        
                        Mem[idx].Add(key, arms);
                        MemTimer[idx].Add(key, Timer);
                        new_r = reward;
                        new_v = v;
                        
                        //if(Mem.Count % 1000 == 0)
                        //{
                        //    Logger.WriteLog("MCTS Memory Size over {0}", Mem.Count);
                        //}
                    }
                    arms.Item1[actid] = new_r;
                    arms.Item2[actid] = new_v;
                    //Logger.WriteLog("success ");
                }
                catch(Exception e)
                {
                    if(arms == null) { Logger.WriteLog("arms is null ????");}
                    Logger.WriteLog("actid {0}, visit len {1}, q len {2}", actid, arms.Item1.Length, arms.Item2.Length);
                    throw new Exception(e.ToString());
                }
            }

            public void UpdateTiming()
            {
                Timer += 1;
            }
        }

}