using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class DataEnviroument 
        {
            public List<Tuple<int, int, int>> Triple { get; set; }
            // group of source and relation : <src, rel>  
            public List<Tuple<int, int>> Grouple { get; set; }
            
            public Dictionary<int, List<int>> TruthDict { get; set; }

            public Dictionary<int, List<int>> pTruthDict { get; set; }

            public GraphVocab GraphVoc { get; set; }
            public int GroupleNum { get { return Grouple.Count; } }
            public int TripleNum { get { return Triple.Count; } } 

            public Tuple<List<int>, List<float>>[] Results = null;
            public Dictionary<string, float> Eval = null;

            public void InitTripleResults()
            {
                Results = new Tuple<List<int>, List<float>>[TripleNum];
                for(int i = 0;  i < TripleNum; i++)
                {
                    Results[i] = new Tuple<List<int>, List<float>>(new List<int>(), new List<float>());
                }
            }

            public void ClearResults()
            {
                for(int i = 0;  i < TripleNum; i++)
                {
                    Results[i].Item1.Clear();
                    Results[i].Item2.Clear();
                }
            }

            public void PushResults(int idx, int predId, float score)
            {
                Results[idx].Item1.Add(predId);
                Results[idx].Item2.Add(score);
            }

            public Dictionary<string, float> HitK(bool ascending)
            {
                Dictionary<string, float> report = new Dictionary<string, float>();

                int THREAD_NUM = ParameterSetting.BasicMathLibThreadNum;
                int total = TripleNum;
                int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;

                float[] mAP = new float[THREAD_NUM];
                float[] mHit1 = new float[THREAD_NUM];
                float[] mHit3 = new float[THREAD_NUM];
                float[] mHit5 = new float[THREAD_NUM];
                float[] mHit10 = new float[THREAD_NUM];
                float[] mHit20 = new float[THREAD_NUM];
                float[] mHited = new float[THREAD_NUM];
                int[] mLeafNode = new int[THREAD_NUM];
                
                Parallel.For(0, THREAD_NUM, thread_idx =>
                {
                    for (int t = 0; t < process_len; t++)
                    {
                        int rawIdx = thread_idx * process_len + t;
                        if (rawIdx < total)
                        {
                            IEnumerable<int> orders = ascending ?
                                Results[rawIdx].Item2.Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index) :
                                Results[rawIdx].Item2.Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index).Reverse();
                            
                            HashSet<int> predHash = new HashSet<int>();
                            foreach(int idx in orders)
                            {
                                int predId = Results[rawIdx].Item1[idx];
                                if(predHash.Contains(predId)) continue;
                                predHash.Add(predId);

                                int pos = predHash.Count;
                                
                                if (Triple[rawIdx].Item3 == predId)
                                {
                                    mAP[thread_idx] += 1.0f / pos;
                                    if(pos <= 1) { mHit1[thread_idx] += 1; }
                                    if (pos <= 3) { mHit3[thread_idx] += 1; }
                                    if (pos <= 5) { mHit5[thread_idx] += 1; }
                                    if (pos <= 10) { mHit10[thread_idx] += 1; }
                                    if (pos <= 20) { mHit20[thread_idx] += 1; }
                                    mHited[thread_idx] += 1;
                                    break;
                                }
                            }
                            int leafNode = predHash.Count;
                            mLeafNode[thread_idx] += leafNode;
                        }
                    }
                });

                report["AP"] = mAP.Sum();
                report["Hit1"] = mHit1.Sum();
                report["Hit3"] = mHit3.Sum();
                report["Hit5"] = mHit5.Sum();
                report["Hit10"] = mHit10.Sum();
                report["Hit20"] = mHit20.Sum();
                report["Hited"] = mHited.Sum();
                report["LEAF-NODE"] = mLeafNode.Sum();
                return report;   
            }


            public DataEnviroument(GraphVocab graphVoc, string tripleFile, string[] graphFiles)
            {
                GraphVoc = graphVoc;
                Triple = new List<Tuple<int, int, int>>();
                Grouple = new List<Tuple<int, int>>();
                TruthDict = new Dictionary<int, List<int>>();
                pTruthDict = new Dictionary<int, List<int>>();
                int missNum = 0;
                using (StreamReader tripleReader = new StreamReader(tripleFile))
                {
                    while (!tripleReader.EndOfStream)
                    {
                        string[] tokens = tripleReader.ReadLine().Split('\t');

                        if (!GraphVoc.EntityDict.ContainsKey(tokens[0]))
                        {
                            missNum += 1;
                            continue;
                        }

                        if (!GraphVoc.RelationDict.ContainsKey(tokens[1]))
                        {
                            missNum += 1;
                            continue;
                        }

                        if (!GraphVoc.EntityDict.ContainsKey(tokens[2]))
                        {
                            missNum += 1;
                            continue;
                        }

                        int e1 = GraphVoc.EntityDict[tokens[0]];
                        int r = GraphVoc.RelationDict[tokens[1]];
                        int e2 = GraphVoc.EntityDict[tokens[2]];

                        Triple.Add(new Tuple<int, int, int>(e1, r, e2));

                        int gKey = GraphVoc.Key(e1, r);
                        
                        if (!pTruthDict.ContainsKey(gKey))
                        {
                            pTruthDict.Add(gKey, new List<int>());
                            Grouple.Add(new Tuple<int, int>(e1, r));
                        }
                        pTruthDict[gKey].Add(e2);
                    }
                    Logger.WriteLog("{0} Graph miss connections {1}", tripleFile, missNum);
                }

                foreach (string gStr in graphFiles)
                {
                    using (StreamReader gReader = new StreamReader(gStr))
                    {
                        while (!gReader.EndOfStream)
                        {
                            string[] tokens = gReader.ReadLine().Split('\t');

                            if (!GraphVoc.EntityDict.ContainsKey(tokens[0]))
                            {
                                //Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                                continue;
                            }

                            if (!GraphVoc.RelationDict.ContainsKey(tokens[1]))
                            {
                                //Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                                continue;
                            }

                            if (!GraphVoc.EntityDict.ContainsKey(tokens[2]))
                            {
                                //Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                                continue;
                            }

                            int e1 = GraphVoc.EntityDict[tokens[0]];
                            int r = GraphVoc.RelationDict[tokens[1]];
                            int e2 = GraphVoc.EntityDict[tokens[2]];

                            int gKey = GraphVoc.Key(e1, r);

                            if (!TruthDict.ContainsKey(gKey))
                            {
                                TruthDict.Add(gKey, new List<int>());
                            }
                            TruthDict[gKey].Add(e2);
                        }
                    }
                }
            }
        }
    
}
