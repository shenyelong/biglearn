using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class GraphDataPanel 
        {
            public static GraphEnviroument GraphEnv { get; set; }

            public static DataEnviroument TrainData { get; set; }
            public static DataEnviroument TestData { get; set; }
            public static DataEnviroument DevData { get; set; }

            public static GraphVocab GraphVoc { get; set; }

            public static void ReportOverlap(DataEnviroument refD, DataEnviroument queryD)
            {
                int overlap_unique = 0;
                int overlap_overall = 0;
                foreach(int idx in queryD.pTruthDict.Keys)
                {
                    if(refD.TruthDict.ContainsKey(idx))
                    {
                        overlap_unique += 1;
                        overlap_overall += queryD.pTruthDict[idx].Count;
                    }
                }
                Logger.WriteLog("overlap unique {0}, percentage {1}", overlap_unique, overlap_unique * 1.0f / queryD.GroupleNum);
                Logger.WriteLog("overlap overall {0}, percentage {1}", overlap_overall, overlap_overall * 1.0f / queryD.TripleNum);
            }
            
            // build device graph.
            public static void Init(string dataDir, int initMaxNeighbor)
            {
                GraphVoc = new GraphVocab(dataDir + @"/vocab/entity_vocab.json", dataDir + @"/vocab/relation_vocab.json");

                GraphEnv = new GraphEnviroument(GraphVoc, initMaxNeighbor, dataDir + @"/graph.txt");

                TrainData = new DataEnviroument(GraphVoc, dataDir + @"/train.txt",
                                            new string[] { dataDir + @"/train.txt",
                                                           dataDir + @"/graph.txt" });

                TestData = new DataEnviroument(GraphVoc, dataDir + @"/test.txt",
                                            new string[] { dataDir + @"/test.txt",
                                                           dataDir + @"/train.txt",
                                                           dataDir + @"/dev.txt",
                                                           dataDir + @"/graph.txt" });
                TestData.InitTripleResults();

                DevData = new DataEnviroument(GraphVoc, dataDir + @"/dev.txt",
                                            new string[] { dataDir + @"/test.txt",
                                                           dataDir + @"/train.txt",
                                                           dataDir + @"/dev.txt",
                                                           dataDir + @"/graph.txt" });
                DevData.InitTripleResults();

            }

        }

        
}
