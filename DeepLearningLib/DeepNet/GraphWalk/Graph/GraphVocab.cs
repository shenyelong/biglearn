using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
	public class GraphVocab
	{
		public Dictionary<string, int> EntityDict { get; set; }
        public Dictionary<string, int> RelationDict { get; set; }

        public string START_SYMBOL = "#START#";
        public string END_SYMBOL = "#END#";
            
        public int START_IDX { get { return RelationDict[START_SYMBOL]; } }
        public int END_IDX { get { return RelationDict[END_SYMBOL]; } }

        public int EntityNum { get { return EntityDict.Count; } }
        public int RelationNum { get { return RelationDict.Count; } }

        public string[] EntityLookup { get; set; }
        public string[] RelationLookup { get; set; }

        public int[] ReverseRelationTable { get; set; }

        public GraphVocab(string entityJson, string relationJson)
        {
        	EntityDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(entityJson)); //dataDir + @"/vocab/entity_vocab.json"));
            RelationDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(relationJson)); // dataDir + @"/vocab/relation_vocab.json"));
                
            RelationDict.Add(END_SYMBOL, RelationDict.Count);
            RelationDict.Add(START_SYMBOL, RelationDict.Count);

            EntityLookup = new string[EntityNum];
            foreach (KeyValuePair<string, int> item in EntityDict) EntityLookup[item.Value] = item.Key;
 
            RelationLookup = new string[RelationNum];
            foreach (KeyValuePair<string, int> item in RelationDict) RelationLookup[item.Value] = item.Key;
            
            ReverseRelationTable = new int[RelationNum];
            for(int i = 0; i < RelationNum; i++)
            {
                ReverseRelationTable[i] = ReverseRelation(i);
                if(ReverseRelationTable[i] == -1)
                {
                    Logger.WriteLog("Can not find the reverse relation {0}: {1}", i, RelationLookup[i]);
                }
            }
        }


        public int Key(int srcIdx, int relIdx)
        {
            return srcIdx * RelationDict.Count + relIdx;
        }

        int ReverseRelation(int relation)
            {
                string relStr = RelationLookup[relation];
                if(relStr.StartsWith("_"))
                {
                    string newRel = relStr.Substring(1);
                    if(RelationDict.ContainsKey(newRel))
                    {
                        return RelationDict[newRel];
                    }
                }

                string newRel2 = "_" + relStr;
                if (RelationDict.ContainsKey(newRel2))
                {
                    return RelationDict[newRel2];
                }


                if(relStr.EndsWith("_inv"))
                {
                    string newRel3 = relStr.Substring(0, relStr.Length - 4);
                    if(RelationDict.ContainsKey(newRel3))
                    {
                        return RelationDict[newRel3];
                    }
                }

                string newRel4 = relStr + "_inv";
                if (RelationDict.ContainsKey(newRel4))
                {
                    return RelationDict[newRel4];
                }
                return -1;
            }

	}
}