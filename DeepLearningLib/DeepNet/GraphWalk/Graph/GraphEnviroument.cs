using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class GraphEnviroument 
        {
            public List<Tuple<int, int>>[] Graph { get; set; }
            public int MaxNeighborNum { get; set; }
            public int TotalLinkNum { get; set; }
            public GraphVocab GraphVoc { get; set; }
            
            public CudaPieceInt LinkIdx { get; set; } 
            public CudaPieceInt LinkNodes { get; set; }
            public CudaPieceInt LinkRels { get; set; }

            public void BuildLinkGraph(DeviceType device)
            {
                Logger.WriteLog("starting build link graph .... ");

                LinkIdx = new CudaPieceInt(GraphVoc.EntityNum, device);
                LinkNodes = new CudaPieceInt(TotalLinkNum, device);
                LinkRels = new CudaPieceInt(TotalLinkNum, device);

                int lbase = 0;
                for(int i = 0; i < GraphVoc.EntityNum; i++)
                {
                    LinkIdx.MemPtr[i] = lbase + Graph[i].Count;

                    for(int n = 0; n < Graph[i].Count; n++)
                    {
                        LinkNodes.MemPtr[lbase + n] = Graph[i][n].Item2;
                        LinkRels.MemPtr[lbase + n] = Graph[i][n].Item1;
                    }
                    lbase = LinkIdx.MemPtr[i];
                }

                if(LinkIdx.EffectiveSize != GraphVoc.EntityNum)
                {
                    throw new Exception(string.Format("link idx EffectiveSize {0}, entity num {1} doesnt match! ", LinkIdx.EffectiveSize, GraphVoc.EntityNum));
                }

                if(LinkNodes.EffectiveSize != TotalLinkNum || LinkRels.EffectiveSize != TotalLinkNum)
                {
                    throw new Exception(string.Format("link nodes/rel EffectiveSize {0}, {1}, total link num {2} doesnt match! ", 
                                    LinkNodes.EffectiveSize, LinkRels.EffectiveSize, TotalLinkNum));
                }

                LinkIdx.SyncFromCPU();
                LinkNodes.SyncFromCPU();
                LinkRels.SyncFromCPU();

                Logger.WriteLog("build link graph done");
                
            }

            public GraphEnviroument(GraphVocab graphVoc,
                                    int maxNeighborNum,
                                    string graphFile)
            {
                GraphVoc = graphVoc;
                Graph = new List<Tuple<int, int>>[GraphVoc.EntityNum];
                
                for(int i=0; i < GraphVoc.EntityNum; i++)
                {
                    Graph[i] = new List<Tuple<int, int>>();
                }

                int missNum = 0;
                using (StreamReader graphReader = new StreamReader(graphFile))
                {
                    while (!graphReader.EndOfStream)
                    {
                        string[] tokens = graphReader.ReadLine().Split('\t');

                        if (!GraphVoc.EntityDict.ContainsKey(tokens[0]))
                        {
                            missNum += 1;
                            continue;
                        }

                        if (!GraphVoc.RelationDict.ContainsKey(tokens[1]))
                        {
                            missNum += 1;
                            continue;
                        }

                        if(!GraphVoc.EntityDict.ContainsKey(tokens[2]))
                        {
                            missNum += 1;
                            continue;
                        }

                        int e1 = GraphVoc.EntityDict[tokens[0]];
                        int r = GraphVoc.RelationDict[tokens[1]];
                        int e2 = GraphVoc.EntityDict[tokens[2]];

                        //if (!Graph.ContainsKey(e1)) { Graph.Add(e1, new List<Tuple<int, int>>()); }

                        if (maxNeighborNum > 0 && Graph[e1].Count >= maxNeighborNum) continue;

                        Graph[e1].Add(new Tuple<int, int>(r, e2));
                    }
                    Logger.WriteLog("{0} Graph miss connections {1}", graphFile, missNum);
                }

                MaxNeighborNum = Graph.Select(i => i.Count).Max() + 1;
                TotalLinkNum = Graph.Select(i => i.Count).Sum();
                //foreach (int entity in Graph.Keys)
                //{
                //    if (Graph[entity].Count + 1 >= MaxNeighborNum ) { MaxNeighborNum = Graph[entity].Count + 1; }
                //}
                Logger.WriteLog("Max Neighbor Number {0}", MaxNeighborNum);
                Logger.WriteLog("Total Connection Number {0}", TotalLinkNum);
                   
            }
        }
}
