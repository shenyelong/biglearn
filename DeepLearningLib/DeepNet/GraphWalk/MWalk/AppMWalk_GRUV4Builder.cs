using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
    /*
    public class AppMWalk_GRUV4Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int RNN_Dim { get { return int.Parse(Argument["RNN-DIM"].Value); } }
            //public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] DNN_DROPS { get { return Argument["DNN-DROPS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static int[] V_DIMS { get { return Argument["V-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] V_DROPS { get { return Argument["V-DROPS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }
            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int DEV_AS_TRAIN { get { return int.Parse(Argument["DEV-TRAIN"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }
            
            public static int ActorMiniBatch { get { return int.Parse(Argument["ACTOR-MINI-BATCH"].Value); } }
            public static int ActorEpoch { get { return int.Parse(Argument["ACTOR-EPOCH"].Value); } }

            public static int ActorPolicy { get { return int.Parse(Argument["ACTOR-POLICY"].Value); } } 
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static int LearnerMiniBatch { get { return int.Parse(Argument["LEARNER-MINI-BATCH"].Value); } }
            public static int LearnerEpoch { get { return int.Parse(Argument["LEARNER-EPOCH"].Value); } }
            public static int ModelSyncUp { get { return int.Parse(Argument["MODEL-SYNCUP"].Value); } }
            public static int ReplayBufferSize { get { return int.Parse(Argument["REPLAY-BUFF-SIZE"].Value); } } 
            public static int TReplaySize { get { return int.Parse(Argument["T-REPLAY-SIZE"].Value); } } 
            public static float ReplayDecay { get { return float.Parse(Argument["REPLAY-DECAY"].Value); } }

            public static float Lambda { get { return float.Parse(Argument["LAMBDA"].Value); } }

            public static int Train_MCTS_NUM { get { return int.Parse(Argument["TRAIN-MCTS-NUM"].Value); } } 
            public static int Test_MCTS_NUM {  get { return int.Parse(Argument["TEST-MCTS-NUM"].Value); } } 
            public static List<Tuple<int, float>> ScheduleEpsilon
            {
                get
                {
                    return Argument["SCHEDULE-EPSILON"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static List<Tuple<int, float>> TestScheduleEpsilon
            {
                get
                {
                    return Argument["TEST-SCHEDULE-EPSILON"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));

                Argument.Add("RNN-DIM", new ParameterArgument("100", "RNN hidden state dim."));

                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                Argument.Add("DNN-DROPS", new ParameterArgument("0,0", "DNN Drop outs."));
                
                Argument.Add("V-DIMS", new ParameterArgument("100,100", "Score DNN dimensions"));
                Argument.Add("V-DROPS", new ParameterArgument("0,0", "Score DNN Dropouts"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
 
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));


                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));
                
                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("ALPHA-1", new ParameterArgument("0","max entropy for termination gate."));
                Argument.Add("ALPHA-2", new ParameterArgument("0", "max entropy for action gate."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));


                Argument.Add("ACTOR-MINI-BATCH", new ParameterArgument("128", "actor mini batch size"));
                Argument.Add("ACTOR-EPOCH", new ParameterArgument("5", "actor training epoch size"));

                Argument.Add("ACTOR-POLICY", new ParameterArgument("4", "0:UnifiedSampling; 1:ThompasSampling; 2:UCB; 3:AlphaGoZero1; 4:AlphaGoZero2; 5:PUCB"));

                Argument.Add("LEARNER-MINI-BATCH", new ParameterArgument("32", "learner mini batch size"));
                Argument.Add("LEARNER-EPOCH", new ParameterArgument("5", "learner training epoch size"));
                Argument.Add("MODEL-SYNCUP", new ParameterArgument("3000", "model syncup iteration."));   
                Argument.Add("REPLAY-BUFF-SIZE", new ParameterArgument("3000000", "buffer size for the replay memory."));   
                Argument.Add("T-REPLAY-SIZE", new ParameterArgument("5000", "threshold of replay buffer size."));   
                Argument.Add("REPLAY-DECAY", new ParameterArgument("0.99995", "Replay Decay Value."));   
                Argument.Add("LAMBDA", new ParameterArgument("0.01", "lambda for Q learning."));

                Argument.Add("SCHEDULE-EPSILON", new ParameterArgument("0:0.5,300:0.3,1000:0.1", "Epsilon Schedule"));
                Argument.Add("TEST-SCHEDULE-EPSILON", new ParameterArgument("0:0.5,300:0.3,1000:0.1", "Epsilon Schedule for test."));

                Argument.Add("TRAIN-MCTS-NUM", new ParameterArgument("200", "Train MCTS Number."));   
                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("1000", "Test MCTS Number."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_MWALK_GRUV4; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }        

        static Random SampleRandom = new Random(DeepNet.BuilderParameters.RandomSeed + 101);

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure NodeEmbed { get; set; }
            public EmbedStructure RelEmbed { get; set; } 
            public DNNStructure VDNN { get; set; }            
            public DNNStructure AttDNN { get; set; }
            public GRUCell GruCell { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeDim, int relDim, int rnnDim, DeviceType device)
            {
                NodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); 
                RelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); 

                int embedDim = nodeDim + relDim;
                GruCell = AddLayer(new GRUCell(relDim, rnnDim, device));

                int inputDim = rnnDim + embedDim;

                AttDNN = AddLayer(new DNNStructure(rnnDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DROPS,
                                           BuilderParameters.DNN_DIMS.Select(i => true).ToArray(),
                                           device));
                
                VDNN = AddLayer(new DNNStructure(inputDim, BuilderParameters.V_DIMS,
                                           BuilderParameters.V_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.V_DROPS,
                                           BuilderParameters.V_DIMS.Select(i => true).ToArray(),
                                           device));
            }
            
            public override void CopyFrom(IData other) 
            {
                NeuralWalkerModel src = (NeuralWalkerModel)other;

                NodeEmbed.CopyFrom(src.NodeEmbed);
                RelEmbed.CopyFrom(src.RelEmbed);
                GruCell.CopyFrom(src.GruCell);
                AttDNN.CopyFrom(src.AttDNN);
                VDNN.CopyFrom(src.VDNN);
            }


            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                NodeEmbed = DeserializeModel<EmbedStructure>(reader, device);  
                RelEmbed = DeserializeModel<EmbedStructure>(reader, device);  
                GruCell = DeserializeModel<GRUCell>(reader, device);
                AttDNN = DeserializeModel<DNNStructure>(reader, device);
                VDNN = DeserializeModel<DNNStructure>(reader, device);
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public class RewardAssignRunner : StructRunner
        {
            public StatusData Status { get; set; }
            public CudaPieceFloat Label { get; set; }
            public RewardAssignRunner(StatusData status, RunnerBehavior behavior) 
                : base(Structure.Empty, behavior)
            {
                Status = status;
                Label = new CudaPieceFloat(Status.MaxBatchSize, behavior.Device);
            }

            public override void Forward()
            {
                GraphQueryData Query = Status.GraphQuery;
                Label.EffectiveSize = Status.BatchSize;
                for(int i = 0; i < Status.BatchSize; i++)
                {
                    int predId = Status.NodeID[i];

                    int origialB = Status.GetOriginalStatsIndex(i);

                    int targetId = Query.RawTarget[origialB];
                    int raw_b = Query.RawIndex[origialB];
                    int raw_src = Query.RawSource[origialB];
                    int raw_rel = Query.RawRel[origialB];

                    float label = 0;
                    if(targetId > -1)
                    {
                        if (Query.BlackTargets[origialB].Contains(predId) && predId != targetId)
                        {
                            label = 0;
                        }
                        else if (targetId == predId)
                        {
                            label = 1;
                        }
                        else
                        {
                            label = 0;
                        }
                    }
                    else
                    {
                        if (Query.BlackTargets[origialB].Contains(predId))
                        {
                            label = 1;
                        }
                        else  
                        {
                            label = 0;
                        }
                    }
                    Label[i] = label;
                }
                Label.SyncFromCPU();
            }
        }

        // must support beam search.
        public static void ReinforceWalkModel(ComputationGraph cg, 
                                  GraphQueryData graphQuery, 
                                  GraphEnviroument graph, 
                                  int policyId, 
                                  int maxHop, 
                                  MCTSMemory actMem,
                                  RateScheduler epsilon,
                                  NeuralWalkerModel model, 
                                  RunnerBehavior Behavior)
        {
            HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, model.NodeEmbed, Behavior));
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawRel, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));
            
            if(Behavior.RunMode == DNNRunMode.Train)
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(srcEmbed, 0.2f, Behavior));

            if(Behavior.RunMode == DNNRunMode.Train)
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(relEmbed, 0.2f, Behavior));

            HiddenBatchData iEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { srcEmbed, relEmbed }, Behavior));

            IntArgument batchSizeArg = new IntArgument("sample-batch-size"); 
            cg.AddRunner(new HiddenDataBatchSizeRunner(iEmbed, batchSizeArg, Behavior));

            HiddenBatchData initZeroH0 = (HiddenBatchData)cg.AddRunner(new ZeroMatrixRunner(relEmbed.MAX_BATCHSIZE, model.GruCell.HiddenStateDim, batchSizeArg, Behavior));
            GRUStateRunner initStateRunner = new GRUStateRunner(model.GruCell, initZeroH0, relEmbed, Behavior);
            cg.AddRunner(initStateRunner);

            HiddenBatchData H1 = initStateRunner.Output;
            StatusData status = new StatusData(graphQuery, graphQuery.RawSource, H1, Behavior.Device);
            
            HiddenBatchData tgtEmbed = srcEmbed;

            #region multi-hop expansion
            // travel in the knowledge graph.
            for (int i = 0; i < maxHop; i++)
            {
                CandidateV2Runner candidateActionRunner = new CandidateV2Runner(status, graph, 1, Behavior);
                cg.AddRunner(candidateActionRunner);
                status.MatchCandidate_Node = candidateActionRunner.LinkNodes;
                status.MatchCandidate_Rel = candidateActionRunner.LinkRels;

                HiddenBatchData candNodeEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.LinkNodes, 
                                                                        candidateActionRunner.LinkNodes.Size, model.NodeEmbed, Behavior));

                HiddenBatchData candRelEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.LinkRels, 
                                                                        candidateActionRunner.LinkRels.Size, model.RelEmbed, Behavior));

                if(Behavior.RunMode == DNNRunMode.Train)
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(candNodeEmbed, 0.2f, Behavior));
                if(Behavior.RunMode == DNNRunMode.Train)
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(candRelEmbed, 0.2f, Behavior));

                SeqMatrixData seqCandEmbed = new SeqMatrixData(candRelEmbed.Dim, candRelEmbed.MAX_BATCHSIZE, status.MaxBatchSize,
                                                               candRelEmbed.Output.Data, candRelEmbed.Deriv.Data, candidateActionRunner.LinkIdxs, candidateActionRunner.LinkMargin, Behavior.Device);

                DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.AttDNN, status.StateEmbed, Behavior);
                cg.AddRunner(srcHiddenRunner);

                SeqInnerProductRunner actRunner = new SeqInnerProductRunner(new MatrixData(srcHiddenRunner.Output), seqCandEmbed, Behavior);
                cg.AddRunner(actRunner);
                SeqVectorData actScore = new SeqVectorData(actRunner.Output.MaxLength,
                                                           status.MaxBatchSize, actRunner.Output.Output, actRunner.Output.Deriv,
                                                           candidateActionRunner.LinkIdxs, candidateActionRunner.LinkMargin, Behavior.Device);
                status.MatchCandidateQ = actScore;

                SeqVecSoftmaxRunner actNormRunner = new SeqVecSoftmaxRunner(actScore, 10, Behavior, true)
                { 
                        IsBackProp = false,
                        IsUpdate = false
                };
                cg.AddRunner(actNormRunner);
                status.MatchCandidateProb = actNormRunner.Output;
                
                BasicPolicyRunner policyRunner = null;
                // Epislon greedy policy.
                if(policyId == 0)
                {
                    policyRunner = new ThompasSamplingRunner(status, graph.MaxNeighborNum, SampleRandom, epsilon, Behavior);
                }
                else if(policyId == 1)
                {
                    policyRunner = new ReplayPolicyRunner(status, graph.MaxNeighborNum, Behavior);
                }
                else if(policyId == 2)
                {
                    policyRunner = new MCTSPolicyV2Runner(status, graph.MaxNeighborNum, actMem, 
                                    BuilderParameters.PUCT_C, BuilderParameters.PUCT_D, SampleRandom, epsilon, Behavior);
                }
                // beam search policy.
                //else if(policyId == 2)
                //{
                //    policyRunner = new BeamSearchActionRunner(status, graph.MaxNeighborNum, beamSize, Behavior);
                //}
                policyRunner.ReportPerEpoch = 200;
                cg.AddRunner(policyRunner);

                #region take action.

                //MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                //cg.AddRunner(srcExpRunner);

                MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candRelEmbed, policyRunner.MatchPath, 2, Behavior);
                cg.AddRunner(tgtExpRunner);

                //MatrixExpansionRunner rQExpRunner = new MatrixExpansionRunner(Rq, policyRunner.MatchPath, 1, Behavior);
                //cg.AddRunner(rQExpRunner);
                //Rq = rQExpRunner.Output;

                //GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
                GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, status.StateEmbed, tgtExpRunner.Output, Behavior);
                cg.AddRunner(stateRunner);

                //MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbed, policyRunner.MatchPath, 2, Behavior);
                //HiddenBatchData selCandEmbed = (HiddenBatchData)cg.AddRunner(new MatrixExpansionRunner(candEmbed, policyRunner.MatchPath, 2, Behavior));

                tgtEmbed = (HiddenBatchData)cg.AddRunner(new MatrixExpansionRunner(candNodeEmbed, policyRunner.MatchPath, 2, Behavior));
                
                //MatrixExpansionRunner rQExpRunner = new MatrixExpansionRunner(Rq, policyRunner.MatchPath, 1, Behavior);
                //cg.AddRunner(rQExpRunner);
                //Rq = rQExpRunner.Output;
                //GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, status.StateEmbed, selCandEmbed, Behavior);
                //cg.AddRunner(stateRunner);
                #endregion.

                StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                    policyRunner.ActIndex, policyRunner.StatusIndex, stateRunner.Output, Behavior.Device);
                status = nextStatus;

                HiddenBatchData vEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { iEmbed, status.StateEmbed }, Behavior));
                DNNRunner<HiddenBatchData> vHiddenRunner = new DNNRunner<HiddenBatchData>(model.VDNN, vEmbed, Behavior);
                cg.AddRunner(vHiddenRunner);

                MatrixInnerProductRunner vRunner = new MatrixInnerProductRunner(new MatrixData(vHiddenRunner.Output), new MatrixData(tgtEmbed), null, Behavior);
                cg.AddRunner(vRunner);
                status.Score = vRunner.Output;
            }
            #endregion.
        }

        // actor model.
        public static void ActorModel(ComputationGraph cg, DataEnviroument data, DataRandomShuffling shuffle, 
                                      GraphEnviroument graph, int maxHop, EpisodicMemory mem, MCTSMemory actMem,
                                      RateScheduler epsilon, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            // sample dataset and push into the replay memory.
            SampleRunner SmpRunner = new SampleRunner(data, BuilderParameters.ActorMiniBatch, 1, BuilderParameters.ActorEpoch, true, shuffle, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            // mcts policy.
            ReinforceWalkModel(cg, interface_data, graph, 2, maxHop, actMem, epsilon, model, Behavior);

            ReplayRollRunner replayRollRunner = new ReplayRollRunner(interface_data, mem, actMem, Behavior) { ReportPerEpoch = 10000 };
            cg.AddRunner(replayRollRunner);

            cg.SetDelegateModel(model);
        }

        // learner model.
        public static void LearnerModel(ComputationGraph cg, DataEnviroument data,  GraphEnviroument graph, int maxHop, EpisodicMemory mem, 
                                                NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            ReplayMemRunner replayRunner = new ReplayMemRunner(mem, data, BuilderParameters.LearnerMiniBatch, BuilderParameters.LearnerEpoch, Behavior);
            cg.AddDataRunner(replayRunner);
            GraphQueryData interface_data = replayRunner.Output;
            
            ReinforceWalkModel(cg, interface_data, graph, 1, maxHop, null, null, model, Behavior);

            for(int i = 0; i < interface_data.StatusPath.Count; i++)
            {
                StatusData st = interface_data.StatusPath[i];
                if(st.Score != null)
                {
                    HiddenBatchData vData = new HiddenBatchData(st.Score); 
                    
                    RewardAssignRunner rRunner = new RewardAssignRunner(st, Behavior);
                    cg.AddRunner(rRunner);

                    CrossEntropyRunner ceRunner = new CrossEntropyRunner(rRunner.Label, vData, Behavior) { name = "cerunner " + i.ToString() };
                    cg.AddObjective(ceRunner);
                }
            }

            QLearnV2Runner qRunner = new QLearnV2Runner(interface_data, BuilderParameters.REWARD_DISCOUNT, BuilderParameters.Lambda, Behavior);
            cg.AddObjective(qRunner);

            cg.SetDelegateModel(model);
        }


        public static void TestBeamModel(ComputationGraph cg, DataEnviroument data, DataRandomShuffling shuffle, GraphEnviroument graph, MCTSMemory actMem,
            int maxHop, RateScheduler epsilon, NeuralWalkerModel model, RunnerBehavior Behavior) 
        {
            // work as beam search.
            SampleRunner smpRunner = new SampleRunner(data, BuilderParameters.TestMiniBatchSize, 1, BuilderParameters.TEST_SAMPLES, false, shuffle, Behavior);
            cg.AddDataRunner(smpRunner);
            GraphQueryData interface_data = smpRunner.Output;

            ReinforceWalkModel(cg, interface_data, graph, 2, maxHop, actMem, epsilon, model, Behavior);
            
            ReplayRollRunner replayRollRunner = new ReplayRollRunner(interface_data, null, actMem, Behavior) { ReportPerEpoch = 1000000 };
            cg.AddRunner(replayRollRunner);

            // aggregate last step q value;
            GraphHitKRunner hitkRunner = new GraphHitKRunner(interface_data, data, 2, Behavior);
            hitkRunner.ReportPerEpoch = 1000000;
            cg.AddRunner(hitkRunner);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            
            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };
            
            Logger.WriteLog("Loading Training/Validation/Test Data.");
            GraphDataPanel.Init(BuilderParameters.InputDir, BuilderParameters.MAX_ACTION_NUM);
            GraphDataPanel.GraphEnv.BuildLinkGraph(device);
            
            DataRandomShuffling trainShuff = new DataRandomShuffling(GraphDataPanel.TrainData.GroupleNum);
            DataRandomShuffling testShuff = new DataRandomShuffling(GraphDataPanel.TestData.TripleNum);
            DataRandomShuffling devShuff = new DataRandomShuffling(GraphDataPanel.DevData.TripleNum);

            Logger.WriteLog("test overlap with training stat ...");
            GraphDataPanel.ReportOverlap(GraphDataPanel.TrainData, GraphDataPanel.TestData);

            Logger.WriteLog("dev overlap with training stat ...");
            GraphDataPanel.ReportOverlap(GraphDataPanel.TrainData, GraphDataPanel.DevData);
            
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            model.InitOptimization(behavior);

            Logger.WriteLog("MWalk-GRU Model Parameter Number {0}", model.ParamNumber);

            EpisodicMemory Memory = new EpisodicMemory(true, false, 
                                                        BuilderParameters.ReplayBufferSize,
                                                        BuilderParameters.ReplayDecay,
                                                        BuilderParameters.REWARD_DISCOUNT,
                                                        BuilderParameters.TReplaySize);

            ComputationGraph testCG = new ComputationGraph(behavior);
            ComputationGraph validCG = new ComputationGraph(behavior);
            ComputationGraph actorCG = new ComputationGraph(behavior);
            ComputationGraph learnerCG = new ComputationGraph(behavior);

            RateScheduler testEpsilon = new RateScheduler(BuilderParameters.TestScheduleEpsilon);
            // suppose we have at most 1000 mcts numbers.
            MCTSMemory mctsMemory = new MCTSMemory();

            {
                TestBeamModel(testCG, GraphDataPanel.TestData, testShuff, GraphDataPanel.GraphEnv, mctsMemory,
                    BuilderParameters.MAX_HOP, testEpsilon, model, behavior);
             
                TestBeamModel(validCG, GraphDataPanel.DevData, devShuff, GraphDataPanel.GraphEnv, mctsMemory,
                    BuilderParameters.MAX_HOP, testEpsilon, model, behavior);   
            }
          
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    NeuralWalkerModel backupModel = new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device);
                    backupModel.CopyFrom(model);
                    Logger.WriteLog("Generate backup Model.");

                    DataEnviroument trainData = GraphDataPanel.TrainData;

                   
                    //List<Tuple<int, float>> schedule = new List<Tuple<int, float>>();
                    //schedule.Add(new Tuple<int, float> (0, 0.5f));
                    //schedule.Add(new Tuple<int, float> (300, 0.3f));
                    //schedule.Add(new Tuple<int, float> (1000, 0.1f));
                    RateScheduler epsilon = new RateScheduler(BuilderParameters.ScheduleEpsilon);

                    ActorModel(actorCG, trainData, trainShuff,
                            GraphDataPanel.GraphEnv, BuilderParameters.MAX_HOP, Memory, mctsMemory, epsilon, backupModel,behavior);
            
                    LearnerModel(learnerCG, trainData,  
                            GraphDataPanel.GraphEnv, BuilderParameters.MAX_HOP, Memory, model, behavior);

                    actorCG.Init();
                    learnerCG.IsReportObjectives = false;
                    
                    int actorEpoch = 0;

                    for(int iter = 1; iter <= OptimizerParameters.Iteration; iter++)
                    {
                        // actor model collect the data into replay memory.
                        int record_idx = 0;
                        
                        trainShuff.Shuffling();
                        trainShuff.SetCursor(0);

                        while(!trainShuff.IsDone)
                        {
                            epsilon.Init();
                            mctsMemory.Clear();
                            for(int i = 0; i < BuilderParameters.Train_MCTS_NUM; i++)
                            {
                                trainShuff.SetCursor(record_idx);
                                int idx = 0; 

                                while(true)
                                {
                                    actorCG.Forward();
                                    if(actorCG.IsTerminate) break;
                                    
                                    //Logger.WriteLog("forward {0}", idx);
                                    if(Memory.IsReplayBufferReady)
                                    {
                                        learnerCG.Execute();
                                    }
                                    idx += 1;
                                }
                                actorCG.Complete();
                                actorCG.Init();
                                
                                epsilon.Incr();
                            }
                            //Logger.WriteLog("keep looping ...");
                            record_idx = trainShuff.GetCursor();
                            backupModel.CopyFrom(model);
                        }
                        actorEpoch += 1;
                        Logger.WriteLog("actor CG restart iteration : {0}; epoch {1}", iter, actorEpoch);
                        
                        {
                            //Logger.WriteLog("Epislon {0}", epsilon.CurrentEps);
                            if( actorEpoch % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                            {
                                Logger.WriteLog("Evaluation at actorEpoch {0}", actorEpoch);

                                GraphDataPanel.TestData.ClearResults();

                                record_idx = 0;
                                testShuff.SetCursor(0);
                                while(!testShuff.IsDone)
                                {
                                    GraphHitKRunner.Epoch = 0;
                                    testEpsilon.Init();
                                    mctsMemory.Clear();
                                    for(int i = 0; i < BuilderParameters.Test_MCTS_NUM; i++)
                                    {
                                        //Logger.WriteLog("test mcts number {0}...", i);
                                        testShuff.SetCursor(record_idx); 
                                        testCG.Execute();
                                        testEpsilon.Incr();
                                    }
                                    //Logger.WriteLog("finish one epoch in test...");
                                    record_idx = testShuff.GetCursor();
                                }
                                GraphHitKRunner.Report(GraphDataPanel.TestData);

                                //results.Clear();
                                //TestMemory.Clear();
                                GraphDataPanel.DevData.ClearResults();
                                record_idx = 0;
                                devShuff.SetCursor(0);
                                while(!devShuff.IsDone)
                                {
                                    GraphHitKRunner.Epoch = 0;
                                    testEpsilon.Init();
                                    mctsMemory.Clear();
                                    for(int i=0; i < BuilderParameters.Test_MCTS_NUM; i++)
                                    {
                                        //Logger.WriteLog("dev mcts number {0}...", i);
                                        devShuff.SetCursor(record_idx);
                                        validCG.Execute();
                                        testEpsilon.Incr();
                                    }
                                    //Logger.WriteLog("finish one epoch in dev...");
                                    record_idx = devShuff.GetCursor();
                                }
                                GraphHitKRunner.Report(GraphDataPanel.DevData);
                            }
                        }
                    }
                    break;
            }    
            Logger.CloseLog();
        }

    }*/
}
