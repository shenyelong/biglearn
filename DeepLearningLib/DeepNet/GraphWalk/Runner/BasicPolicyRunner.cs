
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
		public class BasicPolicyRunner : CompositeNetRunner
        {
            public new StatusData Input { get; set; }

            /// <summary>
            /// MatchData.
            /// </summary>
            public BiMatchBatchData MatchPath = null;

            /// <summary>
            /// new Node Index.
            /// </summary>
            public List<int> NodeIndex { get; set; }

            /// <summary>
            /// next step log probability.
            /// </summary>
            public List<float> LogProb { get; set; }

            /// <summary>
            /// pre sel action and pre status. 
            /// </summary>
            public List<int> ActIndex { get; set; }
            public List<int> StatusIndex { get; set; }

            public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                NodeIndex = new List<int>();
                LogProb = new List<float>();
                ActIndex = new List<int>();
                StatusIndex = new List<int>();
            }

        }
}