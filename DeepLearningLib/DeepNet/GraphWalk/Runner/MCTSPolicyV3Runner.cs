

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class MCTSPolicyV3Runner : BasicPolicyRunner
        {
            MCTSMemory Memory { get; set; }

            float Alpha_C { get; set; }
            float Alpha_D { get; set; }

            Random mrandom { get; set; }
            RateScheduler Epsilon { get; set; }
            Random[] ThreadRandoms { get; set; }

            float[] TmpQ { get; set; }

            int BanditPolicyId { get; set; }
            /// <summary>
            /// match candidate, and match probability. 
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSPolicyV3Runner(StatusData input, int maxNeighbor, MCTSMemory actMem,
            						float alpha_c, 
            						float alpha_d, 
                                    Random randomizer, 
                                    RateScheduler epsilon, 
                                    int banditPolicyId,
            						RunnerBehavior behavior) : base(input, behavior)
            {
                Memory = actMem;

                Alpha_C = alpha_c;
                Alpha_D = alpha_d;

                mrandom = randomizer;
                Epsilon = epsilon;

                BanditPolicyId = banditPolicyId;

                MatchPath = new BiMatchBatchData(
                    //new BiMatchBatchDataStat()
                //{
                //    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                //    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                //    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                //},
                    Input.MaxBatchSize,
                    Input.MaxBatchSize,
                    Input.MaxBatchSize * maxNeighbor,
                    behavior.Device);

                ThreadRandoms = new Random[Input.MaxBatchSize];
                for(int i=0; i < Input.MaxBatchSize; i++)
                {
                    ThreadRandoms[i] = new Random(i + randomizer.Next());
                }
                TmpQ = new float[Input.MaxBatchSize * maxNeighbor];
            }

            public override void Forward()
            {
            	if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }
                var perf_mcs = PerfCounter.Manager.Instance["mctsSamplingV3Runner"].Begin();

                Input.MatchCandidateProb.Output.SyncToCPU();

                NodeIndex.Clear();
                ActIndex.Clear();
                StatusIndex.Clear();
                LogProb.Clear();
                MatchPath.Clear();
                
                int currentStep = Input.Step;
                int batchSize = Input.MatchCandidateProb.Segment;
                
                int[] actIndex = new int[batchSize];

                int THREAD_NUM = ParameterSetting.BasicMathLibThreadNum;
                int total = batchSize;
                int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
                
                Parallel.For(0, THREAD_NUM, thread_idx =>
                {
                    for (int t = 0; t < process_len; t++)
                    {
                        int i = thread_idx * process_len + t;
                        if( i >= total) continue;
                            //Parallel.For(0, batchSize, i =>
                        //{
                        int e = Input.MatchCandidateProb.SegmentIdx[i];
                        int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx[i - 1];

                        int actionDim = e - s;

                        int origialB = Input.GetOriginalStatsIndex(i);
                        int rawIdx = Input.GraphQuery.RawIndex[origialB];

                        int[] path = Input.GetArmPath(i); 
                        //rawIdx -> rawRel;
                        
                        //Tuple<float[], float[], float> arms = Memory == null ? null : Memory.Search(rawIdx, path, thread_idx);  
                        
                        //Memory.Search(rawIdx, Pkey);
                        
                        float r = (float)ThreadRandoms[i].NextDouble();
                        
                        int idx = -1;


                        if (Epsilon != null && Epsilon.AmIin(r)) 
                        {
                            idx = ThreadRandoms[i].Next(0, actionDim);
                        }
                        else 
                        {
                            MCTSNode arms = Memory == null ? null : Memory.SearchMCTSNode(rawIdx, path, thread_idx);
                            
                            if(BanditPolicyId == 3)
                            {
                                idx = BanditAlg.StocasticBandit(arms, Input.MatchCandidateProb.Output.TakeCPU(0).ToArray(), TmpQ, 5, s, actionDim, Alpha_C, Alpha_D, ThreadRandoms[i]);
                            }
                            else if(BanditPolicyId == 4)
                            {
                                idx = BanditAlg.AlphaGoZeroBandit(arms, Input.MatchCandidateProb.Output.TakeCPU(0).ToArray(), s, actionDim, Alpha_C, Alpha_D);
                            }
                        }
                        int selectIdx = s + idx;
                        actIndex[i] = selectIdx;
                    }
                });
                

                for (int i = 0; i < batchSize; i++)
                {
                    int selectIdx = actIndex[i];
                    float lp = (float)Math.Log(Input.MatchCandidateProb.Output[selectIdx]);
                    //total_smp_prob += Input.MatchCandidateProb.Output.MemPtr[selectIdx];
                    //total_smp += 1;
                    float prob = Input.GetLogProb(i) + lp; // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                    {
                        ActIndex.Add(selectIdx);
                        StatusIndex.Add(i);
                        int n = Input.MatchCandidate != null ? Input.MatchCandidate.Item1[selectIdx] :
                                                                Input.MatchCandidate_Node[selectIdx];
                        NodeIndex.Add(n);
                        LogProb.Add(prob);
                        MatchPath.Push(i, selectIdx, 1);
                    }
                }
                MatchPath.PushDone();

                PerfCounter.Manager.Instance["mctsSamplingV3Runner"].TakeCount(perf_mcs);
            }
        }
}