
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
	public class ScoreHitKRunner : StructRunner
    {
         
            public Dictionary<string, float> Result { get; set; }
            GraphQueryData Data { get; set; }
            HiddenBatchData Score { get; set; }
            
            int MiniBatchCounter = 0;
            int SmpNum = 0;
            bool IsAscending { get; set; }
            //order = 0: ascedning order; order = 1: descending order;
            public ScoreHitKRunner(GraphQueryData data, HiddenBatchData score, bool isAscending, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                Score = score;
                IsAscending = isAscending;
                Result = new Dictionary<string, float>();
            }

            public override void Forward()
            {
                EvalUtil.HitK(Score, Data.BlackTargets, Data.RawTarget, Data.Roll, IsAscending, Result);
                MiniBatchCounter += 1;
                SmpNum += Score.BatchSize;
            }
            
            public override void Init()
            {
                Result.Clear();
                MiniBatchCounter = 0;
                SmpNum = 0;
            }

            public override void Complete()
            {
                foreach(KeyValuePair<string, float> p in Result)
                {
                    Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / SmpNum);
                }
            }
    }
}