using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        class CandidateRunner : CompositeNetRunner
        {
            public new StatusData Input;
            GraphEnviroument Graph;

            public BiMatchBatchData Match;
            public new Tuple<List<int>, List<int>> Output = null;
            
            bool FilterR = false;
            /// <summary>
            /// Input : State.
            /// node and rel embed.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="nodeEmbed"></param>
            /// <param name="relEmbed"></param>
            /// <param name="behavior"></param>
            public CandidateRunner(StatusData input, GraphEnviroument graph, RunnerBehavior behavior) 
                : this(input, graph, false, behavior)
            { }

            public CandidateRunner(StatusData input, GraphEnviroument graph, bool filterR, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                Graph = graph;
                FilterR = filterR;

                int maxMatchSize = Input.MaxBatchSize * Graph.MaxNeighborNum;
                
                Match = new BiMatchBatchData(
                //    new BiMatchBatchDataStat()
                //{
                //    MAX_MATCH_BATCHSIZE = maxMatchSize,
                //    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                //    MAX_TGT_BATCHSIZE = maxMatchSize
                //}, 
                    maxMatchSize,
                    Input.MaxBatchSize,
                    maxMatchSize,
                    behavior.Device);

                Output = new Tuple<List<int>, List<int>>(new List<int>(maxMatchSize), new List<int>(maxMatchSize));
            }

            public override void Forward()
            {
                Output.Item1.Clear();
                Output.Item2.Clear();
                Match.Clear();

                var perf_candidate = PerfCounter.Manager.Instance["candidateRunner_forward"].Begin();

                int cursor = 0;
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    // current node.
                    int seedNode = Input.NodeID[b];

                    // query idx. 
                    int qid = Input.GetOriginalStatsIndex(b);

                    // raw node.
                    int rawN = Input.GraphQuery.RawSource[qid];

                    // raw relation.
                    int rawR = Input.GraphQuery.RawRel[qid];

                    // raw answer.
                    int answerN = Input.GraphQuery.RawTarget[qid];

                    int candidateNum = 0;

                    //&& !IsLast
                    //if (Graph.Graph.ContainsKey(seedNode) )
                    {
                        for (int nei = 0; nei < Graph.Graph[seedNode].Count; nei++)
                        {
                            int lid = Graph.Graph[seedNode][nei].Item1;
                            int tgtNode = Graph.Graph[seedNode][nei].Item2;

                            // filter the existing relation links.
                            if(FilterR)
                            {
                                //&& tgtNode == answerN
                                if (seedNode == rawN && lid == rawR) { continue; }
                                //seedNode == answerN && 
                                if (tgtNode == rawN && lid == Graph.GraphVoc.ReverseRelationTable[rawR]) { continue; }
                            }
                            else
                            {
                                if (seedNode == rawN && lid == rawR && tgtNode == answerN) { continue; }
                                if (tgtNode == rawN && seedNode == answerN && lid == Graph.GraphVoc.ReverseRelationTable[rawR]) { continue; }
                            }
                            Output.Item1.Add(tgtNode);
                            Output.Item2.Add(lid); 
                        
                            Match.Push(b, cursor, 1);
                            cursor += 1;
                            candidateNum += 1;
                        }
                    }
                    // add end action for each node.
                    {
                        Output.Item1.Add(seedNode);
                        Output.Item2.Add(Graph.GraphVoc.END_IDX); 

                        Match.Push(b, cursor, 1);
                        cursor += 1;
                        candidateNum += 1;
                    }
                }

                var perf_candidate_match = PerfCounter.Manager.Instance["candidateRunner_match_forward"].Begin();
                Match.PushDone();
                PerfCounter.Manager.Instance["candidateRunner_match_forward"].TakeCount(perf_candidate_match);

                PerfCounter.Manager.Instance["candidateRunner_forward"].TakeCount(perf_candidate);
            }
        }
    
}
