using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
	public class ReplayRollRunner : StructRunner
    	{
            new GraphQueryData Input { get; set; }
            EpisodicMemory Memory { get; set; }
            MCTSMemory ActMemory { get; set; }
            int PosNum = 0;
            int NegNum = 0;
            int BlackNum = 0;

            int SmpNum = 0;
            int Epoch = 0;
            
            public override void Init()
            {
                SmpNum = 0;
                PosNum = 0;
                NegNum = 0;
                BlackNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                if(Epoch % ReportPerEpoch == 0)
                {
                    ReportNum();
                }
            }

            public void ReportNum()
            {
                Logger.WriteLog("Sample Num {0}", SmpNum);
                Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosNum / (SmpNum + 0.000001f));
                Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegNum / (SmpNum + 0.000001f));
                Logger.WriteLog("Black Num {0}, Black Mean {1}", BlackNum, BlackNum / (SmpNum + 0.000001f));
            }

            public ReplayRollRunner(GraphQueryData input, EpisodicMemory memory, RunnerBehavior behavior) 
                : this(input, memory, null, behavior)
            { }

            public ReplayRollRunner(GraphQueryData input, EpisodicMemory memory, MCTSMemory actMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
                ActMemory = actMem;
            }

            public override void Forward()
            {
                var perf_replayRoll = PerfCounter.Manager.Instance["replayRollRunner"].Begin();
                
                for (int i = 0; i < Input.StatusPath.Count ; i++)
                    {
                        StatusData cst = Input.StatusPath[i];
                        if(cst.Score != null) cst.Score.Output.SyncToCPU();
                    }

                StatusData st = Input.StatusPath.Last();
                int t = st.Step; 

                for (int i = 0; i < st.BatchSize; i++)
                {
                    int b = i; 
                    int origialB = st.GetOriginalStatsIndex(b);

                    int predId = st.NodeID[b];

                    int targetId = Input.RawTarget[origialB];
                    int raw_b = Input.RawIndex[origialB];
                    int raw_src = Input.RawSource[origialB];
                    int raw_rel = Input.RawRel[origialB];

                    float replayReward = EvalUtil.TrueReward(predId, targetId, Input.BlackTargets[origialB]);

                    // if(replayReward )

                    // if(targetId > -1)
                    // {
                    //     if (Input.BlackTargets[origialB].Contains(predId) && predId != targetId)
                    //     {
                    //         isReplay = false;
                    //         BlackNum += 1;
                    //     }
                    //     else if (targetId == predId)
                    //     {
                    //         replayReward = 1;
                    //         PosNum += 1;
                    //     }
                    //     else
                    //     {
                    //         replayReward = 0;
                    //         NegNum += 1;
                    //     }
                    // }
                    // else
                    // {
                    //     if (Input.BlackTargets[origialB].Contains(predId))
                    //     {
                    //         replayReward = 1;
                    //         PosNum += 1;
                    //     }
                    //     else  
                    //     {
                    //         replayReward = 0;
                    //         NegNum += 1;
                    //     }
                    // }
 
                    //Tuple<int, int, int, int>[] visitPath = st.GetPath(b);
                    int[] armPath = st.GetArmPath(b);

                    if(ActMemory != null)
                    {
                        //int[] relPath = st.GetRelPath(b);
                        if(Behavior.RunMode == DNNRunMode.Train)
                        {
                            ActMemory.UpdateMem(raw_b, armPath, replayReward <= 0 ? 0 : replayReward, 1);
                            StatusData cst = st;
                            for(int pt = t-1; pt > 0; pt--)
                            {
                                int pb = cst.GetPreStatusIndex(b);
                                StatusData pst = Input.StatusPath[pt];

                                int nPredId = pst.NodeID[pb];
                                float r = EvalUtil.TrueReward(nPredId, targetId, Input.BlackTargets[origialB]);
                                int[] nArmPath = cst.GetArmPath(pb);
                                ActMemory.UpdateMem(raw_b, nArmPath, r <= 0? 0: r, 1);
                                b = pb;
                                cst = pst;
                            }
                        }
                        else
                        {
                            float est_r =  Util.Logistic(st.Score.Output[b]);
                            ActMemory.UpdateMem(raw_b, armPath, est_r, 1);
                            StatusData cst = st;
                            for(int pt = t-1; pt > 0; pt--)
                            {
                                int pb = cst.GetPreStatusIndex(b);
                                StatusData pst = Input.StatusPath[pt];

                                int nPredId = pst.NodeID[pb];
                                float r = Util.Logistic(pst.Score.Output[pb]);
                                int[] nArmPath = pst.GetArmPath(pb);
                                ActMemory.UpdateMem(raw_b, nArmPath, r <= 0 ? 0: r, 1);
                                b = pb;
                                cst = pst;
                            }
                        }
                    }
                    if(Memory != null)
                    {
                        Memory.AddReplayBuff(raw_b, raw_src, raw_rel, targetId, armPath, replayReward <= 0 ? 0 : replayReward); 
                    }
                    SmpNum += 1;
                }
                PerfCounter.Manager.Instance["replayRollRunner"].TakeCount(perf_replayRoll);
            }
        }
}