using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
	public class RollBackRunner : StructRunner
    	{
            new GraphQueryData Input { get; set; }
            EpisodicMemory Memory { get; set; }

            int PosNum = 0;
            int NegNum = 0;
            int BlackNum = 0;
            float V = 0;
            int SmpNum = 0;
            int Epoch = 0;
            
            public float PosReward = 1;
            public float NegReward = -1;
            public override void Init()
            {
                SmpNum = 0;
                PosNum = 0;
                NegNum = 0;
                BlackNum = 0;
                V = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                //if(Behavior.RunMode == DNNRunMode.Train)
                { 
                    Memory.UpdateTiming();
                }

                if(Epoch % ReportPerEpoch == 0)
                {
                    ReportNum();
                }
            }

            public void ReportNum()
            {
                Logger.WriteLog("Sample Num {0}", SmpNum);
                Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosNum / (SmpNum + 0.000001f));
                Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegNum / (SmpNum + 0.000001f));
                Logger.WriteLog("Black Num {0}, Black Mean {1}", BlackNum, BlackNum / (SmpNum + 0.000001f));
                Logger.WriteLog("V Score {0}, V Mean {1}", V, V / (SmpNum + 0.000001f));
                   
            }

            public RollBackRunner(GraphQueryData input, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                StatusData st = Input.StatusPath.Last();
                int t = st.Step; 

                st.Score.Output.SyncToCPU();

                for (int i = 0; i < st.BatchSize; i++)
                {
                    int b = i; 
                    int origialB = st.GetOriginalStatsIndex(b);

                    int predId = st.NodeID[b];

                    int targetId = Input.RawTarget[origialB];
                    int raw_b = Input.RawIndex[origialB];
                    int raw_src = Input.RawSource[origialB];
                    int raw_rel = Input.RawRel[origialB];

                    bool isReplay = true;
                    float mctsReward = (float)Math.Tanh(st.Score.Output[i]); // Util.Logistic(st.Score.Output.MemPtr[i]);
                    float replayReward = 0;

                    if (Input.BlackTargets[origialB].Contains(predId) && predId != targetId)
                    {
                        BlackNum += 1;
                        isReplay = false;
                    }
                    else if (targetId == predId)
                    {
                        replayReward = PosReward;
                        PosNum += 1;
                        isReplay = true;
                    }
                    else
                    {
                        replayReward = NegReward;
                        NegNum += 1;
                        isReplay = true;
                    }
                    V += mctsReward;

                    int[] visitPath = st.GetArmPath(b);
                    
                    //Tuple<int[], int[]> visitPath = st.GetSimplePath(b);
                    //Tuple<int, int, int, int>[] visitPath = st.GetPath(b);
                    //Memory.UpdateMem(raw_b, raw_src, raw_rel, visitPath, mctsReward);
                    if(isReplay && Behavior.RunMode == DNNRunMode.Train)
                    {
                        Memory.AddReplayBuff(raw_b, raw_src, raw_rel, targetId, visitPath, replayReward); 
                    }
                    SmpNum += 1;
                }
            }
        }
}