using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
	/// <summary>
        /// Sample Graph in Memory. 
        /// </summary>
        class ReplayMemRunner : StructRunner
        {
            EpisodicMemory Mem { get; set; }
            DataEnviroument Data { get; set; }
            public new GraphQueryData Output { get; set; }
            
            int MaxBatchSize { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1384);
            int MiniBatchCounter = 0;
            int DebugSamples = 0;
            public ReplayMemRunner(EpisodicMemory mem, DataEnviroument data, int groupSize, int debugSamples, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
            	Mem = mem;
                Data = data;

                MaxBatchSize = groupSize;
                DebugSamples = debugSamples; 
                
                Output = new GraphQueryData(MaxBatchSize, behavior.Device);
                
                Output.GuidPath = new List<int[]>();
                Output.GuidReward = new List<float>();
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                MiniBatchCounter = 0;
            }

            public override void Forward()
            {
                Output.Clear();

                MiniBatchCounter += 1;

                if (DebugSamples > 0 && MiniBatchCounter > DebugSamples) { IsTerminate = true; return; }

                int groupIdx = 0;
                while (groupIdx < MaxBatchSize)
                {
                    Tuple<int, int, int, int, int[], float> smp = Mem.SampleReplayData();

                    int idx = smp.Item1;    
                    int srcId = smp.Item2; 
                    int linkId = smp.Item3;
                    int tgtId = smp.Item4;
                    int[] path = smp.Item5;
                    float reward = smp.Item6;
                    List<int> truthIds = Data.TruthDict[Data.GraphVoc.Key(srcId, linkId)];
                    {
                        Output.Push(idx, srcId, linkId, tgtId, truthIds, Data.GraphVoc.START_IDX);

                        Output.GuidPath.Add(path);
                        
                        Output.GuidReward.Add(reward); //.Add(reward);
                    }
                    groupIdx += 1;
                }
                Output.PushDone();
                //Output.GuidReward.EffectiveSize = groupIdx;
                //Output.GuidReward.SyncFromCPU();
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }
}