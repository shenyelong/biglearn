

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class BeamSearchRunner : BasicPolicyRunner
        {
            int BeamSize = 1;

            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchRunner(StatusData input, int maxNeighbor, int beamSize, RunnerBehavior behavior) : base(input, behavior)
            {
                BeamSize = beamSize;

                MatchPath = new BiMatchBatchData(
                //    new BiMatchBatchDataStat()
                //{
                //    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                //    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                //    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                //}, 
                    Input.GraphQuery.MaxBatchSize * BeamSize,
                    Input.MaxBatchSize,
                    Input.MaxBatchSize * maxNeighbor,
                    behavior.Device);
            }

            public override void Forward()
            {
                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();

                NodeIndex.Clear();
                LogProb.Clear();
                ActIndex.Clear();
                StatusIndex.Clear();
                MatchPath.Clear();

                int cursor = 0;

                for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                {
                    List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);
                    MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);
                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx[b - 1];
                        {
                            for (int t = s; t < e; t++)
                            {
                                float tmp_prob = Input.GetLogProb(b) + (float)Math.Log(Input.MatchCandidateProb.Output[t]);
                                topKheap.push_pair(new Tuple<int, int>(b, t), tmp_prob);
                            }
                        }
                    }

                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();

                        NodeIndex.Add(Input.MatchCandidate.Item1[p.Key.Item2]);
                        LogProb.Add(p.Value);

                        StatusIndex.Add(p.Key.Item1);
                        ActIndex.Add(p.Key.Item2);
                        MatchPath.Push(p.Key.Item1, p.Key.Item2, p.Value);
                        
                        cursor += 1;
                    }
                }
                MatchPath.PushDone();
            }
        }
}