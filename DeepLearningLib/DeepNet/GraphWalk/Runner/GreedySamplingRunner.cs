using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class GreedySamplingRunner : BasicPolicyRunner
        {
             /// <summary>
            /// match candidate, and match probability. randomizer should be in through outside.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public GreedySamplingRunner(StatusData input, int maxNeighbor, RunnerBehavior behavior) : base(input, behavior)
            {
                MatchPath = new BiMatchBatchData(
                    //new BiMatchBatchDataStat()
                //{
                //    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                //    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                //    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                //}, 
                    Input.MaxBatchSize,
                    Input.MaxBatchSize,
                    Input.MaxBatchSize * maxNeighbor,
                    behavior.Device);
            }

            public unsafe override void Forward()
            {
                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();
                //Input.MatchCandidateProb.SegmentIdx.SyncToCPU();
                NodeIndex.Clear();
                LogProb.Clear();
                ActIndex.Clear();
                StatusIndex.Clear();
                MatchPath.Clear();

                //if(Input.MatchCandidate == null)
                //{
                //    Input.MatchCandidate_Node.SyncToCPU();
                //}

                int currentStep = Input.Step;
                int batchSize = Input.MatchCandidateProb.Segment;
                for (int i = 0; i < batchSize; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx[i - 1];

                    int actionDim = e - s;
                    
                    if(actionDim == 0)
                    {
                        throw new Exception("actionDim is zero, it is not acceptable.");
                    }

                    //float[] prior_r = new float[actionDim];
                    //Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                    int selectIdx = Util.MaximumValue(Input.MatchCandidateProb.Output.CpuPtr, s, e);

                    

                    float prob = Input.GetLogProb(i) + (float)Math.Log(Input.MatchCandidateProb.Output[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                    {
                        ActIndex.Add(selectIdx);
                        StatusIndex.Add(i);

                        int nidx = Input.MatchCandidate != null ? Input.MatchCandidate.Item1[selectIdx] :
                                                                Input.MatchCandidate_Node[selectIdx];

                        NodeIndex.Add(nidx);
                        LogProb.Add(prob);
                        MatchPath.Push(i, selectIdx, 1);
                    }
                }
                MatchPath.PushDone();
            }
        }
}
