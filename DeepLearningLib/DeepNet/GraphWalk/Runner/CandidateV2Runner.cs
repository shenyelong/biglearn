using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        class CandidateV2Runner : CompositeNetRunner
        {
            public new StatusData Input;
            GraphEnviroument Graph;

            //public BiMatchBatchData Match;
            
            public CudaPieceInt SrcIdxs = null;

            public CudaPieceInt LinkIdxs = null;
            public CudaPieceInt LinkMargin = null;
            public CudaPieceInt LinkNodes = null;
            public CudaPieceInt LinkRels = null;
            public int MaskType { get; set; }

            // masktype = 0 : mask single connection. (src, rel, tgt); masktype = 1 : mask multiple connections (src, rel, *);
            public CandidateV2Runner(StatusData input, GraphEnviroument graph, int maskType, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                Graph = graph;
                MaskType = maskType;
                int maxMatchSize = Input.MaxBatchSize * Graph.MaxNeighborNum;
                
                SrcIdxs = new CudaPieceInt(Input.MaxBatchSize, behavior.Device);

                LinkIdxs = new CudaPieceInt(Input.MaxBatchSize, behavior.Device);
                LinkMargin = new CudaPieceInt(maxMatchSize, behavior.Device);
                LinkNodes = new CudaPieceInt(maxMatchSize, behavior.Device);
                LinkRels = new CudaPieceInt(maxMatchSize, behavior.Device);
            }

            public override void Forward()
            {
                var perf_candidate = PerfCounter.Manager.Instance["candidateRunner_forward"].Begin();

                SrcIdxs.EffectiveSize = Input.BatchSize;
                for(int b = 0; b < Input.BatchSize; b++)
                {
                    SrcIdxs.MemPtr[b] = Input.NodeID[b];
                }
                SrcIdxs.SyncFromCPU();

                if(Graph.LinkIdx == null || Graph.LinkNodes == null || Graph.LinkRels == null)
                {
                    throw new Exception("Graph information is null");
                }
                LinkIdxs.EffectiveSize = Input.BatchSize;
                ComputeLib.GraphNeighborNum(SrcIdxs, Input.BatchSize, Graph.LinkIdx, Graph.LinkNodes, Graph.LinkRels, 
                                Input.GraphQuery.RawSourceInts, Input.GraphQuery.RawRelInts, Input.GraphQuery.RawInvRelInts, Input.GraphQuery.RawTargetInts, 
                                LinkIdxs, MaskType);

                LinkIdxs.SyncToCPU();
                int acc = 0;
                for(int b = 0; b < Input.BatchSize; b++)
                {
                    //if(LinkIdxs.MemPtr[b] == 1) //{ //}
                    LinkIdxs.MemPtr[b] = acc + LinkIdxs.MemPtr[b];
                    acc = LinkIdxs.MemPtr[b];
                }
                LinkIdxs.SyncFromCPU();

                LinkNodes.EffectiveSize = acc;
                LinkRels.EffectiveSize = acc;
                LinkMargin.EffectiveSize = acc;
                ComputeLib.GraphNeighborInfo(SrcIdxs, Input.BatchSize, Graph.LinkIdx, Graph.LinkNodes, Graph.LinkRels, 
                                Input.GraphQuery.RawSourceInts, Input.GraphQuery.RawRelInts, Input.GraphQuery.RawInvRelInts, Input.GraphQuery.RawTargetInts, 
                                LinkIdxs, LinkNodes, LinkRels, LinkMargin, Graph.GraphVoc.END_IDX, MaskType);

                LinkNodes.SyncToCPU();
                LinkRels.SyncToCPU();
                // int cursor = 0;
                // for (int b = 0; b < Input.BatchSize; b++)
                // {
                //     // current node.
                //     int seedNode = Input.NodeID[b];

                //     // query idx. 
                //     int qid = Input.GetOriginalStatsIndex(b);

                //     // raw node.
                //     int rawN = Input.GraphQuery.RawSource[qid];

                //     // raw relation.
                //     int rawR = Input.GraphQuery.RawRel[qid];

                //     // raw answer.
                //     int answerN = Input.GraphQuery.RawTarget[qid];

                //     int candidateNum = 0;

                //     //&& !IsLast
                //     //if (Graph.Graph.ContainsKey(seedNode) )
                //     {
                //         for (int nei = 0; nei < Graph.Graph[seedNode].Count; nei++)
                //         {
                //             int lid = Graph.Graph[seedNode][nei].Item1;
                //             int tgtNode = Graph.Graph[seedNode][nei].Item2;

                //             // filter the existing relation links.
                //             if(FilterR)
                //             {
                //                 //&& tgtNode == answerN
                //                 if (seedNode == rawN && lid == rawR) { continue; }
                //                 //seedNode == answerN && 
                //                 if (tgtNode == rawN && lid == Graph.GraphVoc.ReverseRelationTable[rawR]) { continue; }
                //             }
                //             else
                //             {
                //                 if (seedNode == rawN && lid == rawR && tgtNode == answerN) { continue; }
                //                 if (tgtNode == rawN && seedNode == answerN && lid == Graph.GraphVoc.ReverseRelationTable[rawR]) { continue; }
                //             }
                //             Output.Item1.Add(tgtNode);
                //             Output.Item2.Add(lid); 
                        
                //             Match.Push(b, cursor, 1);
                //             cursor += 1;
                //             candidateNum += 1;
                //         }
                //     }
                //     // add end action for each node.
                //     {
                //         Output.Item1.Add(seedNode);
                //         Output.Item2.Add(Graph.GraphVoc.END_IDX); 

                //         Match.Push(b, cursor, 1);
                //         cursor += 1;
                //         candidateNum += 1;
                //     }
                // }

                // var perf_candidate_match = PerfCounter.Manager.Instance["candidateRunner_match_forward"].Begin();
                // Match.PushDone();
                // PerfCounter.Manager.Instance["candidateRunner_match_forward"].TakeCount(perf_candidate_match);

                PerfCounter.Manager.Instance["candidateRunner_forward"].TakeCount(perf_candidate);
            }
        }
    
}
