

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class MCTSPolicyV2Runner : BasicPolicyRunner
        {
            MCTSMemory Memory { get; set; }

            float Alpha_C { get; set; }
            float Alpha_D { get; set; }

            Random mrandom { get; set; }
            RateScheduler Epsilon { get; set; }
            Random[] ThreadRandoms { get; set; }

            float[] TmpQ { get; set; }
            /// <summary>
            /// match candidate, and match probability. 
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSPolicyV2Runner(StatusData input, int maxNeighbor, MCTSMemory actMem,
            						float alpha_c, 
            						float alpha_d, 
                                    Random randomizer, 
                                    RateScheduler epsilon, 
            						RunnerBehavior behavior) : base(input, behavior)
            {
                Memory = actMem;

                Alpha_C = alpha_c;
                Alpha_D = alpha_d;

                mrandom = randomizer;
                Epsilon = epsilon;
                MatchPath = new BiMatchBatchData(

                    //new BiMatchBatchDataStat()
                //{
                //    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                //    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                //    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                //}, 
                    Input.MaxBatchSize,
                    Input.MaxBatchSize,
                    Input.MaxBatchSize * maxNeighbor,
                    behavior.Device);

                ThreadRandoms = new Random[Input.MaxBatchSize];
                for(int i=0; i < Input.MaxBatchSize; i++)
                {
                    ThreadRandoms[i] = new Random(i + randomizer.Next());
                }
                TmpQ = new float[Input.MaxBatchSize * maxNeighbor];
            }

            public unsafe override void Forward()
            {
            	if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }
                var perf_mcs = PerfCounter.Manager.Instance["mctsSamplingV2Runner"].Begin();

                Input.MatchCandidateProb.Output.SyncToCPU();

                NodeIndex.Clear();
                ActIndex.Clear();
                StatusIndex.Clear();
                LogProb.Clear();
                MatchPath.Clear();
                
                //if(Input.MatchCandidate == null)
                //{
                //    Input.MatchCandidate_Node.SyncToCPU();
                //}

                int currentStep = Input.Step;
                int batchSize = Input.MatchCandidateProb.Segment;
                
                int[] actIndex = new int[batchSize];

                int THREAD_NUM = ParameterSetting.BasicMathLibThreadNum;
                int total = batchSize;
                int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
                
                Parallel.For(0, THREAD_NUM, thread_idx =>
                {
                    for (int t = 0; t < process_len; t++)
                    {
                        int i = thread_idx * process_len + t;
                        if( i >= total) continue;
                            //Parallel.For(0, batchSize, i =>
                        //{
                        int e = Input.MatchCandidateProb.SegmentIdx[i];
                        int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx[i - 1];

                        int actionDim = e - s;

                        int origialB = Input.GetOriginalStatsIndex(i);
                        int rawIdx = Input.GraphQuery.RawIndex[origialB];
                        int rawRel = Input.GraphQuery.RawRel[origialB];
                        //string Pkey = Input.GetPathKey(i);

                        int[] path = Input.GetArmPath(i); //GetRelPath(i);
                        //Tuple<int[], int[]> path = Input.GetSimplePath(i);
                        //Console.WriteLine("search raw idx {0}, key {1} ...", rawIdx, Pkey);
                        //rawIdx -> rawRel;
                        Tuple<float[], float[], float> arms = Memory == null ? null : Memory.Search(rawIdx, path);  
                        //Memory.Search(rawIdx, Pkey);
                        
                        float r = (float)ThreadRandoms[i].NextDouble();
                        
                        int idx = (Epsilon != null && Epsilon.AmIin(r)) ? ThreadRandoms[i].Next(0, actionDim)
                                            : BanditAlg.StocasticBandit(arms, 

                                                //Input.MatchCandidateProb.Output.MemPtr, 
                                                Input.MatchCandidateProb.Output.TakeCPU(0).ToArray(),
                                                TmpQ, 5, s, actionDim, Alpha_C, Alpha_D, ThreadRandoms[i]);

                                            //BanditAlg.StocasticBandit(arms, Input.MatchCandidate_Rel.MemPtr, Input.MatchCandidateProb.Output.MemPtr, 
                                            //    TmpQ, 5, s, actionDim, Alpha_C, Alpha_D, ThreadRandoms[i]);
                                            //BanditAlg.AlphaGoZeroBandit(arms, Input.MatchCandidate_Rel.MemPtr, Input.MatchCandidateProb.Output.MemPtr, s, actionDim, Alpha_C, Alpha_D); 
                        int selectIdx = s + idx;
                        actIndex[i] = selectIdx;
                    }
                });
                //     
                    
                //     if( arms != null && (actionDim != arms.Item1.Length || actionDim != arms.Item2.Length) ) 
                //     {    
                //         foreach(KeyValuePair<string, Tuple<float[], float[]>> tmpM in Memory.Mem[rawIdx])
                //         {
                //             Logger.WriteLog("key {0}, length {1}", tmpM.Key, tmpM.Value.Item1.Length);
                //         }
                //         throw new Exception(string.Format("MctsPolicyV2Runner: idx {0} actionDim {1} doesn't match MemActDim1: {2}, MemActDim2: {3}, Pkey: {4}",
                //                     rawIdx, actionDim, arms.Item1.Length, arms.Item2.Length, Pkey));
                //     }

                //     //float[] prior_r = new float[actionDim];
                //     //Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                //     float r = (float)ThreadRandoms[i].NextDouble();
                //     int idx = (Epsilon != null && Epsilon.AmIin(r)) ? ThreadRandoms[i].Next(0, actionDim)
                //                         :  BanditAlg.AlphaGoZeroBandit(arms, Input.MatchCandidateProb.Output.MemPtr, s, actionDim, Alpha_C, Alpha_D); 
                    
                //     int selectIdx = s + idx;
                    
                // });

                for (int i = 0; i < batchSize; i++)
                {
                    int selectIdx = actIndex[i];
                    float lp = (float)Math.Log(Input.MatchCandidateProb.Output[selectIdx]);
                    //total_smp_prob += Input.MatchCandidateProb.Output.MemPtr[selectIdx];
                    //total_smp += 1;
                    float prob = Input.GetLogProb(i) + lp; // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                    {
                        ActIndex.Add(selectIdx);
                        StatusIndex.Add(i);
                        int n = Input.MatchCandidate != null ? Input.MatchCandidate.Item1[selectIdx] :
                                                                Input.MatchCandidate_Node[selectIdx];
                        NodeIndex.Add(n);
                        LogProb.Add(prob);
                        MatchPath.Push(i, selectIdx, 1);
                    }
                }

                // for (int i = 0; i < batchSize; i++)
                // {
                //     int origialB = Input.GetOriginalStatsIndex(i);
                //     int rawIdx = Input.GraphQuery.RawIndex[origialB];

                //     int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                //     int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                //     string Pkey = Input.GetPathKey(i);
                //     Logger.WriteLog("search raw idx {0}, key {1}", rawIdx, Pkey);
                //     Tuple<float[], float[], float> arms = Memory.Search(rawIdx, Pkey);

                //     int actionDim = e - s;
                //     if(actionDim == 0)
                //     	throw new Exception(string.Format("MctsPolicyV2Runner: actionDim is zero, it is not acceptable. s : {0}, e: {1}", s, e));

                //     if( arms != null && (actionDim != arms.Item1.Length || actionDim != arms.Item2.Length) ) 
                //     {    
                //         foreach(KeyValuePair<string, Tuple<float[], float[]>> tmpM in Memory.Mem[rawIdx])
                //         {
                //             Logger.WriteLog("key {0}, length {1}", tmpM.Key, tmpM.Value.Item1.Length);
                //         }
                //     	throw new Exception(string.Format("MctsPolicyV2Runner: idx {0} actionDim {1} doesn't match MemActDim1: {2}, MemActDim2: {3}, Pkey: {4}",
                //                     rawIdx, actionDim, arms.Item1.Length, arms.Item2.Length, Pkey));
                //     }

                //     //float[] prior_r = new float[actionDim];
                //     //Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                //     float r = (float)mrandom.NextDouble();
                //     int idx = (Epsilon != null && Epsilon.AmIin(r)) ? mrandom.Next(0, actionDim)
                //                         :  BanditAlg.AlphaGoZeroBandit(arms, Input.MatchCandidateProb.Output.MemPtr, s, actionDim, Alpha_C, Alpha_D); 
                     
                //     int selectIdx = s + idx;
                //     float prob = Input.GetLogProb(i) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                //     {
                //         ActIndex.Add(selectIdx);
                //         StatusIndex.Add(i);
                //         int n = Input.MatchCandidate != null ? Input.MatchCandidate.Item1[selectIdx] :
                //                                                 Input.MatchCandidate_Node.MemPtr[selectIdx];

                //         NodeIndex.Add(n); //Input.MatchCandidate.Item1[selectIdx]);
                //         LogProb.Add(prob);
                //         MatchPath.Push(i, selectIdx, 1);
                //     }
                // }
                MatchPath.PushDone();

                PerfCounter.Manager.Instance["mctsSamplingV2Runner"].TakeCount(perf_mcs);
            }
        }
}