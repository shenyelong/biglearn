using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class ThompasSamplingRunner : BasicPolicyRunner
        {
            Random mrandom { get; set; }
            RateScheduler Epsilon { get; set; }
            Random[] ThreadRandoms { get; set; }

            float total_smp_prob = 0;
            float total_smp = 0;
            /// <summary>
            /// match candidate, and match probability. randomizer should be in through outside.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public ThompasSamplingRunner(StatusData input, int maxNeighbor, Random randomizer, RunnerBehavior behavior)
                : this(input, maxNeighbor, randomizer, null, behavior)
            { }

            public ThompasSamplingRunner(StatusData input, int maxNeighbor, Random randomizer, RateScheduler epsilon, RunnerBehavior behavior) : base(input, behavior)
            {
                mrandom = randomizer;
                Epsilon = epsilon;

                MatchPath = new BiMatchBatchData(
                //     new BiMatchBatchDataStat()
                // {
                //     MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                //     MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                //     MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                // }, 
                    Input.MaxBatchSize,
                    Input.MaxBatchSize,
                    Input.MaxBatchSize * maxNeighbor,
                    behavior.Device);

                ThreadRandoms = new Random[Input.MaxBatchSize];
                for(int i=0; i < Input.MaxBatchSize; i++)
                {
                    ThreadRandoms[i] = new Random(i + randomizer.Next());
                }
            }

            public override void Init()
            {
                total_smp = 0;
                total_smp_prob = 0;
            }

            public override void Complete()
            {
                iteration += 1;
                if(iteration % ReportPerEpoch == 0)
                {
                    Logger.WriteLog("Average ts sample Prob {0}", (total_smp_prob / total_smp));
                }
            }

            public unsafe override void Forward()
            {
                var perf_ts = PerfCounter.Manager.Instance["thompasSamplingRunner"].Begin();

                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }
                int batchSize = Input.MatchCandidateProb.Segment;

                Input.MatchCandidateProb.Output.SyncToCPU();
                //Input.MatchCandidateProb.SegmentIdx.SyncToCPU();
                                
                int[] actIndex = new int[batchSize];
                Parallel.For(0, batchSize, i =>
                {
                    int e = Input.MatchCandidateProb.SegmentIdx[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx[i - 1];

                    int actionDim = e - s;
                    
                    if(actionDim == 0)
                    {
                        throw new Exception("actionDim is zero, it is not acceptable.");
                    }
                    float r = (float)ThreadRandoms[i].NextDouble();
                    int idx = (Epsilon != null && Epsilon.AmIin(r)) ? ThreadRandoms[i].Next(0, actionDim) : 
                    Util.Sample(Input.MatchCandidateProb.Output.CpuPtr, s, actionDim, ThreadRandoms[i]);
                    int selectIdx = s + idx;
                    actIndex[i] = selectIdx;
                });

                NodeIndex.Clear();
                LogProb.Clear();
                ActIndex.Clear();
                StatusIndex.Clear();
                MatchPath.Clear();

                for (int i = 0; i < batchSize; i++)
                {
                    int selectIdx = actIndex[i];
                    float lp = (float)Math.Log(Input.MatchCandidateProb.Output[selectIdx]);
                    total_smp_prob += Input.MatchCandidateProb.Output[selectIdx];
                    total_smp += 1;
                    float prob = Input.GetLogProb(i) + lp; 
                    {
                        ActIndex.Add(selectIdx);
                        StatusIndex.Add(i);
                        int n = Input.MatchCandidate != null ? Input.MatchCandidate.Item1[selectIdx] :
                                                                Input.MatchCandidate_Node.MemPtr[selectIdx];
                        NodeIndex.Add(n);
                        LogProb.Add(prob);
                        MatchPath.Push(i, selectIdx, 1);
                    }
                }
                MatchPath.PushDone();

                PerfCounter.Manager.Instance["thompasSamplingRunner"].TakeCount(perf_ts);
            }
        }
}
