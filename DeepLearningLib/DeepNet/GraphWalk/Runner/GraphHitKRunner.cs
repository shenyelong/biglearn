
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
	public class GraphHitKRunner : StructRunner
    {
            GraphQueryData Query { get; set; }
            
            public static int Epoch = 0;
            
            DataEnviroument Data = null;

            // 0 : probabolity; 1 : score; 2: path score;
            int ScoreType { get; set; }
            
            public GraphHitKRunner(GraphQueryData query, DataEnviroument data, int scoreType, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Epoch = 0;
                ScoreType = scoreType;
                Data = data;
            }

            // public override void Init()
            // {
            //     if(!IsExternalR) Result.Clear();
            // }

            public override void Complete()
            {
                Epoch += 1;
            	if(Epoch % ReportPerEpoch == 0)
                {
                    Logger.WriteLog("Report in Epoch {0}", Epoch);
                    Report(Data);
                }
            }

            public static void Report(DataEnviroument data)
            {
                data.Eval = data.HitK(false);
            	foreach(KeyValuePair<string, float> p in data.Eval)
                {
                	Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / data.TripleNum);
                }
                //return r;
            }

            public override void Forward()
            {
                if(ScoreType == 0)
                {
                    EvalUtil.AggregrateProb(Query, Data);
                }
                else if(ScoreType == 1)
                {
                    EvalUtil.AggregrateScore(Query, Data);
                }
                else if(ScoreType == 2)
                {
                    EvalUtil.AggregratePathScore(Query, Data);
                }
            }
        }
 }