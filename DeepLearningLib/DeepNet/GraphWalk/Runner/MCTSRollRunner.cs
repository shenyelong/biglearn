using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
	public class MCTSRollRunner : StructRunner
    	{
            new GraphQueryData Input { get; set; }
            MCTSMemory ActMemory { get; set; }
            int PosNum = 0;
            int NegNum = 0;
            int BlackNum = 0;

            int SmpNum = 0;
            int Epoch = 0;
            
            public override void Init()
            {
                SmpNum = 0;
                PosNum = 0;
                NegNum = 0;
                BlackNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                if(Epoch % ReportPerEpoch == 0)
                {
                    ReportNum();
                }
            }

            public void ReportNum()
            {
                Logger.WriteLog("Sample Num {0}", SmpNum);
                Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosNum / (SmpNum + 0.000001f));
                Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegNum / (SmpNum + 0.000001f));
                Logger.WriteLog("Black Num {0}, Black Mean {1}", BlackNum, BlackNum / (SmpNum + 0.000001f));
            }

            public MCTSRollRunner(GraphQueryData input, MCTSMemory actMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                ActMemory = actMem;
            }

            public override void Forward()
            {
                var perf_replayRoll = PerfCounter.Manager.Instance["mctsRollRunner"].Begin();
                
                for (int i = 0; i < Input.StatusPath.Count ; i++)
                    {
                        StatusData cst = Input.StatusPath[i];
                        if(cst.Score != null) cst.Score.Output.SyncToCPU();
                    }

                StatusData st = Input.StatusPath.Last();
                int t = st.Step; 


                for (int i = 0; i < st.BatchSize; i++)
                {
                    int b = i; 
                    int origialB = st.GetOriginalStatsIndex(b);
                    int raw_b = Input.RawIndex[origialB];

                    ActMemory.PreUpdateDicts(raw_b);
                }
                SmpNum += st.BatchSize;
                //for (int i = 0; i < st.BatchSize; i++)
                int THREAD_NUM = ParameterSetting.BasicMathLibThreadNum;
                int total = st.BatchSize;
                int process_len = (total + THREAD_NUM - 1) / THREAD_NUM;
                
                Parallel.For(0, THREAD_NUM, thread_idx =>
                {
                    for (int thread_t = 0; thread_t < process_len; thread_t++)
                    {
                        int i = thread_idx * process_len + thread_t;
                        if( i >= total) continue;

                        int b = i; 
                        int origialB = st.GetOriginalStatsIndex(b);

                        int predId = st.NodeID[b];

                        int targetId = Input.RawTarget[origialB];
                        int raw_b = Input.RawIndex[origialB];
                        float score = Util.Logistic(st.Score.Output[b]);

                        //float MCTSReward(float score, int predId, int tgtId, List<int> blackList)
                        float mctsReward = EvalUtil.MCTSReward(score, predId, targetId, Input.BlackTargets[origialB]);

                        int[] armPath = st.GetArmPath(b);
 
                        ActMemory.PostUpdateMem(raw_b, armPath, mctsReward, 1);
                        StatusData cst = st;
                        for(int pt = t-1; pt > 0; pt--)
                        {
                            int pb = cst.GetPreStatusIndex(b);
                            StatusData pst = Input.StatusPath[pt];

                            int nPredId = pst.NodeID[pb];
                            float r = Util.Logistic(pst.Score.Output[pb]);
                            int[] nArmPath = pst.GetArmPath(pb);

                            float nMctsReward = EvalUtil.MCTSReward(r, nPredId, targetId, Input.BlackTargets[origialB]);

                            ActMemory.PostUpdateMem(raw_b, nArmPath, nMctsReward, 1);
                            
                            b = pb;
                            cst = pst;
                        }
                    }                    
                });


                // for (int i = 0; i < st.BatchSize; i++)
                // {
                //     int b = i; 
                //     int origialB = st.GetOriginalStatsIndex(b);

                //     int predId = st.NodeID[b];

                //     int targetId = Input.RawTarget[origialB];
                //     int raw_b = Input.RawIndex[origialB];
                //     int raw_src = Input.RawSource[origialB];
                //     int raw_rel = Input.RawRel[origialB];
                //     float score = Util.Logistic(st.Score.Output.MemPtr[b]);

                //     //float MCTSReward(float score, int predId, int tgtId, List<int> blackList)
                //     float mctsReward = EvalUtil.MCTSReward(score, predId, targetId, Input.BlackTargets[origialB]);

                //     int[] armPath = st.GetArmPath(b);

                //     {
                //         {
                //             ActMemory.UpdateMem(raw_b, armPath, mctsReward, 1);
                //             StatusData cst = st;
                //             for(int pt = t-1; pt > 0; pt--)
                //             {
                //                 int pb = cst.GetPreStatusIndex(b);
                //                 StatusData pst = Input.StatusPath[pt];

                //                 int nPredId = pst.NodeID[pb];
                //                 float r = Util.Logistic(pst.Score.Output.MemPtr[pb]);
                //                 int[] nArmPath = pst.GetArmPath(pb);

                //                 float nMctsReward = EvalUtil.MCTSReward(r, nPredId, targetId, Input.BlackTargets[origialB]);

                //                 ActMemory.UpdateMem(raw_b, nArmPath, nMctsReward, 1);
                                
                //                 b = pb;
                //                 cst = pst;
                //             }
                //         }
                        
                //     }
                    
                //     SmpNum += 1;
                // }
                PerfCounter.Manager.Instance["mctsRollRunner"].TakeCount(perf_replayRoll);
            }
        }
}