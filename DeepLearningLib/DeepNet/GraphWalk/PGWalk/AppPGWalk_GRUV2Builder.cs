using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
namespace BigLearn.DeepNet.Graph
{
    public class AppPGWalk_GRUV2Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int RNN_Dim { get { return int.Parse(Argument["RNN-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }


            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int MIN_HOP { get { return int.Parse(Argument["MIN-HOP"].Value); } }

            public static int SCORE_TYPE { get { return int.Parse(Argument["SCORE-TYPE"].Value); } }
            public static int ROLL_NUM { get { return int.Parse(Argument["ROLL-NUM"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }
            public static float REWARD_MCTS_DISCOUNT { get { return float.Parse(Argument["REWARD-MCTS-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int TEST_MCTS_NUM { get { return int.Parse(Argument["TEST-MCTS-NUM"].Value); } }

            public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static float MSE_LAMBDA { get { return float.Parse(Argument["MSE-LAMBDA"].Value); } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static List<Tuple<int, float>> Reward_Feedback
            {
                get
                {
                    return Argument["R-FB"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static float Epsilon(int step)
            {
                for (int i = 0; i < Reward_Feedback.Count; i++)
                {
                    if (step < Reward_Feedback[i].Item1)
                    {
                        float lambda = (step - Reward_Feedback[i - 1].Item1) * 1.0f / (Reward_Feedback[i].Item1 - Reward_Feedback[i - 1].Item1);
                        return lambda * Reward_Feedback[i].Item2 + (1 - lambda) * Reward_Feedback[i - 1].Item2;
                    }
                }
                return Reward_Feedback.Last().Item2;
            }

            public static int EStrategy(int mcts_idx)
            {
                string[] idxs = Argument["MCTS-EXP"].Value.Split(',').ToArray();
                for (int i = 0; i < idxs.Length; i++)
                {
                    if (mcts_idx < int.Parse(idxs[i]))
                    {
                        return i;
                    }
                }
                return idxs.Length - 1;
            }

            public static float UPDATE_R_DISCOUNT { get { return float.Parse(Argument["UPDATE-R-DISCOUNT"].Value); } }

            public static float BASE_LAMBDA { get { return float.Parse(Argument["BASE-LAMBDA"].Value); } }
            public static float PROB_LAMBDA { get { return float.Parse(Argument["PROB-LAMBDA"].Value); } }
            public static float SCORE_LAMBDA { get { return float.Parse(Argument["SCORE-LAMBDA"].Value); } }

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }

            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int AGG_SCORE { get { return int.Parse(Argument["AGG-SCORE"].Value); } }

            public static int MAP_EVAL { get { return int.Parse(Argument["MAP-EVAL"].Value); } }

            public static float BETA { get { return float.Parse(Argument["BETA"].Value); } }
            public static float ALPHA_1 { get { return float.Parse(Argument["ALPHA-1"].Value); } }
            public static float ALPHA_2 { get { return float.Parse(Argument["ALPHA-2"].Value); } }

            public static float TRAIN_C { get { return float.Parse(Argument["TRAIN-C"].Value); } }
            public static float TRAIN_L { get { return float.Parse(Argument["TRAIN-L"].Value); } }
            public static float TEST_C { get { return float.Parse(Argument["TEST-C"].Value); } }
            public static float TEST_L { get { return float.Parse(Argument["TEST-L"].Value); } }

            public static int TRAIN_STRATEGY { get { return int.Parse(Argument["TRAIN-STR"].Value); } }

            public static int TRAIN_SAMPLES { get { return int.Parse(Argument["TRAIN-SAMPLES"].Value); } }
            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int STEP_FILTER { get { return int.Parse(Argument["STEP-FILTER"].Value); } }

            public static int DEV_AS_TRAIN { get { return int.Parse(Argument["DEV-TRAIN"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("RNN-DIM", new ParameterArgument("100", "RNN hidden state dim."));

                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));
                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));


                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
                Argument.Add("MIN-HOP", new ParameterArgument("2", "minimum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));

                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                Argument.Add("MCTS-NUM", new ParameterArgument("64", "Monto Carlo Tree Search Number"));

                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("ALPHA-1", new ParameterArgument("0","max entropy for termination gate."));
                Argument.Add("ALPHA-2", new ParameterArgument("0", "max entropy for action gate."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_PGWALK_GRUV2; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure NodeEmbed { get; set; }
            public EmbedStructure RelEmbed { get; set; }

            public DNNStructure SrcDNN { get; set; }
            public GRUCellV2 GruCell { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeDim, int relDim, int rnnDim, DeviceType device)
            {
                Logger.WriteLog("Node Num {0} Embed {1}", entityNum, nodeDim);
                Logger.WriteLog("Relation Num {0} Embed {1}", relationNum, relDim);
                
                NodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                RelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

                int embedDim = nodeDim + relDim;

                //GruCell = AddLayer(new GRUCell(StateDim + BuilderParameters.DNN_DIMS.Last(), StateDim, device));
                GruCell = AddLayer(new GRUCellV2(embedDim, rnnDim, device));

                int inputDim = embedDim + relDim;

                List<int> mapLayers = new List<int>(BuilderParameters.DNN_DIMS);
                mapLayers.Add(embedDim);

                SrcDNN = AddLayer(new DNNStructure(inputDim, mapLayers.ToArray(),
                                           mapLayers.Select(i => A_Func.Tanh).ToArray(),
                                           mapLayers.Select(i => true).ToArray(),
                                           device));
            }
            // public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

            //     NodeEmbed = DeserializeModel<EmbedStructure>(reader, device); // DeviceType.CPU_FAST_VECTOR);
            //     RelEmbed =  DeserializeModel<EmbedStructure>(reader, device); // DeviceType.CPU_FAST_VECTOR);
            //     GruCell = DeserializeModel<GRUCellV2>(reader, device);
            //     SrcDNN = DeserializeModel<DNNStructure>(reader, device);
            // }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }
        
        static Random SampleRandom = new Random(DeepNet.BuilderParameters.RandomSeed + 101);

        public static ComputationGraph BuildComputationGraph(DataEnviroument data, GraphEnviroument graph, 
            int batchSize, int roll, int debugSamples, int beamSize, int policyId,
            int maxHop, NeuralWalkerModel model, RunnerBehavior Behavior) 
        {
            ComputationGraph cg = new ComputationGraph(Behavior);

            // sample a list of tuplet from the graph.
            SampleRunner SmpRunner = new SampleRunner(data, batchSize, roll, debugSamples, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            //HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, model.NodeEmbed, Behavior));
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(interface_data.RawRel, interface_data.MaxBatchSize, model.RelEmbed, Behavior));

            IntArgument batchSizeArg = new IntArgument("sample-batch-size"); 
            cg.AddRunner(new HiddenDataBatchSizeRunner(relEmbed, batchSizeArg, Behavior));

            //VectorExpanRunner startRunner = new VectorExpanRunner(DataPanel.START_IDX, batchSizeArg, graphQuery.MaxBatchSize, Behavior);
            //cg.AddRunner(startRunner);
            //HiddenBatchData startEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(startRunner.Ints, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));
            HiddenBatchData startEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(interface_data.RawStart, interface_data.MaxBatchSize, model.RelEmbed, Behavior));
            
            HiddenBatchData initZeroH0 = (HiddenBatchData)cg.AddRunner(new ZeroMatrixRunner(startEmbed.MAX_BATCHSIZE, model.GruCell.HiddenStateDim, batchSizeArg, Behavior));

            GRUStateV2Runner initStateRunner = new GRUStateV2Runner(model.GruCell, initZeroH0, startEmbed, Behavior);
            cg.AddRunner(initStateRunner);
            
            HiddenBatchData H1 = initStateRunner.Output;
            HiddenBatchData Rq = relEmbed;
            
            {
                StatusData status = new StatusData(interface_data, interface_data.RawSource, H1, Behavior.Device);

                #region multi-hop expansion
                for (int i = 0; i < maxHop; i++)
                {
                    CandidateRunner candidateActionRunner = new CandidateRunner(status, graph, Behavior);
                    cg.AddRunner(candidateActionRunner);
                    status.MatchCandidate = candidateActionRunner.Output;

                    HiddenBatchData candRelEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.Output.Item2, 
                                                                        candidateActionRunner.Match.MAX_MATCH_BATCHSIZE, 
                                                                        model.RelEmbed, Behavior));
                
                    HiddenBatchData I_data = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
                                            { status.StateEmbed, Rq }, Behavior));

                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, I_data, Behavior);
                    cg.AddRunner(srcHiddenRunner);
                    
                    MatrixInnerProductRunner innerRunner = new MatrixInnerProductRunner(new MatrixData(srcHiddenRunner.Output),
                                                                                        new MatrixData(candRelEmbed), candidateActionRunner.Match, Behavior);
                    cg.AddRunner(innerRunner);

                    status.MatchCandidateQ = new SeqVectorData(innerRunner.Output.MaxLength,
                                                           status.MaxBatchSize, innerRunner.Output.Output, innerRunner.Output.Deriv,
                                                           candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, Behavior.Device);

                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(status.MatchCandidateQ, 1, Behavior, true);
                    cg.AddRunner(normAttRunner);
                    status.MatchCandidateProb = normAttRunner.Output;

                    BasicPolicyRunner policyRunner = null;
                    if (policyId == 0)
                    {
                        policyRunner = new ThompasSamplingRunner(status, graph.MaxNeighborNum, SampleRandom, Behavior);
                    }
                    else if(policyId == 1)
                    {
                        policyRunner = new BeamSearchRunner(status, graph.MaxNeighborNum, beamSize, Behavior);
                    }
                    cg.AddRunner(policyRunner);

                        #region take action.
                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(srcExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candRelEmbed, policyRunner.MatchPath, 2, Behavior);
                        cg.AddRunner(tgtExpRunner);

                        MatrixExpansionRunner rQExpRunner = new MatrixExpansionRunner(Rq, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(rQExpRunner);
                        Rq = rQExpRunner.Output;

                        GRUStateV2Runner stateRunner = new GRUStateV2Runner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
                        cg.AddRunner(stateRunner);
                        #endregion.

                        StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                            policyRunner.ActIndex, policyRunner.StatusIndex, stateRunner.Output, Behavior.Device);
                        status = nextStatus;
                }
                #endregion.
            }

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                PolicyGradientRunner pgRunner = new PolicyGradientRunner(interface_data, BuilderParameters.ALPHA_2, BuilderParameters.REWARD_DISCOUNT, Behavior);
                cg.AddObjective(pgRunner);
            }
            else
            {
                GraphHitKRunner hitRunner = new GraphHitKRunner(interface_data, data, 0, Behavior);
                cg.AddRunner(hitRunner);
            }
            cg.SetDelegateModel(model);
            
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            GraphDataPanel.Init(BuilderParameters.InputDir, BuilderParameters.MAX_ACTION_NUM);
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model = new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device);

            //    BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
            //    new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
            //                          BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device) :
            //    new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            model.InitOptimization(behavior);

            ComputationGraph testCG = null;
            ComputationGraph validCG = null;

            {
                testCG = BuildComputationGraph(GraphDataPanel.TestData, GraphDataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1,  BuilderParameters.TEST_SAMPLES, BuilderParameters.BeamSize, 1,
                                                  BuilderParameters.MAX_HOP,  model, behavior);

                validCG = BuildComputationGraph(GraphDataPanel.DevData, GraphDataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1,  BuilderParameters.TEST_SAMPLES, BuilderParameters.BeamSize, 1,
                                                  BuilderParameters.MAX_HOP,  model, behavior);
            }
            
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(BuilderParameters.DEV_AS_TRAIN > 0 ? GraphDataPanel.DevData : GraphDataPanel.TrainData, GraphDataPanel.GraphEnv, BuilderParameters.MiniBatchSize,
                                                                     BuilderParameters.ROLL_NUM, BuilderParameters.TRAIN_SAMPLES, 0, 0,
                                                                     BuilderParameters.MAX_HOP, model, behavior);
                    
                    float best_dev_hit1 = 0;
                    Dictionary<string, float> test_results = null;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);
                            GraphDataPanel.TestData.ClearResults();
                            testCG.Execute();
                            GraphDataPanel.DevData.ClearResults();
                            validCG.Execute();

                            float score = GraphDataPanel.DevData.Eval["Hit1"];
                            if(score > best_dev_hit1)
                            {
                                best_dev_hit1 = score;
                                test_results = GraphDataPanel.TestData.Eval;
                            }

                            if(test_results != null)
                            {
                                Logger.WriteLog("test results until now ... ");
                                foreach(KeyValuePair<string, float> p in test_results)
                                {
                                    Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / GraphDataPanel.TestData.TripleNum);
                                }
                            }
                        }
                    }

                    Logger.WriteLog("Final test results ... ");
                    if(test_results != null)
                    {
                        foreach(KeyValuePair<string, float> p in test_results)
                        {
                            Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / GraphDataPanel.TestData.TripleNum);
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    testCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }
        
    }   
}
