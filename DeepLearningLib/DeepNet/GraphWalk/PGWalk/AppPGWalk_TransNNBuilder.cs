using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
namespace BigLearn.DeepNet.Graph
{
    public class AppPGWalk_TransNNBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            
            public static int RNN_Dim { get { return int.Parse(Argument["RNN-DIM"].Value); } }
            
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] DNN_DROPS { get { return Argument["DNN-DROPS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static int[] SCORE_DIMS { get { return Argument["SCORE-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] SCORE_DROPS { get { return Argument["SCORE-DROPS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int MIN_HOP { get { return int.Parse(Argument["MIN-HOP"].Value); } }

            public static int SCORE_TYPE { get { return int.Parse(Argument["SCORE-TYPE"].Value); } }
            public static int ROLL_NUM { get { return int.Parse(Argument["ROLL-NUM"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }
            public static float REWARD_MCTS_DISCOUNT { get { return float.Parse(Argument["REWARD-MCTS-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int TEST_MCTS_NUM { get { return int.Parse(Argument["TEST-MCTS-NUM"].Value); } }

            public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static float MSE_LAMBDA { get { return float.Parse(Argument["MSE-LAMBDA"].Value); } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static List<Tuple<int, float>> Reward_Feedback
            {
                get
                {
                    return Argument["R-FB"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static float Epsilon(int step)
            {
                for (int i = 0; i < Reward_Feedback.Count; i++)
                {
                    if (step < Reward_Feedback[i].Item1)
                    {
                        float lambda = (step - Reward_Feedback[i - 1].Item1) * 1.0f / (Reward_Feedback[i].Item1 - Reward_Feedback[i - 1].Item1);
                        return lambda * Reward_Feedback[i].Item2 + (1 - lambda) * Reward_Feedback[i - 1].Item2;
                    }
                }
                return Reward_Feedback.Last().Item2;
            }

            public static int EStrategy(int mcts_idx)
            {
                string[] idxs = Argument["MCTS-EXP"].Value.Split(',').ToArray();
                for (int i = 0; i < idxs.Length; i++)
                {
                    if (mcts_idx < int.Parse(idxs[i]))
                    {
                        return i;
                    }
                }
                return idxs.Length - 1;
            }

            public static float UPDATE_R_DISCOUNT { get { return float.Parse(Argument["UPDATE-R-DISCOUNT"].Value); } }

            public static float BASE_LAMBDA { get { return float.Parse(Argument["BASE-LAMBDA"].Value); } }
            public static float PROB_LAMBDA { get { return float.Parse(Argument["PROB-LAMBDA"].Value); } }
            public static float SCORE_LAMBDA { get { return float.Parse(Argument["SCORE-LAMBDA"].Value); } }

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }

            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int AGG_SCORE { get { return int.Parse(Argument["AGG-SCORE"].Value); } }

            public static int MAP_EVAL { get { return int.Parse(Argument["MAP-EVAL"].Value); } }

            public static float BETA { get { return float.Parse(Argument["BETA"].Value); } }
            public static float ALPHA_1 { get { return float.Parse(Argument["ALPHA-1"].Value); } }
            public static float ALPHA_2 { get { return float.Parse(Argument["ALPHA-2"].Value); } }

            public static float TRAIN_C { get { return float.Parse(Argument["TRAIN-C"].Value); } }
            public static float TRAIN_L { get { return float.Parse(Argument["TRAIN-L"].Value); } }
            public static float TEST_C { get { return float.Parse(Argument["TEST-C"].Value); } }
            public static float TEST_L { get { return float.Parse(Argument["TEST-L"].Value); } }

            public static int TRAIN_STRATEGY { get { return int.Parse(Argument["TRAIN-STR"].Value); } }

            public static int TRAIN_SAMPLES { get { return int.Parse(Argument["TRAIN-SAMPLES"].Value); } }
            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int STEP_FILTER { get { return int.Parse(Argument["STEP-FILTER"].Value); } }

            public static int DEV_AS_TRAIN { get { return int.Parse(Argument["DEV-TRAIN"].Value); } }

            public static float EntityEmdDrop { get { return float.Parse(Argument["ENTITY-EMD-DROP"].Value); } }
            public static float RelEmdDrop { get { return float.Parse(Argument["REL-EMD-DROP"].Value); } }

            public static float PG_ALPHA { get { return float.Parse(Argument["PG-ALPHA"].Value); } }
            public static int TEST_ROLL_NUM { get { return int.Parse(Argument["TEST-ROLL-NUM"].Value); } }
            public static int TEST_POOL { get { return int.Parse(Argument["TEST-POOL"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("RNN-DIM", new ParameterArgument("100", "RNN hidden state dim."));

                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                Argument.Add("DNN-DROPS", new ParameterArgument("0,0", "DNN Drop outs."));
                
                Argument.Add("SCORE-DIMS", new ParameterArgument("100,100", "Score DNN dimensions"));
                Argument.Add("SCORE-DROPS", new ParameterArgument("0,0", "Score DNN Dropouts"));
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF.")); 

                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
                Argument.Add("MIN-HOP", new ParameterArgument("2", "minimum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));

                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                Argument.Add("MCTS-NUM", new ParameterArgument("64", "Monto Carlo Tree Search Number"));

                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("ALPHA-1", new ParameterArgument("0","max entropy for termination gate."));
                Argument.Add("ALPHA-2", new ParameterArgument("0", "max entropy for action gate."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));

                Argument.Add("ENTITY-EMD-DROP", new ParameterArgument("0", "dropout rate for entity embedding."));
                Argument.Add("REL-EMD-DROP", new ParameterArgument("0","dropout rate for relation embedding."));

                Argument.Add("PG-ALPHA", new ParameterArgument("0.1f", "policy gradient weight(alpha)"));
                Argument.Add("TEST-ROLL-NUM", new ParameterArgument("1", "test roll number"));
                Argument.Add("TEST-POOL", new ParameterArgument("0", "0:max pooling; 1:average pooling;"));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_PGWALK_TRANSNN; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class SparseVectorRunner : StructRunner
        {
            List<List<int>> Labels { get; set; }
            int Dim { get; set; }
            int MaxBatchSize { get; set; }
            public SparseVectorData Vec { get; set; }
            public SparseVectorRunner(List<List<int>> labels, int maxBatchSize, int dim, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Labels = labels;
                Dim = dim;
                MaxBatchSize = maxBatchSize;
                Vec = new SparseVectorData(MaxBatchSize * Dim, behavior.Device);
            }

            public override void Forward()
            {
                int cursor = 0;
                for(int b =0 ; b < Labels.Count; b++)
                {
                    int labelBase = b * Dim;
                    foreach(int t in Labels[b].Distinct())
                    {
                        Vec.Idx[cursor] = labelBase + t;
                        Vec.Value[cursor] = 1;
                        cursor += 1;
                    }
                }
                Vec.Length = cursor;
                Vec.SyncFromCPU();
            }
        }

        public class ProbAssignRunner : StructRunner
        {
            public SparseVectorData PosLabel { get; set; }
            //List<List<int>> PosLabelList = null;
            public HiddenBatchData Score { get; set; }
            //CudaPieceFloat output { get; set; }
            //CudaPieceFloat one { get; set; }
            public CudaPieceFloat Prob { get; set; }
            public ProbAssignRunner(HiddenBatchData score, SparseVectorData posLabel, RunnerBehavior behavior) 
                : base(Structure.Empty, behavior)
            {
                Score = score;
                PosLabel = posLabel;
                //output = new CudaPieceFloat(score.MAX_BATCHSIZE * score.Dim, behavior.Device);
                Behavior.Resource.RegisteOneVectorFloat(score.Dim);
                Behavior.Resource.RegisteFloatVector(score.MAX_BATCHSIZE * score.Dim, "tmp_float_1");
                //one = new CudaPieceFloat(score.Dim, behavior.Device);
                //one.Init(1);
                Prob = new CudaPieceFloat(score.MAX_BATCHSIZE, behavior.Device);
            }

            public override void Forward()
            {
                //tmp = logistic(s)
                ComputeLib.Logistic(Score.Output.Data, 0, Behavior.Resource.MemF["tmp_float_1"], 0, Score.BatchSize * Score.Dim, 1);

                // tmp =  tmp - label_i  
                ComputeLib.SparseVectorAdd(PosLabel.Idx, PosLabel.Value, Behavior.Resource.MemF["tmp_float_1"], -1, PosLabel.Length);
                
                // tmp = 1 - tmp ;
                ComputeLib.Init_Vector(Behavior.Resource.MemF["tmp_float_1"], 0, 1, Score.BatchSize * Score.Dim, -1);

                //CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, float alpha, float beta, int size);
                ComputeLib.Log(Behavior.Resource.MemF["tmp_float_1"], 0, Behavior.Resource.MemF["tmp_float_1"], 0, 0, 1, Score.BatchSize * Score.Dim);

                Prob.EffectiveSize = Score.BatchSize;
                ComputeLib.Sgemv(Behavior.Resource.MemF["tmp_float_1"], Behavior.Resource.One, Score.BatchSize, Score.Dim, Prob, false, 0, 1);
                
            }
        }

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure NodeEmbed { get; set; }
            public EmbedStructure RelEmbed { get; set; }

            public GRUCell GruCell { get; set; }
            public DNNStructure SrcDNN { get; set; }
            public DNNStructure ScoreDNN { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeDim, int relDim, int rnnDim, int hop, DeviceType device)
            {
                Logger.WriteLog("Node Num {0} Embed {1}", entityNum, nodeDim);
                Logger.WriteLog("Relation Num {0} Embed {1}", relationNum, relDim);
                
                NodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                RelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

                int embedDim = nodeDim + relDim;

                // input embedDim -> rnnDim; 
                GruCell = AddLayer(new GRUCell(embedDim, rnnDim, device));

                SrcDNN = AddLayer(new DNNStructure(rnnDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DROPS,
                                           BuilderParameters.DNN_DIMS.Select(i => true).ToArray(),
                                           device));

                int inputDim = (embedDim + rnnDim) * (hop + 1);

                ScoreDNN = AddLayer(new DNNStructure(inputDim, BuilderParameters.SCORE_DIMS,
                                           BuilderParameters.SCORE_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.SCORE_DROPS,
                                           BuilderParameters.SCORE_DIMS.Select(i => true).ToArray(),
                                           device));

                //List<int> dnnDims = new List<int>(BuilderParameters.SCORE_DIMS);
                //dnnDims.Add(entityNum);

                //List<A_Func> dnnFunc = BuilderParameters.SCORE_DIMS.Select(i => A_Func.Tanh).ToList();
                //dnnFunc.Add(A_Func.Linear);

                //List<float> dnnDrops = new List<float>( BuilderParameters.SCORE_DROPS);
                //dnnDrops.Add(0);

                //ScoreDNN = AddLayer(new DNNStructure(inputDim, dnnDims.ToArray(),
                //                           dnnFunc.ToArray(),
                //                           dnnDrops.ToArray(),
                //                           dnnDims.Select(i => true).ToArray(),
                //                           device));

            }
            // public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

            //     NodeEmbed = DeserializeModel<EmbedStructure>(reader, device); // DeviceType.CPU_FAST_VECTOR);
            //     RelEmbed = DeserializeModel<EmbedStructure>(reader, device); // DeviceType.CPU_FAST_VECTOR);
            //     GruCell = DeserializeModel<GRUCell>(reader, device);
            //     SrcDNN = DeserializeModel<DNNStructure>(reader, device);
            //     ScoreDNN = DeserializeModel<DNNStructure>(reader, device);   
            // }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }
        
        static Random SampleRandom = new Random(DeepNet.BuilderParameters.RandomSeed + 101);

        public static void ReinforceWalkModel(ComputationGraph cg, 
                                  GraphQueryData graphQuery, 
                                  GraphEnviroument graph, 
                                  int maxHop, 
                                  NeuralWalkerModel model, 
                                  RunnerBehavior Behavior)
        {
            HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, model.NodeEmbed, Behavior));
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawRel, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));
            
            if(Behavior.RunMode == DNNRunMode.Train && BuilderParameters.EntityEmdDrop > 0)
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(srcEmbed, BuilderParameters.EntityEmdDrop, Behavior));

            if(Behavior.RunMode == DNNRunMode.Train && BuilderParameters.RelEmdDrop > 0)
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(relEmbed, BuilderParameters.RelEmdDrop, Behavior));
            
            HiddenBatchData iEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { srcEmbed, relEmbed }, Behavior));

            IntArgument batchSizeArg = new IntArgument("sample-batch-size"); 
            cg.AddRunner(new HiddenDataBatchSizeRunner(iEmbed, batchSizeArg, Behavior));

            HiddenBatchData initZeroH0 = (HiddenBatchData)cg.AddRunner(new ZeroMatrixRunner(iEmbed.MAX_BATCHSIZE, model.GruCell.HiddenStateDim, batchSizeArg, Behavior));
            GRUStateRunner initStateRunner = new GRUStateRunner(model.GruCell, initZeroH0, iEmbed, Behavior);
            cg.AddRunner(initStateRunner);

            HiddenBatchData H1 = initStateRunner.Output;

            HiddenBatchData accEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { iEmbed, H1 }, Behavior));
            StatusData status = new StatusData(graphQuery, graphQuery.RawSource, H1, accEmbed, Behavior.Device);

            #region multi-hop expansion
            // travel four steps in the knowledge graph.
            for (int i = 0; i < maxHop; i++)
            {
                CandidateV2Runner candidateActionRunner = new CandidateV2Runner(status, graph, 1, Behavior);
                cg.AddRunner(candidateActionRunner);
                //status.MatchCandidate = candidateActionRunner.Output;
                status.MatchCandidate_Node = candidateActionRunner.LinkNodes;
                status.MatchCandidate_Rel = candidateActionRunner.LinkRels;

                HiddenBatchData candNodeEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.LinkNodes, 
                                                                        candidateActionRunner.LinkNodes.Size, 
                                                                        model.NodeEmbed, Behavior));

                HiddenBatchData candRelEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.LinkRels, 
                                                                        candidateActionRunner.LinkRels.Size, 
                                                                        model.RelEmbed, Behavior));

                if(Behavior.RunMode == DNNRunMode.Train && BuilderParameters.EntityEmdDrop > 0)
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(candNodeEmbed, BuilderParameters.EntityEmdDrop, Behavior));

                if(Behavior.RunMode == DNNRunMode.Train && BuilderParameters.RelEmdDrop > 0)
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(candRelEmbed, BuilderParameters.RelEmdDrop, Behavior));

                //HiddenBatchData candEmbed = (HiddenBatchData)cg.AddRunner(new AdditionRunner(candNodeEmbed, candRelEmbed, Behavior));
                HiddenBatchData candEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { candNodeEmbed, candRelEmbed }, Behavior));

                //SeqMatrixData(int column, int maxRow, int maxSegment, CudaPieceFloat data, CudaPieceFloat deriv, CudaPieceInt segmentIdx, CudaPieceInt segmentMargin, DeviceType device) :

                SeqMatrixData seqCandEmbed = new SeqMatrixData(candEmbed.Dim, candEmbed.MAX_BATCHSIZE, status.MaxBatchSize,
                                                            candEmbed.Output.Data, candEmbed.Deriv.Data, candidateActionRunner.LinkIdxs, candidateActionRunner.LinkMargin, Behavior.Device);

                //HiddenBatchData I_data = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
                //                            { status.StateEmbed, queryEmbed }, Behavior));
                DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                cg.AddRunner(srcHiddenRunner);

                SeqInnerProductRunner innerRunner = new SeqInnerProductRunner(new MatrixData(srcHiddenRunner.Output), seqCandEmbed, Behavior);
                cg.AddRunner(innerRunner);
                status.MatchCandidateQ = new SeqVectorData(innerRunner.Output.MaxLength,
                                                           status.MaxBatchSize, innerRunner.Output.Output, innerRunner.Output.Deriv,
                                                           candidateActionRunner.LinkIdxs, candidateActionRunner.LinkMargin, Behavior.Device);
                
                SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(status.MatchCandidateQ, 1, Behavior, true);
                cg.AddRunner(normAttRunner);
                status.MatchCandidateProb = normAttRunner.Output;
                

                BasicPolicyRunner policyRunner = null;
                if(Behavior.RunMode == DNNRunMode.Train)
                {
                    policyRunner = new ThompasSamplingRunner(status, graph.MaxNeighborNum, SampleRandom, Behavior);
                }
                else
                {
                    policyRunner = new GreedySamplingRunner(status, graph.MaxNeighborNum, Behavior);
                }
                // mcts policy.
                //if(policyId == 0)
                //{
                //    policyRunner = new MCTSPolicyRunner(status, graph.MaxNeighborNum, mem, BuilderParameters.PUCT_C, BuilderParameters.PUCT_D, Behavior);
                //}
                // replay policy.
                //else if(policyId == 1)
                //{
                //    policyRunner = new ReplayPolicyRunner(status, graph.MaxNeighborNum, Behavior);
                //}
                // beam search policy.
                //else if(policyId == 2)
                //{
                //    policyRunner = new BeamSearchActionRunner(status, graph.MaxNeighborNum, beamSize, Behavior);
                //}
                cg.AddRunner(policyRunner);

                //SeqMatrixReduceRunner poolRunner = new SeqMatrixReduceRunner(seqCandEmbed, normAttRunner.Output, Behavior);
                //cg.AddRunner(poolRunner);
                //MatrixData poolData = poolRunner.Output;

                #region take action.                
                // MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                // cg.AddRunner(srcExpRunner);

                // MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbed, policyRunner.MatchPath, 2, Behavior);
                // cg.AddRunner(tgtExpRunner);

                // MatrixExpansionRunner srcAccRunner = new MatrixExpansionRunner(status.InputEmbed, policyRunner.MatchPath, 1, Behavior);
                // cg.AddRunner(srcAccRunner);

                // GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior); 
                // cg.AddRunner(stateRunner);

                // HiddenBatchData accHEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { srcAccRunner.Output, tgtExpRunner.Output, stateRunner.Output}, Behavior));
                MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbed, policyRunner.MatchPath, 2, Behavior);
                cg.AddRunner(tgtExpRunner);

                GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, status.StateEmbed, tgtExpRunner.Output, Behavior); 
                cg.AddRunner(stateRunner);

                HiddenBatchData accHEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { status.InputEmbed, tgtExpRunner.Output, stateRunner.Output}, Behavior));
                #endregion.

                StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                    policyRunner.ActIndex, policyRunner.StatusIndex, stateRunner.Output, accHEmbed, Behavior.Device);

                status = nextStatus;
            }
            #endregion.
        }

        public static ComputationGraph BuildComputationGraph(DataEnviroument data, GraphEnviroument graph, 
                    int batchSize, int roll, int debugSamples, int beamSize, int policyId,
                    int maxHop, NeuralWalkerModel model, RunnerBehavior Behavior) 
        {
            // build computation graph.
            ComputationGraph cg = new ComputationGraph(Behavior);

            // sample a list of tuplet from the graph.
            SampleRunner SmpRunner = Behavior.RunMode == DNNRunMode.Train ?
                                                 new SampleRunner(data, batchSize, roll, debugSamples, true, Behavior) :
                                                 new SampleRunner(data, batchSize, 1, debugSamples, false, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData graphQuery = SmpRunner.Output;
            //GraphQueryData graphQuery, 
            //                      GraphEnviroument graph, 
            //                      int maxHop, 
            //                      NeuralWalkerModel model, 
            //                      RunnerBehavior Behavior
            ReinforceWalkModel(cg, graphQuery, graph, maxHop, model, Behavior);

            StatusData st = graphQuery.StatusPath.Last();

            HiddenBatchData hid = st.InputEmbed;
            //if(Behavior.RunMode == DNNRunMode.Train)
            //{
            //    DropOutRunner dropRunner = new DropOutRunner(new MatrixData(st.InputEmbed), 0.2f, Behavior);
            //    cg.AddRunner(dropRunner);
            //    hid = new HiddenBatchData(dropRunner.Output);
            //}

            DNNRunner<HiddenBatchData> outputRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, hid, Behavior);
            cg.AddRunner(outputRunner);
            HiddenBatchData outEmbed = outputRunner.Output;

            MatrixProductRunner scoreProductRunner = new MatrixProductRunner( new MatrixData(outEmbed), new MatrixData(model.NodeEmbed), Behavior);
            cg.AddRunner(scoreProductRunner);
            HiddenBatchData score = new HiddenBatchData(scoreProductRunner.Output);
            //HiddenBatchData score = outputRunner.Output;

            if(Behavior.RunMode == DNNRunMode.Train)
            {
                //List<List<int>> labels, int maxBatchSize, int dim, RunnerBehavior behavior
                SparseVectorRunner sparseLabelRunner = new SparseVectorRunner(graphQuery.BlackTargets, graphQuery.MaxBatchSize, score.Dim, Behavior);
                cg.AddRunner(sparseLabelRunner);

                ProbAssignRunner probRunner = new ProbAssignRunner(score, sparseLabelRunner.Vec, Behavior);
                cg.AddRunner(probRunner);
                
                PolicyGradientCRRunner pgRunner = new PolicyGradientCRRunner(graphQuery, BuilderParameters.ALPHA_2, 
                                            BuilderParameters.REWARD_DISCOUNT, BuilderParameters.PG_ALPHA, probRunner.Prob, roll, Behavior);
                cg.AddObjective(pgRunner);

                BinaryCrossEntropyRunner bceLossRunner = new BinaryCrossEntropyRunner(score, sparseLabelRunner.Vec, Behavior);
                cg.AddObjective(bceLossRunner);
            }
            else
            {
                ScoreHitKRunner hitRunner = new ScoreHitKRunner(graphQuery, score, false, Behavior);
                cg.AddRunner(hitRunner);
            }
            // if (Behavior.RunMode == DNNRunMode.Train)
            // {
            //     PolicyGradientRunner pgRunner = new PolicyGradientRunner(interface_data, BuilderParameters.ALPHA_2, BuilderParameters.REWARD_DISCOUNT, Behavior);
            //     cg.AddObjective(pgRunner);
            // }
            // else
            // {
            //     GraphHitKRunner hitRunner = new GraphHitKRunner(interface_data, null, 0, Behavior);
            //     cg.AddRunner(hitRunner);
            // }
            cg.SetDelegateModel(model);
            
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            GraphDataPanel.Init(BuilderParameters.InputDir, BuilderParameters.MAX_ACTION_NUM);
            GraphDataPanel.GraphEnv.BuildLinkGraph(device);
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model = new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, BuilderParameters.MAX_HOP, device);

            //    BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
            //    new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
            //                          BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, BuilderParameters.MAX_HOP, device) :
            //    new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            model.InitOptimization(behavior);

            Logger.WriteLog("PG_TransPath Model Parameter Number {0}, EntityNum {1}, RelationNum {2}", model.ParamNumber, GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum);


            ComputationGraph testCG = null;
            ComputationGraph validCG = null;

            {
                testCG = BuildComputationGraph(GraphDataPanel.TestData, GraphDataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1,  BuilderParameters.TEST_SAMPLES, BuilderParameters.BeamSize, 1,
                                                  BuilderParameters.MAX_HOP,  model, behavior);

                validCG = BuildComputationGraph(GraphDataPanel.DevData, GraphDataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1,  BuilderParameters.TEST_SAMPLES, BuilderParameters.BeamSize, 1,
                                                  BuilderParameters.MAX_HOP,  model, behavior);
            }
            
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(BuilderParameters.DEV_AS_TRAIN > 0 ? GraphDataPanel.DevData : GraphDataPanel.TrainData, GraphDataPanel.GraphEnv, BuilderParameters.MiniBatchSize,
                                                                     BuilderParameters.ROLL_NUM, BuilderParameters.TRAIN_SAMPLES, 0, 0,
                                                                     BuilderParameters.MAX_HOP, model, behavior);
                    
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);
                            //validCG.Execute();
                            testCG.Execute();
                            validCG.Execute();
                            //using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            //{
                            //    model.Serialize(writer);
                            //}
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    testCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }
        
    }   
}
