using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
    public class AppTransConv_Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
            public static int MCTSNum { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static int EmbedW { get { return int.Parse(Argument["EMBED-W"].Value); } }
            public static int N_EmbedH { get { return int.Parse(Argument["N-EMBED-H"].Value); } }
            public static int R_EmbedH { get { return int.Parse(Argument["R-EMBED-H"].Value); } }

            public static int RNN_Dim { get { return int.Parse(Argument["RNN-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] DNN_DROPS { get { return Argument["DNN-DROPS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int MIN_HOP { get { return int.Parse(Argument["MIN-HOP"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }
            //public static float REWARD_MCTS_DISCOUNT { get { return float.Parse(Argument["REWARD-MCTS-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }
            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int DEV_AS_TRAIN { get { return int.Parse(Argument["DEV-TRAIN"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }
            public static int ActorMiniBatch { get { return int.Parse(Argument["ACTOR-MINI-BATCH"].Value); } }
            public static int ActorPolicy { get { return int.Parse(Argument["ACTOR-POLICY"].Value); } } 
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static int LearnerMiniBatch { get { return int.Parse(Argument["LEARNER-MINI-BATCH"].Value); } }
            public static int LearnerEpoch { get { return int.Parse(Argument["LEARNER-EPOCH"].Value); } }
            public static int ModelSyncUp { get { return int.Parse(Argument["MODEL-SYNCUP"].Value); } }
            public static int ReplayBufferSize { get { return int.Parse(Argument["REPLAY-BUFF-SIZE"].Value); } } 
            public static int TReplaySize { get { return int.Parse(Argument["T-REPLAY-SIZE"].Value); } } 
            public static float ReplayDecay { get { return float.Parse(Argument["REPLAY-DECAY"].Value); } }
            
            public static int NTrial { get { return int.Parse(Argument["NTRIAL"].Value); } }

            public static int TiedEntityEmd { get { return int.Parse(Argument["TIED-ENTITY-EMD"].Value); } }


            public static float EntityEmdDrop { get { return float.Parse(Argument["ENTITY-EMD-DROP"].Value); } }
            public static float RelEmdDrop { get { return float.Parse(Argument["REL-EMD-DROP"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("EMBED-W", new ParameterArgument("16","embedding width."));
                Argument.Add("N-EMBED-H", new ParameterArgument("10", "Node Embedding Height."));
                Argument.Add("R-EMBED-H", new ParameterArgument("10", "Relation Embedding Height."));
                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                Argument.Add("DNN-DROPS", new ParameterArgument("0,0", "DNN Dropout."));
                
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));
                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
                Argument.Add("MIN-HOP", new ParameterArgument("2", "minimum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
                Argument.Add("MCTS-NUM", new ParameterArgument("0", "Monto Carlo Tree Search Number"));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));

                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                
                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("ALPHA-1", new ParameterArgument("0","max entropy for termination gate."));
                Argument.Add("ALPHA-2", new ParameterArgument("0", "max entropy for action gate."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));

                Argument.Add("ACTOR-MINI-BATCH", new ParameterArgument("128", "actor mini batch size"));
                Argument.Add("ACTOR-POLICY", new ParameterArgument("4", "0:UnifiedSampling; 1:ThompasSampling; 2:UCB; 3:AlphaGoZero1; 4:AlphaGoZero2; 5:PUCB"));

                Argument.Add("LEARNER-MINI-BATCH", new ParameterArgument("32", "learner mini batch size"));
                Argument.Add("LEARNER-EPOCH", new ParameterArgument("5", "learner training epoch size"));
                Argument.Add("MODEL-SYNCUP", new ParameterArgument("3000", "model syncup iteration."));   
                Argument.Add("REPLAY-BUFF-SIZE", new ParameterArgument("3000000", "buffer size for the replay memory."));   
                Argument.Add("T-REPLAY-SIZE", new ParameterArgument("5000", "threshold of replay buffer size."));   
                Argument.Add("REPLAY-DECAY", new ParameterArgument("0.99995", "Replay Decay Value."));   
                Argument.Add("NTRIAL", new ParameterArgument("15", "Negative Trial"));
                Argument.Add("TIED-ENTITY-EMD", new ParameterArgument("0", "0:untied entity embedding; 1:tied entity embedding.") );
                
                Argument.Add("ENTITY-EMD-DROP", new ParameterArgument("0", "dropout rate for entity embedding."));
                Argument.Add("REL-EMD-DROP", new ParameterArgument("0","dropout rate for relation embedding."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_TRANSCONV_; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }
  
        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure InNodeEmbed { get; set; }
            public EmbedStructure RelEmbed { get; set; }
            public TensorConvStructure Conv { get; set; }
            public DNNStructure ScoreDNN { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeH, int relH, int embedW, DeviceType device)
            {
                int nodeDim = nodeH * embedW;
                int relDim = relH * embedW;

                InNodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                RelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
                
                Conv = AddLayer(new TensorConvStructure(3, 3, 1, 32, device));

                int inputW = embedW;
                int inputH = nodeH + relH;
                Logger.WriteLog("input W {0}, H {1}", inputW, inputH);
                int outputW = TensorConvUtil.FilterOutput(inputW, 3, 0, 1);
                int outputH = TensorConvUtil.FilterOutput(inputH, 3, 0, 1);
                Logger.WriteLog("output W {0}, H {1}", outputW, outputH);
                
                int fc_input = outputW * outputH * 32;
                Logger.WriteLog("fc dim {0}", fc_input);
                List<int> dnnDims = new List<int>(BuilderParameters.DNN_DIMS);
                dnnDims.Add(entityNum);

                List<A_Func> dnnFunc = BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToList();
                dnnFunc.Add(A_Func.Linear);

                List<float> dnnDrops = new List<float>( BuilderParameters.DNN_DROPS);
                dnnDrops.Add(0);

                ScoreDNN = AddLayer(new DNNStructure(fc_input, dnnDims.ToArray(),
                                           dnnFunc.ToArray(),
                                           dnnDrops.ToArray(),
                                           dnnDims.Select(i => true).ToArray(),
                                           device));
            }
            
            // public override void CopyFrom(IData other) 
            // {
            //     NeuralWalkerModel src = (NeuralWalkerModel)other;
            //     InNodeEmbed.CopyFrom(src.InNodeEmbed);
            //     RelEmbed.CopyFrom(src.RelEmbed);
            //     Conv.CopyFrom(src.Conv);
            //     ScoreDNN.CopyFrom(src.ScoreDNN);
            // }


            // public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
            //     InNodeEmbed = DeserializeModel<EmbedStructure>(reader, device); 
            //     RelEmbed =  DeserializeModel<EmbedStructure>(reader, device); 
            //     Conv = DeserializeModel<TensorConvStructure>(reader, device);
            //     ScoreDNN = DeserializeModel<DNNStructure>(reader, device);
            // }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public static void TransConvModel(ComputationGraph cg, DataEnviroument data,
                                       int batchSize, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            SampleRunner SmpRunner = new SampleRunner(data, batchSize, 1, 0, Behavior.RunMode == DNNRunMode.Train ? true : false, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData graphQuery = SmpRunner.Output;

            HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, model.InNodeEmbed, Behavior));
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawRel, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));

            if(Behavior.RunMode == DNNRunMode.Train && BuilderParameters.EntityEmdDrop > 0)
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(srcEmbed, BuilderParameters.EntityEmdDrop, Behavior));

            if(Behavior.RunMode == DNNRunMode.Train && BuilderParameters.RelEmdDrop > 0)
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(relEmbed, BuilderParameters.RelEmdDrop, Behavior));

            HiddenBatchData iEmbed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
                                            { srcEmbed, relEmbed }, Behavior));

            Tensor4DData feaData = new Tensor4DData(BuilderParameters.EmbedW, BuilderParameters.N_EmbedH + BuilderParameters.R_EmbedH, 1, graphQuery.MaxBatchSize,
                                                    iEmbed.Output.Data, iEmbed.Deriv.Data, Behavior.Device);

            TensorConvRunner convRunner = new TensorConvRunner(model.Conv, feaData, 0, 1, A_Func.Rectified, Behavior);
            cg.AddRunner(convRunner);

            MatrixData oEmbed = convRunner.Output.Context;

            if(Behavior.RunMode == DNNRunMode.Train)
            {
                DropOutRunner dropRunner = new DropOutRunner(oEmbed, 0.3f, Behavior);
                cg.AddRunner(dropRunner);
                oEmbed = dropRunner.Output;
            }

            DNNRunner<HiddenBatchData> outputRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, new HiddenBatchData(oEmbed), Behavior);
            cg.AddRunner(outputRunner);
            HiddenBatchData score = outputRunner.Output;

            if(Behavior.RunMode == DNNRunMode.Train)
            {
                // obviously the method has a better objective function.
                BinaryCrossEntropyRunner bceLossRunner = new BinaryCrossEntropyRunner(score, graphQuery.BlackTargets, Behavior);
                cg.AddObjective(bceLossRunner);
            }
            else
            {
                ScoreHitKRunner hitRunner = new ScoreHitKRunner(graphQuery, score, false, Behavior);
                cg.AddRunner(hitRunner);
            }
            cg.SetDelegateModel(model);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };
            
            Logger.WriteLog("Loading Training/Validation/Test Data.");
            GraphDataPanel.Init(BuilderParameters.InputDir, BuilderParameters.MAX_ACTION_NUM);
            Logger.WriteLog("Load Data Finished.");

            //int entityNum, int relationNum, int nodeH, int relH, int embedW, DeviceType device)

            NeuralWalkerModel model = new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
                                      BuilderParameters.N_EmbedH, BuilderParameters.R_EmbedH, BuilderParameters.EmbedW, device);

            //    BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
            //    new NeuralWalkerModel(GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum, 
            //                          BuilderParameters.N_EmbedH, BuilderParameters.R_EmbedH, BuilderParameters.EmbedW, device) :
            //    new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            model.InitOptimization(behavior);

            Logger.WriteLog("TransConv_ Model Parameter Number {0}, EntityNum {1}, RelationNum {2}", model.ParamNumber, GraphDataPanel.GraphVoc.EntityNum, GraphDataPanel.GraphVoc.RelationNum);

            ComputationGraph testCG = new ComputationGraph(behavior);
            ComputationGraph validCG = new ComputationGraph(behavior);
            ComputationGraph trainCG = new ComputationGraph(behavior);

            TransConvModel(testCG, GraphDataPanel.TestData, BuilderParameters.TestMiniBatchSize, model, behavior);

            TransConvModel(validCG, GraphDataPanel.DevData, BuilderParameters.TestMiniBatchSize, model, behavior);

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    TransConvModel(trainCG, GraphDataPanel.TrainData, BuilderParameters.ActorMiniBatch, model, 
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    
                    for(int iter = 1; iter <= OptimizerParameters.Iteration; iter++)
                    {
                        trainCG.Execute();
                        if(iter % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            testCG.Execute();
                            validCG.Execute();
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    break;
            }    
            Logger.CloseLog();
        }
    }
}
