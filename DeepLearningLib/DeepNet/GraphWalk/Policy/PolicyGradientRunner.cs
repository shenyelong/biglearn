using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
namespace BigLearn.DeepNet.Graph
{
		public class PolicyGradientRunner : ObjectiveRunner
        {
            //Random random = new Random();
            new GraphQueryData Input { get; set; }
            Dictionary<int, float> Success = new Dictionary<int, float>();
            int Epoch = 0;
            int totalBatchNum = 0;
            
            float Entropy = 0;
            int uniqueNum = 0;
            int uniqueLeafNum = 0;
            float Discount = 0;

            public override void Init()
            {
                Success.Clear();
                uniqueNum = 0;
                uniqueLeafNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                int sc = Success.Where(i => i.Value > 0).Count();
                int t = Success.Count;
                Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
                Logger.WriteLog("unique leaf number {0}", uniqueLeafNum * 1.0f / uniqueNum);

                float alpha_entropy = Entropy * (float)Math.Pow(0.995f, totalBatchNum / 200);
                Logger.WriteLog("Entropy Term {0}", alpha_entropy);
            }

            public PolicyGradientRunner(GraphQueryData input, float entropy, float discount, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Entropy = entropy;
                Discount = discount;
            }

            public override void Forward()
            {
                float trueReward = 0;
                int sampleNum = 0;
                int pos_smp = 0;
                int neg_smp = 0;
                ObjectiveDict.Clear();

                Dictionary<int, HashSet<int>> visitNodes = new Dictionary<int, HashSet<int>>();
                Dictionary<int, float> TmpSuccess = new Dictionary<int, float>();
                // mcts sampling.
                float avg_t_prob = 0;
                int avg_t_count = 0;

                float avg_act_prob = 0;
                int avg_act_count = 0;

                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    if (Input.StatusPath[t].MatchCandidateProb != null)
                    {
                        Input.StatusPath[t].MatchCandidateProb.Deriv.SyncToCPU();
                        Input.StatusPath[t].MatchCandidateProb.Output.SyncToCPU();
                    }
                 }

                //float 
                {
                	StatusData st = Input.StatusPath.Last();
                    for (int i = 0; i < st.BatchSize; i++)
                    {
                        int t = st.Step;
                        int b = i;

                        int origialB = st.GetOriginalStatsIndex(b);
                        int predId = st.NodeID[b];
                        float neg_likihood = st.GetLogProb(b);
                        
                        int targetId = Input.RawTarget[origialB];
                        int rawIdx = Input.RawIndex[origialB];

                        if(targetId < 0) { throw new Exception("target Idx is zero. it is incorrect in pg runner."); }
                        if (!visitNodes.ContainsKey(rawIdx)) visitNodes.Add(rawIdx, new HashSet<int>());
                        if (!visitNodes[rawIdx].Contains(predId)) visitNodes[rawIdx].Add(predId);
 
                        float update_v = 0;
                        float true_v = 0;
                        if (targetId == predId)
                        {
                            true_v = 1;
                            update_v = 1;
                            pos_smp += 1;
                        }
                        else
                        {
                            true_v = 0;
                            update_v = 0;
                            neg_smp += 1;
                        }

                        trueReward += true_v;
                        sampleNum += 1;

                        if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
                        Success[rawIdx] += true_v;

                        if (!TmpSuccess.ContainsKey(rawIdx)) TmpSuccess[rawIdx] = 0;
                        TmpSuccess[rawIdx] += true_v;

                        if (Input.BlackTargets[origialB].Contains(predId) && targetId != predId)
                        {
                            continue;
                        }

                        {
                            float adv = update_v;
                            StatusData cst = st;
                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                StatusData pst = Input.StatusPath[pt];
                                int pb = cst.GetPreStatusIndex(b);
                                int sidx = cst.PreSelActIndex[b];

                                float discount = (float)Math.Pow(Discount, t - pt);
                                {
                                    pst.MatchCandidateProb.Deriv[sidx] = discount * adv;
                                    avg_act_prob += pst.MatchCandidateProb.Output[sidx];
                                    avg_act_count += 1;
                                }
                                
                                cst = pst;
                                b = pb;
                            }
                        }
                    }
                }
                uniqueNum += visitNodes.Count;
                uniqueLeafNum += visitNodes.Select(t => t.Value.Count).Sum();
                

                ObjectiveDict["ACT-AVG-PROB"] = avg_act_prob / avg_act_count;

                
                ObjectiveDict["POS-NUM"] = pos_smp;
                ObjectiveDict["NEG-NUM"] = neg_smp;

                ObjectiveDict["Success-Rate"] = TmpSuccess.Where(i => i.Value > 0).Count() * 1.0f / TmpSuccess.Count;

                ObjectiveScore = trueReward / (sampleNum + float.Epsilon);

                totalBatchNum += 1;
                float alpha_entropy = Entropy * (float)Math.Pow(0.90f, totalBatchNum / 200);

                {
                    for (int i = Input.StatusPath.Count - 1; i >= 0; i--)
                    {
                        StatusData st = Input.StatusPath[i];
                        if (st.MatchCandidateProb != null)
                        {
                            st.MatchCandidateProb.Deriv.SyncFromCPU();
                            ComputeLib.DerivLogProbEntropy(st.MatchCandidateProb.SegmentIdx, st.MatchCandidateProb.Output,
                                st.MatchCandidateProb.Deriv, 1, alpha_entropy, Util.LargeEpsilon, st.MatchCandidateProb.Segment);
                        }
                    }
                }
            }
        }
}
