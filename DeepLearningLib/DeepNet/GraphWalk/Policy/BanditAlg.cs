using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet.Graph
{
        public class BanditAlg 
        {
        	public static int AlphaGoZeroBandit(Tuple<float[], float[], float> arms, float[] prior, int dim, float alpha_c, float alpha_d)
            {
            	if(arms == null) return Util.MaximumValue(prior);

                float totalVisit = (float)arms.Item2.Sum() * arms.Item3;
                float totalReward = (float)arms.Item1.Sum() * arms.Item3;
                float avgQ = totalReward * 1.0f / totalVisit;
                float maxV = -100000000;
                int maxI = -1;

                for (int i = 0; i < dim; i++)
                {
                    double q = arms.Item2[i] == 0 ? avgQ : arms.Item1[i] * 1.0f / arms.Item2[i];
                    double u = alpha_c * Math.Pow(prior[i], alpha_d) * Math.Sqrt(totalVisit) / (arms.Item2[i] * arms.Item3 + 1.0f);
                    float s = (float)(q + u);
                    if (s > maxV)
                    {
                        maxV = s;
                        maxI = i;
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBandit(Tuple<float[], float[], float> arms, float[] prior, int start, int dim, float alpha_c, float alpha_d)
            {
                if(arms == null) return Util.MaximumValue(prior, start, start + dim) - start;

                float totalVisit = (float)arms.Item2.Sum() * arms.Item3;
                float totalReward = (float)arms.Item1.Sum() * arms.Item3;
                float avgQ = totalReward * 1.0f / totalVisit;
                float maxV = -100000000;
                int maxI = -1;

                for (int i = 0; i < dim; i++)
                {
                    double q = arms.Item2[i] == 0 ? avgQ : arms.Item1[i] * 1.0f / arms.Item2[i];
                    double u = alpha_c * Math.Pow(prior[start + i], alpha_d) * Math.Sqrt(totalVisit) / (arms.Item2[i] * arms.Item3 + 1.0f);
                    float s = (float)(q + u);
                    if (s > maxV)
                    {
                        maxV = s;
                        maxI = i;
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBandit(float[] arm_q, float[] arm_v, float w, float[] prior, int start, int dim, float alpha_c, float alpha_d)
            {
                if(arm_q == null || arm_v == null) return Util.MaximumValue(prior, start, start + dim) - start;

                float totalVisit = (float)arm_v.Sum() * w;
                float totalReward = (float)arm_q.Sum() * w;
                float avgQ = totalReward * 1.0f / totalVisit;
                float maxV = -100000000;
                int maxI = 0;

                for (int i = 0; i < dim; i++)
                {
                    double q = arm_v[i] == 0 ? avgQ : arm_q[i] * 1.0f / arm_v[i];
                    double u = alpha_c * Math.Pow(prior[i + start], alpha_d) * Math.Sqrt(totalVisit) / (arm_v[i] *  w + 1.0f);
                    float s = (float)(q + u);
                    if (s > maxV)
                    {
                        maxV = s;
                        maxI = i;
                    }
                }
                return maxI;
            }
            
            public static int AlphaGoZeroBandit(Tuple<float[], float[], float> arms, int[] armPos, float[] prior, int start, int dim, float alpha_c, float alpha_d)
            {
                if(arms == null) return Util.MaximumValue(prior, start, start + dim) - start;
                
                float tQ = 0;
                float tV = 0;
                float wei = arms.Item3;
                for(int i = 0; i < dim; i++)
                {
                    int armIdx = armPos[i + start];
                    float arm_q = arms.Item1[armIdx];
                    float arm_v = arms.Item2[armIdx];

                    tQ += arm_q * wei;
                    tV += arm_v * wei;
                }
                float avgQ = tQ * 1.0f / tV;
                
                float maxV = -100000000;
                int maxI = -1;

                for (int i = 0; i < dim; i++)
                {
                    int armIdx = armPos[i + start];
                    float arm_q = arms.Item1[armIdx];
                    float arm_v = arms.Item2[armIdx];

                    double q = arm_v <= 0.0001 ? avgQ : arm_q * 1.0f / arm_v;
                    double u = alpha_c * Math.Pow(prior[start + i], alpha_d) * Math.Sqrt(tV) / (arm_v * wei + 1.0f);
                    float s = (float)(q + u);
                    if (s > maxV)
                    {
                        maxV = s;
                        maxI = i;
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBandit(MCTSNode arms, float[] prior, int start, int dim, float alpha_c, float alpha_d)
            {
                if(arms == null) return Util.MaximumValue(prior, start, start + dim) - start;

                float totalVisit = arms.V;
                float totalReward = arms.Q;
                float avgQ = totalReward * 1.0f / totalVisit;
                float maxV = -100000000;
                int maxI = -1;

                for (int i = 0; i < dim; i++)
                {
                    float q = arms.arm_v[i] <= 0.0001f ? avgQ : (arms.arm_q[i] * 1.0f / arms.arm_v[i]);
                    float u = (float)( alpha_c * Math.Pow(prior[start + i], alpha_d) * Math.Sqrt(totalVisit) / (arms.arm_v[i] + 1.0f) );
                    float s = (q + u);
                    if (s > maxV)
                    {
                        maxV = s;
                        maxI = i;
                    }
                }
                return maxI;
            }

            public static int StocasticBandit(Tuple<float[], float[], float> arms, int[] armPos, float[] prior, float[] tmpQ, float gamma, int start, int dim, float alpha_c, float alpha_d, Random random)
            {
                if(arms == null) return Util.Sample(prior, start, dim, random);
                    //return Util.MaximumValue(prior, start, start + dim) - start;
                
                float tQ = 0;
                float tV = 0;
                float wei = arms.Item3;
                for(int i = 0; i < dim; i++)
                {
                    int armIdx = armPos[i + start];
                    float arm_q = arms.Item1[armIdx];
                    float arm_v = arms.Item2[armIdx];

                    tQ += arm_q * wei;
                    tV += arm_v * wei;
                }
                float avgQ = tQ * 1.0f / tV;
                float norm =  (float)Math.Log(tV);
                for (int i = 0; i < dim; i++)
                {
                    int armIdx = armPos[i + start];
                    float arm_q = arms.Item1[armIdx];
                    float arm_v = arms.Item2[armIdx];

                    double q = arm_v <= 0.0001 ? avgQ : arm_q * 1.0f / arm_v;
                    double u = alpha_c * Math.Pow(prior[start + i], alpha_d) * Math.Sqrt(norm / (arm_v * wei + 1.0f));
                    tmpQ[start + i] = (float)(q + u);
                }
                Util.Softmax(tmpQ, start, tmpQ, start, dim, gamma);
                return Util.Sample(tmpQ, start, dim, random);
            }

            public static int StocasticBandit(Tuple<float[], float[], float> arms, float[] prior, float[] tmpQ, float gamma, int start, int dim, float alpha_c, float alpha_d, Random random)
            {
                if(arms == null) return Util.Sample(prior, start, dim, random);
                    //return Util.MaximumValue(prior, start, start + dim) - start;
                
                float tQ = 0;
                float tV = 0;
                float wei = arms.Item3;
                for(int i = 0; i < dim; i++)
                {
                    float arm_q = arms.Item1[i];
                    float arm_v = arms.Item2[i];
                    tQ += arm_q * wei;
                    tV += arm_v * wei;
                }
                float avgQ = tQ * 1.0f / tV;
                float norm =  (float)Math.Log(tV);
                for (int i = 0; i < dim; i++)
                {
                    int armIdx = i;
                    float arm_q = arms.Item1[armIdx];
                    float arm_v = arms.Item2[armIdx];

                    double q = arm_v <= 0.0001 ? avgQ : arm_q * 1.0f / arm_v;
                    double u = alpha_c * Math.Pow(prior[start + i], alpha_d) * Math.Sqrt(norm / (arm_v * wei + 1.0f));
                    tmpQ[start + i] = (float)(q + u);
                }
                Util.Softmax(tmpQ, start, tmpQ, start, dim, gamma);
                return Util.Sample(tmpQ, start, dim, random);
            }

            public static int StocasticBandit(MCTSNode arms, float[] prior, float[] tmpQ, float gamma, int start, int dim, float alpha_c, float alpha_d, Random random)
            {
                if(arms == null) return Util.Sample(prior, start, dim, random);

                float tQ = arms.Q;
                float tV = arms.V;

                float avgQ = tQ * 1.0f / tV;
                float norm =  (float)Math.Log(tV);
                for (int i = 0; i < dim; i++)
                {
                    int armIdx = i;
                    float arm_q = arms.arm_q[armIdx];
                    float arm_v = arms.arm_v[armIdx];

                    float q = arm_v <= 0.0001f ? avgQ : arm_q * 1.0f / arm_v;
                    float u = (float)( alpha_c * Math.Pow(prior[start + i], alpha_d) * Math.Sqrt(norm / (arm_v + 1.0f)) );
                    tmpQ[start + i] = (q + u);
                }
                Util.Softmax(tmpQ, start, tmpQ, start, dim, gamma);
                return Util.Sample(tmpQ, start, dim, random);
            }            

        }
}
