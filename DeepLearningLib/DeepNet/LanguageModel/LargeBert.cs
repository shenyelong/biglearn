using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Language
{
    unsafe public class LargeBert : ComputationGraph
    {
        public string name = "BertLargeModel";

        public class BertLargeModel : CompositeNNStructure
        {
            public int vocabSize = 30522;
            public int pos = 512;
            public int segType = 2;

            public int embed = 1024;
            public int layer = 24;

            public int category = 2;

            public EmbedStructure TokenEmbed { get; set; }
            public EmbedStructure PosEmbed { get; set; }
            public EmbedStructure TokenTypeEmbed { get; set; }

            public LayerStructure InputNorm { get; set; }

            VectorData mScale = null;
            public VectorData NormScale { get { if(mScale == null) mScale = new VectorData(embed, InputNorm.weight, InputNorm.WeightGrad, InputNorm.DeviceType); return mScale; } }

            VectorData mBias = null;
            public VectorData NormBias { get { if(mBias == null) mBias = new VectorData(embed, InputNorm.bias, InputNorm.BiasGrad, InputNorm.DeviceType); return mBias; } }

            VectorData tBias = null;
            public VectorData TBias { get { if(tBias == null) tBias = new VectorData(vocabSize, TokenBias.bias, InputNorm.BiasGrad, InputNorm.DeviceType); return tBias; } }

            VectorData oScale = null;
            public VectorData OutputNormScale { get { if(oScale == null) oScale = new VectorData(embed, OutputNorm.weight, OutputNorm.WeightGrad, OutputNorm.DeviceType); return oScale; } }
            
            VectorData oBias = null;
            public VectorData OutputNormBias { get { if(oBias == null) oBias = new VectorData(embed, OutputNorm.bias, OutputNorm.BiasGrad, OutputNorm.DeviceType); return oBias; } }


            public TransformerStructure[] Blocks { get; set; }
            public LayerStructure Pooler { get; set; }
            public LayerStructure Classifier { get; set; }
            
            // output layer.
            public LayerStructure OutputLayer { get; set; }
            
            // output layer normalization.
            public LayerStructure OutputNorm { get; set; }
            
            // token bias.
            public LayerStructure TokenBias { get; set; }

            // public BertLargeModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

            //     TokenEmbed = DeserializeModel<EmbedStructure>(reader, device);
            //     PosEmbed = DeserializeModel<EmbedStructure>(reader, device);
            //     TokenTypeEmbed = DeserializeModel<EmbedStructure>(reader, device);
                
            //     InputNorm = DeserializeModel<LayerStructure>(reader, device);

            //     Blocks = new TransformerStructure[layer];
            //     for(int i=0; i < layer; i++)
            //     {
            //         Blocks[i] = DeserializeModel<TransformerStructure>(reader, device);     
            //     }

            //     Pooler = DeserializeModel<LayerStructure>(reader, device);     
            //     Classifier = DeserializeModel<LayerStructure>(reader, device);     

            //     OutputLayer = DeserializeModel<LayerStructure>(reader, device);
            //     OutputNorm = DeserializeModel<LayerStructure>(reader, device);
            //     TokenBias = DeserializeModel<LayerStructure>(reader, device);
            // }

            public BertLargeModel(DeviceType device) : this(2, 24, -1, device) { }
            public BertLargeModel(int mlayer, DeviceType device) : this(2, mlayer, -1, device) { }
            public BertLargeModel(int mcate, int mlayer, DeviceType device) : this(mcate, mlayer, -1, device) { }
            public BertLargeModel(int mcate, int mlayer, int frozenlayer, DeviceType device)
            {
                layer = mlayer;
                category = mcate;

                if(frozenlayer >= 0)
                {
                    TokenEmbed = new EmbedStructure(vocabSize, embed, -0.034f, 0.034f, device);
                    PosEmbed = new EmbedStructure(pos, embed, -0.034f, 0.034f, device);
                    TokenTypeEmbed = new EmbedStructure(segType, embed, -0.034f, 0.034f, device);

                    InputNorm = new LayerStructure(1, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device);
                }
                else
                {
                    TokenEmbed = AddLayer(new EmbedStructure(vocabSize, embed, -0.034f, 0.034f, device)); 
                    PosEmbed = AddLayer(new EmbedStructure(pos, embed, -0.034f, 0.034f, device)); 
                    TokenTypeEmbed = AddLayer(new EmbedStructure(segType, embed, -0.034f, 0.034f, device));

                    InputNorm = AddLayer(new LayerStructure(1, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                }

                InputNorm.weight.Init(1);
                InputNorm.bias.Init(0);

                Blocks = new TransformerStructure[layer];
                for(int i=0; i < layer; i++)
                {
                    if(frozenlayer >= i + 1)
                    {
                        Blocks[i] = new TransformerStructure(embed, device);
                    }
                    else
                    {
                        Blocks[i] = AddLayer(new TransformerStructure(embed, device));
                    }
                }

                Pooler = AddLayer(new LayerStructure(embed, embed, A_Func.Tanh, N_Type.Convolution_layer, 1, 0, true, device));
                Pooler.weight.Init(2 * 0.034f, -0.034f);

                Classifier = AddLayer(new LayerStructure(embed, category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                //float scale, float bias) 
                Classifier.weight.Init(2 * 0.034f, -0.034f);

                OutputLayer = AddLayer(new LayerStructure(embed, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                
                OutputNorm = AddLayer(new LayerStructure(1, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device)); 
                OutputNorm.weight.Init(1);
                OutputNorm.bias.Init(0);

                TokenBias = AddLayer(new LayerStructure(1, vocabSize, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                TokenBias.bias.Init(0);
            }

            // public override void CopyFrom(IData other)
            // {
            //     BertLargeModel src = (BertLargeModel)other;

            //     TokenEmbed.Embedding.CopyFrom(src.TokenEmbed.Embedding);
            //     TokenTypeEmbed.Embedding.CopyFrom(src.TokenTypeEmbed.Embedding);
            //     PosEmbed.Embedding.CopyFrom(src.PosEmbed.Embedding);

            //     InputNorm.CopyFrom(src.InputNorm);

            //     for(int c = 0; c < layer; c++) Blocks[c].CopyFrom(src.Blocks[c]);

            //     Pooler.CopyFrom(src.Pooler);

            //     Classifier.CopyFrom(src.Classifier);

            //     OutputLayer.CopyFrom(src.OutputLayer);
            //     OutputNorm.CopyFrom(src.OutputNorm);
            //     TokenBias.CopyFrom(src.TokenBias);
            // }

            public unsafe BertLargeModel(string meta_file, string bin_file, DeviceType device) : this(-1, 2, meta_file, bin_file, device) { }
            public unsafe BertLargeModel(int mcate, string meta_file, string bin_file, DeviceType device) : this(-1, mcate, meta_file, bin_file, device) { }
            public unsafe BertLargeModel(int frozenlayer, int mcate, string meta_file, string bin_file, DeviceType device) : this(mcate, 24, frozenlayer, device)
            {
                using(StreamReader metaReader = new StreamReader(meta_file))
                using(BinaryReader binReader = new BinaryReader(new FileStream(bin_file, FileMode.Open, FileAccess.Read)))
                {
                    while(!metaReader.EndOfStream)
                    {
                        string[] items = metaReader.ReadLine().Split('\t');
                        string itemName = items[0];
                        int itemSize = int.Parse(items[1]);

                        if(itemName.Equals("bert/embeddings/word_embeddings:0"))
                        {
                            TokenEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize TokenEmbed from checkpoint");
                            continue;
                        }
                        if(itemName.Equals("bert/embeddings/token_type_embeddings:0"))
                        {
                            TokenTypeEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize TokenTypeEmbed from checkpoint");
                            continue;
                        }
                        if(itemName.Equals("bert/embeddings/position_embeddings:0"))
                        {
                            PosEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize PosEmbed from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/LayerNorm/gamma:0"))
                        {
                            InputNorm.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize embedding-gamma from checkpoint");
                            continue;
                        }
                        if(itemName.Equals("bert/embeddings/LayerNorm/beta:0"))
                        {
                            InputNorm.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize embedding-beta from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/pooler/dense/kernel:0"))
                        {
                            Pooler.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize pooling-weight from checkpoint");
                            continue;
                        }
                        if(itemName.Equals("bert/pooler/dense/bias:0"))
                        {
                            Pooler.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize pooling-bias from checkpoint");
                            continue;
                        }

                        bool isFound = false;

                        for(int c = 0; c < layer; c++)
                        {
                            if(isFound) break;

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/query/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/query/bias:0"))
                            {
                                byte[] mem = binReader.ReadBytes(sizeof(float) * itemSize);
                                Blocks[c].QKVProject.bias.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                //Blocks[c].QProject.bias.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/key/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr + embed, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/key/bias:0"))
                            {
                                Blocks[c].QKVProject.bias.BlockCopy(sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/value/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr + 2 * embed, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/value/bias:0"))
                            {
                                Blocks[c].QKVProject.bias.BlockCopy(2 * sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/dense/kernel:0"))
                            {
                                Blocks[c].AttProject.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/dense/bias:0"))
                            {
                                Blocks[c].AttProject.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/LayerNorm/gamma:0"))
                            {
                                Blocks[c].Norm1.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/LayerNorm/beta:0"))
                            {
                                Blocks[c].Norm1.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/intermediate/dense/kernel:0"))
                            {
                                Blocks[c].MLP1.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/intermediate/dense/bias:0"))
                            {
                                Blocks[c].MLP1.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/dense/kernel:0"))
                            {
                                Blocks[c].MLP2.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/dense/bias:0"))
                            {
                                Blocks[c].MLP2.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/LayerNorm/gamma:0"))
                            {
                                Blocks[c].Norm2.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/LayerNorm/beta:0"))
                            {
                                Blocks[c].Norm2.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                        }
                        
                        if(!isFound)
                        {
                            Logger.WriteLog("Component is not found {0} : size {1}, skip...", itemName, itemSize);
                            binReader.ReadBytes(sizeof(float) * itemSize);
                        }

                    }
                }
                TokenEmbed.Embedding.SyncFromCPU();
                TokenTypeEmbed.Embedding.SyncFromCPU();
                PosEmbed.Embedding.SyncFromCPU();
                InputNorm.SyncFromCPU();
                Pooler.SyncFromCPU();
                for(int c = 0; c < layer; c++) Blocks[c].SyncFromCPU();
            }
        }


        public new BertLargeModel Model { get; set; }
        
        public SoftmaxObjRunner CERunner { get; set; }
        public SoftmaxObjRunner WordRunner { get; set; }

        public TextSamplingRunner BatchRunner = null;
        public SparseVectorData Labels { get; set; }

        public NdArrayData Featurer { get; set; }
        public NdArrayData PoolFeaturer { get; set; }
        public HiddenBatchData Slice { get; set; }
        public HiddenBatchData Pool { get; set; }
 
        public CudaPieceInt TokenIds { get; set; }
        public CudaPieceInt SegmentIds { get; set; }
        public CudaPieceInt MaskIds { get; set; }

        public CudaPieceFloat MaskScale { get; set; }
        public CudaPieceFloat MaskBias { get; set; }

        // words mask idx.
        public CudaPieceInt MaskWordsIdxs { get; set; }
        public SparseVectorData MaskWordsLabels { get; set; }

        public int MaxSeqLen { get; set; }
        public int MaxMaskLen { get; set; }

        public HiddenBatchData ClassLogit { get; set; }

        public LargeBert(BertLargeModel model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior;  
            Model = model;  
            SetDelegateModel(Model);
        }

        public float AvgLoss { get { return CERunner.AvgPerplexity; } }
        public float AvgAccuracy { get { return CERunner.AvgAccuracy; } }

        public void SetTrainSet(TextDataSet dataset)
        {
            SetTrainMode();
            if(BatchRunner != null) BatchRunner.SetDataset(dataset);
        }

        public void SetPredictSet(TextDataSet dataset)
        {
            SetPredictMode();
            if(BatchRunner != null) BatchRunner.SetDataset(dataset);
        }

        public void BuildFeatureBlock(int batchSize, int max_seq_len, int layer)
        {
            BuildFeatureBlock(batchSize, max_seq_len, layer, 1);
        }

        public void BuildFeatureBlock(int batchSize, int max_seq_len, int layer, int isLn)
        {
            int head = 16;
            int attdim_per_head = Model.embed / head;
            Logger.WriteLog("attention dim per head {0}", attdim_per_head);

            Logger.WriteLog("Build CG in data sampling.");

            HiddenBatchData w_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.TokenEmbed, Behavior));
            HiddenBatchData t_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(SegmentIds, SegmentIds.Size, Model.TokenTypeEmbed, Behavior));

            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);
            
            IntArgument[] shape = new IntArgument[] { emd_dim, sent_size, batch_size };

            IntArgument b_size = new IntArgument("b", 1);
            IntArgument[] p_shape = new IntArgument[] { emd_dim, sent_size, b_size};
            
            NdArrayData W_tensor = new NdArrayData(shape, w_embed.Output.Data, w_embed.Deriv.Data, Behavior.Device);
            NdArrayData T_tensor = new NdArrayData(shape, t_embed.Output.Data, t_embed.Deriv.Data, Behavior.Device);

            Logger.WriteLog("Build CG in word and type embeding.");
            NdArrayData P_tensor = new NdArrayData(p_shape, Model.PosEmbed.Embedding.SubArray(0, Model.embed * max_seq_len), 
                                                            Model.PosEmbed.EmbeddingGrad == null ? null : Model.PosEmbed.EmbeddingGrad.SubArray(0, Model.embed * max_seq_len), Behavior.Device);

            NdArrayData WT_tensor = W_tensor.Add(T_tensor, Session, Behavior);
            NdArrayData WTP_tensor = WT_tensor.AddExpand(P_tensor, Session, Behavior);

            NdArrayData normWTP = WTP_tensor;
            if(isLn == 1)
            {
                NdArrayData _normWTP = WTP_tensor.Norm(0, Session, Behavior);
                normWTP = _normWTP.DotAndAdd(Model.NormScale, Model.NormBias, Session, Behavior);
            }

            //normWTP = normWTP.Dropout(0.1f, Session, Behavior); 
            
            CudaPieceFloat scaleVec = new CudaPieceFloat(2, Behavior.Device);
            scaleVec[0] = 0;
            scaleVec[1] = (float)(1.0f / Math.Sqrt(attdim_per_head));
            scaleVec.SyncFromCPU();

            CudaPieceFloat biasVec = new CudaPieceFloat(2, Behavior.Device);
            biasVec[0] = (float)-1e9;
            biasVec[1] = 0;
            biasVec.SyncFromCPU();

            MaskScale = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskIds, MaskIds.Size, new EmbedStructure(2, 1, scaleVec, Behavior.Device), Behavior))).Output.Data; 
            MaskBias = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskIds, MaskIds.Size, new EmbedStructure(2, 1, biasVec, Behavior.Device), Behavior))).Output.Data; 

            Logger.WriteLog("Build CG in word and type and position embeding.");
            for(int i = 0; i < layer; i++)
            {
                TransformerRunner blockRunner = new TransformerRunner(Model.Blocks[i], head, max_seq_len, normWTP, MaskScale, MaskBias, isLn == 1, Behavior);
                AddRunner(blockRunner);
                normWTP = blockRunner.Output;
            }

            Featurer = normWTP;

            IntArgument firstSlice = new IntArgument("first-slice", 1);
            IntArgument offsetDim = new IntArgument("offset", 0);
            
            TensorSliceRunner sliceRunner = new TensorSliceRunner(Featurer, new IntArgument[] { offsetDim, offsetDim, offsetDim },
                                                                           new IntArgument[] { emd_dim, firstSlice, batch_size },
                                                                           Behavior);
            Logger.WriteLog("Build CG in slice runner.");

            AddRunner(sliceRunner);
            Slice = sliceRunner.Output.Reshape(new IntArgument[] { emd_dim, batch_size }).ToHDB();

            FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Pooler, Slice, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fcRunner);
            Pool = fcRunner.Output;
        }

        public void BuildMaskLM(int batchSize, int max_seq_len, int layer, int max_mask_len)
        {
            BuildMaskLM(batchSize, max_seq_len, layer, max_mask_len, 1);
        }

        public void BuildMaskLM(int batchSize, int max_seq_len, int layer, int max_mask_len, int isLn)
        {
            MaxSeqLen = max_seq_len;
            MaxMaskLen = max_mask_len;

            TokenIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            SegmentIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            MaskIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);

            // next sentence prediction labels.
            Labels = new SparseVectorData(batchSize, Behavior.Device, false);
            Labels.Value.Init(1);

            // mask word prediction labels.
            MaskWordsIdxs = new CudaPieceInt(batchSize * max_mask_len, Behavior.Device);
            MaskWordsLabels = new SparseVectorData(batchSize * max_mask_len, Behavior.Device, false);
            MaskWordsLabels.Value.Init(1);


            BuildFeatureBlock(batchSize, max_seq_len, layer, isLn);

            // FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Pooler, Slice, Behavior); // fc2Runner.Output, Behavior);
            // AddRunner(fcRunner);
            // Pool = fcRunner.Output;

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, Pool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            ClassLogit = classifierRunner.Output;

            CERunner = new SoftmaxObjRunner(Labels, ClassLogit, true, Behavior); 
            AddObjective(CERunner); 

            HiddenBatchData MaskWords = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskWordsIdxs, MaskWordsIdxs.Size, Featurer.Output, Featurer.Deriv, batchSize * max_seq_len, Featurer.Dimensions[0].Default, Behavior))); 

            NdArrayData mw = MaskWords.FNN(Model.OutputLayer, Session, Behavior).Act(A_Func.Gelu, Session, Behavior).ToND();

            NdArrayData _normw = mw.Norm(0, Session, Behavior);
            NdArrayData normw = _normw.DotAndAdd(Model.OutputNormScale, Model.OutputNormBias, Session, Behavior);

            MatrixProductRunner scoreProductRunner = new MatrixProductRunner(normw.ToMD(), new MatrixData(Model.TokenEmbed), Behavior);
            AddRunner(scoreProductRunner);
            HiddenBatchData score =  scoreProductRunner.Output.Add(Model.TBias, Session, Behavior).ToHDB();
            WordRunner = new SoftmaxObjRunner(MaskWordsLabels, score, false, Behavior);
            AddObjective(WordRunner); 
        }

        public unsafe float[] RunMaskLM(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr msk_pos, IntPtr msk_label, IntPtr msk_num, IntPtr label, int batchSize, bool isUpdate)
        {
            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(SegmentIds.CudaPtr, segIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen);
            
            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;

            int * p_label = (int * )label.ToPointer();
            {
                for(int iter = 0; iter < batchSize; iter ++)
                {
                    Labels.Idx[iter] = iter * 2 + (int)p_label[iter];
                }
            }
            Labels.Length = batchSize;
            Labels.Idx.SyncFromCPU();

            int * p_msk_pos = (int * )msk_pos.ToPointer();
            int * p_msk_label = (int *)msk_label.ToPointer();
            int * p_msk_num = (int *)msk_num.ToPointer();
            {
                int idx = 0;
                for(int iter = 0; iter < batchSize; iter ++)
                {
                    for(int i = 0; i < p_msk_num[iter]; i++)
                    {
                        MaskWordsIdxs[idx] = p_msk_pos[iter * MaxMaskLen + i] + iter * MaxSeqLen;
                        MaskWordsLabels.Idx[idx] = p_msk_label[iter * MaxMaskLen + i] + idx * Model.vocabSize;
                        idx += 1;
                    }
                }
                MaskWordsIdxs.EffectiveSize = idx;
                MaskWordsLabels.Length = idx;
            }
            MaskWordsIdxs.SyncFromCPU();
            MaskWordsLabels.Idx.SyncFromCPU();
            
            StepV3Run();

            if(isUpdate) StepV3Update();

            return new float[] {CERunner.CurrentPerplexity, WordRunner.CurrentPerplexity, CERunner.CurrentAcc * 1.0f/CERunner.CurrentSmp }; // WordRunner.CurrentPerplexity};
        }
        

        public void Build(int batchSize, int max_seq_len)
        {
            TokenIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            SegmentIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            MaskIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);

            Labels = new SparseVectorData(batchSize, Behavior.Device, false);
            Labels.Value.Init(1);

            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);

            BuildFeatureBlock(batchSize, max_seq_len, Model.layer);
            
            //FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Pooler, Slice, Behavior); // fc2Runner.Output, Behavior);
            //AddRunner(fcRunner);
            //HiddenBatchData interm = fcRunner.Output;

            PoolFeaturer = Pool.ToT4D().ToND();
            //interm = interm.Dropout(0.1f, Session, Behavior);

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, Pool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            HiddenBatchData o = classifierRunner.Output;
            
            //SparseVectorData label, HiddenBatchData output, RunnerBehavior behavior)
            CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
            //CrossEntropyRunner(Label, o, Behavior);
            AddObjective(CERunner); 
        }

        // Run langage model.
        // public float RunMaskLM(IntPtr tokenIds, IntPtr segmentIds, IntPtr tokenLen, int batchSize)
        // {
        //     return 0;
        // }

        public void BuildCG(int batchSize, int max_seq_len)
        {
            
            BatchRunner = new TextSamplingRunner(Model.vocabSize, Model.category, max_seq_len, batchSize,  Behavior);
            AddDataRunner(BatchRunner); 

            TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
            SegmentIds = BatchRunner.SegmentIds; //new SparseMatrixData(2, batchSize, batchSize * max_seq_len, Behavior.Device, false);
            MaskIds = BatchRunner.MaskIds;  

            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);

            BuildFeatureBlock(batchSize, max_seq_len, Model.layer);
            
            Labels = BatchRunner.Labels;
            //SparseVectorData Label = BatchRunner.Labels;
            
            //FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Pooler, Slice, Behavior); // fc2Runner.Output, Behavior);
            //AddRunner(fcRunner);
            //HiddenBatchData interm = fcRunner.Output;

            PoolFeaturer = Pool.ToT4D().ToND();
            //interm = interm.Dropout(0.1f, Session, Behavior);

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, Pool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            HiddenBatchData o = classifierRunner.Output;
            
            //SparseVectorData label, HiddenBatchData output, RunnerBehavior behavior)
            CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
            //CrossEntropyRunner(Label, o, Behavior);
            AddObjective(CERunner); 
        
        }
    }


    public class DistLargeBert
    {
        public LargeBert[] Nets { get; set; }
        int DeviceNum { get { return DeviceIds.Length; } }
        int[] DeviceIds { get; set; }
        IntPtr Comm { get; set; }
        CudaPieceFloat[] Grads { get; set; }

        public int MaxSeqLen { get; set; }
        public int Layer { get; set; }
        public int MaxMaskLen { get; set; }

        public unsafe DistLargeBert(LargeBert.BertLargeModel model, StructureLearner learner, int deviceNum)
        {
            DeviceIds = new int[deviceNum];
            for(int i = 0; i < deviceNum; i++) 
            {
                DeviceIds[i] = i;
            }
            fixed (int * pDevice = & DeviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, deviceNum);
            }

            Grads = new CudaPieceFloat[deviceNum];
            Nets = new LargeBert[deviceNum];
            Parallel.For(0, DeviceNum, i => 
            {
                int devId = DeviceIds[i];
                Logger.WriteLog("Create Device {0}", devId);
                DeviceType device = MathOperatorManager.SetDevice(devId);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

                LargeBert.BertLargeModel device_model = new LargeBert.BertLargeModel(model.category, model.layer, device);
                device_model.CopyFrom(model);

                Grads[i] = device_model.InitOptimizer(learner, behavior);

                Nets[i] = new LargeBert(device_model, behavior);
            });
        }

        public void BuildMaskLM(int batchSize, int max_seq_len, int layer, int max_mask_len)
        {
            BuildMaskLM(batchSize, max_seq_len, layer, max_mask_len, 1);
        }

        public void BuildMaskLM(int batchSize, int max_seq_len, int layer, int max_mask_len, int isLn)
        {
            MaxSeqLen = max_seq_len;
            Layer = layer;
            MaxMaskLen = max_mask_len;

            int subBatchSize = batchSize / DeviceNum;

            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].BuildMaskLM(subBatchSize, max_seq_len, layer, max_mask_len, isLn);
            });
        }

        public void Init()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Init();
            });
        }

        public void Complete()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Complete();
            });
        }

        public void SetTrainMode()
        {
            foreach(LargeBert net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Train;
            }
        }

        public void SetPredictMode()
        {
            foreach(LargeBert net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Predict;
            }
        } 

        public void SaveModel(string path)
        {
            Nets[0].Model.Save(path);
        }

        public unsafe float[] RunMaskLM(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr msk_pos, IntPtr msk_label, IntPtr msk_num, IntPtr label, int batchSize)
        {
            float[] loss1 = new float[DeviceNum];
            float[] loss2 = new float[DeviceNum];
            float[] loss3 = new float[DeviceNum];

            int subBatchSize = batchSize / DeviceNum;

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                float[] loss = Nets[i].RunMaskLM( 
                                    IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
                                    IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(msk_pos, sizeof(int) * i * subBatchSize * MaxMaskLen),
                                    IntPtr.Add(msk_label, sizeof(int) * i * subBatchSize * MaxMaskLen),
                                    IntPtr.Add(msk_num, sizeof(int) * i * subBatchSize),
                                    IntPtr.Add(label, sizeof(int) * i * subBatchSize),
                                    subBatchSize, false);

                if(Nets[i].Behavior.RunMode == DNNRunMode.Train)
                {    
                    Cudalib.AllReduce(Grads[i].CudaPtr, Grads[i].Size, i, Comm);
                    Nets[i].StepV3Update();
                }
                loss1[i] = loss[0];
                loss2[i] = loss[1];
                loss3[i] = loss[2];
            });
            return new float[] { loss1.Average(), loss2.Average(), loss3.Average() };
        }

    }

}
