using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    public class BertFeatureBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("BERT-LAYER", new ParameterArgument("12", "pre-trained bert layers."));

                Argument.Add("OUTPUT",new ParameterArgument(string.Empty, "output feature file."));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }
            public static int BERT_LAYER { get { return int.Parse(Argument["BERT-LAYER"].Value); } }

            public static string OutputFile { get { return Argument["OUTPUT"].Value; } }

        }

        public override BuilderType Type { get { return BuilderType.BERT_FEATUER_EXTRACTOR; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        class NdArrayDumpRunner : StructRunner
        {
            string Path { get; set; }
            StreamWriter Writer { get; set; }
            NdArrayData Data { get; set; }
            public NdArrayDumpRunner(NdArrayData data, string path, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                Path = path;
            }

            public override void Init()
            {
                Writer = new StreamWriter(Path);
            }

            public override void Complete()
            {
                Writer.Close();
            }

            public override void Forward()
            {
                Data.Output.SyncToCPU();
                int dim = Data.Dimensions[0].Value;
                int batch = Data.Length / dim;
                for(int i = 0 ; i < batch; i++)
                {
                    Writer.WriteLine(string.Join(",", Data.Output.TakeCPU(i * dim, dim)));
                }
                Writer.WriteLine();
            }
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data ...");

            TextDataSet featureSet = new TextDataSet(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");
            
            Bert.BertBaseModel baseModel = 
                    BuilderParameters.BERT_SEED.Equals(string.Empty) ? new Bert.BertBaseModel(BuilderParameters.BERT_LAYER, device):
                                                                       new Bert.BertBaseModel(BuilderParameters.BERT_SEED + @"/exp.label", BuilderParameters.BERT_SEED + @"/exp.bin", device);

            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }; 
            //baseModel.InitOptimizer(behavior);

            Bert bertRunner = new Bert(baseModel, behavior);
            bertRunner.BuildCG(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ); //, BuilderParameters.BERT_LAYER);
            bertRunner.AddRunner(new NdArrayDumpRunner(bertRunner.PoolFeaturer, BuilderParameters.OutputFile, behavior));

            bertRunner.SetPredictSet(featureSet);
            bertRunner.Execute(false);
            
        }
    }
}
