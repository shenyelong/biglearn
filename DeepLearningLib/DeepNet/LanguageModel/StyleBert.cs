using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Language
{
    unsafe public class StyleBert : ComputationGraph
    {
        public string name = "StyleBertModel";

        public class StyleBertModel : CompositeNNStructure
        {
            public int vocabSize = 30522;
            public int pos = 512;
            public int segType = 2;
            public int embed = 768;
            public int layer = 12;

            public int category = 2;

            // token embedding structure.
            public EmbedStructure TokenEmbed { get; set; }

            public EmbedStructure PosEmbed { get; set; }

            public EmbedStructure TokenTypeEmbed { get; set; }

            public LayerStructure InputNorm { get; set; }

            VectorData mScale = null;
            public VectorData NormScale { get { if(mScale == null) mScale = new VectorData(embed, InputNorm.weight, InputNorm.WeightGrad, InputNorm.DeviceType); return mScale; } }
            
            VectorData mBias = null;
            public VectorData NormBias { get { if(mBias == null) mBias = new VectorData(embed, InputNorm.bias, InputNorm.BiasGrad, InputNorm.DeviceType); return mBias; } }

            //intermedia Blocks.
            public TransformerStructure[] Blocks { get; set; }

            public LayerStructure Pooler { get; set; }
            public LayerStructure Classifier { get; set; }
            
            public StyleBertModel(int mcate, int mlayer, DeviceType device)
            {
                layer = mlayer;
                category = mcate;

                TokenEmbed = AddLayer(new EmbedStructure(vocabSize, embed, device)); // -0.034f, 0.034f, device)); 
                PosEmbed = AddLayer(new EmbedStructure(pos, embed, device)); // -0.034f, 0.034f, device)); 
                TokenTypeEmbed = AddLayer(new EmbedStructure(segType, embed, device)); // -0.034f, 0.034f, device));

                InputNorm = AddLayer(new LayerStructure(1, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                InputNorm.weight.Init(1);
                InputNorm.bias.Init(0);

                Blocks = new TransformerStructure[layer];
                for(int i=0; i < layer; i++)
                {
                    Blocks[i] = AddLayer(new TransformerStructure(embed, device));   
                }

                Pooler = AddLayer(new LayerStructure(embed, embed, A_Func.Tanh, N_Type.Convolution_layer, 1, 0, true, device));
                Pooler.bias.Init(0);
                Pooler.weight.Init(2 * 0.034f, -0.034f);

                Classifier = AddLayer(new LayerStructure(embed, category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                //float scale, float bias) 
                Classifier.weight.Init(2 * 0.034f, -0.034f);
                //Classifier.weight.Init(0);
                Classifier.bias.Init(0);
            }

            // load model from pretrained model.
            public unsafe StyleBertModel(int mcate, string meta_file, string bin_file, DeviceType device) : this(mcate, 12, device)
            {
                using(StreamReader metaReader = new StreamReader(meta_file))
                using(BinaryReader binReader = new BinaryReader(new FileStream(bin_file, FileMode.Open, FileAccess.Read)))
                {
                    while(!metaReader.EndOfStream)
                    {
                        string[] items = metaReader.ReadLine().Split('\t');
                        string itemName = items[0];
                        int itemSize = int.Parse(items[1]);

                        if(itemName.Equals("bert/embeddings/word_embeddings:0"))
                        {
                            TokenEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize TokenEmbed from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/token_type_embeddings:0"))
                        {
                            TokenTypeEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize TokenTypeEmbed from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/position_embeddings:0"))
                        {
                            PosEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize PosEmbed from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/LayerNorm/gamma:0"))
                        {
                            InputNorm.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize embedding-gamma from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/LayerNorm/beta:0"))
                        {
                            InputNorm.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize embedding-beta from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/pooler/dense/kernel:0"))
                        {
                            Pooler.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize pooling-weight from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/pooler/dense/bias:0"))
                        {
                            Pooler.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize pooling-bias from checkpoint");
                            continue;
                        }

                        // skip.
                        if(itemName.Equals("cls/predictions/output_bias:0"))
                        {
                            TokenBias.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/output_bias:0");
                            continue;
                        }
                        // skip ZN661XUS7JX297447
                        if(itemName.Equals("cls/predictions/transform/LayerNorm/beta:0"))
                        {
                            OutputNorm.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/transform/LayerNorm/beta:0");
                            continue;
                        }
                        if(itemName.Equals("cls/predictions/transform/LayerNorm/gamma:0"))
                        {
                            OutputNorm.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/transform/LayerNorm/gamma:0");
                            continue;
                        }
                        if(itemName.Equals("cls/predictions/transform/dense/bias:0"))
                        {
                            OutputLayer.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/transform/dense/bias:0");
                            continue;
                        }
                        if(itemName.Equals("cls/predictions/transform/dense/kernel:0"))
                        {
                            OutputLayer.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            //binReader.ReadBytes(sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize cls/predictions/transform/dense/kernel:0");
                            continue;
                        }

                        // if(category == 2)
                        // {
                        //     // next sentence prediction weight.
                        //     if(itemName.Equals("cls/seq_relationship/output_bias:0"))
                        //     {
                        //         Classifier.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                        //         Logger.WriteLog("Initialize output-bias from checkpoint {0}", itemSize);
                        //         continue;
                        //     }
                        //     // next sentence prediction weight.
                        //     if(itemName.Equals("cls/seq_relationship/output_weights:0"))
                        //     {
                        //        Classifier.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                        //        BasicMathlib.Transpose(Classifier.weight.CpuPtr, 2, embed);
                        //        Logger.WriteLog("Initialize output-weight from checkpoint {0}", itemSize);
                        //        continue;
                        //     }
                        // }
                        
                        bool isFound = false;
                        for(int c = 0; c < layer; c++)
                        {
                            if(isFound) break;
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/query/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                //Blocks[c].QKVProject.weight.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                //Buffer.BlockCopy(src, offset, MemPtr, Offset * sizeof(float), length);
                                //Blocks[c].QProject.weight.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;

                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/query/bias:0"))
                            {
                                byte[] mem = binReader.ReadBytes(sizeof(float) * itemSize);
                                Blocks[c].QKVProject.bias.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                //Blocks[c].QProject.bias.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/key/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr + embed, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                //Blocks[c].QKVProject.weight.BlockCopy(sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/key/bias:0"))
                            {
                                Blocks[c].QKVProject.bias.BlockCopy(sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/value/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr + 2 * embed, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                //Blocks[c].QKVProject.weight.BlockCopy(2 * sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/value/bias:0"))
                            {
                                Blocks[c].QKVProject.bias.BlockCopy(2 * sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/dense/kernel:0"))
                            {
                                Blocks[c].AttProject.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/dense/bias:0"))
                            {
                                Blocks[c].AttProject.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/LayerNorm/gamma:0"))
                            {
                                Blocks[c].Norm1.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/LayerNorm/beta:0"))
                            {
                                Blocks[c].Norm1.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/intermediate/dense/kernel:0"))
                            {
                                Blocks[c].MLP1.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/intermediate/dense/bias:0"))
                            {
                                Blocks[c].MLP1.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/dense/kernel:0"))
                            {
                                Blocks[c].MLP2.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/dense/bias:0"))
                            {
                                Blocks[c].MLP2.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/LayerNorm/gamma:0"))
                            {
                                Blocks[c].Norm2.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/LayerNorm/beta:0"))
                            {
                                Blocks[c].Norm2.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                        }
                        if(!isFound)
                        {
                            Logger.WriteLog("Component is not found {0} : size {1}, skip...", itemName, itemSize);
                            binReader.ReadBytes(sizeof(float) * itemSize);
                        }
                    }
                }
                TokenEmbed.Embedding.SyncFromCPU();
                TokenTypeEmbed.Embedding.SyncFromCPU();
                PosEmbed.Embedding.SyncFromCPU();
                InputNorm.SyncFromCPU();
                
                Pooler.SyncFromCPU();
                //Classifier.SyncFromCPU();

                //OutputLayer.SyncFromCPU();
                //OutputNorm.SyncFromCPU();
                //TokenBias.SyncFromCPU();
                for(int c = 0; c < layer; c++) Blocks[c].SyncFromCPU();
            }
        }

        public new StyleBertModel Model { get; set; }
        
        public CudaPieceInt TokenIds { get; set; }
        public CudaPieceInt SegmentIds { get; set; }
        public CudaPieceInt MaskIds { get; set; }

        public SparseVectorData Labels { get; set; }

        public CudaPieceFloat MaskScale { get; set; }
        public CudaPieceFloat MaskBias { get; set; }

        SoftmaxObjRunner CERunner { get; set; }

        public NdArrayData Featurer { get; set; }
        public HiddenBatchData Slice { get; set; }
        public HiddenBatchData Pool { get; set; }
        public HiddenBatchData ClassLogit { get; set; }

        public NdArrayData ConFeaturer { get; set; }

        public int MaxSeqLen { get; set; }

        public StyleBert(StyleBertModel model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior;  
            Model = model;  
            SetDelegateModel(Model);
        }

        // prequest TokenIds, SegmentIds, MaskIds.
        public void BuildBertModel(int batchSize, int max_seq_len)
        {
            Logger.WriteLog("Build Computation Graph for Bert.");

            MaxSeqLen = max_seq_len;

            Logger.WriteLog("Word and Type embeding module.");
            HiddenBatchData w_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.TokenEmbed, Behavior));
            HiddenBatchData t_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(SegmentIds, SegmentIds.Size, Model.TokenTypeEmbed, Behavior));

            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);
            IntArgument[] shape = new IntArgument[] { emd_dim, sent_size, batch_size };
            
            NdArrayData W_tensor = new NdArrayData(shape, w_embed.Output.Data, w_embed.Deriv.Data, Behavior.Device);
            NdArrayData T_tensor = new NdArrayData(shape, t_embed.Output.Data, t_embed.Deriv.Data, Behavior.Device);

            Logger.WriteLog("Position embeding module.");
            IntArgument b_size = new IntArgument("b", 1);
            IntArgument[] p_shape = new IntArgument[] { emd_dim, sent_size, b_size};

            NdArrayData P_tensor = new NdArrayData(p_shape, Model.PosEmbed.Embedding.SubArray(0, Model.embed * max_seq_len), 
                                                            Model.PosEmbed.EmbeddingGrad == null ? null : Model.PosEmbed.EmbeddingGrad.SubArray(0, Model.embed * max_seq_len), Behavior.Device);

            Logger.WriteLog("Combine Word Type and Position Embedding.");
            NdArrayData WT_tensor = W_tensor.Add(T_tensor, Session, Behavior);
            NdArrayData WTP_tensor = WT_tensor.AddExpand(P_tensor, Session, Behavior);

            NdArrayData _normWTP = WTP_tensor.Norm(0, Session, Behavior);
            NdArrayData normWTP = _normWTP.DotAndAdd(Model.NormScale, Model.NormBias, Session, Behavior);
            
            int head = 12;
            int attdim_per_head = Model.embed / head;
            Logger.WriteLog("attention dim per head {0}", attdim_per_head);

            //normWTP = normWTP.Dropout(0.1f, Session, Behavior); 
            CudaPieceFloat scaleVec = new CudaPieceFloat(2, Behavior.Device);
            scaleVec[0] = 0;
            scaleVec[1] = (float)(1.0f / Math.Sqrt(attdim_per_head));
            scaleVec.SyncFromCPU();

            CudaPieceFloat biasVec = new CudaPieceFloat(2, Behavior.Device);
            biasVec[0] = (float)-1e9;
            biasVec[1] = 0;
            biasVec.SyncFromCPU();

            MaskScale = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskIds, MaskIds.Size, new EmbedStructure(2, 1, scaleVec, Behavior.Device), Behavior))).Output.Data; 
            MaskBias = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskIds, MaskIds.Size, new EmbedStructure(2, 1, biasVec, Behavior.Device), Behavior))).Output.Data; 

            Logger.WriteLog("Build Transformer for Language.");
            for(int i = 0; i < Model.layer; i++)
            {
                TransformerRunner blockRunner = new TransformerRunner(Model.Blocks[i], head, max_seq_len, normWTP, MaskScale, MaskBias, Behavior);
                AddRunner(blockRunner);
                normWTP = blockRunner.Output;
            }

            Featurer = normWTP;

            IntArgument firstSlice = new IntArgument("first-slice", 1);
            IntArgument offsetDim = new IntArgument("offset", 0);
            
            TensorSliceRunner sliceRunner = new TensorSliceRunner(Featurer, new IntArgument[] { offsetDim, offsetDim, offsetDim },
                                                                           new IntArgument[] { emd_dim, firstSlice, batch_size },
                                                                           Behavior);
            Logger.WriteLog("Build CG in slice runner.");

            AddRunner(sliceRunner);
            Slice = sliceRunner.Output.Reshape(new IntArgument[] { emd_dim, batch_size }).ToHDB();
        }

        // needs to . 
        public unsafe void RunStyleTransfer(IntPtr tokenIdx, IntPtr maskIdx, IntPtr targetStyles, int batchSize)
        {
            // obtain the batchsize.
            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen);

            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;


            Behavior.Computelib.LookupForward(CudaPieceInt.Empty, TokenIds, TokenIds.EffectiveSize, Model.TokenEmbed, Output.Output.Data, EmbedVocabSize, EmbedDim);

            StepV3Run();
            Cudalib.CudaCopy(ConFeaturer.Output.CudaPtr, Featurer.Output.CudaPtr, batchSize * MaxSeqLen * Model.embed, 0, 0);

            for(int i = 0; i < 100; i++)
            {   
                
            }
            // context feature.
            //Cudalib.CopyFrom()
            

            Cudalib.CudaCopyOutFloat(Pool.Output.Data.CudaPtr, clsPool, batchSize * Model.embed);
            
        }

        public unsafe void RunSentenceLogit(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr sentLogit, int batchSize)
        {
            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(SegmentIds.CudaPtr, segIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen);

            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;


            StepV3Run();

            Cudalib.CudaCopyOutFloat(ClassLogit.Output.Data.CudaPtr, sentLogit, batchSize * 2);
            
        }

        public unsafe float[] RunMaskLM(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr msk_pos, IntPtr msk_label, IntPtr msk_num, IntPtr label, int batchSize)
        {
            return RunMaskLM(tokenIdx, segIdx, maskIdx, msk_pos, msk_label, msk_num, label, batchSize, false);
        }

        public unsafe float[] RunMaskLM(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr msk_pos, IntPtr msk_label, IntPtr msk_num, IntPtr label, int batchSize, bool isUpdate)
        {
            //Cudalib.CudaInit(Behavior.Device);

            //CudaCopyInInt(IntPtr gpu_ints, IntPtr int_array, int len); 
            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(SegmentIds.CudaPtr, segIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen);
            
            int * p_label = (int * )label.ToPointer();
            {
                for(int iter = 0; iter < batchSize; iter ++)
                {
                    Labels.Idx[iter] = iter * 2 + (int)p_label[iter];
                    //Console.Write(Labels.Idx[iter].ToString()+" ");
                }
                //Console.Write("\n");
            }
            
            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;
            
            Labels.Length = batchSize;
            Labels.Idx.SyncFromCPU();

            int * p_msk_pos = (int * )msk_pos.ToPointer();
            int * p_msk_label = (int *)msk_label.ToPointer();
            int * p_msk_num = (int *)msk_num.ToPointer();
            {
                int idx = 0;
                for(int iter = 0; iter < batchSize; iter ++)
                {
                    for(int i = 0; i < p_msk_num[iter]; i++)
                    {
                        MaskWordsIdxs[idx] = p_msk_pos[iter * MaxMaskLen + i] + iter * MaxSeqLen;
                        MaskWordsLabels.Idx[idx] = p_msk_label[iter * MaxMaskLen + i] + idx * Model.vocabSize;
                        idx += 1;
                        //Console.Write(p_msk_label[iter * MaxMaskLen + i]+ " ");
                    }
                    //Console.Write("\n");
                }
                MaskWordsIdxs.EffectiveSize = idx;
                MaskWordsLabels.Length = idx;
            }
            MaskWordsIdxs.SyncFromCPU();
            MaskWordsLabels.Idx.SyncFromCPU();
            
            StepV3Run();

            if(isUpdate) StepV3Update();

            return new float[] {CERunner.CurrentPerplexity, WordRunner.CurrentPerplexity, CERunner.CurrentAcc * 1.0f/CERunner.CurrentSmp }; // WordRunner.CurrentPerplexity};
        }

        public int MaxSeqLen { get; set; }
        public int MaxMaskLen { get; set; }

        public void BuildMaskLM(int batchSize, int max_seq_len, int layer, int max_mask_len)
        {
            MaxSeqLen = max_seq_len;
            MaxMaskLen = max_mask_len;

            TokenIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            SegmentIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            MaskIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);

            //int maxLength, DeviceType device, bool isDeriv = true 
            Labels = new SparseVectorData(batchSize, Behavior.Device, false);
            Labels.Value.Init(1);

            BuildBertModel(batchSize, max_seq_len, layer);

            // next sentence prediction .
            FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Pooler, Slice, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fcRunner);
            Pool = fcRunner.Output;

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, Pool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            ClassLogit = classifierRunner.Output;

            CERunner = new SoftmaxObjRunner(Labels, ClassLogit, true, Behavior); 
            AddObjective(CERunner); 

            // mask word prediction.
            MaskWordsIdxs = new CudaPieceInt(batchSize * max_mask_len, Behavior.Device);
            MaskWordsLabels = new SparseVectorData(batchSize * max_mask_len, Behavior.Device, false);
            MaskWordsLabels.Value.Init(1);

            HiddenBatchData MaskWords = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskWordsIdxs, MaskWordsIdxs.Size, Featurer.Output, Featurer.Deriv, batchSize * max_seq_len, Featurer.Dimensions[0].Default, Behavior))); 

            NdArrayData mw = MaskWords.FNN(Model.OutputLayer, Session, Behavior).Act(A_Func.Gelu, Session, Behavior).ToND();

            NdArrayData _normw = mw.Norm(0, Session, Behavior);
            NdArrayData normw = _normw.DotAndAdd(Model.OutputNormScale, Model.OutputNormBias, Session, Behavior);

            MatrixProductRunner scoreProductRunner = new MatrixProductRunner(normw.ToMD(), new MatrixData(Model.TokenEmbed), Behavior);
            AddRunner(scoreProductRunner);
            HiddenBatchData score =  scoreProductRunner.Output.Add(Model.TBias, Session, Behavior).ToHDB();
            WordRunner = new SoftmaxObjRunner(MaskWordsLabels, score, false, Behavior);
            AddObjective(WordRunner); 
        }

        public void BuildFeatureBlock(int batchSize, int max_seq_len, int layer)
        {
            BatchRunner = new TextSamplingRunner(Model.vocabSize, Model.category, max_seq_len, batchSize, Behavior);
            AddDataRunner(BatchRunner);

            //SparseMatrixData TokenIdxs { get; set; }
            //SparseMatrixData SegmentIds { get; set; }
            //SparseMatrixData MaskIds { get; set; }
            //  (float)(1.0f / Math.Sqrt(attdim_per_head)), (float)-1e9, 
            TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
            SegmentIds = BatchRunner.SegmentIds; //new SparseMatrixData(2, batchSize, batchSize * max_seq_len, Behavior.Device, false);
            MaskIds = BatchRunner.MaskIds;

            BuildBertModel(batchSize, max_seq_len, layer);
        }

        public void BuildCG(int batchSize, int max_seq_len)
        {
            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);

            BuildFeatureBlock(batchSize, max_seq_len, Model.layer);
            
            SparseVectorData Label = BatchRunner.Labels;
            
            FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Pooler, Slice, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(fcRunner);
            Pool = fcRunner.Output;

            PoolFeaturer = Pool.ToT4D().ToND();
            //interm = interm.Dropout(0.1f, Session, Behavior);

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, Pool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            ClassLogit = classifierRunner.Output;
            
            //SparseVectorData label, HiddenBatchData output, RunnerBehavior behavior)
            CERunner = new SoftmaxObjRunner(Label, ClassLogit, true, Behavior); 
            //CrossEntropyRunner(Label, o, Behavior);
            AddObjective(CERunner); 
        
        }
    }


    // public class DistStyleBert
    // {
    //     public Bert[] Nets { get; set; }
    //     int DeviceNum { get { return DeviceIds.Length; } }
    //     int[] DeviceIds { get; set; }

    //     IntPtr Comm { get; set; }
    //     CudaPieceFloat[] Grads { get; set; }


    //     public unsafe DistStyleBert(Bert.BertBaseModel model, StructureLearner learner, int deviceNum)
    //     {
    //         DeviceIds = new int[deviceNum];
    //         for(int i = 0; i < deviceNum; i++) DeviceIds[i] = i;

    //         fixed (int * pDevice = & DeviceIds[0])
    //         {
    //             Comm = Cudalib.CommInitAll((IntPtr)pDevice, deviceNum);
    //         }
    //         Grads = new CudaPieceFloat[deviceNum];
    //         Nets = new Bert[deviceNum];

    //         // int subBatchSize = batchSize / DeviceNum;
            
    //         //int idx = 0;
    //         Parallel.For(0, DeviceNum, i =>  //foreach(int devId in DeviceIds)
    //         {
    //             int devId = DeviceIds[i];

    //             Logger.WriteLog("Create Device {0}", devId);
    //             DeviceType device = MathOperatorManager.SetDevice(devId);
    //             IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
    //             RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

    //             //nt mcate, int mlayer, DeviceType device
    //             Bert.BertBaseModel device_model = new Bert.BertBaseModel(model.category, model.layer, device);
    //             device_model.CopyFrom(model);

    //             //StructureLearner learner, RunnerBehavior behavior)
    //             // initoptimizer;

    //             // InitOptimizer(StructureLearner learner, RunnerBehavior behavior) 
    //             Grads[i] = device_model.InitOptimizer(learner, behavior);
    //             //Logger.WriteLog("gradient size {0}", Grads[idx].Size);
    //             // Bert(model, env_mode)
    //             Nets[i] = new Bert(device_model, behavior);
    //             //net.BuildCG(subBatchSize);
                
    //             //idx += 1;
    //         });
    //     }

    //     public int MaxSeqLen { get; set; }
    //     public int Layer { get; set; }
    //     public int MaxMaskLen { get; set; }

    //     public void BuildMaskLM(int batchSize, int max_seq_len, int layer, int max_mask_len)
    //     {
    //         MaxSeqLen = max_seq_len;
    //         Layer = layer;
    //         MaxMaskLen = max_mask_len;

    //         int subBatchSize = batchSize / DeviceNum;
    //         //int idx = 0;
    //         //foreach(Bert net in Nets)
    //         Parallel.For(0, DeviceNum, i =>
    //         {
    //             Cudalib.CudaInit(DeviceIds[i]);
    //             Nets[i].BuildMaskLM(subBatchSize, max_seq_len, layer, max_mask_len);
    //         });
    //     }

    //     public void Init()
    //     {
    //         Parallel.For(0, DeviceNum, i =>
    //         {
    //             Cudalib.CudaInit(DeviceIds[i]);
    //             Nets[i].Init();
    //         });
    //     }

    //     public void Complete()
    //     {
    //         Parallel.For(0, DeviceNum, i =>
    //         {
    //             Cudalib.CudaInit(DeviceIds[i]);
    //             Nets[i].Complete();
    //         });
    //     }

    //     public void SetTrainMode()
    //     {
    //         foreach(Bert net in Nets)
    //         {
    //             net.Behavior.RunMode = DNNRunMode.Train;
    //         }
    //     }

    //     public void SetPredictMode()
    //     {
    //         foreach(Bert net in Nets)
    //         {
    //             net.Behavior.RunMode = DNNRunMode.Predict;
    //         }
    //     } 

    //     public void SaveModel(string path)
    //     {
    //         Nets[0].Model.Save(path);
    //     }

    //     public unsafe void RunClsPool(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx,  IntPtr clsPool, int batchSize)
    //     {
    //         int subBatchSize = batchSize / DeviceNum;

    //         Parallel.For(0, DeviceNum, i => 
    //         //int i = 1;
    //         {
    //             //Logger.WriteLog("Device {0} Start Set Device", i);
    //             Cudalib.CudaInit(DeviceIds[i]);

    //             //Logger.WriteLog("Device {0} Start Run", i);
                
    //             Nets[i].RunClsPool(IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
    //                                 IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
    //                                 IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
    //                                 IntPtr.Add(clsPool, sizeof(float) * i * subBatchSize * 768),
    //                                 subBatchSize);
    //         });
    //     }

    //     public unsafe void RunSentenceLogit(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr sentLogit, int batchSize)
    //     {
    //         int subBatchSize = batchSize / DeviceNum;

    //         Parallel.For(0, DeviceNum, i => 
    //         //int i = 1;
    //         {
    //             //Logger.WriteLog("Device {0} Start Set Device", i);
    //             Cudalib.CudaInit(DeviceIds[i]);

    //             //Logger.WriteLog("Device {0} Start Run", i);
                
    //             Nets[i].RunSentenceLogit(IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
    //                                 IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
    //                                 IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
    //                                 IntPtr.Add(sentLogit, sizeof(float) * i * subBatchSize * 2),
    //                                 subBatchSize);
    //         });
    //     }

    //     public unsafe float[] RunMaskLM(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr msk_pos, IntPtr msk_label, IntPtr msk_num, IntPtr label, int batchSize)
    //     //public float RunMaskLM(IntPtr batch, IntPtr target, IntPtr o, int idx, int batchSize)
    //     {
    //         //int memsize = 224 * 224 * 3 * batchSize;
    //         //int subBatchSize = batchSize / DeviceNum;
    //         //int subSize = 224 * 224 * 3 * subBatchSize;
    //         float[] loss1 = new float[DeviceNum];
    //         float[] loss2 = new float[DeviceNum];
    //         float[] loss3 = new float[DeviceNum];

    //         //Logger.WriteLog("batch size {0}", subBatchSize);
    //         //IntPtr.Add(cudaPiecePointer, sizeof(float) * offset)
            
    //         int subBatchSize = batchSize / DeviceNum;
    //         //Logger.WriteLog("Total Device Num {0} ", DeviceNum);

    //         Parallel.For(0, DeviceNum, i => 
    //         //int i = 1;
    //         {
    //             //Logger.WriteLog("Device {0} Start Set Device", i);
    //             Cudalib.CudaInit(DeviceIds[i]);

    //             //Logger.WriteLog("Device {0} Start Run", i);
                
    //             float[] loss = Nets[i].RunMaskLM( IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),  
    //                                 IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
    //                                 IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
    //                                 IntPtr.Add(msk_pos, sizeof(int) * i * subBatchSize * MaxMaskLen),
    //                                 IntPtr.Add(msk_label, sizeof(int) * i * subBatchSize * MaxMaskLen),
    //                                 IntPtr.Add(msk_num, sizeof(int) * i * subBatchSize),
    //                                 IntPtr.Add(label, sizeof(int) * i * subBatchSize),
    //                                 subBatchSize, false);

    //             //Grads[i].Print("all reduce before " + i.ToString(), 100, true);
    //             if(Nets[i].Behavior.RunMode == DNNRunMode.Train)
    //             {    
    //                 //Logger.WriteLog("Device {0} Before Reduce", i);
    //                 Cudalib.AllReduce(Grads[i].CudaPtr, Grads[i].Size, i, Comm);
    //                 //Logger.WriteLog("Device {0} After Reduce", i);
    //                 Nets[i].StepV3Update();
    //             }
    //             //Cudalib.Zero(Grads[i].CudaPtr, Grads[i].Size);
    //             loss1[i] = loss[0];
    //             loss2[i] = loss[1];
    //             loss3[i] = loss[2];
    //             // all reduce happen here.
    //         });
    //         return new float[] { loss1.Average(), loss2.Average(), loss3.Average() };
    //     }

    // }

}
