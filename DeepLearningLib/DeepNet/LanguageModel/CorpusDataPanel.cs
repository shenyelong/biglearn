using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;

namespace BigLearn.DeepNet.Language
{
        public class CorpusDataPanel 
        {
            public static List<int> TrainData { get; set; }
            public static List<int> TestData { get; set; }
            public static List<int> ValidData { get; set; }
            
            public static int VocabSize { get; set; }
            public static List<int> LoadData(string fileName)
            {
                List<int> chars = new List<int>();
                using(StreamReader reader = new StreamReader(fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Trim().Split(' ');
                        foreach(string s in items)
                        {
                            if(s.Trim().Equals("")) continue;
                            int c = int.Parse(s);
                            chars.Add(c);
                        }
                    }
                }
                return chars;
            }
            // build device graph.
            public static void Init(string dataDir)
            {
                if(File.GetAttributes(dataDir).HasFlag(FileAttributes.Directory))
                {
                    TrainData = LoadData(dataDir + @"/train.txt");
                    TestData = LoadData(dataDir + @"/test.txt");
                    ValidData = LoadData(dataDir + @"/valid.txt");
                    VocabSize = new int[] { TrainData.Max(), TestData.Max(), ValidData.Max() }.Max() + 1;
                    Logger.WriteLog("Vocab Word Size {0}", VocabSize);
                }
                else
                {
                    TrainData = LoadData(dataDir);
                }
                Logger.WriteLog("Train Word Size {0}", TrainData.Count);
                
                
            }

        }
}
