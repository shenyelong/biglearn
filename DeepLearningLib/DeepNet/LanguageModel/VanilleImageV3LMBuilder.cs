using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    public class VanilleImageV3LMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("IS-SHARE-EMD", new ParameterArgument("0", "0:no share embedding; 1:share embedding."));
                Argument.Add("W-EMBED", new ParameterArgument("300", "word embedding dimension."));
                
                //Argument.Add("P-EMBED", new ParameterArgument("100", "word embedding dimension."));
                Argument.Add("P-LEN", new ParameterArgument("256", "sentence length."));
                
                Argument.Add("T-LAYER", new ParameterArgument("8", "Transformer Layer."));
                Argument.Add("T-DIM", new ParameterArgument("1024", "Transformer Dimension."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("HISTORY", new ParameterArgument("1", "History words been seen."));
                
                //Argument.Add("STRIDE_W", new ParameterArgument("1", "History words been seen."));
                Argument.Add("CONV-HEAD", new ParameterArgument("8", "number of conv head."));
                Argument.Add("FDEPTH", new ParameterArgument("96", "filter depth."));

                Argument.Add("CNN-INIT-RANGE", new ParameterArgument("0", "convolutional init range."));
                Argument.Add("IS-LAYER-NORM", new ParameterArgument("1", "adding layer normalization in the model."));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }

            public static int IS_SHARE_EMD { get { return int.Parse(Argument["IS-SHARE-EMD"].Value); } }
            
            public static int W_EMBED { get { return int.Parse(Argument["W-EMBED"].Value); } }
            //public static int P_EMBED { get { return int.Parse(Argument["P-EMBED"].Value); } }
            public static int P_LEN { get { return int.Parse(Argument["P-LEN"].Value); } }

            public static int T_LAYER { get { return int.Parse(Argument["T-LAYER"].Value); } }
            public static int T_DIM { get { return int.Parse(Argument["T-DIM"].Value); } }
            
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int HISTORY { get { return int.Parse(Argument["HISTORY"].Value); } }

            public static int CONV_HEAD { get { return int.Parse(Argument["CONV-HEAD"].Value); } }
            public static int FilterDepth { get { return int.Parse(Argument["FDEPTH"].Value); } }

            public static float CNN_INIT_RANGE { get { return float.Parse(Argument["CNN-INIT-RANGE"].Value); } }
            public static int IS_LAYER_NORM { get { return int.Parse(Argument["IS-LAYER-NORM"].Value); } }
        
        }

        public override BuilderType Type { get { return BuilderType.LANGUAGE_IMAGE_LM_V3; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }
        
        public class TransformerLMModel : CompositeNNStructure
        {
            public EmbedStructure WordInEmbed { get; set; }
            public EmbedStructure WordOutEmbed { get; set; }
            
            public TensorConvStructure[] Conv { get; set; }
            
            public TransformerStructure[] Blocks { get; set; }

            public DNNStructure Decoder { get; set; }

            public LayerStructure InputNorm { get; set; }

            VectorData mScale = null;
            
            public VectorData NormScale 
            { 
                get 
                { 
                    if(mScale == null) mScale = new VectorData(BuilderParameters.T_DIM, InputNorm.weight, InputNorm.WeightGrad, InputNorm.DeviceType); 
                    return mScale;
                }
            }
            
            VectorData mBias = null;
            public VectorData NormBias 
            { 
                get 
                { 
                    if(mBias == null) mBias = new VectorData(BuilderParameters.T_DIM, InputNorm.bias, InputNorm.BiasGrad, InputNorm.DeviceType);  
                    return mBias;
                }
            }

            public TransformerLMModel(int vocabSize, int wordEmdDim, int posNum, DeviceType device)
            {
                // no position embedding.
                WordInEmbed = AddLayer(new EmbedStructure(vocabSize, wordEmdDim, device)); // DeviceType.CPU_FAST_VECTOR));
                
                // it is shared word embedding by default.
                WordOutEmbed = WordInEmbed;
                
                int embed = wordEmdDim; //posEmdDim + 

                Conv = new TensorConvStructure[BuilderParameters.CONV_HEAD];

                int filterWidth = embed / BuilderParameters.CONV_HEAD;
                for(int i = 0; i < BuilderParameters.CONV_HEAD; i++)
                {
                    Conv[i] = AddLayer(new TensorConvStructure(filterWidth, BuilderParameters.HISTORY, 1, BuilderParameters.FilterDepth, device));
                    //(float scale, float bias)
                    if(BuilderParameters.CNN_INIT_RANGE > 0)
                    {
                        Conv[i].Filter.Init(2 * BuilderParameters.CNN_INIT_RANGE, -BuilderParameters.CNN_INIT_RANGE);
                    }
                }

                InputNorm = AddLayer(new LayerStructure(1, BuilderParameters.T_DIM, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                InputNorm.weight.Init(1);
                InputNorm.bias.Init(0);

                Blocks = new TransformerStructure[BuilderParameters.T_LAYER];
                for(int i=0; i < BuilderParameters.T_LAYER; i++)
                {
                    Blocks[i] = AddLayer(new TransformerStructure(BuilderParameters.T_DIM, device));
                }

                Decoder = AddLayer(new DNNStructure(BuilderParameters.T_DIM, 
                                                    new int[] { wordEmdDim }, 
                                                    new A_Func[] { A_Func.Linear },
                                                    new float[] { 0.0f },
                                                    new bool[] { true },
                                                    device));
            }

            // public TransformerLMModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
            //     WordInEmbed = DeserializeModel<EmbedStructure>(reader, device); 
            //     WordOutEmbed = WordInEmbed;
                
            //     Conv = new TensorConvStructure[BuilderParameters.CONV_HEAD];
            //     for(int i = 0; i < BuilderParameters.CONV_HEAD; i++)
            //     {
            //         Conv[i] = DeserializeModel<TensorConvStructure>(reader, device);
            //     }

            //     InputNorm = DeserializeModel<LayerStructure>(reader, device);

            //     Blocks = new TransformerStructure[BuilderParameters.T_LAYER];
            //     for(int i = 0; i < BuilderParameters.T_LAYER; i++)
            //     {
            //         Blocks[i] = DeserializeModel<TransformerStructure>(reader, device);
            //     }

            //     Decoder = DeserializeModel<DNNStructure>(reader, device);
            // }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public class CNNTFCG : ComputationGraph
        {
            public override void GradientRegular()
            {
                float clipNorm = OptimizerParameters.StructureOptimizer.GradNormClip;
                if(clipNorm > 0)
                {
                    float gradL2 = Behavior.Computelib.DotProduct(Model.Grad, Model.Grad, Model.Grad.Size);
                    float norm = (float)Math.Sqrt(gradL2);
                    
                    if(norm > clipNorm)
                    {
                        /// b = bwei b + awei * a;
                        //void Scale_Vector(CudaPieceFloat a, int offsetA, CudaPieceFloat b, int offsetB, int len, float awei, float bwei);
                        Behavior.Computelib.Scale_Vector(Model.Grad, 0, Model.Grad, 0, Model.Grad.Size, 0, clipNorm / norm);
                    }
                    Logger.WriteLog("gradient norm {0}", Math.Sqrt(gradL2));
                }
            }

            public CNNTFCG(List<int> data, int vocabSize, int batchSize, int chunkSize, TransformerLMModel model, RunnerBehavior behavior) : base(behavior)
            {
                Behavior = behavior;
                Model = model;  

                ChunkSamplingRunner chunkSampleRunner = new ChunkSamplingRunner(data, vocabSize, chunkSize, BuilderParameters.HISTORY, batchSize, true, 2000, Behavior);
                AddDataRunner(chunkSampleRunner);
                SparseMatrixData chunk = chunkSampleRunner.Output;

                HiddenBatchData w_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(chunk.FeatureIdx, chunk.MaxLength, model.WordInEmbed, Behavior));
                
                IntArgument head_dim = new IntArgument("Conv_Head_Dim", w_embed.Dim / BuilderParameters.CONV_HEAD);
                IntArgument head_num = new IntArgument("Conv_Head_Num", BuilderParameters.CONV_HEAD);
                IntArgument sent_size = new IntArgument("sent_size", chunkSize + BuilderParameters.HISTORY - 1);
                IntArgument batch_size = chunk.ShapeRow;
                // IntArgument[] shape = new IntArgument[] { head_dim, head_num, sent_size, chunk.ShapeRow };
                // NdArrayData tensorEmbed = new NdArrayData(shape, w_embed.Output.Data, w_embed.Deriv.Data, Behavior.Device);  
                // IntArgument sent_size = new IntArgument("sent_size", chunkSize + BuilderParameters.HISTORY - 1);

                IntArgument[] embedShape = new IntArgument[] { head_dim, head_num, sent_size, batch_size };
                NdArrayData embedData = new NdArrayData(embedShape, w_embed.Output.Data, w_embed.Deriv.Data, 
                                                        Behavior.Device).Transpose(new int[] { 0, 2, 3, 1 }, Session, Behavior);
                
                IntArgument[] sliceShape = new IntArgument[] { head_dim, sent_size, new IntArgument("depth", 1), batch_size };
                List<NdArrayData> sliceInput = embedData.Split(sliceShape, BuilderParameters.CONV_HEAD, Session, Behavior);
                List<NdArrayData> sliceOutput = new List<NdArrayData>();

                for(int i=0; i < BuilderParameters.CONV_HEAD; i++)
                {
                    TensorConvRunner convRunner = new TensorConvRunner(model.Conv[i], sliceInput[i].ToT4D(), 0, 0, 1, 1, A_Func.Linear, Behavior);
                    AddRunner(convRunner);
                    sliceOutput.Add(convRunner.Output.ToND());
                }
                NdArrayData merge_data = RunHelper.Merge(sliceOutput, Session, Behavior).Transpose(new int[] { 0, 2, 4, 1, 3 }, Session, Behavior);

                IntArgument newWidth = new IntArgument("encode_emd_dim", merge_data.Shape[0].Default * merge_data.Shape[1].Default * merge_data.Shape[2].Default);

                NdArrayData tensorEmbed = merge_data.Reshape(newWidth, merge_data.Shape[3], merge_data.Shape[4]);

                // IntArgument[] shapes, List<StructRunner> session, RunnerBehavior behavior)

                //TensorConvRunner convRunner = new TensorConvRunner(model.Conv, w_embed_tensor, 0, 0, 1, BuilderParameters.StrideWidth, A_Func.Tanh, Behavior);
                //AddRunner(convRunner);
                //Tensor4DData convOutput = convRunner.Output;
                //IntArgument newWidth = new IntArgument("encode_emd_dim", convOutput.Width * convOutput.Depth);
                if(tensorEmbed.Shape[1].Default != chunkSize)
                {
                    throw new Exception(string.Format("Conv height size is not match with the chunk size {0}, {1}", chunkSize, tensorEmbed.Shape[1].Default));
                }
                //NdArrayData tensorEmbed = convOutput.ToND().Transpose(new int[] { 2, 0, 1, 3}, Session, Behavior).Reshape(newWidth, convOutput.Shape[1], convOutput.Shape[3]);
                if(tensorEmbed.Shape[0].Default != BuilderParameters.T_DIM)
                {
                    throw new Exception(string.Format("Conv new width is not match with tansformer dim {0}, {1}", tensorEmbed.Shape[0].Default, BuilderParameters.T_DIM));
                }
                
                if(BuilderParameters.IS_LAYER_NORM > 0)
                    tensorEmbed = tensorEmbed.Norm(0, Session, Behavior);
                //NdArrayData _normWTP = tensorEmbed.Norm(0, Session, Behavior);
                tensorEmbed = tensorEmbed.DotAndAdd(model.NormScale, model.NormBias, Session, Behavior);

                for(int i = 0; i < model.Blocks.Length; i++)
                {
                    TransformerRunner blockRunner = new TransformerRunner(model.Blocks[i], 16, chunkSize, tensorEmbed, null, null, BuilderParameters.IS_LAYER_NORM > 0, Behavior);
                    AddRunner(blockRunner);
                    tensorEmbed = blockRunner.Output;
                }
                
                Logger.WriteLog("output embed batch size {0}, dim {1}", tensorEmbed.Shape[1].Default * tensorEmbed.Shape[2].Default, newWidth.Default );

                HiddenBatchData outputEmbed = new HiddenBatchData(tensorEmbed.Shape[1].Default * tensorEmbed.Shape[2].Default, newWidth.Default, tensorEmbed.Output, tensorEmbed.Deriv, Behavior.Device);

                DNNRunner<HiddenBatchData> decodeRunner = new DNNRunner<HiddenBatchData>(model.Decoder, outputEmbed, Behavior);
                AddRunner(decodeRunner);
                HiddenBatchData decodEmbed = decodeRunner.Output;

                MatrixProductRunner scoreProductRunner = new MatrixProductRunner( new MatrixData(decodEmbed), new MatrixData(model.WordOutEmbed), Behavior);
                AddRunner(scoreProductRunner);
                HiddenBatchData score = new HiddenBatchData(scoreProductRunner.Output);

                SoftmaxObjRunner objRunner = new SoftmaxObjRunner(chunkSampleRunner.Label, score, Behavior);

                if(Behavior.RunMode == DNNRunMode.Train)
                {
                    AddObjective(objRunner); 
                }
                else
                {
                    AddRunner(objRunner);
                }
                SetDelegateModel(Model);
            }
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            CorpusDataPanel.Init(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");

            TransformerLMModel lmModel = new TransformerLMModel(CorpusDataPanel.VocabSize, BuilderParameters.W_EMBED, 
                                                                BuilderParameters.P_LEN, device);

            if(! BuilderParameters.SEED_MODEL.Equals(string.Empty))
            {
                lmModel.Deserialize(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)));
            }

            //BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
            //                                new TransformerLMModel(CorpusDataPanel.VocabSize, BuilderParameters.W_EMBED, 
            //                                                    BuilderParameters.P_LEN, device)
            //                              : new TransformerLMModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);

            lmModel.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:

                    CNNTFCG trainCG = new CNNTFCG(CorpusDataPanel.TrainData, CorpusDataPanel.VocabSize, BuilderParameters.BATCH_SIZE, BuilderParameters.P_LEN, lmModel, 
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    
                    ComputationGraph validCG = new CNNTFCG(CorpusDataPanel.TestData, CorpusDataPanel.VocabSize, 1, BuilderParameters.P_LEN, lmModel, 
                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        trainCG.Execute();

                        if(iter % 100 == 0)
                        {
                            validCG.Execute();
                            using(BinaryWriter modelWriter = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "transformerlm." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            {
                                lmModel.Serialize(modelWriter);
                            }
                        }

                    } 
                    break;
                case DNNRunMode.Predict:
                    CNNTFCG testCG = new CNNTFCG(CorpusDataPanel.TestData, CorpusDataPanel.VocabSize, 1, BuilderParameters.P_LEN, lmModel, 
                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    testCG.Execute();
                    break;
            }
        }
    }
}
