using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Language
{
    unsafe public class LargeBertQA : ComputationGraph
    {
        public string name = "BertLargeModel";

        public class BertQAModel : CompositeNNStructure
        {
            public LargeBert.BertLargeModel BertModel { get; set; }

            public int embed { get { return BertModel.embed; } } // = 1024;
            public int layer { get { return BertModel.layer; } }

            public int SpecialCategory { get; set; }

            // special token -- classifier.
            public LayerStructure SpecialClassifier { get; set; }
            
            public LayerStructure StartVector { get; set; }
            public LayerStructure EndVector { get; set; }

            void InitAnswerLayer(DeviceType device)
            {
                SpecialClassifier = AddLayer(new LayerStructure(BertModel.embed, SpecialCategory, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                StartVector = AddLayer(new LayerStructure(BertModel.embed, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                EndVector = AddLayer(new LayerStructure(BertModel.embed, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

                SpecialClassifier.weight.IsRandomVector = true;
                StartVector.weight.IsRandomVector = true;
                EndVector.weight.IsRandomVector = true;
            }

            public BertQAModel(int frozenlayer, int special_category, string meta_file, string bin_file, DeviceType device) : base(device) 
            {
                SpecialCategory = special_category;
                BertModel = AddLayer(new LargeBert.BertLargeModel(frozenlayer, 2, meta_file, bin_file, device));
                InitAnswerLayer(device);
            }

            public BertQAModel(int special_category, DeviceType device) : base(device)
            {
                SpecialCategory = special_category;
                BertModel = AddLayer(new LargeBert.BertLargeModel(2, 24, -1, device));
                InitAnswerLayer(device);
            }
        }

        public new BertQAModel Model { get; set; }

        LargeBert BertRunner { get; set; } 

        public CudaPieceInt TokenIds { get { return BertRunner.TokenIds; } set { BertRunner.TokenIds = value; } }
        public CudaPieceInt SegmentIds { get { return BertRunner.SegmentIds; } set { BertRunner.SegmentIds = value; } }
        public CudaPieceInt MaskIds { get { return BertRunner.MaskIds; } set { BertRunner.MaskIds = value; } }

        // docoment token mask.
        public CudaPieceInt MaskDoc { get; set; }

        // binary classification for each category.
        public CudaPieceFloat CateLabel { get; set; }
        public CudaPieceFloat StartLabel { get; set; }
        public CudaPieceFloat EndLabel { get; set; }

        public int MaxSeqLen { get; set; }

        public SmoothCrossEntropyRunner CateRunner { get; set; }
        public SmoothCrossEntropyRunner StartRunner { get; set; }
        public SmoothCrossEntropyRunner EndRunner { get; set; }

        HiddenBatchData StartV { get; set; }
        HiddenBatchData EndV { get; set; }
        HiddenBatchData CateV { get; set; }

        public double Cate_Loss { get { return CateRunner.ObjectiveScore; } }
        public double Start_Loss { get { return StartRunner.ObjectiveScore; } }
        public double End_Loss { get { return EndRunner.ObjectiveScore; } }
        
        public LargeBertQA(BertQAModel model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior;  
            Model = model;  
            SetDelegateModel(Model);
        }

        public void BuildCG(int batchSize, int max_seq_len, float dropout, int poolType, int isAvgCateLoss, int isAvgSpanLoss, float cateWeight, float spanWeight) 
        {
        	BuildCG(batchSize, max_seq_len, dropout, poolType, 0, isAvgCateLoss, isAvgSpanLoss, cateWeight, spanWeight);
        }

        public void BuildCG(int batchSize, int max_seq_len, float dropout, int poolType, float smooth, int isAvgCateLoss, int isAvgSpanLoss, float cateWeight, float spanWeight) 
        {
            MaxSeqLen = max_seq_len;
            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);

            // binary category labels.
            CateLabel = new CudaPieceFloat(batchSize * Model.SpecialCategory, Behavior.Device);

            StartLabel = new CudaPieceFloat(batchSize * max_seq_len, Behavior.Device);
            EndLabel = new CudaPieceFloat(batchSize * max_seq_len, Behavior.Device);

            MaskDoc = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            
            BertRunner = new LargeBert(Model.BertModel, Behavior);

            TokenIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            SegmentIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            MaskIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);

            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, Model.layer);
            AddRunner(BertRunner);

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = poolType == 0 ? BertRunner.Slice : BertRunner.Pool;
            
            if(dropout > 0)
            {
                feature = feature.Dropout(dropout, Session, Behavior);
                pool = pool.Dropout(dropout, Session, Behavior);
            }

            FullyConnectHiddenRunner<HiddenBatchData> specRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.SpecialClassifier, pool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(specRunner);
            CateV = specRunner.Output;

	    // category softmax.
            CateRunner = new SmoothCrossEntropyRunner(CateLabel, CateV, smooth, Behavior) { IsAvg = isAvgCateLoss > 0, Weight = cateWeight };
            AddObjective(CateRunner);

	    // change to mask softmax.
            HiddenBatchData doc_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskDoc, MaskDoc.Size, feature.Output, feature.Deriv, max_seq_len * batchSize, Model.embed, Behavior));
	     
            // balance the loss of category and span.
            FullyConnectHiddenRunner<HiddenBatchData> startRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.StartVector, doc_embed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(startRunner);
            StartV = startRunner.Output;

            FullyConnectHiddenRunner<HiddenBatchData> endRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.EndVector, doc_embed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(endRunner);
            EndV = endRunner.Output;
            // start softmax.
            StartRunner = new SmoothCrossEntropyRunner(StartLabel, StartV, smooth, Behavior) { IsAvg = isAvgSpanLoss > 0, Weight = spanWeight }; 
            AddObjective(StartRunner);
	    
	    // end softmax.
            EndRunner = new SmoothCrossEntropyRunner(EndLabel, EndV, smooth, Behavior) { IsAvg = isAvgSpanLoss > 0, Weight = spanWeight }; 
            AddObjective(EndRunner);
        }
        
        public unsafe int[] SetupTarget(int[] acc_nums, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int unk_flag_index, int batchSize)
        {
            int * p_start_label = (int *)startLabel.ToPointer();
            int * p_end_label = (int *)endLabel.ToPointer();

            int * p_cate_label = (int *)specLabel.ToPointer();
            
            StartLabel.EffectiveSize = acc_nums[batchSize]; // * MaxSeqLen;
            EndLabel.EffectiveSize = acc_nums[batchSize]; // batchSize * MaxSeqLen;
            CateLabel.EffectiveSize = batchSize * Model.SpecialCategory;
            
            StartLabel.Zero();
            EndLabel.Zero();
            CateLabel.Zero();

            int unknown_num = 0;
            int start_only_num = 0;
            int end_only_num = 0;

            //int batch_word = 0;
            for(int b = 0; b < batchSize; b ++)
            {
                int startIdx = p_start_label[b];
                int endIdx = p_end_label[b];

                int word = acc_nums[b + 1] - acc_nums[b];

                int batch_word = acc_nums[b];

                bool isStartPoint = false;
                if(startIdx >= 0 && startIdx < word)
                { 
                    StartLabel[batch_word + startIdx] = 1;
                    isStartPoint = true;
                }

                bool isEndPoint = false;
                if(endIdx >= 0 && endIdx < word)
                { 
                    EndLabel[batch_word + endIdx] = 1;
                    isEndPoint = true;
                }

                if(isStartPoint && isEndPoint)
                {
                    CateLabel[b * Model.SpecialCategory + p_cate_label[b]] = 1; //0.95f;
                }
                else if(!isStartPoint && !isEndPoint)
                {
                    CateLabel[b * Model.SpecialCategory + unk_flag_index] = 1;
                    unknown_num += 1;
                }
                else if(isStartPoint && !isEndPoint)
                {
                    start_only_num += 1;
                    float converage_len = (word - startIdx) / (endIdx - startIdx);
                    
                    CateLabel[b * Model.SpecialCategory + unk_flag_index] += (1 - converage_len);
                    CateLabel[b * Model.SpecialCategory + p_cate_label[b]] += converage_len;
                }
                else if(!isStartPoint && isEndPoint)
                {
                    end_only_num += 1;
                    float converage_len = endIdx / (endIdx - startIdx);

                    CateLabel[b * Model.SpecialCategory + unk_flag_index] += (1 - converage_len);
                    CateLabel[b * Model.SpecialCategory + p_cate_label[b]] += converage_len;
                }
            }
            
            StartLabel.SyncFromCPU();
            EndLabel.SyncFromCPU();
            CateLabel.SyncFromCPU();

            return new int[] { unknown_num, start_only_num, end_only_num };
        }

        public unsafe int[] SetupInput(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, int batchSize)
        {
            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;
            
            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(SegmentIds.CudaPtr, segIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen); 

            int[] acc_nums = new int[batchSize + 1];
            acc_nums[0] = 0;
            int * p_mask_doc = (int * )maskDoc.ToPointer();
            {
                int batch_word = 0;
                for(int b = 0; b < batchSize; b ++)
                {
                    int word = 0;
                    for(int s = 0; s < MaxSeqLen; s ++)
                    {
                        int pos = b * MaxSeqLen + s;
                        if(p_mask_doc[pos] == 1)
                        {
                            MaskDoc[batch_word + word] = pos;
                            word += 1;
                        }
                    }
                    batch_word += word;
                    acc_nums[b + 1] = batch_word;
                }
                MaskDoc.EffectiveSize = batch_word;
                MaskDoc.SyncFromCPU();
            }
            return acc_nums;
        }

        public unsafe int[] Predict(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr cateLogit, IntPtr startLogit, IntPtr endLogit, int batchSize)
        {
            int[] acc_nums = SetupInput(tokenIdx, segIdx, maskIdx, maskDoc, batchSize);

            StepV3Run();
            
            Cudalib.CudaCopyOutFloat(CateV.Output.Data.CudaPtr, cateLogit, batchSize * Model.SpecialCategory);
            Cudalib.CudaCopyOutFloat(StartV.Output.Data.CudaPtr, startLogit, MaskDoc.EffectiveSize);
            Cudalib.CudaCopyOutFloat(EndV.Output.Data.CudaPtr, endLogit, MaskDoc.EffectiveSize);
            return acc_nums;
        }
    }


    public class DistLargeBertQA
    {
        public LargeBertQA[] Nets { get; set; }
        int DeviceNum { get { return DeviceIds.Length; } }
        int[] DeviceIds { get; set; }

        IntPtr Comm { get; set; }
        CudaPieceFloat[] Grads { get; set; }

        LargeBertQA.BertQAModel Model { get; set; }

        public unsafe DistLargeBertQA(LargeBertQA.BertQAModel model, StructureLearner learner, int deviceNum)
        {
            Model = model;
            DeviceIds = new int[deviceNum];
            for(int i = 0; i < deviceNum; i++) DeviceIds[i] = i;

            fixed (int * pDevice = & DeviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, deviceNum);
            }
            Grads = new CudaPieceFloat[deviceNum];
            Nets = new LargeBertQA[deviceNum];

            Parallel.For(0, DeviceNum, i =>  //foreach(int devId in DeviceIds)
            {
                int devId = DeviceIds[i];

                Logger.WriteLog("Create Device {0}", devId);
                DeviceType device = MathOperatorManager.SetDevice(devId);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

                //nt mcate, int mlayer, DeviceType device
                LargeBertQA.BertQAModel device_model = new LargeBertQA.BertQAModel(model.SpecialCategory, device);
                device_model.CopyFrom(model);

                // InitOptimizer(StructureLearner learner, RunnerBehavior behavior) 
                Grads[i] = device_model.InitOptimizer(learner, behavior);
                //Logger.WriteLog("gradient size {0}", Grads[idx].Size);
                // Bert(model, env_mode)
                Nets[i] = new LargeBertQA(device_model, behavior);
                //net.BuildCG(subBatchSize);
                //idx += 1;
            });
        }

        public int MaxSeqLen { get; set; }

        public void BuildCG(int batchSize, int max_seq_len, float dropout, int poolType, int isAvgCateLoss, int isAvgSpanLoss, float cateWeight, float spanWeight) 
        {
           	BuildCG(batchSize, max_seq_len, dropout, poolType, 0, isAvgCateLoss, isAvgSpanLoss, cateWeight, spanWeight);
        }

        public void BuildCG(int batchSize, int max_seq_len, float dropout, int poolType, float smooth, int isAvgCateLoss, int isAvgSpanLoss, float cateWeight, float spanWeight) 
        {
            MaxSeqLen = max_seq_len;

            int subBatchSize = batchSize / DeviceNum;
            //int idx = 0;
            //foreach(Bert net in Nets)
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].BuildCG(subBatchSize, max_seq_len, dropout, poolType, smooth, isAvgCateLoss, isAvgSpanLoss, cateWeight, spanWeight);
                //BuildMaskLM(subBatchSize, max_seq_len, layer, max_mask_len);
            });
        }

        public void Init()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Init();
            });
        }

        public void Complete()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Complete();
            });
        }

        public void SetTrainMode()
        {
            foreach(LargeBertQA net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Train;
            }
        }

        public void SetPredictMode()
        {
            foreach(LargeBertQA net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Predict;
            }
        } 

        public void SaveModel(string path)
        {
            Nets[0].Model.Save(path);
        }

        public unsafe int[] Predict(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr cateProb, IntPtr startLogit, IntPtr endLogit, int batchSize)
        {
            int subBatchSize = batchSize / DeviceNum;

            int[] acc_doc_spans = new int[batchSize + 1];
            acc_doc_spans[0] = 0;

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                int[] acc_doc_span = Nets[i].Predict(IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
                                    IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(maskDoc, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    
                                    IntPtr.Add(cateProb, sizeof(float) * i * subBatchSize * Model.SpecialCategory),
                                    IntPtr.Add(startLogit, sizeof(float) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(endLogit, sizeof(float) * i * subBatchSize * MaxSeqLen),
                                    subBatchSize);

                for(int k = 1; k <= subBatchSize; k++)
                {
                    acc_doc_spans[i * subBatchSize + k] = acc_doc_span[k];
                }
            });

            int lastEnd = 0;
            int[] new_acc_doc_span = new int[batchSize + 1];
            new_acc_doc_span[0] = lastEnd;

            for(int i = 0; i < DeviceNum; i++)
            {
                for(int l = 1; l <= subBatchSize; l++)
                {
                    new_acc_doc_span[i * subBatchSize + l] = acc_doc_spans[i * subBatchSize + l] + lastEnd;
                }
                lastEnd += acc_doc_spans[(i + 1) * subBatchSize];
            }

            int startPos = acc_doc_spans[subBatchSize];
            
            float * p_start_logit = (float * )startLogit.ToPointer();
            float * p_end_logit = (float * )endLogit.ToPointer();
            
            for(int i = 1; i < DeviceNum; i++)
            {
                for(int l = 0; l < acc_doc_spans[(i + 1) * subBatchSize]; l++)
                {
                    p_start_logit[startPos + l] = p_start_logit[i * subBatchSize * MaxSeqLen + l]; 
                    p_end_logit[startPos + l] = p_end_logit[i * subBatchSize * MaxSeqLen + l]; 
                }
                startPos += acc_doc_spans[(i + 1) * subBatchSize];
            }

            return new_acc_doc_span;
        }

        public unsafe float[] Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize)
        {
            return Run(tokenIdx, segIdx, maskIdx, maskDoc, specLabel, startLabel, endLabel, 3, batchSize);
        }

        public unsafe float[] Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int unk_flag_index, int batchSize)
        {
            float[] cate_loss = new float[DeviceNum];
            float[] start_loss = new float[DeviceNum];
            float[] end_loss = new float[DeviceNum];

            int subBatchSize = batchSize / DeviceNum;

            int[] unknowns = new int[DeviceNum];
            int[] leftOnly = new int[DeviceNum];
            int[] rightOnly = new int[DeviceNum];

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                int[] acc_nums = Nets[i].SetupInput(
                                    IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
                                    IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(maskDoc, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    subBatchSize);

            
                int[] r = Nets[i].SetupTarget(acc_nums, 
                                    IntPtr.Add(specLabel, sizeof(int) * i * subBatchSize),                    
                                    IntPtr.Add(startLabel, sizeof(int) * i * subBatchSize),
                                    IntPtr.Add(endLabel, sizeof(int) * i * subBatchSize),
                                    unk_flag_index,
                                    subBatchSize);
                unknowns[i] = r[0];
                leftOnly[i] = r[1];
                rightOnly[i] = r[2];
            });
            
            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                Nets[i].StepV3Run();

                cate_loss[i] = (float)Nets[i].Cate_Loss;
                start_loss[i] = (float)Nets[i].Start_Loss;
                end_loss[i] = (float)Nets[i].End_Loss;
            
                if(Nets[i].Behavior.RunMode == DNNRunMode.Train)
                {    
                    Cudalib.AllReduce(Grads[i].CudaPtr, Grads[i].Size, i, Comm);
                    Nets[i].StepV3Update();
                }
            });
            
            return new float[] { cate_loss.Sum(), start_loss.Sum(), end_loss.Sum(), unknowns.Sum() * 1.0f / batchSize, leftOnly.Sum() * 1.0f / batchSize, rightOnly.Sum() * 1.0f / batchSize };
        }

    }

}
