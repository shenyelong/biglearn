using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Language
{
    unsafe public class LargeBertRAM : ComputationGraph
    {
        public string name = "BertLargeModel";

        public class RAMModel : CompositeNNStructure
        {
            public LargeBert.BertLargeModel BertModel { get; set; }

            public int vocabSize { get { return BertModel.vocabSize; } } 
            public int pos { get { return BertModel.pos; } } 
            public int segType { get { return BertModel.segType; } }

            public int embed { get { return BertModel.embed; } } // = 1024;
            public int layer { get { return BertModel.layer; } }

            public int category { get { return BertModel.category; } }//= 2;

            public LayerStructure QProject { get; set; }
            public LayerStructure KProject { get; set; }
            public LayerStructure VProject { get; set; }

            // gru cells.
            public List<GRUCell> GruCells { get; set; }

            public LayerStructure Classifier { get; set; }
            //public LayerStructure T { get; set; }

            public RAMModel(int frozenlayer, int category, string meta_file, string bin_file, int attLayer, DeviceType device)
            {
                BertModel = AddLayer(new LargeBert.BertLargeModel(frozenlayer, category, meta_file, bin_file, device));

                int stateDim = BertModel.embed;
                QProject = AddLayer(new LayerStructure(BertModel.embed, stateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, false, device));
                KProject = AddLayer(new LayerStructure(BertModel.embed, stateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, false, device));
                VProject = AddLayer(new LayerStructure(BertModel.embed, stateDim, A_Func.Linear, N_Type.Convolution_layer, 1, 0, false, device));

                GruCells = new List<GRUCell>();
                for(int i = 0; i < attLayer; i++)
                {
                    GruCells.Add(AddLayer(new GRUCell(stateDim, stateDim, device)));
                }

                Classifier = AddLayer(new LayerStructure(stateDim, category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                //T = AddLayer(new LayerStructure(stateDim, 2, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            }
        }

        public new RAMModel Model { get; set; }

        LargeBert BertRunner { get; set; } 

        public SoftmaxObjRunner CERunner { get; set; }

        public TextSamplingRunner BatchRunner = null;

        public HiddenBatchData RAMPool { get; set; }

        public CudaPieceInt TokenIds { get; set; }
        public CudaPieceInt SegmentIds { get; set; }
        public CudaPieceInt MaskIds { get; set; }
        public SparseVectorData Labels { get; set; }

        public LargeBertRAM(RAMModel model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior;  
            Model = model;  
            SetDelegateModel(Model);
        }

        // // call complete after training on every epoch.
        // public void SaveModel(int epoch)
        // {
        //     using(BinaryWriter modelWriter = new BinaryWriter(new FileStream("bertBase.ft." + epoch.ToString(), FileMode.Create, FileAccess.Write)))
        //         Model.Serialize(modelWriter);
        // }

        public float AvgLoss { get { return CERunner.AvgPerplexity; } }

        public float AvgAccuracy { get { return CERunner.AvgAccuracy; } }

        public void SetTrainSet(TextDataSet dataset)
        {
            SetTrainMode();
            if(BatchRunner != null) BatchRunner.SetDataset(dataset);
        }

        public void SetPredictSet(TextDataSet dataset)
        {
            SetPredictMode();
            if(BatchRunner != null) BatchRunner.SetDataset(dataset);
        }

        public override void GradientRegular()
        {
            float clipNorm = Model.Learner.GradNormClip;
            if(clipNorm > 0)
            {
                float gradL2 = Behavior.Computelib.DotProduct(Model.Grad, Model.Grad, Model.Grad.Size);
                float norm = (float)Math.Sqrt(gradL2);
                
                if(norm > clipNorm)
                {
                    /// b = bwei b + awei * a;
                    Behavior.Computelib.Scale_Vector(Model.Grad, 0, Model.Grad, 0, Model.Grad.Size, 0, clipNorm / norm);
                }
            }
        }


        public void BuildFeatureBlock(int batchSize, int max_seq_len, int layer, int recursiveStep, bool hardAtt, bool dymMem, List<Tuple<int, float>> tempSchedule)
        {
            Logger.WriteLog("start Building bert feature layer....");

            BertRunner = new LargeBert(Model.BertModel, Behavior);
            
            BertRunner.TokenIds = TokenIds;
            BertRunner.SegmentIds = SegmentIds;
            BertRunner.MaskIds = MaskIds;

            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, layer);
            AddRunner(BertRunner);

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = BertRunner.Slice;

            Logger.WriteLog("Build bert feature layer.");

            int head = 16;

            IntArgument emd_dim = feature.Dimensions[0];
            IntArgument active_dim = feature.Dimensions[1];
            IntArgument batch_size = feature.Dimensions[2];

            IntArgument headdim = new IntArgument("head-dim", emd_dim.Default / head);
            IntArgument headnum = new IntArgument("head-num", head);

            IntArgument headbatch = new IntArgument("head-batch", head * batch_size.Default);
            
            NdArrayData kfeature = feature.FNN(Model.KProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;
            NdArrayData vfeature = feature.FNN(Model.VProject, Session, Behavior).Reshape(headdim, headnum, active_dim, batch_size).Transpose(new int[] {0, 2, 1, 3}, Session, Behavior);;

            HiddenBatchData initZeroH0 = new HiddenBatchData(batchSize, pool.Dim, false, Behavior.Device);
            initZeroH0.Output.Data.Init(0);

            List<HiddenBatchData> si_list = new List<HiddenBatchData>();
            HiddenBatchData si = pool;
            for(int l = 0; l < Model.GruCells.Count; l++)
            {
                GRUStateRunner initStateRunner = new GRUStateRunner(Model.GruCells[l], initZeroH0, si, Behavior);
                AddRunner(initStateRunner);
                si = initStateRunner.Output;

                si_list.Add(si);
            }

            RateScheduler TempRate = new RateScheduler(GlobalUpdateStep, tempSchedule);  

            NdArrayData MaskScale = new NdArrayData(Behavior.Device, BertRunner.MaskScale, null, active_dim, new IntArgument("seq", 1), new IntArgument("head", 1), batch_size );
            NdArrayData MaskBias = new NdArrayData(Behavior.Device, BertRunner.MaskBias, null, active_dim, new IntArgument("seq", 1), new IntArgument("head", 1), batch_size );
            
            int incMemRate = recursiveStep + 1;
            IntArgument large_mem = new IntArgument("dy-mem-size", incMemRate + active_dim.Default);

            if(dymMem)
            {
                DynMemMaskRunner dynRunner = new DynMemMaskRunner(kfeature, vfeature, MaskScale, MaskBias, incMemRate, Behavior);
                AddRunner(dynRunner);

                kfeature = dynRunner.MemNewKey;
                vfeature = dynRunner.MemNewValue;

                MaskScale = dynRunner.NewMaskScale;
                MaskBias = dynRunner.NewMaskBias;
            }

            for(int i = 0; i < recursiveStep; i++)
            {
                HiddenBatchData si_q = si_list.Last().FNN(Model.QProject, Session, Behavior);

                NdArrayData qx = si_q.ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batch_size);

                // softmax and sampling. 
                NdArrayData attScore = qx.MatMul(0, kfeature, 1, Session, Behavior);

                attScore = attScore.DotAndAdd(MaskScale, MaskBias, Session, Behavior);

                //.Softmax(Session, Behavior);
                HiddenBatchData attProb = attScore.Reshape(dymMem ? large_mem : active_dim, headbatch).ToHDB();

                GumbelSoftmaxRunner smRunner = new GumbelSoftmaxRunner(attProb, TempRate, hardAtt, Behavior);
                AddRunner(smRunner);
                attScore = smRunner.Output.ToND().Reshape(attScore.Shape);

                NdArrayData aData = attScore.MatMul(0, vfeature, 0, Session, Behavior);
                HiddenBatchData attData = aData.Reshape(emd_dim, batch_size).ToHDB();

                List<HiddenBatchData> next_si_list = new List<HiddenBatchData>();
                for(int l = 0; l < Model.GruCells.Count; l++)
                {
                    GRUStateRunner stateRunner = new GRUStateRunner(Model.GruCells[l], si_list[l], attData, Behavior);
                    AddRunner(stateRunner);
                    attData = stateRunner.Output;
                    next_si_list.Add(attData);
                }
                si_list = next_si_list;

                if(dymMem)
                {
                    NdArrayData n_kfeature = si_list.Last().FNN(Model.KProject, Session, Behavior).ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batch_size);
                    NdArrayData n_vfeature = si_list.Last().FNN(Model.VProject, Session, Behavior).ToND().Reshape(headdim, new IntArgument("seq", 1), headnum, batch_size);

                    //AppendMemMaskRunner(NdArrayData srcMemKey, NdArrayData srcMemValue, int index, NdArrayData oldMaskScale, NdArrayData oldMaskBias, NdArrayData nMemKey, NdArrayData nMemValue, RunnerBehavior behavior)
                    AppendMemMaskRunner appMemRunner = new AppendMemMaskRunner(kfeature, vfeature, active_dim.Default + i, MaskScale, MaskBias, n_kfeature, n_vfeature, Behavior);
                    Session.Add(appMemRunner);

                    kfeature = appMemRunner.MemKey;
                    vfeature = appMemRunner.MemValue;

                    MaskScale = appMemRunner.NewMaskScale;
                    MaskBias = appMemRunner.NewMaskBias;

                    //memMaskScale = appMemRunner.MemMaskScale;
                    //memMaskBias = appMemRunner.MemMaskBias;
                }
            }

            RAMPool = si_list.Last(); 
        }

        public void BuildCG(int batchSize, int max_seq_len, int bertLayer, int recursiveStep, bool hardAtt, bool dymMem, List<Tuple<int, float>> tempSchedule) 
        {
            // sample mini batch dataset.
            BatchRunner = new TextSamplingRunner(Model.vocabSize, Model.category, max_seq_len, batchSize, Behavior);
            AddDataRunner(BatchRunner);

            //SparseMatrixData TokenIdxs { get; set; }
            //SparseMatrixData SegmentIds { get; set; }
            //SparseMatrixData MaskIds { get; set; }

            TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
            SegmentIds = BatchRunner.SegmentIds; //new SparseMatrixData(2, batchSize, batchSize * max_seq_len, Behavior.Device, false);
            MaskIds = BatchRunner.MaskIds; 


            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);

            //int batchSize, int max_seq_len, int layer, int recursiveStep, List<Tuple<int, float>> tempSchedule) 
            BuildFeatureBlock(batchSize, max_seq_len, bertLayer, recursiveStep, hardAtt, dymMem, tempSchedule);
            
            Labels = BatchRunner.Labels;
            //SparseVectorData Label = BatchRunner.Labels;

            FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, RAMPool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(classifierRunner);
            HiddenBatchData o = classifierRunner.Output;
            
            //SparseVectorData label, HiddenBatchData output, RunnerBehavior behavior)
            CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
            //CrossEntropyRunner(Label, o, Behavior);
            AddObjective(CERunner); 
        
        }
    }
}
