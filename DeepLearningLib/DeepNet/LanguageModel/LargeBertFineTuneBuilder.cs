using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    public class LargeBertFineTuneBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("BERT-LAYER", new ParameterArgument("12", "pre-trained bert layers."));

                Argument.Add("CATEGORY", new ParameterArgument("2", "classification type"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }
            public static int BERT_LAYER { get { return int.Parse(Argument["BERT-LAYER"].Value); } }

            public static int CATEGORY { get { return int.Parse(Argument["CATEGORY"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.LARGE_BERT_FINE_TUNE; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data ...");

            TextDataSet trainSet = new TextDataSet(BuilderParameters.Data + @"/train.index");
            TextDataSet testSet = new TextDataSet(BuilderParameters.Data + @"/dev.index");

            Logger.WriteLog("Load Data Finished.");
            
            LargeBert.BertLargeModel model = 
                    BuilderParameters.BERT_SEED.Equals(string.Empty) ? new LargeBert.BertLargeModel(BuilderParameters.CATEGORY, BuilderParameters.BERT_LAYER, device):
                                                                       new LargeBert.BertLargeModel(BuilderParameters.CATEGORY, BuilderParameters.BERT_SEED + @"/exp.label", BuilderParameters.BERT_SEED + @"/exp.bin", device);

            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }; 
            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);

            LargeBert bertRunner = new LargeBert(model, behavior);
            bertRunner.BuildCG(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ);

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {   
                        bertRunner.SetTrainSet(trainSet);
                        bertRunner.Execute();
                        Logger.WriteLog("Training avg Loss {0}, avg Accuracy {1}", bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        //if(iter % 5 == 0)
                        {
                            bertRunner.SetPredictSet(testSet);
                            bertRunner.Execute(false);
                            Logger.WriteLog("Testing avg Loss {0}, avg Accuracy {1}", bertRunner.AvgLoss, bertRunner.AvgAccuracy);
                        }

                    } 
                    break;
                case DNNRunMode.Predict:
                    break;
            }
        }
    }
}
