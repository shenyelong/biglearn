using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    public class VanilleTransformerLMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("IS-SHARE-EMD", new ParameterArgument("0", "0:no share embedding; 1:share embedding."));
                Argument.Add("W-EMBED", new ParameterArgument("300", "word embedding dimension."));
                Argument.Add("P-EMBED", new ParameterArgument("100", "word embedding dimension."));
                Argument.Add("P-LEN", new ParameterArgument("256", "sentence length."));
                
                Argument.Add("T-LAYER", new ParameterArgument("8", "Transformer Layer."));
                Argument.Add("T-DIM", new ParameterArgument("1024", "Transformer Dimension."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("IS-SAVED-POS-EMD", new ParameterArgument("1", "Seed Model for an optional seed model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }

            public static int IS_SHARE_EMD { get { return int.Parse(Argument["IS-SHARE-EMD"].Value); } }
            
            public static int W_EMBED { get { return int.Parse(Argument["W-EMBED"].Value); } }
            public static int P_EMBED { get { return int.Parse(Argument["P-EMBED"].Value); } }
            public static int P_LEN { get { return int.Parse(Argument["P-LEN"].Value); } }

            public static int T_LAYER { get { return int.Parse(Argument["T-LAYER"].Value); } }
            public static int T_DIM { get { return int.Parse(Argument["T-DIM"].Value); } }
            
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int IS_SAVED_POS_EMD { get { return int.Parse(Argument["IS-SAVED-POS-EMD"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.LANGUAGE_TRANSFORMER_LM; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class TransformerLMModel : CompositeNNStructure
        {
            public EmbedStructure WordInEmbed { get; set; }
            public EmbedStructure WordOutEmbed { get; set; }
            public EmbedStructure PosEmbed { get; set; }
            public DNNStructure Encoder { get; set; }
            public TransformerStructure[] Blocks { get; set; }
            public DNNStructure Decoder { get; set; }
            public TransformerLMModel(int vocabSize, int wordEmdDim, int posNum, int posEmdDim, DeviceType device)
            {

                WordInEmbed = AddLayer(new EmbedStructure(vocabSize, wordEmdDim, -0.01f, 0.01f, device)); // DeviceType.CPU_FAST_VECTOR));

                PosEmbed = AddLayer(new EmbedStructure(posNum, posEmdDim, -0.01f, 0.01f, device));

                if(BuilderParameters.IS_SHARE_EMD > 0)
                {
                    WordOutEmbed = WordInEmbed;
                }
                else
                {
                    WordOutEmbed = AddLayer(new EmbedStructure(vocabSize, wordEmdDim, -0.01f, 0.01f, device)); // DeviceType.CPU_FAST_VECTOR));
                }

                int embed = wordEmdDim; //posEmdDim + 

                Encoder = AddLayer(new DNNStructure(embed, 
                                                    new int[] { BuilderParameters.T_DIM }, 
                                                    new A_Func[] { A_Func.Tanh },
                                                    new float[] { 0.0f },
                                                    new bool[] { true },
                                                    device));

                Blocks = new TransformerStructure[BuilderParameters.T_LAYER];
                for(int i=0; i < BuilderParameters.T_LAYER; i++)
                {
                    Blocks[i] = AddLayer(new TransformerStructure(BuilderParameters.T_DIM, device));
                }

                Decoder = AddLayer(new DNNStructure(BuilderParameters.T_DIM, 
                                                    new int[] { wordEmdDim }, 
                                                    new A_Func[] { A_Func.Linear },
                                                    new float[] { 0.0f },
                                                    new bool[] { true },
                                                    device));
            }

            CudaPieceFloat ModelParam;
            CudaPieceFloat ModelGrad;

            public TransformerLMModel(TransformerLMModel srcModel, DeviceType device)
            {

            }

            public TransformerLMModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
                WordInEmbed = DeserializeModel<EmbedStructure>(reader, device); 
                
                if(BuilderParameters.IS_SAVED_POS_EMD > 0)
                {
                    PosEmbed = DeserializeModel<EmbedStructure>(reader, device);
                }

                if(BuilderParameters.IS_SHARE_EMD > 0)
                {
                    WordOutEmbed = WordInEmbed;
                }
                else
                {
                    WordOutEmbed = DeserializeModel<EmbedStructure>(reader, device);
                }
                Encoder = DeserializeModel<DNNStructure>(reader, device);

                Blocks = new TransformerStructure[BuilderParameters.T_LAYER];
                for(int i = 0; i < BuilderParameters.T_LAYER; i++)
                {
                    Blocks[i] = DeserializeModel<TransformerStructure>(reader, device);
                }

                Decoder = DeserializeModel<DNNStructure>(reader, device);
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public static void TransLMModel(ComputationGraph cg, List<int> data, int vocabSize,
                                       int batchSize, int chunkSize, TransformerLMModel model, RunnerBehavior Behavior)
        {
            ChunkSamplingRunner chunkSampleRunner = new ChunkSamplingRunner(data, vocabSize, chunkSize, batchSize, false, 2000, Behavior);
            cg.AddDataRunner(chunkSampleRunner);
            SparseMatrixData chunk = chunkSampleRunner.Output;

            HiddenBatchData w_embed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(chunk.FeatureIdx, chunk.MaxLength, model.WordInEmbed, Behavior));
            //HiddenBatchData p_embed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(chunkSampleRunner.PosIdx, chunk.MaxLength, model.PosEmbed, Behavior));

            HiddenBatchData wp_embed = w_embed;
            //HiddenBatchData wp_embed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
            //                                                                                  { w_embed, p_embed }, Behavior));
            Logger.WriteLog("embed batch size {0}, dim {1} ", wp_embed.MAX_BATCHSIZE, wp_embed.Dim);
            DNNRunner<HiddenBatchData> encodeRunner = new DNNRunner<HiddenBatchData>(model.Encoder, wp_embed, Behavior);
            cg.AddRunner(encodeRunner);
            HiddenBatchData encodeEmbed = encodeRunner.Output;

            IntArgument emd_dim = new IntArgument("emd_dim", encodeEmbed.Dim);
            IntArgument sent_size = new IntArgument("sent_size", chunkSize);
            IntArgument[] shape = new IntArgument[] { emd_dim, sent_size, chunk.ShapeRow };
            NdArrayData tensorEmbed = new NdArrayData(shape, encodeEmbed.Output.Data, encodeEmbed.Deriv.Data, Behavior.Device);

            for(int i = 0; i < model.Blocks.Length; i++)
            {
                TransformerRunner blockRunner = new TransformerRunner(model.Blocks[i], 16, chunkSize, tensorEmbed, Behavior);
                cg.AddRunner(blockRunner);
                tensorEmbed = blockRunner.Output;
            }
            
            Logger.WriteLog("output embed batch size {0}, dim {1}", encodeEmbed.MAX_BATCHSIZE, encodeEmbed.Dim );

            HiddenBatchData outputEmbed = new HiddenBatchData(encodeEmbed.MAX_BATCHSIZE, encodeEmbed.Dim, tensorEmbed.Output, tensorEmbed.Deriv, Behavior.Device);

            DNNRunner<HiddenBatchData> decodeRunner = new DNNRunner<HiddenBatchData>(model.Decoder, outputEmbed, Behavior);
            cg.AddRunner(decodeRunner);
            HiddenBatchData decodEmbed = decodeRunner.Output;

            MatrixProductRunner scoreProductRunner = new MatrixProductRunner( new MatrixData(decodEmbed), new MatrixData(model.WordOutEmbed), Behavior);
            cg.AddRunner(scoreProductRunner);
            HiddenBatchData score = new HiddenBatchData(scoreProductRunner.Output);

            SoftmaxObjRunner objRunner = new SoftmaxObjRunner(chunkSampleRunner.Label, score, Behavior);
            cg.AddObjective(objRunner); 
            cg.SetDelegateModel(model);
        }

        public static void PredLMModel(ComputationGraph cg, List<int> data, int vocabSize,
                                       int batchSize, int chunkSize, TransformerLMModel model, RunnerBehavior Behavior)
        {
            ChunkSamplingRunner chunkSampleRunner = new ChunkSamplingRunner(data, vocabSize, chunkSize, batchSize, true, 3000, Behavior);
            cg.AddDataRunner(chunkSampleRunner);
            SparseMatrixData chunk = chunkSampleRunner.Output;

            HiddenBatchData w_embed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(chunk.FeatureIdx, chunk.MaxLength, model.WordInEmbed, Behavior));
            //HiddenBatchData p_embed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(chunkSampleRunner.PosIdx, chunk.MaxLength, model.PosEmbed, Behavior));

            HiddenBatchData wp_embed = w_embed;
            //HiddenBatchData wp_embed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
            //                                                                                  { w_embed, p_embed }, Behavior));
            Logger.WriteLog("embed batch size {0}, dim {1} ", wp_embed.MAX_BATCHSIZE, wp_embed.Dim);
            DNNRunner<HiddenBatchData> encodeRunner = new DNNRunner<HiddenBatchData>(model.Encoder, wp_embed, Behavior);
            cg.AddRunner(encodeRunner);
            HiddenBatchData encodeEmbed = encodeRunner.Output;

            IntArgument emd_dim = new IntArgument("emd_dim", encodeEmbed.Dim);
            IntArgument sent_size = new IntArgument("sent_size", chunkSize);
            IntArgument[] shape = new IntArgument[] { emd_dim, sent_size, chunk.ShapeRow };
            NdArrayData tensorEmbed = new NdArrayData(shape, encodeEmbed.Output.Data, encodeEmbed.Deriv.Data, Behavior.Device);

            for(int i = 0; i < model.Blocks.Length; i++)
            {
                TransformerRunner blockRunner = new TransformerRunner(model.Blocks[i], 16, chunkSize, tensorEmbed, Behavior);
                cg.AddRunner(blockRunner);
                tensorEmbed = blockRunner.Output;
            }
            
            Logger.WriteLog("output embed batch size {0}, dim {1}", encodeEmbed.MAX_BATCHSIZE, encodeEmbed.Dim );

            HiddenBatchData outputEmbed = new HiddenBatchData(encodeEmbed.MAX_BATCHSIZE, encodeEmbed.Dim, tensorEmbed.Output, tensorEmbed.Deriv, Behavior.Device);

            DNNRunner<HiddenBatchData> decodeRunner = new DNNRunner<HiddenBatchData>(model.Decoder, outputEmbed, Behavior);
            cg.AddRunner(decodeRunner);
            HiddenBatchData decodEmbed = decodeRunner.Output;

            MatrixProductRunner scoreProductRunner = new MatrixProductRunner( new MatrixData(decodEmbed), new MatrixData(model.WordOutEmbed), Behavior);
            cg.AddRunner(scoreProductRunner);
            HiddenBatchData score = new HiddenBatchData(scoreProductRunner.Output);

            SoftmaxObjRunner objRunner = new SoftmaxObjRunner(chunkSampleRunner.Label, score, Behavior);
            cg.AddRunner(objRunner); 
            
            cg.SetDelegateModel(model);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            CorpusDataPanel.Init(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");

            // initialize a model with CPU memory.
            TransformerLMModel lmModel = BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                    new TransformerLMModel(CorpusDataPanel.VocabSize, BuilderParameters.W_EMBED, BuilderParameters.P_LEN, BuilderParameters.P_EMBED, DeviceType.CPU)
                    : new TransformerLMModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), DeviceType.CPU);

            TransformerLMModel[] lmModels = new TransformerLMModel[4];
			ComputationGraph[] trainCGs = new ComputationGraph[4];

			if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
            
            for(int i=0;i<4;i++)
            {
	            DeviceType device = MathOperatorManager.SetDevice(i);

	            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };
	            // need to make the model store in different gpus.
	            
	            lmModel.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
	        	

	            ComputationGraph trainCG = new ComputationGraph(behavior);
                TransLMModel(trainCG, CorpusDataPanel.TrainData, CorpusDataPanel.VocabSize, BuilderParameters.BATCH_SIZE, BuilderParameters.P_LEN, lmModel,  behavior);
	        	
	        }

	        for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                trainCGs[0].StepV2();
                trainCGs[1].StepV2();
                trainCGs[2].StepV2();
                trainCGs[3].StepV2();
                
                if(iter % 100 == 0)
                {
                    validCG.Execute();
                    using(BinaryWriter modelWriter = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "transformerlm." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            {
                                lmModel.Serialize(modelWriter);
                            }
                }

            } 
        }
    }
}
