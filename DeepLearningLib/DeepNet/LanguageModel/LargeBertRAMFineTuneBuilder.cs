using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    // large bert ram fine tune model.
    public class LargeBertRAMFineTuneBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("CATEGORY", new ParameterArgument("2", "classification type"));

                Argument.Add("TEMP", new ParameterArgument("0:0.2", "temp schedule."));   

                Argument.Add("RECURRENT-LAYER",new ParameterArgument("1", "recurrent layers."));

                Argument.Add("RECURRENT-STEP",new ParameterArgument("5", "recurrent layers."));

                Argument.Add("HARD-GUMBEL", new ParameterArgument("0", "0:soft gumbel; 1:hard gumbel."));

                Argument.Add("DYNAMIC-MEM", new ParameterArgument("0", "0:no dynamic memory, 1: dynamic memory."));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }

            public static int CATEGORY { get { return int.Parse(Argument["CATEGORY"].Value); } }

            public static List<Tuple<int, float>> Temp { get {
                    return Argument["TEMP"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                } }

            public static int Recurrent_Layer { get { return int.Parse(Argument["RECURRENT-LAYER"].Value); } }

            public static int Recurrent_Step { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }

            public static int Hard_Gumbel { get { return int.Parse(Argument["HARD-GUMBEL"].Value); } }

            public static int Dynamic_Mem { get { return int.Parse(Argument["DYNAMIC-MEM"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.LARGE_BERT_RAM_FINE_TUNE; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data ...");

            TextDataSet trainSet = new TextDataSet(BuilderParameters.Data + @"/train.index");
            TextDataSet testSet = new TextDataSet(BuilderParameters.Data + @"/dev.index");

            Logger.WriteLog("Load Data Finished.");
            
            // (int frozenlayer, int category, string meta_file, string bin_file, int attLayer, DeviceType device) 
            LargeBertRAM.RAMModel model = new LargeBertRAM.RAMModel(-1, BuilderParameters.CATEGORY, BuilderParameters.BERT_SEED + @"/exp.label", BuilderParameters.BERT_SEED + @"/exp.bin", BuilderParameters.Recurrent_Layer, device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }; 
            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);

            LargeBertRAM bertRamRunner = new LargeBertRAM(model, behavior);

            //int batchSize, int max_seq_len, int bertLayer, int recursiveStep, List<Tuple<int, float>> tempSchedule) 
            bertRamRunner.BuildCG(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ, 24, BuilderParameters.Recurrent_Step, BuilderParameters.Hard_Gumbel > 0, BuilderParameters.Dynamic_Mem > 0, BuilderParameters.Temp);

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {   
                        bertRamRunner.SetTrainSet(trainSet);
                        bertRamRunner.Execute();
                        Logger.WriteLog("Training avg Loss {0}, avg Accuracy {1}", bertRamRunner.AvgLoss, bertRamRunner.AvgAccuracy);
                        //if(iter % 5 == 0)
                        {
                            bertRamRunner.SetPredictSet(testSet);
                            bertRamRunner.Execute(false);
                            Logger.WriteLog("Testing avg Loss {0}, avg Accuracy {1}", bertRamRunner.AvgLoss, bertRamRunner.AvgAccuracy);
                        }

                    } 
                    break;
                case DNNRunMode.Predict:
                    break;
            }
        }
    }
}
