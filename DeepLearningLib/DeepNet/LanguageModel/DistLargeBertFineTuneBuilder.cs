using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    public class DistLargeBertFineTuneBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUIDS", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("BERT-SEED", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                
                Argument.Add("MAX-SEQ", new ParameterArgument("256", "max sequence length"));

                Argument.Add("BERT-LAYER", new ParameterArgument("12", "pre-trained bert layers."));

                Argument.Add("CATEGORY", new ParameterArgument("2", "classification type"));
            }


            //Argument["SCHEDULE-LR"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]) 
            public static int[] GPUIDS { get { return Argument["GPUIDS"].Value.Split(',').Select(i => int.Parse(i)).ToArray(); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string BERT_SEED { get { return Argument["BERT-SEED"].Value; } }

            public static int MAX_SEQ { get { return int.Parse(Argument["MAX-SEQ"].Value); } }
            public static int BERT_LAYER { get { return int.Parse(Argument["BERT-LAYER"].Value); } }

            public static int CATEGORY { get { return int.Parse(Argument["CATEGORY"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.DIST_LARGE_BERT_FINE_TUNE; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public unsafe override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data ...");

            TextDataSet trainSet = new TextDataSet(BuilderParameters.Data + @"/train.index");
            TextDataSet testSet = new TextDataSet(BuilderParameters.Data + @"/dev.index");

            int DeviceNum = BuilderParameters.GPUIDS.Length;

            RunnerBehavior dataBehavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = MathOperatorManager.SetDevice(-1), Computelib = null };

            Logger.WriteLog("Load Data Finished.");
            
            LargeBert.BertLargeModel model = new LargeBert.BertLargeModel(BuilderParameters.CATEGORY, BuilderParameters.BERT_SEED + @"/exp.label", BuilderParameters.BERT_SEED + @"/exp.bin", DeviceType.CPU);

            LargeBert.BertLargeModel[] Models = new LargeBert.BertLargeModel[ DeviceNum ];
            LargeBert[] Berts = new LargeBert[ DeviceNum ];

            TextSamplingRunner dataLoader = new TextSamplingRunner(model.vocabSize, model.category, BuilderParameters.MAX_SEQ, BuilderParameters.BATCH_SIZE * BuilderParameters.GPUIDS.Length, dataBehavior);

            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            
            IntPtr Comm = IntPtr.Zero;
            fixed (int * pDevice = & BuilderParameters.GPUIDS[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, DeviceNum);
            }

            CudaPieceFloat[] Grads = new CudaPieceFloat[DeviceNum];

            for(int d = 0; d < BuilderParameters.GPUIDS.Length; d++)
            {
                DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUIDS[d]);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

                Models[d] = new LargeBert.BertLargeModel(device);
                Models[d].CopyFrom(model);

                // need to make the model store in different gpus.
                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };
                
                Grads[d] = Models[d].InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);

                Berts[d] = new LargeBert(Models[d], behavior);
                Berts[d].Build(BuilderParameters.BATCH_SIZE, BuilderParameters.MAX_SEQ); 
            }
            //Model.vocabSize, Model.category, max_seq_len, batchSize, (float)(1.0f / Math.Sqrt(attdim_per_head)), (float)-1e9, Behavior); 

            

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {   
                        dataLoader.SetDataset(trainSet);
                        dataLoader.Init();

                        for(int i = 0; i < DeviceNum; i++)
                        {
                            Berts[i].SetTrainMode();
                            Berts[i].Init();
                        }

                        float totalLoss = 0;
                        int batchNum = 0;
                        while(true)
                        {
                            dataLoader.Forward();
                            if(dataLoader.IsTerminate) break;
                            
                            float[] stepLosses = new float[DeviceNum];

                            Parallel.For(0, DeviceNum, i => 
                            {
                                Cudalib.CudaInit(BuilderParameters.GPUIDS[i]);

                                int* token_ptr = dataLoader.TokenIdxs.CpuPtr + BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE * i;
                                {
                                    Cudalib.CudaCopyInFloat(Berts[i].TokenIds.CudaPtr, (IntPtr)token_ptr, BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE);
                                }
                                
                                int* segment_ptr = dataLoader.SegmentIds.CpuPtr + BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE * i;
                                {
                                    Cudalib.CudaCopyInFloat(Berts[i].SegmentIds.CudaPtr, (IntPtr)segment_ptr, BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE);
                                }

                                int* mask_ptr = dataLoader.MaskIds.CpuPtr + BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE * i;
                                {
                                    Cudalib.CudaCopyInFloat(Berts[i].MaskIds.CudaPtr, (IntPtr)mask_ptr, BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE);
                                }

                                for(int b = 0; b < BuilderParameters.BATCH_SIZE; b++)
                                {
                                    Berts[i].Labels.Idx[b] = dataLoader.Labels.Idx[b + i * BuilderParameters.BATCH_SIZE] - i * BuilderParameters.CATEGORY * BuilderParameters.BATCH_SIZE;
                                }
                                Berts[i].Labels.Idx.SyncFromCPU();

                                Berts[i].StepV3Run();

                                Cudalib.AllReduce(Grads[i].CudaPtr, Grads[i].Size, i, Comm);
                                Berts[i].StepV3Update();
                                
                                //Cudalib.Zero(Grads[i].CudaPtr, Grads[i].Size);
                                stepLosses[i] = (float)Berts[i].StepLoss;
                            });
                            float loss = (float)stepLosses.Sum() / DeviceNum;

                            totalLoss += loss;
                            batchNum += 1;

                            if(batchNum % 100 == 0)
                            {
                                Logger.WriteLog("Train Batch {0}, Average Loss {1}, Current Loss {2}", batchNum, totalLoss / batchNum, loss);
                            }
                        }
                        for(int i = 0; i < DeviceNum; i++)
                        {
                            Berts[i].Complete();
                        }
                        Logger.WriteLog("Overall Train Batch {0}, Average Loss {1} ", batchNum, totalLoss / batchNum );


                        dataLoader.SetDataset(testSet);
                        dataLoader.Init();

                        for(int i = 0; i < DeviceNum; i++)
                        {
                            Berts[i].SetPredictMode();
                            Berts[i].Init();
                        }

                        totalLoss = 0;
                        batchNum = 0;
                        float accuracy = 0;
                        float smp = 0;
                        while(true)
                        {
                            dataLoader.Forward();
                            if(dataLoader.IsTerminate) break;
                            
                            int[] tmp_accuracy = new int[DeviceNum];
                            int[] tmp_smp = new int[DeviceNum];
                            float[] stepLosses = new float[DeviceNum];

                            Parallel.For(0, DeviceNum, i => 
                            {
                                Cudalib.CudaInit(BuilderParameters.GPUIDS[i]);

                                int* token_ptr = dataLoader.TokenIdxs.CpuPtr + BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE * i;
                                {
                                    Cudalib.CudaCopyInFloat(Berts[i].TokenIds.CudaPtr, (IntPtr)token_ptr, BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE);
                                }
                                
                                int* segment_ptr = dataLoader.SegmentIds.CpuPtr + BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE * i;
                                {
                                    Cudalib.CudaCopyInFloat(Berts[i].SegmentIds.CudaPtr, (IntPtr)segment_ptr, BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE);
                                }

                                int* mask_ptr = dataLoader.MaskIds.CpuPtr + BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE * i;
                                {
                                    Cudalib.CudaCopyInFloat(Berts[i].MaskIds.CudaPtr, (IntPtr)mask_ptr, BuilderParameters.MAX_SEQ * BuilderParameters.BATCH_SIZE);
                                }

                                for(int b = 0; b < BuilderParameters.BATCH_SIZE; b++)
                                {
                                    Berts[i].Labels.Idx[b] = dataLoader.Labels.Idx[b + i * BuilderParameters.BATCH_SIZE] - i * BuilderParameters.CATEGORY * BuilderParameters.BATCH_SIZE;
                                }
                                Berts[i].Labels.Idx.SyncFromCPU();

                                Berts[i].StepV3Run();

                                stepLosses[i] = (float)Berts[i].StepLoss;
                                tmp_accuracy[i] = Berts[i].CERunner.CurrentAcc;
                                tmp_smp[i] = Berts[i].CERunner.CurrentSmp;
                            });
                            
                            accuracy += tmp_accuracy.Sum();
                            smp += tmp_smp.Sum();
                            float loss = (float)stepLosses.Sum() / DeviceNum;

                            totalLoss += loss;
                            batchNum += 1;

                            if(batchNum % 100 == 0)
                            {
                                Logger.WriteLog("Predict Batch {0}, Average Accuracy {1}, Average Loss {2}", batchNum, accuracy * 1.0f / smp, totalLoss / batchNum);
                            }
                        }
                        for(int i = 0; i < DeviceNum; i++)
                        {
                            Berts[i].Complete();
                        }

                    } 
                    break;
                case DNNRunMode.Predict:
                    break;
            }
        }
    }
}
