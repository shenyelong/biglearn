using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Language
{
    unsafe public class MetaLargeBertQA : ComputationGraph
    {
        public string name = "MetaBertLargeModel";

        public class BertQAModel : CompositeNNStructure
        {
            public LargeBert.BertLargeModel BertModel { get; set; }

            public int embed { get { return BertModel.embed; } } // = 1024;
            public int layer { get { return BertModel.layer; } }

            public int SpecialCategory { get; set; }

            public LayerStructure StartVector { get; set; }
            public LayerStructure EndVector { get; set; }
            public LayerStructure SpecialClassifier { get; set; }            

            public BertQAModel(int frozenlayer, int special_category, string meta_file, string bin_file, DeviceType device)
            {
                SpecialCategory = special_category;

                BertModel = AddLayer(new LargeBert.BertLargeModel(frozenlayer, 2, meta_file, bin_file, device));

                SpecialClassifier = AddLayer(new LayerStructure(BertModel.embed, special_category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                
                StartVector = AddLayer(new LayerStructure(BertModel.embed, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                EndVector = AddLayer(new LayerStructure(BertModel.embed, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));


            }

            public BertQAModel(int special_category, DeviceType device)
            {
                SpecialCategory = special_category;
                
                //int mcate, int mlayer, int frozenlayer, DeviceType device)
                
                BertModel = AddLayer(new LargeBert.BertLargeModel(2, 24, -1, device));
                
                SpecialClassifier = AddLayer(new LayerStructure(BertModel.embed, special_category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                
                StartVector = AddLayer(new LayerStructure(BertModel.embed, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                EndVector = AddLayer(new LayerStructure(BertModel.embed, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
            }
            
            //public 

            public BertQAModel(string model_path, DeviceType device) : this(new BinaryReader(File.Open(model_path, FileMode.Open)), device)
            { }

            public BertQAModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                BertModel = DeserializeModel<LargeBert.BertLargeModel>(reader, device);

                SpecialClassifier = DeserializeModel<LayerStructure>(reader, device); 
                StartVector = DeserializeModel<LayerStructure>(reader, device);
                EndVector = DeserializeModel<LayerStructure>(reader, device);

                SpecialCategory = SpecialClassifier.Neural_Out;

                Logger.WriteLog("Specifal Category Classifier : {0}", SpecialCategory);
            
            }

            // initialize meta optimization. 
            public void InitMetaOptimizer(RunnerBehavior behavior)
            {
                AllocateGradient(behavior.Device);

                BertModel.TokenEmbed.EmbeddingOptimizer = new 
                BertModel.PosEmbed.EmbeddingOptimizer = new 
            }
        }

        public new BertQAModel Model { get; set; }
        LargeBert BertRunner { get; set; } 

        public CudaPieceInt TokenIds { get { return BertRunner.TokenIds; } set { BertRunner.TokenIds = value; } }
        public CudaPieceInt SegmentIds { get { return BertRunner.SegmentIds; } set { BertRunner.SegmentIds = value; } }
        public CudaPieceInt MaskIds { get { return BertRunner.MaskIds; } set { BertRunner.MaskIds = value; } }

        public CudaPieceInt MaskDoc { get; set; }

        public SparseVectorData CateLabels { get; set; }

        // binary classification for category classification.
        public CudaPieceFloat BinCateLabels { get; set; }

        public CudaPieceFloat StartLabel { get; set; }
        public CudaPieceFloat EndLabel { get; set; }

        public int MaxSeqLen { get; set; }

        public int CateObj = 0; // 0: cerunner; 1:softmaxrunner;
        // for softmax runner.
        public SoftmaxObjRunner CateRunner { get; set; }
        // for ce runner.
        public CrossEntropyRunner CategoryRunner { get; set; }

        public CrossEntropyRunner StartRunner { get; set; }

        public CrossEntropyRunner EndRunner { get; set; }

        HiddenBatchData StartV { get; set; }
        HiddenBatchData EndV { get; set; }
        HiddenBatchData CateV { get; set; }

        public double Loss1 { get { return CateObj == 0 ? CategoryRunner.ObjectiveScore : CateRunner.CurrentPerplexity; } }
        public double Loss2 { get { return StartRunner.ObjectiveScore; } }
        public double Loss3 { get { return EndRunner.ObjectiveScore; } }
        
        public MetaLargeBertQA(BertQAModel model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior;  
            Model = model;  
            SetDelegateModel(Model);
        }

        public void BuildCG(int batchSize, int max_seq_len, float dropout, int cateObj, int poolType) 
        {
            MaxSeqLen = max_seq_len;
            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);

            CateLabels = new SparseVectorData(batchSize, Behavior.Device, false);
            CateLabels.Value.Init(1);

            // binary category labels.
            BinCateLabels = new CudaPieceFloat(batchSize * Model.SpecialCategory, Behavior.Device);

            StartLabel = new CudaPieceFloat(batchSize * max_seq_len, Behavior.Device);
            EndLabel = new CudaPieceFloat(batchSize * max_seq_len, Behavior.Device);

            MaskDoc = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            
            BertRunner = new LargeBert(Model.BertModel, Behavior);
            
            TokenIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            SegmentIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            MaskIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);

            BertRunner.BuildFeatureBlock(batchSize, max_seq_len, Model.layer);
            AddRunner(BertRunner);

            NdArrayData feature = BertRunner.Featurer;
            HiddenBatchData pool = poolType == 0 ? BertRunner.Slice : BertRunner.Pool;
            
            if(dropout > 0)
            {
                feature = feature.Dropout(dropout, Session, Behavior);
                pool = pool.Dropout(dropout, Session, Behavior);
            }

            FullyConnectHiddenRunner<HiddenBatchData> specRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.SpecialClassifier, pool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(specRunner);
            CateV = specRunner.Output;
            
            CateObj = cateObj;

            if(CateObj == 0)
            {
                CategoryRunner = new CrossEntropyRunner(BinCateLabels, CateV, Behavior);
                AddObjective(CategoryRunner);
            }
            else if(CateObj == 1)
            {
                CateRunner = new SoftmaxObjRunner(CateLabels, CateV, true, Behavior); 
                AddObjective(CateRunner); 
            }

            HiddenBatchData doc_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskDoc, MaskDoc.Size, feature.Output, feature.Deriv, max_seq_len * batchSize, Model.embed, Behavior));

            FullyConnectHiddenRunner<HiddenBatchData> startRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.StartVector, doc_embed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(startRunner);
            StartV = startRunner.Output;

            StartRunner = new CrossEntropyRunner(StartLabel, StartV, Behavior); 
            AddObjective(StartRunner);

            FullyConnectHiddenRunner<HiddenBatchData> endRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.EndVector, doc_embed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(endRunner);
            EndV = endRunner.Output;
            
            EndRunner = new CrossEntropyRunner(EndLabel, EndV, Behavior); 
            AddObjective(EndRunner);
        }


        public unsafe void SetDistNum(int cateObjNum, int spanLabelNum)
        {
            if(CateObj == 0)
            {
                CategoryRunner.DistNum = cateObjNum;
            }
            StartRunner.DistNum = spanLabelNum;
            EndRunner.DistNum = spanLabelNum;
        }

        public unsafe int SetupInput(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize)
        {
            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;
            
            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(SegmentIds.CudaPtr, segIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen); 

            int * p_mask_doc = (int * )maskDoc.ToPointer();
            int * p_cate_label = (int *)specLabel.ToPointer();
            
            int * p_start_label = (int *)startLabel.ToPointer();
            int * p_end_label = (int *)endLabel.ToPointer();

            StartLabel.EffectiveSize = batchSize * MaxSeqLen;
            EndLabel.EffectiveSize = batchSize * MaxSeqLen;
            BinCateLabels.EffectiveSize = batchSize * Model.SpecialCategory;
            
            StartLabel.Zero();
            EndLabel.Zero();
            BinCateLabels.Zero(); // Init(0);
            {
                int batch_word = 0;
                for(int b = 0; b < batchSize; b ++)
                {
                    CateLabels.Idx[b] = b * Model.SpecialCategory + p_cate_label[b]; 
                    BinCateLabels[b * Model.SpecialCategory + p_cate_label[b]] = 1; //0.95f;

                    //if(p_cate_label[b] > 0) continue;
                    
                    int word = 0;
                    for(int s = 0; s < MaxSeqLen; s++)
                    {
                        int iter = b * MaxSeqLen + s;
                        if(p_mask_doc[iter] == 1)
                        {
                            MaskDoc[batch_word + word] = iter;
                            word += 1;
                        }
                    }

                    int startIdx = p_start_label[b];
                    int endIdx = p_end_label[b];

                    if(startIdx >= 0 && startIdx < word) StartLabel[batch_word + startIdx] = 1;

                    if(endIdx >= 0 && endIdx < word) EndLabel[batch_word + endIdx] = 1;

                    batch_word += word;
                }
                MaskDoc.EffectiveSize = batch_word;
                MaskDoc.SyncFromCPU();

                CateLabels.Length = batchSize;
                CateLabels.Idx.SyncFromCPU();

                StartLabel.EffectiveSize = batch_word;
                EndLabel.EffectiveSize = batch_word;
                StartLabel.SyncFromCPU();
                EndLabel.SyncFromCPU();

                BinCateLabels.SyncFromCPU();

                return batch_word;
            }
        }


        public unsafe int[] Predict(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr cateLogit, IntPtr startLogit, IntPtr endLogit, int batchSize)
        {
            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;
            
            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(SegmentIds.CudaPtr, segIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen); 

            int[] acc_nums = new int[batchSize + 1];
            acc_nums[0] = 0;
            int * p_mask_doc = (int * )maskDoc.ToPointer();
            {
                int batch_word = 0;
                for(int b = 0; b < batchSize; b ++)
                {
                    int word = 0;
                    for(int s = 0; s < MaxSeqLen; s ++)
                    {
                        int pos = b * MaxSeqLen + s;
                        if(p_mask_doc[pos] == 1)
                        {
                            MaskDoc[batch_word + word] = pos;
                            word += 1;
                        }
                    }
                    batch_word += word;
                    acc_nums[b + 1] = batch_word;
                }
                MaskDoc.EffectiveSize = batch_word;
                MaskDoc.SyncFromCPU();
            }

            if(CateObj == 1)
            {
                CateRunner.IsSavePred = true;
                CateRunner.Pred.Clear();
            }

            StepV3Run();
            
            if(CateObj == 0)
            {
                Cudalib.CudaCopyOutFloat(CateV.Output.Data.CudaPtr, cateLogit, batchSize * Model.SpecialCategory);
            }
            else if(CateObj == 1)
            {
                float * p_cateLogit = (float * )cateLogit.ToPointer();
                {
                    for(int b = 0; b < batchSize; b ++)
                    {
                        for(int s = 0; s < Model.SpecialCategory; s ++)
                        {
                            p_cateLogit[b * Model.SpecialCategory + s] = CateRunner.Pred[b][s];
                        }
                    }
                }
            }

            Cudalib.CudaCopyOutFloat(StartV.Output.Data.CudaPtr, startLogit, MaskDoc.EffectiveSize);
            Cudalib.CudaCopyOutFloat(EndV.Output.Data.CudaPtr, endLogit, MaskDoc.EffectiveSize);

            return acc_nums;
        }
    }


    public class DistMetaLargeBertQA
    {
        public LargeBertQA[] Nets { get; set; }
        int DeviceNum { get { return DeviceIds.Length; } }
        int[] DeviceIds { get; set; }

        IntPtr Comm { get; set; }
        CudaPieceFloat[] Grads { get; set; }

        //public DistBert(Bert.BertBaseModel model, RunnerBehavior behavior)
        //{
        //    Behavior = behavior;  
        //    Model = model;  
        //    SetDelegateModel(Model);
        //}
        LargeBertQA.BertQAModel Model { get; set; }

        public unsafe DistLargeBertQA(LargeBertQA.BertQAModel model, StructureLearner learner, int deviceNum)
        {
            Model = model;
            DeviceIds = new int[deviceNum];
            for(int i = 0; i < deviceNum; i++) DeviceIds[i] = i;

            fixed (int * pDevice = & DeviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, deviceNum);
            }
            Grads = new CudaPieceFloat[deviceNum];
            Nets = new LargeBertQA[deviceNum];

            Parallel.For(0, DeviceNum, i =>  //foreach(int devId in DeviceIds)
            {
                int devId = DeviceIds[i];

                Logger.WriteLog("Create Device {0}", devId);
                DeviceType device = MathOperatorManager.SetDevice(devId);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

                //nt mcate, int mlayer, DeviceType device
                LargeBertQA.BertQAModel device_model = new LargeBertQA.BertQAModel(model.SpecialCategory, device);
                device_model.CopyFrom(model);

                // InitOptimizer(StructureLearner learner, RunnerBehavior behavior) 
                Grads[i] = device_model.InitOptimizer(learner, behavior);
                //Logger.WriteLog("gradient size {0}", Grads[idx].Size);
                // Bert(model, env_mode)
                Nets[i] = new LargeBertQA(device_model, behavior);
                //net.BuildCG(subBatchSize);
                //idx += 1;
            });
        }

        public int MaxSeqLen { get; set; }

        public void BuildCG(int batchSize, int max_seq_len)
        {
            BuildCG(batchSize, max_seq_len, 0);
        }
        public void BuildCG(int batchSize, int max_seq_len, float dropout)
        {
            BuildCG(batchSize, max_seq_len, dropout, 0);
        }
        public void BuildCG(int batchSize, int max_seq_len, float dropout, int cateObj)
        {
            BuildCG(batchSize, max_seq_len, dropout, cateObj, 0);
        }
        public void BuildCG(int batchSize, int max_seq_len, float dropout, int cateObj, int poolType)
        {
            MaxSeqLen = max_seq_len;

            int subBatchSize = batchSize / DeviceNum;
            //int idx = 0;
            //foreach(Bert net in Nets)
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].BuildCG(subBatchSize, max_seq_len, dropout, cateObj, poolType);
                //BuildMaskLM(subBatchSize, max_seq_len, layer, max_mask_len);
            });
        }

        public void Init()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Init();
            });
        }

        public void Complete()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Complete();
            });
        }

        public void SetTrainMode()
        {
            foreach(LargeBertQA net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Train;
            }
        }

        public void SetPredictMode()
        {
            foreach(LargeBertQA net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Predict;
            }
        } 

        public void SaveModel(string path)
        {
            Nets[0].Model.Save(path);
        }

        public unsafe int[] Predict(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr cateProb, IntPtr startLogit, IntPtr endLogit, int batchSize)
        {
            int subBatchSize = batchSize / DeviceNum;

            int[] acc_doc_spans = new int[batchSize + 1];
            acc_doc_spans[0] = 0;

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                int[] acc_doc_span = Nets[i].Predict(IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
                                    IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(maskDoc, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    
                                    IntPtr.Add(cateProb, sizeof(float) * i * subBatchSize * Model.SpecialCategory),
                                    IntPtr.Add(startLogit, sizeof(float) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(endLogit, sizeof(float) * i * subBatchSize * MaxSeqLen),
                                    subBatchSize);

                for(int k = 1; k <= subBatchSize; k++)
                {
                    acc_doc_spans[i * subBatchSize + k] = acc_doc_span[k];
                }
            });

            int lastEnd = 0;
            int[] new_acc_doc_span = new int[batchSize + 1];
            new_acc_doc_span[0] = lastEnd;

            for(int i = 0; i < DeviceNum; i++)
            {
                for(int l = 1; l <= subBatchSize; l++)
                {
                    new_acc_doc_span[i * subBatchSize + l] = acc_doc_spans[i * subBatchSize + l] + lastEnd;
                }
                lastEnd += acc_doc_spans[(i + 1) * subBatchSize];
            }


            int startPos = acc_doc_spans[subBatchSize];
            
            float * p_start_logit = (float * )startLogit.ToPointer();
            float * p_end_logit = (float * )endLogit.ToPointer();
            
            for(int i = 1; i < DeviceNum; i++)
            {
                for(int l = 0; l < acc_doc_spans[(i + 1) * subBatchSize]; l++)
                {
                    p_start_logit[startPos + l] = p_start_logit[i * subBatchSize * MaxSeqLen + l]; 
                    p_end_logit[startPos + l] = p_end_logit[i * subBatchSize * MaxSeqLen + l]; 
                }
                startPos += acc_doc_spans[(i + 1) * subBatchSize];
            }

            return new_acc_doc_span;
        }

        //public unsafe float[] Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize)
        //{
        //    return Run(tokenIdx, segIdx, maskIdx, maskDoc, specLabel, startLabel, endLabel, batchSize, true);
        //}
        public unsafe float[] Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize)
        {
            return Run(tokenIdx, segIdx, maskIdx, maskDoc, specLabel, startLabel, endLabel, batchSize, 0);
        }

        public unsafe float[] Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize, int distrAvgType)
        {
            float[] loss1 = new float[DeviceNum];
            float[] loss2 = new float[DeviceNum];
            float[] loss3 = new float[DeviceNum];

            int subBatchSize = batchSize / DeviceNum;

            int[] spanNums = new int[DeviceNum];
            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                spanNums[i] = Nets[i].SetupInput(IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
                                    IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(maskDoc, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    
                                    IntPtr.Add(specLabel, sizeof(int) * i * subBatchSize),
                                    
                                    IntPtr.Add(startLabel, sizeof(int) * i * subBatchSize),
                                    IntPtr.Add(endLabel, sizeof(int) * i * subBatchSize),
                                    subBatchSize);
            });

            if(distrAvgType == 1)
            {
                for(int i = 0; i < DeviceNum; i++)
                {
                    Nets[i].SetDistNum(batchSize, batchSize);
                }
            }

            //if(isDistributed)
            //{
            //    int allSpanNum = spanNums.Sum();
            //    for(int i = 0; i < DeviceNum; i++)
            //    {
            //        Nets[i].SetDistNum(batchSize * Model.SpecialCategory, batchSize * MaxSeqLen);
            //    }
            //}

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                Nets[i].StepV3Run();

                loss1[i] = (float)Nets[i].Loss1;
                loss2[i] = (float)Nets[i].Loss2;
                loss3[i] = (float)Nets[i].Loss3;

                //CateRunner.CurrentPerplexity
                //return new float[] { (float)CategoryRunner.ObjectiveScore, (float)StartRunner.ObjectiveScore, (float)EndRunner.ObjectiveScore };

                //Grads[i].Print("all reduce before " + i.ToString(), 100, true);
                if(Nets[i].Behavior.RunMode == DNNRunMode.Train)
                {    
                    //Logger.WriteLog("Device {0} Before Reduce", i);
                    Cudalib.AllReduce(Grads[i].CudaPtr, Grads[i].Size, i, Comm);
                    //Logger.WriteLog("Device {0} After Reduce", i);
                    Nets[i].StepV3Update();
                }
                //Cudalib.Zero(Grads[i].CudaPtr, Grads[i].Size);
                //loss1[i] = loss[0];
                //loss2[i] = loss[1];
                //loss3[i] = loss[2];
                // all reduce happen here.
            });
            return new float[] { loss1.Average(), loss2.Average(), loss3.Average() };
        }

    }

}
