using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    public class VanilleImageLMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("IS-SHARE-EMD", new ParameterArgument("0", "0:no share embedding; 1:share embedding."));
                Argument.Add("W-EMBED", new ParameterArgument("300", "word embedding dimension."));
                
                //Argument.Add("P-EMBED", new ParameterArgument("100", "word embedding dimension."));
                Argument.Add("P-LEN", new ParameterArgument("256", "sentence length."));
                
                Argument.Add("T-LAYER", new ParameterArgument("8", "Transformer Layer."));
                Argument.Add("T-DIM", new ParameterArgument("1024", "Transformer Dimension."));

                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Batch Size."));
                
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("HISTORY", new ParameterArgument("1", "History words been seen."));
                
                //Argument.Add("STRIDE_W", new ParameterArgument("1", "History words been seen."));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }

            public static int IS_SHARE_EMD { get { return int.Parse(Argument["IS-SHARE-EMD"].Value); } }
            
            public static int W_EMBED { get { return int.Parse(Argument["W-EMBED"].Value); } }
            //public static int P_EMBED { get { return int.Parse(Argument["P-EMBED"].Value); } }
            public static int P_LEN { get { return int.Parse(Argument["P-LEN"].Value); } }

            public static int T_LAYER { get { return int.Parse(Argument["T-LAYER"].Value); } }
            public static int T_DIM { get { return int.Parse(Argument["T-DIM"].Value); } }
            
            public static int BATCH_SIZE { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int HISTORY { get { return int.Parse(Argument["HISTORY"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.LANGUAGE_IMAGE_LM; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }
        
        public class TransformerLMModel : CompositeNNStructure
        {
            public EmbedStructure WordInEmbed { get; set; }
            public EmbedStructure WordOutEmbed { get; set; }
            
            public TensorConvStructure Conv { get; set; }
            
            //public DNNStructure Encoder { get; set; }

            public TransformerStructure[] Blocks { get; set; }

            public DNNStructure Decoder { get; set; }

            public TransformerLMModel(int vocabSize, int wordEmdDim, int posNum, DeviceType device)
            {
                WordInEmbed = AddLayer(new EmbedStructure(vocabSize, wordEmdDim, -0.01f, 0.01f, device)); // DeviceType.CPU_FAST_VECTOR));
                
                if(BuilderParameters.IS_SHARE_EMD > 0)
                {
                    WordOutEmbed = WordInEmbed;
                }
                else
                {
                    WordOutEmbed = AddLayer(new EmbedStructure(vocabSize, wordEmdDim, device)); // DeviceType.CPU_FAST_VECTOR));
                }

                int embed = wordEmdDim; //posEmdDim + 

                Conv = AddLayer(new TensorConvStructure(embed, BuilderParameters.HISTORY, 1, BuilderParameters.T_DIM, device));

                // Encoder = AddLayer(new DNNStructure(embed, 
                //                                     new int[] { BuilderParameters.T_DIM }, 
                //                                     new A_Func[] { A_Func.Tanh },
                //                                     new float[] { 0.0f },
                //                                     new bool[] { true },
                //                                     device));

                Blocks = new TransformerStructure[BuilderParameters.T_LAYER];
                for(int i=0; i < BuilderParameters.T_LAYER; i++)
                {
                    Blocks[i] = AddLayer(new TransformerStructure(BuilderParameters.T_DIM, device));
                }

                Decoder = AddLayer(new DNNStructure(BuilderParameters.T_DIM, 
                                                    new int[] { wordEmdDim }, 
                                                    new A_Func[] { A_Func.Linear },
                                                    new float[] { 0.0f },
                                                    new bool[] { true },
                                                    device));
            }

            // public TransformerLMModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
            //     WordInEmbed = DeserializeModel<EmbedStructure>(reader, device); 
                

            //     if(BuilderParameters.IS_SHARE_EMD > 0)
            //     {
            //         WordOutEmbed = WordInEmbed;
            //     }
            //     else
            //     {
            //         WordOutEmbed = DeserializeModel<EmbedStructure>(reader, device);
            //     }

            //     Conv = DeserializeModel<TensorConvStructure>(reader, device);

            //     //Encoder = DeserializeModel<DNNStructure>(reader, device);

            //     Blocks = new TransformerStructure[BuilderParameters.T_LAYER];
            //     for(int i = 0; i < BuilderParameters.T_LAYER; i++)
            //     {
            //         Blocks[i] = DeserializeModel<TransformerStructure>(reader, device);
            //     }

            //     Decoder = DeserializeModel<DNNStructure>(reader, device);
            // }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public static void ImageLMModel(ComputationGraph cg, List<int> data, int vocabSize,
                                       int batchSize, int chunkSize, TransformerLMModel model, RunnerBehavior Behavior)
        {
            ChunkSamplingRunner chunkSampleRunner = new ChunkSamplingRunner(data, vocabSize, chunkSize, BuilderParameters.HISTORY, batchSize, true, 2000, Behavior);
            cg.AddDataRunner(chunkSampleRunner);
            SparseMatrixData chunk = chunkSampleRunner.Output;

            HiddenBatchData w_embed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(chunk.FeatureIdx, chunk.MaxLength, model.WordInEmbed, Behavior));
            //HiddenBatchData p_embed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(chunkSampleRunner.PosIdx, chunk.MaxLength, model.PosEmbed, Behavior));
            //HiddenBatchData wp_embed = w_embed;
            //HiddenBatchData wp_embed = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
            //                                 { w_embed, p_embed }, Behavior));
            
            IntArgument sent_size = new IntArgument("sent_size", chunkSize + BuilderParameters.HISTORY - 1);
            IntArgument[] imageShape = new IntArgument[] { w_embed.Shape[0], sent_size, new IntArgument("Depth", 1), chunk.ShapeRow };
            Tensor4DData w_embed_tensor = new Tensor4DData(Behavior.Device, w_embed.Output.Data, w_embed.Deriv.Data, imageShape);  

            TensorConvRunner convRunner = new TensorConvRunner(model.Conv, w_embed_tensor, 0, 0, 1, 1, A_Func.Tanh, Behavior);
            cg.AddRunner(convRunner);
            Tensor4DData convOutput = convRunner.Output;
            IntArgument newWidth = new IntArgument("encode_emd_dim", convOutput.Width * convOutput.Depth);

            if(convOutput.Height != chunkSize)
            {
                throw new Exception(string.Format("Conv height size is not match with the chunk size {0}, {1}", chunkSize, convOutput.Height));
            }

            NdArrayData tensorEmbed = convOutput.ToND().Transpose(new int[] { 2, 0, 1, 3}, cg.Session, Behavior).Reshape(newWidth, convOutput.Shape[1], convOutput.Shape[3]);

            //IntArgument emd_dim = w_embed.Shape[0];
            
            // IntArgument head_dim = new IntArgument("Conv_Head_Dim", w_embed.Dim / 8);
            // IntArgument head_num = new IntArgument("Conv_Head_Num", 8);
            
            // IntArgument sent_size = new IntArgument("sent_size", chunkSize);
            // IntArgument[] shape = new IntArgument[] { head_dim, head_num, sent_size, chunk.ShapeRow };
            // NdArrayData tensorEmbed = new NdArrayData(shape, w_embed.Output.Data, w_embed.Deriv.Data, Behavior.Device);  

            // NdArrayData sliceEmbed = tensorEmbed.Transpose(new int[] { 0, 2, 3, 1}, cg.Session, Behavior);
            // IntArgument depth = new IntArgument("depth", 1);
            
            // IntArgument[] sliceShape = new IntArgument[] { head_dim, sent_size, depth, chunk.ShapeRow };
            // List<NdArrayData> slices = sliceEmbed.Split(sliceShape, 8, cg.Session, Behavior);

            // for(int i=0;i<8;i++)
            // {
            //     //TensorConvStructure model, Tensor4DData input, int padHeight, int padWidth, int strideHeight, int strideWidth, A_Func af, RunnerBehavior behavior) 
            //     TensorConvRunner convRunner = new TensorConvRunner(model.Conv[i], slices[i].ToT4D(), 0, 0, 1, 1, A_Func.Tanh, Behavior);
            //     cg.AddRunner(convRunner);
            //     Tensor4DData outConvData = convRunner.Output;


            // }

            //tensorEmbed.Transpose(this NdArrayData data, int[] transIdx, List<StructRunner> session, RunnerBehavior behavior)

            //Logger.WriteLog("embed batch size {0}, dim {1} ", wp_embed.MAX_BATCHSIZE, wp_embed.Dim);

            //DNNRunner<HiddenBatchData> encodeRunner = new DNNRunner<HiddenBatchData>(model.Encoder, wp_embed, Behavior);
            //cg.AddRunner(encodeRunner);
            //HiddenBatchData encodeEmbed = encodeRunner.Output;

            //IntArgument emd_dim = new IntArgument("emd_dim", encodeEmbed.Dim);
            //IntArgument sent_size = new IntArgument("sent_size", chunkSize);
            //IntArgument[] shape = new IntArgument[] { emd_dim, sent_size, chunk.ShapeRow };
            //NdArrayData tensorEmbed = new NdArrayData(shape, encodeEmbed.Output.Data, encodeEmbed.Deriv.Data, Behavior.Device);

            for(int i = 0; i < model.Blocks.Length; i++)
            {
                TransformerRunner blockRunner = new TransformerRunner(model.Blocks[i], 16, chunkSize, tensorEmbed, Behavior);
                cg.AddRunner(blockRunner);
                tensorEmbed = blockRunner.Output;
            }
            
            Logger.WriteLog("output embed batch size {0}, dim {1}", convOutput.Shape[1].Default * convOutput.Shape[3].Default, newWidth.Default );

            HiddenBatchData outputEmbed = new HiddenBatchData(convOutput.Shape[1].Default * convOutput.Shape[3].Default, newWidth.Default, tensorEmbed.Output, tensorEmbed.Deriv, Behavior.Device);

            DNNRunner<HiddenBatchData> decodeRunner = new DNNRunner<HiddenBatchData>(model.Decoder, outputEmbed, Behavior);
            cg.AddRunner(decodeRunner);
            HiddenBatchData decodEmbed = decodeRunner.Output;

            MatrixProductRunner scoreProductRunner = new MatrixProductRunner( new MatrixData(decodEmbed), new MatrixData(model.WordOutEmbed), Behavior);
            cg.AddRunner(scoreProductRunner);
            HiddenBatchData score = new HiddenBatchData(scoreProductRunner.Output);

            SoftmaxObjRunner objRunner = new SoftmaxObjRunner(chunkSampleRunner.Label, score, Behavior);

            if(Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddObjective(objRunner); 
            }
            else
            {
                cg.AddRunner(objRunner);
            }

            cg.SetDelegateModel(model);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };
            
            Logger.WriteLog("Loading Training/Validation Data.");
            CorpusDataPanel.Init(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");

            TransformerLMModel lmModel = new TransformerLMModel(CorpusDataPanel.VocabSize, BuilderParameters.W_EMBED, 
                                                                BuilderParameters.P_LEN, device);

            if(! BuilderParameters.SEED_MODEL.Equals(string.Empty))
            {
                lmModel.Deserialize(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)));
            }

            //TransformerLMModel lmModel = BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
            //                                new TransformerLMModel(CorpusDataPanel.VocabSize, BuilderParameters.W_EMBED, 
            //                                                    BuilderParameters.P_LEN, device)
            //                              : new TransformerLMModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            
            lmModel.InitOptimization(behavior);

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:

                    ComputationGraph trainCG = new ComputationGraph(behavior);
                    ImageLMModel(trainCG, CorpusDataPanel.TrainData, CorpusDataPanel.VocabSize, BuilderParameters.BATCH_SIZE, BuilderParameters.P_LEN, lmModel, behavior);

                    ComputationGraph validCG = new ComputationGraph(behavior);
                    ImageLMModel(validCG, CorpusDataPanel.TestData, CorpusDataPanel.VocabSize, 1, BuilderParameters.P_LEN, lmModel, behavior);

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        trainCG.Execute();

                        if(iter % 100 == 0)
                        {
                            validCG.Execute();
                            using(BinaryWriter modelWriter = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "transformerlm." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                                    {
                                        lmModel.Serialize(modelWriter);
                                    }
                        }

                    } 
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph testCG = new ComputationGraph(behavior);
                    ImageLMModel(testCG, CorpusDataPanel.TestData, CorpusDataPanel.VocabSize, 1, BuilderParameters.P_LEN, lmModel, behavior);
                    testCG.Execute();
                    break;
            }
        }
    }
}
