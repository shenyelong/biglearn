using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;

namespace BigLearn.DeepNet.Language
{
        public class BookCorpus 
        {
            public static List<List<List<int>>> TrainData { get; set; }
            
            public static int VocabSize { get; set; }

            public static List<List<List<int>>> LoadBookCorpus(string fileName)
            {
                List<List<List<int>>> chars = new List<List<List<int>>>();
                chars.Add(new List<List<int>>());
                
                using(StreamReader reader = new StreamReader(fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine().Trim();
                        string[] items = reader.ReadLine().Trim().Split(' ');
                        foreach(string s in items)
                        {
                            if(s.Trim().Equals("")) continue;
                            int c = int.Parse(s);
                            chars.Add(c);
                        }
                    }
                }
                return chars;
            }
            // build device graph.
            public static void Init(string dataDir)
            {
                TrainData = LoadData(dataDir + @"/train.txt");
                Logger.WriteLog("Train Word Size {0}", TrainData.Count);

                VocabSize = new int[] { TrainData.Max(), TestData.Max(), ValidData.Max() }.Max() + 1;
                Logger.WriteLog("Vocab Word Size {0}", VocabSize);
                
            }

        }
}
