using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
using BigLearn.DeepNet;

namespace BigLearn.DeepNet.Language
{
		/// <summary>
        /// Sample Graph. 
        /// </summary>
        public class ChunkSamplingRunner : StructRunner
        {
            List<int> Data { get; set; }

            int ChunkSize { get; set; }

            int MaxBatchSize { get; set; }

            bool IsOrder { get; set; }

            int DebugSamples { get; set; }

            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);
            
            int MiniBatchCounter = 0;

            int[] BatchCursor = null;
            int VocabSize { get; set; }
            public SparseVectorData Label = null;

            public new SparseMatrixData Output { get; set; }
            
            public CudaPieceInt PosIdx { get; set; }

            public new CudaPieceInt SampleMargin { get; set; }

            // default 1.
            public int History = 1;


            public ChunkSamplingRunner(List<int> data, int vocabSize, int chunkSize, int maxBatchSize, bool isOrder, int debugSamples, RunnerBehavior behavior) 
                : this(data, vocabSize, chunkSize, 1, maxBatchSize, isOrder, debugSamples, behavior)
            { }

            public ChunkSamplingRunner(List<int> data, int vocabSize, int chunkSize, int history, int maxBatchSize, bool isOrder, int debugSamples, RunnerBehavior behavior) 
                : base(Structure.Empty, behavior)
            {
                Data = data;
                VocabSize = vocabSize;
                ChunkSize = chunkSize;
                History = history;

                BatchCursor = new int[maxBatchSize];

                MaxBatchSize = maxBatchSize;

                IsOrder = isOrder;

                DebugSamples = debugSamples;
                
                Output = new SparseMatrixData(vocabSize, maxBatchSize, maxBatchSize * (chunkSize + history - 1), behavior.Device, false);

                SampleMargin = new CudaPieceInt(maxBatchSize * (chunkSize + history - 1), behavior.Device);
                
                PosIdx = new CudaPieceInt(maxBatchSize * (chunkSize + history - 1), behavior.Device);

                Output.FeatureValue.Init(1);
                for(int i = 0; i < maxBatchSize; i++)
                {
                    Output.SampleIdx[i] = (chunkSize + history - 1) * (i + 1);

                    for(int k = 0; k < chunkSize + history - 1; k++)
                    {
                        SampleMargin[i * (chunkSize + history - 1) + k] = i;
                        PosIdx[i * (chunkSize + history - 1) + k] = k;
                    }
                }
                Output.SampleIdx.SyncFromCPU();
                SampleMargin.SyncFromCPU();
                PosIdx.SyncFromCPU();

                Label = new SparseVectorData(maxBatchSize * chunkSize, behavior.Device, false);
                Label.Value.Init(1);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                MiniBatchCounter = 0;

                if(Behavior.RunMode == DNNRunMode.Train)
                {
                    for(int i = 0; i < MaxBatchSize; i++)
                    {
                        BatchCursor[i] = random.Next(0, Data.Count - ChunkSize - History + 1);
                    }
                }
                else
                {
                    BatchCursor[0] = 0;
                }
            }
            public override void Forward()
            {
                Output.Row = 0;
                Output.Length = 0;

                MiniBatchCounter += 1;
                if (DebugSamples > 0 && MiniBatchCounter > DebugSamples) { IsTerminate = true; return; }

                var perf_dataSample = PerfCounter.Manager.Instance["corpusSampling"].Begin();

                int groupIdx = 0; 
                while (groupIdx < MaxBatchSize)
                {
                    if(IsOrder && BatchCursor[groupIdx] + ChunkSize + History > Data.Count) { break; }
                    if(!IsOrder)
                    {
                        BatchCursor[groupIdx] = random.Next(0, Data.Count - ChunkSize - History + 1);
                    }
                    
                    int lidx = 0;
                    for(int m = 0; m < ChunkSize + History - 1; m++)
                    {
                        Output.FeatureIdx[groupIdx * (ChunkSize + History - 1) + m] = Data[BatchCursor[groupIdx] + m];
                        if(m < History - 1) continue;

                        Label.Idx[groupIdx * ChunkSize + lidx] = (groupIdx  * ChunkSize + lidx) * VocabSize + Data[BatchCursor[groupIdx] + 1 + m];
                        lidx += 1;
                    }
                    

                    if(IsOrder)
                    {
                        BatchCursor[groupIdx] = BatchCursor[groupIdx] + ChunkSize;

                        if(Behavior.RunMode == DNNRunMode.Train)
                        {
                            if(BatchCursor[groupIdx] + ChunkSize + History > Data.Count)
                            {
                                BatchCursor[groupIdx] = 0 + random.Next(ChunkSize);
                            }
                        }
                    }
                    groupIdx += 1;
                }

                Label.Length = groupIdx * ChunkSize;
                Label.Idx.SyncFromCPU();

                Output.Row = groupIdx;
                Output.Length = groupIdx * (ChunkSize + History - 1);
                Output.FeatureIdx.SyncFromCPU();
                
                SampleMargin.EffectiveSize = groupIdx * (ChunkSize + History - 1);
                PosIdx.EffectiveSize = groupIdx * (ChunkSize + History - 1);

                PerfCounter.Manager.Instance["corpusSampling"].TakeCount(perf_dataSample);

                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }
}