using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
using System.Dynamic;

namespace BigLearn.DeepNet.Language
{
    /*
    unsafe public class LargeBertQAV2 : ComputationGraph
    {
        public string name = "BertLargeModel";

        public class BertQAModel : CompositeNNStructure
        {
            public int vocabSize = 30522;
            public int pos = 512;
            public int segType = 2;

            public int queryType = 2;

            public int embed = 1024;
            public int layer = 24;

            public int SpecialCategory { get { return SpecialClassifier.Neural_Out; } }

            // token embedding structure.
            public EmbedStructure TokenEmbed { get; set; }
            
            public EmbedStructure PosEmbed { get; set; }
            
            public EmbedStructure TokenTypeEmbed { get; set; }

            public EmbedStructure QueryTypeEmbed { get; set; }

            public LayerStructure InputNorm { get; set; }
            
            VectorData mScale = null;
            public VectorData NormScale { get { if(mScale == null) mScale = new VectorData(embed, InputNorm.weight, InputNorm.WeightGrad, InputNorm.DeviceType); return mScale; } }
            
            VectorData mBias = null;
            public VectorData NormBias { get { if(mBias == null) mBias = new VectorData(embed, InputNorm.bias, InputNorm.BiasGrad, InputNorm.DeviceType); return mBias; } }

            public TransformerStructure[] Blocks { get; set; }

            public LayerStructure Pooler { get; set; }
            
            public LayerStructure SpecialClassifier { get; set; }
            
            public LayerStructure StartVector { get; set; }

            public LayerStructure EndVector { get; set; }


            void LoadFromTFBert(string meta_file, string bin_file)
            {
                using(StreamReader metaReader = new StreamReader(meta_file))
                using(BinaryReader binReader = new BinaryReader(new FileStream(bin_file, FileMode.Open, FileAccess.Read)))
                {
                    while(!metaReader.EndOfStream)
                    {
                        string[] items = metaReader.ReadLine().Split('\t');
                        string itemName = items[0];
                        int itemSize = int.Parse(items[1]);

                        if(itemName.Equals("bert/embeddings/word_embeddings:0"))
                        {
                            TokenEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize TokenEmbed from checkpoint");
                            continue;
                        }
                        if(itemName.Equals("bert/embeddings/token_type_embeddings:0"))
                        {
                            TokenTypeEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize TokenTypeEmbed from checkpoint");
                            continue;
                        }
                        if(itemName.Equals("bert/embeddings/position_embeddings:0"))
                        {
                            PosEmbed.Embedding.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize PosEmbed from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/embeddings/LayerNorm/gamma:0"))
                        {
                            InputNorm.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize embedding-gamma from checkpoint");
                            continue;
                        }
                        if(itemName.Equals("bert/embeddings/LayerNorm/beta:0"))
                        {
                            InputNorm.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize embedding-beta from checkpoint");
                            continue;
                        }

                        if(itemName.Equals("bert/pooler/dense/kernel:0"))
                        {
                            Pooler.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize pooling-weight from checkpoint");
                            continue;
                        }
                        if(itemName.Equals("bert/pooler/dense/bias:0"))
                        {
                            Pooler.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                            Logger.WriteLog("Initialize pooling-bias from checkpoint");
                            continue;
                        }

                        bool isFound = false;

                        for(int c = 0; c < layer; c++)
                        {
                            if(isFound) break;

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/query/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/query/bias:0"))
                            {
                                byte[] mem = binReader.ReadBytes(sizeof(float) * itemSize);
                                Blocks[c].QKVProject.bias.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                //Blocks[c].QProject.bias.BlockCopy(mem, 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/key/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr + embed, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/key/bias:0"))
                            {
                                Blocks[c].QKVProject.bias.BlockCopy(sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/value/kernel:0"))
                            {
                                float[] tmpMem = new float[itemSize]; 
                                Buffer.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, tmpMem, 0, sizeof(float) * itemSize);
                                fixed(float * pMem = &tmpMem[0])
                                {
                                    BasicMathlib.Matrix_AdditionEx(pMem, embed, pMem, embed, Blocks[c].QKVProject.weight.CpuPtr + 2 * embed, 3 * embed, embed, itemSize / embed, 1, 0, 0);
                                }
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/self/value/bias:0"))
                            {
                                Blocks[c].QKVProject.bias.BlockCopy(2 * sizeof(float) * itemSize, binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/dense/kernel:0"))
                            {
                                Blocks[c].AttProject.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/dense/bias:0"))
                            {
                                Blocks[c].AttProject.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/LayerNorm/gamma:0"))
                            {
                                Blocks[c].Norm1.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/attention/output/LayerNorm/beta:0"))
                            {
                                Blocks[c].Norm1.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }

                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/intermediate/dense/kernel:0"))
                            {
                                Blocks[c].MLP1.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/intermediate/dense/bias:0"))
                            {
                                Blocks[c].MLP1.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/dense/kernel:0"))
                            {
                                Blocks[c].MLP2.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/dense/bias:0"))
                            {
                                Blocks[c].MLP2.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/LayerNorm/gamma:0"))
                            {
                                Blocks[c].Norm2.weight.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                            if(itemName.Equals("bert/encoder/layer_"+c.ToString()+"/output/LayerNorm/beta:0"))
                            {
                                Blocks[c].Norm2.bias.BlockCopy(binReader.ReadBytes(sizeof(float) * itemSize), 0, sizeof(float) * itemSize);
                                Logger.WriteLog("Initialize" + itemName + " from checkpoint");
                                isFound = true;
                                continue;
                            }
                        }
                        if(!isFound)
                        {
                            Logger.WriteLog("Component is not found {0} : size {1}, skip...", itemName, itemSize);
                            binReader.ReadBytes(sizeof(float) * itemSize);
                        }
                    }
                }
                TokenEmbed.Embedding.SyncFromCPU();
                TokenTypeEmbed.Embedding.SyncFromCPU();
                PosEmbed.Embedding.SyncFromCPU();
                InputNorm.SyncFromCPU();
                Pooler.SyncFromCPU();
                for(int c = 0; c < layer; c++) Blocks[c].SyncFromCPU();
            }



            public BertQAModel(int special_category, DeviceType device)
            {
                TokenEmbed = AddLayer(new EmbedStructure(vocabSize, embed, -0.034f, 0.034f, device)); 
                
                PosEmbed = AddLayer(new EmbedStructure(pos, embed, -0.034f, 0.034f, device)); 

                TokenTypeEmbed = AddLayer(new EmbedStructure(segType, embed, -0.034f, 0.034f, device));

                QueryTypeEmbed = AddLayer(new EmbedStructure(queryType, embed, 0, 0, device));

                InputNorm = AddLayer(new LayerStructure(1, embed, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

                InputNorm.weight.Init(1);
                InputNorm.bias.Init(0);

                Blocks = new TransformerStructure[layer];
                for(int i = 0; i < layer; i++)
                {
                    Blocks[i] = AddLayer(new TransformerStructure(embed, device));   
                }

                Pooler = AddLayer(new LayerStructure(embed, embed, A_Func.Tanh, N_Type.Convolution_layer, 1, 0, true, device));
                Pooler.weight.Init(2 * 0.034f, -0.034f);

                SpecialClassifier = AddLayer(new LayerStructure(embed, special_category, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                
                StartVector = AddLayer(new LayerStructure(embed, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));
                
                EndVector = AddLayer(new LayerStructure(embed, 1, A_Func.Linear, N_Type.Convolution_layer, 1, 0, true, device));

                SpecialClassifier.weight.IsRandomVector = true;
                
                StartVector.weight.IsRandomVector = true;

                EndVector.weight.IsRandomVector = true;
            }


            public BertQAModel(int special_category, string meta_file, string bin_file, DeviceType device) : this(special_category, device)
            {
                LoadFromTFBert(meta_file, bin_file);
            }

            //public BertQAModel(string model_path, DeviceType device) : this(new BinaryReader(File.Open(model_path, FileMode.Open)), device) 
            //{
            //    Logger.WriteLog("special_category : {0}", SpecialCategory);
            //}

            // public BertQAModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

            //     TokenEmbed = DeserializeModel<EmbedStructure>(reader, device);
            //     PosEmbed = DeserializeModel<EmbedStructure>(reader, device);
            //     TokenTypeEmbed = DeserializeModel<EmbedStructure>(reader, device);
            //     QueryTypeEmbed = DeserializeModel<EmbedStructure>(reader, device);
            //     // AddLayer(new EmbedStructure(queryType, embed, 0, 0, device));

            //     InputNorm = DeserializeModel<LayerStructure>(reader, device);

            //     Blocks = new TransformerStructure[layer];
            //     for(int i=0; i < layer; i++)
            //     {
            //         Blocks[i] = DeserializeModel<TransformerStructure>(reader, device);     
            //     }

            //     Pooler = DeserializeModel<LayerStructure>(reader, device); 
            //     //AddLayer(new LayerStructure(embed, embed, A_Func.Tanh, N_Type.Convolution_layer, 1, 0, true, device));
            //     //Pooler.weight.Init(2 * 0.034f, -0.034f);

            //     SpecialClassifier = DeserializeModel<LayerStructure>(reader, device); 
            //     StartVector = DeserializeModel<LayerStructure>(reader, device);
            //     EndVector = DeserializeModel<LayerStructure>(reader, device);

            //     Logger.WriteLog("Specifal Category Classifier : {0}", SpecialCategory);
            // }
        }

        public new BertQAModel Model { get; set; }

        public CudaPieceInt TokenIds { get; set; }
        public CudaPieceInt SegmentIds { get; set; }
        public CudaPieceInt MaskIds { get; set; }
        public CudaPieceInt QueryMaskIds { get; set; }


        public CudaPieceInt MaskDoc { get; set; }

        //public SparseVectorData CateLabels { get; set; }

        // binary classification for category classification.
        public CudaPieceFloat BinCateLabels { get; set; }

        public CudaPieceFloat StartLabel { get; set; }
        public CudaPieceFloat EndLabel { get; set; }

        public int MaxSeqLen { get; set; }

        
        // for ce runner.
        public CrossEntropyRunner CategoryRunner { get; set; }

        public CrossEntropyRunner StartRunner { get; set; }

        public CrossEntropyRunner EndRunner { get; set; }

        HiddenBatchData StartV { get; set; }
        HiddenBatchData EndV { get; set; }
        HiddenBatchData CateV { get; set; }

        public double Loss1 { get { return CategoryRunner.ObjectiveScore; } }
        public double Loss2 { get { return StartRunner.ObjectiveScore; } }
        public double Loss3 { get { return EndRunner.ObjectiveScore; } }
        
        public LargeBertQAV2(BertQAModel model, RunnerBehavior behavior) : base(behavior)
        {
            Behavior = behavior;  
            Model = model;  
            SetDelegateModel(Model);
        }

        public void BuildCG(int batchSize, int max_seq_len, float dropout, int poolType) 
        {
            MaxSeqLen = max_seq_len;

            TokenIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            SegmentIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            MaskIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            QueryMaskIds = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);

            MaskDoc = new CudaPieceInt(batchSize * max_seq_len, Behavior.Device);
            

            // binary category labels.
            BinCateLabels = new CudaPieceFloat(batchSize * Model.SpecialCategory, Behavior.Device);

            StartLabel = new CudaPieceFloat(batchSize * max_seq_len, Behavior.Device);
            EndLabel = new CudaPieceFloat(batchSize * max_seq_len, Behavior.Device);

            HiddenBatchData w_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.TokenEmbed, Behavior));
            HiddenBatchData t_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(SegmentIds, SegmentIds.Size, Model.TokenTypeEmbed, Behavior));
            HiddenBatchData q_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(QueryMaskIds, QueryMaskIds.Size, Model.QueryTypeEmbed, Behavior));

            IntArgument emd_dim = new IntArgument("emd", Model.embed);
            IntArgument sent_size = new IntArgument("sent_size", max_seq_len);
            IntArgument batch_size = new IntArgument("batch_size", batchSize);
            IntArgument[] shape = new IntArgument[] { emd_dim, sent_size, batch_size };

            NdArrayData W_tensor = new NdArrayData(shape, w_embed.Output.Data, w_embed.Deriv.Data, Behavior.Device);
            NdArrayData T_tensor = new NdArrayData(shape, t_embed.Output.Data, t_embed.Deriv.Data, Behavior.Device);
            NdArrayData Q_tensor = new NdArrayData(shape, q_embed.Output.Data, q_embed.Deriv.Data, Behavior.Device);

            NdArrayData WTQ_tensor = W_tensor.Add(T_tensor, Session, Behavior).Add(Q_tensor, Session, Behavior);

            IntArgument b_size = new IntArgument("b", 1);
            IntArgument[] p_shape = new IntArgument[] { emd_dim, sent_size, b_size};
            NdArrayData P_tensor = new NdArrayData(p_shape, Model.PosEmbed.Embedding.SubArray(0, Model.embed * max_seq_len), 
                                                            Model.PosEmbed.EmbeddingGrad == null ? null : Model.PosEmbed.EmbeddingGrad.SubArray(0, Model.embed * max_seq_len), Behavior.Device);

            NdArrayData WTQP_tensor = WTQ_tensor.AddExpand(P_tensor, Session, Behavior);

            NdArrayData _normI = WTQP_tensor.Norm(0, Session, Behavior);
            NdArrayData normI = _normI.DotAndAdd(Model.NormScale, Model.NormBias, Session, Behavior);

            int head = 16;
            int attdim_per_head = Model.embed / head;
            Logger.WriteLog("attention dim per head {0}", attdim_per_head);

            CudaPieceFloat scaleVec = new CudaPieceFloat(2, Behavior.Device);
            scaleVec[0] = 0;
            scaleVec[1] = (float)(1.0f / Math.Sqrt(attdim_per_head));
            scaleVec.SyncFromCPU();

            CudaPieceFloat biasVec = new CudaPieceFloat(2, Behavior.Device);
            biasVec[0] = (float)-1e9;
            biasVec[1] = 0;
            biasVec.SyncFromCPU();

            CudaPieceFloat MaskScale = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskIds, MaskIds.Size, new EmbedStructure(2, 1, scaleVec, Behavior.Device), Behavior))).Output.Data; 
            CudaPieceFloat MaskBias = ((HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskIds, MaskIds.Size, new EmbedStructure(2, 1, biasVec, Behavior.Device), Behavior))).Output.Data; 

            Logger.WriteLog("Build transformer block.");
            for(int i = 0; i < Model.layer; i++)
            {
                TransformerRunner blockRunner = new TransformerRunner(Model.Blocks[i], head, max_seq_len, normI, MaskScale, MaskBias, Behavior);
                AddRunner(blockRunner);
                normI = blockRunner.Output;
            }

            IntArgument firstSlice = new IntArgument("first-slice", 1);
            IntArgument offsetDim = new IntArgument("offset", 0);
            
            TensorSliceRunner sliceRunner = new TensorSliceRunner(normI, new IntArgument[] { offsetDim, offsetDim, offsetDim },
                                                                           new IntArgument[] { emd_dim, firstSlice, batch_size },
                                                                           Behavior);
            AddRunner(sliceRunner);
            HiddenBatchData slice = sliceRunner.Output.Reshape(new IntArgument[] { emd_dim, batch_size }).ToHDB();


            HiddenBatchData pool = slice;
            if(poolType == 1)
            {
                FullyConnectHiddenRunner<HiddenBatchData> fcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Pooler, pool, Behavior); // fc2Runner.Output, Behavior);
                AddRunner(fcRunner);
                pool = fcRunner.Output;
            }

            if(dropout > 0)
            {
                normI = normI.Dropout(dropout, Session, Behavior);
                pool = pool.Dropout(dropout, Session, Behavior);
            }

            FullyConnectHiddenRunner<HiddenBatchData> specRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.SpecialClassifier, pool, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(specRunner);
            CateV = specRunner.Output;
            
            CategoryRunner = new CrossEntropyRunner(BinCateLabels, CateV, Behavior) { IsReportBCE = true };
            AddObjective(CategoryRunner);
            

            HiddenBatchData doc_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(MaskDoc, MaskDoc.Size, normI.Output, normI.Deriv, max_seq_len * batchSize, Model.embed, Behavior));

            FullyConnectHiddenRunner<HiddenBatchData> startRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.StartVector, doc_embed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(startRunner);
            StartV = startRunner.Output;

            StartRunner = new CrossEntropyRunner(StartLabel, StartV, Behavior) { IsReportBCE = true }; 
            AddObjective(StartRunner);

            FullyConnectHiddenRunner<HiddenBatchData> endRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.EndVector, doc_embed, Behavior); // fc2Runner.Output, Behavior);
            AddRunner(endRunner);
            EndV = endRunner.Output;
            
            EndRunner = new CrossEntropyRunner(EndLabel, EndV, Behavior) { IsReportBCE = true }; 
            AddObjective(EndRunner);
        }


        public unsafe void SetDistNum(int cateObjNum, int spanLabelNum)
        {
            CategoryRunner.DistNum = cateObjNum;
            StartRunner.DistNum = spanLabelNum;
            EndRunner.DistNum = spanLabelNum;
        }

        // the document should be masked.
        public unsafe int[] SetupInput(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr qryMaskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize, int unknownType)
        {
            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;
            QueryMaskIds.EffectiveSize = batchSize * MaxSeqLen;

            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(SegmentIds.CudaPtr, segIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen); 
            Cudalib.CudaCopyInInt(QueryMaskIds.CudaPtr, qryMaskIdx, batchSize * MaxSeqLen);

            int * p_mask_doc = (int * )maskDoc.ToPointer();

            int * p_cate_label = (int *)specLabel.ToPointer();
            
            int * p_start_label = (int *)startLabel.ToPointer();
            int * p_end_label = (int *)endLabel.ToPointer();

            StartLabel.EffectiveSize = batchSize * MaxSeqLen;
            EndLabel.EffectiveSize = batchSize * MaxSeqLen;
            BinCateLabels.EffectiveSize = batchSize * Model.SpecialCategory;
            
            int unknown_num = 0;
            int start_only_num = 0;
            int end_only_num = 0;

            StartLabel.Zero();
            EndLabel.Zero();
            BinCateLabels.Zero(); // Init(0);
            {
                int batch_word = 0;
                for(int b = 0; b < batchSize; b ++)
                {
                    //if(p_cate_label[b] > 0) continue;
                    int word = 0;
                    for(int s = 0; s < MaxSeqLen; s++)
                    {
                        int iter = b * MaxSeqLen + s;
                        if(p_mask_doc[iter] == 1)
                        {
                            MaskDoc[batch_word + word] = iter;
                            word += 1;
                        }
                    }

                    int startIdx = p_start_label[b];
                    int endIdx = p_end_label[b];

                    bool isStartPoint = false;
                    if(startIdx >= 0 && startIdx < word)
                    { 
                        StartLabel[batch_word + startIdx] = 1;
                        isStartPoint = true;
                    }

                    bool isEndPoint = false;
                    if(endIdx >= 0 && endIdx < word)
                    { 
                        EndLabel[batch_word + endIdx] = 1;
                        isEndPoint = true;
                    }


                    if(unknownType == 0)
                    {
                        BinCateLabels[b * Model.SpecialCategory + p_cate_label[b]] = 1;

                        if(p_cate_label[b] == 3) unknown_num += 1;
                        if(isStartPoint && !isEndPoint) start_only_num += 1;
                        if(!isStartPoint && isEndPoint) end_only_num += 1;

                    }
                    else if(unknownType == 1)
                    {
                        if(isStartPoint && isEndPoint)
                        {
                            BinCateLabels[b * Model.SpecialCategory + p_cate_label[b]] = 1; //0.95f;
                            if(p_cate_label[b] == 3) unknown_num += 1;
                        }
                        else if(!isStartPoint && !isEndPoint)
                        {
                            BinCateLabels[b * Model.SpecialCategory + 3] = 1;
                            unknown_num += 1;
                        }
                        else if(isStartPoint && !isEndPoint)
                        {
                            start_only_num += 1;
                            float converage_len = (word - startIdx) / (endIdx - startIdx);
                            if(converage_len >= 0 && converage_len <= 1)
                            {
                                BinCateLabels[b * Model.SpecialCategory + 3] += (1 - converage_len);
                                BinCateLabels[b * Model.SpecialCategory + p_cate_label[b]] += converage_len;
                            }
                            else
                            {
                                Logger.WriteLog("start point, end point, word number,", startIdx, endIdx, word);
                                BinCateLabels[b * Model.SpecialCategory + 3] = 1;
                                unknown_num += 1;
                            }
                        }
                        else if(!isStartPoint && isEndPoint)
                        {
                            end_only_num += 1;
                            float converage_len = endIdx / (endIdx - startIdx);
                            if(converage_len >= 0 && converage_len <= 1)
                            {
                                BinCateLabels[b * Model.SpecialCategory + 3] += (1 - converage_len);
                                BinCateLabels[b * Model.SpecialCategory + p_cate_label[b]] += converage_len;
                            }
                            else
                            {
                                Logger.WriteLog("start point, end point, word number,", startIdx, endIdx, word);
                                BinCateLabels[b * Model.SpecialCategory + 3] = 1;
                                unknown_num += 1;
                            }
                        }
                    }
                    batch_word += word;
                }
                MaskDoc.EffectiveSize = batch_word;
                MaskDoc.SyncFromCPU();

                StartLabel.EffectiveSize = batch_word;
                EndLabel.EffectiveSize = batch_word;
                
                StartLabel.SyncFromCPU();
                EndLabel.SyncFromCPU();

                BinCateLabels.SyncFromCPU();

                return new int[] { batch_word, unknown_num, start_only_num, end_only_num };
            }
        }


        public unsafe int[] Predict(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr qryMaskIdx, IntPtr maskDoc, IntPtr cateLogit, IntPtr startLogit, IntPtr endLogit, int batchSize)
        {
            TokenIds.EffectiveSize = batchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = batchSize * MaxSeqLen;
            MaskIds.EffectiveSize = batchSize * MaxSeqLen;
            QueryMaskIds.EffectiveSize = batchSize * MaxSeqLen;

            Cudalib.CudaCopyInInt(TokenIds.CudaPtr, tokenIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(SegmentIds.CudaPtr, segIdx, batchSize * MaxSeqLen);
            Cudalib.CudaCopyInInt(MaskIds.CudaPtr, maskIdx, batchSize * MaxSeqLen); 
            Cudalib.CudaCopyInInt(QueryMaskIds.CudaPtr, qryMaskIdx, batchSize * MaxSeqLen);

            int[] acc_nums = new int[batchSize + 1];
            acc_nums[0] = 0;
            int * p_mask_doc = (int * )maskDoc.ToPointer();
            {
                int batch_word = 0;
                for(int b = 0; b < batchSize; b ++)
                {
                    int word = 0;
                    for(int s = 0; s < MaxSeqLen; s ++)
                    {
                        int pos = b * MaxSeqLen + s;
                        if(p_mask_doc[pos] == 1)
                        {
                            MaskDoc[batch_word + word] = pos;
                            word += 1;
                        }
                    }
                    batch_word += word;
                    acc_nums[b + 1] = batch_word;
                }
                MaskDoc.EffectiveSize = batch_word;
                MaskDoc.SyncFromCPU();
            }
 
            StepV3Run();
            
            Cudalib.CudaCopyOutFloat(CateV.Output.Data.CudaPtr, cateLogit, batchSize * Model.SpecialCategory);
            Cudalib.CudaCopyOutFloat(StartV.Output.Data.CudaPtr, startLogit, MaskDoc.EffectiveSize);
            Cudalib.CudaCopyOutFloat(EndV.Output.Data.CudaPtr, endLogit, MaskDoc.EffectiveSize);

            return acc_nums;
        }

    }


    public class DistLargeBertQAV2
    {
        public LargeBertQAV2[] Nets { get; set; }
        int DeviceNum { get { return DeviceIds.Length; } }
        int[] DeviceIds { get; set; }

        IntPtr Comm { get; set; }
        CudaPieceFloat[] Grads { get; set; }

        LargeBertQAV2.BertQAModel Model { get; set; }

        public unsafe DistLargeBertQAV2(LargeBertQAV2.BertQAModel model, StructureLearner learner, int deviceNum)
        {
            Model = model;
            DeviceIds = new int[deviceNum];
            for(int i = 0; i < deviceNum; i++) DeviceIds[i] = i;

            fixed (int * pDevice = & DeviceIds[0])
            {
                Comm = Cudalib.CommInitAll((IntPtr)pDevice, deviceNum);
            }
            Grads = new CudaPieceFloat[deviceNum];
            Nets = new LargeBertQAV2[deviceNum];

            Parallel.For(0, DeviceNum, i =>  //foreach(int devId in DeviceIds)
            {
                int devId = DeviceIds[i];

                Logger.WriteLog("Create Device {0}", devId);
                DeviceType device = MathOperatorManager.SetDevice(devId);
                IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
                RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

                LargeBertQAV2.BertQAModel device_model = new LargeBertQAV2.BertQAModel(model.SpecialCategory, device);
                device_model.CopyFrom(model);

                Grads[i] = device_model.InitOptimizer(learner, behavior);

                Nets[i] = new LargeBertQAV2(device_model, behavior);
            });
        }

        public int MaxSeqLen { get; set; }

        public void BuildCG(int batchSize, int max_seq_len, float dropout, int poolType)
        {
            MaxSeqLen = max_seq_len;

            int subBatchSize = batchSize / DeviceNum;
            //int idx = 0;
            //foreach(Bert net in Nets)
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                // BuildCG(int batchSize, int max_seq_len, float dropout, int poolType)  
                Nets[i].BuildCG(subBatchSize, max_seq_len, dropout, poolType);
                //BuildMaskLM(subBatchSize, max_seq_len, layer, max_mask_len);
            });
        }

        public void Init()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Init();
            });
        }

        public void Complete()
        {
            Parallel.For(0, DeviceNum, i =>
            {
                Cudalib.CudaInit(DeviceIds[i]);
                Nets[i].Complete();
            });
        }

        public void SetTrainMode()
        {
            foreach(LargeBertQAV2 net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Train;
            }
        }

        public void SetPredictMode()
        {
            foreach(LargeBertQAV2 net in Nets)
            {
                net.Behavior.RunMode = DNNRunMode.Predict;
            }
        } 

        public void SaveModel(string path)
        {
            Nets[0].Model.Save(path);
        }
        //10.93.128.213
        public unsafe int[] Predict(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr qryMaskIdx, IntPtr maskDoc, IntPtr cateProb, IntPtr startLogit, IntPtr endLogit, int batchSize)
        {
            int subBatchSize = batchSize / DeviceNum;

            int[] acc_doc_spans = new int[batchSize + 1];
            acc_doc_spans[0] = 0;

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                int[] acc_doc_span = Nets[i].Predict(IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
                                    IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(qryMaskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    
                                    IntPtr.Add(maskDoc, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(cateProb, sizeof(float) * i * subBatchSize * Model.SpecialCategory),
                                    IntPtr.Add(startLogit, sizeof(float) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(endLogit, sizeof(float) * i * subBatchSize * MaxSeqLen),
                                    subBatchSize);

                for(int k = 1; k <= subBatchSize; k++)
                {
                    acc_doc_spans[i * subBatchSize + k] = acc_doc_span[k];
                }
            });

            int lastEnd = 0;
            int[] new_acc_doc_span = new int[batchSize + 1];
            new_acc_doc_span[0] = lastEnd;

            for(int i = 0; i < DeviceNum; i++)
            {
                for(int l = 1; l <= subBatchSize; l++)
                {
                    new_acc_doc_span[i * subBatchSize + l] = acc_doc_spans[i * subBatchSize + l] + lastEnd;
                }
                lastEnd += acc_doc_spans[(i + 1) * subBatchSize];
            }


            int startPos = acc_doc_spans[subBatchSize];
            
            float * p_start_logit = (float * )startLogit.ToPointer();
            float * p_end_logit = (float * )endLogit.ToPointer();
            
            for(int i = 1; i < DeviceNum; i++)
            {
                for(int l = 0; l < acc_doc_spans[(i + 1) * subBatchSize]; l++)
                {
                    p_start_logit[startPos + l] = p_start_logit[i * subBatchSize * MaxSeqLen + l]; 
                    p_end_logit[startPos + l] = p_end_logit[i * subBatchSize * MaxSeqLen + l]; 
                }
                startPos += acc_doc_spans[(i + 1) * subBatchSize];
            }

            return new_acc_doc_span;
        }


        public unsafe float[] Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr qryMaskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize, int distrAvgType, int unknownType)
        {
            float[] loss1 = new float[DeviceNum];
            float[] loss2 = new float[DeviceNum];
            float[] loss3 = new float[DeviceNum];

            int subBatchSize = batchSize / DeviceNum;

            int[] spanNums = new int[DeviceNum];
            int[] unknowns = new int[DeviceNum];
            int[] leftOnly = new int[DeviceNum];
            int[] rightOnly = new int[DeviceNum];

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                int[] r = Nets[i].SetupInput(IntPtr.Add(tokenIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(segIdx, sizeof(int) * i * subBatchSize * MaxSeqLen), 
                                    IntPtr.Add(maskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(qryMaskIdx, sizeof(int) * i * subBatchSize * MaxSeqLen),
                                    IntPtr.Add(maskDoc, sizeof(int) * i * subBatchSize * MaxSeqLen),  
                                    IntPtr.Add(specLabel, sizeof(int) * i * subBatchSize),
                                    
                                    IntPtr.Add(startLabel, sizeof(int) * i * subBatchSize),
                                    IntPtr.Add(endLabel, sizeof(int) * i * subBatchSize),
                                    subBatchSize, unknownType);
                spanNums[i] = r[0];
                unknowns[i] = r[1];
                leftOnly[i] = r[2];
                rightOnly[i] = r[3];
            });

            if(distrAvgType == 1)
            {
                for(int i = 0; i < DeviceNum; i++)
                {
                    Nets[i].SetDistNum(batchSize, batchSize);
                }
            }
            else if(distrAvgType == 2)
            {
                for(int i = 0; i < DeviceNum; i++)
                {
                    Nets[i].SetDistNum(batchSize * Model.SpecialCategory, batchSize);
                }
            }

            Parallel.For(0, DeviceNum, i => 
            {
                Cudalib.CudaInit(DeviceIds[i]);

                Nets[i].StepV3Run();

                loss1[i] = (float)Nets[i].Loss1;
                loss2[i] = (float)Nets[i].Loss2;
                loss3[i] = (float)Nets[i].Loss3;
                
                if(Nets[i].Behavior.RunMode == DNNRunMode.Train)
                {    
                    Cudalib.AllReduce(Grads[i].CudaPtr, Grads[i].Size, i, Comm);
                    Nets[i].StepV3Update();
                }
            });
            return new float[] { loss1.Average(), loss2.Average(), loss3.Average(), unknowns.Sum() * 1.0f / batchSize, leftOnly.Sum() * 1.0f / batchSize, rightOnly.Sum() * 1.0f / batchSize };
        }

    }*/

}
