using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;

using BigLearn;

namespace BigLearn.DeepNet.Language
{

    public class TextDataSet
    {
        public List<int> All_Labels { get; set; }
        public List<int[]> All_Idxs { get; set; }
        public List<int[]> All_Segs { get; set; }
        public List<int[]> All_Masks { get; set; }

        void LoadAllDataset(string file)
        {
            All_Labels = new List<int>();
            All_Idxs = new List<int[]>();
            All_Segs = new List<int[]>();
            All_Masks = new List<int[]>();

            using(StreamReader reader = new StreamReader(file))
            {
                while(!reader.EndOfStream)
                {
                    string[] items = reader.ReadLine().Split('\t');
                    
                    int label = int.Parse(items[0]);
                    All_Labels.Add(label);

                    int[] idxs = items[1].Split(' ').Select(i => int.Parse(i)).ToArray();
                    All_Idxs.Add(idxs);

                    int[] masks = items[2].Split(' ').Select(i => int.Parse(i)).ToArray();
                    All_Masks.Add(masks);

                    int[] segs = items[3].Split(' ').Select(i => int.Parse(i)).ToArray();
                    All_Segs.Add(segs);

                    
                }
            }
        }

        public TextDataSet(string file)
        {
            LoadAllDataset(file);
        }
    }


    public class TextSamplingRunner : StructRunner
    {

        TextDataSet DataSet { get; set; }

        public SparseVectorData Labels { get; set; }

        public CudaPieceInt TokenIdxs { get; set; }
        public CudaPieceInt SegmentIds { get; set; }
        public CudaPieceInt MaskIds { get; set; }

        //public CudaPieceFloat MaskScale { get; set; }
        //public CudaPieceFloat MaskBias { get; set; }
        //public SparseMatrixData MaskIds { get; set; }

        public DataRandomShuffling Shuffle = null;

        Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);
        
        int VocabSize { get; set; }
        int MaxSeqLen { get; set; }
        int MaxBatchSize { get; set; }
        
        //float MaskScaleValue { get; set; }
        //float MaskBiasValue { get; set; }

        int Category { get; set; }

        public TextSamplingRunner(int vocabSize, int category, int maxSeqLen, int maxBatchSize, RunnerBehavior behavior) // float mask_scale, float mask_bias,
            : base(Structure.Empty, behavior)
        {
            VocabSize = vocabSize;
            MaxSeqLen = maxSeqLen;
            MaxBatchSize = maxBatchSize;

            //MaskScaleValue = mask_scale;
            //MaskBiasValue = mask_bias;

            Labels = new SparseVectorData(MaxBatchSize, behavior.Device, false);
            Labels.Value.Init(1);

            TokenIdxs = new CudaPieceInt(MaxBatchSize * MaxSeqLen, behavior.Device); // new SparseMatrixData(vocabSize, maxBatchSize, maxBatchSize * maxSeqLen, behavior.Device, false);
            SegmentIds = new CudaPieceInt(MaxBatchSize * MaxSeqLen, behavior.Device); // new SparseMatrixData(2, maxBatchSize, maxBatchSize * maxSeqLen, behavior.Device, false);
            MaskIds = new CudaPieceInt(MaxBatchSize * MaxSeqLen, behavior.Device);

            //MaskScale = new CudaPieceFloat(MaxBatchSize * MaxSeqLen, behavior.Device);
            //MaskBias = new CudaPieceFloat(MaxBatchSize * MaxSeqLen, behavior.Device);
            
            Category = category;
        }

        public void SetDataset(TextDataSet dataSet)
        {
            DataSet = dataSet;
            Shuffle = new DataRandomShuffling(DataSet.All_Labels.Count, random);
        }

        public override void Init()
        {
            IsTerminate = false;
            IsContinue = true;
            Logger.WriteLog("Data sampling initialize.********************************");
            if(Behavior.RunMode == DNNRunMode.Train) Shuffle.Shuffling();
        }

        public override void Forward()
        {
            var perf_dataSample = PerfCounter.Manager.Instance["corpusSampling"].Begin();

            if(Shuffle.IsDone) { IsTerminate = true; return; }
            
            int groupIdx = 0; 

            Labels.Length = MaxBatchSize;

            TokenIdxs.EffectiveSize = MaxBatchSize * MaxSeqLen;
            SegmentIds.EffectiveSize = MaxBatchSize * MaxSeqLen;

            //MaskIds.Row = MaxBatchSize;
            //MaskIds.Length = MaxBatchSize * MaxSeqLen;
            MaskIds.EffectiveSize = MaxBatchSize * MaxSeqLen;
            //MaskScale.EffectiveSize = MaxBatchSize * MaxSeqLen;
            //MaskBias.EffectiveSize = MaxBatchSize * MaxSeqLen;

            while (groupIdx < MaxBatchSize)
            {
                int idx = Shuffle.OrderNext();

                if( idx == -1) { idx = Shuffle.Random(); } 

                Labels.Idx[groupIdx] = Category * groupIdx + DataSet.All_Labels[idx];

                TokenIdxs.BlockCopy(groupIdx * MaxSeqLen * sizeof(int), DataSet.All_Idxs[idx], 0,  MaxSeqLen * sizeof(int));
                SegmentIds.BlockCopy(groupIdx * MaxSeqLen * sizeof(int), DataSet.All_Segs[idx], 0,  MaxSeqLen * sizeof(int));
                MaskIds.BlockCopy(groupIdx * MaxSeqLen * sizeof(int), DataSet.All_Masks[idx], 0,  MaxSeqLen * sizeof(int));
                
                //for(int i = 0; i < MaxSeqLen; i++)
                //{
                //    MaskIds[groupIdx * MaxSeqLen + i] = DataSet.All_Masks[idx][i];
                    //MaskScale[groupIdx * MaxSeqLen + i] = DataSet.All_Masks[idx][i] * 1.0f * MaskScaleValue;
                    //MaskBias[groupIdx * MaxSeqLen + i] = DataSet.All_Masks[idx][i] > 0 ? 0 : MaskBiasValue;
                //}
                //MaskIds.FeatureIdx.BlockCopy(groupIdx * MaxSeqLen * sizeof(int), DataSet.All_Masks[idx], 0,  MaxSeqLen * sizeof(int));
                groupIdx += 1;
            }

            Labels.Idx.SyncFromCPU();

            TokenIdxs.SyncFromCPU();
            SegmentIds.SyncFromCPU();

            //MaskScale.SyncFromCPU();
            //MaskBias.SyncFromCPU();
            MaskIds.SyncFromCPU();
            //MaskIds.FeatureIdx.SyncFromCPU();

            PerfCounter.Manager.Instance["corpusSampling"].TakeCount(perf_dataSample);
        }   
            
    }
}
