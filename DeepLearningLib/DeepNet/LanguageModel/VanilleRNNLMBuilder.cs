using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;

namespace BigLearn.DeepNet.Language
{
    public class VanilleRNNLMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                Argument.Add("DATA", new ParameterArgument(string.Empty, "Data Folder."));

                Argument.Add("IS-SHARE-EMD", new ParameterArgument("0", "0:no share embedding; 1:share embedding."));
                Argument.Add("EMBED", new ParameterArgument("300", "word embedding dimension."));
                
                Argument.Add("RNN-DIMS", new ParameterArgument("512,512", "RNN embedding dimensions."));
                Argument.Add("DNN-DIMS", new ParameterArgument("512,300", "DNN embedding dimensions."));
                
                Argument.Add("P-LEN", new ParameterArgument("256", "passage Length."));
                
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                // the seed model.
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                Argument.Add("TEST-BATCH", new ParameterArgument("1024", "test number of mini batches."));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string Data { get { return Argument["DATA"].Value; } }

            public static int IS_SHARE_EMD { get { return int.Parse(Argument["IS-SHARE-EMD"].Value); } }
            public static int EMBED { get { return int.Parse(Argument["EMBED"].Value); } }
            
            public static int[] RNN_DIMS { get { return Argument["RNN-DIMS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int P_LEN { get { return int.Parse(Argument["P-LEN"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath  { get { return (LogFile + Argument["MODEL-PATH"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }
            public static int TEST_BATCH { get { return int.Parse(Argument["TEST-BATCH"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.LANGUAGE_RNNLM; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class RNNLMModel : CompositeNNStructure
        {
            public EmbedStructure WordInEmbed { get; set; }
            public EmbedStructure WordOutEmbed { get; set; }

            public LSTMStructure RNN { get; set; }
            public DNNStructure Decoder { get; set; }

            public RNNLMModel(int vocabSize, int embedDim, DeviceType device)
            {
                WordInEmbed = AddLayer(new EmbedStructure(vocabSize, embedDim, device)); // DeviceType.CPU_FAST_VECTOR));
                if(BuilderParameters.IS_SHARE_EMD > 0)
                {
                    WordOutEmbed = WordInEmbed;
                }
                else
                {
                    WordOutEmbed = AddLayer(new EmbedStructure(vocabSize, embedDim, device)); // DeviceType.CPU_FAST_VECTOR));
                }

                RNN = AddLayer(new LSTMStructure(embedDim, BuilderParameters.RNN_DIMS, device));
                Decoder = AddLayer(new DNNStructure(BuilderParameters.RNN_DIMS.Last(), 
                                                    BuilderParameters.DNN_DIMS, 
                                                    BuilderParameters.DNN_DIMS.Select(i => A_Func.Linear).ToArray(),
                                                    BuilderParameters.DNN_DIMS.Select(i => 0.0f).ToArray(),
                                                    BuilderParameters.DNN_DIMS.Select(i => true).ToArray(),
                                                    device));
            }

            // public RNNLMModel(BinaryReader reader, DeviceType device)
            // {
            //     int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
            //     WordInEmbed = DeserializeModel<EmbedStructure>(reader, device); 

            //     if(BuilderParameters.IS_SHARE_EMD > 0)
            //     {
            //         WordOutEmbed = WordInEmbed;
            //     }
            //     else
            //     {
            //         WordOutEmbed = DeserializeModel<EmbedStructure>(reader, device);
            //     }
            //     RNN = DeserializeModel<LSTMStructure>(reader, device);
            //     Decoder = DeserializeModel<DNNStructure>(reader, device);
            // }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public static void TransLMModel(ComputationGraph cg, List<int> data, int vocabSize,
                                       int batchSize, int chunkSize, RNNLMModel model, RunnerBehavior Behavior)
        {
            ChunkSamplingRunner chunkSampleRunner = new ChunkSamplingRunner(data, vocabSize, chunkSize, batchSize, true, 2000, Behavior);
            cg.AddDataRunner(chunkSampleRunner);
            SparseMatrixData chunk = chunkSampleRunner.Output;

            HiddenBatchData embed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(chunk.FeatureIdx, chunk.MaxLength, model.WordInEmbed, Behavior));

            List<HiddenBatchData> preO = new List<HiddenBatchData>();
            List<HiddenBatchData> preC = new List<HiddenBatchData>();

            for(int i = 0; i < model.RNN.LSTMCells.Count; i++)
            {
                preO.Add(new HiddenBatchData(batchSize, model.RNN.LSTMCells[i].CellDim, false, Behavior.Device));
                preC.Add(new HiddenBatchData(batchSize, model.RNN.LSTMCells[i].CellDim, false, Behavior.Device));
                preO[i].Output.Data.Init(0);
                preC[i].Output.Data.Init(0);
            }

            SeqDenseBatchData seqEmbed = new SeqDenseBatchData(
                    //new SequenceDataStat(chunk.MaxRow, chunk.MaxLength, embed.Dim),
                        chunk.SampleIdx, chunkSampleRunner.SampleMargin, 
                        chunk.MaxRow, chunk.MaxLength, embed.Dim,
                        embed.Output.Data, embed.Deriv.Data, Behavior.Device);

            LSTMRunner lstmRunner = new LSTMRunner(model.RNN, seqEmbed, preO, preC, Behavior, true);
            cg.AddRunner(lstmRunner);
            SeqDenseBatchData seqOutput = lstmRunner.Output;

            List<HiddenBatchData> lastO = lstmRunner.LastStatusO;
            List<HiddenBatchData> lastC = lstmRunner.LastStatusC;

            HiddenBatchData outputEmbed = new HiddenBatchData(seqOutput.MAX_SENTSIZE, seqOutput.Dim, seqOutput.SentOutput, seqOutput.SentDeriv, Behavior.Device);

            DNNRunner<HiddenBatchData> decodeRunner = new DNNRunner<HiddenBatchData>(model.Decoder, outputEmbed, Behavior);
            cg.AddRunner(decodeRunner);
            HiddenBatchData decodEmbed = decodeRunner.Output;

            MatrixProductRunner scoreProductRunner = new MatrixProductRunner( new MatrixData(decodEmbed), new MatrixData(model.WordOutEmbed), Behavior);
            cg.AddRunner(scoreProductRunner);
            HiddenBatchData score = new HiddenBatchData(scoreProductRunner.Output);

            cg.AddRunner(new SwapRunner(lastO, preO, Behavior));
            cg.AddRunner(new SwapRunner(lastC, preC, Behavior));
            

            SoftmaxObjRunner objRunner = new SoftmaxObjRunner(chunkSampleRunner.Label, score, Behavior);
            cg.AddObjective(objRunner); 
            cg.SetDelegateModel(model);
        }

        public class SwapRunner : StructRunner
        {
            List<HiddenBatchData> Dst { get; set; }
            List<HiddenBatchData> Src { get; set; }
            public SwapRunner(List<HiddenBatchData> src, List<HiddenBatchData> dst, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            { 
                Dst = dst;
                Src = src;
            }

            public override void Forward()
            {
                for(int i=0; i<Dst.Count;i++)
                {
                    Dst[i].BatchSize = Src[i].BatchSize;
                    Dst[i].Output.Data.CopyFrom(Src[i].Output.Data);
                }
            }
        }

        public static void PredLMModel(ComputationGraph cg, List<int> data, int vocabSize,
                                       int batchSize, int chunkSize, RNNLMModel model, RunnerBehavior Behavior)
        {
            ChunkSamplingRunner chunkSampleRunner = new ChunkSamplingRunner(data, vocabSize, chunkSize, batchSize, true, BuilderParameters.TEST_BATCH, Behavior);
            cg.AddDataRunner(chunkSampleRunner);
            SparseMatrixData chunk = chunkSampleRunner.Output;

            HiddenBatchData embed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(chunk.FeatureIdx, chunk.MaxLength, model.WordInEmbed, Behavior));

            SeqDenseBatchData seqEmbed = new SeqDenseBatchData(
                //new SequenceDataStat(chunk.MaxRow, chunk.MaxLength, embed.Dim),
                        chunk.SampleIdx, chunkSampleRunner.SampleMargin, 
                        chunk.MaxRow, chunk.MaxLength, embed.Dim,
                        embed.Output.Data, embed.Deriv.Data, Behavior.Device);

            List<HiddenBatchData> preO = new List<HiddenBatchData>();
            List<HiddenBatchData> preC = new List<HiddenBatchData>();

            for(int i = 0; i < model.RNN.LSTMCells.Count; i++)
            {
                preO.Add(new HiddenBatchData(batchSize, model.RNN.LSTMCells[i].CellDim, Behavior.RunMode, Behavior.Device));
                preC.Add(new HiddenBatchData(batchSize, model.RNN.LSTMCells[i].CellDim, Behavior.RunMode, Behavior.Device));
                preO[i].Output.Data.Init(0);
                preC[i].Output.Data.Init(0);
            }

            LSTMRunner lstmRunner = new LSTMRunner(model.RNN, seqEmbed, preO, preC, Behavior, true);
            cg.AddRunner(lstmRunner);
            SeqDenseBatchData seqOutput = lstmRunner.Output;

            List<HiddenBatchData> lastO = lstmRunner.LastStatusO;
            List<HiddenBatchData> lastC = lstmRunner.LastStatusC;

            HiddenBatchData outputEmbed = new HiddenBatchData(seqOutput.MAX_SENTSIZE, seqOutput.Dim, seqOutput.SentOutput, seqOutput.SentDeriv, Behavior.Device);

            DNNRunner<HiddenBatchData> decodeRunner = new DNNRunner<HiddenBatchData>(model.Decoder, outputEmbed, Behavior);
            cg.AddRunner(decodeRunner);
            HiddenBatchData decodEmbed = decodeRunner.Output;

            MatrixProductRunner scoreProductRunner = new MatrixProductRunner( new MatrixData(decodEmbed), new MatrixData(model.WordOutEmbed), Behavior);
            cg.AddRunner(scoreProductRunner);
            HiddenBatchData score = new HiddenBatchData(scoreProductRunner.Output);

            SoftmaxObjRunner objRunner = new SoftmaxObjRunner(chunkSampleRunner.Label, score, Behavior);
            cg.AddRunner(objRunner); 

            cg.AddRunner(new SwapRunner(lastO, preO, Behavior));
            cg.AddRunner(new SwapRunner(lastC, preC, Behavior));
            
            cg.SetDelegateModel(model);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            RunnerBehavior behavior = new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib };

            Logger.WriteLog("Loading Training/Validation Data.");
            CorpusDataPanel.Init(BuilderParameters.Data);
            Logger.WriteLog("Load Data Finished.");

            RNNLMModel lmModel = new RNNLMModel(CorpusDataPanel.VocabSize, BuilderParameters.EMBED, device);

            //BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
            //                    new RNNLMModel(CorpusDataPanel.VocabSize, BuilderParameters.EMBED, device)
            //                  : new RNNLMModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device); 
            lmModel.InitOptimization(behavior);

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = new ComputationGraph(behavior);
                    TransLMModel(trainCG, CorpusDataPanel.TrainData, CorpusDataPanel.VocabSize, 32, BuilderParameters.P_LEN, lmModel, behavior);

                    ComputationGraph validCG = new ComputationGraph(behavior);
                    PredLMModel(validCG, CorpusDataPanel.TestData, CorpusDataPanel.VocabSize, 1, BuilderParameters.P_LEN, lmModel, behavior);

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        trainCG.Execute();
                        if(iter % 100 == 0)
                        {
                            validCG.Execute();
                            using(BinaryWriter modelWriter = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "rnnlm." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            {
                                lmModel.Serialize(modelWriter);
                            }
                        }
                    } 
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph testCG = new ComputationGraph(behavior);
                    PredLMModel(testCG, CorpusDataPanel.TestData, CorpusDataPanel.VocabSize, 1, BuilderParameters.P_LEN, lmModel, behavior);

                    testCG.Execute();
                    break;
            }
        }
    }
}
