using BigLearn;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.DeepNet.DSSM
{
	public class DataPanel
    {
        public static List<List<Dictionary<int, float>>> SeqSrc { get; set; }
        public static List<List<Dictionary<int, float>>> SeqTgt { get; set; }

        public static Tuple<List<List<Dictionary<int, float>>>, List<List<Dictionary<int, float>>>> LoadFeaDataset(string inputFile)
        {
        	List<List<Dictionary<int, float>>> Src = new List<List<Dictionary<int, float>>>();
        	List<List<Dictionary<int, float>>> Tgt = new List<List<Dictionary<int, float>>>();

        	using(StreamReader reader = new StreamReader(inputFile))
        	{
        		while(!reader.EndOfStream)
        		{
        			string[] items = reader.ReadLine().Split('\t');
	                string src = items[0].Trim();
	                string tgt = items[1].Trim();
        			
            		List<Dictionary<int, float>> src_fea = TextUtil.Str2Dicts(src);
            		List<Dictionary<int, float>> tgt_fea = TextUtil.Str2Dicts(tgt);

            		Src.Add(src_fea);
            		Tgt.Add(tgt_fea);
        		}
        	}
        	return new Tuple<List<List<Dictionary<int, float>>>, List<List<Dictionary<int, float>>>>(Src, Tgt);
        }

        public static void Init(string input)
        {
        	Tuple<List<List<Dictionary<int, float>>>, List<List<Dictionary<int, float>>>> data = LoadFeaDataset(input);
        	SeqSrc = data.Item1;
        	SeqTgt = data.Item2;
        }
    }        
}
