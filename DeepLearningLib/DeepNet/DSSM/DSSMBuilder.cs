using BigLearn;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.DeepNet.DSSM
{
    public class DSSMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
           public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
           public static string Input { get { return Argument["INPUT-DATA"].Value; } }


           public static string TestStream { get { return Argument["TEST"].Value; } }

           public static string L3GVocab { get { return Argument["L3G-VOCAB"].Value; } }

           public static string TrainSrcBin { get { return Argument["TRAIN-SRC"].Value; } }
           public static string TrainTgtBin { get { return Argument["TRAIN-TGT"].Value; } }
           public static string TrainMatchBin { get { return Argument["TRAIN-MATCH"].Value; } }
           public static bool IsTrainFile { get { return !(TrainSrcBin.Equals(string.Empty) || TrainTgtBin.Equals(string.Empty)); } }
           public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            
           public static int[] SRC_LAYER_DIM { get { return Argument["SRC-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
           public static A_Func[] SRC_ACTIVATION { get { return Argument["SRC-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
           public static bool[] SRC_LAYER_BIAS { get { return Argument["SRC-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
           public static N_Type[] SRC_ARCH { get { return Argument["SRC-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
           public static int[] SRC_WINDOW { get { return Argument["SRC-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
           public static bool IS_SRC_SEED_UPDATE { get { return int.Parse(Argument["SRC-SEED-UPDATE"].Value) > 0; } }
           public static float[] SRC_DROPOUT { get { if (Argument["SRC-DROPOUT"].Value.Equals(string.Empty)) return SRC_LAYER_DIM.Select(i => 0.0f).ToArray(); else return Argument["SRC-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

           public static int[] TGT_LAYER_DIM { get { return Argument["TGT-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
           public static A_Func[] TGT_ACTIVATION { get { return Argument["TGT-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
           public static bool[] TGT_LAYER_BIAS { get { return Argument["TGT-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
           public static N_Type[] TGT_ARCH { get { return Argument["TGT-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
           public static int[] TGT_WINDOW { get { return Argument["TGT-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
           public static bool IS_TGT_SEED_UPDATE { get { return int.Parse(Argument["TGT-SEED-UPDATE"].Value) > 0; } }
           public static float[] TGT_DROPOUT { get { if (Argument["TGT-DROPOUT"].Value.Equals(string.Empty)) return TGT_LAYER_DIM.Select(i => 0.0f).ToArray(); else return Argument["TGT-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

           public static bool IsShareModel { get { return int.Parse(Argument["SHARE-MODEL"].Value) > 0; } }
           public static SimilarityType SimiType { get { return (SimilarityType)(int.Parse(Argument["SIM-TYPE"].Value)); } }

           public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
           public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
           public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }
           public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }
           public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
           public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
           public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
           public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
           public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
           public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
           public static int BatchIteration { get { return int.Parse(Argument["BATCH-ITERATION"].Value); } }
           public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
           public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

           public static string ScoreOutputPath
           {
               get
               {
                   return Argument["SCORE-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.score" : Argument["SCORE-PATH"].Value;
               }
           }
           public static string MetricOutputPath
           {
               get
               {
                   return Argument["METRIC-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.metric" : Argument["METRIC-PATH"].Value;
               }
           }

           public static string SRC_EMBED_FILE { get { return Argument["SRC-EMBED-FILE"].Value; } }
           public static string TGT_EMBED_FILE { get { return Argument["TGT-EMBED-FILE"].Value; } }

           
           
           static BuilderParameters()
           {
               Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));
               Argument.Add("INPUT-DATA", new ParameterArgument(string.Empty, "Train Data."));


               Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "Src Train Data."));
               Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "Tgt Train Data."));
               Argument.Add("TRAIN-MATCH", new ParameterArgument(string.Empty, "Match Train Data."));

               Argument.Add("L3G-VOCAB", new ParameterArgument(string.Empty, "Score Valid Data."));
               Argument.Add("MINI-BATCH", new ParameterArgument(string.Empty, "Mini Batch Size."));

               Argument.Add("SRC-LAYER-DIM", new ParameterArgument("300,300", "Source Layer Dim"));
               Argument.Add("SRC-ACTIVATION", new ParameterArgument("1,1", ParameterUtil.EnumValues(typeof(A_Func))));
               Argument.Add("SRC-LAYER-BIAS", new ParameterArgument("0,0", "Source Layer Bias"));
               Argument.Add("SRC-ARCH", new ParameterArgument("1,0", ParameterUtil.EnumValues(typeof(N_Type))));
               Argument.Add("SRC-WINDOW", new ParameterArgument("3,1", "Source DNN Window Size"));
               Argument.Add("SRC-DROPOUT", new ParameterArgument(string.Empty, "Source DNN Dropout"));

               Argument.Add("TGT-LAYER-DIM", new ParameterArgument("300,300", "Target Layer Dim"));
               Argument.Add("TGT-ACTIVATION", new ParameterArgument("1,1", ParameterUtil.EnumValues(typeof(A_Func))));
               Argument.Add("TGT-LAYER-BIAS", new ParameterArgument("0,0", "Target Layer Bias"));
               Argument.Add("TGT-ARCH", new ParameterArgument("1,0", ParameterUtil.EnumValues(typeof(N_Type))));
               Argument.Add("TGT-WINDOW", new ParameterArgument("3,1", "Target DNN Window Size"));
               Argument.Add("TGT-DROPOUT", new ParameterArgument(string.Empty, "Target DNN Dropout"));

               Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

               Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
               Argument.Add("SHARE-MODEL", new ParameterArgument("0", "Share Source and Target Model; 0 : no share; 1 : share;"));
               Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));

               Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.LogBayesianRating).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
               Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
               Argument.Add("NTRAIL", new ParameterArgument("20", "Negative Sample Number."));

             
               Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
             
               Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.NDCG).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));
               Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
               Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));
               Argument.Add("SRC-EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether src embedding is outputted"));
               Argument.Add("TGT-EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether tgt embedding is outputted"));
           }
        }

        public override BuilderType Type { get { return BuilderType.DSSM; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class CDSSMGraph : ComputationGraph
        {
            public class CDSSMModel : CompositeNNStructure
            {
                public DNNStructure TgtDNN { get; set; }
                public DNNStructure SrcDNN { get; set; }
                public bool TiedModel { get; set; }

                public CDSSMModel(DeviceType device, int srcDim, int tgtDim, int hidDim, int winSize, int hidLayer, A_Func a_func, float dropout, bool isTiedModel)
                {

                    SimiType = simType; // DSSMBuilderParameters.SimiType;
                    IsShareModel = isShareModel; // DSSMBuilderParameters.IsShareModel;
                    int srcFeatureDim = srcDim; // DataPanel.Train.Stat.SrcStat.MAX_FEATUREDIM;
                    int tgtFeatureDim = tgtDim; // DataPanel.Train.Stat.TgtStat.MAX_FEATUREDIM;
                    if (IsShareModel)
                    {
                        srcFeatureDim = Math.Max(srcFeatureDim, tgtFeatureDim);
                        tgtFeatureDim = Math.Max(srcFeatureDim, tgtFeatureDim);
                    }
                    SrcDnn = new DNNStructure(srcFeatureDim, srcLayerDim, srcA_Func, srcBias, srcArch, srcWin, srcDropout, device);
                    SrcDnn.Init();

                    if (IsShareModel) TgtDnn = SrcDnn;
                    else
                    {
                        TgtDnn = new DNNStructure(tgtFeatureDim, tgtLayerDim, tgtA_Func, tgtBias, tgtArch, tgtWin, tgtDropout, device);
                        TgtDnn.Init();
                    }
                }

                public override void Serialize(BinaryWriter mwriter)
                {
                    FeatureEmbed.Serialize(mwriter);
                    mwriter.Write(Layer);
                    for(int i = 0; i < Layer; i++)
                    {
                        InitStates[i].Serialize(mwriter);
                    }
                    RAN.Serialize(mwriter); 
                    SelfPool.Serialize(mwriter);
                    Classifier.Serialize(mwriter);                         
                }

                public CDSSMModel(BinaryReader reader, DeviceType device)
                {
                    DeviceType = device;

                    FeatureEmbed = DeserializeModel<EmbedStructure>(reader, device); 
                    int layer = reader.ReadInt32();
                    InitStates = new List<EmbedStructure>();
                    for(int i = 0; i < layer; i++)
                    {
                        InitStates.Add(DeserializeModel<EmbedStructure>(reader, device));
                    }
                    RAN = DeserializeModel<RANStructure>(reader, device);
                    SelfPool = DeserializeModel<SelfAttPoolStructure>(reader, device);
                    Classifier = DeserializeModel<LayerStructure>(reader, device);
                }

            }

            public new RANModel Model { get; set; }

            public DataRunner BatchRunner { get; set; }
            
            public List<float[]> Pred { get; set; }
            
            public HiddenBatchData RAN_Pool { get { return RAN.Output; } }
            public List<HiddenBatchData> RAN_Att { get { return RAN.AttData; } }
            public List<CudaPieceInt> RAN_Att_Index { get { return RAN.AttIndex; } }
            public RANRunner RAN { get; set; }

            public RANClassifier(RANModel model, RunnerBehavior behavior)
            {
                Behavior = behavior;  
                Model = model;  
                SetDelegateModel(Model);
            }

            public void SetTrainSet(DataFrame dataset)
            {
                SetTrainMode();
                BatchRunner.SetDataset(dataset);
            }

            public void SetPredictSet(DataFrame dataset)
            {
                SetPredictMode();
                BatchRunner.SetDataset(dataset);
            }

            
            DumpAttentionRunner AttIdxRunner { get; set; }
            public string DumpAttPath 
            {
                get { return AttIdxRunner.DumpPath; }
                set { AttIdxRunner.DumpPath = value; }
            }

            public void BuildRAN(int maxActiveNum, int batchSize, int head, int recursiveStep)
            {
                // int category, int maxActNum, int maxBatchSize, RunnerBehavior behavior) 
                BatchRunner = new DataRunner(BuilderParameters.Category, maxActiveNum, batchSize, Behavior);
                AddDataRunner(BatchRunner);

                CudaPieceInt TokenIds = BatchRunner.TokenIdxs; //new SparseMatrixData(Model.vocabSize, batchSize, batchSize * max_seq_len, Behavior.Device, false);
                SparseVectorData Labels = BatchRunner.Labels;

                //CudaPieceFloat Label = BatchRunner.Labels;
                //Label = new CudaPieceFloat(batchSize, Behavior.Device);

                Logger.WriteLog("Build CG in data sampling.");
                HiddenBatchData fea_embed = (HiddenBatchData)AddRunner(new LookupEmbedRunner(TokenIds, TokenIds.Size, Model.FeatureEmbed, Behavior));

                // embedding dimension.
                IntArgument emd_dim = new IntArgument("emd", Model.FeatureEmdDim);
                IntArgument active_dim = new IntArgument("active-size", maxActiveNum);
                IntArgument batch_size = new IntArgument("batch_size", batchSize);
                NdArrayData W_tensor = new NdArrayData(Behavior.Device, fea_embed.Output.Data, fea_embed.Deriv.Data, emd_dim, active_dim, batch_size);

                RateScheduler TempRate = new RateScheduler(GlobalUpdateStep, BuilderParameters.Temp);  

                NdArrayData initZero = new NdArrayData(Behavior.Device, false, emd_dim, batch_size);  
                initZero.Output.Init(0); 
                
                List<HiddenBatchData> si_list = new List<HiddenBatchData>();
                for(int i = 0; i < Model.InitStates.Count; i++)
                {
                    HiddenBatchData initState = new HiddenBatchData(1, Model.FeatureEmdDim, Model.InitStates[i].Embedding, Model.InitStates[i].EmbeddingGrad, Model.InitStates[i].DeviceType);
                    HiddenBatchData si = initZero.AddExpand(initState.ToND(), Session, Behavior).ToHDB(); 
                    si_list.Add(si);
                }

                // RANStructure model, NdArrayData input, int nhead, RateScheduler tempRate, bool hardGumbel, List<HiddenBatchData> states, int step, RunnerBehavior behavior
                RAN = new RANRunner(Model.RAN, W_tensor, head, TempRate, BuilderParameters.HardGumbel, si_list, recursiveStep, Behavior);
                AddRunner(RAN);
                
                //List<CudaPieceInt> attIdx, IntArgument batch, int head, string dumpFile, RunnerBehavior behavior)
                AttIdxRunner = new DumpAttentionRunner(RAN.AttIndex, batch_size, head, Behavior);
                AddRunner(AttIdxRunner);
                
                IntArgument state_dim = new IntArgument("state", Model.RAN.StateDim);
                IntArgument recursive_dim = new IntArgument("recursive", recursiveStep);
                NdArrayData recOutput = RAN.Outputs.Concate(Session, Behavior).ToND().Reshape(state_dim, recursive_dim, batch_size);

                SelfAttPoolRunner selfPoolRunner = new SelfAttPoolRunner(Model.SelfPool, recOutput, head, TempRate, BuilderParameters.HardGumbel, Behavior);
                AddRunner(selfPoolRunner);
                
                // selfPoolRunner.Output // RAN.Output
                FullyConnectHiddenRunner<HiddenBatchData> classifierRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Classifier, selfPoolRunner.Output, Behavior); // fc2Runner.Output, Behavior);
                AddRunner(classifierRunner);
                HiddenBatchData o = classifierRunner.Output;
                
                SoftmaxObjRunner CERunner = new SoftmaxObjRunner(Labels, o, true, Behavior); 
                AddObjective(CERunner); 
                CERunner.IsSavePred = true;

                Pred = CERunner.Pred;
            }

        }




        public static ComputationGraph BuildComputationGraph(DSSMDataCashier data, RunnerBehavior Behavior, DSSMStructure dssmModel)
        {
            ComputationGraph cg = new ComputationGraph();

            //cg.SetDataSource(data);
            DSSMData dssmData = (DSSMData)cg.AddDataRunner(new DataRunner<DSSMData, DSSMDataStat>(data, Behavior));

            /// embedding for source text.
            HiddenBatchData srcOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(dssmModel.SrcDnn, dssmData.SrcInput, Behavior));
            /// embedding for target text.
            HiddenBatchData tgtOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(dssmModel.TgtDnn, dssmData.TgtInput, Behavior));

            //BiMatchBatchData matchData = new BiMatchBatchData(data.Stat.SrcStat.MAX_BATCHSIZE,
            //                    Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.NTrail : 0, BuilderParameters.Device, DSMlib.BuilderParameters.RandomSeed != 13);


            IntArgument arg = new IntArgument("batchSize");
            cg.AddRunner(new HiddenDataBatchSizeRunner(srcOutput, arg, Behavior));

            BiMatchBatchData matchData = (BiMatchBatchData)cg.AddRunner(new RandomBiMatchRunner(data.Stat.SrcStat.MAX_BATCHSIZE,
                                (Behavior.RunMode == DNNRunMode.Train || DSSMGraphParameters.Evaluation == EvaluationType.PRECISIONK) ? DSSMGraphParameters.NTrail : 0, arg, Behavior, DeepNet.BuilderParameters.RandomSeed != 13));

            /// similarity between source text and target text.
            HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(srcOutput, tgtOutput, matchData, DSSMGraphParameters.SimiType, Behavior));

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    switch (DSSMGraphParameters.LossFunction)
                    {
                        case LossFunctionType.SampleSoftmax:
                            if (data.IsScore) cg.AddObjective(new MultiClassSoftmaxRunner(null, dssmData.ScoreInput, simiOutput.Output, simiOutput.Deriv, DSSMGraphParameters.Gamma, Behavior));
                            else cg.AddObjective(new MultiClassSoftmaxRunner(null, simiOutput.Output, simiOutput.Deriv, DSSMGraphParameters.Gamma, Behavior));
                            break;
                        case LossFunctionType.SampleLogisicalRegression:
                            if (data.IsScore) cg.AddObjective(new SampleCrossEntropyRunner(dssmData.ScoreInput, simiOutput.Output, simiOutput.Deriv, DSSMGraphParameters.Gamma, Behavior));
                            else cg.AddObjective(new CrossEntropyRunner(matchData.MatchInfo, simiOutput.Output, simiOutput.Deriv, DSSMGraphParameters.Gamma, Behavior));
                            break;
                        case LossFunctionType.Regression:
                            cg.AddObjective(new MSERunner(simiOutput.Output, dssmData.ScoreInput, null, simiOutput.Deriv, Behavior));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    if (!DSSMGraphParameters.SRC_EMBED_FILE.Equals(string.Empty))
                    {
                        cg.AddRunner(new VectorDumpRunner(srcOutput.Output, DSSMGraphParameters.SRC_EMBED_FILE));
                    }

                    // Dump Tgt Embedding Vector.
                    if (!DSSMGraphParameters.TGT_EMBED_FILE.Equals(string.Empty))
                    {
                        cg.AddRunner(new VectorDumpRunner(tgtOutput.Output, DSSMGraphParameters.TGT_EMBED_FILE));
                    }

                    switch (DSSMGraphParameters.Evaluation)
                    {
                        case EvaluationType.NDCG:
                            cg.AddRunner(new NDCGDiskDumpRunner(simiOutput.Output, DSSMGraphParameters.MappingValidData, DSSMGraphParameters.ScoreOutputPath, DSSMGraphParameters.MetricOutputPath));
                            break;
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(dssmData.ScoreInput.Data, simiOutput.Output, DSSMGraphParameters.ScoreOutputPath, DSSMGraphParameters.MetricOutputPath));
                            break;
                        case EvaluationType.MSE:
                            cg.AddRunner(new RMSEDiskDumpRunner(dssmData.ScoreInput.Data, simiOutput.Output, DSSMGraphParameters.Gamma, true, DSSMGraphParameters.ScoreOutputPath));
                            break;
                        case EvaluationType.PRECISIONK:
                            cg.AddRunner(new PrecisionKRunner(simiOutput.Output, matchData, DSSMGraphParameters.MappingValidData, DSSMGraphParameters.ScoreOutputPath));
                            break;
                        case EvaluationType.PLAINOUTPUT:
                            cg.AddRunner(new VectorDumpRunner(simiOutput.Output, DSSMGraphParameters.ScoreOutputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init(BuilderParameters.Input);
            Logger.WriteLog("Load Data Finished.");
 
        }

        
    }
}
