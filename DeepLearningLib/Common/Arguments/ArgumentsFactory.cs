﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BigLearn.Common
{
    public static class ArgumentsFactory<T> where T : ArgumentsBase, new()
    {
        private static Dictionary<string, PropertyInfo> PairArgs;

        private static Dictionary<string, PropertyInfo> FlagArgs;

        static ArgumentsFactory()
        {
            GetArgumentProperties(typeof(T), out PairArgs, out FlagArgs);
        }

        public static T GetInstance(string[] args)
        {
            Dictionary<int, string> unrecognized = new Dictionary<int, string>();
            Dictionary<string, string> kvPairs = CommandLineSwitchesToKeyValuePairs(args, unrecognized);

            T data = GetInstance(kvPairs);
            if (data is CmdArgumentsBase)
            {
                (data as CmdArgumentsBase).UnrecognizedArgs = unrecognized;

                if ((data as CmdArgumentsBase).NeedHelp)
                {
                    data.Usage();
                    Environment.Exit(0);
                }
            }

            return data;
        }

        public static T GetInstance(IEnumerable<KeyValuePair<string, string>> argPairs)
        {
            string jsonStr = argPairs.KeyValuePairs2Json();
            return GetInstance(jsonStr);
        }

        public static T GetInstance(string jsonStr)
        {
            return JsonConvert.DeserializeObject<T>(jsonStr, new ArgumentJsonConverter(), new StringEnumConverter());
        }

        public static T LoadFromFile(string jsonFile)
        {
            return GetInstance(File.ReadAllText(jsonFile));
        }

        private static Dictionary<string, string> CommandLineSwitchesToKeyValuePairs(string[] args, Dictionary<int, string> unrecognized)
        {
            Dictionary<string, string> kvPairs = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            for (int i = 0; i < args.Length;)
            {
                // Command line switches must start with "-"
                if (!args[i].StartsWith("-"))
                {
                    unrecognized[i] = args[i];
                    i++;
                    continue;
                }

                string key = args[i].TrimStart('-');
                string keyPrefix = key.Split(".".ToArray(), StringSplitOptions.RemoveEmptyEntries)[0];

                if (PairArgs.ContainsKey(keyPrefix) && i + 1 < args.Length)
                {
                    PropertyInfo prop = PairArgs[keyPrefix];
                    ArgumentAttribute argAttri = prop.GetArgumentAttribute();
                    if (prop.PropertyType.IsArray)
                    {
                        if (keyPrefix != key)
                        {
                            throw new NotSupportedException(string.Format("Array of complex type are not supported in command line switches. Input switch: -{0}", key));
                        }

                        char[] sep = { ',' };
                        if (argAttri.Separator != null && argAttri.Separator.Length > 0)
                        {
                            sep = argAttri.Separator;
                        }

                        string[] values = args[i + 1].Split(sep, StringSplitOptions.RemoveEmptyEntries);
                        for (int v = 0; v < values.Length; v++)
                        {
                            kvPairs[string.Format("{0}[{1}]", key, v)] = values[v];
                        }
                    }
                    else
                    {
                        kvPairs[key] = args[i + 1];
                    }

                    i += 2;
                }
                else if (FlagArgs.ContainsKey(keyPrefix))
                {
                    kvPairs[key] = "true";
                    i++;
                }
                else
                {
                    unrecognized[i] = args[i];
                    i++;
                }
            }

            return kvPairs;
        }

        public static void GetArgumentProperties(Type type, out Dictionary<string, PropertyInfo> pairArgs, out Dictionary<string, PropertyInfo> flagArgs)
        {
            pairArgs = new Dictionary<string, PropertyInfo>(StringComparer.OrdinalIgnoreCase);
            flagArgs = new Dictionary<string, PropertyInfo>(StringComparer.OrdinalIgnoreCase);

            var argProperties = type.GetArgumentProperties();
            foreach (var prop in argProperties)
            {
                ArgumentAttribute argFieldDef = prop.GetArgumentAttribute();
                string[] switches = argFieldDef.Aliases.Union(new string[] { prop.Name }).ToArray();

                foreach (var s in switches)
                {
                    if (argFieldDef.IsPair)
                    {
                        pairArgs[s] = prop;
                    }
                    else
                    {
                        flagArgs[s] = prop;
                    }
                }
            }
        }
    }
}
