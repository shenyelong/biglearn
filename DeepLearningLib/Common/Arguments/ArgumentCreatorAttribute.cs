﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.Common
{
    public class ArgumentCreatorAttribute : Attribute
    {
        private IArgumentCreator creator;

        public ArgumentCreatorAttribute(Type creatorType)
        {
            if (typeof(IArgumentCreator).IsAssignableFrom(creatorType))
            {
                this.CreatorType = creatorType;
            }
            else
            {
                throw new ArgumentException("The creator type must implement IArgumentCreator interface.");
            }
        }

        public Type CreatorType { get; set; }

        public IArgumentCreator Creator
        {
            get
            {
                if (this.creator == null)
                {
                    this.creator = (IArgumentCreator)Activator.CreateInstance(this.CreatorType);
                }

                return creator;
            }
        }
    }

    public interface IArgumentCreator
    {
        object Create(JObject jobj);
    }
}
