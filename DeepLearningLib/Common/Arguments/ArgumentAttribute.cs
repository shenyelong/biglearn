﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BigLearn.Common
{
    /// <summary>
    /// A property attribute to specify how will the property get value from command line argument.
    /// </summary>
    public class ArgumentAttribute : Attribute
    {
        public ArgumentAttribute(string comments)
            : this(null, comments)
        {
        }

        public ArgumentAttribute(string switches, string comments)
        {
            this.Switches = switches;
            this.Comments = comments;
            this.IsPair = true;
            this.Optional = true;
        }

        /// <summary>
        /// The command line switches of the argument. Switches can be multiple seperated by '|', e.g -V|-Vervose.
        /// It will use the name of the property as the switch by default.
        /// </summary>
        public string Switches { get; set; }

        /// <summary>
        /// If the argument is paired with name and value or just a single flag.
        /// </summary>
        public bool IsPair { get; set; }

        /// <summary>
        /// Comments on the argument about the meaning and usage of it.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// If the argument is optional
        /// </summary>
        public bool Optional { get; set; }

        /// <summary>
        /// Default values
        /// </summary>
        public object Default { get; set; }

        /// <summary>
        /// Separators for array/list parameters, default is ','
        /// </summary>
        public char[] Separator { get; set; }

        public string[] Aliases
        {
            get
            {
                string switchStr = this.Switches ?? string.Empty;
                return switchStr.Split("|".ToArray(), StringSplitOptions.RemoveEmptyEntries).Select(s => s.TrimStart('-')).Distinct(StringComparer.OrdinalIgnoreCase).ToArray();
            }
        }
    }
}
