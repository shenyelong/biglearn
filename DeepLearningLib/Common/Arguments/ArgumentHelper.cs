﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BigLearn.Common
{
    public static class ArgumentHelper
    {
        public static IEnumerable<PropertyInfo> GetArgumentProperties(this Type objectType)
        {
            return objectType.GetProperties().Where(p => Attribute.IsDefined(p, typeof(ArgumentAttribute), true));
        }

        public static ArgumentAttribute GetArgumentAttribute(this PropertyInfo prop)
        {
            return (ArgumentAttribute)Attribute.GetCustomAttribute(prop, typeof(ArgumentAttribute), true);
        }

        public static ArgumentAttribute GetArgumentAttribute(this Type type)
        {
            return (ArgumentAttribute)Attribute.GetCustomAttribute(type, typeof(ArgumentAttribute), true);
        }

        public static IArgumentCreator GetArgumentCreator(this PropertyInfo prop)
        {
            ArgumentCreatorAttribute creatorAttri = (ArgumentCreatorAttribute)Attribute.GetCustomAttribute(prop, typeof(ArgumentCreatorAttribute), true);
            if (creatorAttri != null)
            {
                return creatorAttri.Creator;
            }
            else
            {
                return prop.PropertyType.GetArgumentCreator();
            }
        }

        public static IArgumentCreator GetArgumentCreator(this Type type)
        {
            ArgumentCreatorAttribute creatorAttri = (ArgumentCreatorAttribute)Attribute.GetCustomAttribute(type, typeof(ArgumentCreatorAttribute), true);
            if (creatorAttri != null)
            {
                return creatorAttri.Creator;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// TODO: Only 1-dimention arrays are supported
        /// </summary>
        /// <param name="argPairs"></param>
        /// <returns></returns>
        public static string KeyValuePairs2Json(this IEnumerable<KeyValuePair<string, string>> argPairs)
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            using (JsonWriter jsonWriter = new JsonTextWriter(sw) { Formatting = Formatting.Indented })
            {
                jsonWriter.WriteStartObject();
                int idx;
                var groups = argPairs.GroupBy(kv => ExtractName(kv.Key.Split(".".ToArray(), StringSplitOptions.RemoveEmptyEntries)[0], out idx));

                foreach (var gr in groups)
                {
                    string propertyName = gr.Key;

                    jsonWriter.WritePropertyName(propertyName);
                    string fullName = gr.First().Key;
                    fullName = fullName.Split(".".ToArray(), StringSplitOptions.RemoveEmptyEntries)[0];
                    // The value is an array
                    if (fullName != propertyName)
                    {
                        var arrayGroups = gr.GroupBy(kv =>
                        {
                            int id;
                            string name = ExtractName(kv.Key.Split(".".ToArray(), StringSplitOptions.RemoveEmptyEntries)[0], out id);
                            return id;
                        })
                        .OrderBy(g => g.Key)
                        .Select(g => GetRawValueStr(g, fullName))
                        .ToArray();

                        jsonWriter.WriteStartArray();
                        foreach (var g in arrayGroups)
                        {
                            jsonWriter.WriteRawValue(g);
                        }

                        jsonWriter.WriteEndArray();
                    }
                    else
                    {
                        jsonWriter.WriteRawValue(GetRawValueStr(gr, fullName));
                    }
                }

                jsonWriter.WriteEndObject();
                jsonWriter.Flush();
            }

            return sb.ToString();
        }

        private static string GetRawValueStr(IEnumerable<KeyValuePair<string, string>> kvPairs, string prefixName)
        {
            string valStr = "\"" + kvPairs.First().Value + "\"";
            if (kvPairs.First().Key.Contains('.'))
            {
                var subDict = kvPairs.ToDictionary(kv => kv.Key.Substring((prefixName + ".").Length), kv => kv.Value);
                valStr = KeyValuePairs2Json(subDict);
            }
            else
            {
                valStr = EscapeSingleQuote(valStr);
            }

            return valStr;
        }

        public static string ExtractName(string key, out int index)
        {
            index = -1;
            var match = ArrayPropertyNameRegex.Match(key);
            if (match.Success)
            {
                index = int.Parse(match.Groups[2].Value);
                return match.Groups[1].Value;
            }

            return key;
        }

        private static bool NeedEscape(char ch)
        {
            return !(ch == '\\' || ch == '"' || ch == 'n' || ch == 'r' || ch == 't');
        }

        private static string EscapeSingleQuote(string input)
        {
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
            {
                output.Append(input[i]);
                if (input[i] == '\\' && (i==input.Length-1 || NeedEscape(input[i+1])) && (i == 0 || input[i - 1]!='\\'))
                {
                    output.Append(input[i]);
                }
            }

            return output.ToString();
        }

        private static Regex ArrayPropertyNameRegex = new Regex(@"^([^\[\]]+)\[(\d+)\]$");
    }
}
