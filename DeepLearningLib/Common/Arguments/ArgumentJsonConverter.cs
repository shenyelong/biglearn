﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.Common
{
    /// <summary>
    /// TODO: Only array type are supported
    /// </summary>
    public class ArgumentJsonConverter : JsonConverter
    {
        public ArgumentJsonConverter()
        {
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(ArgumentsBase).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jobj = JObject.Load(reader);
            NormalizePropertyNames(objectType, jobj);

            IArgumentCreator creator = objectType.GetArgumentCreator();
            object target = null;
            if (creator != null)
            {
                target = creator.Create(jobj);
            }
            else
            {
                target = Activator.CreateInstance(objectType);
            }

            Populate(jobj, target, serializer);
            return target;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Type type = value.GetType();
            var argProperties = type.GetArgumentProperties();
            writer.WriteStartObject();
            foreach (var p in argProperties)
            {
                writer.WritePropertyName(p.Name);
                serializer.Serialize(writer, p.GetValue(value));
            }

            writer.WriteEndObject();
        }

        private void NormalizePropertyNames(Type objectType, JObject jobj)
        {
            var argProperties = objectType.GetArgumentProperties();
            Dictionary<string, string> propertyNameAliases = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            foreach (var prop in argProperties)
            {
                ArgumentAttribute argFieldDef = prop.GetArgumentAttribute();
                foreach (var s in argFieldDef.Aliases)
                {
                    propertyNameAliases[s] = prop.Name;
                }

                propertyNameAliases[prop.Name] = prop.Name;
            }

            foreach (var p in jobj.Properties().ToList())
            {
                if (propertyNameAliases.ContainsKey(p.Name))
                {
                    jobj.Remove(p.Name);
                    jobj.Add(propertyNameAliases[p.Name], p.Value);
                }
            }
        }

        private void Populate(JObject obj, object target, JsonSerializer serializer)
        {
            Type type = target.GetType();
            foreach (var p in obj.Properties())
            {
                object val = null;
                PropertyInfo prop = type.GetProperty(p.Name);
                if (prop == null || !prop.CanWrite)
                {
                    continue;
                }

                IArgumentCreator creator = prop.GetArgumentCreator();
                if (creator != null)
                {
                    if (p.Value.Type == JTokenType.Object)
                    {
                        val = creator.Create((JObject)p.Value);
                        Populate((JObject)p.Value, val, serializer);
                    }
                    else if (p.Value.Type == JTokenType.Array)
                    {
                        if (!prop.PropertyType.IsArray)
                        {
                            throw new InvalidCastException(string.Format("Can't convert json array to type {0}", prop.PropertyType.FullName));
                        }

                        val = ExpandArray(p.Value as JArray, creator, prop.PropertyType.GetElementType(), serializer);
                    }
                    else
                    {
                        throw new InvalidCastException(string.Format("Can't convert json type of {0} to type {1}", p.Value.Type, prop.PropertyType.FullName));
                    }
                }
                else if (p.Value.Type != JTokenType.Null)
                {
                    val = p.Value.ToObject(prop.PropertyType, serializer);
                }

                prop.SetValue(target, val);
            }
        }

        private Array ExpandArray(JArray a, IArgumentCreator creator, Type type, JsonSerializer serializer)
        {
            if (creator == null)
            {
                creator = type.GetArgumentCreator();
            }

            Array arrayVal = Array.CreateInstance(type, a.Count);
            var elements = a.Select(e =>
            {
                if (e.Type == JTokenType.Array)
                {
                    return ExpandArray(e as JArray, creator, type.GetElementType(), serializer);
                }
                else if (e.Type == JTokenType.Object && creator != null)
                {
                    object obj = creator.Create(e as JObject);
                    Populate(e as JObject, obj, serializer);
                    return obj;
                }
                else if (e.Type == JTokenType.Null)
                {
                    return null;
                }
                else
                {
                    return e.ToObject(type, serializer);
                }
            });

            int i = 0;
            foreach (var o in elements)
            {
                arrayVal.SetValue(o, i++);
            }

            return arrayVal;
        }
    }
}
