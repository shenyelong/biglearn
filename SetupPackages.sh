#!/bin/bash

#@echo off
nugetTool=./Tools/NuGet/NuGet.exe
echo Pull down BigLearnBuilds package ...
InstallCmd="mono $nugetTool install BigLearnBuilds -Source https://msblox.pkgs.visualstudio.com/_packaging/BigLearnBuilds/nuget/v3/index.json"

mkfifo mypipe
$InstallCmd > mypipe&

while IFS= read -r line;
do 
	if [[ $line =~ "Added package ".* ]]; then
		match="Added package '\(.*\)' to.*"
		package=$(expr "$line" : "$match")
		echo "Newly installed package $package."
	elif [[ $line =~ "Package ".*" is already installed." ]]; then
		match="Package \"\(.*\)\" is already installed."
		package=$(expr "$line" : "$match")
		echo "$package has allready been installed."
	fi
done < mypipe

rm mypipe

if [[ -z $package ]]; then
	echo Failed to install package.
	exit -1 
fi

echo Try to update targets folder with package files ...
if [ -L Targets ]; then
	rm Targets
elif [ -d Targets ]; then 

	rmdir Targets
fi

ln -s Packages/$package/Targets Targets

echo Finished to intall target packages
