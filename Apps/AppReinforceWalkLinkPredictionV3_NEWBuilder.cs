﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
    public class AppReinforceWalkLinkPredictionV3_NEWBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
            public static int TrainBeamSize { get { return int.Parse(Argument["TRAIN-BEAM-SIZE"].Value); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }


            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int SCORE_TYPE { get { return int.Parse(Argument["SCORE-TYPE"].Value); } }
            public static int ROLL_NUM { get { return int.Parse(Argument["ROLL-NUM"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }
            public static float REWARD_MCTS_DISCOUNT { get { return float.Parse(Argument["REWARD-MCTS-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int TEST_MCTS_NUM { get { return int.Parse(Argument["TEST-MCTS-NUM"].Value); } }

            public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static float MSE_LAMBDA { get { return float.Parse(Argument["MSE-LAMBDA"].Value); } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static List<Tuple<int, float>> Reward_Feedback
            {
                get
                {
                    return Argument["R-FB"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static float Epsilon(int step)
            {
                for (int i = 0; i < Reward_Feedback.Count; i++)
                {
                    if (step < Reward_Feedback[i].Item1)
                    {
                        float lambda = (step - Reward_Feedback[i - 1].Item1) * 1.0f / (Reward_Feedback[i].Item1 - Reward_Feedback[i - 1].Item1);
                        return lambda * Reward_Feedback[i].Item2 + (1 - lambda) * Reward_Feedback[i - 1].Item2;
                    }
                }
                return Reward_Feedback.Last().Item2;
            }

            public static int EStrategy(int mcts_idx)
            {
                string[] idxs = Argument["MCTS-EXP"].Value.Split(',').ToArray();
                for (int i = 0; i < idxs.Length; i++)
                {
                    if (mcts_idx < int.Parse(idxs[i]))
                    {
                        return i;
                    }
                }
                return idxs.Length - 1;
            }

            public static float UPDATE_R_DISCOUNT { get { return float.Parse(Argument["UPDATE-R-DISCOUNT"].Value); } }

            public static float BASE_LAMBDA { get { return float.Parse(Argument["BASE-LAMBDA"].Value); } }
            public static float PROB_LAMBDA { get { return float.Parse(Argument["PROB-LAMBDA"].Value); } }
            public static float SCORE_LAMBDA { get { return float.Parse(Argument["SCORE-LAMBDA"].Value); } }

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }

            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int AGG_SCORE { get { return int.Parse(Argument["AGG-SCORE"].Value); } }

            public static int MAP_EVAL { get { return int.Parse(Argument["MAP-EVAL"].Value); } }

            public static float BETA { get { return float.Parse(Argument["BETA"].Value); } }
            public static float GAMMA { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static float TRAIN_C { get { return float.Parse(Argument["TRAIN-C"].Value); } }
            public static float TRAIN_L { get { return float.Parse(Argument["TRAIN-L"].Value); } }
            public static float TEST_C { get { return float.Parse(Argument["TEST-C"].Value); } }
            public static float TEST_L { get { return float.Parse(Argument["TEST-L"].Value); } }

            public static int TRAIN_STRATEGY { get { return int.Parse(Argument["TRAIN-STR"].Value); } }

            public static int MASK_BLACK { get { return int.Parse(Argument["MASK-BLACK"].Value); } }

            /// <summary>
            /// add a reset action.
            /// but another problem is 
            /// </summary>
            public static int RESET_MEM { get { return int.Parse(Argument["RESET-MEM"].Value); } }

            public static int IS_SHARE_NODE { get { return int.Parse(Argument["IS-SHARE-NODE"].Value); } }
            public static int IS_SHARE_RELATION { get { return int.Parse(Argument["IS-SHARE-RELATION"].Value); } }

            public static int IS_REVERSE_REL { get { return int.Parse(Argument["IS-REVERSE-REL"].Value); } }

            public static int IS_SHORTPATH_STAT { get { return int.Parse(Argument["IS-SHORTPATH-STAT"].Value); } }

            public static string INIT_NODE_EMD { get { return Argument["NODE-EMBED"].Value; } }
            public static string INIT_REL_EMD { get { return Argument["REL-EMBED"].Value; } }

            public static int TRAIN_SAMPLES { get { return int.Parse(Argument["TRAIN-SAMPLES"].Value); } }
            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int STEP_FILTER { get { return int.Parse(Argument["STEP-FILTER"].Value); } }

            public static int IS_Q_BEAM { get { return int.Parse(Argument["IS-Q-BEAM"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));
                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));


                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));

                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                Argument.Add("MCTS-NUM", new ParameterArgument("64", "Monto Carlo Tree Search Number"));

                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("GAMMA", new ParameterArgument("0", "hingle loss gamma."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("MASK-BLACK", new ParameterArgument("0", "0: no mask; 1: mask black."));

                Argument.Add("RESET-MEM", new ParameterArgument("1", "epoch numbers to reset memory."));

                Argument.Add("IS-SHARE-NODE", new ParameterArgument("0", "0:no share node; 1: share node."));
                Argument.Add("IS-SHARE-RELATION", new ParameterArgument("0", "0:no share relaiton; 1: share relation."));

                Argument.Add("TRAIN-BEAM-SIZE", new ParameterArgument("1", "training beam size."));
                Argument.Add("IS-REVERSE-REL", new ParameterArgument("0", "1: fill reverse relation in the graph."));

                Argument.Add("IS-SHORTPATH-STAT", new ParameterArgument("1", "0:no short path stat; 1:short path stat;"));

                Argument.Add("NODE-EMBED", new ParameterArgument(string.Empty, "node embedding file."));
                Argument.Add("REL-EMBED", new ParameterArgument(string.Empty, "relation embedding file."));
                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));
                Argument.Add("IS-Q-BEAM", new ParameterArgument("0","0:normal beam search; 1:Q beam search."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_REINFORCE_WALK_LINK_PREDICTION_V3_NEW; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        /// <summary>
        /// episodic memory structure.
        /// </summary>
        public class EpisodicMemory
        {
            Dictionary<string, Tuple<List<Tuple<float, float>>, int>> Mem = new Dictionary<string, Tuple<List<Tuple<float, float>>, int>>();
            public bool IsGlobal = true;
            int Time { get; set; }
            public EpisodicMemory(bool isGlobal)
            {
                IsGlobal = isGlobal;
                Time = 0;
            }
            public void Clear()
            {
                Mem.Clear();
                Time = 0;
            }
            /// <summary>
            /// memory index.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="actionCount"></param>
            /// <param name="topK"></param>
            /// <returns></returns>
            public List<Tuple<float, float>> Search(string key)
            {
                if (Mem.ContainsKey(key))
                {
                    return Mem[key].Item1;
                }
                else
                {
                    return null;
                }
            }

            public void Update(string key, int actid, int totalAct, float reward, float v = 1.0f)
            {
                float new_u = 0;
                float new_c = 0;
                // update memory.
                if (Mem.ContainsKey(key))
                {
                    float u = Mem[key].Item1[actid].Item1;
                    float c = Mem[key].Item1[actid].Item2;
                    new_u = u + reward; // c / (c + v) * u + v / (c + v) * reward;
                    new_c = c + v;
                }
                else
                {
                    Mem.Add(key, new Tuple<List<Tuple<float, float>>, int>(new List<Tuple<float, float>>(), Time));
                    for (int a = 0; a < totalAct; a++)
                    {
                        Mem[key].Item1.Add(new Tuple<float, float>(0, 0));
                    }
                    new_u = reward;
                    new_c = v;
                }
                Mem[key].Item1[actid] = new Tuple<float, float>(new_u, new_c);
            }

            public void UpdateTiming()
            {
                Time += 1;
            }
        }

        public class BanditAlg
        {
            /// <summary>
            /// Exp 0.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UniformRandomStrategy0(int dim, Random random)
            {
                return random.Next(dim);
            }

            /// <summary>
            /// Exp 1.
            /// </summary>
            /// <param name="actdim"></param>
            /// <param name="rand_term"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int CascadeRandomStrategy1(int actdim, float rand_term, Random random)
            {
                if (random.NextDouble() > rand_term)
                {
                    return actdim;
                }
                return random.Next(actdim);
            }

            /// <summary>
            /// Exp 2.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int ThompasSampling(float[] prior, Random random)
            {
                int idx = Util.Sample(prior.ToArray(), random);
                return idx;
            }


            /// <summary>
            /// Exp 3. Standard UCB Bandit.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UCB1Bandit(List<Tuple<float, float>> arms, int dim, float c, Random random)
            {
                if (arms == null)
                {
                    return UniformRandomStrategy0(dim, random);
                }

                float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 0.1f);
                List<float> v = new List<float>();
                foreach (Tuple<float, float> arm in arms)
                {
                    v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
                }
                int idx = Util.MaximumValue(v.ToArray());
                return idx;
            }

            public static int PUCB(List<Tuple<float, float>> arms, List<float> prior, float mb, float c, float m, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float sum = prior.Sum() + mb * prior.Count;

                    float t = (float)arms.Select(i => i.Item2).Sum();

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float si = arms[i].Item1;
                        float xi = (arms[i].Item2 == 0 ? 1 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        float ci = si == 0 ? 0 : c * (float)Math.Sqrt(Math.Log(t) / si);
                        float mi = (t == 0 ? 1 : (float)Math.Sqrt(Math.Log(t) / t)) * m * sum / (prior[i] + mb);
                        float s = xi + ci - mi;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            /// <summary>
            /// Exp 4. PUCT.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int AlphaGoZeroBandit(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(log_total) / (arms[i].Item2 + 1.0f) +
                                                        (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            public static int AlphaGoZeroBanditV2(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i];// + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float total = (float)arms.Select(i => i.Item2).Sum();

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(total) / (arms[i].Item2 + 1.0f) +
                                                       (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBanditV3(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(log_total / (arms[i].Item2 + 1.0f)) +
                                                        (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            /// <summary>
            /// s = 
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int AlphaGoZeroBandit_Stochastic(List<Tuple<float, float>> arms, List<float> prior_q, float d, float lambda, Random random)
            {
                // score = c * p * sqrt(log_total) / n +  
                int armNum = prior_q.Count;
                float[] prob = null;
                //if (arms == null)
                //{
                //    prob = Util.Softmax(prior_q.ToArray(), 1);
                //}
                //else
                {
                    float[] k = new float[prior_q.Count];

                    //float log_total = arms == null ? 0 : (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

                    for (int idx = 0; idx < prior_q.Count; idx++)
                    {
                        k[idx] = (prior_q[idx] * d + (arms == null ? 0 : arms[idx].Item1) + lambda * random.Next()) / (d + (arms == null ? 0 : arms[idx].Item2) + lambda);
                    }
                    prob = Util.Softmax(k, 1);
                }
                return Util.Sample(prob, random);
            }


            /// <summary>
            /// mb = 0.00001;
            /// c = exploration.
            /// m = 
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="mb"></param>
            /// <param name="c"></param>
            /// <param name="m"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int PUCB2(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum());
                    List<float> v = new List<float>();
                    foreach (Tuple<float, float> arm in arms)
                    {
                        v.Add(arm.Item1 / (arm.Item2 + 1.0f) + c * (float)Math.Sqrt(log_total / (arm.Item2)) + (float)random.NextDouble() * 0.0001f);
                    }
                    int idx = Util.MaximumValue(v.ToArray());
                }
                return maxI;
            }


        }

        public class GraphQueryData : BatchData
        {
            public List<int> RawSource = new List<int>();
            public List<Tuple<int, int>> RawQuery = new List<Tuple<int, int>>();
            public List<int> RawTarget = new List<int>();

            // the edge already exists in KB.
            public List<HashSet<int>> BlackTargets = new List<HashSet<int>>();

            public List<int> RawIndex = new List<int>();

            public int BatchSize { get { return RawQuery.Count; } }
            public int MaxBatchSize { get; set; }

            public List<StatusData> StatusPath = new List<StatusData>();
            public List<Tuple<int, int>> Results = new List<Tuple<int, int>>();

            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public GraphQueryData(int maxBatchSize, DeviceType device)
            {
                MaxBatchSize = maxBatchSize;
            }

            public GraphQueryData(GraphQueryData data)
            {
                RawSource = data.RawSource;
                RawQuery = data.RawQuery;
                RawTarget = data.RawTarget;
                BlackTargets = data.BlackTargets;
                RawIndex = data.RawIndex;

                MaxBatchSize = data.MaxBatchSize;
            }

            public List<int> GetBatchIdxs(int b, int step)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < StatusPath[step].BatchSize; i++)
                {
                    if (StatusPath[step].GetOriginalStatsIndex(i) == b)
                        r.Add(i);
                }
                return r;
            }
        }
        
        public class StatusData : BatchData
        {
            /// <summary>
            /// Raw Query.
            /// </summary>
            public GraphQueryData GraphQuery { get; set; }

            /// <summary>
            /// Node ID.
            /// </summary>
            public List<int> NodeID { get; set; }

            /// <summary>
            /// Termination QValue.
            /// </summary>
            // public HiddenBatchData Term = null;

            /// <summary>
            /// Termination QValue.
            /// </summary>
            public HiddenBatchData TermQ = null;

            /// <summary>
            /// Nontermination QValue.
            /// </summary>
            public HiddenBatchData NonTermQ = null;

            /// <summary>
            /// Score for each instance.
            /// </summary>
            public HiddenBatchData Score = null;

            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            List<float> LogProb { get; set; }

            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            public List<Tuple<int, int>> MatchCandidate = null;
            public SeqVectorData MatchCandidateQ = null;
            public SeqVectorData MatchCandidateProb = null;

            public List<int> PreSelActIndex = null;
            public List<int> PreStatusIndex = null;
            public int GetPreStatusIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else return PreStatusIndex[batchIdx];
            }
            public int GetOriginalStatsIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else
                {
                    int b = GetPreStatusIndex(batchIdx);
                    return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
                }
            }

            public List<string> StatusKey = null;
            public string GetStatusKey(int b)
            {
                if (Step == 0) return string.Format("ID:{0} R{2} : N:{1}", GraphQuery.RawIndex[b], DataPanel.EntityLookup[GraphQuery.RawQuery[b].Item1], DataPanel.RelationLookup[GraphQuery.RawQuery[b].Item2]);
                else return StatusKey[b];
            }

            /// <summary>
            /// constrastive reward.
            /// </summary>
            public List<float> CR { get; set; }

            public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.BatchSize; } }

            /// <summary>
            /// logprobability of the node.
            /// </summary>
            /// <param name="batchIdx"></param>
            /// <returns></returns>
            public float GetLogProb(int batchIdx)
            {
                if (Step == 0) { return 0; }
                else { return LogProb[batchIdx]; }
            }

            /// <summary>
            /// Log Termination Probability.
            /// </summary>
            /// <param name="b"></param>
            /// <param name="isT"></param>
            /// <returns></returns>
            public float LogPTerm(int b, bool isT)
            {
                if (isT) { return (float)Util.LogLogistial(TermQ.Output.Data.MemPtr[b] - NonTermQ.Output.Data.MemPtr[b]); }
                else { return (float)Util.LogLogistial(NonTermQ.Output.Data.MemPtr[b] - TermQ.Output.Data.MemPtr[b]); }
            }

            ///// <summary>
            ///// IsBlack Node.
            ///// </summary>
            ///// <param name="b"></param>
            ///// <returns></returns>
            //public bool IsBlack(int b)
            //{
            //    int queryIdx = GetOriginalStatsIndex(b);
            //    if (GraphQuery.BlackTargets[queryIdx].Contains(NodeID[b])) { return true; }
            //    else return false;
            //}

            public int GetActionDim(int b)
            {
                return GetActionEndIndex(b) - GetActionStartIndex(b);
            }

            public int GetActionStartIndex(int b)
            {
                return b == 0 ? 0 : MatchCandidateProb.SegmentIdx.MemPtr[b - 1];
            }
            public int GetActionEndIndex(int b)
            {
                return MatchCandidateProb.SegmentIdx.MemPtr[b];
            }

            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public HiddenBatchData StateEmbed { get; set; }

            public int Step;

            public StatusData(GraphQueryData interData, List<int> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) :
                this(interData, nodeIndex, null, null, null, null, stateEmbed, device)
            { }

            public StatusData(GraphQueryData interData,
                              List<int> nodeIndex, List<float> logProb, List<int> selectedAction, List<int> preStatusIndex, List<string> statusKey,
                              HiddenBatchData stateEmbed, DeviceType device)
            {
                GraphQuery = interData;
                GraphQuery.StatusPath.Add(this);

                NodeID = nodeIndex;
                StateEmbed = stateEmbed;

                LogProb = logProb;
                PreSelActIndex = selectedAction;
                PreStatusIndex = preStatusIndex;
                StatusKey = statusKey;
                //SelectedAction = selectedAction;

                CR = new List<float>();

                Step = GraphQuery.StatusPath.Count - 1;
            }
        }

        /// <summary>
        /// Sample Graph. 
        /// </summary>
        class SampleRunner : StructRunner
        {
            DataEnviroument Data { get; set; }
            public new GraphQueryData Output { get; set; }
            int MaxBatchSize { get; set; }
            int GroupSize { get; set; }
            int Roll { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);

            int MiniBatchCounter = 0;

            public SampleRunner(DataEnviroument data, int groupSize, int roll, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                GroupSize = groupSize;
                Roll = roll;
                MaxBatchSize = GroupSize * Roll;
                Shuffle = new DataRandomShuffling(Data.Triple.Count, random);
                Output = new GraphQueryData(MaxBatchSize, behavior.Device);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
                MiniBatchCounter = 0;
            }

            public override void Forward()
            {
                Output.RawQuery.Clear();
                Output.RawSource.Clear();
                Output.RawTarget.Clear();
                Output.BlackTargets.Clear();
                Output.RawIndex.Clear();

                MiniBatchCounter += 1;

                if(Behavior.RunMode == DNNRunMode.Train && BuilderParameters.TRAIN_SAMPLES > 0 && MiniBatchCounter % BuilderParameters.TRAIN_SAMPLES == 0)
                {
                    IsTerminate = true; return;
                }

                if (Behavior.RunMode == DNNRunMode.Predict && BuilderParameters.TEST_SAMPLES > 0 && MiniBatchCounter % BuilderParameters.TEST_SAMPLES == 0)
                {
                    IsTerminate = true; return;
                }


                int groupIdx = 0;

                while (groupIdx < GroupSize)
                {
                    int idx = Shuffle.RandomNext(); // Behavior.RunMode == DNNRunMode.Train ?  : Shuffle.OrderNext();
                    if (idx <= -1) { break; }
                    if( Behavior.RunMode == DNNRunMode.Train && Data.ShortestReachStep.Count > 0 && 
                        Data.ShortestReachStep[idx] <= BuilderParameters.STEP_FILTER ) { continue; }

                    int srcId = Data.Triple[idx].Item1;
                    int linkId = Data.Triple[idx].Item2;
                    int tgtId = Data.Triple[idx].Item3;
                    HashSet<int> truthIds = Data.TruthDict[new Tuple<int, int>(srcId, linkId)];

                    for (int roll = 0; roll < Roll; roll++)
                    {
                        Output.RawQuery.Add(new Tuple<int, int>(srcId, linkId));
                        Output.RawSource.Add(srcId);
                        Output.RawIndex.Add(idx);
                        Output.RawTarget.Add(tgtId);
                        Output.BlackTargets.Add(truthIds);
                    }
                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }

        class StatusEmbedRunner : CompositeNetRunner
        {
            public new List<Tuple<int, int>> Input { get; set; }

            public new HiddenBatchData Output { get; set; }

            EmbedStructure InNodeEmbed { get; set; }
            EmbedStructure LinkEmbed { get; set; }

            public StatusEmbedRunner(List<Tuple<int, int>> input, int maxBatchSize, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                InNodeEmbed = inNodeEmbed;
                LinkEmbed = linkEmbed;
                // concate of node embedding and relation embedding.
                Output = new HiddenBatchData(maxBatchSize, inNodeEmbed.Dim + linkEmbed.Dim, DNNRunMode.Train, behavior.Device);
            }

            public override void Forward()
            {
                if (BuilderParameters.INIT_NODE_EMD.Equals(string.Empty) || BuilderParameters.INIT_REL_EMD.Equals(string.Empty))
                {
                    InNodeEmbed.Embedding.SyncToCPU();
                    LinkEmbed.Embedding.SyncToCPU();
                }

                //BatchLinks.Clear();
                int batchSize = 0;
                while (batchSize < Input.Count)
                {
                    int srcId = Input[batchSize].Item1;
                    int linkId = Input[batchSize].Item2;

                    int bidx = batchSize;

                    if (BuilderParameters.N_EmbedDim > 0)
                    {
                        FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim,
                                                InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);
                    }
                    FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim + InNodeEmbed.Dim,
                                            LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);
                    batchSize += 1;
                }
                Output.BatchSize = batchSize;
                Output.Output.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (BuilderParameters.INIT_NODE_EMD.Equals(string.Empty) || BuilderParameters.INIT_REL_EMD.Equals(string.Empty))
                {
                    Output.Deriv.Data.SyncToCPU(Output.Dim * Output.BatchSize);

                    InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
                    LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

                    for (int b = 0; b < Output.BatchSize; b++)
                    {
                        int srcIdx = Input[b].Item1;
                        int linkIdx = Input[b].Item2;

                        if (BuilderParameters.N_EmbedDim > 0)
                        {
                            FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
                                            Output.Deriv.Data.MemPtr, b * Output.Dim, InNodeEmbed.Dim, 1, 1);
                        }
                        FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim,
                                        Output.Deriv.Data.MemPtr, b * Output.Dim + InNodeEmbed.Dim, LinkEmbed.Dim, 1, 1);
                    }
                    InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
                    LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
                }
            }
        }

        class CandidateActionRunner : CompositeNetRunner
        {
            public new StatusData Input;
            GraphEnviroument Graph;
            public new List<Tuple<int, int>> Output = new List<Tuple<int, int>>();
            public BiMatchBatchData Match;

            /// <summary>
            /// Input : State.
            /// node and rel embed.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="nodeEmbed"></param>
            /// <param name="relEmbed"></param>
            /// <param name="behavior"></param>
            public CandidateActionRunner(StatusData input,  GraphEnviroument graph, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                Graph = graph;
                Match = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize * Graph.MaxNeighborNum,
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * Graph.MaxNeighborNum
                }, behavior.Device);
            }

            public override void Forward()
            {
                Output.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
                int cursor = 0;
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    // current node.
                    int seedNode = Input.NodeID[b];

                    // query idx. 
                    int qid = Input.GetOriginalStatsIndex(b);

                    // raw node.
                    int rawN = Input.GraphQuery.RawQuery[qid].Item1;

                    // raw relation.
                    int rawR = Input.GraphQuery.RawQuery[qid].Item2;

                    // raw answer.
                    int answerN = Input.GraphQuery.RawTarget[qid];

                    int candidateNum = 0;

                   
                    if (Graph.Graph.ContainsKey(seedNode))
                    {
                        for (int nei = 0; nei < Graph.Graph[seedNode].Count; nei++)
                        {
                            int lid = Graph.Graph[seedNode][nei].Item1;
                            int tgtNode = Graph.Graph[seedNode][nei].Item2;
                            if (seedNode == rawN && lid == rawR && tgtNode == answerN) { continue; }
                            if (seedNode == answerN && lid == DataPanel.ReverseRelation(rawR) && tgtNode == rawN) continue;

                            Output.Add(new Tuple<int, int>(tgtNode, lid));
                            match.Add(new Tuple<int, int, float>(b, cursor, 1));
                            cursor += 1;
                            candidateNum += 1;
                        }
                    }
                    if (candidateNum == 0)
                    {
                        Output.Add(new Tuple<int, int>(seedNode, Graph.StopIdx));
                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
                        cursor += 1;
                        candidateNum += 1;
                        //throw new NotImplementedException("Candidate Number is zero!");
                    }
                }
                Match.SetMatch(match);
            }
        }

        static Random SampleRandom = new Random(DeepNet.BuilderParameters.RandomSeed);

        public class BasicPolicyRunner : CompositeNetRunner
        {
            public new StatusData Input { get; set; }

            /// <summary>
            /// MatchData.
            /// </summary>
            public BiMatchBatchData MatchPath = null;

            /// <summary>
            /// new Node Index.
            /// </summary>
            public List<int> NodeIndex { get; set; }

            /// <summary>
            /// next step log probability.
            /// </summary>
            public List<float> LogProb { get; set; }

            /// <summary>
            /// pre sel action and pre status. 
            /// </summary>
            public List<int> PreSelActIndex { get; set; }
            public List<int> PreStatusIndex { get; set; }

            public List<string> StatusKey { get; set; }

            public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                NodeIndex = new List<int>();
                LogProb = new List<float>();
                PreSelActIndex = new List<int>();
                PreStatusIndex = new List<int>();
                StatusKey = new List<string>();
            }
        }

        
        class BeamSearchActionRunner : BasicPolicyRunner
        {
            int BeamSize = 1;

            bool IsLast = false;
            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchActionRunner(StatusData input, int maxNeighbor, int beamSize, RunnerBehavior behavior, bool isLast = false) : base(input, behavior)
            {
                BeamSize = beamSize;
                IsLast = isLast;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);
            }

            public override void Forward()
            {
                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();
                Input.MatchCandidateQ.Output.SyncToCPU();
                Input.TermQ.Output.SyncToCPU();
                Input.NonTermQ.Output.SyncToCPU();

                NodeIndex.Clear();
                LogProb.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                StatusKey.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                // for each beam.
                for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                {
                    List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);

                    MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                        float nonLogTerm = Input.LogPTerm(b, false);
                        float logTerm = Input.LogPTerm(b, true);
                        //{
                        //    Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, b));
                        //}

                        {
                            Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, b));
                        }

                        for (int t = s; t < e; t++)
                        {
                            float prob = Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                            topKheap.push_pair(new Tuple<int, int>(b, t), prob);
                        }
                        //topKheap.push_pair(new Tuple<int, int>(b, -1), Input.GetLogProb(b) + logTerm);
                    }

                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();

                        //if(p.Key.Item2 == -1) { Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, p.Key.Item1)); continue; }

                        match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));

                        NodeIndex.Add(Input.MatchCandidate[p.Key.Item2].Item1);
                        LogProb.Add(p.Value);

                        PreStatusIndex.Add(p.Key.Item1);
                        PreSelActIndex.Add(p.Key.Item2);

                        string statusQ = string.Format("{0}-R:{2}->N:{1}", Input.GetStatusKey(p.Key.Item1), DataPanel.EntityLookup[Input.MatchCandidate[p.Key.Item2].Item1], 
                            DataPanel.RelationLookup[Input.MatchCandidate[p.Key.Item2].Item2]);

                        StatusKey.Add(statusQ);
                    }
                }
                MatchPath.SetMatch(match);
            }
        }

        class QBeamSearchActionRunner : BasicPolicyRunner
        {
            int BeamSize = 1;

            bool IsLast = false;
            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public QBeamSearchActionRunner(StatusData input, int maxNeighbor, int beamSize, RunnerBehavior behavior, bool isLast = false) : base(input, behavior)
            {
                BeamSize = beamSize;
                IsLast = isLast;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);
            }

            public override void Forward()
            {
                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();
                Input.MatchCandidateQ.Output.SyncToCPU();
                Input.TermQ.Output.SyncToCPU();
                Input.NonTermQ.Output.SyncToCPU();

                NodeIndex.Clear();
                LogProb.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                StatusKey.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                // for each beam.
                for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                {
                    List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);

                    MinMaxHeap<Tuple<int, int, float>> topKheap = new MinMaxHeap<Tuple<int, int, float>>(BeamSize, 1);

                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                        float nonLogTerm = Input.LogPTerm(b, false);
                        float logTerm = Input.LogPTerm(b, true);
                        //{
                        //    Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, b));
                        //}

                        {
                            Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, b));
                        }

                        for (int t = s; t < e; t++)
                        {
                            float prob = Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                            float q = Input.MatchCandidateQ.Output.MemPtr[t]; // Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                            topKheap.push_pair(new Tuple<int, int, float>(b, t, prob), q);
                        }
                        //topKheap.push_pair(new Tuple<int, int>(b, -1), Input.GetLogProb(b) + logTerm);
                    }

                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int, float>, float> p = topKheap.PopTop();

                        //if(p.Key.Item2 == -1) { Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, p.Key.Item1)); continue; }

                        match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));

                        NodeIndex.Add(Input.MatchCandidate[p.Key.Item2].Item1);

                        LogProb.Add(p.Key.Item3);

                        PreStatusIndex.Add(p.Key.Item1);
                        PreSelActIndex.Add(p.Key.Item2);

                        string statusQ = string.Format("{0}-R:{2}->N:{1}", Input.GetStatusKey(p.Key.Item1), DataPanel.EntityLookup[Input.MatchCandidate[p.Key.Item2].Item1], 
                            DataPanel.RelationLookup[Input.MatchCandidate[p.Key.Item2].Item2]);

                        StatusKey.Add(statusQ);
                    }
                }
                MatchPath.SetMatch(match);
            }
        }


        class MCTSActionSamplingRunner : BasicPolicyRunner
        {
            int lineIdx = 0;
            EpisodicMemory Memory { get; set; }
            //Random random = new Random(21);
            public int MCTSIdx { get; set; }
            public override void Init()
            {
                lineIdx = 0;
            }
            bool IsLast = false;
            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSActionSamplingRunner(StatusData input, int maxNeighbor, int mctsIdx, EpisodicMemory memory, RunnerBehavior behavior, bool isLast = false) : base(input, behavior)
            {
                Memory = memory;
                MCTSIdx = mctsIdx;
                IsLast = isLast;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
                NodeIndex.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                LogProb.Clear();
                StatusKey.Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                Input.MatchCandidateQ.Output.SyncToCPU();
                Input.TermQ.Output.SyncToCPU();
                Input.NonTermQ.Output.SyncToCPU();
                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                int currentStep = Input.Step;
                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    int selectIdx = s;

                    string Qkey = Input.GetStatusKey(i);

                    int actionDim = e - s;

                    //key = key + "-" + 
                    List<Tuple<float, float>> m = Memory.Search(Qkey);
                    float[] prior_r = null;
                                        
                    // probability of the policy.
                    {
                        prior_r = new float[actionDim + 1];
                        Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                        float t = Util.Logistic(Input.TermQ.Output.Data.MemPtr[i] - Input.NonTermQ.Output.Data.MemPtr[i]);
                        if (t >= 1 - Util.LargeEpsilon) { t = 1 - Util.LargeEpsilon; }
                        if (t <= Util.LargeEpsilon) { t = Util.LargeEpsilon; }

                        prior_r[actionDim] = t;
                        for (int a = 0; a < actionDim; a++) { prior_r[a] = prior_r[a] * (1 - t); }
                    }

                    //float[] prior_q = null;
                    //{
                    //    prior_q = new float[actionDim + 1];
                    //    Array.Copy(Input.MatchCandidateQ.Output.MemPtr, s, prior_q, 0, actionDim);
                    //    prior_q[actionDim] = Input.Term.Output.Data.MemPtr[i];
                    //}
                    
                    int idx = 0;

                    if (IsLast)
                    {
                        idx = actionDim;
                    }
                    else
                    {
                        int strategy = BuilderParameters.EStrategy(MCTSIdx);
                        //Console.WriteLine("MCTS Index {0}, strategy {1}", MCTSIdx, strategy);
                        switch (strategy)
                        {
                            case 0: idx = BanditAlg.UniformRandomStrategy0(prior_r.Length, SampleRandom); break;
                            case 1: idx = BanditAlg.ThompasSampling(prior_r, SampleRandom); break;
                            case 2: idx = BanditAlg.UCB1Bandit(m, prior_r.Length, BuilderParameters.UCB1_C, SampleRandom); break;
                            case 3: idx = BanditAlg.AlphaGoZeroBandit(m, prior_r.ToList(), BuilderParameters.PUCT_C, SampleRandom); break;
                            case 4: idx = BanditAlg.AlphaGoZeroBanditV2(m, prior_r.ToList(), BuilderParameters.PUCT_C, SampleRandom); break;
                            case 5: idx = BanditAlg.PUCB(m, prior_r.ToList(), BuilderParameters.PUCB_B, BuilderParameters.PUCB_C, BuilderParameters.PUCB_M, SampleRandom); break;
                            //case 6: idx = BanditAlg.AlphaGoZeroBandit_Stochastic(m, prior_q.ToList(), Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.TRAIN_C : BuilderParameters.TEST_C,
                            //                                                                          Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.TRAIN_L : BuilderParameters.TEST_L, SampleRandom); break;
                        }
                    }

                    if (idx == actionDim)
                    {
                        Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, i));
                    }
                    else
                    {
                        selectIdx = s + idx;

                        PreSelActIndex.Add(selectIdx);
                        PreStatusIndex.Add(i);

                        string statusQ = string.Format("{0}-R:{2}->N:{1}", Qkey, DataPanel.EntityLookup[Input.MatchCandidate[selectIdx].Item1], DataPanel.RelationLookup[Input.MatchCandidate[selectIdx].Item2]);
                        StatusKey.Add(statusQ);

                        NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);

                        float nonLogTerm = 0;
                        { nonLogTerm = Input.LogPTerm(i, false); }

                        float prob = Input.GetLogProb(i) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                        LogProb.Add(prob);

                        match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
                    }
                }
                MatchPath.SetMatch(match);
                lineIdx += Input.MatchCandidateProb.Segment;
            }
        }

        ///// <summary>
        ///// it is a little difficult.
        ///// </summary>
        //class RewardRunner : ObjectiveRunner
        //{
        //    //Random random = new Random();
        //    new List<GraphQueryData> Input { get; set; }
        //    //EpisodicMemory Memory { get; set; }

        //    Dictionary<int, float> Success = new Dictionary<int, float>();
        //    Dictionary<int, float> StepSuccess = new Dictionary<int, float>();
        //    int Epoch = 0;
        //    float baseline = 0;
        //    int uniqueNum = 0;
        //    int uniqueLeafNum = 0;
        //    //int SmpNum = 0;
        //    public override void Init()
        //    {
        //        Success.Clear();
        //        StepSuccess.Clear();

        //        uniqueNum = 0;
        //        uniqueLeafNum = 0;
        //    }

        //    public void Update(float avgReward)
        //    {
        //        baseline = baseline * BuilderParameters.BETA + avgReward * (1 - BuilderParameters.BETA);
        //    }

        //    public override void Complete()
        //    {
        //        Epoch += 1;
        //        foreach (KeyValuePair<int, float> i in StepSuccess)
        //        {
        //            Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
        //        }
        //        int sc = Success.Where(i => i.Value > 0).Count();
        //        int t = Success.Count;
        //        Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
        //        Logger.WriteLog("Baseline {0}", baseline);
        //        Logger.WriteLog("unique leaf number {0}", uniqueLeafNum * 1.0f / uniqueNum);
        //    }

        //    public RewardRunner(List<GraphQueryData> input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        //    {
        //        Input = input;
        //    }

        //    public override void Forward()
        //    {
        //        float trueReward = 0;
        //        int sampleNum = 0;
        //        float pos_mseloss = 0;
        //        float neg_mseloss = 0;
        //        int pos_smp = 0;
        //        int neg_smp = 0;
        //        ObjectiveDict.Clear();

        //        //List<Tuple<int,float>>[] scores = DataUtil.AggregrateScore(Input, BuilderParameters.PROB_LAMBDA, BuilderParameters.SCORE_LAMBDA, BuilderParameters.AGG_SCORE);

        //        Dictionary<int, HashSet<int>> visitNodes = new Dictionary<int, HashSet<int>>();

        //        // mcts sampling.
        //        for (int m = 0; m < Input.Count; m++)
        //        {
        //            for (int i = 0; i < Input[m].Results.Count; i++)
        //            {
        //                int t = Input[m].Results[i].Item1;
        //                int b = Input[m].Results[i].Item2;
        //                int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
        //                int predId = Input[m].StatusPath[t].NodeID[b];

        //                int targetId = Input[m].RawTarget[origialB];
        //                int rawIdx = Input[m].RawIndex[origialB];

        //                string mkey = string.Format("Term {0}", t);
        //                if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
        //                ObjectiveDict[mkey] += 1;


        //                if (!visitNodes.ContainsKey(rawIdx))
        //                {
        //                    visitNodes.Add(rawIdx, new HashSet<int>());
        //                }

        //                if (!visitNodes[rawIdx].Contains(predId))
        //                {
        //                    visitNodes[rawIdx].Add(predId);
        //                }


        //                float update_v = 0;

        //                float true_v = 0;
        //                if (targetId == predId)
        //                {
        //                    true_v = BuilderParameters.POS_R;
        //                    update_v = true_v;
        //                }
        //                else
        //                {
        //                    true_v = BuilderParameters.NEG_R;
        //                    update_v = true_v;
        //                }

        //                if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
        //                Success[rawIdx] += true_v;

        //                if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
        //                StepSuccess[t] += true_v;

        //                float estimate_v = Util.Logistic(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
                        
        //                if (Input[m].BlackTargets[origialB].Contains(predId) && targetId != predId)
        //                    continue;
        //                {
        //                    trueReward += true_v;
        //                    sampleNum += 1;

        //                    if (true_v > 0.5)
        //                    {
        //                        pos_mseloss += Math.Abs(true_v - estimate_v);
        //                        pos_smp += 1;
        //                    }
        //                    else
        //                    {
        //                        neg_mseloss += Math.Abs(true_v - estimate_v);
        //                        neg_smp += 1;
        //                    }

        //                    // Policy gradient for path finding.
        //                    StatusData st = Input[m].StatusPath[t];

        //                    float adv = update_v - baseline;
                            

        //                    //if (t != BuilderParameters.MAX_HOP)
        //                    st.Term.Deriv.Data.MemPtr[b] = adv * (1 - st.Term.Output.Data.MemPtr[b]);
        //                    // MSE error for reward estimation.
        //                    st.Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (update_v - estimate_v);

        //                    for (int pt = t - 1; pt >= 0; pt--)
        //                    {
        //                        StatusData pst = Input[m].StatusPath[pt];
        //                        int pb = st.GetPreStatusIndex(b);
        //                        int sidx = st.PreSelActIndex[b];

        //                        float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);

        //                        //if (BuilderParameters.CRITIC_TYPE == 0)
        //                        {
        //                            if (pst.Term != null)
        //                            {
        //                                pst.Term.Deriv.Data.MemPtr[pb] = discount * adv * (-pst.Term.Output.Data.MemPtr[pb]);
        //                            }
        //                            pst.MatchCandidateProb.Deriv.MemPtr[sidx] = discount * adv;
        //                        }
                                
        //                        st = pst;
        //                        b = pb;
        //                    }
        //                }
        //            }
        //        }
        //        uniqueNum += visitNodes.Count;
        //        uniqueLeafNum += visitNodes.Select(t => t.Value.Count).Sum();
        //        Update(trueReward * 1.0f / sampleNum);

        //        //ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
        //        ObjectiveDict["TERM-POS-MSE-LOSS"] = pos_mseloss / (pos_smp + 1);
        //        ObjectiveDict["TERM-NEG-MSE-LOSS"] = neg_mseloss / (neg_smp + 1);

        //        ObjectiveDict["TERM-POS-NUM"] = pos_smp;
        //        ObjectiveDict["TERM-NEG-NUM"] = neg_smp;


        //        ObjectiveScore = trueReward / (sampleNum + float.Epsilon);

        //        //if(BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) Memory.Clear();
        //        //average ground truth results.
        //        for (int p = 0; p < Input.Count; p++)
        //        {
        //            for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
        //            {
        //                StatusData st = Input[p].StatusPath[i];
        //                if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
        //                if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
        //                if (st.Score != null) st.Score.Deriv.SyncFromCPU();
        //            }
        //            Input[p].Results.Clear();
        //        }
        //    }
        //}

        class QRewardRunner : ObjectiveRunner
        {
            //Random random = new Random();
            new List<GraphQueryData> Input { get; set; }
            //EpisodicMemory Memory { get; set; }

            Dictionary<int, float> Success = new Dictionary<int, float>();
            Dictionary<int, Tuple<float,float>> StepSuccess = new Dictionary<int, Tuple<float, float>>();
            int Epoch = 0;
            float baseline = 0;
            int uniqueNum = 0;
            int uniqueLeafNum = 0;
            int uniqueTreeNode = 0;
            //int SmpNum = 0;
            public override void Init()
            {
                Success.Clear();
                StepSuccess.Clear();

                uniqueNum = 0;
                uniqueLeafNum = 0;
                uniqueTreeNode = 0;
            }

            public void Update(float avgReward)
            {
                baseline = baseline * BuilderParameters.BETA + avgReward * (1 - BuilderParameters.BETA);
            }

            public override void Complete()
            {
                Epoch += 1;
                foreach (KeyValuePair<int, Tuple<float,float>> i in StepSuccess)
                {
                    Logger.WriteLog("Step {0}, Visit {1} , Success  {2}", i.Key, i.Value.Item1, i.Value.Item2);
                }
                int sc = Success.Where(i => i.Value > 0).Count();
                int t = Success.Count;
                Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
                Logger.WriteLog("Baseline {0}", baseline);
                Logger.WriteLog("unique leaf number {0}", uniqueLeafNum * 1.0f / uniqueNum);
                Logger.WriteLog("unique node number {0}", uniqueTreeNode * 1.0f / uniqueNum);

            }

            public QRewardRunner(List<GraphQueryData> input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
            }

            public override void Forward()
            {
                float trueReward = 0;
                int sampleNum = 0;
                float pos_q = 0;
                float neg_q = 0;
                float black_q = 0;
                int pos_smp = 0;
                int neg_smp = 0;
                int black_smp = 0;
                ObjectiveDict.Clear();

                //List<Tuple<int,float>>[] scores = DataUtil.AggregrateScore(Input, BuilderParameters.PROB_LAMBDA, BuilderParameters.SCORE_LAMBDA, BuilderParameters.AGG_SCORE);

                Dictionary<int, HashSet<int>> visitNodes = new Dictionary<int, HashSet<int>>();
                Dictionary<int, HashSet<int>> visitTrees = new Dictionary<int, HashSet<int>>();

                Dictionary<int, int> stepStat = new Dictionary<int, int>();
                Dictionary<int, float> stepQ = new Dictionary<int, float>();

                Dictionary<int, int> stepBlackStat = new Dictionary<int, int>();
                Dictionary<int, float> TmpSuccess = new Dictionary<int, float>();

                // mcts sampling.
                for (int m = 0; m < Input.Count; m++)
                {
                    for (int i = 0; i < Input[m].Results.Count; i++)
                    {
                        int t = Input[m].Results[i].Item1;
                        int b = Input[m].Results[i].Item2;
                        int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = Input[m].StatusPath[t].NodeID[b];

                        int targetId = Input[m].RawTarget[origialB];
                        int rawIdx = Input[m].RawIndex[origialB];


                        if (!visitNodes.ContainsKey(rawIdx))
                        {
                            visitNodes.Add(rawIdx, new HashSet<int>());
                        }
                        if (!visitNodes[rawIdx].Contains(predId))
                        {
                            visitNodes[rawIdx].Add(predId);
                        }

                        if (!visitTrees.ContainsKey(rawIdx))
                        {
                            visitTrees.Add(rawIdx, new HashSet<int>());
                        }
                        if (!visitTrees[rawIdx].Contains(predId))
                        {
                            visitTrees[rawIdx].Add(predId);
                        }

                        float update_v = 0;

                        //float estimate_v = Util.Logistic(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
                        float tQ = Input[m].StatusPath[t].TermQ.Output.Data.MemPtr[b];

                      
                        float true_v = 0;
                        if (targetId == predId)
                        {
                            true_v = BuilderParameters.POS_R;
                            update_v = 1;

                            // MSE error for reward estimation.
                            //if (BuilderParameters.GAMMA <= 0)
                            //{
                            //    Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (1 - estimate_v);
                            //}
                            pos_q += tQ; // estimate_v;
                            pos_smp += 1;
                        }
                        else
                        {
                            true_v = BuilderParameters.NEG_R;
                            update_v = 0;
                            // MSE error for reward estimation.
                            //if (BuilderParameters.GAMMA <= 0)
                            //{
                            //    Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (-estimate_v);
                            //}
                            neg_q += tQ; // estimate_v;
                            neg_smp += 1;
                        }


                        string mkey = string.Format("Term {0} Visit", t);
                        if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
                        ObjectiveDict[mkey] += 1;

                        string skey = string.Format("Term {0} Success", t);
                        if (!ObjectiveDict.ContainsKey(skey)) { ObjectiveDict[skey] = 0; }
                        ObjectiveDict[skey] += update_v;

                        if (!stepStat.ContainsKey(t))
                        {
                            stepStat[t] = 0;
                        }
                        stepStat[t] += 1;

                        if (!stepQ.ContainsKey(t))
                        {
                            stepQ[t] = 0;
                        }
                        stepQ[t] += tQ;


                        if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
                        Success[rawIdx] += true_v;

                        if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = new Tuple<float, float>(0, 0);
                        StepSuccess[t] = new Tuple<float, float>(StepSuccess[t].Item1 + 1, StepSuccess[t].Item2 + update_v);

                        if (!TmpSuccess.ContainsKey(rawIdx)) TmpSuccess[rawIdx] = 0;
                        TmpSuccess[rawIdx] += true_v;


                        if (Input[m].BlackTargets[origialB].Contains(predId) && targetId != predId)
                        {
                            if (!stepBlackStat.ContainsKey(t)) { stepBlackStat[t] = 0; }
                            stepBlackStat[t] += 1;
                            black_smp += 1;
                            black_q += tQ;
                            if (BuilderParameters.MASK_BLACK > 0)
                            {
                                continue;
                            }
                        }

                        {
                            trueReward += update_v;
                            sampleNum += 1;

                            // Policy gradient for path finding.
                            StatusData st = Input[m].StatusPath[t];

                            //float adv = update_v - baseline;

                            //if (t != BuilderParameters.MAX_HOP)
                            st.TermQ.Deriv.Data.MemPtr[b] += (true_v - st.TermQ.Output.Data.MemPtr[b]);// * 1.0f / Input.Count;
                            
                            if(t == BuilderParameters.MAX_HOP - 1)
                            {
                                st.NonTermQ.Deriv.Data.MemPtr[b] += (true_v - st.NonTermQ.Output.Data.MemPtr[b]); // * 1.0f / Input.Count;
                            }

                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                StatusData pst = Input[m].StatusPath[pt];
                                
                                int pb = st.GetPreStatusIndex(b);
                                int sidx = st.PreSelActIndex[b];

                                float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);

                                //if (BuilderParameters.CRITIC_TYPE == 0)
                                {
                                    float bestTQ = Math.Max(st.TermQ.Output.Data.MemPtr[b], st.NonTermQ.Output.Data.MemPtr[b]);
                                    pst.MatchCandidateQ.Deriv.MemPtr[sidx] +=
                                        (BuilderParameters.REWARD_DISCOUNT * bestTQ - pst.MatchCandidateQ.Output.MemPtr[sidx]); // * 1.0f / Input.Count;// discount * adv;

                                    int e = pst.MatchCandidateQ.SegmentIdx.MemPtr[pb];
                                    int s = pb == 0 ? 0 : pst.MatchCandidateQ.SegmentIdx.MemPtr[pb - 1];

                                    int maxQidx = Util.MaximumValue(pst.MatchCandidateQ.Output.MemPtr, s, e);
                                    pst.NonTermQ.Deriv.Data.MemPtr[pb] +=
                                        (BuilderParameters.REWARD_DISCOUNT * pst.MatchCandidateQ.Output.MemPtr[maxQidx] - pst.NonTermQ.Output.Data.MemPtr[pb]);// * 1.0f / Input.Count;
                                }

                                if (!visitTrees[rawIdx].Contains(pst.NodeID[pb]))
                                {
                                    visitTrees[rawIdx].Add(pst.NodeID[pb]);
                                }

                                st = pst;
                                b = pb;
                            }
                        }
                    }
                }
                uniqueNum += visitNodes.Count;
                uniqueLeafNum += visitNodes.Select(t => t.Value.Count).Sum();
                uniqueTreeNode += visitTrees.Select(t => t.Value.Count).Sum();
                Update(trueReward * 1.0f / sampleNum);

                foreach(int t in stepStat.Keys)
                {
                    ObjectiveDict["STEP-"+t.ToString()+"-AVG-Q"] = stepQ[t] * 1.0f / stepStat[t];
                }

                foreach(int t in stepBlackStat.Keys)
                {
                    ObjectiveDict["STEP-" + t.ToString() + "-BLACK"] = stepBlackStat[t];
                }

                //ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
                ObjectiveDict["POS-TQ"] = pos_q / (pos_smp + 1);
                ObjectiveDict["NEG-TQ"] = neg_q / (neg_smp + 1);
                ObjectiveDict["BLACK-TQ"] = black_q / (black_smp + 1);

                ObjectiveDict["NEG-NUM"] = neg_smp;
                ObjectiveDict["POS-NUM"] = pos_smp;
                ObjectiveDict["BLACK-NUM"] = black_smp;

                ObjectiveDict["UNIQUE-LEAF-NUM"] = visitNodes.Select(t => t.Value.Count).Sum() * 1.0f / visitNodes.Count;
                ObjectiveDict["UNIQUE-TREE-MODE"] = visitTrees.Select(t => t.Value.Count).Sum() * 1.0f / visitTrees.Count;

                ObjectiveScore = trueReward / (sampleNum + float.Epsilon);

                //if(BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) Memory.Clear();
                //average ground truth results.
                for (int p = 0; p < Input.Count; p++)
                {
                    for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
                    {
                        StatusData st = Input[p].StatusPath[i];
                        if (st.TermQ != null) st.TermQ.Deriv.Data.SyncFromCPU();
                        if (st.NonTermQ != null) st.NonTermQ.Deriv.Data.SyncFromCPU();
                        if (st.MatchCandidateQ != null) st.MatchCandidateQ.Deriv.SyncFromCPU();
                        if (st.Score != null) st.Score.Deriv.SyncFromCPU();
                    }
                    Input[p].Results.Clear();
                }
            }
        }

        class QRewardAdvRunner : ObjectiveRunner
        {
            //Random random = new Random();
            new List<GraphQueryData> Input { get; set; }
            //EpisodicMemory Memory { get; set; }

            Dictionary<int, float> Success = new Dictionary<int, float>();
            Dictionary<int, Tuple<float, float>> StepSuccess = new Dictionary<int, Tuple<float, float>>();
            int Epoch = 0;
            float baseline = 0;
            int uniqueNum = 0;
            int uniqueLeafNum = 0;
            int uniqueTreeNode = 0;
            //int SmpNum = 0;
            public override void Init()
            {
                Success.Clear();
                StepSuccess.Clear();

                uniqueNum = 0;
                uniqueLeafNum = 0;
                uniqueTreeNode = 0;
            }

            public void Update(float avgReward)
            {
                baseline = baseline * BuilderParameters.BETA + avgReward * (1 - BuilderParameters.BETA);
            }

            public override void Complete()
            {
                Epoch += 1;
                foreach (KeyValuePair<int, Tuple<float, float>> i in StepSuccess)
                {
                    Logger.WriteLog("Step {0}, Visit {1} , Success  {2}", i.Key, i.Value.Item1, i.Value.Item2);
                }
                int sc = Success.Where(i => i.Value > 0).Count();
                int t = Success.Count;
                Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
                Logger.WriteLog("Baseline {0}", baseline);
                Logger.WriteLog("unique leaf number {0}", uniqueLeafNum * 1.0f / uniqueNum);
                Logger.WriteLog("unique node number {0}", uniqueTreeNode * 1.0f / uniqueNum);

            }

            public QRewardAdvRunner(List<GraphQueryData> input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
            }

            public override void Forward()
            {
                float trueReward = 0;
                int sampleNum = 0;
                float pos_mseloss = 0;
                float neg_mseloss = 0;
                int pos_smp = 0;
                int neg_smp = 0;
                ObjectiveDict.Clear();

                //List<Tuple<int,float>>[] scores = DataUtil.AggregrateScore(Input, BuilderParameters.PROB_LAMBDA, BuilderParameters.SCORE_LAMBDA, BuilderParameters.AGG_SCORE);

                /// sum score values for the same leaf nodes.
                List<Tuple<int, float>>[] scores = DataUtil.AggregrateScore(Input, 0, 1, 4);

                int[] pos_index = new int[Input[0].BatchSize];
                Dictionary<int, float>[] deriv = new Dictionary<int, float>[Input[0].BatchSize];

                float pos_v = 0;
                int pos_i = 0;

                float neg_v = 0;
                int neg_i = 0;

                //int maxHit1 = 0;
                for (int originB = 0; originB < Input[0].BatchSize; originB++)
                {
                    int rawSource = Input[0].RawSource[originB];
                    int rawTarget = Input[0].RawTarget[originB];

                    deriv[originB] = new Dictionary<int, float>();

                    for (int k = 0; k < scores[originB].Count; k++)
                    {
                        int mkey = scores[originB][k].Item1;
                        float mvalue = scores[originB][k].Item2;
                        if (mkey == rawTarget)
                        {
                            deriv[originB].Add(mkey, 1 - Util.Logistic(mvalue));
                            pos_v += Util.Logistic(mvalue);
                            pos_i += 1;
                        }
                        else
                        {
                            deriv[originB].Add(mkey, -Util.Logistic(mvalue));
                            neg_v += Util.Logistic(mvalue);
                            neg_i += 1;
                        }
                    }
                }

                ObjectiveDict.Add("Positive-Nodes", pos_i);
                ObjectiveDict.Add("Negative-Nodes", neg_i);

                ObjectiveDict.Add("Positive-V", pos_v * 1.0f / pos_i);
                ObjectiveDict.Add("Negative-V", neg_v * 1.0f / neg_i);

                Dictionary<int, HashSet<int>> visitNodes = new Dictionary<int, HashSet<int>>();
                Dictionary<int, HashSet<int>> visitTrees = new Dictionary<int, HashSet<int>>();

                // mcts sampling.
                for (int m = 0; m < Input.Count; m++)
                {
                    for (int i = 0; i < Input[m].Results.Count; i++)
                    {
                        int t = Input[m].Results[i].Item1;
                        int b = Input[m].Results[i].Item2;
                        int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = Input[m].StatusPath[t].NodeID[b];

                        int targetId = Input[m].RawTarget[origialB];
                        int rawIdx = Input[m].RawIndex[origialB];

                        string mkey = string.Format("Term {0}", t);
                        if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
                        ObjectiveDict[mkey] += 1;


                        if (!visitNodes.ContainsKey(rawIdx))
                        {
                            visitNodes.Add(rawIdx, new HashSet<int>());
                        }
                        if (!visitNodes[rawIdx].Contains(predId))
                        {
                            visitNodes[rawIdx].Add(predId);
                        }

                        if (!visitTrees.ContainsKey(rawIdx))
                        {
                            visitTrees.Add(rawIdx, new HashSet<int>());
                        }
                        if (!visitTrees[rawIdx].Contains(predId))
                        {
                            visitTrees[rawIdx].Add(predId);
                        }

                        float update_v = 0;

                        //float estimate_v = Util.Logistic(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
                        float tQ = Input[m].StatusPath[t].TermQ.Output.Data.MemPtr[b];

                        if (BuilderParameters.MASK_BLACK > 0 && Input[m].BlackTargets[origialB].Contains(predId) && targetId != predId)
                            continue;

                        float true_v = 0;
                        if (targetId == predId)
                        {
                            true_v = BuilderParameters.POS_R;
                            update_v = 1;

                            // MSE error for reward estimation.
                            //if (BuilderParameters.GAMMA <= 0)
                            //{
                            //    Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (1 - estimate_v);
                            //}
                            pos_mseloss += tQ; // estimate_v;
                            pos_smp += 1;
                        }
                        else
                        {
                            true_v = BuilderParameters.NEG_R;
                            update_v = 0;
                            // MSE error for reward estimation.
                            //if (BuilderParameters.GAMMA <= 0)
                            //{
                            //    Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (-estimate_v);
                            //}
                            neg_mseloss += tQ; // estimate_v;
                            neg_smp += 1;
                        }

                        //if(BuilderParameters.GAMMA > 0)
                        //{
                        
                        //}

                        if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
                        Success[rawIdx] += true_v;

                        if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = new Tuple<float, float>(0, 0);
                        StepSuccess[t] = new Tuple<float, float>(StepSuccess[t].Item1 + 1, StepSuccess[t].Item2 + update_v);

                        {
                            trueReward += true_v;
                            sampleNum += 1;

                            if (deriv[origialB].ContainsKey(predId))
                            {
                                Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] += BuilderParameters.MSE_LAMBDA * deriv[origialB][predId];
                            }

                            // Q-Learning for path finding.
                            StatusData st = Input[m].StatusPath[t];

                            //if (t != BuilderParameters.MAX_HOP)
                            st.TermQ.Deriv.Data.MemPtr[b] += (true_v - st.TermQ.Output.Data.MemPtr[b]);// * 1.0f / Input.Count;

                            if (t == BuilderParameters.MAX_HOP - 1)
                            {
                                st.NonTermQ.Deriv.Data.MemPtr[b] += (true_v - st.NonTermQ.Output.Data.MemPtr[b]); // * 1.0f / Input.Count;
                            }




                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                StatusData pst = Input[m].StatusPath[pt];

                                int pb = st.GetPreStatusIndex(b);
                                int sidx = st.PreSelActIndex[b];

                                float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);

                                //if (BuilderParameters.CRITIC_TYPE == 0)
                                {
                                    float bestTQ = Math.Max(st.TermQ.Output.Data.MemPtr[b], st.NonTermQ.Output.Data.MemPtr[b]);
                                    pst.MatchCandidateQ.Deriv.MemPtr[sidx] +=
                                        (BuilderParameters.REWARD_DISCOUNT * bestTQ - pst.MatchCandidateQ.Output.MemPtr[sidx]); // * 1.0f / Input.Count;// discount * adv;

                                    int e = pst.MatchCandidateQ.SegmentIdx.MemPtr[pb];
                                    int s = pb == 0 ? 0 : pst.MatchCandidateQ.SegmentIdx.MemPtr[pb - 1];

                                    int maxQidx = Util.MaximumValue(pst.MatchCandidateQ.Output.MemPtr, s, e);
                                    pst.NonTermQ.Deriv.Data.MemPtr[pb] +=
                                        (BuilderParameters.REWARD_DISCOUNT * pst.MatchCandidateQ.Output.MemPtr[maxQidx] - pst.NonTermQ.Output.Data.MemPtr[pb]);// * 1.0f / Input.Count;
                                }

                                if (!visitTrees[rawIdx].Contains(pst.NodeID[pb]))
                                {
                                    visitTrees[rawIdx].Add(pst.NodeID[pb]);
                                }

                                st = pst;
                                b = pb;
                            }
                        }
                    }
                }
                uniqueNum += visitNodes.Count;
                uniqueLeafNum += visitNodes.Select(t => t.Value.Count).Sum();
                uniqueTreeNode += visitTrees.Select(t => t.Value.Count).Sum();
                Update(trueReward * 1.0f / sampleNum);

                //ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
                ObjectiveDict["TERM-POS-MSE-LOSS"] = pos_mseloss / (pos_smp + 1);
                ObjectiveDict["TERM-NEG-MSE-LOSS"] = neg_mseloss / (neg_smp + 1);

                ObjectiveDict["TERM-POS-NUM"] = pos_smp;
                ObjectiveDict["TERM-NEG-NUM"] = neg_smp;


                ObjectiveScore = trueReward / (sampleNum + float.Epsilon);

                //if(BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) Memory.Clear();
                //average ground truth results.
                for (int p = 0; p < Input.Count; p++)
                {
                    for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
                    {
                        StatusData st = Input[p].StatusPath[i];
                        if (st.TermQ != null) st.TermQ.Deriv.Data.SyncFromCPU();
                        if (st.NonTermQ != null) st.NonTermQ.Deriv.Data.SyncFromCPU();
                        if (st.MatchCandidateQ != null) st.MatchCandidateQ.Deriv.SyncFromCPU();
                        if (st.Score != null) st.Score.Deriv.SyncFromCPU();
                    }
                    Input[p].Results.Clear();
                }
            }
        }

        //class SeqRewardRunner : ObjectiveRunner
        //{
        //    new List<GraphQueryData> Input { get; set; }

        //    Dictionary<int, float> Success = new Dictionary<int, float>();
        //    Dictionary<int, float> StepSuccess = new Dictionary<int, float>();
        //    int Epoch = 0;
        //    float baseline = 0;

        //    int uniqueNum = 0;
        //    int uniqueLeafNum = 0;
        //    //int SmpNum = 0;
        //    public override void Init()
        //    {
        //        Success.Clear();
        //        StepSuccess.Clear();
        //        uniqueNum = 0;
        //        uniqueLeafNum = 0;
        //    }

        //    public void Update(float avgReward)
        //    {
        //        baseline = baseline * BuilderParameters.BETA + avgReward * (1 - BuilderParameters.BETA);
        //    }

        //    public override void Complete()
        //    {
        //        Epoch += 1;
        //        foreach (KeyValuePair<int, float> i in StepSuccess)
        //        {
        //            Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
        //        }
        //        int sc = Success.Where(i => i.Value > 0).Count();
        //        int t = Success.Count;
        //        Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
        //        Logger.WriteLog("Baseline {0}", baseline);

        //        Logger.WriteLog("unique leaf node", uniqueLeafNum * 1.0f / uniqueNum);
        //    }

        //    public SeqRewardRunner(List<GraphQueryData> input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        //    {
        //        Input = input;
        //    }

        //    public override void Forward()
        //    {
        //        float trueReward = 0;
        //        int sampleNum = 0;
        //        float pos_mseloss = 0;
        //        float neg_mseloss = 0;
        //        int pos_smp = 0;
        //        int neg_smp = 0;
        //        ObjectiveDict.Clear();

        //        List<Tuple<float, float, float>>[] LogProbs = new List<Tuple<float, float, float>>[Input[0].BatchSize];
        //        float[,] PropRewards = new float[Input[0].BatchSize, Input.Count];
        //        float[,] SelfRewards = new float[Input[0].BatchSize, Input.Count];


        //        string[,] Keys = new string[Input[0].BatchSize, Input.Count];
        //        int[] count = new int[Input[0].BatchSize];
        //        List<int>[] mctss = new List<int>[Input[0].BatchSize];
        //        for (int i = 0; i < Input[0].BatchSize; i++)
        //        {
        //            LogProbs[i] = new List<Tuple<float, float, float>>();
        //            mctss[i] = new List<int>();
        //        }

        //        Dictionary<int, HashSet<int>> visitNodes = new Dictionary<int, HashSet<int>>();

        //        // mcts sampling.
        //        for (int m = 0; m < Input.Count; m++)
        //        {
        //            //if(Input[m].Results.Count != Input[m].BatchSize)
        //            //{
        //            //    Console.WriteLine("BUG2");
        //            //}
        //            //if(m == 2) { Console.WriteLine("debug"); }
        //            List<int> mmindx = new List<int>();
        //            for (int i = 0; i < Input[m].Results.Count; i++)
        //            {
        //                int t = Input[m].Results[i].Item1;
        //                int b = Input[m].Results[i].Item2;
        //                int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
        //                int predId = Input[m].StatusPath[t].NodeID[b];

        //                int targetId = Input[m].RawTarget[origialB];
        //                int rawIdx = Input[m].RawIndex[origialB];

        //                float log_v = Util.LogLogistial(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
        //                float log_pv = Util.LogNLogistial(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);

        //                if(!visitNodes.ContainsKey(rawIdx))
        //                {
        //                    visitNodes.Add(rawIdx, new HashSet<int>());
        //                }

        //                if(!visitNodes[rawIdx].Contains(predId))
        //                {
        //                    visitNodes[rawIdx].Add(predId);
        //                }

        //                Keys[origialB, m] = Input[m].StatusPath[t].GetStatusKey(b);

        //                //it will terminate with probability of estimate_v;
        //                if (Input[m].BlackTargets[origialB].Contains(predId) && targetId != predId)
        //                {
        //                    LogProbs[origialB].Add(new Tuple<float, float, float>(0, 0, 0));
        //                    continue;
        //                }

        //                float true_v = 0;
        //                if (targetId == predId)
        //                {
        //                    true_v = BuilderParameters.POS_R;
        //                }
        //                else
        //                {
        //                    true_v = BuilderParameters.NEG_R;
        //                }

        //                float pLog = LogProbs[origialB].Count == 0 ? 0 : LogProbs[origialB].Last().Item1;
        //                LogProbs[origialB].Add(new Tuple<float, float, float>(pLog + log_pv, pLog + log_v, true_v));

        //                //if(Keys[origialB, m] == null)
        //                //{
        //                //    Console.WriteLine("BUG");
        //                //}
        //                count[origialB] += 1;
        //                mctss[origialB].Add(m);
        //                mmindx.Add(origialB);
        //            }
        //        }

        //        uniqueNum += visitNodes.Count;
        //        uniqueLeafNum += visitNodes.Select(t => t.Value.Count).Sum();

        //        // rewards propagation.
        //        for (int i = 0; i < LogProbs.Length; i++)
        //        {
        //            for (int m = LogProbs[i].Count - 1; m >= 0; m--)
        //            {
        //                if (m == LogProbs[i].Count - 1)
        //                {
        //                    SelfRewards[i, m] = (float)Math.Exp(Util.LogAdd(LogProbs[i][m].Item1, LogProbs[i][m].Item2)) * LogProbs[i][m].Item3;
        //                }
        //                else
        //                {
        //                    SelfRewards[i, m] = (float)Math.Exp(LogProbs[i][m].Item2) * LogProbs[i][m].Item3;
        //                    PropRewards[i, m] = BuilderParameters.REWARD_MCTS_DISCOUNT * (SelfRewards[i, m + 1] + PropRewards[i, m + 1]);
        //                }
        //            }
        //        }


        //        // mcts sampling.
        //        for (int m = 0; m < Input.Count; m++)
        //        {
        //            for (int i = 0; i < Input[m].Results.Count; i++)
        //            {
        //                int t = Input[m].Results[i].Item1;
        //                int b = Input[m].Results[i].Item2;
        //                int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
        //                int predId = Input[m].StatusPath[t].NodeID[b];

        //                int targetId = Input[m].RawTarget[origialB];
        //                int rawIdx = Input[m].RawIndex[origialB];

        //                string mkey = string.Format("Term {0}", t);
        //                if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
        //                ObjectiveDict[mkey] += 1;

        //                float update_v = 0;

        //                float true_v = 0;
        //                if (targetId == predId)
        //                {
        //                    true_v = BuilderParameters.POS_R;
        //                    update_v = true_v;
        //                }
        //                else
        //                {
        //                    true_v = BuilderParameters.NEG_R;
        //                    update_v = true_v;
        //                }

        //                if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
        //                Success[rawIdx] += true_v;

        //                if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
        //                StepSuccess[t] += true_v;

        //                float estimate_v = Util.Logistic(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);

        //                //it will terminate with probability of estimate_v;


        //                //if (Input[m].BlackTargets[origialB].Contains(predId) && targetId != predId)
        //                //    continue;
        //                {
        //                    trueReward += true_v;
        //                    sampleNum += 1;

        //                    if (true_v > 0.5)
        //                    {
        //                        pos_mseloss += Math.Abs(true_v - estimate_v);
        //                        pos_smp += 1;
        //                    }
        //                    else
        //                    {
        //                        neg_mseloss += Math.Abs(true_v - estimate_v);
        //                        neg_smp += 1;
        //                    }

        //                    // Policy gradient for path finding.
        //                    StatusData st = Input[m].StatusPath[t];

        //                    float adv = update_v - baseline;

        //                    // MSE error for reward estimation.
        //                    st.Score.Deriv.Data.MemPtr[b] = (SelfRewards[origialB, m]) * (1 - estimate_v) + (PropRewards[origialB, m]) * (-estimate_v); // // BuilderParameters.MSE_LAMBDA * (update_v - estimate_v);

        //                    adv = (SelfRewards[origialB, m] + PropRewards[origialB, m]);
        //                    //if (t != BuilderParameters.MAX_HOP)
        //                    st.Term.Deriv.Data.MemPtr[b] = adv * (1 - st.Term.Output.Data.MemPtr[b]);

        //                    for (int pt = t - 1; pt >= 0; pt--)
        //                    {
        //                        StatusData pst = Input[m].StatusPath[pt];
        //                        int pb = st.GetPreStatusIndex(b);
        //                        int sidx = st.PreSelActIndex[b];

        //                        float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);

        //                        //if (BuilderParameters.CRITIC_TYPE == 0)
        //                        {
        //                            if (pst.Term != null)
        //                            {
        //                                pst.Term.Deriv.Data.MemPtr[pb] = discount * adv * (-pst.Term.Output.Data.MemPtr[pb]);
        //                            }
        //                            pst.MatchCandidateProb.Deriv.MemPtr[sidx] = discount * adv;
        //                        }

        //                        st = pst;
        //                        b = pb;
        //                    }
        //                }
        //            }
        //        }

        //        Update(trueReward * 1.0f / sampleNum);

        //        //ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
        //        ObjectiveDict["TERM-POS-MSE-LOSS"] = pos_mseloss / (pos_smp + 1);
        //        ObjectiveDict["TERM-NEG-MSE-LOSS"] = neg_mseloss / (neg_smp + 1);

        //        ObjectiveDict["TERM-POS-NUM"] = pos_smp;
        //        ObjectiveDict["TERM-NEG-NUM"] = neg_smp;


        //        ObjectiveScore = trueReward / (sampleNum + float.Epsilon);

        //        //if(BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) Memory.Clear();
        //        //average ground truth results.
        //        for (int p = 0; p < Input.Count; p++)
        //        {
        //            for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
        //            {
        //                StatusData st = Input[p].StatusPath[i];
        //                if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
        //                if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
        //                if (st.Score != null) st.Score.Deriv.SyncFromCPU();
        //            }
        //            Input[p].Results.Clear();
        //        }
        //    }
        //}


        ///// <summary>
        ///// it is a little difficult.
        ///// </summary>
        //class RewardV2Runner : ObjectiveRunner
        //{
        //    //Random random = new Random();
        //    new List<GraphQueryData> Input { get; set; }
        //    //EpisodicMemory Memory { get; set; }

        //    Dictionary<int, float> Success = new Dictionary<int, float>();
        //    Dictionary<int, float> StepSuccess = new Dictionary<int, float>();
        //    int Epoch = 0;
        //    float baseline = 0;
        //    public override void Init()
        //    {
        //        Success.Clear();
        //        StepSuccess.Clear();
        //    }

        //    public void Update(float avgReward)
        //    {
        //        baseline = baseline * BuilderParameters.BETA + avgReward * (1 - BuilderParameters.BETA);
        //    }

        //    public override void Complete()
        //    {
        //        Epoch += 1;
        //        foreach (KeyValuePair<int, float> i in StepSuccess)
        //        {
        //            Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
        //        }
        //        int sc = Success.Where(i => i.Value > 0).Count();
        //        int t = Success.Count;
        //        Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
        //        Logger.WriteLog("Baseline {0}", baseline);
        //    }

        //    public RewardV2Runner(List<GraphQueryData> input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        //    {
        //        Input = input;
        //    }

        //    public override void Forward()
        //    {
        //        float trueReward = 0;
        //        int sampleNum = 0;
        //        //float pos_mseloss = 0;
        //        //float neg_mseloss = 0;
        //        //int pos_smp = 0;
        //        //int neg_smp = 0;
        //        ObjectiveDict.Clear();

        //        List<Tuple<int, float>>[] scores = DataUtil.AggregrateScore(Input, BuilderParameters.PROB_LAMBDA, BuilderParameters.SCORE_LAMBDA, 2);
        //        //float[] score_sum = new float[Input[0].BatchSize];


        //        List<float[]> probs = new List<float[]>();
        //        List<float[]> svalues = new List<float[]>();
        //        List<float> svalues_sum = new List<float>();
        //        List<float> rewards = new List<float>();
        //        for (int originB = 0; originB < Input[0].BatchSize; originB++)
        //        {
        //            int rawSource = Input[0].RawSource[originB];
        //            int rawTarget = Input[0].RawTarget[originB];

        //            //scores[originB].Sort((x, y) => y.Item2.CompareTo(x.Item2));
        //            float[] svalue = scores[originB].Select(k => k.Item2).ToArray();
        //            float svalue_sum = Util.LogSum(svalue, svalue.Length);
        //            //score_sum[originB] = svalue_sum;

        //            float[] prob = Util.Softmax(svalue, 1);

        //            probs.Add(prob);
        //            svalues.Add(svalue);
        //            svalues_sum.Add(svalue_sum);
        //            float r = 0;
        //            for (int k = 0; k < scores[originB].Count; k++)
        //            {
        //                if (scores[originB][k].Item1 == rawTarget)
        //                {
        //                    r = prob[k];
        //                    break;
        //                }
        //            }
        //            rewards.Add(r);
        //        }

        //        // mcts sampling.
        //        for (int m = 0; m < Input.Count; m++)
        //        {
        //            for (int i = 0; i < Input[m].Results.Count; i++)
        //            {
        //                int t = Input[m].Results[i].Item1;
        //                int b = Input[m].Results[i].Item2;
        //                int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
        //                int predId = Input[m].StatusPath[t].NodeID[b];

        //                int targetId = Input[m].RawTarget[origialB];
        //                int rawIdx = Input[m].RawIndex[origialB];

        //                float estimate_v = Input[m].StatusPath[t].Score.Output.Data.MemPtr[b];

        //                for (int k = 0; k < scores[origialB].Count; k++)
        //                {
        //                    if (scores[origialB][k].Item1 == predId && predId == targetId)
        //                    {
        //                        Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (float)(Math.Exp(estimate_v - scores[origialB][k].Item1)  - Math.Exp(estimate_v - svalues_sum[origialB]));
        //                    }
        //                    else if (scores[origialB][k].Item1 == predId)
        //                    {
        //                        Input[m].StatusPath[t].Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (float)(-Math.Exp(estimate_v - svalues_sum[origialB]));
        //                    }
        //                }
        //            }
        //        }


        //        for (int m = 0; m < Input.Count; m++)
        //        {
        //            for (int i = 0; i < Input[m].Results.Count; i++)
        //            {
        //                int t = Input[m].Results[i].Item1;
        //                int b = Input[m].Results[i].Item2;
        //                int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
        //                int predId = Input[m].StatusPath[t].NodeID[b];

        //                int targetId = Input[m].RawTarget[origialB];
        //                int rawIdx = Input[m].RawIndex[origialB];

        //                float estimate_v = Input[m].StatusPath[t].Score.Output.Data.MemPtr[b];


        //                string mkey = string.Format("Term {0}", t);
        //                if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
        //                ObjectiveDict[mkey] += 1;

        //                float update_v = 0;

        //                float true_v = 0;
        //                if (targetId == predId)
        //                {
        //                    true_v = BuilderParameters.POS_R;
        //                    update_v = true_v;
        //                }
        //                else
        //                {
        //                    true_v = BuilderParameters.NEG_R;
        //                    update_v = true_v;
        //                }

        //                if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
        //                Success[rawIdx] += true_v;

        //                if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
        //                StepSuccess[t] += true_v;


        //                //if (Input[m].BlackTargets[origialB].Contains(predId) && targetId != predId)
        //                //    continue;
        //                {
        //                    trueReward += true_v;
        //                    sampleNum += 1;

        //                    // Policy gradient for path finding.
        //                    StatusData st = Input[m].StatusPath[t];

        //                    float adv = rewards[origialB] - baseline;
        //                    //if (BuilderParameters.CRITIC_TYPE == 1)
        //                    //{
        //                    //    adv = update_v - estimate_v;
        //                    //}

        //                    //if (t != BuilderParameters.MAX_HOP)
        //                    st.Term.Deriv.Data.MemPtr[b] = adv * (1 - st.Term.Output.Data.MemPtr[b]);

        //                    for (int pt = t - 1; pt >= 0; pt--)
        //                    {
        //                        StatusData pst = Input[m].StatusPath[pt];
        //                        int pb = st.GetPreStatusIndex(b);
        //                        int sidx = st.PreSelActIndex[b];

        //                        float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);

        //                        //if (BuilderParameters.CRITIC_TYPE == 0)
        //                        {
        //                            if (pst.Term != null)
        //                            {
        //                                pst.Term.Deriv.Data.MemPtr[pb] = discount * update_v * (-pst.Term.Output.Data.MemPtr[pb]);
        //                            }
        //                            pst.MatchCandidateProb.Deriv.MemPtr[sidx] = discount * update_v;
        //                        }
        //                        //else if (BuilderParameters.CRITIC_TYPE == 1)
        //                        //{
        //                        //    float p_estimate_v = Util.Logistic(pst.Score.Output.Data.MemPtr[pb]);
        //                        //    if (pst.Term != null)
        //                        //    {
        //                        //        pst.Term.Deriv.Data.MemPtr[pb] = (discount * update_v - p_estimate_v) * (-pst.Term.Output.Data.MemPtr[pb]);
        //                        //    }
        //                        //    pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (discount * update_v - p_estimate_v);
        //                        //    pst.Score.Deriv.Data.MemPtr[pb] = BuilderParameters.MSE_LAMBDA * (discount * update_v - p_estimate_v);
        //                        //}
        //                        st = pst;
        //                        b = pb;
        //                    }
        //                }
        //            }
        //        }


        //        Update(rewards.Average());

        //        //ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
        //        //ObjectiveDict["TERM-POS-MSE-LOSS"] = pos_mseloss / (pos_smp + 1);
        //        //ObjectiveDict["TERM-NEG-MSE-LOSS"] = neg_mseloss / (neg_smp + 1);

        //       // ObjectiveDict["TERM-POS-NUM"] = pos_smp;
        //       // ObjectiveDict["TERM-NEG-NUM"] = neg_smp;


        //        ObjectiveScore = rewards.Average();

        //        //if(BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) Memory.Clear();
        //        //average ground truth results.
        //        for (int p = 0; p < Input.Count; p++)
        //        {
        //            for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
        //            {
        //                StatusData st = Input[p].StatusPath[i];
        //                if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
        //                if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
        //                if (st.Score != null) st.Score.Deriv.SyncFromCPU();
        //            }
        //            Input[p].Results.Clear();
        //        }

        //    }
        //}

        // update reward feedback 
        class RewardFeedbackRunner : StructRunner
        {
            new GraphQueryData Input { get; set; }
            EpisodicMemory Memory { get; set; }

            float PosMean = 0;
            float NegMean = 0;
            int PosNum = 0;
            int NegNum = 0;

            int Epoch = 0;
            public override void Init()
            {
                Epoch = 0;
                PosMean = 0;
                PosNum = 0;

                NegMean = 0;
                NegNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                //Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosMean / (PosNum + 0.000001f));
                //Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegMean / (NegNum + 0.000001f));

            }

            public RewardFeedbackRunner(GraphQueryData input, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                // calculate termination probability.
                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    //if (Input.StatusPath[t].Term != null)
                    {
                        //ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].BatchSize, 1);
                        //ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.StatusPath[t].BatchSize, 1 - Util.LargeEpsilon, Util.LargeEpsilon);
                        Input.StatusPath[t].TermQ.Output.Data.SyncToCPU();
                        Input.StatusPath[t].NonTermQ.Output.Data.SyncToCPU();
                        Array.Clear(Input.StatusPath[t].TermQ.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
                        Array.Clear(Input.StatusPath[t].NonTermQ.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);

                    }
                    //if (Input.StatusPath[t].MatchCandidateProb != null)
                    {
                        Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
                        Array.Clear(Input.StatusPath[t].MatchCandidateQ.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateQ.Length);
                    }
                    //if (Input.StatusPath[t].Score != null)
                    {
                        Input.StatusPath[t].Score.Output.Data.SyncToCPU();
                        Array.Clear(Input.StatusPath[t].Score.Deriv.Data.MemPtr, 0, Input.StatusPath[t].Score.Output.Data.EffectiveSize);
                    }
                }

                // add last step states.
                for (int i = 0; i < Input.Results.Count; i++)
                {
                    int t = Input.Results[i].Item1;
                    int b = Input.Results[i].Item2;
                    int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);
                    int predId = Input.StatusPath[t].NodeID[b];

                    int targetId = Input.RawTarget[origialB];

                    float feedback_v = 0;

                    float estimate_v = Input.StatusPath[t].TermQ.Output.Data.MemPtr[b]; // Util.Logistic(Input.StatusPath[t].Score.Output.Data.MemPtr[b]);// Math.Max(0, );
                                                                                                      //if (estimate_v > 1) estimate_v = 1;
                    float true_v = BuilderParameters.NEG_R;
                    if (Input.BlackTargets[origialB].Contains(predId) && predId != targetId)
                    {
                        true_v = BuilderParameters.BLACK_R;
                        estimate_v = BuilderParameters.BLACK_R;
                    }
                    else if (targetId == predId)
                    {
                        true_v = BuilderParameters.POS_R;
                        PosMean += estimate_v;
                        PosNum += 1;
                    }
                    else
                    {
                        true_v = BuilderParameters.NEG_R;
                        NegMean += estimate_v;
                        NegNum += 1;
                    }

                    //if (Behavior.RunMode == DNNRunMode.Train || SampleRandom.NextDouble() < BuilderParameters.Epsilon(Epoch)) { feedback_v = true_v; }
                    //else { feedback_v = estimate_v; }
                    feedback_v = estimate_v * BuilderParameters.UPDATE_R_DISCOUNT;

                    //feedback_v = feedback_v * BuilderParameters.UPDATE_R_DISCOUNT;
                    float v = 1; //* BuilderParameters.UPDATE_R_DISCOUNT;
                    
                    StatusData st = Input.StatusPath[t];
                    string key = st.GetStatusKey(b);
                    //if (t != BuilderParameters.MAX_HOP)
                    //{
                    //float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t);
                    Memory.Update(key, st.GetActionDim(b), st.GetActionDim(b) + 1, feedback_v, v);
                    //}

                    for (int pt = t - 1; pt >= 0; pt--)
                    {
                        StatusData pst = Input.StatusPath[pt];
                        int pb = st.GetPreStatusIndex(b);
                        string pkey = pst.GetStatusKey(pb);

                        int sidx = st.PreSelActIndex[b] - pst.GetActionStartIndex(pb);
                        float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);
                        Memory.Update(pkey, sidx, pst.GetActionDim(pb) + 1, feedback_v * discount, v * discount);

                        st = pst;
                        b = pb;
                    }
                }
                Memory.UpdateTiming();
            }
        }

        /// <summary>
        /// REINFORCE-WALK prediction Runner.
        /// </summary>
        class MAPPredictionRunner : StructRunner
        {
            //float MAP = 0;

            //int SmpIdx = 0;
            List<GraphQueryData> Query { get; set; }
            int Iteration = 0;
            //Dictionary<int, float> Success = new Dictionary<int, float>();
            //Dictionary<int, float> StepSuccess = new Dictionary<int, float>();
            static string TAG = BuilderParameters.LogFile;
            string TmpInFile = "/cygdrive/d/users/yeshen.REDMOND/TMP/" + TAG + "-TMP.in";
            string TmpInFileWin = @"D:\users\yeshen.REDMOND\TMP\" + TAG + "-TMP.in";


            string TmpOutFile = "/cygdrive/d/users/yeshen.REDMOND/TMP/" + TAG + "-TMP.out";
            string TmpOutFileWin = @"D:\users\yeshen.REDMOND\TMP\" + TAG + "-TMP.out";

            string TmpEvalFileWin = @"c:\cygwin64\bin\" + TAG + "-eval.sh";

            string TmpSortFile = "/cygdrive/d/users/yeshen.REDMOND/TMP/" + TAG + "-TMP.sort";
            string TmpSortFileWin = @"D:\users\yeshen.REDMOND\TMP\" + TAG + "-TMP.sort";

            int TotalResultNode = 0;
            int TotalBatchNum = 0;
            StreamWriter InWriter = null;
            public MAPPredictionRunner(List<GraphQueryData> query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Iteration = 0;

                // string InexactPath = Path.GetFullPath(TmpInFile);
                // string SortexactPath = Path.GetFullPath(TmpSortFile);
                // string OutInexactPath = Path.GetFullPath(TmpOutFile);

                using (StreamWriter writer = new StreamWriter(TmpEvalFileWin))
                {
                    writer.WriteLine("cd \"$(dirname \"$0\")\"  ");
                    writer.WriteLine("python nell_eval.py " + TmpInFile + " " + TmpSortFile + " " + TmpOutFile + " ");

                    //writer.WriteLine("cd /cygdrive/c/cygwin64/bin/ ");
                    //writer.WriteLine("/cygdrive/c/cygwin64/bin/python /cygdrive/c/cygwin64/bin/nell_eval.py " + TmpInFile + " " + TmpSortFile + " " + TmpOutFile + " ");
                }

            }

            public override void Init()
            {
                InWriter = new StreamWriter(TmpInFileWin);
                File.Copy(BuilderParameters.InputDir + "sort_test.pairs", TmpSortFileWin, true);
                if (File.Exists(TmpOutFileWin)) File.Delete(TmpOutFileWin);

                TotalResultNode = 0;
                TotalBatchNum = 0;
            }


            public override void Complete()
            {
                InWriter.Close();
                //File.Copy(BuilderParameters.InputDir + "sort_test.pairs", @"c:\cygwin64\bin\sort_test.pairs", true);
                using (Process callProcess = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = @"c:\cygwin64\bin\mintty.exe",
                        Arguments = "/bin/bash -l -e \"/cygdrive/c/cygwin64/bin/" + TAG + "-eval.sh\"",
                        //\"{0}\" \"{1}\" \"{2}\" \"{3}\"", BuilderParameters.EVALUATION_TOOL, TmpInFile, BuilderParameters.InputDir+ "sort_test.pairs", TmpOutFile),
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        WindowStyle = ProcessWindowStyle.Hidden,
                    }
                })
                {
                    callProcess.Start();
                    callProcess.WaitForExit();
                }
                //Thread.Sleep(1000);
                while (!File.Exists(TmpOutFileWin))
                { }

                {
                    string line = File.ReadAllText(TmpOutFileWin);
                    Logger.WriteLog("MAP {0}", line);
                    Logger.WriteLog("Avg Visit Leaf Node {0}", TotalResultNode * 1.0f / TotalBatchNum);
                    if (File.Exists(TmpInFileWin)) File.Delete(TmpInFileWin);
                    if (File.Exists(TmpOutFileWin)) File.Delete(TmpOutFileWin);
                }
                //else
                //{
                //    Logger.WriteLog("MAP {0}", 0);
                //}
                Iteration += 1;
            }
            public override void Forward()
            {
                List<Tuple<int, float>>[] scoreDict = DataUtil.AggregrateScore(Query, BuilderParameters.PROB_LAMBDA, BuilderParameters.SCORE_LAMBDA, BuilderParameters.AGG_SCORE);

                for (int originB = 0; originB < Query[0].BatchSize; originB++)
                {
                    int rawSource = Query[0].RawSource[originB];
                    foreach (Tuple<int, float> item in scoreDict[originB])
                    {
                        InWriter.WriteLine("{0}\t{1}\t{2}", DataPanel.EntityLookup[rawSource], DataPanel.EntityLookup[item.Item1], item.Item2);
                    }
                }
                TotalBatchNum += Query[0].BatchSize;
                HashSet<int>[] uniqueHash = DataUtil.AggregrateUnique(Query);
                TotalResultNode += uniqueHash.Select(t => t.Count).Sum();
                for (int p = 0; p < Query.Count; p++)
                {
                    Query[p].Results.Clear();
                }
            }
        }

        class HitKRunner : StructRunner
        {
            //float MAP = 0;

            //int SmpIdx = 0;
            List<GraphQueryData> Query { get; set; }
            int Iteration = 0;
            int TotalResultNode = 0;
            int Hit1 = 0;
            int Hit3 = 0;
            int Hit5 = 0;
            int Hit10 = 0;
            int Hit20 = 0;
            float AP = 0;
            int SmpNum = 0;
            string ResultDump = "";
            StreamWriter writer = null;
            public HitKRunner(List<GraphQueryData> query, RunnerBehavior behavior, string resultDump = "") : base(Structure.Empty, behavior)
            {
                Query = query;
                Iteration = 0;
                ResultDump = resultDump;
            }

            public override void Init()
            {
                Hit1 = 0;
                Hit3 = 0;
                Hit5 = 0;
                Hit10 = 0;
                Hit20 = 0;
                AP = 0;
                SmpNum = 0;
                TotalResultNode = 0;

                if (ResultDump != "")
                {
                    writer = new StreamWriter(ResultDump);
                }
            }

            

            public override void Complete()
            {
                Logger.WriteLog("MRR {0}", AP * 1.0f / SmpNum);
                Logger.WriteLog("Hit1 {0}", Hit1 * 1.0f / SmpNum);
                Logger.WriteLog("Hit3 {0}", Hit3 * 1.0f / SmpNum);
                Logger.WriteLog("Hit5 {0}", Hit5 * 1.0f / SmpNum);
                Logger.WriteLog("Hit10 {0}", Hit10 * 1.0f / SmpNum);
                Logger.WriteLog("Hit20 {0}", Hit20 * 1.0f / SmpNum);
                Logger.WriteLog("Avg Visit Leaf Node {0}", TotalResultNode * 1.0f / SmpNum);
                Iteration += 1;

                if(writer != null)
                {
                    writer.Close();
                }
            }

            public override void Forward()
            {
                List<Tuple<int, float>>[] scoreDict = DataUtil.AggregrateScore(Query, BuilderParameters.PROB_LAMBDA, BuilderParameters.SCORE_LAMBDA, BuilderParameters.AGG_SCORE);

                List<Tuple<int, string, float, float>>[] pathResults = new List<Tuple<int, string, float, float>>[Query[0].BatchSize];
                for (int i = 0; i < Query[0].BatchSize; i++)
                {
                    pathResults[i] = new List<Tuple<int, string, float, float>>();
                }

                for (int m = 0; m < Query.Count; m++)
                {
                    for (int i = 0; i < Query[m].Results.Count; i++)
                    {
                        int t = Query[m].Results[i].Item1;
                        int b = Query[m].Results[i].Item2;
                        int origialB = Query[m].StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = Query[m].StatusPath[t].NodeID[b];


                        float tQ = Query[m].StatusPath[t].TermQ.Output.Data.MemPtr[b];
                        string pathKey = Query[m].StatusPath[t].GetStatusKey(b);

                        float black = 0;
                        // filter the edge which already exists.
                        if (Query[m].BlackTargets[origialB].Contains(predId) && Query[m].RawTarget[origialB] != predId)
                        {
                            black = 1;
                        }
                        pathResults[origialB].Add(new Tuple<int, string, float, float>(predId, pathKey,  tQ, black));
                    }
                }

                for (int originB = 0; originB < Query[0].BatchSize; originB++)
                {
                    int rawSource = Query[0].RawSource[originB];
                    int rawTarget = Query[0].RawTarget[originB];
                    scoreDict[originB].Sort((x, y) => y.Item2.CompareTo(x.Item2));

                    int pos = 1;
                    bool isFound = false;
                    HashSet<int> visitIdx = new HashSet<int>();
                    foreach (Tuple<int, float> item in scoreDict[originB])
                    {
                        if (visitIdx.Contains(item.Item1)) continue;
                        visitIdx.Add(item.Item1);
                        if(item.Item1 == rawTarget)
                        {
                            isFound = true;
                            break;
                        }
                        pos += 1;
                    }

                    
                    if (isFound)
                    {
                        AP += 1.0f / pos;
                        if(pos <= 1) { Hit1 += 1; }
                        if (pos <= 3) { Hit3 += 1; }
                        if (pos <= 5) { Hit5 += 1; }
                        if (pos <= 10) { Hit10 += 1; }
                        if (pos <= 20) { Hit20 += 1; }
                    }


                    if (writer != null)
                    {
                        string p = pos.ToString();
                        if (!isFound) p = "-1";

                        writer.WriteLine(DataPanel.EntityLookup[Query[0].RawSource[originB]] + "\t" +
                                         DataPanel.RelationLookup[Query[0].RawQuery[originB].Item2] + "\t" +
                                         DataPanel.EntityLookup[Query[0].RawTarget[originB]] + "\t" + p + "\t" + scoreDict[originB].Count.ToString());

                        for (int p1 = 0; p1 < pathResults[originB].Count; p1++)
                        {
                            writer.WriteLine("@MCTS {0}@ {1} @ {2} @ {3} @ {4}", p1, pathResults[originB][p1].Item1, pathResults[originB][p1].Item2, pathResults[originB][p1].Item3, pathResults[originB][p1].Item4);
                        }
                    }
                }
                SmpNum += Query[0].BatchSize;

                HashSet<int>[] scoreHash = DataUtil.AggregrateUnique(Query);
                TotalResultNode += scoreHash.Select(m => m.Count).Sum();

                for (int p = 0; p < Query.Count; p++)
                {
                    Query[p].Results.Clear();
                }
            }
        }


        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure InNodeEmbed { get; set; }
            public EmbedStructure InRelEmbed { get; set; }

            public EmbedStructure CNodeEmbed { get; set; }
            public EmbedStructure CRelEmbed { get; set; }

            public DNNStructure SrcDNN { get; set; }
            public DNNStructure TgtDNN { get; set; }

            public LayerStructure AttEmbed { get; set; }
            public DNNStructure TermDNN { get; set; }
            public DNNStructure NonTermDNN { get; set; }
            public DNNStructure ScoreDNN { get; set; }

            public GRUCell GruCell { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeDim, int relDim, DeviceType device)
            {
                InNodeEmbed = new EmbedStructure(entityNum, nodeDim, device); // DeviceType.CPU_FAST_VECTOR));
                InRelEmbed = new EmbedStructure(relationNum, relDim, device); // DeviceType.CPU_FAST_VECTOR));

                //if(!BuilderParameters.INIT_NODE_EMD.Equals(string.Empty))
                //{
                //    DataPanel.InitNodeEmbedLayers(BuilderParameters.INIT_NODE_EMD, InNodeEmbed, device);
                //    CNodeEmbed = InNodeEmbed;
                //}
                //else
                {
                    AddLayer(InNodeEmbed);
                }

                //if (!BuilderParameters.INIT_REL_EMD.Equals(string.Empty))
                //{
                //    DataPanel.InitRelEmbedLayers(BuilderParameters.INIT_REL_EMD, InRelEmbed, device);
                //    CRelEmbed = InRelEmbed;
                //}
                //else
                {
                    AddLayer(InRelEmbed);
                }

                if (BuilderParameters.IS_SHARE_NODE > 0)
                {
                    CNodeEmbed = InNodeEmbed;
                }
                else
                {
                    CNodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device));// DeviceType.CPU_FAST_VECTOR));
                }

                if (BuilderParameters.IS_SHARE_RELATION > 0)
                {
                    CRelEmbed = InRelEmbed;
                }
                else
                {
                    CRelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
                }

                int StateDim = nodeDim + relDim;

                SrcDNN = AddLayer(new DNNStructure(StateDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                TgtDNN = AddLayer(new DNNStructure(StateDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                AttEmbed = AddLayer(new LayerStructure(BuilderParameters.DNN_DIMS.Last(), 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device));

                TermDNN = AddLayer(new DNNStructure(StateDim + BuilderParameters.DNN_DIMS.Last(), BuilderParameters.T_NET, BuilderParameters.T_AF,
                                         BuilderParameters.T_NET.Select(i => true).ToArray(), device));

                ScoreDNN = AddLayer(new DNNStructure(StateDim + BuilderParameters.DNN_DIMS.Last(), BuilderParameters.S_NET, BuilderParameters.S_AF,
                                         BuilderParameters.S_NET.Select(i => true).ToArray(), device));

                GruCell = AddLayer(new GRUCell(StateDim + BuilderParameters.DNN_DIMS.Last(), StateDim, device));

                NonTermDNN = AddLayer(new DNNStructure(StateDim + BuilderParameters.DNN_DIMS.Last(), BuilderParameters.T_NET, BuilderParameters.T_AF,
                                         BuilderParameters.T_NET.Select(i => true).ToArray(), device));

            }
            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                InNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                InRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

                if (BuilderParameters.IS_SHARE_NODE > 0)
                {
                    CNodeEmbed = InNodeEmbed;
                }
                else
                {
                    CNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                }

                if (BuilderParameters.IS_SHARE_RELATION > 0)
                {
                    CRelEmbed = InRelEmbed;
                }
                else
                {
                    CRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                }
                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
                TgtDNN = (DNNStructure)DeserializeNextModel(reader, device);

                AttEmbed = (LayerStructure)DeserializeNextModel(reader, device);
                TermDNN = (DNNStructure)DeserializeNextModel(reader, device);

                //if (BuilderParameters.SEED_VERSION == 1)
                //{
                ScoreDNN = (DNNStructure)DeserializeNextModel(reader, device);
                //}
                //else
                //{
                //    ScoreDNN = AddLayer(new DNNStructure(BuilderParameters.N_EmbedDim + BuilderParameters.R_EmbedDim, BuilderParameters.S_NET, BuilderParameters.S_AF,
                //                         BuilderParameters.S_NET.Select(i => true).ToArray(), device));
                //}
                GruCell = (GRUCell)DeserializeNextModel(reader, device);

                NonTermDNN = (DNNStructure)DeserializeNextModel(reader, device);
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        class MemoryScheduleRunner : StructRunner
        {
            //EpisodicMemory GMemory { get; set; }
            EpisodicMemory LMemory { get; set; }
            //public new List<EpisodicMemory> Output { get; set; }
            //EpisodicMemory globalMemory,
            public MemoryScheduleRunner(EpisodicMemory localMemory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                LMemory = localMemory;
            }
            int Epoch = 0;
            public override void Complete()
            {
                Epoch += 1;
                //if (Behavior.RunMode == DNNRunMode.Predict || Epoch % BuilderParameters.RESET_MEM == 0)
                //{
                LMemory.Clear();
                //}
            }
            public override void Forward()
            {
                LMemory.Clear();
            }
        }

        public static ComputationGraph BuildComputationGraph(DataEnviroument data, GraphEnviroument graph, int batchSize, int roll, int mctsNum, int beamSize,
            int maxHop, EpisodicMemory mem, NeuralWalkerModel model, int eval_type, string resultDump, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            // sample a list of tuplet from the graph.
            SampleRunner SmpRunner = new SampleRunner(data, batchSize, roll, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            MemoryScheduleRunner memRunner = new MemoryScheduleRunner(mem, Behavior);
            cg.AddRunner(memRunner);
            
            // get the embedding from the query data.
            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
            cg.AddRunner(statusEmbedRunner);

            List<GraphQueryData> Queries = new List<GraphQueryData>();
            // MCTS path number.
            for (int j = 0; j < mctsNum; j++)
            {
                GraphQueryData newQuery = new GraphQueryData(interface_data);
                HiddenBatchData RawQueryEmbed = statusEmbedRunner.Output;
                StatusData status = new StatusData(newQuery, newQuery.RawSource, RawQueryEmbed, Behavior.Device);

                #region multi-hop expansion
                // travel four steps in the knowledge graph.
                for (int i = 0; i < maxHop; i++)
                {
                    // given status, obtain the match. miniBatch * maxNeighbor number.
                    CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, graph, Behavior);
                    cg.AddRunner(candidateActionRunner);

                    // miniMatch * maxNeighborNumber.
                    StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, model.CNodeEmbed, model.CRelEmbed, Behavior);
                    cg.AddRunner(candEmbedRunner);

                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                    cg.AddRunner(srcHiddenRunner);

                    DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
                    cg.AddRunner(candHiddenRunner);

                    SeqDenseBatchData neiSeqData = new SeqDenseBatchData(new SequenceDataStat() { FEATURE_DIM = candHiddenRunner.Output.Dim, MAX_BATCHSIZE = status.MaxBatchSize, MAX_SEQUENCESIZE = candHiddenRunner.Output.MAX_BATCHSIZE },
                                                                             candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, candHiddenRunner.Output.Output.Data, candHiddenRunner.Output.Deriv.Data, Behavior.Device);

                    // neighbor dataset.
                    HiddenBatchData neiData = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(neiSeqData, Behavior));

                    // alignment miniBatch * miniBatch * neighbor.
                    VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
                                                                                candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
                    cg.AddRunner(attentionRunner);

                    status.MatchCandidateQ = new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
                                                                                status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
                                                                                candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
                                                                                Behavior.Device);

                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(status.MatchCandidateQ, 1, Behavior, true);
                    cg.AddRunner(normAttRunner);

                    status.MatchCandidate = candidateActionRunner.Output;
                    status.MatchCandidateProb = normAttRunner.Output;

                    MatrixData newStateData = (MatrixData)cg.AddRunner(new MatrixConcateRunner(new List<MatrixData>() { new MatrixData(status.StateEmbed), new MatrixData(neiData) }, Behavior));

                    DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, new HiddenBatchData(newStateData), Behavior);
                    cg.AddRunner(termRunner);
                    status.TermQ = termRunner.Output;

                    DNNRunner<HiddenBatchData> nonTermRunner = new DNNRunner<HiddenBatchData>(model.NonTermDNN, new HiddenBatchData(newStateData), Behavior);
                    cg.AddRunner(nonTermRunner);
                    status.NonTermQ = nonTermRunner.Output;
                    
                    DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, new HiddenBatchData(newStateData), Behavior) { name = "V_SCORE" };
                    cg.AddRunner(scoreRunner);
                    status.Score = scoreRunner.Output;

                    BasicPolicyRunner policyRunner = null;
                    if (beamSize == 1)
                    {
                        policyRunner = new MCTSActionSamplingRunner(status, graph.MaxNeighborNum, j, mem, Behavior, i == maxHop - 1);
                    }
                    else
                    {
                        if (BuilderParameters.IS_Q_BEAM > 0)
                        {
                            policyRunner = new QBeamSearchActionRunner(status, graph.MaxNeighborNum, beamSize, Behavior, i == maxHop - 1);
                        }
                        else
                        {
                            policyRunner = new BeamSearchActionRunner(status, graph.MaxNeighborNum, beamSize, Behavior, i == maxHop - 1);
                        }
                    }

                    cg.AddRunner(policyRunner);

                    if (i < maxHop - 1)
                    {
                        #region take action.
                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(srcExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
                        cg.AddRunner(tgtExpRunner);

                        MatrixExpansionRunner neiExpRunner = new MatrixExpansionRunner(neiData, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(neiExpRunner);

                        //MatrixExpansionRunner rawExpRunner = new MatrixExpansionRunner(RawQueryEmbed, policyRunner.MatchPath, 1, Behavior);
                        //cg.AddRunner(rawExpRunner);
                        //RawQueryEmbed = rawExpRunner.Output;

                        //, rawExpRunner.Output 
                        HiddenBatchData concateData = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { neiExpRunner.Output, tgtExpRunner.Output }, Behavior));

                        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, concateData, Behavior);
                        cg.AddRunner(stateRunner);
                        #endregion.

                        StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                            policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, policyRunner.StatusKey, stateRunner.Output, Behavior.Device);
                        status = nextStatus;
                    }
                }
                #endregion.

                {
                    RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(newQuery, mem, Behavior);
                    cg.AddRunner(rewardFeedRunner);
                }
                Queries.Add(newQuery);
            }

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                if(BuilderParameters.AGG_SCORE == 4)
                {
                    QRewardAdvRunner qAdvRunner = new QRewardAdvRunner(Queries, Behavior);
                    cg.AddObjective(qAdvRunner);
                }
                else
                {
                    QRewardRunner qRunner = new QRewardRunner(Queries, Behavior);
                    cg.AddObjective(qRunner);
                }
            }
            else
            {
                if (eval_type > 0)
                {
                    MAPPredictionRunner predRunner = new MAPPredictionRunner(Queries, Behavior);
                    cg.AddRunner(predRunner);
                }
                else
                {
                    HitKRunner hitRunner = new HitKRunner(Queries, Behavior, resultDump);
                    cg.AddRunner(hitRunner);
                }
            }
            cg.SetDelegateModel(model);
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

           
            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            //if (BuilderParameters.RunMode == DNNRunMode.Train)
           // {
             //  
            //}

            if (BuilderParameters.IS_SHORTPATH_STAT == 1)
            {
                Dictionary<int, int> step1 = DataPanel.GraphEnv.TestDataGraph(DataPanel.TrainData); //, "");
                foreach (KeyValuePair<int, int> i in step1)
                {
                    Logger.WriteLog("Train Step {0}, Stat {1}", i.Key, i.Value);
                }

                Dictionary<int, int> step2 = DataPanel.GraphEnv.TestDataGraph(DataPanel.TestData); //, BuilderParameters.LogFile + "test.bfs");
                foreach (KeyValuePair<int, int> i in step2)
                {
                    Logger.WriteLog("Test Step {0}, Stat {1}", i.Key, i.Value);
                }

                Dictionary<int, int> step3 = DataPanel.GraphEnv.TestDataGraph(DataPanel.DevData); //, BuilderParameters.LogFile + "dev.bfs");
                foreach (KeyValuePair<int, int> i in step3)
                {
                    Logger.WriteLog("Dev Step {0}, Stat {1}", i.Key, i.Value);
                }
            }

            if(BuilderParameters.IS_SHORTPATH_STAT == 2)
            {
                return;
            }

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);


            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(DataPanel.EntityDict.Count, DataPanel.RelationDict.Count, BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);

            if (BuilderParameters.RunMode == DNNRunMode.Train)
            {
                model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
            }
            EpisodicMemory Memory = new EpisodicMemory(true);
            ComputationGraph testCG = null;
            ComputationGraph validCG = null;
            // prediction. 0:beam search; 1:mcts search;
            if (BuilderParameters.SCORE_TYPE == 0)
            {
                testCG = BuildComputationGraph(DataPanel.TestData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1, 1, BuilderParameters.BeamSize, BuilderParameters.MAX_HOP, Memory, model,
                                                 BuilderParameters.MAP_EVAL, BuilderParameters.LogFile + "test.dump",
                                                 new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                validCG = BuildComputationGraph(DataPanel.DevData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                             1, 1, BuilderParameters.BeamSize, BuilderParameters.MAX_HOP, Memory, model,
                                             0, BuilderParameters.LogFile + "valid.dump",
                                             new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

            }
            else if (BuilderParameters.SCORE_TYPE == 1)
            {
                testCG = BuildComputationGraph(DataPanel.TestData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1, BuilderParameters.TEST_MCTS_NUM, 1, BuilderParameters.MAX_HOP, Memory, model,
                                                 BuilderParameters.MAP_EVAL, BuilderParameters.LogFile + "test.dump",
                                                 new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                validCG = BuildComputationGraph(DataPanel.DevData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1, BuilderParameters.TEST_MCTS_NUM, 1, BuilderParameters.MAX_HOP, Memory, model,
                                                 0, BuilderParameters.LogFile + "valid.dump",
                                                 new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainData, DataPanel.GraphEnv, BuilderParameters.MiniBatchSize,
                                                                     BuilderParameters.ROLL_NUM, BuilderParameters.MCTS_NUM, BuilderParameters.TrainBeamSize, BuilderParameters.MAX_HOP, Memory, model,
                                                                     0, BuilderParameters.LogFile + "train.dump",
                                                                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    //testCG.Execute();
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        //ActionSampler = new Random(10);
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);
                            //validCG.Execute();
                            testCG.Execute();
                            validCG.Execute();  
                        }
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                        {
                            model.Serialize(writer);
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    testCG.Execute();
                    validCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }
        
        class DataUtil
        {
            /// <summary>
            /// type 0: list all scores; 1: max aggregrate; 2 : sum aggregrate.
            /// </summary>
            /// <param name="query"></param>
            /// <param name="prob_wei"></param>
            /// <param name="score_wei"></param>
            /// <param name="type"></param>
            /// <returns></returns>
            public static List<Tuple<int, float>>[] AggregrateScore(List<GraphQueryData> query, float prob_wei, float score_wei, int aggType = 0)
            {
                List<Tuple<int, float>>[] result = new List<Tuple<int, float>>[query[0].BatchSize];
                List<Tuple<float, float, float>>[] prob = new List<Tuple<float, float, float>>[query[0].BatchSize];
                for (int b = 0; b < query[0].BatchSize; b++)
                {
                    result[b] = new List<Tuple<int, float>>();
                    prob[b] = new List<Tuple<float, float, float>>();
                }

                
                for (int m = 0; m < query.Count; m++)
                {
                    for (int i = 0; i < query[m].Results.Count; i++)
                    {
                        int t = query[m].Results[i].Item1;
                        int b = query[m].Results[i].Item2;
                        int origialB = query[m].StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = query[m].StatusPath[t].NodeID[b];

                        float v = prob_wei * (query[m].StatusPath[t].GetLogProb(b) + query[m].StatusPath[t].LogPTerm(b, true)); // (float)Math.Log(query[m].StatusPath[t].Term.Output.Data.MemPtr[b]));

                        float mmv = query[m].StatusPath[t].Score.Output.Data.MemPtr[b];
                        float ms = Util.LogLogistial(mmv); //query[m].StatusPath[t].Score.Output.Data.MemPtr[b]; // 
                        float nms = Util.LogNLogistial(mmv);

                        float tQ = query[m].StatusPath[t].TermQ.Output.Data.MemPtr[b];
                        v = v + ms * score_wei;


                        // filter the edge which already exists.
                        if(query[m].BlackTargets[origialB].Contains(predId) && query[m].RawTarget[origialB] != predId)
                        {
                            continue;
                        }

                        bool isFound = false;
                        if (aggType == 1)
                        {
                            for (int idx = 0; idx < result[origialB].Count; idx++)
                            {
                                int pre_k = result[origialB][idx].Item1;
                                if (pre_k == predId)
                                {
                                    float pre_v = result[origialB][idx].Item2;
                                    result[origialB].RemoveAt(idx);
                                    result[origialB].Add(new Tuple<int, float>(pre_k, Math.Max(pre_v, v)));
                                    isFound = true;
                                    break;
                                }
                            }
                        }
                        if (aggType == 11)
                        {
                            v = tQ;
                            for (int idx = 0; idx < result[origialB].Count; idx++)
                            {
                                int pre_k = result[origialB][idx].Item1;
                                if (pre_k == predId)
                                {
                                    float pre_v = result[origialB][idx].Item2;
                                    result[origialB].RemoveAt(idx);
                                    result[origialB].Add(new Tuple<int, float>(pre_k, Math.Max(pre_v, v)));
                                    isFound = true;
                                    break;
                                }
                            }
                        }
                        if (aggType == 111)
                        {
                            v = tQ;
                            for (int idx = 0; idx < result[origialB].Count; idx++)
                            {
                                int pre_k = result[origialB][idx].Item1;
                                if (pre_k == predId)
                                {
                                    float pre_v = result[origialB][idx].Item2;
                                    result[origialB].RemoveAt(idx);
                                    result[origialB].Add(new Tuple<int, float>(pre_k, pre_v + v));
                                    isFound = true;
                                    break;
                                }
                            }
                        }
                        if (aggType == 12)
                        {
                            v = 1;
                            for (int idx = 0; idx < result[origialB].Count; idx++)
                            {
                                int pre_k = result[origialB][idx].Item1;
                                if (pre_k == predId)
                                {
                                    float pre_v = result[origialB][idx].Item2;
                                    result[origialB].RemoveAt(idx);
                                    result[origialB].Add(new Tuple<int, float>(pre_k, pre_v + v));
                                    isFound = true;
                                    break;
                                }
                            }
                        }
                        else if (aggType == 2)
                        {
                            for (int idx = 0; idx < result[origialB].Count; idx++)
                            {
                                int pre_k = result[origialB][idx].Item1;
                                if (pre_k == predId)
                                {
                                    float pre_v = result[origialB][idx].Item2;
                                    result[origialB].RemoveAt(idx);
                                    result[origialB].Add(new Tuple<int, float>(pre_k, Util.LogAdd(pre_v, v)));
                                    isFound = true;
                                    break;
                                }
                            }
                        }

                        else if (aggType == 3)
                        {
                            float pre_log = m == 0 ? 0 : prob[origialB][m - 1].Item1;
                            float mv =  pre_log + ms;

                            for (int idx = 0; idx < result[origialB].Count; idx++)
                            {
                                int pre_k = result[origialB][idx].Item1;
                                if (pre_k == predId)
                                {
                                    float pre_v = result[origialB][idx].Item2;
                                    result[origialB].RemoveAt(idx);
                                    result[origialB].Add(new Tuple<int, float>(pre_k, Util.LogAdd(pre_v, mv)));
                                    isFound = true;
                                    break;
                                }
                            }

                            prob[origialB].Add(new Tuple<float, float, float>(pre_log + nms, 0, 0));
                        }

                        else if(aggType == 4)
                        {
                            v = mmv;
                            for (int idx = 0; idx < result[origialB].Count; idx++)
                            {
                                int pre_k = result[origialB][idx].Item1;
                                if (pre_k == predId)
                                {
                                    float pre_v = result[origialB][idx].Item2;
                                    result[origialB].RemoveAt(idx);
                                    result[origialB].Add(new Tuple<int, float>(pre_k, pre_v + v));
                                    isFound = true;
                                    break;
                                }
                            }
                        }

                        if (aggType == 0 || !isFound)
                        {
                            result[origialB].Add(new Tuple<int, float>(predId, v));
                        }
                    }
                }
                
                return result;
            }

            public static HashSet<int>[] AggregrateUnique(List<GraphQueryData> query)
            {
                HashSet<int>[] result = new HashSet<int>[query[0].BatchSize];
                for (int b = 0; b < query[0].BatchSize; b++)
                {
                    result[b] = new HashSet<int>();
                }


                for (int m = 0; m < query.Count; m++)
                {
                    for (int i = 0; i < query[m].Results.Count; i++)
                    {
                        int t = query[m].Results[i].Item1;
                        int b = query[m].Results[i].Item2;
                        int origialB = query[m].StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = query[m].StatusPath[t].NodeID[b];


                        // filter the edge which already exists.
                        if (query[m].BlackTargets[origialB].Contains(predId) && query[m].RawTarget[origialB] != predId)
                        {
                            continue;
                        }
                        if (!result[origialB].Contains(predId))
                        {
                            result[origialB].Add(predId);
                        }
                    }
                }

                return result;
            }

        }


        public class DataPanel
        {
            public static GraphEnviroument GraphEnv { get; set; }

            public static DataEnviroument TrainData { get; set; }
            public static DataEnviroument TestData { get; set; }
            public static DataEnviroument DevData { get; set; }


            public static Dictionary<string, int> EntityDict { get; set; }
            public static Dictionary<string, int> RelationDict { get; set; }

            public static Dictionary<int, string> EntityLookup { get; set; }
            public static Dictionary<int, string> RelationLookup { get; set; }

            public static int ReverseRelation(int relation)
            {
                string relStr = RelationLookup[relation];
                if (relStr.StartsWith("_"))
                {
                    string newRel = relStr.Substring(1);
                    if (RelationDict.ContainsKey(newRel))
                    {
                        return RelationDict[newRel];
                    }
                }

                string newRel2 = "_" + relStr;
                if (RelationDict.ContainsKey(newRel2))
                {
                    return RelationDict[newRel2];
                }


                if (relStr.EndsWith("_inv"))
                {
                    string newRel3 = relStr.Substring(0, relStr.Length - 4);
                    if (RelationDict.ContainsKey(newRel3))
                    {
                        return RelationDict[newRel3];
                    }
                }

                string newRel4 = relStr + "_inv";
                if (RelationDict.ContainsKey(newRel4))
                {
                    return RelationDict[newRel4];
                }

                return -1;
                //throw new NotImplementedException("Cannot find the reverse Relation " + relStr);
            }

            public static string START_SYMBOL = "#START#";
            public static string END_SYMBOL = "#END#";

            public static void Init()
            {
                EntityDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + @"\vocab\entity_vocab.json"));
                RelationDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + @"\vocab\relation_vocab.json"));
                RelationDict.Add(END_SYMBOL, RelationDict.Count);
                RelationDict.Add(START_SYMBOL, RelationDict.Count);

                EntityLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in EntityDict)
                {
                    EntityLookup.Add(item.Value, item.Key);
                }

                RelationLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in RelationDict)
                {
                    RelationLookup.Add(item.Value, item.Key);
                }

                GraphEnv = new GraphEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"\graph.txt");

                TrainData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"\train.txt",
                                            new string[] { BuilderParameters.InputDir + @"\train.txt" });

                TestData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"\test.txt",
                                            new string[] { BuilderParameters.InputDir + @"\test.txt",
                                                           BuilderParameters.InputDir + @"\train.txt",
                                                           BuilderParameters.InputDir + @"\dev.txt",
                                                           BuilderParameters.InputDir + @"\graph.txt" });

                DevData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"\dev.txt",
                                            new string[] { BuilderParameters.InputDir + @"\test.txt",
                                                           BuilderParameters.InputDir + @"\train.txt",
                                                           BuilderParameters.InputDir + @"\dev.txt",
                                                           BuilderParameters.InputDir + @"\graph.txt" });
            }

        }

        public class DataEnviroument
        {
            public List<Tuple<int, int, int>> Triple { get; set; }
            public List<int> ShortestReachStep { get; set; }
            public Dictionary<Tuple<int, int>, HashSet<int>> TruthDict { get; set; }

            public DataEnviroument(Dictionary<string, int> entityDict, Dictionary<string, int> relationDict, string tripleFile, string[] graphFiles)
            {
                Triple = new List<Tuple<int, int, int>>();
                TruthDict = new Dictionary<Tuple<int, int>, HashSet<int>>();
                ShortestReachStep = new List<int>();
                int missNum = 0;
                using (StreamReader tripleReader = new StreamReader(tripleFile))
                {
                    while (!tripleReader.EndOfStream)
                    {
                        string[] tokens = tripleReader.ReadLine().Split('\t');

                        //if (!entityDict.ContainsKey(tokens[0]) || !relationDict.ContainsKey(tokens[1]) || !entityDict.ContainsKey(tokens[2]))
                        //{
                        //    Console.WriteLine("Graph doesn't exist {0}, {1}, {2}", tokens[0], tokens[1], tokens[2]);
                        //    missNum += 1;
                        //    continue;
                        //}

                        if (!entityDict.ContainsKey(tokens[0]))
                        {
                            Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                            missNum += 1;
                            continue;
                        }

                        if (!relationDict.ContainsKey(tokens[1]))
                        {
                            Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                            missNum += 1;
                            continue;
                        }

                        if (!entityDict.ContainsKey(tokens[2]))
                        {
                            Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                            missNum += 1;
                            continue;
                        }

                        int e1 = entityDict[tokens[0]];
                        int r = relationDict[tokens[1]];
                        int e2 = entityDict[tokens[2]];

                        Triple.Add(new Tuple<int, int, int>(e1, r, e2));
                    }

                    Logger.WriteLog("{0} Graph miss connections {1}", tripleFile, missNum);
                }

                foreach (string gStr in graphFiles)
                {
                    using (StreamReader gReader = new StreamReader(gStr))
                    {
                        while (!gReader.EndOfStream)
                        {
                            string[] tokens = gReader.ReadLine().Split('\t');

                            //if (!entityDict.ContainsKey(tokens[0]) || !relationDict.ContainsKey(tokens[1]) || !entityDict.ContainsKey(tokens[2]))
                            //{
                            //    Console.WriteLine("Graph doesn't exist {0}, {1}, {2}", tokens[0], tokens[1], tokens[2]);
                            //    continue;
                            //}

                            if (!entityDict.ContainsKey(tokens[0]))
                            {
                                Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                                continue;
                            }

                            if (!relationDict.ContainsKey(tokens[1]))
                            {
                                Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                                continue;
                            }

                            if (!entityDict.ContainsKey(tokens[2]))
                            {
                                Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                                continue;
                            }

                            int e1 = entityDict[tokens[0]];
                            int r = relationDict[tokens[1]];
                            int e2 = entityDict[tokens[2]];

                            Tuple<int, int> gKey = new Tuple<int, int>(e1, r);

                            if (!TruthDict.ContainsKey(gKey))
                            {
                                TruthDict.Add(gKey, new HashSet<int>());
                            }
                            TruthDict[gKey].Add(e2);
                        }
                    }
                }
            }
        }

        public class GraphEnviroument
        {
            public Dictionary<int, List<Tuple<int, int>>> Graph { get; set; }

            public int MaxNeighborNum { get; set; }
            public int StopIdx { get; set; }
            public int StartIdx { get; set; }
            public GraphEnviroument(Dictionary<string, int> entityDict, Dictionary<string, int> relationDict, string graphFile)
            {
                Graph = new Dictionary<int, List<Tuple<int, int>>>();
                StopIdx = relationDict[DataPanel.END_SYMBOL];
                StartIdx = relationDict[DataPanel.START_SYMBOL];
                int missNum = 0;
                using (StreamReader graphReader = new StreamReader(graphFile))
                {
                    while (!graphReader.EndOfStream)
                    {
                        string[] tokens = graphReader.ReadLine().Split('\t');

                        if (!entityDict.ContainsKey(tokens[0]))
                        {
                            Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                            missNum += 1;
                            continue;
                        }

                        if (!relationDict.ContainsKey(tokens[1]))
                        {
                            Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                            missNum += 1;
                            continue;
                        }

                        if (!entityDict.ContainsKey(tokens[2]))
                        {
                            Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                            missNum += 1;
                            continue;
                        }

                        int e1 = entityDict[tokens[0]];
                        int r = relationDict[tokens[1]];
                        int e2 = entityDict[tokens[2]];

                        if (!Graph.ContainsKey(e1)) { Graph.Add(e1, new List<Tuple<int, int>>()); }

                        // set the maximum action number.
                        if (BuilderParameters.MAX_ACTION_NUM > 0 && Graph[e1].Count >= BuilderParameters.MAX_ACTION_NUM)
                            continue;
                        Graph[e1].Add(new Tuple<int, int>(r, e2));
                    }

                    Logger.WriteLog("{0} Graph miss connections {1}", graphFile, missNum);
                }

                MaxNeighborNum = 0;
                foreach (int entity in Graph.Keys)
                {
                    //Graph[entity].Add(new Tuple<int, int>(StopIdx, entity));
                    if (Graph[entity].Count + 1 >= MaxNeighborNum) { MaxNeighborNum = Graph[entity].Count + 1; }
                }

                Logger.WriteLog("Max Neighbor Number {0}", MaxNeighborNum);
            }

            public Dictionary<int, int> TestDataGraph(DataEnviroument data)
            {
                Dictionary<int, int> stepStat = new Dictionary<int, int>();
                data.ShortestReachStep.Clear();
                foreach (Tuple<int, int, int> item in data.Triple)
                {
                    int step = ShortestPath(item.Item1, item.Item3, item.Item2, BuilderParameters.MAX_HOP);
                    if (!stepStat.ContainsKey(step))
                    {
                        stepStat.Add(step, 1);
                    }
                    else
                    {
                        stepStat[step] += 1;
                    }
                    data.ShortestReachStep.Add(step);
                }
                return stepStat;
            }

            public HashSet<int> NeighborSet(int e, int maxHop)
            {
                List<KeyValuePair<int, int>> searchNodes = new List<KeyValuePair<int, int>>();
                HashSet<int> history = new HashSet<int>();

                searchNodes.Add(new KeyValuePair<int, int>(e, 0));
                history.Add(e);

                int begin = 0;
                int end = searchNodes.Count;
                int iter = 0;
                for (iter = 0; iter <= maxHop; iter++)
                {
                    for (int i = begin; i < end; i++)
                    {
                        KeyValuePair<int, int> xnode = searchNodes[i];

                        // no neighbor continue;
                        if (!Graph.ContainsKey(xnode.Key)) continue;

                        foreach (Tuple<int, int> transitem in Graph[xnode.Key])
                        {
                            // histroy connected node : continue;
                            if (history.Contains(transitem.Item2)) continue;

                            int step = xnode.Value + 1;
                            history.Add(transitem.Item2);
                            searchNodes.Add(new KeyValuePair<int, int>(transitem.Item2, step));
                        }
                    }
                    begin = end;
                    end = searchNodes.Count;
                }
                return history;
            }

            /// <summary>
            /// calculate the shortest path from entity e1 and entity e2.
            /// </summary>
            /// <param name="e1"></param>
            /// <param name="e2"></param>
            /// <param name="blackrid"></param>
            /// <param name="maxHop"></param>
            /// <returns></returns>
            public int ShortestPath(int e1, int e2, int blackrid, int maxHop)
            {
                List<KeyValuePair<int, int>> searchNodes = new List<KeyValuePair<int, int>>();
                HashSet<int> history = new HashSet<int>();

                searchNodes.Add(new KeyValuePair<int, int>(e1, 0));
                history.Add(e1);

                int begin = 0;
                int end = searchNodes.Count;
                int iter = 0;
                for (iter = 0; iter <= maxHop; iter++)
                {
                    for (int i = begin; i < end; i++)
                    {
                        KeyValuePair<int, int> xnode = searchNodes[i];

                        // no neighbor continue;
                        if (!Graph.ContainsKey(xnode.Key)) continue;

                        foreach (Tuple<int, int> transitem in Graph[xnode.Key])
                        {
                            // histroy connected node : continue;
                            if (history.Contains(transitem.Item2)) continue;

                            // black link : continue;
                            if (xnode.Key == e1 && transitem.Item1 == blackrid && transitem.Item2 == e2) continue;
                            if (xnode.Key == e2 && transitem.Item1 == DataPanel.ReverseRelation(blackrid) && transitem.Item2 == e1) continue;

                            int step = xnode.Value + 1;
                            history.Add(transitem.Item2);
                            searchNodes.Add(new KeyValuePair<int, int>(transitem.Item2, step));

                            if (transitem.Item2 == e2) return step;
                        }
                    }
                    begin = end;
                    end = searchNodes.Count;
                }

                return -1;
            }


        }
    }
}
