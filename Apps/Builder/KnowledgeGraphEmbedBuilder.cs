﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    /// <summary>
    /// Embed Knowledge Graph.
    /// TransE.
    /// </summary>
    public class KnowledgeGraphEmbedBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
            public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }
            
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static string TestData { get { return Argument["TEST"].Value; } }

            public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
            public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }

            public static int EmbedDim { get { return int.Parse(Argument["EMBED-DIM"].Value); } }
            
            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }
            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static int BatchIteration { get { return int.Parse(Argument["BATCH-ITERATION"].Value); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static string ScoreOutputPath
            {
                get
                {
                    return Argument["SCORE-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.score" : Argument["SCORE-PATH"].Value;
                }
            }
            public static string MetricOutputPath
            {
                get
                {
                    return Argument["METRIC-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.metric" : Argument["METRIC-PATH"].Value;
                }
            }
            public static DeviceType Device
            {
                get
                {
                    if (GPUID >= 0)
                    {
                        return DeviceType.GPU;
                    }
                    else if (GPUID == -1)
                    {
                        return DeviceType.CPU;
                    }
                    else
                    {
                        return DeviceType.CPU_FAST_VECTOR;
                    }
                }
            }

            static BuilderParameters()
            {
                Argument.Add("ENTITY2ID", new ParameterArgument(string.Empty, "Entity 2 ID."));
                Argument.Add("RELATION2ID", new ParameterArgument(string.Empty, "Relation 2 ID."));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
                Argument.Add("EMBED-DIM", new ParameterArgument("100", "Embedding Dim"));


                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-VERSION", new ParameterArgument(((int)DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR).ToString(), ParameterUtil.EnumValues(typeof(DNNModelVersion))));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SHARE-MODEL", new ParameterArgument("0", "Share Source and Target Model; 0 : no share; 1 : share;"));
                Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));

                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.SampleSoftmax).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
                Argument.Add("NTRAIL", new ParameterArgument("20", "Negative Sample Number."));

                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "DSSM Learn Rate."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.AdaGrad).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
                Argument.Add("ADABOOST", new ParameterArgument("0.01", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

                Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("BATCH-ITERATION", new ParameterArgument("20", "Train Batch Iteration."));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.NDCG).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));
                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));

                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));
            }
        }

        public override BuilderType Type { get { return BuilderType.KNOWLEDGE_GRAPH_EMBED; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            


            GraphLinearEmbedStruct model = new GraphLinearEmbedStruct(DataPanel.EntityNum, DataPanel.RelationNum, BuilderParameters.EmbedDim);

            //GraphBiLinearEmbedStruct model = null;
            //if (!BuilderParameters.SEED_MODEL.Equals(string.Empty))
            //{
            //    model = new GraphBiLinearEmbedStruct(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), DeviceType.CPU);
            //}
            //else model = new GraphBiLinearEmbedStruct(DataPanel.EntityNum, DataPanel.RelationNum, BuilderParameters.EmbedDim);

            ComputationGraph trainCG = BuildComputationGraph(model, DataPanel.Train, DNNRunMode.Train); //  BuildComputationGraphV2(model, DataPanel.Train, DNNRunMode.Train);

            ComputationGraph predCG = null;
            if (BuilderParameters.IsValidFile) predCG = BuildComputationGraph(model, DataPanel.Test, DNNRunMode.Predict);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            double validScore = 0;
            double bestValidScore = double.MinValue;
            double bestValidIter = -1;

            model.EntityOptimizer = new SparseSGDOptimizer(model.EntityVec, BuilderParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = device, Computelib = computeLib });
            model.RelationOptimizer = new SparseSGDOptimizer(model.RelationVec, BuilderParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = device, Computelib = computeLib });
            trainCG.SetDelegateModel(model);


            //SparseSGDOptimizerFlow trainflow = new SparseSGDOptimizerFlow();
            //StructureLearner leaner = new StructureLearner
            //{
            //    LearnRate = BuilderParameters.LearnRate
            //};
            //trainflow.Init(model, new RunnerBehavior() { Device = DeviceType.CPU }, trainCG, leaner);

            int IterN = 20;
            for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
            {
                double loss = trainCG.Execute();
                                //trainflow.Run();
                Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                if (DeepNet.BuilderParameters.ModelSavePerIteration)
                {
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\Embed.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()), FileMode.Create, FileAccess.Write)))
                    {
                        model.Serialize(writer);
                    }
                }

                if ((iter + 1) % IterN == 0)
                {
                    if (BuilderParameters.IsValidFile)
                    {
                        validScore = predCG.Execute();
                        if (validScore >= bestValidScore)
                        {
                            bestValidScore = validScore;
                            bestValidIter = iter;
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\Embed.{1}.model", BuilderParameters.ModelOutputPath, "done"), FileMode.Create, FileAccess.Write)))
                            {
                                model.Serialize(writer);
                            }
                        }
                        Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                    }
                }
            }
            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);

        }

        //public class SparseGradientOptimizer : GradientOptimizer
        //{
        //    float UpdateRate { get; set; }
        //    bool IsNorm = true;
        //    public SparseGradientOptimizer(CudaPieceFloat weight, float learnRate, RunnerBehavior behavior) : base(behavior)
        //    {
        //        UpdateRate = learnRate;
        //        GradientStep = 1.0f;
        //        Parameter = weight;
        //        Gradient = new CudaPieceFloat(weight.Size, true, behavior.Device == DeviceType.GPU);
        //    }

        //    //public virtual void PushGradientIndex(int idx, int range)
        //    //{
        //    //    if (GradientIndex.ContainsKey(idx)) GradientIndex[idx] = Math.Max(GradientIndex[idx], range);
        //    //    else GradientIndex[idx] = range;
        //    //}

        //    public override void BeforeGradient()
        //    { }

        //    public override void AfterGradient()
        //    {
        //        Gradient.SyncToCPU();
        //        Parameter.SyncToCPU();

        //        var sparse_update_perf = PerfCounter.Manager.Instance["Sparse Update AVX"].Begin();
        //        //Parallel.ForEach(GradientIndex, gIdx =>
        //        foreach(KeyValuePair<int,int> gIdx in SparseGradientDict)
        //        {
        //            AVXFastVector p = new AVXFastVector(Parameter.MemPtr, gIdx.Key, gIdx.Value, false);
        //            AVXFastVector g = new AVXFastVector(Gradient.MemPtr, gIdx.Key, gIdx.Value, false);
        //            p.AddScale(UpdateRate, g);

        //            if (IsNorm)
        //            {
        //                double x = p.L2Norm;
        //                if (x > 1) { p.Scale((float)(1.0 / x)); }
        //            }

        //            //for (int s = 0; s < gIdx.Value; s++)
        //            //{
        //            //    Parameter.MemPtr[gIdx.Key + s] += UpdateRate * Gradient.MemPtr[gIdx.Key + s];
        //            //}

        //            //if (IsNorm)
        //            //{
        //            //    double x = 0;
        //            //    for (int s = 0; s < gIdx.Value; s++)
        //            //    {
        //            //        x = x + Parameter.MemPtr[gIdx.Key + s] * Parameter.MemPtr[gIdx.Key + s];
        //            //    }
        //            //    x = Math.Sqrt(x);
        //            //    if (x > 1)
        //            //        for (int ii = 0; ii < gIdx.Value; ii++)
        //            //            Parameter.MemPtr[gIdx.Key + ii] = (float)(Parameter.MemPtr[gIdx.Key + ii] / x);
        //            //}
        //        }
        //        //);
        //        Parameter.SyncFromCPU();

        //        ClearSparseGradient();
        //        ComputeLib.Zero(Gradient, Gradient.Size);

        //        PerfCounter.Manager.Instance["Sparse Update AVX"].TakeCount(sparse_update_perf);
        //    }
        //}

        //public class SparseSGDOptimizerFlow : OptimizerFlow
        //{
        //    float LearnRate { get; set; }

        //    public override void Init(Structure model, RunnerBehavior behavior, ComputationGraph cg, StructureLearner learner)
        //    {
        //        Model = model;
        //        CG = cg;
        //        Device = behavior.Device;
        //        LearnRate = learner.LearnRate;

        //        /// **************** Init Model-Weight Optimizer; ***************** ///
        //        foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
        //            optimizer.Optimizer = new SparseGradientOptimizer(optimizer.Parameter, LearnRate, behavior);
        //    }

        //    public override double Run()
        //    {
        //        double Loss = 0;
        //        int batchIdx = 0;
        //        CG.Init();
        //        while (CG.FetchData())
        //        {
        //            if (!CG.IsContinue) continue;

        //            Loss = batchIdx * 1.0f / (batchIdx + 1) * Loss + 1.0f / (batchIdx + 1) * CG.ForwardLoss();

        //            if (++batchIdx % 50 == 0) Logger.WriteLog("Train Batch Num {0}, AvgLoss {1}", batchIdx, Loss);

        //            foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
        //                optimizer.Optimizer.BeforeGradient();

        //            CG.BackwardGrad();

        //            foreach (ModelOptimizer optimizer in Model.ModelOptimizers)
        //                optimizer.Optimizer.AfterGradient();
        //        }
        //        CG.Complete();

        //        return Loss;
        //    }
        //}

        public static ComputationGraph BuildComputationGraph(GraphLinearEmbedStruct model, List<Tuple<int, int, int>> data, DNNRunMode mode)
        {
            ComputationGraph cg = new ComputationGraph();

            /// Get Random Batch Data. 
            TripleRelationData batchData = (TripleRelationData)cg.AddDataRunner(new TripleRelationDataRunner(data, 
                new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

            switch (mode)
            {
                case DNNRunMode.Train:
                    /// Get Negative Batch.
                    TripleRelationData negData = (TripleRelationData)cg.AddRunner(new NegativeTripleRunner<TripleRelationData>(batchData,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    /// Feed target into TransE Model.
                    HiddenBatchData targetScore = (HiddenBatchData)cg.AddRunner(new TransERunner<TripleRelationData>(model, batchData,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    /// Feed Negative into TransE Model.
                    HiddenBatchData negScore = (HiddenBatchData)cg.AddRunner(new TransERunner<TripleRelationData>(model, negData,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    cg.AddObjective(new MarginLossRunner(1.0f, targetScore, negScore,
                            new RunnerBehavior() { RunMode = mode, Device = DeviceType.CPU }));
                    break;
                case DNNRunMode.Predict:
                    TripleRelationData testData = (TripleRelationData)cg.AddRunner(new FullNegativeTripleRunner<TripleRelationData>(batchData, DataPanel.EntityNum,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    /// Feed Negative into TransE Model.
                    HiddenBatchData testScore = (HiddenBatchData)cg.AddRunner(new TransERunner<TripleRelationData>(model, testData,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    cg.AddRunner(new HitMeanRankDumpRunner(testScore, batchData, 10, DataPanel.EntityNum));
                    break;
            }
            return cg;
        }

        public static ComputationGraph BuildComputationGraphV2(GraphBiLinearEmbedStruct model, List<Tuple<int, int, int>> data, DNNRunMode mode)
        {
            ComputationGraph cg = new ComputationGraph();

            /// Get Random Batch Data. 
            TripleRelationData batchData = (TripleRelationData)cg.AddDataRunner(new TripleRelationDataRunner(data,
                new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

            switch (mode)
            {
                case DNNRunMode.Train:
                    /// Get Negative Batch.
                    TripleRelationData negData = (TripleRelationData)cg.AddRunner(new NegativeTripleRunner<TripleRelationData>(batchData,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    /// Feed target into TransE Model.
                    HiddenBatchData targetScore = (HiddenBatchData)cg.AddRunner(new BiTransERunner<TripleRelationData>(model, batchData,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    /// Feed Negative into TransE Model.
                    HiddenBatchData negScore = (HiddenBatchData)cg.AddRunner(new BiTransERunner<TripleRelationData>(model, negData,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    cg.AddObjective(new MarginLossRunner(1.0f, targetScore, negScore,
                            new RunnerBehavior() { RunMode = mode, Device = DeviceType.CPU }));
                    break;
                case DNNRunMode.Predict:
                    TripleRelationData testData = (TripleRelationData)cg.AddRunner(new FullNegativeTripleRunner<TripleRelationData>(batchData, DataPanel.EntityNum,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    /// Feed Negative into TransE Model.
                    HiddenBatchData testScore = (HiddenBatchData)cg.AddRunner(new BiTransERunner<TripleRelationData>(model, testData,
                        new RunnerBehavior() { Device = DeviceType.CPU, RunMode = mode }));

                    cg.AddRunner(new HitMeanRankDumpRunner(testScore, batchData, 10, DataPanel.EntityNum));
                    break;
            }
            return cg;
        }

        public class GraphLinearEmbedStruct : Structure
        {
            public CudaPieceFloat EntityVec;
            public CudaPieceFloat RelationVec;

            public GradientOptimizer EntityOptimizer { get { return StructureOptimizer["EntityVec"].Optimizer; } set { StructureOptimizer["EntityVec"].Optimizer = value; } }
            public GradientOptimizer RelationOptimizer { get { return StructureOptimizer["RelationVec"].Optimizer; } set { StructureOptimizer["RelationVec"].Optimizer = value; } }

            public int EntityNum;
            public int RelationNum;
            public int Dim;

            public void NormEntity()
            {
                Parallel.For(0, EntityNum, i =>
                    {
                        double x = Math.Sqrt(EntityVec.MemPtr.Skip(i * Dim).Take(Dim).Select(v => v * v).Sum());
                        if (x > 1)
                            for (int ii = 0; ii < Dim; ii++)
                                EntityVec.MemPtr[i * Dim + ii] = (float)(EntityVec.MemPtr[i * Dim + ii] / x);
                    });
            }

            public override void Serialize(BinaryWriter writer)
            {
                writer.Write(EntityNum);
                writer.Write(RelationNum);
                writer.Write(Dim);

                EntityVec.SyncToCPU();
                RelationVec.SyncToCPU();

                for (int i = 0; i < EntityNum * Dim; i++) writer.Write(EntityVec.MemPtr[i]);
                for (int i = 0; i < RelationNum * Dim; i++) writer.Write(RelationVec.MemPtr[i]);
            }

            public override void Deserialize(BinaryReader reader, DeviceType device)
            {
                EntityNum = reader.ReadInt32();
                RelationNum = reader.ReadInt32();
                Dim = reader.ReadInt32();
                EntityVec = new CudaPieceFloat(EntityNum * Dim, true, device == DeviceType.GPU);
                RelationVec = new CudaPieceFloat(RelationNum * Dim, true, device == DeviceType.GPU);

                Buffer.BlockCopy(reader.ReadBytes(sizeof(Single) * EntityNum * Dim), 0, EntityVec.MemPtr, 0, sizeof(Single) * EntityNum * Dim);
                Buffer.BlockCopy(reader.ReadBytes(sizeof(Single) * RelationNum * Dim), 0, RelationVec.MemPtr, 0, sizeof(Single) * RelationNum * Dim);
            }

            public GraphLinearEmbedStruct(int entityNum, int relationNum, int dim)
            {
                Dim = dim;
                EntityNum = entityNum;
                RelationNum = relationNum;
                EntityVec = new CudaPieceFloat(EntityNum * Dim, true, false);
                RelationVec = new CudaPieceFloat(RelationNum * Dim, true, false);
                DeviceType = DeviceType.CPU;

                EntityVec.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));
                RelationVec.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));
                NormEntity();
            }

            protected override void InitStructureOptimizer()
            {
                StructureOptimizer.Add("EntityVec", new ModelOptimizer() { Parameter = EntityVec });
                StructureOptimizer.Add("RelationVec", new ModelOptimizer() { Parameter = RelationVec });
            }
        }

        public class GraphBiLinearEmbedStruct : Structure
        {
            public CudaPieceFloat EntityVecIn;
            public CudaPieceFloat EntityVecOut;
            public CudaPieceFloat RelationVec;

            public GradientOptimizer EntityInOptimizer { get { return StructureOptimizer["EntityVecIn"].Optimizer; } }
            public GradientOptimizer EntityOutOptimizer { get { return StructureOptimizer["EntityVecOut"].Optimizer; } }
            public GradientOptimizer RelationOptimizer { get { return StructureOptimizer["RelationVec"].Optimizer; } }

            public int EntityNum;
            public int RelationNum;
            public int Dim;

            public override void Serialize(BinaryWriter writer)
            {
                writer.Write(EntityNum);
                writer.Write(RelationNum);
                writer.Write(Dim);

                EntityVecIn.SyncToCPU();
                EntityVecOut.SyncToCPU();
                RelationVec.SyncToCPU();

                for (int i = 0; i < EntityNum * Dim; i++) writer.Write(EntityVecIn.MemPtr[i]);
                for (int i = 0; i < EntityNum * Dim; i++) writer.Write(EntityVecOut.MemPtr[i]);
                for (int i = 0; i < RelationNum * Dim; i++) writer.Write(RelationVec.MemPtr[i]);
            }

            public override void Deserialize(BinaryReader reader, DeviceType device)
            {
                EntityNum = reader.ReadInt32();
                RelationNum = reader.ReadInt32();
                Dim = reader.ReadInt32();
                EntityVecIn = new CudaPieceFloat(EntityNum * Dim, true, device == DeviceType.GPU);
                EntityVecOut = new CudaPieceFloat(EntityNum * Dim, true, device == DeviceType.GPU);
                RelationVec = new CudaPieceFloat(RelationNum * Dim, true, device == DeviceType.GPU);

                Buffer.BlockCopy(reader.ReadBytes(sizeof(Single) * EntityNum * Dim), 0, EntityVecIn.MemPtr, 0, sizeof(Single) * EntityNum * Dim);
                Buffer.BlockCopy(reader.ReadBytes(sizeof(Single) * EntityNum * Dim), 0, EntityVecOut.MemPtr, 0, sizeof(Single) * EntityNum * Dim);
                Buffer.BlockCopy(reader.ReadBytes(sizeof(Single) * RelationNum * Dim), 0, RelationVec.MemPtr, 0, sizeof(Single) * RelationNum * Dim);
            }

            public void NormEntity()
            {
                Parallel.For(0, EntityNum, i =>
                {
                    double x = Math.Sqrt(EntityVecIn.MemPtr.Skip(i * Dim).Take(Dim).Select(v => v * v).Sum());
                    if (x > 1)
                        for (int ii = 0; ii < Dim; ii++)
                            EntityVecIn.MemPtr[i * Dim + ii] = (float)(EntityVecIn.MemPtr[i * Dim + ii] / x);
                });

                Parallel.For(0, EntityNum, i =>
                {
                    double x = Math.Sqrt(EntityVecOut.MemPtr.Skip(i * Dim).Take(Dim).Select(v => v * v).Sum());
                    if (x > 1)
                        for (int ii = 0; ii < Dim; ii++)
                            EntityVecOut.MemPtr[i * Dim + ii] = (float)(EntityVecOut.MemPtr[i * Dim + ii] / x);
                });
            }

            public GraphBiLinearEmbedStruct(int entityNum, int relationNum, int dim)
            {
                Dim = dim;
                EntityNum = entityNum;
                RelationNum = relationNum;
                EntityVecIn = new CudaPieceFloat(EntityNum * Dim, true, false);
                EntityVecOut = new CudaPieceFloat(EntityNum * Dim, true, false);
                RelationVec = new CudaPieceFloat(RelationNum * Dim, true, false);
                DeviceType = DeviceType.CPU;

                EntityVecIn.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));
                EntityVecOut.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));
                RelationVec.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));
                NormEntity();
            }

            public GraphBiLinearEmbedStruct(BinaryReader reader, DeviceType device) : base(reader, device)
            { }
            protected override void InitStructureOptimizer()
            {
                StructureOptimizer.Add("EntityVecIn", new ModelOptimizer() { Parameter = EntityVecIn });
                StructureOptimizer.Add("EntityVecOut", new ModelOptimizer() { Parameter = EntityVecOut });
                StructureOptimizer.Add("RelationVec", new ModelOptimizer() { Parameter = RelationVec });
            }
        }

        public class TransERunner<IN> : StructRunner<GraphLinearEmbedStruct, IN> where IN : TripleRelationData
        {
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            bool IsL1Flag = true;

            public TransERunner(GraphLinearEmbedStruct model, TripleRelationData data, RunnerBehavior behavior)
                : base(model, data, behavior)
            {
                Output = new HiddenBatchData(data.MAX_BATCH_SIZE, 1, Behavior.RunMode, Behavior.Device);
            }

            public override void Forward()
            {
                Input.LeftEntity.SyncToCPU();
                Input.RightEntity.SyncToCPU();
                Input.Relation.SyncToCPU();

                Model.EntityVec.SyncToCPU();
                Model.RelationVec.SyncToCPU();
                Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
                {
                    double sum = 0;
                    int eid1 = Input.LeftEntity.MemPtr[i];
                    int eid2 = Input.RightEntity.MemPtr[i];
                    int rid = Input.Relation.MemPtr[i];

                    for (int ii = 0; ii < Model.Dim; ii++)
                    {
                        float w = Model.EntityVec.MemPtr[eid2 * Model.Dim + ii] - Model.EntityVec.MemPtr[eid1 * Model.Dim + ii] - Model.RelationVec.MemPtr[rid * Model.Dim + ii];
                        if (IsL1Flag) sum += Math.Abs(w);
                        else sum += w * w;
                    }
                    Output.Output.Data.MemPtr[i] = (float)sum;
                });
                Output.BatchSize = Input.BatchSize;
                Output.Output.Data.SyncFromCPU();
            }

            public override void CleanDeriv()
            {
                Output.Deriv.Data.Zero();
            }

            public override void Update()
            {
                //Model.EntityOptimizer.BeforeGradient();
                //Model.RelationOptimizer.BeforeGradient();
                for (int ii = 0; ii < Input.BatchSize; ii++)
                {
                    int eid1 = Input.LeftEntity.MemPtr[ii];
                    int eid2 = Input.RightEntity.MemPtr[ii];
                    int rid = Input.Relation.MemPtr[ii];

                    (Model.RelationOptimizer).PushSparseGradient(rid * Model.Dim, Model.Dim);
                    (Model.EntityOptimizer).PushSparseGradient(eid1 * Model.Dim, Model.Dim);
                    (Model.EntityOptimizer).PushSparseGradient(eid2 * Model.Dim, Model.Dim);
                }

                Output.Deriv.Data.SyncToCPU();
                Parallel.For(0, Model.Dim, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
                {
                    for (int ii = 0; ii < Input.BatchSize; ii++)
                    {
                        int eid1 = Input.LeftEntity.MemPtr[ii];
                        int eid2 = Input.RightEntity.MemPtr[ii];
                        int rid = Input.Relation.MemPtr[ii];
                        float w = Model.EntityVec.MemPtr[eid2 * Model.Dim + i] - Model.EntityVec.MemPtr[eid1 * Model.Dim + i] - Model.RelationVec.MemPtr[rid * Model.Dim + i];
                        if (IsL1Flag)
                        {
                            if (w >= 0) w = Output.Deriv.Data.MemPtr[ii];
                            else w = -Output.Deriv.Data.MemPtr[ii];
                        }
                        else w = 2 * w * Output.Deriv.Data.MemPtr[ii];


                        Model.RelationOptimizer.Gradient.MemPtr[rid * Model.Dim + i] -= Model.RelationOptimizer.GradientStep * w;
                        Model.EntityOptimizer.Gradient.MemPtr[eid1 * Model.Dim + i] -= Model.EntityOptimizer.GradientStep * w;
                        Model.EntityOptimizer.Gradient.MemPtr[eid2 * Model.Dim + i] += Model.EntityOptimizer.GradientStep * w;
                    }
                });



                //Model.EntityOptimizer.AfterGradient();
                //Model.RelationOptimizer.AfterGradient();
                //Model.NormEntity();
            }
        }

        public class BiTransERunner<IN> : StructRunner<GraphBiLinearEmbedStruct, IN> where IN : TripleRelationData
        {
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            bool IsL1Flag = true;

            public BiTransERunner(GraphBiLinearEmbedStruct model, TripleRelationData data, RunnerBehavior behavior)
                : base(model, data, behavior)
            {
                Output = new HiddenBatchData(data.MAX_BATCH_SIZE, 1, Behavior.RunMode, Behavior.Device);
            }

            public override void Forward()
            {
                Input.LeftEntity.SyncToCPU();
                Input.RightEntity.SyncToCPU();
                Input.Relation.SyncToCPU();

                Model.EntityVecIn.SyncToCPU();
                Model.EntityVecOut.SyncToCPU();
                Model.RelationVec.SyncToCPU();

                Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
                {
                    double sum = 0;
                    int eid1 = Input.LeftEntity.MemPtr[i];
                    int eid2 = Input.RightEntity.MemPtr[i];
                    int rid = Input.Relation.MemPtr[i];

                    for (int ii = 0; ii < Model.Dim; ii++)
                    {
                        float w1 = Model.EntityVecOut.MemPtr[eid2 * Model.Dim + ii] - Model.EntityVecIn.MemPtr[eid1 * Model.Dim + ii] - Model.RelationVec.MemPtr[rid * Model.Dim + ii];
                        float w2 = Model.EntityVecOut.MemPtr[eid1 * Model.Dim + ii] - Model.EntityVecIn.MemPtr[eid2 * Model.Dim + ii] + Model.RelationVec.MemPtr[rid * Model.Dim + ii];
                        if (IsL1Flag) sum += Math.Abs(w1) + Math.Abs(w2);
                        else sum += w1 * w1 + w2 * w2;
                    }
                    Output.Output.Data.MemPtr[i] = (float)sum;
                });
                Output.BatchSize = Input.BatchSize;
                Output.Output.Data.SyncFromCPU();
            }

            public override void CleanDeriv()
            {
                Output.Deriv.Data.Zero();
            }

            public override void Update()
            {
                //Model.EntityOptimizer.BeforeGradient();
                //Model.RelationOptimizer.BeforeGradient();
                for (int ii = 0; ii < Input.BatchSize; ii++)
                {
                    int eid1 = Input.LeftEntity.MemPtr[ii];
                    int eid2 = Input.RightEntity.MemPtr[ii];
                    int rid = Input.Relation.MemPtr[ii];

                    (Model.RelationOptimizer).PushSparseGradient(rid * Model.Dim, Model.Dim);
                    
                    (Model.EntityInOptimizer).PushSparseGradient(eid1 * Model.Dim, Model.Dim);
                    (Model.EntityOutOptimizer).PushSparseGradient(eid1 * Model.Dim, Model.Dim);

                    (Model.EntityInOptimizer).PushSparseGradient(eid2 * Model.Dim, Model.Dim);
                    (Model.EntityOutOptimizer).PushSparseGradient(eid2 * Model.Dim, Model.Dim);
                }

                Output.Deriv.Data.SyncToCPU();
                
                Parallel.For(0, Model.Dim, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
                {
                    for (int ii = 0; ii < Input.BatchSize; ii++)
                    {
                        int eid1 = Input.LeftEntity.MemPtr[ii];
                        int eid2 = Input.RightEntity.MemPtr[ii];
                        int rid = Input.Relation.MemPtr[ii];
                        float w1 = Model.EntityVecOut.MemPtr[eid2 * Model.Dim + i] - Model.EntityVecIn.MemPtr[eid1 * Model.Dim + i] - Model.RelationVec.MemPtr[rid * Model.Dim + i];
                        float w2 = Model.EntityVecOut.MemPtr[eid1 * Model.Dim + i] - Model.EntityVecIn.MemPtr[eid2 * Model.Dim + i] + Model.RelationVec.MemPtr[rid * Model.Dim + i];
                        
                        if (IsL1Flag)
                        {
                            if (w1 >= 0) w1 = Output.Deriv.Data.MemPtr[ii];
                            else w1 = -Output.Deriv.Data.MemPtr[ii];
                        }
                        else w1 = 2 * w1 * Output.Deriv.Data.MemPtr[ii];
                        Model.RelationOptimizer.Gradient.MemPtr[rid * Model.Dim + i] -= Model.RelationOptimizer.GradientStep * w1;
                        Model.EntityInOptimizer.Gradient.MemPtr[eid1 * Model.Dim + i] -= Model.EntityInOptimizer.GradientStep * w1;
                        Model.EntityOutOptimizer.Gradient.MemPtr[eid2 * Model.Dim + i] += Model.EntityOutOptimizer.GradientStep * w1;

                        if (IsL1Flag)
                        {
                            if (w2 >= 0) w2 = Output.Deriv.Data.MemPtr[ii];
                            else w2 = -Output.Deriv.Data.MemPtr[ii];
                        }
                        else w2 = 2 * w2 * Output.Deriv.Data.MemPtr[ii];
                        Model.RelationOptimizer.Gradient.MemPtr[rid * Model.Dim + i] += Model.RelationOptimizer.GradientStep * w2;
                        Model.EntityInOptimizer.Gradient.MemPtr[eid2 * Model.Dim + i] -= Model.EntityInOptimizer.GradientStep * w2;
                        Model.EntityOutOptimizer.Gradient.MemPtr[eid1 * Model.Dim + i] += Model.EntityOutOptimizer.GradientStep * w2;
                    }
                });
                //Model.EntityOptimizer.AfterGradient();
                //Model.RelationOptimizer.AfterGradient();
                //Model.NormEntity();
            }
        }

        public class NegativeTripleRunner<IN> : StructRunner<Structure, IN> where IN : TripleRelationData 
        {
            public new TripleRelationData Output { get { return (TripleRelationData)base.Output; } set { base.Output = value; } }

            int NegVersion = 0; // 0 : unif; 1 : bern;
            
            

            public NegativeTripleRunner(TripleRelationData input, RunnerBehavior behavior)
                : base(Structure.Empty, input, behavior)
            {
                Output = new TripleRelationData(Input.MAX_BATCH_SIZE);
            }

            /// <summary>
            /// Generate Negative Triples From Knowledge Graph.
            /// </summary>
            public override void Forward()
            {
                Input.LeftEntity.SyncToCPU();
                Input.RightEntity.SyncToCPU();
                Input.Relation.SyncToCPU();

                Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
                    {
                        Random MRandom = new Random((int)DateTime.Now.ToBinary());

                        double pr = 0;
                        int tail = Input.RightEntity.MemPtr[i];
                        int head = Input.LeftEntity.MemPtr[i];
                        int r = Input.Relation.MemPtr[i];
                        /// unif;
                        if (NegVersion == 0) pr = 0.5;
                        else pr = DataPanel.TailProb[r] / (DataPanel.TailProb[r] + DataPanel.HeadProb[r]);
                        if (MRandom.NextDouble() < pr)
                        {
                            do
                            {
                                tail = MRandom.Next(DataPanel.EntityNum);
                            } while (DataPanel.IsInGraph(head, tail, r));
                        }
                        else
                        {
                            do
                            {
                                head = MRandom.Next(DataPanel.EntityNum);
                            } while (DataPanel.IsInGraph(head, tail, r));
                        }
                        Output.LeftEntity.MemPtr[i] = head;
                        Output.RightEntity.MemPtr[i] = tail;
                        Output.Relation.MemPtr[i] = r;
                    });
                Output.BatchSize = Input.BatchSize;
                Output.LeftEntity.SyncFromCPU();
                Output.RightEntity.SyncFromCPU();
                Output.Relation.SyncFromCPU();
            }
        }
        
        public class FullNegativeTripleRunner<IN> : StructRunner<Structure, IN> where IN : TripleRelationData 
        {
            public new TripleRelationData Output { get { return (TripleRelationData)base.Output; } set { base.Output = value; } }

            int EntityNum { get; set; }
            public FullNegativeTripleRunner(TripleRelationData input, int entityNum, RunnerBehavior behavior)
                : base(Structure.Empty, input, behavior)
            {
                EntityNum = entityNum;
                Output = new TripleRelationData(Input.MAX_BATCH_SIZE * EntityNum * 2);
            }

            /// <summary>
            /// Generate Negative Triples From Knowledge Graph.
            /// </summary>
            public override void Forward()
            {
                Input.LeftEntity.SyncToCPU();
                Input.RightEntity.SyncToCPU();
                Input.Relation.SyncToCPU();

                Parallel.For(0, Input.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = 32 }, i =>
                    {
                        int tail = Input.RightEntity.MemPtr[i];
                        int head = Input.LeftEntity.MemPtr[i];
                        int r = Input.Relation.MemPtr[i];

                        for (int h = 0; h < EntityNum; h++)
                        {
                            Output.LeftEntity.MemPtr[i * EntityNum * 2 + h] = h;
                            Output.RightEntity.MemPtr[i * EntityNum * 2 + h] = tail;
                            Output.Relation.MemPtr[i * EntityNum * 2 + h] = r;
                        }
                        for (int t = 0; t < EntityNum; t++)
                        {
                            Output.LeftEntity.MemPtr[i * EntityNum * 2 + EntityNum + t] = head;
                            Output.RightEntity.MemPtr[i * EntityNum * 2 + EntityNum + t] = t;
                            Output.Relation.MemPtr[i * EntityNum * 2 + EntityNum + t] = r;
                        }
                    });
                Output.BatchSize = Input.BatchSize * EntityNum * 2;
                Output.LeftEntity.SyncFromCPU();
                Output.RightEntity.SyncFromCPU();
                Output.Relation.SyncFromCPU();
            }
        }

        public class TripleRelationData : BatchData
        {
            public int MAX_BATCH_SIZE { get; set; }
            public CudaPieceInt LeftEntity;
            public CudaPieceInt RightEntity;
            public CudaPieceInt Relation;
            public int BatchSize { get; set; }
            public TripleRelationData(int maxBatchSize)
            {
                MAX_BATCH_SIZE = maxBatchSize;
                LeftEntity = new CudaPieceInt(MAX_BATCH_SIZE, true, false);
                RightEntity = new CudaPieceInt(MAX_BATCH_SIZE, true, false);
                Relation = new CudaPieceInt(MAX_BATCH_SIZE, true, false);
            }
        }

        public class TripleRelationDataRunner : StructRunner
        {
            List<Tuple<int, int, int>> Dataset { get; set; }
            
            DataRandomShuffling Shuffle;

            public new TripleRelationData Output { get { return (TripleRelationData)base.Output; } set { base.Output = value; } }

            int BatchSize = 64;
            public TripleRelationDataRunner(List<Tuple<int, int, int>> data, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Dataset = data;
                Output = new TripleRelationData(BatchSize);
                Shuffle = new DataRandomShuffling(data.Count);
            }
             
            public override void Init()
            {
                Shuffle.Init();
                IsTerminate = false;
            }

            public override void Forward()
            {
                Output.BatchSize = 0;
                for (int i = 0; i < BatchSize; i++)
                {
                    int id = Shuffle.RandomNext();
                    if (id == -1) break;
                    Output.LeftEntity.MemPtr[i] = Dataset[id].Item1;
                    Output.RightEntity.MemPtr[i] = Dataset[id].Item2;
                    Output.Relation.MemPtr[i] = Dataset[id].Item3;
                    Output.BatchSize += 1;
                }
                if (Output.BatchSize == 0) IsTerminate = true;
                else
                {
                    Output.LeftEntity.SyncFromCPU(Output.BatchSize);
                    Output.RightEntity.SyncFromCPU(Output.BatchSize);
                    Output.Relation.SyncFromCPU(Output.BatchSize);
                }
            }
        }

        public class HitMeanRankDumpRunner : ObjectiveRunner
        {
            public new  HiddenBatchData Output { get; set; }
            TripleRelationData Label { get; set; }
            int HitK { get; set; }
            int CandidateNum { get; set; }
            /// <summary>
            /// Output Score & 
            /// </summary>
            /// <param name="output"></param>
            /// <param name="output"></param>
            /// <param name="gamma"></param>
            /// <param name="outputFileName"></param>
            public HitMeanRankDumpRunner(HiddenBatchData output, TripleRelationData groundTruth, int hitK, int candidateNum)
                : base(Structure.Empty, new RunnerBehavior())
            {
                Label = groundTruth;
                Output = output;
                HitK = hitK;
                CandidateNum = candidateNum;
            }


            int lsum = 0, lsum_filter = 0;
            int rsum = 0, rsum_filter = 0;
            int lp_n = 0, lp_n_filter = 0;
            int rp_n = 0, rp_n_filter = 0;
            int totalSampleNum = 0;
            public override void Init()
            {
                totalSampleNum = 0;
                lsum = 0;
                lsum_filter = 0;
                rsum = 0;
                rsum_filter = 0;
                lp_n = 0;
                lp_n_filter = 0;
                rp_n = 0;
                rp_n_filter = 0;
            }

            public override void Complete()
            {
                Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 1.0 / totalSampleNum));
                Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 1.0 / totalSampleNum));
                Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 1.0 / totalSampleNum));
                Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 1.0 / totalSampleNum));

                Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 1.0 / totalSampleNum));
                Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 1.0 / totalSampleNum));
                Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 1.0 / totalSampleNum));
                Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 1.0 / totalSampleNum));


                Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 0.5 / totalSampleNum));
                Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 0.5 / totalSampleNum));
                Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 0.5 / totalSampleNum));
                Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 0.5 / totalSampleNum));
                ObjectiveScore = (lp_n_filter + rp_n_filter) * 0.5 / totalSampleNum;
            }

            public override void Forward()
            {
                Label.LeftEntity.SyncToCPU();
                Label.RightEntity.SyncToCPU();
                Label.Relation.SyncToCPU();

                Output.Output.Data.SyncToCPU();
                totalSampleNum += Label.BatchSize;
                Parallel.For(0, Label.BatchSize, i => //  (int i = 0; i < Label.BatchSize; i++)
                {
                    int head = Label.LeftEntity.MemPtr[i];
                    int tail = Label.RightEntity.MemPtr[i];
                    int r = Label.Relation.MemPtr[i];

                    IEnumerable<float> lscore = Output.Output.Data.MemPtr.Skip(i * CandidateNum * 2).Take(CandidateNum);
                    int[] lindicies = Enumerable.Range(0, CandidateNum).ToArray();
                    Array.Sort(lscore.ToArray(), lindicies);

                    int filter = 0;
                    for (int c = CandidateNum - 1; c >= 0; c--)
                    {
                        int chead = lindicies[CandidateNum - 1 - c];
                        if (!DataPanel.IsInGraph(chead, tail, r)) filter++;
                        if (chead == head)
                        {
                            Interlocked.Add(ref lsum, CandidateNum - c);
                            Interlocked.Add(ref lsum_filter, filter + 1);
                            //lsum += CandidateNum - c;
                            //lsum_filter += filter + 1;
                            if (CandidateNum - c <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
                            if (filter < HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
                            break;
                        }
                    }

                    IEnumerable<float> rscore = Output.Output.Data.MemPtr.Skip(i * CandidateNum * 2 + CandidateNum).Take(CandidateNum);
                    int[] rindicies = Enumerable.Range(0, CandidateNum).ToArray();
                    Array.Sort(rscore.ToArray(), rindicies);

                    filter = 0;
                    for (int c = CandidateNum - 1; c >= 0; c--)
                    {
                        int ctail = rindicies[CandidateNum - 1 - c];
                        if (!DataPanel.IsInGraph(head, ctail, r)) filter++;
                        if (ctail == tail)
                        {
                            Interlocked.Add(ref rsum, CandidateNum - c);
                            Interlocked.Add(ref rsum_filter, filter + 1);
                            if (CandidateNum - c <= HitK) Interlocked.Increment(ref rp_n);  //rp_n += 1;
                            if (filter < HitK) Interlocked.Increment(ref rp_n_filter); // += 1; rp_n_filter += 1;
                            break;
                        }
                    }
                });
            }
        }



        public class DataPanel
        {
            static RelationGraphData knowledgeGraph;

            public static int EntityNum { get { return knowledgeGraph.EntityId.Count; } }
            public static int RelationNum { get { return knowledgeGraph.RelationId.Count; } }

            public static List<Tuple<int, int, int>> Train {
                get { return knowledgeGraph.Train; } }

            public static List<Tuple<int, int, int>> Test {
                get { return knowledgeGraph.Test; } }

            public static List<Tuple<int, int, int>> Valid {
                get { return knowledgeGraph.Valid; } }


            public static double[] HeadProb { get { return knowledgeGraph.HeadProb; } }

            public static double[] TailProb { get { return knowledgeGraph.TailProb; } }

            public static bool IsInGraph(int eid1, int eid2, int rid)
            {
                if (knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
                else return false;
            }

            public static void Init()
            {
                knowledgeGraph = new RelationGraphData();

                knowledgeGraph.EntityId = RelationGraphData.LoadMapId(BuilderParameters.Entity2Id);
                knowledgeGraph.RelationId = RelationGraphData.LoadMapId(BuilderParameters.Relation2Id);

                knowledgeGraph.Train = knowledgeGraph.LoadGraph(BuilderParameters.TrainData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, true);
                knowledgeGraph.Valid = knowledgeGraph.LoadGraph(BuilderParameters.ValidData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
                knowledgeGraph.Test = knowledgeGraph.LoadGraph(BuilderParameters.TestData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
            }
        }
    }
}
