﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class Seq2SeqKnowledgeQABuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "MemNN Training set."));
                Argument.Add("DEV", new ParameterArgument(string.Empty, "MemNN Dev set."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "MemNN Valid set."));

                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));
                Argument.Add("VOCAB-SIZE", new ParameterArgument("50000", "Vocab Size"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("MAX-SRC-LEN", new ParameterArgument("1024", "max src length"));
                Argument.Add("MAX-TGT-LEN", new ParameterArgument("256", "max tgt length"));
                Argument.Add("MAX-REF-LEN", new ParameterArgument("256", "max ref length"));
                Argument.Add("TOP-K-REF", new ParameterArgument("5", "top K ref"));

                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));

                //Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "SRC Train Data."));
                //Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "TGT Train Data."));
                //Argument.Add("TRAIN-KNOW", new ParameterArgument(string.Empty, "Knowledge Train Data."));
                //Argument.Add("TRAIN-MAP", new ParameterArgument(string.Empty, "MAP Train Data."));

                //Argument.Add("DEV-SRC", new ParameterArgument(string.Empty, "SRC Dev Data."));
                //Argument.Add("DEV-TGT", new ParameterArgument(string.Empty, "TGT Dev Data."));
                //Argument.Add("DEV-KNOW", new ParameterArgument(string.Empty, "Knowledge Dev Data."));
                //Argument.Add("DEV-MAP", new ParameterArgument(string.Empty, "MAP Dev Data."));

                //Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                //Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));
                //Argument.Add("VALID-KNOW", new ParameterArgument(string.Empty, "Knowledge Valid Data."));
                //Argument.Add("VALID-MAP", new ParameterArgument(string.Empty, "MAP Valid Data."));

                // Word Embedding.
                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-WIN", new ParameterArgument("1", "DNN Layer WindowSize"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument(((int)A_Func.Linear).ToString(), "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("EMBED-LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding File"));

                // Src and Reference
                Argument.Add("TEXT-LSTM-DIM", new ParameterArgument("300", "Lstm Dimension"));;

                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Data.
            public static string TrainFile { get { return Argument["TRAIN"].Value; } }
            public static bool IsTrainFile { get { return (!TrainFile.Equals(string.Empty)); } }
            public static string TrainIndexFile { get { return string.Format("{0}.{1}.index", TrainFile, VocabSize); } }
            public static string TrainSrcData { get { return string.Format("{0}.{1}.{2}.src.bin", TrainFile, VocabSize, MiniBatchSize); } }
            public static string TrainTgtData { get { return string.Format("{0}.{1}.{2}.tgt.bin", TrainFile, VocabSize, MiniBatchSize); } }
            public static string TrainRefData { get { return string.Format("{0}.{1}.{2}.ref.bin", TrainFile, VocabSize, MiniBatchSize); } }
            public static string TrainMatchData { get { return string.Format("{0}.{1}.{2}.mat.bin", TrainFile, VocabSize, MiniBatchSize); } }

            public static string DevFile { get { return Argument["DEV"].Value; } }
            public static bool IsDevFile { get { return (!DevFile.Equals(string.Empty)); } }
            public static string DevIndexFile { get { return string.Format("{0}.{1}.index", DevFile, VocabSize); } }
            public static string DevSrcData { get { return string.Format("{0}.{1}.{2}.src.bin", DevFile, VocabSize, MiniBatchSize); } }
            public static string DevTgtData { get { return string.Format("{0}.{1}.{2}.tgt.bin", DevFile, VocabSize, MiniBatchSize); } }
            public static string DevRefData { get { return string.Format("{0}.{1}.{2}.ref.bin", DevFile, VocabSize, MiniBatchSize); } }
            public static string DevMatchData { get { return string.Format("{0}.{1}.{2}.mat.bin", DevFile, VocabSize, MiniBatchSize); } }

            public static string ValidFile { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return (!ValidFile.Equals(string.Empty)); } }
            public static string ValidIndexFile { get { return string.Format("{0}.{1}.index", ValidFile, VocabSize); } }
            public static string ValidSrcData { get { return string.Format("{0}.{1}.{2}.src.bin", ValidFile, VocabSize, MiniBatchSize); } }
            public static string ValidTgtData { get { return string.Format("{0}.{1}.{2}.tgt.bin", ValidFile, VocabSize, MiniBatchSize); } }
            public static string ValidRefData { get { return string.Format("{0}.{1}.{2}.ref.bin", ValidFile, VocabSize, MiniBatchSize); } }
            public static string ValidMatchData { get { return string.Format("{0}.{1}.{2}.mat.bin", ValidFile, VocabSize, MiniBatchSize); } }
            #endregion

            public static string Vocab { get { return Argument["VOCAB"].Value == string.Empty ? string.Format("{0}.{1}.vocab", TrainFile, VocabSize) : Argument["VOCAB"].Value; } }
            public static int VocabSize { get { return int.Parse(Argument["VOCAB-SIZE"].Value); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int MaxSrcLen { get { return int.Parse(Argument["MAX-TGT-LEN"].Value); } }
            public static int MaxTgtLen { get { return int.Parse(Argument["MAX-SRC-LEN"].Value); } }
            public static int MaxRefLen { get { return int.Parse(Argument["MAX-REF-LEN"].Value); } }
            public static int TopKRef { get { return int.Parse(Argument["TOP-K-REF"].Value); } }


            public static int BeginWordIndex { get { return 0; } }
            public static int TerminalWordIndex { get { return 0; } }
            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }


            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] EMBED_LAYER_WIN { get { return Argument["EMBED-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] EMBED_DROPOUT
            {
                get
                {
                    if (Argument["EMBED-LAYER-DROPOUT"].Value.Equals(string.Empty)) return EMBED_LAYER_DIM.Select(i => 0.0f).ToArray();
                    else return Argument["EMBED-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray();
                }
            }
            public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }

            public static int TEXT_LSTM_DIM { get { return int.Parse(Argument["TEXT-LSTM-DIM"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.SEQ2SEQ_KNOWLEDGE_QA; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> refData,
                                                             IDataCashier<BiMatchBatchData, BiMatchBatchDataStat> matchData,

                                                             // text embedding cnn.
                                                             LayerStructure WordEmbed,

                                                             // query encoder.
                                                             LSTMStructure SrcD1LSTM, LSTMStructure SrcD2LSTM,

                                                             // knowledge encoder.
                                                             LSTMStructure RefD1LSTM, LSTMStructure RefD2LSTM,

                                                             // MemNN encoder & decoder.
                                                             DNNStructure addressStruct, DNNStructure contentStruct,

                                                             // MLP O and C.
                                                             // DNNStructure InitOStruct, DNNStructure InitCStruct,
                                                             
                                                             // answer decoder.
                                                             LSTMStructure decoder, EmbedStructure embed,
                                                             
                                                             CompositeNNStructure Model,
                                                             // suppose no attention at this point.
                                                             // List<AttentionStructure> attentions, 
                                                             // mode and device id.
                                                             RunnerBehavior Behavior, EvaluationType evalType)
        {
            ComputationGraph cg = new ComputationGraph();

            /**************** Src, Tgt, Ref, Match Data  *********/
            SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));
            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));
            SeqSparseDataSource RefData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(refData, Behavior));
            BiMatchBatchData MatchData = (BiMatchBatchData)cg.AddDataRunner(new DataRunner<BiMatchBatchData, BiMatchBatchDataStat>(matchData, Behavior));

            /**************** Src Data Embedding *********/
            SeqDenseBatchData SrcWordEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(WordEmbed, SrcData.SequenceData, Behavior));
            
            SeqDenseRecursiveData SrcD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(SrcWordEmbed, false, Behavior));
            FastLSTMDenseRunner<SeqDenseRecursiveData> SrcD1LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(SrcD1LSTM.LSTMCells[0], SrcD1LstmInput, Behavior);
            SeqDenseRecursiveData SrcD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(SrcD1LSTMRunner);

            SeqDenseRecursiveData SrcD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(SrcWordEmbed, true, Behavior));
            FastLSTMDenseRunner<SeqDenseRecursiveData> SrcD2LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(SrcD2LSTM.LSTMCells[0], SrcD2LstmInput, Behavior);
            SeqDenseRecursiveData SrcD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(SrcD2LSTMRunner);

            HiddenBatchData SrcD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1LstmOutput, true, 0, SrcD1LstmOutput.MapForward, Behavior));
            HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2LstmOutput, false, 0, SrcD2LstmOutput.MapForward, Behavior));
            HiddenBatchData SrcO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1O, SrcD2O }, Behavior));

            HiddenBatchData SrcD1MatchO = (HiddenBatchData)cg.AddRunner(new MatrixExpansionRunner(SrcD1O, MatchData, 0, Behavior));
            HiddenBatchData SrcD2MatchO = (HiddenBatchData)cg.AddRunner(new MatrixExpansionRunner(SrcD2O, MatchData, 0, Behavior));

            /**************** Ref Data Embedding *********/
            SeqDenseBatchData RefWordEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(WordEmbed, RefData.SequenceData, Behavior));

            SeqDenseRecursiveData RefD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(RefWordEmbed, false, Behavior));
            FastLSTMDenseRunner<SeqDenseRecursiveData> RefD1LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(RefD1LSTM.LSTMCells[0], RefD1LstmInput, SrcD1MatchO, SrcD1MatchO, null, Behavior);
            SeqDenseRecursiveData RefD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(RefD1LSTMRunner);

            SeqDenseRecursiveData RefD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(RefWordEmbed, true, Behavior));
            FastLSTMDenseRunner<SeqDenseRecursiveData> RefD2LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(RefD2LSTM.LSTMCells[0], RefD2LstmInput, SrcD2MatchO, SrcD2MatchO, null, Behavior);
            SeqDenseRecursiveData RefD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(RefD2LSTMRunner);

            HiddenBatchData RefD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(RefD1LstmOutput, true, 0, RefD1LstmOutput.MapForward, Behavior));
            HiddenBatchData RefD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(RefD2LstmOutput, false, 0, RefD2LstmOutput.MapForward, Behavior));
            HiddenBatchData RefO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { RefD1O, RefD2O }, Behavior));

            /*********** Src to Ref data ****************/
            HiddenBatchData RefAddress = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(addressStruct, RefO, Behavior));
            HiddenBatchData RefContent = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(contentStruct, RefO, Behavior));
            HiddenBatchData RefAnswer = (HiddenBatchData)cg.AddRunner(new ExternalMemoryRunner(SrcO, new MemoryQAHelpData(MatchData, Behavior.Device), RefAddress, RefContent, Behavior));

            if (Behavior.RunMode == DNNRunMode.Train || evalType == EvaluationType.PERPLEXITY)
            {
                /*
                List<MLPAttentionV2Runner> tgtAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                    tgtAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i], null, BuilderParameters.TgtSeqMaxLen,
                        TgtData.SequenceData.Stat.MAX_BATCHSIZE, TgtData.SequenceData.Stat.MAX_SEQUENCESIZE, Behavior) : null);
                */

                /**************** Target LSTM Decoder *********/
                SeqDenseRecursiveData TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(decoder.LSTMCells[0], TgtData.SequenceData, false, RefAnswer, RefAnswer, null, Behavior));
                for (int i = 1; i < decoder.LSTMCells.Count; i++)
                    TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(decoder.LSTMCells[i], TgtlstmOutput, RefAnswer, RefAnswer, null, Behavior));

                if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                    cg.AddRunner(new ModelDiskDumpRunner(Model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\MemSeq2Seq"));

                if (Behavior.RunMode == DNNRunMode.Train)
                    cg.AddObjective(new EmbedFullySoftmaxRunner(embed, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
                else
                    cg.AddRunner(new EmbedFullySoftmaxRunner(embed, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath, Behavior));
            }
            else
            {
                /*
                List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                    decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i], null, TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);
                */

                List<Tuple<HiddenBatchData, HiddenBatchData>> InitStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
                InitStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(RefAnswer, RefAnswer));
                // the probability of exactly generate the true tgt.
                LSTMDecodingRunner decodeRunner = new LSTMDecodingRunner(decoder, embed, InitStatus, null, Behavior, true);

                EnsembleBeamSearchRunner beamSearchRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
                    BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, embed.VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);

                beamSearchRunner.AddBeamSearch(decodeRunner);
                cg.AddRunner(beamSearchRunner);

                switch (evalType)
                {
                    case EvaluationType.ACCURACY:
                        cg.AddRunner(new BeamAccuracyRunner(beamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                        break;
                    case EvaluationType.BSBLEU:
                        cg.AddRunner(new BLEUDiskDumpRunner(beamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                        break;
                    case EvaluationType.ROUGE:
                        cg.AddRunner(new ROUGEDiskDumpRunner(beamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                        break;
                    case EvaluationType.TOPKBEAM:
                        cg.AddRunner(new TopKBeamRunner(beamSearchRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                        break;
                }
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            //ConvStructure WordEmbed,
            //                                                 // query encoder.
            //                                                 LSTMStructure SrcD1LSTM, LSTMStructure SrcD2LSTM,
            //                                                 // knowledge encoder.
            //                                                 LSTMStructure RefD1LSTM, LSTMStructure RefD2LSTM,

            //                                                 // MemNN encoder & decoder.
            //                                                 DNNStructure addressStruct, DNNStructure contentStruct,

            //                                                 // MLP O and C.
            //                                                 DNNStructure InitOStruct, DNNStructure InitCStruct,

            //                                                 // answer decoder.
            //                                                 LSTMStructure decoder, EmbedStructure embed,

            //                                                 CompositeNNStructure Model,
            //                                                 // suppose no attention at this point.
            //                                                 // List<AttentionStructure> attentions, 
            //                                                 // mode and device id.
            //                                                 RunnerBehavior Behavior, EvaluationType evalType)


            int aDirectLSTM = 1;
            int bDirectLSTM = 1;

            int ensemble = aDirectLSTM + bDirectLSTM;
            CompositeNNStructure modelStructure = new CompositeNNStructure();

            /// word embedding layer.
            LayerStructure WordEmbed = null;

            LSTMStructure SrcD1LSTM = null;
            LSTMStructure SrcD2LSTM = null;

            LSTMStructure RefD1LSTM = null;
            LSTMStructure RefD2LSTM = null;

            DNNStructure AddressStruct = null;
            DNNStructure ContentStruct = null;

            //DNNStructure InitOStruct = null;
            //DNNStructure InitCStruct = null;

            LSTMStructure TgtLSTM = null;
            EmbedStructure DecodeEmbed = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                Logger.WriteLog("Init Word Embedding Encoder.");

                WordEmbed = (new LayerStructure(DataPanel.WordDim, BuilderParameters.EMBED_LAYER_DIM[0],
                        BuilderParameters.EMBED_ACTIVATION[0], N_Type.Convolution_layer, BuilderParameters.EMBED_LAYER_WIN[0],
                        BuilderParameters.EMBED_DROPOUT[0], false, device));
                #region Word Embedding.
                int embedDim = BuilderParameters.EMBED_LAYER_DIM[0];

                if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
                {
                    int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];
                    int halfWinStart = BuilderParameters.EMBED_LAYER_WIN[0] / 2; 
                    Logger.WriteLog("Init Glove Word Embedding ...");
                    int hit = 0;
                    using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string[] items = mreader.ReadLine().Split(' ');
                            string word = items[0];
                            float[] vec = new float[items.Length - 1];
                            for (int i = 1; i < items.Length; i++)
                            {
                                vec[i - 1] = float.Parse(items[i]);
                            }

                            int wordIdx = DataPanel.WordFreqDict.IndexItem(word);
                            if (wordIdx >= 0)
                            {
                                int feaIdx = 2 + wordIdx;
                                for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                                {
                                    for (int i = 0; i < wordDim; i++)
                                    {
                                        WordEmbed.weight.MemPtr[((win + halfWinStart) * DataPanel.WordDim + feaIdx) * wordDim + i] = vec[i];
                                    }
                                }
                                hit++;
                            }
                        }
                    }
                    WordEmbed.weight.SyncFromCPU();
                    Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.WordFreqDict.ItemDictSize);
                }
                #endregion.

                Logger.WriteLog("Init Src LSTM.");
                // Src Encoder.
                SrcD1LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.TEXT_LSTM_DIM }, DeviceType.GPU);
                SrcD2LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.TEXT_LSTM_DIM }, DeviceType.GPU);

                Logger.WriteLog("Init Ref LSTM.");
                RefD1LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.TEXT_LSTM_DIM }, DeviceType.GPU);
                RefD2LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.TEXT_LSTM_DIM }, DeviceType.GPU);

                Logger.WriteLog("Init Address Encoder.");
                // Address of the knowledge sentence.
                AddressStruct = new DNNStructure(BuilderParameters.TEXT_LSTM_DIM * ensemble,
                                                new int[] { BuilderParameters.TEXT_LSTM_DIM * ensemble },
                                                new A_Func[] { A_Func.Tanh },
                                                new bool[] { false });

                Logger.WriteLog("Init Content Encoder.");
                // Content of the knowledge sentence.
                ContentStruct = new DNNStructure(BuilderParameters.TEXT_LSTM_DIM * ensemble,
                                                new int[] { BuilderParameters.TEXT_LSTM_DIM * ensemble },
                                                new A_Func[] { A_Func.Tanh },
                                                new bool[] { false });

                Logger.WriteLog("Init Target LSTM.");
                TgtLSTM = new LSTMStructure(DataPanel.WordDim, new int[] { BuilderParameters.TEXT_LSTM_DIM  * ensemble}, DeviceType.GPU);

                Logger.WriteLog("Init Target Word Decoder");
                // Answer word embedding structure.
                DecodeEmbed = new EmbedStructure(DataPanel.WordDim, BuilderParameters.TEXT_LSTM_DIM * ensemble, DeviceType.GPU);

                //// from the knowledge content to initO.
                //initOStruct = new DNNStructure(BuilderParameters.LAYER_DIM.Last(),
                //                               new int[] { BuilderParameters.LAYER_DIM.Last() },
                //                               new A_Func[] { A_Func.Tanh },
                //                               new bool[] { true });
                //// from the knowledge content to initC.
                //initCStruct = new DNNStructure(BuilderParameters.LAYER_DIM.Last(),
                //                               new int[] { BuilderParameters.LAYER_DIM.Last() },
                //                               new A_Func[] { A_Func.Tanh },
                //                               new bool[] { true });

                modelStructure.AddLayer(WordEmbed);
                modelStructure.AddLayer(SrcD1LSTM);
                modelStructure.AddLayer(SrcD2LSTM);
                modelStructure.AddLayer(RefD1LSTM);
                modelStructure.AddLayer(RefD2LSTM);
                modelStructure.AddLayer(AddressStruct);
                modelStructure.AddLayer(ContentStruct);
                //modelStructure.AddLayer(initOStruct);
                //modelStructure.AddLayer(initCStruct);
                modelStructure.AddLayer(TgtLSTM);
                modelStructure.AddLayer(DecodeEmbed);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);

                    WordEmbed = (LayerStructure)modelStructure.CompositeLinks[0];
                    SrcD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[1];
                    SrcD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[2];
                    RefD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[3];
                    RefD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[4];
                    AddressStruct = (DNNStructure)modelStructure.CompositeLinks[5];
                    ContentStruct = (DNNStructure)modelStructure.CompositeLinks[6];
                    //initOStruct = (DNNStructure)modelStructure.CompositeLinks[5];
                    //initCStruct = (DNNStructure)modelStructure.CompositeLinks[6];
                    TgtLSTM = (LSTMStructure)modelStructure.CompositeLinks[7];
                    DecodeEmbed = (EmbedStructure)modelStructure.CompositeLinks[8];
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    
                    //modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin, DataPanel.TrainRefBin, DataPanel.TrainMatchBin,
                        WordEmbed, SrcD1LSTM, SrcD2LSTM, RefD1LSTM, RefD2LSTM, AddressStruct, ContentStruct, // initOStruct, initCStruct, 
                        TgtLSTM, DecodeEmbed, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, EvaluationType.PERPLEXITY);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                    {
                        validCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, DataPanel.ValidRefBin, DataPanel.ValidMatchBin,
                            WordEmbed, SrcD1LSTM, SrcD2LSTM, RefD1LSTM, RefD2LSTM, AddressStruct, ContentStruct, // initOStruct, initCStruct, 
                            TgtLSTM, DecodeEmbed, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, EvaluationType.TOPKBEAM);
                    }

                    ComputationGraph devCG = null;
                    if(BuilderParameters.IsDevFile)
                    {
                        devCG = BuildComputationGraph(DataPanel.DevSrcBin, DataPanel.DevTgtBin, DataPanel.DevRefBin, DataPanel.DevMatchBin,
                            WordEmbed, SrcD1LSTM, SrcD2LSTM, RefD1LSTM, RefD2LSTM, AddressStruct, ContentStruct, 
                            TgtLSTM, DecodeEmbed, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, EvaluationType.PERPLEXITY);
                    }

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    double bestDevScore = double.MaxValue;
                    double bestDevIter = -1;
                    double bestDevValidScore = 0;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        string modelName = string.Format(@"{0}\\MemSeq2Seq.{1}.model", BuilderParameters.ModelOutputPath, iter);

                        using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                        {
                            modelStructure.Serialize(writer);
                        }

                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }

                        if (BuilderParameters.IsDevFile)
                        {
                            double devScore = devCG.Execute();
                            if (devScore < bestDevScore)
                            {
                                bestDevScore = devScore;
                                bestDevIter = iter;
                                bestDevValidScore = validScore;
                            }
                            Logger.WriteLog("Best Dev Score {0} at iteration {1}  Valid Score {2}", bestDevScore, bestDevIter, bestDevValidScore);
                        }

                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, DataPanel.ValidRefBin, DataPanel.ValidMatchBin,
                            WordEmbed, SrcD1LSTM, SrcD2LSTM, RefD1LSTM, RefD2LSTM, AddressStruct, ContentStruct, // initOStruct, initCStruct, 
                            TgtLSTM, DecodeEmbed, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, EvaluationType.TOPKBEAM);

                    double predScore = predCG.Execute();
                    Logger.WriteLog("Prediction Score {0}", predScore);
                    break;
            }
            Logger.CloseLog();

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {

            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainRefBin = null;
            public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> TrainMatchBin = null;

            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidRefBin = null;
            public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> ValidMatchBin = null;

            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevTgtBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevRefBin = null;
            public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> DevMatchBin = null;

            public static ItemFreqIndexDictionary WordFreqDict = null;

            public static int WordDim { get { return WordFreqDict.ItemDictSize + 2; } }

            static void ExtractVocab(string s2sRefFile, string vocab)
            {
                WordFreqDict = new ItemFreqIndexDictionary("WordVocab");

                Console.WriteLine("Extracting Vocab from File {0}", s2sRefFile);

                int lineIdx = 0;
                using (StreamReader mreader = new StreamReader(s2sRefFile))
                {
                    while (!mreader.EndOfStream)
                    {
                        string line = mreader.ReadLine();
                        string[] items = line.Split('\t');
                        string query = items[0];
                        string answer = items[1];
                        string[] refs = items[2].Split(new string[] { "<sep>" }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if(TextUtil.IsEnglishNumber(word)) WordFreqDict.PushItem(word);
                        }

                        foreach (string word in answer.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (TextUtil.IsEnglishNumber(word)) WordFreqDict.PushItem(word);
                        }

                        foreach(string refstr in refs)
                        {
                            foreach (string word in refstr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                if (TextUtil.IsEnglishNumber(word)) WordFreqDict.PushItem(word);
                            }
                        }
                        if (++lineIdx % 1000 == 0) Console.WriteLine("Extract Vocab from File {0}", lineIdx++);
                    }
                    
                    // { }
                }

                /// keep top 50K vocabary.
                WordFreqDict.Filter(0, BuilderParameters.VocabSize);
                using (StreamWriter mwriter = new StreamWriter(vocab))
                {
                    WordFreqDict.Save(mwriter);
                }
            }

            /// <summary>
            /// Begin/End = 0;
            /// Unknown = 1;
            /// </summary>
            static void ExtractCorpusIndex(string s2sRefFile, string srsRefIndexFile, bool fillEmpty = false)
            {
                Console.WriteLine("Extract Index from File {0}", srsRefIndexFile);
                int lineIdx = 0;
                using (StreamWriter indexWriter = new StreamWriter(srsRefIndexFile))
                {
                    using (StreamReader mreader = new StreamReader(s2sRefFile))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string line = mreader.ReadLine();
                            string[] items = line.Split('\t');
                            string query = items[0];
                            string answer = items[1];
                            string[] refs = items[2].Split(new string[] { "<sep>" }, StringSplitOptions.RemoveEmptyEntries);

                            List<int> queryIndex = new List<int>();
                            foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                int idx = WordFreqDict.IndexItem(word);
                                if(idx >= 0) queryIndex.Add(idx >= 0 ? idx + 2 : 1);
                            }
                            if (fillEmpty && queryIndex.Count == 0) queryIndex.Add(0);

                            List<int> answerIndex = new List<int>();
                            foreach (string word in answer.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                int idx = WordFreqDict.IndexItem(word);
                                if (idx >= 0) answerIndex.Add(idx >= 0 ? idx + 2 : 1);
                            }
                            if (fillEmpty && answerIndex.Count == 0) answerIndex.Add(0);

                            List<string> refsIndex = new List<string>();
                            foreach (string refstr in refs)
                            {
                                List<int> refIndex = new List<int>();
                                foreach (string word in refstr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int idx = WordFreqDict.IndexItem(word);
                                    if (idx >= 0) refIndex.Add(idx >= 0 ? idx + 2 : 1);
                                }
                                refsIndex.Add(string.Join(" ", refIndex));
                            }

                            if (fillEmpty && refsIndex.Count == 0) refsIndex.Add("0");

                            indexWriter.WriteLine(string.Join(" ", queryIndex) + "\t" + string.Join(" ", answerIndex) + "\t" + string.Join("#", refsIndex));
                            if (++lineIdx % 10000 == 0) { Console.WriteLine("Extract Index from File {0}", lineIdx); }
                        }
                    }
                }
            }

            static void ExtractCorpusBinary(string corpus, string srcbin, string tgtbin, string refbin, string matchbin)
            {
                // Input Source Sequence.
                BinaryWriter srcBatchDataWriter = new BinaryWriter(new FileStream(srcbin, FileMode.Create, FileAccess.Write));
                SeqSparseDataSource srcBatchData = new SeqSparseDataSource(new SequenceDataStat() { FEATURE_DIM = WordFreqDict.ItemDictSize + 2 }, DeviceType.CPU);
                
                // Input Target Sequence.
                BinaryWriter tgtBatchDataWriter = new BinaryWriter(new FileStream(tgtbin, FileMode.Create, FileAccess.Write));
                SeqSparseDataSource tgtBatchData = new SeqSparseDataSource(new SequenceDataStat() { FEATURE_DIM = WordFreqDict.ItemDictSize + 2 }, DeviceType.CPU);
                
                // Input Ref Sequence.
                BinaryWriter refBatchDataWriter = new BinaryWriter(new FileStream(refbin, FileMode.Create, FileAccess.Write));
                SeqSparseDataSource refBatchData = new SeqSparseDataSource(new SequenceDataStat() { FEATURE_DIM = WordFreqDict.ItemDictSize + 2 }, DeviceType.CPU);

                // Input Mapping Info.
                BinaryWriter matchDataWriter = new BinaryWriter(new FileStream(matchbin, FileMode.Create, FileAccess.Write));
                BiMatchBatchData matchData = new BiMatchBatchData(new BiMatchBatchDataStat(), DeviceType.CPU);
                List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();

                int lineIdx = 0;
                using (StreamReader reader = new StreamReader(corpus))
                {
                    while(!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        string[] items = line.Split(new string[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);
                        if(items.Length != 3)
                        {
                            Console.WriteLine("Skip line {0}", lineIdx);
                            continue;
                        }
                        string[] srcItems = items[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] tgtItems = items[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] refItemList = items[2].Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);

                        List<List<int>> refnItemInts = new List<List<int>>();
                        foreach (string con in refItemList)
                        {
                            string[] refItems = con.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if(refItems.Count() > 0) refnItemInts.Add(refItems.Select(i => int.Parse(i)).ToList());
                        }

                        Tuple<List<int>, List<int>, List<List<int>>> smp = new Tuple<List<int>, List<int>, List<List<int>>>(
                            srcItems.Select(i => int.Parse(i)).ToList(),
                            tgtItems.Select(i => int.Parse(i)).ToList(),
                            refnItemInts
                            );

                        if (smp.Item1.Count == 0 || smp.Item2.Count == 0 || smp.Item3.Count == 0)
                        {
                            Console.WriteLine("Skip Empty Pairs {0}!!", lineIdx);
                            continue;
                        }

                        //if ((smp.Item1.Count >= BuilderParameters.MaxSrcLen && BuilderParameters.MaxSrcLen > 0)
                        //    || (smp.Item2.Count >= BuilderParameters.MaxTgtLen && BuilderParameters.MaxTgtLen > 0))
                        //{
                        //    continue;
                        //}

                        List<Tuple<Dictionary<int, float>, float>> srcFea = new List<Tuple<Dictionary<int, float>, float>>();
                        for (int sIdx = 0; sIdx < smp.Item1.Count; sIdx++)
                        {
                            int srcId = smp.Item1[sIdx];
                            Dictionary<int, float> d = new Dictionary<int, float>();
                            d.Add(srcId, 1);
                            srcFea.Add(new Tuple<Dictionary<int, float>, float>(d, -1));

                            if (sIdx >= BuilderParameters.MaxSrcLen - 1) break;
                        }
                        srcBatchData.PushSample(srcFea);

                        List<Tuple<Dictionary<int, float>, float>> tgtFea = new List<Tuple<Dictionary<int, float>, float>>();
                        Dictionary<int, float> bgn = new Dictionary<int, float>();
                        bgn.Add(0, 1);
                        tgtFea.Add(new Tuple<Dictionary<int, float>, float>(bgn, smp.Item2.First()));
                        for (int sIdx = 0; sIdx < smp.Item2.Count - 1; sIdx++)
                        {
                            int tgtId = smp.Item2[sIdx];
                            Dictionary<int, float> d = new Dictionary<int, float>();
                            d.Add(tgtId, 1);
                            tgtFea.Add(new Tuple<Dictionary<int, float>, float>(d, smp.Item2[sIdx + 1]));
                            if (sIdx >= BuilderParameters.MaxTgtLen - 2) break;
                        }
                        Dictionary<int, float> end = new Dictionary<int, float>();
                        end.Add(smp.Item2.Last(), 1);
                        tgtFea.Add(new Tuple<Dictionary<int, float>, float>(end, 0));
                        tgtBatchData.PushSample(tgtFea);


                        for (int i = 0; i < smp.Item3.Count; i++)
                        {
                            List<Tuple<Dictionary<int, float>, float>> refFea = new List<Tuple<Dictionary<int, float>, float>>();
                            for (int cIdx = 0; cIdx < smp.Item3[i].Count; cIdx++)
                            {
                                int conId = smp.Item3[i][cIdx];
                                Dictionary<int, float> d = new Dictionary<int, float>();
                                d.Add(conId, 1);
                                refFea.Add(new Tuple<Dictionary<int, float>, float>(d, -1));

                                if (cIdx >= BuilderParameters.MaxRefLen - 1) break;
                            }
                            refBatchData.PushSample(refFea);
                            matchList.Add(new Tuple<int, int, float>(srcBatchData.BatchSize - 1, refBatchData.BatchSize - 1, 1));

                            if (i >= BuilderParameters.TopKRef - 1) break;
                        }

                        if (srcBatchData.BatchSize >= BuilderParameters.MiniBatchSize ||
                            tgtBatchData.BatchSize >= BuilderParameters.MiniBatchSize)
                        {
                            srcBatchData.PopBatchToStat(srcBatchDataWriter);
                            tgtBatchData.PopBatchToStat(tgtBatchDataWriter);
                            refBatchData.PopBatchToStat(refBatchDataWriter);

                            matchData.SetMatch(matchList);
                            matchData.PopBatchToStat(matchDataWriter);
                            matchList.Clear();
                        }

                        if (++lineIdx % 10000 == 0) { Console.WriteLine("Extract Binary from File {0}", lineIdx); }
                    }
                }
                Console.WriteLine("Completed! Saving Statistic...");
                srcBatchData.PopBatchCompleteStat(srcBatchDataWriter);
                tgtBatchData.PopBatchCompleteStat(tgtBatchDataWriter);
                refBatchData.PopBatchCompleteStat(refBatchDataWriter);

                if (matchList.Count > 0) matchData.SetMatch(matchList);
                matchData.PopBatchCompleteStat(matchDataWriter);
                Console.WriteLine("Done!");

                Console.WriteLine("Source Data Stat : {0}", srcBatchData.Stat.ToString());
                Console.WriteLine("Target Data Stat : {0}", tgtBatchData.Stat.ToString());
                Console.WriteLine("Ref Data Stat : {0}", refBatchData.Stat.ToString());
                Console.WriteLine("Match Data Stat : {0}", matchData.Stat.ToString());
            }

            public static void Init()
            {
                /// Step 1 : Extract Vocab from training Corpus.
                if (!File.Exists(BuilderParameters.Vocab)) ExtractVocab(BuilderParameters.TrainFile, BuilderParameters.Vocab);
                else
                {
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab)) WordFreqDict = new ItemFreqIndexDictionary(mreader);
                }

                /// Step 2 : Corpus 2 Index.
                if (BuilderParameters.IsTrainFile && !File.Exists(BuilderParameters.TrainIndexFile))
                    ExtractCorpusIndex(BuilderParameters.TrainFile, BuilderParameters.TrainIndexFile, false);
                if (BuilderParameters.IsDevFile && !File.Exists(BuilderParameters.DevIndexFile))
                    ExtractCorpusIndex(BuilderParameters.DevFile, BuilderParameters.DevIndexFile, true);
                if (BuilderParameters.IsValidFile && !File.Exists(BuilderParameters.ValidIndexFile))
                    ExtractCorpusIndex(BuilderParameters.ValidFile, BuilderParameters.ValidIndexFile, true);

                /// Step 3 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile &&
                    (!File.Exists(BuilderParameters.TrainSrcData) || !File.Exists(BuilderParameters.TrainTgtData) || !File.Exists(BuilderParameters.TrainRefData)))
                {
                    ExtractCorpusBinary(BuilderParameters.TrainIndexFile, BuilderParameters.TrainSrcData, BuilderParameters.TrainTgtData, BuilderParameters.TrainRefData, BuilderParameters.TrainMatchData);
                }
                if (BuilderParameters.IsDevFile &&
                    (!File.Exists(BuilderParameters.DevSrcData) || !File.Exists(BuilderParameters.DevTgtData) || !File.Exists(BuilderParameters.DevRefData)))
                {
                    ExtractCorpusBinary(BuilderParameters.DevIndexFile, BuilderParameters.DevSrcData, BuilderParameters.DevTgtData, BuilderParameters.DevRefData, BuilderParameters.DevMatchData);
                }
                if (BuilderParameters.IsValidFile &&
                    (!File.Exists(BuilderParameters.ValidSrcData) || !File.Exists(BuilderParameters.ValidTgtData) || !File.Exists(BuilderParameters.ValidRefData)))
                {
                    ExtractCorpusBinary(BuilderParameters.ValidIndexFile, BuilderParameters.ValidSrcData, BuilderParameters.ValidTgtData, BuilderParameters.ValidRefData, BuilderParameters.ValidMatchData);
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);
                    ValidRefBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidRefData);
                    ValidMatchBin = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>(BuilderParameters.ValidMatchData);

                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                    ValidRefBin.InitThreadSafePipelineCashier(100, false);
                    ValidMatchBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.IsDevFile)
                {
                    DevSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevSrcData);
                    DevTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevTgtData);
                    DevRefBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevRefData);
                    DevMatchBin = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>(BuilderParameters.DevMatchData);

                    DevSrcBin.InitThreadSafePipelineCashier(100, false);
                    DevTgtBin.InitThreadSafePipelineCashier(100, false);
                    DevRefBin.InitThreadSafePipelineCashier(100, false);
                    DevMatchBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainSrcData);
                    TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainTgtData);
                    TrainRefBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainRefData);
                    TrainMatchBin = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>(BuilderParameters.TrainMatchData);

                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainRefBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainMatchBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }
            }
        }
    }
}