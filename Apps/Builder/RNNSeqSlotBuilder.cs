﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class RNNSeqSlotBuilderParameters : BaseModelArgument<RNNSeqSlotBuilderParameters>
    {
        static RNNSeqSlotBuilderParameters()
        {
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
            Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.MSE).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
            Argument.Add("LAYER-LAG", new ParameterArgument("5", "DNN Layer Dim"));
            Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
            Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));

            Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.Regression).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
            Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
            Argument.Add("LABEL-MIN", new ParameterArgument("0", "Label Start with 0 by default;"));
            Argument.Add("LABEL-MAX", new ParameterArgument("0", "Label End with 5 by default;"));

            Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
            Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

            Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
            Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
        }

        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
        public static string TrainData { get { return Argument["TRAIN"].Value; } }
        public static string ValidData { get { return Argument["VALID"].Value; } }
        public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
        public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
        public static int[] LAYER_LAG { get { return Argument["LAYER-LAG"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
        public static float LabelMinValue { get { return float.Parse(Argument["LABEL-MIN"].Value); } }
        public static float LabelMaxValue { get { return float.Parse(Argument["LABEL-MAX"].Value); } }
        public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }

        public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
        public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

        public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }
    }

    public class RNNSeqSlotBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.RNNSEQSLOT; } }
        public override void InitStartup(string fileName)
        {
            RNNSeqSlotBuilderParameters.Parse(fileName);
            Cudalib.CudaInit(RNNSeqSlotBuilderParameters.GPUID);
        }

        public override void Rock()
        {
            Logger.OpenLog(RNNSeqSlotBuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            switch (RNNSeqSlotBuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    // LossDerivParameter lossDerivParameter = new LossDerivParameter()
                    // {
                    //     InputDataType = DataSourceID.SequenceSlotData,
                    //     OutputDataType = DataSourceID.SeqDenseBatchData,
                    //     lossType = RNNSeqSlotBuilderParameters.LossFunction,
                    //     Gamma = RNNSeqSlotBuilderParameters.Gamma,
                    //     LabelMinValue = RNNSeqSlotBuilderParameters.LabelMinValue,
                    //     LabelMaxValue = RNNSeqSlotBuilderParameters.LabelMaxValue
                    // };

                    RNNStructure rnnModel = new RNNStructure(DataPanel.Train.Stat.FEATURE_DIM, RNNSeqSlotBuilderParameters.LAYER_DIM,
                                                            RNNSeqSlotBuilderParameters.LAYER_LAG, RNNSeqSlotBuilderParameters.ACTIVATION, RNNSeqSlotBuilderParameters.LAYER_BIAS);

                    //DataPanel.Train,
                    RNNSeqSlotRunner<SeqSparseSlotDataSource> trainer = new RNNSeqSlotRunner<SeqSparseSlotDataSource>(rnnModel, new RunnerBehavior() { RunMode = DNNRunMode.Train });
                    //LossDerivParameter LossParameter = lossDerivParameter;

                    //if (RNNBuilderParameters.IsValidFile) evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                    //if (RNNBuilderParameters.IsValidFile) evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);

                    rnnModel.InitOptimizer(new StructureLearner()
                    {
                        LearnRate = RNNSeqSlotBuilderParameters.LearnRate,
                        Optimizer = RNNSeqSlotBuilderParameters.Optimizer,
                        AdaBoost = RNNSeqSlotBuilderParameters.AdaBoost,
                        ShrinkLR = RNNSeqSlotBuilderParameters.ShrinkLR,
                        BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                        EpochNum = RNNSeqSlotBuilderParameters.Iteration
                    });

                    if (!Directory.Exists(RNNSeqSlotBuilderParameters.ModelOutputPath)) Directory.CreateDirectory(RNNSeqSlotBuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < RNNSeqSlotBuilderParameters.Iteration; iter++)
                    {
                        double loss = trainer.Validation(DataPanel.Train.GetInstances(false));
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss / (DataPanel.Train.Stat.TotalBatchNumber + float.Epsilon));
                        rnnModel.Save(string.Format(@"{0}\\RNN.{1}.model", RNNSeqSlotBuilderParameters.ModelOutputPath, iter.ToString()));
                    }
                    trainer.Dispose();
                    break;
            }

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseSlotDataSource, SequenceDataStat> Train = null;

            public static void Init()
            {
                if (RNNSeqSlotBuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseSlotDataSource, SequenceDataStat>(RNNSeqSlotBuilderParameters.TrainData);
                    Train.PreL2();
                }

            }

            internal static void DeInit()
            {
                if (null != Train)
                {
                    Train.Dispose();
                    Train = null;
                }
            }
        }
    }
}
