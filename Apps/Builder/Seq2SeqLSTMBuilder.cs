﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    //public class Seq2SeqEmbedLSTMBuilder : Builder
    //{
    //    class BuilderParameters : BaseModelArgument<BuilderParameters>
    //    {
    //        static BuilderParameters()
    //        {
    //            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
    //            Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

    //            Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "SRC Train Data."));
    //            Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "TGT Train Data."));
    //            Argument.Add("TRAIN-MAP", new ParameterArgument(string.Empty, "MAP Train Data."));

    //            Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
    //            Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));
    //            Argument.Add("VALID-MAP", new ParameterArgument(string.Empty, "MAP Valid Data."));

    //            Argument.Add("DEV-SRC", new ParameterArgument(string.Empty, "SRC Dev Dataset."));
    //            Argument.Add("DEV-TGT", new ParameterArgument(string.Empty, "TGT Dev Dataset."));
    //            Argument.Add("DEV-MAP", new ParameterArgument(string.Empty, "MAP Dev Dataset."));

    //            Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

    //            ///Language Word Error Rate.
    //            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

    //            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
    //            Argument.Add("LAYER-ATTENTION", new ParameterArgument("1", "Layer Attention"));

    //            Argument.Add("SRC-EMBED", new ParameterArgument("620", "Src String Dim"));
    //            Argument.Add("TGT-EMBED", new ParameterArgument("500", "Tgt String Dim"));
    //            Argument.Add("ATT-HIDDEN", new ParameterArgument("1000", "Attention Layer Hidden Dim"));

    //            ///Softmax Randomup.
    //            Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
    //            Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
    //            Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
    //            Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

    //            Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
    //            Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
    //            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
    //            Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
    //        }

    //        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
    //        public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
    //        //(DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
    //        public static string TrainSrcData { get { return Argument["TRAIN-SRC"].Value; } }
    //        public static string TrainTgtData { get { return Argument["TRAIN-TGT"].Value; } }
    //        public static string TrainMapData { get { return Argument["TRAIN-MAP"].Value; } }
    //        public static bool IsTrainFile { get { return (!TrainSrcData.Equals(string.Empty)) && (!TrainTgtData.Equals(string.Empty)); } }

    //        public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
    //        public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
    //        public static string ValidMapData { get { return Argument["VALID-MAP"].Value; } }
    //        public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

    //        public static string DevSrcData { get { return Argument["DEV-SRC"].Value; } }
    //        public static string DevTgtData { get { return Argument["DEV-TGT"].Value; } }
    //        public static string DevMapData { get { return Argument["DEV-MAP"].Value; } }
    //        public static bool IsDevFile { get { return (!DevSrcData.Equals(string.Empty)) && (!DevTgtData.Equals(string.Empty)); } }

    //        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
    //        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static bool[] LAYER_ATTENTION { get { return Argument["LAYER-ATTENTION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }

    //        public static int Src_Embed { get { return int.Parse(Argument["SRC-EMBED"].Value); } }
    //        public static int Tgt_Embed { get { return int.Parse(Argument["TGT-EMBED"].Value); } }

    //        public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

    //        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

    //        public static string Vocab { get { return Argument["VOCAB"].Value; } }
    //        public static Vocab2Freq mVocabDict = null;
    //        public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
    //        public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
    //        public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

    //        public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
    //        public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
    //        public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }


    //        public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
    //        public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
    //        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
    //        public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
    //    }

    //    public override BuilderType Type { get { return BuilderType.SEQ2SEQ_EMBED_LSTM; } }

    //    public override void InitStartup(string fileName)
    //    {
    //        BuilderParameters.Parse(fileName);
    //        MathOperatorManager.SetDevice(BuilderParameters.GPUID);
    //    }

    //    public static List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> LSTMEncoder(ComputationGraph cg, LSTMStructure lstmModel,
    //        SeqDenseRecursiveData input, RunnerBehavior behavior)
    //    {
    //        List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> memory = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
    //        SeqDenseRecursiveData Input = input;
    //        /***************** LSTM Encoder ******************************/
    //        for (int i = 0; i < lstmModel.LSTMCells.Count; i++)
    //        {
    //            FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], Input, behavior);
    //            cg.AddRunner(encoderRunner);
    //            memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(encoderRunner.Output, encoderRunner.C));
    //        }
    //        return memory;
    //    }

    //    public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
    //                                                         IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
    //                                                         IDataCashier<DenseBatchData, DenseDataStat> mapData,

    //                                                         EmbedStructure embedSrc, EmbedStructure embedTgt,
    //                                                         //LayerStructure transO, LayerStructure transC,
    //                                                         LSTMStructure srcD1LSTM, LSTMStructure srcD2LSTM, LSTMStructure decoder,
    //                                                         List<MLPAttentionStructure> attentions, EmbedStructure decodeEmbedTgt,
    //                                                         DNNRunMode mode, CompositeNNStructure Model, int deviceID, EvaluationType evalType)
    //    {
    //        ComputationGraph cg = new ComputationGraph() { StatusReportSteps = mode == DNNRunMode.Train ? 50 : 500 };

    //        DeviceType device = MathOperatorManager.SetDevice(deviceID);
    //        CudaMathOperation lib = new CudaMathOperation(true, true);
    //        RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };

    //        /**************** Get Source String Data from DataCashier *********/
    //        SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));
    //        DenseBatchData MapData = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(mapData, Behavior));
    //        SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));


    //        SeqDenseRecursiveData SrcEmbedD1 = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData.SequenceData, false, Behavior));
    //        SeqDenseRecursiveData SrcEmbedD2 = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData.SequenceData, true, Behavior));

    //        List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD1Memory = LSTMEncoder(cg, srcD1LSTM, SrcEmbedD1, Behavior);
    //        List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD2Memory = LSTMEncoder(cg, srcD2LSTM, SrcEmbedD2, Behavior);

    //        List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcEnsembleMemory = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
    //        List<Tuple<HiddenBatchData, HiddenBatchData>> SrcEnsembleStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

    //        for (int i = 0; i < SrcD1Memory.Count; i++)
    //        {
    //            HiddenBatchData SrcD1_O = (HiddenBatchData)cg.AddRunner(new Transform_TransSeqMatrix2MatrixRunner(SrcD1Memory[i].Item1, Behavior));
    //            HiddenBatchData SrcD1_C = (HiddenBatchData)cg.AddRunner(new Transform_TransSeqMatrix2MatrixRunner(SrcD1Memory[i].Item2, Behavior));

    //            HiddenBatchData SrcD2_O = (HiddenBatchData)cg.AddRunner(new Transform_TransSeqMatrix2MatrixRunner(SrcD2Memory[i].Item1, Behavior));
    //            HiddenBatchData SrcD2_C = (HiddenBatchData)cg.AddRunner(new Transform_TransSeqMatrix2MatrixRunner(SrcD2Memory[i].Item2, Behavior));

    //            HiddenBatchData SrcEnsemble_O = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1_O, SrcD2_O }, Behavior));
    //            HiddenBatchData SrcEnsemble_C = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1_C, SrcD2_C }, Behavior));

    //            SeqDenseRecursiveData SrcEnsemble_OSeq = (SeqDenseRecursiveData)cg.AddRunner(new Transform_Matrix2TransSeqMatrixRunner(SrcEnsemble_O, SrcD1Memory[i].Item1.Stat,
    //                SrcD1Memory[i].Item1.SampleIdx, SrcD1Memory[i].Item1.SentMargin, SrcD1Memory[i].Item1.RecurrentInfo, Behavior));
    //            SeqDenseRecursiveData SrcEnsemble_CSeq = (SeqDenseRecursiveData)cg.AddRunner(new Transform_Matrix2TransSeqMatrixRunner(SrcEnsemble_C, SrcD1Memory[i].Item2.Stat,
    //                SrcD1Memory[i].Item2.SampleIdx, SrcD1Memory[i].Item2.SentMargin, SrcD1Memory[i].Item2.RecurrentInfo, Behavior));
    //            SrcEnsembleMemory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(SrcEnsemble_OSeq, SrcEnsemble_CSeq));

    //            HiddenBatchData SrcStatus_O = (HiddenBatchData)cg.AddRunner(new LinkMatrixRunner(SrcEnsemble_OSeq, MapData.Data, Behavior));
    //            HiddenBatchData SrcStatus_C = (HiddenBatchData)cg.AddRunner(new LinkMatrixRunner(SrcEnsemble_CSeq, MapData.Data, Behavior));
    //            SrcEnsembleStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(SrcStatus_O, SrcStatus_C));
    //            /*
    //            List<SeqDenseBatchData> inputOList = new List<SeqDenseBatchData>();
    //            inputOList.Add(SrcMemory[i].Item1);
    //            inputOList.Add(ReverseSrcMemory[i].Item1);
    //            EnsembleConcateSeqRunner ensembleORunner = new EnsembleConcateSeqRunner(inputOList, Behavior);
    //            cg.AddRunner(ensembleORunner);

    //            List<SeqDenseBatchData> inputCList = new List<SeqDenseBatchData>();
    //            inputCList.Add(SrcMemory[i].Item2);
    //            inputCList.Add(ReverseSrcMemory[i].Item2);
    //            EnsembleConcateSeqRunner ensembleCRunner = new EnsembleConcateSeqRunner(inputCList, Behavior);
    //            cg.AddRunner(ensembleCRunner);

    //            SrcEnsembleMemory.Add(new Tuple<SeqDenseBatchData, SeqDenseBatchData>(ensembleORunner.Output, ensembleCRunner.Output));
    //            HiddenBatchData SrcO = (HiddenBatchData)cg.AddRunner(new LinkMatrixRunner(ensembleORunner.Output, SrcHelp, MapData.Data, Behavior));
    //            HiddenBatchData SrcC = (HiddenBatchData)cg.AddRunner(new LinkMatrixRunner(ensembleCRunner.Output, SrcHelp, MapData.Data, Behavior));

    //            HiddenBatchData newSrcO = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transO, SrcO, Behavior));
    //            HiddenBatchData newSrcC = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transC, SrcC, Behavior));
    //            SrcEnsembleStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(newSrcO, newSrcC));
    //            */
    //        }

    //        //(SeqDenseBatchData)cg.AddRunner(new SeqEmbedRunner(embedSrc, SrcData.SequenceData, SrcHelp, Behavior));
    //        //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(SrcData.SequenceData, Behavior));

    //        //SeqHelpData TgtHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(TgtData.SequenceData, Behavior));
    //        //SeqDenseBatchData SrcEmbedData = (SeqDenseBatchData)cg.AddRunner(new SeqEmbedRunner(embedSrc, SrcData.SequenceData, SrcHelp, Behavior));
    //        //SeqDenseBatchData SrcReverseEmbedData = (SeqDenseBatchData)cg.AddRunner(new ReverseDenseSeqRunner(SrcEmbedData, SrcHelp, Behavior));

    //        //List<Tuple<HiddenBatchData, HiddenBatchData>> SrcStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

    //        /**************** Bidirection Source LSTM Encoder Ensemble *********/
    //        switch (mode)
    //        {
    //            case DNNRunMode.Train:
    //                List<MLPAttentionV2Runner> tgtAttentionRunners = new List<MLPAttentionV2Runner>();
    //                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
    //                    tgtAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ? new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i].Item1, 
    //                        SrcEnsembleMemory[i].Item1.RecurrentInfo, MapData, BuilderParameters.TgtSeqMaxLen, TgtData.SequenceData.Stat, Behavior) : null);

    //                /**************** Target LSTM Decoder *********/
    //                SeqDenseRecursiveData TgtEmbedD1 = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedTgt, TgtData.SequenceData, false, Behavior));

    //                //SeqDenseBatchData TgtlstmOutput = (SeqDenseBatchData)cg.AddRunner(new SeqEmbedRunner(embedTgt, TgtData.SequenceData, TgtHelp, Behavior));
    //                for (int i = 0; i < decoder.LSTMCells.Count; i++)
    //                {
    //                    TgtEmbedD1 = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(decoder.LSTMCells[i],
    //                        TgtEmbedD1, SrcEnsembleStatus[i].Item1, SrcEnsembleStatus[i].Item2, tgtAttentionRunners[i], Behavior));
    //                }

    //                if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
    //                    cg.AddRunner(new ModelDiskDumpRunner(Model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\Seq2Seq"));

    //                cg.AddObjective(new EmbedFullySoftmaxRunner(decodeEmbedTgt, TgtlstmOutput, TgtHelp, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
    //                break;
    //            case DNNRunMode.Predict:

    //                if (evalType == EvaluationType.PERPLEXITY)
    //                {
    //                    List<MLPAttentionRunner> predTgtAttentionRunners = new List<MLPAttentionRunner>();
    //                    for (int i = 0; i < SrcEnsembleMemory.Count; i++)
    //                        predTgtAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
    //                            new MLPAttentionRunner(attentions[i], SrcEnsembleMemory[i].Item1, SrcHelp, MapData,
    //                            BuilderParameters.TgtSeqMaxLen, TgtData.SequenceData.Stat, Behavior) : null);

    //                    /**************** Target LSTM Decoder *********/
    //                    SeqDenseBatchData predTgtlstmOutput = (SeqDenseBatchData)cg.AddRunner(new SeqEmbedRunner(embedTgt, TgtData.SequenceData, TgtHelp, Behavior));
    //                    for (int i = 0; i < decoder.LSTMCells.Count; i++)
    //                    {
    //                        predTgtlstmOutput = (SeqDenseBatchData)cg.AddRunner(new LSTMDenseRunner<SeqDenseBatchData>(decoder.LSTMCells[i],
    //                            predTgtlstmOutput, TgtHelp, SrcEnsembleStatus[i].Item1, SrcEnsembleStatus[i].Item2, predTgtAttentionRunners[i], Behavior));
    //                    }
    //                    cg.AddRunner(new EmbedFullySoftmaxRunner(decodeEmbedTgt, predTgtlstmOutput, TgtHelp, TgtData.SequenceLabel, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath, Behavior));
    //                }
    //                else
    //                {
    //                    List<MLPAttentionRunner> decodeAttentionRunners = new List<MLPAttentionRunner>();
    //                    for (int i = 0; i < SrcMemory.Count; i++)
    //                        decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
    //                            new MLPAttentionRunner(attentions[i], SrcEnsembleMemory[i].Item1, SrcHelp, MapData,
    //                                TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);

    //                    // the probability of exactly generate the true tgt.
    //                    LSTMEmbedBeamSearchRunner decodeRunner = new LSTMEmbedBeamSearchRunner(decoder, embedTgt, decodeEmbedTgt, SrcEnsembleStatus, decodeAttentionRunners,
    //                        BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth,
    //                        BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex, Behavior);
    //                    cg.AddRunner(decodeRunner);

    //                    switch (evalType)
    //                    {
    //                        case EvaluationType.ACCURACY:
    //                            cg.AddRunner(new BeamAccuracyRunner(decodeRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
    //                            break;
    //                        case EvaluationType.BLEU:
    //                            cg.AddRunner(new BLEUDiskDumpRunner(decodeRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
    //                            break;
    //                        case EvaluationType.ROUGE:
    //                            cg.AddRunner(new ROUGEDiskDumpRunner(decodeRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
    //                            break;
    //                        case EvaluationType.TOPKBEAM:
    //                            cg.AddRunner(new TopKBeamRunner(decodeRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
    //                            break;
    //                    }
    //                }
    //                break;
    //        }
    //        return cg;
    //    }

    //    public override void Rock()
    //    {
    //        Logger.OpenLog(BuilderParameters.LogFile);

    //        Logger.WriteLog("Loading Training/Validation Data.");
    //        DataPanel.Init();
    //        Logger.WriteLog("Load Data Finished.");
    //        switch (BuilderParameters.RunMode)
    //        {
    //            case DNNRunMode.Train:
    //                int aDirectLSTM = 1;
    //                int bDirectLSTM = 1;
    //                int ensemble = aDirectLSTM + bDirectLSTM;

    //                CompositeNNStructure modelStructure = new CompositeNNStructure();

    //                LSTMStructure encodeLSTMStructure = null;
    //                LSTMStructure reverseEncodeLSTMStructure = null;
    //                LSTMStructure decodeLSTMStructure = null;

    //                LayerStructure transO = null;
    //                LayerStructure transC = null;

    //                EmbedStructure embedSrc = null;
    //                EmbedStructure embedTgt = null;
    //                EmbedStructure embedDecodeTgt = null;

    //                List<MLPAttentionStructure> attentionStructures = new List<MLPAttentionStructure>();

    //                if (BuilderParameters.SeedModel.Equals(string.Empty))
    //                {
    //                    encodeLSTMStructure = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM, DeviceType.GPU);
    //                    reverseEncodeLSTMStructure = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM, DeviceType.GPU);
    //                    decodeLSTMStructure = new LSTMStructure(BuilderParameters.Tgt_Embed, BuilderParameters.LAYER_DIM, BuilderParameters.LAYER_DIM.Select(i => i * ensemble).ToArray(), DeviceType.GPU);

    //                    transO = new LayerStructure(BuilderParameters.LAYER_DIM.Last() * ensemble, BuilderParameters.LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);
    //                    transC = new LayerStructure(BuilderParameters.LAYER_DIM.Last() * ensemble, BuilderParameters.LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);

    //                    embedSrc = new EmbedStructure(DataPanel.TrainSrcBin.Stat.FEATURE_DIM, BuilderParameters.Src_Embed, DeviceType.GPU);
    //                    embedTgt = new EmbedStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.Tgt_Embed, DeviceType.GPU);
    //                    embedDecodeTgt = new EmbedStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM.Last(), DeviceType.GPU);

    //                    for (int i = 0; i < BuilderParameters.LAYER_DIM.Length; i++)
    //                        attentionStructures.Add(new MLPAttentionStructure(BuilderParameters.LAYER_DIM[i], ensemble * BuilderParameters.LAYER_DIM[i], BuilderParameters.Attention_Hidden, DeviceType.GPU));

    //                    modelStructure.AddLayer(encodeLSTMStructure);
    //                    modelStructure.AddLayer(reverseEncodeLSTMStructure);
    //                    modelStructure.AddLayer(decodeLSTMStructure);

    //                    modelStructure.AddLayer(transO);
    //                    modelStructure.AddLayer(transC);

    //                    modelStructure.AddLayer(embedSrc);
    //                    modelStructure.AddLayer(embedTgt);
    //                    modelStructure.AddLayer(embedDecodeTgt);

    //                    for (int i = 0; i < attentionStructures.Count; i++)
    //                        modelStructure.AddLayer(attentionStructures[i]);
    //                }
    //                else
    //                {
    //                    using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
    //                    {
    //                        modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);

    //                        encodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[0];
    //                        reverseEncodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[1];
    //                        decodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[2];

    //                        transO = (LayerStructure)modelStructure.CompositeLinks[3];
    //                        transC = (LayerStructure)modelStructure.CompositeLinks[4];

    //                        embedSrc = (EmbedStructure)modelStructure.CompositeLinks[5];
    //                        embedTgt = (EmbedStructure)modelStructure.CompositeLinks[6];
    //                        embedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[7];
    //                        for (int i = 8; i < modelStructure.CompositeLinks.Count; i++)
    //                            attentionStructures.Add((MLPAttentionStructure)modelStructure.CompositeLinks[i]);
    //                    }
    //                }


    //                modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer);

    //                ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin, DataPanel.TrainMapBin,
    //                    embedSrc, embedTgt, transO, transC, encodeLSTMStructure, reverseEncodeLSTMStructure, decodeLSTMStructure, attentionStructures, embedDecodeTgt,
    //                    DNNRunMode.Train, modelStructure, BuilderParameters.GPUID, EvaluationType.PERPLEXITY);

    //                ComputationGraph validCG = null;
    //                if (BuilderParameters.IsValidFile)
    //                    validCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, DataPanel.ValidMapBin,
    //                        embedSrc, embedTgt, transO, transC, encodeLSTMStructure, reverseEncodeLSTMStructure, decodeLSTMStructure, attentionStructures, embedDecodeTgt,
    //                        DNNRunMode.Predict, modelStructure, BuilderParameters.GPUID, BuilderParameters.Evaluation);


    //                ComputationGraph devCG = null;
    //                if (BuilderParameters.IsDevFile)
    //                    devCG = BuildComputationGraph(DataPanel.DevSrcBin, DataPanel.DevTgtBin, DataPanel.DevMapBin,
    //                         embedSrc, embedTgt, transO, transC, encodeLSTMStructure, reverseEncodeLSTMStructure, decodeLSTMStructure, attentionStructures, embedDecodeTgt,
    //                        DNNRunMode.Predict, modelStructure, BuilderParameters.GPUID, EvaluationType.PERPLEXITY);

    //                if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

    //                double validScore = 0;
    //                double bestValidScore = double.MinValue;
    //                double bestValidIter = -1;

    //                double bestDevScore = double.MaxValue;
    //                double bestDevIter = -1;
    //                double bestDevValidScore = 0;

    //                for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
    //                {
    //                    double loss = trainCG.Execute();
    //                    Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

    //                    string modelName = string.Format(@"{0}\\Seq2Seq.{1}.model", BuilderParameters.ModelOutputPath, iter);

    //                    using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
    //                    {
    //                        modelStructure.Serialize(writer);
    //                    }

    //                    if (BuilderParameters.IsValidFile)
    //                    {
    //                        validScore = validCG.Execute();
    //                        if (validScore >= bestValidScore)
    //                        {
    //                            bestValidScore = validScore;
    //                            bestValidIter = iter;
    //                        }
    //                        Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
    //                    }

    //                    if (BuilderParameters.IsDevFile)
    //                    {
    //                        double devScore = devCG.Execute();
    //                        if (devScore < bestDevScore)
    //                        {
    //                            bestDevScore = devScore;
    //                            bestDevIter = iter;
    //                            bestDevValidScore = validScore;
    //                        }
    //                        Logger.WriteLog("Best Dev Score {0} at iteration {1}  Valid Score {2}", bestDevScore, bestDevIter, bestDevValidScore);
    //                    }
    //                }
    //                break;
    //            case DNNRunMode.Predict:
    //                CompositeNNStructure predModel = new CompositeNNStructure(new BinaryReader(new FileStream(BuilderParameters.ModelOutputPath, FileMode.Open, FileAccess.Read)), DeviceType.GPU);

    //                LSTMStructure predEncodeLSTMStructure = (LSTMStructure)predModel.CompositeLinks[0];
    //                LSTMStructure predReverseEncodeLSTMStructure = (LSTMStructure)predModel.CompositeLinks[1];
    //                LSTMStructure predDecodeLSTMStructure = (LSTMStructure)predModel.CompositeLinks[2];

    //                LayerStructure predTransO = (LayerStructure)predModel.CompositeLinks[3];
    //                LayerStructure predTransC = (LayerStructure)predModel.CompositeLinks[4];

    //                EmbedStructure predEmbedSrc = (EmbedStructure)predModel.CompositeLinks[5];
    //                EmbedStructure predEmbedTgt = (EmbedStructure)predModel.CompositeLinks[6];
    //                EmbedStructure predEmbedDecodeTgt = (EmbedStructure)predModel.CompositeLinks[7];

    //                List<MLPAttentionStructure> predAttentions = new List<MLPAttentionStructure>();
    //                for (int i = 8; i < predModel.CompositeLinks.Count; i++)
    //                    predAttentions.Add((MLPAttentionStructure)predModel.CompositeLinks[i]);

    //                ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, DataPanel.ValidMapBin,
    //                        predEmbedSrc, predEmbedTgt, predTransO, predTransC, predEncodeLSTMStructure, predReverseEncodeLSTMStructure,
    //                        predDecodeLSTMStructure, predAttentions, predEmbedDecodeTgt,
    //                        DNNRunMode.Predict, predModel, BuilderParameters.GPUID, BuilderParameters.Evaluation);

    //                double predScore = predCG.Execute();
    //                Logger.WriteLog("Prediction Score {0}", predScore);
    //                break;
    //        }
    //        Logger.CloseLog();

    //    }

    //    /// <summary>
    //    /// Data Panel.
    //    /// </summary>
    //    public class DataPanel
    //    {

    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainSrcBin = null;
    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;
    //        public static DataCashier<DenseBatchData, DenseDataStat> TrainMapBin = null;

    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;
    //        public static DataCashier<DenseBatchData, DenseDataStat> ValidMapBin = null;

    //        /// <summary>
    //        ///  Report PPL instead of BLEU Score.
    //        /// </summary>
    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevSrcBin = null;
    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevTgtBin = null;
    //        public static DataCashier<DenseBatchData, DenseDataStat> DevMapBin = null;

    //        public static void Init()
    //        {

    //            if (BuilderParameters.IsValidFile)
    //            {
    //                ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
    //                ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);
    //                ValidMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidMapData);

    //                ValidSrcBin.InitThreadSafePipelineCashier(100, false);
    //                ValidTgtBin.InitThreadSafePipelineCashier(100, false);
    //                ValidMapBin.InitThreadSafePipelineCashier(100, false);
    //            }

    //            if (BuilderParameters.IsDevFile)
    //            {

    //                DevSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevSrcData);
    //                DevTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevTgtData);
    //                DevMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.DevMapData);

    //                DevSrcBin.InitThreadSafePipelineCashier(100, false);
    //                DevTgtBin.InitThreadSafePipelineCashier(100, false);
    //                DevMapBin.InitThreadSafePipelineCashier(100, false);
    //            }

    //            if (BuilderParameters.RunMode == DNNRunMode.Train)
    //            {
    //                TrainSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainSrcData);
    //                TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainTgtData);
    //                TrainMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainMapData);

    //                TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //                TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //                TrainMapBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //            }

    //        }
    //    }
    //}
}
