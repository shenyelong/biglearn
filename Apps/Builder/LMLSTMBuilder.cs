﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{

    public class LMLSTMBuilderParameters : BaseModelArgument<LMLSTMBuilderParameters>
    {
        static LMLSTMBuilderParameters()
        {
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
            Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
            Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

            Argument.Add("TRAIN-DEBUG", new ParameterArgument("0", "Train Data."));

            ///Language Word Error Rate.
            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.PERPLEXITY).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));

            Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
            Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

            ///Softmax Randomup.
            Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.HierarcialSoftmax).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
            Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

            Argument.Add("NTRAIL", new ParameterArgument("1", "Number of Negative Samples."));
            
            Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
            Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

            Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
            Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
        }

        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
        public static string TrainData { get { return Argument["TRAIN"].Value; } }
        public static string ValidData { get { return Argument["VALID"].Value; } }
        public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
        public static string Vocab { get { return Argument["VOCAB"].Value; } }
        
        public static int TrainDebug { get { return int.Parse(Argument["TRAIN-DEBUG"].Value); } }

        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        
        public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
        public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

        public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

        public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }

        public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
        public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

        public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }

        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }
    }


    public class LMLSTMBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.LMLSTM; } }

        public override void InitStartup(string fileName)
        {
            LMLSTMBuilderParameters.Parse(fileName);
            Cudalib.CudaInit(LMLSTMBuilderParameters.GPUID);
        }

        public override void Rock()
        {
            Logger.OpenLog(LMLSTMBuilderParameters.LogFile);

            Vocab2Freq vocab = null;
            if (!LMLSTMBuilderParameters.Vocab.Equals(string.Empty))
            {
                vocab = new Vocab2Freq(LMLSTMBuilderParameters.Vocab);
            }
            int vocabNum = vocab.Vocab.Count;
            int cNum = 100;
            //class Number is 100 by default.
            List<List<int>> class2Vocab = vocab.Vocab2ClassV2(cNum);
            Tuple<int[], int[], int[], int[], int> vocabClassInfo = Util.ClusterIndex(class2Vocab, vocabNum);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            switch (LMLSTMBuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    LSTMStructure lstmModel = new LSTMStructure(DataPanel.Train.Stat.FEATURE_DIM, LMLSTMBuilderParameters.LAYER_DIM);
                    LSTMPredStructure lstmPredModel = new LSTMPredStructure(lstmModel, DataPanel.Train.Stat.FEATURE_DIM);
                    lstmPredModel.InitClassEmbedding(cNum);

                    lstmPredModel.InitOptimizer(new StructureLearner()
                    {
                        LearnRate = LMLSTMBuilderParameters.LearnRate,
                        Optimizer = LMLSTMBuilderParameters.Optimizer,
                        AdaBoost = LMLSTMBuilderParameters.AdaBoost,
                        ShrinkLR = LMLSTMBuilderParameters.ShrinkLR,
                        BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                        EpochNum = LMLSTMBuilderParameters.Iteration,
                        ClipDelta = LMLSTMBuilderParameters.DELTACLIP,
                        ClipWeight = LMLSTMBuilderParameters.WEIGHTCLIP
                    });

                    //EvaluationParameter evalParameter = new EvaluationParameter()
                    //{
                    //    evalType = LMLSTMBuilderParameters.Evaluation,
                    //    InputDataType = DataSourceID.SeqSparseDataSource,
                    //    OutputDataType = DataSourceID.SeqDenseBatchData,
                    //    Param = new PerplexityEvalParameter()
                    //    {
                    //        Gamma = LMLSTMBuilderParameters.Gamma,
                    //        ScoreFile = LMLSTMBuilderParameters.ScoreOutputPath,
                    //        MetricFile = LMLSTMBuilderParameters.MetricOutputPath,
                    //        vSpace = lstmPredModel.Embedding,
                    //        vBias = lstmPredModel.Bias,

                    //        cSpace = lstmPredModel.ClassEmbedding,
                    //        cBias = lstmPredModel.ClassBias,
                    //        classNum = cNum,

                    //        wordClassIndex = new CudaPieceInt(vocabClassInfo.Item4, true, true),
                    //        classVocabNum = vocabClassInfo.Item5,
                    //        vocab2class = new CudaPieceInt(vocabClassInfo.Item1, true, true),
                    //        classIndex = new CudaPieceInt(vocabClassInfo.Item2, true, true),
                    //        wordIndex = new CudaPieceInt(vocabClassInfo.Item3, true, true),

                    //        wordOutput = new CudaPieceFloat(DataPanel.Valid.Stat.MAX_SEQUENCESIZE * vocabClassInfo.Item5, true, true),
                    //        classOutput = new CudaPieceFloat(DataPanel.Valid.Stat.MAX_SEQUENCESIZE * cNum, true, true),

                    //        wordAct1 = new CudaPieceInt(DataPanel.Valid.Stat.MAX_SEQUENCESIZE * vocabClassInfo.Item5, true, true),
                    //        wordAct2 = new CudaPieceInt(DataPanel.Valid.Stat.MAX_SEQUENCESIZE * vocabClassInfo.Item5, true, true),

                    //        IsHierPerplexity = LMLSTMBuilderParameters.LossFunction == LossFunctionType.HierarcialSoftmax ? true: false,
                            
                    //        vocabSize = DataPanel.Train.Stat.FEATURE_DIM,
                    //        result = new CudaPieceFloat(DataPanel.Valid.Stat.MAX_SEQUENCESIZE * DataPanel.Valid.Stat.FEATURE_DIM, true, true),
                    //        Prob = new CudaPieceFloat(DataPanel.Valid.Stat.MAX_SEQUENCESIZE, true, true)
                    //    }
                    //};


                    //DataPanel.Train,
                    LMLSTMRunner<SeqSparseDataSource> trainer = new LMLSTMRunner<SeqSparseDataSource>(lstmPredModel, new RunnerBehavior() { RunMode = DNNRunMode.Train });
                    //LossDerivParameter LossParameter = new LossDerivParameter()
                    //{
                    //    InputDataType = DataSourceID.SeqSparseDataSource,
                    //    OutputDataType = DataSourceID.SeqDenseBatchData,
                    //    lossType = LMLSTMBuilderParameters.LossFunction,
                    //    Gamma = LMLSTMBuilderParameters.Gamma,
                    //    obj = new EmbeddingLossDerivParameter()
                    //    {
                    //        Alpha = new CudaPieceFloat(DataPanel.Train.Stat.MAX_SEQUENCESIZE, true, true),
                    //        Simi = new CudaPieceFloat(DataPanel.Train.Stat.MAX_SEQUENCESIZE * (LMLSTMBuilderParameters.NTrail + 1), true, true),

                    //        gamma = LMLSTMBuilderParameters.Gamma,
                    //        NegativeNum = LMLSTMBuilderParameters.NTrail,
                    //        vocabSize = DataPanel.Train.Stat.FEATURE_DIM,
                    //        NegativeIndex = new CudaPieceInt(DataPanel.Train.Stat.MAX_SEQUENCESIZE * LMLSTMBuilderParameters.NTrail, true, true),
                    //        vSpace = lstmPredModel.Embedding,
                    //        vBias = lstmPredModel.Bias,
                    //        Vocab = vocab.Vocab,

                    //        cSpace = lstmPredModel.ClassEmbedding,
                    //        cBias = lstmPredModel.ClassBias,
                    //        classNum = cNum,
                              
                    //        wordClassIndex = new CudaPieceInt(vocabClassInfo.Item4, true, true),
                    //        classVocabNum = vocabClassInfo.Item5,
                    //        vocab2class = new CudaPieceInt(vocabClassInfo.Item1, true, true),
                    //        classIndex = new CudaPieceInt(vocabClassInfo.Item2, true, true),
                    //        wordIndex = new CudaPieceInt(vocabClassInfo.Item3, true, true),

                    //        wordOutput = new CudaPieceFloat(DataPanel.Train.Stat.MAX_SEQUENCESIZE * vocabClassInfo.Item5, true, true),
                    //        classOutput = new CudaPieceFloat(DataPanel.Train.Stat.MAX_SEQUENCESIZE * cNum, true, true),

                    //        wordAct1 = new CudaPieceInt(DataPanel.Train.Stat.MAX_SEQUENCESIZE * vocabClassInfo.Item5, true, true),
                    //        wordAct2 = new CudaPieceInt(DataPanel.Train.Stat.MAX_SEQUENCESIZE * vocabClassInfo.Item5, true, true),

                    //        Prob = new CudaPieceFloat(DataPanel.Train.Stat.MAX_SEQUENCESIZE, true, true),
                    //        stepSize = LMLSTMBuilderParameters.LearnRate,
                    //        VocabSampleSize = 100000,
                    //        VocabSampleTable = Util.VocabSampleTable(vocab.Vocab, 100000, 0.1f)
                    //    }
                    //};

                    LMLSTMRunner<SeqSparseDataSource> evaler = null;
                    if (LMLSTMBuilderParameters.IsValidFile)
                    {
                        //DataPanel.Valid, 
                        evaler = new LMLSTMRunner<SeqSparseDataSource>(lstmPredModel, new RunnerBehavior() { RunMode = DNNRunMode.Predict });
                        //evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                    }

                    if (!Directory.Exists(LMLSTMBuilderParameters.ModelOutputPath)) Directory.CreateDirectory(LMLSTMBuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < LMLSTMBuilderParameters.Iteration; iter++)
                    {
                        //double loss = trainer.Train(DataPanel.Train.GetInstances(true));
                        double loss = 0;
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss / (DataPanel.Train.Stat.TotalBatchNumber + float.Epsilon));

                        //if (LMLSTMBuilderParameters.IsValidFile) evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                        lstmModel.Save(string.Format(@"{0}\\LSTM.{1}.model", LMLSTMBuilderParameters.ModelOutputPath, iter.ToString()));
                    }
                    trainer.Dispose();
                    evaler.Dispose();
                    break;
            }

        }


        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (LMLSTMBuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(LMLSTMBuilderParameters.ValidData);
                    Valid.PreL2();
                }

                if (LMLSTMBuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(LMLSTMBuilderParameters.TrainData);
                    Train.PreL2();
                    Train.TrainBatchNum = LMLSTMBuilderParameters.TrainDebug;
                }
            }

            internal static void DeInit()
            {
                if (null != Train)
                {
                    Train.Dispose();
                    Train = null;
                }

                if (null != Valid)
                {
                    Valid.Dispose();
                    Valid = null;
                }
            }
        }
    }
}
