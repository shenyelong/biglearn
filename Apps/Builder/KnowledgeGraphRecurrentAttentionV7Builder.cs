﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class KnowledgeGraphRecurrentAttentionV7Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static DNNRunMode RunMode { get { return int.Parse(Argument["RUN-MODE"].Value) == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
            public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }

            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static string TestData { get { return Argument["TEST"].Value; } }

            public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
            public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int EmbedDim { get { return int.Parse(Argument["EMBED-DIM"].Value); } }
            public static string EPS_SCHEDULE { get { return Argument["EPS-SCHEDULE"].Value; } }
            public static string HOP_SCHEDULE { get { return Argument["HOP-SCHEDULE"].Value; } }

            public static string EMBED_LR_SCHEDULE { get { return Argument["EBD-LR-SCHEDULE"].Value; } }
            public static string ATTENTION_LR_SCHEDULE { get { return Argument["ATT-LR-SCHEDULE"].Value; } }

            public static int MAX_NEIGHBOR { get { return int.Parse(Argument["MAX-NEIGHBOR"].Value); } }
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }
            public static int AttDim { get { return int.Parse(Argument["ATT-DIM"].Value); } }
            public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }
            public static int L1 { get { return int.Parse(Argument["L1"].Value); } }

            public static float Margin { get { return float.Parse(Argument["MARGIN"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static int PRED_PER_ITER { get { return int.Parse(Argument["PRED-PER-ITER"].Value); } }

            public static string EMBED_MODEL_FILE { get { return Argument["EMBED-MODEL-PATH"].Value; } }

            

            static BuilderParameters()
            {
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("ENTITY2ID", new ParameterArgument(string.Empty, "Entity 2 ID."));
                Argument.Add("RELATION2ID", new ParameterArgument(string.Empty, "Relation 2 ID."));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("EMBED-MODEL-PATH", new ParameterArgument(string.Empty, "Seed Model Path"));


                Argument.Add("EMBED-DIM", new ParameterArgument("100", "Embedding Dim"));
                Argument.Add("EPS-SCHEDULE", new ParameterArgument("0:1,5000000:1,50000000:1,100000000:1,200000000:0.1,400000000:0.05", "Epslion Schedule."));
                Argument.Add("HOP-SCHEDULE", new ParameterArgument("0:0,5000000:0,50000000:0,100000000:0,200000000:1,400000000:2", "Hop Schedule."));
                Argument.Add("EBD-LR-SCHEDULE", new ParameterArgument("0:0.01,5000000:0.005,50000000:0.001,100000000:0.0005,200000000:0.0001", "EMD Lr Schedule."));
                Argument.Add("ATT-LR-SCHEDULE", new ParameterArgument("0:0,5000000:0,50000000:0.01,100000000:0.005,200000000:0.001,400000000:0.0001", "Hop Schedule."));

                Argument.Add("MAX-NEIGHBOR", new ParameterArgument("10", "Maximum Neighbor Set."));
                Argument.Add("MAX-HOP", new ParameterArgument("2", "Max Hop."));
                Argument.Add("ATT-DIM", new ParameterArgument("32", "Hidden Dim of Attention Layer."));

                Argument.Add("NTRAIL", new ParameterArgument("50", "Number of Negative Samples"));
                Argument.Add("L1", new ParameterArgument("1", "L1 Loss"));
                Argument.Add("MARGIN", new ParameterArgument("1", "Margin Parameter"));

                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.005", "DSSM Learn Rate."));
                Argument.Add("DELTA-CLIP", new ParameterArgument("0.1", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("5", "Model Weight Clip"));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("PRED-PER-ITER", new ParameterArgument("50", "Train Iteration."));
            }
        }

        public override BuilderType Type { get { return BuilderType.RECURRENT_ATTENTION_GRAPH_EMBED_V7; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<Tuple<int, float>> ScheduleEpsilon(string schedule)
        {
            return schedule.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
        }

        public float Schedule(string schedule, int step)
        {
            List<Tuple<int, float>> scheduleList = ScheduleEpsilon(schedule);
            for (int i = 0; i < scheduleList.Count; i++)
            {
                if (step < scheduleList[i].Item1)
                {
                    float lambda = (step - scheduleList[i - 1].Item1) * 1.0f / (scheduleList[i].Item1 - scheduleList[i - 1].Item1);
                    return lambda * scheduleList[i].Item2 + (1 - lambda) * scheduleList[i - 1].Item2;
                }
            }
            return scheduleList.Last().Item2;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            MathOperatorManager.MathDevice = DeviceType.CPU_FAST_VECTOR;
            RunnerBehavior behavior = new RunnerBehavior() { Device = MathOperatorManager.MathDevice, RunMode = DNNRunMode.Train, Computelib = new FastVectorOperation() };

            DeepGraphEmbedingStruct DeepEmbed = new DeepGraphEmbedingStruct(DataPanel.knowledgeGraph, BuilderParameters.EmbedDim, BuilderParameters.AttDim, BuilderParameters.MAX_HOP, behavior.Device);

            if (!BuilderParameters.EMBED_MODEL_FILE.Equals(string.Empty))
                DeepEmbed.DeserializeEmbedModel(new BinaryReader(new FileStream(BuilderParameters.EMBED_MODEL_FILE, FileMode.Open, FileAccess.Read)), behavior.Device);

            if (!BuilderParameters.SEED_MODEL.Equals(string.Empty))
                DeepEmbed.Deserialize(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), behavior.Device);

            if (BuilderParameters.RunMode == DNNRunMode.Train)
            {
                DeepEmbed.EntityOptimizer = new SparseGradientOptimizer(DeepEmbed.EntityVec, true, BuilderParameters.LearnRate, behavior);
                DeepEmbed.RelationOptimizer = new SparseGradientOptimizer(DeepEmbed.RelationVec, false, BuilderParameters.LearnRate, behavior);

                DeepEmbed.InputEntityEmbedOptimizer = new ClipSGDOptimizer(DeepEmbed.InputEntityEmbedMatrix, BuilderParameters.LearnRate, BuilderParameters.DELTACLIP, BuilderParameters.WEIGHTCLIP, behavior);
                DeepEmbed.InputRelationEmbedOptimizer = new ClipSGDOptimizer(DeepEmbed.InputRelationEmbedMatrix, BuilderParameters.LearnRate, BuilderParameters.DELTACLIP, BuilderParameters.WEIGHTCLIP, behavior);

                DeepEmbed.OutputEntityEmbedOptimizer = new ClipSGDOptimizer(DeepEmbed.OutputEntityEmbedMatrix, BuilderParameters.LearnRate, BuilderParameters.DELTACLIP, BuilderParameters.WEIGHTCLIP, behavior);
                DeepEmbed.OutputRelationEmbedOptimizer = new ClipSGDOptimizer(DeepEmbed.OutputRelationEmbedMatrix, BuilderParameters.LearnRate, BuilderParameters.DELTACLIP, BuilderParameters.WEIGHTCLIP, behavior);

                DeepEmbed.GRUSubQueryCell.InitOptimizer(new StructureLearner()
                {
                    Optimizer = DNNOptimizerType.ClipSGD,
                    LearnRate = BuilderParameters.LearnRate,
                    ClipDelta = BuilderParameters.DELTACLIP,
                    ClipWeight = BuilderParameters.WEIGHTCLIP,
                    device = behavior.Device
                });
            }

            DeepEmbed.MaxNeighbor = BuilderParameters.MAX_NEIGHBOR;
            DeepEmbed.InitGraphRunners(behavior);

            bool IsL1Flag = BuilderParameters.L1 >= 1;
            int NegVersion = 0;
            int learnSampleCount = 0;

            PerfCounter.Manager instance = new PerfCounter.Manager();
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);


            CudaPieceFloat wpDeriv = new CudaPieceFloat(DeepEmbed.Dim, true, false);
            CudaPieceFloat wnDeriv = new CudaPieceFloat(DeepEmbed.Dim, true, false);

            for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
            {
                int rowCount = 0;
                double loss = 0;

                if (BuilderParameters.RunMode != DNNRunMode.Predict)
                {
                    DataRandomShuffling Shuffle = new DataRandomShuffling(DataPanel.Train.Count);

                    HashSet<int> isHeadBefore = new HashSet<int>();
                    while (true)
                    {
                        int id = Shuffle.RandomNext();
                        if (id == -1) break;

                        learnSampleCount += 1;
                        DeepEmbed.Eps = Schedule(BuilderParameters.EPS_SCHEDULE, learnSampleCount);
                        DeepEmbed.MaxHop = (int)Schedule(BuilderParameters.HOP_SCHEDULE, learnSampleCount);

                        float emd_lr = Schedule(BuilderParameters.EMBED_LR_SCHEDULE, learnSampleCount);
                        float att_lr = Schedule(BuilderParameters.ATTENTION_LR_SCHEDULE, learnSampleCount);

                        DeepEmbed.EntityOptimizer.GradientStep = emd_lr;
                        DeepEmbed.RelationOptimizer.GradientStep = emd_lr;

                        DeepEmbed.InputEntityEmbedOptimizer.GradientStep = att_lr;
                        DeepEmbed.InputRelationEmbedOptimizer.GradientStep = att_lr;

                        DeepEmbed.OutputEntityEmbedOptimizer.GradientStep = att_lr;
                        DeepEmbed.OutputRelationEmbedOptimizer.GradientStep = att_lr;

                        foreach (ModelOptimizer opt in DeepEmbed.GRUSubQueryCell.ModelOptimizers)
                            opt.Optimizer.GradientStep = att_lr;

                        int leftEntityId = DataPanel.Train[id].Item1;
                        int rightEntityId = DataPanel.Train[id].Item2;
                        int relationId = DataPanel.Train[id].Item3;

                        int head;
                        int tail;
                        int connection;
                        HashSet<int> negtail = new HashSet<int>();

                        var trainSampleTimer = instance["Train"].Begin();
                        double pr = NegVersion == 0 ? 0.5 : DataPanel.TailProb[relationId] / (DataPanel.TailProb[relationId] + DataPanel.HeadProb[relationId]);
                        if (ParameterSetting.Random.NextDouble() < pr)
                        {
                            head = leftEntityId;
                            connection = 1;
                            tail = rightEntityId;
                            for (int ntrail = 0; ntrail < BuilderParameters.NTrail; ntrail++)
                            {
                                int negID = -1;
                                do
                                {
                                    negID = ParameterSetting.Random.Next(DataPanel.EntityNum);
                                } while (DataPanel.IsInGraph(head, negID, relationId) || negtail.Contains(negID));
                                negtail.Add(negID);
                            }
                        }
                        else
                        {
                            head = rightEntityId;
                            connection = -1;
                            tail = leftEntityId;
                            for (int ntrail = 0; ntrail < BuilderParameters.NTrail; ntrail++)
                            {
                                int negID = -1;
                                do
                                {
                                    negID = ParameterSetting.Random.Next(DataPanel.EntityNum);
                                } while (DataPanel.IsInGraph(negID, head, relationId) || negtail.Contains(negID));
                                negtail.Add(negID);
                            }
                        }

                        DeepEmbed.blackNodeList.Clear();
                        DeepEmbed.SetBlackConnection(leftEntityId, relationId, rightEntityId);
                        DeepEmbed.GraphNodeRunner[head].Data.Step = 0;
                        DeepEmbed.GraphNodeRunner[head].Data.Query.BatchSize = 1;
                        if (connection == 1)
                        {
                            behavior.Computelib.Add_Vector(DeepEmbed.GraphNodeRunner[head].Data.Query.Output.Data, 0,
                                                            DeepEmbed.RelationVec, relationId * DeepEmbed.Dim, DeepEmbed.Dim, 0, 1);
                        }
                        else
                        {
                            behavior.Computelib.Add_Vector(DeepEmbed.GraphNodeRunner[head].Data.Query.Output.Data, 0,
                                                            DeepEmbed.RelationVec, (DeepEmbed.RelationNum + relationId) * DeepEmbed.Dim, DeepEmbed.Dim, 0, 1);
                        }
                        DeepEmbed.GraphNodeRunner[head].Forward();
                        DeepEmbed.ClearBlackConnection();

                        foreach (ModelOptimizer optimizer in DeepEmbed.ModelOptimizers)
                            optimizer.Optimizer.BeforeGradient();

                        DeepEmbed.GraphNodeRunner[head].CleanDeriv();

                        behavior.Computelib.Zero(DeepEmbed.GraphNodeRunner[head].Data.Query.Deriv.Data, DeepEmbed.Dim);
                        behavior.Computelib.Zero(DeepEmbed.GraphNodeRunner[head].Data.Answer.Deriv.Data, DeepEmbed.Dim);

                        /// to speed up convergence we have multiple neg samples.
                        /// positive loss.
                        double PosSum = 0;
                        float[] wPos = new float[DeepEmbed.Dim];

                        for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                        {
                            wPos[ii] = DeepEmbed.EntityVec.MemPtr[(tail + DeepEmbed.EntityNum) * DeepEmbed.Dim + ii] - DeepEmbed.GraphNodeRunner[head].Data.Answer.Output.Data.MemPtr[ii];
                            if (IsL1Flag) PosSum += Math.Abs(wPos[ii]);
                            else PosSum += wPos[ii] * wPos[ii];
                        }

                        if (!IsL1Flag) PosSum = Math.Sqrt(PosSum);

                        int derivNum = 0;

                        float[] wNeg = new float[DeepEmbed.Dim];

                        int nid = 0;
                        foreach (int negID in negtail)
                        {
                            double NegSum = 0;
                            for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                            {
                                wNeg[ii] = DeepEmbed.EntityVec.MemPtr[(negID + DeepEmbed.EntityNum) * DeepEmbed.Dim + ii] - DeepEmbed.GraphNodeRunner[head].Data.Answer.Output.Data.MemPtr[ii];
                                if (IsL1Flag) NegSum += Math.Abs(wNeg[ii]);
                                else NegSum += wNeg[ii] * wNeg[ii];
                            }

                            if (!IsL1Flag) NegSum = Math.Sqrt(NegSum);

                            if (PosSum + BuilderParameters.Margin > NegSum)
                            {
                                loss += BuilderParameters.Margin + PosSum - NegSum;

                                for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                                {
                                    float pDeriv = 0;
                                    float nDeriv = 0;
                                    if (IsL1Flag)
                                    {
                                        if (wPos[ii] > 0) pDeriv = 1;
                                        else if (wPos[ii] < 0) pDeriv = -1;
                                        if (wNeg[ii] > 0) nDeriv = -1;
                                        else if (wNeg[ii] < 0) nDeriv = 1;
                                    }
                                    else
                                    {
                                        pDeriv = (float)(wPos[ii] / (PosSum + float.Epsilon));
                                        nDeriv = -(float)(wNeg[ii] / (NegSum + float.Epsilon));
                                    }
                                    DeepEmbed.GraphNodeRunner[head].Data.Answer.Deriv.Data.MemPtr[ii] += pDeriv + nDeriv;
                                    wpDeriv.MemPtr[ii] = pDeriv;
                                    wnDeriv.MemPtr[ii] = nDeriv;
                                }

                                ((SparseGradientOptimizer)DeepEmbed.EntityOptimizer).PushGradientIndex((tail + DeepEmbed.EntityNum) * DeepEmbed.Dim, DeepEmbed.Dim);
                                behavior.Computelib.Add_Vector(DeepEmbed.EntityOptimizer.Gradient, (tail + DeepEmbed.EntityNum) * DeepEmbed.Dim,
                                                           wpDeriv, 0, DeepEmbed.Dim, 1, -1 * DeepEmbed.EntityOptimizer.GradientStep);

                                ((SparseGradientOptimizer)DeepEmbed.EntityOptimizer).PushGradientIndex((negID + DeepEmbed.EntityNum) * DeepEmbed.Dim, DeepEmbed.Dim);
                                behavior.Computelib.Add_Vector(DeepEmbed.EntityOptimizer.Gradient, (negID + DeepEmbed.EntityNum) * DeepEmbed.Dim,
                                                           wnDeriv, 0, DeepEmbed.Dim, 1, -1 * DeepEmbed.EntityOptimizer.GradientStep);
                                derivNum += 1;
                            }

                            nid += 1;
                        }

                        if (derivNum > 0)
                        {
                            DeepEmbed.GraphNodeRunner[head].Backward(false);

                            DeepEmbed.GraphNodeRunner[head].Update();

                            if (connection == 1)
                            {
                                ((SparseGradientOptimizer)DeepEmbed.RelationOptimizer).PushGradientIndex(relationId * DeepEmbed.Dim, DeepEmbed.Dim);
                                behavior.Computelib.Add_Vector(DeepEmbed.RelationOptimizer.Gradient, relationId * DeepEmbed.Dim,
                                                           DeepEmbed.GraphNodeRunner[head].Data.Query.Deriv.Data, 0, DeepEmbed.Dim, 1, DeepEmbed.RelationOptimizer.GradientStep);
                            }
                            else
                            {
                                ((SparseGradientOptimizer)DeepEmbed.RelationOptimizer).PushGradientIndex((relationId + DeepEmbed.RelationNum) * DeepEmbed.Dim, DeepEmbed.Dim);
                                behavior.Computelib.Add_Vector(DeepEmbed.RelationOptimizer.Gradient, (relationId + DeepEmbed.RelationNum) * DeepEmbed.Dim,
                                                            DeepEmbed.GraphNodeRunner[head].Data.Query.Deriv.Data, 0, DeepEmbed.Dim, 1, DeepEmbed.RelationOptimizer.GradientStep);
                            }

                            foreach (ModelOptimizer optimizer in DeepEmbed.ModelOptimizers)
                                optimizer.Optimizer.AfterGradient();
                        }
                        instance["Train"].TakeCount(trainSampleTimer);

                        if (++rowCount % 1000 == 0)
                        {
                            Logger.WriteLog("Avg Loss {0}, Eps {1}, Hop {2}, EMD-LR {3}, ATT-LR {4}, Count {5}, Iteration {6}",
                                loss * 1.0f / rowCount, DeepEmbed.Eps, DeepEmbed.MaxHop, emd_lr, att_lr, rowCount, iter);
                            Logger.WriteLog("Perf " + instance["Train"].ToString() + "\n");
                        }
                    }
                    string modelName = string.Format(@"{0}\\KnowledgeGraphEmbedding.{1}.model", BuilderParameters.ModelOutputPath, iter);
                    DeepEmbed.Save(modelName);
                }

                if ((iter + 1) % BuilderParameters.PRED_PER_ITER == 0 || BuilderParameters.RunMode == DNNRunMode.Predict)
                {
                    DeepEmbed.Eps = Schedule(BuilderParameters.EPS_SCHEDULE, learnSampleCount);
                    DeepEmbed.MaxHop = (int)Schedule(BuilderParameters.HOP_SCHEDULE, learnSampleCount);
                    Logger.WriteLog("Predict Eps {0}, Hop {1}", DeepEmbed.Eps, DeepEmbed.MaxHop);

                    int lsum = 0, lsum_filter = 0;
                    int rsum = 0, rsum_filter = 0;
                    /*
                    int lsumnei1 = 0;
                    int lsumnei2 = 0;
                    int lsumnei3 = 0;
                    int rsumnei1 = 0;
                    int rsumnei2 = 0;
                    int rsumnei3 = 0;
                    */

                    int lp_n = 0, lp_n_filter = 0;
                    int rp_n = 0, rp_n_filter = 0;
                    /*
                    int l_nei1_hit = 0;
                    int l_nei2_hit = 0;
                    int l_nei3_hit = 0;
                    int r_nei1_hit = 0;
                    int r_nei2_hit = 0;
                    int r_nei3_hit = 0;
                    */

                    int HitK = 10;

                    // Test.
                    //Parallel.For(0, DataPanel.knowledgeGraph.Test.Count, itemIdx => // 
                    for (int itemIdx = 0; itemIdx < DataPanel.knowledgeGraph.Test.Count; itemIdx++)
                    {
                        int head = DataPanel.knowledgeGraph.Test[itemIdx].Item1;
                        int tail = DataPanel.knowledgeGraph.Test[itemIdx].Item2;
                        int r = DataPanel.knowledgeGraph.Test[itemIdx].Item3;

                        DeepEmbed.GraphNodeRunner[head].Data.Step = 0;
                        DeepEmbed.GraphNodeRunner[head].Data.Query.BatchSize = 1;
                        DeepEmbed.blackNodeList.Clear();

                        //FastVector.Add_Vector(DeepEmbed.GraphNodeRunner[head].Data.Query.Output.Data.MemPtr, 0, DeepEmbed.RelationVec.MemPtr, r * DeepEmbed.Dim,
                        //    DeepEmbed.Dim, 0, 1);
                        behavior.Computelib.Add_Vector(DeepEmbed.GraphNodeRunner[head].Data.Query.Output.Data, 0, DeepEmbed.RelationVec, r * DeepEmbed.Dim, DeepEmbed.Dim, 0, 1);

                        DeepEmbed.GraphNodeRunner[head].Forward();

                        float[] tscores = new float[DeepEmbed.EntityNum];
                        Parallel.For(0, DeepEmbed.EntityNum, new ParallelOptions() { MaxDegreeOfParallelism = 32 }, e =>
                        {
                            float dvalue = 0;
                            for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                            {
                                float tmp = DeepEmbed.EntityVec.MemPtr[(e + DeepEmbed.EntityNum) * DeepEmbed.Dim + ii] - DeepEmbed.GraphNodeRunner[head].Data.Answer.Output.Data.MemPtr[ii];
                                if (IsL1Flag) dvalue += Math.Abs(tmp);
                                else dvalue += tmp * tmp;
                            }
                            tscores[e] = dvalue;
                        });

                        int[] lindicies = Enumerable.Range(0, DeepEmbed.EntityNum).ToArray();
                        Array.Sort(tscores, lindicies);

                        int filter = 0;
                        //int nei1filter = 0;
                        //int nei2filter = 0;
                        //int nei3filter = 0;
                        for (int c = DeepEmbed.EntityNum - 1; c >= 0; c--)
                        {
                            int ctail = lindicies[DeepEmbed.EntityNum - 1 - c];
                            if (!DataPanel.IsInGraph(head, ctail, r)) filter++;
                            //if (DataPanel.Neighbor1Set[head].Contains(ctail) && !DataPanel.IsInGraph(head, ctail, r)) nei1filter++;
                            //if (DataPanel.Neighbor2Set[head].Contains(ctail) && !DataPanel.IsInGraph(head, ctail, r)) nei2filter++;
                            //if (DataPanel.Neighbor3Set[head].Contains(ctail) && !DataPanel.IsInGraph(head, ctail, r)) nei3filter++;

                            if (ctail == tail)
                            {
                                Interlocked.Add(ref lsum, DeepEmbed.EntityNum - c);
                                Interlocked.Add(ref lsum_filter, filter);

                                /*
                                if (DataPanel.Neighbor1Set[head].Contains(ctail))
                                {
                                    Interlocked.Add(ref lsumnei1, nei1filter);
                                    if (nei1filter <= HitK) Interlocked.Increment(ref l_nei1_hit);
                                }
                                else
                                {
                                    Interlocked.Add(ref lsumnei1, DeepEmbed.EntityNum - c - nei1filter + DataPanel.Neighbor1Set[head].Count);
                                    if(DeepEmbed.EntityNum - c - nei1filter + DataPanel.Neighbor1Set[head].Count <= HitK) Interlocked.Increment(ref l_nei1_hit);
                                }

                                if (DataPanel.Neighbor2Set[head].Contains(ctail))
                                {
                                    Interlocked.Add(ref lsumnei2, nei2filter);
                                    if (nei2filter <= HitK) Interlocked.Increment(ref l_nei2_hit);
                                }
                                else
                                {
                                    Interlocked.Add(ref lsumnei2, DeepEmbed.EntityNum - c - nei2filter + DataPanel.Neighbor2Set[head].Count);
                                    if (DeepEmbed.EntityNum - c - nei2filter + DataPanel.Neighbor2Set[head].Count <= HitK) Interlocked.Increment(ref l_nei2_hit);
                                }

                                if (DataPanel.Neighbor3Set[head].Contains(ctail))
                                {
                                    Interlocked.Add(ref lsumnei3, nei3filter);
                                    if (nei3filter <= HitK) Interlocked.Increment(ref l_nei3_hit);
                                }
                                else
                                {
                                    Interlocked.Add(ref lsumnei3, DeepEmbed.EntityNum - c - nei3filter + DataPanel.Neighbor3Set[head].Count);
                                    if (DeepEmbed.EntityNum - c - nei3filter + DataPanel.Neighbor3Set[head].Count <= HitK) Interlocked.Increment(ref l_nei3_hit);
                                }
                                */
                                if (DeepEmbed.EntityNum - c <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
                                if (filter <= HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
                                break;
                            }
                        }

                        DeepEmbed.GraphNodeRunner[tail].Data.Step = 0;
                        DeepEmbed.GraphNodeRunner[tail].Data.Query.BatchSize = 1;
                        DeepEmbed.blackNodeList.Clear();

                        behavior.Computelib.Add_Vector(DeepEmbed.GraphNodeRunner[tail].Data.Query.Output.Data, 0, DeepEmbed.RelationVec, (r + DeepEmbed.RelationNum) * DeepEmbed.Dim, DeepEmbed.Dim, 0, 1);

                        DeepEmbed.GraphNodeRunner[tail].Forward();

                        float[] hscores = new float[DeepEmbed.EntityNum];
                        Parallel.For(0, DeepEmbed.EntityNum, new ParallelOptions() { MaxDegreeOfParallelism = 32 }, e =>
                        {
                            float dvalue = 0;
                            for (int ii = 0; ii < DeepEmbed.Dim; ii++)
                            {
                                float tmp = DeepEmbed.EntityVec.MemPtr[(e + DeepEmbed.EntityNum) * DeepEmbed.Dim + ii] - DeepEmbed.GraphNodeRunner[tail].Data.Answer.Output.Data.MemPtr[ii];
                                if (IsL1Flag) dvalue += Math.Abs(tmp);
                                else dvalue += tmp * tmp;
                            }
                            hscores[e] = dvalue;
                        });

                        int[] hindicies = Enumerable.Range(0, DeepEmbed.EntityNum).ToArray();
                        Array.Sort(hscores, hindicies);

                        filter = 0;
                        //nei1filter = 0;
                        //nei2filter = 0;
                        //nei3filter = 0;
                        for (int c = DeepEmbed.EntityNum - 1; c >= 0; c--)
                        {
                            int chead = hindicies[DeepEmbed.EntityNum - 1 - c];
                            if (!DataPanel.IsInGraph(chead, tail, r)) filter++;
                            //if (DataPanel.Neighbor1Set[chead].Contains(tail) && !DataPanel.IsInGraph(chead, tail, r)) nei1filter++;
                            //if (DataPanel.Neighbor2Set[chead].Contains(tail) && !DataPanel.IsInGraph(chead, tail, r)) nei2filter++;
                            //if (DataPanel.Neighbor3Set[chead].Contains(tail) && !DataPanel.IsInGraph(chead, tail, r)) nei3filter++;

                            if (chead == head)
                            {
                                Interlocked.Add(ref rsum, DeepEmbed.EntityNum - c);
                                Interlocked.Add(ref rsum_filter, filter);
                                /*
                                if (DataPanel.Neighbor1Set[chead].Contains(tail))
                                {
                                    Interlocked.Add(ref rsumnei1, nei1filter);
                                    if (nei1filter <= HitK) Interlocked.Increment(ref r_nei1_hit);
                                }
                                else
                                {
                                    Interlocked.Add(ref rsumnei1, DeepEmbed.EntityNum - c - nei1filter + DataPanel.Neighbor1Set[chead].Count);
                                    if(DeepEmbed.EntityNum - c - nei1filter + DataPanel.Neighbor1Set[chead].Count <= HitK) Interlocked.Increment(ref r_nei1_hit);
                                }

                                if (DataPanel.Neighbor2Set[chead].Contains(tail))
                                {
                                    Interlocked.Add(ref rsumnei2, nei2filter);
                                    if (nei2filter <= HitK) Interlocked.Increment(ref r_nei2_hit);
                                }
                                else
                                {
                                    Interlocked.Add(ref rsumnei2, DeepEmbed.EntityNum - c - nei2filter + DataPanel.Neighbor2Set[chead].Count);
                                    if (DeepEmbed.EntityNum - c - nei2filter + DataPanel.Neighbor2Set[chead].Count <= HitK) Interlocked.Increment(ref r_nei2_hit);
                                }

                                if (DataPanel.Neighbor3Set[chead].Contains(tail))
                                {
                                    Interlocked.Add(ref rsumnei3, nei3filter);
                                    if (nei3filter <= HitK) Interlocked.Increment(ref r_nei3_hit);
                                }
                                else
                                {
                                    Interlocked.Add(ref rsumnei3, DeepEmbed.EntityNum - c - nei3filter + DataPanel.Neighbor3Set[chead].Count);
                                    if (DeepEmbed.EntityNum - c - nei3filter + DataPanel.Neighbor3Set[chead].Count <= HitK) Interlocked.Increment(ref r_nei3_hit);
                                }
                                */
                                if (DeepEmbed.EntityNum - c <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
                                if (filter <= HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
                                break;
                            }
                        }
                        if (itemIdx % 1000 == 0) Console.WriteLine("Predline {0}", itemIdx);
                    }
                    int totalSampleNum = DataPanel.Test.Count;
                    Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Left Filter Nei 1 Rank {0}", lsumnei1 * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Left Filter Nei 2 Rank {0}", lsumnei2 * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Left Filter Nei 3 Rank {0}", lsumnei3 * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Left Filter Nei 1 Hit {0}", l_nei1_hit * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Left Filter Nei 2 Hit {0}", l_nei2_hit * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Left Filter Nei 3 Hit {0}", l_nei3_hit * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Right Filter Nei 1 Rank {0}", rsumnei1 * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Right Filter Nei 2 Rank {0}", rsumnei2 * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Right Filter Nei 3 Rank {0}", rsumnei3 * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Right Filter Nei 1 Hit {0}", r_nei1_hit * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Right Filter Nei 2 Hit {0}", r_nei2_hit * 1.0 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Right Filter Nei 3 Hit {0}", r_nei3_hit * 1.0 / totalSampleNum));
                    Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 0.5 / totalSampleNum));
                    Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 0.5 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Overall Filter Nei 1 Rank {0}", (lsumnei1 + rsumnei1) * 0.5 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Overall Filter Nei 2 Rank {0}", (lsumnei2 + rsumnei2) * 0.5 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Overall Filter Nei 3 Rank {0}", (lsumnei3 + rsumnei3) * 0.5 / totalSampleNum));
                    Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 0.5 / totalSampleNum));
                    Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 0.5 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Overall Filter Nei 1 Hit {0}", (l_nei1_hit + r_nei1_hit) * 0.5 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Overall Filter Nei 2 Hit {0}", (l_nei2_hit + r_nei2_hit) * 0.5 / totalSampleNum));
                    //Logger.WriteLog(string.Format("Overall Filter Nei 3 Hit {0}", (l_nei3_hit + r_nei3_hit) * 0.5 / totalSampleNum));
                    if (BuilderParameters.RunMode == DNNRunMode.Predict) break;
                }
            }
        }

        public class SparseGradientOptimizer : GradientOptimizer
        {
            Dictionary<int, int> GradientIndex = new Dictionary<int, int>();
            bool IsNorm = true;
            public SparseGradientOptimizer(CudaPieceFloat weight, bool isNorm, float learnRate, RunnerBehavior behavior) : base(behavior)
            {
                ///it will always equal to learning rate.
                GradientStep = learnRate;
                Parameter = weight;
                IsNorm = isNorm;
                Gradient = new CudaPieceFloat(weight.Size, true, behavior.Device == DeviceType.GPU);
            }

            public virtual void PushGradientIndex(int idx, int range)
            {
                if (GradientIndex.ContainsKey(idx)) GradientIndex[idx] = Math.Max(GradientIndex[idx], range);
                else GradientIndex[idx] = range;
            }

            public override void BeforeGradient()
            {
                Behavior.Computelib.Zero(Gradient, Gradient.Size);
                GradientIndex.Clear();
            }

            public override void AfterGradient()
            {
                Gradient.SyncToCPU();
                Parameter.SyncToCPU();
                Parallel.ForEach(GradientIndex, gIdx =>
                {
                    for (int s = 0; s < gIdx.Value; s++)
                    {
                        Parameter.MemPtr[gIdx.Key + s] += Gradient.MemPtr[gIdx.Key + s];
                    }

                    if (IsNorm)
                    {
                        double x = 0;
                        for (int s = 0; s < gIdx.Value; s++)
                        {
                            x = x + Parameter.MemPtr[gIdx.Key + s] * Parameter.MemPtr[gIdx.Key + s];
                        }
                        x = Math.Sqrt(x);
                        if (x < double.Epsilon) x = double.Epsilon;
                        for (int ii = 0; ii < gIdx.Value; ii++)
                            Parameter.MemPtr[gIdx.Key + ii] = (float)(Parameter.MemPtr[gIdx.Key + ii] / x);
                    }
                });

                Parameter.SyncFromCPU();
            }
        }

        public class DeepGraphEmbedingStruct : Structure
        {
            public int EntityNum;
            public int RelationNum;
            public int Dim;
            public CudaPieceFloat EntityVec;
            public CudaPieceFloat RelationVec;

            public int StatusHiddenDim;

            public CudaPieceFloat InputEntityEmbedMatrix;
            public CudaPieceFloat InputRelationEmbedMatrix;

            public CudaPieceFloat OutputEntityEmbedMatrix;
            public CudaPieceFloat OutputRelationEmbedMatrix;

            public override List<Structure> SubStructure { get { return new List<Structure>() { (Structure)GRUSubQueryCell }; } }
            public GRUCell GRUSubQueryCell;

            public GradientOptimizer EntityOptimizer { get { return StructureOptimizer["EntityVec"].Optimizer; } set { StructureOptimizer["EntityVec"].Optimizer = value; } }
            public GradientOptimizer RelationOptimizer { get { return StructureOptimizer["RelationVec"].Optimizer; } set { StructureOptimizer["RelationVec"].Optimizer = value; } }

            public GradientOptimizer InputEntityEmbedOptimizer { get { return StructureOptimizer["InputEntityEmbedMatrix"].Optimizer; } set { StructureOptimizer["InputEntityEmbedMatrix"].Optimizer = value; } }
            public GradientOptimizer InputRelationEmbedOptimizer { get { return StructureOptimizer["InputRelationEmbedMatrix"].Optimizer; } set { StructureOptimizer["InputRelationEmbedMatrix"].Optimizer = value; } }

            public GradientOptimizer OutputEntityEmbedOptimizer { get { return StructureOptimizer["OutputEntityEmbedMatrix"].Optimizer; } set { StructureOptimizer["OutputEntityEmbedMatrix"].Optimizer = value; } }
            public GradientOptimizer OutputRelationEmbedOptimizer { get { return StructureOptimizer["OutputRelationEmbedMatrix"].Optimizer; } set { StructureOptimizer["OutputRelationEmbedMatrix"].Optimizer = value; } }


            public int MaxHop = 1;
            public float ExploreDiscount = 0.8f;
            public float Eps = 0.9f;
            public int MaxNeighbor = 5;

            public RelationGraphData GraphData;

            public GraphNodeRunner[] GraphNodeRunner;


            public HashSet<int> blackNodeList = new HashSet<int>();
            public void AddBlackNode(int nid)
            {
                blackNodeList.Add(nid);
            }

            #region black connection in training prase.
            int blackeid1 { get; set; }
            int blackeid2 { get; set; }
            int blackrid { get; set; }

            public bool IsBlackConnection(int eid1, int rid, int eid2)
            {
                if (blackeid1 == eid1 && blackeid2 == eid2 && blackrid == rid) return true;
                return false;
            }

            public void SetBlackConnection(int eid1, int rid, int eid2)
            {
                blackeid1 = eid1;
                blackeid2 = eid2;
                blackrid = rid;
            }

            public void ClearBlackConnection()
            {
                blackeid1 = -1;
                blackrid = -1;
                blackeid2 = -1;
            }
            #endregion.

            public void NormEntity()
            {
                Parallel.For(0, 2 * EntityNum, i =>
                {
                    double x = Math.Sqrt(EntityVec.MemPtr.Skip(i * Dim).Take(Dim).Select(v => v * v).Sum());
                    if (x > 1)
                        for (int ii = 0; ii < Dim; ii++)
                            EntityVec.MemPtr[i * Dim + ii] = (float)(EntityVec.MemPtr[i * Dim + ii] / x);
                });
            }

            #region serialize and deserialize.
            public override void Serialize(BinaryWriter writer)
            {
                writer.Write(EntityNum);
                writer.Write(RelationNum);
                writer.Write(Dim);
                writer.Write(StatusHiddenDim);

                EntityVec.Serialize(writer);
                RelationVec.Serialize(writer);

                InputRelationEmbedMatrix.Serialize(writer);
                InputEntityEmbedMatrix.Serialize(writer);

                OutputRelationEmbedMatrix.Serialize(writer);
                OutputEntityEmbedMatrix.Serialize(writer);

                GRUSubQueryCell.Serialize(writer);
            }

            public override void Deserialize(BinaryReader reader, DeviceType device)
            {
                EntityNum = reader.ReadInt32();
                RelationNum = reader.ReadInt32();
                Dim = reader.ReadInt32();
                StatusHiddenDim = reader.ReadInt32();

                EntityVec = new CudaPieceFloat(reader, device);
                RelationVec = new CudaPieceFloat(reader, device);

                InputRelationEmbedMatrix = new CudaPieceFloat(reader, device);
                InputEntityEmbedMatrix = new CudaPieceFloat(reader, device);

                OutputRelationEmbedMatrix = new CudaPieceFloat(reader, device);
                OutputEntityEmbedMatrix = new CudaPieceFloat(reader, device);

                GRUSubQueryCell = new GRUCell(reader, device);
            }

            public void DeserializeEmbedModel(BinaryReader reader, DeviceType device)
            {
                int mEntityNum = reader.ReadInt32();
                int mRelationNum = reader.ReadInt32();
                int mDim = reader.ReadInt32();
                int mStatusHiddenDim = reader.ReadInt32();

                EntityVec = new CudaPieceFloat(reader, device);
                RelationVec = new CudaPieceFloat(reader, device);
            }
            #endregion.

            public DeepGraphEmbedingStruct(RelationGraphData graphData, int dim, int attentionEmbed, int maxHop, DeviceType device)
            {
                GraphData = graphData;

                EntityNum = GraphData.EntityNum;
                RelationNum = GraphData.RelationNum;
                Dim = dim;

                StatusHiddenDim = attentionEmbed;
                MaxHop = maxHop;

                DeviceType = device;

                // Entity Vector. encode and decode.
                EntityVec = new CudaPieceFloat(2 * EntityNum * Dim, true, false);
                EntityVec.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));
                NormEntity();

                /// indirection & outdirection.
                RelationVec = new CudaPieceFloat(2 * RelationNum * Dim, true, false);
                RelationVec.Init((float)(12.0f / Math.Sqrt(Dim)), (float)(-6.0f / Math.Sqrt(Dim)));

                /********This is the basic attention model, while, it is not very useful. *****************/

                /***********  Input Status  ********************************/
                InputEntityEmbedMatrix = new CudaPieceFloat(Dim * StatusHiddenDim, true, false);
                InputEntityEmbedMatrix.Init((float)(12.0f / Math.Sqrt(Dim + StatusHiddenDim)), (float)(-6.0f / Math.Sqrt(Dim + StatusHiddenDim)));

                InputRelationEmbedMatrix = new CudaPieceFloat(Dim * StatusHiddenDim, true, false);
                InputRelationEmbedMatrix.Init((float)(12.0f / Math.Sqrt(Dim + StatusHiddenDim)), (float)(-6.0f / Math.Sqrt(Dim + StatusHiddenDim)));

                /***********  Output Status  ********************************/
                OutputEntityEmbedMatrix = new CudaPieceFloat(Dim * StatusHiddenDim, true, false);
                OutputEntityEmbedMatrix.Init((float)(12.0f / Math.Sqrt(Dim + StatusHiddenDim)), (float)(-6.0f / Math.Sqrt(Dim + StatusHiddenDim)));

                OutputRelationEmbedMatrix = new CudaPieceFloat(Dim * StatusHiddenDim, true, false);
                OutputRelationEmbedMatrix.Init((float)(12.0f / Math.Sqrt(Dim + StatusHiddenDim)), (float)(-6.0f / Math.Sqrt(Dim + StatusHiddenDim)));

                GRUSubQueryCell = new GRUCell(Dim, Dim, DeviceType);

            }

            public DeepGraphEmbedingStruct(BinaryReader reader, DeviceType device) : base(reader, device) { }

            public void InitGraphRunners(RunnerBehavior behavior)
            {
                GraphNodeRunner = new GraphNodeRunner[EntityNum];
                for (int i = 0; i < EntityNum; i++) GraphNodeRunner[i] = new GraphNodeRunner(this, i, behavior);
            }

            protected override void InitStructureOptimizer()
            {
                StructureOptimizer.Add("EntityVec", new ModelOptimizer() { Parameter = EntityVec });
                StructureOptimizer.Add("RelationVec", new ModelOptimizer() { Parameter = RelationVec });
                StructureOptimizer.Add("InputEntityEmbedMatrix", new ModelOptimizer() { Parameter = InputEntityEmbedMatrix });
                StructureOptimizer.Add("InputRelationEmbedMatrix", new ModelOptimizer() { Parameter = InputRelationEmbedMatrix });
                StructureOptimizer.Add("OutputEntityEmbedMatrix", new ModelOptimizer() { Parameter = OutputEntityEmbedMatrix });
                StructureOptimizer.Add("OutputRelationEmbedMatrix", new ModelOptimizer() { Parameter = OutputRelationEmbedMatrix });
            }
        }

        public class QAData
        {
            /// <summary>
            /// run time data for graph node.
            /// </summary>
            public int Step;
            public HiddenBatchData Query;
            public HiddenBatchData Answer;
            public bool IsExpandGate;

            public HiddenBatchData GreenAnswer;

            #region current and next status.
            public HiddenBatchData CurrentStatus;
            public HiddenBatchData[] NextStatus;
            #endregion

            public HiddenBatchData AttentionWeight;


            public int QueryDim { get; set; }
            public int AnswerDim { get; set; }
            public int AttentionHiddenDim { get; set; }
            public int NeighborNum { get; set; }
            //public List<Tuple<GraphNodeRunner, QAData>> NeiQAData = new List<Tuple<GraphNodeRunner, QAData>>();

            public List<int> NeiSelection = new List<int>();

            public QAData() { }
            public QAData(int queryDim, int answerDim, int attentionHiddenDim, int neighborNum, RunnerBehavior behavior)
            {
                QueryDim = queryDim;
                AnswerDim = answerDim;
                AttentionHiddenDim = attentionHiddenDim;
                NeighborNum = neighborNum;

                Query = new HiddenBatchData(1, QueryDim, behavior.RunMode, behavior.Device);
                Answer = new HiddenBatchData(1, AnswerDim, behavior.RunMode, behavior.Device);
                Step = 0;
                IsExpandGate = false;

                GreenAnswer = new HiddenBatchData(1, AnswerDim, behavior.RunMode, behavior.Device);

                #region current and next status.
                CurrentStatus = new HiddenBatchData(1, AttentionHiddenDim, behavior.RunMode, behavior.Device);
                NextStatus = new HiddenBatchData[NeighborNum];
                for (int i = 0; i < NeighborNum; i++) NextStatus[i] = new HiddenBatchData(1, AttentionHiddenDim, behavior.RunMode, behavior.Device);
                #endregion

                AttentionWeight = new HiddenBatchData(1, 1 + NeighborNum, behavior.RunMode, behavior.Device);
            }
        }

        public class SimpleAnswerRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed;
            public int NodeID { get; protected set; }

            public HiddenBatchData Query { get; set; }
            public HiddenBatchData Answer { get; set; }
            public SimpleAnswerRunner(DeepGraphEmbedingStruct graphEmbed, int nodeID, HiddenBatchData query, HiddenBatchData answer, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                NodeID = nodeID;
                Query = query;
                Answer = answer;
            }

            public override void Forward()
            {
                ///Answer.BatchSize = 1;
                /// batch size will always be 1
                for (int ii = 0; ii < GraphEmbed.Dim; ii++)
                    Answer.Output.Data.MemPtr[ii] = Query.Output.Data.MemPtr[ii] + GraphEmbed.EntityVec.MemPtr[NodeID * GraphEmbed.Dim + ii];
                //ComputeLib.Add_Vector(Answer.Output.Data, Query.Output.Data, GraphEmbed.Dim, 0, 1);
                //ComputeLib.Add_Vector(Answer.Output.Data, 0, GraphEmbed.EntityVec, NodeID * GraphEmbed.Dim, GraphEmbed.Dim, 1, 1);
                //FastVector.Add(Query.Output.Data.MemPtr, 0,
                //               GraphEmbed.EntityVec.MemPtr, NodeID * GraphEmbed.Dim,
                //               Answer.Output.Data.MemPtr, 0, GraphEmbed.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.Add_Vector(Query.Deriv.Data, Answer.Deriv.Data, GraphEmbed.Dim, 1, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Answer.Deriv.Data, Answer.Deriv.Data.Size);
            }

            public override void Update()
            {
                ((SparseGradientOptimizer)GraphEmbed.EntityOptimizer).PushGradientIndex(NodeID * GraphEmbed.Dim, GraphEmbed.Dim);
                ComputeLib.Add_Vector(GraphEmbed.EntityOptimizer.Gradient, NodeID * GraphEmbed.Dim, Answer.Deriv.Data, 0, GraphEmbed.Dim, 1, GraphEmbed.EntityOptimizer.GradientStep);
            }
        }
        /*
        /// <summary>
        /// subquery = query - relation
        /// </summary>
        public class SubQueryRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed;
            public int Connection { get; set; }
            public int RelationID { get; protected set; }

            public HiddenBatchData Query { get; set; }
            public HiddenBatchData SubQuery { get; set; }
            public SubQueryRunner(DeepGraphEmbedingStruct graphEmbed, int connection, int relationId, HiddenBatchData query, HiddenBatchData subQuery, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                Connection = connection;    // +1 or -1;
                RelationID = relationId;
                Query = query;
                SubQuery = subQuery;
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                if (Connection == 1)
                    for (int ii = 0; ii < GraphEmbed.Dim; ii++)
                        SubQuery.Output.Data.MemPtr[ii] = Query.Output.Data.MemPtr[ii] - GraphEmbed.RelationVec.MemPtr[RelationID * GraphEmbed.Dim + ii];
                else
                    for (int ii = 0; ii < GraphEmbed.Dim; ii++)
                        SubQuery.Output.Data.MemPtr[ii] = Query.Output.Data.MemPtr[ii] - GraphEmbed.RelationVec.MemPtr[(GraphEmbed.RelationNum + RelationID) * GraphEmbed.Dim + ii];
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.Add_Vector(Query.Deriv.Data, SubQuery.Deriv.Data, GraphEmbed.Dim, 1, 1);
                //FastVector.Add(SubQuery.Deriv.Data.MemPtr, 0, Query.Deriv.Data.MemPtr, 0, Query.Deriv.Data.MemPtr, 0, GraphEmbed.Dim);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(SubQuery.Deriv.Data, GraphEmbed.Dim);
                //FastVector.ZeroItems(SubQuery.Deriv.Data.MemPtr, GraphEmbed.Dim);
            }

            public override void Update()
            {
                if (Connection == 1)
                {
                    ((SparseGradientOptimizer)GraphEmbed.RelationOptimizer).PushGradientIndex(RelationID * GraphEmbed.Dim, GraphEmbed.Dim);
                    ComputeLib.Add_Vector(GraphEmbed.RelationOptimizer.Gradient, RelationID * GraphEmbed.Dim,
                            SubQuery.Deriv.Data, 0, GraphEmbed.Dim, 1, - GraphEmbed.RelationOptimizer.GradientStep);
                    //FastVector.Add_Vector(GraphEmbed.RelationOptimizer.Gradient.MemPtr, RelationID * GraphEmbed.Dim,
                    //        SubQuery.Deriv.Data.MemPtr, 0, GraphEmbed.Dim, 1, -GraphEmbed.RelationOptimizer.GradientStep);
                }
                else
                {
                    ((SparseGradientOptimizer)GraphEmbed.RelationOptimizer).PushGradientIndex((RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, GraphEmbed.Dim);
                    ComputeLib.Add_Vector(GraphEmbed.RelationOptimizer.Gradient, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim,
                           SubQuery.Deriv.Data, 0, GraphEmbed.Dim, 1, -GraphEmbed.RelationOptimizer.GradientStep);
                    //FastVector.Add_Vector(GraphEmbed.RelationOptimizer.Gradient.MemPtr, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim,
                    //        SubQuery.Deriv.Data.MemPtr, 0, GraphEmbed.Dim, 1, -GraphEmbed.RelationOptimizer.GradientStep);
                }
            }
        }
        */

        public class GRUSubQueryRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed;
            public int Connection { get; set; }
            public int RelationID { get; protected set; }

            public HiddenBatchData Query { get; set; }
            public HiddenBatchData SubQuery { get; set; }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            public GRUSubQueryRunner(DeepGraphEmbedingStruct graphEmbed, int connection, int relationId, HiddenBatchData query, HiddenBatchData subQuery, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                Connection = connection;    // +1 or -1;
                RelationID = relationId;
                Query = query;
                SubQuery = subQuery;

                GateR = new HiddenBatchData(1, GraphEmbed.Dim, DNNRunMode.Train, Behavior.Device);
                GateZ = new HiddenBatchData(1, GraphEmbed.Dim, DNNRunMode.Train, Behavior.Device);
                HHat = new HiddenBatchData(1, GraphEmbed.Dim, DNNRunMode.Train, Behavior.Device);
                RestH = new HiddenBatchData(1, GraphEmbed.Dim, DNNRunMode.Train, Behavior.Device);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                if (Connection == 1)
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim, GraphEmbed.GRUSubQueryCell.Wr, 0, GateR.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 0, 1, false, false);
                else
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, GraphEmbed.GRUSubQueryCell.Wr, 0, GateR.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, GraphEmbed.GRUSubQueryCell.Ur, 0, GateR.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, GraphEmbed.GRUSubQueryCell.Br, 1, GraphEmbed.Dim);

                /*Wz X -> GateZ*/
                if (Connection == 1)
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim, GraphEmbed.GRUSubQueryCell.Wz, 0, GateZ.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 0, 1, false, false);
                else
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, GraphEmbed.GRUSubQueryCell.Wz, 0, GateZ.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, GraphEmbed.GRUSubQueryCell.Uz, 0, GateZ.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, GraphEmbed.GRUSubQueryCell.Bz, 1, GraphEmbed.Dim);

                /*Wh X -> HHat*/
                if (Connection == 1)
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim, GraphEmbed.GRUSubQueryCell.Wh, 0, HHat.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 0, 1, false, false);
                else
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, GraphEmbed.GRUSubQueryCell.Wh, 0, HHat.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 0, 1, false, false);

                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, 1, GraphEmbed.Dim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, GraphEmbed.GRUSubQueryCell.Uh, 0, HHat.Output.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, GraphEmbed.GRUSubQueryCell.Bh, 1, GraphEmbed.Dim);

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                for (int ii = 0; ii < GraphEmbed.Dim; ii++)
                    SubQuery.Output.Data.MemPtr[ii] = (1 - GateZ.Output.Data.MemPtr[ii]) * Query.Output.Data.MemPtr[ii] + GateZ.Output.Data.MemPtr[ii] * HHat.Output.Data.MemPtr[ii];
            }

            public override void Backward(bool cleanDeriv)
            {
                for (int ii = 0; ii < GraphEmbed.Dim; ii++)
                {
                    Query.Deriv.Data.MemPtr[ii] = SubQuery.Deriv.Data.MemPtr[ii] * (1 - GateZ.Output.Data.MemPtr[ii]);
                    HHat.Deriv.Data.MemPtr[ii] = SubQuery.Deriv.Data.MemPtr[ii] * GateZ.Output.Data.MemPtr[ii];
                    GateZ.Deriv.Data.MemPtr[ii] = SubQuery.Deriv.Data.MemPtr[ii] * (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii]);
                }

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, 1, GraphEmbed.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Uh, 0, RestH.Deriv.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, 1, GraphEmbed.Dim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, 1, GraphEmbed.Dim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, GraphEmbed.Dim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, GraphEmbed.Dim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Ur, 0, Query.Deriv.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Uz, 0, Query.Deriv.Data, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(SubQuery.Deriv.Data, GraphEmbed.Dim);
            }

            public override void Update()
            {
                if (Connection == 1)
                {
                    ((SparseGradientOptimizer)GraphEmbed.RelationOptimizer).PushGradientIndex(RelationID * GraphEmbed.Dim, GraphEmbed.Dim);
                    ComputeLib.Sgemm(HHat.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Wh, 0, GraphEmbed.RelationOptimizer.Gradient, RelationID * GraphEmbed.Dim, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);
                    ComputeLib.Sgemm(GateR.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Wr, 0, GraphEmbed.RelationOptimizer.Gradient, RelationID * GraphEmbed.Dim, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);
                    ComputeLib.Sgemm(GateZ.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Wz, 0, GraphEmbed.RelationOptimizer.Gradient, RelationID * GraphEmbed.Dim, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);

                    ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim, HHat.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.WhMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.WhMatrixOptimizer.GradientStep, true, false);
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim, GateR.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.WrMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.WrMatrixOptimizer.GradientStep, true, false);
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim, GateZ.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.WzMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.WzMatrixOptimizer.GradientStep, true, false);
                }
                else
                {
                    ((SparseGradientOptimizer)GraphEmbed.RelationOptimizer).PushGradientIndex((RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, GraphEmbed.Dim);
                    ComputeLib.Sgemm(HHat.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Wh, 0, GraphEmbed.RelationOptimizer.Gradient, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);
                    ComputeLib.Sgemm(GateR.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Wr, 0, GraphEmbed.RelationOptimizer.Gradient, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);
                    ComputeLib.Sgemm(GateZ.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.Wz, 0, GraphEmbed.RelationOptimizer.Gradient, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);

                    ComputeLib.Sgemm(GraphEmbed.RelationVec, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, HHat.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.WhMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.WhMatrixOptimizer.GradientStep, true, false);
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, GateR.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.WrMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.WrMatrixOptimizer.GradientStep, true, false);
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, GateZ.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.WzMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.WzMatrixOptimizer.GradientStep, true, false);
                }

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.UhMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.UrMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, GraphEmbed.GRUSubQueryCell.UzMatrixOptimizer.Gradient, 0, 1, GraphEmbed.Dim, GraphEmbed.Dim, 1, GraphEmbed.GRUSubQueryCell.UzMatrixOptimizer.GradientStep, true, false);
            }
        }

        /// <summary>
        /// Input State Embedding.
        /// </summary>
        public class InputStateRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed;
            public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            public int EntityID { get; set; }

            public InputStateRunner(DeepGraphEmbedingStruct graphEmbed, int entityID, HiddenBatchData inputQuery,
                HiddenBatchData outputEmbedding, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                EntityID = entityID;
                Input = inputQuery;
                Output = outputEmbedding;
            }

            public override void Forward()
            {
                Output.BatchSize = 1;

                ComputeLib.Sgemm(GraphEmbed.EntityVec, EntityID * GraphEmbed.Dim,
                                 GraphEmbed.InputEntityEmbedMatrix, 0,
                                 Output.Output.Data, 0,
                                 1, GraphEmbed.Dim, Output.Dim, 0, 1, false, false);

                ComputeLib.Sgemm(Input.Output.Data, 0,
                                 GraphEmbed.InputRelationEmbedMatrix, 0,
                                 Output.Output.Data, 0,
                                 1, Input.Dim, Output.Dim, 1, 1, false, false);

                ComputeLib.Tanh(Output.Output.Data, 0, Output.Output.Data, 0, Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.DerivTanh(Output.Output.Data, 0, Output.Deriv.Data, 0, Output.Deriv.Data, 0, Output.Dim, 0, 1);

                ComputeLib.Sgemm(Output.Deriv.Data, 0,
                                 GraphEmbed.InputRelationEmbedMatrix, 0,
                                 Input.Deriv.Data, 0,
                                 1, Output.Dim, Input.Dim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Deriv.Data.Size);
            }

            public override void Update()
            {
                ComputeLib.Sgemm(Input.Output.Data, 0,
                                 Output.Deriv.Data, 0,
                                 GraphEmbed.InputRelationEmbedOptimizer.Gradient, 0, 1, Input.Dim, Output.Dim,
                                 1, GraphEmbed.InputRelationEmbedOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(GraphEmbed.EntityVec, EntityID * GraphEmbed.Dim,
                                 Output.Deriv.Data, 0,
                                 GraphEmbed.InputEntityEmbedOptimizer.Gradient, 0, 1, GraphEmbed.Dim, Output.Dim,
                                 1, GraphEmbed.InputEntityEmbedOptimizer.GradientStep, true, false);

                ((SparseGradientOptimizer)GraphEmbed.EntityOptimizer).PushGradientIndex(EntityID * GraphEmbed.Dim, GraphEmbed.Dim);
                ComputeLib.Sgemm(Output.Deriv.Data, 0,
                                 GraphEmbed.InputEntityEmbedMatrix, 0,
                                 GraphEmbed.EntityOptimizer.Gradient, EntityID * GraphEmbed.Dim,
                                 1, Output.Dim, GraphEmbed.Dim,
                                 1, GraphEmbed.EntityOptimizer.GradientStep, false, true);
            }
        }

        /// <summary>
        /// Next State Embedding.
        /// </summary>
        public class NextStateRunner : StructRunner
        {
            public DeepGraphEmbedingStruct GraphEmbed { get; set; }

            public int EntityID { get; set; }
            public int RelationID { get; set; }
            public int Connection { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            public NextStateRunner(DeepGraphEmbedingStruct graphEmbed, int entityId, int connection, int relationID,
                HiddenBatchData output, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Output = output;
                GraphEmbed = graphEmbed;
                EntityID = entityId;
                RelationID = relationID;
                Connection = connection;
            }

            public override void Forward()
            {

                ComputeLib.Sgemm(GraphEmbed.EntityVec, EntityID * GraphEmbed.Dim,
                                 GraphEmbed.OutputEntityEmbedMatrix, 0,
                                 Output.Output.Data, 0,
                                 1, GraphEmbed.Dim, GraphEmbed.StatusHiddenDim, 0, 1, false, false);

                if (Connection == 1)
                {
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim,
                                     GraphEmbed.OutputRelationEmbedMatrix, 0,
                                     Output.Output.Data, 0,
                                     1, GraphEmbed.Dim, GraphEmbed.StatusHiddenDim, 1, 1, false, false);
                }
                else
                {
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim,
                                     GraphEmbed.OutputRelationEmbedMatrix, 0,
                                     Output.Output.Data, 0,
                                     1, GraphEmbed.Dim, GraphEmbed.StatusHiddenDim, 1, 1, false, false);
                }

                ComputeLib.Tanh(Output.Output.Data, 0, Output.Output.Data, 0, Output.Dim);
            }


            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Deriv.Data.Size);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.DerivTanh(Output.Output.Data, 0, Output.Deriv.Data, 0, Output.Deriv.Data, 0, Output.Dim, 0, 1);
            }

            public override void Update()
            {
                ComputeLib.Sgemm(GraphEmbed.EntityVec, EntityID * GraphEmbed.Dim,
                                 Output.Deriv.Data, 0,
                                 GraphEmbed.OutputEntityEmbedOptimizer.Gradient, 0,
                                 1, GraphEmbed.Dim, GraphEmbed.StatusHiddenDim, 1, GraphEmbed.OutputEntityEmbedOptimizer.GradientStep, true, false);

                ((SparseGradientOptimizer)GraphEmbed.EntityOptimizer).PushGradientIndex(EntityID * GraphEmbed.Dim, GraphEmbed.Dim);
                ComputeLib.Sgemm(Output.Deriv.Data, 0,
                                 GraphEmbed.OutputEntityEmbedMatrix, 0,
                                 GraphEmbed.EntityOptimizer.Gradient, EntityID * GraphEmbed.Dim,
                                 1, Output.Dim, GraphEmbed.Dim, 1, GraphEmbed.EntityOptimizer.GradientStep, false, true);

                if (Connection == 1)
                {
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, RelationID * GraphEmbed.Dim,
                                     Output.Deriv.Data, 0,
                                     GraphEmbed.OutputRelationEmbedOptimizer.Gradient, 0,
                                     1, GraphEmbed.Dim, GraphEmbed.StatusHiddenDim,
                                     1, GraphEmbed.OutputRelationEmbedOptimizer.GradientStep, true, false);

                    ((SparseGradientOptimizer)GraphEmbed.RelationOptimizer).PushGradientIndex(RelationID * GraphEmbed.Dim, GraphEmbed.Dim);
                    ComputeLib.Sgemm(Output.Deriv.Data, 0,
                                     GraphEmbed.OutputRelationEmbedMatrix, 0,
                                     GraphEmbed.RelationOptimizer.Gradient, RelationID * GraphEmbed.Dim,
                                     1, Output.Dim, GraphEmbed.Dim, 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);
                }
                else
                {
                    ComputeLib.Sgemm(GraphEmbed.RelationVec, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim,
                                     Output.Deriv.Data, 0,
                                     GraphEmbed.OutputRelationEmbedOptimizer.Gradient, 0,
                                     1, GraphEmbed.Dim, GraphEmbed.StatusHiddenDim,
                                     1, GraphEmbed.OutputRelationEmbedOptimizer.GradientStep, true, false);

                    ((SparseGradientOptimizer)GraphEmbed.RelationOptimizer).PushGradientIndex((RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim, GraphEmbed.Dim);
                    ComputeLib.Sgemm(Output.Deriv.Data, 0,
                                     GraphEmbed.OutputRelationEmbedMatrix, 0,
                                     GraphEmbed.RelationOptimizer.Gradient, (RelationID + GraphEmbed.RelationNum) * GraphEmbed.Dim,
                                     1, Output.Dim, GraphEmbed.Dim, 1, GraphEmbed.RelationOptimizer.GradientStep, false, true);
                }
            }
        }

        public class TransSimilarityRunner : StructRunner
        {
            HiddenBatchData InputA { get; set; }
            HiddenBatchData InputB { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            int OutputIdx { get; set; }

            float L2ASquare { get; set; }
            float L2BSquare { get; set; }

            float Bias { get; set; }
            SimilarityType SimType { get; set; }
            public TransSimilarityRunner(HiddenBatchData inputA, HiddenBatchData inputB, float bias, SimilarityType simType,
                HiddenBatchData output, int outputIdx, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                InputA = inputA;
                InputB = inputB;
                Output = output;
                OutputIdx = outputIdx;
                Bias = bias;
                SimType = simType;
            }

            public override void Forward()
            {
                switch (SimType)
                {
                    case SimilarityType.InnerProduct:
                        Output.Output.Data.MemPtr[OutputIdx] = Bias + 
                            FastVector.DotProduct(InputA.Output.Data.MemPtr, InputB.Output.Data.MemPtr, InputA.Output.Data.Size);

                            //FastVector.Create(InputA.Output.Data.MemPtr).DotProduct(InputB.Output.Data.MemPtr);
                        break;
                    case SimilarityType.CosineSimilarity:
                        L2ASquare = FastVector.L2Norm(InputA.Output.Data.MemPtr, 0, InputA.Output.Data.Size) + Util.GPUEpsilon;
                            //FastVector.Create(InputA.Output.Data.MemPtr).L2Norm + Util.GPUEpsilon;
                        L2BSquare = FastVector.L2Norm(InputB.Output.Data.MemPtr, 0, InputB.Output.Data.Size) + Util.GPUEpsilon;
                            //FastVector.Create(InputB.Output.Data.MemPtr).L2Norm + Util.GPUEpsilon;
                        float dp = FastVector.DotProduct(InputA.Output.Data.MemPtr, InputB.Output.Data.MemPtr, InputA.Output.Data.Size);

                        //L2ASquare = FastVector.Create(InputA.Output.Data.MemPtr).L2Norm + Util.GPUEpsilon;
                        //L2BSquare = FastVector.Create(InputB.Output.Data.MemPtr).L2Norm + Util.GPUEpsilon;
                        //float dp = FastVector.Create(InputA.Output.Data.MemPtr).DotProduct(FastVector.Create(InputB.Output.Data.MemPtr));
                        Output.Output.Data.MemPtr[OutputIdx] = Bias + dp / (L2ASquare * L2BSquare);
                        break;
                }
            }

            public override void CleanDeriv()
            {
                Output.Deriv.Data.MemPtr[OutputIdx] = 0;
            }

            /// <summary>
            /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                switch (SimType)
                {
                    case SimilarityType.InnerProduct:
                        FastVector.AddScale(Output.Deriv.Data.MemPtr[OutputIdx], InputB.Output.Data.MemPtr, InputA.Deriv.Data.MemPtr, InputB.Output.Data.Size); 
                        FastVector.AddScale(Output.Deriv.Data.MemPtr[OutputIdx], InputA.Output.Data.MemPtr, InputB.Deriv.Data.MemPtr, InputA.Output.Data.Size); 

                        //FastVector.Create(InputA.Deriv.Data.MemPtr).AddScale(Output.Deriv.Data.MemPtr[OutputIdx], InputB.Output.Data.MemPtr);
                        //FastVector.Create(InputB.Deriv.Data.MemPtr).AddScale(Output.Deriv.Data.MemPtr[OutputIdx], InputA.Output.Data.MemPtr);
                        break;
                    case SimilarityType.CosineSimilarity:
                        float a = Output.Output.Data.MemPtr[OutputIdx] - Bias;
                        float b = L2ASquare;
                        float c = L2BSquare;

                        for (int i = 0; i < InputA.Dim; i++)
                        {
                            float aDeriv = (InputB.Output.Data.MemPtr[i] / (b * c) - InputA.Output.Data.MemPtr[i] * a / (b * b * b * c));
                            float bDeriv = (InputA.Output.Data.MemPtr[i] / (b * c) - InputB.Output.Data.MemPtr[i] * a / (b * c * c * c));

                            InputA.Deriv.Data.MemPtr[i] = Output.Deriv.Data.MemPtr[OutputIdx] * aDeriv;
                            InputB.Deriv.Data.MemPtr[i] = Output.Deriv.Data.MemPtr[OutputIdx] * bDeriv;
                        }
                        break;
                }
            }
        }

        public class AdditionRunner : StructRunner
        {
            HiddenBatchData Dst { get; set; }
            HiddenBatchData Src { get; set; }
            float Alpha { get; set; }
            float Beta { get; set; }

            HiddenBatchData BetaWeight = null;
            int BetaIdx;

            public AdditionRunner(HiddenBatchData dst, HiddenBatchData src, float alpha, HiddenBatchData beta, int betaIdx, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Dst = dst;
                Src = src;
                Alpha = alpha;
                BetaWeight = beta;
                BetaIdx = betaIdx;
            }

            public override void Forward()
            {
                //Dst.BatchSize = 1;
                if (BetaWeight != null) Beta = BetaWeight.Output.Data.MemPtr[BetaIdx];
                ComputeLib.Add_Vector(Dst.Output.Data, Src.Output.Data, Dst.Dim, Alpha, Beta);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Dst.Deriv.Data, Dst.Deriv.Data.Size);
            }

            /// <summary>
            /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                if (BetaWeight != null) BetaWeight.Deriv.Data.MemPtr[BetaIdx] += 
                    FastVector.DotProduct(Src.Output.Data.MemPtr, Dst.Deriv.Data.MemPtr, Src.Output.Data.Size);

                    //FastVector.Create(Dst.Deriv.Data.MemPtr).DotProduct(Src.Output.Data.MemPtr);
                FastVector.AddScale(Beta, Dst.Deriv.Data.MemPtr, Src.Deriv.Data.MemPtr, Src.Deriv.Data.Size); 
                //FastVector.Create(Src.Deriv.Data.MemPtr).AddScale(Beta, Dst.Deriv.Data.MemPtr);
            }
        }

        public class SoftmaxRunner : StructRunner
        {
            public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            int[] InputSelect { get; set; }
            float Gamma { get; set; }

            public SoftmaxRunner(HiddenBatchData input, int[] inputSelect, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Output = input;
                Gamma = gamma;
                InputSelect = inputSelect;
            }

            public override void Forward()
            {
                Util.Softmax(Input.Output.Data.MemPtr, InputSelect, Output.Output.Data.MemPtr, Gamma);
            }

            /// <summary>
            /// cleanDeriv: 0, lastDeriv = Weight * Deriv; 1, lastDeriv = lastDeriv + Weight * Deriv;
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                float tmpSum1 = 0;
                for (int i = 0; i < InputSelect.Length; i++)
                {
                    int idx = InputSelect[i];
                    tmpSum1 += Input.Output.Data.MemPtr[idx] * Output.Deriv.Data.MemPtr[idx];
                }
                for (int i = 0; i < InputSelect.Length; i++)
                {
                    int idx = InputSelect[i];
                    Input.Deriv.Data.MemPtr[idx] = Gamma * Input.Output.Data.MemPtr[idx] * (Output.Deriv.Data.MemPtr[idx] - tmpSum1);
                }
            }
        }

        public class GraphNodeRunner : StructRunner
        {
            DeepGraphEmbedingStruct GraphEmbed;
            public int NodeID { get; protected set; }
            public QAData Data { get; set; }

            ComputationGraph ExecuteCG { get; set; }

            SimpleAnswerRunner FinalAnswerRunner = null;
            SimpleAnswerRunner GreenAnswerRunner = null;
            InputStateRunner CurrentStatusRunner = null;
            List<Tuple<int, NextStateRunner, TransSimilarityRunner>> NextStatusRunners = new List<Tuple<int, NextStateRunner, TransSimilarityRunner>>();

            float Gamma = 10.0f;

            public GraphNodeRunner(DeepGraphEmbedingStruct graphEmbed, int nodeID, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GraphEmbed = graphEmbed;
                NodeID = nodeID;
                Data = new QAData(GraphEmbed.Dim, GraphEmbed.Dim, GraphEmbed.StatusHiddenDim, GraphEmbed.GraphData.TrainNeighborLink[nodeID].Count, Behavior);

                FinalAnswerRunner = new SimpleAnswerRunner(GraphEmbed, NodeID, Data.Query, Data.Answer, Behavior);
                GreenAnswerRunner = new SimpleAnswerRunner(GraphEmbed, NodeID, Data.Query, Data.GreenAnswer, Behavior);
                CurrentStatusRunner = new InputStateRunner(GraphEmbed, NodeID, Data.Query, Data.CurrentStatus, Behavior);

                int linkIdx = 0;
                foreach (Tuple<int, int, int> connection in GraphEmbed.GraphData.TrainNeighborLink[NodeID])
                {
                    NextStatusRunners.Add(new Tuple<int, NextStateRunner, TransSimilarityRunner>(linkIdx,
                        new NextStateRunner(GraphEmbed, connection.Item3, connection.Item1, connection.Item2, Data.NextStatus[linkIdx], Behavior),
                        new TransSimilarityRunner(Data.CurrentStatus, Data.NextStatus[linkIdx], 0, SimilarityType.CosineSimilarity, Data.AttentionWeight, linkIdx + 1, Behavior)));
                    linkIdx += 1;
                }

                Data.AttentionWeight.Output.Data.MemPtr[0] = 0.6f;
                //QueryHiddenRunner = new QueryProjectionRunner(GraphEmbed, Data.Query, Data.QueryHidden, Behavior);
                //QueryActivationRunner = new ActivationRunner(Data.QueryHidden, A_Func.Tanh, Behavior);
                //QueryAttentionRunner = new DotProductRunner(Data.QueryHidden, GraphEmbed.OutputAttentionVec, GraphEmbed.OutputEmbedOptimizer, Data.AttentionWeight, 0, 1, Behavior);
            }

            public override void Forward()
            {
                GraphEmbed.AddBlackNode(NodeID);

                bool isTerminal = false;
                #region Terminal Conditions.
                if (Data.Step >= GraphEmbed.MaxHop) isTerminal = true;
                //if (Math.Pow(GraphEmbed.ExploreDiscount, Data.Step) < ParameterSetting.Random.NextDouble()) isTerminal = true;
                //if (ComputeLib.L2Norm(Data.Query.Output.Data, Data.Query.Dim) < 0.01) isTerminal = true;
                #endregion.

                ExecuteCG = new ComputationGraph();

                if (isTerminal)
                {
                    FinalAnswerRunner.Forward();
                    ExecuteCG.AddRunner(FinalAnswerRunner);
                    Data.IsExpandGate = false;
                    return;
                }
                else
                {
                    GreenAnswerRunner.Forward();
                    ExecuteCG.AddRunner(GreenAnswerRunner);
                    Data.IsExpandGate = true;
                }

                CurrentStatusRunner.Forward();
                ExecuteCG.AddRunner(CurrentStatusRunner);

                if (ParameterSetting.Random.NextDouble() >= GraphEmbed.Eps)
                {
                    MinMaxHeap<int> minmaxHeap = new MinMaxHeap<int>(GraphEmbed.MaxNeighbor, 1);
                    //select the best candidate.
                    for (int mn = 0; mn < NextStatusRunners.Count; mn++)
                    {
                        Tuple<int, int, int> connection = GraphEmbed.GraphData.TrainNeighborLink[NodeID][mn];
                        if ((connection.Item1 > 0 && GraphEmbed.IsBlackConnection(NodeID, connection.Item2, connection.Item3)) ||
                        (connection.Item1 < 0 && GraphEmbed.IsBlackConnection(connection.Item3, connection.Item2, NodeID))) continue;
                        if (GraphEmbed.blackNodeList.Contains(connection.Item3)) continue;

                        /// neighbor status embedding.
                        NextStatusRunners[mn].Item2.Forward();
                        NextStatusRunners[mn].Item3.Forward();
                        minmaxHeap.push_pair(mn, Data.AttentionWeight.Output.Data.MemPtr[mn + 1]);
                    }

                    Data.NeiSelection.Clear();
                    for (int mn = 0; mn < minmaxHeap.topK.Count; mn++)
                    {
                        int linkID = minmaxHeap.topK[mn].Key;
                        Tuple<int, int, int> connection = GraphEmbed.GraphData.TrainNeighborLink[NodeID][linkID];
                        if (GraphEmbed.blackNodeList.Contains(connection.Item3)) continue;

                        GraphEmbed.AddBlackNode(connection.Item3);

                        ExecuteCG.AddRunner(NextStatusRunners[linkID].Item2);
                        ExecuteCG.AddRunner(NextStatusRunners[linkID].Item3);
                        Data.NeiSelection.Add(linkID);
                    }
                }
                else
                {
                    //select random candidate.
                    DataRandomShuffling randomSelect = new DataRandomShuffling(NextStatusRunners.Count);
                    Data.NeiSelection.Clear();
                    while (Data.NeiSelection.Count < GraphEmbed.MaxNeighbor)
                    {
                        int neighborId = randomSelect.RandomNext();
                        if (neighborId == -1) break;
                        Tuple<int, int, int> connection = GraphEmbed.GraphData.TrainNeighborLink[NodeID][neighborId];
                        if ((connection.Item1 > 0 && GraphEmbed.IsBlackConnection(NodeID, connection.Item2, connection.Item3)) ||
                        (connection.Item1 < 0 && GraphEmbed.IsBlackConnection(connection.Item3, connection.Item2, NodeID))) continue;
                        if (GraphEmbed.blackNodeList.Contains(connection.Item3)) continue;

                        GraphEmbed.AddBlackNode(connection.Item3);

                        NextStatusRunners[neighborId].Item2.Forward();
                        NextStatusRunners[neighborId].Item3.Forward();

                        ExecuteCG.AddRunner(NextStatusRunners[neighborId].Item2);
                        ExecuteCG.AddRunner(NextStatusRunners[neighborId].Item3);
                        Data.NeiSelection.Add(neighborId);
                    }
                }

                //-->
                //QueryActivationRunner.Forward();
                //QueryAttentionRunner.Forward();
                //ExecuteCG.AddRunner(QueryActivationRunner);
                //ExecuteCG.AddRunner(QueryAttentionRunner);
                // do something here.
                SoftmaxRunner softmaxRunner = new SoftmaxRunner(Data.AttentionWeight,
                    Enumerable.Range(0, Data.NeiSelection.Count + 1).Select(i => i == 0 ? 0 : Data.NeiSelection[i - 1] + 1).ToArray(), Gamma, Behavior);
                softmaxRunner.Forward();
                ExecuteCG.AddRunner(softmaxRunner);

                for (int idx = 0; idx < Data.NeiSelection.Count; idx++)
                {
                    int linkID = Data.NeiSelection[idx];
                    Tuple<int, int, int> connection = GraphEmbed.GraphData.TrainNeighborLink[NodeID][linkID];

                    GraphNodeRunner subRunner = GraphEmbed.GraphNodeRunner[connection.Item3];
                    subRunner.Data.Step = Data.Step + 1;
                    subRunner.Data.Query.BatchSize = 1;
                    subRunner.Forward();
                    ExecuteCG.AddRunner(subRunner);

                    //SubQueryRunner subQueryRunner = new SubQueryRunner(GraphEmbed, connection.Item1, connection.Item2, Data.Query, subRunner.Data.Query, Behavior);
                    GRUSubQueryRunner subQueryRunner = new GRUSubQueryRunner(GraphEmbed, connection.Item1, connection.Item2, Data.Query, subRunner.Data.Query, Behavior);
                    subQueryRunner.Forward();

                    ExecuteCG.AddRunner(subQueryRunner);
                }

                AdditionRunner queryAnswerRunner = new AdditionRunner(Data.Answer, Data.GreenAnswer, 0, Data.AttentionWeight, 0, Behavior);
                queryAnswerRunner.Forward();
                ExecuteCG.AddRunner(queryAnswerRunner);

                for (int i = 0; i < Data.NeiSelection.Count; i++)
                {
                    int linkID = Data.NeiSelection[i];
                    Tuple<int, int, int> connection = GraphEmbed.GraphData.TrainNeighborLink[NodeID][linkID];
                    GraphNodeRunner subRunner = GraphEmbed.GraphNodeRunner[connection.Item3];

                    AdditionRunner relationAnswerRunner = new AdditionRunner(Data.Answer, subRunner.Data.Answer, 1, Data.AttentionWeight, linkID + 1, Behavior);
                    relationAnswerRunner.Forward();
                    ExecuteCG.AddRunner(relationAnswerRunner);
                }
                return;
            }

            public override void Backward(bool cleanDeriv)
            {
                ExecuteCG.Backward();
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Data.AttentionWeight.Deriv.Data, Data.AttentionWeight.Dim);
                ExecuteCG.CleanDeriv();
            }

            public override void Update()
            {
                ExecuteCG.Update();
            }
        }

        public class DataPanel
        {
            public static RelationGraphData knowledgeGraph;

            public static int EntityNum { get { return knowledgeGraph.EntityId.Count; } }
            public static int RelationNum { get { return knowledgeGraph.RelationId.Count; } }

            public static List<Tuple<int, int, int>> Train
            {
                get { return knowledgeGraph.Train; }
            }

            public static List<Tuple<int, int, int>> Test
            {
                get { return knowledgeGraph.Test; }
            }

            public static List<Tuple<int, int, int>> Valid
            {
                get { return knowledgeGraph.Valid; }
            }


            public static double[] HeadProb { get { return knowledgeGraph.HeadProb; } }

            public static double[] TailProb { get { return knowledgeGraph.TailProb; } }

            public static bool IsInGraph(int eid1, int eid2, int rid)
            {
                if (knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
                else return false;
            }

            public static string GraphAnalysis(int maxHop)
            {
                /********** Graph analysis **************/
                Dictionary<int, int> pathDistr = new Dictionary<int, int>();
                for (int i = -1; i <= maxHop; i++) pathDistr[i] = 0;
                for (int itemIdx = 0; itemIdx < knowledgeGraph.Train.Count; itemIdx++)
                {
                    int head = knowledgeGraph.Train[itemIdx].Item1;
                    int tail = knowledgeGraph.Train[itemIdx].Item2;
                    int r = knowledgeGraph.Train[itemIdx].Item3;

                    int m = knowledgeGraph.ShortestPath(head, tail, r, maxHop);
                    pathDistr[m] += 1;
                }
                return string.Join("\n", pathDistr.Select(i => i.Key + ":" + i.Value));
            }

            public static string GraphTestAnalysis(int maxHop)
            {
                Dictionary<int, int> pathDistr = new Dictionary<int, int>();
                for (int i = -1; i <= maxHop; i++) pathDistr[i] = 0;
                for (int itemIdx = 0; itemIdx < knowledgeGraph.Test.Count; itemIdx++)
                {
                    int head = knowledgeGraph.Test[itemIdx].Item1;
                    int tail = knowledgeGraph.Test[itemIdx].Item2;
                    int r = knowledgeGraph.Test[itemIdx].Item3;

                    int m = knowledgeGraph.ShortestPath(head, tail, r, maxHop);
                    pathDistr[m] += 1;
                }
                return string.Join("\n", pathDistr.Select(i => i.Key + ":" + i.Value));
            }

            public static void Init()
            {
                knowledgeGraph = new RelationGraphData();

                knowledgeGraph.EntityId = RelationGraphData.LoadMapId(BuilderParameters.Entity2Id);
                knowledgeGraph.RelationId = RelationGraphData.LoadMapId(BuilderParameters.Relation2Id);

                knowledgeGraph.Train = knowledgeGraph.LoadGraph(BuilderParameters.TrainData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, true);
                knowledgeGraph.Valid = knowledgeGraph.LoadGraph(BuilderParameters.ValidData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
                knowledgeGraph.Test = knowledgeGraph.LoadGraph(BuilderParameters.TestData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
            }

            public static List<HashSet<int>> Neighbor1Set = new List<HashSet<int>>();
            public static List<HashSet<int>> Neighbor2Set = new List<HashSet<int>>();
            public static List<HashSet<int>> Neighbor3Set = new List<HashSet<int>>();

            public static void InitNeighborSet()
            {
                for (int i = 0; i < knowledgeGraph.EntityNum; i++)
                {
                    Neighbor1Set.Add(knowledgeGraph.NeighborSet(i, 1));
                    Neighbor2Set.Add(knowledgeGraph.NeighborSet(i, 2));
                    Neighbor3Set.Add(knowledgeGraph.NeighborSet(i, 3));
                }
            }

        }
    }
}
