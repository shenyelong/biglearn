﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class Seq2SeqQABuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "SRC Train Data."));
                Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "TGT Train Data."));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));

                Argument.Add("DEV-SRC", new ParameterArgument(string.Empty, "SRC Dev Dataset."));
                Argument.Add("DEV-TGT", new ParameterArgument(string.Empty, "TGT Dev Dataset."));

                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("LAYER-ATTENTION", new ParameterArgument("1", "Layer Attention"));
                Argument.Add("ATT-HIDDEN", new ParameterArgument("64", "Attention Layer Hidden Dim"));

                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
                Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            public static string TrainSrcData { get { return Argument["TRAIN-SRC"].Value; } }
            public static string TrainTgtData { get { return Argument["TRAIN-TGT"].Value; } }
            public static bool IsTrainFile { get { return (!TrainSrcData.Equals(string.Empty)) && (!TrainTgtData.Equals(string.Empty)); } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

            public static string DevSrcData { get { return Argument["DEV-SRC"].Value; } }
            public static string DevTgtData { get { return Argument["DEV-TGT"].Value; } }
            public static bool IsDevFile { get { return (!DevSrcData.Equals(string.Empty)) && (!DevTgtData.Equals(string.Empty)); } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_ATTENTION { get { return Argument["LAYER-ATTENTION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string Vocab { get { return Argument["VOCAB"].Value; } }
            public static Vocab2Freq mVocabDict = null;
            public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
            public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
            public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.SEQ2SEQ_QA; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> AddLSTMEncoder(ComputationGraph cg, LSTMStructure lstmModel, SeqSparseBatchData input, bool isReverse, //SeqHelpData inputHelp,
            RunnerBehavior behavior)
        {
            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> memory = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
            /**************** Source LSTM Encoder *********/
            FastLSTMSparseRunner<SeqSparseBatchData> encoderRunner = new FastLSTMSparseRunner<SeqSparseBatchData>(lstmModel.LSTMCells[0], input, isReverse, behavior);
            SeqDenseRecursiveData SrclstmOutput = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);
            memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(encoderRunner.Output, encoderRunner.C));
            /***************** MultiLayer LSTM Encoder ******************************/
            for (int i = 1; i < lstmModel.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> hEncoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], SrclstmOutput, behavior);
                SrclstmOutput = (SeqDenseRecursiveData)cg.AddRunner(hEncoderRunner);
                memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(hEncoderRunner.Output, hEncoderRunner.C));
            }
            return memory;
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData, 
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             //IDataCashier<DenseBatchData, DenseDataStat> mapData,
            LSTMStructure d1Encoder, LSTMStructure d2Encoder, LSTMStructure decoder, EmbedStructure embed, 
            List<MLPAttentionStructure> attentions, CompositeNNStructure Model, RunnerBehavior Behavior, EvaluationType evalType = EvaluationType.PERPLEXITY)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));

            //DenseBatchData MapData = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(mapData, Behavior));

            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));
            //SeqHelpData TgtHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(TgtData.SequenceData, Behavior));

            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD1Memory = AddLSTMEncoder(cg, d1Encoder, SrcData.SequenceData, false, Behavior);
            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD2Memory = AddLSTMEncoder(cg, d2Encoder, SrcData.SequenceData, true, Behavior);

            /**************** Bidirection Source LSTM Encoder Ensemble *********/
            List<SeqDenseBatchData> SrcEnsembleMemory = new List<SeqDenseBatchData>();
            List<Tuple<HiddenBatchData, HiddenBatchData>> SrcEnsembleStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
            for (int i = 0; i < SrcD1Memory.Count; i++)
            {
                HiddenBatchData SrcD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item1, true, 0, SrcD1Memory[i].Item1.MapForward, Behavior));
                HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item1, false, 0, SrcD2Memory[i].Item1.MapForward, Behavior));

                HiddenBatchData SrcD1C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item2, true, 0, SrcD1Memory[i].Item2.MapForward, Behavior));
                HiddenBatchData SrcD2C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item2, false, 0, SrcD2Memory[i].Item2.MapForward, Behavior));

                HiddenBatchData SrcO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1O, SrcD2O }, Behavior));
                HiddenBatchData SrcC = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1C, SrcD2C }, Behavior));
                SrcEnsembleStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(SrcO, SrcC));

                SeqDenseBatchData SrcSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item1, Behavior));
                SeqDenseBatchData SrcSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item1, Behavior));
                
                //SeqDenseBatchData SrcSeqD1C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item2, Behavior));
                //SeqDenseBatchData SrcSeqD2C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item2, Behavior));

                SeqDenseBatchData SrcSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1O, SrcSeqD2O }, Behavior));
                //SeqDenseBatchData SrcSeqC = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1C, SrcSeqD2C }, Behavior));

                SrcEnsembleMemory.Add(SrcSeqO);
            }

            if (Behavior.RunMode == DNNRunMode.Train || evalType == EvaluationType.PERPLEXITY)
            {
                List<MLPAttentionV2Runner> tgtAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                    tgtAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i], BuilderParameters.TgtSeqMaxLen, 
                        TgtData.SequenceData.Stat.MAX_BATCHSIZE, TgtData.SequenceData.Stat.MAX_SEQUENCESIZE, Behavior) : null);

                /**************** Target LSTM Decoder *********/
                SeqDenseRecursiveData TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(decoder.LSTMCells[0],
                    TgtData.SequenceData, false, SrcEnsembleStatus[0].Item1, SrcEnsembleStatus[0].Item2, tgtAttentionRunners[0], Behavior));
                for (int i = 1; i < decoder.LSTMCells.Count; i++)
                    TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(decoder.LSTMCells[i],
                        TgtlstmOutput, SrcEnsembleStatus[i].Item1, SrcEnsembleStatus[i].Item2, tgtAttentionRunners[i], Behavior));

                if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                    cg.AddRunner(new ModelDiskDumpRunner(Model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\Seq2Seq"));

                if (Behavior.RunMode == DNNRunMode.Train)
                    cg.AddObjective(new EmbedFullySoftmaxRunner(embed, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
                else
                    cg.AddRunner(new EmbedFullySoftmaxRunner(embed, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath, Behavior));
            }
            else
            {
                List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                    decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i],  TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior)  : null);

                // the probability of exactly generate the true tgt.
                LSTMDecodingRunner decodeRunner = new LSTMDecodingRunner(decoder, embed, SrcEnsembleStatus,
                    decodeAttentionRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), Behavior, true);

                EnsembleBeamSearchRunner beamSearchRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
                    BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, embed.VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);

                beamSearchRunner.AddBeamSearch(decodeRunner);
                cg.AddRunner(beamSearchRunner);

                switch (evalType)
                {
                    case EvaluationType.ACCURACY:
                        cg.AddRunner(new BeamAccuracyRunner(beamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                        break;
                    case EvaluationType.BSBLEU:
                        cg.AddRunner(new BLEUDiskDumpRunner(beamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                        break;
                    case EvaluationType.ROUGE:
                        cg.AddRunner(new ROUGEDiskDumpRunner(beamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                        break;
                    case EvaluationType.TOPKBEAM:
                        cg.AddRunner(new TopKBeamRunner(beamSearchRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                        break;
                }
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            int aDirectLSTM = 1;
            int bDirectLSTM = 1;

            int ensemble = aDirectLSTM + bDirectLSTM;

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            LSTMStructure encodeLSTMStructure = null;
            LSTMStructure reverseEncodeLSTMStructure = null;
            LSTMStructure decodeLSTMStructure = null;
            EmbedStructure embedStructure = null;
            List<MLPAttentionStructure> attentionStructures = new List<MLPAttentionStructure>();

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                encodeLSTMStructure = new LSTMStructure(DataPanel.TrainSrcBin.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM.Select(i => i / ensemble).ToArray(), DeviceType.GPU);
                reverseEncodeLSTMStructure = new LSTMStructure(DataPanel.TrainSrcBin.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM.Select(i => i / ensemble).ToArray(), DeviceType.GPU);
                decodeLSTMStructure = new LSTMStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM, BuilderParameters.LAYER_DIM, DeviceType.GPU);
                embedStructure = new EmbedStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM.Last(), DeviceType.GPU);

                for (int i = 0; i < BuilderParameters.LAYER_DIM.Length; i++)
                    attentionStructures.Add(new MLPAttentionStructure(BuilderParameters.LAYER_DIM[i], BuilderParameters.LAYER_DIM[i], BuilderParameters.Attention_Hidden, DeviceType.GPU));

                modelStructure.AddLayer(encodeLSTMStructure);
                modelStructure.AddLayer(reverseEncodeLSTMStructure);
                modelStructure.AddLayer(decodeLSTMStructure);
                modelStructure.AddLayer(embedStructure);
                for (int i = 0; i < attentionStructures.Count; i++)
                    modelStructure.AddLayer(attentionStructures[i]);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read)))
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                    encodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[0];
                    reverseEncodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[1];
                    decodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[2];
                    embedStructure = (EmbedStructure)modelStructure.CompositeLinks[3];
                    for (int i = 4; i < modelStructure.CompositeLinks.Count; i++)
                        attentionStructures.Add((MLPAttentionStructure)modelStructure.CompositeLinks[i]);
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    //.InitOptimizer(OptimizerParameters.StructureOptimizer);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin, // DataPanel.TrainMapBin,
                        encodeLSTMStructure, reverseEncodeLSTMStructure, decodeLSTMStructure, embedStructure, attentionStructures, 
                         modelStructure, new RunnerBehavior() { RunMode =  DNNRunMode.Train, Device = device, Computelib = computeLib });

                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, //DataPanel.ValidMapBin,
                            encodeLSTMStructure, reverseEncodeLSTMStructure, decodeLSTMStructure, embedStructure, attentionStructures, 
                            modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.Evaluation);

                    ComputationGraph devCG = null;
                    if (BuilderParameters.IsDevFile)
                        devCG = BuildComputationGraph(DataPanel.DevSrcBin, DataPanel.DevTgtBin, //DataPanel.DevMapBin,
                            encodeLSTMStructure, reverseEncodeLSTMStructure, decodeLSTMStructure, embedStructure, attentionStructures, 
                            modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    double bestDevScore = double.MaxValue;
                    double bestDevIter = -1;
                    double bestDevValidScore = 0;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        string modelName = string.Format(@"{0}\\Seq2Seq.{1}.model", BuilderParameters.ModelOutputPath, iter);
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                            modelStructure.Serialize(writer);
                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }

                        if (BuilderParameters.IsDevFile)
                        {
                            double devScore = devCG.Execute();
                            if (devScore < bestDevScore)
                            {
                                bestDevScore = devScore;
                                bestDevIter = iter;
                                bestDevValidScore = validScore;
                            }
                            Logger.WriteLog("Best Dev Score {0} at iteration {1}  Valid Score {2}", bestDevScore, bestDevIter, bestDevValidScore);
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, //DataPanel.ValidMapBin,
                            encodeLSTMStructure, reverseEncodeLSTMStructure, decodeLSTMStructure, embedStructure, attentionStructures, 
                            modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.Evaluation);
                    double predScore = predCG.Execute();
                    Logger.WriteLog("Prediction Score {0}", predScore);
                    break;
            }
            Logger.CloseLog();

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;
            //public static DataCashier<DenseBatchData, DenseDataStat> TrainMapBin = null;

            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;
            //public static DataCashier<DenseBatchData, DenseDataStat> ValidMapBin = null;

            /// <summary>
            ///  Report PPL instead of BLEU Score.
            /// </summary>
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevTgtBin = null;
            //public static DataCashier<DenseBatchData, DenseDataStat> DevMapBin = null;

            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);
                    //ValidMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidMapData);

                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                    //ValidMapBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.IsDevFile)
                {
                    DevSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevSrcData);
                    DevTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevTgtData);
                    //DevMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.DevMapData);

                    DevSrcBin.InitThreadSafePipelineCashier(100, false);
                    DevTgtBin.InitThreadSafePipelineCashier(100, false);
                    //DevMapBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainSrcData);
                    TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainTgtData);
                    //TrainMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainMapData);

                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    //TrainMapBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

            }
        }
    }
}