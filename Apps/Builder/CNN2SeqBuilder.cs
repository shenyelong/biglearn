﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class CNN2SeqBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "SRC Train Data."));
                Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "TGT Train Data."));
                Argument.Add("TRAIN-MAP", new ParameterArgument(string.Empty, "MAP Train Data."));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));
                Argument.Add("VALID-MAP", new ParameterArgument(string.Empty, "MAP Valid Data."));

                Argument.Add("DEV-SRC", new ParameterArgument(string.Empty, "SRC Dev Dataset."));
                Argument.Add("DEV-TGT", new ParameterArgument(string.Empty, "TGT Dev Dataset."));
                Argument.Add("DEV-MAP", new ParameterArgument(string.Empty, "MAP Dev Dataset."));

                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.ACCURACY).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("256", "DNN Layer Dim"));

                Argument.Add("SRC-LAYER-DIM", new ParameterArgument("256,256", "Source Layer Dim"));
                Argument.Add("SRC-ACTIVATION", new ParameterArgument("1,1", ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("SRC-WINDOW", new ParameterArgument("5,5", "Source DNN Window Size"));

                Argument.Add("ATT-HIDDEN", new ParameterArgument("256", "Attention Layer Hidden Dim"));
                Argument.Add("IS-ATT", new ParameterArgument("1", "1: Attention Enable; 0: Attention Disable"));
                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
                Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            public static string TrainSrcData { get { return Argument["TRAIN-SRC"].Value; } }
            public static string TrainTgtData { get { return Argument["TRAIN-TGT"].Value; } }
            public static string TrainMapData { get { return Argument["TRAIN-MAP"].Value; } }
            public static bool IsTrainFile { get { return (!TrainSrcData.Equals(string.Empty)) && (!TrainTgtData.Equals(string.Empty)); } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static string ValidMapData { get { return Argument["VALID-MAP"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

            public static string DevSrcData { get { return Argument["DEV-SRC"].Value; } }
            public static string DevTgtData { get { return Argument["DEV-TGT"].Value; } }
            public static string DevMapData { get { return Argument["DEV-MAP"].Value; } }
            public static bool IsDevFile { get { return (!DevSrcData.Equals(string.Empty)) && (!DevTgtData.Equals(string.Empty)); } }


            public static string Vocab { get { return Argument["VOCAB"].Value; } }
            public static Vocab2Freq mVocabDict = null;
            public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
            public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            
            /// <summary>
            /// Decode LSTM Dimension.
            /// </summary>
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] SRC_LAYER_DIM { get { return Argument["SRC-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] SRC_ACTIVATION { get { return Argument["SRC-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] SRC_WINDOW { get { return Argument["SRC-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

            public static bool IsAttention { get { return int.Parse(Argument["IS-ATT"].Value) > 0; } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
            public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }


            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

        }

        public override BuilderType Type { get { return BuilderType.CNN2SEQ; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            OptimizerParameters.Parse(fileName);
            Cudalib.CudaInit(BuilderParameters.GPUID);
        }
        
        public class MapDataHelpRunner : StructRunner
        {
            public CudaPieceInt SrcShuffle;
            public CudaPieceInt TgtShuffle;
            public DenseBatchData MapData;
            public MapDataHelpRunner(DenseBatchData mapData, RunnerBehavior behavior): base(Structure.Empty, behavior)
            {
                MapData = mapData;
                SrcShuffle = new CudaPieceInt(MapData.Stat.MAX_BATCHSIZE / 2, true, Behavior.Device == DeviceType.GPU);
                TgtShuffle = new CudaPieceInt(MapData.Stat.MAX_BATCHSIZE / 2, true, Behavior.Device == DeviceType.GPU);
            }
            public override void Forward()
            {
                MapData.Data.SyncToCPU();

                for (int i = 0; i < MapData.BatchSize / 2; i++) SrcShuffle.MemPtr[i] = (int)MapData.Data.MemPtr[i];
                for (int i = 0; i < MapData.BatchSize / 2; i++) TgtShuffle.MemPtr[i] = (int)MapData.Data.MemPtr[MapData.BatchSize / 2 + i];
                SrcShuffle.SyncFromCPU();
                TgtShuffle.SyncFromCPU();
            }
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             //IDataCashier<DenseBatchData, DenseDataStat> mapData,
                                                             List<LayerStructure> srcLayers,
                                                             LSTMStructure decoder,
                                                             MLPAttentionStructure attentions, 
                                                             EmbedStructure decodeEmbedTgt,
                                                             DNNRunMode mode, CompositeNNStructure Model, int deviceID, EvaluationType evalType)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = mode == DNNRunMode.Train ? 50 : 500 };

            DeviceType device = MathOperatorManager.SetDevice(deviceID);
            CudaMathOperation lib = new CudaMathOperation(true, true);
            RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };

            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));

            SeqDenseBatchData SrcOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(srcLayers[0], SrcData.SequenceData, Behavior));
            for(int i=1;i<srcLayers.Count;i++)
                SrcOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(srcLayers[i], SrcOutput, Behavior));


            /// no need to resort the output.
            //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(SrcData.SequenceData, Behavior, false));
            //DenseBatchData MapData = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(mapData, Behavior));
            //MapDataHelpRunner mapHelpRunner = new MapDataHelpRunner(MapData, Behavior);
            //cg.AddRunner(mapHelpRunner);
            SeqDenseBatchData SrcEnsembleMemory = SrcOutput;
            //HiddenBatchData SrcStatusOriginal = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(SrcOutput, Behavior));
            //HiddenBatchData SrcStatus = (HiddenBatchData)cg.AddRunner(new MatrixShuffleRunner(SrcStatusOriginal, CudaPieceInt.Empty, mapHelpRunner.SrcShuffle, Behavior));

            //HiddenBatchData SrcStatus = (HiddenBatchData)cg.AddRunner(new )
            HiddenBatchData SrcStatus = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcEnsembleMemory, true, 0, Behavior));

            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));
            //SeqHelpData TgtHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(TgtData.SequenceData, Behavior));

            switch (mode)
            {
                case DNNRunMode.Train:
                    //MLPAttentionRunner tgtAttentionRunners = new MLPAttentionRunner(attentions, SrcEnsembleMemory, SrcHelp, MapData,
                    //     BuilderParameters.TgtSeqMaxLen, TgtData.SequenceData.Stat, Behavior);
                    //SeqDenseBatchData TgtlstmOutput = (SeqDenseBatchData)cg.AddRunner(new LSTMSparseRunner<SeqSparseBatchData>(decoder.LSTMCells[0],
                    //    TgtData.SequenceData, TgtHelp, SrcStatus, SrcStatus,
                    //    BuilderParameters.IsAttention ? tgtAttentionRunners : null, Behavior));


                    MLPAttentionV2Runner tgtAttentionRunners = new MLPAttentionV2Runner(attentions, SrcEnsembleMemory, BuilderParameters.TgtSeqMaxLen,
                        TgtData.SequenceData.Stat.MAX_BATCHSIZE, TgtData.SequenceData.Stat.MAX_SEQUENCESIZE, Behavior);
                    /**************** Target LSTM Decoder *********/
                    SeqDenseRecursiveData TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(decoder.LSTMCells[0],
                        TgtData.SequenceData, false, SrcStatus, SrcStatus,
                        BuilderParameters.IsAttention ? tgtAttentionRunners : null, Behavior));

                    if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                    {
                        cg.AddRunner(new ModelDiskDumpRunner(Model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\Seq2Seq"));
                    }
                    //embed, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
                    cg.AddObjective(new EmbedFullySoftmaxRunner(decodeEmbedTgt, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:

                    if (evalType == EvaluationType.PERPLEXITY)
                    {
                        MLPAttentionV2Runner predtgtAttentionRunners = new MLPAttentionV2Runner(attentions, SrcEnsembleMemory, BuilderParameters.TgtSeqMaxLen,
                        TgtData.SequenceData.Stat.MAX_BATCHSIZE, TgtData.SequenceData.Stat.MAX_SEQUENCESIZE, Behavior);
                        /**************** Target LSTM Decoder *********/
                        SeqDenseRecursiveData predTgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(decoder.LSTMCells[0],
                            TgtData.SequenceData, false, SrcStatus, SrcStatus,
                            BuilderParameters.IsAttention ? predtgtAttentionRunners : null, Behavior));
                        cg.AddRunner(new EmbedFullySoftmaxRunner(decodeEmbedTgt, predTgtlstmOutput, predTgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));

                        //MLPAttentionRunner predTgtAttentionRunners = new MLPAttentionRunner(attentions, SrcEnsembleMemory, SrcHelp, MapData,
                        // BuilderParameters.TgtSeqMaxLen, TgtData.SequenceData.Stat, Behavior);
                        /**************** Target LSTM Decoder *********/
                        //SeqDenseBatchData predTgtlstmOutput = (SeqDenseBatchData)cg.AddRunner(new LSTMSparseRunner<SeqSparseBatchData>(decoder.LSTMCells[0],
                        //    TgtData.SequenceData, TgtHelp, SrcStatus, SrcStatus,
                        //    BuilderParameters.IsAttention ? predTgtAttentionRunners : null,
                        //    Behavior));
                        //cg.AddRunner(new EmbedFullySoftmaxRunner(decodeEmbedTgt, predTgtlstmOutput, TgtHelp.LagForward, TgtData.SequenceLabel, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath, Behavior));
                    }
                    else
                    {
                        MLPAttentionV2Runner decodeAttentionRunners = new MLPAttentionV2Runner(attentions, SrcEnsembleMemory, 
                            TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior);

                        List<MLPAttentionV2Runner> softRunners = new List<MLPAttentionV2Runner>();
                        if (BuilderParameters.IsAttention) softRunners.Add(decodeAttentionRunners);
                        else softRunners.Add(null);

                        List<Tuple<HiddenBatchData, HiddenBatchData>> statusList = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
                        statusList.Add(new Tuple<HiddenBatchData, HiddenBatchData>(SrcStatus, SrcStatus));


                        // the probability of exactly generate the true tgt.
                        LSTMDecodingRunner decodeRunner = new LSTMDecodingRunner(decoder, decodeEmbedTgt, statusList,
                            softRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), Behavior, true);

                        
                        //MLPAttentionRunner decodeAttentionRunners = new MLPAttentionRunner(attentions, SrcEnsembleMemory, SrcHelp, MapData,
                        //            TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior);
                        // the probability of exactly generate the true tgt.
                        //LSTMDecodingRunner decodeRunner = new LSTMDecodingRunner(decoder, decodeEmbedTgt, statusList, 
                        //    softRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(),  Behavior);

                        EnsembleBeamSearchRunner beamRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
                            BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, decodeEmbedTgt.VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);
                        beamRunner.AddBeamSearch(decodeRunner);
                        cg.AddRunner(beamRunner);

                        switch (evalType)
                        {
                            case EvaluationType.ACCURACY:
                                cg.AddRunner(new BeamAccuracyRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                                break;
                            case EvaluationType.BSBLEU:
                                cg.AddRunner(new BLEUDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                                break;
                            case EvaluationType.ROUGE:
                                cg.AddRunner(new ROUGEDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                                break;
                            case EvaluationType.TOPKBEAM:
                                cg.AddRunner(new TopKBeamRunner(beamRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                                break;
                        }
                    }
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    CompositeNNStructure modelStructure = new CompositeNNStructure();

                    List<LayerStructure> srcLayers = new List<LayerStructure>();
                    LSTMStructure decodeLSTMStructure = null;
                    EmbedStructure embedDecodeTgt = null;
                    MLPAttentionStructure attentionStructures = null;

                    if (BuilderParameters.SeedModel.Equals(string.Empty))
                    {
                        decodeLSTMStructure = new LSTMStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM, BuilderParameters.LAYER_DIM, DeviceType.GPU);
                        embedDecodeTgt = new EmbedStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM.Last(), DeviceType.GPU);

                        int inputDim = DataPanel.TrainSrcBin.Stat.FEATURE_DIM;
                        for (int i = 0; i < BuilderParameters.SRC_LAYER_DIM.Length; i++)
                        {
                            srcLayers.Add((new LayerStructure(inputDim, BuilderParameters.SRC_LAYER_DIM[i], BuilderParameters.SRC_ACTIVATION[i], N_Type.Convolution_layer, BuilderParameters.SRC_WINDOW[i], 0, true)));
                            inputDim = BuilderParameters.SRC_LAYER_DIM[i];
                        }
                        attentionStructures = new MLPAttentionStructure(BuilderParameters.LAYER_DIM.Last(), BuilderParameters.LAYER_DIM.Last(), BuilderParameters.Attention_Hidden, DeviceType.GPU);

                        modelStructure.AddLayer(decodeLSTMStructure);
                        modelStructure.AddLayer(embedDecodeTgt);
                        modelStructure.AddLayer(attentionStructures);
                        for(int i=0;i<srcLayers.Count; i++) modelStructure.AddLayer(srcLayers[i]);
                    }
                    else
                    {
                        using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                        {
                            modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                            decodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[0];
                            embedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[1];
                            attentionStructures = (MLPAttentionStructure)modelStructure.CompositeLinks[2];
                            for (int i = 3; i < modelStructure.CompositeLinks.Count; i++) srcLayers.Add((LayerStructure)modelStructure.CompositeLinks[i]);
                        }
                    }


                    modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin, 
                        srcLayers, decodeLSTMStructure, attentionStructures, embedDecodeTgt, DNNRunMode.Train, modelStructure, BuilderParameters.GPUID, EvaluationType.PERPLEXITY);

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                    {
                        validCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, 
                            srcLayers, decodeLSTMStructure, attentionStructures, embedDecodeTgt, DNNRunMode.Predict, modelStructure, BuilderParameters.GPUID, BuilderParameters.Evaluation);
                    }

                    ComputationGraph devCG = null;
                    if(BuilderParameters.IsDevFile)
                    {
                        devCG = BuildComputationGraph(DataPanel.DevSrcBin, DataPanel.DevTgtBin, 
                            srcLayers, decodeLSTMStructure, attentionStructures, embedDecodeTgt, DNNRunMode.Predict, modelStructure, BuilderParameters.GPUID, EvaluationType.PERPLEXITY);
                    }

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    double bestDevScore = double.MaxValue;
                    double bestDevIter = -1;
                    double bestDevValidScore = 0;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        string modelName = string.Format(@"{0}\\Seq2Seq.{1}.model", BuilderParameters.ModelOutputPath, iter);

                        using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                        {
                            modelStructure.Serialize(writer);
                        }

                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }

                        if (BuilderParameters.IsDevFile)
                        {
                            double devScore = devCG.Execute();
                            if (devScore < bestDevScore)
                            {
                                bestDevScore = devScore;
                                bestDevIter = iter;
                                bestDevValidScore = validScore;
                            }
                            Logger.WriteLog("Best Dev Score {0} at iteration {1}  Valid Score {2}", bestDevScore, bestDevIter, bestDevValidScore);
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    CompositeNNStructure predModel = new CompositeNNStructure(new BinaryReader(new FileStream(BuilderParameters.ModelOutputPath, FileMode.Open, FileAccess.Read)), DeviceType.GPU);
                    LSTMStructure pDecodeLSTMStructure = (LSTMStructure)predModel.CompositeLinks[0];
                    EmbedStructure pEmbedDecodeTgt = (EmbedStructure)predModel.CompositeLinks[1];
                    MLPAttentionStructure pAttentionStructures = (MLPAttentionStructure)predModel.CompositeLinks[2];
                    List<LayerStructure> pSrcLayers = new List<LayerStructure>();
                    for (int i = 3; i < predModel.CompositeLinks.Count; i++) pSrcLayers.Add((LayerStructure)predModel.CompositeLinks[i]);
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, 
                         pSrcLayers, pDecodeLSTMStructure, pAttentionStructures, pEmbedDecodeTgt, DNNRunMode.Predict, predModel, BuilderParameters.GPUID, BuilderParameters.Evaluation);
                    double predScore = predCG.Execute();
                    Logger.WriteLog("Prediction Score {0}", predScore);
                    break;
            }
            Logger.CloseLog();

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {

            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;
            //public static DataCashier<DenseBatchData, DenseDataStat> TrainMapBin = null;

            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;
            //public static DataCashier<DenseBatchData, DenseDataStat> ValidMapBin = null;

            /// <summary>
            ///  Report PPL instead of BLEU Score.
            /// </summary>
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevTgtBin = null;
            //public static DataCashier<DenseBatchData, DenseDataStat> DevMapBin = null;

            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);
                    //ValidMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidMapData);

                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                    //ValidMapBin.InitThreadSafePipelineCashier(100, false);
                }

                if(BuilderParameters.IsDevFile)
                {
                    DevSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevSrcData);
                    DevTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevTgtData);
                    //DevMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.DevMapData);

                    DevSrcBin.InitThreadSafePipelineCashier(100, false);
                    DevTgtBin.InitThreadSafePipelineCashier(100, false);
                    //DevMapBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainSrcData);
                    TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainTgtData);
                    //TrainMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainMapData);

                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    //TrainMapBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

            }
        }
    }
}