﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class MultiSentCNNBuilderParameters : BaseModelArgument<MultiSentCNNBuilderParameters>
    {
        public static string TrainData { get { return Argument["TRAIN"].Value; } }
        public static string ValidData { get { return Argument["VALID"].Value; } }
        public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

        public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

        public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static int[] WINDOW_SIZE { get { return Argument["WINDOW-SIZE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static N_Type[] ARCH { get { return Argument["ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
        public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
        public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
        public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }

        /// <summary>
        /// Only need to Specify Data and Archecture of Deep Nets.
        /// </summary>
        static MultiSentCNNBuilderParameters()
        {
            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

            Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
            Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

            Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.Regression).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.MSE).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
            Argument.Add("WINDOW-SIZE", new ParameterArgument("1", "CNN Window Size"));
            Argument.Add("ARCH", new ParameterArgument(((int)N_Type.Fully_Connected).ToString(), ParameterUtil.EnumValues(typeof(N_Type))));
            Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
            Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
            Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

            Argument.Add("MODEL-PATH", new ParameterArgument(@"\\DEEP-06\Test-JDSSM\Model_Path\", "Model Path"));
            Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));
        }
    }

    public class MultiSentCNNBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.MSCNN; } }

        public override void InitStartup(string fileName)
        {
            MultiSentCNNBuilderParameters.Parse(fileName);
        }

        ComputationGraph BuildCG(IDataCashier<SeqSentenceBatchInputData, SeqSentenceInputDataStat> data,
            IDataCashier<DenseBatchData, DenseDataStat> labelData,
            LayerStructure layer1CNN, LayerStructure layer2CNN, LayerStructure dnn, RunnerBehavior behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            SeqSentenceBatchInputData Data = (SeqSentenceBatchInputData)cg.AddDataRunner(new DataRunner<SeqSentenceBatchInputData, SeqSentenceInputDataStat>(data, behavior));
            DenseBatchData Label = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(labelData, behavior));

            SeqSparseBatchData wordData = (SeqSparseBatchData)cg.AddRunner(new Transform_SeqSentence2SequenceRunner(Data, behavior));
            HiddenBatchData sentenceData = (HiddenBatchData)cg.AddRunner(new ConvSparseRunner<SeqSparseBatchData>(layer1CNN, wordData, behavior));

            SeqDenseBatchData paraData = (SeqDenseBatchData)cg.AddRunner(new Transform_Matrix2SeqMatrixRunner(sentenceData, Data, behavior));
            HiddenBatchData output = (HiddenBatchData)cg.AddRunner(new ConvDenseRunner<SeqDenseBatchData>(layer2CNN, paraData, behavior));

            HiddenBatchData score = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(dnn, output, behavior));

            switch (behavior.RunMode)
            {
                case DNNRunMode.Train:
                    switch (MultiSentCNNBuilderParameters.LossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            cg.AddObjective(new CrossEntropyRunner(Label.Data, output.Output, output.Deriv, 1, behavior));
                            break;
                        case LossFunctionType.MultiClassification:
                            cg.AddObjective(new MultiClassSoftmaxRunner(Label.Data, output.Output, output.Deriv, 1, behavior));
                            break;
                        case LossFunctionType.Regression:
                            cg.AddObjective(new MSERunner(output.Output, Label, null, output.Deriv, behavior));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    switch (MultiSentCNNBuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(Label.Data, output.Output, "tmp.score", "tmp.metric"));
                            break;
                        case EvaluationType.MSE:
                            cg.AddRunner(new RMSEDiskDumpRunner(Label.Data, output.Output, 1, false, "tmp.score"));
                            break;
                        case EvaluationType.ACCURACY:
                            cg.AddRunner(new AccuracyDiskDumpRunner(Label.Data, output.Output, 1, "tmp.score"));
                            break;
                    }
                    break;
            }

            return cg;
        }
        public override void Rock()
        {
            Logger.OpenLog(MultiSentCNNBuilderParameters.LogFile);

            switch (MultiSentCNNBuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    Logger.WriteLog("Loading Training/Validation Data.");
                    DataPanel.Init();
                    Logger.WriteLog("Load Data Finished.");

                    DeviceBehavior device = new DeviceBehavior(BuilderParameters.GPUID);

                    //EvaluationParameter evalParameter = new EvaluationParameter()
                    //{
                    //    evalType = MultiSentCNNBuilderParameters.Evaluation,
                    //    InputDataType = DataSourceID.SeqSentenceBatchInputLabelData,
                    //    OutputDataType = DataSourceID.HiddenBatchData,
                    //    Param = new BasicEvalParameter()
                    //    {
                    //        Gamma = 1, //MultiSentCNNBuilderParameters.Gamma,
                    //        LabelMaxValue = 0, // MultiSentCNNBuilderParameters.LabelMaxValue,
                    //        LabelMinValue = 0, //MultiSentCNNBuilderParameters.LabelMinValue,
                    //        ScoreFile = "multiSent.Score", // MultiSentCNNBuilderParameters.ScoreOutputPath,
                    //        MetricFile = "multiSent.Metric" // MultiSentCNNBuilderParameters.MetricOutputPath
                    //    }
                    //};

                    //LossDerivParameter lossDerivParameter = new LossDerivParameter()
                    //{
                    //    InputDataType = DataSourceID.SeqSentenceBatchInputLabelData,
                    //    OutputDataType = DataSourceID.HiddenBatchData,
                    //    lossType = MultiSentCNNBuilderParameters.LossFunction,
                    //    Gamma = 1, //MultiSentCNNBuilderParameters.Gamma,
                    //    LabelMinValue = 0, //MultiSentCNNBuilderParameters.LabelMinValue,
                    //    LabelMaxValue = 0, //MultiSentCNNBuilderParameters.LabelMaxValue
                    //};

                    ///int[] layerDim, A_Func[] activation, bool[] isbias, N_Type[] arch, int[] wind)
                    Logger.WriteLog("Loading CNN Structure.");
                    DNNStructure dnnModel = new DNNStructure(
                                        DataPanel.Train.Stat.MAX_FEATUREDIM,
                                        MultiSentCNNBuilderParameters.LAYER_DIM,
                                        MultiSentCNNBuilderParameters.ACTIVATION,
                                        MultiSentCNNBuilderParameters.LAYER_BIAS,
                                        MultiSentCNNBuilderParameters.ARCH,
                                        MultiSentCNNBuilderParameters.WINDOW_SIZE,
                                        MultiSentCNNBuilderParameters.LAYER_DROPOUT);

                    dnnModel.InitOptimizer(
                                        new StructureLearner()
                                        {
                                            LearnRate = MultiSentCNNBuilderParameters.LearnRate,
                                            Optimizer = MultiSentCNNBuilderParameters.Optimizer,
                                            AdaBoost = MultiSentCNNBuilderParameters.AdaBoost,
                                            ShrinkLR = MultiSentCNNBuilderParameters.ShrinkLR,
                                            BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                                            EpochNum = MultiSentCNNBuilderParameters.Iteration
                                        }, device.TrainMode);

                    ComputationGraph trainCG = BuildCG(DataPanel.Train, DataPanel.TrainLabel, dnnModel.NeuralLinks[0], dnnModel.NeuralLinks[1], dnnModel.NeuralLinks[2], device.TrainMode);
                    trainCG.SetDelegateModel(dnnModel);

                    ComputationGraph predCG = BuildCG(DataPanel.Valid, DataPanel.ValidLabel, dnnModel.NeuralLinks[0], dnnModel.NeuralLinks[1], dnnModel.NeuralLinks[2], device.TrainMode);
                    //DataPanel.Valid,
                    //DNNRunner<SeqSentenceBatchInputLabelData> evaler = new DNNRunner<SeqSentenceBatchInputLabelData>(dnnModel,  new RunnerBehavior() { RunMode = DNNRunMode.Predict });

                    //DataProcessor trainProcessor = new DataProcessor(DataPanel.Train);
                    //DataProcessor validProcessor = new DataProcessor(DataPanel.Valid);

                    for (int iter = 0; iter < MultiSentCNNBuilderParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        //.Train(DataPanel.Train.GetInstances(true));
                        //float loss = 0;
                        //trainProcessor.Init();
                        //int batchIdx = 0;
                        //while (trainProcessor.RandomNext())
                        //{
                        //    trainer.Forward();

                        //    trainer.Output.Output.Data.CopyOutFromCuda();
                        //    switch (MultiSentCNNBuilderParameters.LossFunction)
                        //    {
                        //        case LossFunctionType.MultiClassification:
                        //            loss += LossFunction.MulticlassCrossEntropyLoss(trainer.Output.Output.Data, trainer.Input.Label, trainer.Output.Deriv.Data, trainer.Output.Dim, 1, 1, trainer.Output.BatchSize);
                        //            break;
                        //        case LossFunctionType.BinaryClassification:
                        //            loss += LossFunction.CrossEntropyLoss(trainer.Output.Output.Data, trainer.Input.Label, trainer.Output.Deriv.Data, trainer.Output.BatchSize);
                        //            break;
                        //        case LossFunctionType.Regression:
                        //            loss += LossFunction.RegressionLoss(trainer.Output.Output.Data, trainer.Input.Label, trainer.Output.Deriv.Data, trainer.Input.BatchSize);
                        //            break;
                        //    }
                        //    trainer.Backward(true);
                        //    trainer.Update();
                        //    if (++batchIdx % 100 == 0) Logger.WriteLog("Batch Num {0}", batchIdx);
                        //}
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss / (DataPanel.Train.Stat.TotalBatchNumber + float.Epsilon));

                        predCG.Execute();
                        //evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                        //EvaluationSet evaluator = EvaluationSet.CreateEvaluation(MultiSentCNNBuilderParameters.Evaluation);
                        //validProcessor.Init();
                        //while (validProcessor.Next())
                        //{
                        //    evaler.Forward();
                        //    evaler.Output.Output.Data.CopyOutFromCuda();
                        //    switch (MultiSentCNNBuilderParameters.Evaluation)
                        //    {
                        //        case EvaluationType.AUC:
                        //            evaluator.PushScore(evaler.Input.Label.MemPtr, evaler.Output.Output.Data.MemPtr, evaler.Output.Output.BatchSize);
                        //            break;
                        //        case EvaluationType.MSE:
                        //            evaluator.PushScore(evaler.Input.Label.MemPtr, evaler.Output.Output.Data.MemPtr, evaler.Output.Output.BatchSize);
                        //            break;
                        //        case EvaluationType.ACCURACY:
                        //            evaluator.PushScore(evaler.Input.Label.MemPtr, evaler.Output.Output.Data.MemPtr, evaler.Output.Dim, evaler.Output.Output.BatchSize);
                        //            break;
                        //    }
                        //}
                        //List<string> evalInfo = new List<string>();
                        //float score = evaluator.Evaluation(out evalInfo);
                        //Logger.WriteLog("Evaluation :\n{0}\n{1}", score, string.Join("\n", evalInfo));
                        dnnModel.ModelSave_C_OR_DSSM_GPU_V4_2015_APR(string.Format(@"{0}\\DNN.{1}.model", MultiSentCNNBuilderParameters.ModelOutputPath, iter.ToString()));
                    }
                    
                    break;
                case DNNRunMode.Predict:
                    
                    break;
            }
            Logger.CloseLog();
        }


        public class DataPanel
        {
            public static DataCashier<SeqSentenceBatchInputData, SeqSentenceInputDataStat> Train = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainLabel = null;

            public static DataCashier<SeqSentenceBatchInputData, SeqSentenceInputDataStat> Valid = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidLabel = null;

            public static void Init()
            {
                if (MultiSentCNNBuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSentenceBatchInputData, SeqSentenceInputDataStat>(MultiSentCNNBuilderParameters.TrainData);
                    TrainLabel = new DataCashier<DenseBatchData, DenseDataStat>(MultiSentCNNBuilderParameters.TrainData+".label");

                    Train.InitThreadSafePipelineCashier(64, 13);
                    TrainLabel.InitThreadSafePipelineCashier(64, 13);
                }
                //    Train = new SeqSentenceBatchInputLabelData(MultiSentCNNBuilderParameters.TrainData, BatchInputMode.LoadContainer);

                if (MultiSentCNNBuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSentenceBatchInputData, SeqSentenceInputDataStat>(MultiSentCNNBuilderParameters.ValidData);
                    ValidLabel = new DataCashier<DenseBatchData, DenseDataStat>(MultiSentCNNBuilderParameters.ValidData+".label");

                    Valid.InitThreadSafePipelineCashier(100, false);
                    ValidLabel.InitThreadSafePipelineCashier(100, false);

                }
                //    Valid = new SeqSentenceBatchInputLabelData(MultiSentCNNBuilderParameters.ValidData, BatchInputMode.LoadContainer);
            }
        }
    }
}
