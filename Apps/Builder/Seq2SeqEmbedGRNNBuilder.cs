﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class Seq2SeqEmbedGRNNBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "SRC Train Data."));
                Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "TGT Train Data."));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));

                Argument.Add("DEV-SRC", new ParameterArgument(string.Empty, "SRC Dev Dataset."));
                Argument.Add("DEV-TGT", new ParameterArgument(string.Empty, "TGT Dev Dataset."));

                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("LAYER-ATTENTION", new ParameterArgument("1", "Layer Attention"));

                Argument.Add("SRC-EMBED", new ParameterArgument("620", "Src String Dim"));
                Argument.Add("TGT-EMBED", new ParameterArgument("500", "Tgt String Dim"));
                Argument.Add("ATT-HIDDEN", new ParameterArgument("1000", "Attention Layer Hidden Dim"));

                Argument.Add("CONTEXT-LEN", new ParameterArgument("0", "Context Vector length"));
                Argument.Add("DECODE-DIM", new ParameterArgument("200", "Decoding Dimension"));
                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
                Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            //(DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainSrcData { get { return Argument["TRAIN-SRC"].Value; } }
            public static string TrainTgtData { get { return Argument["TRAIN-TGT"].Value; } }
            public static bool IsTrainFile { get { return (!TrainSrcData.Equals(string.Empty)) && (!TrainTgtData.Equals(string.Empty)); } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

            public static string DevSrcData { get { return Argument["DEV-SRC"].Value; } }
            public static string DevTgtData { get { return Argument["DEV-TGT"].Value; } }
            public static bool IsDevFile { get { return (!DevSrcData.Equals(string.Empty)) && (!DevTgtData.Equals(string.Empty)); } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_ATTENTION { get { return Argument["LAYER-ATTENTION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }

            public static int Src_Embed { get { return int.Parse(Argument["SRC-EMBED"].Value); } }
            public static int Tgt_Embed { get { return int.Parse(Argument["TGT-EMBED"].Value); } }

            public static int Decode_Embed { get { return int.Parse(Argument["DECODE-DIM"].Value); } }
            public static int Context_Len { get { return int.Parse(Argument["CONTEXT-LEN"].Value); } }

            public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string Vocab { get { return Argument["VOCAB"].Value; } }
            public static Vocab2Freq mVocabDict = null;
            public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
            public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
            public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }


            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.SEQ2SEQ_EMBED_GRNN; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<SeqDenseRecursiveData> AddGRUEncoder(ComputationGraph cg, GRUStructure gruModel,
            SeqDenseRecursiveData input, RunnerBehavior behavior)
        {
            List<SeqDenseRecursiveData> memory = new List<SeqDenseRecursiveData>();
            SeqDenseRecursiveData SrcGruOutput = input;
            /***************** GRU Encoder ******************************/
            for (int i = 0; i < gruModel.GRUCells.Count; i++)
            {
                FastGRUDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(gruModel.GRUCells[i], SrcGruOutput, behavior);
                SrcGruOutput = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);
                memory.Add(encoderRunner.Output);
            }
            return memory;
        }
        

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             EmbedStructure embedSrc, EmbedStructure embedTgt,
                                                             LayerStructure transO, 
                                                             GRUStructure D1Encoder, GRUStructure D2Encoder, GRUStructure decoder,
                                                             List<MLPAttentionStructure> attentions, 
                                                             LayerStructure U0, LayerStructure V0, LayerStructure C0,
                                                             EmbedStructure decodeEmbedTgt,
                                                             RunnerBehavior Behavior, CompositeNNStructure Model, EvaluationType evalType)
        {
            ComputationGraph cg = new ComputationGraph();
            
            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));

            SeqDenseRecursiveData SrcD1Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData.SequenceData, false, Behavior));
            SeqDenseRecursiveData SrcD2Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData.SequenceData, true, Behavior));

            List<SeqDenseRecursiveData> SrcD1Memory = AddGRUEncoder(cg, D1Encoder, SrcD1Embed, Behavior);
            List<SeqDenseRecursiveData> SrcD2Memory = AddGRUEncoder(cg, D2Encoder, SrcD2Embed, Behavior);

            /**************** Bidirection Source GRNN Encoder Ensemble *********/
            List<SeqDenseBatchData> SrcEnsembleMemory = new List<SeqDenseBatchData>();
            List<HiddenBatchData> SrcEnsembleStatus = new List<HiddenBatchData>();
            for (int i = 0; i < SrcD1Memory.Count; i++)
            {
                HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i], false, 0, SrcD2Memory[i].MapForward, Behavior));
                HiddenBatchData newSrcO = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transO, SrcD2O, Behavior));
                SrcEnsembleStatus.Add(newSrcO);
                
                SeqDenseBatchData SrcSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i], Behavior));
                SeqDenseBatchData SrcSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i], Behavior));
                
                SeqDenseBatchData SrcSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1O, SrcSeqD2O }, Behavior));
                SrcEnsembleMemory.Add(SrcSeqO);
            }
            
            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));
            
            if (Behavior.RunMode == DNNRunMode.Train || evalType == EvaluationType.PERPLEXITY)
            {
                List<MLPAttentionV2Runner> tgtAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                    tgtAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i], BuilderParameters.TgtSeqMaxLen,
                        TgtData.SequenceData.Stat.MAX_BATCHSIZE, TgtData.SequenceData.Stat.MAX_SEQUENCESIZE, Behavior) : null);

                /**************** Target GRU Decoder *********/
                SeqDenseRecursiveData TgtEmbed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedTgt, TgtData.SequenceData, false, Behavior));

                SeqDenseRecursiveData TgtgruOutput = TgtEmbed;
                SeqDenseRecursiveData TgtLag1Output = null;
                for (int i = 0; i < decoder.GRUCells.Count; i++)
                {
                    FastGRUDenseRunner<SeqDenseRecursiveData> tmpRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(decoder.GRUCells[i],
                        TgtgruOutput, SrcEnsembleStatus[i], tgtAttentionRunners[i], Behavior);
                    TgtgruOutput = tmpRunner.Output;
                    TgtLag1Output = tmpRunner.Lag1Seq;
                    cg.AddRunner(tmpRunner);
                }

                if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                    cg.AddRunner(new ModelDiskDumpRunner(Model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\seq2seq"));

                //******* Generate Decode Vector **********/
                
                HiddenBatchData V0Output = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(V0, 
                    new HiddenBatchData(TgtEmbed.MAX_SENTSIZE, TgtEmbed.Dim, TgtEmbed.SentOutput, TgtEmbed.SentDeriv, Behavior.Device), Behavior));

                HiddenBatchData C0Output = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(C0,
                    new HiddenBatchData(tgtAttentionRunners.First().ZSeq.MAX_SENTSIZE, tgtAttentionRunners.First().ZSeq.Dim,
                    tgtAttentionRunners.First().ZSeq.SentOutput, tgtAttentionRunners.First().ZSeq.SentDeriv, Behavior.Device), Behavior));

                HiddenBatchData U0Output = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(U0,
                    new HiddenBatchData(TgtLag1Output.MAX_SENTSIZE, TgtLag1Output.Dim, TgtLag1Output.SentOutput, TgtLag1Output.SentDeriv, Behavior.Device), Behavior));

                HiddenBatchData T = (HiddenBatchData)cg.AddRunner(new AdditionMatrixRunner(new List<HiddenBatchData>() { V0Output, C0Output, U0Output }, Behavior));

                HiddenBatchData TmaxOutput = (HiddenBatchData)cg.AddRunner(new MaxoutRunner<HiddenBatchData>(T, 2, Behavior));
                
                if (Behavior.RunMode == DNNRunMode.Train)
                    cg.AddObjective(new EmbedFullySoftmaxRunner(decodeEmbedTgt, 
                        new SeqDenseBatchData(TgtgruOutput.Stat, TgtgruOutput.SampleIdx, TgtgruOutput.SentMargin, TmaxOutput.Output.Data, TmaxOutput.Deriv.Data, Behavior.Device),
                        //new SeqDenseBatchData(TgtgruOutput.Stat, TgtgruOutput.SampleIdx, TgtgruOutput.SentMargin, TgtgruOutput.SentOutput, TgtgruOutput.SentDeriv, Behavior.Device),
                        TgtgruOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
                else
                    cg.AddRunner(new EmbedFullySoftmaxRunner(decodeEmbedTgt,
                        new SeqDenseBatchData(TgtgruOutput.Stat, TgtgruOutput.SampleIdx, TgtgruOutput.SentMargin, TmaxOutput.Output.Data, TmaxOutput.Deriv.Data, Behavior.Device),
                        //new SeqDenseBatchData(TgtgruOutput.Stat, TgtgruOutput.SampleIdx, TgtgruOutput.SentMargin, TgtgruOutput.SentOutput, TgtgruOutput.SentDeriv, Behavior.Device),
                        TgtgruOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath, Behavior));
            }
            else
            {
                List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
                for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                    decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i], TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);

                // the probability of exactly generate the true tgt.
                GRNNEmbedDecodingRunner decodeRunner = new GRNNEmbedDecodingRunner(decoder, embedTgt, decodeEmbedTgt, SrcEnsembleStatus,
                    decodeAttentionRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), V0, C0, U0, Behavior);

                EnsembleBeamSearchRunner beamRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
                    BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth,
                    decodeEmbedTgt.VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);
                beamRunner.AddBeamSearch(decodeRunner);
                cg.AddRunner(beamRunner);

                switch (evalType)
                {
                    case EvaluationType.ACCURACY:
                        cg.AddRunner(new BeamAccuracyRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                        break;
                    case EvaluationType.BSBLEU:
                        cg.AddRunner(new BLEUDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                        break;
                    case EvaluationType.ROUGE:
                        cg.AddRunner(new ROUGEDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                        break;
                    case EvaluationType.TOPKBEAM:
                        cg.AddRunner(new TopKBeamRunner(beamRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                        break;
                }
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            int aDirectLSTM = 1;
            int bDirectLSTM = 1;
            int ensemble = aDirectLSTM + bDirectLSTM;

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            GRUStructure D1encodeStructure = null;
            GRUStructure D2encodeStructure = null;
            GRUStructure decodeStructure = null;

            LayerStructure transO = null;

            EmbedStructure embedSrc = null;
            EmbedStructure embedTgt = null;
            EmbedStructure embedDecodeTgt = null;

            LayerStructure Vo = null;
            LayerStructure Uo = null;
            LayerStructure Co = null;

            List<MLPAttentionStructure> attentionStructures = new List<MLPAttentionStructure>();

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                D1encodeStructure = new GRUStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM, DeviceType.GPU);
                D2encodeStructure = new GRUStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM, DeviceType.GPU);
                decodeStructure = new GRUStructure(BuilderParameters.Tgt_Embed, BuilderParameters.LAYER_DIM, BuilderParameters.LAYER_DIM.Select(i => i * ensemble).ToArray(), DeviceType.GPU);

                transO = new LayerStructure(BuilderParameters.LAYER_DIM.Last(), BuilderParameters.LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);

                embedSrc = new EmbedStructure(DataPanel.TrainSrcBin.Stat.FEATURE_DIM, BuilderParameters.Src_Embed, DeviceType.GPU);
                embedTgt = new EmbedStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.Tgt_Embed, DeviceType.GPU);
                embedDecodeTgt = new EmbedStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, 512, DeviceType.GPU);

                for (int i = 0; i < BuilderParameters.LAYER_DIM.Length; i++)
                    attentionStructures.Add(new MLPAttentionStructure(BuilderParameters.LAYER_DIM[i], ensemble * BuilderParameters.LAYER_DIM[i], BuilderParameters.Attention_Hidden, DeviceType.GPU));

                Vo = new LayerStructure(embedTgt.Dim, 1024, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false);
                Co = new LayerStructure(ensemble * BuilderParameters.LAYER_DIM.Last(), 1024, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false);
                Uo = new LayerStructure(BuilderParameters.LAYER_DIM.Last(), 1024, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false);

                modelStructure.AddLayer(D1encodeStructure);
                modelStructure.AddLayer(D2encodeStructure);
                modelStructure.AddLayer(decodeStructure);

                modelStructure.AddLayer(transO);

                modelStructure.AddLayer(embedSrc);
                modelStructure.AddLayer(embedTgt);
                modelStructure.AddLayer(embedDecodeTgt);

                modelStructure.AddLayer(Vo);
                modelStructure.AddLayer(Co);
                modelStructure.AddLayer(Uo);

                for (int i = 0; i < attentionStructures.Count; i++)
                    modelStructure.AddLayer(attentionStructures[i]);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                    int link = 0;
                    D1encodeStructure = (GRUStructure)modelStructure.CompositeLinks[link++];
                    D2encodeStructure = (GRUStructure)modelStructure.CompositeLinks[link++];
                    decodeStructure = (GRUStructure)modelStructure.CompositeLinks[link++];

                    transO = (LayerStructure)modelStructure.CompositeLinks[link++];

                    embedSrc = (EmbedStructure)modelStructure.CompositeLinks[link++];
                    embedTgt = (EmbedStructure)modelStructure.CompositeLinks[link++];
                    embedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[link++];

                    Vo = (LayerStructure)modelStructure.CompositeLinks[link++];
                    Co = (LayerStructure)modelStructure.CompositeLinks[link++];
                    Uo = (LayerStructure)modelStructure.CompositeLinks[link++];
                    for (int i = link; i < modelStructure.CompositeLinks.Count; i++)
                        attentionStructures.Add((MLPAttentionStructure)modelStructure.CompositeLinks[link++]);
                }
            }


            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin,
                        embedSrc, embedTgt, transO, D1encodeStructure, D2encodeStructure, decodeStructure, attentionStructures, Uo, Vo, Co, embedDecodeTgt,
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib },
                        modelStructure, EvaluationType.PERPLEXITY);

                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                            embedSrc, embedTgt, transO, D1encodeStructure, D2encodeStructure, decodeStructure, attentionStructures, Uo, Vo, Co, embedDecodeTgt,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
                            modelStructure, BuilderParameters.Evaluation);

                    ComputationGraph devCG = null;
                    if (BuilderParameters.IsDevFile)
                        devCG = BuildComputationGraph(DataPanel.DevSrcBin, DataPanel.DevTgtBin,
                            embedSrc, embedTgt, transO, D1encodeStructure, D2encodeStructure, decodeStructure, attentionStructures, Uo, Vo, Co, embedDecodeTgt,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
                            modelStructure, EvaluationType.PERPLEXITY);

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    double bestDevScore = double.MaxValue;
                    double bestDevIter = -1;
                    double bestDevValidScore = 0;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        string modelName = string.Format(@"{0}\\Seq2Seq.{1}.model", BuilderParameters.ModelOutputPath, iter);

                        using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                        {
                            modelStructure.Serialize(writer);
                        }

                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }

                        if (BuilderParameters.IsDevFile)
                        {
                            double devScore = devCG.Execute();
                            if (devScore < bestDevScore)
                            {
                                bestDevScore = devScore;
                                bestDevIter = iter;
                                bestDevValidScore = validScore;
                            }
                            Logger.WriteLog("Best Dev Score {0} at iteration {1}  Valid Score {2}", bestDevScore, bestDevIter, bestDevValidScore);
                        }

                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                            embedSrc, embedTgt, transO, D1encodeStructure, D2encodeStructure, decodeStructure, attentionStructures, Uo, Vo, Co, embedDecodeTgt,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
                            modelStructure, BuilderParameters.Evaluation);

                    double predScore = predCG.Execute();
                    Logger.WriteLog("Prediction Score {0}", predScore);
                    break;
            }
            Logger.CloseLog();

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {

            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;

            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;


            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevTgtBin = null;

            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);

                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.IsDevFile)
                {
                    DevSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevSrcData);
                    DevTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevTgtData);

                    DevSrcBin.InitThreadSafePipelineCashier(100, false);
                    DevTgtBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainSrcData);
                    TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainTgtData);

                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

            }
        }
    }
}