﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// LSTM + Sequence Pooling + Sequence Label.
    /// </summary>
    public class GRUSeqPoolingLabelBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "LSTM Layer Dim"));
                Argument.Add("POOL-DROPOUT", new ParameterArgument("0", "LSTM Max-Pooling Dropout."));

                Argument.Add("NN-ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("NN-LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("NN-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("NN-LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.BinaryClassification).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));


                Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

                Argument.Add("CHECK-GRAD", new ParameterArgument("0", "Check Gradient!"));
                Argument.Add("DEBUG-TRAIN", new ParameterArgument("0", "Debugset for Training"));

            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static bool IsTrainFile { get { return !TrainData.Equals(string.Empty); } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

            public static int Debug_Train { get { return int.Parse(Argument["DEBUG-TRAIN"].Value); } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float POOL_DROPOUT { get { return float.Parse(Argument["POOL-DROPOUT"].Value); } }
            public static int[] NN_LAYER_DIM { get { return Argument["NN-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] NN_ACTIVATION { get { return Argument["NN-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] NN_LAYER_BIAS { get { return Argument["NN-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] NN_LAYER_DROPOUT { get { return Argument["NN-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

            public static string ScoreOutputPath
            {
                get
                {
                    if (!Argument["SCORE-PATH"].Value.Equals(string.Empty)) return Argument["SCORE-PATH"].Value;
                    else return LogFile + ".tmp.score";
                }
            }

            public static string MetricOutputPath
            {
                get
                {
                    if (!Argument["METRIC-PATH"].Value.Equals(string.Empty)) return Argument["METRIC-PATH"].Value;
                    else return LogFile + ".tmp.metric";
                }
            }

            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

            public static bool CheckGrad { get { return int.Parse(Argument["CHECK-GRAD"].Value) > 0; } }
        }

        public override BuilderType Type { get { return BuilderType.GRUSEQPOOL; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);

        }

        public ComputationGraph BuildComputationGraph(DataCashier<SeqSparseDataSource, SequenceDataStat> data, DNNRunMode mode, GRUStructure gruModel, DNNStructure dnnModel)
        {
            ComputationGraph cg = new ComputationGraph();

            DeviceType device = MathOperatorManager.Device(BuilderParameters.GPUID);
            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            SeqDenseBatchData gruOutput = (SeqDenseBatchData)cg.AddRunner(new GRUSparseRunner<SeqSparseBatchData>(gruModel.GRUCells[0], dataSource.SequenceData,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            for (int i = 1; i < gruModel.GRUCells.Count; i++)
                gruOutput = (SeqDenseBatchData)cg.AddRunner(new GRUDenseRunner<SeqDenseBatchData>(gruModel.GRUCells[i], gruOutput,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            HiddenBatchData poolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(gruOutput,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            HiddenBatchData dropOutput = poolingOutput;
            if (mode == DNNRunMode.Train && BuilderParameters.POOL_DROPOUT > 0 && BuilderParameters.POOL_DROPOUT < 1)
            {
                dropOutput = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(poolingOutput, BuilderParameters.POOL_DROPOUT,
                        new RunnerBehavior() { RunMode = mode, Device = device }));
            }

            HiddenBatchData labelOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(dnnModel, dropOutput,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            switch (mode)
            {
                case DNNRunMode.Train:
                    switch (BuilderParameters.LossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, labelOutput.Output, labelOutput.Deriv, BuilderParameters.Gamma,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                        case LossFunctionType.MultiClassification:
                            cg.AddObjective(new MultiClassSoftmaxRunner(dataSource.InstanceLabel, labelOutput.Output, labelOutput.Deriv, BuilderParameters.Gamma,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                    }

                    break;
                case DNNRunMode.Predict:
                    switch (BuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, labelOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                            break;
                        case EvaluationType.ACCURACY:
                            cg.AddRunner(new AccuracyDiskDumpRunner(dataSource.InstanceLabel, labelOutput.Output, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            //run lstm pooling runner.
            Logger.OpenLog(BuilderParameters.LogFile);

            MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    /// the example of building the computational graph. 
                    /// construct model parameters.
                    //CompositeNNStructure deepModel = new CompositeNNStructure();
                    GRUStructure gruModel = new GRUStructure(DataPanel.Train.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM);
                    DNNStructure dnnModel = new DNNStructure(BuilderParameters.LAYER_DIM.Last(),
                                                BuilderParameters.NN_LAYER_DIM,
                                                BuilderParameters.NN_ACTIVATION,
                                                BuilderParameters.NN_LAYER_DROPOUT,
                                                BuilderParameters.NN_LAYER_BIAS);

                    gruModel.InitOptimizer(new StructureLearner()
                    {
                        Optimizer = BuilderParameters.Optimizer,
                        LearnRate = BuilderParameters.LearnRate,
                        AdaBoost = BuilderParameters.AdaBoost,
                        ClipDelta = BuilderParameters.DELTACLIP,
                        ClipWeight = BuilderParameters.WEIGHTCLIP
                    });

                    dnnModel.InitOptimizer(new StructureLearner()
                    {
                        Optimizer = BuilderParameters.Optimizer,
                        LearnRate = BuilderParameters.LearnRate,
                        AdaBoost = BuilderParameters.AdaBoost,
                        ClipDelta = BuilderParameters.DELTACLIP,
                        ClipWeight = BuilderParameters.WEIGHTCLIP
                    });

                    /// construct single source computational graph.
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, DNNRunMode.Train, gruModel, dnnModel);
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, gruModel, dnnModel);
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        string modelName = string.Format(@"{0}\\LSTMPool.{1}.model", BuilderParameters.ModelOutputPath, iter);
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                        {
                            gruModel.Serialize(writer);
                            dnnModel.Serialize(writer);
                        }

                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = predCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                                File.Copy(string.Format(@"{0}\\LSTMPool.{1}.model", BuilderParameters.ModelOutputPath, bestValidIter.ToString()),
                                          string.Format(@"{0}\\LSTMPool.{1}.model", BuilderParameters.ModelOutputPath, "done"), true);
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }
                    }
                    Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                    break;
                case DNNRunMode.Predict:

                    GRUStructure pgruModel = null;
                    DNNStructure pdnnModel = null;
                    using (BinaryReader reader = new BinaryReader(new FileStream(BuilderParameters.ModelOutputPath, FileMode.Open, FileAccess.Read)))
                    {
                        pgruModel = new GRUStructure(reader, MathOperatorManager.Device(BuilderParameters.GPUID));
                        pdnnModel = new DNNStructure(reader.BaseStream, DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR, MathOperatorManager.Device(BuilderParameters.GPUID));
                    }
                    ComputationGraph pCG = BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, pgruModel, pdnnModel);
                    if (BuilderParameters.IsValidFile) validScore = pCG.Execute();
                    break;
            }

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (BuilderParameters.IsTrainFile)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainData);
                    Train.InitThreadSafePipelineCashier(100, true);
                }

                if (BuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }

            internal static void DeInit()
            {
                if (null != Train) Train.Dispose(); Train = null;
                if (null != Valid) Valid.Dispose(); Valid = null;
            }
        }
    }
}
