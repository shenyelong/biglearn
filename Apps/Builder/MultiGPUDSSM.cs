﻿using BigLearn;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.DeepNet
{
    /// <summary>
    /// In this builder, i want to support mult-gpu training with global parameter server. Hope it could work.
    /// Take LSTM Seq Label Model as an Example.
    /// </summary>
    public class MultiGPUDSSMBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.MULTI_GPU_DSSM; } }

        public override void InitStartup(string fileName)
        {
            DSSMGraphParameters.Parse(fileName);
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");
            DSSMGraphBuilder.DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            DeviceType mainDevice = MathOperatorManager.SetDevice(DSSMGraphParameters.GPUID);
            IMathOperationManager mainComputelib = MathOperatorManager.CreateInstance(mainDevice);
            DSSMStructure dssmModel = null;
            if (DSSMGraphParameters.SeedModel != string.Empty)
            {
                dssmModel = new DSSMStructure(new FileStream(DSSMGraphParameters.SeedModel, FileMode.Open, FileAccess.Read), DSSMGraphParameters.Device);
            }

            switch (DSSMGraphParameters.RunMode)
            {
                case DNNRunMode.Train:
                    Logger.WriteLog("Loading DSSM Structure.");
                    if(dssmModel == null)
                    {
                        dssmModel = new DSSMStructure(DSSMGraphParameters.Device,
                                DSSMGraphParameters.SimiType, DSSMGraphParameters.IsShareModel,
                                DSSMGraphBuilder.DataPanel.Train.Stat.SrcStat.FEATURE_DIM, DSSMGraphParameters.SRC_LAYER_DIM, DSSMGraphParameters.SRC_ACTIVATION,
                                DSSMGraphParameters.SRC_LAYER_BIAS, DSSMGraphParameters.SRC_ARCH, DSSMGraphParameters.SRC_WINDOW, DSSMGraphParameters.SRC_DROPOUT,
                                DSSMGraphBuilder.DataPanel.Train.Stat.TgtStat.FEATURE_DIM, DSSMGraphParameters.TGT_LAYER_DIM, DSSMGraphParameters.TGT_ACTIVATION,
                                DSSMGraphParameters.TGT_LAYER_BIAS, DSSMGraphParameters.TGT_ARCH, DSSMGraphParameters.TGT_WINDOW, DSSMGraphParameters.TGT_DROPOUT);
                    }
                    Logger.WriteLog(dssmModel.SrcDnn.DNN_Descr());
                    Logger.WriteLog(dssmModel.TgtDnn.DNN_Descr());
                    Logger.WriteLog("Load DSSM Structure Finished.");

                    /// Prediction Computation Graph.
                    ComputationGraph predCG = null;
                    if (DSSMGraphParameters.IsValidFile)
                        predCG = DSSMGraphBuilder.BuildComputationGraph(DSSMGraphBuilder.DataPanel.Valid, new RunnerBehavior()
                            { Computelib = mainComputelib, Device = mainDevice, RunMode = DNNRunMode.Predict }, dssmModel);


                    /// Global Model Parameter in CPU.
                    CudaPieceFloat modelParameter = new CudaPieceFloat(dssmModel.ParamNumber, true, false);
                    dssmModel.GetModelParameter(modelParameter, 0);
                    ParameterServer parameterServer = new ParameterServer(modelParameter, 10, 10);

                    /// Multiple Computation Graph for Data Partitions.
                    List<ComputationGraph> trainCGs = new List<ComputationGraph>();
                    List<Structure> trainCGStructure = new List<Structure>();

                    List<int> deviceList = new List<int>() { 1, 2 };
                    for (int i = 0; i < deviceList.Count; i++)
                    {
                        DeviceType threadDevice = MathOperatorManager.SetDevice(deviceList[i]);
                        IMathOperationManager threadComputelib = MathOperatorManager.CreateInstance(threadDevice);

                        DSSMStructure deviceStructure = new DSSMStructure(DSSMGraphParameters.Device,
                                DSSMGraphParameters.SimiType, DSSMGraphParameters.IsShareModel,
                                DSSMGraphBuilder.DataPanel.Train.Stat.SrcStat.FEATURE_DIM, DSSMGraphParameters.SRC_LAYER_DIM, DSSMGraphParameters.SRC_ACTIVATION,
                                DSSMGraphParameters.SRC_LAYER_BIAS, DSSMGraphParameters.SRC_ARCH, DSSMGraphParameters.SRC_WINDOW, DSSMGraphParameters.SRC_DROPOUT,
                                DSSMGraphBuilder.DataPanel.Train.Stat.TgtStat.FEATURE_DIM, DSSMGraphParameters.TGT_LAYER_DIM, DSSMGraphParameters.TGT_ACTIVATION,
                                DSSMGraphParameters.TGT_LAYER_BIAS, DSSMGraphParameters.TGT_ARCH, DSSMGraphParameters.TGT_WINDOW, DSSMGraphParameters.TGT_DROPOUT);

                        //SGDOptimizer optimizer = new SGDOptimizer(deviceStructure, DSSMGraphParameters.LearnRate, 0, new RunnerBehavior() { Device = DSSMGraphParameters.Device });
                        deviceStructure.InitOptimizer(new StructureLearner() { Optimizer = DNNOptimizerType.SGD, LearnRate = DSSMGraphParameters.LearnRate, device = threadDevice },
                                       new RunnerBehavior() { Device = threadDevice, Computelib = threadComputelib });

                        trainCGStructure.Add(deviceStructure);

                        ///Now the computation graph will be in device (i);
                        ComputationGraph trainCG = DSSMGraphBuilder.BuildComputationGraph(DSSMGraphBuilder.DataPanel.Train, new RunnerBehavior()
                        {
                            Computelib = threadComputelib,
                            Device = threadDevice,
                            RunMode = DNNRunMode.Train
                        }, deviceStructure);
                        trainCGs.Add(trainCG);
                    }

                    if (!Directory.Exists(DSSMGraphParameters.ModelOutputPath)) Directory.CreateDirectory(DSSMGraphParameters.ModelOutputPath);

                    /// Select Best Model During Training.
                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    for (int iter = 0; iter < DSSMGraphParameters.Iteration; iter++)
                    {
                        double loss = parameterServer.ExecuteTrainMultipleComputationGraph(deviceList, trainCGs, trainCGStructure);

                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\DSSM.{1}.model", 
                                DSSMGraphParameters.ModelOutputPath, iter.ToString()), FileMode.Create, FileAccess.Write)))
                            {
                                dssmModel.Serialize(writer);
                            }
                        }


                        Cudalib.CudaSetDevice(DSSMGraphParameters.GPUID);
                        dssmModel.SetModelParameter(parameterServer.ModelParameter, 0);

                        //pushLoop = pushLoop + 10;
                        //pullLoop = pullLoop + 10;
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\DSSM.{1}.model", DSSMGraphParameters.ModelOutputPath, iter.ToString()), FileMode.Create, FileAccess.Write)))
                            {
                                dssmModel.Serialize(writer);
                            }
                        }
                        if (DSSMGraphParameters.IsValidFile)
                        {
                            validScore = predCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                                using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\DSSM.{1}.model", DSSMGraphParameters.ModelOutputPath, "done"), FileMode.Create, FileAccess.Write)))
                                {
                                    dssmModel.Serialize(writer);
                                }
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }
                    }
                    Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                    /// To Do: Support Batch Training in Computation Graph.
                    break;
                case DNNRunMode.Predict:
                    Logger.WriteLog("Total Sample Number {0}", DSSMGraphBuilder.DataPanel.Valid.Stat.SrcStat.TotalSampleNumber);
                    if (DSSMGraphParameters.IsValidFile)
                    {
                        ComputationGraph cg = DSSMGraphBuilder.BuildComputationGraph(DSSMGraphBuilder.DataPanel.Valid, new RunnerBehavior()
                        {
                            RunMode = DNNRunMode.Predict,
                            Computelib = mainComputelib,
                            Device = mainDevice
                        }, dssmModel);
                        cg.Execute();
                    }
                    break;
            }
            Logger.CloseLog();
        }

    }
}
