﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class MultiAgentDeepQNetworkBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }

            public static string EnvirOutputPath { get { return Argument["ENVIR-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }

            public static float WeightInitBias { get { return float.Parse(Argument["INIT-WEIGHT-BIAS"].Value); } }

            public static float WeightInitScale { get { return float.Parse(Argument["INIT-WEIGHT-SCALE"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int STOCHASTIC_QLEARN { get { return int.Parse(Argument["STOCHASTIC-QLEARN"].Value); } }

            public static List<Tuple<int, float>> ScheduleEpsilon
            {
                get
                {
                    return Argument["SCHEDULE-EPSILON"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            /// <summary>
            /// Only need to Specify Data and Archecture of Deep Nets.
            /// </summary>
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));

                Argument.Add("LAYER-DIM", new ParameterArgument("100,4", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(string.Format("{0},{1}", (int)A_Func.Tanh, (int)A_Func.Linear), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1,1", "DNN Layer Bias"));
                Argument.Add("LAYER-DROPOUT", new ParameterArgument("0,0", "DNN Layer Dropout"));

                Argument.Add("MODEL-PATH", new ParameterArgument(@"\\DEEP-06\Test-JDSSM\Model_Path\", "Model Path"));
                Argument.Add("ENVIR-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));

                Argument.Add("INIT-WEIGHT-BIAS", new ParameterArgument("0", "Init Model Weight Bias"));
                Argument.Add("INIT-WEIGHT-SCALE", new ParameterArgument("-1", "Init Model Weight Scale"));

                Argument.Add("SCHEDULE-EPSILON", new ParameterArgument("0:1,100000:0.1,1000000:0.01", "Epsilon Schedule"));

                Argument.Add("STOCHASTIC-QLEARN", new ParameterArgument("0", "0 : No Stochastic Q; 1 : Non Shared Stochastic Q; 2 : Shared Stochastic"));
            }
        }

        public override BuilderType Type { get { return BuilderType.MULTI_DQN; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            OptimizerParameters.Parse(fileName);

            if (BuilderParameters.Device == DeviceType.GPU) Cudalib.CudaInit(BuilderParameters.GPUID);
        }

        
        public class AgentType1
        {
            DNNStructure QNetwork = null;
            DNNStructure TargetNetwork = null;

            GameEnvironment Environ { get; set; }
            GameDataCashier Sense { get; set; }

            public ComputationGraph CG { get; set; }
            public AgentType1(GameEnvironment environ, int id)
            {
                Environ = environ;
                Sense = new GameDataCashier(Environ, id);

                Logger.WriteLog("Init Agent DNN Structure.");
                
                QNetwork = new DNNStructure(Sense.StatusFeatureDim,
                        BuilderParameters.LAYER_DIM,
                        BuilderParameters.ACTIVATION,
                        BuilderParameters.LAYER_BIAS);
                TargetNetwork = new DNNStructure(Sense.StatusFeatureDim, BuilderParameters.LAYER_DIM, BuilderParameters.ACTIVATION, BuilderParameters.LAYER_BIAS);
                TargetNetwork.Init(QNetwork);

                QNetwork.InitOptimizer(new StructureLearner()
                {
                    LearnRate = OptimizerParameters.LearnRate,
                    Optimizer = OptimizerParameters.Optimizer,
                    EpochNum = OptimizerParameters.Iteration,
                    ShrinkLR = OptimizerParameters.ShrinkLR
                });

                Logger.WriteLog(QNetwork.DNN_Descr());

                CG = BuildComputationGraph();
            }

            public ComputationGraph BuildComputationGraph()
            {
                ComputationGraph cg = new ComputationGraph();

                CudaMathOperation computeLib = new CudaMathOperation(true, true);

                GeneralBatchInputData senseInput = (GeneralBatchInputData)cg.AddRunner(new SenseRunner(Sense,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });

                HiddenBatchData actionInput = (HiddenBatchData)cg.AddRunner(new FullyConnectInputRunner<GeneralBatchInputData>(QNetwork.NeuralLinks[0], senseInput,
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device, Computelib = computeLib }){ IsBackProp = false });
                actionInput = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(actionInput, BuilderParameters.LAYER_DROPOUT[0],
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device, Computelib = computeLib }){ IsBackProp = false });
                for (int i = 1; i < QNetwork.NeuralLinks.Count; i++)
                {
                    actionInput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(QNetwork.NeuralLinks[i], actionInput,
                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });
                    actionInput = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(actionInput, BuilderParameters.LAYER_DROPOUT[i],
                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device, Computelib = computeLib }){ IsBackProp = false });
                }
                cg.AddRunner(new ActionRunner(Sense, actionInput, BuilderParameters.ScheduleEpsilon,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });

                ReplayBatchData replayData = (ReplayBatchData)cg.AddRunner(new ReplayMemoryDataRunner(Sense,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });

                cg.AddRunner(new ModelSyncRunner(QNetwork, TargetNetwork, 512,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });

                List<CudaPieceInt> dropOutMask = new List<CudaPieceInt>();
                HiddenBatchData targetAction = (HiddenBatchData)cg.AddRunner(new FullyConnectInputRunner<GeneralBatchInputData>(TargetNetwork.NeuralLinks[0], replayData.Target,
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });
                if (BuilderParameters.STOCHASTIC_QLEARN > 0)
                {
                    DropOutProcessor<HiddenBatchData> tmpDropout = new DropOutProcessor<HiddenBatchData>(targetAction, BuilderParameters.LAYER_DROPOUT[0],
                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false };
                    targetAction = (HiddenBatchData)cg.AddRunner(tmpDropout);
                    dropOutMask.Add(tmpDropout.DropoutMask);
                }

                for (int i = 1; i < TargetNetwork.NeuralLinks.Count; i++)
                {
                    targetAction = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(TargetNetwork.NeuralLinks[i], targetAction,
                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });
                    if (BuilderParameters.STOCHASTIC_QLEARN > 0)
                    {
                        DropOutProcessor<HiddenBatchData> tmpDropout = new DropOutProcessor<HiddenBatchData>(targetAction, BuilderParameters.LAYER_DROPOUT[i],
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false };
                        targetAction = (HiddenBatchData)cg.AddRunner(tmpDropout);
                        dropOutMask.Add(tmpDropout.DropoutMask);
                        //targetAction = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(targetAction, BuilderParameters.LAYER_DROPOUT[i],
                        //    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device }) { IsBackProp = false });
                    }
                }
                //HiddenBatchData targetAction = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(TargetNetwork, replayData.Target,
                //    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

                DenseBatchData updateReward = (DenseBatchData)cg.AddRunner(new RewardUpdateRunner(replayData.Reward, targetAction,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });

                //HiddenBatchData sourceAction = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(QNetwork, replayData.Source,
                //    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }));

                HiddenBatchData sourceAction = (HiddenBatchData)cg.AddRunner(new FullyConnectInputRunner<GeneralBatchInputData>(QNetwork.NeuralLinks[0], replayData.Source,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }));

                if (BuilderParameters.STOCHASTIC_QLEARN == 1)
                { 
                    sourceAction = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(sourceAction, BuilderParameters.LAYER_DROPOUT[0],
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }));
                }
                else if (BuilderParameters.STOCHASTIC_QLEARN == 2)
                {
                    sourceAction = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(sourceAction, BuilderParameters.LAYER_DROPOUT[0], dropOutMask[0],
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }));
                }

                for (int i = 1; i < QNetwork.NeuralLinks.Count; i++)
                {
                    sourceAction = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(QNetwork.NeuralLinks[i], sourceAction,
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }));
                    if (BuilderParameters.STOCHASTIC_QLEARN == 1)
                    {
                        sourceAction = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(sourceAction, BuilderParameters.LAYER_DROPOUT[i],
                           new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }));
                    }
                    else if(BuilderParameters.STOCHASTIC_QLEARN == 2)
                    {
                        sourceAction = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(sourceAction, BuilderParameters.LAYER_DROPOUT[i], dropOutMask[i],
                            new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }));
                    }
                }

                cg.AddObjective(new MSERunner(sourceAction.Output, updateReward, replayData.Action.Data, sourceAction.Deriv,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device, Computelib = computeLib }) { IsBackProp = false });
                return cg;
            }

        }

        public class AgentType2
        {
            DNNStructure QNetwork = null;
            DNNStructure TargetNetwork = null;

            GameEnvironment Environ { get; set; }
            GameDataCashier Sense { get; set; }

            public ComputationGraph CG { get; set; }
            public AgentType2(GameEnvironment environ, int id)
            {
                Environ = environ;
                Sense = new GameDataCashier(Environ, id);

                Logger.WriteLog("Init Agent DNN Structure.");
                
                //LAYER-DIM	32,32,16,4	300,300	,128
                //ACTIVATION	1,1,1,0	1,1	,1
                //LAYER-BIAS	1,1,1,0	0,0	,0

                QNetwork = new DNNStructure(Sense.StatusFeatureDim,
                        new int[] { 4 }, new A_Func[] { A_Func.Linear }, new bool[] { false });

                TargetNetwork = new DNNStructure(Sense.StatusFeatureDim,
                    new int[] { 4 }, new A_Func[] { A_Func.Linear }, new bool[] { false });

                TargetNetwork.Init(QNetwork);

                QNetwork.InitOptimizer(new StructureLearner()
                {
                    LearnRate = OptimizerParameters.LearnRate,
                    Optimizer = OptimizerParameters.Optimizer,
                    EpochNum = OptimizerParameters.Iteration,
                    ShrinkLR = OptimizerParameters.ShrinkLR
                });

                Logger.WriteLog(QNetwork.DNN_Descr());
                CG = BuildComputationGraph();
            }

            public ComputationGraph BuildComputationGraph()
            {
                ComputationGraph cg = new ComputationGraph();

                GeneralBatchInputData senseInput = (GeneralBatchInputData)cg.AddRunner(new SenseRunner(Sense,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

                HiddenBatchData actionInput = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(QNetwork, senseInput,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

                cg.AddRunner(new ActionRunner(Sense, actionInput, BuilderParameters.ScheduleEpsilon,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

                ReplayBatchData replayData = (ReplayBatchData)cg.AddRunner(new ReplayMemoryDataRunner(Sense,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

                cg.AddRunner(new ModelSyncRunner(QNetwork, TargetNetwork, 512,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

                HiddenBatchData targetAction = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(TargetNetwork, replayData.Target,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

                DenseBatchData updateReward = (DenseBatchData)cg.AddRunner(new RewardUpdateRunner(replayData.Reward, targetAction,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

                HiddenBatchData sourceAction = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(QNetwork, replayData.Source,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }));

                cg.AddObjective(new MSERunner(sourceAction.Output, updateReward, replayData.Action.Data, sourceAction.Deriv,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });
                return cg;
            }
        }

        public override void Rock()
        {

            Logger.OpenLog(BuilderParameters.LogFile);

            GameEnvironment enviroument = new GameEnvironment(BuilderParameters.EnvirOutputPath, new List<int>() { 1, 1 });
            
            AgentType1 agentA = new AgentType1(enviroument, 0);
            //AgentType2 agentB = new AgentType2(enviroument, 1);

            Logger.WriteLog("Init Agent Finished.");

            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            agentA.CG.Init();
            //agentB.CG.Init();
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                agentA.CG.Step();
                //agentB.CG.Step();
            }
            agentA.CG.Complete();
            //agentB.CG.Complete();
            enviroument.CloseReplayStream();
            Logger.CloseLog();
        }
    }
}
