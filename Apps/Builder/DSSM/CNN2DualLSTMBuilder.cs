﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class CNN2DualLSTMDSSMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static string TrainStream { get { return Argument["TRAIN"].Value; } }
            public static string L3GVocab { get { return Argument["L3G-VOCAB"].Value; } }
            public static string WordVocab { get { return Argument["WORD-VOCAB"].Value; } }

            public static string TrainSrcBin { get { return Argument["TRAIN-SRC"].Value; } }
            public static string TrainTgtBin { get { return Argument["TRAIN-TGT"].Value; } }

            public static string ValidateSrcBin { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidateTgtBin { get { return Argument["VALID-TGT"].Value; } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static PoolingType PoolingType { get { return (PoolingType)int.Parse(Argument["POOLING"].Value); } }


            public static SimilarityType SimiType { get { return (SimilarityType)(int.Parse(Argument["SIM-TYPE"].Value)); } }

            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }

            public static string SRC_EMBED_FILE { get { return Argument["SRC-EMBED-FILE"].Value; } }
            public static string TGT_EMBED_FILE { get { return Argument["TGT-EMBED-FILE"].Value; } }

            public static DeviceType Device { get { return MathOperatorManager.Device(GPUID); } }

            public static int[] SRC_CNN_LAYER_DIM { get { return Argument["SRC-CNN-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] SRC_LSTM_LAYER_DIM { get { return Argument["SRC-LSTM-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] TGT_CNN_LAYER_DIM { get { return Argument["TGT-CNN-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int WORD_LENGTH_LIMIT { get { return int.Parse(Argument["WORD-LENGTH-LIMIT"].Value); } }

            public static string SENT_SEPARATOR { get { return Argument["SENT-SEPARATOR"].Value; } }

            public static A_Func[] SRC_CNN_ACTIVATION { get { return Argument["SRC-CNN-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static A_Func[] TGT_CNN_ACTIVATION { get { return Argument["SRC-CNN-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static float[] SRC_CNN_DROPOUT { get { return Argument["SRC-CNN-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static float[] TGT_CNN_DROPOUT { get { return Argument["TGT-CNN-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            static BuilderParameters()
            {
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "Src Train Data."));
                Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "Tgt Train Data."));
                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "Src Validation Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "Tgt Validation Data."));
                Argument.Add("L3G-VOCAB", new ParameterArgument(string.Empty, "Tri-letter vocabulary."));
                Argument.Add("WORD-VOCAB", new ParameterArgument(string.Empty, "Word vocabulary"));
                Argument.Add("MINI-BATCH", new ParameterArgument(string.Empty, "Mini Batch Size."));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.SampleSoftmax).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
                Argument.Add("NTRAIL", new ParameterArgument("4", "Negative Sample Number."));
                Argument.Add("WORD-LENGTH-LIMIT", new ParameterArgument("30", "Maximal Word Length."));

                Argument.Add("GPUID", new ParameterArgument("0", "GPU Device ID"));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.NDCG).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));
                Argument.Add("SRC-EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether src embedding is outputted"));
                Argument.Add("TGT-EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether tgt embedding is outputted"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));
                Argument.Add("POOLING", new ParameterArgument(@"0", "Pooling Type"));

                Argument.Add("SRC-CNN-LAYER-DIM", new ParameterArgument(@"300,512", "Source CNN"));
                Argument.Add("SRC-LSTM-LAYER-DIM", new ParameterArgument(@"512", "Source LSTM"));
                Argument.Add("TGT-CNN-LAYER-DIM", new ParameterArgument(@"300,512", "TARGET CNN"));
                Argument.Add("SRC-CNN-DROPOUT", new ParameterArgument(@"0,0", "SRC CNN DROPOUT"));
                Argument.Add("TGT-CNN-DROPOUT", new ParameterArgument(@"0,0", "TGT CNN DROPOUT"));
                Argument.Add("SRC-CNN-ACTIVATION", new ParameterArgument(@"1,1", "SRC CNN ACTIVATION"));
                Argument.Add("TGT-CNN-ACTIVATION", new ParameterArgument(@"1,1", "TGT CNN ACTIVATION"));

                //SentSpliters
                Argument.Add("SENT-SEPARATOR", new ParameterArgument(@"###", "SENTENCE SEPARATOR"));

            }
        }

        public override BuilderType Type { get { return BuilderType.CNN_DUAL_LSTM_DSSM; } }

        /// <summary>
        /// example config 1. : \\gcr\Teams\B99\BigLearn\PublicDataSource\LSTM-DSSM-V3\LSTM-Pair.config
        /// </summary>
        /// <param name="fileName"></param>
        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            OptimizerParameters.Parse(fileName);
        }

        public class RandomSequenceMemoryHelpRunner : StructRunner<Structure, BatchData>
        {
            public new MemoryQAHelpData Output { get { return (MemoryQAHelpData)base.Output; } set { base.Output = value; } }

            SeqDenseBatchData InputData;
            RecurrentSeqInfo RecussiveInfo = null;
            BiMatchBatchData Match = null;

            public BiMatchBatchData NewMatchData = null;
            public RandomSequenceMemoryHelpRunner(BiMatchBatchData match, SeqDenseBatchData data, RecurrentSeqInfo recurrsiveInfo, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                InputData = data;
                RecussiveInfo = recurrsiveInfo;
                Match = match;
                Output = new MemoryQAHelpData(match.Stat.MAX_MATCH_BATCHSIZE, data.Stat.MAX_SEQUENCESIZE * (Match.Dim), Behavior.Device);
                NewMatchData = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_MATCH_BATCHSIZE = data.Stat.MAX_BATCHSIZE * (Match.Dim),
                    MAX_SRC_BATCHSIZE = data.Stat.MAX_BATCHSIZE * (Match.Dim),
                    MAX_TGT_BATCHSIZE = data.Stat.MAX_BATCHSIZE
                }, Behavior.Device);
                NewMatchData.Dim = (Match.Dim);
            }

            public override void Forward()
            {
                //if (Match.NTrial >= 0) { Match.GenerateMatch(InputData.BatchSize); }
                
                InputData.SampleIdx.SyncToCPU(InputData.BatchSize);
                int memIdx = 0;
                for (int i = 0; i < Match.MatchSize; i++)
                {
                    // pick one match.
                    int srcId = Match.SrcIdx.MemPtr[i];
                    int tgtId = Match.TgtIdx.MemPtr[i];

                    int memBgn = srcId == 0 ? 0 : InputData.SampleIdx.MemPtr[srcId - 1];
                    int memEnd = InputData.SampleIdx.MemPtr[srcId];

                    for (int m = memBgn; m < memEnd; m++)
                    {
                        Output.MatchSrc.MemPtr[memIdx] = tgtId;
                        int newPos = RecussiveInfo == null ? m : RecussiveInfo.MapForwardMem[m];
                        Output.MatchMem.MemPtr[memIdx] = newPos;
                        Output.MatchOut.MemPtr[memIdx] = i;
                        memIdx += 1;
                    }
                    Output.BatchMemIdx.MemPtr[i] = memIdx;// memEnd - memBgn + (i == 0 ? 0 : Output.BatchMemIdx.MemPtr[i - 1]);
                }
                Output.BatchSize = Match.MatchSize;
                Output.MemorySize = memIdx;// Output.BatchMemIdx.MemPtr[Match.MatchSize - 1];
                
                Output.BatchMemIdx.SyncFromCPU(Output.BatchSize);
                Output.MatchSrc.SyncFromCPU(Output.MemorySize);
                Output.MatchMem.SyncFromCPU(Output.MemorySize);
                Output.MatchOut.SyncFromCPU(Output.MemorySize);

                NewMatchData.MatchSize = InputData.BatchSize * (Match.Dim);
                NewMatchData.SrcSize = InputData.BatchSize * (Match.Dim); 
                NewMatchData.TgtSize = InputData.BatchSize;

                for (int b = 0; b < InputData.BatchSize; b++)
                {
                    for (int i = 0; i < Match.Dim; i++)
                    {
                        NewMatchData.SrcIdx.MemPtr[b * (Match.Dim) + i] = b * (Match.Dim) + i;
                        NewMatchData.TgtIdx.MemPtr[b * (Match.Dim) + i] = Match.TgtIdx.MemPtr[b * (Match.Dim) + i];
                        NewMatchData.MatchInfo.MemPtr[b * (Match.Dim) + i] = 0;
                    }
                    NewMatchData.MatchInfo.MemPtr[b * (Match.Dim)] = 1;
                }

                Util.InverseMatchIdx(NewMatchData.SrcIdx.MemPtr, NewMatchData.MatchSize, NewMatchData.Src2MatchIdx.MemPtr, NewMatchData.Src2MatchElement.MemPtr, NewMatchData.SrcSize);
                Util.InverseMatchIdx(NewMatchData.TgtIdx.MemPtr, NewMatchData.MatchSize, NewMatchData.Tgt2MatchIdx.MemPtr, NewMatchData.Tgt2MatchElement.MemPtr, NewMatchData.TgtSize);

                NewMatchData.SrcIdx.SyncFromCPU(NewMatchData.MatchSize);
                NewMatchData.TgtIdx.SyncFromCPU(NewMatchData.MatchSize);
                NewMatchData.MatchInfo.SyncFromCPU(NewMatchData.MatchSize);
                NewMatchData.Src2MatchIdx.SyncFromCPU(NewMatchData.SrcSize);
                NewMatchData.Src2MatchElement.SyncFromCPU(NewMatchData.MatchSize);
                NewMatchData.Tgt2MatchIdx.SyncFromCPU(NewMatchData.TgtSize);
                NewMatchData.Tgt2MatchElement.SyncFromCPU(NewMatchData.MatchSize);
            }
        }

        public static ComputationGraph BuildComputationGraph(DataCashier<SeqSentenceBatchInputData, SeqSentenceInputDataStat> srcData,
                                                             DataCashier<SeqSparseBatchData, SequenceDataStat> tgtData,
            DNNRunMode mode, DNNStructure srcSentenceModel, LSTMStructure srcParagraphLSTM, DNNStructure tgtSentenceModel,
            PoolingType pt)
        {
            ComputationGraph cg = new ComputationGraph();

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            CudaMathOperation lib = new CudaMathOperation(true, true);
            RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };

            /**************** Get Data from DataCashier *********/
            SeqSentenceBatchInputData srcParagraphData = (SeqSentenceBatchInputData)cg.AddDataRunner(
                new DataRunner<SeqSentenceBatchInputData, SeqSentenceInputDataStat>(srcData, Behavior));

            SeqSparseBatchData tgtSentenceData = (SeqSparseBatchData)cg.AddDataRunner(
                new DataRunner<SeqSparseBatchData, SequenceDataStat>(tgtData, Behavior));

            SeqSparseBatchData srcSentenceData = (SeqSparseBatchData)cg.AddRunner(
                new Transform_SeqSentence2SequenceRunner(srcParagraphData, Behavior));

            //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dssmData.SrcInput, Behavior));
            //SeqHelpData TgtHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dssmData.TgtInput, Behavior));

            /*************** CNN for Source Input. ********************/
            HiddenBatchData srcSentenceOutput = (HiddenBatchData)cg.AddRunner(
                new DNNRunner<SeqSparseBatchData>(srcSentenceModel, srcSentenceData, Behavior));

            SeqDenseBatchData srcMultiSentOutput = (SeqDenseBatchData)cg.AddRunner(
                new Transform_Matrix2SeqMatrixRunner(srcSentenceOutput, srcParagraphData, Behavior));

            /// LSTM model needs to be speedup.
            //SeqDenseBatchData srclstmOutput = (SeqDenseBatchData)cg.AddRunner(
            //    new LSTMDenseRunner<SeqDenseBatchData>(srcParagraphLSTM.LSTMCells[0], srcMultiSentOutput, Behavior));
            SeqDenseRecursiveData srclstmInput = (SeqDenseRecursiveData)cg.AddRunner(
                new Cook_Seq2TransposeSeqRunner(srcMultiSentOutput, false, Behavior));
            SeqDenseRecursiveData srclstmOutput_tmp = (SeqDenseRecursiveData)cg.AddRunner(
                new FastLSTMDenseRunner<SeqDenseRecursiveData>(srcParagraphLSTM.LSTMCells[0], srclstmInput, Behavior));
            SeqDenseRecursiveData srclstmOutput = srclstmOutput_tmp;
            //SeqDenseBatchData srclstmOutput =  (SeqDenseBatchData)cg.AddRunner(
            //    new Cook_TransposeSeq2SeqRunner(srclstmOutput_tmp, Behavior));

            /*************** CNN for target Input. ********************/
            HiddenBatchData tgtSentenceOutput = (HiddenBatchData)cg.AddRunner(
                new DNNRunner<SeqSparseBatchData>(tgtSentenceModel, tgtSentenceData, Behavior));


            /*************** Sample Source-Target Match Data. ********************/
            //BiMatchBatchData matchData = new BiMatchBatchData(tgtSentenceData.Stat.MAX_BATCHSIZE,
            //    mode == DNNRunMode.Train ? BuilderParameters.NTrail : 0, BuilderParameters.Device, DSMlib.BuilderParameters.RandomSeed != 13);


            IntArgument arg = new IntArgument("batchSize");
            cg.AddRunner(new HiddenDataBatchSizeRunner(tgtSentenceOutput, arg, Behavior));
            BiMatchBatchData matchData = (BiMatchBatchData)cg.AddRunner(new RandomBiMatchRunner(tgtSentenceData.Stat.MAX_BATCHSIZE, 
                mode == DNNRunMode.Train ? BuilderParameters.NTrail : 0, arg, Behavior, DeepNet.BuilderParameters.RandomSeed != 13));


            /*************** similarity between source text and target text. ********************/
            HiddenBatchData simiOutput = null;

            /*************** Pooling LSTM over Source Input. ********************/
            HiddenBatchData srcOutput = null;
            switch(pt)
            {
                case PoolingType.MAX:
                    {
                        if (BuilderParameters.RunMode == DNNRunMode.Train) Console.WriteLine("USING Max Pooling");
                        srcOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(srclstmOutput, srclstmOutput.MapForward, Behavior));
                        break;
                    }
                case PoolingType.LAST:
                    {
                        if (BuilderParameters.RunMode == DNNRunMode.Train) Console.WriteLine("Using Last Pooling");
                        srcOutput = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(srclstmOutput, true, 0, srclstmOutput.MapForward, Behavior));
                        break;
                    }
                case PoolingType.AVG:
                    {
                        throw new Exception("AVG no supported yet");
                    }
                case PoolingType.ATT:
                    {
                        HiddenBatchData memoryData = new HiddenBatchData(srclstmOutput.MAX_SENTSIZE, srclstmOutput.Dim, srclstmOutput.SentOutput, srclstmOutput.SentDeriv, Behavior.Device);
                        Console.WriteLine("Using Attention Pooling");
                        if (mode == DNNRunMode.Predict)
                        {
                            MemoryQAHelpData memoryHelpData = (MemoryQAHelpData)cg.AddRunner(new SequenceMemoryHelpRunner(srclstmOutput, srclstmOutput.RecurrentInfo, Behavior));
                            srcOutput = (HiddenBatchData)cg.AddRunner(new ExternalMemoryRunner(tgtSentenceOutput, memoryHelpData, memoryData, memoryData, Behavior));
                        }
                        else
                        {
                            RandomSequenceMemoryHelpRunner sampleRunner = new RandomSequenceMemoryHelpRunner(matchData, srclstmOutput, srclstmOutput.RecurrentInfo, Behavior);
                            MemoryQAHelpData memoryHelpData = (MemoryQAHelpData)cg.AddRunner(sampleRunner);
                            srcOutput = (HiddenBatchData)cg.AddRunner(new ExternalMemoryRunner(tgtSentenceOutput, memoryHelpData, memoryData, memoryData, Behavior));

                            simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(srcOutput, tgtSentenceOutput, sampleRunner.NewMatchData,
                                             BuilderParameters.SimiType, new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
                        }
                        break;
                    }
                default:
                    {
                        throw new Exception("unknown pooling option");
                    }
            }


            

            if (pt == PoolingType.ATT && mode == DNNRunMode.Train)
            {
            }
            else
            {
                simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(srcOutput, tgtSentenceOutput, matchData,
                                             BuilderParameters.SimiType, new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
            }
            switch (mode)
            {
                case DNNRunMode.Train:
                    switch (BuilderParameters.LossFunction)
                    {
                        case LossFunctionType.SampleSoftmax:
                            cg.AddObjective(new MultiClassSoftmaxRunner(null, simiOutput.Output, simiOutput.Deriv, BuilderParameters.Gamma,
                                                new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
                            break;
                        case LossFunctionType.SampleLogisicalRegression:
                            cg.AddObjective(new CrossEntropyRunner(matchData.MatchInfo, simiOutput.Output, simiOutput.Deriv, BuilderParameters.Gamma,
                                                new RunnerBehavior() { RunMode = mode, Device = BuilderParameters.Device }));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    if (!BuilderParameters.SRC_EMBED_FILE.Equals(string.Empty))
                    {
                        cg.AddRunner(new VectorDumpRunner(srcOutput.Output, BuilderParameters.SRC_EMBED_FILE));
                    }

                    if (!BuilderParameters.TGT_EMBED_FILE.Equals(string.Empty))
                    {
                        cg.AddRunner(new VectorDumpRunner(tgtSentenceOutput.Output, BuilderParameters.TGT_EMBED_FILE));
                    }

                    break;
            }

            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            DeviceType device = BuilderParameters.Device;
            if (device == DeviceType.GPU) Cudalib.CudaInit(BuilderParameters.GPUID);

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    {
                        CompositeNNStructure hybridModel = new CompositeNNStructure();
                        if (string.IsNullOrEmpty(BuilderParameters.SEED_MODEL) == false)
                        {
                            Console.WriteLine("Loading Seed Model {0}", BuilderParameters.SEED_MODEL);
                            hybridModel = new CompositeNNStructure(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
                            DateTime pre = DateTime.Now;
                            Logger.WriteLog("Initial L1 Norm:{0}, Take time:{1} for calculation", 
                                hybridModel.L1Norm(), DateTime.Now.Subtract(pre));
                        }
                        else
                        {
                        Logger.WriteLog("Model Structure Initalization.");
                        // Init Model Structure.
                            DNNStructure cnnSrcSeqModel = new DNNStructure(
                                DataPanel.TrainSrcBin.Stat.MAX_FEATUREDIM, BuilderParameters.SRC_CNN_LAYER_DIM, BuilderParameters.SRC_CNN_ACTIVATION, new bool[] { false, false },
                             new N_Type[] { N_Type.Convolution_layer, N_Type.Fully_Connected }, new int[] { 3, 1 }, BuilderParameters.SRC_CNN_DROPOUT, DeviceType.GPU);

                            LSTMStructure lstmSrcSeqModel = new LSTMStructure(BuilderParameters.SRC_CNN_LAYER_DIM.Last(), BuilderParameters.SRC_LSTM_LAYER_DIM, DeviceType.GPU);


                            DNNStructure cnnTgtSeqModel = new DNNStructure(
                                DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.TGT_CNN_LAYER_DIM, BuilderParameters.TGT_CNN_ACTIVATION, new bool[] { false, false },
                             new N_Type[] { N_Type.Convolution_layer, N_Type.Fully_Connected }, new int[] { 3, 1 }, BuilderParameters.TGT_CNN_DROPOUT, DeviceType.GPU);


                        hybridModel.AddLayer(cnnSrcSeqModel);
                        hybridModel.AddLayer(lstmSrcSeqModel);
                        hybridModel.AddLayer(cnnTgtSeqModel);
                        }
                        
                        hybridModel.InitOptimizer(OptimizerParameters.StructureOptimizer);
                        ComputationGraph cg = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin,
                            DNNRunMode.Train, hybridModel.CompositeLinks[0] as DNNStructure, hybridModel.CompositeLinks[1] as LSTMStructure, hybridModel.CompositeLinks[2] as DNNStructure, BuilderParameters.PoolingType);

                        for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                        {
                            double loss = cg.Execute();
                            //Logger.WriteLog("Iteration {0}, Total MiniBatch {1}, Avg Loss {2}, and CosSim = {3}/{4}={5}, L1Norm:{6}", iter, DataPanel.TrainSrcBin.Stat.TotalBatchNumber, loss,
                            //        DSSMLossDerivParameter.COSINE_SIM_SCORE, DataPanel.TrainSrcBin.Stat.TotalSampleNumber, DSSMLossDerivParameter.COSINE_SIM_SCORE / DataPanel.TrainSrcBin.Stat.TotalSampleNumber,
                            //        hybridModel.L1Norm());
                            //DSSMLossDerivParameter.COSINE_SIM_SCORE = 0.0;

                            #region model serialization
                            if (Directory.Exists(BuilderParameters.ModelOutputPath) == false)
                                Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
                            string model_prefix = Path.Combine(BuilderParameters.ModelOutputPath, string.Format("model_{0}", iter));
                            hybridModel.Serialize(new BinaryWriter(new FileStream(model_prefix, FileMode.Create, FileAccess.Write, FileShare.None, FileUtil.BUFFER_SIZE)));
                            #endregion
                        }
                        break;
                    }
                case DNNRunMode.Predict:
                    {
                        #region load model
                        Logger.WriteLog("Loading Model Structure.");
                        CompositeNNStructure nnmodel = new CompositeNNStructure(new BinaryReader(new FileStream(BuilderParameters.ModelOutputPath, FileMode.Open, FileAccess.Read)), device);

                        #endregion
                        ComputationGraph cg = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin,
                            DNNRunMode.Predict, nnmodel.CompositeLinks[0] as DNNStructure, nnmodel.CompositeLinks[1] as LSTMStructure, 
                            nnmodel.CompositeLinks[2] as DNNStructure, BuilderParameters.PoolingType);
                        cg.Execute();
                        break;
                    }
            }

        
        }
            

        public class DataPanel
        {
            public static DataCashier<SeqSentenceBatchInputData, SeqSentenceInputDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainTgtBin = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train && (File.Exists(BuilderParameters.TrainSrcBin) == false
                    || File.Exists(BuilderParameters.TrainTgtBin) == false))
                {
                    // convert the text string 2 binary.
                    string[] SentSpliters = BuilderParameters.SENT_SEPARATOR.Split("|".ToArray());//new string[] { "###" };
                    string[] WordSpliters = TextTokenizeParameters.WordTokens;

                    Console.WriteLine("Load L3G Vocab File {0} AND Word Vocab:{1}.....", BuilderParameters.L3GVocab, BuilderParameters.WordVocab);

                    L3GFreqWordTextHash wordHash = new L3GFreqWordTextHash(BuilderParameters.WordVocab, BuilderParameters.L3GVocab,
                        wordSplitters: WordSpliters, maxWordLength:BuilderParameters.WORD_LENGTH_LIMIT);
                    wordHash.TextTokenizer = new SimpleTextTokenizer(WordSpliters, SentSpliters, int.MaxValue, BuilderParameters.WORD_LENGTH_LIMIT, int.MaxValue);

                    BinaryWriter srcDataWriter = new BinaryWriter(new FileStream(BuilderParameters.TrainSrcBin, FileMode.Create, FileAccess.Write));
                    SeqSentenceBatchInputData srcBinary = new SeqSentenceBatchInputData(new SeqSentenceInputDataStat() { MAX_FEATUREDIM = wordHash.VocabSize }, DeviceType.CPU);
                    

                    BinaryWriter tgtDataWriter = new BinaryWriter(new FileStream(BuilderParameters.TrainTgtBin, FileMode.Create, FileAccess.Write));
                    SeqSparseBatchData tgtBinary = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = wordHash.VocabSize }, DeviceType.CPU);
                    
                    using (StreamReader reader = new StreamReader(BuilderParameters.TrainStream))
                    {
                        int rowIdx = 0;
                        while (!reader.EndOfStream)
                        {
                            string[] items = reader.ReadLine().Split('\t');
                            string src = items[1].Trim().ToLower();
                            string tgt = items[2].Trim().ToLower();

                            List<List<Dictionary<int, float>>> src_fea = wordHash.MultiSeqFeatureExtract(src); // WordSpliters, SentSpliters, int.MaxValue, int.MaxValue, int.MaxValue, BuilderParameters.WORD_LENGTH_LIMIT);
                            List<Dictionary<int, float>> tgt_fea = wordHash.SeqFeatureExtract(tgt);//, WordSpliters, int.MaxValue, int.MaxValue, BuilderParameters.WORD_LENGTH_LIMIT);

                            if (src_fea.Count == 0)
                            {
                                src_fea.Add(new List<Dictionary<int, float>>());
                                src_fea[0].Add(new Dictionary<int, float>());
                                src_fea[0][0].Add(0, 0);
                            }

                            if (tgt_fea.Count == 0)
                            {
                                tgt_fea.Add(new Dictionary<int, float>());
                                tgt_fea[0].Add(0, 0);
                            }

                            srcBinary.PushSample(src_fea);
                            tgtBinary.PushSample(tgt_fea);

                            if (srcBinary.BatchSize >= BuilderParameters.MiniBatchSize)
                            {
                                srcBinary.PopBatchToStat(srcDataWriter);
                                tgtBinary.PopBatchToStat(tgtDataWriter);
                            }
                            if (++rowIdx % 100000 == 0)
                                Console.WriteLine("Extract Row {0} into binary", (rowIdx));
                        }

                        srcBinary.PopBatchCompleteStat(srcDataWriter);
                        Console.WriteLine("Source Data Stat {0}", srcBinary.Stat.ToString());

                        tgtBinary.PopBatchCompleteStat(tgtDataWriter);
                        Console.WriteLine("Target Data Stat {0}", tgtBinary.Stat.ToString());
                    }
                }

                TrainSrcBin = new DataCashier<SeqSentenceBatchInputData, SeqSentenceInputDataStat>(BuilderParameters.TrainSrcBin, BuilderParameters.TrainSrcBin + ".cursor");
                TrainTgtBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainTgtBin, BuilderParameters.TrainTgtBin + ".cursor");
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainSrcBin.InitThreadSafePipelineCashier(128, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(128, ParameterSetting.RANDOM_SEED);
                }
                else
                {
                    TrainSrcBin.InitThreadSafePipelineCashier(128, false);
                    TrainTgtBin.InitThreadSafePipelineCashier(128, false);
                }
            }
        }
    }
}
