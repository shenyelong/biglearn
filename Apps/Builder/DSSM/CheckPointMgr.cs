﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class CheckPointMgr
    {
        public static void MakeCheckPoint(DSSMStructure dssmModel, OptimizerFlow optimizer, ModelMetaInfo meta, string outputPath, int iter, DeviceType device, BuilderType type)
        {
            CheckPointInfo chk = new CheckPointInfo();
            chk.CurrentTrainIter = iter;
            chk.ModelInfo = meta;
            chk.ModelDevice = device;
            chk.ModelType = type;
            chk.Model = dssmModel;
            chk.Optimizer = optimizer;

            Console.WriteLine("[CHECK-POINT] Make check point at iter: {0}", chk.CurrentTrainIter);
            Console.WriteLine("================================");
            Console.WriteLine("{0}", chk.ModelInfo.ToString());
            Console.WriteLine("================================");
            int extensionIdx = outputPath.LastIndexOf(".");
            if (outputPath.StartsWith("cosmos://", StringComparison.OrdinalIgnoreCase) ||
                outputPath.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ||
                outputPath.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                string localName = string.Format("local_checkpoint.{0}.bin", iter);
                using (Stream ss = FileUtil.CreateWriteFS(localName)) // StreamFactory.GetWriteStream(localName))
                {
                    IOUnitWriter iow = new IOUnitWriter(ss);
                    iow.Write(chk);
                    iow.Flush();
                    ss.Close();
                }

                Console.WriteLine("Finished to save checkpoint to local in {0} s", sw.ElapsedMilliseconds/1000.0f);
                string tempRemoteName = outputPath.Substring(0, extensionIdx) + ".tmp" + outputPath.Substring(extensionIdx);
                sw.Restart();
                Console.WriteLine("Start to upload local checkpoint to remote {0}", outputPath);
                using (Stream local = FileUtil.CreateReadFS(localName)) // StreamFactory.GetReadStream(localName, false))
                using (Stream remote = FileUtil.CreateWriteFS(tempRemoteName)) // StreamFactory.GetWriteStream(tempRemoteName))
                {
                    local.CopyTo(remote, 32 * 1024 * 1024);
                    remote.Close();
                }

                Console.WriteLine("Finished to upload checkpoint to remote in {0} s", sw.ElapsedMilliseconds / 1000.0f);
                sw.Restart();

                if (File.Exists(outputPath))
                {
                    string oldName = string.Format("{0}.{1}{2}", outputPath.Substring(0, extensionIdx), iter - 1, outputPath.Substring(extensionIdx));
                    File.Move(outputPath, oldName);
                    //StreamFactory.Move(outputPath, oldName);
                }

                File.Move(tempRemoteName, outputPath);
                Console.WriteLine("Finished to rename checkpoint in {0} s", sw.ElapsedMilliseconds / 1000.0f);
                try
                {
                    File.Delete(localName);
                }
                catch
                {
                }
            }
            else
            {
                string tempName = outputPath.Substring(0, extensionIdx) + ".tmp" + outputPath.Substring(extensionIdx);
                using (Stream ss = FileUtil.CreateWriteFS(tempName)) // StreamFactory.GetWriteStream(tempName))
                {
                    IOUnitWriter iow = new IOUnitWriter(ss);
                    iow.Write(chk);
                    iow.Flush();
                    ss.Flush();
                    ss.Close();
                }
                File.Move(tempName, outputPath);
                //StreamFactory.Move(tempName, outputPath);
            }
        }

        public static CheckPointInfo LoadCheckPoint(string path, ref int iter)
        {
            Stream ms = null;
            ModelMetaInfo meta = null;

            Console.WriteLine("Start to load model from check point {0}", path);

            if (path.StartsWith("cosmos://", StringComparison.OrdinalIgnoreCase) || path.StartsWith("http://", StringComparison.OrdinalIgnoreCase) || path.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Copy remote stream to local before read it.");
                using (Stream remote = FileUtil.CreateReadFS(path)) // StreamFactory.GetReadStream(path, false))
                using (FileStream fs = new FileStream("Checkpoint.dat", FileMode.Create, FileAccess.Write))
                {
                    remote.CopyTo(fs, 32 * 1024 * 1024);
                }

                ms = new FileStream("Checkpoint.dat", FileMode.Open, FileAccess.Read);
            }
            else
            {
                ms = FileUtil.CreateReadFS(path); // StreamFactory.GetReadStream(path, false);
            }

            IOUnitReader ior = new IOUnitReader(ms);
            CheckPointInfo chk = (CheckPointInfo)ior.Read();
            iter = chk.CurrentTrainIter + 1;
            meta = chk.ModelInfo;

            ((DSSMStructure)chk.Model).Meta = meta;

            Console.WriteLine("[CHECK-POINT] Load check point from iter: {0}", chk.CurrentTrainIter);
            Console.WriteLine("================================");
            Console.WriteLine("{0}", chk.ModelInfo.ToString());
            Console.WriteLine("================================");

            Console.WriteLine("Finished to load model from check point...");
            return chk;
        }

        public static void SaveSeedModel(DSSMStructure dssmModel, ModelMetaInfo meta, string modelPath, string metaPath)
        {
            Console.WriteLine("Save model to {0}, Meta: {1}", modelPath, metaPath?? string.Empty);
            Stream ss = FileUtil.CreateWriteFS(modelPath); // StreamFactory.GetWriteStream(modelPath);
            using (BinaryWriter bw = new BinaryWriter(ss))
            {
                dssmModel.Serialize(bw);
            }

            if (!string.IsNullOrEmpty(metaPath))
            {
                using (StreamWriter tr = FileUtil.CreateStreamWrite(metaPath))
                {
                    foreach (var kv in meta.ToKeyValues())
                    {
                        tr.WriteLine("{0}\t{1}", kv.Key, kv.Value);
                    }
                }
            }
        }

        public static DSSMStructure LoadSeedModel(string modelPath, string metaPath, DeviceType device)
        {
            DSSMStructure dssmModel;
            Stream ms = FileUtil.CreateReadFS(modelPath); // StreamFactory.GetReadStream(modelPath);
            dssmModel = new DSSMStructure(ms, device);
            if (!string.IsNullOrEmpty(metaPath))
            {
                string modelMeta = File.ReadAllText(metaPath);
                TextMetaReader reader = new TextMetaReader(new StringReader(modelMeta));
                dssmModel.Meta = (ModelMetaInfo)reader.Read(); ;
            }

            return dssmModel;
        }

    }
}
