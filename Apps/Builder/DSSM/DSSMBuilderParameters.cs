﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class DSSMBuilderParameters : BaseModelArgument<DSSMBuilderParameters>
    {
        public static string QueryTrainData { get { return Argument["QUERY"].Value; } }
        public static string DocTrainData { get { return Argument["DOC"].Value; } }
        public static string ScoreTrainData { get { return Argument["SCORE"].Value; } }

        public static string QueryValidData { get { return Argument["QUERY-VALID"].Value; } }
        public static string DocValidData { get { return Argument["DOC-VALID"].Value; } }
        public static string ScoreValidData { get { return Argument["SCORE-VALID"].Value; } }
        public static string MappingValidData { get { return Argument["MAP-VALID"].Value; } }
        public static bool IsValidFile { get { return !(QueryValidData.Equals(string.Empty) || DocValidData.Equals(string.Empty)); } }

        public static int[] SRC_LAYER_DIM { get { return Argument["SRC-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static A_Func[] SRC_ACTIVATION { get { return Argument["SRC-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
        public static bool[] SRC_LAYER_BIAS { get { return Argument["SRC-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
        public static N_Type[] SRC_ARCH { get { return Argument["SRC-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
        public static int[] SRC_WINDOW { get { return Argument["SRC-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static string SEED_MODEL_PREFIX { get { return Argument["SEED-MODEL-PREFIX"].Value; } }
        public static bool IS_SRC_SEED_UPDATE { get { return int.Parse(Argument["SRC-SEED-UPDATE"].Value) > 0; } }
        public static float[] SRC_DROPOUT { get { if (Argument["SRC-DROPOUT"].Value.Equals(string.Empty)) return SRC_LAYER_DIM.Select(i => 0.0f).ToArray(); else  return Argument["SRC-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

        public static int[] TGT_LAYER_DIM { get { return Argument["TGT-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static A_Func[] TGT_ACTIVATION { get { return Argument["TGT-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
        public static bool[] TGT_LAYER_BIAS { get { return Argument["TGT-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
        public static N_Type[] TGT_ARCH { get { return Argument["TGT-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
        public static int[] TGT_WINDOW { get { return Argument["TGT-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static bool IS_TGT_SEED_UPDATE { get { return int.Parse(Argument["TGT-SEED-UPDATE"].Value) > 0; } }
        public static float[] TGT_DROPOUT { get { if (Argument["TGT-DROPOUT"].Value.Equals(string.Empty)) return TGT_LAYER_DIM.Select(i => 0.0f).ToArray(); else  return Argument["TGT-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

        public static DNNModelVersion MODEL_VERSION { get { return (DNNModelVersion)int.Parse(Argument["MODEL-VERSION"].Value); } }

        public static bool IsShareModel { get { return int.Parse(Argument["SHARE-MODEL"].Value) > 0; } }
        public static SimilarityType SimiType { get { return (SimilarityType)(int.Parse(Argument["SIM-TYPE"].Value)); } }

        public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
        public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }
        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }
        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
        public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
        public static int BatchIteration { get { return int.Parse(Argument["BATCH-ITERATION"].Value); } }
        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

        public static string ScoreOutputPath
        {
            get
            {
                if (RunMode == DNNRunMode.Predict) return Argument["SCORE-PATH"].Value;
                else return LogFile + ".tmp.score";
            }
        }
        public static string MetricOutputPath
        {
            get
            {
                if (RunMode == DNNRunMode.Predict) return Argument["METRIC-PATH"].Value;
                else return LogFile + ".tmp.metric";
            }
        }
        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
        public static DeviceType Device
        {
            get
            {
                if (GPUID >= 0)
                {
                    return DeviceType.GPU;
                }
                else if (GPUID == -1)
                {
                    return DeviceType.CPU;
                }
                else
                {
                    return DeviceType.CPU_FAST_VECTOR;
                }
            }
        }

        public static bool IsSrcEmbedding { get { return int.Parse(Argument["IS-SRC-EMBED"].Value) > 0; } }
        public static bool IsTgtEmbedding { get { return int.Parse(Argument["IS-TGT-EMBED"].Value) > 0; } }

        public static string TrainData { get { return Argument["TRAIN-DATA"].Value; } }

        public static string TrainStat { get { return Argument["TRAIN-STAT"].Value; } }

        public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

        public static string SeedModelMeta { get { return Argument["SEED-MODEL-META"].Value; } }

        public static string OutModel { get { return Argument["OUT-MODEL"].Value; } }

        public static string OutModelMeta { get { return Argument["OUT-MODEL-META"].Value; } }

        static DSSMBuilderParameters()
        {
            Argument.Add("QUERY", new ParameterArgument(null, "QUERY Train Data."));
            Argument.Add("DOC", new ParameterArgument(null, "DOC Train Data."));
            Argument.Add("SCORE", new ParameterArgument(string.Empty, "SCORE Train Data."));

            Argument.Add("QUERY-VALID", new ParameterArgument(string.Empty, "QUERY Valid Data."));
            Argument.Add("DOC-VALID", new ParameterArgument(string.Empty, "DOC Valid Data."));
            Argument.Add("SCORE-VALID", new ParameterArgument(string.Empty, "Score Valid Data."));
            Argument.Add("MAP-VALID", new ParameterArgument(string.Empty, "MAP Valid Data."));

            Argument.Add("SRC-LAYER-DIM", new ParameterArgument("300,300", "Source Layer Dim"));
            Argument.Add("SRC-ACTIVATION", new ParameterArgument("1,1", ParameterUtil.EnumValues(typeof(A_Func))));
            Argument.Add("SRC-LAYER-BIAS", new ParameterArgument("0,0", "Source Layer Bias"));
            Argument.Add("SRC-ARCH", new ParameterArgument("1,0", ParameterUtil.EnumValues(typeof(N_Type))));
            Argument.Add("SRC-WINDOW", new ParameterArgument("3,1", "Source DNN Window Size"));
            Argument.Add("SEED-MODEL-PREFIX", new ParameterArgument(string.Empty, "Seed Model Prefix for an optional seed model, it will be concatenated with .Query and .Doc"));
            Argument.Add("SRC-SEED-UPDATE", new ParameterArgument("1", "Update Source Seed Model. 1: true; 0 : false;"));
            Argument.Add("SRC-DROPOUT", new ParameterArgument(string.Empty, "Source DNN Dropout"));

            Argument.Add("TGT-LAYER-DIM", new ParameterArgument("300,300", "Target Layer Dim"));
            Argument.Add("TGT-ACTIVATION", new ParameterArgument("1,1", ParameterUtil.EnumValues(typeof(A_Func))));
            Argument.Add("TGT-LAYER-BIAS", new ParameterArgument("0,0", "Target Layer Bias"));
            Argument.Add("TGT-ARCH", new ParameterArgument("1,0", ParameterUtil.EnumValues(typeof(N_Type))));
            Argument.Add("TGT-WINDOW", new ParameterArgument("3,1", "Target DNN Window Size"));
            Argument.Add("TGT-SEED-MODEL", new ParameterArgument(string.Empty, "Target Seed Model."));
            Argument.Add("TGT-SEED-UPDATE", new ParameterArgument("1", "Update Target Seed Model. 1: true; 0 : false;"));
            Argument.Add("TGT-DROPOUT", new ParameterArgument(string.Empty, "Target DNN Dropout"));

            Argument.Add("MODEL-VERSION", new ParameterArgument(((int)DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR).ToString(), ParameterUtil.EnumValues(typeof(DNNModelVersion))));

            Argument.Add("MODEL-PATH", new ParameterArgument(".", "Model Path"));
            Argument.Add("SHARE-MODEL", new ParameterArgument("0", "Share Source and Target Model; 0 : no share; 1 : share;"));
            Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));

            Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.SampleSoftmax).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
            Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
            Argument.Add("NTRAIL", new ParameterArgument("20", "Negative Sample Number."));

            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "DSSM Learn Rate."));
            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.AdaGrad).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
            Argument.Add("ADABOOST", new ParameterArgument("0.01", "Ada Gradient Adjustment."));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
            Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
            Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
            Argument.Add("BATCH-ITERATION", new ParameterArgument("20", "Train Batch Iteration."));
            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.NDCG).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));
            Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
            Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
            Argument.Add("IS-SRC-EMBED", new ParameterArgument("0", "Indicates whether src embedding is outputted"));
            Argument.Add("IS-TGT-EMBED", new ParameterArgument("0", "Indicates whether tgt embedding is outputted"));

            Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));


            Argument.Add("TRAIN-DATA", new ParameterArgument(null, "Trainning data in a single stream"));
            Argument.Add("TRAIN-STAT", new ParameterArgument(null, "The stat information of trainning data in a single stream"));
            Argument.Add("SEED-MODEL", new ParameterArgument(null, "Seed model data in a single stream"));
            Argument.Add("SEED-MODEL-META", new ParameterArgument(null, "Seed model meta information in a single stream"));

            Argument.Add("OUT-MODEL", new ParameterArgument(null, "Output model data in a single stream"));
            Argument.Add("OUT-MODEL-META", new ParameterArgument(null, "Output model meta infomation in a single text"));
        }
    }
}
