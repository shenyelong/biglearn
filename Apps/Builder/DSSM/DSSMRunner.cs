﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    // totally staled.
    //public class DSSMRunner<IN> : StructRunner<DSSMStructure, IN> where IN : DSSMData
    //{
    //    /// <summary>
    //    /// Semantic between (query, doc) pairs.
    //    /// </summary>
    //    public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
    //    public DNNRunner<SeqSparseBatchData> SrcRunner = null;
    //    public DNNRunner<SeqSparseBatchData> TgtRunner = null;

    //    public HiddenBatchData SrcOutput { get { return ((HiddenBatchData)SrcRunner.Output); } }
    //    public HiddenBatchData TgtOutput { get { return ((HiddenBatchData)TgtRunner.Output); } }

    //    public DSSMLossDerivParameter lossDerivParameter = new DSSMLossDerivParameter();


    //    public DSSMTrainParameters Parameter { get { return (DSSMTrainParameters)Behavior.BehaviorParameter; } }
    //    public DSSMRunner(DSSMStructure dssmStructure, RunnerBehavior behavior) //DSSMData dssmData, 
    //        : base(dssmStructure, behavior)
    //    {
    //        //SrcRunner = StructRunner.CreateRunner(Model.SrcDnn, Input.SrcInput, behavior);

    //        SrcRunner = new DNNRunner<SeqSparseBatchData>(Model.SrcDnn, behavior);   //Input.SrcInput.GetType(), 
    //        TgtRunner = new DNNRunner<SeqSparseBatchData>(Model.TgtDnn, behavior);  //StructRunner.CreateRunner(Model.TgtDnn, Input.TgtInput.GetType(), behavior);
    //        //Output = new HiddenBatchData();
    //        Console.WriteLine("Create DSSM Runner.");
    //    }

    //    DenseBatchData scoreData { get; set; }
    //    public override IN Input
    //    {
    //        get
    //        {
    //            return (IN)new DSSMData(SrcRunner.Input, TgtRunner.Input, scoreData); // base.Input;
    //        }
    //        set
    //        {
    //            SrcRunner.Input = value.SrcInput;
    //            TgtRunner.Input = value.TgtInput;
    //            scoreData = value.ScoreInput;
    //        }
    //    }

    //    public override void InitMemory()
    //    {
    //        if (Output.MAX_BATCHSIZE < Input.BatchSize)
    //        {
    //            switch (Behavior.RunMode)
    //            {
    //                case DNNRunMode.Predict:
    //                    //Output.Init(Input.Stat.SrcStat.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
    //                    break;
    //                case DNNRunMode.Train:
    //                    //Output.Init(Input.Stat.SrcStat.MAX_BATCHSIZE, Parameter.NegativeNum + 1, Behavior.RunMode, Behavior.Device);
    //                    if (Parameter.LossType == LossFunctionType.SampleSoftmax || Parameter.LossType == LossFunctionType.SampleLogisicalRegression)
    //                    {
    //                        lossDerivParameter.NegativeNum = Parameter.NegativeNum;
    //                        lossDerivParameter.NegativeIndex = new CudaPieceInt(Parameter.NegativeNum * Input.Stat.SrcStat.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //                        lossDerivParameter.InverseNegativeIdx = new CudaPieceInt(Parameter.NegativeNum * Input.Stat.SrcStat.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //                        lossDerivParameter.InverseNegativeValue = new CudaPieceInt(Parameter.NegativeNum * Input.Stat.SrcStat.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //                        lossDerivParameter.PosPairDeriv = new PairLayerDeriv(Input.Stat.SrcStat.MAX_BATCHSIZE * Model.OutputDim, Behavior.Device);
    //                        lossDerivParameter.NegPairDeriv = new PairLayerDeriv(Parameter.NegativeNum * Input.Stat.SrcStat.MAX_BATCHSIZE * Model.OutputDim, Behavior.Device);

    //                        lossDerivParameter.SrcOutput = SrcOutput;
    //                        lossDerivParameter.TgtOutput = TgtOutput;
    //                        lossDerivParameter.SimType = Model.SimiType;
    //                    }

    //                    break;
    //            }
    //        }
    //    }

    //    public override void Forward()
    //    {
    //        InitMemory();
    //        SrcRunner.Forward();
    //        TgtRunner.Forward();

    //        Output.BatchSize = Input.BatchSize;
    //        switch (Model.SimiType)
    //        {
    //            case SimilarityType.CosineSimilarity:
    //                MathOperatorManager.GlobalInstance.Cosine_Similarity(SrcOutput.Output.Data, TgtOutput.Output.Data, Output.Output.Data,
    //                    1, Input.Stat.SrcStat.MAX_BATCHSIZE, 0, Input.BatchSize, Model.OutputDim, Util.GPUEpsilon);
    //                break;
    //            case SimilarityType.InnerProduct:
    //                MathOperatorManager.GlobalInstance.InnerProduct_Similarity(SrcOutput.Output.Data, TgtOutput.Output.Data, Output.Output.Data,
    //                    Input.BatchSize, Model.OutputDim);
    //                break;
    //        }
    //    }

    //    public override void Backward(bool cleanDeriv)
    //    {
    //        TgtRunner.Backward(cleanDeriv);
    //        SrcRunner.Backward(cleanDeriv);
    //    }

    //    public override void CleanDeriv()
    //    {
    //        TgtRunner.CleanDeriv();
    //        SrcRunner.CleanDeriv();
    //    }

    //    public override void Update()
    //    {
    //        TgtRunner.Update();
    //        SrcRunner.Update();
    //    }

    //    public Tuple<float[], float[], float[]> Predict(IN data)
    //    {
    //        this.Input = data;
    //        this.Forward();
    //        Output.Output.Data.CopyOutFromCuda();
    //        SrcOutput.Output.Data.CopyOutFromCuda();
    //        TgtOutput.Output.Data.CopyOutFromCuda();
    //        Tuple<float[], float[], float[]> result =
    //            new Tuple<float[], float[], float[]>(Output.Output.Data.MemPtr, SrcOutput.Output.Data.MemPtr, TgtOutput.Output.Data.MemPtr);
    //        return result;
    //    }


    //    #region Dispose Function is not necessary.
    //    ~DSSMRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposed)
    //        {
    //            return;
    //        }

    //        disposed = true;

    //        if (disposing)
    //        {
    //            if (Output != null) Output.Dispose(); Output = null;
    //            if (lossDerivParameter.NegativeIndex != null) lossDerivParameter.NegativeIndex.Dispose(); Output = null;
    //            if (lossDerivParameter.InverseNegativeIdx != null) lossDerivParameter.InverseNegativeIdx.Dispose(); lossDerivParameter.InverseNegativeIdx = null;
    //            if (lossDerivParameter.InverseNegativeValue != null) lossDerivParameter.InverseNegativeValue.Dispose(); lossDerivParameter.InverseNegativeValue = null;
    //            if (lossDerivParameter.PosPairDeriv != null) lossDerivParameter.PosPairDeriv.Dispose(); lossDerivParameter.PosPairDeriv = null;
    //            if (lossDerivParameter.NegPairDeriv != null) lossDerivParameter.NegPairDeriv.Dispose(); lossDerivParameter.NegPairDeriv = null;
    //            if (SrcRunner != null) SrcRunner.Dispose(); SrcRunner = null;
    //            if (TgtRunner != null) TgtRunner.Dispose(); TgtRunner = null;
    //        }

    //        base.Dispose(disposing);
    //    }
    //    #endregion.
    //}
}
