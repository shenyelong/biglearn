﻿using BigLearn;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigLearn.DeepNet
{
    public class DSSMGraphBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.DSSM_GRAPH; } }

        public override void InitStartup(string fileName)
        {
            DSSMGraphParameters.Parse(fileName);
        }

        public static ComputationGraph BuildComputationGraph(DSSMDataCashier data, RunnerBehavior Behavior, DSSMStructure dssmModel)
        {
            ComputationGraph cg = new ComputationGraph();

            //cg.SetDataSource(data);
            DSSMData dssmData = (DSSMData)cg.AddDataRunner(new DataRunner<DSSMData, DSSMDataStat>(data, Behavior));

            /// embedding for source text.
            HiddenBatchData srcOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(dssmModel.SrcDnn, dssmData.SrcInput, Behavior));
            /// embedding for target text.
            HiddenBatchData tgtOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(dssmModel.TgtDnn, dssmData.TgtInput, Behavior));

            //BiMatchBatchData matchData = new BiMatchBatchData(data.Stat.SrcStat.MAX_BATCHSIZE,
            //                    Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.NTrail : 0, BuilderParameters.Device, DSMlib.BuilderParameters.RandomSeed != 13);


            IntArgument arg = new IntArgument("batchSize");
            cg.AddRunner(new HiddenDataBatchSizeRunner(srcOutput, arg, Behavior));

            BiMatchBatchData matchData = (BiMatchBatchData)cg.AddRunner(new RandomBiMatchRunner(data.Stat.SrcStat.MAX_BATCHSIZE,
                                (Behavior.RunMode == DNNRunMode.Train || DSSMGraphParameters.Evaluation == EvaluationType.PRECISIONK) ? DSSMGraphParameters.NTrail : 0, arg, Behavior, DeepNet.BuilderParameters.RandomSeed != 13));

            /// similarity between source text and target text.
            HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(srcOutput, tgtOutput, matchData, DSSMGraphParameters.SimiType, Behavior));

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    switch (DSSMGraphParameters.LossFunction)
                    {
                        case LossFunctionType.SampleSoftmax:
                            if (data.IsScore) cg.AddObjective(new MultiClassSoftmaxRunner(null, dssmData.ScoreInput, simiOutput.Output, simiOutput.Deriv, DSSMGraphParameters.Gamma, Behavior));
                            else cg.AddObjective(new MultiClassSoftmaxRunner(null, simiOutput.Output, simiOutput.Deriv, DSSMGraphParameters.Gamma, Behavior));
                            break;
                        case LossFunctionType.SampleLogisicalRegression:
                            if (data.IsScore) cg.AddObjective(new SampleCrossEntropyRunner(dssmData.ScoreInput, simiOutput.Output, simiOutput.Deriv, DSSMGraphParameters.Gamma, Behavior));
                            else cg.AddObjective(new CrossEntropyRunner(matchData.MatchInfo, simiOutput.Output, simiOutput.Deriv, DSSMGraphParameters.Gamma, Behavior));
                            break;
                        case LossFunctionType.Regression:
                            cg.AddObjective(new MSERunner(simiOutput.Output, dssmData.ScoreInput, null, simiOutput.Deriv, Behavior));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    if (!DSSMGraphParameters.SRC_EMBED_FILE.Equals(string.Empty))
                    {
                        cg.AddRunner(new VectorDumpRunner(srcOutput.Output, DSSMGraphParameters.SRC_EMBED_FILE));
                    }

                    // Dump Tgt Embedding Vector.
                    if (!DSSMGraphParameters.TGT_EMBED_FILE.Equals(string.Empty))
                    {
                        cg.AddRunner(new VectorDumpRunner(tgtOutput.Output, DSSMGraphParameters.TGT_EMBED_FILE));
                    }

                    switch (DSSMGraphParameters.Evaluation)
                    {
                        case EvaluationType.NDCG:
                            cg.AddRunner(new NDCGDiskDumpRunner(simiOutput.Output, DSSMGraphParameters.MappingValidData, DSSMGraphParameters.ScoreOutputPath, DSSMGraphParameters.MetricOutputPath));
                            break;
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(dssmData.ScoreInput.Data, simiOutput.Output, DSSMGraphParameters.ScoreOutputPath, DSSMGraphParameters.MetricOutputPath));
                            break;
                        case EvaluationType.MSE:
                            cg.AddRunner(new RMSEDiskDumpRunner(dssmData.ScoreInput.Data, simiOutput.Output, DSSMGraphParameters.Gamma, true, DSSMGraphParameters.ScoreOutputPath));
                            break;
                        case EvaluationType.PRECISIONK:
                            cg.AddRunner(new PrecisionKRunner(simiOutput.Output, matchData, DSSMGraphParameters.MappingValidData, DSSMGraphParameters.ScoreOutputPath));
                            break;
                        case EvaluationType.PLAINOUTPUT:
                            cg.AddRunner(new VectorDumpRunner(simiOutput.Output, DSSMGraphParameters.ScoreOutputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            IMathOperationManager Computelib = MathOperatorManager.CreateInstance(BuilderParameters.Device);

            Logger.OpenLog(DSSMGraphParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            if (DataPanel.Valid != null && DataPanel.Valid.Stat != null)
                Logger.WriteLog("Test Data Stat : {0}", DataPanel.Valid.Stat.ToString());

            Logger.WriteLog("Load Data Finished.");
            DSSMStructure dssmModel = null;
            switch (DSSMGraphParameters.RunMode)
            {
                case DNNRunMode.Train:
                    dssmModel = Train(BuilderParameters.Device, Computelib);
                    break;
                case DNNRunMode.Predict:
                    if (DSSMGraphParameters.SeedModel != string.Empty)
                    {
                        Logger.WriteLog("Loading DSSM Structure From {0} at : {1}", DSSMGraphParameters.SeedModel, DateTime.Now);
                        dssmModel = new DSSMStructure(FileUtil.CreateReadFS(DSSMGraphParameters.SeedModel), DSSMGraphParameters.Device);
                        Logger.WriteLog("finish loading model at :{0}", DateTime.Now);
                    }

                    DateTime pretime = DateTime.Now;
                    Logger.WriteLog("Total Sample Number {0}", DataPanel.Valid.Stat.SrcStat.TotalSampleNumber);
                    ComputationGraph cg = BuildComputationGraph(DataPanel.Valid, new RunnerBehavior()
                    { Computelib = Computelib, Device = BuilderParameters.Device, RunMode = DNNRunMode.Predict }, dssmModel);
                    cg.Execute();
                    TimeSpan ts = DateTime.Now.Subtract(pretime);
                    Logger.WriteLog("Total Prediction cost:{0} with {1} samples,  avg:{2} ms", ts, DataPanel.Valid.Stat.SrcStat.TotalSampleNumber,
                        ts.Milliseconds * 1.0 / DataPanel.Valid.Stat.SrcStat.TotalSampleNumber);
                    break;
            }

            Logger.CloseLog();
        }

        private DSSMStructure Train(DeviceType device, IMathOperationManager Computelib)
        {
            // Take checkpoint first
            CheckPointInfo chk = SetupModel();
            DSSMStructure dssmModel = chk.Model as DSSMStructure;

            ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, new RunnerBehavior()
            { Computelib = Computelib, Device = device, RunMode = DNNRunMode.Train }, dssmModel);

            ComputationGraph predCG = null;
            if (DSSMGraphParameters.IsValidFile) predCG = BuildComputationGraph(DataPanel.Valid, new RunnerBehavior()
            { Computelib = Computelib, Device = device, RunMode = DNNRunMode.Predict }, dssmModel);

            if (!Directory.Exists(DSSMGraphParameters.ModelOutputPath) && !string.IsNullOrEmpty(DSSMGraphParameters.ModelOutputPath))
            {
                Directory.CreateDirectory(DSSMGraphParameters.ModelOutputPath);
            }

            StructureLearner learner = new StructureLearner()
            {
                LearnRate = DSSMGraphParameters.LearnRate,
                Optimizer = DSSMGraphParameters.Optimizer,
                BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                EpochNum = DSSMGraphParameters.Iteration,
                ShrinkLR = DSSMGraphParameters.ShrinkLR,
                device = DSSMGraphParameters.Device,
                GradientAggFactory = null,
                OptArgs = OptimizerParameters.OptimizerArgs
            };

            learner.GradientAggFactory = OptimizerParameters.GradientAgg;

            OptimizerFlow optimizer = OptimizerFlow.CreateOptimizerFlow(dssmModel, trainCG,
                DSSMGraphParameters.Optimizer,
                learner,
                new RunnerBehavior() { Device = DSSMGraphParameters.Device });

            chk.LoadOptimizer(optimizer);
            chk.Dispose();

            /// Select Best Model During Training.
            double validScore = 0;
            double bestValidScore = double.MinValue;
            double bestValidIter = -1;
            double learnRate = DSSMGraphParameters.LearnRate;
            ModelMetaInfo meta = (ModelMetaInfo)dssmModel.Meta;
            Logger.WriteLog("Train using optimizer: {0} with args: {1}", DSSMGraphParameters.Optimizer, OptimizerParameters.OptimizerArgs);
            Logger.WriteLog("Train Learn Rate {0}", learnRate);
            //validScore = predCG.Execute();
            for (; this.trainIter < DSSMGraphParameters.Iteration; this.trainIter++)
            {
                DateTime pre = DateTime.Now;
                double loss = optimizer.Run();
                double trainLoss = optimizer.EvaluateTranningLoss();
                meta.LossInfo[this.trainIter] = trainLoss;
                //Logger.WriteLog("Iteration {0}, Total MiniBatch {1}, TrainingLoss: {2}, Avg Loss {3}, and CosSim = {4}/{5}={6}, Time Cost: {7}s",
                //    this.trainIter, DataPanel.Train.Stat.TotalBatchNumber, trainLoss, loss,
                //    DSSMLossDerivParameter.COSINE_SIM_SCORE, DataPanel.Train.Stat.TotalSampleNumber,
                //    DSSMLossDerivParameter.COSINE_SIM_SCORE / DataPanel.Train.Stat.TotalSampleNumber, DateTime.Now.Subtract(pre).TotalSeconds);
                //DSSMLossDerivParameter.COSINE_SIM_SCORE = 0.0;
                if ((DeepNet.BuilderParameters.ModelSavePerIteration && BuilderParameters.DistMode == PlatformType.SingleBox) || this.trainIter == DSSMGraphParameters.Iteration - 1)
                {
                    pre = DateTime.Now;
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\DSSM.{1}.model", DSSMGraphParameters.ModelOutputPath, this.trainIter.ToString()), FileMode.Create, FileAccess.Write, FileShare.None, FileUtil.BUFFER_SIZE)))
                    {
                        dssmModel.Serialize(writer);
                    }

                    Console.WriteLine("Model {0} Serialize take {1} ", this.trainIter, DateTime.Now.Subtract(pre).TotalSeconds);
                }

                // Only do check point in the master node.
                if (this.ParId == 0 && !string.IsNullOrEmpty(BuilderParameters.CheckPoint))
                {
                    meta.UpdateParameters(DSSMGraphParameters.ToKeyValues());
                    CheckPointMgr.MakeCheckPoint(dssmModel, optimizer, meta, BuilderParameters.CheckPoint, this.trainIter, DSSMGraphParameters.Device, BuilderType.DSSM_GRAPH);
                }

                if (DSSMGraphParameters.IsValidFile)
                {
                    validScore = predCG.Execute();
                    if (validScore >= bestValidScore)
                    {
                        bestValidScore = validScore;
                        bestValidIter = this.trainIter;
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\DSSM.{1}.model", DSSMGraphParameters.ModelOutputPath, "done"), FileMode.Create, FileAccess.Write)))
                        {
                            dssmModel.Serialize(writer);
                        }
                    }
                    else
                    {
                        //Learn Rate Adjustment.
                        //learnRate = learnRate * 0.5;
                        //optimizer.DecreaseLRonValidationDontImprove(0.5f);
                    }
                    Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                }
            }

            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);

            // To Do: Support Batch Training in Computation Graph.
            //if (DSSMGraphParameters.BatchIteration > 0)
            //{
            //    dssmModel.InitGlobalOptimizer(new StructureLearner()
            //    {
            //        LearnRate = DSSMGraphParameters.LearnRate,
            //        Optimizer = DNNOptimizerType.BatchLBFGS,
            //        AdaBoost = DSSMGraphParameters.AdaBoost,
            //        BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
            //        EpochNum = DSSMGraphParameters.BatchIteration,
            //        ShrinkLR = DSSMGraphParameters.ShrinkLR
            //    });
            //}

            if (!string.IsNullOrEmpty(DSSMGraphParameters.OutModel))
            {
                meta.DataMeta = DataPanel.Train.Stat;
                CheckPointMgr.SaveSeedModel(dssmModel, meta, DSSMGraphParameters.OutModel, DSSMGraphParameters.OutModelMeta);
            }

            return dssmModel;
        }

        private CheckPointInfo SetupModel()
        {
            this.trainIter = 0;
            this.ParId = BuilderParameters.WorkerId;
            DSSMStructure dssmModel;
            CheckPointInfo checkPoint = null;
            if (!string.IsNullOrEmpty(BuilderParameters.CheckPoint) && File.Exists(BuilderParameters.CheckPoint)) // StreamFactory.StreamExists(BuilderParameters.CheckPoint))
            {
                Logger.WriteLog("Loading DSSM check point from {0} at : {1}", BuilderParameters.CheckPoint, DateTime.Now);
                checkPoint = CheckPointMgr.LoadCheckPoint(BuilderParameters.CheckPoint, ref this.trainIter);
                dssmModel = checkPoint.Model as DSSMStructure;
                Logger.WriteLog("finish loading model at :{0}", DateTime.Now);
            }
            else if (!string.IsNullOrEmpty(DSSMGraphParameters.SeedModel))
            {
                dssmModel = CheckPointMgr.LoadSeedModel(DSSMGraphParameters.SeedModel, DSSMGraphParameters.SeedModelMeta, DSSMGraphParameters.Device);
            }
            else
            {
                Logger.WriteLog("Init a New DSSM Structure.");
                dssmModel = new DSSMStructure(DSSMGraphParameters.Device,
                        DSSMGraphParameters.SimiType, DSSMGraphParameters.IsShareModel,
                        DataPanel.Train.Stat.SrcStat.FEATURE_DIM, DSSMGraphParameters.SRC_LAYER_DIM, DSSMGraphParameters.SRC_ACTIVATION,
                        DSSMGraphParameters.SRC_LAYER_BIAS, DSSMGraphParameters.SRC_ARCH, DSSMGraphParameters.SRC_WINDOW, DSSMGraphParameters.SRC_DROPOUT,
                        DataPanel.Train.Stat.TgtStat.FEATURE_DIM, DSSMGraphParameters.TGT_LAYER_DIM, DSSMGraphParameters.TGT_ACTIVATION,
                        DSSMGraphParameters.TGT_LAYER_BIAS, DSSMGraphParameters.TGT_ARCH, DSSMGraphParameters.TGT_WINDOW, DSSMGraphParameters.TGT_DROPOUT);

                if(!DSSMGraphParameters.InitSrcEmbedding.Equals(string.Empty))
                {
                    int wordSrcDim = DSSMGraphParameters.SRC_LAYER_DIM[0];
                    int halfWinSrcStart = DSSMGraphParameters.SRC_WINDOW[0] / 2; // * DataPanel.WordDim;

                    Logger.WriteLog("Init Src Word Embedding ...");
                    int hit = 0;
                    using (StreamReader mreader = new StreamReader(DSSMGraphParameters.InitSrcEmbedding))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string[] items = mreader.ReadLine().Split(' ');
                            int wordIdx = int.Parse(items[0]);
                            float[] vec = new float[items.Length - 1];
                            for (int i = 1; i < items.Length; i++)
                            {
                                vec[i - 1] = float.Parse(items[i]);
                            }

                            for (int win = -halfWinSrcStart; win < DSSMGraphParameters.SRC_WINDOW[0] - halfWinSrcStart; win++)
                            {
                                for (int i = 0; i < wordSrcDim; i++)
                                {
                                    dssmModel.SrcDnn.NeuralLinks[0].weight.MemPtr[
                                        ((win + halfWinSrcStart) * DataPanel.Train.Stat.SrcStat.FEATURE_DIM + wordIdx) * wordSrcDim + i] = vec[i];
                                }
                            }
                            hit++;

                        }
                    }
                    dssmModel.SrcDnn.NeuralLinks[0].weight.SyncFromCPU();
                    Logger.WriteLog("Init Src Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.Train.Stat.SrcStat.FEATURE_DIM);
                }

                if (!DSSMGraphParameters.InitTgtEmbedding.Equals(string.Empty))
                {
                    int wordTgtDim = DSSMGraphParameters.TGT_LAYER_DIM[0];
                    int halfWinTgtStart = DSSMGraphParameters.TGT_WINDOW[0] / 2; // * DataPanel.WordDim;

                    Logger.WriteLog("Init Tgt Word Embedding ...");
                    int hit = 0;
                    using (StreamReader mreader = new StreamReader(DSSMGraphParameters.InitTgtEmbedding))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string[] items = mreader.ReadLine().Split(' ');
                            int wordIdx = int.Parse(items[0]);
                            float[] vec = new float[items.Length - 1];
                            for (int i = 1; i < items.Length; i++)
                            {
                                vec[i - 1] = float.Parse(items[i]);
                            }

                            for (int win = -halfWinTgtStart; win < DSSMGraphParameters.TGT_WINDOW[0] - halfWinTgtStart; win++)
                            {
                                for (int i = 0; i < wordTgtDim; i++)
                                {
                                    dssmModel.TgtDnn.NeuralLinks[0].weight.MemPtr[
                                        ((win + halfWinTgtStart) * DataPanel.Train.Stat.TgtStat.FEATURE_DIM + wordIdx) * wordTgtDim + i] = vec[i];
                                }
                            }
                            hit++;
                        }
                    }
                    dssmModel.TgtDnn.NeuralLinks[0].weight.SyncFromCPU();
                    Logger.WriteLog("Init Tgt Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.Train.Stat.TgtStat.FEATURE_DIM);
                }
            }

            Logger.WriteLog(dssmModel.SrcDnn.DNN_Descr());
            Logger.WriteLog(dssmModel.TgtDnn.DNN_Descr());
            Logger.WriteLog("Load DSSM Structure Finished.");



            if (dssmModel.Meta == null)
            {
                ModelMetaInfo meta = new ModelMetaInfo();
                meta.DataMeta = DataPanel.Train.Stat;
                dssmModel.Meta = meta;
            }

            ((ModelMetaInfo)(dssmModel.Meta)).UpdateParameters(DSSMGraphParameters.ToKeyValues());

            if (checkPoint == null)
            {
                checkPoint = new CheckPointInfo()
                {
                    Model = dssmModel,
                    CurrentTrainIter = this.trainIter
                };
            }

            return checkPoint;
        }

        public class DataPanel
        {
            public static DSSMDataCashier Train = null;
            public static DSSMDataCashier Valid = null;

            public static void Init()
            {
                if (DSSMGraphParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DSSMDataCashier(DSSMGraphParameters.QueryTrainData, DSSMGraphParameters.DocTrainData, DSSMGraphParameters.ScoreTrainData);
                    Train.InitThreadSafePipelineCashier(100, true);
                }
                if (DSSMGraphParameters.IsValidFile)
                {
                    Valid = new DSSMDataCashier(DSSMGraphParameters.QueryValidData, DSSMGraphParameters.DocValidData, DSSMGraphParameters.ScoreValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }
        }
    }
}
