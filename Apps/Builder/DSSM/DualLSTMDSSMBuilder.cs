﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class DualLSTMDSSMBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static string QueryTrainData { get { return Argument["QUERY"].Value; } }
            public static string DocTrainData { get { return Argument["DOC"].Value; } }
            public static string ScoreTrainData { get { return Argument["SCORE"].Value; } }
            public static bool IsTrainFile { get { return !(QueryTrainData.Equals(string.Empty) || DocTrainData.Equals(string.Empty)); } }

            public static string QueryValidData { get { return Argument["QUERY-VALID"].Value; } }
            public static string DocValidData { get { return Argument["DOC-VALID"].Value; } }
            public static string ScoreValidData { get { return Argument["SCORE-VALID"].Value; } }
            public static string MappingValidData { get { return Argument["MAP-VALID"].Value; } }

            public static bool IsValidFile { get { return !(QueryValidData.Equals(string.Empty) || DocValidData.Equals(string.Empty)); } }

            public static int[] LSTM_LAYER_DIM { get { return Argument["LSTM-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static bool IS_BILSTM { get { return int.Parse(Argument["BI-DIRECTION"].Value) > 0; } }
            public static PoolingType PoolingType { get { return (PoolingType)int.Parse(Argument["POOLING"].Value); } }

            public static SimilarityType SimiType { get { return (SimilarityType)(int.Parse(Argument["SIM-TYPE"].Value)); } }

            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }

            
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static string SRC_EMBED_FILE { get { return Argument["SRC-EMBED-FILE"].Value; } }
            public static string TGT_EMBED_FILE { get { return Argument["TGT-EMBED-FILE"].Value; } }

            public static string ScoreOutputPath
            {
                get
                {
                    return Argument["SCORE-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.score" : Argument["SCORE-PATH"].Value;
                }
            }
            public static string MetricOutputPath
            {
                get
                {
                    return Argument["METRIC-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.metric" : Argument["METRIC-PATH"].Value;
                }
            }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            
            public static int SrcEmbedDim { get { return int.Parse(Argument["SRC-EMBED-DIM"].Value); } }
            public static int TgtEmbedDim { get { return int.Parse(Argument["TGT-EMBED-DIM"].Value); } }
            public static string InitSrcEmbedding { get { return Argument["INIT-SRC-EMBEDDING"].Value; } }
            public static string InitTgtEmbedding { get { return Argument["INIT-TGT-EMBEDDING"].Value; } }

            static BuilderParameters()
            {
                Argument.Add("QUERY", new ParameterArgument(string.Empty, "QUERY Train Data."));
                Argument.Add("DOC", new ParameterArgument(string.Empty, "DOC Train Data."));
                Argument.Add("SCORE", new ParameterArgument(string.Empty, "SCORE Train Data."));

                Argument.Add("QUERY-VALID", new ParameterArgument(string.Empty, "QUERY Valid Data."));
                Argument.Add("DOC-VALID", new ParameterArgument(string.Empty, "DOC Valid Data."));
                Argument.Add("SCORE-VALID", new ParameterArgument(string.Empty, "Score Valid Data."));
                Argument.Add("MAP-VALID", new ParameterArgument(string.Empty, "MAP Valid Data."));

                Argument.Add("LSTM-LAYER-DIM", new ParameterArgument("300,300", "Source Layer Dim"));
                Argument.Add("BI-DIRECTION", new ParameterArgument("1", "0 : One Directional LSTM; 1 : Bi Directional LSTM"));
                Argument.Add("POOLING", new ParameterArgument(@"1", "Pooling Type;  0 : Max Pooling; 1 : Last State Pooling."));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.SampleSoftmax).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
                Argument.Add("NTRAIL", new ParameterArgument("20", "Negative Sample Number."));

                Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.NDCG).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));
                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));
                Argument.Add("SRC-EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether src embedding is outputted"));
                Argument.Add("TGT-EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether tgt embedding is outputted"));

                Argument.Add("SRC-EMBED-DIM", new ParameterArgument("0", "Source String Embedding Dim!"));
                Argument.Add("TGT-EMBED-DIM", new ParameterArgument("0", "Target String Embedding Dim!"));

                Argument.Add("INIT-SRC-EMBEDDING", new ParameterArgument(string.Empty, "Init Source String Embedding!"));
                Argument.Add("INIT-TGT-EMBEDDING", new ParameterArgument(string.Empty, "Init Target String Embedding!"));

            }
        }

        public override BuilderType Type { get { return BuilderType.DUAL_LSTM_DSSM; } }

        /// <summary>
        /// example config 1. : \\gcr\Teams\B99\BigLearn\PublicDataSource\LSTM-DSSM-V3\LSTM-Pair.config
        /// </summary>
        /// <param name="fileName"></param>
        public override void InitStartup(string fileName)
        {
            base.InitStartup(fileName);
            BuilderParameters.Parse(fileName);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> tgtData,
                                                             IDataCashier<DenseBatchData, DenseDataStat> labelData,
                                                             RunnerBehavior Behavior, LayerStructure srcEmbedding, LayerStructure tgtEmbedding,
                                                             LSTMStructure srcLSTM, LSTMStructure srcRevLSTM, LSTMStructure tgtLSTM, LSTMStructure tgtRevLSTM)
        {


            ComputationGraph cg = new ComputationGraph();

            /**************** Get Data from DataCashier *********/
            SeqSparseBatchData src = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(srcData, Behavior));
            SeqSparseBatchData tgt = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(tgtData, Behavior));

            DenseBatchData label = null;
            if (labelData != null) label = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(labelData, Behavior));

            /*************** LSTM for Source Input. ********************/
            SeqDenseRecursiveData lstmSrcOutput = null;
            SeqDenseBatchData srcEmbedOutput = null;

            if (srcEmbedding != null) srcEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(srcEmbedding, src, Behavior));
            if (srcEmbedding == null)
            {
                lstmSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(srcLSTM.LSTMCells[0], src, true, Behavior));
                for (int i = 1; i < srcLSTM.LSTMCells.Count; i++)
                    lstmSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(srcLSTM.LSTMCells[i], lstmSrcOutput, Behavior));
            }
            else
            {
                lstmSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(srcEmbedOutput, true, Behavior));
                for (int i = 0; i < srcLSTM.LSTMCells.Count; i++)
                    lstmSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(srcLSTM.LSTMCells[i], lstmSrcOutput, Behavior));
            }

            /*************** LSTM for Target Input. ********************/
            SeqDenseRecursiveData lstmTgtOutput = null;
            SeqDenseBatchData tgtEmbedOutput = null;

            if (tgtEmbedding != null) tgtEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(tgtEmbedding, tgt, Behavior));
            if (tgtEmbedding == null)
            {
                lstmTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(tgtLSTM.LSTMCells[0], tgt, true, Behavior));
                for (int i = 1; i < tgtLSTM.LSTMCells.Count; i++)
                    lstmTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(tgtLSTM.LSTMCells[i], lstmTgtOutput, Behavior));
            }
            else
            {
                lstmTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(tgtEmbedOutput, true, Behavior));
                for (int i = 0; i < tgtLSTM.LSTMCells.Count; i++)
                    lstmTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(tgtLSTM.LSTMCells[i], lstmTgtOutput, Behavior));
            }

            HiddenBatchData srcOutput = null;
            HiddenBatchData tgtOutput = null;

            if (BuilderParameters.IS_BILSTM)
            {
                /***************Bi LSTM for Source Input. ********************/
                SeqDenseRecursiveData lstmBiSrcOutput = null;
                if (srcEmbedding == null)
                {
                    lstmBiSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(srcRevLSTM.LSTMCells[0], src, false, Behavior));
                    for (int i = 1; i < srcRevLSTM.LSTMCells.Count; i++)
                        lstmBiSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(srcRevLSTM.LSTMCells[i], lstmBiSrcOutput, Behavior));
                }
                else
                {
                    lstmBiSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(srcEmbedOutput, false, Behavior));
                    for (int i = 0; i < srcRevLSTM.LSTMCells.Count; i++)
                        lstmBiSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(srcRevLSTM.LSTMCells[i], lstmBiSrcOutput, Behavior));
                }

                /***************Bi LSTM for Target Input. ********************/
                SeqDenseRecursiveData lstmBiTgtOutput = null;
                if (tgtEmbedding == null)
                {
                    lstmBiTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(tgtRevLSTM.LSTMCells[0], tgt, false, Behavior));
                    for (int i = 1; i < tgtRevLSTM.LSTMCells.Count; i++)
                        lstmBiTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(tgtRevLSTM.LSTMCells[i], lstmBiTgtOutput, Behavior));
                }
                else
                {
                    lstmBiTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(tgtEmbedOutput, false, Behavior));
                    for (int i = 0; i < tgtRevLSTM.LSTMCells.Count; i++)
                        lstmBiTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(tgtRevLSTM.LSTMCells[i], lstmBiTgtOutput, Behavior));
                }

                switch (BuilderParameters.PoolingType)
                {
                    case PoolingType.MAX:
                        HiddenBatchData srcM1Output = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmSrcOutput, lstmSrcOutput.MapForward, Behavior));
                        HiddenBatchData srcM2Output = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmBiSrcOutput, lstmBiSrcOutput.MapForward, Behavior));
                        srcOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { srcM1Output, srcM2Output }, Behavior));

                        HiddenBatchData tgtM1Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(lstmTgtOutput, false, 0, lstmTgtOutput.MapForward, Behavior));
                        HiddenBatchData tgtM2Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(lstmBiTgtOutput, true, 0, lstmBiTgtOutput.MapForward, Behavior));
                        tgtOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { tgtM1Output, tgtM2Output }, Behavior));
                        break;
                    case PoolingType.LAST:
                        HiddenBatchData srcD1Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(lstmSrcOutput, false, 0, lstmSrcOutput.MapForward, Behavior));
                        HiddenBatchData srcD2Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(lstmBiSrcOutput, true, 0, lstmBiSrcOutput.MapForward, Behavior));
                        srcOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { srcD1Output, srcD2Output }, Behavior));

                        HiddenBatchData tgtD1Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(lstmTgtOutput, false, 0, lstmTgtOutput.MapForward, Behavior));
                        HiddenBatchData tgtD2Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(lstmBiTgtOutput, true, 0, lstmBiTgtOutput.MapForward, Behavior));
                        tgtOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { tgtD1Output, tgtD2Output }, Behavior));
                        break;
                }
            }
            else
            {
                switch (BuilderParameters.PoolingType)
                {
                    case PoolingType.MAX:
                        srcOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmSrcOutput, lstmSrcOutput.MapForward, Behavior));
                        tgtOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmTgtOutput, lstmTgtOutput.MapForward, Behavior));
                        break;
                    case PoolingType.LAST:
                        srcOutput = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(lstmSrcOutput, false, 0, lstmSrcOutput.MapForward, Behavior));
                        tgtOutput = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(lstmTgtOutput, false, 0, lstmTgtOutput.MapForward, Behavior));
                        break;
                }
            }

            IntArgument arg = new IntArgument("batchSize");
            cg.AddRunner(new HiddenDataBatchSizeRunner(srcOutput, arg, Behavior));

            /*************** Sample Source-Target Match Data. ********************/
            //BiMatchBatchData matchData = new BiMatchBatchData(src.Stat.MAX_BATCHSIZE, Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.NTrail : 0, BuilderParameters.Device, false);
            BiMatchBatchData matchData = (BiMatchBatchData)cg.AddRunner(new RandomBiMatchRunner(src.Stat.MAX_BATCHSIZE,
                                (Behavior.RunMode == DNNRunMode.Train || BuilderParameters.Evaluation == EvaluationType.PRECISIONK) ? BuilderParameters.NTrail : 0, arg, Behavior, false));

            /*************** similarity between source text and target text. ********************/
            HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(srcOutput, tgtOutput, matchData, BuilderParameters.SimiType, Behavior));
            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    switch (BuilderParameters.LossFunction)
                    {
                        case LossFunctionType.SampleSoftmax:
                            if (label != null) cg.AddObjective(new MultiClassSoftmaxRunner(null, label, simiOutput.Output, simiOutput.Deriv, BuilderParameters.Gamma, Behavior));
                            else cg.AddObjective(new MultiClassSoftmaxRunner(null, simiOutput.Output, simiOutput.Deriv, BuilderParameters.Gamma, Behavior));
                            break;
                        case LossFunctionType.SampleLogisicalRegression:
                            if (label != null) cg.AddObjective(new SampleCrossEntropyRunner(label, simiOutput.Output, simiOutput.Deriv, BuilderParameters.Gamma, Behavior));
                            else cg.AddObjective(new CrossEntropyRunner(matchData.MatchInfo, simiOutput.Output, simiOutput.Deriv, BuilderParameters.Gamma, Behavior));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    if (!BuilderParameters.SRC_EMBED_FILE.Equals(string.Empty))
                    {
                        cg.AddRunner(new VectorDumpRunner(srcOutput.Output, BuilderParameters.SRC_EMBED_FILE));
                    }

                    if (!BuilderParameters.TGT_EMBED_FILE.Equals(string.Empty))
                    {
                        cg.AddRunner(new VectorDumpRunner(tgtOutput.Output, BuilderParameters.TGT_EMBED_FILE));
                    }

                    switch (BuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(label.Data, simiOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                            break;
                        case EvaluationType.MSE:
                            cg.AddRunner(new RMSEDiskDumpRunner(label.Data, simiOutput.Output, BuilderParameters.Gamma, true, BuilderParameters.ScoreOutputPath));
                            break;
                        case EvaluationType.NDCG:
                            cg.AddRunner(new NDCGDiskDumpRunner(simiOutput.Output, BuilderParameters.MappingValidData, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                            break;
                        case EvaluationType.PRECISIONK:
                            cg.AddRunner(new PrecisionKRunner(simiOutput.Output, matchData, BuilderParameters.MappingValidData, BuilderParameters.ScoreOutputPath));
                            break;
                    }
                    break;
            }

            return cg;
        }
        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager ComputeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure deepNet = null;
            LSTMStructure queryD1Model = null;
            LSTMStructure queryD2Model = null;
            LSTMStructure docD1Model = null;
            LSTMStructure docD2Model = null;
            LayerStructure SrcEmbed = null;
            LayerStructure TgtEmbed = null;

            if (BuilderParameters.SEED_MODEL != string.Empty)
            {
                deepNet = new CompositeNNStructure(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            }
            else
            {
                deepNet = new CompositeNNStructure();

                int inputSrcDim = DataPanel.TrainSrcBin.Stat.FEATURE_DIM;
                int inputTgtDim = DataPanel.TrainTgtBin.Stat.FEATURE_DIM;

                if (BuilderParameters.SrcEmbedDim > 0)
                {
                    SrcEmbed = (new LayerStructure(inputSrcDim, BuilderParameters.SrcEmbedDim,
                         A_Func.Linear, N_Type.Convolution_layer, 1, 0, false, device));
                    
                    deepNet.AddLayer(SrcEmbed);
                    if (!BuilderParameters.InitSrcEmbedding.Equals(string.Empty))
                    {
                        int wordDim = BuilderParameters.SrcEmbedDim;
                        Logger.WriteLog("Init Src Word Embedding ...");
                        int hit = 0;
                        using (StreamReader mreader = new StreamReader(BuilderParameters.InitSrcEmbedding))
                        {
                            while (!mreader.EndOfStream)
                            {
                                string[] items = mreader.ReadLine().Split(' ');
                                int wordIdx = int.Parse(items[0]);
                                float[] vec = new float[items.Length - 1];
                                for (int i = 1; i < items.Length; i++)
                                {
                                    vec[i - 1] = float.Parse(items[i]);
                                }

                                for (int i = 0; i < wordDim; i++)
                                {
                                    SrcEmbed.weight.MemPtr[wordIdx * wordDim + i] = vec[i];
                                }
                                hit++;
                            }
                        }
                        SrcEmbed.weight.SyncFromCPU();
                        Logger.WriteLog("Init Src Word Embedding Done, {0} words initalized in total {1}.", hit, inputSrcDim);
                    }
                    inputSrcDim = BuilderParameters.SrcEmbedDim;
                }

                if(BuilderParameters.TgtEmbedDim > 0)
                {
                    TgtEmbed = (new LayerStructure(inputTgtDim, BuilderParameters.TgtEmbedDim,
                         A_Func.Linear, N_Type.Convolution_layer, 1, 0, false, device));
                    deepNet.AddLayer(TgtEmbed);

                    if (!BuilderParameters.InitTgtEmbedding.Equals(string.Empty))
                    {
                        int wordDim = BuilderParameters.TgtEmbedDim;
                        Logger.WriteLog("Init Tgt Word Embedding ...");
                        int hit = 0;
                        using (StreamReader mreader = new StreamReader(BuilderParameters.InitTgtEmbedding))
                        {
                            while (!mreader.EndOfStream)
                            {
                                string[] items = mreader.ReadLine().Split(' ');
                                int wordIdx = int.Parse(items[0]);
                                float[] vec = new float[items.Length - 1];
                                for (int i = 1; i < items.Length; i++)
                                {
                                    vec[i - 1] = float.Parse(items[i]);
                                }
                                for (int i = 0; i < wordDim; i++)
                                {
                                    TgtEmbed.weight.MemPtr[wordIdx * wordDim + i] = vec[i];
                                }
                                hit++;
                            }
                        }
                        TgtEmbed.weight.SyncFromCPU();
                        Logger.WriteLog("Init Tgt Word Embedding Done, {0} words initalized in total {1}.", hit, inputTgtDim);
                    }
                    inputTgtDim = BuilderParameters.TgtEmbedDim;
                }

                deepNet.AddLayer(new LSTMStructure(inputSrcDim, BuilderParameters.LSTM_LAYER_DIM, device));
                deepNet.AddLayer(new LSTMStructure(inputTgtDim, BuilderParameters.LSTM_LAYER_DIM, device));

                if (BuilderParameters.IS_BILSTM)
                {
                    deepNet.AddLayer(new LSTMStructure(inputSrcDim, BuilderParameters.LSTM_LAYER_DIM, device));
                    deepNet.AddLayer(new LSTMStructure(inputTgtDim, BuilderParameters.LSTM_LAYER_DIM, device));
                }

            }

            int link = 0;
            if (BuilderParameters.SrcEmbedDim > 0) SrcEmbed = (LayerStructure)deepNet.CompositeLinks[link++];
            if (BuilderParameters.TgtEmbedDim > 0) TgtEmbed = (LayerStructure)deepNet.CompositeLinks[link++];
            queryD1Model = (LSTMStructure)deepNet.CompositeLinks[link++];
            docD1Model = (LSTMStructure)deepNet.CompositeLinks[link++];
            if (BuilderParameters.IS_BILSTM)
            {
                queryD2Model = (LSTMStructure)deepNet.CompositeLinks[link++];
                docD2Model = (LSTMStructure)deepNet.CompositeLinks[link++];
            }
            
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    Logger.WriteLog("Loading DSSM Structure.");

                    Logger.WriteLog(queryD1Model.Descr());
                    Logger.WriteLog(docD1Model.Descr());
                    Logger.WriteLog("Load BiLSTM Structure Finished.");

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin, DataPanel.TrainLabelBin, 
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = ComputeLib },
                        SrcEmbed, TgtEmbed, queryD1Model, queryD2Model, docD1Model, docD2Model);
                    
                    ComputationGraph predCG = null;
                    if (BuilderParameters.IsValidFile)
                        predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, DataPanel.ValidLabelBin,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = ComputeLib },
                            SrcEmbed, TgtEmbed, queryD1Model, queryD2Model, docD1Model, docD2Model);

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    trainCG.InitOptimizer(deepNet, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = ComputeLib });

                    // Select Best Model During Training.
                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\DSSM.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()), FileMode.Create, FileAccess.Write)))
                            {
                                deepNet.Serialize(writer);
                            }
                        }

                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = predCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                                using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\DSSM.{1}.model", BuilderParameters.ModelOutputPath, "done"), FileMode.Create, FileAccess.Write)))
                                {
                                    deepNet.Serialize(writer);
                                }
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }
                    }
                    Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);

                    break;
                case DNNRunMode.Predict:
                    Logger.WriteLog("Loading DSSM Structure.");
                    Logger.WriteLog("Total Sample Number {0}", DataPanel.ValidSrcBin.Stat.TotalSampleNumber);
                    if (BuilderParameters.IsValidFile)
                    {
                        ComputationGraph cg = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, DataPanel.ValidLabelBin,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = ComputeLib },
                            SrcEmbed, TgtEmbed, queryD1Model, queryD2Model, docD1Model, docD2Model);
                        cg.Execute();
                    }
                    break;
            }
            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainTgtBin = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainLabelBin = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidTgtBin = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidLabelBin = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.QueryTrainData, BuilderParameters.QueryTrainData+".cursor");
                    TrainTgtBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.DocTrainData, BuilderParameters.DocTrainData + ".cursor");
                    if (!BuilderParameters.ScoreTrainData.Equals(string.Empty)) TrainLabelBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ScoreTrainData);

                    TrainSrcBin.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(64,  ParameterSetting.RANDOM_SEED);
                    if (TrainLabelBin != null) TrainLabelBin.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }
                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.QueryValidData);
                    ValidTgtBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.DocValidData);
                    if (!BuilderParameters.ScoreValidData.Equals(string.Empty)) ValidLabelBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ScoreValidData);

                    ValidSrcBin.InitThreadSafePipelineCashier(64, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(64, false);
                    if (ValidLabelBin != null) ValidLabelBin.InitThreadSafePipelineCashier(64, false);
                }
            }


        }
    }
}
