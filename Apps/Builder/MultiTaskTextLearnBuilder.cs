﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
namespace BigLearn.DeepNet
{
    public class MultiTaskTextLearnBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                
                Argument.Add("LAYER-DIM", new ParameterArgument("1", "LSTM Layer Dim"));
                Argument.Add("POOL-DROPOUT", new ParameterArgument("0", "LSTM Max-Pooling Dropout."));

                Argument.Add("TASK-NUM", new ParameterArgument("1", "Multi Task Number."));

                Argument.Add("TRAIN-S", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID-S", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("NN-ACTIVATION-S", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("NN-LAYER-BIAS-S", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("NN-LAYER-DIM-S", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("NN-LAYER-DROPOUT-S", new ParameterArgument("0", "DNN Layer Dropout"));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.BinaryClassification).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

                Argument.Add("CHECK-GRAD", new ParameterArgument("0", "Check Gradient!"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return TrainData.Length > 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static int TaskNum { get { return int.Parse(Argument["TASK-NUM"].Value); } }
            public static string[] TrainData { get { return Argument["TRAIN-S"].Value.Split(';'); } }
            public static string[] ValidData { get { return Argument["VALID-S"].Value.Split(';'); } }

            public static List<int[]> NN_LAYER_DIM { get { return Argument["NN-LAYER-DIM-S"].Value.Split(';').Select(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray()).ToList(); } }
            public static List<A_Func[]> NN_ACTIVATION { get { return Argument["NN-ACTIVATION-S"].Value.Split(';').Select(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray()).ToList(); } }

            public static List<bool[]> NN_LAYER_BIAS { get { return Argument["NN-LAYER-BIAS-S"].Value.Split(';').Select(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray()).ToList(); } }
            public static List<float[]> NN_LAYER_DROPOUT { get { return Argument["NN-LAYER-DROPOUT-S"].Value.Split(';').Select(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray()).ToList(); } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float POOL_DROPOUT { get { return float.Parse(Argument["POOL-DROPOUT"].Value); } }
            
            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

            public static string ScoreOutputPath
            {
                get
                {
                    if (!Argument["SCORE-PATH"].Value.Equals(string.Empty)) return Argument["SCORE-PATH"].Value;
                    else return LogFile + ".tmp.score";
                }
            }

            public static string MetricOutputPath
            {
                get
                {
                    if (!Argument["METRIC-PATH"].Value.Equals(string.Empty)) return Argument["METRIC-PATH"].Value;
                    else return LogFile + ".tmp.metric";
                }
            }

            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

            public static bool CheckGrad { get { return int.Parse(Argument["CHECK-GRAD"].Value) > 0; } }
        }

        public override BuilderType Type { get { return BuilderType.MULTITASK_LSTMSEQPOOL; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public ComputationGraph BuildComputationGraph(DataCashier<SeqSparseDataSource, SequenceDataStat> data, DNNRunMode mode, LSTMStructure lstmModel, DNNStructure dnnModel)
        {
            ComputationGraph cg = new ComputationGraph();

            DeviceType device = MathOperatorManager.Device(BuilderParameters.GPUID);
            CudaMathOperation lib = new CudaMathOperation(true, true);
            RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };

            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dataSource.SequenceData, Behavior));

            SeqDenseRecursiveData lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(lstmModel.LSTMCells[0], dataSource.SequenceData, false, Behavior));

            for (int i = 1; i < lstmModel.LSTMCells.Count; i++)
                lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], lstmOutput, Behavior));

            HiddenBatchData poolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmOutput, lstmOutput.MapForward, Behavior));

            HiddenBatchData dropOutput = poolingOutput;
            if (mode == DNNRunMode.Train && BuilderParameters.POOL_DROPOUT > 0 && BuilderParameters.POOL_DROPOUT < 1)
            {
                dropOutput = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(poolingOutput, BuilderParameters.POOL_DROPOUT,
                        new RunnerBehavior() { RunMode = mode, Device = device }));
            }

            HiddenBatchData labelOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(dnnModel, dropOutput,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            switch (mode)
            {
                case DNNRunMode.Train:
                    switch (BuilderParameters.LossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, labelOutput.Output, labelOutput.Deriv, BuilderParameters.Gamma,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                        case LossFunctionType.MultiClassification:
                            cg.AddObjective(new MultiClassSoftmaxRunner(dataSource.InstanceLabel, labelOutput.Output, labelOutput.Deriv, BuilderParameters.Gamma,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                    }

                    break;
                case DNNRunMode.Predict:
                    switch (BuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, labelOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                            break;
                        case EvaluationType.ACCURACY:
                            cg.AddRunner(new AccuracyDiskDumpRunner(dataSource.InstanceLabel, labelOutput.Output, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            //run lstm pooling runner.
            Logger.OpenLog(BuilderParameters.LogFile);

            MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    /// the example of building the computational graph. 
                    /// construct model parameters.
                    //CompositeNNStructure deepModel = new CompositeNNStructure();
                    LSTMStructure lstmModel = new LSTMStructure(DataPanel.Train[0].Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM);
                    lstmModel.InitOptimizer(new StructureLearner()
                    {
                        Optimizer = BuilderParameters.Optimizer,
                        LearnRate = BuilderParameters.LearnRate,
                        AdaBoost = BuilderParameters.AdaBoost,
                        ClipDelta = BuilderParameters.DELTACLIP,
                        ClipWeight = BuilderParameters.WEIGHTCLIP
                    });
                    List<DNNStructure> dnnModels = new List<DNNStructure>();

                    List<ComputationGraph> trainGraphs = new List<ComputationGraph>();
                    List<ComputationGraph> predGraphs = new List<ComputationGraph>();
                    for (int i = 0; i < BuilderParameters.TaskNum; i++)
                    {
                        DNNStructure dnnModel = new DNNStructure(BuilderParameters.LAYER_DIM.Last(),
                                                    BuilderParameters.NN_LAYER_DIM[i],
                                                    BuilderParameters.NN_ACTIVATION[i],
                                                    BuilderParameters.NN_LAYER_DROPOUT[i],
                                                    BuilderParameters.NN_LAYER_BIAS[i]);

                        dnnModel.InitOptimizer(new StructureLearner()
                        {
                            Optimizer = BuilderParameters.Optimizer,
                            LearnRate = BuilderParameters.LearnRate,
                            AdaBoost = BuilderParameters.AdaBoost,
                            ClipDelta = BuilderParameters.DELTACLIP,
                            ClipWeight = BuilderParameters.WEIGHTCLIP
                        });

                        ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train[i], DNNRunMode.Train, lstmModel, dnnModel);
                        ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid[i], DNNRunMode.Predict, lstmModel, dnnModel);
                        trainGraphs.Add(trainCG);
                        predGraphs.Add(predCG);
                        dnnModels.Add(dnnModel);
                    }
                    
                    /// construct single source computational graph.
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double[] validScore = new double[BuilderParameters.TaskNum];
                    double[] bestValidScore = Enumerable.Range(0, BuilderParameters.TaskNum).Select(i => double.MinValue).ToArray();
                    double[] bestValidIter = Enumerable.Range(0, BuilderParameters.TaskNum).Select(i => -1.0).ToArray();

                    for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
                    {
                        for (int t = 0; t < BuilderParameters.TaskNum; t++)
                        {
                            double loss = trainGraphs[t].Execute();
                            Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                            //string modelName = string.Format(@"{0}\\LSTMPool.{1}.model", BuilderParameters.ModelOutputPath, iter);
                            //using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                            //{
                            //    lstmModel.Serialize(writer);
                            //    dnnModel.Serialize(writer);
                            //}
                            validScore[t] = predGraphs[t].Execute();
                            if (validScore[t] >= bestValidScore[t])
                            {
                                bestValidScore[t] = validScore[t];
                                bestValidIter[t] = iter;
                                //File.Copy(string.Format(@"{0}\\LSTMPool.{1}.model", BuilderParameters.ModelOutputPath, bestValidIter.ToString()),
                                //          string.Format(@"{0}\\LSTMPool.{1}.model", BuilderParameters.ModelOutputPath, "done"), true);
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore[t], bestValidIter[t]);
                        }
                    }
                    for (int t = 0; t < BuilderParameters.TaskNum; t++)
                    {
                        Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore[t], bestValidIter[t]);
                    }
                    break;
                case DNNRunMode.Predict:
                    break;
            }

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static List<DataCashier<SeqSparseDataSource, SequenceDataStat>> Train = new List<DataCashier<SeqSparseDataSource, SequenceDataStat>>();
            public static List<DataCashier<SeqSparseDataSource, SequenceDataStat>> Valid = new List<DataCashier<SeqSparseDataSource, SequenceDataStat>>();

            public static void Init()
            {
                for(int i=0;i<BuilderParameters.TrainData.Length;i++)
                {
                    Train.Add(new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainData[i]));
                    Train[i].InitThreadSafePipelineCashier(100, true);
                }

                for (int i = 0; i < BuilderParameters.ValidData.Length; i++)
                {
                    Valid.Add(new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidData[i]));
                    Valid[i].InitThreadSafePipelineCashier(100, false);
                }
            }
        }
    }
}
