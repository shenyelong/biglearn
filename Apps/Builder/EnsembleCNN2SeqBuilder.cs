﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class EnsembleCNN2SeqBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));
                Argument.Add("VALID-MAP", new ParameterArgument(string.Empty, "MAP Valid Data."));

                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.ACCURACY).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("256", "DNN Layer Dim"));

                Argument.Add("SRC-LAYER-DIM", new ParameterArgument("256,256", "Source Layer Dim"));
                Argument.Add("SRC-ACTIVATION", new ParameterArgument("1,1", ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("SRC-WINDOW", new ParameterArgument("5,5", "Source DNN Window Size"));

                Argument.Add("ATT-HIDDEN", new ParameterArgument("256", "Attention Layer Hidden Dim"));
                Argument.Add("IS-ATT", new ParameterArgument("1", "Is Attention Model"));

                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
                Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return DNNRunMode.Predict; } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static string ValidMapData { get { return Argument["VALID-MAP"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

            public static string Vocab { get { return Argument["VOCAB"].Value; } }
            public static Vocab2Freq mVocabDict = null;
            public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
            public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            /// <summary>
            /// Decode LSTM Dimension.
            /// </summary>
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] SRC_LAYER_DIM { get { return Argument["SRC-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] SRC_ACTIVATION { get { return Argument["SRC-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] SRC_WINDOW { get { return Argument["SRC-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }
            public static bool IsAttention { get { return int.Parse(Argument["IS-ATT"].Value) > 0; } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
            public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }


            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

        }

        public override BuilderType Type { get { return BuilderType.ENSEMBLE_CNN2SEQ; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            OptimizerParameters.Parse(fileName);
            Cudalib.CudaInit(BuilderParameters.GPUID);
        }

        public class MapDataHelpRunner : StructRunner
        {
            public CudaPieceInt SrcShuffle;
            public CudaPieceInt TgtShuffle;
            public DenseBatchData MapData;
            public MapDataHelpRunner(DenseBatchData mapData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                MapData = mapData;
                SrcShuffle = new CudaPieceInt(MapData.Stat.MAX_BATCHSIZE / 2, true, Behavior.Device == DeviceType.GPU);
                TgtShuffle = new CudaPieceInt(MapData.Stat.MAX_BATCHSIZE / 2, true, Behavior.Device == DeviceType.GPU);
            }
            public override void Forward()
            {
                MapData.Data.SyncToCPU();

                for (int i = 0; i < MapData.BatchSize / 2; i++) SrcShuffle.MemPtr[i] = (int)MapData.Data.MemPtr[i];
                for (int i = 0; i < MapData.BatchSize / 2; i++) TgtShuffle.MemPtr[i] = (int)MapData.Data.MemPtr[MapData.BatchSize / 2 + i];
                SrcShuffle.SyncFromCPU();
                TgtShuffle.SyncFromCPU();
            }
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             IDataCashier<DenseBatchData, DenseDataStat> mapData,
                                                             List<LayerStructure>[] srcLayers,
                                                             LSTMStructure[] decoder,
                                                             MLPAttentionStructure[] attentions,
                                                             EmbedStructure[] decodeEmbedTgt,
                                                             DNNRunMode mode, CompositeNNStructure[] Model, int EnsembleNum, int deviceID, EvaluationType evalType)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = mode == DNNRunMode.Train ? 50 : 500 };

            DeviceType device = MathOperatorManager.SetDevice(deviceID);
            CudaMathOperation lib = new CudaMathOperation(true, true);
            RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };

            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));

            /// no need to resort the output.
            SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(SrcData.SequenceData, Behavior, false));

            DenseBatchData MapData = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(mapData, Behavior));
            MapDataHelpRunner mapHelpRunner = new MapDataHelpRunner(MapData, Behavior);
            cg.AddRunner(mapHelpRunner);

            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));
            SeqHelpData TgtHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(TgtData.SequenceData, Behavior));

            EnsembleBeamSearchRunner ensembleBeamSearchRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
                 BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, decodeEmbedTgt.First().VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);

            for (int ensemble = 0; ensemble < EnsembleNum; ensemble++)
            {
                SeqDenseBatchData SrcOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(srcLayers[ensemble][0], SrcData.SequenceData, Behavior));
                for (int i = 1; i < srcLayers[ensemble].Count; i++)
                    SrcOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(srcLayers[ensemble][i], SrcOutput, Behavior));

                SeqDenseBatchData SrcEnsembleMemory = SrcOutput;
                HiddenBatchData SrcStatusOriginal = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(SrcOutput, Behavior));
                HiddenBatchData SrcStatus = (HiddenBatchData)cg.AddRunner(new MatrixShuffleRunner(SrcStatusOriginal, CudaPieceInt.Empty, mapHelpRunner.SrcShuffle, Behavior));

                MLPAttentionRunner decodeAttentionRunners = new MLPAttentionRunner(attentions[ensemble], SrcEnsembleMemory, SrcHelp, MapData,
                        TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior);

                List<MLPAttentionRunner> softRunners = new List<MLPAttentionRunner>();

                //softRunners.Add(decodeAttentionRunners);
                if (BuilderParameters.IsAttention) softRunners.Add(decodeAttentionRunners);
                else softRunners.Add(null);

                List<Tuple<HiddenBatchData, HiddenBatchData>> statusList = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
                statusList.Add(new Tuple<HiddenBatchData, HiddenBatchData>(SrcStatus, SrcStatus));

                // the probability of exactly generate the true tgt.
                LSTMDecodingRunner decodeRunner = new LSTMDecodingRunner(decoder[ensemble], decodeEmbedTgt[ensemble], statusList, 
                    softRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), Behavior);
                ensembleBeamSearchRunner.AddBeamSearch(decodeRunner);
                //cg.AddRunner(decodeRunner);
            }
            cg.AddRunner(ensembleBeamSearchRunner);


            switch (evalType)
            {
                case EvaluationType.ACCURACY:
                    cg.AddRunner(new BeamAccuracyRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                    break;
                case EvaluationType.BSBLEU:
                    cg.AddRunner(new BLEUDiskDumpRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
                case EvaluationType.ROUGE:
                    cg.AddRunner(new ROUGEDiskDumpRunner(ensembleBeamSearchRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                    break;
                case EvaluationType.TOPKBEAM:
                    cg.AddRunner(new TopKBeamRunner(ensembleBeamSearchRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                    break;
            }


            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            List<CompositeNNStructure> modelStructures = new List<CompositeNNStructure>();
            string[] models = BuilderParameters.ModelOutputPath.Split(';').ToArray();

            foreach(string model in models)
            {
                modelStructures.Add(new CompositeNNStructure(new BinaryReader(new FileStream(model, FileMode.Open, FileAccess.Read)), DeviceType.GPU));
            }

            //decodeLSTMStructure = (LSTMStructure)modelStructure.CompositeLinks[0];
            //embedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[1];
            //attentionStructures = (AttentionStructure)modelStructure.CompositeLinks[2];
            //for (int i = 3; i < modelStructure.CompositeLinks.Count; i++) srcLayers.Add((ConvStructure)modelStructure.CompositeLinks[i]);

            List<LayerStructure>[] srcLayers = new List<LayerStructure>[modelStructures.Count]; 
            for (int t = 0; t < modelStructures.Count; t++)
            {
                srcLayers[t] = new List<LayerStructure>();
                for (int i = 3; i < modelStructures[t].CompositeLinks.Count; i++)
                    srcLayers[t].Add((LayerStructure)modelStructures[t].CompositeLinks[i]);
            }

            ComputationGraph cg = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin, DataPanel.ValidMapBin,
                srcLayers, modelStructures.Select(m => (LSTMStructure)m.CompositeLinks[0]).ToArray(),
                modelStructures.Select(m => (MLPAttentionStructure)m.CompositeLinks[2]).ToArray(),
                modelStructures.Select(m => (EmbedStructure)m.CompositeLinks[1]).ToArray(), DNNRunMode.Predict,
                modelStructures.ToArray(), models.Length, BuilderParameters.GPUID, BuilderParameters.Evaluation);
            double predScore = cg.Execute();
            Logger.WriteLog("Prediction Score {0}", predScore);
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidMapBin = null;

            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);
                    ValidMapBin = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidMapData);

                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                    ValidMapBin.InitThreadSafePipelineCashier(100, false);
                }
            }
        }
    }
}