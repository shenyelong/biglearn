﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class MultiTaskBuilder : Builder
    {
        public class MULTITASK_SEQPOOLParameters : BaseModelArgument<MULTITASK_SEQPOOLParameters>
        {
            static MULTITASK_SEQPOOLParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("TASK-NUM", new ParameterArgument("1", "Multi Task Number."));

                /**************************Shared NN Config**************************************/
                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("WINDOW-SIZE", new ParameterArgument("1", "CNN Window Size"));
                Argument.Add("ARCH", new ParameterArgument(((int)N_Type.Fully_Connected).ToString(), ParameterUtil.EnumValues(typeof(N_Type))));
                Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

                /**************************Classification Config**************************************/
                //data
                Argument.Add("TRAIN-S", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID-S", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("SHARED-NET-TYPE", new ParameterArgument(((int)SharedNetType.LSTMNet).ToString(), "Shared Net Type"));
                Argument.Add("POOL-DROPOUT", new ParameterArgument("0", "LSTM Max-Pooling Dropout."));

                Argument.Add("NN-ACTIVATION-S", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("NN-LAYER-BIAS-S", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("NN-LAYER-DIM-S", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("NN-LAYER-DROPOUT-S", new ParameterArgument("0", "DNN Layer Dropout"));
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("LOSS-FUNCTION-S", new ParameterArgument(((int)LossFunctionType.BinaryClassification).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("EVALUATION-S", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                /**************************DSSM Config**************************************/
                Argument.Add("QUERY", new ParameterArgument(string.Empty, "QUERY Train Data."));
                Argument.Add("DOC", new ParameterArgument(string.Empty, "DOC Train Data."));
                Argument.Add("SCORE", new ParameterArgument(string.Empty, "SCORE Train Data."));
                Argument.Add("QUERY-VALID", new ParameterArgument(string.Empty, "QUERY Valid Data."));
                Argument.Add("DOC-VALID", new ParameterArgument(string.Empty, "DOC Valid Data."));
                Argument.Add("SCORE-VALID", new ParameterArgument(string.Empty, "Score Valid Data."));

                Argument.Add("PAIRNN-ENABLE", new ParameterArgument("1", "If enable the DSSM"));
                Argument.Add("PAIRNN-ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("PAIRNN-LAYER-BIAS", new ParameterArgument("1", "DSSM Layer Bias"));
                Argument.Add("PAIRNN-LAYER-DIM", new ParameterArgument("1", "DSSM Layer Dim"));
                Argument.Add("PAIRNN-LAYER-DROPOUT", new ParameterArgument("0", "DSSM Layer Dropout"));
                Argument.Add("PAIRNN-GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("PAIRNN-LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.PairwiseRank).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("PAIRNN-EVALUATION", new ParameterArgument(((int)EvaluationType.NDCG).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));
                Argument.Add("NTRAIL", new ParameterArgument("0", "Negative Sample Number."));
                Argument.Add("SIM-TYPE", new ParameterArgument(((int)SimilarityType.CosineSimilarity).ToString(), ParameterUtil.EnumValues(typeof(SimilarityType))));


                Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

                Argument.Add("SCORE-DIR", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

                Argument.Add("CHECK-GRAD", new ParameterArgument("0", "Check Gradient!"));
                Argument.Add("PREDICTION-LAG", new ParameterArgument("1000", "prediction lag"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return TrainData.Length > 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static int TaskNum { get { return int.Parse(Argument["TASK-NUM"].Value); } }
            public static string[] TrainData { get { return Argument["TRAIN-S"].Value.Split(';'); } }
            public static string[] ValidData { get { return Argument["VALID-S"].Value.Split(';'); } }

            public static List<int[]> NN_LAYER_DIM { get { return Argument["NN-LAYER-DIM-S"].Value.Split(';').Select(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray()).ToList(); } }
            public static List<A_Func[]> NN_ACTIVATION { get { return Argument["NN-ACTIVATION-S"].Value.Split(';').Select(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray()).ToList(); } }

            public static List<bool[]> NN_LAYER_BIAS { get { return Argument["NN-LAYER-BIAS-S"].Value.Split(';').Select(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray()).ToList(); } }
            public static List<float[]> NN_LAYER_DROPOUT { get { return Argument["NN-LAYER-DROPOUT-S"].Value.Split(';').Select(s => s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray()).ToList(); } }

            public static EvaluationType[] Evaluations { get { return Argument["EVALUATION-S"].Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(i => (EvaluationType)int.Parse(i)).ToArray(); } }
            public static LossFunctionType[] LossFunctions { get { return Argument["LOSS-FUNCTION-S"].Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(i => (LossFunctionType)int.Parse(i)).ToArray(); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] WINDOW_SIZE { get { return Argument["WINDOW-SIZE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static N_Type[] ARCH { get { return Argument["ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static string QueryTrainData { get { return Argument["QUERY"].Value; } }
            public static string DocTrainData { get { return Argument["DOC"].Value; } }
            public static string ScoreTrainData { get { return Argument["SCORE"].Value; } }

            public static bool IsTrainFile { get { return !(QueryTrainData.Equals(string.Empty) || DocTrainData.Equals(string.Empty)); } }

            public static string QueryValidData { get { return Argument["QUERY-VALID"].Value; } }
            public static string DocValidData { get { return Argument["DOC-VALID"].Value; } }
            public static string ScoreValidData { get { return Argument["SCORE-VALID"].Value; } }
            public static bool IsValidFile { get { return !(QueryValidData.Equals(string.Empty) || DocValidData.Equals(string.Empty)); } }

            public static bool PairNN_Enable { get { return bool.Parse(Argument["PAIRNN-ENABLE"].Value); } }
            public static int[] PairNN_LAYER_DIM { get { return Argument["PAIRNN-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] PairNN_WINDOW_SIZE { get { return Argument["PAIRNN-WINDOW-SIZE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static N_Type[] PairNN_ARCH { get { return Argument["PAIRNN-ARCH"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (N_Type)int.Parse(i)).ToArray(); } }
            public static A_Func[] PairNN_ACTIVATION { get { return Argument["PAIRNN-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] PairNN_LAYER_BIAS { get { return Argument["PAIRNN-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] PairNN_LAYER_DROPOUT { get { return Argument["PAIRNN-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static EvaluationType PairNNEvaluation { get { return (EvaluationType)int.Parse(Argument["PAIRNN-EVALUATION"].Value); } }
            public static LossFunctionType PairNNLossFunction { get { return (LossFunctionType)int.Parse(Argument["PAIRNN-LOSS-FUNCTION"].Value); } }
            public static SimilarityType SimiType { get { return (SimilarityType)(int.Parse(Argument["SIM-TYPE"].Value)); } }

            public static float PAIRNNGamma { get { return float.Parse(Argument["PAIRNN-GAMMA"].Value); } }

            public static int NTrail { get { return int.Parse(Argument["NTRAIL"].Value); } }


            public static SharedNetType SharedNNType { get { return (SharedNetType)int.Parse(Argument["SHARED-NET-TYPE"].Value); } }

            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

            public static int PredictionLag { get { return int.Parse(Argument["PREDICTION-LAG"].Value); } }

            public static float POOL_DROPOUT { get { return float.Parse(Argument["POOL-DROPOUT"].Value); } }

            public static string ScoreOutputDir
            {
                get
                {
                    return Argument["SCORE-DIR"].Value;
                }
            }

            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

            public static bool CheckGrad { get { return int.Parse(Argument["CHECK-GRAD"].Value) > 0; } }
        }

        public override BuilderType Type { get { return BuilderType.MULTITASK_SEQPOOL; } }

        public override void InitStartup(string fileName)
        {
            MULTITASK_SEQPOOLParameters.Parse(fileName);
        }
        public enum SharedNetType { LSTMNet = 0, GRUNet = 1, CNNNet = 2 }

        public static ComputationGraph BuildPairNNComputationGraph(DSSMDataCashier data, DNNRunMode mode, Structure sharedModel, DNNStructure srcDNNModel, DNNStructure tgtDNNModel,
            string scoreData,string outputPath, string metricOutputPath)
        {
            ComputationGraph cg = new ComputationGraph();
            DeviceType device = MathOperatorManager.Device(MULTITASK_SEQPOOLParameters.GPUID);
            CudaMathOperation lib = new CudaMathOperation(true, true);
            RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };


            /**************** Get Data from DataCashier *********/
            DSSMData dssmData = (DSSMData)cg.AddDataRunner(new DataRunner<DSSMData, DSSMDataStat>(data, Behavior));

            HiddenBatchData srcSharedRunner = null;
            HiddenBatchData tgtSharedRunner = null;
            switch (MULTITASK_SEQPOOLParameters.SharedNNType)
            {
                case SharedNetType.GRUNet:
                    GRUStructure gruModel = sharedModel as GRUStructure;
                    //src runer
                    SeqDenseBatchData gruSrcOutput = (SeqDenseBatchData)cg.AddRunner(new GRUSparseRunner<SeqSparseBatchData>(gruModel.GRUCells[0], dssmData.SrcInput,
                    Behavior));

                    for (int i = 1; i < gruModel.GRUCells.Count; i++)
                        gruSrcOutput = (SeqDenseBatchData)cg.AddRunner(new GRUDenseRunner<SeqDenseBatchData>(gruModel.GRUCells[i], gruSrcOutput,
                            Behavior));

                    HiddenBatchData gruSrcPoolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(gruSrcOutput,
                            new RunnerBehavior() { RunMode = mode, Device = device }));

                    srcSharedRunner = gruSrcPoolingOutput;
                    if (mode == DNNRunMode.Train && MULTITASK_SEQPOOLParameters.POOL_DROPOUT > 0 && MULTITASK_SEQPOOLParameters.POOL_DROPOUT < 1)
                    {
                        srcSharedRunner = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(srcSharedRunner, MULTITASK_SEQPOOLParameters.POOL_DROPOUT,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                    }
                    ///tgt
                    SeqDenseBatchData gruTgtOutput = (SeqDenseBatchData)cg.AddRunner(new GRUSparseRunner<SeqSparseBatchData>(gruModel.GRUCells[0], dssmData.TgtInput,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

                    for (int i = 1; i < gruModel.GRUCells.Count; i++)
                        gruSrcOutput = (SeqDenseBatchData)cg.AddRunner(new GRUDenseRunner<SeqDenseBatchData>(gruModel.GRUCells[i], gruTgtOutput,
                            new RunnerBehavior() { RunMode = mode, Device = device }));

                    HiddenBatchData gruTgtPoolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(gruTgtOutput,
                            new RunnerBehavior() { RunMode = mode, Device = device }));

                    tgtSharedRunner = gruSrcPoolingOutput;
                    if (mode == DNNRunMode.Train && MULTITASK_SEQPOOLParameters.POOL_DROPOUT > 0 && MULTITASK_SEQPOOLParameters.POOL_DROPOUT < 1)
                    {
                        tgtSharedRunner = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(tgtSharedRunner, MULTITASK_SEQPOOLParameters.POOL_DROPOUT,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                    }

                    break;
                case SharedNetType.CNNNet:
                    srcSharedRunner = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(sharedModel as DNNStructure, dssmData.SrcInput,
                       new RunnerBehavior() { RunMode = mode, Device = device }));
                    tgtSharedRunner = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(sharedModel as DNNStructure, dssmData.TgtInput,
                       new RunnerBehavior() { RunMode = mode, Device = device }));
                    break;
                default:
                    LSTMStructure lstmModel = sharedModel as LSTMStructure;

                    //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dssmData.SrcInput, Behavior));

                    //src runer
                    SeqDenseRecursiveData lstmSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(lstmModel.LSTMCells[0], dssmData.SrcInput, false, Behavior));

                    for (int i = 1; i < lstmModel.LSTMCells.Count; i++)
                        lstmSrcOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], lstmSrcOutput,  Behavior));

                    HiddenBatchData lstmSrcPoolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmSrcOutput, lstmSrcOutput.MapForward, Behavior));

                    srcSharedRunner = lstmSrcPoolingOutput;
                    if (mode == DNNRunMode.Train && MULTITASK_SEQPOOLParameters.POOL_DROPOUT > 0 && MULTITASK_SEQPOOLParameters.POOL_DROPOUT < 1)
                    {
                        srcSharedRunner = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(srcSharedRunner, MULTITASK_SEQPOOLParameters.POOL_DROPOUT,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                    }

                    //SeqHelpData TgtHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dssmData.TgtInput, Behavior));

                    ///tgt
                    SeqDenseRecursiveData lstmTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(lstmModel.LSTMCells[0], dssmData.TgtInput, false, Behavior));

                    for (int i = 1; i < lstmModel.LSTMCells.Count; i++)
                        lstmTgtOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], lstmTgtOutput,  Behavior));

                    HiddenBatchData lstmTgtPoolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmTgtOutput, lstmTgtOutput.MapForward, Behavior));

                    tgtSharedRunner = lstmTgtPoolingOutput;
                    if (mode == DNNRunMode.Train && MULTITASK_SEQPOOLParameters.POOL_DROPOUT > 0 && MULTITASK_SEQPOOLParameters.POOL_DROPOUT < 1)
                    {
                        tgtSharedRunner = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(tgtSharedRunner, MULTITASK_SEQPOOLParameters.POOL_DROPOUT,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                    }
                    break;
            }


            HiddenBatchData srcOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(srcDNNModel, srcSharedRunner,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            HiddenBatchData tgtOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(tgtDNNModel, tgtSharedRunner,
                        new RunnerBehavior() { RunMode = mode, Device = device }));

            /*************** Sample Source-Target Match Data. ********************/
            //BiMatchBatchData matchData = new BiMatchBatchData(data.Stat.SrcStat.MAX_BATCHSIZE,
            //                    mode == DNNRunMode.Train ? MULTITASK_SEQPOOLParameters.NTrail : 0, device, false);

            IntArgument arg = new IntArgument("arg");
            cg.AddRunner(new HiddenDataBatchSizeRunner(srcOutput, arg, Behavior));

            BiMatchBatchData matchData = (BiMatchBatchData)cg.AddRunner(new RandomBiMatchRunner(data.Stat.SrcStat.MAX_BATCHSIZE,
                mode == DNNRunMode.Train ? MULTITASK_SEQPOOLParameters.NTrail : 0, arg, Behavior, false));

            /*************** similarity between source text and target text. ********************/
            HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(srcOutput, tgtOutput, matchData,
                                         MULTITASK_SEQPOOLParameters.SimiType, new RunnerBehavior() { RunMode = mode, Device = device }));
            switch (mode)
            {
                case DNNRunMode.Train:
                    switch (MULTITASK_SEQPOOLParameters.PairNNLossFunction)
                    {
                        case LossFunctionType.SampleSoftmax:
                            if (data.IsScore) cg.AddObjective(new MultiClassSoftmaxRunner(null, dssmData.ScoreInput, simiOutput.Output, simiOutput.Deriv, MULTITASK_SEQPOOLParameters.PAIRNNGamma,
                                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            else cg.AddObjective(new MultiClassSoftmaxRunner(null, simiOutput.Output, simiOutput.Deriv, MULTITASK_SEQPOOLParameters.PAIRNNGamma,
                                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                        case LossFunctionType.SampleLogisicalRegression:
                            if (data.IsScore) cg.AddObjective(new SampleCrossEntropyRunner(dssmData.ScoreInput,
                                                 simiOutput.Output, simiOutput.Deriv, MULTITASK_SEQPOOLParameters.PAIRNNGamma,
                                                 new RunnerBehavior() { RunMode = mode, Device = device }));
                            else cg.AddObjective(new CrossEntropyRunner(matchData.MatchInfo, simiOutput.Output, simiOutput.Deriv, MULTITASK_SEQPOOLParameters.PAIRNNGamma,
                                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    switch (MULTITASK_SEQPOOLParameters.PairNNEvaluation)
                    {
                        case EvaluationType.NDCG:
                            cg.AddRunner(new NDCGDiskDumpRunner(simiOutput.Output, scoreData, outputPath, metricOutputPath));
                            break;
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(dssmData.ScoreInput.Data, simiOutput.Output, outputPath, metricOutputPath));
                            break;
                        case EvaluationType.MSE:
                            cg.AddRunner(new RMSEDiskDumpRunner(dssmData.ScoreInput.Data, simiOutput.Output, MULTITASK_SEQPOOLParameters.PAIRNNGamma, true, outputPath));
                            break;
                    }
                    break;
            }

            return cg;
        }

        public ComputationGraph BuildComputationGraph(DataCashier<SeqSparseDataSource, SequenceDataStat> data, DNNRunMode mode, Structure sharedModel, DNNStructure dnnModel, 
            LossFunctionType lossFunction, EvaluationType eval, string outputPath, string metricOutputPath)
        {
            ComputationGraph cg = new ComputationGraph();

            DeviceType device = MathOperatorManager.Device(MULTITASK_SEQPOOLParameters.GPUID);
            CudaMathOperation lib = new CudaMathOperation(true, true);
            RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };

            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data, Behavior));

            HiddenBatchData sharedRunner = null;
            switch (MULTITASK_SEQPOOLParameters.SharedNNType)
            {
                case SharedNetType.GRUNet:
                    GRUStructure gruModel = sharedModel as GRUStructure;
                    SeqDenseBatchData gruOutput = (SeqDenseBatchData)cg.AddRunner(new GRUSparseRunner<SeqSparseBatchData>(gruModel.GRUCells[0], dataSource.SequenceData, Behavior));

                    for (int i = 1; i < gruModel.GRUCells.Count; i++)
                        gruOutput = (SeqDenseBatchData)cg.AddRunner(new GRUDenseRunner<SeqDenseBatchData>(gruModel.GRUCells[i], gruOutput,
                            new RunnerBehavior() { RunMode = mode, Device = device }));

                    HiddenBatchData grupoolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(gruOutput,
                            new RunnerBehavior() { RunMode = mode, Device = device }));

                    sharedRunner = grupoolingOutput;
                    if (mode == DNNRunMode.Train && MULTITASK_SEQPOOLParameters.POOL_DROPOUT > 0 && MULTITASK_SEQPOOLParameters.POOL_DROPOUT < 1)
                    {
                        sharedRunner = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(sharedRunner, MULTITASK_SEQPOOLParameters.POOL_DROPOUT,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                    }

                    break;
                case SharedNetType.CNNNet:
                    sharedRunner = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(sharedModel as DNNStructure, dataSource.SequenceData,
                       new RunnerBehavior() { RunMode = mode, Device = device }));
                    break;
                default:
                    LSTMStructure lstmModel = sharedModel as LSTMStructure;

                    //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dataSource.SequenceData, Behavior));

                    SeqDenseRecursiveData lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(lstmModel.LSTMCells[0], dataSource.SequenceData, false,  Behavior));

                    for (int i = 1; i < lstmModel.LSTMCells.Count; i++)
                        lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], lstmOutput, Behavior));

                    HiddenBatchData poolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(lstmOutput, lstmOutput.MapForward, Behavior));

                    sharedRunner = poolingOutput;
                    if (mode == DNNRunMode.Train && MULTITASK_SEQPOOLParameters.POOL_DROPOUT > 0 && MULTITASK_SEQPOOLParameters.POOL_DROPOUT < 1)
                    {
                        sharedRunner = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(poolingOutput, MULTITASK_SEQPOOLParameters.POOL_DROPOUT,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                    }
                    break;
            }

            HiddenBatchData labelOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(dnnModel, sharedRunner,
                    new RunnerBehavior() { RunMode = mode, Device = device }));

            switch (mode)
            {
                case DNNRunMode.Train:
                    switch (lossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, labelOutput.Output, labelOutput.Deriv, MULTITASK_SEQPOOLParameters.Gamma,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                        case LossFunctionType.MultiClassification:
                            cg.AddObjective(new MultiClassSoftmaxRunner(dataSource.InstanceLabel, labelOutput.Output, labelOutput.Deriv, MULTITASK_SEQPOOLParameters.Gamma,
                                new RunnerBehavior() { RunMode = mode, Device = device }));
                            break;
                    }

                    break;
                case DNNRunMode.Predict:
                    switch (eval)
                    {
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, labelOutput.Output, outputPath, metricOutputPath));
                            break;
                        case EvaluationType.ACCURACY:
                            cg.AddRunner(new AccuracyDiskDumpRunner(dataSource.InstanceLabel, labelOutput.Output, MULTITASK_SEQPOOLParameters.Gamma, outputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            //run lstm pooling runner.
            Logger.OpenLog(MULTITASK_SEQPOOLParameters.LogFile);

            MathOperatorManager.SetDevice(MULTITASK_SEQPOOLParameters.GPUID);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            Logger.WriteLog("Shared Neural Net Type: {0}.", MULTITASK_SEQPOOLParameters.SharedNNType.ToString());
            switch (MULTITASK_SEQPOOLParameters.RunMode)
            {
                case DNNRunMode.Train:
                    {
                        Structure shareModel = null;
                        switch (MULTITASK_SEQPOOLParameters.SharedNNType)
                        {

                            case SharedNetType.GRUNet:
                                shareModel = new GRUStructure(DataPanel.Train[0].Stat.FEATURE_DIM, MULTITASK_SEQPOOLParameters.LAYER_DIM);
                                shareModel.InitOptimizer(new StructureLearner()
                                {
                                    Optimizer = MULTITASK_SEQPOOLParameters.Optimizer,
                                    LearnRate = MULTITASK_SEQPOOLParameters.LearnRate,
                                    AdaBoost = MULTITASK_SEQPOOLParameters.AdaBoost,
                                    ClipDelta = MULTITASK_SEQPOOLParameters.DELTACLIP,
                                    ClipWeight = MULTITASK_SEQPOOLParameters.WEIGHTCLIP
                                });

                                break;
                            case SharedNetType.CNNNet:
                                shareModel = new DNNStructure(
                                            DataPanel.Train[0].Stat.FEATURE_DIM,
                                            MULTITASK_SEQPOOLParameters.LAYER_DIM,
                                            MULTITASK_SEQPOOLParameters.ACTIVATION,
                                            MULTITASK_SEQPOOLParameters.LAYER_BIAS,
                                            MULTITASK_SEQPOOLParameters.ARCH,
                                            MULTITASK_SEQPOOLParameters.WINDOW_SIZE,
                                            MULTITASK_SEQPOOLParameters.LAYER_DROPOUT);

                                shareModel.InitOptimizer(new StructureLearner()
                                                    {
                                                        LearnRate = MULTITASK_SEQPOOLParameters.LearnRate,
                                                        Optimizer = MULTITASK_SEQPOOLParameters.Optimizer,
                                                        AdaBoost = MULTITASK_SEQPOOLParameters.AdaBoost,
                                                        ShrinkLR = MULTITASK_SEQPOOLParameters.ShrinkLR,
                                                        BatchNum = DataPanel.Train[0].Stat.TotalBatchNumber,
                                                        EpochNum = MULTITASK_SEQPOOLParameters.Iteration
                                                    });
                                break;
                            default:
                                shareModel = new LSTMStructure(DataPanel.FeatureDim, MULTITASK_SEQPOOLParameters.LAYER_DIM);
                                shareModel.InitOptimizer(new StructureLearner()
                                {
                                    Optimizer = MULTITASK_SEQPOOLParameters.Optimizer,
                                    LearnRate = MULTITASK_SEQPOOLParameters.LearnRate,
                                    AdaBoost = MULTITASK_SEQPOOLParameters.AdaBoost,
                                    ClipDelta = MULTITASK_SEQPOOLParameters.DELTACLIP,
                                    ClipWeight = MULTITASK_SEQPOOLParameters.WEIGHTCLIP
                                });
                                break;
                        }

                        List<DNNStructure> dnnModels = new List<DNNStructure>();
                        if (!Directory.Exists(MULTITASK_SEQPOOLParameters.ScoreOutputDir)) Directory.CreateDirectory(MULTITASK_SEQPOOLParameters.ScoreOutputDir);

                        List<ComputationGraph> trainGraphs = new List<ComputationGraph>();
                        List<ComputationGraph> predGraphs = new List<ComputationGraph>();
                        for (int i = 0; i < MULTITASK_SEQPOOLParameters.TaskNum; i++)
                        {
                            DNNStructure dnnModel = new DNNStructure(MULTITASK_SEQPOOLParameters.LAYER_DIM.Last(),
                                                        MULTITASK_SEQPOOLParameters.NN_LAYER_DIM[i],
                                                        MULTITASK_SEQPOOLParameters.NN_ACTIVATION[i],
                                                        MULTITASK_SEQPOOLParameters.NN_LAYER_DROPOUT[i],
                                                        MULTITASK_SEQPOOLParameters.NN_LAYER_BIAS[i]);

                            dnnModel.InitOptimizer(new StructureLearner()
                            {
                                Optimizer = MULTITASK_SEQPOOLParameters.Optimizer,
                                LearnRate = MULTITASK_SEQPOOLParameters.LearnRate,
                                AdaBoost = MULTITASK_SEQPOOLParameters.AdaBoost,
                                ClipDelta = MULTITASK_SEQPOOLParameters.DELTACLIP,
                                ClipWeight = MULTITASK_SEQPOOLParameters.WEIGHTCLIP
                            });

                            string scorePath = Path.Combine(MULTITASK_SEQPOOLParameters.ScoreOutputDir, "Task_" + i + ".score");
                            string mertricPath = Path.Combine(MULTITASK_SEQPOOLParameters.ScoreOutputDir, "Task_" + i + ".metric");
                            ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train[i], DNNRunMode.Train, shareModel, dnnModel, MULTITASK_SEQPOOLParameters.LossFunctions[i], MULTITASK_SEQPOOLParameters.Evaluations[i], scorePath, mertricPath);
                            ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid[i], DNNRunMode.Predict, shareModel, dnnModel, MULTITASK_SEQPOOLParameters.LossFunctions[i], MULTITASK_SEQPOOLParameters.Evaluations[i], scorePath, mertricPath);
                            trainGraphs.Add(trainCG);
                            predGraphs.Add(predCG);
                            dnnModels.Add(dnnModel);
                        }

                        ///DSSM
                        DNNStructure srcModel = null;
                        DNNStructure tgtModel = null;

                        if (MULTITASK_SEQPOOLParameters.PairNN_Enable) 
                        {
                           srcModel = new DNNStructure(MULTITASK_SEQPOOLParameters.LAYER_DIM.Last(),
                                                        MULTITASK_SEQPOOLParameters.PairNN_LAYER_DIM,
                                                        MULTITASK_SEQPOOLParameters.PairNN_ACTIVATION,
                                                        MULTITASK_SEQPOOLParameters.PairNN_LAYER_DROPOUT,
                                                        MULTITASK_SEQPOOLParameters.PairNN_LAYER_BIAS);

                            srcModel.InitOptimizer(new StructureLearner()
                            {
                                Optimizer = MULTITASK_SEQPOOLParameters.Optimizer,
                                LearnRate = MULTITASK_SEQPOOLParameters.LearnRate,
                                AdaBoost = MULTITASK_SEQPOOLParameters.AdaBoost,
                                ClipDelta = MULTITASK_SEQPOOLParameters.DELTACLIP,
                                ClipWeight = MULTITASK_SEQPOOLParameters.WEIGHTCLIP
                            });

                            tgtModel = new DNNStructure(MULTITASK_SEQPOOLParameters.LAYER_DIM.Last(),
                                                        MULTITASK_SEQPOOLParameters.PairNN_LAYER_DIM,
                                                        MULTITASK_SEQPOOLParameters.PairNN_ACTIVATION,
                                                        MULTITASK_SEQPOOLParameters.PairNN_LAYER_DROPOUT,
                                                        MULTITASK_SEQPOOLParameters.PairNN_LAYER_BIAS);

                            tgtModel.InitOptimizer(new StructureLearner()
                            {
                                Optimizer = MULTITASK_SEQPOOLParameters.Optimizer,
                                LearnRate = MULTITASK_SEQPOOLParameters.LearnRate,
                                AdaBoost = MULTITASK_SEQPOOLParameters.AdaBoost,
                                ClipDelta = MULTITASK_SEQPOOLParameters.DELTACLIP,
                                ClipWeight = MULTITASK_SEQPOOLParameters.WEIGHTCLIP
                            });
                            string scorePath = Path.Combine(MULTITASK_SEQPOOLParameters.ScoreOutputDir, "RankTask.score");
                            string mertricPath = Path.Combine(MULTITASK_SEQPOOLParameters.ScoreOutputDir, "RankTask.metric");
                            ComputationGraph trainCG = BuildPairNNComputationGraph(DataPanel.DSSMTrain, DNNRunMode.Train, shareModel, srcModel, tgtModel, MULTITASK_SEQPOOLParameters.ScoreTrainData,scorePath, mertricPath);
                            ComputationGraph predCG =   BuildPairNNComputationGraph(DataPanel.DSSMValid, DNNRunMode.Predict, shareModel, srcModel, tgtModel, MULTITASK_SEQPOOLParameters.ScoreValidData,scorePath, mertricPath);
                            trainGraphs.Add(trainCG);
                            predGraphs.Add(predCG);
                        }

                        if (!Directory.Exists(MULTITASK_SEQPOOLParameters.ModelOutputPath)) Directory.CreateDirectory(MULTITASK_SEQPOOLParameters.ModelOutputPath);

                        
                        Random taskRandom = new Random(13);
                        int NumTask = MULTITASK_SEQPOOLParameters.TaskNum;
                        if (MULTITASK_SEQPOOLParameters.PairNN_Enable) 
                        {
                            NumTask += 1;
                        }
                        double[] validScore = new double[NumTask];
                        double[] bestValidScore = Enumerable.Range(0, NumTask).Select(i => double.MinValue).ToArray();
                        double[] bestValidIter = Enumerable.Range(0, NumTask).Select(i => -1.0).ToArray();
                        int[] taskIters = new int[NumTask];

                        for (int iter = 0; iter < MULTITASK_SEQPOOLParameters.Iteration; iter++)
                        {
                            int taskId = taskRandom.Next(0, NumTask);

                            if(taskIters[taskId] == 0)
                            {
                                trainGraphs[taskId].Init();
                            }
                            bool terminate = trainGraphs[taskId].Step();
                            taskIters[taskId]++;
                            double loss = trainGraphs[taskId].StepLoss; // trainGraphs[taskId].MiniBatchExecute(taskIters[taskId]++, taskId, iter);
                            if ((iter + 1) % MULTITASK_SEQPOOLParameters.PredictionLag == 0)
                            {
                                string sharedmodelName = string.Format("{0}\\shared_{1}th.model", MULTITASK_SEQPOOLParameters.ModelOutputPath, (iter + 1) / MULTITASK_SEQPOOLParameters.PredictionLag);
                                using (BinaryWriter writer = new BinaryWriter(new FileStream(sharedmodelName, FileMode.Create, FileAccess.Write)))
                                {
                                    writer.Write((int)MULTITASK_SEQPOOLParameters.SharedNNType);
                                    shareModel.Serialize(writer);
                                }
                                for (int t = 0; t < MULTITASK_SEQPOOLParameters.TaskNum; t++)
                                {
                                    string modelName = string.Format(@"{0}\\Task_{1}_{2}th.model", MULTITASK_SEQPOOLParameters.ModelOutputPath, t, (iter + 1) / MULTITASK_SEQPOOLParameters.PredictionLag);
                                    using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                                    {
                                        //shareModel.Serialize(writer);
                                        dnnModels[t].Serialize(writer);
                                    }

                                    validScore[t] = predGraphs[t].Execute();

                                    if (validScore[t] >= bestValidScore[t])
                                    {
                                        bestValidScore[t] = validScore[t];
                                        bestValidIter[t] = iter;
                                    }
                                    Logger.WriteLog("For task {0} Best Valid Score {1} at iteration {2}", t, bestValidScore[t], bestValidIter[t] + 1);
                                }

                                if (MULTITASK_SEQPOOLParameters.PairNN_Enable) 
                                {
                                    string srcModelName = string.Format(@"{0}\\RankTask_SRC_{1}th.model", MULTITASK_SEQPOOLParameters.ModelOutputPath, (iter + 1) / MULTITASK_SEQPOOLParameters.PredictionLag);
                                    string tgtModelName = string.Format(@"{0}\\RankTask_TGT_{1}th.model", MULTITASK_SEQPOOLParameters.ModelOutputPath, (iter + 1) / MULTITASK_SEQPOOLParameters.PredictionLag);
                                    using (BinaryWriter srcWriter = new BinaryWriter(new FileStream(srcModelName, FileMode.Create, FileAccess.Write)))
                                    using (BinaryWriter tgtWriter = new BinaryWriter(new FileStream(tgtModelName, FileMode.Create, FileAccess.Write)))
                                    {
                                        srcModel.Serialize(srcWriter);
                                        tgtModel.Serialize(tgtWriter);
                                    }

                                    validScore[MULTITASK_SEQPOOLParameters.TaskNum] = predGraphs[MULTITASK_SEQPOOLParameters.TaskNum].Execute();

                                    if (validScore[MULTITASK_SEQPOOLParameters.TaskNum] >= bestValidScore[MULTITASK_SEQPOOLParameters.TaskNum])
                                    {
                                        bestValidScore[MULTITASK_SEQPOOLParameters.TaskNum] = validScore[MULTITASK_SEQPOOLParameters.TaskNum];
                                        bestValidIter[MULTITASK_SEQPOOLParameters.TaskNum] = iter;
                                    }
                                    Logger.WriteLog("For task {0} Best Valid Score {1} at iteration {2}", MULTITASK_SEQPOOLParameters.TaskNum, bestValidScore[MULTITASK_SEQPOOLParameters.TaskNum], bestValidIter[MULTITASK_SEQPOOLParameters.TaskNum] + 1);
                                }
                            }
                        }
                        for (int t = 0; t < MULTITASK_SEQPOOLParameters.TaskNum; t++)
                        {
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore[t], bestValidIter[t]);
                        }
                        break;
                    }
                case DNNRunMode.Predict:
                    
                    break;
            }

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static List<DataCashier<SeqSparseDataSource, SequenceDataStat>> Train = new List<DataCashier<SeqSparseDataSource, SequenceDataStat>>();
            public static List<DataCashier<SeqSparseDataSource, SequenceDataStat>> Valid = new List<DataCashier<SeqSparseDataSource, SequenceDataStat>>();
            public static DSSMDataCashier DSSMTrain = null;
            public static DSSMDataCashier DSSMValid = null;
            public static int FeatureDim { set; get; }

            public static void Init()
            {
                int featureDim = 0;
                if (MULTITASK_SEQPOOLParameters.TaskNum > 0)
                {
                    for (int i = 0; i < MULTITASK_SEQPOOLParameters.TrainData.Length; i++)
                    {
                        Train.Add(new DataCashier<SeqSparseDataSource, SequenceDataStat>(MULTITASK_SEQPOOLParameters.TrainData[i]));
                        Train[i].InitThreadSafePipelineCashier(100, true);
                        if (featureDim < Train[i].Stat.FEATURE_DIM) 
                        {
                            featureDim = Train[i].Stat.FEATURE_DIM;
                        }
                    }

                    for (int i = 0; i < MULTITASK_SEQPOOLParameters.ValidData.Length; i++)
                    {
                        Valid.Add(new DataCashier<SeqSparseDataSource, SequenceDataStat>(MULTITASK_SEQPOOLParameters.ValidData[i]));
                        Valid[i].InitThreadSafePipelineCashier(100, false);
                    }
                }
                if (MULTITASK_SEQPOOLParameters.PairNN_Enable)
                {
                    //Ranking
                    if (MULTITASK_SEQPOOLParameters.RunMode == DNNRunMode.Train)
                    {
                        DSSMTrain = new DSSMDataCashier(MULTITASK_SEQPOOLParameters.QueryTrainData, MULTITASK_SEQPOOLParameters.DocTrainData, MULTITASK_SEQPOOLParameters.ScoreTrainData);
                        DSSMTrain.InitThreadSafePipelineCashier(100, true);
                        if (featureDim < DSSMTrain.Stat.SrcStat.FEATURE_DIM)
                        {
                            featureDim = DSSMTrain.Stat.SrcStat.FEATURE_DIM;
                        }
                        if (featureDim < DSSMTrain.Stat.TgtStat.FEATURE_DIM)
                        {
                            featureDim = DSSMTrain.Stat.TgtStat.FEATURE_DIM;
                        }
                    }
                    if (MULTITASK_SEQPOOLParameters.IsValidFile)
                    {
                        DSSMValid = new DSSMDataCashier(MULTITASK_SEQPOOLParameters.QueryValidData, MULTITASK_SEQPOOLParameters.DocValidData, MULTITASK_SEQPOOLParameters.ScoreValidData);
                        DSSMValid.InitThreadSafePipelineCashier(100, false);
                    }
                }

                FeatureDim = featureDim;
            }
        }
    }
}
