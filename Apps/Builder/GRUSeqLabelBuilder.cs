﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class GRUSeqLabelBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));

                Argument.Add("STACK", new ParameterArgument("5", "RNN Output Stack Length"));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));


                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model Path"));

            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }


            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }

            public static int STACK { get { return int.Parse(Argument["STACK"].Value); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return (Argument["SEED-MODEL"].Value); } }

            public static string ScoreOutputPath
            {
                get
                {
                    if (RunMode == DNNRunMode.Predict) return Argument["SCORE-PATH"].Value;
                    else return LogFile + ".tmp.score";
                }
            }
            public static string MetricOutputPath
            {
                get
                {
                    if (RunMode == DNNRunMode.Predict) return Argument["METRIC-PATH"].Value;
                    else return LogFile + ".tmp.metric";
                }
            }
        }

        public override BuilderType Type { get { return BuilderType.GRUSEQLABEL; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public ComputationGraph BuildComputationGraph(DataCashier<SeqSparseDataSource, SequenceDataStat> data, 
            RunnerBehavior behavior, GRUStructure seqModel, LayerStructure outputModel )
        {
            ComputationGraph cg = new ComputationGraph();
            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data, behavior));

            //SeqDenseBatchData gruOutput = (SeqDenseBatchData)cg.AddRunner(new GRUSparseRunner<SeqSparseBatchData>(seqModel.GRUCells[0], dataSource.SequenceData, behavior));
            //for (int i = 1; i < seqModel.GRUCells.Count; i++)
            //    gruOutput = (SeqDenseBatchData)cg.AddRunner(new GRUDenseRunner<SeqDenseBatchData>(seqModel.GRUCells[i], gruOutput, behavior));

            SeqDenseRecursiveData gruOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastGRUSparseRunner(cg, seqModel.GRUCells[0], null, dataSource.SequenceData, false, behavior));
            for(int i=1;i<seqModel.GRUCells.Count;i++)
                gruOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastGRUDenseRunner<SeqDenseRecursiveData>(seqModel.GRUCells[i], gruOutput, behavior));

            SeqDenseBatchData seqOutput = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(gruOutput, behavior));

            HiddenBatchData lastOutput = (HiddenBatchData)cg.AddRunner(new SeqLabelRunner<SeqDenseBatchData>(outputModel, BuilderParameters.STACK, seqOutput, behavior));

            switch (behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, lastOutput.Output, lastOutput.Deriv, 1, behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, lastOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
            }
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure DeepNet = new CompositeNNStructure();
            GRUStructure gruModel = null;
            LayerStructure outputModel = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                gruModel = new GRUStructure(DataPanel.Train.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM, device);
                outputModel = new LayerStructure((BuilderParameters.STACK + 1) * BuilderParameters.LAYER_DIM.Last(), 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device);
                DeepNet.AddLayer(gruModel);
                DeepNet.AddLayer(outputModel);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    DeepNet = new CompositeNNStructure(modelReader, device);
                    int link = 0;

                    gruModel = (GRUStructure)DeepNet.CompositeLinks[link++];
                    outputModel = (LayerStructure)DeepNet.CompositeLinks[link++];
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train,
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, gruModel, outputModel);

                    trainCG.InitOptimizer(DeepNet, OptimizerParameters.StructureOptimizer, 
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid,
                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, gruModel, outputModel);

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss / (DataPanel.Train.Stat.TotalBatchNumber + float.Epsilon));

                        if (BuilderParameters.IsValidFile)
                        {
                            predCG.Execute();
                            Tuple<List<string>, float> results = AUCEvaluationSet.EvaluateAUC(BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath);
                            Logger.WriteLog(string.Join("\n", results.Item1));
                        }
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\GRU.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()), FileMode.Create)))
                        {
                            DeepNet.Serialize(writer);
                        }
                    }
                    ////DataPanel.Train,
                    break;
                case DNNRunMode.Predict:
                    //GRULabelStructure validGRULabelModel = new GRULabelStructure(
                    //    new BinaryReader(new FileStream(BuilderParameters.ModelOutputPath, FileMode.Open, FileAccess.Read)), BuilderParameters.Device);
                    //GRUSeqLabelRunner<SeqSparseDataSource> predictor = new GRUSeqLabelRunner<SeqSparseDataSource>(validGRULabelModel, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device });

                    //OutputParameter oParam = new OutputParameter()
                    //{
                    //    OutputPath = BuilderParameters.ScoreOutputPath,
                    //};
                    //predictor.GenerateOutput(DataPanel.Valid.GetInstances(false), oParam);
                    break;
            }

        }


        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainData);
                    Train.InitThreadSafePipelineCashier(100, true);
                }
                if (BuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }

            internal static void DeInit()
            {
                if (null != Train) Train.Dispose(); Train = null;
                if (null != Valid) Valid.Dispose(); Valid = null;
            }
        }
    }
}