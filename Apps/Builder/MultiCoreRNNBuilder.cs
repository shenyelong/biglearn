﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class MultiCoreRNNBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.MSE).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("LAYER-LAG", new ParameterArgument("5", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));

                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.Regression).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model Path"));

                Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

                Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));
                Argument.Add("CHECK-GRAD", new ParameterArgument("0", "Check Gradient!"));

                Argument.Add("CROSS-DEVICES", new ParameterArgument(string.Empty, "Cross Device Path"));
                Argument.Add("OUTMODEL-PATH", new ParameterArgument(string.Empty, "Output Model Path"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DeviceType Device
            {
                get
                {
                    if (GPUID >= 0)
                    {
                        return DeviceType.GPU;
                    }
                    else if (GPUID == -1)
                    {
                        return DeviceType.CPU;
                    }
                    else
                    {
                        return DeviceType.CPU_FAST_VECTOR;
                    }
                }
            }

            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static int[] LAYER_LAG { get { return Argument["LAYER-LAG"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModelPath { get { return (Argument["SEED-MODEL"].Value); } }

            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }
            public static bool CheckGrad { get { return int.Parse(Argument["CHECK-GRAD"].Value) > 0; } }
        
            //Argument.Add("CROSS-DEVICES", new ParameterArgument(string.Empty, "Cross Device Path"));
            //Argument.Add("OUTMODEL-PATH", new ParameterArgument(string.Empty, "Output Model Path"));
            //Argument.Add("INMODEL-PATH", new ParameterArgument(string.Empty, "Input Model Path"));
            public static string OutModelPath { get { return Argument["OUTMODEL-PATH"].Value; } }
            public static string[] CrossDevices { get { return Argument["CROSS-DEVICES"].Value.Split(new char[]{';'},StringSplitOptions.RemoveEmptyEntries).ToArray(); } }
        }

        /// <summary>
        /// Let's construct the data flow.
        /// 
        /// Build the data graph.
        /// 
        /// RNNData trainData = new RNNData('data path'); data will be in disk; only meta info will be load; 
        /// 
        /// 
        /// </summary>
        class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (BuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidData);
                    if (BuilderParameters.Device == DeviceType.GPU) Valid.InitPipelineCashier(100, 10);
                    else Valid.InitPipelineCashier(100, 0);
                }

                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainData);

                    if (BuilderParameters.Device == DeviceType.GPU) Train.InitPipelineCashier(100, 10);
                    else Train.InitPipelineCashier(100, 0);
                }
            }

            internal static void DeInit()
            {
                if (null != Train) Train.Dispose(); Train = null;
                if (null != Valid) Valid.Dispose(); Valid = null;
            }
        }

        public override BuilderType Type { get { return BuilderType.MULTICORE_RNNSEQPRED; } }
        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            if (BuilderParameters.Device == DeviceType.GPU) Cudalib.CudaInit(BuilderParameters.GPUID);
        }

        public unsafe override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            //EvaluationParameter evalParameter = new EvaluationParameter()
            //{
            //    evalType = BuilderParameters.Evaluation,
            //    InputDataType = DataSourceID.SeqSparseDataSource,
            //    OutputDataType = DataSourceID.SeqDenseBatchData,
            //    Param = new BasicEvalParameter()
            //    {
            //        Gamma = BuilderParameters.Gamma,
            //        ScoreFile = BuilderParameters.ScoreOutputPath,
            //        MetricFile = BuilderParameters.MetricOutputPath
            //    }
            //};

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    //LossDerivParameter lossDerivParameter = new LossDerivParameter()
                    //{
                    //    InputDataType = DataSourceID.SeqSparseDataSource,
                    //    OutputDataType = DataSourceID.SeqDenseBatchData,
                    //    lossType = BuilderParameters.LossFunction,
                    //    Gamma = BuilderParameters.Gamma,
                    //};

                    RNNStructure rnnModel = new RNNStructure(DataPanel.Train.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM,
                                                            BuilderParameters.LAYER_LAG, BuilderParameters.ACTIVATION, BuilderParameters.LAYER_BIAS,
                                                            BuilderParameters.Device);

                    //DataPanel.Train,
                    RNNSeqPredRunner<SeqSparseDataSource> trainer = new RNNSeqPredRunner<SeqSparseDataSource>(rnnModel, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device });
                    //trainer.LossParameter = lossDerivParameter;

                    RNNSeqPredRunner<SeqSparseDataSource> evaler = null;
                    if (BuilderParameters.IsValidFile)
                    {
                        //DataPanel.Valid, 
                        evaler = new RNNSeqPredRunner<SeqSparseDataSource>(rnnModel, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = BuilderParameters.Device });
                        //evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                    }

                    rnnModel.InitOptimizer(new StructureLearner()
                    {
                        LearnRate = BuilderParameters.LearnRate,
                        Optimizer = BuilderParameters.Optimizer,
                        AdaBoost = BuilderParameters.AdaBoost,
                        ShrinkLR = BuilderParameters.ShrinkLR,
                        BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                        EpochNum = BuilderParameters.Iteration,
                        ClipDelta = BuilderParameters.DELTACLIP,
                        ClipWeight = BuilderParameters.WEIGHTCLIP,
                        device = BuilderParameters.Device
                    });

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    //if (BuilderParameters.CheckGrad)
                    //{
                    //    if (trainer.GradientCheck(DataPanel.Train.GetInstances(false)))
                    //        Logger.WriteLog("Gradient Check Successfully!");
                    //    else
                    //        Logger.WriteLog("Gradient Check Failed!");
                    //}

                    OutModel OutModelCache = new OutModel(trainer.Model.ParamNumber, 10, BuilderParameters.OutModelPath);
                    InModel InModelCache = new InModel(trainer.Model.ParamNumber, 10, BuilderParameters.CrossDevices.ToList());

                    for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
                    {
                        ///support mode IN-OUT;
                        double loss = 0; // MultiCoreTrain(DataPanel.Train.GetInstances(true, new Random((int)DateTime.Now.TimeOfDay.TotalSeconds)), OutModelCache, InModelCache);

                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss / (DataPanel.Train.Stat.TotalBatchNumber + float.Epsilon));
                        //if (BuilderParameters.IsValidFile) evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
                        rnnModel.Save(string.Format(@"{0}\\RNN.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()));
                    }
                    OutModelCache.IsCompleted = true;
                    InModelCache.IsCompleted = true;
                    trainer.Dispose();
                    evaler.Dispose();
                    break;
                case DNNRunMode.Predict:
                    //RNNStructure rnn_pred_Model = new RNNStructure(new BinaryReader(new FileStream(BuilderParameters.SeedModelPath, FileMode.Open, FileAccess.Read)), BuilderParameters.Device);
                    //RNNSeqPredRunner<SeqSparseDataSource> predictor = new RNNSeqPredRunner<SeqSparseDataSource>(rnn_pred_Model, new RunnerBehavior() { RunMode = DNNRunMode.Predict });
                    //if (BuilderParameters.Evaluation == EvaluationType.VECTOROUTPUT) predictor.ExtractEmbedding(DataPanel.Valid.GetInstances(false), evalParameter);
                    //else predictor.ExtractFinalLabel(DataPanel.Valid.GetInstances(false), evalParameter);
                    //predictor.Dispose();
                    break;
            }

        }

        /// <summary>
        /// Train Data with Model Communication. 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public double MultiCoreTrain<IN> (ComputationGraph cg, Structure Model, IEnumerable<IN> data, OutModel outModel, InModel inModel, RunnerBehavior behavior) where IN : BatchData
        {
            DateTime pre = DateTime.Now;

            double loss = 0;
            int batchIdx = 0;
            var trainingCounter = PerfCounter.Manager.Instance["Training"].Begin();
            var enumerateCounter = PerfCounter.Manager.Instance["EnumerateSample"].Begin();
            var trainingOneSampleCounter = PerfCounter.Manager.Instance["TrainingOneSample"].Begin();
            foreach (IN sample in data)
            {
                PerfCounter.Manager.Instance["EnumerateSample"].TakeCount(enumerateCounter);

                //TODO: set input to CG;
                //runner.Input = sample;
                trainingOneSampleCounter.Restart();

                cg.Step();

                PerfCounter.Manager.Instance["TrainingOneSample"].TakeCount(trainingOneSampleCounter);

                /// save model every 50 minibatches.
                if (++batchIdx % 20 == 0)
                {
                    Logger.WriteLog("Save Model : Train Batch Num {0}, AvgLoss {1}", batchIdx, loss / batchIdx);
                    int writeIdx;
                    CudaPieceFloat model = outModel.GetWriteCache(out writeIdx);
                    Model.GetModelParameter(model, 0);
                    outModel.WriteCacheDone(writeIdx);

                    int readIdx;
                    if (inModel.ReadModel(out readIdx))
                    {
                        Logger.WriteLog("Aggragate Model.");
                        Model.AggragateModelParameter(behavior.Computelib, inModel.ModelParameter(readIdx), 0, 0.7f, 0.3f);
                        inModel.ReadModelDone(readIdx);
                    }

                    // Save the model to "model.dump". 
                    // Program.SaveAsync("model.out.dump", Model.GetGlobalParameter();
                    // Model.SetGlobalParameter().
                    // Load model from "model.in.dump, 
                }


                enumerateCounter.Restart();
            }

            Logger.WriteLog("Train Loss:{0}, Batch:{1}, Time cost: {2}s", loss / batchIdx, batchIdx, (DateTime.Now - pre).TotalSeconds);
            PerfCounter.Manager.Instance["Training"].TakeCount(trainingCounter);
            return loss;
        }

        /// <summary>
        /// Model Communication Outbox.
        /// </summary>
        public class OutModel
        {
            List<CudaPieceFloat> CacheModelParameter = new List<CudaPieceFloat>();
            public bool IsCompleted { get; set; }
            BlockingCollection<int> WriteCache = null; // new BlockingCollection<int>(4);
            BlockingCollection<int> ReadCache = null; // new BlockingCollection<int>(4);

            /// <summary>
            /// Construction Function for IO-Model;
            /// </summary>
            /// <param name="parameterNum">Number of Parameters.</param>
            /// <param name="cacheNum">Number of Caches.</param>
            public OutModel(int parameterNum, int cacheNum, string outPath)
            {
                IsCompleted = false;
                WriteCache = new BlockingCollection<int>(cacheNum);
                ReadCache = new BlockingCollection<int>(cacheNum);

                for (int i = 0; i < cacheNum; i++)
                {
                    CacheModelParameter.Add(new CudaPieceFloat(parameterNum, true, true));
                    WriteCache.Add(i);
                }

                var consumerTask = Task.Run(() =>
                {
                    while (!IsCompleted)
                    {
                        int readIdx = ReadCache.Take();
                        /// save memory to path;
                        ToIO(readIdx, outPath);
                        WriteCache.Add(readIdx);
                    }
                });
            }

            public void ToIO(int idx, string path)
            {
                for (int k = 0; k < CacheModelParameter.Count; k++)
                {
                    try
                    {
                        using (FileStream stream = new FileStream(path + "." + (idx + k).ToString(), FileMode.Create, FileAccess.Write, FileShare.None))
                        {
                            using (BinaryWriter writer = new BinaryWriter(stream))
                            {
                                CacheModelParameter[idx].CopyOutFromCuda();
                                for (int i = 0; i < CacheModelParameter[idx].Size; i++)
                                {
                                    writer.Write(CacheModelParameter[idx].MemPtr[i]);
                                }
                                break;
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }


            /// <summary>
            /// Get the write cache.
            /// </summary>
            /// <returns></returns>
            public CudaPieceFloat GetWriteCache(out int writeIdx)
            {
                writeIdx = WriteCache.Take();
                return CacheModelParameter[writeIdx];
            }

            public void WriteCacheDone(int writeIdx)
            {
                ReadCache.Add(writeIdx);
                if (ReadCache.Count > 1)
                {
                    int readIdx;
                    if (ReadCache.TryTake(out readIdx))
                    {
                        WriteCache.Add(readIdx);
                    }
                }
            }
        }

        /// <summary>
        /// Model Communication Inbox.
        /// </summary>
        public class InModel
        {
            List<CudaPieceFloat> CacheModelParameter = new List<CudaPieceFloat>();
            public bool IsCompleted { get; set; }
            BlockingCollection<int> WriteCache = null; // new BlockingCollection<int>(4);
            BlockingCollection<int> ReadCache = null; // new BlockingCollection<int>(4);

            public CudaPieceFloat ModelParameter(int idx) { return CacheModelParameter[idx]; }

            public InModel(int parameterNum, int cacheNum, List<string> neighborPath)
            {
                IsCompleted = false;
                WriteCache = new BlockingCollection<int>(cacheNum);
                ReadCache = new BlockingCollection<int>(cacheNum);

                for (int i = 0; i < cacheNum; i++)
                {
                    CacheModelParameter.Add(new CudaPieceFloat(parameterNum, true, true));
                    WriteCache.Add(i);
                }

                var producerTask = Task.Run(() =>
                {
                    while (!IsCompleted)
                    {
                        /// check the model available from neighbors.

                        /// prepare to write the "idx"s block.
                        int writeIdx = WriteCache.Take();

                        int cacheAvailableNum = 0;
                        Array.Clear(CacheModelParameter[writeIdx].MemPtr, 0, CacheModelParameter[writeIdx].MemPtr.Length);
                        foreach (string path in neighborPath)
                        {
                            if (InIO(writeIdx, path))
                            {
                                cacheAvailableNum += 1;
                            }
                        }
                        if (cacheAvailableNum > neighborPath.Count / 2)
                        {
                            //FastVector v2 = FastVector.Create(CacheModelParameter[writeIdx].MemPtr);
                            //v2.Scale(1.0f / cacheAvailableNum);
                            FastVector.Scale(CacheModelParameter[writeIdx].MemPtr, 1.0f / cacheAvailableNum, CacheModelParameter[writeIdx].Size);


                            CacheModelParameter[writeIdx].SyncFromCPU();
                            ReadCache.Add(writeIdx);

                            if (ReadCache.Count > 1)
                            {
                                int readIdx;
                                if (ReadCache.TryTake(out readIdx))
                                {
                                    WriteCache.Add(readIdx);
                                }
                            }
                        }
                        else
                        {
                            WriteCache.Add(writeIdx);
                        }
                        //int readIdx = ReadCache.Take();
                        ///// save memory to path;
                        //ToIO(readIdx, outPath);
                        //WriteCache.Add(readIdx);
                    }
                });

                //WriteCache = new BlockingCollection<int>(cacheNum);
                //ReadCache = new BlockingCollection<int>(cacheNum);
                //for (int i = 0; i < cacheNum; i++)
                //{
                //    CacheModelParameter.Add(new CudaPieceFloat(parameterNum, true, true));
                //    WriteCache.Add(i);
                //}
            }

            public unsafe bool InIO(int idx, string path)
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                /// get the latest model file.
                FileInfo[] files = dir.GetFiles().OrderByDescending(p => p.LastWriteTime).ToArray();
                for (int k = 0; k < files.Length; k++)
                {
                    try
                    {
                        string fileName = files[k].FullName;
                        using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            using (BinaryReader reader = new BinaryReader(stream))
                            {
                                float[] cpuMemory = new float[CacheModelParameter[idx].Size];
                                Buffer.BlockCopy(reader.ReadBytes(sizeof(float) * CacheModelParameter[idx].Size), 0, cpuMemory, 0, sizeof(float) * CacheModelParameter[idx].Size);

                                //FastVector v1 = FastVector.Create(cpuMemory);
                                //FastVector v2 = FastVector.Create(CacheModelParameter[idx].MemPtr);
                                //v2.Add(v1);

                                fixed( float * pCpuM = &cpuMemory[0])
                                {
                                    FastVector.Add_Vector(CacheModelParameter[idx].CpuPtr, 0, pCpuM, 0, 
                                        CacheModelParameter[idx].Size, 1, 1);
                                }
                                return true;
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
                return false;
            }

            public bool ReadModel(out int idx)
            {
                return ReadCache.TryTake(out idx);
            }

            public void ReadModelDone(int idx)
            {
                WriteCache.Add(idx);
            }
        }

    }

}
