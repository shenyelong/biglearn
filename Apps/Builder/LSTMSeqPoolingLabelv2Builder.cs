﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// LSTM + Sequence Pooling + Sequence Label.
    /// </summary>
    public class LSTMSeqPoolingLabelV2Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("TRAIN-RAW", new ParameterArgument(string.Empty, "Train Raw Data."));
                Argument.Add("VALID-RAW", new ParameterArgument(string.Empty, "Valid Raw Data."));

                Argument.Add("EMBED-DIM", new ParameterArgument("100", "Embed Dim"));
                Argument.Add("LAYER-DIM", new ParameterArgument("1", "LSTM Layer Dim"));
                Argument.Add("POOL-DROPOUT", new ParameterArgument("0", "LSTM Max-Pooling Dropout."));

                Argument.Add("BI-DIRECTION", new ParameterArgument("1", "0 : One Directional LSTM; 1 : Bi Directional LSTM"));
                Argument.Add("POOLING", new ParameterArgument(@"1", "Pooling Type;  0 : Max Pooling; 1 : Last State Pooling."));

                Argument.Add("NN-ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("NN-LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("NN-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("NN-LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

                Argument.Add("MINI-BATCH", new ParameterArgument("64", " Mini Batch Size."));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.BinaryClassification).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
                Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument(string.Empty, "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static bool IsTrainFile { get { return !TrainData.Equals(string.Empty); } }
            public static string TrainRawData { get { return Argument["TRAIN-RAW"].Value; } }

            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
            public static string ValidRawData { get { return Argument["VALID-RAW"].Value; } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static int EMBED_DIM { get { return int.Parse(Argument["EMBED-DIM"].Value); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float POOL_DROPOUT { get { return float.Parse(Argument["POOL-DROPOUT"].Value); } }
            public static int[] NN_LAYER_DIM { get { return Argument["NN-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] NN_ACTIVATION { get { return Argument["NN-ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] NN_LAYER_BIAS { get { return Argument["NN-LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] NN_LAYER_DROPOUT { get { return Argument["NN-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static int MiniBatch { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static bool IS_BILSTM { get { return int.Parse(Argument["BI-DIRECTION"].Value) > 0; } }
            public static PoolingType PoolingType { get { return (PoolingType)int.Parse(Argument["POOLING"].Value); } }

            public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
            public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

            public static string ScoreOutputPath
            {
                get
                {
                    if (!Argument["SCORE-PATH"].Value.Equals(string.Empty)) return Argument["SCORE-PATH"].Value;
                    else return LogFile + ".tmp.score";
                }
            }

            public static string MetricOutputPath
            {
                get
                {
                    if (!Argument["METRIC-PATH"].Value.Equals(string.Empty)) return Argument["METRIC-PATH"].Value;
                    else return LogFile + ".tmp.metric";
                }
            }

            public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
            public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
            public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
            public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }
        }

        public override BuilderType Type { get { return BuilderType.LSTMSEQPOOL_V2; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public ComputationGraph BuildComputationGraph(DataCashier<SeqSparseDataSource, SequenceDataStat> data,
            DNNRunMode mode, LayerStructure embed, LSTMStructure lstmModelD1, LSTMStructure lstmModelD2, DNNStructure dnnModel, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data, Behavior));

            // SeqHelpData seqTrans = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dataSource.SequenceData,
            //     new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib }));

            SeqDenseBatchData embedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(embed, dataSource.SequenceData, Behavior));

            SeqDenseRecursiveData D1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(embedOutput, false, Behavior));
            SeqDenseRecursiveData D1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModelD1.LSTMCells[0], D1LstmInput, Behavior));
            for (int i = 1; i < lstmModelD1.LSTMCells.Count; i++)
                D1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModelD1.LSTMCells[i], D1LstmOutput, Behavior));

            SeqDenseRecursiveData D2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(embedOutput, true, Behavior));
            SeqDenseRecursiveData D2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModelD2.LSTMCells[0], D2LstmInput, Behavior));
            for (int i = 1; i < lstmModelD2.LSTMCells.Count; i++)
                D2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModelD2.LSTMCells[i], D2LstmOutput, Behavior));

            HiddenBatchData poolingOutput = null;
            if (BuilderParameters.PoolingType == PoolingType.LAST)
            {
                HiddenBatchData QueryD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(D1LstmOutput, true, 0, D1LstmOutput.MapForward, Behavior));
                HiddenBatchData QueryD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(D2LstmOutput, false, 0, D2LstmOutput.MapForward, Behavior));
                poolingOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { QueryD1O, QueryD2O }, Behavior));
                Logger.WriteLog("Query Pool Type {0}", PoolingType.LAST);
            }
            else if (BuilderParameters.PoolingType == PoolingType.MAX)
            {
                SeqDenseBatchData QuerySeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(D1LstmOutput, Behavior));
                SeqDenseBatchData QuerySeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(D2LstmOutput, Behavior));

                SeqDenseBatchData QuerySeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QuerySeqD1O, QuerySeqD2O }, Behavior));
                poolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QuerySeqO, Behavior));
                Logger.WriteLog("Query Pool Type {0}", PoolingType.MAX);
            }


            //SeqDenseRecursiveData lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(lstmModel.LSTMCells[0], dataSource.SequenceData, false, Behavior));
            //for (int i = 1; i < lstmModel.LSTMCells.Count; i++)
            //    lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], lstmOutput, Behavior));
            //SeqDenseBatchData outputData = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(lstmOutput, Behavior));
            //HiddenBatchData poolingOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(outputData,  Behavior));

            HiddenBatchData dropOutput = poolingOutput;
            if (mode == DNNRunMode.Train && BuilderParameters.POOL_DROPOUT > 0 && BuilderParameters.POOL_DROPOUT < 1)
            {
                dropOutput = (HiddenBatchData)cg.AddRunner(new DropOutProcessor<HiddenBatchData>(poolingOutput, BuilderParameters.POOL_DROPOUT, Behavior));
            }

            HiddenBatchData labelOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(dnnModel, dropOutput, Behavior));

            switch (mode)
            {
                case DNNRunMode.Train:
                    switch (BuilderParameters.LossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, labelOutput.Output, labelOutput.Deriv, BuilderParameters.Gamma, Behavior));
                            break;
                        case LossFunctionType.MultiClassification:
                            cg.AddObjective(new MultiClassSoftmaxRunner(dataSource.InstanceLabel, labelOutput.Output, labelOutput.Deriv, BuilderParameters.Gamma, Behavior));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    switch (BuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, labelOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                            break;
                        case EvaluationType.ACCURACY:
                            cg.AddRunner(new AccuracyDiskDumpRunner(dataSource.InstanceLabel, labelOutput.Output, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            //run lstm pooling runner.
            Logger.OpenLog(BuilderParameters.LogFile);

            //MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            LayerStructure embedLayer = null;

            //context cnn layer.
            LSTMStructure D1LSTM = null;
            LSTMStructure D2LSTM = null;

            //reason net structure.
            DNNStructure dnnModel = null;

            int embedDim = DataPanel.WordDim;
            embedLayer = (new LayerStructure(DataPanel.WordDim, BuilderParameters.EMBED_DIM, A_Func.Linear, N_Type.Convolution_layer, 1, 0, false, device));
            embedDim = BuilderParameters.EMBED_DIM;

            D1LSTM = new LSTMStructure(embedDim, BuilderParameters.LAYER_DIM, device, RndRecurrentInit.RndOrthogonal);
            D2LSTM = new LSTMStructure(embedDim, BuilderParameters.LAYER_DIM, device, RndRecurrentInit.RndOrthogonal);
            embedDim = 2 * BuilderParameters.LAYER_DIM.Last();
            dnnModel = new DNNStructure(embedDim,
                                                BuilderParameters.NN_LAYER_DIM,
                                                BuilderParameters.NN_ACTIVATION,
                                                BuilderParameters.NN_LAYER_DROPOUT,
                                                BuilderParameters.NN_LAYER_BIAS);

            modelStructure.AddLayer(embedLayer);
            modelStructure.AddLayer(D1LSTM);
            modelStructure.AddLayer(D2LSTM);
            modelStructure.AddLayer(dnnModel);

            /// construct single source computational graph.
            ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, DNNRunMode.Train, embedLayer, D1LSTM, D2LSTM, dnnModel,
                new RunnerBehavior() { Device = device, RunMode = DNNRunMode.Train, Computelib = computeLib });
            trainCG.InitOptimizer(modelStructure, new StructureLearner()
            {
                Optimizer = BuilderParameters.Optimizer,
                LearnRate = BuilderParameters.LearnRate,
                AdaBoost = BuilderParameters.AdaBoost,
                ClipDelta = BuilderParameters.DELTACLIP,
                ClipWeight = BuilderParameters.WEIGHTCLIP
            }, new RunnerBehavior() { Device = device, RunMode = DNNRunMode.Train, Computelib = computeLib });

            ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, embedLayer, D1LSTM, D2LSTM, dnnModel,
                new RunnerBehavior() { Device = device, RunMode = DNNRunMode.Predict, Computelib = computeLib });

            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            double validScore = 0;
            double bestValidScore = double.MinValue;
            double bestValidIter = -1;

            for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
            {
                double loss = trainCG.Execute();
                Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);


                if (BuilderParameters.IsValidFile)
                {
                    validScore = predCG.Execute();
                    if (validScore >= bestValidScore)
                    {
                        bestValidScore = validScore;
                        bestValidIter = iter;
                    }
                    Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                }
            }
            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            static BasicTextHash TextHash = null;
            public static int WordDim { get { return TextHash.VocabSize; } }

            public static void Init()
            {
                if((BuilderParameters.IsTrainFile && !File.Exists(BuilderParameters.TrainData))  || (!File.Exists(BuilderParameters.TrainData + ".vocab")))
                {
                    string[] lines = File.ReadAllLines(BuilderParameters.TrainRawData);

                    if (!File.Exists(BuilderParameters.TrainData + ".vocab"))
                    {
                        TextHash = new WordTextHash(new SimpleTextTokenizer(new string[] { " " }, null));
                        foreach(string text in lines.Select( i => i.Split('\t')[0]))
                        {
                            TextHash.VocabExtract(text);
                        }
                        using (StreamWriter mwriter = new StreamWriter(BuilderParameters.TrainData + ".vocab"))
                        {
                            TextHash.Serialize(mwriter);
                        }
                    }
                    else { TextHash = new WordTextHash(BuilderParameters.TrainData + ".vocab", new SimpleTextTokenizer(new string[] { " " }, null)); }

                    BinaryWriter trainWriter = new BinaryWriter(FileUtil.CreateWriteFS(BuilderParameters.TrainData));
                    SeqSparseDataSource train = new SeqSparseDataSource(new SequenceDataStat() { FEATURE_DIM = TextHash.VocabSize, MAX_BATCHSIZE = BuilderParameters.MiniBatch }, DeviceType.CPU);
                    foreach (string[] items in lines.Select(i => i.Split('\t')))
                    {
                        List<Dictionary<int, float>> fea = TextHash.SeqFeatureExtract(items[0]);
                        train.PushSample(fea.Select(i => new Tuple<Dictionary<int, float>, float>(i, float.Parse(items[1]))).ToList());
                        if (train.BatchSize >= BuilderParameters.MiniBatch) { train.PopBatchToStat(trainWriter); }
                    }
                    train.PopBatchCompleteStat(trainWriter);
                }

                if(BuilderParameters.IsValidFile && !File.Exists(BuilderParameters.ValidData))
                {
                    string[] lines = File.ReadAllLines(BuilderParameters.ValidRawData);

                    BinaryWriter validWriter = new BinaryWriter(FileUtil.CreateWriteFS(BuilderParameters.ValidData));
                    SeqSparseDataSource valid = new SeqSparseDataSource(new SequenceDataStat() { FEATURE_DIM = TextHash.VocabSize, MAX_BATCHSIZE = BuilderParameters.MiniBatch }, DeviceType.CPU);
                    foreach (string[] items in lines.Select(i => i.Split('\t')))
                    {
                        List<Dictionary<int, float>> fea = TextHash.SeqFeatureExtract(items[0]);
                        valid.PushSample(fea.Select(i => new Tuple<Dictionary<int, float>, float>(i, float.Parse(items[1]))).ToList());
                        if (valid.BatchSize >= BuilderParameters.MiniBatch) { valid.PopBatchToStat(validWriter); }
                    }
                    valid.PopBatchCompleteStat(validWriter);
                }

                if (BuilderParameters.IsTrainFile)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainData);
                    Train.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsValidFile)
                {
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }

            internal static void DeInit()
            {
                if (null != Train) Train.Dispose(); Train = null;
                if (null != Valid) Valid.Dispose(); Valid = null;
            }
        }
    }
}
