﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using BigLearn;

namespace BigLearn.DeepNet
{
    

    //public class BpLdaDNNBuilderParameters : BaseModelArgument<BpLdaDNNBuilderParameters>
    //{
    //    public static string TrainData { get { return Argument["TRAIN"].Value; } }
    //    public static string ValidData { get { return Argument["VALID"].Value; } }
    //    public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
    //    public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

    //    public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
    //    public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
    //    public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

    //    public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
    //    public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

    //    /// <summary>
    //    /// /////////////////////////////////////
    //    /// </summary>
    //    public static int HIDDEN_DIM { get { return int.Parse(Argument["HIDDEN-DIM"].Value); } }
    //    public static int OUTPUT_DIM { get { return int.Parse(Argument["OUTPUT-DIM"].Value); } }
    //    public static float GAMMA { get { return float.Parse(Argument["GAMMA"].Value); } }
    //    public static float ETA { get { return float.Parse(Argument["ETA"].Value); } }
    //    public static float T_VALUE { get { return float.Parse(Argument["T-VALUE"].Value); } }
    //    public static float ALPHA { get { return float.Parse(Argument["ALPHA"].Value); } }
    //    public static float BETA { get { return float.Parse(Argument["BETA"].Value); } }
    //    public static int LAYER_DIM { get { return int.Parse(Argument["LAYER-DIM"].Value); } }
    //    public static float MU_U { get { return float.Parse(Argument["MU_U"].Value); } }
    //    public static float MU_PHI { get { return float.Parse(Argument["MU_PHI"].Value); } }
    //    public static int SEED { get { return int.Parse(Argument["SEED"].Value); } }
    //    public static int USE_ADAPTIVE { get { return int.Parse(Argument["USE_ADAPTIVE"].Value); } }


    //    public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
    //    public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
    //    public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

    //    public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

    //    public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }

    //    public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }

    //    public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }

    //    public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }

    //    public static float LabelMinValue { get { return float.Parse(Argument["LABEL-MIN"].Value); } }
    //    public static float LabelMaxValue { get { return float.Parse(Argument["LABEL-MAX"].Value); } }
    //    /// <summary>
    //    /// Only need to Specify Data and Archecture of Deep Nets.
    //    /// </summary>
    //    static BpLdaDNNBuilderParameters()
    //    {
    //        Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));
    //        Argument.Add("HIDDEN-DIM", new ParameterArgument("5", "Hidden Number nodes per layer/iteration of LDA"));
    //        Argument.Add("OUTPUT-DIM", new ParameterArgument("1", "OUTPUT Number of LDA"));
    //        Argument.Add("GAMMA", new ParameterArgument("1.0f", "Gamma LDA"));
    //        Argument.Add("ETA", new ParameterArgument("0.5f", "ETA of LDA"));
    //        Argument.Add("T-VALUE", new ParameterArgument("1.0f", "TValue of LDA"));
    //        Argument.Add("ALPHA", new ParameterArgument("1.001f", "parameter"));
    //        Argument.Add("BETA", new ParameterArgument("1.0001f", "parameter"));
    //        Argument.Add("LAYER-DIM", new ParameterArgument("10", "Number of layer/iterations"));
    //        Argument.Add("MU_U", new ParameterArgument("1.0001f", "parameter"));
    //        Argument.Add("MU_PHI", new ParameterArgument("1.0001f", "parameter"));
    //        Argument.Add("SEED", new ParameterArgument("13", "parameter"));
    //        Argument.Add("USE_ADAPTIVE", new ParameterArgument("1", "parameter"));

    //        Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
    //        Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
    //        Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

    //        Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
    //        Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
    //        Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
    //        Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
    //        Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

    //        Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.Regression).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
    //        Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.MSE).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));


    //        Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
    //        Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
    //        Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

    //        Argument.Add("MODEL-PATH", new ParameterArgument(@"\\DEEP-06\Test-JDSSM\Model_Path\", "Model Path"));
    //        Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
    //        Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
    //        Argument.Add("LABEL-MIN", new ParameterArgument("1", "Label Start with 1 by default;"));
    //        Argument.Add("LABEL-MAX", new ParameterArgument("5", "Label End with 5 by default;"));
    //    }
    //}

    //public class TrainResult
    //{
    //    private int _iteration;
    //    private float _averageLoss;
    //    private float _score;
    //    private string _path;

    //    public TrainResult(int iteration, float averageLoss, float score, string modelPath)
    //    {
    //        _iteration = iteration;
    //        _averageLoss = averageLoss;
    //        _score = score;
    //        _path = modelPath;
    //    }

    //    public int Iteration { get { return _iteration; } }
    //    public float AverageLoss { get { return _averageLoss; } }
    //    public float Score { get { return _score; } }
    //    public string Path { get { return _path; } }
    //}

    //public enum BpLdaDNNModelVersion
    //{
    //    /// <summary>
    //    /// Very first version of CRM 11_4
    //    /// </summary>
    //    BP_S_LDA_GPU_V1_2015_NOV = 0,
    //}

    ///// <summary>
    ///// This class defines and initializes the model parameters of BP-sLDA
    ///// </summary>
    //public sealed class BpLdaDNNStructure : Structure
    //{
    //    public CudaPieceFloat Phi;
    //    public CudaPieceFloat U;
    //    public CudaPieceFloat b; // number of hidden
    //    public float Beta;
    //    public float gamma;

    //    public int InputDim;
    //    public int HiddenDim;
    //    public int OutputDim;
    //    public int LayerDim;

    //    ~BpLdaDNNStructure()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (this.disposed)
    //        {
    //            return;
    //        }

    //        this.disposed = true;

    //        if (disposing)
    //        {
    //            Phi.Dispose();
    //            U.Dispose();
    //            b.Dispose();
    //        }

    //        base.Dispose(disposing);
    //    }

    //    public BpLdaDNNStructure(int featureSize,
    //        int hiddenDim,
    //        int outputDim,
    //        int layerDim,
    //        float alpha,
    //        float beta,
    //        DeviceType device)
    //    {
    //        InputDim = featureSize;
    //        HiddenDim = hiddenDim;
    //        OutputDim = outputDim;
    //        LayerDim = layerDim;

    //        Phi = new CudaPieceFloat(InputDim * HiddenDim, true, true);

    //        Phi.Init(1, 1);
    //        Phi.Normalize(hiddenDim, InputDim);

    //        U = new CudaPieceFloat(outputDim * hiddenDim, true, true);

    //        U.InitByMultiplyingBias(1, 0.01f);

    //        b = new CudaPieceFloat(HiddenDim, true, true);
    //        float temp = (float)(alpha - 1.0);
    //        b.Init(temp);

    //        Beta = beta;

    //        gamma = BpLdaDNNBuilderParameters.GAMMA;
    //    }

    //    public void ModelSave_BP_S_LDA_GPU_V1_2015_NOV(string modelFile, int inputDim)
    //    {
    //        using (FileStream mstream = new FileStream(modelFile, FileMode.Create, FileAccess.Write))
    //        {
    //            using (BinaryWriter mwriter = new BinaryWriter(mstream))
    //            {
    //                // Writing hyper parameters
    //                mwriter.Write(BpLdaDNNBuilderParameters.HIDDEN_DIM);
    //                mwriter.Write(BpLdaDNNBuilderParameters.LAYER_DIM);
    //                mwriter.Write(BpLdaDNNBuilderParameters.GAMMA);
    //                mwriter.Write(BpLdaDNNBuilderParameters.OUTPUT_DIM);
    //                mwriter.Write(inputDim);
    //                mwriter.Write(BpLdaDNNBuilderParameters.ETA);
    //                mwriter.Write(BpLdaDNNBuilderParameters.ALPHA);
    //                mwriter.Write(BpLdaDNNBuilderParameters.BETA);
    //                mwriter.Write(BpLdaDNNBuilderParameters.T_VALUE);
    //                switch (BpLdaDNNBuilderParameters.LossFunction)
    //                { 
    //                    case LossFunctionType.BinaryClassification:
    //                        mwriter.Write((int)LossFunctionType.BinaryClassification);
    //                        break;
    //                    case LossFunctionType.Regression:
    //                        mwriter.Write((int)LossFunctionType.Regression);
    //                        break;
    //                    default:
    //                        mwriter.Write("unknownLossFunction");
    //                        break;
    //                }
    //                mwriter.Write(BpLdaDNNBuilderParameters.SEED);

    //                // Writing info of Phi
    //                mwriter.Write(InputDim);
    //                mwriter.Write(HiddenDim);
    //                Phi.CopyOutFromCuda();
    //                for (int i = 0; i < InputDim * HiddenDim; i++)
    //                {
    //                    mwriter.Write(Phi.MemPtr[i]);
    //                }

    //                // Writing info of U
    //                mwriter.Write(OutputDim);
    //                mwriter.Write(HiddenDim);
    //                U.CopyOutFromCuda();
    //                for (int i = 0; i < OutputDim * HiddenDim; i++)
    //                {
    //                    mwriter.Write(U.MemPtr[i]);
    //                }

    //            }
    //        }
    //    }

    //    public void ModelLoad_BP_S_LDA_GPU_V1_2015_NOV(string modelFile)
    //    {
    //        using (FileStream mstream = new FileStream(modelFile, FileMode.Open, FileAccess.Read))
    //        {
    //            using (BinaryReader mreader = new BinaryReader(mstream))
    //            { }
    //        }    
    //    }

    //    public BpLdaDNNStructure(string dnnModel, BpLdaDNNModelVersion modelVersion, DeviceType device)
    //    {
    //        switch (modelVersion)
    //        {
    //            case BpLdaDNNModelVersion.BP_S_LDA_GPU_V1_2015_NOV:
    //                ModelLoad_BP_S_LDA_GPU_V1_2015_NOV(dnnModel);
    //                break;
    //        }
    //    }

    //}


    ///// <summary>
    ///// This runner responsible for allocating all the temporary variables that would be used in forward/backward/update
    ///// </summary>
    ///// <typeparam name="IN"></typeparam>
    //public class BpLdaDNNRunner<IN> : StructRunner<BpLdaDNNStructure, IN> where IN : GeneralBatchInputData
    //{
    //    public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

    //    public CudaPieceFloat Theta;
    //    public CudaPieceFloat T;
    //    public CudaPieceFloat iterT;
    //    CudaPieceFloat Grad_Q_U;
    //    CudaPieceFloat Grad_Q_Phi;
    //    CudaPieceFloat AdaGradSum;
    //    CudaPieceFloat MuPhiSearch;

    //    // Temporary variable for backward function
    //    CudaPieceFloat d_xi;
    //    CudaPieceFloat d_tempDenseMat;
    //    CudaPieceFloat d_temp_theta_xi;
    //    CudaPieceFloat d_temp_theta_xi_b_T_OVER_theta_lm1_2;
    //    CudaPieceFloat d_thetaRatio;
    //    CudaPieceFloat d_sumGrad_Q_U; //outputdim * hiddenDim * batchsize
    //    CudaPieceFloat d_sumTempDenseMat; // batchsize * hiddenDim
    //    CudaPieceFloat d_tempResBatch; // batchsize

    //    // Temporary variable for Forward function
    //    CudaPieceFloat d_Phitheta;
    //    CudaPieceFloat d_tempSparseMat;
    //    CudaPieceFloat d_tempSparseMatTemp1;
    //    CudaPieceFloat d_tempDenseMatTemp;
    //    CudaPieceFloat d_NegGradTemp;
    //    CudaPieceFloat d_sum;
    //    CudaPieceFloat d_sumGradProj;

    //    CudaPieceInt d_flagWhileBreak; // batchsize
    //    CudaPieceInt d_whileBreakCount; // 1
    //    CudaPieceInt whileBreakCount; //1

    //    CudaPieceFloat d_LogThetaTemp;
    //    CudaPieceFloat d_loss_pre;
    //    CudaPieceFloat d_loss_post;
    //    CudaPieceFloat d_TLayerValue; // batchsize
    //    CudaPieceFloat d_MaxColValue; // batchsize
    //    CudaPieceFloat d_SumColValue; // batchsize

    //    // Temporary variable for update function
    //    CudaPieceFloat d_update;
    //    CudaPieceFloat d_tempColSum;
    //    CudaPieceFloat d_tempColMax;

    //    CudaPieceFloat d_temp_Xt_OVER_Phitheta; // elementSize
    //    CudaPieceFloat d_tempSparseMatValue; // elementSize
    //    CudaPieceFloat d_sumAdaGradSum; // hiddenDim * inputDim
    //    CudaPieceFloat d_sumTempColSum; // hiddenDim * inputDim

    //    // GPU variable for loss for current batch
    //    public CudaPieceFloat d_Loss_Per_Epoch;

    //    /// <summary>
    //    /// Initialize the class variables 
    //    /// </summary>
    //    /// <param name="Theta"></param> used in forward
    //    /// <param name="T"></param> used in forward and backward
    //    /// <param name="iterT"></param> variable to keep track the last layer used for a sample 
    //    /// <param name="Grad_Q_U"></param> gradient for model param U
    //    /// <param name="Grad_Q_Phi"></param> gradient for model param Phi
    //    /// <param name="AdaGradSum"></param> used to update model param in Update
    //    /// <param name="MuPhiSearch"></param> temporary variable used in Update
    //    /// <param name="d_xi"></param> used in back propagarion
    //    /// <param name="d_tempDenseMat"></param>
    //    /// <param name="d_temp_theta_xi"></param>
    //    /// <param name="d_temp_theta_xi_b_T_OVER_theta_lm1_2"></param>
    //    /// <param name="d_thetaRatio"></param>
    //    /// <param name="d_sumGrad_Q_U"></param> used to collect values that would be submm
    //    /// <param name="d_sumTempDenseMat"></param>
    //    /// <param name="d_tempResBatch"></param>
    //    /// <param name="d_Phitheta"></param>
    //    /// <param name="d_tempSparseMat"></param>
    //    /// <param name="d_tempSparseMatTemp1"></param>
    //    /// <param name="d_tempDenseMatTemp"></param>
    //    /// <param name="d_NegGradTemp"></param>
    //    /// <param name="d_sum"></param>
    //    /// <param name="d_sumGradProj"></param>
    //    /// <param name="d_flagWhileBreak"></param>
    //    /// <param name="d_whileBreakCount"></param>
    //    /// <param name="whileBreakCount"></param>
    //    /// <param name="d_LogThetaTemp"></param>
    //    /// <param name="d_loss_pre"></param>
    //    /// <param name="d_loss_post"></param>
    //    /// <param name="d_TLayerValue"></param>
    //    /// <param name="d_MaxColValue"></param>
    //    /// <param name="d_SumColValue"></param>
    //    /// <param name="d_update"></param>
    //    /// <param name="d_tempColSum"></param>
    //    /// <param name="d_tempColMax"></param>
    //    /// <param name="d_temp_Xt_OVER_Phitheta"></param>
    //    /// <param name="d_tempSparseMatValue"></param>
    //    /// <param name="d_sumAdaGradSum"></param>
    //    /// <param name="d_sumTempColSum"></param>
    //    /// <param name="d_Loss_Per_Epoch"></param>
    //    public void setAllocatedCudaMemory(
    //        CudaPieceFloat Theta,
    //        CudaPieceFloat T,
    //        CudaPieceFloat iterT,
    //        CudaPieceFloat Grad_Q_U,
    //        CudaPieceFloat Grad_Q_Phi,
    //        CudaPieceFloat AdaGradSum,
    //        CudaPieceFloat MuPhiSearch,

    //        // Temporary variable for backward function
    //        CudaPieceFloat d_xi,
    //        CudaPieceFloat d_tempDenseMat,
    //        CudaPieceFloat d_temp_theta_xi,
    //        CudaPieceFloat d_temp_theta_xi_b_T_OVER_theta_lm1_2,
    //        CudaPieceFloat d_thetaRatio,
    //        CudaPieceFloat d_sumGrad_Q_U, //outputdim * hiddenDim * batchsize
    //        CudaPieceFloat d_sumTempDenseMat, // batchsize * hiddenDim
    //        CudaPieceFloat d_tempResBatch, // batchsize

    //        // Temporary variable for Forward function
    //        CudaPieceFloat d_Phitheta,
    //        CudaPieceFloat d_tempSparseMat,
    //        CudaPieceFloat d_tempSparseMatTemp1,
    //        CudaPieceFloat d_tempDenseMatTemp,
    //        CudaPieceFloat d_NegGradTemp,
    //        CudaPieceFloat d_sum,
    //        CudaPieceFloat d_sumGradProj,

    //        CudaPieceInt d_flagWhileBreak, // batchsize
    //        CudaPieceInt d_whileBreakCount, // 1
    //        CudaPieceInt whileBreakCount, //1

    //        CudaPieceFloat d_LogThetaTemp,
    //        CudaPieceFloat d_loss_pre,
    //        CudaPieceFloat d_loss_post,
    //        CudaPieceFloat d_TLayerValue, // batchsize
    //        CudaPieceFloat d_MaxColValue, // batchsize
    //        CudaPieceFloat d_SumColValue, // batchsize

    //        // Temporary variable for update function
    //        CudaPieceFloat d_update,
    //        CudaPieceFloat d_tempColSum,
    //        CudaPieceFloat d_tempColMax,


    //        CudaPieceFloat d_temp_Xt_OVER_Phitheta, // elementSize
    //        CudaPieceFloat d_tempSparseMatValue, // elementSize
    //        CudaPieceFloat d_sumAdaGradSum, // hiddenDim * inputDim
    //        CudaPieceFloat d_sumTempColSum, // hiddenDim * inputDim

    //        // GPU variable for loss for current batch
    //        CudaPieceFloat d_Loss_Per_Epoch
    //        )
    //    {
    //        this.Theta = Theta;
    //        this.T = T;
    //        this.iterT = iterT;
    //        this.Grad_Q_U = Grad_Q_U;
    //        this.Grad_Q_Phi = Grad_Q_Phi;
    //        this.AdaGradSum = AdaGradSum;
    //        this.MuPhiSearch = MuPhiSearch;

    //        // Temporary variable for backward function
    //        this.d_xi = d_xi;
    //        this.d_tempDenseMat = d_tempDenseMat;
    //        this.d_temp_theta_xi = d_temp_theta_xi;
    //        this.d_temp_theta_xi_b_T_OVER_theta_lm1_2 = d_temp_theta_xi_b_T_OVER_theta_lm1_2;
    //        this.d_thetaRatio = d_thetaRatio;
    //        this.d_sumGrad_Q_U = d_sumGrad_Q_U; //outputdim * hiddenDim * batchsize
    //        this.d_sumTempDenseMat = d_sumTempDenseMat; // batchsize * hiddenDim
    //        this.d_tempResBatch = d_tempResBatch; // batchsize

    //        // Temporary variable for Forward function
    //        this.d_Phitheta = d_Phitheta;
    //        this.d_tempSparseMat = d_tempSparseMat;
    //        this.d_tempSparseMatTemp1 = d_tempSparseMatTemp1;
    //        this.d_tempDenseMatTemp = d_tempDenseMatTemp;
    //        this.d_NegGradTemp = d_NegGradTemp;
    //        this.d_sum = d_sum;
    //        this.d_sumGradProj = d_sumGradProj;

    //        this.d_flagWhileBreak = d_flagWhileBreak; // batchsize
    //        this.d_whileBreakCount = d_whileBreakCount; // 1
    //        this.whileBreakCount = whileBreakCount; //1

    //        this.d_LogThetaTemp = d_LogThetaTemp;
    //        this.d_loss_pre = d_loss_pre;
    //        this.d_loss_post = d_loss_post;
    //        this.d_TLayerValue = d_TLayerValue; // batchsize
    //        this.d_MaxColValue = d_MaxColValue; // batchsize
    //        this.d_SumColValue = d_SumColValue; // batchsize

    //        // Temporary variable for update function
    //        this.d_update = d_update;
    //        this.d_tempColSum = d_tempColSum;
    //        this.d_tempColMax = d_tempColMax;


    //        this.d_temp_Xt_OVER_Phitheta = d_temp_Xt_OVER_Phitheta; // elementSize
    //        this.d_tempSparseMatValue = d_tempSparseMatValue; // elementSize
    //        this.d_sumAdaGradSum = d_sumAdaGradSum; // hiddenDim * inputDim
    //        this.d_sumTempColSum = d_sumTempColSum; // hiddenDim * inputDim

    //        // GPU variable for loss for current batch
    //        this.d_Loss_Per_Epoch = d_Loss_Per_Epoch;
    //    }

    //    // Initialize
    //    public void initCudaMemory()
    //    {
    //        Theta.Init(oneOverHiddenDim);
    //        T.Init(BpLdaDNNBuilderParameters.T_VALUE);
    //        iterT.Init(1);
    //        Grad_Q_U.Zero();
    //        Grad_Q_Phi.Zero();
    //        AdaGradSum.Init(0.0f);
    //        MuPhiSearch.Init(mu_Phi);
    //        d_update.Init(0.0f);
    //        d_tempColSum.Init(0.0f);
    //        d_tempColMax.Init(float.MinValue);
    //        d_Loss_Per_Epoch.Init(0.0f);
    //    }

    //    // Should make it a hyper parameter
    //    float eta = BpLdaDNNBuilderParameters.ETA;

    //    // Although initialized in constructor, just givign a default value
    //    float mu_Phi = 0.0002f;
    //    float mu_U = 0.1f;

    //    // Currently is not no use. Remove in future
    //    int MaxLineSearchIter = 5;

    //    float TotalSamples;
    //    int CntModelUpdate = 0;
    //    int useAdaptivenHidLayer = 1;

    //    public float oneOverHiddenDim;

    //    int calculateSoftMax;

    //    public BpLdaDNNRunner(BpLdaDNNStructure model, RunnerBehavior behavior, GeneralBatchInputDataStat stat)
    //        : base(model, behavior)
    //    {
            
    //        // Setting step sizes
    //        mu_U = BpLdaDNNBuilderParameters.MU_U;
    //        mu_Phi = BpLdaDNNBuilderParameters.MU_PHI;

    //        // Use adaptive ?
    //        useAdaptivenHidLayer = BpLdaDNNBuilderParameters.USE_ADAPTIVE;

    //        // Output of dim 1 for now
    //        Output = new HiddenBatchData(stat.MAX_BATCHSIZE, model.OutputDim, behavior.RunMode, Behavior.Device);

    //        TotalSamples = stat.TotalSampleNumber;

    //        // Used during Update for running average
    //        CntModelUpdate = 0;

    //        oneOverHiddenDim = (float)(1.0 / (model.HiddenDim * 1.0));

    //        // Currently supports two loss function  regression and binary classification
    //        switch (BpLdaDNNBuilderParameters.LossFunction)
    //        {
    //            case LossFunctionType.BinaryClassification:
    //                calculateSoftMax = 1;
    //                break;
    //            case LossFunctionType.Regression:
    //                calculateSoftMax = 0;
    //                break;
    //            default:
    //                calculateSoftMax = 0;
    //                break;
    //        }
    //    }

    //    ~BpLdaDNNRunner()
    //    {
    //        this.Dispose(false);
    //    }

    //    private bool disposed = false;
    //    protected override void Dispose(bool disposing)
    //    {
    //        if (disposed)
    //        {
    //            return;
    //        }

    //        disposed = true;
    //        base.Dispose(disposing);
    //    }

    //    /// <summary>
    //    /// Forward activation of BP-sLDA
    //    /// </summary>
    //    public override void Forward()
    //    {
    //        Bp_Lda_Forward(
    //            Model.Phi,
    //            Theta,
    //            Model.b,
    //            Model.U,
    //            T,
    //            d_TLayerValue,
    //            d_MaxColValue,
    //            d_SumColValue,
    //            iterT,
    //            Output.Output.Data,
    //            null,
    //            //Input.MatchLabelValue,
    //            d_Loss_Per_Epoch,
    //            Input.BatchIdx,
    //            Input.FeatureIdx,
    //            Input.FeatureValue,
    //            Input.Stat.MAX_ELEMENTSIZE,
    //            eta,
    //            useAdaptivenHidLayer,
    //            Input.BatchSize,
    //            Model.InputDim,
    //            Model.HiddenDim,
    //            Model.OutputDim,
    //            Model.LayerDim,
    //            d_Phitheta,
    //            d_tempSparseMat,
    //            d_tempSparseMatTemp1,
    //            d_tempDenseMatTemp,
    //            d_NegGradTemp,
    //            d_LogThetaTemp,
    //            d_loss_pre,
    //            d_loss_post,
    //            d_sum,
    //            d_sumGradProj,
    //            d_flagWhileBreak, // batchsize
    //            d_whileBreakCount, // 1
    //            whileBreakCount, //1
    //            Model.gamma,
    //            calculateSoftMax,
    //            MaxLineSearchIter); // 0 for regression; 1 for softmax binary classification
    //    }

    //    /// <summary>
    //    /// Backward function of BP-sLDA
    //    /// </summary>
    //    /// <param name="cleanDeriv"></param>
    //    public override void Backward(bool cleanDeriv)
    //    {
    //        // used to prepolulate the Grad_Q_Phi
    //        float negativeBetaMinusOneOverTotalSamples = (-1.0f) * ((Model.Beta - 1) / ((float)TotalSamples));
            
    //        /************NOT SUPER IMPORTANT AT ALL BECAUSE WE DO IT INSIDE THE CUDA**************/
    //        /************Reset after each epoch*******/
    //        //Grad_Q_U.Zero();
    //        //Grad_Q_Phi.Zero();

    //        Bp_Lda_Backward(
    //            Output.Output.Data,
    //            null,
    //            //Input.MatchLabelValue,
    //            Theta,
    //            Model.Phi,
    //            Model.b,
    //            Model.U,
    //            T,
    //            iterT,
    //            Grad_Q_U,
    //            Grad_Q_Phi,
    //            Input.BatchIdx,
    //            Input.FeatureIdx,
    //            Input.FeatureValue,
    //            Model.gamma,
    //            negativeBetaMinusOneOverTotalSamples,
    //            Input.BatchSize,
    //            Model.InputDim,
    //            Model.HiddenDim,
    //            Model.OutputDim,
    //            Model.LayerDim,
    //            Input.ElementSize,
    //            d_xi,
    //            d_tempDenseMat,
    //            d_temp_theta_xi,
    //            d_temp_theta_xi_b_T_OVER_theta_lm1_2,
    //            d_thetaRatio,
    //            d_sumGrad_Q_U,
    //            d_temp_Xt_OVER_Phitheta, // elementSize
    //            d_tempSparseMatValue, // elementSize
    //            d_sumTempDenseMat,
    //            d_tempSparseMat,
    //            d_tempResBatch,
    //            calculateSoftMax // 0 for regression; 1 for softmax binary classification
    //            );
    //    }

    //    /// <summary>
    //    /// Update operation of BP-sLDA
    //    /// </summary>
    //    public override void Update()
    //    {
    //        // used for calculating running average for adagradsum
    //        CntModelUpdate++;

    //        Bp_Lda_Update(
    //            Grad_Q_U,
    //            Grad_Q_Phi,
    //            Model.U,
    //            Model.Phi,
    //            AdaGradSum,
    //            d_sumAdaGradSum,
    //            d_sumTempColSum,
    //            MuPhiSearch,
    //            mu_U,
    //            mu_Phi,
    //            CntModelUpdate,
    //            Model.InputDim,
    //            Model.HiddenDim,
    //            Model.OutputDim,
    //            d_update,
    //            d_tempColSum,
    //            d_tempColMax
    //            );
    //    }

    //    /// <summary>
    //    /// Wrapper to call cudalib.Forward
    //    /// </summary>
    //    public void Bp_Lda_Forward(
    //        CudaPieceFloat phi,
    //        CudaPieceFloat theta,
    //        CudaPieceFloat b,
    //        CudaPieceFloat U,
    //        CudaPieceFloat T,
    //        CudaPieceFloat d_TLayerValue,
    //        CudaPieceFloat d_MaxColValue,
    //        CudaPieceFloat d_SumColValue,
    //        CudaPieceFloat iterT,
    //        CudaPieceFloat output,
    //        CudaPieceFloat matchLabelValue,
    //        CudaPieceFloat d_Loss_Per_Epoch,
    //        CudaPieceInt matchIdx,
    //        CudaPieceInt featureIdx,
    //        CudaPieceFloat featureValue,
    //        int elementSize,
    //        float eta,
    //        int useAdaptivenHidLayer,
    //        int batchsize,
    //        int inputDim,
    //        int hiddenDim,
    //        int outputDim,
    //        int layerDim,
    //        CudaPieceFloat d_Phitheta,
    //        CudaPieceFloat d_tempSparseMat,
    //        CudaPieceFloat d_tempSparseMatTemp1,
    //        CudaPieceFloat d_tempDenseMatTemp,
    //        CudaPieceFloat d_NegGradTemp,
    //        CudaPieceFloat d_LogThetaTemp,
    //        CudaPieceFloat d_loss_pre,
    //        CudaPieceFloat d_loss_post,
    //        CudaPieceFloat d_sum,
    //        CudaPieceFloat d_sumGradProj,
    //        CudaPieceInt d_flagWhileBreak, // batchsize
    //        CudaPieceInt d_whileBreakCount, // 1
    //        CudaPieceInt whileBreakCount, //1
    //        float gamma,
    //        int calculateSoftMax,
    //        int MaxLineSearchIter)
    //    {
    //        Cudalib.Bp_Lda_Forward(
    //            phi.CudaPtr,
    //            theta.CudaPtr,
    //            b.CudaPtr,
    //            U.CudaPtr,
    //            T.CudaPtr,
    //            d_TLayerValue.CudaPtr,
    //            d_MaxColValue.CudaPtr,
    //            d_SumColValue.CudaPtr,
    //            iterT.CudaPtr,
    //            output.CudaPtr,
    //            matchLabelValue.CudaPtr,
    //            d_Loss_Per_Epoch.CudaPtr,
    //            matchIdx.CudaPtr,
    //            featureIdx.CudaPtr,
    //            featureValue.CudaPtr,
    //            elementSize,
    //            eta,
    //            useAdaptivenHidLayer,
    //            batchsize,
    //            inputDim,
    //            hiddenDim,
    //            outputDim,
    //            layerDim,
    //            d_Phitheta.CudaPtr,
    //            d_tempSparseMat.CudaPtr,
    //            d_tempSparseMatTemp1.CudaPtr,
    //            d_tempDenseMatTemp.CudaPtr,
    //            d_NegGradTemp.CudaPtr,
    //            d_LogThetaTemp.CudaPtr,
    //            d_loss_pre.CudaPtr,
    //            d_loss_post.CudaPtr,
    //            d_sum.CudaPtr,
    //            d_sumGradProj.CudaPtr,
    //            d_flagWhileBreak.CudaPtr, // batchsize
    //            d_whileBreakCount.CudaPtr, // 1
    //            whileBreakCount.MemPtr, //1
    //            gamma,
    //            calculateSoftMax,
    //            MaxLineSearchIter
    //        );
    //    }

    //    /// <summary>
    //    /// Wrapper to call cudalib.Backward
    //    /// </summary>
    //    public void Bp_Lda_Backward(
    //        CudaPieceFloat output,
    //        CudaPieceFloat matchLabelValue,
    //        CudaPieceFloat theta,
    //        CudaPieceFloat phi,
    //        CudaPieceFloat b,
    //        CudaPieceFloat U,
    //        CudaPieceFloat T,
    //        CudaPieceFloat iterT,
    //        CudaPieceFloat grad_Q_U,
    //        CudaPieceFloat grad_Q_Phi,
    //        CudaPieceInt matchIdx,
    //        CudaPieceInt featureIdx,
    //        CudaPieceFloat featureValue,
    //        float gamma,
    //        float negativeBetaMinusOneOverTotalSamples,
    //        int batchsize,
    //        int inputDim,
    //        int hiddenDim,
    //        int outputDim,
    //        int layerDim,
    //        int elementSize,
    //        CudaPieceFloat d_xi,
    //        CudaPieceFloat d_tempDenseMat,
    //        CudaPieceFloat d_temp_theta_xi,
    //        CudaPieceFloat d_temp_theta_xi_b_T_OVER_theta_lm1_2,
    //        CudaPieceFloat d_thetaRatio,
    //        CudaPieceFloat d_sumGrad_Q_U,
    //        CudaPieceFloat d_temp_Xt_OVER_Phitheta, // elementSize
    //        CudaPieceFloat d_tempSparseMatValue, // elementSize
    //        CudaPieceFloat d_sumTempDenseMat,
    //        CudaPieceFloat d_tempSparseMat, // elementSize
    //        CudaPieceFloat d_tempResBatch,
    //        int calculateSoftMax
    //        )
    //    {
    //        Cudalib.Bp_Lda_Backward(
    //            output.CudaPtr,
    //            matchLabelValue.CudaPtr,
    //            theta.CudaPtr,
    //            phi.CudaPtr,
    //            b.CudaPtr,
    //            U.CudaPtr,
    //            T.CudaPtr,
    //            iterT.CudaPtr,
    //            grad_Q_U.CudaPtr,
    //            grad_Q_Phi.CudaPtr,
    //            matchIdx.CudaPtr,
    //            featureIdx.CudaPtr,
    //            featureValue.CudaPtr,
    //            gamma,
    //            negativeBetaMinusOneOverTotalSamples,
    //            batchsize,
    //            inputDim,
    //            hiddenDim,
    //            outputDim,
    //            layerDim,
    //            elementSize,
    //            d_xi.CudaPtr,
    //            d_tempDenseMat.CudaPtr,
    //            d_temp_theta_xi.CudaPtr,
    //            d_temp_theta_xi_b_T_OVER_theta_lm1_2.CudaPtr,
    //            d_thetaRatio.CudaPtr,
    //            d_sumGrad_Q_U.CudaPtr,
    //            d_temp_Xt_OVER_Phitheta.CudaPtr, // elementSize
    //            d_tempSparseMatValue.CudaPtr, // elementSize
    //            d_sumTempDenseMat.CudaPtr,
    //            d_tempSparseMat.CudaPtr,
    //            d_tempResBatch.CudaPtr,
    //            calculateSoftMax
    //        );
    //    }

    //    /// <summary>
    //    /// Wrapper to call cudalib.Update
    //    /// </summary>
    //    public void Bp_Lda_Update(
    //        CudaPieceFloat grad_Q_U,
    //        CudaPieceFloat grad_Q_Phi,
    //        CudaPieceFloat U,
    //        CudaPieceFloat phi,
    //        CudaPieceFloat adaGradSum,
    //        CudaPieceFloat d_sumAdaGradSum,
    //        CudaPieceFloat d_sumTempColSum,
    //        CudaPieceFloat muPhiSearch,
    //        float mu_U,
    //        float mu_Phi,
    //        int cntModelUpdate,
    //        int inputDim,
    //        int hiddenDim,
    //        int outputDim,
    //        CudaPieceFloat d_update,
    //        CudaPieceFloat d_tempColSum,
    //        CudaPieceFloat d_tempColMax)
    //    {
    //        Cudalib.Bp_Lda_Update(
    //            grad_Q_U.CudaPtr,
    //            grad_Q_Phi.CudaPtr,
    //            U.CudaPtr,
    //            phi.CudaPtr,
    //            adaGradSum.CudaPtr,
    //            d_sumAdaGradSum.CudaPtr,
    //            d_sumTempColSum.CudaPtr,
    //            muPhiSearch.CudaPtr,
    //            mu_U,
    //            mu_Phi,
    //            cntModelUpdate,
    //            inputDim,
    //            hiddenDim,
    //            outputDim,
    //            d_update.CudaPtr,
    //            d_tempColSum.CudaPtr,
    //            d_tempColMax.CudaPtr
    //            );
    //    }
    //}

    //public class BpLdaDNNBuilder : Builder
    //{
    //    public override BuilderType Type { get { return BuilderType.BPLDADNN; } }

    //    public override void InitStartup(string fileName)
    //    {
    //        BpLdaDNNBuilderParameters.Parse(fileName);
    //        if (BpLdaDNNBuilderParameters.Device == DeviceType.GPU)
    //        {
    //            Cudalib.CudaInit(BpLdaDNNBuilderParameters.GPUID);
    //        }
    //    }

    //    public override void Rock()
    //    {
    //        switch (BpLdaDNNBuilderParameters.RunMode)
    //        {
    //            case DNNRunMode.Train:
    //                var training = Train();
    //                foreach (var t in training)
    //                {
    //                    // Run the training by enumerating
    //                }
    //                break;
    //        }
    //    }

    //    public IEnumerable<TrainResult> Train()
    //    {
    //        Logger.OpenLog(BpLdaDNNBuilderParameters.LogFile);
    //        Logger.WriteLog("Loading Training/Validation Data.");
    //        DataPanel.Init();
    //        Logger.WriteLog("Load Data Finished.");

    //        Logger.WriteLog("Loading DNN Structure.");
    //        BpLdaDNNStructure ldaModel = new BpLdaDNNStructure(
    //                DataPanel.Train.Stat.MAX_FEATUREDIM,
    //                BpLdaDNNBuilderParameters.HIDDEN_DIM,
    //                BpLdaDNNBuilderParameters.OUTPUT_DIM,
    //                BpLdaDNNBuilderParameters.LAYER_DIM,
    //                BpLdaDNNBuilderParameters.ALPHA,
    //                BpLdaDNNBuilderParameters.BETA,
    //                DeviceType.GPU);


    //        #region initialize cuda temp variables

    //        int maxBatchSize = Math.Max(DataPanel.Train.Stat.MAX_BATCHSIZE, DataPanel.Valid.Stat.MAX_BATCHSIZE);
    //        int maxElementSize = Math.Max(DataPanel.Train.Stat.MAX_ELEMENTSIZE, DataPanel.Valid.Stat.MAX_ELEMENTSIZE);

    //        /// Create Memory for temporal variables.
    //        /// Allocating one more layer for 0th layer : (ldaModel.LayerDim + 1)
    //        CudaPieceFloat Theta = new CudaPieceFloat((ldaModel.LayerDim + 1) * maxBatchSize * ldaModel.HiddenDim, true, true);

    //        /// Allocating one more layer for 0th iteration : (ldaModel.LayerDim + 1)
    //        CudaPieceFloat T = new CudaPieceFloat((ldaModel.LayerDim + 1) * maxBatchSize, true, true);

    //        CudaPieceFloat iterT = new CudaPieceFloat(maxBatchSize, true, true);

    //        CudaPieceFloat Grad_Q_U = new CudaPieceFloat(ldaModel.OutputDim * ldaModel.HiddenDim, true, true);

    //        CudaPieceFloat Grad_Q_Phi = new CudaPieceFloat(ldaModel.InputDim * ldaModel.HiddenDim, true, true);

    //        CudaPieceFloat AdaGradSum = new CudaPieceFloat(ldaModel.HiddenDim, true, true);

    //        CudaPieceFloat d_sumAdaGradSum = new CudaPieceFloat(ldaModel.HiddenDim * ldaModel.InputDim, true, true);
    //        CudaPieceFloat d_sumTempColSum = new CudaPieceFloat(ldaModel.HiddenDim * ldaModel.InputDim, true, true);

    //        CudaPieceFloat MuPhiSearch = new CudaPieceFloat(ldaModel.HiddenDim, true, true);

    //        CudaPieceFloat d_xi = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_tempDenseMat = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_temp_theta_xi = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_temp_theta_xi_b_T_OVER_theta_lm1_2 = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_thetaRatio = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);

    //        //outputdim * hiddenDim * batchsize
    //        CudaPieceFloat d_sumGrad_Q_U = new CudaPieceFloat(ldaModel.OutputDim * ldaModel.HiddenDim * maxBatchSize, true, true);
    //        CudaPieceFloat d_sumTempDenseMat = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_tempResBatch = new CudaPieceFloat(maxBatchSize, true, true);

    //        // Temporary variable for Forward function
    //        CudaPieceFloat d_Phitheta = new CudaPieceFloat(maxElementSize, true, true);
    //        CudaPieceFloat d_tempSparseMat = new CudaPieceFloat(maxElementSize, true, true);
    //        CudaPieceFloat d_tempSparseMatTemp1 = new CudaPieceFloat(maxElementSize, true, true);
    //        CudaPieceFloat d_tempDenseMatTemp = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_NegGradTemp = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_sum = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_sumGradProj = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);

    //        CudaPieceInt d_flagWhileBreak = new CudaPieceInt(maxBatchSize, true, true); // batchsize
    //        CudaPieceInt d_whileBreakCount = new CudaPieceInt(1, true, true); // 1
    //        CudaPieceInt whileBreakCount = new CudaPieceInt(1, true, false); //1

    //        CudaPieceFloat d_LogThetaTemp = new CudaPieceFloat(maxBatchSize * ldaModel.HiddenDim, true, true);
    //        CudaPieceFloat d_loss_pre = new CudaPieceFloat(maxBatchSize, true, true);
    //        CudaPieceFloat d_loss_post = new CudaPieceFloat(maxBatchSize, true, true);
    //        CudaPieceFloat d_TLayerValue = new CudaPieceFloat(maxBatchSize, true, true);
    //        CudaPieceFloat d_MaxColValue = new CudaPieceFloat(maxBatchSize, true, true);
    //        CudaPieceFloat d_SumColValue = new CudaPieceFloat(maxBatchSize, true, true);

    //        CudaPieceFloat d_temp_Xt_OVER_Phitheta = new CudaPieceFloat(maxElementSize, true, true);
    //        CudaPieceFloat d_tempSparseMatValue = new CudaPieceFloat(maxElementSize, true, true);

    //        CudaPieceFloat d_update = new CudaPieceFloat(ldaModel.InputDim * ldaModel.HiddenDim, true, true);

    //        CudaPieceFloat d_tempColSum = new CudaPieceFloat(ldaModel.HiddenDim, true, true);

    //        CudaPieceFloat d_tempColMax = new CudaPieceFloat(ldaModel.HiddenDim, true, true);

    //        CudaPieceFloat d_Loss_Per_Epoch = new CudaPieceFloat(1, true, true);

    //        #endregion

    //        #region setup trainer , assign cuda memory and init
    //        BpLdaDNNRunner<GeneralBatchInputData> trainer = new BpLdaDNNRunner<GeneralBatchInputData>(ldaModel, new RunnerBehavior() { RunMode = DNNRunMode.Train }, DataPanel.Train.Stat);
    //        trainer.setAllocatedCudaMemory(
    //            Theta,
    //            T,
    //            iterT,
    //            Grad_Q_U,
    //            Grad_Q_Phi,
    //            AdaGradSum,
    //            MuPhiSearch,

    //            // Temporary variable for backward function
    //            d_xi,
    //            d_tempDenseMat,
    //            d_temp_theta_xi,
    //            d_temp_theta_xi_b_T_OVER_theta_lm1_2,
    //            d_thetaRatio,
    //            d_sumGrad_Q_U, //outputdim * hiddenDim * batchsize
    //            d_sumTempDenseMat, // batchsize * hiddenDim
    //            d_tempResBatch, // batchsize

    //            d_Phitheta,
    //            d_tempSparseMat,
    //            d_tempSparseMatTemp1,
    //            d_tempDenseMatTemp,
    //            d_NegGradTemp,
    //            d_sum,
    //            d_sumGradProj,

    //            d_flagWhileBreak, // batchsize
    //            d_whileBreakCount, // 1
    //            whileBreakCount, //1

    //            d_LogThetaTemp,
    //            d_loss_pre,
    //            d_loss_post,
    //            d_TLayerValue, // batchsize
    //            d_MaxColValue, // batchsize
    //            d_SumColValue, // batchsize

    //            // Temporary variable for update function
    //            d_update,
    //            d_tempColSum,
    //            d_tempColMax,

    //            d_temp_Xt_OVER_Phitheta, // elementSize
    //            d_tempSparseMatValue, // elementSize
    //            d_sumAdaGradSum, // hiddenDim * inputDim
    //            d_sumTempColSum, // hiddenDim * inputDim

    //            // GPU variable for loss for current batch
    //            d_Loss_Per_Epoch
    //            );

    //        trainer.initCudaMemory();
    //        #endregion

    //        #region setup evaler , assign cuda memory and init
    //        BpLdaDNNRunner<GeneralBatchInputData> evaler = new BpLdaDNNRunner<GeneralBatchInputData>(ldaModel, new RunnerBehavior() { RunMode = DNNRunMode.Predict }, DataPanel.Valid.Stat);

    //        evaler.setAllocatedCudaMemory(
    //            Theta,
    //            T,
    //            iterT,
    //            Grad_Q_U,
    //            Grad_Q_Phi,
    //            AdaGradSum,
    //            MuPhiSearch,

    //            // Temporary variable for backward function
    //            d_xi,
    //            d_tempDenseMat,
    //            d_temp_theta_xi,
    //            d_temp_theta_xi_b_T_OVER_theta_lm1_2,
    //            d_thetaRatio,
    //            d_sumGrad_Q_U, //outputdim * hiddenDim * batchsize
    //            d_sumTempDenseMat, // batchsize * hiddenDim
    //            d_tempResBatch, // batchsize

    //            d_Phitheta,
    //            d_tempSparseMat,
    //            d_tempSparseMatTemp1,
    //            d_tempDenseMatTemp,
    //            d_NegGradTemp,
    //            d_sum,
    //            d_sumGradProj,

    //            d_flagWhileBreak, // batchsize
    //            d_whileBreakCount, // 1
    //            whileBreakCount, //1

    //            d_LogThetaTemp,
    //            d_loss_pre,
    //            d_loss_post,
    //            d_TLayerValue, // batchsize
    //            d_MaxColValue, // batchsize
    //            d_SumColValue, // batchsize

    //            // Temporary variable for update function
    //            d_update,
    //            d_tempColSum,
    //            d_tempColMax,

    //            d_temp_Xt_OVER_Phitheta, // elementSize
    //            d_tempSparseMatValue, // elementSize
    //            d_sumAdaGradSum, // hiddenDim * inputDim
    //            d_sumTempColSum, // hiddenDim * inputDim

    //            // GPU variable for loss for current batch
    //            d_Loss_Per_Epoch
    //            );

    //        evaler.initCudaMemory();
    //        #endregion

    //        //DataProcessor trainProcessor = new DataProcessor(DataPanel.Train);
    //        //DataProcessor validProcessor = new DataProcessor(DataPanel.Valid);

    //        Stopwatch stopWatch = new Stopwatch();
    //        Stopwatch stopWatchIO = new Stopwatch();

    //        double TotTime = 0.0f;
    //        int TotSamplesThisEpoch = 0;
    //        double TotTrainTimeThisEpoch = 0.0f;
    //        double TotIOTimeThisEpoch = 0.0f;

    //        double TimeFwd = 0.0;
    //        double TimeBck = 0.0;
    //        double TimeUpd = 0.0;

    //        float pScore = 0.0f;
    //        float maxPScore = 0.0f;

    //        // Create folder to save model files
    //        if (!Directory.Exists(BpLdaDNNBuilderParameters.ModelOutputPath))
    //        {
    //            Directory.CreateDirectory(BpLdaDNNBuilderParameters.ModelOutputPath);
    //        } 

    //        for (int iter = 0; iter < BpLdaDNNBuilderParameters.Iteration; iter++)
    //        {
    //            // Reseting loss variable to zero
    //            trainer.d_Loss_Per_Epoch.Init(0.0f);

    //            //trainProcessor.Init();
    //            int batchIdx = 0;

    //            Logger.WriteLog("Iteration {0}", iter);


    //            Stopwatch WatchFwd = new Stopwatch();
    //            Stopwatch WatchBck = new Stopwatch();
    //            Stopwatch WatchUpd = new Stopwatch();


    //            //while (true)
    //            foreach (GeneralBatchInputData ffdata in DataPanel.Train.GetInstances(true))
    //            {
    //                stopWatchIO.Start();
    //                //bool hasMore = trainProcessor.RandomNext();
    //                stopWatchIO.Stop();
    //                TotIOTimeThisEpoch += stopWatchIO.Elapsed.TotalSeconds;
    //                stopWatchIO.Reset();

    //                //if (!hasMore)
    //                //{
    //                //    break;
    //                //}
    //                trainer.Input = ffdata;
    //                stopWatch.Start();

    //                // Forward propagation
    //                WatchFwd.Start();
    //                trainer.Forward();
    //                WatchFwd.Stop();
    //                TimeFwd += WatchFwd.Elapsed.TotalMilliseconds;
    //                WatchFwd.Reset();

    //                // Backward propagation
    //                WatchBck.Start();
    //                trainer.Backward(true);
    //                WatchBck.Stop();
    //                TimeBck += WatchBck.Elapsed.TotalMilliseconds;
    //                WatchBck.Reset();

    //                // Update
    //                WatchUpd.Start();
    //                trainer.Update();
    //                WatchUpd.Stop();
    //                TimeUpd += WatchUpd.Elapsed.TotalMilliseconds;
    //                WatchUpd.Reset();

    //                stopWatch.Stop();
    //                TimeSpan ts = stopWatch.Elapsed;
    //                TotTime += ts.TotalMilliseconds;
    //                TotTrainTimeThisEpoch += ts.TotalMilliseconds;
    //                TotSamplesThisEpoch += trainer.Input.BatchSize;
    //                stopWatch.Reset();

    //                if ((batchIdx + 1) % 20 == 0)
    //                {
    //                    Console.WriteLine(
    //                        "- Ep#{0}/{1} Bat#{2} Speed={3} Sec/Sample. IOSpeed={4} Sec/Sample ",
    //                        iter + 1, BpLdaDNNBuilderParameters.Iteration,
    //                        batchIdx + 1,
    //                        ((double)TotTrainTimeThisEpoch / TotSamplesThisEpoch),
    //                        ((double)(TotTrainTimeThisEpoch + TotIOTimeThisEpoch) / TotSamplesThisEpoch)
    //                        );
    //                    Console.WriteLine("  TimeFwd = {0} Sec/Samples, TimeBck = {1} Sec/Samples, TimeUpd = {2} Sec/Samples",
    //                            ((double)TimeFwd / TotSamplesThisEpoch), ((double)TimeBck / TotSamplesThisEpoch),
    //                            ((double)TimeUpd / TotSamplesThisEpoch));
    //                    Console.WriteLine("----------------------------------------------------");
    //                }
    //                batchIdx++;
    //            }


    //            var averageLoss = 0.0f;
    //            var predLoss = 0.0f;
    //            if (BpLdaDNNBuilderParameters.LossFunction == LossFunctionType.Regression)
    //            {
    //                trainer.d_Loss_Per_Epoch.CopyOutFromCuda();
    //                averageLoss = trainer.d_Loss_Per_Epoch.MemPtr[0] / (DataPanel.Train.Stat.TotalSampleNumber + float.Epsilon);
    //                Logger.WriteLog("Training Iteration: {0}, Avg Loss: {1}, training time: {2} -----  {3} samples/s",
    //                    iter,
    //                    averageLoss,
    //                    TotTrainTimeThisEpoch,
    //                     (int)((double)TotSamplesThisEpoch / TotTrainTimeThisEpoch));


    //                ///******************* For regression *********************/
    //                // Reseting Loss variable to zero
    //                evaler.d_Loss_Per_Epoch.Init(0.0f);
    //                evaler.Theta.Init(trainer.oneOverHiddenDim);
    //                evaler.T.Init(1);
    //                evaler.iterT.Init(1);

    //                //MSEEvaluationSet evaluator = new MSEEvaluationSet();
    //                //evaluator.ScoreFile = "tmp1.score";
    //                foreach (GeneralBatchInputData data in DataPanel.Valid.GetInstances(false))
    //                {
    //                    evaler.Input = data;
    //                    evaler.Forward();
    //                    //evaler.Output.Output.Data.CopyOutFromCuda();
    //                    //evaler.Input.MatchLabelValue.CopyOutFromCuda();
    //                    //evaluator.PushScore(evaler.Input.MatchLabelValue.MemPtr, evaler.Output.Output.Data.MemPtr, evaler.Input.BatchSize);
    //                }

    //                //List<string> pEvalInfo = new List<string>();
    //                //float pScore = evaluator.EvaluationCPU(out pEvalInfo);
    //                //Logger.WriteLog("Evaluation MSE : {0}", string.Join("\n", pEvalInfo));

                    
    //                evaler.d_Loss_Per_Epoch.CopyOutFromCuda();
    //                predLoss = evaler.d_Loss_Per_Epoch.MemPtr[0] / (DataPanel.Valid.Stat.TotalSampleNumber + float.Epsilon);

    //                Logger.WriteLog("Validation Iteration {0}, Avg Loss {1}", iter, predLoss);
    //            }
    //            else if (BpLdaDNNBuilderParameters.LossFunction == LossFunctionType.BinaryClassification)
    //            {
    //                /******************For Softmax************************/
    //                EvaluationSet evaluator = EvaluationSet.CreateEvaluation(EvaluationType.AUC);
    //                evaluator.ScoreFile = "out_iter_" + iter + ".score";
    //                //validProcessor.Init();
    //                //while (validProcessor.RandomNext())
    //                //{
    //                foreach (GeneralBatchInputData data in DataPanel.Valid.GetInstances(false))
    //                {
    //                    evaler.Input = data;
    //                    evaler.Forward();
    //                    //evaler.Input.MatchLabelValue.CopyOutFromCuda();
    //                    //evaler.Output.Output.Data.CopyOutFromCuda();
    //                    //evaluator.PushScore(evaler.Input.MatchLabelValue.MemPtr, evaler.Output.Output.Data.MemPtr, evaler.Input.BatchSize);
    //                }
    //                List<string> pEvalInfo = new List<string>();
    //                pScore = evaluator.Evaluation(out pEvalInfo);
    //                Logger.WriteLog("Evaluation AUC : {0}", string.Join("\n", pEvalInfo));
    //            }

    //            string modelPath = string.Format(@"{0}\\BP_S_LDA.{1}.model", BpLdaDNNBuilderParameters.ModelOutputPath, iter.ToString());
    //            if (pScore > maxPScore)
    //            {
    //                maxPScore = pScore;
    //            }
    //            ldaModel.ModelSave_BP_S_LDA_GPU_V1_2015_NOV(modelPath, ldaModel.InputDim);
    //            yield return new TrainResult(iter, averageLoss, predLoss, modelPath);
    //        }

    //        Logger.WriteLog("Total time: {0}", TotTime);
    //        Logger.WriteLog("PScore: {0}", maxPScore);


    //        trainer.Dispose();
    //        evaler.Dispose();
    //        DataPanel.DeInit();
    //        Logger.CloseLog();
    //    }

    //    public void Predict()
    //    { 
            
    //    }

    //    public class DataPanel
    //    {
    //        //public static MatchBatchInputData Train = null;
    //        //public static MatchBatchInputData Valid = null;
    //        public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Train = null;
    //        public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Valid = null;

    //        public static void Init()
    //        {
    //            if (BpLdaDNNBuilderParameters.RunMode == DNNRunMode.Train)
    //            {
    //                Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BpLdaDNNBuilderParameters.TrainData);
    //                Train.InitPipelineCashier(70, 20);
    //            }
    //            //Train = new MatchBatchInputData(BpLdaDNNBuilderParameters.TrainData, BatchInputMode.LoadContainer, BpLdaDNNBuilderParameters.Device);
    //            if (BpLdaDNNBuilderParameters.IsValidFile)
    //            {
    //                Valid = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BpLdaDNNBuilderParameters.ValidData);
    //                Valid.InitPipelineCashier(50, 10);
    //            }
    //            //Valid = new MatchBatchInputData(BpLdaDNNBuilderParameters.ValidData, BatchInputMode.LoadContainer, BpLdaDNNBuilderParameters.Device);
    //        }

    //        internal static void DeInit()
    //        {
    //            if (null != Train)
    //            {
    //                Train.Dispose();
    //                Train = null;
    //            }

    //            if (null != Valid)
    //            {
    //                Valid.Dispose();
    //                Valid = null;
    //            }
    //        }
    //    }
    //}


}
