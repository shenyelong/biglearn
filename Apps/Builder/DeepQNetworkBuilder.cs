﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// Deep Q Network for Reinforcement Learning.
    /// </summary>
    public class DeepQNetworkBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }

            public static string EnvirOutputPath { get { return Argument["ENVIR-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }

            public static float WeightInitBias { get { return float.Parse(Argument["INIT-WEIGHT-BIAS"].Value); } }

            public static float WeightInitScale { get { return float.Parse(Argument["INIT-WEIGHT-SCALE"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }


            /// <summary>
            /// Only need to Specify Data and Archecture of Deep Nets.
            /// </summary>
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));

                Argument.Add("LAYER-DIM", new ParameterArgument("100,4", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(string.Format("{0},{1}", (int)A_Func.Tanh, (int)A_Func.Linear), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1,1", "DNN Layer Bias"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("ENVIR-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));

                Argument.Add("INIT-WEIGHT-BIAS", new ParameterArgument("0", "Init Model Weight Bias"));
                Argument.Add("INIT-WEIGHT-SCALE", new ParameterArgument("-1", "Init Model Weight Scale"));
            }
        }


        public override BuilderType Type { get { return BuilderType.DQN; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
            OptimizerParameters.Parse(fileName);

            if (BuilderParameters.Device == DeviceType.GPU) Cudalib.CudaInit(BuilderParameters.GPUID);
        }

        public ComputationGraph BuildComputationGraph(GameDataCashier sense, DNNStructure QNetwork, DNNStructure TargetNetwork)
        {
            ComputationGraph cg = new ComputationGraph();

            GeneralBatchInputData senseInput = (GeneralBatchInputData)cg.AddRunner(new SenseRunner(sense,
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

            HiddenBatchData actionInput = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(QNetwork, senseInput, 
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }){IsBackProp = false});

            cg.AddRunner(new ActionRunner(sense, actionInput,
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

            ReplayBatchData replayData = (ReplayBatchData)cg.AddRunner(new ReplayMemoryDataRunner(sense,
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

            cg.AddRunner(new ModelSyncRunner(QNetwork, TargetNetwork, 512,
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }) { IsBackProp = false });

            HiddenBatchData targetAction = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(TargetNetwork, replayData.Target,
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }){IsBackProp = false} );

            DenseBatchData updateReward = (DenseBatchData)cg.AddRunner(new RewardUpdateRunner(replayData.Reward, targetAction,
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }){IsBackProp = false} );

            HiddenBatchData sourceAction = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(QNetwork, replayData.Source,
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }));

            cg.AddObjective(new MSERunner(sourceAction.Output, updateReward, replayData.Action.Data, sourceAction.Deriv, 
                new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = BuilderParameters.Device }){IsBackProp = false} );
            return cg;
        }
        public override void Rock()
        {

            Logger.OpenLog(BuilderParameters.LogFile);

            GameEnvironment enviroument = new GameEnvironment(BuilderParameters.EnvirOutputPath);
            GameDataCashier sense = new GameDataCashier(enviroument);

            DNNStructure QNetwork = null;
            DNNStructure TargetNetwork = null;


            Logger.WriteLog("Loading DNN Structure.");

            if (BuilderParameters.SEED_MODEL != string.Empty)
            {
                QNetwork = new DNNStructure(BuilderParameters.SEED_MODEL, DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR, BuilderParameters.Device);
                TargetNetwork = new DNNStructure(BuilderParameters.SEED_MODEL, DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR, BuilderParameters.Device);
            }
            else
            {
                QNetwork = new DNNStructure(sense.StatusFeatureDim, 
                    BuilderParameters.LAYER_DIM, 
                    BuilderParameters.ACTIVATION, 
                    BuilderParameters.LAYER_BIAS);
                TargetNetwork = new DNNStructure(sense.StatusFeatureDim, BuilderParameters.LAYER_DIM, BuilderParameters.ACTIVATION, BuilderParameters.LAYER_BIAS);
                TargetNetwork.Init(QNetwork);
            }
            Logger.WriteLog(QNetwork.DNN_Descr());
            Logger.WriteLog("Load DNN Structure Finished.");

            QNetwork.InitOptimizer(new StructureLearner()
            {
                LearnRate = OptimizerParameters.LearnRate,
                Optimizer = OptimizerParameters.Optimizer,
                EpochNum = OptimizerParameters.Iteration,
                ShrinkLR = OptimizerParameters.ShrinkLR
            });

            ComputationGraph trainCG = BuildComputationGraph(sense, QNetwork, TargetNetwork);

            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                double loss = trainCG.Execute();
                QNetwork.Save(string.Format(@"{0}\\DNN.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()));
            }
            enviroument.CloseReplayStream();
            Logger.CloseLog();
        }
    }
}
