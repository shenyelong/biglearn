﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class LSTMSeqLabelBuilderParameters : BaseModelArgument<LSTMSeqLabelBuilderParameters>
    {
        static LSTMSeqLabelBuilderParameters()
        {
            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
            Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));
            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
            Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
            Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
            Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));

            Argument.Add("STACK", new ParameterArgument("5", "RNN Output Stack Length"));

            Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.BinaryClassification).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
            Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

            Argument.Add("DELTA-CLIP", new ParameterArgument("0", "Model Update Clip."));
            Argument.Add("WEIGHT-CLIP", new ParameterArgument("0", "Model Weight Clip"));

            Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
            Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

            Argument.Add("ADABOOST", new ParameterArgument("1", "Ada Gradient Adjustment."));
            Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
            Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
            Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
            Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

            Argument.Add("CHECK-GRAD", new ParameterArgument("0", "Check Gradient!"));
        }

        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
        public static DeviceType Device
        {
            get
            {
                if (GPUID >= 0)
                {
                    return DeviceType.GPU;
                }
                else if (GPUID == -1)
                {
                    return DeviceType.CPU;
                }
                else
                {
                    return DeviceType.CPU_FAST_VECTOR;
                }
            }
        }

        public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
        public static string TrainData { get { return Argument["TRAIN"].Value; } }
        public static string ValidData { get { return Argument["VALID"].Value; } }
        public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }

        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
        public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
        public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }

        public static int STACK { get { return int.Parse(Argument["STACK"].Value); } }
        public static float DELTACLIP { get { return float.Parse(Argument["DELTA-CLIP"].Value); } }
        public static float WEIGHTCLIP { get { return float.Parse(Argument["WEIGHT-CLIP"].Value); } }

        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
        public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

        public static string ScoreOutputPath
        {
            get
            {
                if (RunMode == DNNRunMode.Predict) return Argument["SCORE-PATH"].Value;
                else return LogFile + ".tmp.score";
            }
        }
        public static string MetricOutputPath
        {
            get
            {
                if (RunMode == DNNRunMode.Predict) return Argument["METRIC-PATH"].Value;
                else return LogFile + ".tmp.metric";
            }
        }

        public static float AdaBoost { get { return float.Parse(Argument["ADABOOST"].Value); } }
        public static float ShrinkLR { get { return float.Parse(Argument["SHRINKLR"].Value); } }
        public static float LearnRate { get { return float.Parse(Argument["LEARN-RATE"].Value); } }
        public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }
        public static DNNOptimizerType Optimizer { get { return (DNNOptimizerType)(int.Parse(Argument["OPTIMIZER"].Value)); } }

        public static bool CheckGrad { get { return int.Parse(Argument["CHECK-GRAD"].Value) > 0; } }
    }

    public class LSTMSeqLabelBuilder : Builder
    {
        public override BuilderType Type { get { return BuilderType.LSTMSEQLABEL; } }

        public override void InitStartup(string fileName)
        {
            LSTMSeqLabelBuilderParameters.Parse(fileName);
        }

        public static ComputationGraph BuildComputationGraph(DataCashier<SeqSparseDataSource, SequenceDataStat> data, DNNRunMode mode,
            LSTMLabelStructure lstmLabelModel)
        {
            ComputationGraph cg = new ComputationGraph();

            DeviceType device = MathOperatorManager.SetDevice(LSTMSeqLabelBuilderParameters.GPUID);
            CudaMathOperation lib = new CudaMathOperation(true, true);
            RunnerBehavior Behavior = new RunnerBehavior() { RunMode = mode, Device = device, Computelib = lib };

            /// Data Source Runner. 
            SeqSparseDataSource dataSource = (SeqSparseDataSource)cg.AddRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(data, Behavior));

            //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(dataSource.SequenceData, Behavior));
            //SeqDenseBatchData lstmOutput = (SeqDenseBatchData)cg.AddRunner(new LSTMSparseRunner<SeqSparseBatchData>(lstmLabelModel.LSTM.LSTMCells[0], dataSource.SequenceData, SrcHelp, Behavior));
            //for (int i = 1; i < lstmLabelModel.LSTM.LSTMCells.Count; i++)
            //    lstmOutput = (SeqDenseBatchData)cg.AddRunner(new LSTMDenseRunner<SeqDenseBatchData>(lstmLabelModel.LSTM.LSTMCells[i], lstmOutput, SrcHelp, Behavior));

            SeqDenseRecursiveData lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(lstmLabelModel.LSTM.LSTMCells[0], dataSource.SequenceData, false, Behavior));
            for (int i = 1; i < lstmLabelModel.LSTM.LSTMCells.Count; i++)
                lstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmLabelModel.LSTM.LSTMCells[i], lstmOutput, Behavior));

            SeqDenseBatchData lstmOutput2 = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(lstmOutput, Behavior));

            HiddenBatchData lastOutput = (HiddenBatchData)cg.AddRunner(new SeqLabelRunner<SeqDenseBatchData>(lstmLabelModel.Output, lstmLabelModel.StackLen, lstmOutput2, Behavior));

            switch (mode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new CrossEntropyRunner(dataSource.InstanceLabel, lastOutput.Output, lastOutput.Deriv, LSTMSeqLabelBuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new AUCDiskDumpRunner(dataSource.InstanceLabel, lastOutput.Output, LSTMSeqLabelBuilderParameters.ScoreOutputPath, LSTMSeqLabelBuilderParameters.MetricOutputPath));
                    break;
            }
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(LSTMSeqLabelBuilderParameters.LogFile);
            MathOperatorManager.SetDevice(LSTMSeqLabelBuilderParameters.GPUID);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            switch (LSTMSeqLabelBuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    LSTMStructure lstmModel = new LSTMStructure(DataPanel.Train.Stat.FEATURE_DIM, LSTMSeqLabelBuilderParameters.LAYER_DIM, LSTMSeqLabelBuilderParameters.Device);
                    LSTMLabelStructure lstmLabelModel = new LSTMLabelStructure(lstmModel, LSTMSeqLabelBuilderParameters.STACK, 1, LSTMSeqLabelBuilderParameters.Device);
                    lstmLabelModel.InitOptimizer(new StructureLearner()
                    {
                        LearnRate = LSTMSeqLabelBuilderParameters.LearnRate,
                        Optimizer = LSTMSeqLabelBuilderParameters.Optimizer,
                        AdaBoost = LSTMSeqLabelBuilderParameters.AdaBoost,
                        ShrinkLR = LSTMSeqLabelBuilderParameters.ShrinkLR,
                        BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                        EpochNum = LSTMSeqLabelBuilderParameters.Iteration,
                        ClipDelta = LSTMSeqLabelBuilderParameters.DELTACLIP,
                        ClipWeight = LSTMSeqLabelBuilderParameters.WEIGHTCLIP,
                        device = LSTMSeqLabelBuilderParameters.Device
                    });

                    /// Multiple Computation Graphs for Data Partitions on Multiple GPUs.
                    /// Suppose the Computation Graph can be run on only one device.

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, DNNRunMode.Train, lstmLabelModel);
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, lstmLabelModel);


                    if (!Directory.Exists(LSTMSeqLabelBuilderParameters.ModelOutputPath)) Directory.CreateDirectory(LSTMSeqLabelBuilderParameters.ModelOutputPath);

                    for (int iter = 0; iter < LSTMSeqLabelBuilderParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (LSTMSeqLabelBuilderParameters.IsValidFile) predCG.Execute();
                        lstmLabelModel.Save(string.Format(@"{0}\\LSTM.{1}.model", LSTMSeqLabelBuilderParameters.ModelOutputPath, iter.ToString()));
                    }
                    ////DataPanel.Train,
                    break;
                case DNNRunMode.Predict:
                    LSTMLabelStructure validlstmLabelModel = new LSTMLabelStructure(
                        new BinaryReader(new FileStream(LSTMSeqLabelBuilderParameters.ModelOutputPath, FileMode.Open, FileAccess.Read)), LSTMSeqLabelBuilderParameters.Device);
                    
                    if(LSTMSeqLabelBuilderParameters.IsValidFile)
                    {
                        ComputationGraph validCG = BuildComputationGraph(DataPanel.Valid, DNNRunMode.Predict, validlstmLabelModel);
                        validCG.Execute();
                        Logger.WriteLog(string.Join("\n", AUCEvaluationSet.EvaluateAUC(LSTMSeqLabelBuilderParameters.ScoreOutputPath, LSTMSeqLabelBuilderParameters.MetricOutputPath).Item1));
                    }
                    break;
            }

        }


        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Train = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> Valid = null;

            public static void Init()
            {
                if (LSTMSeqLabelBuilderParameters.RunMode == DNNRunMode.Train)
                {
                    Train = new DataCashier<SeqSparseDataSource, SequenceDataStat>(LSTMSeqLabelBuilderParameters.TrainData);
                    Train.InitThreadSafePipelineCashier(100, true);
                }
                if (LSTMSeqLabelBuilderParameters.IsValidFile)
                { 
                    Valid = new DataCashier<SeqSparseDataSource, SequenceDataStat>(LSTMSeqLabelBuilderParameters.ValidData);
                    Valid.InitThreadSafePipelineCashier(100, false);
                }
            }

            internal static void DeInit()
            {
                if (null != Train) Train.Dispose(); Train = null;
                if (null != Valid) Valid.Dispose(); Valid = null;
            }
        }
    }
}
