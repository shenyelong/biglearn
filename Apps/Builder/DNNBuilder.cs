﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{


    public class DNNBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static DNNRunMode RunMode { get { return TrainData.Equals(string.Empty) ? DNNRunMode.Predict : DNNRunMode.Train; } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static string TestData { get { return Argument["TEST"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
            public static bool IsTestFile { get { return !TestData.Equals(string.Empty); } }
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static int FeatureDim { get { return int.Parse(Argument["FEATURE-DIM"].Value); } }
            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static bool IsSparseFeature { get { return int.Parse(Argument["IS-SPARSE-FEA"].Value) > 0; } }


            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }


            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }

            public static float WeightInitBias { get { return float.Parse(Argument["INIT-WEIGHT-BIAS"].Value); } }

            public static float WeightInitScale { get { return float.Parse(Argument["INIT-WEIGHT-SCALE"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            /// <summary>
            /// Only need to Specify Data and Archecture of Deep Nets.
            /// </summary>
            static BuilderParameters()
            {
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "TEST Data."));
                Argument.Add("GPUID", new ParameterArgument("0", "GPU Device ID"));

                Argument.Add("FEATURE-DIM", new ParameterArgument("0", "Feature Dimension."));
                Argument.Add("MINI-BATCH", new ParameterArgument("1024", "Mini-Batch Size."));
                Argument.Add("IS-SPARSE-FEA", new ParameterArgument("1", "Is Sparse Input Feature."));


                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));

                Argument.Add("BATCH-ITERATION", new ParameterArgument("0", "Train Batch Iteration."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));


                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.Regression).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.MSE).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("LAYER-ARCH", new ParameterArgument(string.Empty, ParameterUtil.EnumValues(typeof(N_Type))));
                Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

                Argument.Add("MODEL-PATH", new ParameterArgument(@"\\DEEP-06\Test-JDSSM\Model_Path\", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("INIT-WEIGHT-BIAS", new ParameterArgument("0", "Init Model Weight Bias"));
                Argument.Add("INIT-WEIGHT-SCALE", new ParameterArgument("-1", "Init Model Weight Scale"));
            }
        }

        public override BuilderType Type { get { return BuilderType.DNN; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> feaData, RunnerBehavior Behavior, DNNStructure dnnModel)
        {
            ComputationGraph cg = new ComputationGraph();

            /**************** Get Data from DataCashier *********/
            GeneralBatchInputData fea = (GeneralBatchInputData)cg.AddDataRunner(
                new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(feaData, Behavior));

            /*************** DNN for Source Input. ********************/
            HiddenBatchData output = (HiddenBatchData)cg.AddRunner(
                new DNNRunner<GeneralBatchInputData>(dnnModel, fea, Behavior));

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    switch (BuilderParameters.LossFunction)
                    {
                        case LossFunctionType.BinaryClassification:
                            cg.AddObjective(new CrossEntropyRunner(fea.InstanceLabel, output.Output, output.Deriv, BuilderParameters.Gamma, Behavior));
                            break;
                        case LossFunctionType.MultiClassification:
                            cg.AddObjective(new MultiClassSoftmaxRunner(fea.InstanceLabel, output.Output, output.Deriv, BuilderParameters.Gamma, Behavior));
                            break;
                        case LossFunctionType.Regression:
                            cg.AddObjective(new MSERunner(output.Output, new DenseBatchData(new DenseDataStat() { Dim = 1, MAX_BATCHSIZE = fea.Stat.MAX_BATCHSIZE }, fea.InstanceLabel, Behavior.Device), null, output.Deriv, Behavior));
                            break;
                    }
                    break;
                case DNNRunMode.Predict:
                    switch (BuilderParameters.Evaluation)
                    {
                        case EvaluationType.AUC:
                            cg.AddRunner(new AUCDiskDumpRunner(fea.InstanceLabel, output.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                            break;
                        case EvaluationType.MSE:
                            cg.AddRunner(new RMSEDiskDumpRunner(fea.InstanceLabel, output.Output, 1, false, BuilderParameters.ScoreOutputPath));
                            break;
                        case EvaluationType.ACCURACY:
                            cg.AddRunner(new AccuracyDiskDumpRunner(fea.InstanceLabel, output.Output, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath));
                            break;
                    }
                    break;
            }
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager ComputeLib = MathOperatorManager.CreateInstance(device);
            Logger.WriteLog("Loading DNN Structure.");

            DNNStructure dnnModel = null;
            if (!BuilderParameters.SEED_MODEL.Equals(string.Empty))
            {
                dnnModel = new DNNStructure(BuilderParameters.SEED_MODEL, DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR,
                        device);
            }
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:

                    if (dnnModel == null)
                    {
                        dnnModel = new DNNStructure( DataPanel.Train.Stat.MAX_FEATUREDIM,BuilderParameters.LAYER_DIM,
                                           BuilderParameters.ACTIVATION,BuilderParameters.LAYER_DROPOUT, BuilderParameters.LAYER_BIAS, device);
                    }
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, new RunnerBehavior()
                        { RunMode =  DNNRunMode.Train, Device = device, Computelib = ComputeLib }, dnnModel);

                    ComputationGraph validCG = BuilderParameters.IsValidFile ?
                        BuildComputationGraph(DataPanel.Valid, new RunnerBehavior()
                        { RunMode = DNNRunMode.Predict, Device = device, Computelib = ComputeLib }, dnnModel) : null;

                    ComputationGraph testCG = BuilderParameters.IsTestFile ?
                        BuildComputationGraph(DataPanel.Test, new RunnerBehavior()
                        { RunMode = DNNRunMode.Predict, Device = device, Computelib = ComputeLib }, dnnModel) : null;

                    /// GradientCheck
                    //if (OptimizerParameters.Optimizer == DNNOptimizer.GlobalGradientCheck)
                    //{
                    //    GradientCheckFlow gradCheck = new GradientCheckFlow(dnnModel, MathOperatorManager.MathDevice, trainCG);
                    //    gradCheck.Run();
                    //}

                    trainCG.InitOptimizer(dnnModel, OptimizerParameters.StructureOptimizer, new RunnerBehavior(){ RunMode = DNNRunMode.Train, Device = device, Computelib = ComputeLib });
                    //new StructureLearner()
                    //{
                    //    LearnRate = OptimizerParameters.LearnRate,
                    //    Optimizer = OptimizerParameters.Optimizer,
                    //    ShrinkLR = OptimizerParameters.ShrinkLR,
                    //    BatchNum = DataPanel.Train.Stat.TotalBatchNumber,
                    //    EpochNum = OptimizerParameters.Iteration
                    //}, new RunnerBehavior()
                    //{ RunMode = DNNRunMode.Train, Device = device, Computelib = ComputeLib } );

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1} at:{2}", iter, loss, DateTime.Now);

                        dnnModel.Serialize(FileUtil.CreateBinaryWrite(string.Format(@"{0}\\DNN.{1}.model", BuilderParameters.ModelOutputPath, iter.ToString())));

                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }

                        if (BuilderParameters.IsTestFile) { Logger.WriteLog("Test Score {0} ", testCG.Execute()); }
                    }
                    if (BuilderParameters.IsValidFile)
                        Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                    break;
                case DNNRunMode.Predict:
                    Logger.WriteLog("Loading DNN Structure.");
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.Test, new RunnerBehavior()
                        { RunMode = DNNRunMode.Predict, Device = device, Computelib = ComputeLib }, dnnModel);
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
            Console.WriteLine("All Done at:{0}", DateTime.Now);

        }

        public class DataPanel
        {
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Train = null;

            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Valid = null;

            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Test = null;

            public static void ExtractBinary(string fileName)
            {
                if (File.Exists(fileName + ".fea.bin")) return;

                BinaryWriter dataWriter = new BinaryWriter(new FileStream(fileName + ".fea.bin", FileMode.Create, FileAccess.Write));
                GeneralBatchInputData data = new GeneralBatchInputData(new GeneralBatchInputDataStat() { MAX_FEATUREDIM = BuilderParameters.FeatureDim,
                    MAX_BATCHSIZE = BuilderParameters.MiniBatchSize, FeatureType = BuilderParameters.IsSparseFeature ? FeatureDataType.SparseFeature : FeatureDataType.DenseFeature }, DeviceType.CPU);
                using (StreamReader reader = new StreamReader(fileName))
                {
                    int rowIdx = 0;
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Trim().Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        float l = float.Parse(items[0]);

                        Dictionary<int, float> features = TextUtil.TLCFeatureExtract(items, ref data.Stat.MAX_FEATUREDIM);

                        data.PushSample(features, l);
                        if (data.BatchSize >= BuilderParameters.MiniBatchSize)
                        {
                            data.PopBatchToStat(dataWriter);
                        }
                        if (++rowIdx % 100000 == 0)
                            Console.WriteLine("Extract Row {0} into binary", (rowIdx));
                    }
                    data.PopBatchCompleteStat(dataWriter);
                    Console.WriteLine("Feature Stat {0}", data.Stat.ToString());
                }
            }

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    ExtractBinary(BuilderParameters.TrainData);
                    Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainData + ".fea.bin");
                    Train.InitThreadSafePipelineCashier(128, ParameterSetting.RANDOM_SEED);
                }
                if (BuilderParameters.IsValidFile)
                {
                    ExtractBinary(BuilderParameters.ValidData);
                    Valid = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.ValidData + ".fea.bin");
                    Valid.InitThreadSafePipelineCashier(128, false);
                }

                if (BuilderParameters.IsTestFile)
                {
                    ExtractBinary(BuilderParameters.TestData);
                    Test = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestData + ".fea.bin");
                    Test.InitThreadSafePipelineCashier(128, false);
                }
            }

        }
    }

}
