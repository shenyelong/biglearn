﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

namespace BigLearn.DeepNet
{
    public class DynamicReasonetServer : DynamicObject
    {
        AppReasoNetReaderSQuADV14Builder builder;
        public DynamicReasonetServer(string confFile)
        {
            BuilderParameters.Parse(confFile);
            OptimizerParameters.Parse(confFile);

            ParameterSetting.RANDOM_SEED = BuilderParameters.RandomSeed;
            builder = new AppReasoNetReaderSQuADV14Builder();
            builder.InitStartup(confFile);
            //builder.InitModel();
        }

        public bool Init()
        {
            return builder.InitModel();
        }
        public double Add(double a, double b)
        {
            return builder.Add(a, b);
        }

        public string execute(string dev_vocab, string para, string query, string ans)
        {
            return builder.RunModel(dev_vocab, para, query, ans);
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;
            Console.WriteLine("calling try get memeber");
            switch (binder.Name)
            {
                case "init":
                    result = (Func<bool>)(()
                          => builder.InitModel());
                    return true;
                case "execute":
                    result = (Func<string, string, string, string, string>)((string dev, string para, string ques, string ans)
                          => builder.RunModel(dev, para, ques, ans));
                    return true;
                case "add":
                    result = (Func<double, double, double>)((double a, double b) => builder.Add(a, b));
                    return true; 
            }
            return false;
        }
    }
}
