﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;

namespace BigLearn.DeepNet
{
    public class AppTransEV3Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
            public static int MCTSNum { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int RNN_Dim { get { return int.Parse(Argument["RNN-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int MIN_HOP { get { return int.Parse(Argument["MIN-HOP"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }
            //public static float REWARD_MCTS_DISCOUNT { get { return float.Parse(Argument["REWARD-MCTS-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }
            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int DEV_AS_TRAIN { get { return int.Parse(Argument["DEV-TRAIN"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }
            public static int ActorMiniBatch { get { return int.Parse(Argument["ACTOR-MINI-BATCH"].Value); } }
            public static int ActorPolicy { get { return int.Parse(Argument["ACTOR-POLICY"].Value); } } 
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static int LearnerMiniBatch { get { return int.Parse(Argument["LEARNER-MINI-BATCH"].Value); } }
            public static int LearnerEpoch { get { return int.Parse(Argument["LEARNER-EPOCH"].Value); } }
            public static int ModelSyncUp { get { return int.Parse(Argument["MODEL-SYNCUP"].Value); } }
            public static int ReplayBufferSize { get { return int.Parse(Argument["REPLAY-BUFF-SIZE"].Value); } } 
            public static int TReplaySize { get { return int.Parse(Argument["T-REPLAY-SIZE"].Value); } } 
            public static float ReplayDecay { get { return float.Parse(Argument["REPLAY-DECAY"].Value); } }
            
            public static int NTrial { get { return int.Parse(Argument["NTRIAL"].Value); } }

            public static int TiedEntityEmd { get { return int.Parse(Argument["TIED-ENTITY-EMD"].Value); } }


            public static float EntityEmdDrop { get { return float.Parse(Argument["ENTITY-EMD-DROP"].Value); } }
            public static float RelEmdDrop { get { return float.Parse(Argument["REL-EMD-DROP"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));
                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));


                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
                Argument.Add("MIN-HOP", new ParameterArgument("2", "minimum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
                Argument.Add("MCTS-NUM", new ParameterArgument("0", "Monto Carlo Tree Search Number"));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));

                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                
                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("ALPHA-1", new ParameterArgument("0","max entropy for termination gate."));
                Argument.Add("ALPHA-2", new ParameterArgument("0", "max entropy for action gate."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));

                Argument.Add("ACTOR-MINI-BATCH", new ParameterArgument("128", "actor mini batch size"));
                Argument.Add("ACTOR-POLICY", new ParameterArgument("4", "0:UnifiedSampling; 1:ThompasSampling; 2:UCB; 3:AlphaGoZero1; 4:AlphaGoZero2; 5:PUCB"));

                Argument.Add("LEARNER-MINI-BATCH", new ParameterArgument("32", "learner mini batch size"));
                Argument.Add("LEARNER-EPOCH", new ParameterArgument("5", "learner training epoch size"));
                Argument.Add("MODEL-SYNCUP", new ParameterArgument("3000", "model syncup iteration."));   
                Argument.Add("REPLAY-BUFF-SIZE", new ParameterArgument("3000000", "buffer size for the replay memory."));   
                Argument.Add("T-REPLAY-SIZE", new ParameterArgument("5000", "threshold of replay buffer size."));   
                Argument.Add("REPLAY-DECAY", new ParameterArgument("0.99995", "Replay Decay Value."));   
                Argument.Add("NTRIAL", new ParameterArgument("15", "Negative Trial"));
                Argument.Add("TIED-ENTITY-EMD", new ParameterArgument("0", "0:untied entity embedding; 1:tied entity embedding.") );
                
                Argument.Add("ENTITY-EMD-DROP", new ParameterArgument("0", "dropout rate for entity embedding."));
                Argument.Add("REL-EMD-DROP", new ParameterArgument("0","dropout rate for relation embedding."));

            }
        }

        public override BuilderType Type { get { return BuilderType.APP_TRANSE_V3; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class GraphQueryData : BatchData
        {
            public List<int> RawSource = new List<int>();
            public List<int> RawRel = new List<int>();
            public List<int> RawTarget = new List<int>();
            public List<int> RawIndex = new List<int>();

            // the edge already exists in KB.
            public List<HashSet<int>> BlackTargets = new List<HashSet<int>>();

            public SparseVectorData PosLabels = null;
            public SparseVectorData Mask = null;

            public int BatchSize { get { return RawSource.Count; } }
            public int MaxBatchSize { get; set; }

            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public GraphQueryData(int maxBatchSize, DeviceType device)
            {
                MaxBatchSize = maxBatchSize;
                PosLabels = new SparseVectorData(maxBatchSize * DataPanel.EntityDict.Count, device);
                Mask = new SparseVectorData(maxBatchSize * DataPanel.EntityDict.Count, device);
            }
        }

        /// <summary>
        /// Sample Graph. 
        /// </summary>
        class SampleRunner : StructRunner
        {
            DataEnviroument Data { get; set; }
            public new GraphQueryData Output { get; set; }
            int MaxBatchSize { get; set; }
            int GroupSize { get; set; }

            DataRandomShuffling Shuffle { get; set; }

            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);

            int MiniBatchCounter = 0;

            public SampleRunner(DataEnviroument data, int groupSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                GroupSize = groupSize;
                MaxBatchSize = GroupSize;
                Shuffle = new DataRandomShuffling(Data.Triple.Count, random);
                Output = new GraphQueryData(MaxBatchSize, behavior.Device);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
                MiniBatchCounter = 0;
            }

            public unsafe override void Forward()
            {
                Output.RawRel.Clear();
                Output.RawSource.Clear();
                Output.RawTarget.Clear();
                Output.BlackTargets.Clear();
                Output.RawIndex.Clear();

                MiniBatchCounter += 1;
                int groupIdx = 0;
                int cursor = 0;
                while (groupIdx < GroupSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext(); 
                    if (idx <= -1) { break; }

                    int srcId = -1;
                    int linkId = -1;
                    int tgtId = -1;

                    {
                        srcId = Data.Triple[idx].Item1;
                        linkId = Data.Triple[idx].Item2;
                        tgtId = Data.Triple[idx].Item3;
                    }

                    HashSet<int> truthIds = Data.TruthDict[new Tuple<int, int>(srcId, linkId)];
                    Output.RawRel.Add(linkId);
                    Output.RawSource.Add(srcId);
                    Output.RawIndex.Add(idx);
                    Output.RawTarget.Add(tgtId);
                    Output.BlackTargets.Add(truthIds);

                    int labelBase = groupIdx * DataPanel.EntityDict.Count;

                    Output.PosLabels.Idx.MemPtr[groupIdx] = labelBase + tgtId;
                    Output.PosLabels.Value.MemPtr[groupIdx] = 0.99f;

                    foreach(int t in truthIds)
                    {
                        if(t == tgtId) continue;

                        Output.Mask.Idx.MemPtr[cursor] = labelBase + t;
                        Output.Mask.Value.MemPtr[cursor] = 0;
                        cursor += 1;
                    }
                    groupIdx += 1;
                }
                Output.Mask.Length = cursor;
                Output.Mask.SyncFromCPU();
                
                Output.PosLabels.Length = groupIdx;
                Output.PosLabels.SyncFromCPU();

                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }

        public class EvalRunner : StructRunner
        {
            public Dictionary<string, float> Result { get; set; }
            GraphQueryData Data { get; set; }
            HiddenBatchData Score { get; set; }
            int MiniBatchCounter = 0;
            int Order { get; set; }
            //order = 0: ascedning order; order = 1: descending order;
            public EvalRunner(GraphQueryData data, HiddenBatchData score, int order, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                Score = score;
                Order = order;
                Result = new Dictionary<string, float>();
            }

            public override void Forward()
            {
                DataUtil.HitK(Score, Data.BlackTargets, Data.RawTarget, Order, Result); 
                MiniBatchCounter += 1;
            }
            
            public override void Init()
            {
                Result.Clear();
                MiniBatchCounter = 0;
            }

            public override void Complete()
            {
                float cnt = Result["SMP"];
                foreach(KeyValuePair<string, float> p in Result)
                {
                    Logger.WriteLog("{0} : {1}", p.Key, p.Value * 1.0f / cnt);
                }
            }
        }

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure InNodeEmbed { get; set; }
            public EmbedStructure RelEmbed { get; set; }
            public DNNStructure ScoreDNN { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeDim, int relDim, DeviceType device)
            {
                InNodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                RelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
                
                List<int> mapLayers = new List<int>() { entityNum };
                ScoreDNN = AddLayer(new DNNStructure(nodeDim, mapLayers.ToArray(),
                                           mapLayers.Select(i => A_Func.Linear).ToArray(),
                                           mapLayers.Select(i => true).ToArray(),
                                           device));
            }
            
            public override void CopyFrom(IData other) 
            {
                NeuralWalkerModel src = (NeuralWalkerModel)other;
                InNodeEmbed.CopyFrom(src.InNodeEmbed);
                RelEmbed.CopyFrom(src.RelEmbed);
                ScoreDNN.CopyFrom(src.ScoreDNN);
            }


            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
                InNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); 
                RelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); 
                ScoreDNN = (DNNStructure)DeserializeNextModel(reader, device);
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public static void TransETrainModel(ComputationGraph cg, DataEnviroument data,
                                       int batchSize, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            SampleRunner SmpRunner = new SampleRunner(data, batchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData graphQuery = SmpRunner.Output;

            HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, model.InNodeEmbed, Behavior));
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawRel, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));

            if(BuilderParameters.EntityEmdDrop > 0)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(srcEmbed, BuilderParameters.EntityEmdDrop, Behavior));
            }

            if(BuilderParameters.RelEmdDrop > 0)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(relEmbed, BuilderParameters.RelEmdDrop, Behavior));
            }

            HiddenBatchData ansEmbed = (HiddenBatchData)cg.AddRunner(new AdditionRunner(srcEmbed, relEmbed, Behavior));

            DNNRunner<HiddenBatchData> outputRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, ansEmbed, Behavior);
            cg.AddRunner(outputRunner);
            HiddenBatchData score = outputRunner.Output;

            // obviously the method has a better objective function.
            BinaryCrossEntropyRunner bceLossRunner = new BinaryCrossEntropyRunner(score, graphQuery.PosLabels, graphQuery.Mask, Behavior);
            cg.AddObjective(bceLossRunner);
            
            cg.SetDelegateModel(model);
        }

        // learner model.
        public static void TransEPredModel(ComputationGraph cg, DataEnviroument data,
                                       int batchSize, NeuralWalkerModel model, RunnerBehavior Behavior)
        {
            SampleRunner SmpRunner = new SampleRunner(data, batchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData graphQuery = SmpRunner.Output;

            HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, model.InNodeEmbed, Behavior));
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawRel, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));
            
            HiddenBatchData ansEmbed = (HiddenBatchData)cg.AddRunner(new AdditionRunner(srcEmbed, relEmbed, Behavior));

            DNNRunner<HiddenBatchData> outputRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, ansEmbed, Behavior);
            cg.AddRunner(outputRunner);
            HiddenBatchData score = outputRunner.Output;

            cg.AddRunner(new EvalRunner(graphQuery, score, 1, Behavior));
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(DataPanel.EntityDict.Count, DataPanel.RelationDict.Count, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            Logger.WriteLog("Model Parameter Number {0}", model.ParamNumber);

            ComputationGraph testCG = new ComputationGraph();
            ComputationGraph validCG = new ComputationGraph();
            ComputationGraph trainCG = new ComputationGraph();

            TransEPredModel(testCG, DataPanel.TestData, BuilderParameters.TestMiniBatchSize, model, 
                new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

            TransEPredModel(validCG, DataPanel.DevData, BuilderParameters.TestMiniBatchSize, model, 
                new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    TransETrainModel(trainCG, DataPanel.TrainData, BuilderParameters.ActorMiniBatch, model, 
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    
                    for(int iter = 1; iter <= OptimizerParameters.Iteration; iter++)
                    {
                        trainCG.Execute();
                        if(iter % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            testCG.Execute();
                            validCG.Execute();
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    break;
            }    
            Logger.CloseLog();
        }
        
        class DataUtil 
        {
            public static void HitK(HiddenBatchData score, List<HashSet<int>> black, List<int> label, int order, Dictionary<string, float> result)
            {
                float AP = 0;
                float Hit1 = 0;
                float Hit3 = 0;
                float Hit5 = 0;
                float Hit10 = 0;
                float Hit20 = 0;

                if(!result.ContainsKey("AP")) result["AP"] = 0;
                if(!result.ContainsKey("Hit1")) result["Hit1"] = 0;
                if(!result.ContainsKey("Hit3")) result["Hit3"] = 0;
                if(!result.ContainsKey("Hit5")) result["Hit5"] = 0;
                if(!result.ContainsKey("Hit10")) result["Hit10"] = 0;
                if(!result.ContainsKey("Hit20")) result["Hit20"] = 0;
                if(!result.ContainsKey("SMP")) result["SMP"] = 0;
                
                score.Output.Data.SyncToCPU(score.BatchSize * score.Dim);

                for(int idx = 0; idx < score.BatchSize; idx++)
                {
                    int pos = 0;

                    IEnumerable<int> orders = order == 0 ?
                                                    score.Output.Data.MemPtr.Skip(idx * score.Dim).Take(score.Dim).Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index)
                                                    : score.Output.Data.MemPtr.Skip(idx * score.Dim).Take(score.Dim).Select((item, index) => new { item, index }).OrderBy(a => a.item).Select(a => a.index).Reverse();
                    foreach(int predId in orders)
                    {
                        if(black[idx].Contains(predId) && label[idx] != predId)
                        {
                            continue;
                        }
                        pos += 1;
                        if (label[idx] == predId)
                        {
                            AP += 1.0f / pos;
                            if(pos <= 1) { Hit1 += 1; }
                            if (pos <= 3) { Hit3 += 1; }
                            if (pos <= 5) { Hit5 += 1; }
                            if (pos <= 10) { Hit10 += 1; }
                            if (pos <= 20) { Hit20 += 1; }
                            break;
                        }
                    }
                }

                result["AP"] += AP;
                result["Hit1"] += Hit1;
                result["Hit3"] += Hit3;
                result["Hit5"] += Hit5;
                result["Hit10"] += Hit10;
                result["Hit20"] += Hit20;
                result["SMP"] += score.BatchSize;
            }
        }

        public class DataPanel 
        {
            public static DataEnviroument TrainData { get; set; }
            public static DataEnviroument TestData { get; set; }
            public static DataEnviroument DevData { get; set; }

            public static Dictionary<string, int> EntityDict { get; set; }
            public static Dictionary<string, int> RelationDict { get; set; }

            public static Dictionary<int, string> EntityLookup { get; set; }
            public static Dictionary<int, string> RelationLookup { get; set; }

            public static void Init()
            {
                EntityDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + "/vocab/entity_vocab.json"));
                RelationDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + "/vocab/relation_vocab.json"));

                EntityLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in EntityDict)
                {
                    EntityLookup.Add(item.Value, item.Key);
                }

                RelationLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in RelationDict)
                {
                    RelationLookup.Add(item.Value, item.Key);
                }

                TrainData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + "/train.txt",
                                            new string[] { BuilderParameters.InputDir + "/train.txt" });

                TestData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + "/test.txt",
                                            new string[] { BuilderParameters.InputDir + "/test.txt",
                                                           BuilderParameters.InputDir + "/train.txt",
                                                           BuilderParameters.InputDir + "/dev.txt" });

                DevData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + "/dev.txt",
                                            new string[] { BuilderParameters.InputDir + "/test.txt",
                                                           BuilderParameters.InputDir + "/train.txt",
                                                           BuilderParameters.InputDir + "/dev.txt" });
            }
        }

        public class DataEnviroument 
        {
            public List<Tuple<int, int, int>> Triple { get; set; }
            public Dictionary<Tuple<int, int>, HashSet<int>> TruthDict { get; set; }
            public List<Tuple<int, int>> Grouple { get; set; }

            public DataEnviroument(Dictionary<string, int> entityDict, Dictionary<string, int> relationDict, string tripleFile, string[] graphFiles)
            {
                Triple = new List<Tuple<int, int, int>>();
                TruthDict = new Dictionary<Tuple<int, int>, HashSet<int>>();
                Grouple = new List<Tuple<int, int>>();

                int missNum = 0;
                using (StreamReader tripleReader = new StreamReader(tripleFile))
                {
                    while (!tripleReader.EndOfStream)
                    {
                        string[] tokens = tripleReader.ReadLine().Split('\t');

                        if (!entityDict.ContainsKey(tokens[0]))
                        {
                            //Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                            missNum += 1;
                            continue;
                        }

                        if (!relationDict.ContainsKey(tokens[1]))
                        {
                            //Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                            missNum += 1;
                            continue;
                        }

                        if (!entityDict.ContainsKey(tokens[2]))
                        {
                            //Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                            missNum += 1;
                            continue;
                        }

                        int e1 = entityDict[tokens[0]];
                        int r = relationDict[tokens[1]];
                        int e2 = entityDict[tokens[2]];

                        Triple.Add(new Tuple<int, int, int>(e1, r, e2));
                    }
                    Logger.WriteLog("{0} Graph miss connections {1}", tripleFile, missNum);
                }

                foreach (string gStr in graphFiles)
                {
                    using (StreamReader gReader = new StreamReader(gStr))
                    {
                        while (!gReader.EndOfStream)
                        {
                            string[] tokens = gReader.ReadLine().Split('\t');

                            if (!entityDict.ContainsKey(tokens[0]))
                            {
                                //Console.WriteLine("Graph doesn't exist node {0}", tokens[0]);
                                continue;
                            }

                            if (!relationDict.ContainsKey(tokens[1]))
                            {
                                //Console.WriteLine("Graph doesn't exist relation {0}", tokens[1]);
                                continue;
                            }

                            if (!entityDict.ContainsKey(tokens[2]))
                            {
                                //Console.WriteLine("Graph doesn't exist node {0}", tokens[2]);
                                continue;
                            }

                            int e1 = entityDict[tokens[0]];
                            int r = relationDict[tokens[1]];
                            int e2 = entityDict[tokens[2]];

                            Tuple<int, int> gKey = new Tuple<int, int>(e1, r);

                            if (!TruthDict.ContainsKey(gKey))
                            {
                                TruthDict.Add(gKey, new HashSet<int>());
                                Grouple.Add(gKey);
                            }
                            if(!TruthDict[gKey].Contains(e2)) TruthDict[gKey].Add(e2);
                        }
                    }
                }
            }
        }

    }
}
