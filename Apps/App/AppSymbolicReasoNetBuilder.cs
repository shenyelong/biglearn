﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;

namespace BigLearn.DeepNet
{
    
    public class AppSymbolicReasoNetBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID."));

                Argument.Add("QUERY-SET",new ParameterArgument("100000", "Query Set."));
                Argument.Add("CONTAIN-LIM", new ParameterArgument("20", "Contain Lim."));
                Argument.Add("MAX-SEARCH-LEN", new ParameterArgument("10", "Contain Lim."));
                
                Argument.Add("BATCH-SIZE", new ParameterArgument("16", "Mini Batch Size"));

                Argument.Add("SCHEDULE-EPSILON", new ParameterArgument("0:1,3000000:0.1,10000000:0.001,50000000:0.000001", "Epsilon Schedule"));

                

                // Map Input to Status. 
                Argument.Add("SX", new ParameterArgument(string.Empty, "Convolutional Filter SX"));
                Argument.Add("STRIDE", new ParameterArgument(string.Empty, "Convolutional Filter Stride"));
                Argument.Add("O", new ParameterArgument(string.Empty, "Convolutional Output Dim"));
                Argument.Add("PAD", new ParameterArgument(string.Empty, "Pad space for image"));
                Argument.Add("AF", new ParameterArgument(string.Empty, "Activation functions"));
                Argument.Add("POOLSX", new ParameterArgument(string.Empty, "Pooling SX"));
                Argument.Add("POOLSTRIDE", new ParameterArgument(string.Empty, "Pooling Stride"));

                Argument.Add("RECURRENT-T", new ParameterArgument("10,10,1", "Recurrent Terminate Net."));

                Argument.Add("EMBED-DIM", new ParameterArgument("32", "memory size"));
                Argument.Add("STATUS-DIM", new ParameterArgument("128", "memory size"));

                Argument.Add("MEM-ATT-GAMMA", new ParameterArgument("10", " attention gamma"));
                Argument.Add("MEM-ATT-HIDDEN", new ParameterArgument("128", " attention hidden size."));
                Argument.Add("MEM-ATT-TYPE", new ParameterArgument("1", "0:inner product; 1: weighted inner product;"));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("10", "Reasoning Steps."));

                Argument.Add("INIT-STATUS", new ParameterArgument("0", "0:Fix Init Status; 1: Random Init Status;"));

                //Argument.Add("TRAIN-MINI-BATCH", new ParameterArgument("64", "Mini Batch for Train."));
                //Argument.Add("DEV-MINI-BATCH", new ParameterArgument("32", "Mini Batch for Dev."));

                //Argument.Add("MAX-TRAIN-SRC-LENGTH", new ParameterArgument("50", "MAX src length."));
                //Argument.Add("MAX-TRAIN-TGT-LENGTH", new ParameterArgument("52", "MAX tgt length."));

                /////Language Word Error Rate.
                //Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));


                //Argument.Add("SRC-EMBED", new ParameterArgument("620", "Src String Dim"));
                //Argument.Add("SRC-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));

                //Argument.Add("TGT-EMBED", new ParameterArgument("620", "Tgt String Dim"));
                //Argument.Add("TGT-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));

                //Argument.Add("IS-SHARE-EMBED", new ParameterArgument("0", "0 : nonshared input and output; 1 : shared input and output;"));
                //Argument.Add("TGT-DECODE-EMBED", new ParameterArgument("620", "Tgt String  Decode Dim"));

                //Argument.Add("CONTEXT-LEN", new ParameterArgument("0", "Context Vector length"));
                //Argument.Add("DECODE-DIM", new ParameterArgument("200", "Decoding Dimension"));
                /////Softmax Randomup.
                //Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                //Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                //Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
                //Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                //Argument.Add("ENSEMBLE-RL", new ParameterArgument("0", "0:max terminate prob; 1:average terminate prob."));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                //Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static int QUERY_SET { get { return int.Parse(Argument["QUERY-SET"].Value); } }
            public static int CONTAIN_LIM { get { return int.Parse(Argument["CONTAIN-LIM"].Value); } }
            public static int MAX_SEARCH_LEN { get { return int.Parse(Argument["MAX-SEARCH-LEN"].Value); } }

            public static string QUERY_PATH { get { return string.Format("Q.{0}.{1}.{2}.set", QUERY_SET, CONTAIN_LIM, MAX_SEARCH_LEN); } }

            public static List<Tuple<int, float>> ScheduleEpsilon
            {
                get
                {
                    return Argument["SCHEDULE-EPSILON"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }
            public static float Epsilon(int step)
            {
                for (int i = 0; i < ScheduleEpsilon.Count; i++)
                {
                    if (step < ScheduleEpsilon[i].Item1)
                    {
                        float lambda = (step - ScheduleEpsilon[i - 1].Item1) * 1.0f / (ScheduleEpsilon[i].Item1 - ScheduleEpsilon[i - 1].Item1);
                        return lambda * ScheduleEpsilon[i].Item2 + (1 - lambda) * ScheduleEpsilon[i - 1].Item2;
                    }
                }
                return ScheduleEpsilon.Last().Item2;
            }


            public static int BatchSize { get { return int.Parse(Argument["BATCH-SIZE"].Value); } }

            public static int[] SX { get { return Argument["SX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] Stride { get { return Argument["STRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] O { get { return Argument["O"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] Pad { get { return Argument["PAD"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] Afs { get { return Argument["AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] PoolSX { get { return Argument["POOLSX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] PoolStride { get { return Argument["POOLSTRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] Recurrent_T { get { return Argument["RECURRENT-T"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int StatusDim { get { return int.Parse(Argument["STATUS-DIM"].Value); } }
            public static int EmbedDim { get { return int.Parse(Argument["EMBED-DIM"].Value); } }

            public static float MemAttGamma { get { return float.Parse(Argument["MEM-ATT-GAMMA"].Value); } }
            public static int MemAttHidden { get { return int.Parse(Argument["MEM-ATT-HIDDEN"].Value); } }
            public static int MemAttType { get { return int.Parse(Argument["MEM-ATT-TYPE"].Value); } }

            public static int Recurrent_Step { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }

            public static int Init_Status { get { return int.Parse(Argument["INIT-STATUS"].Value); } }

            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.APP_SYMBOLIC_REASONET; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }


        public static ComputationGraph BuildActorGraph(string queryPath, DNNStructure stateDNN, DNNStructure tgtDNN, DNNStructure scoreDNN,
            GRUCell gru, DNNStructure vDNN, RunnerBehavior behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            SymbolicDataRunner dataRunner = new SymbolicDataRunner(BuilderParameters.BatchSize, queryPath, true, behavior)  { IsBackProp = false };
            cg.AddDataRunner(dataRunner);

            List<List<SymbolicState>> query_seq = new List<List<SymbolicState>>();
            List<HiddenBatchData> state_seq = new List<HiddenBatchData>();
            List<HiddenBatchData> v_seq = new List<HiddenBatchData>();

            // Init query and state.
            List<SymbolicState> query = dataRunner.OutQueries;
            HiddenBatchData state = (HiddenBatchData)cg.AddRunner(new SymbolicStateRunner(query, BuilderParameters.BatchSize, stateDNN, behavior));
            HiddenBatchData v = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(vDNN, state, behavior));
            
            query_seq.Add(query);
            state_seq.Add(state);
            v_seq.Add(v);
            
            for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
            {
                StateActorRunner actorRunner = new StateActorRunner(query, state, tgtDNN, scoreDNN, behavior);
                cg.AddRunner(actorRunner);
                List<SymbolicState> answer = actorRunner.Answer;

                HiddenBatchData x = (HiddenBatchData)cg.AddRunner(new SymbolicStateRunner(answer, BuilderParameters.BatchSize, stateDNN, behavior));

                GRUQueryRunner queryRewriteRunner = new GRUQueryRunner(gru, state, x, behavior);
                cg.AddRunner(queryRewriteRunner);
                HiddenBatchData newState = queryRewriteRunner.Output;

                HiddenBatchData newV = (HiddenBatchData)cg.AddRunner(new DNNRunner<HiddenBatchData>(vDNN, newState, behavior));

                state = newState;
                query = answer;
                v = newV;

                query_seq.Add(query);
                state_seq.Add(state);
                v_seq.Add(v);
            }

            //float lambda = BuilderParameters.RL_DISCOUNT;
            //for (int i = BuilderParameters.Recurrent_Step - 1; i >= 0; i--)
            //{
            //    rewards_seq[i]
            //}

            return cg;
        }

        class RewardVDerivRunner : StructRunner
        {
            List<List<SymbolicState>> QuerySeq;
            List<HiddenBatchData> StateSeq;
            List<HiddenBatchData> VSeq;

            public RewardVDerivRunner(List<List<SymbolicState>> query_seq, List<HiddenBatchData> state_seq, List<HiddenBatchData> v_seq, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                QuerySeq = query_seq;
                StateSeq = state_seq;
                VSeq = v_seq;
            }

            //unsafe public override void Forward()
            //{
            //    Output.BatchSize = Queries.Count;
            //    for (int i = 0; i < Queries.Count; i++)
            //    {
            //        Array.Copy(Queries[i].Fea, 0, XVec.Data.MemPtr, i * SymbolicState.StateDim, SymbolicState.StateDim);
            //    }
            //    XVec.Data.SyncFromCPU();
            //    Runner.Forward();
            //}

            //public override void Backward(bool cleanDeriv)
            //{
            //    Runner.Backward(cleanDeriv);
            //}

            //public override void CleanDeriv()
            //{
            //    Runner.CleanDeriv();
            //}

            //public override void Update()
            //{
            //    Runner.Update();
            //}
        }

        public class Actor
        {
            public virtual SymbolicState Action(SymbolicState s)
            {
                throw new NotImplementedException();
            }

            public virtual void Feedback(List<SymbolicState> path, float reward)
            {
                throw new NotImplementedException();
            }
        }

        public class RandomActor : Actor
        {
            Random random = new Random();
            public RandomActor() { }
            public override SymbolicState Action(SymbolicState s)
            {
                List<SymbolicState> results = s.NeighborStates();
                int idx = random.Next(results.Count);
                return results[idx];
            }
            public override void Feedback(List<SymbolicState> path, float reward)
            { }
        }

        public static int UCBBandit(List<Tuple<float, float>> arms, float c, Random random)
        {
            float log_total = (float) Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);
            List<float> v = new List<float>();
            foreach (Tuple<float, float> arm in arms)
            {
                v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f))  + (float)random.NextDouble() * 0.0001f);
            }
            int idx = Util.MaximumValue(v.ToArray());
            return idx;
        }

        /// <summary>
        /// Monte Carlo Tree Search.
        /// </summary>
        public class MonteCarloActor : Actor
        {
            Dictionary<string, Tuple<float, float>> Mem = new Dictionary<string, Tuple<float, float>>();
            Random random = new Random();
            float c = 1.0f;

            public override SymbolicState Action(SymbolicState s)
            {
                List<SymbolicState> neighbors = s.NeighborStates();
                List<Tuple<float, float>> arms = new List<Tuple<float, float>>();
                int nIdx = 0;
                foreach (SymbolicState n in neighbors)
                {
                    arms.Add(LookUp(n.StateKey));
                    nIdx += 1;
                }

                int idx = UCBBandit(arms, c, random);
                return neighbors[idx];
            }

            public Tuple<float, float> LookUp(string key)
            {
                if (!Mem.ContainsKey(key))
                {
                    Mem.Add(key, new Tuple<float, float>(0, 0));
                }
                return Mem[key];
            }

            public void Write(string key, float reward)
            {
                Mem[key] = new Tuple<float, float>(Mem[key].Item1 + reward, Mem[key].Item2 + 1);
            }

            public override void Feedback(List<SymbolicState> path, float reward)
            {
                foreach (SymbolicState s in path)
                {
                    Write(s.StateKey, reward);
                }
            }
        }

        /// <summary>
        /// Differentiable neural dictionary.
        /// </summary>
        public class DND
        {
            int KeyDim { get; set; }
            int TopK = 50;
            public DND(int keyDim)
            {
                KeyDim = keyDim;
            }

            List<Tuple<float[], float>> NeuralDictionary = new List<Tuple<float[], float>>();

            float GaussianKernel(float[] key1, float[] key2)
            {
                float d = 0;
                for (int i = 0; i < key1.Length; i++)
                {
                    d = (key1[i] - key2[i]) * (key1[i] - key2[i]);
                }
                return 1.0f / (d + 0.001f);
            }

            public float Lookup(float[] key)
            {
                MinMaxHeap<int> heap = new MinMaxHeap<int>(TopK, 1);
                for (int i = 0; i < NeuralDictionary.Count; i++)
                {
                    float v = GaussianKernel(key, NeuralDictionary[i].Item1);
                    heap.push_pair(i, v);
                }
                float sum_k = heap.topK.Select(i => i.Value).Sum();
                float sum_v = 0;
                foreach(KeyValuePair<int,float> item in heap.topK)
                {
                    sum_v += item.Value / sum_k * NeuralDictionary[item.Key].Item2;
                }
                return sum_v;
            }

            public float Write(float[] key, float v)
            {
                return 0;
            }

            //public void Lookup()
        }

        /// <summary>
        /// Neural Episodic Control.
        /// </summary>
        public class NeuralEpisodicActor : Actor
        {
            Dictionary<string, Tuple<float, float>> Mem = new Dictionary<string, Tuple<float, float>>();
            Random random = new Random();
            float c = 1.0f;
            float lr = 0.5f;
            int poscorrect = 0;
            int negcorrect = 0;
            int poswrong = 0;
            int negwrong = 0;
            bool CanSimulate = false;
            // Tomson Sampling.
            // Q_t = \lambda Q_{t+1} + r_t
            // 
            // variance = sqrt( (x-v)^2 ) 

            RunnerBehavior Behavior = null;
            DNNStructure Model = null;
            DNNRunner<HiddenBatchData> Runner = null;
            HiddenBatchData Input = null;
            
            ComputationGraph TrainCG = null;
            HiddenBatchData TrainInput = null;
            HiddenBatchData TrainOutput = null;
            DenseBatchData TrainLabel = null;
            int BatchSize = 1024;

            public NeuralEpisodicActor(RunnerBehavior rb)
            {
                Behavior = rb;
                Model = new DNNStructure(SymbolicState.Fea2Dim, new int[] { 2 },
                    new A_Func[] { A_Func.Linear }, new bool[] { true }, Behavior.Device);
                Model.Init(0.02f, -0.01f);
                //Model.neurallinks[0].weight.MemPtr[0] = -100;
                //Model.neurallinks[0].weight.MemPtr[1] = 100;

                //Model.neurallinks[0].weight.MemPtr[2] = -100;
                //Model.neurallinks[0].weight.MemPtr[3] = 100;

                //Model.neurallinks[0].weight.MemPtr[4] = -100;
                //Model.neurallinks[0].weight.MemPtr[5] = 100;

                //Model.neurallinks[0].weight.SyncFromCPU();

                //Model.neurallinks[0].bias.MemPtr[0] = 100;
                //Model.neurallinks[0].bias.MemPtr[1] = 0;
                //Model.neurallinks[0].bias.SyncFromCPU();



                Input = new HiddenBatchData(1, SymbolicState.Fea2Dim, new CudaPieceFloat(SymbolicState.Fea2Dim, Behavior.Device), null, Behavior.Device);
                Runner = new DNNRunner<HiddenBatchData>(Model, Input, Behavior);

                TrainInput = new HiddenBatchData(BatchSize, SymbolicState.Fea2Dim, new CudaPieceFloat(SymbolicState.Fea2Dim * BatchSize, Behavior.Device), null, Behavior.Device);
                TrainLabel = new DenseBatchData(BatchSize, 1, Behavior.Device);
                Model.InitOptimizer(OptimizerParameters.StructureOptimizer, Behavior);

                TrainCG = new ComputationGraph();
                /*************** DNN for Source Input. ********************/
                TrainOutput = (HiddenBatchData)TrainCG.AddRunner(new DNNRunner<GeneralBatchInputData>(Model, TrainInput, Behavior));
                //TrainCG.AddObjective(new MSERunner(TrainOutput.Output, TrainLabel, CudaPieceFloat.Empty, TrainOutput.Deriv, Behavior));
                //TrainCG.AddObjective(new CrossEntropyRunner(TrainLabel.Data, TrainOutput.Output, TrainOutput.Deriv, 1, Behavior));
                TrainCG.AddObjective(new MultiClassSoftmaxRunner(TrainLabel.Data, TrainOutput.Output, TrainOutput.Deriv, 1, Behavior));

                TrainCG.SetDelegateModel(Model);

            }

            bool RandomT(float t)
            {
                if (t > 0.5f) { return true; }
                else return false;

                //if (random.NextDouble() < t) { return true; }
                //return false;
            }

            public float ComputeTQ(SymbolicState s)
            {
                float[] fea = s.Fea2;
                for (int i = 0; i < fea.Length; i++)
                {
                    Input.Output.Data.MemPtr[i] = fea[i];
                }
                Input.BatchSize = 1;
                Input.Output.Data.SyncFromCPU();
                Runner.Forward();
                Runner.Output.Output.Data.SyncToCPU();
                float[] h = Runner.Output.Output.Data.MemPtr;
                h = Util.Softmax(h, 1);
                return h[1]; // Util.Logistic(h[0]);
            }

            int simulate = 50;
            int depth = 12;

            public SymbolicState Next(SymbolicState s)
            {
                List<SymbolicState> neighbors = s.NeighborStates();
                List<Tuple<float, float>> arms = new List<Tuple<float, float>>();
                int nIdx = 0;
                foreach (SymbolicState n in neighbors)
                {
                    arms.Add(LookUp(n.StateKey));
                    nIdx += 1;
                }

                int idx = UCBBandit(arms, c, random);
                return neighbors[idx];
            }

            public override SymbolicState Action(SymbolicState s)
            {
                // simulation.
                if (CanSimulate)
                {
                    for (int e = 0; e < simulate; e++)
                    {
                        List<SymbolicState> path = new List<SymbolicState>();

                        SymbolicState q = s;
                        float log_p = 0;
                        float reward = 0;
                        for (int d = 0; d < depth; d++)
                        {
                            path.Add(q);
                            float r = ComputeTQ(q);
                            if (RandomT(r)) { reward = 1; log_p += (float)Math.Log(poscorrect / (poscorrect + negwrong + 0.001f) + 0.0001f); break; }
                            log_p += (float)Math.Log(negcorrect / (negcorrect + poswrong + 0.001f) + 0.0001f);
                            q = Next(q);
                        }

                        //logP record the variance of the reward.

                        Feedback(path, reward * (float)Math.Exp(log_p) * lr);
                    }
                }

                return Next(s);
            }

            public Tuple<float, float> LookUp(string key)
            {
                if (!Mem.ContainsKey(key))
                {
                    Mem.Add(key, new Tuple<float, float>(0, 0));
                }
                return Mem[key];
            }

            public void Write(string key, float reward)
            {
                Mem[key] = new Tuple<float, float>(Mem[key].Item1 + reward, Mem[key].Item2 + 1);
            }

            public override void Feedback(List<SymbolicState> path, float reward)
            {
                foreach (SymbolicState s in path)
                {
                    Write(s.StateKey, reward);
                }

                //PushReplay(path, reward);
            }

            Queue<Tuple<SymbolicState, float>> ReplayMem = new Queue<Tuple<SymbolicState, float>>();
            
            public void PushReplay(List<SymbolicState> path, float feedback)
            {
                for (int i = 0; i < path.Count - 1; i++)
                {
                    ReplayMem.Enqueue(new Tuple<SymbolicState, float>(path[i], 0));
                }
                ReplayMem.Enqueue(new Tuple<SymbolicState, float>(path.Last(), feedback));

                if (ReplayMem.Count >= 5 * BatchSize)
                {
                    Model.neurallinks[0].weight.SyncToCPU();
                    Model.neurallinks[0].bias.SyncToCPU();
                    for (int i = 0; i < BatchSize; i++)
                    {
                        int idx = random.Next(ReplayMem.Count);

                        float[] fea = ReplayMem.ElementAt(idx).Item1.Fea2;
                        for (int f = 0; f < SymbolicState.Fea2Dim; f++)
                        {
                            TrainInput.Output.Data.MemPtr[i * SymbolicState.Fea2Dim + f] = fea[f];
                        }

                        TrainLabel.Data.MemPtr[i] = ReplayMem.ElementAt(idx).Item2;

                        //if(TrainLabel.Data.MemPtr[i] > 0)
                        //{
                        //    Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX {0}", i);
                        //}
                    }
                    TrainInput.BatchSize = BatchSize;
                    TrainLabel.BatchSize = BatchSize;
                    TrainInput.Output.Data.SyncFromCPU();
                    TrainLabel.Data.SyncFromCPU();
                    TrainCG.Step();
                    double loss = TrainCG.StepLoss;
                    TrainOutput.Output.Data.SyncToCPU();

                    for (int i = 0; i < BatchSize; i++)
                    {
                        float r = Util.Softmax(new float[] { TrainOutput.Output.Data.MemPtr[i * 2 + 0], TrainOutput.Output.Data.MemPtr[i * 2 + 1] }, 1)[1];
                        float reward = RandomT(r) ? 1 : 0;

                        if (TrainLabel.Data.MemPtr[i] > 0)
                        {
                            if (reward > 0) { poscorrect += 1; }
                            else { poswrong += 1; }
                        }
                        else
                        {
                            if (reward <= 0) { negcorrect += 1; }
                            else { negwrong += 1; }
                        }
                    }
                    Console.WriteLine("Pos_Correct {0}, Neg_Correct {1}, Pos_Wrong {2}, Neg_Wrong {3}, Loss {4}", poscorrect, negcorrect, poswrong, negwrong, loss);

                    if (poscorrect / (poscorrect + negwrong + 0.001f) > 0.75f && negcorrect / (negcorrect + poswrong + 0.001f) > 0.75f) { CanSimulate = true; }
                    else { CanSimulate = false; }
                }

                while (ReplayMem.Count >= 100000)
                {
                    ReplayMem.Dequeue();
                }
                /// train model.
            }
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");
            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");
            Console.SetWindowSize(
                Math.Min(150, Console.LargestWindowWidth),
                Math.Min(60, Console.LargestWindowHeight));


            //RandomActor actor = new RandomActor();
            Actor actor = new NeuralEpisodicActor(new RunnerBehavior()
            { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }); //MonteCarloActor(); //RandomActor(); // 
            int monte_carlo_search = 100;

            for (int iter = 0; iter < 10; iter++)
            {
                Console.WriteLine("Iteration {0}", iter);
                List<int> avgMonteCarlo = new List<int>();
                List<int> avgSearchDepth = new List<int>();
                foreach (SymbolicState q in DataPanel.Query)
                {
                    // maximum recurrent step;
                    bool IsSuccess = false;
                    int epoch = 0;
                    int successDepth = -1;
                    for (epoch = 0; epoch < monte_carlo_search; epoch++)
                    {
                        SymbolicState state = q;
                        List<SymbolicState> path = new List<SymbolicState>();
                        successDepth = -1;
                        IsSuccess = false;
                        for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
                        {
                            state = actor.Action(state);
                            path.Add(state);
                            IsSuccess = state.IsSuccess;
                            if (IsSuccess) { break; }
                        }

                        int reward = 0;
                        if (IsSuccess) { reward = 1; }

                        actor.Feedback(path, reward);
                        ((NeuralEpisodicActor)actor).PushReplay(path, reward);
                        if (IsSuccess)
                        {
                            successDepth = path.Count;
                            break;
                        }
                    }

                    if (!IsSuccess) avgMonteCarlo.Add(-1);
                    else avgMonteCarlo.Add(epoch);

                    if (!IsSuccess) avgSearchDepth.Add(-1);
                    else avgSearchDepth.Add(successDepth);

                    int notComplete = avgMonteCarlo.Select(i => i == -1 ? 1 : 0).Sum();
                    int Complete = avgMonteCarlo.Select(i => i > -1 ? 1 : 0).Sum();
                    int totalLen = avgMonteCarlo.Select(i => i > -1 ? i : 0).Sum();

                    int totalSearchDepth = avgSearchDepth.Select(i => i > -1 ? i : 0).Sum();

                    Console.WriteLine("{0} Not Completed, {1} Completed, {2} AvgSearchLen, {3} AvgSearchDepth, {4} CurrentSearchEpoch, {5} Depth",
                        notComplete, Complete, totalLen / (Complete + 0.0001f), totalSearchDepth / (Complete + 0.0001f),  epoch, successDepth);
                }
            }
            Logger.CloseLog();
        }

        public class SymbolicState
        {
            public int A;
            public int a = 0;
            public int B;
            public int b = 0;
            public int C;
            public int c = 0;
            public int Q;
            public int Step = 0;
            
            public string QueryKey { get { return string.Format("{0}\t{1}\t{2}\t{3}", Q, A, B, C); } }
            public string StateKey { get { return string.Format("{0}#{1}#{2}#{3}\t{4}\t{5}\t{6}", Q, A, B, C, a, b, c); } }

            public static int MaxNeighborNum { get { return 12; } }

            public static int FeaDim = 7;
            public float[] Fea { get { return new float[] { Q, A, a, B, b, C, c }; } }

            public static int Fea2Dim = 3;
            public float[] Fea2 { get { return new float[] { a == Q ? 1 : 0, b == Q ? 1 : 0, c == Q ? 1 : 0 }; } }


            public SymbolicState() { }

            public SymbolicState(string mkey)
            {
                string[] items = mkey.Split('\t');
                Q = int.Parse(items[0]);
                A = int.Parse(items[1]);
                B = int.Parse(items[2]);
                C = int.Parse(items[3]);
            }

            public SymbolicState(Random random, HashSet<string> keys)
            {
                while (true)
                {
                    Q = random.Next(1, 20);
                    A = random.Next(1, 20);
                    B = random.Next(1, 20);
                    C = random.Next(1, 20);
                    if (keys.Contains(this.QueryKey)) continue;
                    if (Q == A || Q == B || Q == C) continue;
                    if (Q > A && Q > B && Q > C) continue;
                    Queue<SymbolicState> stacks = new Queue<SymbolicState>();
                    stacks.Enqueue(this);

                    while(stacks.Count > 0)
                    {
                        SymbolicState s = stacks.Dequeue();
                        if (s.IsSuccess) { return; }
                        if (s.Step >= 5) break;
                        List<SymbolicState> neighbors = s.NeighborStates();
                        foreach(SymbolicState ns in neighbors) stacks.Enqueue(ns);
                    }
                }
            }

            public SymbolicState Transform(int aa, int bb, int cc, int s)
            {
                return new SymbolicState() { A = A, B = B, C = C, a = aa, b = bb, c = cc, Q = Q, Step = s + 1 }; 
            }
            
            public bool IsSuccess { get { if (Q == a || Q == b || Q == c) return true; return false; } }

            public List<SymbolicState> NeighborStates()
            {
                List<SymbolicState> results = new List<SymbolicState>();

                // A -> Full;
                // A -> empty;
                // A -> B;
                // A -> C;
                if (a < A) { results.Add(Transform(A, b, c, Step)); }
                if (a > 0) { results.Add(Transform(0, b, c, Step)); }
                if (a > 0 && b < B) { if (a + b >= B) results.Add(Transform(a + b - B, B, c, Step)); else results.Add(Transform(0, a + b, c, Step)); }
                if (a > 0 && c < C) { if (a + c >= C) results.Add(Transform(a + c - C, b, C, Step)); else results.Add(Transform(0, b, a + c, Step)); }

                // B -> Full;
                // B -> empty;
                // B -> A;
                // B -> C;
                if( b < B) { results.Add(Transform(a, B, c, Step)); }
                if (b > 0) { results.Add(Transform(a, 0, c, Step)); }
                if (b > 0 && a < A) { if (a + b >= A) results.Add(Transform(A, a + b - A, c, Step)); else results.Add(Transform(a + b, 0, c, Step)); }
                if (b > 0 && c < C) { if (b + c >= C) results.Add(Transform(a, b + c - C, C, Step)); else results.Add(Transform(a, 0, b + c, Step)); }

                // C -> Full;
                // C -> empty;
                // C -> A;
                // C -> B;
                if( c < C) { results.Add(Transform(a, b, C, Step)); }
                if (c > 0) { results.Add(Transform(a, b, 0, Step)); }
                if (c > 0 && a < A) { if (a + c >= A) results.Add(Transform(A, b, a + c - A, Step)); else results.Add(Transform(a + c, b, 0, Step)); }
                if (c > 0 && b < B) { if (b + c >= B) results.Add(Transform(a, B, b + c - B, Step)); else results.Add(Transform(a, b + c, 0, Step)); }

                // doing nothing;
                //results.Add(Transform(a, b, c, Step));

                return results;
            }
        }

        public class SymbolicStateRunner : StructRunner
        {
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } protected set { base.Output = value; } }
            List<SymbolicState> Queries = null;
            DNNStructure DNN;
            DenseBatchData XVec;
            DNNRunner<HiddenBatchData> Runner = null;
            public SymbolicStateRunner(List<SymbolicState> input, int maxBatchSize, DNNStructure dnn, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Queries = input;
                XVec = new DenseBatchData(maxBatchSize, SymbolicState.FeaDim, behavior.Device);
                DNN = dnn;
                Runner = new DNNRunner<HiddenBatchData>(DNN, new HiddenBatchData(maxBatchSize, XVec.Stat.Dim, XVec.Data, null, Behavior.Device), Behavior);
                Output = Runner.Output;
            }

            unsafe public override void Forward()
            {
                Output.BatchSize = Queries.Count;
                for (int i = 0; i < Queries.Count; i++)
                {
                    Array.Copy(Queries[i].Fea, 0, XVec.Data.MemPtr, i * SymbolicState.FeaDim, SymbolicState.FeaDim);
                }
                XVec.Data.SyncFromCPU();
                Runner.Forward();
            }

            public override void Backward(bool cleanDeriv)
            {
                Runner.Backward(cleanDeriv);
            }

            public override void CleanDeriv()
            {
                Runner.CleanDeriv();
            }

            public override void Update()
            {
                Runner.Update();
            }
        }


        public class SymbolicDataRunner : StructRunner
        {
            bool IsRandom = false;
            int BatchSize = 0;
            int CurrentIdx = 0;
            //GameDataCashier Interface { get; set; }
            //public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } protected set { base.Output = value; } }
            public List<SymbolicState> OutQueries = new List<SymbolicState>();
            List<SymbolicState> Queries = new List<SymbolicState>();

            public SymbolicDataRunner(int batchSize, string filePath, bool isRandom, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                IsRandom = isRandom;
                BatchSize = batchSize;
                //Output = new DenseBatchData(BatchSize, SymbolicState.StateDim, behavior.Device);
                IsUpdate = false;
                IsBackProp = false;

                using (StreamReader mreader = new StreamReader(filePath))
                {
                    while (!mreader.EndOfStream)
                    {
                        string[] items = mreader.ReadLine().Split('\t');
                        int q = int.Parse(items[0]);
                        int a = int.Parse(items[1]);
                        int b = int.Parse(items[2]);
                        int c = int.Parse(items[3]);
                        SymbolicState s = new SymbolicState() { Q = q, A = a, B = b, C = c };
                        Queries.Add(s);
                    }
                }
                CurrentIdx = 0;
                if (IsRandom) Queries = CommonExtractor.RandomShuffle<SymbolicState>(Queries, 10000, 13).ToList();
            }

            unsafe public override void Forward()
            {
                OutQueries.Clear();
                for (int i = 0; i < BatchSize; i++)
                {
                    OutQueries.Add(Queries[CurrentIdx]);
                    //Output.Data.MemPtr[i * SymbolicState.StateDim + 0] = Queries[CurrentIdx].Q;
                    //Output.Data.MemPtr[i * SymbolicState.StateDim + 1] = Queries[CurrentIdx].A;
                    //Output.Data.MemPtr[i * SymbolicState.StateDim + 2] = Queries[CurrentIdx].a;
                    //Output.Data.MemPtr[i * SymbolicState.StateDim + 3] = Queries[CurrentIdx].B;
                    //Output.Data.MemPtr[i * SymbolicState.StateDim + 4] = Queries[CurrentIdx].b;
                    //Output.Data.MemPtr[i * SymbolicState.StateDim + 5] = Queries[CurrentIdx].C;
                    //Output.Data.MemPtr[i * SymbolicState.StateDim + 6] = Queries[CurrentIdx].c;
                    CurrentIdx++;
                    if(CurrentIdx == Queries.Count)
                    {
                        if (IsRandom) Queries = CommonExtractor.RandomShuffle<SymbolicState>(Queries, 10000, 13).ToList();
                        CurrentIdx = 0;
                    }
                }
                //Output.BatchSize = BatchSize;
                //Output.Data.SyncFromCPU();
            }
        }

        class TwoWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            SeqDenseBatchData AData { get; set; }
            HiddenBatchData CData { get; set; }
            CudaPieceInt EnsembleMask2;
            CudaPieceFloat tmpOutput;

            /// <summary>
            /// output = [AData, AData * CData]
            /// </summary>
            /// <param name="aData"></param>
            /// <param name="cData"></param>
            /// <param name="behavior"></param>
            public TwoWayEnsembleRunner(SeqDenseBatchData aData, HiddenBatchData cData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                AData = aData;
                CData = cData;
                if (AData.Dim != CData.Dim) { throw new Exception(string.Format("dimension adata {0} is not the same as cdata {1}", AData.Dim, CData.Dim)); }
                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 2, Behavior.Device);
                tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);
                EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 1 + (s * 2);
                EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;
                ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
                                             AData.SentOutput, 0, AData.Dim,
                                             Output.SentOutput, 0, Output.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 0);
                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, AData.SentMargin, 0, EnsembleMask2, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  Output.SentSize, AData.Dim, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
                                              Output.SentSize, AData.Dim, 1, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 1);
            }
        }

        public class StateActorRunner : StructRunner
        {
            List<SymbolicState> Queries;
            HiddenBatchData InputState;

            DNNStructure TgtDNN;
            DenseBatchData TgtVec;
            SeqDenseBatchData TgtOut;
            DNNRunner<HiddenBatchData> TgtRunner = null;
            CudaPieceInt SmpIdx;
            CudaPieceInt SentMargin;
            TwoWayEnsembleRunner EnsembleRunner = null;
            SeqDenseBatchData CombineOutput = null;

            DNNStructure ScoreDNN;
            DNNRunner<HiddenBatchData> ScoreRunner = null;
            HiddenBatchData ScoreData = null;

            public List<SymbolicState> Answer;

            int Step = 0;
            public StateActorRunner(List<SymbolicState> input, HiddenBatchData inputState, DNNStructure tgtDNN, DNNStructure scoreDNN, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Queries = input;
                InputState = inputState;
                TgtDNN = tgtDNN;
                ScoreDNN = scoreDNN;

                TgtVec = new DenseBatchData(inputState.MAX_BATCHSIZE * SymbolicState.MaxNeighborNum, SymbolicState.FeaDim, Behavior.Device);
                TgtRunner = new DNNRunner<HiddenBatchData>(TgtDNN, new HiddenBatchData(TgtVec.Stat.MAX_BATCHSIZE, TgtVec.Stat.Dim, TgtVec.Data, null, Behavior.Device), Behavior);

                SmpIdx = new CudaPieceInt(inputState.MAX_BATCHSIZE, Behavior.Device);
                SentMargin = new CudaPieceInt(inputState.MAX_BATCHSIZE * SymbolicState.MaxNeighborNum, Behavior.Device);
                TgtOut = new SeqDenseBatchData(
                    new SequenceDataStat(inputState.MAX_BATCHSIZE, SymbolicState.MaxNeighborNum * inputState.MAX_BATCHSIZE, TgtRunner.Output.Dim),
                    SmpIdx, SentMargin, TgtRunner.Output.Output.Data, TgtRunner.Output.Deriv.Data, Behavior.Device);

                EnsembleRunner = new TwoWayEnsembleRunner(TgtOut, InputState, Behavior);
                CombineOutput = EnsembleRunner.Output;

                ScoreRunner = new DNNRunner<HiddenBatchData>(ScoreDNN, 
                    new HiddenBatchData(CombineOutput.MAX_SENTSIZE, CombineOutput.Dim, CombineOutput.SentOutput, CombineOutput.SentDeriv, Behavior.Device), Behavior);
                ScoreData = ScoreRunner.Output;

                Answer = new List<SymbolicState>();
            }

            unsafe public override void Forward()
            {
                Answer.Clear();

                List<List<SymbolicState>> candidates = new List<List<SymbolicState>>();
                foreach (SymbolicState s in Queries)
                {
                    candidates.Add(s.NeighborStates());
                }

                TgtVec.BatchSize = candidates.Select(i => i.Count).Sum();
                TgtOut.BatchSize = Queries.Count;

                int idx = 0;
                for (int i = 0; i < Queries.Count; i++)
                {
                    SmpIdx.MemPtr[i] = (i == 0 ? 0 : SmpIdx.MemPtr[i - 1]) + candidates[i].Count;
                    foreach (SymbolicState s in candidates[i])
                    {
                        Array.Copy(s.Fea, 0, TgtVec.Data.MemPtr, idx * SymbolicState.FeaDim, SymbolicState.FeaDim);
                        SentMargin.MemPtr[idx] = i;
                        idx += 1;
                    }
                }
                SmpIdx.SyncFromCPU();
                SentMargin.SyncFromCPU();
                TgtVec.Data.SyncFromCPU();

                TgtRunner.Forward();
                EnsembleRunner.Forward();
                ScoreRunner.Forward();

                ScoreData.Output.Data.SyncToCPU();
                idx = 0;
                for (int i = 0; i < Queries.Count; i++)
                {
                    float[] a = new float[candidates[i].Count];
                    for (int t = 0; t < candidates[i].Count; t++)
                    {
                        a[t] = ScoreData.Output.Data.MemPtr[idx];
                        idx += 1;
                    }
                    a = Util.Softmax(a, 1);
                    int action = Util.Sample(a, ParameterSetting.Random);

                    float epsilon = BuilderParameters.Epsilon(Step);

                    double mp = ParameterSetting.Random.NextDouble();
                    if (mp >= epsilon)
                    {
                        action = Util.MaximumValue(a);
                    }
                    else
                    {
                        action = ParameterSetting.Random.Next(candidates[i].Count);
                    }
                    Answer.Add(candidates[i][action]);
                }
                Step += 1;
            }

            public override void Backward(bool cleanDeriv)
            {
                ScoreRunner.Backward(cleanDeriv);
                EnsembleRunner.Backward(cleanDeriv);
                TgtRunner.Backward(cleanDeriv);
            }

            public override void CleanDeriv()
            {
                TgtRunner.CleanDeriv();
                EnsembleRunner.CleanDeriv();
                ScoreRunner.CleanDeriv();
            }

            public override void Update()
            {
                TgtRunner.Update();
                EnsembleRunner.Update();
                ScoreRunner.Update();
            }
        }


        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static List<SymbolicState> Query = new List<SymbolicState>();
            public static void RandomQuery(int number)
            {
                Random random = new Random();
                HashSet<string> keys = new HashSet<string>(); 
       
                for (int i = 0; i < number; i++)
                {
                    SymbolicState s = new SymbolicState(random, keys);
                    Query.Add(s);
                    if (i % 100 == 0) { Console.WriteLine(string.Format("Search Examples {0}", i)); }
                    keys.Add(s.QueryKey);
                }

                using (StreamWriter mwriter = new StreamWriter(BuilderParameters.QUERY_PATH))
                {
                    foreach(SymbolicState s in Query)
                    {
                        mwriter.WriteLine(s.QueryKey);
                    }
                    mwriter.Close();
                }
            }
            
            public static void LoadQuery()
            {
                using (StreamReader mreader = new StreamReader(BuilderParameters.QUERY_PATH))
                {
                    while(!mreader.EndOfStream)
                    {
                        SymbolicState s = new SymbolicState(mreader.ReadLine().Trim());
                        Query.Add(s);
                    }
                }
            }

            public static void Init()
            {
                if(!File.Exists(BuilderParameters.QUERY_PATH)) RandomQuery(BuilderParameters.QUERY_SET);

                LoadQuery();
            }
        }
    }


}
