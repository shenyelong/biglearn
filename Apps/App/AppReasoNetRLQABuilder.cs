﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppReasoNetRLQABuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                // training data folder.
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Testing Folder"));

                Argument.Add("VOCAB-SIZE", new ParameterArgument("50000", "Vocab Size"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-WIN", new ParameterArgument("1", "DNN Layer WindowSize"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument(((int)A_Func.Linear).ToString(), "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("EMBED-LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding File"));

                Argument.Add("CON-LSTM-DIM", new ParameterArgument("300", "Lstm Dimension"));
                Argument.Add("QUERY-LSTM-DIM", new ParameterArgument("300", "Lstm Dimension"));

                Argument.Add("RECURRENT-GATE-TYPE", new ParameterArgument(((int)Recurrent_Unit.LSTM).ToString(), ParameterUtil.EnumValues(typeof(Recurrent_Unit))));

                Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                Argument.Add("ATT-AF", new ParameterArgument(((int)A_Func.Rectified).ToString(), "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.RL)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
                Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("RL-ALL", new ParameterArgument((((int)RLType.RL_All)).ToString(), "RL Type : " + ParameterUtil.EnumValues(typeof(RLType))));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAX)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string TrainFile { get { return Argument["TRAIN"].Value; } }
            public static bool IsTrainFile { get { return (!TrainFile.Equals(string.Empty)); } }
            public static string TrainIndexData { get { return string.Format("{0}.{1}.idx.tsv", TrainFile, VocabSize); } }
            public static string TrainContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TrainFile, VocabSize, MiniBatchSize); } }
            public static string TrainQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TrainFile, VocabSize, MiniBatchSize); } }
            public static string TrainAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TrainFile, VocabSize, MiniBatchSize); } }

            public static string TestFile { get { return Argument["TEST"].Value; } }
            public static bool IsTestFile { get { return (!TestFile.Equals(string.Empty)); } }
            public static string TestIndexData { get { return string.Format("{0}.{1}.idx.tsv", TestFile, VocabSize); } }
            public static string TestContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TestFile, VocabSize, MiniBatchSize); } }
            public static string TestQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TestFile, VocabSize, MiniBatchSize); } }
            public static string TestAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TestFile, VocabSize, MiniBatchSize); } }

            public static int VocabSize { get { return int.Parse(Argument["VOCAB-SIZE"].Value); } }
            public static string Vocab { get { return Argument["VOCAB"].Value == string.Empty ? string.Format("{0}.{1}.vocab", TrainFile, VocabSize) : Argument["VOCAB"].Value; } }



            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] EMBED_LAYER_WIN { get { return Argument["EMBED-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] EMBED_DROPOUT
            {
                get
                {
                    if (Argument["EMBED-LAYER-DROPOUT"].Value.Equals(string.Empty)) return EMBED_LAYER_DIM.Select(i => 0.0f).ToArray();
                    else return Argument["EMBED-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray();
                }
            }

            public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }

            public static Recurrent_Unit Recurrent_Gate_Type { get { return (Recurrent_Unit)int.Parse(Argument["RECURRENT-GATE-TYPE"].Value); } }

            public static int QUERY_LSTM_DIM { get { return int.Parse(Argument["QUERY-LSTM-DIM"].Value); } }
            public static int CON_LSTM_DIM { get { return int.Parse(Argument["CON-LSTM-DIM"].Value); } }

            public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
            public static A_Func ATT_AF { get { return (A_Func)int.Parse(Argument["ATT-AF"].Value); } }
            public static CrossSimType ATT_TYPE { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }

            public static PoolingType RECURRENT_POOL { get { return (PoolingType)int.Parse(Argument["RECURRENT-POOL"].Value); } }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

            public static string DebugFile { get { return Argument["DEBUG-FILE"].Value; } }
            public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }

            public static RLType RL_ALL { get { return (RLType)int.Parse(Argument["RL-ALL"].Value); } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }
            public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASON_NET_RL_QA; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        enum RLType { RL_Sup = 0, RL_All = 1, RL_B = 2, RL_AB = 3 }
        enum PredType { RL_MAX = 0, RL_AVG = 1 }
        /// <summary>
        /// Fix Iteration, SoftAttention, Supervised Training.
        /// </summary>
        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            HiddenBatchData InitStatus { get; set; }

            /// <summary>
            /// Memory.
            /// </summary>
            SeqDenseBatchData Memory { get; set; }

            /// <summary>
            /// Status 2 Memory Match Data.
            /// </summary>
            BiMatchBatchData MatchData { get; set; }

            PoolingType ReasonPool = BuilderParameters.RECURRENT_POOL;
            // PoolingType.LAST;

            string DebugFile { get; set; }
            StreamWriter DebugWriter = null;
            int DebugSample = 0;
            int DebugIter = 0;
            public ReasonNetRunner(
                HiddenBatchData initStatus, SeqDenseBatchData mem, BiMatchBatchData matchData, int maxIterateNum,
                GRUCell gruCell, MLPAttentionStructure memAttentionStruct, LayerStructure answerStruct, LayerStructure terminalStruct,
                float gamma, RunnerBehavior behavior, string debugFile = "") : base(Structure.Empty, behavior)
            {
                InitStatus = initStatus;
                Memory = mem;
                MatchData = matchData;

                DebugFile = debugFile;

                MaxIterationNum = maxIterateNum;

                GruCell = gruCell;
                MemAtt = memAttentionStruct;
                TerminalNN = terminalStruct;
                AnswerNN = answerStruct;

                //if (BuilderParameters.ATT_TYPE == CrossSimType.Addition || BuilderParameters.ATT_TYPE == CrossSimType.Product || BuilderParameters.ATT_TYPE == CrossSimType.SubCosine)
                {
                    MemoryHidden = new SeqDenseBatchData(
                        new SequenceDataStat()
                        {
                            MAX_BATCHSIZE = Memory.Stat.MAX_BATCHSIZE,
                            MAX_SEQUENCESIZE = Memory.Stat.MAX_SEQUENCESIZE,
                            FEATURE_DIM = memAttentionStruct.HiddenDim
                        }, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);
                    SoftLinearSimRunner initAttRunner = new SoftLinearSimRunner(InitStatus, MemoryHidden, MatchData, BuilderParameters.ATT_TYPE, MemAtt, Behavior, BuilderParameters.ATT_AF);
                    AttentionRunner.Add(initAttRunner);
                }
                //else if (BuilderParameters.ATT_TYPE == AttentionType.Cosine)
                //{
                //    SoftCosineAttentionRunner initAttRunner = new SoftCosineAttentionRunner(InitStatus, Memory, MatchData, Behavior);
                //    AttentionRunner.Add(initAttRunner);
                //}

                TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(terminalStruct, InitStatus, Behavior));
                AnswerRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(answerStruct, InitStatus, Behavior));

                HiddenBatchData lastAtt = (HiddenBatchData)AttentionRunner[0].Output;
                HiddenBatchData lastStatus = InitStatus;

                for (int i = 0; i < maxIterateNum - 1; i++)
                {
                    MemoryRetrievalRunner xRunner = new MemoryRetrievalRunner(lastAtt, Memory, MatchData, gamma, Behavior);
                    MemRetrievalRunner.Add(xRunner);

                    GRUQueryRunner yRunner = new GRUQueryRunner(gruCell, lastStatus, xRunner.Output, Behavior);
                    StatusRunner.Add(yRunner);
                    lastStatus = yRunner.Output;

                    //if (BuilderParameters.ATT_TYPE == CrossSimType.Addition || BuilderParameters.ATT_TYPE == CrossSimType.Product || BuilderParameters.ATT_TYPE == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner attRunner = new SoftLinearSimRunner(lastStatus, MemoryHidden, MatchData, BuilderParameters.ATT_TYPE, MemAtt, Behavior, BuilderParameters.ATT_AF);
                        AttentionRunner.Add(attRunner);
                        lastAtt = attRunner.Output;
                    }
                    
                    TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(terminalStruct, lastStatus, Behavior));
                    AnswerRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(answerStruct, lastStatus, Behavior));
                }

                if (ReasonPool == PoolingType.AVG || ReasonPool == PoolingType.ATT || ReasonPool == PoolingType.TREE)
                {
                    FinalAnswer = new HiddenBatchData(AnswerSeqData.Last().MAX_BATCHSIZE, AnswerSeqData.Last().Dim, DNNRunMode.Train, Behavior.Device);
                    TerminalProb = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, MaxIterationNum, DNNRunMode.Train, Behavior.Device);
                }
                else if (ReasonPool == PoolingType.LAST)
                {
                    FinalAnswer = AnswerSeqData.Last();
                }
                else if(ReasonPool == PoolingType.RL)
                {
                    AnswerProb = new HiddenBatchData[MaxIterationNum];
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i] = new HiddenBatchData(AnswerSeqData[i].MAX_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                    }

                    FinalAnswer = new HiddenBatchData(AnswerSeqData.Last().MAX_BATCHSIZE, AnswerSeqData.Last().Dim, DNNRunMode.Train, Behavior.Device);
                }
            }

            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }

            SeqDenseBatchData MemoryHidden = null;

            /**** Main Component of Reasoning Net**********/
            GRUCell GruCell = null;
            MLPAttentionStructure MemAtt = null;
            LayerStructure TerminalNN = null;
            LayerStructure AnswerNN = null;
            /**** Main Component of Reasoning Net**********/

            List<MemoryRetrievalRunner> MemRetrievalRunner = new List<MemoryRetrievalRunner>();
            List<GRUQueryRunner> StatusRunner = new List<GRUQueryRunner>();
            List<SoftLinearSimRunner> AttentionRunner = new List<SoftLinearSimRunner>();
            List<FullyConnectHiddenRunner<HiddenBatchData>> TerminalRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();
            List<FullyConnectHiddenRunner<HiddenBatchData>> AnswerRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

            /// <summary>
            /// samples of answer data.
            /// </summary>
            public HiddenBatchData[] AnswerSeqData { get { return AnswerRunner.Select(i => (HiddenBatchData)i.Output).ToArray(); } }
            public HiddenBatchData[] AnswerProb = null;

            public HiddenBatchData FinalAnswer = null;
            HiddenBatchData TerminalProb = null;

            /// <summary>
            /// Output Debug File.
            /// </summary>
            public override void Init()
            {
                if (DebugFile != "" && !DebugFile.Equals(string.Empty))
                {
                    DebugWriter = new StreamWriter(DebugFile + DateTime.Now.ToFileTime().ToString() + "." + DebugIter.ToString() + ".iter");
                    DebugSample = 0;
                    DebugIter += 1;
                }
            }

            /// <summary>
            /// Save Debug File.
            /// </summary>
            public override void Complete()
            {
                if(DebugWriter!= null)
                {
                    DebugWriter.Close();
                }
            }

            public override void Forward()
            {
                //if (BuilderParameters.ATT_TYPE == CrossSimType.Addition || BuilderParameters.ATT_TYPE == CrossSimType.Product
                //    || BuilderParameters.ATT_TYPE == CrossSimType.SubCosine)
                {
                    MemoryHidden.BatchSize = Memory.BatchSize;
                    MemoryHidden.SentSize = Memory.SentSize;
                    ComputeLib.Sgemm(Memory.SentOutput, 0, MemAtt.Wm, 0, MemoryHidden.SentOutput, 0, Memory.SentSize, Memory.Dim, MemAtt.HiddenDim, 0, 1, false, false);
                    if (MemAtt.IsBias) ComputeLib.Matrix_Add_Linear(MemoryHidden.SentOutput, MemAtt.B, Memory.SentSize, MemAtt.HiddenDim);
                }

                AttentionRunner[0].Forward();
                TerminalRunner[0].Forward();
                AnswerRunner[0].Forward();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Forward();
                    // update Status.
                    StatusRunner[i].Forward();
                    // take new Attention
                    AttentionRunner[i + 1].Forward();
                    // take action, terminal or not.
                    TerminalRunner[i + 1].Forward();
                    // generate temporal answer.
                    AnswerRunner[i + 1].Forward();
                }

                #region Average Pooling
                if (ReasonPool == PoolingType.AVG)
                {
                    FinalAnswer.BatchSize = AnswerSeqData[0].BatchSize;
                    float p = 1.0f / MaxIterationNum;
                    ComputeLib.Add_Vector(FinalAnswer.Output.Data, AnswerSeqData[0].Output.Data, AnswerSeqData[0].BatchSize * AnswerSeqData[0].Dim, 0, p);
                    for (int i = 1; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Add_Vector(FinalAnswer.Output.Data, AnswerSeqData[i].Output.Data, AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim, 1, p);
                    }
                }
                #endregion.

                #region Attention Pooling.
                else if (ReasonPool == PoolingType.ATT)
                {
                    TerminalProb.BatchSize = TerminalRunner[0].Output.BatchSize;

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        for (int b = 0; b < TerminalRunner[i].Output.BatchSize; b++)
                        {
                            TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i] = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                        }
                    }
                    TerminalProb.Output.Data.SyncFromCPU(TerminalProb.BatchSize * MaxIterationNum);
                    ComputeLib.SoftMax(TerminalProb.Output.Data, TerminalProb.Output.Data, MaxIterationNum, TerminalProb.BatchSize, 1);

                    FinalAnswer.BatchSize = AnswerSeqData[0].BatchSize;

                    TerminalProb.Output.Data.SyncToCPU(TerminalProb.BatchSize * MaxIterationNum);
                    Array.Clear(FinalAnswer.Output.Data.MemPtr, 0, FinalAnswer.BatchSize * FinalAnswer.Dim);
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerSeqData[i].Output.Data.SyncToCPU(AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim);

                        for (int b = 0; b < AnswerSeqData[i].BatchSize; b++)
                        {
                            float p = TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];
                            for (int m = 0; m < AnswerSeqData[i].Dim; m++)
                                FinalAnswer.Output.Data.MemPtr[b * FinalAnswer.Dim + m] += p * AnswerSeqData[i].Output.Data.MemPtr[b * FinalAnswer.Dim + m];
                        }
                    }
                    FinalAnswer.Output.Data.SyncFromCPU(FinalAnswer.BatchSize * FinalAnswer.Dim);
                }
                #endregion.

                #region Cascading Attention.
                else if (ReasonPool == PoolingType.TREE)
                {
                    TerminalProb.BatchSize = TerminalRunner[0].Output.BatchSize;
                    FinalAnswer.BatchSize = AnswerSeqData[0].BatchSize;

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
                        //ComputeLib.Logistic(AnswerSeqData[i].Output.Data, 0, AnswerSeqData[i].Output.Data, 0, AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim, 1);
                        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        AnswerSeqData[i].Output.Data.SyncToCPU(AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim);
                    }

                    for (int b = 0; b < TerminalProb.BatchSize; b++)
                    {
                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i] = p;
                            acc_log_t += (float)Math.Log(1 - t_i);
                        }
                    }
                    //TerminalProb.Output.Data.SyncFromCPU(TerminalProb.BatchSize * MaxIterationNum);

                    Array.Clear(FinalAnswer.Output.Data.MemPtr, 0, FinalAnswer.BatchSize * FinalAnswer.Dim);
                    for (int b = 0; b < FinalAnswer.BatchSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float p_i = (float)Math.Exp(TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i]);
                            for (int m = 0; m < FinalAnswer.Dim; m++)
                            {
                                FinalAnswer.Output.Data.MemPtr[b * FinalAnswer.Dim + m] += p_i * AnswerSeqData[i].Output.Data.MemPtr[b * FinalAnswer.Dim + m];

                                if (float.IsNaN(FinalAnswer.Output.Data.MemPtr[b * FinalAnswer.Dim + m]))
                                {
                                    Logger.WriteLog("Sad thing happen again, hope we can find the reason p_i: {0}, a_i {1}", p_i, AnswerSeqData[i].Output.Data.MemPtr[b * FinalAnswer.Dim + m]);
                                    Console.ReadLine();
                                }
                            }
                        }
                    }
                    FinalAnswer.Output.Data.SyncFromCPU(FinalAnswer.BatchSize * FinalAnswer.Dim);
                }
                #endregion.

                #region Last Pooling.
                else if (ReasonPool == PoolingType.LAST)
                {
                    FinalAnswer = AnswerSeqData.Last();

                    /// dump the intermedia results.
                    if (DebugWriter != null)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            AnswerSeqData[i].Output.Data.SyncToCPU(AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim);
                        }

                        for (int i = 0; i < MaxIterationNum - 1; i++)
                        {
                            MemRetrievalRunner[i].SoftmaxAddress.Output.Data.SyncToCPU();
                        }

                        for (int b = 0; b < FinalAnswer.BatchSize; b++)
                        {
                            int attBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int attEnd = MatchData.Src2MatchIdx.MemPtr[b];

                            DebugWriter.WriteLine("Sample {0}.............", DebugSample + b);
                            for (int i = 0; i < MaxIterationNum; i++)
                            {
                                string attentionStr = "";
                                if (i > 0)
                                {
                                    for (int m = attBgn; m < attEnd; m++)
                                    {
                                        attentionStr = attentionStr + MemRetrievalRunner[i - 1].SoftmaxAddress.Output.Data.MemPtr[m].ToString() + " ";
                                    }
                                }
                                //float termProb = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                                float ansProb = Util.Logistic(AnswerSeqData[i].Output.Data.MemPtr[b]);

                                DebugWriter.WriteLine("T {0}\t{1}\t{2}\t{3}", i, ansProb, 0, attentionStr);
                            }
                            DebugWriter.WriteLine("Sample {0}\t{1}", DebugSample + b, FinalAnswer.Output.Data.MemPtr[b]);
                            DebugWriter.WriteLine();
                        }

                        DebugSample += FinalAnswer.BatchSize;
                    }

                }
                #endregion.

                #region RL Pooling.
                else if(ReasonPool == PoolingType.RL)
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        AnswerSeqData[i].Output.Data.SyncToCPU(AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim);
                    }

                    FinalAnswer.BatchSize = AnswerSeqData.Last().BatchSize;
                    //Array.Clear(FinalAnswer.Output.Data.MemPtr, 0, FinalAnswer.BatchSize);
                    for (int b = 0; b < FinalAnswer.BatchSize; b++)
                    {
                        int max_iter = 0;
                        double max_p = 0;
                        float pos_prob = 0;
                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t += (float)Math.Log(1 - t_i);

                            pos_prob += AnswerProb[i].Output.Data.MemPtr[b] * Util.Logistic(AnswerSeqData[i].Output.Data.MemPtr[b]);

                            if (Math.Exp(p) > max_p)
                            {
                                max_p = Math.Exp(p);
                                max_iter = i;
                            }
                        }

                        if (BuilderParameters.PRED_RL == PredType.RL_MAX)
                        {
                            for (int m = 0; m < AnswerSeqData[max_iter].Dim; m++)
                            {
                                FinalAnswer.Output.Data.MemPtr[b * AnswerSeqData[max_iter].Dim + m] =
                                    AnswerSeqData[max_iter].Output.Data.MemPtr[b * AnswerSeqData[max_iter].Dim + m];
                            }
                        }
                        else if (BuilderParameters.PRED_RL == PredType.RL_AVG)
                        {
                            FinalAnswer.Output.Data.MemPtr[b] = pos_prob;
                        }
                    }
                    FinalAnswer.Output.Data.SyncFromCPU(FinalAnswer.BatchSize * FinalAnswer.Dim);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i].BatchSize = AnswerSeqData[i].BatchSize;
                        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);                        
                    }


                    // write the reasonet status.
                    if (DebugWriter != null)
                    {
                        for (int i = 0; i < MaxIterationNum - 1; i++)
                        {
                            MemRetrievalRunner[i].SoftmaxAddress.Output.Data.SyncToCPU();
                        }

                        for (int b = 0; b < FinalAnswer.BatchSize; b++)
                        {
                            int attBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int attEnd = MatchData.Src2MatchIdx.MemPtr[b];

                            DebugWriter.WriteLine("Sample {0}.............", DebugSample + b);
                            for (int i = 0; i < MaxIterationNum; i++)
                            {
                                string attentionStr = "";
                                if (i > 0)
                                {
                                    for (int m = attBgn; m < attEnd; m++)
                                    {
                                        attentionStr = attentionStr + MemRetrievalRunner[i - 1].SoftmaxAddress.Output.Data.MemPtr[m].ToString() + " ";
                                    }
                                }
                                float termProb = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                                float ansProb = Util.Logistic(AnswerSeqData[i].Output.Data.MemPtr[b]);

                                DebugWriter.WriteLine("T {0}\t{1}\t{2}\t{3}", i, ansProb, termProb, attentionStr);
                            }
                            DebugWriter.WriteLine("Sample {0}\t{1}", DebugSample + b, FinalAnswer.Output.Data.MemPtr[b]);
                            DebugWriter.WriteLine();
                        }

                        DebugSample += FinalAnswer.BatchSize;
                    }
                }
                #endregion.
            }

            #region Clean Deriv.
            public override void CleanDeriv()
            {
                if (BuilderParameters.ATT_TYPE == CrossSimType.Addition || BuilderParameters.ATT_TYPE == CrossSimType.Product
                    || BuilderParameters.ATT_TYPE == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(MemoryHidden.SentDeriv, MemoryHidden.SentSize * MemoryHidden.Dim);
                }

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    AnswerRunner[i + 1].CleanDeriv();

                    TerminalRunner[i + 1].CleanDeriv();
                    // take action. new Attention
                    AttentionRunner[i + 1].CleanDeriv();
                    // update Status.
                    StatusRunner[i].CleanDeriv();
                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].CleanDeriv();
                }
                AttentionRunner[0].CleanDeriv();
                TerminalRunner[0].CleanDeriv();
                AnswerRunner[0].CleanDeriv();

                if (ReasonPool == PoolingType.AVG || ReasonPool == PoolingType.ATT || ReasonPool == PoolingType.TREE)
                {
                    ComputeLib.Zero(FinalAnswer.Deriv.Data, FinalAnswer.BatchSize * FinalAnswer.Dim);
                }
            }
            #endregion.

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                #region Average Pooling.
                if (ReasonPool == PoolingType.AVG)
                {
                    float p = 1.0f / MaxIterationNum;
                    for (int i = 0; i < MaxIterationNum; i++)
                        ComputeLib.Add_Vector(AnswerSeqData[i].Deriv.Data, FinalAnswer.Deriv.Data, AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim, 0, p);
                }
                #endregion.

                #region Attention Pooling.
                else if (ReasonPool == PoolingType.ATT)
                {
                    FinalAnswer.Deriv.Data.SyncToCPU(FinalAnswer.BatchSize * FinalAnswer.Dim);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(AnswerSeqData[i].Deriv.Data.MemPtr, 0, AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim);

                        for (int b = 0; b < AnswerSeqData[i].BatchSize; b++)
                        {
                            float p = TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];
                            float pDeriv = 0;
                            for (int m = 0; m < AnswerSeqData[i].Dim; m++)
                            {
                                AnswerSeqData[i].Deriv.Data.MemPtr[b * FinalAnswer.Dim + m] += FinalAnswer.Deriv.Data.MemPtr[b * FinalAnswer.Dim + m] * p;
                                pDeriv += FinalAnswer.Deriv.Data.MemPtr[b * FinalAnswer.Dim + m] * AnswerSeqData[i].Output.Data.MemPtr[b * FinalAnswer.Dim + m];
                            }
                            TerminalProb.Deriv.Data.MemPtr[b * MaxIterationNum + i] = pDeriv;
                        }
                        AnswerSeqData[i].Deriv.Data.SyncFromCPU(AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim);
                    }

                    TerminalProb.Deriv.Data.SyncFromCPU(MaxIterationNum * TerminalProb.BatchSize);
                    ComputeLib.DerivSoftmax(TerminalProb.Output.Data, TerminalProb.Deriv.Data, TerminalProb.Deriv.Data, MaxIterationNum, TerminalProb.BatchSize, 0);
                    TerminalProb.Deriv.Data.SyncToCPU(MaxIterationNum * TerminalProb.BatchSize);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        for (int b = 0; b < TerminalRunner[i].Output.BatchSize; b++)
                        {
                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] = TerminalProb.Deriv.Data.MemPtr[b * MaxIterationNum + i];
                        }
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }
                #endregion.

                #region cascading Pooling.
                else if (ReasonPool == PoolingType.TREE)
                {
                    FinalAnswer.Deriv.Data.SyncToCPU(FinalAnswer.BatchSize * FinalAnswer.Dim);

                    for (int b = 0; b < FinalAnswer.BatchSize; b++)
                    {
                        float[] deriv_i = new float[MaxIterationNum];
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            deriv_i[i] = 0;
                            for (int m = 0; m < FinalAnswer.Dim; m++)
                            {
                                deriv_i[i] += AnswerSeqData[i].Output.Data.MemPtr[b * FinalAnswer.Dim + m] * FinalAnswer.Deriv.Data.MemPtr[b * FinalAnswer.Dim + m];
                            }
                        }

                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float acc_t_p = TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + i];
                            for (int m = 0; m < AnswerSeqData[i].Dim; m++)
                            {
                                AnswerSeqData[i].Deriv.Data.MemPtr[b * FinalAnswer.Dim + m] = FinalAnswer.Deriv.Data.MemPtr[b * FinalAnswer.Dim + m] * (float)Math.Exp(acc_t_p);
                                if (AnswerSeqData[i].Deriv.Data.MemPtr[b * FinalAnswer.Dim + m] > Math.Abs(1))
                                {
                                    Logger.WriteLog("Answer Deriv is too large {0}, prob {1}, ans deriv {2}", AnswerSeqData[i].Deriv.Data.MemPtr[b * FinalAnswer.Dim + m], Math.Exp(acc_t_p),
                                        FinalAnswer.Deriv.Data.MemPtr[b * FinalAnswer.Dim + m]);
                                    //Console.ReadLine();
                                }
                            }

                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 2: terminal prob over the threshold {0}!!!", t_i); }

                            if (t_i <= Util.GPUEpsilon || t_i >= 1 - Util.GPUEpsilon)
                            {
                                TerminalRunner[i].Output.Deriv.Data.MemPtr[b] = 0;

                                Logger.WriteLog("Prob touch the bound {0}, step {1}", t_i, i);
                                //Console.ReadLine();
                            }
                            else
                            {
                                float deriv_t_i = deriv_i[i] * (float)Math.Exp(acc_t_p - Math.Log(t_i));
                                for (int hp = i + 1; hp < MaxIterationNum; hp++)
                                {
                                    acc_t_p = TerminalProb.Output.Data.MemPtr[b * MaxIterationNum + hp];
                                    t_i = TerminalRunner[hp].Output.Output.Data.MemPtr[b];
                                    if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 3: terminal prob over the threshold {0}!!!", t_i); }
                                    deriv_t_i += -deriv_i[hp] * (float)Math.Exp(acc_t_p - Math.Log(1 - t_i));
                                }
                                if (i == MaxIterationNum - 1)
                                {
                                    TerminalRunner[i].Output.Deriv.Data.MemPtr[b] = 0;
                                }
                                else
                                {
                                    TerminalRunner[i].Output.Deriv.Data.MemPtr[b] = deriv_t_i;
                                }

                                if (TerminalRunner[i].Output.Deriv.Data.MemPtr[b] > Math.Abs(1))
                                {
                                    Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalRunner[i].Output.Deriv.Data.MemPtr[b], i);
                                }
                            }
                        }
                    }

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerSeqData[i].Deriv.Data.SyncFromCPU(AnswerSeqData[i].BatchSize * AnswerSeqData[i].Dim);
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);

                        ComputeLib.DerivLogistic(TerminalRunner[i].Output.Output.Data, 0,
                            TerminalRunner[i].Output.Deriv.Data, 0, TerminalRunner[i].Output.Deriv.Data, 0,
                            TerminalRunner[i].Output.BatchSize, 0, 1);
                    }

                }
                #endregion.

                #region RL Attention.
                else if (ReasonPool == PoolingType.RL)
                {
                    int BatchSize = AnswerSeqData.Last().BatchSize;

                    /// according to loss, assign reward to each terminal node.
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(TerminalRunner[i].Output.Deriv.Data.MemPtr, 0, BatchSize);
                    }

                    for (int b = 0; b < BatchSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] += reward * (1 - TerminalRunner[i].Output.Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                TerminalRunner[hp].Output.Deriv.Data.MemPtr[b] += - (float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * reward * TerminalRunner[hp].Output.Output.Data.MemPtr[b];
                            }
                            
                            if (TerminalRunner[i].Output.Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalRunner[i].Output.Deriv.Data.MemPtr[b], i);
                            }
                        }
                    }

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                        /*
                        ComputeLib.DerivLogistic(TerminalRunner[i].Output.Output.Data, 0,
                            TerminalRunner[i].Output.Deriv.Data, 0, TerminalRunner[i].Output.Deriv.Data, 0,
                            TerminalRunner[i].Output.BatchSize, 0, 1);*/
                    }
                }
                #endregion.

                for (int i = MaxIterationNum - 2; i >= 0; i--)
                {
                    AnswerRunner[i + 1].Backward(cleanDeriv);

                    TerminalRunner[i + 1].Backward(cleanDeriv);
                    // take new Attention.
                    AttentionRunner[i + 1].Backward(cleanDeriv);
                    // update Status.
                    StatusRunner[i].Backward(cleanDeriv);
                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Backward(cleanDeriv);
                }
                AttentionRunner[0].Backward(cleanDeriv);
                TerminalRunner[0].Backward(cleanDeriv);
                AnswerRunner[0].Backward(cleanDeriv);

                if (BuilderParameters.ATT_TYPE == CrossSimType.Addition || BuilderParameters.ATT_TYPE == CrossSimType.Product
                    || BuilderParameters.ATT_TYPE == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(MemoryHidden.SentDeriv, 0,
                                     MemAtt.Wm, 0,
                                     Memory.SentDeriv, 0,
                                     Memory.SentSize, MemAtt.HiddenDim, MemAtt.MemoryDim, 1, 1, false, true);
                }
            }

            public override void Update()
            {
                if (BuilderParameters.ATT_TYPE == CrossSimType.Addition || BuilderParameters.ATT_TYPE == CrossSimType.Product
                    || BuilderParameters.ATT_TYPE == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        MemAtt.WmMatrixOptimizer.BeforeGradient();
                        if (MemAtt.IsBias) MemAtt.BMatrixOptimizer.BeforeGradient();
                    }

                    if (MemAtt.IsBias)
                    {
                        ComputeLib.ColumnWiseSum(MemoryHidden.SentDeriv, MemAtt.BMatrixOptimizer.Gradient, Memory.SentSize, MemAtt.HiddenDim, MemAtt.BMatrixOptimizer.GradientStep);
                    }

                    ComputeLib.Sgemm(Memory.SentOutput, 0,
                                     MemoryHidden.SentDeriv, 0,
                                     MemAtt.WmMatrixOptimizer.Gradient, 0,
                                     Memory.SentSize, MemAtt.MemoryDim, MemAtt.HiddenDim,
                                     1, MemAtt.WmMatrixOptimizer.GradientStep, true, false);
                }

                AttentionRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AttentionRunner[0].Update();

                TerminalRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                TerminalRunner[0].Update();

                AnswerRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AnswerRunner[0].Update();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    MemRetrievalRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    StatusRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    AttentionRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    TerminalRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    AnswerRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Update();

                    // update Status.
                    StatusRunner[i].Update();

                    // take action. new Attention
                    AttentionRunner[i + 1].Update();

                    TerminalRunner[i + 1].Update();

                    AnswerRunner[i + 1].Update();
                }
                if (BuilderParameters.ATT_TYPE == CrossSimType.Addition || BuilderParameters.ATT_TYPE == CrossSimType.Product
                    || BuilderParameters.ATT_TYPE == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        MemAtt.WmMatrixOptimizer.AfterGradient();
                        if (MemAtt.IsBias) MemAtt.BMatrixOptimizer.AfterGradient();
                    }
                }
            }
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<DenseBatchData, DenseDataStat> answer,

                                                             // text embedding cnn.
                                                             List<LayerStructure> EmbedCNN,

                                                             // context lstm 
                                                             LSTMStructure ContextD1LSTM, LSTMStructure ContextD2LSTM,

                                                             // query lstm
                                                             LSTMStructure QueryD1LSTM, LSTMStructure QueryD2LSTM,

                                                             // reason net;
                                                             GRUCell StateStruct, MLPAttentionStructure AttStruct, LayerStructure AnswerStruct, LayerStructure TerminalStruct,

                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ Query Passage Candidate data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
            DenseBatchData AnswerData = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(answer, Behavior));

            /************************************************************ CNN on Query data *********/
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], QueryData, Behavior));
            for (int i = 1; i < EmbedCNN.Count; i++)
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], QueryEmbedOutput, Behavior));

            /************************************************************ LSTM on Query data *********/
            HiddenBatchData QueryOutput = null;

            SeqDenseRecursiveData QueryD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(QueryEmbedOutput, false, Behavior));
            FastLSTMDenseRunner<SeqDenseRecursiveData> queryD1LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(QueryD1LSTM.LSTMCells[0], QueryD1LstmInput, Behavior);
            SeqDenseRecursiveData QueryD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(queryD1LSTMRunner);

            SeqDenseRecursiveData QueryD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(QueryEmbedOutput, true, Behavior));
            FastLSTMDenseRunner<SeqDenseRecursiveData> queryD2LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(QueryD2LSTM.LSTMCells[0], QueryD2LstmInput, Behavior);
            SeqDenseRecursiveData QueryD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(queryD2LSTMRunner);

            HiddenBatchData QueryD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD1LstmOutput, true, 0, QueryD1LstmOutput.MapForward, Behavior));
            HiddenBatchData QueryD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD2LstmOutput, false, 0, QueryD2LstmOutput.MapForward, Behavior));
            HiddenBatchData QueryO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { QueryD1O, QueryD2O }, Behavior));
            QueryOutput = QueryO;

            /************************************************************ CNN on Passage data *********/
            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], ContextData, Behavior));
            for (int i = 1; i < EmbedCNN.Count; i++)
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], ContextEmbedOutput, Behavior));

            SeqDenseBatchData ContextSeqO = null;
            /************************************************************ LSTM on Candidate Word data *********/
            SeqDenseRecursiveData ContextD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(ContextEmbedOutput, false, Behavior));
            FastLSTMDenseRunner<SeqDenseRecursiveData> contextD1LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD1LSTM.LSTMCells[0], ContextD1LstmInput, Behavior);
            SeqDenseRecursiveData ContextD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(contextD1LSTMRunner);

            SeqDenseRecursiveData ContextD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(ContextEmbedOutput, true, Behavior));
            FastLSTMDenseRunner<SeqDenseRecursiveData> contextD2LSTMRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(ContextD2LSTM.LSTMCells[0], ContextD2LstmInput, Behavior);
            SeqDenseRecursiveData ContextD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(contextD2LSTMRunner);

            SeqDenseBatchData ContextSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(ContextD1LstmOutput, Behavior));
            SeqDenseBatchData ContextSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(ContextD2LstmOutput, Behavior));
            ContextSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextSeqD1O, ContextSeqD2O }, Behavior));


            /************************************************************ LSTM on Passage data *********/
            SeqBiMatchRunner ContextMemRunner = new SeqBiMatchRunner(ContextSeqO, Behavior);
            cg.AddRunner(ContextMemRunner);
            BiMatchBatchData ContextMatch = ContextMemRunner.Output;

            ReasonNetRunner reasonRunner = new ReasonNetRunner(QueryOutput, ContextSeqO, ContextMatch, BuilderParameters.RECURRENT_STEP,
                    StateStruct, AttStruct, AnswerStruct, TerminalStruct, BuilderParameters.Gamma, Behavior, Behavior.RunMode == DNNRunMode.Train ? string.Empty : BuilderParameters.DebugFile);
            cg.AddRunner(reasonRunner);
            
            
            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                        cg.AddRunner(new ModelDiskDumpRunner(model, DeepNet.BuilderParameters.ModelUpdatePerSave, Path.Combine(BuilderParameters.ModelOutputPath, string.Format("ReasoNet"))));
                            //BuilderParameters.ModelOutputPath + "/ReasoNet"));
                    /// for each candidate answer, we calculate the loss
                    if(BuilderParameters.RL_ALL == RLType.RL_All && BuilderParameters.RECURRENT_POOL == PoolingType.RL)
                        cg.AddObjective(new BinaryContrastiveRewardRunner(AnswerData.Data, reasonRunner.AnswerSeqData, reasonRunner.AnswerProb, 1, Behavior));
                    else if(BuilderParameters.RL_ALL == RLType.RL_Sup && BuilderParameters.RECURRENT_POOL == PoolingType.RL)
                        cg.AddObjective(new MultiInstanceCrossEntropyRunner(AnswerData.Data, reasonRunner.AnswerSeqData, reasonRunner.AnswerProb, 1, Behavior));
                    else if (BuilderParameters.RL_ALL == RLType.RL_B && BuilderParameters.RECURRENT_POOL == PoolingType.RL)
                        cg.AddObjective(new BinaryStandardRewardRunner(AnswerData.Data, reasonRunner.AnswerSeqData, reasonRunner.AnswerProb, 1, Behavior));
                    else if(BuilderParameters.RL_ALL == RLType.RL_AB && BuilderParameters.RECURRENT_POOL == PoolingType.RL)
                        cg.AddObjective(new BinaryMovingAvgRewardRunner(AnswerData.Data, reasonRunner.AnswerSeqData, reasonRunner.AnswerProb, 1, Behavior));
                    else if (BuilderParameters.RECURRENT_POOL == PoolingType.LAST)
                        cg.AddObjective(new CrossEntropyRunner(AnswerData.Data, reasonRunner.FinalAnswer.Output, reasonRunner.FinalAnswer.Deriv, 1, Behavior));
                    //cg.AddObjective(new CrossEntropyRunner(AnswerData.Data, AnswerOutput.Output, AnswerOutput.Deriv, BuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    HiddenBatchData AnswerOutput = reasonRunner.FinalAnswer;
                    cg.AddRunner(new AUCDiskDumpRunner(AnswerData.Data, AnswerOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
            }

            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            List<LayerStructure> embedLayers = new List<LayerStructure>();

            //context cnn layer.
            LSTMStructure contextD1LSTM = null;
            LSTMStructure contextD2LSTM = null;

            //query cnn layer.
            LSTMStructure queryD1LSTM = null;
            LSTMStructure queryD2LSTM = null;

            //reason net structure.
            GRUCell stateStruct = null;
            MLPAttentionStructure attStruct = null;
            LayerStructure terminalStruct = null;
            LayerStructure answerStruct = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int embedDim = DataPanel.WordDim;
                for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++)
                {
                    embedLayers.Add((new LayerStructure(embedDim, BuilderParameters.EMBED_LAYER_DIM[i],
                        BuilderParameters.EMBED_ACTIVATION[i], N_Type.Convolution_layer, BuilderParameters.EMBED_LAYER_WIN[i],
                        BuilderParameters.EMBED_DROPOUT[i], false, device)));
                    embedDim = BuilderParameters.EMBED_LAYER_DIM[i];
                }

                #region init word embedding with glove vector.
                if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
                {
                    int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];

                    int halfWinStart = BuilderParameters.EMBED_LAYER_WIN[0] / 2; // * DataPanel.WordDim;

                    Logger.WriteLog("Init Glove Word Embedding ...");
                    int hit = 0;
                    using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string[] items = mreader.ReadLine().Split(' ');
                            string word = items[0];
                            float[] vec = new float[items.Length - 1];
                            for (int i = 1; i < items.Length; i++)
                            {
                                vec[i - 1] = float.Parse(items[i]);
                            }

                            int wordIdx = DataPanel.WordFreqDict.IndexItem(word);
                            if (wordIdx >= 0)
                            {
                                int feaIdx = wordIdx;

                                for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                                {
                                    for (int i = 0; i < wordDim; i++)
                                    {
                                        embedLayers[0].weight.MemPtr[((win + halfWinStart) * DataPanel.WordDim + feaIdx) * wordDim + i] = vec[i];
                                    }
                                }
                                hit++;
                            }
                        }
                    }
                    embedLayers[0].weight.SyncFromCPU();
                    Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.WordFreqDict.ItemDictSize);
                }
                #endregion.

                int conDim = embedDim;
                int queryDim = embedDim;

                contextD1LSTM = new LSTMStructure(conDim, new int[] { BuilderParameters.CON_LSTM_DIM }, device, BuilderParameters.RndInit);
                contextD2LSTM = new LSTMStructure(conDim, new int[] { BuilderParameters.CON_LSTM_DIM }, device, BuilderParameters.RndInit);
                conDim = 2 * BuilderParameters.CON_LSTM_DIM;

                queryD1LSTM = new LSTMStructure(queryDim, new int[] { BuilderParameters.QUERY_LSTM_DIM }, device, BuilderParameters.RndInit);
                queryD2LSTM = new LSTMStructure(queryDim, new int[] { BuilderParameters.QUERY_LSTM_DIM }, device, BuilderParameters.RndInit);
                queryDim = 2 * BuilderParameters.QUERY_LSTM_DIM;

                stateStruct = new GRUCell(conDim, queryDim, device, BuilderParameters.RndInit);
                attStruct = new MLPAttentionStructure(queryDim, conDim, BuilderParameters.ATT_HID_DIM, device);
                terminalStruct = new LayerStructure(queryDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                answerStruct = new LayerStructure(queryDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);

                for (int i = 0; i < embedLayers.Count; i++) modelStructure.AddLayer(embedLayers[i]);

                modelStructure.AddLayer(stateStruct);
                modelStructure.AddLayer(attStruct);
                modelStructure.AddLayer(terminalStruct);
                modelStructure.AddLayer(answerStruct);

                modelStructure.AddLayer(contextD1LSTM);
                modelStructure.AddLayer(contextD2LSTM);

                modelStructure.AddLayer(queryD1LSTM);
                modelStructure.AddLayer(queryD2LSTM);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                    int link = 0;
                    for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++)
                        embedLayers.Add((LayerStructure)modelStructure.CompositeLinks[link++]);

                    stateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                    attStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    terminalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                    answerStruct = (LayerStructure)modelStructure.CompositeLinks[link++];

                    contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    contextD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];

                    queryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    queryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainAnswer,
                        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                        stateStruct, attStruct, answerStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph testCG = null;
                    if (BuilderParameters.IsTestFile)
                        testCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, answerStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("ReasoNet.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                        double testScore = 0;
                        if (testCG != null)
                        {
                            testScore = testCG.Execute();
                            Logger.WriteLog("Test Score {0}, At iter {1}", testScore, bestIter);
                        }

                        if (testScore > bestTestScore)
                        {
                            bestTestScore = testScore; bestIter = iter;
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "ReasoNet.best.model"), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }
                        Logger.WriteLog("Best Test Score {0}, {1}, At iter {1}", bestTestScore, bestTestScore, bestIter);
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, answerStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainAnswer = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestQuery = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TestAnswer = null;


            public static ItemFreqIndexDictionary WordFreqDict = null;

            public static int WordDim { get { return WordFreqDict.ItemDictSize; } }

            static int WordVocabLimited = BuilderParameters.VocabSize;

            static void ExtractVocab(string s2sRefFile, string vocab)
            {
                WordFreqDict = new ItemFreqIndexDictionary("WordVocab");

                Console.WriteLine("Extracting Vocab from File {0}", s2sRefFile);

                int lineIdx = 0;
                using (StreamReader mreader = new StreamReader(s2sRefFile))
                {
                    while (!mreader.EndOfStream)
                    {
                        string line = mreader.ReadLine();
                        string[] items = line.Split('\t');

                        string passage = items[0];
                        string query = items[1];
                        float answer = float.Parse(items[2]);

                        foreach (string word in passage.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            WordFreqDict.PushItem(word);
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            WordFreqDict.PushItem(word);
                        }

                        if (++lineIdx % 1000 == 0) Console.WriteLine("Extract Vocab from File {0}", lineIdx++);
                    }
                }

                /// keep top 50K vocabary.
                WordFreqDict.Filter(0, BuilderParameters.VocabSize);
                using (StreamWriter mwriter = new StreamWriter(vocab))
                {
                    WordFreqDict.Save(mwriter);
                }
            }

            /// <summary>
            /// Unknown Index = 0;
            /// </summary>
            static void ExtractCorpusIndex(string s2sRefFile, string srsRefIndexFile)
            {
                Console.WriteLine("Extract Index from File {0}", srsRefIndexFile);
                int lineIdx = 0;
                using (StreamWriter indexWriter = new StreamWriter(srsRefIndexFile))
                {
                    using (StreamReader mreader = new StreamReader(s2sRefFile))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string line = mreader.ReadLine();
                            string[] items = line.Split('\t');
                            string passage = items[0];
                            string query = items[1];
                            float answer = float.Parse(items[2]);

                            List<int> passageIndex = new List<int>();
                            foreach (string word in passage.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                int idx = WordFreqDict.IndexItem(word);
                                if (idx >= 0) passageIndex.Add(idx);
                            }

                            List<int> queryIndex = new List<int>();
                            foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                int idx = WordFreqDict.IndexItem(word);
                                if (idx >= 0) queryIndex.Add(idx);
                            }

                            indexWriter.WriteLine(string.Join(" ", passageIndex) + "\t" + string.Join(" ", queryIndex) + "\t" + answer.ToString());
                            if (++lineIdx % 10000 == 0) { Console.WriteLine("Extract Index from File {0}", lineIdx); }
                        }
                    }
                }
            }

            static void ExtractCorpusBinary(string questIndexFile, string contextBin, string queryBin, string answerBin, int miniBatchSize)
            {
                SeqSparseBatchData passage = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                DenseBatchData answer = new DenseBatchData(new DenseDataStat() { Dim = 1, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(contextBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryBin);
                BinaryWriter answerWriter = FileUtil.CreateBinaryWrite(answerBin);

                using (StreamReader indexReader = new StreamReader(questIndexFile))
                {
                    int lineIdx = 0;
                    while (!indexReader.EndOfStream)
                    {
                        string[] items = indexReader.ReadLine().Split('\t');

                        List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[0].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; contextFea.Add(tmp); }
                        passage.PushSample(contextFea);

                        List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                        foreach (string term in items[1].Split(' ')) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; queryFea.Add(tmp); }
                        query.PushSample(queryFea);

                        float score = float.Parse(items[2]);
                        answer.PushSample(new float[] { score }, 1);

                        if (passage.BatchSize >= miniBatchSize)
                        {
                            passage.PopBatchToStat(contextWriter);
                            query.PopBatchToStat(queryWriter);
                            answer.PopBatchToStat(answerWriter);
                        }

                        if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                    }
                    passage.PopBatchCompleteStat(contextWriter);
                    query.PopBatchCompleteStat(queryWriter);
                    answer.PopBatchCompleteStat(answerWriter);

                    Console.WriteLine("Context Stat {0}", passage.Stat.ToString());
                    Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                    Console.WriteLine("Answer Stat {0}", answer.Stat.ToString());
                }
            }

            public static void Init()
            {
                #region Preprocess Data.

                /// Step 1 : Extract Vocab from training Corpus.
                if (!File.Exists(BuilderParameters.Vocab)) ExtractVocab(BuilderParameters.TrainFile, BuilderParameters.Vocab);
                else { using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab)) { WordFreqDict = new ItemFreqIndexDictionary(mreader); } }

                /// Step 2 : Corpus 2 Index.
                if (BuilderParameters.IsTrainFile && !File.Exists(BuilderParameters.TrainIndexData)) { ExtractCorpusIndex(BuilderParameters.TrainFile, BuilderParameters.TrainIndexData); }
                if (BuilderParameters.IsTestFile && !File.Exists(BuilderParameters.TestIndexData)) { ExtractCorpusIndex(BuilderParameters.TestFile, BuilderParameters.TestIndexData); }

                /// Step 3 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile &&
                    (!File.Exists(BuilderParameters.TrainContextBinary) || !File.Exists(BuilderParameters.TrainQueryBinary) || !File.Exists(BuilderParameters.TrainAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TrainIndexData, BuilderParameters.TrainContextBinary, BuilderParameters.TrainQueryBinary, BuilderParameters.TrainAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsTestFile &&
                    (!File.Exists(BuilderParameters.TestContextBinary) || !File.Exists(BuilderParameters.TestQueryBinary) || !File.Exists(BuilderParameters.TestAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TestIndexData, BuilderParameters.TestContextBinary, BuilderParameters.TestQueryBinary, BuilderParameters.TestAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                #endregion.

                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBinary, BuilderParameters.TrainContextBinary + ".cursor");
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBinary, BuilderParameters.TrainQueryBinary + ".cursor");
                    TrainAnswer = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainAnswerBinary, BuilderParameters.TrainAnswerBinary + ".cursor");

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainAnswer.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsTestFile)
                {
                    TestContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestContextBinary, BuilderParameters.TestContextBinary + ".cursor");
                    TestQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestQueryBinary, BuilderParameters.TestQueryBinary + ".cursor");
                    TestAnswer = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TestAnswerBinary, BuilderParameters.TestAnswerBinary + ".cursor");

                    TestContext.InitThreadSafePipelineCashier(64, false);
                    TestQuery.InitThreadSafePipelineCashier(64, false);
                    TestAnswer.InitThreadSafePipelineCashier(64, false);
                }
            }
        }
    }
}