﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
using System.IO;

namespace BigLearn.DeepNet
{
    public class AppReasoNetBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static DNNRunMode RunMode { get { return TrainData.Equals(string.Empty) ? DNNRunMode.Predict : DNNRunMode.Train; } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static string TestData { get { return Argument["TEST"].Value; } }
            public static bool IsValidFile { get { return !ValidData.Equals(string.Empty); } }
            public static bool IsTestFile { get { return !TestData.Equals(string.Empty); } }
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static int FeatureDim { get { return int.Parse(Argument["FEATURE-DIM"].Value); } }
            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static bool IsSparseFeature { get { return int.Parse(Argument["IS-SPARSE-FEA"].Value) > 0; } }



            public static int Recurrent_Step { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }

            public static float MemAttGamma { get { return float.Parse(Argument["MEM-ATT-GAMMA"].Value); } }
            public static int MemAttHidden { get { return int.Parse(Argument["MEM-ATT-HIDDEN"].Value); } }
            public static int MemAttType { get { return int.Parse(Argument["MEM-ATT-TYPE"].Value); } }




            public static LossFunctionType LossFunction { get { return (LossFunctionType)(int.Parse(Argument["LOSS-FUNCTION"].Value)); } }
            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }

            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }


            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static DeviceType Device { get { return GPUID >= 0 ? DeviceType.GPU : DeviceType.CPU; } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }

            public static float WeightInitBias { get { return float.Parse(Argument["INIT-WEIGHT-BIAS"].Value); } }

            public static float WeightInitScale { get { return float.Parse(Argument["INIT-WEIGHT-SCALE"].Value); } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            /// <summary>
            /// Only need to Specify Data and Archecture of Deep Nets.
            /// </summary>
            static BuilderParameters()
            {
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "TEST Data."));
                Argument.Add("GPUID", new ParameterArgument("0", "GPU Device ID"));

                Argument.Add("FEATURE-DIM", new ParameterArgument("0", "Feature Dimension."));
                Argument.Add("MINI-BATCH", new ParameterArgument("1024", "Mini-Batch Size."));
                Argument.Add("IS-SPARSE-FEA", new ParameterArgument("1", "Is Sparse Input Feature."));


                Argument.Add("RECURRENT-STEP", new ParameterArgument("10", "Reasoning Steps."));

                Argument.Add("MEM-ATT-GAMMA", new ParameterArgument("10", " attention gamma"));
                Argument.Add("MEM-ATT-HIDDEN", new ParameterArgument("128", " attention hidden size."));
                Argument.Add("MEM-ATT-TYPE", new ParameterArgument("1", "0:inner product; 1: weighted inner product;"));



                Argument.Add("SHRINKLR", new ParameterArgument("0.001", "Shrink LR"));
                Argument.Add("LEARN-RATE", new ParameterArgument("0.0001", "Learn Rate."));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));
                Argument.Add("OPTIMIZER", new ParameterArgument(((int)DNNOptimizerType.SGD).ToString(), ParameterUtil.EnumValues(typeof(DNNOptimizerType))));


                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));


                Argument.Add("LOSS-FUNCTION", new ParameterArgument(((int)LossFunctionType.Regression).ToString(), ParameterUtil.EnumValues(typeof(LossFunctionType))));
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.MSE).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("ACTIVATION", new ParameterArgument(((int)A_Func.Tanh).ToString(), ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("LAYER-BIAS", new ParameterArgument("1", "DNN Layer Bias"));
                Argument.Add("LAYER-ARCH", new ParameterArgument(string.Empty, ParameterUtil.EnumValues(typeof(N_Type))));
                Argument.Add("LAYER-DROPOUT", new ParameterArgument("0", "DNN Layer Dropout"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("INIT-WEIGHT-BIAS", new ParameterArgument("0", "Init Model Weight Bias"));
                Argument.Add("INIT-WEIGHT-SCALE", new ParameterArgument("-1", "Init Model Weight Scale"));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_BUILDER; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        class TransformSparseMatrixRunner : StructRunner
        {
            public new SparseMatrixData Output { get { return (SparseMatrixData)base.Output; } set { base.Output = value; } }
            GeneralBatchInputData _Input;
            CudaPieceInt SmpIdx;
            CudaPieceInt FeaIdx;
            public TransformSparseMatrixRunner(GeneralBatchInputData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                _Input = input;
                SmpIdx = new CudaPieceInt(input.Stat.MAX_ELEMENTSIZE, Behavior.Device);
                FeaIdx = new CudaPieceInt(input.Stat.MAX_ELEMENTSIZE, Behavior.Device);

                Output = new SparseMatrixData(input.Stat.MAX_FEATUREDIM, input.Stat.MAX_ELEMENTSIZE, input.Stat.MAX_ELEMENTSIZE,
                                                    SmpIdx, FeaIdx, input.DenseFeatureData, null, Behavior.Device);

                int m = 0;
                for (int i = 0; i < input.Stat.MAX_BATCHSIZE; i++)
                {
                    for (int v = 0; v < input.Stat.MAX_FEATUREDIM; v++)
                    {
                        SmpIdx.MemPtr[m] = m + 1;
                        FeaIdx.MemPtr[m] = v;
                        m += 1;
                    }
                }
                SmpIdx.SyncFromCPU();
                FeaIdx.SyncFromCPU();
            }

            public override void Forward()
            {
                Output.Row = _Input.BatchSize * _Input.Stat.MAX_FEATUREDIM;
                Output.Length = _Input.BatchSize * _Input.Stat.MAX_FEATUREDIM;
            }

        }

        class GenerateSegMatrixRunner : StructRunner
        {

            public new SeqMatrixData Output { get { return (SeqMatrixData)base.Output; } set { base.Output = value; } }
            public CudaPieceInt SmpIdx;
            public CudaPieceInt SegmentMargin;
            MatrixData _Input;
            int SegSize;
            public GenerateSegMatrixRunner(MatrixData input, int segSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                _Input = input;
                SegSize = segSize;
                SmpIdx = new CudaPieceInt(input.MaxRow / segSize, Behavior.Device);
                SegmentMargin = new CudaPieceInt(input.MaxRow, Behavior.Device);

                for (int i = 0; i < input.MaxRow / segSize; i++)
                {
                    SmpIdx.MemPtr[i] = (i == 0 ? 0 : SmpIdx.MemPtr[i - 1]) + segSize;
                    for (int s = 0; s < segSize; s++)
                    {
                        SegmentMargin.MemPtr[i * segSize + s] = i;
                    }
                }
                SmpIdx.SyncFromCPU();
                SegmentMargin.SyncFromCPU();

                Output = new SeqMatrixData(input.Column, input.MaxRow, input.MaxRow / segSize,
                     input.Output, input.Deriv, SmpIdx, SegmentMargin, Behavior.Device);
            }

            public override void Forward()
            {
                Output.Row = _Input.Row;
                Output.Segment = _Input.Row / SegSize;
            }

        }

        public static ComputationGraph BuildComputationGraph(
                    IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> data,
                    MatrixStructure embeddingStruct,
                    DNNStructure initState,
                    MLPAttentionStructure attStruct,
                                                             GRUCell gruCell,
                                                             List<LayerStructure> termStruct,
                                                             LayerStructure actStruct,
                                                             RunnerBehavior Behavior, CompositeNNStructure Model)
        {
            ComputationGraph cg = new ComputationGraph();

            GeneralBatchInputData indata = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(data, Behavior));
            SparseMatrixData input = (SparseMatrixData)cg.AddRunner(new TransformSparseMatrixRunner(indata, Behavior));

            MatrixData X = (MatrixData)cg.AddRunner(new SparseMultiplicationRunner(input, new MatrixData(embeddingStruct), Behavior));

            SeqDenseBatchData Mem = new SeqDenseBatchData((SeqMatrixData)cg.AddRunner(new GenerateSegMatrixRunner(X, indata.Stat.MAX_FEATUREDIM, Behavior)));
            BiMatchBatchData matchData = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(Mem, Behavior));

            HiddenBatchData i_state = (HiddenBatchData)cg.AddRunner(new DNNRunner<GeneralBatchInputData>(initState, indata, Behavior));
            AdvReasoNetRunner reasonRunner = new AdvReasoNetRunner(i_state, BuilderParameters.Recurrent_Step,
                                                               Mem, matchData, attStruct, BuilderParameters.MemAttGamma, 0.9f,
                                                               gruCell, termStruct, Behavior, (Att_Type)BuilderParameters.MemAttType, true);
            cg.AddRunner(reasonRunner);


            List<HiddenBatchData> Actions = new List<HiddenBatchData>();
            for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
            {
                HiddenBatchData o = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(actStruct, reasonRunner.StatusData[i], Behavior));
                Actions.Add(o);
            }

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new BinaryContrastiveRewardRunner(indata.InstanceLabel, Actions.ToArray(), reasonRunner.AnsProb, 1, Behavior));
                    break;
                case DNNRunMode.Predict:
                    for (int d = 0; d < BuilderParameters.Recurrent_Step; d++)
                    {
                        cg.AddRunner(new ActivationRunner(Actions[d], A_Func.Sigmoid, Behavior));
                    }
                    HiddenBatchData finalOutput = (HiddenBatchData)cg.AddRunner(new WAdditionMatrixRunner(Actions, reasonRunner.AnsProb.ToList(), Behavior));

                    cg.AddRunner(new AUCDiskDumpRunner(indata.InstanceLabel, finalOutput.Output, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                    break;
            }

            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure deepModel = null;

            MatrixStructure embeddingStruct = null;
            DNNStructure initState = null;
            MLPAttentionStructure attStruct = null;
            GRUCell gruCell = null;
            List<LayerStructure> termStruct = new List<LayerStructure>();
            LayerStructure actStruct = null;

            if (BuilderParameters.SEED_MODEL != string.Empty)
            {
                deepModel = new CompositeNNStructure(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            }
            else
            {
                deepModel = new CompositeNNStructure();

                embeddingStruct = new MatrixStructure(100, BuilderParameters.FeatureDim, device);
                deepModel.AddLayer(embeddingStruct);

                initState = new DNNStructure(BuilderParameters.FeatureDim, BuilderParameters.LAYER_DIM,
                        BuilderParameters.ACTIVATION, BuilderParameters.LAYER_BIAS, device);
                deepModel.AddLayer(initState);

                int stateDim = BuilderParameters.LAYER_DIM.Last();

                attStruct = new MLPAttentionStructure(stateDim, 100, 100, device);
                deepModel.AddLayer(attStruct);

                gruCell = new GRUCell(100, stateDim, device);
                deepModel.AddLayer(gruCell);

                int t = stateDim;
                termStruct.Add(new LayerStructure(t, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device));
                deepModel.AddLayer(termStruct[0]);

                actStruct = new LayerStructure(stateDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device, true);
                deepModel.AddLayer(actStruct);
            }

            deepModel.InitOptimizer(OptimizerParameters.StructureOptimizer, new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Train });

            Logger.WriteLog("Loading ReasoNet Structure.");
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            ComputationGraph trainCG = BuildComputationGraph(DataPanel.Train, embeddingStruct, initState, attStruct, gruCell,
                termStruct, actStruct,  new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Train },  deepModel);

            ComputationGraph validCG = BuildComputationGraph(DataPanel.Valid, embeddingStruct, initState, attStruct, gruCell,
                termStruct, actStruct, new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Predict }, deepModel);

            trainCG.SetDelegateModel(deepModel);

            double bestValidScore = double.MinValue;
            int bestIter = -1;
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                Logger.WriteLog("Train Learn Rate {0}", OptimizerParameters.LearnRate);
                double loss = trainCG.Execute(); // trainCG.Execute();
                Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                
                double validScore = validCG.Execute();
                Logger.WriteLog("Iteration {0}, ValidScore {1}", iter, validScore);

                if (validScore >= bestValidScore)
                {
                    bestValidScore = validScore;
                    bestIter = iter;
                }

                Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestIter);
            }

            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Train = null;

            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> Valid = null;

            public static void ExtractBinary(string fileName)
            {
                if (File.Exists(fileName + ".fea.bin")) return;
                BinaryWriter dataWriter = new BinaryWriter(new FileStream(fileName + ".fea.bin", FileMode.Create, FileAccess.Write));
                GeneralBatchInputData data = new GeneralBatchInputData(new GeneralBatchInputDataStat()
                {
                    MAX_FEATUREDIM = BuilderParameters.FeatureDim,
                    MAX_BATCHSIZE = BuilderParameters.MiniBatchSize,
                    FeatureType = BuilderParameters.IsSparseFeature ? FeatureDataType.SparseFeature : FeatureDataType.DenseFeature
                }, DeviceType.CPU);
                using (StreamReader reader = new StreamReader(fileName))
                {
                    int rowIdx = 0;
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Trim().Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        float l = float.Parse(items[0]);

                        Dictionary<int, float> features = TextUtil.TLCFeatureExtract(items, ref data.Stat.MAX_FEATUREDIM);

                        data.PushSample(features, l);
                        if (data.BatchSize >= BuilderParameters.MiniBatchSize)
                        {
                            data.PopBatchToStat(dataWriter);
                        }
                        if (++rowIdx % 100000 == 0)
                            Console.WriteLine("Extract Row {0} into binary", (rowIdx));
                    }
                    data.PopBatchCompleteStat(dataWriter);
                    Console.WriteLine("Feature Stat {0}", data.Stat.ToString());
                }
            }


            public static void Init()
            {
                    ExtractBinary(BuilderParameters.TrainData);
                    Train = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainData + ".fea.bin");
                    Train.InitThreadSafePipelineCashier(128, ParameterSetting.RANDOM_SEED);

                    ExtractBinary(BuilderParameters.ValidData);
                    Valid = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.ValidData + ".fea.bin");
                    Valid.InitThreadSafePipelineCashier(128, false);
                
            }
        }
    }
}
