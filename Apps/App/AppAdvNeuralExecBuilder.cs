﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// The task becomes extremly complicated.
    /// </summary>
    public class AppAdvNeuralExecBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("LSTM-DIM", new ParameterArgument("8", "lstm dimension."));
                Argument.Add("CNN-DIM", new ParameterArgument("8", "cnn dimension."));
                Argument.Add("CNN-WIN", new ParameterArgument("1", "cnn window size."));
                Argument.Add("GROUP-EXEC", new ParameterArgument("10", "Group exec."));
                Argument.Add("RULE", new ParameterArgument("0", "RULE signal."));
                Argument.Add("MEM-SIZE", new ParameterArgument("10000", "RULE signal."));
            }
            public static int[] LSTM { get { return Argument["LSTM-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int CNN { get { return int.Parse(Argument["CNN-DIM"].Value); } }
            public static int CNN_WIN { get { return int.Parse(Argument["CNN-WIN"].Value); } }
            public static int GROUP_EXEC { get { return int.Parse(Argument["GROUP-EXEC"].Value); } }

            public static float RULE { get { return float.Parse(Argument["RULE"].Value); } }
            public static int MEM_SIZE { get { return int.Parse(Argument["MEM-SIZE"].Value); } }


        }
        public override BuilderType Type { get { return BuilderType.APP_ADV_NEURAL_EXEC; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        
        /// <summary>
        /// Record Q Function. (Assume reward is a real value function).
        /// q1, q2, q3, q4, q5.
        /// q1, q2, q3, q4, q5.
        /// Memory is the key value pair.
        /// Attention is searching the memory with key vector.
        /// </summary>
        class EpisodicMemory
        {
            float[] MemKey;
            float[] MemValue;
            int[] Timing;
            public int Size { get; set; }
            public int KeyDim { get; set; }
            public int ValueDim { get; set; }
            int Time { get; set; }
            public EpisodicMemory(int keyDim, int valueDim, int size)
            {
                MemKey = new float[keyDim * size];
                MemValue = new float[valueDim * size];
                Timing = new int[size];

                Size = size;
                KeyDim = keyDim;
                ValueDim = valueDim;

                Time = 0;
            }

            float GaussianKernel(float[] key, int memIdx)
            {
                float d = 0;
                for (int i = 0; i < KeyDim; i++)
                {
                    d = d +  (key[i] - MemKey[memIdx * KeyDim + i]) * (key[i] - MemKey[memIdx * KeyDim + i]);
                }
                return 1.0f / ( (float)Math.Sqrt(d) + 0.0001f);
            }

            public List<Tuple<int, bool, float>> Search(float[] key, int topK)
            {
                MinMaxHeap<int> heap = new MinMaxHeap<int>(topK, 1);
                for (int i = 0; i < Size; i++)
                {
                    if(Timing[i] > 0)
                    {
                        float d = GaussianKernel(key, i);
                        heap.push_pair(i, d);
                    }
                }

                List<Tuple<int, bool, float>> address = new List<Tuple<int, bool, float>>();

                float sum_k = heap.topK.Select(i => i.Value).Sum();
                while(heap.topK.Count > 0)
                {
                    KeyValuePair<int, float> item = heap.PopTop();

                    address.Insert(0, new Tuple<int, bool, float>(item.Key, item.Value > 1.0f/ (0.0000001f + 0.0001f), item.Value / sum_k));
                }

                return address;
            }

            public float[] Attention(List<Tuple<int, bool, float>> address)
            {
                float[] r = new float[ValueDim];
                foreach(Tuple<int, bool, float> item in address)
                {
                    for (int v = 0; v < ValueDim; v++)
                    {
                        r[v] = r[v] + item.Item3 * MemValue[item.Item1 * ValueDim + v];
                    }
                }
                return r;
            }

            public void Update(float[] key, List<Tuple<int, bool, float>> address, int actid, float reward)
            {
                int memIdx = 0;

                float new_u = 0;
                float new_c = 0;

                // update memory.
                if (address.Count > 0 && address[0].Item2)
                {
                    memIdx = address[0].Item1;
                    float c = MemValue[memIdx * ValueDim + actid * 2 + 1];
                    float u = MemValue[memIdx * ValueDim + actid * 2];

                    new_u = c / (c + 1.0f) * u + 1.0f / (c + 1.0f) * reward;
                    new_c = c + 1;
                }
                else
                {

                    memIdx = -1;
                    int minT = int.MaxValue;
                    for (int i = 0; i < Size; i++)
                    {
                        if (Timing[i] == 0)
                        {
                            memIdx = i;
                            break;
                        }
                        else if (Timing[i] < minT)
                        {
                            memIdx = i;
                            minT = Timing[i];
                        }
                    }

                    Array.Copy(key, 0, MemKey, memIdx * KeyDim, KeyDim);
                    Array.Clear(MemValue, memIdx * ValueDim, ValueDim);

                    new_u = reward;
                    new_c = 1;
                }


                MemValue[memIdx * ValueDim + actid * 2] = new_u;
                MemValue[memIdx * ValueDim + actid * 2 + 1] = new_c;

                Timing[memIdx] = Time;

            }

            public void UpdateTiming()
            {
                Time += 1;
            }
        }

        /// <summary>
        /// learning to operate a stack.
        /// </summary>
        class ExecRunner : CompositeNetRunner
        {
            public List<Tuple<string, int>> inputStr { get; set; }
            public int OutputValue { get; set; }
            public float Reward { get; set; }

            protected SeqSparseBatchData Fea { get; set; }
            protected Random random = new Random();
            protected int MaxLength { get; set; }
            protected int MaxFeaDim { get; set; }

            protected CudaPieceFloat Key { get; set; }
            protected CudaPieceFloat Score { get; set; }
            protected CudaPieceFloat Deriv { get; set; }

            List<float[]> Keys = new List<float[]>();
            List<List<Tuple<int, bool, float>>> KeyAddress = new List<List<Tuple<int, bool, float>>>();
            List<int> SelectedAct = new List<int>();
            public static int ActionNum = 4;
            //public ExecRunner(List<Tuple<string, int>> input, int maxActor, int maxLength, int maxFeaDim, 
            //    LayerStructure EmbedCNN, LSTMStructure scanfw, LSTMStructure scanbw, LayerStructure action, RunnerBehavior behavior) : base(behavior)
            //{
            //List<Tuple<string, int>> I = input;
            //for (int i=0;i<maxActor;i++)
            //{
            //    Actors.Add(new LSTMComputeActor(I, maxLength, maxFeaDim, EmbedCNN, scanfw, scanbw, att, behavior));
            //   I = Actors[i].outputStr;
            //}
            //inputStr = input;
            //}

            EpisodicMemory NNMem { get; set; }

            public ExecRunner(List<Tuple<string, int>> input, int maxActor, int maxLength, int maxFeaDim,
                LayerStructure EmbedCNN, LayerStructure cnn, EpisodicMemory mem, LayerStructure action, RunnerBehavior behavior) : base(behavior)
            {
                //List<Tuple<string, int>> I = input;
                //for (int i = 0; i < maxActor; i++)
                //{
                //    Actors.Add(new CNNComputeActor(I, maxLength, maxFeaDim, EmbedCNN, cnn, att, behavior));
                //    I = Actors[i].outputStr;
                //}
                inputStr = input;

                MaxLength = maxLength;
                MaxFeaDim = maxFeaDim;
                Fea = new SeqSparseBatchData(new SequenceDataStat(1, maxLength, maxFeaDim, maxLength), behavior.Device);

                SeqSparseConvRunner<SeqSparseBatchData> embedRunner = new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, Fea, Behavior);
                LinkRunners.Add(embedRunner);

                SeqDenseConvRunner<SeqDenseBatchData> cnnRunner = new SeqDenseConvRunner<SeqDenseBatchData>(cnn, embedRunner.Output, behavior);
                LinkRunners.Add(cnnRunner);

                Key = cnnRunner.Output.SentOutput;

                MatrixMultiplicationRunner actionRunner = new MatrixMultiplicationRunner(new MatrixData(cnnRunner.Output), new MatrixData(action), behavior);
                LinkRunners.Add(actionRunner);

                Score = actionRunner.Output.Output;
                Deriv = actionRunner.Output.Deriv;

                NNMem = mem;
            }


            public override void Forward()
            {
                // Fill Data.
                Fea.Clear();
                List<Dictionary<int, float>> feaMap = new List<Dictionary<int, float>>();
                foreach (Tuple<string, int> it in inputStr)
                {
                    Dictionary<int, float> dict = new Dictionary<int, float>();
                    dict[it.Item2] = 1;
                    feaMap.Add(dict);
                }
                Fea.PushSample(feaMap);

                base.Forward();

                bool rule_operate = random.NextDouble() < BuilderParameters.RULE;
                Score.SyncToCPU(feaMap.Count * ActionNum);
                Key.SyncToCPU(feaMap.Count * NNMem.KeyDim);

                SelectedAct.Clear();
                Keys.Clear();
                KeyAddress.Clear();

                Stack<int> Mem = new Stack<int>();
                for (int i = 0; i < inputStr.Count; i++)
                {
                    float[] score = new float[ActionNum];
                    Array.Copy(Score.MemPtr, i * ActionNum, score, 0, ActionNum);
                    // sample action according to policy.
                    float[] prob = Util.Softmax(score, 1);

                    int selectAct = -1;

                    if (rule_operate)
                    {
                        if (inputStr[i].Item2 == 0) selectAct = 0;
                        else if (inputStr[i].Item2 == 1) selectAct = 1;
                        else if (inputStr[i].Item2 == 2) selectAct = 2;
                        else if (inputStr[i].Item2 == 3) selectAct = 3;
                    }
                    else
                    {
                        float[] k = new float[NNMem.KeyDim];
                        
                        Array.Copy(Key.MemPtr, i * NNMem.KeyDim, k, 0, NNMem.KeyDim);
                        List<Tuple<int,bool,float>> address = NNMem.Search(k, 10);
                        float[] v = NNMem.Attention(address);

                        float sum = 0;
                        for (int a = 0; a < ActionNum; a++)
                        {
                            sum += (v[2 * a + 1] + 1);
                        }

                        float[] s = new float[ActionNum];
                        for (int a = 0; a < ActionNum; a++)
                        {
                            s[a] = 0.1f* prob[a] + v[2 * a] + (float)Math.Sqrt(2 * Math.Log(sum) / (v[2 * a + 1] + 1));
                        }
                        selectAct = Util.MaximumValue(s);

                        Keys.Add(k);
                        KeyAddress.Add(address);
                        //if (Behavior.RunMode == DNNRunMode.Train)
                        //{
                        //    selectAct = Util.Sample(prob, random);
                        //}
                        //else
                        //{
                        //    selectAct = Util.MaximumValue(prob);
                        //}
                    }
                    SelectedAct.Add(selectAct);


                    // push 
                    if(selectAct == 0)
                    {
                        if(inputStr[i].Item2 == 0)
                        {
                            Mem.Push(int.Parse(inputStr[i].Item1));
                        }
                        else
                        {
                            Mem.Push(0);
                        }
                    }
                    // operator +
                    else if(selectAct == 1)
                    {
                        int b = Mem.Count == 0 ? 0 : Mem.Pop();
                        int a = Mem.Count == 0 ? 0 : Mem.Pop();
                        Mem.Push(a + b);
                    } 
                    // operator -
                    else if(selectAct == 2)
                    {
                        int b = Mem.Count == 0 ? 0 : Mem.Pop();
                        int a = Mem.Count == 0 ? 0 : Mem.Pop();
                        Mem.Push(a - b);
                    }
                    // operator *
                    else if(selectAct == 3)
                    {
                        int b = Mem.Count == 0 ? 0 : Mem.Pop();
                        int a = Mem.Count == 0 ? 0 : Mem.Pop();
                        Mem.Push(a * b);
                    }
                }

                OutputValue = Mem.Count == 0 ? 0 : Mem.Pop();

            }

            public void RewardFeedback(float reward)
            {
                for(int i=0;i<inputStr.Count;i++)
                {
                    NNMem.Update(Keys[i], KeyAddress[i], SelectedAct[i], reward);
                }
                //NNMem.Update()
                
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.Zero(Deriv, Deriv.EffectiveSize);
                Deriv.SyncToCPU();
                for (int i = 0; i < SelectedAct.Count; i++)
                {
                    for (int a = 0; a < ActionNum; a++)
                    {
                        float p = Score.MemPtr[i * ActionNum + a];
                        if (a == SelectedAct[i])
                        {
                            Deriv.MemPtr[i * ActionNum + a] = (1 - p) * Reward;
                        }
                        else
                        {
                            Deriv.MemPtr[i * ActionNum + a] = -p * Reward;
                        }
                    }
                }
                Deriv.SyncFromCPU();
                base.Backward(cleanDeriv);
            }


        }

        Random random = new Random(0);
        static int Number_Range = 20;
        Tuple<List<Tuple<string, int>>, int, int> GenerateFormulation(float genComplex = 0.3f)
        {
            double ap = random.NextDouble();
            List<Tuple<string, int>> a = new List<Tuple<string, int>>();
            int aNum = 0;
            int aSort = 0; //0 : +, - ; 1 : * / ; 2 : single.
            if (ap > genComplex)
            {
                aNum = random.Next(1, Number_Range);
                aSort = 10;
                a.Add(new Tuple<string, int>(aNum.ToString(), 0));
            }
            else
            {
                Tuple<List<Tuple<string, int>>, int, int> ta = GenerateFormulation();
                a = ta.Item1;
                aNum = ta.Item2;
                aSort = ta.Item3;
            }

            double bp = random.NextDouble();
            List<Tuple<string, int>> b = new List<Tuple<string, int>>();
            int bNum = 0;
            int bSort = 0; //0 : +, - ; 1 : * / ; 2 : single.
            if (bp > genComplex)
            {
                bNum = random.Next(1, Number_Range);
                bSort = 10;
                b.Add(new Tuple<string, int>(bNum.ToString(), 0));
            }
            else
            {
                Tuple<List<Tuple<string, int>>, int, int> tb = GenerateFormulation();
                b = tb.Item1;
                bNum = tb.Item2;
                bSort = tb.Item3;
            }

            int cNum = 0;
            int cSort = 0; //0 : +, - ; 1 : * / ; 2 : single.

            int op = random.Next(3);
            string opstr = "";
            if(op == 0)
            {
                cNum = aNum + bNum;
                cSort = 0;
                opstr = "+";
            }
            if (op == 1)
            {
                cNum = aNum - bNum;
                cSort = 1;
                opstr = "-";
            }
            if (op == 2)
            {
                cNum = aNum * bNum;
                cSort = 2;
                opstr = "*";
            }
            if (op == 3)
            {
                if (bNum == 0) cNum = 0;
                else cNum = aNum / bNum;
                cSort = 3;
                opstr = "/";
            }

            List<Tuple<string, int>> c = new List<Tuple<string, int>>();

            /*
            if (cSort == 2 && (aSort == 0 || aSort == 1 ))
            {
                c.Add(new Tuple<string, int>("(", 5));
                c.AddRange(a);
                c.Add(new Tuple<string, int>(")", 6));
            }
            else
            {
                c.AddRange(a);
            }
            */
            c.AddRange(a);
            c.AddRange(b);
            c.Add(new Tuple<string, int>(opstr, op + 1));
            /*
            if( (cSort == 2 && (bSort == 0 || bSort == 1)) || ((cSort == 1) && (bSort <=1)) )
            {
                c.Add(new Tuple<string, int>("(", 5));
                c.AddRange(b);
                c.Add(new Tuple<string, int>(")", 6));
            }
            else
            {
                c.AddRange(b);
            }
            */
            return new Tuple<List<Tuple<string, int>>, int, int>(c, cNum, cSort);
        }


        
        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile, !DeepNet.BuilderParameters.IsLogFile);

            //DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            //IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            DeviceBehavior behavior = new DeviceBehavior(DeepNet.BuilderParameters.GPUID);
            StreamWriter train_file = new StreamWriter("train_data.tsv");
            List<Tuple<List<Tuple<string, int>>, int, int>> TrainDataset = new List<Tuple<List<Tuple<string, int>>, int, int>>();
            int maxLen = 0;
            for (int b=0;b<50000;b++)
            {
                Tuple<List<Tuple<string, int>>, int, int> data = GenerateFormulation();
                if (data.Item1.Count <= 3) continue;
                TrainDataset.Add(data);
                train_file.WriteLine(string.Join(" ", data.Item1.Select(i => i.Item1)) + "\t" + data.Item2.ToString());
                if (data.Item1.Count > maxLen) maxLen = data.Item1.Count;
            }
            train_file.Close();

            StreamWriter valid_file = new StreamWriter("valid_data.tsv");
            List<Tuple<List<Tuple<string, int>>, int, int>> ValidDataset = new List<Tuple<List<Tuple<string, int>>, int, int>>();
            for (int b = 0; b < 1000; b++)
            {
                Tuple<List<Tuple<string, int>>, int, int> data = GenerateFormulation();
                if (data.Item1.Count <= 3) continue;
                ValidDataset.Add(data);

                valid_file.WriteLine(string.Join(" ", data.Item1.Select(i => i.Item1)) + "\t" + data.Item2.ToString());
                if (data.Item1.Count > maxLen) maxLen = data.Item1.Count;
                //Console.WriteLine(string.Join(" ",data.Item1.Select(i => i.Item1)));
                //Console.WriteLine(data.Item2);
                //Console.WriteLine();
            }
            valid_file.Close();

            StreamWriter hard_file = new StreamWriter("hard_data.tsv");
            List<Tuple<List<Tuple<string, int>>, int, int>> HardDataset = new List<Tuple<List<Tuple<string, int>>, int, int>>();
            for (int b = 0; b < 1000; b++)
            {
                Tuple<List<Tuple<string, int>>, int, int> data = GenerateFormulation(0.4f);
                if (data.Item1.Count <= 3) continue;
                HardDataset.Add(data);

                hard_file.WriteLine(string.Join(" ", data.Item1.Select(i => i.Item1)) + "\t" + data.Item2.ToString());
                if (data.Item1.Count > maxLen) maxLen = data.Item1.Count;
                //Console.WriteLine(string.Join(" ",data.Item1.Select(i => i.Item1)));
                //Console.WriteLine(data.Item2);
                //Console.WriteLine();
            }
            hard_file.Close();

            Console.WriteLine("Generate train test finished {0}, {1}, {2}.", TrainDataset.Count, ValidDataset.Count, HardDataset.Count);
            
            
            int feaDim = 7;
            int embedDim = 8;
            int lstmDim = BuilderParameters.LSTM[0];
            int maxOperator = maxLen;
            int cnnDim = BuilderParameters.CNN;
            int cnnWin = BuilderParameters.CNN_WIN;

            CompositeNNStructure model = new CompositeNNStructure();

            LayerStructure embed = new LayerStructure(feaDim, embedDim, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false);
            model.AddLayer(embed);

            LSTMStructure scanbw = null;
            LSTMStructure scanfw = null;

            int outputDim = 0;

            LayerStructure cnn = null;
            if (lstmDim > 0)
            {
                scanfw = new LSTMStructure(embedDim, new int[] { lstmDim }, behavior.Device);
                scanbw = new LSTMStructure(embedDim, new int[] { lstmDim }, behavior.Device);
                model.AddLayer(scanfw);
                model.AddLayer(scanbw);

                outputDim = 2 * lstmDim;
            }
            else
            {
                cnn = new LayerStructure(embedDim, cnnDim, A_Func.Tanh, N_Type.Convolution_layer, cnnWin, 0, false, behavior.Device);
                model.AddLayer(cnn);

                outputDim = cnnDim;
            }

            EpisodicMemory Mem = new EpisodicMemory(outputDim, ExecRunner.ActionNum * 2, BuilderParameters.MEM_SIZE);


            LayerStructure att = new LayerStructure(outputDim, ExecRunner.ActionNum, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, behavior.Device);
            model.AddLayer(att);

            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior.TrainMode);

            ExecRunner[] groupRunner = new ExecRunner[BuilderParameters.GROUP_EXEC];
            for(int i=0;i<groupRunner.Length;i++)
            {
                List<Tuple<string, int>> I = new List<Tuple<string, int>>();
                //if(lstmDim > 0) groupRunner[i] = new ExecRunner(I, maxOperator, maxOperator, feaDim, embed, scanfw, scanbw, att, behavior.TrainMode);
                //else 
                groupRunner[i] = new ExecRunner(I, maxOperator, maxOperator, feaDim, embed, cnn, Mem,  att, behavior.TrainMode);
            }

            List<Tuple<string, int>> PredI = new List<Tuple<string, int>>();
            ExecRunner predRunner = //lstmDim > 0 ?
                //new ExecRunner(PredI, maxOperator, maxOperator, feaDim, embed, scanfw, scanbw, att, behavior.PredictMode) :
                new ExecRunner(PredI, maxOperator, maxOperator, feaDim, embed, cnn, Mem, att, behavior.TrainMode) ;


            for (int iter = 0; iter < 200; iter++)
            {
                double avgR = 0;
                int allZero = 0;
                for (int b = 0; b < TrainDataset.Count; b++)
                {
                    Mem.UpdateTiming();

                    int avgReward = 0;
                    for (int t = 0; t < groupRunner.Length; t++)
                    {
                        groupRunner[t].inputStr.Clear();
                        groupRunner[t].inputStr.AddRange(TrainDataset[b].Item1);
                        groupRunner[t].Forward();

                        if (groupRunner[t].OutputValue == TrainDataset[b].Item2)
                        {
                            avgReward += 1;

                            groupRunner[t].RewardFeedback(1);
                        }
                        else
                        {
                            groupRunner[t].RewardFeedback(0);
                        }
                    }
                    

                    if (avgReward > 0)
                    {
                        for (int t = 0; t < groupRunner.Length; t++)
                        {
                            if (groupRunner[t].OutputValue == TrainDataset[b].Item2)
                            {
                                groupRunner[t].Reward = 1.0f / avgReward - 1.0f / groupRunner.Length;
                            }
                            else
                            {
                                groupRunner[t].Reward = -1.0f / groupRunner.Length;
                            }
                        }

                        foreach (ModelOptimizer g in model.ModelOptimizers)
                        {
                            g.Optimizer.BeforeGradient();
                        }
                        for (int t = 0; t < groupRunner.Length; t++)
                        {
                            groupRunner[t].CleanDeriv();
                            groupRunner[t].Backward(false);
                            groupRunner[t].Update();
                        }
                        foreach (ModelOptimizer g in model.ModelOptimizers)
                        {
                            g.Optimizer.AfterGradient();
                        }
                    }
                    else
                    {
                        allZero += 1;
                    }

                    avgR += avgReward * 1.0 / groupRunner.Length;

                    if((b+1) % 100 == 0) { Console.WriteLine("avg reward {0}, all zero {1}, {2}", avgR / b, allZero * 1.0f/ b, b); }
                }


                int CorrectNum = 0;
                for (int b = 0; b < ValidDataset.Count; b++)
                {
                    predRunner.inputStr.Clear();
                    predRunner.inputStr.AddRange(ValidDataset[b].Item1);
                    predRunner.Forward();

                    if (predRunner.OutputValue == ValidDataset[b].Item2)
                    {
                        CorrectNum += 1;
                    }
                }

                Console.WriteLine("Correct Valid Form {0}, Accuracy {1}", CorrectNum, CorrectNum * 1.0f / ValidDataset.Count);


                int HardCorrectNum = 0;
                for (int b = 0; b < HardDataset.Count; b++)
                {
                    predRunner.inputStr.Clear();
                    predRunner.inputStr.AddRange(HardDataset[b].Item1);
                    predRunner.Forward();

                    if (predRunner.OutputValue == HardDataset[b].Item2)
                    {
                        HardCorrectNum += 1;
                    }
                }
                Console.WriteLine("Correct Hard Form {0}, Accuracy {1}", HardCorrectNum, HardCorrectNum * 1.0f / HardDataset.Count);

            }
            //Console.ReadLine();
            

            Logger.CloseLog();
        }

    }
}
