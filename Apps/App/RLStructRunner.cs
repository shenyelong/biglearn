﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{

    public class RewardData
    {
        public float Success = 0;
        public float Fail = 0;
    }

    public class RLData
    {
        public static int PriorSuccess = 10;
        public static int PriorCount = 10;

        public float C = (float)Math.Sqrt(2) / 5.0f ;
        public int StatusNum = 0;

        public CudaPieceFloat Status;
        public CudaPieceFloat Data;
        public CudaPieceFloat Success;
        public CudaPieceFloat Count;

        public RLData(int size, List<float> status, DeviceType device)
        {
            Data = new CudaPieceFloat(size, true, device == DeviceType.GPU);

            StatusNum = status.Count;

            Status = new CudaPieceFloat(StatusNum, true, device == DeviceType.GPU);
            for (int i = 0; i < status.Count; i++)
            {
                Status.MemPtr[i] = status[i];
            }
            Status.SyncFromCPU();


            Success = new CudaPieceFloat(Data.Size * StatusNum, true, device == DeviceType.GPU);
            Count = new CudaPieceFloat(Data.Size * StatusNum, true, device == DeviceType.GPU);
            for (int i = 0; i < Data.Size * StatusNum; i++)
            {
                Success.MemPtr[i] = Util.URandom.Next(0, PriorSuccess) + (float)Util.URandom.NextDouble();
                Count.MemPtr[i] = Success.MemPtr[i] + Util.URandom.Next(0, PriorCount) + (float)Util.URandom.NextDouble();
            }
            Success.SyncFromCPU();
            Count.SyncFromCPU();
        }
    }

    public class RLImageLinkStructure : Structure
    {
        public ImageFilterParameter Param;


        public RLData RLFilter;

        public CudaPieceFloat Filter { get { return RLFilter.Data; } }
        public CudaPieceFloat Bias;
        public override DataSourceID Type { get { return DataSourceID.RLImageLinkStructure; } }

        public RLImageLinkStructure(ImageFilterParameter param, DeviceType device)
        {
            Param = param;
            RLFilter = new RLData(Param.FilterSize, new List<float>() { -0.5f, -0.2f,  0,  0.2f, 0.5f }, device);
            Bias = new CudaPieceFloat(Param.O, true, device == DeviceType.GPU);
            Bias.Init(0);
        }
    }


    public sealed class RLLayerStructure : Structure
    {
        public int Neural_In;
        public int Neural_Out;

        public A_Func Af;

        public RLData RLWeight;
        public CudaPieceFloat Weight { get { return RLWeight.Data; } }
        public CudaPieceFloat Bias;
        public bool IsBias = false;
        public override DataSourceID Type { get { return DataSourceID.RLLayerStructure; } }

        public RLLayerStructure(int layer_in, int layer_out, A_Func af, DeviceType device)
        {
            Neural_In = layer_in;
            Neural_Out = layer_out;
            Af = af;

            RLWeight = new RLData(Neural_In * Neural_Out, new List<float>() { -0.5f, -0.2f,  0,  0.2f, 0.5f }, device);
            Bias = new CudaPieceFloat(Neural_Out, true, device == DeviceType.GPU);
            Bias.Init(0);
        }

    }


    public class RLSampleRunner : StructRunner
    {
        RLData Data;
        RewardData Reward;

        public RLSampleRunner(RLData data, RewardData reward, RunnerBehavior behavior) : base(Structure.Empty, behavior)
        {
            Data = data;
            Reward = reward;
        }

        public override void Forward()
        {
            Cudalib.UCTStateSampling(Data.Count.CudaPtr, Data.Success.CudaPtr, Data.Status.CudaPtr, Data.StatusNum, Data.Data.CudaPtr, Data.Data.Size, Data.C);

        }

        public override void Update()
        {
            //policy network for update.
            //Cudalib.StateUpdating(Data.Count.CudaPtr, Data.Success.CudaPtr, Data.Status.CudaPtr, Data.StatusNum, Data.Data.CudaPtr, Data.Data.Size, 1 , Reward.Success * 1.0f / Reward.Count);
            throw new NotImplementedException();
        }
    }

    public class RLImageConvRunner<IN> : StructRunner<RLImageLinkStructure, IN> where IN : ImageDataSource
    {
        public new ImageDataSource Output { get { return (ImageDataSource)base.Output; } set { base.Output = value; } }
        ImageDataSource LayerPoolingOutput = null;
        CudaPieceInt LayerMaxPoolingIndex = null;

        public RLImageConvRunner(RLImageLinkStructure model, ImageDataSource input, RunnerBehavior behavior) : base(model, input, behavior)
        {

            LayerPoolingOutput = new ImageDataSource(Input.Stat.MAX_BATCHSIZE,
                                               Model.Param.PoolingX(Input.Stat.Width),
                                               Model.Param.PoolingY(Input.Stat.Height),
                                               Model.Param.O,
                                               Behavior.Device);

            if (Model.Param.PoolingParameter.Stride == 1 && Model.Param.PoolingParameter.SX == 1)
            {
                Output = LayerPoolingOutput;
                LayerMaxPoolingIndex = null;
            }
            else
            {
                Output = new ImageDataSource(Input.Stat.MAX_BATCHSIZE,
                                                   Model.Param.OutputWidth(Input.Stat.Width),
                                                   Model.Param.OutputHeight(Input.Stat.Height),
                                                   Model.Param.O,
                                                   Behavior.Device);

                LayerMaxPoolingIndex = new CudaPieceInt(Output.Stat.MAX_BATCHSIZE * Output.Stat.Width * Output.Stat.Height * Output.Stat.Depth , true, Behavior.Device == DeviceType.GPU);
            }
        }

        public override void Forward()
        {
            LayerPoolingOutput.BatchSize = Input.BatchSize;
            Input.Data.SyncToCPU();
            //Cudalib.ImageConvolution(Input.Data.CudaPtr, Input.BatchSize, Input.Stat.Width, Input.Stat.Height, Input.Stat.Depth,
            //                            Model.Filter.CudaPtr, Model.Param.SX, Model.Param.C, Model.Param.O, Model.Param.Pad, Model.Param.Stride,
            //                            LayerPoolingOutput.Data.CudaPtr, LayerPoolingOutput.Stat.Width, LayerPoolingOutput.Stat.Height);
            Model.Filter.SyncToCPU();
            LayerPoolingOutput.Data.SyncToCPU();

            ComputeLib.CNNForwardPropagate(Input.Data, Input.BatchSize, Input.Stat.Depth, Input.Stat.Height, Input.Stat.Width,
                Model.Filter, Model.Param.O, Model.Param.SX, Model.Param.SX, Model.Param.Pad, Model.Param.Stride,
                LayerPoolingOutput.Data, LayerPoolingOutput.Stat.Height, LayerPoolingOutput.Stat.Width);

            Model.Filter.SyncToCPU();
            LayerPoolingOutput.Data.SyncToCPU();
            //PerfCounter.Manager.Instance["Image_Conv_Forward_V1"].TakeCount(forwardCounter);
            //LayerPoolingOutput.Data.CopyOutFromCuda();

            if (LayerMaxPoolingIndex != null)
            {
                Output.BatchSize = Input.BatchSize;
                Cudalib.MaxImagePooling(LayerPoolingOutput.Data.CudaPtr, LayerPoolingOutput.BatchSize, LayerPoolingOutput.Stat.Width, LayerPoolingOutput.Stat.Height, LayerPoolingOutput.Stat.Depth,
                                        LayerMaxPoolingIndex.CudaPtr,
                                        Model.Param.PoolingParameter.SX, Model.Param.PoolingParameter.Stride,
                                        Output.Data.CudaPtr, Output.Stat.Width, Output.Stat.Height);
            }

            Output.Data.SyncToCPU();

            ActivationFuncLib.Image_ActiveOutput(Output.Data, Model.Bias, Model.Param.Af, Output.BatchSize, Output.Stat.Width, Output.Stat.Height, Output.Stat.Depth);

            Output.Data.SyncToCPU();
        }

    }

    public class RLImageFullyConnectRunner<IN> : StructRunner<RLLayerStructure, IN> where IN : ImageDataSource
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

        public RLImageFullyConnectRunner(Structure model, ImageDataSource input, RunnerBehavior behavior) : base(model, input, behavior)
        {
            Output = new HiddenBatchData(Input.Stat.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
        }

        public override void Forward()
        {
            ComputeLib.Matrix_Multipy(Input.Data, Model.Weight, Output.Output.Data, Input.BatchSize,
                            Model.Neural_In, Model.Neural_Out, 0);
            Output.Output.Data.SyncToCPU();
            Output.BatchSize = Input.BatchSize;
            ActivationFuncLib.ActivationFunc(Behavior.Computelib, Output.Output.Data, Output.BatchSize, Output.Dim, Model.Af, Model.Bias);
            Output.Output.Data.SyncToCPU();
        }

    }

    public class RLFullyConnectHiddenRunner<IN> : StructRunner<RLLayerStructure, IN> where IN : HiddenBatchData
    {
        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat BiasOne;

        public RLFullyConnectHiddenRunner(RLLayerStructure model, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
        {
            Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Model.Neural_Out, Behavior.RunMode, Behavior.Device);
            if (Model.IsBias)
            {
                BiasOne = new CudaPieceFloat(Input.MAX_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                BiasOne.Init(1);
            }
        }

        public override void Forward()
        {
            Output.BatchSize = Input.BatchSize;
            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Weight, 0, Output.Output.Data, 0, Input.BatchSize,
                            Model.Neural_In, Model.Neural_Out, 0, 1, false, false);
            Output.Output.Data.SyncToCPU();
            ActivationFuncLib.ActivationFunc(ComputeLib, Output.Output.Data, Input.BatchSize, Model.Neural_Out, Model.Af, Model.Bias);
        }
    }


    public class RLMultiClassSoftmaxRunner : ObjectiveRunner
    {
        public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } set { base.Output = value; } }
        public CudaPieceFloat Label { get; set; }

        public RewardData Reward { get; set; }

        /// <summary>
        /// Label could be null, the first element will be viewed as positive, others are negative.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="output"></param>
        /// <param name="behavior"></param>
        public RLMultiClassSoftmaxRunner(CudaPieceFloat label, DenseBatchData output, RewardData reward, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
        {
            Output = output;
            Label = label;
            Reward = reward;
        }

        public override void Forward()
        {
            Reward.Fail = Output.BatchSize;
            Reward.Success = 0;
            if (Output.BatchSize == 0)
            {
                ObjectiveScore = 0; return;
            }

            Output.Data.SyncToCPU();
            Label.SyncToCPU(Output.BatchSize);

            for (int i = 0; i < Output.BatchSize; i++)
            {
                int target = (int)(Label.MemPtr[i]);

                float[] predArray = Output.Data.MemPtr.Skip(i * Output.Stat.Dim).Take(Output.Stat.Dim).ToArray();
                int maxIndex = Util.MaximumValue(predArray);
                if (maxIndex == target) Reward.Success += 1;
            }

            ObjectiveScore = (Reward.Fail - Reward.Success) * 1.0f / Reward.Fail;
        }
    }
}