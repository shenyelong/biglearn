﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// Sequence Model.
    /// </summary>
    public class AppReinforceWalkSeqModelV2Builder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("VOCAB-DIM", new ParameterArgument(string.Empty, "Source Vocab Dimension."));

                Argument.Add("VOCAB-EMD", new ParameterArgument(string.Empty, "vocab embeddings."));
                Argument.Add("VOCAB-UPDATE", new ParameterArgument("1", "UPDATE vocab embeddings."));

                Argument.Add("MINI-BATCH", new ParameterArgument("64", "Mini Batch Size"));
                Argument.Add("ENCODE-EMBED", new ParameterArgument("64", "Encode Embed Dim"));

                ///Language Word Error Rate.
                //Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));

                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("IS-SHARE-EMD", new ParameterArgument("1", "is share embedding vec."));

                Argument.Add("K-PRED", new ParameterArgument("1", "Next K prediction."));

                Argument.Add("MCTS-EXP", new ParameterArgument("MCTS strategy", "strategy sampling"));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("MCTS-NUM", new ParameterArgument("64", "Monto Carlo Tree Search Number"));

                Argument.Add("RW-NET", new ParameterArgument("100,1", "Reward Network."));
                Argument.Add("RW-AF", new ParameterArgument("2,1", "Reward Afunc."));

                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));

                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("REWARD-S", new ParameterArgument("0", "0:blue; 1:blue-avg; 2:blue-estimate; 3:maxblue."));
                Argument.Add("SEARCH-T", new ParameterArgument("0", "0:beam search; 1:mcts search."));

                Argument.Add("NGRAM-W", new ParameterArgument("0.8,0.2", "ngram weight."));
                Argument.Add("T-NGRAM-W", new ParameterArgument("0.25,0.25,0.25,0.25", "ngram weight."));
                Argument.Add("PL-LR", new ParameterArgument("0:1.0", "Policy Learning rate schedule."));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }

            public static string ValidData { get { return Argument["VALID"].Value; } }

            public static int Vocab_Dim { get { return int.Parse(Argument["VOCAB-DIM"].Value); } }
            public static string Vocab_Emd { get { return (Argument["VOCAB-EMD"].Value); } }
            public static bool Vocab_Update { get { return int.Parse(Argument["VOCAB-UPDATE"].Value) > 0; } }

            public static int Mini_Batch { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int Encode_Embed { get { return int.Parse(Argument["ENCODE-EMBED"].Value); } }
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] RwNet { get { return Argument["RW-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            /// <summary>
            /// A_Func { Linear = 0, Tanh = 1, Rectified = 2, Sigmoid = 3 };
            /// </summary>
            public static A_Func[] RwAf { get { return Argument["RW-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            //public static Vocab2Freq mVocabDict = null;
            //public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Tgt_Vocab); return mVocabDict; } }
            //public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            //public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
         
            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
            public static bool Is_Share_Emd { get { return int.Parse(Argument["IS-SHARE-EMD"].Value) > 0; } }
            public static int K_PRED { get { return int.Parse(Argument["K-PRED"].Value); } }
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }
            public static int EStrategy(int mcts_idx)
            {
                string[] idxs = Argument["MCTS-EXP"].Value.Split(',').ToArray();
                for (int i = 0; i < idxs.Length; i++)
                {
                    if (mcts_idx < int.Parse(idxs[i]))
                    {
                        return i;
                    }
                }
                return idxs.Length - 1;
            }

            public static List<Tuple<int, float>> ScheduleRewardDiscount
            {
                get
                {
                    return Argument["R-FB"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static float Epsilon(int step)
            {
                for (int i = 0; i < ScheduleRewardDiscount.Count; i++)
                {
                    if (step < ScheduleRewardDiscount[i].Item1)
                    {
                        float lambda = (step - ScheduleRewardDiscount[i - 1].Item1) * 1.0f / (ScheduleRewardDiscount[i].Item1 - ScheduleRewardDiscount[i - 1].Item1);
                        return lambda * ScheduleRewardDiscount[i].Item2 + (1 - lambda) * ScheduleRewardDiscount[i - 1].Item2;
                    }
                }
                return ScheduleRewardDiscount.Last().Item2;
            }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }
            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }
            public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static float UPDATE_R_DISCOUNT { get { return float.Parse(Argument["UPDATE-R-DISCOUNT"].Value); } }

            public static float MSE_LAMBDA { get { return float.Parse(Argument["MSE-LAMBDA"].Value); } }
            public static float BASE_LAMBDA { get { return float.Parse(Argument["BASE-LAMBDA"].Value); } }
            public static float PROB_LAMBDA { get { return float.Parse(Argument["PROB-LAMBDA"].Value); } }

            public static int REWARD_S { get { return int.Parse(Argument["REWARD-S"].Value); } }
            public static int SEARCH_T { get { return int.Parse(Argument["SEARCH-T"].Value); } }

            public static float[] NGRAM_W { get { return Argument["NGRAM-W"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static float[] T_NGRAM_W { get { return Argument["T-NGRAM-W"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static List<Tuple<int, float>> PolicyScheduleList
            {
                get
                {
                    return Argument["PL-LR"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }
            public static float PolicySchedule(int epoch)
            {
                for (int i = 0; i < PolicyScheduleList.Count; i++)
                {
                    if (epoch < PolicyScheduleList[i].Item1)
                    {
                        float lambda = (epoch - PolicyScheduleList[i - 1].Item1) * 1.0f / (PolicyScheduleList[i].Item1 - PolicyScheduleList[i - 1].Item1);
                        return lambda * PolicyScheduleList[i].Item2 + (1 - lambda) * PolicyScheduleList[i - 1].Item2;
                    }
                }
                return PolicyScheduleList.Last().Item2;
            }

        }

        public override BuilderType Type { get { return BuilderType.APP_REINFORCE_WALK_SEQ_MODEL_V2; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        /// <summary>
        /// Sample Data. 
        /// </summary>
        class DataSampleRunner : StructRunner
        {
            List<List<int>> Data { get; set; }
            int MaxBatchSize { get; set; }
            int VocabSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);

            /// <summary>
            /// Sequence Sparse Data.
            /// </summary>
            public new SeqSparseBatchData Output { get; set; }
            public CudaPieceFloat NextWord { get; set; }
            public List<string> HistoryWords { get; set; }
            public List<int> SmpIdx { get; set; }
            public DataSampleRunner(List<List<int>> data, int maxBatchSize, int vocabSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                MaxBatchSize = maxBatchSize;
                VocabSize = vocabSize;
                Shuffle = new DataRandomShuffling(Data.Count, random);

                Output = new SeqSparseBatchData(new SequenceDataStat()
                {
                    FEATURE_DIM = VocabSize,
                    MAX_ELEMENTSIZE = maxBatchSize * DataPanel.Max_Seq_Len,
                    MAX_SEQUENCESIZE = maxBatchSize * DataPanel.Max_Seq_Len,
                    MAX_BATCHSIZE = maxBatchSize
                }, behavior.Device);

                // predict the next k words.
                NextWord = new CudaPieceFloat(maxBatchSize * DataPanel.Max_Seq_Len * BuilderParameters.K_PRED, behavior.Device);
                HistoryWords = new List<string>();
                SmpIdx = new List<int>();
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                List<List<Dictionary<int, float>>> dicts = new List<List<Dictionary<int, float>>>();
                HistoryWords.Clear();
                SmpIdx.Clear();
                int nextIdx = 0;
                int groupIdx = 0;
                Output.Clear();
                while (groupIdx < MaxBatchSize)
                {
                    int idx = Shuffle.OrderNext();  // Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }

                    int pos = -1;
                    for (int w = 0; w < Data[idx].Count - 3; w++)
                    {
                        if (Data[idx][w] == 2) pos = w;
                    }

                    if (pos <= 0 || Data[idx].Count - pos > BuilderParameters.K_PRED)
                    {
                        continue;
                    }

                    List<Dictionary<int, float>> wdict = new List<Dictionary<int, float>>();
                    List<int> hisWords = new List<int>();
                    for (int w = 0; w <= pos; w++) // widx in Data[idx])
                    {
                        int widx = Data[idx][w];
                        Dictionary<int, float> d = new Dictionary<int, float>();
                        d.Add(widx, 1);
                        wdict.Add(d);

                        //string currentWord = string.Join("-", hisWords) + "-" + widx.ToString();
                        //HistoryWords.Add(currentWord);
                        hisWords.Add(widx);
                    }


                    for (int n = 0; n < BuilderParameters.K_PRED; n++)
                    {
                        if (pos + 1 + n < Data[idx].Count)
                        {
                            NextWord.MemPtr[nextIdx * BuilderParameters.K_PRED + n] = Data[idx][pos + 1 + n];
                        }
                        else
                        {
                            NextWord.MemPtr[nextIdx * BuilderParameters.K_PRED + n] = 0;
                        }
                    }
                    nextIdx += 1;

                    dicts.Add(wdict);
                    HistoryWords.Add(string.Join("-", hisWords));
                    SmpIdx.Add(idx);
                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
                Output.PushSamples(dicts);

                NextWord.EffectiveSize = nextIdx * BuilderParameters.K_PRED;
                NextWord.SyncFromCPU();
            }
        }

        public class StatusData : BatchData
        {
            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public List<Tuple<HiddenBatchData, HiddenBatchData>> StateEmbed { get; set; }

            public int MaxBatchSize { get { return StateEmbed.Last().Item1.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.Last().Item1.BatchSize; } }

            /// <summary>
            /// Node ID.
            /// </summary>
            public List<int> NodeID { get; set; }

            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            public List<float> LogProb { get; set; }

            public List<string> StatusKey { get; set; }

            /// <summary>
            /// Raw Query.
            /// </summary>
            public GraphQueryData GraphQuery { get; set; }

            public HiddenBatchData CandidateProb = null;
            public HiddenBatchData CandidateScore = null;

            public HiddenBatchData Reward = null;

            /// <summary>
            /// logprobability of the node.
            /// </summary>
            /// <param name="batchIdx"></param>
            /// <returns></returns>
            public float GetLogProb(int batchIdx)
            {
                if (Step == 0) { return 0; }
                else { return LogProb[batchIdx]; }
            }


            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            //public List<Tuple<int, int>> MatchCandidate = null;
            //public SeqVectorData MatchCandidateProb = null;

            public List<int> PreStatusIndex { get; set; }
            public int GetPreStatusIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else return PreStatusIndex[batchIdx];
            }
            public int GetOriginalStatsIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else
                {
                    int b = GetPreStatusIndex(batchIdx);
                    return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
                }
            }

            public List<int> GetHistoryNodeIdxs(int batchIdx)
            {
                List<int> results = new List<int>();
                if (Step > 0)
                {
                    int b = GetPreStatusIndex(batchIdx);
                    List<int> h = GraphQuery.StatusPath[Step - 1].GetHistoryNodeIdxs(b);
                    results.AddRange(h);
                    results.Add(NodeID[batchIdx]);
                }
                return results;
            }


            public int Step;

            public StatusData(GraphQueryData interData, List<int> nodeIndex, List<string> nodeKey, List<Tuple<HiddenBatchData, HiddenBatchData>> stateEmbed, DeviceType device)
            {
                GraphQuery = interData;
                NodeID = nodeIndex;
                StatusKey = nodeKey;
                StateEmbed = stateEmbed;
                Step = GraphQuery.StatusPath.Count;
            }
        }

        /// <summary>
        /// Graph Query Data.
        /// </summary>
        public class GraphQueryData : BatchData
        {
            public List<StatusData> StatusPath = new List<StatusData>();

            /// <summary>
            /// item1 : step; item2 : batchIdx; item3:prob;
            /// </summary>
            public List<Tuple<int, int, float>> Results = new List<Tuple<int, int, float>>();
            public List<float> ResultScore = new List<float>();
            public List<int> SmpIdx { get; set; }
            public void ResultClear()
            {
                Results.Clear();
                ResultScore.Clear();
            }

            public int MaxBatchSize { get { return StatusPath[0].MaxBatchSize; } }
            public int BatchSize { get { return StatusPath[0].BatchSize; } }

            public CudaPieceFloat NextWords { get; set; }
            public int N { get; set; }
            /// <summary>
            ///  start with initial stateEmbed.
            /// </summary>
            /// <param name="stateEmbed"></param>
            /// <param name="device"></param>
            public GraphQueryData(CudaPieceFloat nextWords, List<int> smpIdx, int n, List<string> stateKey, List<Tuple<HiddenBatchData, HiddenBatchData>> stateEmbed, DeviceType device)
            {
                NextWords = nextWords;
                SmpIdx = smpIdx;
                N = n;
                StatusData init_status = new StatusData(this, null, stateKey, stateEmbed, device);
                StatusPath.Add(init_status);
            }

            public void AddNewState(List<int> nodeIdx, List<string> stateKey, List<Tuple<HiddenBatchData, HiddenBatchData>> stateEmbed, List<float> logProb, List<int> preStateIdx, DeviceType device)
            {
                StatusData next_status = new StatusData(this, nodeIdx, stateKey, stateEmbed, device);
                StatusPath.Add(next_status);
                next_status.LogProb = logProb;
                next_status.PreStatusIndex = preStateIdx;
            }

            public List<int> Reference(int orgB)
            {
                List<int> refList = new List<int>();
                NextWords.SyncToCPU();
                for(int i=0;i<BuilderParameters.K_PRED;i++)
                {
                    int n = (int)NextWords.MemPtr[orgB * BuilderParameters.K_PRED + i];
                    refList.Add(n);
                    if (n == 0) break;
                }
                return refList;
            }

            public List<int> Candidate(int b, int step)
            {
                List<int> canList = StatusPath[step].GetHistoryNodeIdxs(b);
                canList.Add(0);
                return canList;
            }

            public List<int> GetBatchIdxs(int b, int step)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < StatusPath[step].BatchSize; i++)
                {
                    if (StatusPath[step].GetOriginalStatsIndex(i) == b) r.Add(i);
                }
                return r;
            }
        }

        class RewardRunner : ObjectiveRunner
        {
            new List<GraphQueryData> Input { get; set; }
            EpisodicMemoryV2 Memory { get; set; }

            int Epoch = 0;

            StreamWriter writer = null;

            public override void Init()
            {
                writer = new StreamWriter(BuilderParameters.ConfigPath + ".tmpPoem.reward");
                Logger.WriteLog("policy learning rate {0}", BuilderParameters.PolicySchedule(Epoch));
            }

            public override void Complete()
            {
                Epoch += 1;
                writer.Close();
            }

            public RewardRunner(List<GraphQueryData> input, EpisodicMemoryV2 memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                float trueReward = 0;
                int sampleNum = 0;
                float mseloss = 0;
                ObjectiveDict.Clear();

                List<List<int>> references = new List<List<int>>();
                List<List<int>> candidates = new List<List<int>>();
                int sumWords = 0;
                // assign rewards for each list of actions.
                for (int m = 0; m < Input.Count; m++)
                {
                    for (int t = Input[m].StatusPath.Count - 1; t >= 0; t--)
                    {
                        if (Input[m].StatusPath[t].CandidateProb != null)
                        {
                            Array.Clear(Input[m].StatusPath[t].CandidateProb.Deriv.Data.MemPtr, 0, Input[m].StatusPath[t].CandidateProb.Output.Data.EffectiveSize);
                        }
                        if (Input[m].StatusPath[t].Reward != null)
                        {
                            Input[m].StatusPath[t].Reward.Output.SyncToCPU();
                            Array.Clear(Input[m].StatusPath[t].Reward.Deriv.Data.MemPtr, 0, Input[m].StatusPath[t].Reward.Output.Data.EffectiveSize);
                        }
                    }
                    for (int i = 0; i < Input[m].Results.Count; i++)
                    {
                        int t = Input[m].Results[i].Item1;
                        int b = Input[m].Results[i].Item2;

                        int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);

                        references.Add(Input[m].Reference(origialB));
                        candidates.Add(Input[m].Candidate(b, t));

                        sumWords += candidates.Last().Count;
                    }
                }
                float sumWordsProb = 1.0f / sumWords;
                float sumMCTSProb = 1.0f / (Input[0].BatchSize * BuilderParameters.MCTS_NUM);
                List<float> true_v_list = PlainBLEUEvaluationSet.EvaluateBLEU(references, candidates, BuilderParameters.T_NGRAM_W, new HashSet<string>() { "1", "2" });

                List<float> baseLines = new List<float>();
                List<int> maxIdx = new List<int>();
                List<float> maxVs = new List<float>();
                float avgLiklihoodProb = 0;
                for (int b = 0; b < Input[0].BatchSize; b++)
                {
                    string hisWords = Input[0].StatusPath[0].StatusKey[b].Replace('-', ' ');

                    List<int> nextWords = new List<int>();
                    for (int g = 0; g < BuilderParameters.K_PRED; g++)
                    {
                        int nw = (int)Input[0].NextWords.MemPtr[b * BuilderParameters.K_PRED + g];
                        nextWords.Add(nw);
                        if (nw == 0) break;
                    }
                    string groundWords = string.Join(" ", nextWords);

                    writer.WriteLine(hisWords);
                    writer.WriteLine(groundWords);
                    float mf = 0;
                    float maxV = float.MinValue;
                    int maxI = -1;
                    for (int m = 0; m < Input.Count; m++)
                    {
                        for (int i = 0; i < Input[m].Results.Count; i++)
                        {
                            int t1 = Input[m].Results[i].Item1;
                            int b1 = Input[m].Results[i].Item2;

                            int origialB = Input[m].StatusPath[t1].GetOriginalStatsIndex(b1);
                            if (origialB == b)
                            {
                                float estimate_v = Input[m].StatusPath[t1].Reward.Output.Data.MemPtr[b1];
                                float score = Input[m].ResultScore[i];
                                writer.WriteLine(string.Join(" ", candidates[m * Input[0].BatchSize + i]) + "\t" + true_v_list[m * Input[0].BatchSize + i].ToString() + "\t" + Input[m].Results[i].Item3.ToString() + "\t" + estimate_v.ToString() + "\t" + score.ToString());
                                mf += true_v_list[m * Input[0].BatchSize + i];

                                if (true_v_list[m * Input[0].BatchSize + i] > maxV) { maxV = true_v_list[m * Input[0].BatchSize + i]; maxI = m; }

                                avgLiklihoodProb += Input[m].Results[i].Item3;
                                break;
                            }
                        }
                    }
                    baseLines.Add(mf / Input.Count);
                    maxIdx.Add(maxI);
                    maxVs.Add(maxV);
                    writer.WriteLine();
                    writer.Flush();
                }


                int v_idx = 0;
                float avgReward = 0;

                //SymbolicState predId = Input.StatusPath[t].NodeID[b];
                for (int m = 0; m < Input.Count; m++)
                {
                    for (int i = 0; i < Input[m].Results.Count; i++)
                    {
                        int t = Input[m].Results[i].Item1;
                        int b = Input[m].Results[i].Item2;

                        int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);

                        string reference = string.Join(" ", Input[m].Reference(origialB));
                        string candidate = string.Join(" ", Input[m].Candidate(b, t));

                        float estimate_v = Input[m].StatusPath[t].Reward.Output.Data.MemPtr[b];

                       

                        if (estimate_v > 1) estimate_v = 1;
                        if (estimate_v < 0) estimate_v = 0;
                        float true_v = true_v_list[v_idx]; 
                        v_idx += 1;

                        sampleNum += 1;
                        mseloss = mseloss + Math.Abs((true_v - estimate_v));
                        trueReward += true_v;
                        if (m == 0)
                        {
                            avgReward += true_v;
                        }

                        // MSE error for reward estimation.
                        Input[m].StatusPath[t].Reward.Deriv.Data.MemPtr[b] = sumMCTSProb * BuilderParameters.MSE_LAMBDA * (true_v - estimate_v)  ;

                        // Policy gradient for path finding.
                        StatusData st = Input[m].StatusPath[t];

                        float reward = 0;
                        if(BuilderParameters.REWARD_S == 0)
                        {
                            reward = true_v;
                        }
                        else if(BuilderParameters.REWARD_S == 1)
                        {
                            reward = true_v - baseLines[origialB];
                        }
                        else if(BuilderParameters.REWARD_S == 2)
                        {
                            reward = true_v - estimate_v;
                        }
                        else if(BuilderParameters.REWARD_S == 3)
                        {
                            if(m == maxIdx[i])
                            {
                                reward = 1;
                            }
                        }

                        if (st.CandidateProb != null)
                        {
                            st.CandidateProb.Deriv.Data.MemPtr[b * BuilderParameters.Vocab_Dim] = BuilderParameters.PolicySchedule(Epoch) * sumWordsProb * 1.0f / (st.CandidateProb.Output.Data.MemPtr[b * BuilderParameters.Vocab_Dim] + Util.LargeEpsilon)
                                * Math.Max(0, reward); // estimate_v));
                        }

                        for (int pt = t - 1; pt >= 0; pt--)
                        {
                            StatusData pst = Input[m].StatusPath[pt];
                            int pb = st.GetPreStatusIndex(b);
                            int sidx = st.NodeID[b];

                            pst.CandidateProb.Deriv.Data.MemPtr[pb * BuilderParameters.Vocab_Dim + sidx] = BuilderParameters.PolicySchedule(Epoch) * sumWordsProb * 1.0f / (pst.CandidateProb.Output.Data.MemPtr[pb * BuilderParameters.Vocab_Dim + sidx] + Util.LargeEpsilon)
                                * (float)Math.Max(0, reward);// estimate_v));

                            st = pst;
                            b = pb;
                        }
                    }

                    for (int t = Input[m].StatusPath.Count - 1; t >= 0; t--)
                    {
                        if (Input[m].StatusPath[t].CandidateProb != null)
                        {
                            Input[m].StatusPath[t].CandidateProb.Deriv.Data.SyncFromCPU();
                        }
                        if (Input[m].StatusPath[t].Reward != null)
                        {
                            Input[m].StatusPath[t].Reward.Deriv.Data.SyncFromCPU();
                        }
                    }
                    Input[m].Results.Clear();
                    Input[m].ResultScore.Clear();
                }

                ObjectiveDict["MSE-LOSS"] = mseloss / (sampleNum + float.Epsilon);
                ObjectiveDict["AVG-BLEU"] = trueReward / (sampleNum + float.Epsilon);

                ObjectiveDict["AVG-LIKLI"] = avgLiklihoodProb / (sampleNum + float.Epsilon);

                ObjectiveDict["AVG-REWARD"] = avgReward / (maxIdx.Count + float.Epsilon);
                ObjectiveDict["UP-REWARD"] = maxVs.Sum() / (maxIdx.Count + float.Epsilon);

                ObjectiveScore = trueReward / (sampleNum + float.Epsilon);
                Memory.Clear();
            }
        }

        class RewardFeedbackRunner : StructRunner
        {
            //Random random = new Random(908);
            new GraphQueryData Input { get; set; }
            EpisodicMemoryV2 Memory { get; set; }

            int Epoch = 0;
            public override void Init()
            {
                Epoch = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
            }

            public RewardFeedbackRunner(GraphQueryData input, EpisodicMemoryV2 memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                // calculate termination probability.
                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    if(Input.StatusPath[t].Reward != null) Input.StatusPath[t].Reward.Output.Data.SyncToCPU();
                }

                Input.ResultScore.Clear();

                List<float> true_v_list = new List<float>();
                List<List<int>> references = new List<List<int>>();
                List<List<int>> candidates = new List<List<int>>();
                for (int i = 0; i < Input.Results.Count; i++)
                {
                    int t = Input.Results[i].Item1;
                    int b = Input.Results[i].Item2;
                    float p = Input.Results[i].Item3;
                    int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);

                    //string reference = string.Join(" ", Input.Reference(origialB));
                    //string candidate = string.Join(" ", Input.Candidate(b, t));
                    //SymbolicState predId = Input.StatusPath[t].NodeID[b];

                
                    float estimate_v = Input.StatusPath[t].Reward.Output.Data.MemPtr[b];
                    if (estimate_v > 1) estimate_v = 1;
                    if (estimate_v < 0) estimate_v = 0;

                    references.Add(Input.Reference(origialB));
                    candidates.Add(Input.Candidate(b, t));
                }

                true_v_list = PlainBLEUEvaluationSet.EvaluateBLEU(references, candidates, BuilderParameters.T_NGRAM_W, new HashSet<string>() { "1", "2" });
                // NLTKBLEUEvaluationSet.EvaluateBLEU(references.ToArray(), candidates.ToArray() ).ToList();
                for (int i = 0; i < Input.Results.Count; i++)
                {
                    int t = Input.Results[i].Item1;
                    int b = Input.Results[i].Item2;
                    float p = Input.Results[i].Item3;
                    int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);

                    float estimate_v = Input.StatusPath[t].Reward.Output.Data.MemPtr[b];
                    if (estimate_v > 1) estimate_v = 1;
                    if (estimate_v < 0) estimate_v = 0;

                    float feedback_v = 0;

                    //if(float.IsNaN( true_v_list[i] ))
                    //{
                    //    Console.WriteLine("Bad haappends ");
                    //}

                    // use true feed back or pesdo reward.
                    if (SampleRandom.NextDouble() < BuilderParameters.Epsilon(Epoch) && Behavior.RunMode == DNNRunMode.Train) { feedback_v = true_v_list[i]; }
                    else { feedback_v = estimate_v; }

                    feedback_v = feedback_v * BuilderParameters.UPDATE_R_DISCOUNT;
                    float v = 1 * BuilderParameters.UPDATE_R_DISCOUNT;

                    float v1 = BuilderParameters.PROB_LAMBDA * p;

                    float s1 = (float)Math.Log(estimate_v + BuilderParameters.BASE_LAMBDA);
                    v1 = v1 + s1 * BuilderParameters.MSE_LAMBDA;

                    Input.ResultScore.Add(v1);

                    StatusData st = Input.StatusPath[t];
                    if (t != BuilderParameters.MAX_HOP) { Memory.Update(st.StatusKey[b], 0, feedback_v, v); }
                
                    for (int pt = t - 1; pt >= 0; pt--)
                    {
                        int pb = st.GetPreStatusIndex(b);
                        StatusData pst = Input.StatusPath[pt];

                        Memory.Update(pst.StatusKey[pb], st.NodeID[b], feedback_v, v);

                        st = pst;
                        b = pb;
                    }
                }
            }
        }


        public static ComputationGraph BuildComputationGraph(List<List<int>> seqData, int batchSize, int vocabSize,
                                                             LMLSTMModel Model, EpisodicMemoryV2 Memory,
                                                             RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            // sample data from seqData.
            DataSampleRunner dataRunner = new DataSampleRunner(seqData, batchSize, vocabSize, Behavior);
            cg.AddDataRunner(dataRunner);
            SeqSparseBatchData seq = dataRunner.Output;
            CudaPieceFloat next = dataRunner.NextWord;
            List<string> hisKey = dataRunner.HistoryWords;
            
            // Sequence Embedding. 
            SeqDenseRecursiveData seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(Model.EmbedIn, seq, false, Behavior) { IsUpdate = BuilderParameters.Vocab_Update });

            // state context.
            List<Tuple<HiddenBatchData, HiddenBatchData>> stateContext = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

            // calculate state context.
            for (int i = 0; i < Model.SeqEncoder.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(Model.SeqEncoder.LSTMCells[i], seqEmbed, Behavior);
                seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);

                HiddenBatchData seqS0 = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(encoderRunner.Output, true, 0, encoderRunner.Output.MapForward, Behavior));
                HiddenBatchData seqC0 = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(encoderRunner.C, true, 0, encoderRunner.C.MapForward, Behavior));

                stateContext.Add(new Tuple<HiddenBatchData, HiddenBatchData>(seqS0, seqC0));
            }

            MatrixData decodeMatrix = new MatrixData(Model.EmbedOut);
            MatrixData encodeMatrix = new MatrixData(Model.EmbedIn);
            
            List<GraphQueryData> Queries = new List<GraphQueryData>();
            for (int p = 0; p < BuilderParameters.MCTS_NUM; p++)
            {
                // obtains the initial states.
                GraphQueryData query = new GraphQueryData(next, dataRunner.SmpIdx, BuilderParameters.K_PRED, hisKey, stateContext, Behavior.Device);
                Queries.Add(query);

                for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
                {
                    // decode state to take action.
                    FullyConnectHiddenRunner<HiddenBatchData> decodeRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Decoder, query.StatusPath[i].StateEmbed.Last().Item1, Behavior);
                    cg.AddRunner(decodeRunner);

                    MatrixProductRunner runner = new MatrixProductRunner(new MatrixData(decodeRunner.Output), decodeMatrix, Behavior) { name = "Depth" + i.ToString() };
                    cg.AddRunner(runner);

                    MatrixSoftmaxRunner softmaxRunner = new MatrixSoftmaxRunner(runner.Output, Behavior);
                    cg.AddRunner(softmaxRunner);

                    // probability of next words.
                    query.StatusPath[i].CandidateScore = new HiddenBatchData(runner.Output);
                    query.StatusPath[i].CandidateProb = new HiddenBatchData(softmaxRunner.Output);

                    EnsembleMatrixRunner concateRunner = new EnsembleMatrixRunner(query.StatusPath[i].StateEmbed.Select(m => m.Item2).ToList(), Behavior);
                    cg.AddRunner(concateRunner);

                    //rewards estimation.
                    DNNRunner<HiddenBatchData> rewardRunner = new DNNRunner<HiddenBatchData>(Model.RwDnn, concateRunner.Output, Behavior);//  query.StatusPath[i].StateEmbed.Last().Item1, Behavior);
                    cg.AddRunner(rewardRunner);

                    // set rewards. 
                    query.StatusPath[i].Reward = rewardRunner.Output;

                    // take action.
                    MCTSActionSamplingRunner actRunner = new MCTSActionSamplingRunner(query.StatusPath[i], p, i == BuilderParameters.MAX_HOP - 1, Memory, Behavior);
                    cg.AddRunner(actRunner);

                    ConvertSparseMatrixRunner wRunner = new ConvertSparseMatrixRunner(actRunner.NodeID, batchSize * DataPanel.Max_Seq_Len, BuilderParameters.Vocab_Dim, Behavior);
                    cg.AddRunner(wRunner);
                    SparseMultiplicationRunner wemdRunner = new SparseMultiplicationRunner(wRunner.Output, encodeMatrix, Behavior);
                    cg.AddRunner(wemdRunner);

                    HiddenBatchData xInput = new HiddenBatchData(wemdRunner.Output);

                    List<Tuple<HiddenBatchData, HiddenBatchData>> newStateContext = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

                    for (int l = 0; l < Model.SeqEncoder.LSTMCells.Count; l++)
                    {
                        MatrixExpansionRunner sExpRunner = new MatrixExpansionRunner(query.StatusPath[i].StateEmbed[l].Item1, actRunner.Match, 1, Behavior);
                        cg.AddRunner(sExpRunner);
                        MatrixExpansionRunner cExpRunner = new MatrixExpansionRunner(query.StatusPath[i].StateEmbed[l].Item2, actRunner.Match, 1, Behavior);
                        cg.AddRunner(cExpRunner);

                        Tuple<HiddenBatchData, HiddenBatchData> state = new Tuple<HiddenBatchData, HiddenBatchData>(sExpRunner.Output, cExpRunner.Output);

                        LSTMStateRunner stateRunner = new LSTMStateRunner(Model.SeqEncoder.LSTMCells[l], state.Item1, state.Item2, xInput, Behavior);
                        cg.AddRunner(stateRunner);

                        newStateContext.Add(new Tuple<HiddenBatchData, HiddenBatchData>(stateRunner.O, stateRunner.C));

                        xInput = stateRunner.O;
                    }

                    query.AddNewState(actRunner.NodeID, actRunner.StatusKey, newStateContext, actRunner.NodeProb, actRunner.PreStatusIdx, Behavior.Device);
                }
                // add feedback using bleu score.
                RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(query, Memory, Behavior);
                cg.AddRunner(rewardFeedRunner);
            }

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    RewardRunner rewardRunner = new RewardRunner(Queries, Memory, Behavior);
                    cg.AddObjective(rewardRunner);
                    break;
                //case DNNRunMode.Predict:
                //    cg.AddRunner(new EmbedFullySoftmaxRunner(Model.EmbedOut, decodeRunner.Output, next, 1, Behavior, false, false)); //
                //    break;
            }
            cg.SetDelegateModel(Model);
            return cg;
        }


        public static ComputationGraph BuildBeamSearchComputationGraph(List<List<int>> seqData, int batchSize, int vocabSize,
                                                             LMLSTMModel Model, EpisodicMemoryV2 Memory,
                                                             RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            // sample data from seqData.
            DataSampleRunner dataRunner = new DataSampleRunner(seqData, batchSize, vocabSize, Behavior);
            cg.AddDataRunner(dataRunner);
            SeqSparseBatchData seq = dataRunner.Output;
            CudaPieceFloat next = dataRunner.NextWord;
            List<string> hisKey = dataRunner.HistoryWords;

            // Sequence Embedding. 
            SeqDenseRecursiveData seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(Model.EmbedIn, seq, false, Behavior) { IsUpdate = BuilderParameters.Vocab_Update });

            // state context.
            List<Tuple<HiddenBatchData, HiddenBatchData>> stateContext = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

            // calculate state context.
            for (int i = 0; i < Model.SeqEncoder.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(Model.SeqEncoder.LSTMCells[i], seqEmbed, Behavior);
                seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);

                HiddenBatchData seqS0 = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(encoderRunner.Output, true, 0, encoderRunner.Output.MapForward, Behavior));
                HiddenBatchData seqC0 = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(encoderRunner.C, true, 0, encoderRunner.C.MapForward, Behavior));

                stateContext.Add(new Tuple<HiddenBatchData, HiddenBatchData>(seqS0, seqC0));
            }

            MatrixData decodeMatrix = new MatrixData(Model.EmbedOut);
            MatrixData encodeMatrix = new MatrixData(Model.EmbedIn);

            List<GraphQueryData> Queries = new List<GraphQueryData>();
            
                // obtains the initial states.
                GraphQueryData query = new GraphQueryData(next, dataRunner.SmpIdx, BuilderParameters.K_PRED, hisKey, stateContext, Behavior.Device);
                Queries.Add(query);

            for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
            {
                // decode state to take action.
                FullyConnectHiddenRunner<HiddenBatchData> decodeRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Decoder, query.StatusPath[i].StateEmbed.Last().Item1, Behavior);
                cg.AddRunner(decodeRunner);

                MatrixProductRunner runner = new MatrixProductRunner(new MatrixData(decodeRunner.Output), decodeMatrix, Behavior) { name = "Depth" + i.ToString() };
                cg.AddRunner(runner);

                MatrixSoftmaxRunner softmaxRunner = new MatrixSoftmaxRunner(runner.Output, Behavior);
                cg.AddRunner(softmaxRunner);

                // probability of next words.
                query.StatusPath[i].CandidateScore = new HiddenBatchData(runner.Output);
                query.StatusPath[i].CandidateProb = new HiddenBatchData(softmaxRunner.Output);

                EnsembleMatrixRunner concateRunner = new EnsembleMatrixRunner(query.StatusPath[i].StateEmbed.Select(m => m.Item2).ToList(), Behavior);
                cg.AddRunner(concateRunner);

                //rewards estimation.
                DNNRunner<HiddenBatchData> rewardRunner = new DNNRunner<HiddenBatchData>(Model.RwDnn, concateRunner.Output, Behavior); // query.StatusPath[i].StateEmbed.Last().Item1, Behavior);
                cg.AddRunner(rewardRunner);

                // set rewards. 
                query.StatusPath[i].Reward = rewardRunner.Output;

                // take action.
                //MCTSActionSamplingRunner actRunner = new MCTSActionSamplingRunner(query.StatusPath[i], p, i == BuilderParameters.MAX_HOP - 1, Memory, Behavior);
                BeamSearchRunner actRunner = new BeamSearchRunner(query.StatusPath[i], BuilderParameters.BeamSearchCandidate, i == BuilderParameters.MAX_HOP - 1, Behavior);
                cg.AddRunner(actRunner);

                ConvertSparseMatrixRunner wRunner = new ConvertSparseMatrixRunner(actRunner.NodeID, batchSize * DataPanel.Max_Seq_Len, BuilderParameters.Vocab_Dim, Behavior);
                cg.AddRunner(wRunner);
                SparseMultiplicationRunner wemdRunner = new SparseMultiplicationRunner(wRunner.Output, encodeMatrix, Behavior);
                cg.AddRunner(wemdRunner);

                HiddenBatchData xInput = new HiddenBatchData(wemdRunner.Output);

                List<Tuple<HiddenBatchData, HiddenBatchData>> newStateContext = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

                for (int l = 0; l < Model.SeqEncoder.LSTMCells.Count; l++)
                {
                    MatrixExpansionRunner sExpRunner = new MatrixExpansionRunner(query.StatusPath[i].StateEmbed[l].Item1, actRunner.Match, 1, Behavior);
                    cg.AddRunner(sExpRunner);
                    MatrixExpansionRunner cExpRunner = new MatrixExpansionRunner(query.StatusPath[i].StateEmbed[l].Item2, actRunner.Match, 1, Behavior);
                    cg.AddRunner(cExpRunner);

                    Tuple<HiddenBatchData, HiddenBatchData> state = new Tuple<HiddenBatchData, HiddenBatchData>(sExpRunner.Output, cExpRunner.Output);

                    LSTMStateRunner stateRunner = new LSTMStateRunner(Model.SeqEncoder.LSTMCells[l], state.Item1, state.Item2, xInput, Behavior);
                    cg.AddRunner(stateRunner);

                    newStateContext.Add(new Tuple<HiddenBatchData, HiddenBatchData>(stateRunner.O, stateRunner.C));

                    xInput = stateRunner.O;
                }

                query.AddNewState(actRunner.NodeID, actRunner.StatusKey, newStateContext, actRunner.NodeProb, actRunner.PreStatusIdx, Behavior.Device);
            }

            cg.AddRunner(new DumpResultRunner(query, BuilderParameters.ScoreOutputPath, Behavior));

            cg.SetDelegateModel(Model);
            return cg;
        }

        public static ComputationGraph BuildMCTSComputationGraph(List<List<int>> seqData, int batchSize, int vocabSize,
                                                         LMLSTMModel Model, EpisodicMemoryV2 Memory,
                                                         RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            // sample data from seqData.
            DataSampleRunner dataRunner = new DataSampleRunner(seqData, batchSize, vocabSize, Behavior);
            cg.AddDataRunner(dataRunner);
            SeqSparseBatchData seq = dataRunner.Output;
            CudaPieceFloat next = dataRunner.NextWord;
            List<string> hisKey = dataRunner.HistoryWords;

            // Sequence Embedding. 
            SeqDenseRecursiveData seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(Model.EmbedIn, seq, false, Behavior) { IsUpdate = BuilderParameters.Vocab_Update });

            // state context.
            List<Tuple<HiddenBatchData, HiddenBatchData>> stateContext = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

            // calculate state context.
            for (int i = 0; i < Model.SeqEncoder.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(Model.SeqEncoder.LSTMCells[i], seqEmbed, Behavior);
                seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);

                HiddenBatchData seqS0 = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(encoderRunner.Output, true, 0, encoderRunner.Output.MapForward, Behavior));
                HiddenBatchData seqC0 = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(encoderRunner.C, true, 0, encoderRunner.C.MapForward, Behavior));

                stateContext.Add(new Tuple<HiddenBatchData, HiddenBatchData>(seqS0, seqC0));
            }

            MatrixData decodeMatrix = new MatrixData(Model.EmbedOut);
            MatrixData encodeMatrix = new MatrixData(Model.EmbedIn);

            List<GraphQueryData> Queries = new List<GraphQueryData>();
            for (int p = 0; p < BuilderParameters.MCTS_NUM; p++)
            {
                // obtains the initial states.
                GraphQueryData query = new GraphQueryData(next, dataRunner.SmpIdx, BuilderParameters.K_PRED, hisKey, stateContext, Behavior.Device);
                Queries.Add(query);

                for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
                {
                    // decode state to take action.
                    FullyConnectHiddenRunner<HiddenBatchData> decodeRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Decoder, query.StatusPath[i].StateEmbed.Last().Item1, Behavior);
                    cg.AddRunner(decodeRunner);

                    MatrixProductRunner runner = new MatrixProductRunner(new MatrixData(decodeRunner.Output), decodeMatrix, Behavior) { name = "Depth" + i.ToString() };
                    cg.AddRunner(runner);

                    MatrixSoftmaxRunner softmaxRunner = new MatrixSoftmaxRunner(runner.Output, Behavior);
                    cg.AddRunner(softmaxRunner);

                    // probability of next words.
                    query.StatusPath[i].CandidateScore = new HiddenBatchData(runner.Output);
                    query.StatusPath[i].CandidateProb = new HiddenBatchData(softmaxRunner.Output);

                    EnsembleMatrixRunner concateRunner = new EnsembleMatrixRunner(query.StatusPath[i].StateEmbed.Select(m => m.Item1).ToList(), Behavior);
                    cg.AddRunner(concateRunner);

                    //rewards estimation.
                    DNNRunner<HiddenBatchData> rewardRunner = new DNNRunner<HiddenBatchData>(Model.RwDnn, concateRunner.Output, Behavior);//  query.StatusPath[i].StateEmbed.Last().Item1, Behavior);
                    cg.AddRunner(rewardRunner);

                    // set rewards. 
                    query.StatusPath[i].Reward = rewardRunner.Output;

                    // take action.
                    MCTSActionSamplingRunner actRunner = new MCTSActionSamplingRunner(query.StatusPath[i], p, i == BuilderParameters.MAX_HOP - 1, Memory, Behavior);
                    cg.AddRunner(actRunner);

                    ConvertSparseMatrixRunner wRunner = new ConvertSparseMatrixRunner(actRunner.NodeID, batchSize * DataPanel.Max_Seq_Len, BuilderParameters.Vocab_Dim, Behavior);
                    cg.AddRunner(wRunner);
                    SparseMultiplicationRunner wemdRunner = new SparseMultiplicationRunner(wRunner.Output, encodeMatrix, Behavior);
                    cg.AddRunner(wemdRunner);

                    HiddenBatchData xInput = new HiddenBatchData(wemdRunner.Output);

                    List<Tuple<HiddenBatchData, HiddenBatchData>> newStateContext = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

                    for (int l = 0; l < Model.SeqEncoder.LSTMCells.Count; l++)
                    {
                        MatrixExpansionRunner sExpRunner = new MatrixExpansionRunner(query.StatusPath[i].StateEmbed[l].Item1, actRunner.Match, 1, Behavior);
                        cg.AddRunner(sExpRunner);
                        MatrixExpansionRunner cExpRunner = new MatrixExpansionRunner(query.StatusPath[i].StateEmbed[l].Item2, actRunner.Match, 1, Behavior);
                        cg.AddRunner(cExpRunner);

                        Tuple<HiddenBatchData, HiddenBatchData> state = new Tuple<HiddenBatchData, HiddenBatchData>(sExpRunner.Output, cExpRunner.Output);

                        LSTMStateRunner stateRunner = new LSTMStateRunner(Model.SeqEncoder.LSTMCells[l], state.Item1, state.Item2, xInput, Behavior);
                        cg.AddRunner(stateRunner);

                        newStateContext.Add(new Tuple<HiddenBatchData, HiddenBatchData>(stateRunner.O, stateRunner.C));

                        xInput = stateRunner.O;
                    }

                    query.AddNewState(actRunner.NodeID, actRunner.StatusKey, newStateContext, actRunner.NodeProb, actRunner.PreStatusIdx, Behavior.Device);
                }
                // add feedback using bleu score.
                RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(query, Memory, Behavior);
                cg.AddRunner(rewardFeedRunner);
            }

            cg.AddRunner(new EnsembleDumpResultRunner(Queries, BuilderParameters.ScoreOutputPath, Behavior));

            cg.SetDelegateModel(Model);
            return cg;

            //switch (Behavior.RunMode)
            //{
            //    case DNNRunMode.Train:
            //        RewardRunner rewardRunner = new RewardRunner(Queries, Memory, Behavior);
            //        cg.AddObjective(rewardRunner);
            //        break;
            //        //case DNNRunMode.Predict:
            //        //    cg.AddRunner(new EmbedFullySoftmaxRunner(Model.EmbedOut, decodeRunner.Output, next, 1, Behavior, false, false)); //
            //        //    break;
            //}
            //cg.SetDelegateModel(Model);
            //return cg;
        }

        class BeamSearchRunner : StructRunner
        {
            int BeamSize = 1;
            new StatusData Input { get; set; }

            public List<int> NodeID = null;
            public List<float> NodeProb = null;
            public BiMatchBatchData Match = null;
            public List<int> PreStatusIdx = null;

            CudaPieceFloat tmpWordIndex { get; set; }
            CudaPieceFloat tmpWordProb { get; set; }

            public List<string> StatusKey = null;

            bool IsLastStep { get; set; }
            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchRunner(StatusData data, int beamSize, bool isLastStep, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                BeamSize = beamSize;
                Input = data;
                IsLastStep = isLastStep;

                tmpWordIndex = new CudaPieceFloat(data.MaxBatchSize * beamSize, Behavior.Device);
                tmpWordProb = new CudaPieceFloat(data.MaxBatchSize * beamSize, Behavior.Device);

                NodeID = new List<int>();
                NodeProb = new List<float>();
                PreStatusIdx = new List<int>();
                StatusKey = new List<string>();

                Match = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);
            }

            public override void Forward()
            {
                NodeID.Clear();
                NodeProb.Clear();
                PreStatusIdx.Clear();
                StatusKey.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                if (IsLastStep)
                {
                    Input.CandidateProb.Output.Data.SyncToCPU();
                    for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                    {
                        List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);
                        foreach (int b in idxs)
                        {
                            float pP = Input.GetLogProb(b);
                            float wp = (float)Math.Log(Input.CandidateProb.Output.Data.MemPtr[b * Input.CandidateProb.Dim]);
                            Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, b, wp + pP));
                        }
                    }
                }
                else
                {
                    Cudalib.KLargestValueBatch(Input.CandidateProb.Output.Data.CudaPtr, Input.BatchSize, Input.CandidateProb.Dim, BeamSize, 1, tmpWordProb.CudaPtr, tmpWordIndex.CudaPtr);

                    tmpWordProb.SyncToCPU();
                    tmpWordIndex.SyncToCPU();
                    // for each beam.
                    for (int i = 0; i < Input.GraphQuery.BatchSize; i++) //.MatchCandidateProb.Segment; i++)
                    {
                        List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);

                        MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

                        foreach (int b in idxs)
                        {
                            //int e = Input.CandidateProb.MemPtr[b];
                            //int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                            float pP = Input.GetLogProb(b);
                            for (int t = 0; t < BeamSize; t++)
                            {
                                int widx = (int)tmpWordIndex.MemPtr[b * BeamSize + t];
                                float wp = (float)Math.Log(tmpWordProb.MemPtr[b * BeamSize + t]);

                                topKheap.push_pair(new Tuple<int, int>(b, widx), wp + pP);
                            }
                        }

                        while (!topKheap.IsEmpty)
                        {
                            KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();

                            int widx = p.Key.Item2;
                            if (widx == 0)
                            {
                                // put in results list.
                                Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, p.Key.Item1, p.Value));
                            }
                            else
                            {
                                string key = Input.StatusKey[p.Key.Item1];

                                match.Add(new Tuple<int, int, float>(p.Key.Item1, NodeID.Count, p.Value));

                                StatusKey.Add(key + '-' + widx.ToString());
                                NodeID.Add(widx);
                                NodeProb.Add(p.Value);
                                PreStatusIdx.Add(p.Key.Item1);
                            }
                        }
                    }
                }
                Match.SetMatch(match);
            }
        }

        public class EpisodicMemoryV2
        {
            Dictionary<string, Tuple<float[], float[]>> Mem = new Dictionary<string, Tuple<float[], float[]>>();
            int VocabDim { get; set; }
            public EpisodicMemoryV2(int vocabDim)
            {
                VocabDim = vocabDim;
            }
            public void Clear()
            {
                Mem.Clear();
            }
            /// <summary>
            /// memory index.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="actionCount"></param>
            /// <param name="topK"></param>
            /// <returns></returns>
            public Tuple<float[], float[]> Search(string key)
            {
                if (Mem.ContainsKey(key))
                {
                    return Mem[key];
                }
                else
                {
                    return new Tuple<float[], float[]>(new float[VocabDim], new float[VocabDim]);
                }
            }

            public void Update(string key, int act, float reward, float v = 1.0f)
            {
                float new_u = 0;
                float new_c = 0;

                // update memory.
                if (Mem.ContainsKey(key))
                {
                    float u = Mem[key].Item1[act];
                    float c = Mem[key].Item2[act];
                    new_u = c / (c + v) * u + v / (c + v) * reward;
                    new_c = c + v;
                }
                else
                {
                    new_u = reward;
                    new_c = v;
                    Mem[key] = new Tuple<float[], float[]>(new float[VocabDim], new float[VocabDim]);
                }
                Mem[key].Item1[act] = new_u;
                Mem[key].Item2[act] = new_c;
            }
        }

        public class BanditAlg
        {
            /// <summary>
            /// Exp 0.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UniformRandomStrategy0(int dim, Random random)
            {
                return random.Next(dim);
            }

            /// <summary>
            /// Exp 1.
            /// </summary>
            /// <param name="actdim"></param>
            /// <param name="rand_term"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int CascadeRandomStrategy1(int actdim, float rand_term, Random random)
            {
                if (random.NextDouble() > rand_term)
                {
                    return actdim;
                }
                return random.Next(actdim);
            }

            /// <summary>
            /// Exp 2.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int ThompasSampling(float[] prior, Random random)
            {
                int idx = Util.Sample(prior.ToArray(), random);
                return idx;
            }

            public static int UCBBandit(List<Tuple<float, float>> arms, float c, Random random)
            {
                float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);
                List<float> v = new List<float>();
                foreach (Tuple<float, float> arm in arms)
                {
                    v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
                }
                int idx = Util.MaximumValue(v.ToArray());
                return idx;
            }

            /// <summary>
            /// Exp 3. Standard UCB Bandit.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UCB1Bandit(Tuple<float[], float[]> arms, int dim, float c, Random random)
            {
                if (arms == null)
                {
                    return UniformRandomStrategy0(dim, random);
                }
                float log_total = (float)Math.Log(arms.Item2.Take(dim).Sum() + 1.0f);
                List<float> v = new List<float>();
                for (int i = 0; i < dim; i++)
                {
                    v.Add(arms.Item1[i] / (arms.Item2[i] + 0.1f) + c * (float)Math.Sqrt(log_total / (arms.Item2[i] + 0.1f)) + (float)random.NextDouble() * 0.0001f);
                }
                int idx = Util.MaximumValue(v.ToArray());

                return idx;
            }

            public static int PUCB(Tuple<float[], float[]> arms, float[] prior, int dim, float mb, float c, float m, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    throw new NotImplementedException("PUCB arms is null");
                }
                else
                {
                    float sum = prior.Sum() + mb * dim;

                    float t = (float)arms.Item2.Sum(); 

                    for (int i = 0; i < dim; i++)
                    {
                        float si = arms.Item1[i];
                        float xi = (arms.Item2[i] == 0 ? 1 : arms.Item1[i] * 1.0f / (arms.Item2[i]));
                        float ci = si == 0 ? 0 : c * (float)Math.Sqrt(Math.Log(t) / si);
                        float mi = (t == 0 ? 1 : (float)Math.Sqrt(Math.Log(t) / t)) * m * sum / (prior[i] + mb);
                        float s = xi + ci - mi;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            /// <summary>
            /// Exp 4. PUCT.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int AlphaGoZeroBandit(Tuple<float[], float[]> arms, float[] prior, int dim, float c, float d, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;
                {
                    float total = (float)arms.Item2.Take(dim).Sum();  
                    for (int i = 0; i < dim; i++)
                    {
                        if (double.IsNaN(prior[i]) || double.IsNaN(prior[i]))
                        {
                            throw new Exception("Wrong on prior probability!");
                        }

                        float s = c * (float)Math.Pow(prior[i], d) * (float)Math.Sqrt(Math.Log(total + 1.1f) / (arms.Item2[i] + 0.1f)) +
                                                       (arms.Item2[i] == 0 ? 0 : arms.Item1[i] * 1.0f / (arms.Item2[i])) + (float)random.NextDouble() * 0.0001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            public static int AlphaGoZeroBanditV2(Tuple<float[], float[]> arms, float[] prior, int dim, float c, float d, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                {
                    float total = (float)arms.Item2.Take(dim).Sum() + 1;

                    for (int i = 0; i < dim; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], d) * (float)Math.Sqrt(total) / (arms.Item2[i] + 1.0f) +
                                                       (arms.Item2[i] == 0 ? 0 : arms.Item1[i] * 1.0f / (arms.Item2[i])) + (float)random.NextDouble() * 0.0001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBanditV3(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float total = (float)arms.Select(i => i.Item2).Sum();

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * prior[i] * (float)Math.Sqrt(total) / (arms[i].Item2 + 1.0f) +
                                                       (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            /// <summary>
            /// mb = 0.00001;
            /// c = exploration.
            /// m = 
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="mb"></param>
            /// <param name="c"></param>
            /// <param name="m"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int PUCB2(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum());
                    List<float> v = new List<float>();
                    foreach (Tuple<float, float> arm in arms)
                    {
                        v.Add(arm.Item1 / (arm.Item2 + 1.0f) + c * (float)Math.Sqrt(log_total / (arm.Item2)) + (float)random.NextDouble() * 0.0001f);
                    }
                    int idx = Util.MaximumValue(v.ToArray());
                }
                return maxI;
            }


        }

        public static Random SampleRandom = new Random(13);

        class MCTSActionSamplingRunner : StructRunner
        {
            new StatusData Input { get; set; }

            public List<int> NodeID = null;
            public List<float> NodeProb = null;
            public BiMatchBatchData Match = null;
            public List<int> PreStatusIdx = null;
            public List<string> StatusKey = null;

            CudaPieceFloat tmpWordIndex { get; set; }
            CudaPieceFloat tmpWordProb { get; set; }

            bool IsLastStep { get; set; }

            EpisodicMemoryV2 Memory { get; set; }

            public int MCTSIdx { get; set; }

            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSActionSamplingRunner(StatusData input, int mctsIdx, bool isLastStep, EpisodicMemoryV2 memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Memory = memory;
                
                Input = input;
                MCTSIdx = mctsIdx;
                IsLastStep = isLastStep;

                NodeID = new List<int>();
                NodeProb = new List<float>();
                Match = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * BuilderParameters.Vocab_Dim,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
                PreStatusIdx = new List<int>();
                StatusKey = new List<string>();

                //tmpWordIndex = new CudaPieceFloat(input.MaxBatchSize * beamSize, Behavior.Device);
                //tmpWordProb = new CudaPieceFloat(input.MaxBatchSize * beamSize, Behavior.Device);
            }

            public override void Forward()
            {
                NodeID.Clear();
                NodeProb.Clear();
                PreStatusIdx.Clear();
                StatusKey.Clear();
                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                Input.CandidateProb.Output.Data.SyncToCPU();
                if (IsLastStep)
                {
                    for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                    {
                        List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);
                        foreach (int b in idxs)
                        {
                            float pP = Input.GetLogProb(b);
                            float wp = (float)Math.Log(Input.CandidateProb.Output.Data.MemPtr[b * Input.CandidateProb.Dim]);
                            Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, b, wp + pP));
                        }
                    }
                }
                else
                {
                    int currentStep = Input.Step;
                    for (int i = 0; i < Input.CandidateProb.BatchSize; i++)
                    {
                        Tuple<float[], float[]> arms = Memory.Search(Input.StatusKey[i]);

                        float[] prior_r = new float[Input.CandidateProb.Dim];
                        Array.Copy(Input.CandidateProb.Output.Data.MemPtr, i * Input.CandidateProb.Dim, prior_r, 0, Input.CandidateProb.Dim);

                        int originalB = Input.GetOriginalStatsIndex(i);
                        List<int> refers = Input.GraphQuery.Reference(originalB);

                        int idx = 0;
                        int strategy = BuilderParameters.EStrategy(MCTSIdx);
                        //Console.WriteLine("MCTS Index {0}, strategy {1}", MCTSIdx, strategy);
                        switch (strategy)
                        {
                            //unified sample next words.
                            case 0: idx = BanditAlg.UniformRandomStrategy0(prior_r.Length, SampleRandom); break;
                            // copy path.
                            case 1: idx = refers[Input.Step]; break;
                            // greedy decoding.
                            case 2: idx = Util.MaximumValue(prior_r); break;
                            // thompas sample.
                            case 3: idx = BanditAlg.ThompasSampling(prior_r, SampleRandom); break;
                            // ucb sample.
                            case 4: idx = BanditAlg.UCB1Bandit(arms, prior_r.Length, BuilderParameters.UCB1_C, SampleRandom); break;
                            // version 1 puct
                            case 5: idx = BanditAlg.AlphaGoZeroBandit(arms, prior_r, prior_r.Length, BuilderParameters.PUCT_C, BuilderParameters.PUCT_D, SampleRandom); break;
                            // version 2 puct
                            case 6: idx = BanditAlg.AlphaGoZeroBanditV2(arms, prior_r, prior_r.Length, BuilderParameters.PUCT_C, BuilderParameters.PUCT_D, SampleRandom); break;
                            // standard version puct.
                            case 7: idx = BanditAlg.PUCB(arms, prior_r, prior_r.Length, BuilderParameters.PUCB_B, BuilderParameters.PUCB_C, BuilderParameters.PUCB_M, SampleRandom); break;
                        }

                        //int idx = BanditAlg.ThompasSampling(m, prior_r.ToList(), 2, random);

                        float pP = Input.GetLogProb(i);
                        string key = Input.StatusKey[i];
                        // sample to end of the sequence.
                        if (idx == 0)
                        {
                            Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, i, pP + (float)Math.Log(Util.SmallEpsilon + prior_r[idx])));
                        }
                        else
                        {
                            match.Add(new Tuple<int, int, float>(i, NodeID.Count, 1));

                            PreStatusIdx.Add(i);
                            NodeID.Add(idx);
                            NodeProb.Add(pP + (float)Math.Log(prior_r[idx]));
                            StatusKey.Add(key + '-' + idx.ToString());
                        }
                    }
                }
                Match.SetMatch(match);
            }
        }

        public class LMLSTMModel : CompositeNNStructure
        {
            public EmbedStructure EmbedIn { get; set; } // = new EmbedStructure(BuilderParameters.Vocab_Dim, BuilderParameters.Encode_Embed, device);
            public EmbedStructure EmbedOut { get; set; }
            public LSTMStructure SeqEncoder { get; set; } // = new LSTMStructure(BuilderParameters.Encode_Embed, BuilderParameters.LAYER_DIM, device);
            public LayerStructure Decoder { get; set; }

            public DNNStructure RwDnn { get; set; }

            public LMLSTMModel(int vocab, int enDim, int[] lstmDims, DeviceType device)
            {
                EmbedIn = AddLayer(new EmbedStructure(vocab, enDim, device));
                SeqEncoder = AddLayer(new LSTMStructure(enDim, lstmDims, device));
                Decoder = AddLayer(new LayerStructure(lstmDims.Last(), enDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true));
                RwDnn = AddLayer(new DNNStructure(lstmDims.Sum(), BuilderParameters.RwNet, BuilderParameters.RwAf, 
                    BuilderParameters.RwNet.Select(i => true).ToArray(), device));

                if(BuilderParameters.Is_Share_Emd)
                {
                    EmbedOut = EmbedIn;
                }
                else
                {
                    EmbedOut = AddLayer(new EmbedStructure(vocab, enDim, device));
                }

                if(!BuilderParameters.Vocab_Emd.Equals(string.Empty))
                {
                    int wordDim = enDim;

                    Logger.WriteLog("Loading Glove Word Embedding ...");
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab_Emd, Encoding.UTF8))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string[] items = mreader.ReadLine().Split('\t');
                            int wordIdx = int.Parse(items[0]);
                            string[] vecStr = items[1].Split(' ');
                            float[] vec = vecStr.Select(i => float.Parse(i)).ToArray();

                            if (vec.Length != wordDim) { throw new Exception(string.Format("word {0} embedding {1} does not match with the dim {2}", wordIdx, vec.Length, wordDim)); }
                            Array.Copy(vec, 0, EmbedIn.Embedding.MemPtr, wordIdx * wordDim, wordDim);
                            if(!BuilderParameters.Is_Share_Emd)
                            {
                                Array.Copy(vec, 0, EmbedOut.Embedding.MemPtr, wordIdx * wordDim, wordDim);
                            }
                        }
                    }
                    EmbedIn.Embedding.SyncFromCPU();
                    EmbedOut.Embedding.SyncFromCPU();

                    Logger.WriteLog("Init Glove Word Embedding Done.");
                }
            }

            public LMLSTMModel(BinaryReader reader, DeviceType device, bool isLMInit)
            {
                if (isLMInit)
                {
                    Logger.WriteLog("Load lm model");
                    int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
                    EmbedIn = (EmbedStructure)DeserializeNextModel(reader, device);
                    SeqEncoder = (LSTMStructure)DeserializeNextModel(reader, device);
                    Decoder = (LayerStructure)DeserializeNextModel(reader, device);

                    RwDnn = AddLayer(new DNNStructure(BuilderParameters.LAYER_DIM.Sum(), BuilderParameters.RwNet, BuilderParameters.RwAf,
                    BuilderParameters.RwNet.Select(i => true).ToArray(), device));

                    if (modelNum > 3)
                    {
                        EmbedOut = (EmbedStructure)DeserializeNextModel(reader, device);
                    }
                    else
                    {
                        EmbedOut = EmbedIn;
                    }

                }
                else
                {
                    Logger.WriteLog("Load RW model");
                    int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
                    EmbedIn = (EmbedStructure)DeserializeNextModel(reader, device);
                    SeqEncoder = (LSTMStructure)DeserializeNextModel(reader, device);
                    Decoder = (LayerStructure)DeserializeNextModel(reader, device);
                    RwDnn = (DNNStructure)DeserializeNextModel(reader, device);
                    if (modelNum > 4)
                    {
                        EmbedOut = (EmbedStructure)DeserializeNextModel(reader, device);
                    }
                    else
                    {
                        EmbedOut = EmbedIn;
                    }
                }
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                if (!BuilderParameters.Vocab_Update)
                {
                    EmbedIn.EmbeddingOptimizer = new SGDOptimizer(EmbedIn.Embedding, 0, behavior);
                    if(!BuilderParameters.Is_Share_Emd)
                    {
                        EmbedOut.EmbeddingOptimizer = new SGDOptimizer(EmbedOut.Embedding, 0, behavior);
                    }
                }
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public class DumpResultRunner : ObjectiveRunner
        {
            GraphQueryData Data { get; set; }
            StreamWriter Writer { get; set; }
            string Path { get; set; }
            float Score1 { get; set; }
            float Score2 { get; set; }
            int SmpNum { get; set; }
            public DumpResultRunner(GraphQueryData data, string path, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                Path = path;
            }

            public override void Init()
            {
                Writer = new StreamWriter(Path);
                SmpNum = 0;
                Score1 = 0;
                Score2 = 0;
            }

            public override void Complete()
            {
                Logger.WriteLog("Avg BLEU Score {0}, {1}", Score1 * 1.0f / SmpNum, Score2 * 1.0f / SmpNum);
                Writer.Close();
            }

            public override void Forward()
            {
                Dictionary<int, Tuple<string, float>> Results = new Dictionary<int, Tuple<string, float>>();

                List<int>[] refers = new List<int>[Data.BatchSize];
                List<int>[] cands = new List<int>[Data.BatchSize];

                for (int i = 0; i < Data.Results.Count; i++)
                {
                    int t = Data.Results[i].Item1;
                    int b = Data.Results[i].Item2;
                    float p = Data.Results[i].Item3;

                    int origialB = Data.StatusPath[t].GetOriginalStatsIndex(b);
                    List<int> r = Data.StatusPath[t].GetHistoryNodeIdxs(b);

                    if (!Results.ContainsKey(origialB))
                    {
                        Results.Add(origialB, new Tuple<string, float>(string.Join(" ", r), p));
                        cands[origialB] = Data.Candidate(b, t);
                    }
                    else if (p > Results[origialB].Item2)
                    {
                        Results[origialB] = new Tuple<string, float>(string.Join(" ", r), p);
                        cands[origialB] = Data.Candidate(b, t);
                    }

                    refers[origialB] = Data.Reference(origialB);
                }

                List<float> bleus1 = PlainBLEUEvaluationSet.EvaluateBLEU(refers.ToList(), cands.ToList(), BuilderParameters.NGRAM_W, new HashSet<string>() { "1", "2" });
                List<float> bleus2 = PlainBLEUEvaluationSet.EvaluateBLEU(refers.ToList(), cands.ToList(), BuilderParameters.NGRAM_W, null);

                Score1 += bleus1.Sum();
                Score2 += bleus2.Sum();
                SmpNum += refers.Length;
                for (int i = 0; i < Data.BatchSize; i++)
                {
                    Writer.WriteLine(Results[i].Item1 + "\t" + Results[i].Item2.ToString() + "\t" + bleus1[i].ToString() + "\t" + Data.SmpIdx[i].ToString() +"\t"+ bleus2[i].ToString());
                    //Writer.WriteLine(Results[i].Item1 + "\t" + Results[i].Item2.ToString());
                }

                Data.Results.Clear();
                Data.ResultScore.Clear();
            }
        }

        public class EnsembleDumpResultRunner : ObjectiveRunner
        {
            List<GraphQueryData> Data { get; set; }
            StreamWriter Writer { get; set; }
            StreamWriter tmpWriter { get; set; }
            string Path { get; set; }
            float Score1 { get; set; }
            float Score2 { get; set; }
            int SmpNum { get; set; }
            public EnsembleDumpResultRunner(List<GraphQueryData> data, string path, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                Path = path;
            }

            public override void Init()
            {
                Writer = new StreamWriter(Path);
                tmpWriter = new StreamWriter(Path + ".tmpPoem.mcts");
                SmpNum = 0;
                Score1 = 0;
                Score2 = 0;
            }

            public override void Complete()
            {
                Logger.WriteLog("Avg BLEU Score {0}, {1}", Score1 * 1.0f / SmpNum, Score2 * 1.0f / SmpNum);
                Writer.Close();
                tmpWriter.Close();
            }

            public override void Forward()
            {
                Dictionary<int, Tuple<string, float>> Results = new Dictionary<int, Tuple<string, float>>();

                List<int>[] refers = new List<int>[Data[0].BatchSize];
                List<int>[] cands = new List<int>[Data[0].BatchSize];

                List<List<int>> references = new List<List<int>>();
                List<List<int>> candidates = new List<List<int>>();

                for (int m = 0; m < Data.Count; m++)
                {
                    for (int i = 0; i < Data[m].Results.Count; i++)
                    {
                        int t = Data[m].Results[i].Item1;
                        int b = Data[m].Results[i].Item2;

                        int origialB = Data[m].StatusPath[t].GetOriginalStatsIndex(b);

                        references.Add(Data[m].Reference(origialB));
                        candidates.Add(Data[m].Candidate(b, t));

                    }

                    for (int i = 0; i < Data[m].Results.Count; i++)
                    {
                        int t = Data[m].Results[i].Item1;
                        int b = Data[m].Results[i].Item2;
                        float p = Data[m].Results[i].Item3;

                        float score = Data[m].ResultScore[i];
                        p = score;
                        int origialB = Data[m].StatusPath[t].GetOriginalStatsIndex(b);
                        List<int> r = Data[m].StatusPath[t].GetHistoryNodeIdxs(b);

                        if (!Results.ContainsKey(origialB))
                        {
                            Results.Add(origialB, new Tuple<string, float>(string.Join(" ", r), p));
                            cands[origialB] = Data[m].Candidate(b, t);
                        }
                        else if (p > Results[origialB].Item2)
                        {
                            Results[origialB] = new Tuple<string, float>(string.Join(" ", r), p);
                            cands[origialB] = Data[m].Candidate(b, t);
                        }
                        refers[origialB] = Data[m].Reference(origialB);


                    }
                }

                List<float> bleus1 = PlainBLEUEvaluationSet.EvaluateBLEU(refers.ToList(), cands.ToList(), BuilderParameters.NGRAM_W, new HashSet<string>() { "1", "2" });
                List<float> bleus2 = PlainBLEUEvaluationSet.EvaluateBLEU(refers.ToList(), cands.ToList(), BuilderParameters.NGRAM_W, null);

                List<float> true_v_list = PlainBLEUEvaluationSet.EvaluateBLEU(references, candidates, BuilderParameters.NGRAM_W, new HashSet<string>() { "1", "2" });

                List<float> baseLines = new List<float>();
                for (int b = 0; b < Data[0].BatchSize; b++)
                {
                    string hisWords = Data[0].StatusPath[0].StatusKey[b].Replace('-', ' ');

                    List<int> nextWords = new List<int>();
                    for (int g = 0; g < BuilderParameters.K_PRED; g++)
                    {
                        int nw = (int)Data[0].NextWords.MemPtr[b * BuilderParameters.K_PRED + g];
                        nextWords.Add(nw);
                        if (nw == 0) break;
                    }
                    string groundWords = string.Join(" ", nextWords);

                    tmpWriter.WriteLine(hisWords);
                    tmpWriter.WriteLine(groundWords);
                    for (int m = 0; m < Data.Count; m++)
                    {
                        for (int i = 0; i < Data[m].Results.Count; i++)
                        {
                            int t1 = Data[m].Results[i].Item1;
                            int b1 = Data[m].Results[i].Item2;

                            int origialB = Data[m].StatusPath[t1].GetOriginalStatsIndex(b1);
                            if (origialB == b)
                            {
                                float estimate_v = Data[m].StatusPath[t1].Reward.Output.Data.MemPtr[b1];
                                if (estimate_v < 0) estimate_v = 0;
                                if (estimate_v > 1) estimate_v = 1;
                                float score = Data[m].ResultScore[i];

                                tmpWriter.WriteLine(string.Join(" ", candidates[m * Data[0].BatchSize + i]) + "\t" + true_v_list[m * Data[0].BatchSize + i].ToString() + "\t" + Data[m].Results[i].Item3.ToString() + "\t" + estimate_v.ToString() + "\t" + score.ToString());
                                
                                break;
                            }
                        }
                    }
                    tmpWriter.WriteLine();
                    tmpWriter.Flush();
                }



                Score1 += bleus1.Sum();
                Score2 += bleus2.Sum();
                SmpNum += refers.Length;
                for (int i = 0; i < Results.Count; i++)
                {
                    Writer.WriteLine(Results[i].Item1 + "\t" + Results[i].Item2.ToString() + "\t" + bleus1[i].ToString() + "\t" + Data[0].SmpIdx[i].ToString() + "\t" + bleus2[i].ToString());
                }

                for (int m = 0; m < Data.Count; m++)
                {
                    Data[m].Results.Clear();
                    Data[m].ResultScore.Clear();
                }
            }
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");

            //DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            //IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            DeviceBehavior behavior = new DeviceBehavior(BuilderParameters.GPUID);
            
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            LMLSTMModel model = null;

            if(BuilderParameters.SeedModel.EndsWith("rw"))
            {
                model = new LMLSTMModel(new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read)), behavior.Device, false);
            }
            else
            { 
                model = new LMLSTMModel(new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read)), behavior.Device, true);
            }
            EpisodicMemoryV2 Memory = new EpisodicMemoryV2(BuilderParameters.Vocab_Dim);

            ComputationGraph validCG = null;

            if (BuilderParameters.SEARCH_T == 0)
            {
                validCG = BuildBeamSearchComputationGraph(DataPanel.ValidCorpus, BuilderParameters.Mini_Batch, BuilderParameters.Vocab_Dim, model, Memory, behavior.PredictMode);
            }
            else if (BuilderParameters.SEARCH_T == 1)
            {
                validCG = BuildMCTSComputationGraph(DataPanel.ValidCorpus, BuilderParameters.Mini_Batch, BuilderParameters.Vocab_Dim, model, Memory, behavior.PredictMode);
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    model.InitOptimization(behavior.TrainMode);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainCorpus, BuilderParameters.Mini_Batch, BuilderParameters.Vocab_Dim, model, Memory, behavior.TrainMode);
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MaxValue;
                    double bestValidIter = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        validScore = validCG.Execute();
                        if (validScore < bestValidScore)
                        {
                            bestValidScore = validScore;
                            bestValidIter = iter;
                        }
                        Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);

                        if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString() + ".rw", FileMode.Create, FileAccess.Write)))
                            {
                                model.Serialize(writer);
                            }
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    double predScore = validCG.Execute();
                    break;
            }
            Logger.CloseLog();

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static List<List<int>> TrainCorpus = null;
            public static List<List<int>> ValidCorpus = null;

            public static int Max_Seq_Len { get; set; }

            static List<List<int>> LoadCorpus(string fileName)
            {
                List<List<int>> Data = new List<List<int>>();
                using (StreamReader reader = new StreamReader(fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        List<int> sInts = items.Select(i => int.Parse(i)).ToList();
                        Data.Add(sInts);
                    }
                }
                return Data;
            }

            public static void Init()
            {
                TrainCorpus = LoadCorpus(BuilderParameters.TrainData);
                ValidCorpus = LoadCorpus(BuilderParameters.ValidData);

                int Max_train_Seq_Len = TrainCorpus.Select(i => i.Count).Max();
                Logger.WriteLog("Max Seq Len for training Corpus {0}", Max_train_Seq_Len);

                int Max_valid_Seq_Len = ValidCorpus.Select(i => i.Count).Max();
                Logger.WriteLog("Max Seq Len for Valid Corpus {0}", Max_valid_Seq_Len);

                Max_Seq_Len = Math.Max(Max_train_Seq_Len, Max_valid_Seq_Len);
            }
        }
    }
}