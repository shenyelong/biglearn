﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppReasoNetReaderSQuADV2Builder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Testing Folder"));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Validation Folder"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("IS-EMBED-UPDATE", new ParameterArgument("0", "0 : Fix embedding; 1 : Update Embedding."));
                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument("1,1", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("EMBED-LAYER-DROPOUT", new ParameterArgument("0,0", "DNN Layer Dropout."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding File"));

                Argument.Add("GRU-DIM", new ParameterArgument("300", "GRU Dimension"));
                Argument.Add("IS-SHARE-RNN", new ParameterArgument("0", "0 : non-share; 1 : share"));

                Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("ATT2-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT2-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("ANS-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ANS-TYPE", new ParameterArgument(((int)CrossSimType.Cosine).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.RL)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));
                Argument.Add("QUERY-POOL", new ParameterArgument((((int)PoolingType.LAST)).ToString(), "Query Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));

                // Softmax Randomup.
                Argument.Add("ATT-GAMMA", new ParameterArgument("10", " Smooth Parameter."));
                Argument.Add("ANS-GAMMA", new ParameterArgument("100", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
                Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("RL-ALL", new ParameterArgument("0", "RL All"));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));

                Argument.Add("SPAN-LENGTH", new ParameterArgument("0", "Span length"));
                Argument.Add("SPAN-POOL", new ParameterArgument((((int)PoolingType.LAST)).ToString(), "Span Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));
            }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string Vocab { get { return Argument["VOCAB"].Value; } }

            #region train, test, validation dataset.
            public static string Train { get { return Argument["TRAIN"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!Train.Equals(string.Empty)); } }
            public static string TrainContext { get { return string.Format("{0}.para", Train); } }
            public static string TrainQuery { get { return string.Format("{0}.ques", Train); } }
            public static string TrainPos { get { return string.Format("{0}.pos", Train); } }

            public static string TrainContextBin { get { return string.Format("{0}.{1}.bin", TrainContext, MiniBatchSize); } }
            public static string TrainQueryBin { get { return string.Format("{0}.{1}.bin", TrainQuery, MiniBatchSize); } }
            public static string TrainPosBin { get { return string.Format("{0}.{1}.bin", TrainPos, MiniBatchSize); } }

            public static string TestFolder { get { return Argument["TEST"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }

            //public static string TestContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TestFolder, VocabSize, MiniBatchSize); } }
            //public static string TestQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TestFolder, VocabSize, MiniBatchSize); } }
            //public static string TestAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TestFolder, VocabSize, MiniBatchSize); } }

            public static string Valid { get { return Argument["VALID"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsValidFile { get { return (!Valid.Equals(string.Empty)); } }
            public static string ValidContext { get { return string.Format("{0}.para", Valid); } }
            public static string ValidQuery { get { return string.Format("{0}.ques", Valid); } }
            public static string ValidPos { get { return string.Format("{0}.pos", Valid); } }
            public static string ValidIds { get { return string.Format("{0}.ids", Valid); } }
            public static string ValidSpan { get { return string.Format("{0}.spans", Valid); } }

            public static string ValidContextBin { get { return string.Format("{0}.{1}.bin", ValidContext, MiniBatchSize); } }
            public static string ValidQueryBin { get { return string.Format("{0}.{1}.bin", ValidQuery, MiniBatchSize); } }
            public static string ValidPosBin { get { return string.Format("{0}.{1}.bin", ValidPos, MiniBatchSize); } }

            //public static string ValidIndexData { get { return string.Format("{0}.{1}.idx.tsv", ValidFolder, VocabSize); } }
            //public static string ValidContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            //public static string ValidQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            //public static string ValidAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            #endregion.

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static bool IS_EMBED_UPDATE { get { return int.Parse(Argument["IS-EMBED-UPDATE"].Value) > 0; } }
            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] EMBED_LAYER_WIN { get { return Argument["EMBED-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] EMBED_DROPOUT
            {
                get
                {
                    if (Argument["EMBED-LAYER-DROPOUT"].Value.Equals(string.Empty)) return EMBED_LAYER_DIM.Select(i => 0.0f).ToArray();
                    else return Argument["EMBED-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray();
                }
            }
            public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }

            public static int GRU_DIM { get { return int.Parse(Argument["GRU-DIM"].Value); } }
            public static bool IS_SHARE_RNN { get { return int.Parse(Argument["IS-SHARE-RNN"].Value) > 0; } }

            //public static bool IS_ENTITY_LSTM { get { return int.Parse(Argument["IS-ENTITY-LSTM"].Value) > 0; } }
            public static Recurrent_Unit Recurrent_Gate_Type { get { return (Recurrent_Unit)int.Parse(Argument["RECURRENT-GATE-TYPE"].Value); } }

            public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
            public static CrossSimType AttType { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }

            public static int ATT2_HID_DIM { get { return int.Parse(Argument["ATT2-HID"].Value); } }
            public static CrossSimType Att2Type { get { return (CrossSimType)int.Parse(Argument["ATT2-TYPE"].Value); } }

            public static int ANS_HID_DIM { get { return int.Parse(Argument["ANS-HID"].Value); } }
            public static CrossSimType AnsType { get { return (CrossSimType)int.Parse(Argument["ANS-TYPE"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static PoolingType RECURRENT_POOL { get { return (PoolingType)int.Parse(Argument["RECURRENT-POOL"].Value); } }
            public static PoolingType QUERY_POOL { get { return (PoolingType)int.Parse(Argument["QUERY-POOL"].Value); } }
            public static bool RL_ALL { get { return int.Parse(Argument["RL-ALL"].Value) > 0; } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
            public static bool IS_NORM_REWARD { get { return int.Parse(Argument["IS-NORM-REWARD"].Value) > 0; } }
            public static float ATT_Gamma { get { return float.Parse(Argument["ATT-GAMMA"].Value); } }
            public static float ANS_Gamma { get { return float.Parse(Argument["ANS-GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

            public static string DebugFile { get { return Argument["DEBUG-FILE"].Value; } }
            public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }

            public static int SPAN_LENGTH { get { return int.Parse(Argument["SPAN-LENGTH"].Value); } }
            public static PoolingType SPAN_POOL { get { return (PoolingType)int.Parse(Argument["SPAN-POOL"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_SQUAD_READER_V2; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        enum PredType { RL_MAXITER, RL_AVGSIM, RL_AVGPROB }

        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class MemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }
            public HiddenBatchData SoftmaxAddress { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                MatchData = matchData;
                Gamma = gamma;
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
            }

            public override void Forward()
            {
                Output.BatchSize = MatchData.SrcSize;

                // softmax 
                // softmax of attention
                ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

                ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
                    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                    Memory.SentSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                //float oDeriv_norm2 = ComputeLib.L2Norm(Output.Deriv.Data, Output.Dim * Output.BatchSize);

                // outputDeriv -> memoryContentDeriv.
                ComputeLib.Scale_MatrixMask(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
                                            Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
                                            Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data, 1);

                //float aDeriv_norm2 = ComputeLib.L2Norm(SoftmaxAddress.Output.Data, SoftmaxAddress.Dim * Memory.SentSize);
                //float mDeriv_norm2 = ComputeLib.L2Norm(Memory.SentDeriv, Memory.Dim * Memory.SentSize);
                //Memory.SentDeriv.SyncToCPU();
                //SoftmaxAddress.Output.Data.SyncToCPU();
                //Output.Deriv.Data.SyncToCPU();

                // outputDeriv -> AddressDeriv.
                ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Fix Iteration, SoftAttention, Supervised Training.
        /// </summary>
        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            HiddenBatchData InitStatus { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }

            SeqDenseBatchData Memory2 { get; set; }
            BiMatchBatchData MatchData2 { get; set; }

            SeqDenseBatchData AnsMem { get; set; }
            BiMatchBatchData AnsMatchData { get; set; }

            PoolingType ReasonPool = BuilderParameters.RECURRENT_POOL;

            string DebugFile { get; set; }
            //StreamWriter DebugWriter = null;
            //int DebugSample = 0;
            //int DebugIter = 0;

            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }
            public ReasonNetRunner(
                HiddenBatchData initStatus, int maxIterateNum,
                SeqDenseBatchData mem, BiMatchBatchData matchData, MLPAttentionStructure attStruct,
                SeqDenseBatchData mem2, BiMatchBatchData matchData2, MLPAttentionStructure att2Struct,
                SeqDenseBatchData ansMem, BiMatchBatchData ansMatchData, MLPAttentionStructure ansStruct,
                GRUCell gruCell, LayerStructure termStruct, float gamma, RunnerBehavior behavior, string debugFile = "") : base(Structure.Empty, behavior)
            {
                InitStatus = initStatus;

                Memory = mem;
                MatchData = matchData;
                AttStruct = attStruct;

                Memory2 = mem2;
                MatchData2 = matchData2;
                Att2Struct = att2Struct;

                AnsMem = ansMem;
                AnsMatchData = ansMatchData;
                AnsStruct = ansStruct;

                GruCell = gruCell;
                TermStruct = termStruct;

                MaxIterationNum = maxIterateNum;
                DebugFile = debugFile;

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    AttHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = Memory.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = Memory.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = attStruct.HiddenDim
                    }, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);
                }
                //else if (BuilderParameters.AttType == CrossSimType.Cosine)
                //{
                //    SoftCosineSimRunner initAttRunner = new SoftCosineSimRunner(InitStatus, Memory, MatchData, Behavior);
                //    AttRunner.Add(initAttRunner);
                //}

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    Att2Hidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = Memory2.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = Memory2.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = att2Struct.HiddenDim
                    }, Memory2.SampleIdx, Memory2.SentMargin, Behavior.Device);
                }
                //else if (BuilderParameters.Att2Type == CrossSimType.Cosine)
                //{
                //    SoftCosineSimRunner initAtt2Runner = new SoftCosineSimRunner(InitStatus, Memory2, MatchData2, Behavior);
                //    Att2Runner.Add(initAtt2Runner);
                //}

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    AnsHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = AnsMem.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = AnsMem.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = AnsStruct.HiddenDim
                    }, AnsMem.SampleIdx, AnsMem.SentMargin, Behavior.Device);
                }
                //else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                //{
                //    SoftCosineSimRunner initAnsStartRunner = new SoftCosineSimRunner(InitStatus, AnsMem, AnsMatchData, Behavior);
                //    AnsStartRunner.Add(initAnsStartRunner);
                //}

                AnsRunner.Add(new SoftLinearSimRunner(InitStatus, AnsHidden, AnsMatchData, BuilderParameters.AnsType, AnsStruct, Behavior));
                TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, InitStatus, Behavior));

                HiddenBatchData lastStatus = InitStatus;
                HiddenBatchData lastAtt = null; // (HiddenBatchData)AttRunner[0].Output;
                HiddenBatchData lastAtt2 = null; // (HiddenBatchData)Att2Runner[0].Output;

                for (int i = 0; i < maxIterateNum - 1; i++)
                {
                    if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner attRunner = new SoftLinearSimRunner(lastStatus, AttHidden, MatchData, BuilderParameters.AttType, AttStruct, Behavior);
                        AttRunner.Add(attRunner);
                        lastAtt = attRunner.Output;
                    }
                    //else if (BuilderParameters.AttType == CrossSimType.Cosine)
                    //{
                    //    SoftCosineSimRunner attRunner = new SoftCosineSimRunner(lastStatus, Memory, MatchData, Behavior);
                    //    AttRunner.Add(attRunner);
                    //    lastAtt = attRunner.Output;
                    //}

                    if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner att2Runner = new SoftLinearSimRunner(lastStatus, Att2Hidden, MatchData2, BuilderParameters.Att2Type, Att2Struct, Behavior);
                        Att2Runner.Add(att2Runner);
                        lastAtt2 = att2Runner.Output;
                    }
                    //else if (BuilderParameters.Att2Type == CrossSimType.Cosine)
                    //{
                    //    SoftCosineSimRunner att2Runner = new SoftCosineSimRunner(lastStatus, Memory2, MatchData2, Behavior);
                    //    Att2Runner.Add(att2Runner);
                    //    lastAtt2 = att2Runner.Output;
                    //}

                    MemoryRetrievalRunner xRunner = new MemoryRetrievalRunner(lastAtt, Memory, MatchData, gamma, Behavior);
                    MemRetrievalRunner.Add(xRunner);

                    MemoryRetrievalRunner x2Runner = new MemoryRetrievalRunner(lastAtt2, Memory2, MatchData2, gamma, Behavior);
                    MemRetrieval2Runner.Add(x2Runner);

                    EnsembleMatrixRunner eRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { xRunner.Output, x2Runner.Output }, Behavior);
                    RetrievalEnsembleRunner.Add(eRunner);

                    GRUQueryRunner yRunner = new GRUQueryRunner(gruCell, lastStatus, eRunner.Output, Behavior);
                    StatusRunner.Add(yRunner);
                    lastStatus = yRunner.Output;

                    if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                    {
                        SoftLinearSimRunner ansRunner = new SoftLinearSimRunner(lastStatus, AnsHidden, AnsMatchData, BuilderParameters.AnsType, AnsStruct, Behavior);
                        AnsRunner.Add(ansRunner);
                    }
                    //else if (BuilderParameters.AnsType == CrossSimType.Cosine)
                    //{
                    //    SoftCosineSimRunner ansStartRunner = new SoftCosineSimRunner(lastStatus, AnsMem, AnsMatchData, Behavior);
                    //    AnsStartRunner.Add(ansStartRunner);
                    //}

                    TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, lastStatus, Behavior));
                }

                AnswerProb = new HiddenBatchData[MaxIterationNum];
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
                }
                FinalAns = new HiddenBatchData(AnsMatchData.Stat.MAX_MATCH_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
            }

            MLPAttentionStructure AttStruct = null;
            SeqDenseBatchData AttHidden = null;
            List<StructRunner> AttRunner = new List<StructRunner>();

            MLPAttentionStructure Att2Struct = null;
            SeqDenseBatchData Att2Hidden = null;
            List<StructRunner> Att2Runner = new List<StructRunner>();

            List<MemoryRetrievalRunner> MemRetrievalRunner = new List<MemoryRetrievalRunner>();
            List<MemoryRetrievalRunner> MemRetrieval2Runner = new List<MemoryRetrievalRunner>();
            List<EnsembleMatrixRunner> RetrievalEnsembleRunner = new List<EnsembleMatrixRunner>();

            GRUCell GruCell = null;
            List<GRUQueryRunner> StatusRunner = new List<GRUQueryRunner>();

            LayerStructure TermStruct = null;
            List<FullyConnectHiddenRunner<HiddenBatchData>> TerminalRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

            MLPAttentionStructure AnsStruct = null;
            SeqDenseBatchData AnsHidden = null;
            List<StructRunner> AnsRunner = new List<StructRunner>();

            public HiddenBatchData[] AnsData { get { return AnsRunner.Select(i => (HiddenBatchData)i.Output).ToArray(); } }
            public HiddenBatchData[] AnswerProb = null;

            public HiddenBatchData TerminalProb = null;
            public HiddenBatchData FinalAns = null;

            /// <summary>
            /// Output Debug File.
            /// </summary>
            public override void Init()
            {
                if (DebugFile != "" && !DebugFile.Equals(string.Empty))
                {
                    //DebugWriter = new StreamWriter(DebugFile + DateTime.Now.ToFileTime().ToString() + "." + DebugIter.ToString() + ".iter");
                    //DebugSample = 0;
                    //DebugIter += 1;
                }
            }

            /// <summary>
            /// Save Debug File.
            /// </summary>
            public override void Complete()
            {
                //if (DebugWriter != null)
                //{
                //    DebugWriter.Close();
                //}
            }

            public override void Forward()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    AttHidden.BatchSize = Memory.BatchSize;
                    AttHidden.SentSize = Memory.SentSize;
                    ComputeLib.Sgemm(Memory.SentOutput, 0, AttStruct.Wm, 0, AttHidden.SentOutput, 0,
                                     Memory.SentSize, Memory.Dim, AttStruct.HiddenDim, 0, 1, false, false);
                    if (AttStruct.IsBias) ComputeLib.Matrix_Add_Linear(AttHidden.SentOutput, AttStruct.B, Memory.SentSize, AttStruct.HiddenDim);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    Att2Hidden.BatchSize = Memory2.BatchSize;
                    Att2Hidden.SentSize = Memory2.SentSize;
                    ComputeLib.Sgemm(Memory2.SentOutput, 0, Att2Struct.Wm, 0, Att2Hidden.SentOutput, 0,
                                     Memory2.SentSize, Memory2.Dim, Att2Struct.HiddenDim, 0, 1, false, false);
                    if (Att2Struct.IsBias) ComputeLib.Matrix_Add_Linear(Att2Hidden.SentOutput, Att2Struct.B, Memory2.SentSize, Att2Struct.HiddenDim);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    AnsHidden.BatchSize = AnsMem.BatchSize;
                    AnsHidden.SentSize = AnsMem.SentSize;
                    //AnsMem.SentOutput.SyncToCPU();
                    ComputeLib.Sgemm(AnsMem.SentOutput, 0, AnsStruct.Wm, 0, AnsHidden.SentOutput, 0,
                                     AnsMem.SentSize, AnsMem.Dim, AnsStruct.HiddenDim, 0, 1, false, false);
                    if (AnsStruct.IsBias) ComputeLib.Matrix_Add_Linear(AnsHidden.SentOutput, AnsStruct.B, AnsMem.SentSize, AnsStruct.HiddenDim);
                    //AnsHidden.SentOutput.SyncToCPU();
                }

                AnsRunner[0].Forward();
                TerminalRunner[0].Forward();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    // take attention
                    AttRunner[i].Forward();
                    Att2Runner[i].Forward();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Forward();
                    MemRetrieval2Runner[i].Forward();
                    RetrievalEnsembleRunner[i].Forward();

                    // update Status.
                    StatusRunner[i].Forward();
                    
                    AnsRunner[i + 1].Forward();
                    // take terminal gate or not.
                    TerminalRunner[i + 1].Forward();
                }

                #region RL Pooling.
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize);
                    }

                    FinalAns.BatchSize = AnsData.Last().BatchSize;
                    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize);
                    AnsMatchData.Src2MatchIdx.SyncToCPU(MatchData.SrcSize);
                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        int max_iter = 0;
                        double max_p = 0;

                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t += (float)Math.Log(1 - t_i);

                            if (Math.Exp(p) > max_p)
                            {
                                max_p = Math.Exp(p);
                                max_iter = i;
                            }

                            if (BuilderParameters.PRED_RL == PredType.RL_AVGSIM)
                            {
                                int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                                int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    FinalAns.Output.Data.MemPtr[m] += (float)Math.Exp(p) * AnsData[i].Output.Data.MemPtr[m];
                                }
                            }
                            if (BuilderParameters.PRED_RL == PredType.RL_AVGPROB)
                            {
                                int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                                int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];
                                double expSum = 0;
                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    expSum += Math.Exp(BuilderParameters.ANS_Gamma * AnsData[i].Output.Data.MemPtr[m]);
                                }

                                for (int m = matchBgn; m < matchEnd; m++)
                                {
                                    FinalAns.Output.Data.MemPtr[m] += 
                                        (float)(Math.Exp(p) * Math.Exp(BuilderParameters.ANS_Gamma * AnsData[i].Output.Data.MemPtr[m]) / expSum);
                                }
                            }
                        }
                        if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
                        {
                            int matchBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];

                            double expSum = 0;
                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                expSum += Math.Exp(BuilderParameters.ANS_Gamma * AnsData[max_iter].Output.Data.MemPtr[m]);
                            }

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalAns.Output.Data.MemPtr[m] = 
                                    (float)(Math.Exp(BuilderParameters.ANS_Gamma * AnsData[max_iter].Output.Data.MemPtr[m]) / expSum);  //AnsStartData[max_iter].Output.Data.MemPtr[m];
                            }
                        }
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize);
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i].BatchSize = MatchData.SrcSize;
                        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
                    }
                    #region debug comment.
                    // write the reasonet status.
                    //if (DebugWriter != null)
                    //{
                    //    for (int i = 0; i < MaxIterationNum - 1; i++)
                    //    {
                    //        MemRetrievalRunner[i].SoftmaxAddress.Output.Data.SyncToCPU();
                    //        MemRetrieval2Runner[i].SoftmaxAddress.Output.Data.SyncToCPU();
                    //    }

                    //    for (int b = 0; b < MatchData.SrcSize; b++)
                    //    {
                    //        int att1Bgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                    //        int att1End = MatchData.Src2MatchIdx.MemPtr[b];

                    //        int att2Bgn = b == 0 ? 0 : MatchData2.Src2MatchIdx.MemPtr[b - 1];
                    //        int att2End = MatchData2.Src2MatchIdx.MemPtr[b];

                    //        int ansBgn = b == 0 ? 0 : AnsMatchData.Src2MatchIdx.MemPtr[b - 1];
                    //        int ansEnd = AnsMatchData.Src2MatchIdx.MemPtr[b];

                    //        DebugWriter.WriteLine("Sample {0}.............", DebugSample + b);

                    //        for (int i = 0; i < MaxIterationNum; i++)
                    //        {
                    //            string att1Str = "";
                    //            string att2Str = "";
                    //            string ansStr = "";

                    //            if (i > 0)
                    //            {
                    //                for (int m = att1Bgn; m < att1End; m++)
                    //                {
                    //                    att1Str = att1Str + MemRetrievalRunner[i - 1].SoftmaxAddress.Output.Data.MemPtr[m].ToString() + " ";
                    //                }

                    //                for (int m = att2Bgn; m < att2End; m++)
                    //                {
                    //                    att2Str = att2Str + MemRetrieval2Runner[i - 1].SoftmaxAddress.Output.Data.MemPtr[m].ToString() + " ";
                    //                }
                    //            }

                    //            float aSum = 0;
                    //            for (int m = ansBgn; m < ansEnd; m++)
                    //            {
                    //                aSum += (float)Math.Exp(BuilderParameters.Gamma * AnsData[i].Output.Data.MemPtr[m]);
                    //            }

                    //            for (int m = ansBgn; m < ansEnd; m++)
                    //            {
                    //                ansStr = ansStr + (Math.Exp(BuilderParameters.Gamma * AnsData[i].Output.Data.MemPtr[m]) / aSum).ToString() + " ";
                    //            }

                    //            float termProb = TerminalRunner[i].Output.Output.Data.MemPtr[b];

                    //            DebugWriter.WriteLine("T {0}\t{1}\t{2}\t{3}\t{4}", i, ansStr, termProb, att1Str, att2Str);
                    //        }

                    //        string finalAnsStr = "";

                    //        for (int m = ansBgn; m < ansEnd; m++)
                    //        {
                    //            finalAnsStr = finalAnsStr + FinalAns.Output.Data.MemPtr[m].ToString() + " ";
                    //        }
                    //        DebugWriter.WriteLine("Sample {0}\t{1}", DebugSample + b, finalAnsStr);
                    //        DebugWriter.WriteLine();
                    //    }
                    //    DebugSample += MatchData.SrcSize;
                    //}
                    #endregion.
                }
                #endregion.
            }
            public override void CleanDeriv()
            {
                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    TerminalRunner[i + 1].CleanDeriv();
                    // take Answer.
                    AnsRunner[i + 1].CleanDeriv();

                    // update Status.
                    StatusRunner[i].CleanDeriv();

                    RetrievalEnsembleRunner[i].CleanDeriv();

                    MemRetrieval2Runner[i].CleanDeriv();
                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].CleanDeriv();

                    Att2Runner[i].CleanDeriv();
                    // take Attention
                    AttRunner[i].CleanDeriv();
                }
                TerminalRunner[0].CleanDeriv();
                AnsRunner[0].CleanDeriv();

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(AttHidden.SentDeriv, AttHidden.SentSize * AttHidden.Dim);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(Att2Hidden.SentDeriv, Att2Hidden.SentSize * Att2Hidden.Dim);
                }

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(AnsHidden.SentDeriv, AnsHidden.SentSize * AnsHidden.Dim);
                }
            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                #region RL Pool.
                //else if (ReasonPool == PoolingType.RL)
                {
                    //int BatchSize = AnswerSeqData.Last().BatchSize;
                    //according to loss, assign reward to each terminal node.
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(TerminalRunner[i].Output.Deriv.Data.MemPtr, 0, AnsMatchData.SrcSize);
                    }

                    for (int b = 0; b < AnsMatchData.SrcSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                            if (BuilderParameters.IS_NORM_REWARD) reward = reward / (i + 1);

                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] += reward * (1 - TerminalRunner[i].Output.Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                TerminalRunner[hp].Output.Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * reward * TerminalRunner[hp].Output.Output.Data.MemPtr[b];
                            }

                            if (TerminalRunner[i].Output.Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalRunner[i].Output.Deriv.Data.MemPtr[b], i);
                            }
                        }
                    }

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }
                #endregion.

                for (int i = MaxIterationNum - 2; i >= 0; i--)
                {
                    TerminalRunner[i + 1].Backward(cleanDeriv);
                    AnsRunner[i + 1].Backward(cleanDeriv);
                    
                    StatusRunner[i].Backward(cleanDeriv);

                    RetrievalEnsembleRunner[i].Backward(cleanDeriv);

                    MemRetrieval2Runner[i].Backward(cleanDeriv);

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Backward(cleanDeriv);

                    Att2Runner[i].Backward(cleanDeriv);
                    // take action. new Attention
                    AttRunner[i].Backward(cleanDeriv);
                    // update Status.
                }
                TerminalRunner[0].Backward(cleanDeriv);
                AnsRunner[0].Backward(cleanDeriv);

                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(AnsHidden.SentDeriv, 0,
                                 AnsStruct.Wm, 0,
                                 AnsMem.SentDeriv, 0,
                                 AnsMem.SentSize, AnsStruct.HiddenDim, AnsStruct.MemoryDim, 1, 1, false, true);
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(Att2Hidden.SentDeriv, 0,
                                 Att2Struct.Wm, 0,
                                 Memory2.SentDeriv, 0,
                                 Memory2.SentSize, Att2Struct.HiddenDim, Att2Struct.MemoryDim, 1, 1, false, true);
                }

                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Sgemm(AttHidden.SentDeriv, 0,
                                 AttStruct.Wm, 0,
                                 Memory.SentDeriv, 0,
                                 Memory.SentSize, AttStruct.HiddenDim, AttStruct.MemoryDim, 1, 1, false, true);
                }
            }

            public override void Update()
            {
                if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AttStruct.IsBias)
                        ComputeLib.ColumnWiseSum(AttHidden.SentDeriv, AttStruct.BMatrixOptimizer.Gradient, Memory.SentSize, AttStruct.HiddenDim, AttStruct.BMatrixOptimizer.GradientStep);

                    ComputeLib.Sgemm(Memory.SentOutput, 0,
                                     AttHidden.SentDeriv, 0,
                                     AttStruct.WmMatrixOptimizer.Gradient, 0,
                                     Memory.SentSize, AttStruct.MemoryDim, AttStruct.HiddenDim,
                                     1, AttStruct.WmMatrixOptimizer.GradientStep, true, false);

                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.AfterGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.AfterGradient();
                    }
                }

                if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        Att2Struct.WmMatrixOptimizer.BeforeGradient();
                        if (Att2Struct.IsBias) Att2Struct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (Att2Struct.IsBias)
                        ComputeLib.ColumnWiseSum(Att2Hidden.SentDeriv, Att2Struct.BMatrixOptimizer.Gradient, Memory2.SentSize, Att2Struct.HiddenDim, Att2Struct.BMatrixOptimizer.GradientStep);

                    ComputeLib.Sgemm(Memory2.SentOutput, 0,
                                     Att2Hidden.SentDeriv, 0,
                                     Att2Struct.WmMatrixOptimizer.Gradient, 0,
                                     Memory2.SentSize, Att2Struct.MemoryDim, Att2Struct.HiddenDim,
                                     1, Att2Struct.WmMatrixOptimizer.GradientStep, true, false);

                    if (!IsDelegateOptimizer)
                    {
                        Att2Struct.WmMatrixOptimizer.AfterGradient();
                        if (Att2Struct.IsBias) Att2Struct.BMatrixOptimizer.AfterGradient();
                    }
                }


                if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AnsStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AnsStruct.IsBias) AnsStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    if (AnsStruct.IsBias)
                        ComputeLib.ColumnWiseSum(AnsHidden.SentDeriv, AnsStruct.BMatrixOptimizer.Gradient, AnsMem.SentSize, AnsStruct.HiddenDim, AnsStruct.BMatrixOptimizer.GradientStep);

                    ComputeLib.Sgemm(AnsMem.SentOutput, 0,
                                     AnsHidden.SentDeriv, 0,
                                     AnsStruct.WmMatrixOptimizer.Gradient, 0,
                                     AnsMem.SentSize, AnsStruct.MemoryDim, AnsStruct.HiddenDim,
                                     1, AnsStruct.WmMatrixOptimizer.GradientStep, true, false);

                    if (!IsDelegateOptimizer)
                    {
                        AnsStruct.WmMatrixOptimizer.AfterGradient();
                        if (AnsStruct.IsBias) AnsStruct.BMatrixOptimizer.AfterGradient();
                    }
                }

                //AttRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                //AttRunner[0].Update();

                //Att2Runner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                //Att2Runner[0].Update();

                AnsRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                TerminalRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;

                AnsRunner[0].Update();
                TerminalRunner[0].Update();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    AttRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    Att2Runner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    MemRetrievalRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    MemRetrieval2Runner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    StatusRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;

                    AnsRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    TerminalRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;

                    // take action. new Attention
                    AttRunner[i].Update();
                    Att2Runner[i].Update();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Update();
                    MemRetrieval2Runner[i].Update();

                    // update Status.
                    StatusRunner[i].Update();

                    AnsRunner[i + 1].Update();
                    TerminalRunner[i + 1].Update();
                }

                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                //{
                //}
                //if (BuilderParameters.Att2Type == CrossSimType.Addition || BuilderParameters.Att2Type == CrossSimType.Product || BuilderParameters.Att2Type == CrossSimType.SubCosine)
                //{
                //}
                //if (BuilderParameters.AnsType == CrossSimType.Addition || BuilderParameters.AnsType == CrossSimType.Product || BuilderParameters.AnsType == CrossSimType.SubCosine)
                //{
                //}
            }
        }
        
        class ExactMatchPredictionRunner : ObjectiveRunner
        {
            HiddenBatchData PosData;
            BiMatchBatchData AnsMatch;
            List<HashSet<string>> PosTrueData;

            string OutputPath = "";
            StreamWriter OutputWriter = null;

            int SampleNum = 0;
            int HitNum = 0;
            int Iteration = 0;

            //List<Dictionary<string, float>> Result = null;
            //List<List<Dictionary<string, float>>> HisResults = new List<List<Dictionary<string, float>>>();

            public ExactMatchPredictionRunner(
                HiddenBatchData posData, BiMatchBatchData ansMatch,
                List<HashSet<string>> posTrueData,
                RunnerBehavior behavior, string outputFile = "") : base(Structure.Empty, behavior)
            {
                PosData = posData;
                AnsMatch = ansMatch;
                PosTrueData = posTrueData;
                
                OutputPath = outputFile;
            }

            string resultFile = string.Empty;
            public override void Init()
            {
                SampleNum = 0;
                HitNum = 0;
                if (!OutputPath.Equals(""))
                {
                    resultFile = OutputPath + "." + Iteration.ToString() + ".result";
                    OutputWriter = new StreamWriter(resultFile);
                }
            }

            public override void Complete()
            {
                Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum);
                if (OutputWriter != null)
                {
                    OutputWriter.Close();

                    //restore.py --span_path dev.nltk.spans --raw_path dev.doc --ids_path dev.ids --fin score.4.result --fout dump.json
                    using (Process callProcess = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
                            Arguments = string.Format("\"{0}\" --span_path \"{1}\" --raw_path \"{2}\" --ids_path \"{3}\" --fin \"{4}\" --fout {5}",
                            @"\PublicDataSource\SQuADV3\restore.py",
                            BuilderParameters.ValidSpan,
                            @"\PublicDataSource\SQuADV3\dev.doc",
                            @"\PublicDataSource\SQuADV3\dev.ids",
                            resultFile,
                            resultFile + ".json"
                            ),
                            CreateNoWindow = true,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                        }
                    })
                    {
                        callProcess.Start();
                        callProcess.WaitForExit();
                    }

                    string resultsOutput = "";
                    using (Process callProcess = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
                            Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"",
                            @"\PublicDataSource\SQuADV3\evaluate-v1.1.py",
                            @"\PublicDataSource\SQuADV3\dev-v1.1.json",
                            resultFile + ".json"
                            ),
                            CreateNoWindow = true,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                        }
                    })
                    {
                        callProcess.Start();
                        resultsOutput = callProcess.StandardOutput.ReadToEnd();
                        callProcess.WaitForExit();
                    }

                    Console.WriteLine("Let see results here {0}", resultsOutput);
                }
                ObjectiveScore = HitNum * 1.0 / SampleNum;
                Iteration += 1;
            }

            public override void Forward()
            {
                PosData.Output.Data.SyncToCPU(PosData.BatchSize);
                AnsMatch.Src2MatchIdx.SyncToCPU(AnsMatch.SrcSize);
                AnsMatch.MatchInfo.SyncToCPU(AnsMatch.MatchSize);

                for (int i = 0; i < AnsMatch.SrcSize; i++)
                {
                    Dictionary<string, float> candScoreDict = new Dictionary<string, float>();
                    string trueCandIdx = string.Empty;

                    int candBgn = i == 0 ? 0 : AnsMatch.Src2MatchIdx.MemPtr[i - 1];
                    int candEnd = AnsMatch.Src2MatchIdx.MemPtr[i];

                    float maxP = 0;
                    int maxIndex = -1;
                    for (int c = candBgn; c < candEnd; c++)
                    {
                        float candScore = PosData.Output.Data.MemPtr[c];
                        if (candScore> maxP)
                        {
                            maxP = candScore;
                            maxIndex = c;
                        }
                    }

                    if (OutputWriter != null)
                    {
                        OutputWriter.WriteLine("{0}\t{1}", maxIndex, maxP);
                    }

                    if(AnsMatch.MatchInfo.MemPtr[maxIndex] > 0)
                    {
                        HitNum += 1;
                    }
                    //if(PosTrueData[SampleNum + i].Contains(string.Format("{0}#{1}", maxStart, maxEnd)))
                    //{
                    //    HitNum += 1;
                    //}
                }
                SampleNum += AnsMatch.SrcSize;
            }

        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             DataCashier<DenseBatchData, DenseDataStat> startingPos,
                                                             DataCashier<DenseBatchData, DenseDataStat> endPos,
                                                             DataCashier<BiMatchBatchData, BiMatchBatchDataStat> labelMatch,

                                                             // text embedding cnn.
                                                             List<LayerStructure> EmbedCNN,

                                                             // context lstm 
                                                             GRUStructure ContextD1LSTM, GRUStructure ContextD2LSTM,

                                                             // query lstm
                                                             GRUStructure QueryD1LSTM, GRUStructure QueryD2LSTM,

                                                             // reason net;
                                                             GRUCell StateStruct, MLPAttentionStructure AttStruct, MLPAttentionStructure Att2Struct,

                                                             MLPAttentionStructure AnsStruct,

                                                             LayerStructure TerminalStruct,
                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior, string resultFile)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ Query Passage Candidate data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
            DenseBatchData StartPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(startingPos, Behavior));
            DenseBatchData EndPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(endPos, Behavior));
            BiMatchBatchData LabelMatch = (BiMatchBatchData)cg.AddDataRunner(new DataRunner<BiMatchBatchData, BiMatchBatchDataStat>(labelMatch, Behavior));
            
            /************************************************************ CNN on Query data *********/
            SeqSparseConvRunner<SeqSparseBatchData> q1Runner = new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], QueryData, Behavior);
            q1Runner.IsBackProp = BuilderParameters.IS_EMBED_UPDATE;
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(q1Runner);
            for (int i = 1; i < EmbedCNN.Count; i++)
            {
                SeqDenseConvRunner<SeqDenseBatchData> q2Runner = new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], QueryEmbedOutput, Behavior);
                q2Runner.IsBackProp = BuilderParameters.IS_EMBED_UPDATE;
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(q2Runner);
            }
            /************************************************************ LSTM on Query data *********/
            HiddenBatchData QueryOutput = null;
            SeqDenseBatchData QuerySeqOutput = null;
            BiMatchBatchData QueryMatch = null;
            {
                SeqDenseRecursiveData QueryD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(QueryEmbedOutput, false, Behavior));
                FastGRUDenseRunner<SeqDenseRecursiveData> queryD1LSTMRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(QueryD1LSTM.GRUCells[0], QueryD1LstmInput, Behavior);
                SeqDenseRecursiveData QueryD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(queryD1LSTMRunner);

                SeqDenseRecursiveData QueryD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(QueryEmbedOutput, true, Behavior));
                FastGRUDenseRunner<SeqDenseRecursiveData> queryD2LSTMRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(QueryD2LSTM.GRUCells[0], QueryD2LstmInput, Behavior);
                SeqDenseRecursiveData QueryD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(queryD2LSTMRunner);

                SeqDenseBatchData QuerySeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(QueryD1LstmOutput, Behavior));
                SeqDenseBatchData QuerySeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(QueryD2LstmOutput, Behavior));
                QuerySeqOutput = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QuerySeqD1O, QuerySeqD2O }, Behavior));
                QueryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(QuerySeqOutput, Behavior));

                if (BuilderParameters.QUERY_POOL == PoolingType.LAST)
                {
                    HiddenBatchData QueryD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD1LstmOutput, true, 0, QueryD1LstmOutput.MapForward, Behavior));
                    HiddenBatchData QueryD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD2LstmOutput, false, 0, QueryD2LstmOutput.MapForward, Behavior));
                    QueryOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { QueryD1O, QueryD2O }, Behavior));
                }
                else if (BuilderParameters.QUERY_POOL == PoolingType.MAX)
                {
                    QueryOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QuerySeqOutput, Behavior));
                }
            }
            //else
            //{
            //    QueryOutput = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryEmbedOutput, Behavior));
            //}

            /************************************************************ Embedding on Passage data *********/
            SeqSparseConvRunner<SeqSparseBatchData> d1Runner = new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN[0], ContextData, Behavior);
            d1Runner.IsBackProp = BuilderParameters.IS_EMBED_UPDATE;
            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(d1Runner);
            for (int i = 1; i < EmbedCNN.Count; i++)
            {
                SeqDenseConvRunner<SeqDenseBatchData> d2Runner = new SeqDenseConvRunner<SeqDenseBatchData>(EmbedCNN[i], ContextEmbedOutput, Behavior);
                d2Runner.IsBackProp = BuilderParameters.IS_EMBED_UPDATE;
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(d2Runner);
            }
            SeqDenseBatchData MemorySeqO = null;
            BiMatchBatchData MemoryMatch = null;
            /************************************************************ LSTM on Passage data *********/
            {
                /************************************************************ LSTM on Candidate Word data *********/
                SeqDenseRecursiveData CandidateD1LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(ContextEmbedOutput, false, Behavior));
                FastGRUDenseRunner<SeqDenseRecursiveData> candidateD1LSTMRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(ContextD1LSTM.GRUCells[0], CandidateD1LstmInput, Behavior);
                SeqDenseRecursiveData CandidateD1LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD1LSTMRunner);

                SeqDenseRecursiveData CandidateD2LstmInput = (SeqDenseRecursiveData)cg.AddRunner(new Cook_Seq2TransposeSeqRunner(ContextEmbedOutput, true, Behavior));
                FastGRUDenseRunner<SeqDenseRecursiveData> candidateD2LSTMRunner = new FastGRUDenseRunner<SeqDenseRecursiveData>(ContextD2LSTM.GRUCells[0], CandidateD2LstmInput, Behavior);
                SeqDenseRecursiveData CandidateD2LstmOutput = (SeqDenseRecursiveData)cg.AddRunner(candidateD2LSTMRunner);

                SeqDenseBatchData CandidateSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD1LstmOutput, Behavior));
                SeqDenseBatchData CandidateSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(CandidateD2LstmOutput, Behavior));
                MemorySeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { CandidateSeqD1O, CandidateSeqD2O }, Behavior));
                MemoryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(MemorySeqO, Behavior));
            }
            //else
            //{
            //    CandidateMatchRunner CandidateMemRunner = new CandidateMatchRunner(CandidateAnswerData, ContextEmbedOutput, Behavior);
            //    cg.AddRunner(CandidateMemRunner);
            //    CandidateMatch = CandidateMemRunner.Output;
            //    CandidateSeqO = ContextEmbedOutput;
            //}

            SeqDenseBatchData AnsMemSeqO = null;
            if(BuilderParameters.SPAN_POOL == PoolingType.MAX)
                AnsMemSeqO = (SeqDenseBatchData)cg.AddRunner(new MaxPoolSpanRunner<SeqDenseBatchData>(MemorySeqO, StartPos, EndPos, LabelMatch, Behavior));
            else if(BuilderParameters.SPAN_POOL == PoolingType.LAST)
                AnsMemSeqO = (SeqDenseBatchData)cg.AddRunner(new LastPoolSpanRunner<SeqDenseBatchData>(MemorySeqO, StartPos, EndPos, LabelMatch, Behavior));
            BiMatchBatchData AnsMemoryMatch = LabelMatch;

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                    cg.AddRunner(new ModelDiskDumpRunner(model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\RecurrentAttention"));
            }

            ReasonNetRunner reasonRunner = new ReasonNetRunner(QueryOutput, BuilderParameters.RECURRENT_STEP,
                MemorySeqO, MemoryMatch, AttStruct,
                QuerySeqOutput, QueryMatch, Att2Struct,
                AnsMemSeqO, AnsMemoryMatch, AnsStruct, 
                StateStruct, TerminalStruct, BuilderParameters.ATT_Gamma, Behavior, Behavior.RunMode == DNNRunMode.Train ? string.Empty : BuilderParameters.DebugFile);
            cg.AddRunner(reasonRunner);

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new BayesianContrastiveRewardRunner(LabelMatch.MatchInfo, reasonRunner.AnsData, reasonRunner.AnswerProb,
                        LabelMatch, BuilderParameters.ANS_Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new ExactMatchPredictionRunner(reasonRunner.FinalAns, LabelMatch, DataPanel.ValidResults, 
                        Behavior, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }

        private int InitEmbedLayers(List<LayerStructure> embedLayers, DeviceType device)
        {
            int embedDim = DataPanel.WordDim;
            for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++)
            {
                embedLayers.Add(new LayerStructure(embedDim, BuilderParameters.EMBED_LAYER_DIM[i],
                    BuilderParameters.EMBED_ACTIVATION[i], N_Type.Convolution_layer, BuilderParameters.EMBED_LAYER_WIN[i],
                    BuilderParameters.EMBED_DROPOUT[i], false, device));
                embedDim = BuilderParameters.EMBED_LAYER_DIM[i];
            }

            #region Glove Work Embedding Initialization.

            if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
            {
                int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];

                int halfWinStart = BuilderParameters.EMBED_LAYER_WIN[0] / 2; // * DataPanel.WordDim;

                Logger.WriteLog("Init Glove Word Embedding ...");
                int hit = 0;
                using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding))
                {
                    while (!mreader.EndOfStream)
                    {
                        string[] items = mreader.ReadLine().Split(' ');
                        string word = items[0];
                        float[] vec = new float[items.Length - 1];
                        for (int i = 1; i < items.Length; i++)
                        {
                            vec[i - 1] = float.Parse(items[i]);
                        }

                        int wordIdx = DataPanel.wordFreqDict.IndexItem(word);
                        if (wordIdx >= 0)
                        {
                            int feaIdx = wordIdx;

                            for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                            {
                                for (int i = 0; i < wordDim; i++)
                                {
                                    embedLayers[0].weight.MemPtr[((win + halfWinStart) * DataPanel.WordDim + feaIdx) * wordDim + i] = vec[i];
                                }
                            }
                            hit++;
                        }
                        else
                        {
                            // init zero or use init random.
                        }
                    }
                }

                for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                {
                    for (int i = 0; i < wordDim; i++)
                    {
                        embedLayers[0].weight.MemPtr[((win + halfWinStart) * DataPanel.WordDim + 1) * wordDim + i] = 0;
                    }
                }

                embedLayers[0].weight.SyncFromCPU();
                Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.wordFreqDict.ItemDictSize);
            }
            #endregion.

            return embedDim;
        }
        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            // word embedding layer.
            List<LayerStructure> embedLayers = new List<LayerStructure>();

            //context lstm layer.
            GRUStructure contextD1LSTM = null;
            GRUStructure contextD2LSTM = null;

            //query lstm layer.
            GRUStructure queryD1LSTM = null;
            GRUStructure queryD2LSTM = null;

            //LayerStructure matchLayer = null;

            // gru for state transformation.
            GRUCell stateStruct = null;

            // MLP as doc memory attention.
            MLPAttentionStructure attStruct = null;

            // MLP as query memory attention.
            MLPAttentionStructure att2Struct = null;

            // attention as pos action.
            MLPAttentionStructure ansStruct = null;

            // Terminal gate module. 
            LayerStructure terminalStruct = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int embedDim = InitEmbedLayers(embedLayers, device);

                int conDim = embedDim;
                int queryDim = embedDim;
                {
                    contextD1LSTM = new GRUStructure(conDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    contextD2LSTM = new GRUStructure(conDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    conDim = 2 * BuilderParameters.GRU_DIM;
                }

                if (!BuilderParameters.IS_SHARE_RNN)
                {
                    queryD1LSTM = new GRUStructure(queryDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    queryD2LSTM = new GRUStructure(queryDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    queryDim = 2 * BuilderParameters.GRU_DIM;
                }
                else
                {
                    queryD1LSTM = contextD1LSTM;
                    queryD2LSTM = contextD2LSTM;
                    queryDim = conDim;
                }
                attStruct = new MLPAttentionStructure(queryDim, conDim, BuilderParameters.ATT_HID_DIM, device);
                att2Struct = new MLPAttentionStructure(queryDim, queryDim, BuilderParameters.ATT2_HID_DIM, device);
                stateStruct = new GRUCell(queryDim + conDim, queryDim, device, BuilderParameters.RndInit);

                if(BuilderParameters.SPAN_POOL == PoolingType.MAX)
                    ansStruct = new MLPAttentionStructure(queryDim, conDim, BuilderParameters.ANS_HID_DIM, device);
                else if(BuilderParameters.SPAN_POOL == PoolingType.LAST)
                    ansStruct = new MLPAttentionStructure(queryDim, 2 * conDim, BuilderParameters.ANS_HID_DIM, device);

                //ansStruct = new MLPAttentionStructure(queryDim, conDim, BuilderParameters.ANS_HID_DIM, device);
                terminalStruct = new LayerStructure(queryDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                //matchLayer = new LayerStructure(BuilderParameters.QUERY_LAYER_DIM.Last(), BuilderParameters.CON_LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true);

                if (BuilderParameters.IS_EMBED_UPDATE)
                {
                    for (int i = 0; i < embedLayers.Count; i++)
                        modelStructure.AddLayer(embedLayers[i]);
                }
                else
                {
                    for (int i = 0; i < embedLayers.Count; i++)
                        embedLayers[i].StructureOptimizer.Clear();
                }

                modelStructure.AddLayer(stateStruct);
                modelStructure.AddLayer(attStruct);
                modelStructure.AddLayer(att2Struct);
                modelStructure.AddLayer(ansStruct);
                modelStructure.AddLayer(terminalStruct);

                {
                    modelStructure.AddLayer(contextD1LSTM);
                    modelStructure.AddLayer(contextD2LSTM);
                }
                if (!BuilderParameters.IS_SHARE_RNN)
                {
                    modelStructure.AddLayer(queryD1LSTM);
                    modelStructure.AddLayer(queryD2LSTM);
                }
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, device);
                    int link = 0;

                    if (BuilderParameters.IS_EMBED_UPDATE)
                    {
                        for (int i = 0; i < BuilderParameters.EMBED_LAYER_DIM.Length; i++)
                            embedLayers.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                    }
                    else
                    {
                        InitEmbedLayers(embedLayers, device);
                    }
                    stateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                    attStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    att2Struct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    ansStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    terminalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];

                    {
                        contextD1LSTM = (GRUStructure)modelStructure.CompositeLinks[link++];
                        contextD1LSTM = (GRUStructure)modelStructure.CompositeLinks[link++];
                    }
                    if (!BuilderParameters.IS_SHARE_RNN)
                    {
                        queryD1LSTM = (GRUStructure)modelStructure.CompositeLinks[link++];
                        queryD2LSTM = (GRUStructure)modelStructure.CompositeLinks[link++];
                    }
                    else
                    {
                        queryD1LSTM = contextD1LSTM;
                        queryD2LSTM = contextD2LSTM;
                    }
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainStartPos, DataPanel.TrainEndPos, DataPanel.TrainLabelMatch, 
                        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                        stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, string.Empty);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    //ComputationGraph testCG = null;
                    //if (BuilderParameters.IsTestFile)
                    //    testCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                    //        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                    //        stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidStartPos, DataPanel.ValidEndPos, DataPanel.ValidLabelMatch,
                            embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                            stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, string.Empty);

                    double bestValidScore =double.MinValue; // validCG.Execute();
                    //double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                        //double testScore = 0;
                        double validScore = 0;
                        //if (testCG != null)
                        //{
                        //    testScore = testCG.Execute();
                        //    Logger.WriteLog("Test Score {0}, At iter {1}", testScore, bestIter);
                        //}
                        if (validCG != null)
                        {
                            validScore = validCG.Execute();
                            Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, iter);
                        }

                        if (validScore > bestValidScore)
                        {
                            bestValidScore = validScore; bestIter = iter;  
                        }
                        Logger.WriteLog("Best Valid Score {0},  At iter {1}", bestValidScore, bestIter);
                    }
                    break;
                case DNNRunMode.Predict:
                    //ComputationGraph predCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                    //        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                    //        stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");
                    //predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainStartPos = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainEndPos = null;
            public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> TrainLabelMatch = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQuery = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidStartPos = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidEndPos = null;
            public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> ValidLabelMatch = null;

            public static string[] ValidIds = null;
            public static List<HashSet<string>> ValidResults = new List<HashSet<string>>();

            public static ItemFreqIndexDictionary wordFreqDict = null;
            public static int WordDim { get { return wordFreqDict.ItemDictSize; } }

            static IEnumerable<Tuple<string, string, string>> ExtractQAS(string paraFile, string queryFile, string ansFile)
            {
                using (StreamReader paraReader = new StreamReader(paraFile))
                using (StreamReader queryReader = new StreamReader(queryFile))
                using (StreamReader ansReader = new StreamReader(ansFile))
                {
                    int lineIdx = 0;
                    while (!ansReader.EndOfStream)
                    {
                        string p = paraReader.ReadLine();
                        string q = queryReader.ReadLine();
                        string a = ansReader.ReadLine();

                        yield return new Tuple<string, string, string>(p, q, a);
                        lineIdx += 1;
                    }
                    Console.WriteLine("Total Number of answers {0}", lineIdx);
                }
            }
            static Tuple<int, int> GetSpan(List<string[]> sentenceIdxs, int startSent, int startIdx, int endSent, int endIdx)
            {
                if(startSent == -1 || startIdx == -1 || endSent == -1 || endIdx == -1)
                {
                    return new Tuple<int, int>(-1, -1);
                }
                if (sentenceIdxs.Count <= startSent || sentenceIdxs.Count <= endSent)
                {
                    Console.WriteLine("Sentence number is not enough! {0}, start {1}, end {2}", sentenceIdxs.Count, startSent, endSent);
                    Console.ReadLine();
                }

                int answerSentStart = sentenceIdxs.Take(startSent).Sum(i => i.Length) + startIdx;
                int answerSentEnd = sentenceIdxs.Take(endSent).Sum(i => i.Length) + endIdx - 1;

                if (startSent != endSent)
                {
                    Console.WriteLine("Sentence start and Sentence end! {0} and {1}, len \t{2}", startSent, endSent, answerSentEnd - answerSentStart + 1);
                }
                return new Tuple<int, int>(answerSentStart, answerSentEnd);
            }

            static int SpanSentIdx(int index, List<string[]> sentenceIdxs)
            {
                int spanSent = 0;
                for (int i = 0; i < sentenceIdxs.Count; i++)
                {
                    if(spanSent + sentenceIdxs[i].Length > index)
                    {
                        return i;
                    }
                    spanSent += sentenceIdxs[i].Length;
                }
                return sentenceIdxs.Count - 1;
            }
            static void ExtractCorpusBinary(string paraFile, string queryFile, string ansFile, string paraFileBin, string queryFileBin, string ansFileBin, bool isShuffle, int miniBatchSize,
                bool isFilter, List<HashSet<string>> resultSave)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                DenseBatchData ansStart = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);
                DenseBatchData ansEnd = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);
                BiMatchBatchData ansMatch = new BiMatchBatchData(new BiMatchBatchDataStat() { }, DeviceType.CPU);
                
                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(paraFileBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryFileBin);
                BinaryWriter ansStartWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".1");
                BinaryWriter ansEndWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".2");
                BinaryWriter ansMatchWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".match");

                IEnumerable<Tuple<string, string, string>> QAS = ExtractQAS(paraFile, queryFile, ansFile);

                IEnumerable<Tuple<string, string, string>> Input = QAS;
                if (isShuffle) Input = CommonExtractor.RandomShuffle<Tuple<string, string, string>>(QAS, 1000000, DeepNet.BuilderParameters.RandomSeed);

                Dictionary<int, int> answerLen = new Dictionary<int, int>();

                int lineIdx = 0;
                int unHitNum = 0;
                foreach (Tuple<string, string, string> smp in Input)
                {
                    string p = smp.Item1;
                    string q = smp.Item2;
                    string a = smp.Item3;

                    List<string[]> sentenceIdxs = p.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries).Select(i => i.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)).ToList();
                    string[] queryIdxs = q.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                    foreach (string[] term in sentenceIdxs) { foreach (string id in term) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(id)] = 1; contextFea.Add(tmp); } }
                    
                    List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                    foreach (string term in queryIdxs) { var tmp = new Dictionary<int, float>(); tmp[int.Parse(term)] = 1; queryFea.Add(tmp); }

                    int answerSentStart = -1;
                    int answerSentEnd = -1;
                    string[] poses = a.Split(new char[] { ',', ')', '(', '#' }, StringSplitOptions.RemoveEmptyEntries);

                    if (resultSave != null) { resultSave.Add(new HashSet<string>()); }
                    for (int m = 0; m < poses.Length / 4; m++)
                    {
                        int startSent = int.Parse(poses[4 * m + 0]);
                        int startIdx = int.Parse(poses[4 * m + 1]);
                        int endSent = int.Parse(poses[4 * m + 2]);
                        int endIdx = int.Parse(poses[4 * m + 3]);
                        Tuple<int, int> r = GetSpan(sentenceIdxs, startSent, startIdx, endSent, endIdx);
                        answerSentStart = r.Item1;
                        answerSentEnd = r.Item2;
                        if (resultSave != null) { resultSave[lineIdx].Add(string.Format("{0}#{1}", answerSentStart, answerSentEnd)); }
                    }
                    if (answerSentStart >= 0 && answerSentEnd >= 0 && answerSentEnd - answerSentStart + 1 > 0)
                    {
                        if(!answerLen.ContainsKey(answerSentEnd - answerSentStart + 1)) answerLen[answerSentEnd - answerSentStart + 1] = 0;
                        answerLen[answerSentEnd - answerSentStart + 1] += 1;
                    }

                    bool HitSpan = false;
                    List<float> spanStarts = new List<float>();
                    List<float> spanEnds = new List<float>();
                    List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
                    int docNum = sentenceIdxs.Select(s => s.Length).Sum();
                    for (int i = 0; i < docNum; i++)
                    {
                        for (int j = 0; j < BuilderParameters.SPAN_LENGTH; j++)
                        {
                            int spanStart = i;
                            int spanEnd = i + j;

                            if (spanEnd >= docNum || SpanSentIdx(spanStart, sentenceIdxs) != SpanSentIdx(spanEnd, sentenceIdxs))
                                continue;

                            if(answerSentStart == spanStart && answerSentEnd == spanEnd)
                            {
                                HitSpan = true;
                                matchList.Add(new Tuple<int, int, float>(query.BatchSize, ansMatch.MatchSize + spanEnds.Count, 1));
                            }
                            else
                            {
                                matchList.Add(new Tuple<int, int, float>(query.BatchSize, ansMatch.MatchSize + spanEnds.Count, 0));
                            }

                            spanStarts.Add(spanStart);
                            spanEnds.Add(spanEnd);
                        }
                    }

                    if (!HitSpan) { unHitNum += 1; if (isFilter) continue; }

                    context.PushSample(contextFea);
                    query.PushSample(queryFea);
                    ansStart.PushSample(spanStarts.ToArray(), spanStarts.Count);
                    ansEnd.PushSample(spanEnds.ToArray(), spanEnds.Count);
                    ansMatch.PushMatch(matchList);

                    if (context.BatchSize >= miniBatchSize)
                    {
                        context.PopBatchToStat(contextWriter);
                        query.PopBatchToStat(queryWriter);
                        ansStart.PopBatchToStat(ansStartWriter);
                        ansEnd.PopBatchToStat(ansEndWriter);
                        ansMatch.PopBatchToStat(ansMatchWriter);
                    }

                    if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                }
                context.PopBatchCompleteStat(contextWriter);
                query.PopBatchCompleteStat(queryWriter);
                ansStart.PopBatchCompleteStat(ansStartWriter);
                ansEnd.PopBatchCompleteStat(ansEndWriter);
                ansMatch.PopBatchCompleteStat(ansMatchWriter);

                if (resultSave != null)
                {
                    using (StreamWriter ansWriter = new StreamWriter(ansFile + ".index"))
                    {
                        foreach (HashSet<string> results in resultSave)
                        {
                            ansWriter.WriteLine(string.Join("\t", results.ToArray()));
                        }
                    }
                }

                Console.WriteLine("Context Stat {0}", context.Stat.ToString());
                Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                Console.WriteLine("Answer Start Stat {0}", ansStart.Stat.ToString());
                Console.WriteLine("Answer End Stat {0}", ansEnd.Stat.ToString());

                int sum = answerLen.Select(i => i.Value).Sum();
                foreach (KeyValuePair<int, int> anstat in answerLen.OrderBy(i => i.Key))
                {
                    Console.WriteLine("Answer Len\t{0}\t, num\t{1}\t, precentage \t{2}", anstat.Key, anstat.Value, anstat.Value * 1.0 / sum);
                }
                Console.WriteLine("UnHit Num {0}", unHitNum);
            }

            public static void Init()
            {
                #region Preprocess Data.
                // Step 1 : Extract Vocab from training Corpus.
                using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
                {
                    wordFreqDict = new ItemFreqIndexDictionary(mreader, false);
                }

                /// Step 3 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile && 
                    !File.Exists(BuilderParameters.TrainQueryBin) &&
                    !File.Exists(BuilderParameters.TrainContextBin) &&
                    !FileUtil.IsFileinUse(new FileInfo(BuilderParameters.TrainQueryBin)) &&
                    !FileUtil.IsFileinUse(new FileInfo(BuilderParameters.TrainContextBin)))
                {
                    ExtractCorpusBinary(
                        BuilderParameters.TrainContext, BuilderParameters.TrainQuery, BuilderParameters.TrainPos,
                        BuilderParameters.TrainContextBin, BuilderParameters.TrainQueryBin, BuilderParameters.TrainPosBin,
                        true, BuilderParameters.MiniBatchSize, true, null);
                }

                if (BuilderParameters.IsValidFile &&
                    !File.Exists(BuilderParameters.ValidQueryBin) &&
                    !File.Exists(BuilderParameters.ValidContextBin) &&
                    !FileUtil.IsFileinUse(new FileInfo(BuilderParameters.ValidQueryBin)) &&
                    !FileUtil.IsFileinUse(new FileInfo(BuilderParameters.ValidContextBin)))
                {
                    ExtractCorpusBinary(
                        BuilderParameters.ValidContext, BuilderParameters.ValidQuery, BuilderParameters.ValidPos,
                        BuilderParameters.ValidContextBin, BuilderParameters.ValidQueryBin, BuilderParameters.ValidPosBin,
                        false, BuilderParameters.MiniBatchSize, false, ValidResults);
                }
                else
                {
                    using (StreamReader ansReader = new StreamReader(BuilderParameters.ValidPos + ".index"))
                    {
                        while (!ansReader.EndOfStream)
                        {
                            string[] items = ansReader.ReadLine().Split('\t');
                            ValidResults.Add(new HashSet<string>(items));
                        }
                    }
                }
                ValidIds = File.ReadAllLines(BuilderParameters.ValidIds);
                #endregion.

                #region Load Data.
                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin);
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin);
                    TrainStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".1");
                    TrainEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".2");
                    TrainLabelMatch = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>(BuilderParameters.TrainPosBin + ".match");

                    TrainContext.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainStartPos.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainEndPos.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainLabelMatch.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin);
                    ValidQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin);
                    ValidStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".1");
                    ValidEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".2");
                    ValidLabelMatch = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>(BuilderParameters.ValidPosBin + ".match");

                    ValidContext.InitThreadSafePipelineCashier(100, false);
                    ValidQuery.InitThreadSafePipelineCashier(100, false);
                    ValidStartPos.InitThreadSafePipelineCashier(100, false);
                    ValidEndPos.InitThreadSafePipelineCashier(100, false);
                    ValidLabelMatch.InitThreadSafePipelineCashier(100, false);
                }
                #endregion.
            }
        }
    }
}
