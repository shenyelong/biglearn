﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;

namespace BigLearn.DeepNet
{
    //public class MazeLanguageBuilder : Builder
    //{
    //    class BuilderParameters : BaseModelArgument<BuilderParameters>
    //    {
    //        static BuilderParameters()
    //        {   
    //        }
    //    }

    //    public override BuilderType Type { get { return BuilderType.APP_MAZE_LANGUAGE; } }

    //    public override void InitStartup(string fileName)
    //    {
    //        BuilderParameters.Parse(fileName);
    //    }


    //    public override void Rock()
    //    {
    //        Logger.OpenLog(BuilderParameters.LogFile);
    //        Logger.WriteLog("Loading Training/Validation Data.");
    //        DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
    //        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
    //        DataPanel.Init();
    //        Logger.WriteLog("Load Data Finished.");
    //        Console.SetWindowSize(
    //            Math.Min(150, Console.LargestWindowWidth),
    //            Math.Min(60, Console.LargestWindowHeight));


    //        //RandomActor actor = new RandomActor();
    //        Actor actor = new NeuralEpisodicActor(new RunnerBehavior()
    //        { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }); //MonteCarloActor(); //RandomActor(); // 
    //        int monte_carlo_search = 100;

    //        for (int iter = 0; iter < 10; iter++)
    //        {
    //            Console.WriteLine("Iteration {0}", iter);
    //            List<int> avgMonteCarlo = new List<int>();
    //            List<int> avgSearchDepth = new List<int>();
    //            foreach (SymbolicState q in DataPanel.Query)
    //            {
    //                // maximum recurrent step;
    //                bool IsSuccess = false;
    //                int epoch = 0;
    //                int successDepth = -1;
    //                for (epoch = 0; epoch < monte_carlo_search; epoch++)
    //                {
    //                    SymbolicState state = q;
    //                    List<SymbolicState> path = new List<SymbolicState>();
    //                    successDepth = -1;
    //                    IsSuccess = false;
    //                    for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
    //                    {
    //                        state = actor.Action(state);
    //                        path.Add(state);
    //                        IsSuccess = state.IsSuccess;
    //                        if (IsSuccess) { break; }
    //                    }

    //                    int reward = 0;
    //                    if (IsSuccess) { reward = 1; }

    //                    actor.Feedback(path, reward);
    //                    ((NeuralEpisodicActor)actor).PushReplay(path, reward);
    //                    if (IsSuccess)
    //                    {
    //                        successDepth = path.Count;
    //                        break;
    //                    }
    //                }

    //                if (!IsSuccess) avgMonteCarlo.Add(-1);
    //                else avgMonteCarlo.Add(epoch);

    //                if (!IsSuccess) avgSearchDepth.Add(-1);
    //                else avgSearchDepth.Add(successDepth);

    //                int notComplete = avgMonteCarlo.Select(i => i == -1 ? 1 : 0).Sum();
    //                int Complete = avgMonteCarlo.Select(i => i > -1 ? 1 : 0).Sum();
    //                int totalLen = avgMonteCarlo.Select(i => i > -1 ? i : 0).Sum();

    //                int totalSearchDepth = avgSearchDepth.Select(i => i > -1 ? i : 0).Sum();

    //                Console.WriteLine("{0} Not Completed, {1} Completed, {2} AvgSearchLen, {3} AvgSearchDepth, {4} CurrentSearchEpoch, {5} Depth",
    //                    notComplete, Complete, totalLen / (Complete + 0.0001f), totalSearchDepth / (Complete + 0.0001f), epoch, successDepth);
    //            }
    //        }
    //        Logger.CloseLog();
    //    }

    //    public class SymbolicState
    //    {
    //        public int A;
    //        public int a = 0;
    //        public int B;
    //        public int b = 0;
    //        public int C;
    //        public int c = 0;
    //        public int Q;
    //        public int Step = 0;

    //        public string QueryKey { get { return string.Format("{0}\t{1}\t{2}\t{3}", Q, A, B, C); } }
    //        public string StateKey { get { return string.Format("{0}#{1}#{2}#{3}\t{4}\t{5}\t{6}", Q, A, B, C, a, b, c); } }

    //        public static int MaxNeighborNum { get { return 12; } }

    //        public static int FeaDim = 7;
    //        public float[] Fea { get { return new float[] { Q, A, a, B, b, C, c }; } }

    //        public static int Fea2Dim = 3;
    //        public float[] Fea2 { get { return new float[] { a == Q ? 1 : 0, b == Q ? 1 : 0, c == Q ? 1 : 0 }; } }


    //        public SymbolicState() { }

    //        public SymbolicState(string mkey)
    //        {
    //            string[] items = mkey.Split('\t');
    //            Q = int.Parse(items[0]);
    //            A = int.Parse(items[1]);
    //            B = int.Parse(items[2]);
    //            C = int.Parse(items[3]);
    //        }

    //        public SymbolicState(Random random, HashSet<string> keys)
    //        {
    //            while (true)
    //            {
    //                Q = random.Next(1, 20);
    //                A = random.Next(1, 20);
    //                B = random.Next(1, 20);
    //                C = random.Next(1, 20);
    //                if (keys.Contains(this.QueryKey)) continue;
    //                if (Q == A || Q == B || Q == C) continue;
    //                if (Q > A && Q > B && Q > C) continue;
    //                Queue<SymbolicState> stacks = new Queue<SymbolicState>();
    //                stacks.Enqueue(this);

    //                while (stacks.Count > 0)
    //                {
    //                    SymbolicState s = stacks.Dequeue();
    //                    if (s.IsSuccess) { return; }
    //                    if (s.Step >= 5) break;
    //                    List<SymbolicState> neighbors = s.NeighborStates();
    //                    foreach (SymbolicState ns in neighbors) stacks.Enqueue(ns);
    //                }
    //            }
    //        }

    //        public SymbolicState Transform(int aa, int bb, int cc, int s)
    //        {
    //            return new SymbolicState() { A = A, B = B, C = C, a = aa, b = bb, c = cc, Q = Q, Step = s + 1 };
    //        }

    //        public bool IsSuccess { get { if (Q == a || Q == b || Q == c) return true; return false; } }

    //        public List<SymbolicState> NeighborStates()
    //        {
    //            List<SymbolicState> results = new List<SymbolicState>();

    //            // A -> Full;
    //            // A -> empty;
    //            // A -> B;
    //            // A -> C;
    //            if (a < A) { results.Add(Transform(A, b, c, Step)); }
    //            if (a > 0) { results.Add(Transform(0, b, c, Step)); }
    //            if (a > 0 && b < B) { if (a + b >= B) results.Add(Transform(a + b - B, B, c, Step)); else results.Add(Transform(0, a + b, c, Step)); }
    //            if (a > 0 && c < C) { if (a + c >= C) results.Add(Transform(a + c - C, b, C, Step)); else results.Add(Transform(0, b, a + c, Step)); }

    //            // B -> Full;
    //            // B -> empty;
    //            // B -> A;
    //            // B -> C;
    //            if (b < B) { results.Add(Transform(a, B, c, Step)); }
    //            if (b > 0) { results.Add(Transform(a, 0, c, Step)); }
    //            if (b > 0 && a < A) { if (a + b >= A) results.Add(Transform(A, a + b - A, c, Step)); else results.Add(Transform(a + b, 0, c, Step)); }
    //            if (b > 0 && c < C) { if (b + c >= C) results.Add(Transform(a, b + c - C, C, Step)); else results.Add(Transform(a, 0, b + c, Step)); }

    //            // C -> Full;
    //            // C -> empty;
    //            // C -> A;
    //            // C -> B;
    //            if (c < C) { results.Add(Transform(a, b, C, Step)); }
    //            if (c > 0) { results.Add(Transform(a, b, 0, Step)); }
    //            if (c > 0 && a < A) { if (a + c >= A) results.Add(Transform(A, b, a + c - A, Step)); else results.Add(Transform(a + c, b, 0, Step)); }
    //            if (c > 0 && b < B) { if (b + c >= B) results.Add(Transform(a, B, b + c - C, Step)); else results.Add(Transform(a, b + c, 0, Step)); }

    //            // doing nothing;
    //            //results.Add(Transform(a, b, c, Step));

    //            return results;
    //        }
    //    }

    //    public class SymbolicStateRunner : StructRunner
    //    {
    //        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } protected set { base.Output = value; } }
    //        List<SymbolicState> Queries = null;
    //        DNNStructure DNN;
    //        DenseBatchData XVec;
    //        DNNRunner<HiddenBatchData> Runner = null;
    //        public SymbolicStateRunner(List<SymbolicState> input, int maxBatchSize, DNNStructure dnn, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Queries = input;
    //            XVec = new DenseBatchData(maxBatchSize, SymbolicState.FeaDim, behavior.Device);
    //            DNN = dnn;
    //            Runner = new DNNRunner<HiddenBatchData>(DNN, new HiddenBatchData(maxBatchSize, XVec.Stat.Dim, XVec.Data, null, Behavior.Device), Behavior);
    //            Output = Runner.Output;
    //        }

    //        unsafe public override void Forward()
    //        {
    //            Output.BatchSize = Queries.Count;
    //            for (int i = 0; i < Queries.Count; i++)
    //            {
    //                Array.Copy(Queries[i].Fea, 0, XVec.Data.MemPtr, i * SymbolicState.FeaDim, SymbolicState.FeaDim);
    //            }
    //            XVec.Data.SyncFromCPU();
    //            Runner.Forward();
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            Runner.Backward(cleanDeriv);
    //        }

    //        public override void CleanDeriv()
    //        {
    //            Runner.CleanDeriv();
    //        }

    //        public override void Update()
    //        {
    //            Runner.Update();
    //        }
    //    }


    //    public class SymbolicDataRunner : StructRunner
    //    {
    //        bool IsRandom = false;
    //        int BatchSize = 0;
    //        int CurrentIdx = 0;
    //        //GameDataCashier Interface { get; set; }
    //        //public new DenseBatchData Output { get { return (DenseBatchData)base.Output; } protected set { base.Output = value; } }
    //        public List<SymbolicState> OutQueries = new List<SymbolicState>();
    //        List<SymbolicState> Queries = new List<SymbolicState>();

    //        public SymbolicDataRunner(int batchSize, string filePath, bool isRandom, RunnerBehavior behavior)
    //            : base(Structure.Empty, behavior)
    //        {
    //            IsRandom = isRandom;
    //            BatchSize = batchSize;
    //            //Output = new DenseBatchData(BatchSize, SymbolicState.StateDim, behavior.Device);
    //            IsUpdate = false;
    //            IsBackProp = false;

    //            using (StreamReader mreader = new StreamReader(filePath))
    //            {
    //                while (!mreader.EndOfStream)
    //                {
    //                    string[] items = mreader.ReadLine().Split('\t');
    //                    int q = int.Parse(items[0]);
    //                    int a = int.Parse(items[1]);
    //                    int b = int.Parse(items[2]);
    //                    int c = int.Parse(items[3]);
    //                    SymbolicState s = new SymbolicState() { Q = q, A = a, B = b, C = c };
    //                    Queries.Add(s);
    //                }
    //            }
    //            CurrentIdx = 0;
    //            if (IsRandom) Queries = CommonExtractor.RandomShuffle<SymbolicState>(Queries, 10000, 13).ToList();
    //        }

    //        unsafe public override void Forward()
    //        {
    //            OutQueries.Clear();
    //            for (int i = 0; i < BatchSize; i++)
    //            {
    //                OutQueries.Add(Queries[CurrentIdx]);
    //                //Output.Data.MemPtr[i * SymbolicState.StateDim + 0] = Queries[CurrentIdx].Q;
    //                //Output.Data.MemPtr[i * SymbolicState.StateDim + 1] = Queries[CurrentIdx].A;
    //                //Output.Data.MemPtr[i * SymbolicState.StateDim + 2] = Queries[CurrentIdx].a;
    //                //Output.Data.MemPtr[i * SymbolicState.StateDim + 3] = Queries[CurrentIdx].B;
    //                //Output.Data.MemPtr[i * SymbolicState.StateDim + 4] = Queries[CurrentIdx].b;
    //                //Output.Data.MemPtr[i * SymbolicState.StateDim + 5] = Queries[CurrentIdx].C;
    //                //Output.Data.MemPtr[i * SymbolicState.StateDim + 6] = Queries[CurrentIdx].c;
    //                CurrentIdx++;
    //                if (CurrentIdx == Queries.Count)
    //                {
    //                    if (IsRandom) Queries = CommonExtractor.RandomShuffle<SymbolicState>(Queries, 10000, 13).ToList();
    //                    CurrentIdx = 0;
    //                }
    //            }
    //            //Output.BatchSize = BatchSize;
    //            //Output.Data.SyncFromCPU();
    //        }
    //    }

    //    class TwoWayEnsembleRunner : StructRunner
    //    {
    //        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
    //        SeqDenseBatchData AData { get; set; }
    //        HiddenBatchData CData { get; set; }
    //        CudaPieceInt EnsembleMask2;
    //        CudaPieceFloat tmpOutput;

    //        /// <summary>
    //        /// output = [AData, AData * CData]
    //        /// </summary>
    //        /// <param name="aData"></param>
    //        /// <param name="cData"></param>
    //        /// <param name="behavior"></param>
    //        public TwoWayEnsembleRunner(SeqDenseBatchData aData, HiddenBatchData cData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            AData = aData;
    //            CData = cData;
    //            if (AData.Dim != CData.Dim) { throw new Exception(string.Format("dimension adata {0} is not the same as cdata {1}", AData.Dim, CData.Dim)); }
    //            Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 2, Behavior.Device);
    //            tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);
    //            EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
    //            for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 1 + (s * 2);
    //            EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
    //        }

    //        public override void Forward()
    //        {
    //            Output.BatchSize = AData.BatchSize;
    //            Output.SentSize = AData.SentSize;
    //            ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
    //                                         AData.SentOutput, 0, AData.Dim,
    //                                         Output.SentOutput, 0, Output.Dim,
    //                                         AData.Dim, AData.SentSize, 1, 0, 0);
    //            ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
    //                                              CudaPieceInt.Empty, 0, AData.SentMargin, 0, EnsembleMask2, 0,
    //                                              AData.SentSize, AData.Dim, 0, 1);
    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
    //                                              EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
    //                                              Output.SentSize, AData.Dim, 0, 1);

    //            ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
    //                                        Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);

    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
    //                                          EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
    //                                          Output.SentSize, AData.Dim, 1, 1);

    //            ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
    //                                         AData.SentDeriv, 0, AData.Dim,
    //                                         AData.SentDeriv, 0, AData.Dim,
    //                                         AData.Dim, AData.SentSize, 1, 0, 1);
    //        }
    //    }

    //    public class StateActorRunner : StructRunner
    //    {
    //        List<SymbolicState> Queries;
    //        HiddenBatchData InputState;

    //        DNNStructure TgtDNN;
    //        DenseBatchData TgtVec;
    //        SeqDenseBatchData TgtOut;
    //        DNNRunner<HiddenBatchData> TgtRunner = null;
    //        CudaPieceInt SmpIdx;
    //        CudaPieceInt SentMargin;
    //        TwoWayEnsembleRunner EnsembleRunner = null;
    //        SeqDenseBatchData CombineOutput = null;

    //        DNNStructure ScoreDNN;
    //        DNNRunner<HiddenBatchData> ScoreRunner = null;
    //        HiddenBatchData ScoreData = null;

    //        public List<SymbolicState> Answer;

    //        int Step = 0;
    //        public StateActorRunner(List<SymbolicState> input, HiddenBatchData inputState, DNNStructure tgtDNN, DNNStructure scoreDNN, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Queries = input;
    //            InputState = inputState;
    //            TgtDNN = tgtDNN;
    //            ScoreDNN = scoreDNN;

    //            TgtVec = new DenseBatchData(inputState.MAX_BATCHSIZE * SymbolicState.MaxNeighborNum, SymbolicState.FeaDim, Behavior.Device);
    //            TgtRunner = new DNNRunner<HiddenBatchData>(TgtDNN, new HiddenBatchData(TgtVec.Stat.MAX_BATCHSIZE, TgtVec.Stat.Dim, TgtVec.Data, null, Behavior.Device), Behavior);

    //            SmpIdx = new CudaPieceInt(inputState.MAX_BATCHSIZE, Behavior.Device);
    //            SentMargin = new CudaPieceInt(inputState.MAX_BATCHSIZE * SymbolicState.MaxNeighborNum, Behavior.Device);
    //            TgtOut = new SeqDenseBatchData(
    //                new SequenceDataStat(inputState.MAX_BATCHSIZE, SymbolicState.MaxNeighborNum * inputState.MAX_BATCHSIZE, TgtRunner.Output.Dim),
    //                SmpIdx, SentMargin, TgtRunner.Output.Output.Data, TgtRunner.Output.Deriv.Data, Behavior.Device);

    //            EnsembleRunner = new TwoWayEnsembleRunner(TgtOut, InputState, Behavior);
    //            CombineOutput = EnsembleRunner.Output;

    //            ScoreRunner = new DNNRunner<HiddenBatchData>(ScoreDNN,
    //                new HiddenBatchData(CombineOutput.MAX_SENTSIZE, CombineOutput.Dim, CombineOutput.SentOutput, CombineOutput.SentDeriv, Behavior.Device), Behavior);
    //            ScoreData = ScoreRunner.Output;

    //            Answer = new List<SymbolicState>();
    //        }

    //        unsafe public override void Forward()
    //        {
    //            Answer.Clear();

    //            List<List<SymbolicState>> candidates = new List<List<SymbolicState>>();
    //            foreach (SymbolicState s in Queries)
    //            {
    //                candidates.Add(s.NeighborStates());
    //            }

    //            TgtVec.BatchSize = candidates.Select(i => i.Count).Sum();
    //            TgtOut.BatchSize = Queries.Count;

    //            int idx = 0;
    //            for (int i = 0; i < Queries.Count; i++)
    //            {
    //                SmpIdx.MemPtr[i] = (i == 0 ? 0 : SmpIdx.MemPtr[i - 1]) + candidates[i].Count;
    //                foreach (SymbolicState s in candidates[i])
    //                {
    //                    Array.Copy(s.Fea, 0, TgtVec.Data.MemPtr, idx * SymbolicState.FeaDim, SymbolicState.FeaDim);
    //                    SentMargin.MemPtr[idx] = i;
    //                    idx += 1;
    //                }
    //            }
    //            SmpIdx.SyncFromCPU();
    //            SentMargin.SyncFromCPU();
    //            TgtVec.Data.SyncFromCPU();

    //            TgtRunner.Forward();
    //            EnsembleRunner.Forward();
    //            ScoreRunner.Forward();

    //            ScoreData.Output.Data.SyncToCPU();
    //            idx = 0;
    //            for (int i = 0; i < Queries.Count; i++)
    //            {
    //                float[] a = new float[candidates[i].Count];
    //                for (int t = 0; t < candidates[i].Count; t++)
    //                {
    //                    a[t] = ScoreData.Output.Data.MemPtr[idx];
    //                    idx += 1;
    //                }
    //                a = Util.Softmax(a, 1);
    //                int action = Util.Sample(a, ParameterSetting.Random);

    //                float epsilon = BuilderParameters.Epsilon(Step);

    //                double mp = ParameterSetting.Random.NextDouble();
    //                if (mp >= epsilon)
    //                {
    //                    action = Util.MaximumValue(a);
    //                }
    //                else
    //                {
    //                    action = ParameterSetting.Random.Next(candidates[i].Count);
    //                }
    //                Answer.Add(candidates[i][action]);
    //            }
    //            Step += 1;
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            ScoreRunner.Backward(cleanDeriv);
    //            EnsembleRunner.Backward(cleanDeriv);
    //            TgtRunner.Backward(cleanDeriv);
    //        }

    //        public override void CleanDeriv()
    //        {
    //            TgtRunner.CleanDeriv();
    //            EnsembleRunner.CleanDeriv();
    //            ScoreRunner.CleanDeriv();
    //        }

    //        public override void Update()
    //        {
    //            TgtRunner.Update();
    //            EnsembleRunner.Update();
    //            ScoreRunner.Update();
    //        }
    //    }


    //    /// <summary>
    //    /// Data Panel.
    //    /// </summary>
    //    public class DataPanel
    //    {
    //        public static List<SymbolicState> Query = new List<SymbolicState>();
    //        public static void RandomQuery(int number)
    //        {
    //            Random random = new Random();
    //            HashSet<string> keys = new HashSet<string>();

    //            for (int i = 0; i < number; i++)
    //            {
    //                SymbolicState s = new SymbolicState(random, keys);
    //                Query.Add(s);
    //                if (i % 100 == 0) { Console.WriteLine(string.Format("Search Examples {0}", i)); }
    //                keys.Add(s.QueryKey);
    //            }

    //            using (StreamWriter mwriter = new StreamWriter(BuilderParameters.QUERY_PATH))
    //            {
    //                foreach (SymbolicState s in Query)
    //                {
    //                    mwriter.WriteLine(s.QueryKey);
    //                }
    //                mwriter.Close();
    //            }
    //        }

    //        public static void LoadQuery()
    //        {
    //            using (StreamReader mreader = new StreamReader(BuilderParameters.QUERY_PATH))
    //            {
    //                while (!mreader.EndOfStream)
    //                {
    //                    SymbolicState s = new SymbolicState(mreader.ReadLine().Trim());
    //                    Query.Add(s);
    //                }
    //            }
    //        }

    //        public static void Init()
    //        {
    //            if (!File.Exists(BuilderParameters.QUERY_PATH)) RandomQuery(BuilderParameters.QUERY_SET);

    //            LoadQuery();
    //        }
    //    }
    //}
}
