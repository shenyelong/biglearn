﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
namespace BigLearn.DeepNet
{
    //public class AppReasoNetReaderSQuADV20Builder : Builder
    //{
    //    public class BuilderParameters : BaseModelArgument<BuilderParameters>
    //    {
    //        static BuilderParameters()
    //        {
    //            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
    //            Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));
    //            Argument.Add("DEV-VOCAB", new ParameterArgument(string.Empty, "Dev vocab Data."));
    //            Argument.Add("CHAR-VOCAB", new ParameterArgument(string.Empty, "Char vocab Data."));
    //            Argument.Add("CHAR-FREQ", new ParameterArgument("5", "Char Freq Threshold"));
    //            Argument.Add("IS-UTF8-VOCAB", new ParameterArgument("1", "UTF-8 format vocab"));
    //            Argument.Add("IS-FIX-VOCAB", new ParameterArgument("0", "load predefined l3g and char vocab."));

    //            Argument.Add("EVAL-PATH", new ParameterArgument(@"\PublicDataSource\SQuADV3\", "Evaluation folder;"));

    //            Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Training Folder"));
    //            Argument.Add("TEST", new ParameterArgument(string.Empty, "Testing Folder"));
    //            Argument.Add("VALID", new ParameterArgument(string.Empty, "Validation Folder"));

    //            Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));
    //            Argument.Add("DEV-MINI-BATCH", new ParameterArgument("4", "Dev Mini Batch Size"));

    //            Argument.Add("IS-CHAR-CASE", new ParameterArgument("1", "Is consider char case? Default(1)"));
    //            Argument.Add("IS-WORD-CASE", new ParameterArgument("1", "Is consider word case? Default(0)"));

    //            Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300", "DNN Layer Dim"));
    //            Argument.Add("EMBED-LAYER-AF", new ParameterArgument("0", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
    //            Argument.Add("WORD-DROPOUT", new ParameterArgument("0", "Dropout for Word Embedding 1."));
    //            Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding 1 File"));
    //            Argument.Add("UNK-INIT", new ParameterArgument("0", "0 : zero initialize; 1 : random initialize"));

    //            Argument.Add("CHAR-EMBED-DIM", new ParameterArgument("0", "Char Embedding Dim."));
    //            Argument.Add("CHAR-EMBED-WIN", new ParameterArgument("5", "Char Embedding Win."));

    //            Argument.Add("HIGHWAY-LAYER", new ParameterArgument("2", "Highway Layers."));
    //            Argument.Add("HIGHWAY-SHARE", new ParameterArgument("0", "0: Nonshare highway; 1 : share highway;"));

    //            Argument.Add("LSTM-DIM", new ParameterArgument("256,256", "LSTM Dimension."));


    //            Argument.Add("MEMORY-ENSEMBLE", new ParameterArgument("2", "2 : two-way ensemble; 3 : three-way ensemble;"));

    //            Argument.Add("SOFT1-DROPOUT", new ParameterArgument("0", "Soft 1 Dropout;"));
    //            Argument.Add("SOFT2-DROPOUT", new ParameterArgument("0", "Soft 2 Dropout;"));
    //            Argument.Add("LSTM-DROPOUT", new ParameterArgument("0", "LSTM Embed Dropout;"));
    //            Argument.Add("EMBED-DROPOUT", new ParameterArgument("0", "Embeding Dropout;"));
    //            Argument.Add("COATT-DROPOUT", new ParameterArgument("0", "CoATT Dropout;"));
    //            Argument.Add("CNN-DROPOUT", new ParameterArgument("0", "CNN Dropout;"));
    //            Argument.Add("L3G-DROPOUT", new ParameterArgument("0", "L3G DNN Dropout;"));
    //            Argument.Add("CHAR-DROPOUT", new ParameterArgument("0", "Char CNN Dropout;"));

    //            Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
    //            Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

    //            Argument.Add("ANS-HID", new ParameterArgument("256", "Answer Hidden Dimension"));
    //            Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
    //            Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

    //            Argument.Add("SELF-ATT-HID", new ParameterArgument("128", "Attention Hidden Dimension"));
    //            Argument.Add("SELF-ATT-TYPE", new ParameterArgument(((int)CrossSimType.Addition).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
    //            Argument.Add("SELF-ATT-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Attention Af : " + ParameterUtil.EnumValues(typeof(A_Func))));

    //            Argument.Add("RECURRENT-DIM", new ParameterArgument("128", "Recurrent state dimension,"));
    //            Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
    //            Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.RL)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));
    //            Argument.Add("QUERY-POOL", new ParameterArgument((((int)PoolingType.LAST)).ToString(), "Query Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));


    //            // Softmax Randomup.
    //            Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

    //            Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
    //            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
    //            Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

    //            Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
    //            Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

    //            Argument.Add("RL-ALL", new ParameterArgument("1", "RL All"));
    //            Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
    //            Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
    //            Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));

    //            Argument.Add("SPAN-OBJECTIVE", new ParameterArgument("0", "Span Objective; 0 : start-end detection; 1 : span start-end detection."));
    //            Argument.Add("SPAN-LENGTH", new ParameterArgument("0", "Span length"));
    //            Argument.Add("TRAIN-SPAN-LENGTH", new ParameterArgument("10000", "Span length"));
    //            Argument.Add("MAX-D-LENGTH", new ParameterArgument("0", "Maximum Document length"));

    //            Argument.Add("ENDPOINT-CONFIG", new ParameterArgument("0", "0: old M2; 1: new M2;"));
    //            Argument.Add("STARTPOINT-CONFIG", new ParameterArgument("0", "0: old M1; 1: new M1; 2: all new M1;"));

    //            //TERMINATE-DISCOUNT
    //            Argument.Add("TERMINATE-DISCOUNT", new ParameterArgument("0.1", "Reward discount on terminate gate;"));
    //        }

    //        /// <summary>
    //        /// DNN Run Mode.
    //        /// </summary>
    //        public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

    //        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
    //        public static string Vocab { get { return Argument["VOCAB"].Value; } }
    //        public static string DEV_Vocab { get { return Argument["DEV-VOCAB"].Value; } }
    //        public static string CHAR_Vocab { get { return Argument["CHAR-VOCAB"].Value; } }

    //        public static int CHAR_FREQ { get { return int.Parse(Argument["CHAR-FREQ"].Value); } }
    //        public static bool IS_UTF8_VOCAB { get { return int.Parse(Argument["IS-UTF8-VOCAB"].Value) > 0; } }
    //        public static bool IS_FIX_VOCAB { get { return int.Parse(Argument["IS-FIX-VOCAB"].Value) > 0; } }
    //        public static bool IS_CHAR_CASE { get { return int.Parse(Argument["IS-CHAR-CASE"].Value) > 0; } }

    //        public static string EVAL_PATH { get { return Argument["EVAL-PATH"].Value; } }

    //        #region train, test, validation dataset.
    //        public static string Train { get { return Argument["TRAIN"].Value.TrimEnd(new char[] { '\\', '/' }); } }
    //        public static bool IsTrainFile { get { return (!Train.Equals(string.Empty)); } }
    //        public static string TrainContext { get { return string.Format("{0}.para.idx", Train); } }
    //        public static string TrainQuery { get { return string.Format("{0}.ques.idx", Train); } }
    //        public static string TrainPos { get { return string.Format("{0}.pos", Train); } }

    //        public static string TrainContextBin { get { return string.Format("{0}.{1}.bin", TrainContext, MiniBatchSize); } }
    //        public static string TrainQueryBin { get { return string.Format("{0}.{1}.bin", TrainQuery, MiniBatchSize); } }
    //        public static string TrainPosBin { get { return string.Format("{0}.{1}.bin", TrainPos, MiniBatchSize); } }

    //        public static string TestFolder { get { return Argument["TEST"].Value.TrimEnd(new char[] { '\\', '/' }); } }
    //        public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }

    //        public static string Valid { get { return Argument["VALID"].Value.TrimEnd(new char[] { '\\', '/' }); } }
    //        public static bool IsValidFile { get { return (!Valid.Equals(string.Empty)); } }
    //        public static string ValidContext { get { return string.Format("{0}.para.idx", Valid); } }
    //        public static string ValidQuery { get { return string.Format("{0}.ques.idx", Valid); } }
    //        public static string ValidPos { get { return string.Format("{0}.pos", Valid); } }
    //        public static string ValidIds { get { return string.Format("{0}.ids", Valid); } }
    //        public static string ValidSpan { get { return string.Format("{0}.spans", Valid); } }

    //        public static string ValidContextBin { get { return string.Format("tmp.para.{0}.bin", MiniBatchSize); } }
    //        public static string ValidQueryBin { get { return string.Format("tmp.ques.{0}.bin", MiniBatchSize); } }
    //        public static string ValidPosBin { get { return string.Format("tmp.pos.{0}.bin", MiniBatchSize); } }
    //        #endregion.

    //        public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
    //        public static int DevMiniBatchSize { get { return int.Parse(Argument["DEV-MINI-BATCH"].Value); } }

    //        public static bool IS_WORD_CASE { get { return int.Parse(Argument["IS-WORD-CASE"].Value) > 0; } }
    //        public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
    //        public static float WORD_DROPOUT { get { return float.Parse(Argument["WORD-DROPOUT"].Value); } }
    //        public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }

    //        public static int UNK_INIT { get { return int.Parse(Argument["UNK-INIT"].Value); } }

    //        public static int[] CHAR_EMBED_DIM { get { return Argument["CHAR-EMBED-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static int[] CHAR_EMBED_WIN { get { return Argument["CHAR-EMBED-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

    //        public static int HighWay_Layer { get { return int.Parse(Argument["HIGHWAY-LAYER"].Value); } }
    //        public static bool HighWay_Share { get { return int.Parse(Argument["HIGHWAY-SHARE"].Value) > 0; } }

    //        public static int[] LSTM_DIM { get { return Argument["LSTM-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }


    //        public static int MEMORY_ENSEMBLE { get { return int.Parse(Argument["MEMORY-ENSEMBLE"].Value); } }

    //        public static float Soft1Dropout { get { return float.Parse(Argument["SOFT1-DROPOUT"].Value); } }
    //        public static float Soft2Dropout { get { return float.Parse(Argument["SOFT2-DROPOUT"].Value); } }
    //        public static float LSTMDropout { get { return float.Parse(Argument["LSTM-DROPOUT"].Value); } }
    //        public static float CoAttDropout { get { return float.Parse(Argument["COATT-DROPOUT"].Value); } }
    //        public static float CNNDROPOUT { get { return float.Parse(Argument["CNN-DROPOUT"].Value); } }
    //        public static float EMBEDDropout { get { return float.Parse(Argument["EMBED-DROPOUT"].Value); } }
    //        public static float L3GDropout { get { return float.Parse(Argument["L3G-DROPOUT"].Value); } }
    //        public static float CHARDropout { get { return float.Parse(Argument["CHAR-DROPOUT"].Value); } }

    //        public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static A_Func T_AF { get { return (A_Func)int.Parse(Argument["T-AF"].Value); } }

    //        public static int ANS_HID_DIM { get { return int.Parse(Argument["ANS-HID"].Value); } }
    //        public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
    //        public static CrossSimType AttType { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }

    //        public static int SELF_ATT_HID_DIM { get { return int.Parse(Argument["SELF-ATT-HID"].Value); } }
    //        public static int SelfAttType { get { return int.Parse(Argument["SELF-ATT-TYPE"].Value); } }
    //        public static A_Func SelfAttAF { get { return (A_Func)int.Parse(Argument["SELF-ATT-AF"].Value); } }

    //        public static int RECURRENT_DIM { get { return int.Parse(Argument["RECURRENT-DIM"].Value); } }
    //        public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
    //        public static PoolingType QUERY_POOL { get { return (PoolingType)int.Parse(Argument["QUERY-POOL"].Value); } }
    //        public static bool RL_ALL { get { return int.Parse(Argument["RL-ALL"].Value) > 0; } }
    //        public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

    //        public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
    //        public static bool IS_NORM_REWARD { get { return int.Parse(Argument["IS-NORM-REWARD"].Value) > 0; } }
    //        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

    //        public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
    //        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
    //        public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

    //        public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }

    //        public static int SPAN_LENGTH { get { return int.Parse(Argument["SPAN-LENGTH"].Value); } }
    //        public static int TRAIN_SPAN_LENGTH { get { return int.Parse(Argument["TRAIN-SPAN-LENGTH"].Value); } }
    //        public static int MAX_D_LENGTH { get { return int.Parse(Argument["MAX-D-LENGTH"].Value); } }

    //        public static int SPAN_OBJECTIVE { get { return int.Parse(Argument["SPAN-OBJECTIVE"].Value); } }
    //        public static int ENDPOINT_CONFIG { get { return int.Parse(Argument["ENDPOINT-CONFIG"].Value); } }
    //        public static int STARTPOINT_CONFIG { get { return int.Parse(Argument["STARTPOINT-CONFIG"].Value); } }
    //        public static float TERMINATE_DISCOUNT { get { return float.Parse(Argument["TERMINATE-DISCOUNT"].Value); } }
    //    }

    //    public override BuilderType Type { get { return BuilderType.APP_REASONET_SQUAD_READER_V20; } }

    //    public override void InitStartup(string fileName)
    //    {
    //        BuilderParameters.Parse(fileName);
    //    }
    //    public enum PredType { RL_MAXITER, RL_AVGSIM, RL_AVGPROB, RL_ENSEMBLE3 }

    //    /// <summary>
    //    /// Memory Retrieval Runner.
    //    /// </summary>
    //    class MemoryRetrievalRunner : StructRunner
    //    {
    //        HiddenBatchData Address { get; set; }
    //        public HiddenBatchData SoftmaxAddress { get; set; }

    //        SeqDenseBatchData Memory { get; set; }
    //        BiMatchBatchData MatchData { get; set; }
    //        float Gamma { get; set; }
    //        /// <summary>
    //        /// softmax on address given query.
    //        /// </summary>
    //        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
    //        CudaPieceInt Tgt2Src;
    //        CudaPieceFloat Tgt2SrcSoftmax;

    //        //CudaPieceFloat tmpMem;
    //        //CudaPieceFloat tmpMem2;
    //        public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Address = address;
    //            Memory = memory;
    //            MatchData = matchData;
    //            Gamma = gamma;
    //            SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
    //            Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
    //            Tgt2Src = new CudaPieceInt(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
    //            Tgt2SrcSoftmax = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);


    //            //tmpMem = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
    //            //tmpMem2 = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
    //        }

    //        public override void Forward()
    //        {
    //            Output.BatchSize = MatchData.SrcSize;

    //            // softmax 
    //            // softmax of attention
    //            ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

    //            ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
    //                SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
    //                Output.Output.Data, 0, CudaPieceInt.Empty, 0,
    //                Memory.SentSize, Memory.Dim, 0, 1);
    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {

    //            //tmpMem.CopyFrom(Memory.SentDeriv);
    //            //tmpMem.Zero();


    //            // outputDeriv -> memoryContentDeriv.

    //            //tmpMem2.CopyFrom(Memory.SentDeriv);
    //            //if (BuilderParameters.DEBUG_MODE == 0)
    //            {
    //                SoftmaxAddress.Output.Data.SyncToCPU(MatchData.MatchSize);
    //                for (int i = 0; i < MatchData.MatchSize; i++)
    //                {
    //                    int matchIdx = MatchData.Tgt2MatchElement.MemPtr[i];
    //                    Tgt2Src.MemPtr[i] = MatchData.SrcIdx.MemPtr[matchIdx];
    //                    Tgt2SrcSoftmax.MemPtr[i] = SoftmaxAddress.Output.Data.MemPtr[matchIdx];
    //                }
    //                Tgt2Src.SyncFromCPU(MatchData.MatchSize);
    //                Tgt2SrcSoftmax.SyncFromCPU(MatchData.MatchSize);
    //                ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, Tgt2Src, 0, Tgt2SrcSoftmax, // SoftmaxAddress.Output.Data,
    //                                             MatchData.Tgt2MatchIdx, MatchData.TgtSize, Memory.SentDeriv /*Memory.SentDeriv*/, 0, CudaPieceInt.Empty, 0,
    //                                             MatchData.MatchSize, Memory.Dim, 1, 1);
    //                //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem, Memory.SentSize, Memory.Dim, 1);
    //            }
    //            //else if (BuilderParameters.DEBUG_MODE == 1)
    //            //{
    //            //    //tmpMem2.Zero();
    //            //    ComputeLib.AccurateScale_Matrix(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
    //            //                                Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
    //            //                                Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data);
    //            //    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem2, Memory.SentSize, Memory.Dim, 1);
    //            //}

    //            //tmpMem.SyncToCPU(Memory.SentSize * Memory.Dim);
    //            //tmpMem2.SyncToCPU(Memory.SentSize * Memory.Dim);
    //            //for (int i = 0; i < Memory.SentSize * Memory.Dim; i++)
    //            //{
    //            //    if (Math.Abs(tmpMem2.MemPtr[i] - tmpMem.MemPtr[i]) >= 0.000000001)
    //            //    {
    //            //        Console.WriteLine("Adopt Debug Model {3},  Error! {0}, {1}, {2}", tmpMem.MemPtr[i], tmpMem2.MemPtr[i], i, BuilderParameters.DEBUG_MODE);
    //            //    }
    //            //}
    //            //ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, )

    //            // outputDeriv -> AddressDeriv.
    //            ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
    //                MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
    //                Memory.Dim, Util.GPUEpsilon);

    //            ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
    //                SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

    //            ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
    //        }
    //    }

    //    /// <summary>
    //    /// GRU Query Runner.
    //    /// </summary>
    //    public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
    //    {
    //        public HiddenBatchData Query { get; set; }

    //        public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

    //        HiddenBatchData GateR; //reset gate
    //        HiddenBatchData GateZ; //update gate
    //        HiddenBatchData HHat; //new memory
    //        HiddenBatchData RestH; //new memory

    //        CudaPieceFloat Ones;
    //        CudaPieceFloat Tmp;
    //        public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
    //        {
    //            Query = query;

    //            Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

    //            GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
    //            GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
    //            HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
    //            RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

    //            Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
    //            Ones.Init(1);

    //            Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
    //        }

    //        public override void Forward()
    //        {
    //            //SubQuery.BatchSize = 1;
    //            /*Wr X -> GateR*/
    //            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
    //            /*h_t-1 -> GateR*/
    //            ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
    //            ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

    //            /*Wz X -> GateZ*/
    //            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
    //            /*h_t-1 -> GateZ*/
    //            ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
    //            ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

    //            /*Wh X -> HHat*/
    //            ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
    //            ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
    //            ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
    //            ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

    //            Output.BatchSize = Query.BatchSize;

    //            //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
    //            // tmp = (1 - GateZ(t))
    //            ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
    //                                           GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
    //                                           Tmp, 0, CudaPieceInt.Empty, 0,
    //                                           Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

    //            // NewQuery = GateZ(t) * HHat
    //            ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

    //            // NewQuery += tmp * h_t-1
    //            ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
    //            ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
    //            // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
    //            ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
    //                                           Query.Output.Data, 0, CudaPieceInt.Empty, 0,
    //                                           Tmp, 0, CudaPieceInt.Empty, 0,
    //                                           Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
    //            ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

    //            ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
    //            ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

    //            ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
    //            ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

    //            ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
    //            ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

    //            ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
    //            ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

    //            ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
    //            ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
    //            ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
    //        }

    //        public override void Update()
    //        {
    //            if (!IsDelegateOptimizer)
    //            {
    //                Model.WhMatrixOptimizer.BeforeGradient();
    //                Model.WrMatrixOptimizer.BeforeGradient();
    //                Model.WzMatrixOptimizer.BeforeGradient();

    //                Model.UhMatrixOptimizer.BeforeGradient();
    //                Model.UrMatrixOptimizer.BeforeGradient();
    //                Model.UzMatrixOptimizer.BeforeGradient();

    //                Model.BhMatrixOptimizer.BeforeGradient();
    //                Model.BrMatrixOptimizer.BeforeGradient();
    //                Model.BzMatrixOptimizer.BeforeGradient();

    //            }
    //            ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
    //            ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
    //            ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

    //            ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
    //            ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
    //            ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

    //            ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
    //            ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
    //            ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

    //            if (!IsDelegateOptimizer)
    //            {
    //                Model.WhMatrixOptimizer.AfterGradient();
    //                Model.WrMatrixOptimizer.AfterGradient();
    //                Model.WzMatrixOptimizer.AfterGradient();

    //                Model.UhMatrixOptimizer.AfterGradient();
    //                Model.UrMatrixOptimizer.AfterGradient();
    //                Model.UzMatrixOptimizer.AfterGradient();

    //                Model.BhMatrixOptimizer.AfterGradient();
    //                Model.BrMatrixOptimizer.AfterGradient();
    //                Model.BzMatrixOptimizer.AfterGradient();
    //            }
    //        }
    //    }


    //    class BatchSizeAssignmentRunner : StructRunner
    //    {
    //        SeqDenseBatchData Data;
    //        IntArgument Arg;
    //        public BatchSizeAssignmentRunner(SeqDenseBatchData seqData, IntArgument arg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Data = seqData;
    //            Arg = arg;
    //        }
    //        public override void Forward()
    //        {
    //            Arg.Value = Data.BatchSize;
    //        }
    //    }

    //    class SentSizeAssignmentRunner : StructRunner
    //    {
    //        SeqDenseBatchData Data;
    //        IntArgument Arg;
    //        public SentSizeAssignmentRunner(SeqDenseBatchData seqData, IntArgument arg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Data = seqData;
    //            Arg = arg;
    //        }
    //        public override void Forward()
    //        {
    //            Arg.Value = Data.SentSize;
    //        }
    //    }

    //    class AnswerModule_V1Runner : CompositeNetRunner
    //    {
    //        public SeqVectorData AnsStart { get; set; }
    //        public SeqVectorData AnsEnd { get; set; }
    //        public AnswerModule_V1Runner(SeqDenseBatchData query, LayerStructure querypool, SeqDenseBatchData memory, LayerStructure ansStart, LayerStructure ansEnd, RunnerBehavior behavior) : base(behavior)
    //        {
    //            MatrixData Q = new MatrixData(query);
    //            VectorData p = new VectorData(querypool.Neural_In, querypool.weight, querypool.WeightGrad, behavior.Device);

    //            MatrixMultiplicationRunner Qp = new MatrixMultiplicationRunner(Q, p, behavior);
    //            LinkRunners.Add(Qp);

    //            SeqVectorData attScore = new SeqVectorData(query.MAX_SENTSIZE, query.MAX_BATCHSIZE, Qp.Output.Output, Qp.Output.Deriv, query.SampleIdx, query.SentMargin, behavior.Device);

    //            SeqVecSoftmaxRunner attSoftmaxRunner = new SeqVecSoftmaxRunner(attScore, 1, behavior);
    //            LinkRunners.Add(attSoftmaxRunner);

    //            SeqVecMultiplicationRunner poolQRunner = new SeqVecMultiplicationRunner(attSoftmaxRunner.Output, Q, behavior);
    //            LinkRunners.Add(poolQRunner);

    //            MatrixData AnsStartMatrix = new MatrixData(ansStart);
    //            MatrixData AnsEndMatrix = new MatrixData(ansEnd);

    //            MatrixMultiplicationRunner StartQRunner = new MatrixMultiplicationRunner(poolQRunner.Output, AnsStartMatrix, behavior);
    //            LinkRunners.Add(StartQRunner);

    //            MatrixMultiplicationRunner EndQRunner = new MatrixMultiplicationRunner(poolQRunner.Output, AnsEndMatrix, behavior);
    //            LinkRunners.Add(EndQRunner);

    //            SeqInnerProductRunner startAttRunner = new SeqInnerProductRunner(StartQRunner.Output, memory, behavior);
    //            LinkRunners.Add(startAttRunner);

    //            SeqInnerProductRunner endAttRunner = new SeqInnerProductRunner(EndQRunner.Output, memory, behavior);
    //            LinkRunners.Add(endAttRunner);

    //            AnsStart = new SeqVectorData(memory.Stat.MAX_SEQUENCESIZE, memory.Stat.MAX_BATCHSIZE, startAttRunner.Output.Output, startAttRunner.Output.Deriv, memory.SampleIdx, memory.SentMargin, behavior.Device);
    //            AnsEnd = new SeqVectorData(memory.Stat.MAX_SEQUENCESIZE, memory.Stat.MAX_BATCHSIZE, endAttRunner.Output.Output, endAttRunner.Output.Deriv, memory.SampleIdx, memory.SentMargin, behavior.Device);
    //        }
    //    }

    //    class TerminationModule_V1Runner : CompositeNetRunner
    //    {
    //        public HiddenBatchData StartTerminateGate { get; set; }
    //        public HiddenBatchData EndTerminateGate { get; set; }
    //        public TerminationModule_V1Runner(SeqDenseBatchData query, List<LayerStructure> startTerm, List<LayerStructure> endTerm, RunnerBehavior behavior) : base(behavior)
    //        {
    //            MaxPoolingRunner<SeqDenseBatchData> maxPool = new MaxPoolingRunner<SeqDenseBatchData>(query, behavior);
    //            LinkRunners.Add(maxPool);

    //            {
    //                HiddenBatchData state = maxPool.Output;
    //                for (int i = 0; i < startTerm.Count; i++)
    //                {
    //                    FullyConnectHiddenRunner<HiddenBatchData> termRunner = new FullyConnectHiddenRunner<HiddenBatchData>(startTerm[i], state, behavior);
    //                    LinkRunners.Add(termRunner);
    //                    state = termRunner.Output;
    //                }
    //                StartTerminateGate = state;
    //            }

    //            {
    //                HiddenBatchData state = maxPool.Output;
    //                for (int i = 0; i < endTerm.Count; i++)
    //                {
    //                    FullyConnectHiddenRunner<HiddenBatchData> termRunner = new FullyConnectHiddenRunner<HiddenBatchData>(endTerm[i], state, behavior);
    //                    LinkRunners.Add(termRunner);
    //                    state = termRunner.Output;
    //                }
    //                EndTerminateGate = state;
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// Design a new style reasoning net for span detection.
    //    /// </summary>
    //    class ReasonNetV3Runner : StructRunner
    //    {

    //        SeqDenseBatchData Query { get; set; }
    //        SeqDenseBatchData Memory { get; set; }
    //        BiMatchBatchData MatchData { get; set; }

    //        string DebugFile { get; set; }

    //        /// <summary>
    //        /// maximum iteration number.
    //        /// </summary>
    //        int MaxIterationNum { get; set; }

    //        public SeqVectorData[] AnsStartData = null;
    //        public SeqVectorData[] AnsEndData = null;

    //        HiddenBatchData[] StartTerminalData = null;
    //        HiddenBatchData[] EndTerminalData = null;

    //        /// <summary>
    //        /// tree probability.
    //        /// </summary>
    //        public HiddenBatchData[] StartAnswerProb = null;
    //        public HiddenBatchData[] EndAnswerProb = null;

    //        /// <summary>
    //        /// predict final start Ans.
    //        /// </summary>
    //        public SeqVectorData FinalStartAns = null;
    //        /// <summary>
    //        /// predict final end Ans.
    //        /// </summary>
    //        public SeqVectorData FinalEndAns = null;

    //        public ReasonNetV3Runner(ComputationGraph cg,
    //            SeqDenseBatchData query, SeqDenseBatchData mem, BiMatchBatchData q2memMatch,
    //            int maxIterateNum,
    //            Tuple<LayerStructure, LayerStructure, LayerStructure> ansStruct,
    //            Tuple<List<LayerStructure>, List<LayerStructure>> termStruct,

    //            Tuple<LayerStructure, int, A_Func> attStruct,
    //            Tuple<LSTMStructure, LSTMStructure> qStruct,

    //            RunnerBehavior behavior, string debugFile = "") :
    //            base(cg, Structure.Empty, null, behavior)
    //        {
    //            MaxIterationNum = maxIterateNum;
    //            DebugFile = debugFile;
    //            // For Forward/Backward Propagation.
    //            {
    //                AnsStartData = new SeqVectorData[MaxIterationNum];
    //                AnsEndData = new SeqVectorData[MaxIterationNum];
    //                StartTerminalData = new HiddenBatchData[MaxIterationNum];
    //                EndTerminalData = new HiddenBatchData[MaxIterationNum];
    //            }

    //            Query = query;
    //            Memory = mem;
    //            MatchData = q2memMatch;

    //            SeqDenseBatchData Q = query;

    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                SeqAlignmentRunnerV2 alignRunner = new SeqAlignmentRunnerV2(new MatrixData(Q), new MatrixData(mem), q2memMatch, attStruct.Item1, behavior, attStruct.Item2, attStruct.Item3);
    //                cg.AddRunner(alignRunner);

    //                MemoryRetrievalRunner docRunner = new MemoryRetrievalRunner(alignRunner.Output, mem, q2memMatch, 1, behavior);
    //                cg.AddRunner(docRunner);

    //                SeqDenseBatchData qAlignFea = new SeqDenseBatchData(new SequenceDataStat(Q.MAX_BATCHSIZE, Q.MAX_SENTSIZE, mem.Dim), Q.SampleIdx, Q.SentMargin,
    //                                                                    docRunner.Output.Output.Data, docRunner.Output.Deriv.Data, Behavior.Device);


    //                EnsembleConcateSeqRunner newIRunner = new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { Q, qAlignFea }, Behavior) { name = i.ToString() + "_merge_Q" };
    //                cg.AddRunner(newIRunner);

    //                BiLSTMRunner newQRunner = new BiLSTMRunner(qStruct.Item1, qStruct.Item2, newIRunner.Output, behavior);
    //                cg.AddRunner(newQRunner);

    //                Q = newQRunner.Output;

    //                AnswerModule_V1Runner ansRunner = new AnswerModule_V1Runner(Q, ansStruct.Item1, mem, ansStruct.Item2, ansStruct.Item3, behavior);
    //                cg.AddRunner(ansRunner);
    //                AnsStartData[i] = ansRunner.AnsStart;
    //                AnsEndData[i] = ansRunner.AnsEnd;

    //                TerminationModule_V1Runner termRunner = new TerminationModule_V1Runner(Q, termStruct.Item1, termStruct.Item2, behavior);
    //                cg.AddRunner(termRunner);
    //                StartTerminalData[i] = termRunner.StartTerminateGate;
    //                EndTerminalData[i] = termRunner.EndTerminateGate;
    //            }

    //            {
    //                StartAnswerProb = new HiddenBatchData[MaxIterationNum];
    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    StartAnswerProb[i] = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
    //                }

    //                EndAnswerProb = new HiddenBatchData[MaxIterationNum];
    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    EndAnswerProb[i] = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
    //                }

    //                FinalStartAns = new SeqVectorData(Memory.MAX_SENTSIZE, Memory.MAX_BATCHSIZE, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);
    //                FinalEndAns = new SeqVectorData(Memory.MAX_SENTSIZE, Memory.MAX_BATCHSIZE, Memory.SampleIdx, Memory.SentMargin, Behavior.Device);
    //            }
    //        }

    //        Dictionary<int, int> StartInferenStepStat = new Dictionary<int, int>();
    //        Dictionary<int, int> EndInferenStepStat = new Dictionary<int, int>();

    //        int batchNum = 0;
    //        /// <summary>
    //        /// Output Debug File.
    //        /// </summary>
    //        public override void Init()
    //        {
    //            if (DebugFile != "" && !DebugFile.Equals(string.Empty)) { }

    //            StartInferenStepStat.Clear();
    //            EndInferenStepStat.Clear();
    //            batchNum = 0;
    //        }

    //        /// <summary>
    //        /// Save Debug File.
    //        /// </summary>
    //        public override void Complete()
    //        {
    //            foreach (KeyValuePair<int, int> stat in StartInferenStepStat)
    //            {
    //                Logger.WriteLog("Start Max Inference Step Stat {0}, number {1}", stat.Key, stat.Value);
    //            }

    //            foreach (KeyValuePair<int, int> stat in EndInferenStepStat)
    //            {
    //                Logger.WriteLog("End Max Inference Step Stat {0}, number {1}", stat.Key, stat.Value);
    //            }
    //        }

    //        public override void Forward()
    //        {
    //            iteration++;

    //            // cascading tree termination probability.
    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                ComputeLib.Logistic(StartTerminalData[i].Output.Data, 0, StartTerminalData[i].Output.Data, 0, StartTerminalData[i].Output.BatchSize, 1);
    //                ComputeLib.ClipVector(StartTerminalData[i].Output.Data, StartTerminalData[i].BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
    //                StartTerminalData[i].Output.Data.SyncToCPU(StartTerminalData[i].BatchSize);

    //                ComputeLib.Logistic(EndTerminalData[i].Output.Data, 0, EndTerminalData[i].Output.Data, 0, EndTerminalData[i].Output.BatchSize, 1);
    //                ComputeLib.ClipVector(EndTerminalData[i].Output.Data, EndTerminalData[i].BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
    //                EndTerminalData[i].Output.Data.SyncToCPU(EndTerminalData[i].BatchSize);
    //            }

    //            for (int b = 0; b < Query.BatchSize; b++)
    //            {
    //                float acc_log_t_start = 0;
    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    float t_i = StartTerminalData[i].Output.Data.MemPtr[b];
    //                    if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

    //                    float p = acc_log_t_start + (float)Math.Log(t_i);
    //                    if (i == MaxIterationNum - 1) p = acc_log_t_start;

    //                    StartAnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
    //                    acc_log_t_start += (float)Math.Log(1 - t_i);
    //                }

    //                float acc_log_t_end = 0;
    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    float t_i = EndTerminalData[i].Output.Data.MemPtr[b];
    //                    if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

    //                    float p = acc_log_t_end + (float)Math.Log(t_i);
    //                    if (i == MaxIterationNum - 1) p = acc_log_t_end;

    //                    EndAnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
    //                    acc_log_t_end += (float)Math.Log(1 - t_i);
    //                }
    //            }
    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                StartAnswerProb[i].BatchSize = Query.BatchSize;
    //                StartAnswerProb[i].Output.Data.SyncFromCPU(StartAnswerProb[i].BatchSize);

    //                EndAnswerProb[i].BatchSize = Query.BatchSize;
    //                EndAnswerProb[i].Output.Data.SyncFromCPU(EndAnswerProb[i].BatchSize);
    //            }

    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                AnsStartData[i].Output.SyncToCPU(AnsStartData[i].Length);
    //                AnsEndData[i].Output.SyncToCPU(AnsEndData[i].Length);
    //            }

    //            FinalStartAns.Segment = Memory.BatchSize;
    //            FinalStartAns.Length = Memory.SentSize;
    //            Array.Clear(FinalStartAns.Output.MemPtr, 0, FinalStartAns.Length);


    //            FinalEndAns.Segment = Memory.BatchSize;
    //            FinalEndAns.Length = Memory.SentSize;
    //            Array.Clear(FinalEndAns.Output.MemPtr, 0, FinalEndAns.Length);

    //            Memory.SampleIdx.SyncToCPU(Memory.BatchSize);
    //            int[] ansIdx = Memory.SampleIdx.MemPtr;

    //            for (int b = 0; b < Query.BatchSize; b++)
    //            {
    //                int max_start_iter = 0;
    //                double max_start_p = 0;

    //                int max_end_iter = 0;
    //                double max_end_p = 0;

    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    float startP = StartAnswerProb[i].Output.Data.MemPtr[b];
    //                    if (startP > max_start_p)
    //                    {
    //                        max_start_p = startP;
    //                        max_start_iter = i;
    //                    }

    //                    float endP = EndAnswerProb[i].Output.Data.MemPtr[b];
    //                    if (endP > max_end_p)
    //                    {
    //                        max_end_p = endP;
    //                        max_end_iter = i;
    //                    }

    //                    if (BuilderParameters.PRED_RL == PredType.RL_AVGPROB)
    //                    {
    //                        int matchBgn = b == 0 ? 0 : ansIdx[b - 1];
    //                        int matchEnd = ansIdx[b];
    //                        double expSum_start = 0;
    //                        double expSum_end = 0;
    //                        for (int m = matchBgn; m < matchEnd; m++)
    //                        {
    //                            expSum_start += Math.Exp(AnsStartData[i].Output.MemPtr[m]);
    //                            expSum_end += Math.Exp(AnsEndData[i].Output.MemPtr[m]);
    //                        }

    //                        for (int m = matchBgn; m < matchEnd; m++)
    //                        {
    //                            FinalStartAns.Output.MemPtr[m] += (float)(startP * Math.Exp(AnsStartData[i].Output.MemPtr[m]) / expSum_start);
    //                            FinalEndAns.Output.MemPtr[m] += (float)(endP * Math.Exp(AnsEndData[i].Output.MemPtr[m]) / expSum_end);
    //                        }
    //                    }
    //                }
    //                if (!StartInferenStepStat.ContainsKey(max_start_iter)) StartInferenStepStat[max_start_iter] = 0;
    //                StartInferenStepStat[max_start_iter] += 1;

    //                if (!EndInferenStepStat.ContainsKey(max_end_iter)) EndInferenStepStat[max_end_iter] = 0;
    //                EndInferenStepStat[max_end_iter] += 1;

    //                if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
    //                {
    //                    int matchBgn = b == 0 ? 0 : ansIdx[b - 1];
    //                    int matchEnd = ansIdx[b];

    //                    double expSum_start = 0;
    //                    double expSum_end = 0;
    //                    for (int m = matchBgn; m < matchEnd; m++)
    //                    {
    //                        expSum_start += Math.Exp(AnsStartData[max_start_iter].Output.MemPtr[m]);
    //                        expSum_end += Math.Exp(AnsEndData[max_end_iter].Output.MemPtr[m]);
    //                    }
    //                    for (int m = matchBgn; m < matchEnd; m++)
    //                    {
    //                        FinalStartAns.Output.MemPtr[m] = (float)(Math.Exp(AnsStartData[max_start_iter].Output.MemPtr[m]) / expSum_start);  //AnsStartData[max_iter].Output.Data.MemPtr[m];
    //                        FinalEndAns.Output.MemPtr[m] = (float)(Math.Exp(AnsEndData[max_end_iter].Output.MemPtr[m]) / expSum_end); // AnsEndData[max_iter].Output.Data.MemPtr[m];
    //                    }
    //                }
    //            }
    //            FinalStartAns.Output.SyncFromCPU(FinalStartAns.Length);
    //            FinalEndAns.Output.SyncFromCPU(FinalEndAns.Length);
    //            batchNum += Query.BatchSize;
    //        }
    //        public override void CleanDeriv()
    //        {
    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                ComputeLib.Zero(StartAnswerProb[i].Deriv.Data, StartAnswerProb[i].BatchSize);
    //                ComputeLib.Zero(EndAnswerProb[i].Deriv.Data, EndAnswerProb[i].BatchSize);
    //            }
    //        }

    //        /// <summary>
    //        /// Stage 1 : Iterative Attention Model (Supervised Training).
    //        /// </summary>
    //        /// <param name="cleanDeriv"></param>
    //        public override void Backward(bool cleanDeriv)
    //        {
    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                Array.Clear(StartTerminalData[i].Deriv.Data.MemPtr, 0, StartTerminalData[i].BatchSize);
    //                Array.Clear(EndTerminalData[i].Deriv.Data.MemPtr, 0, EndTerminalData[i].BatchSize);
    //            }

    //            for (int b = 0; b < Query.BatchSize; b++)
    //            {
    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    float start_reward = StartAnswerProb[i].Deriv.Data.MemPtr[b] * BuilderParameters.TERMINATE_DISCOUNT;
    //                    float end_reward = EndAnswerProb[i].Deriv.Data.MemPtr[b] * BuilderParameters.TERMINATE_DISCOUNT;

    //                    StartTerminalData[i].Deriv.Data.MemPtr[b] += start_reward * (1 - StartTerminalData[i].Output.Data.MemPtr[b]);
    //                    EndTerminalData[i].Deriv.Data.MemPtr[b] += end_reward * (1 - EndTerminalData[i].Output.Data.MemPtr[b]);

    //                    for (int hp = 0; hp < i; hp++)
    //                    {
    //                        StartTerminalData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * start_reward * StartTerminalData[hp].Output.Data.MemPtr[b];
    //                        EndTerminalData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * end_reward * EndTerminalData[hp].Output.Data.MemPtr[b];
    //                    }

    //                    if (StartTerminalData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
    //                    {
    //                        Logger.WriteLog("Start TerminalRunner Deriv is too large {0}, step {1}", StartTerminalData[i].Deriv.Data.MemPtr[b], i);
    //                    }

    //                    if (EndTerminalData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
    //                    {
    //                        Logger.WriteLog("End TerminalRunner Deriv is too large {0}, step {1}", EndTerminalData[i].Deriv.Data.MemPtr[b], i);
    //                    }

    //                }
    //            }

    //            //if (BuilderParameters.IS_FIX_FT)
    //            {
    //                for (int b = 0; b < Query.BatchSize; b++)
    //                {
    //                    StartTerminalData[MaxIterationNum - 1].Deriv.Data.MemPtr[b] = 0;
    //                    EndTerminalData[MaxIterationNum - 1].Deriv.Data.MemPtr[b] = 0;
    //                }
    //            }


    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                StartTerminalData[i].Deriv.Data.SyncFromCPU(StartTerminalData[i].BatchSize);
    //                EndTerminalData[i].Deriv.Data.SyncFromCPU(EndTerminalData[i].BatchSize);
    //            }
    //        }

    //    }


    //    class MatchExecRunner : CompositeNetRunner
    //    {
    //        public SeqVectorData Attentive { get; set; }
    //        public MatchExecRunner(MatrixData q, SeqMatrixData memKey, BiMatchBatchData q2memMatch, LayerStructure attStruct, int op, A_Func afunc, RunnerBehavior behavior) : base(behavior)
    //        {
    //            SeqAlignmentRunnerV2 softSimRunner = new SeqAlignmentRunnerV2(q, memKey, q2memMatch, attStruct, behavior, op, afunc);
    //            LinkRunners.Add(softSimRunner);

    //            SeqVecSoftmaxRunner softmaxRunner = new SeqVecSoftmaxRunner(
    //                                        new SeqVectorData(memKey.MaxLength, memKey.MaxSegment, softSimRunner.Output.Output.Data, softSimRunner.Output.Deriv.Data,
    //                                        memKey.SegmentIdx, memKey.SegmentMargin, Behavior.Device), 1, Behavior);
    //            LinkRunners.Add(softmaxRunner);

    //            // Attention Softmax.
    //            SeqVectorData softAttVec = (SeqVectorData)cg.AddRunner(
    //                                        );

    //            // Attention Retrieve.
    //            MatrixData inputMatrix = (MatrixData)cg.AddRunner(new SeqVecMultiplicationRunner(softAttVec, MemoryMatrix, Behavior));

    //        }
    //    }


    //    class EpisodicAttentionRunner : CompositeNetRunner
    //    {
    //        SeqDenseBatchData Src { get; set; }
    //        SeqDenseBatchData Tgt { get; set; }

    //        SeqDenseBatchData SrcOutput;
    //        public EpisodicAttentionRunner(SeqDenseBatchData source, SeqDenseBatchData target, RunnerBehavior behavior) : base(behavior)
    //        {
    //            Src = source;
    //            Tgt = target;

    //            SrcRE = new SeqDenseBatchData()
    //        }
    //    }

    //    class EpisodicReasoNetRunner : CompositeNetRunner
    //    {
    //        int EpisodicSize { get; set; }
            
    //        SeqDenseBatchData Query { get; set; }
    //        SeqDenseBatchData Doc { get; set; }

    //        /// <summary>
    //        /// att_ij = att( q_i, d_j )
    //        /// </summary>
    //        /// <param name="query"></param>
    //        /// <param name="doc"></param>
    //        /// <param name="episodicSize"></param>
    //        /// <param name="behavior"></param>
    //        public EpisodicReasoNetRunner(SeqDenseBatchData query, SeqDenseBatchData doc, int episodicSize, RunnerBehavior behavior) : base(behavior)
    //        {
    //            EpisodicSize = episodicSize;

    //            Query = query;
    //            Doc = doc;



    //            MatrixData Q = new MatrixData(query);
    //            // Self-attentive init q.
    //            MatrixMultiplicationRunner Qp = new MatrixMultiplicationRunner(Q, initU, behavior);
    //            LinkRunners.Add(Qp);

    //            SeqVectorData attScore = new SeqVectorData(query.MAX_SENTSIZE, query.MAX_BATCHSIZE, Qp.Output.Output, Qp.Output.Deriv, query.SampleIdx, query.SentMargin, behavior.Device);

    //            SeqVecSoftmaxRunner attSoftmaxRunner = new SeqVecSoftmaxRunner(attScore, 1, behavior);
    //            LinkRunners.Add(attSoftmaxRunner);

    //            SeqVecMultiplicationRunner poolQRunner = new SeqVecMultiplicationRunner(attSoftmaxRunner.Output, Q, behavior);
    //            LinkRunners.Add(poolQRunner);

    //            MatrixData Q0 = poolQRunner.Output;

    //            for (int i = 0; i < maxOpNum; i++)
    //            {
    //                // no decision at the first iteration.
    //                if (i == 0)
    //                {

    //                }

    //            }
    //        }
    //    }

    //    class ExactMatchPredictionRunner : ObjectiveRunner
    //    {
    //        SeqVectorData StartPosData;
    //        SeqVectorData EndPosData;
    //        List<HashSet<string>> PosTrueData;

    //        int SampleNum = 0;
    //        int HitNum = 0;
    //        int Iteration = 0;
    //        bool NeedSoftmax = true;
    //        float Gamma = 1;
    //        //List<Dictionary<string, float>> Result = null;
    //        //List<List<Dictionary<string, float>>> HisResults = new List<List<Dictionary<string, float>>>();

    //        public ExactMatchPredictionRunner(
    //            SeqVectorData startPosData, SeqVectorData endPosData,
    //            List<HashSet<string>> posTrueData,
    //            RunnerBehavior behavior, string outputFile = "", bool needSoftmax = true, float gamma = 1) : base(Structure.Empty, behavior)
    //        {
    //            StartPosData = startPosData;
    //            EndPosData = endPosData;
    //            PosTrueData = posTrueData;

    //            OutputPath = outputFile;

    //            NeedSoftmax = needSoftmax;
    //            Gamma = gamma;
    //        }
    //        string OutputPath = "";
    //        string resultFile = string.Empty;
    //        StreamWriter OutputWriter = null;

    //        string dumpFile = string.Empty;
    //        StreamWriter DumpWriter = null;
    //        public override void Init()
    //        {
    //            SampleNum = 0;
    //            HitNum = 0;
    //            if (!OutputPath.Equals(""))
    //            {
    //                resultFile = OutputPath + "." + Iteration.ToString() + ".result";
    //                dumpFile = OutputPath + "." + Iteration.ToString() + ".dump";
    //                OutputWriter = new StreamWriter(resultFile);
    //                DumpWriter = new StreamWriter(dumpFile);
    //            }
    //        }

    //        public override void Complete()
    //        {
    //            Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum);
    //            if (OutputWriter != null)
    //            {
    //                OutputWriter.Close();
    //                DumpWriter.Close();

    //                //restore.py --span_path dev.nltk.spans --raw_path dev.doc --ids_path dev.ids --fin score.4.result --fout dump.json

    //                if (Directory.Exists(BuilderParameters.EVAL_PATH))
    //                {
    //                    using (Process callProcess = new Process()
    //                    {
    //                        StartInfo = new ProcessStartInfo()
    //                        {
    //                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
    //                            Arguments = string.Format("\"{0}\" --span_path \"{1}\" --raw_path \"{2}\" --ids_path \"{3}\" --fin \"{4}\" --fout {5}",
    //                            BuilderParameters.EVAL_PATH + "restore.py",
    //                            BuilderParameters.ValidSpan,
    //                            BuilderParameters.EVAL_PATH + "dev.doc",
    //                            BuilderParameters.EVAL_PATH + "dev.ids",
    //                            resultFile,
    //                            resultFile + ".json"
    //                            ),
    //                            CreateNoWindow = true,
    //                            UseShellExecute = false,
    //                            RedirectStandardOutput = true,
    //                        }
    //                    })
    //                    {
    //                        callProcess.Start();
    //                        callProcess.WaitForExit();
    //                    }

    //                    string resultsOutput = "";
    //                    using (Process callProcess = new Process()
    //                    {
    //                        StartInfo = new ProcessStartInfo()
    //                        {
    //                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
    //                            Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"",
    //                            BuilderParameters.EVAL_PATH + "evaluate-v1.1.py",
    //                            BuilderParameters.EVAL_PATH + "dev-v1.1.json",
    //                            resultFile + ".json"
    //                            ),
    //                            CreateNoWindow = true,
    //                            UseShellExecute = false,
    //                            RedirectStandardOutput = true,
    //                        }
    //                    })
    //                    {
    //                        callProcess.Start();
    //                        resultsOutput = callProcess.StandardOutput.ReadToEnd();
    //                        callProcess.WaitForExit();
    //                    }

    //                    Logger.WriteLog("Let see results here {0}", resultsOutput);
    //                }
    //            }
    //            ObjectiveScore = HitNum * 1.0 / SampleNum;
    //            Iteration += 1;
    //        }

    //        public override void Forward()
    //        {
    //            if (NeedSoftmax) ComputeLib.SparseSoftmax(StartPosData.SegmentIdx, StartPosData.Output, StartPosData.Output, Gamma, StartPosData.Segment);
    //            if (NeedSoftmax) ComputeLib.SparseSoftmax(EndPosData.SegmentIdx, EndPosData.Output, EndPosData.Output, Gamma, EndPosData.Segment);

    //            StartPosData.Output.SyncToCPU(StartPosData.Length);
    //            EndPosData.Output.SyncToCPU(EndPosData.Length);

    //            StartPosData.SegmentIdx.SyncToCPU(StartPosData.Segment);
    //            int[] segIdx = StartPosData.SegmentIdx.MemPtr;

    //            for (int i = 0; i < StartPosData.Segment; i++)
    //            {
    //                Dictionary<string, float> candScoreDict = new Dictionary<string, float>();
    //                string trueCandIdx = string.Empty;

    //                int candBgn = i == 0 ? 0 : segIdx[i - 1];
    //                int candEnd = segIdx[i];

    //                List<string> prob = new List<string>();
    //                for (int c = candBgn; c < candEnd; c++)
    //                {
    //                    float candScoreStart = StartPosData.Output.MemPtr[c]; // Util.Logistic();
    //                    float candScoreEnd = EndPosData.Output.MemPtr[c]; // Util.Logistic(EndPosData.Output.Data.MemPtr[c2]);
    //                    int index = c - candBgn;
    //                    prob.Add(string.Format("{0}#{1}#{2}", index, candScoreStart, candScoreEnd));
    //                }

    //                if (DumpWriter != null) { DumpWriter.WriteLine(string.Join(" ", prob)); }

    //                float maxP = 0;
    //                float sP = 0;
    //                float eP = 0;
    //                int maxStart = -1;
    //                int maxEnd = -1;
    //                for (int c = candBgn; c < candEnd; c++)
    //                {
    //                    float candScoreStart = StartPosData.Output.MemPtr[c]; // Util.Logistic();
    //                    for (int c2 = c; c2 < candEnd; c2++)
    //                    {
    //                        float candScoreEnd = EndPosData.Output.MemPtr[c2]; // Util.Logistic(EndPosData.Output.Data.MemPtr[c2]);

    //                        if (BuilderParameters.SPAN_LENGTH > 0 && c2 - c + 1 > BuilderParameters.SPAN_LENGTH) continue;
    //                        if (candScoreStart * candScoreEnd > maxP)
    //                        {
    //                            maxP = candScoreStart * candScoreEnd;
    //                            sP = candScoreStart;
    //                            eP = candScoreEnd;
    //                            maxStart = c - candBgn;
    //                            maxEnd = c2 - candBgn;
    //                        }
    //                    }
    //                }
    //                if (OutputWriter != null) { OutputWriter.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", maxStart, maxEnd, sP, eP, maxP); }
    //                if (PosTrueData[SampleNum + i].Contains(string.Format("{0}#{1}", maxStart, maxEnd))) { HitNum += 1; }
    //            }
    //            SampleNum += StartPosData.Segment;
    //        }

    //    }

    //    class TwoWayEnsembleRunner : StructRunner
    //    {
    //        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
    //        //CudaPieceInt OutputMask;
    //        SeqDenseBatchData AData { get; set; }
    //        SeqDenseBatchData BData { get; set; }
    //        CudaPieceInt EnsembleMask;

    //        public TwoWayEnsembleRunner(SeqDenseBatchData aData, SeqDenseBatchData bData, RunnerBehavior behavior)
    //        : base(Structure.Empty, behavior)
    //        {
    //            AData = aData;
    //            BData = bData;

    //            Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 3, Behavior.Device);

    //            EnsembleMask = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
    //            for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask.MemPtr[s] = 2 + (s * 3);
    //            EnsembleMask.SyncFromCPU(AData.MAX_SENTSIZE);
    //        }

    //        public override void Forward()
    //        {
    //            Output.BatchSize = AData.BatchSize;
    //            Output.SentSize = AData.SentSize;

    //            ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
    //                                         AData.SentOutput, 0, AData.Dim,
    //                                         Output.SentOutput, 0, Output.Dim,
    //                                         AData.Dim, AData.SentSize, 1, 0, 0);

    //            ComputeLib.Matrix_AdditionEx(BData.SentOutput, 0, BData.Dim,
    //                                         BData.SentOutput, 0, BData.Dim,
    //                                         Output.SentOutput, AData.Dim, Output.Dim,
    //                                         BData.Dim, BData.SentSize, 1, 0, 0);

    //            ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, BData.SentOutput, 0, Output.SentOutput, 0,
    //                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, EnsembleMask, 0,
    //                                              AData.SentSize, AData.Dim, 0, 1);
    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, BData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
    //                                          EnsembleMask, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
    //                                          AData.SentSize, AData.Dim, 1, 1);

    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, BData.SentOutput, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
    //                                              EnsembleMask, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
    //                                              BData.SentSize, BData.Dim, 1, 1);

    //            ComputeLib.Matrix_AdditionEx(Output.SentDeriv, AData.Dim, Output.Dim,
    //                                         BData.SentDeriv, 0, BData.Dim,
    //                                         BData.SentDeriv, 0, BData.Dim,
    //                                         BData.Dim, BData.SentSize, 1, 0, 1);

    //            ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
    //                                         AData.SentDeriv, 0, AData.Dim,
    //                                         AData.SentDeriv, 0, AData.Dim,
    //                                         AData.Dim, AData.SentSize, 1, 0, 1);

    //        }
    //    }

    //    class ThreeWayEnsembleRunner : StructRunner
    //    {
    //        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
    //        //CudaPieceInt OutputMask;
    //        SeqDenseBatchData AData { get; set; }
    //        SeqDenseBatchData BData { get; set; }

    //        HiddenBatchData CData { get; set; }

    //        CudaPieceInt EnsembleMask1;
    //        CudaPieceInt EnsembleMask2;

    //        CudaPieceFloat tmpOutput;
    //        public ThreeWayEnsembleRunner(SeqDenseBatchData aData, SeqDenseBatchData bData, HiddenBatchData cData, RunnerBehavior behavior)
    //        : base(Structure.Empty, behavior)
    //        {
    //            AData = aData;
    //            BData = bData;
    //            CData = cData;

    //            if (aData.Dim != bData.Dim || aData.Dim != cData.Dim || bData.Dim != cData.Dim)
    //            {
    //                throw new Exception(string.Format("The dimension of a {0} b {1} c {2} should be same", aData.Dim, bData.Dim, cData.Dim));
    //            }

    //            Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 4, Behavior.Device);
    //            tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);
    //            EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
    //            for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 2 + (s * 4);
    //            EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

    //            EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
    //            for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 3 + (s * 4);
    //            EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
    //        }

    //        public override void Forward()
    //        {
    //            iteration++;
    //            //if(name.Equals("memoryEnsemble") && iteration == 31)
    //            //{
    //            //    AData.SyncToCPU();
    //            //    BData.SyncToCPU();
    //            //    CData.SyncToCPU();
    //            //}
    //            Output.BatchSize = AData.BatchSize;
    //            Output.SentSize = AData.SentSize;

    //            ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
    //                                         AData.SentOutput, 0, AData.Dim,
    //                                         Output.SentOutput, 0, Output.Dim,
    //                                         AData.Dim, AData.SentSize, 1, 0, 0);

    //            ComputeLib.Matrix_AdditionEx(BData.SentOutput, 0, BData.Dim,
    //                                         BData.SentOutput, 0, BData.Dim,
    //                                         Output.SentOutput, AData.Dim, Output.Dim,
    //                                         BData.Dim, BData.SentSize, 1, 0, 0);

    //            ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, BData.SentOutput, 0, Output.SentOutput, 0,
    //                                              CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, EnsembleMask1, 0,
    //                                              AData.SentSize, AData.Dim, 0, 1);

    //            ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
    //                                              CudaPieceInt.Empty, 0, Output.SentMargin, 0, EnsembleMask2, 0,
    //                                              AData.SentSize, AData.Dim, 0, 1);

    //            //if (name.Equals("memoryEnsemble") && iteration == 31)
    //            //{
    //            //    Output.SyncToCPU();
    //            //}
    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
    //                                          EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
    //                                          Output.SentSize, AData.Dim, 1, 1);


    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
    //                                              EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
    //                                              Output.SentSize, AData.Dim, 0, 1);

    //            ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
    //                                        Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
    //            //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
    //            //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
    //            //                                  Output.SentSize, AData.Dim, 1);


    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, BData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
    //                                          EnsembleMask1, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
    //                                          AData.SentSize, AData.Dim, 1, 1);

    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, BData.SentOutput, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
    //                                              EnsembleMask1, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
    //                                              BData.SentSize, BData.Dim, 1, 1);

    //            ComputeLib.Matrix_AdditionEx(Output.SentDeriv, AData.Dim, Output.Dim,
    //                                         BData.SentDeriv, 0, BData.Dim,
    //                                         BData.SentDeriv, 0, BData.Dim,
    //                                         BData.Dim, BData.SentSize, 1, 0, 1);

    //            ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
    //                                         AData.SentDeriv, 0, AData.Dim,
    //                                         AData.SentDeriv, 0, AData.Dim,
    //                                         AData.Dim, AData.SentSize, 1, 0, 1);

    //        }
    //    }

    //    class TwoAdvWayEnsembleRunner : StructRunner
    //    {
    //        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
    //        //CudaPieceInt OutputMask;
    //        SeqDenseBatchData AData { get; set; }

    //        HiddenBatchData CData { get; set; }

    //        CudaPieceInt EnsembleMask1;
    //        CudaPieceInt EnsembleMask2;

    //        CudaPieceFloat tmpOutput;

    //        /// <summary>
    //        /// output = [CData, AData * CData] 
    //        /// </summary>
    //        /// <param name="aData"></param>
    //        /// <param name="cData"></param>
    //        /// <param name="behavior"></param>
    //        public TwoAdvWayEnsembleRunner(SeqDenseBatchData aData, HiddenBatchData cData, RunnerBehavior behavior)
    //        : base(Structure.Empty, behavior)
    //        {
    //            AData = aData;

    //            CData = cData;

    //            Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 2, Behavior.Device);

    //            tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);

    //            EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
    //            for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 0 + (s * 2);
    //            EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

    //            EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
    //            for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 1 + (s * 2);
    //            EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
    //        }

    //        public override void Forward()
    //        {
    //            Output.BatchSize = AData.BatchSize;
    //            Output.SentSize = AData.SentSize;


    //            ComputeLib.Matrix_AdditionMask(CData.Output.Data, 0, AData.SentMargin, 0,
    //                                           CData.Output.Data, 0, AData.SentMargin, 0,
    //                                           Output.SentOutput, 0, EnsembleMask1, 0,
    //                                           CData.Dim, AData.SentSize, 1, 0, 0);

    //            ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
    //                                              CudaPieceInt.Empty, 0, Output.SentMargin, 0, EnsembleMask2, 0,
    //                                              AData.SentSize, AData.Dim, 0, 1);

    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
    //                                          EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
    //                                          Output.SentSize, AData.Dim, 1, 1);


    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
    //                                              EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
    //                                              Output.SentSize, AData.Dim, 0, 1);

    //            ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
    //                                        Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
    //            //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
    //            //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
    //            //                                  Output.SentSize, AData.Dim, 1);

    //            ComputeLib.ColumnWiseSumMask(Output.SentDeriv, 0, EnsembleMask1, 0, CudaPieceFloat.Empty,
    //                                         Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
    //        }
    //    }

    //    class TwoAdvV2WayEnsembleRunner : StructRunner
    //    {
    //        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
    //        //CudaPieceInt OutputMask;
    //        SeqDenseBatchData AData { get; set; }

    //        HiddenBatchData CData { get; set; }

    //        //CudaPieceInt EnsembleMask1;
    //        CudaPieceInt EnsembleMask2;

    //        CudaPieceFloat tmpOutput;

    //        /// <summary>
    //        /// output = [AData, AData * CData]
    //        /// </summary>
    //        /// <param name="aData"></param>
    //        /// <param name="cData"></param>
    //        /// <param name="behavior"></param>
    //        public TwoAdvV2WayEnsembleRunner(SeqDenseBatchData aData, HiddenBatchData cData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            AData = aData;

    //            CData = cData;

    //            if (AData.Dim != CData.Dim) { throw new Exception(string.Format("dimension adata {0} is not the same as cdata {1}", AData.Dim, CData.Dim)); }

    //            Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 2, Behavior.Device);

    //            tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);

    //            //EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
    //            //for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 0 + (s * 2);
    //            //EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

    //            EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
    //            for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 1 + (s * 2);
    //            EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
    //        }

    //        public override void Forward()
    //        {
    //            Output.BatchSize = AData.BatchSize;
    //            Output.SentSize = AData.SentSize;

    //            //if(name.StartsWith("ensemble_memory"))
    //            //{
    //            //    AData.SentOutput.SyncToCPU(AData.SentSize * AData.Dim);
    //            //    float al1 = AData.SentOutput.MemPtr.Take(AData.SentSize * AData.Dim).Select(i => Math.Abs(i)).Sum();
    //            //    float al2 = AData.SentOutput.MemPtr.Take(AData.SentSize * AData.Dim).Select(i => i * i).Sum();

    //            //    CData.Output.Data.SyncToCPU(CData.BatchSize * CData.Dim);
    //            //    float cl1 = CData.Output.Data.MemPtr.Take(CData.BatchSize * CData.Dim).Select(i => Math.Abs(i)).Sum();
    //            //    float cl2 = CData.Output.Data.MemPtr.Take(CData.BatchSize * CData.Dim).Select(i => i * i).Sum();

    //            //    Console.WriteLine("name {0}, al1 {1}, al2 {2}, cl1 {3}, cl2 {4}", name, al1, al2, cl1, cl2);

    //            //    ComputeLib.Zero(Output.SentOutput, Output.Dim * Output.SentSize);
    //            //    Output.SentOutput.SyncToCPU(Output.SentSize * Output.Dim);
    //            //    Cudalib.TestCuda();
    //            //    AData.SentMargin.SyncToCPU();   
    //            //}

    //            ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
    //                                         AData.SentOutput, 0, AData.Dim,
    //                                         Output.SentOutput, 0, Output.Dim,
    //                                         AData.Dim, AData.SentSize, 1, 0, 0);

    //            //ComputeLib.Matrix_AdditionMask(AData.SentOutput, 0, CudaPieceInt.Empty, 0,
    //            //                               AData.SentOutput, 0, CudaPieceInt.Empty, 0,
    //            //                               Output.SentOutput, 0, EnsembleMask1, 0,
    //            //                               AData.Dim, AData.SentSize, 1, 0, 0);
    //            //if (name.StartsWith("ensemble_memory"))
    //            //{
    //            //    Output.SentOutput.SyncToCPU(Output.SentSize * Output.Dim);
    //            //    float ol1 = Output.SentOutput.MemPtr.Take(Output.SentSize * Output.Dim).Select(i => Math.Abs(i)).Sum();
    //            //    float ol2 = Output.SentOutput.MemPtr.Take(Output.SentSize * Output.Dim).Select(i => i * i).Sum();
    //            //    Cudalib.TestCuda();
    //            //    Console.WriteLine("name {0}, ol1 {1}, ol2 {2}", name, ol1, ol2);

    //            //    EnsembleMask2.SyncToCPU();
    //            //}

    //            ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
    //                                              CudaPieceInt.Empty, 0, AData.SentMargin, 0, EnsembleMask2, 0,
    //                                              AData.SentSize, AData.Dim, 0, 1);


    //            //if (name.StartsWith("ensemble_memory"))
    //            //{
    //            //    Output.SentOutput.SyncToCPU(Output.SentSize * Output.Dim);
    //            //    float ol1 = Output.SentOutput.MemPtr.Take(Output.SentSize * Output.Dim).Select(i => Math.Abs(i)).Sum();
    //            //    float ol2 = Output.SentOutput.MemPtr.Take(Output.SentSize * Output.Dim).Select(i => i * i).Sum();

    //            //    Console.WriteLine("name {0}, ol1 {1}, ol2 {2}", name, ol1, ol2);
    //            //}
    //        }

    //        public override void CleanDeriv()
    //        {
    //            ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
    //        }

    //        public override void Backward(bool cleanDeriv)
    //        {
    //            //EnsembleMask2.SyncToCPU();

    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
    //                                              EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
    //                                              Output.SentSize, AData.Dim, 0, 1);

    //            ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
    //                                        Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
    //            //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
    //            //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
    //            //                                  Output.SentSize, AData.Dim, 1);

    //            ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
    //                                          EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
    //                                          Output.SentSize, AData.Dim, 1, 1);

    //            ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
    //                                         AData.SentDeriv, 0, AData.Dim,
    //                                         AData.SentDeriv, 0, AData.Dim,
    //                                         AData.Dim, AData.SentSize, 1, 0, 1);

    //            //ComputeLib.Matrix_AdditionMask(Output.SentDeriv, 0, EnsembleMask1, 0,
    //            //                               AData.SentDeriv, 0, CudaPieceInt.Empty, 0,
    //            //                               AData.SentDeriv, 0, CudaPieceInt.Empty, 0,
    //            //                               AData.Dim, AData.SentSize, 1, 0, 1);
    //            //EnsembleMask1.SyncToCPU();
    //            //ComputeLib.ColumnWiseSumMask(Output.SentDeriv, 0, EnsembleMask1, 0, CudaPieceFloat.Empty,
    //            //                             Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
    //        }
    //    }

    //    class ExactMatchFeatureRunner : StructRunner
    //    {
    //        public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
    //        //CudaPieceInt OutputMask;
    //        SeqSparseBatchData Tgt { get; set; }
    //        SeqSparseBatchData Src { get; set; }

    //        ItemFreqIndexDictionary Dict { get; set; }
    //        /// <summary>
    //        /// output = [AData, AData * CData]
    //        /// </summary>
    //        /// <param name="aData"></param>
    //        /// <param name="cData"></param>
    //        /// <param name="behavior"></param>
    //        public ExactMatchFeatureRunner(SeqSparseBatchData tgt, SeqSparseBatchData src, ItemFreqIndexDictionary dict, RunnerBehavior behavior) : base(Structure.Empty, behavior)
    //        {
    //            Src = src;
    //            Tgt = tgt;
    //            Dict = dict;
    //            Output = new SeqDenseBatchData(src.Stat, src.SampleIdx, src.SentMargin, new CudaPieceFloat(2 * src.Stat.MAX_SEQUENCESIZE, behavior.Device), null, behavior.Device);
    //        }

    //        public override void Forward()
    //        {
    //            Output.BatchSize = Src.BatchSize;
    //            Output.SentSize = Src.SentSize;

    //            Src.SampleIdx.SyncToCPU();
    //            Src.FeaIdx.SyncToCPU();

    //            Tgt.SampleIdx.SyncToCPU();
    //            Tgt.FeaIdx.SyncToCPU();

    //            for (int b = 0; b < Src.BatchSize; b++)
    //            {
    //                HashSet<int> wordIdx = new HashSet<int>();
    //                HashSet<string> words = new HashSet<string>();

    //                int q_start = b == 0 ? 0 : Tgt.SampleIdx.MemPtr[b - 1];
    //                int q_end = Tgt.SampleIdx.MemPtr[b];
    //                for (int q = q_start; q < q_end; q++)
    //                {
    //                    int wid = Tgt.FeaIdx.MemPtr[q];
    //                    string w = Dict.ItemIndex.NameList[wid].ToLower();

    //                    if (!wordIdx.Contains(wid)) wordIdx.Add(wid);
    //                    if (!words.Contains(w)) words.Add(w);
    //                }

    //                int d_start = b == 0 ? 0 : Src.SampleIdx.MemPtr[b - 1];
    //                int d_end = Src.SampleIdx.MemPtr[b];
    //                for (int d = d_start; d < d_end; d++)
    //                {
    //                    int wid = Src.FeaIdx.MemPtr[d];
    //                    string w = Dict.ItemIndex.NameList[wid].ToLower();

    //                    if (wordIdx.Contains(wid)) Output.SentOutput.MemPtr[d * 2 + 0] = 1;
    //                    else Output.SentOutput.MemPtr[d * 2 + 0] = 0;

    //                    if (words.Contains(w)) Output.SentOutput.MemPtr[d * 2 + 1] = 1;
    //                    else Output.SentOutput.MemPtr[d * 2 + 1] = 0;
    //                }
    //            }

    //            Output.SentOutput.SyncFromCPU();
    //        }
    //    }

    //    public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
    //                                                         IDataCashier<SeqSparseBatchData, SequenceDataStat> query,

    //                                                         IDataCashier<SeqSparseBatchData, SequenceDataStat> contextchar,
    //                                                         IDataCashier<SeqSparseBatchData, SequenceDataStat> querychar,

    //                                                         DataCashier<DenseBatchData, DenseDataStat> startingPos,
    //                                                         DataCashier<DenseBatchData, DenseDataStat> endPos,

    //                                                         // text embedding cnn.
    //                                                         LayerStructure EmbedCNN,
    //                                                         List<LayerStructure> CharEmbedCNN,

    //                                                         List<LayerStructure> HighwayGate,
    //                                                         List<LayerStructure> HighwayConnect,

    //                                                         // multilayer lstm embedding.
    //                                                         List<Tuple<LSTMStructure, LSTMStructure>> EmbedLSTM,

    //                                                         LayerStructure AnsPoolStruct, LayerStructure AnsStartStruct, LayerStructure AnsEndStruct,
    //                                                         List<LayerStructure> TermStartStruct, List<LayerStructure> TermEndStruct,

    //                                                         LayerStructure AlignLayer, int AlignOp, A_Func AlignFunc,
    //                                                         Tuple<LSTMStructure, LSTMStructure> QueryLSTM,

    //                                                         // model.
    //                                                         CompositeNNStructure model, RunnerBehavior Behavior, string resultFile)
    //    {
    //        ComputationGraph cg = new ComputationGraph();


    //        /************************************************************ Query Passage data *********/
    //        #region Query Doc Answer Data.
    //        SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
    //        SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
    //        SeqSparseBatchData QuerycharData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(querychar, Behavior));
    //        SeqSparseBatchData ContextcharData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(contextchar, Behavior));

    //        DenseBatchData StartPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(startingPos, Behavior));
    //        DenseBatchData EndPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(endPos, Behavior));

    //        #endregion.

    //        /************************************************************ Exact Match Feature on Passage. *********/
    //        //SeqDenseBatchData DExactFea = (SeqDenseBatchData)cg.AddRunner(new ExactMatchFeatureRunner(QueryData, ContextData, dict, Behavior) { IsBackProp = false, IsUpdate = false });

    //        #region Word Embedding on Q and D .

    //        /************************************************************ Word Embedding on Query. *********/
    //        SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, QueryData, Behavior) { IsBackProp = false, IsUpdate = false });
    //        if (BuilderParameters.WORD_DROPOUT > 0 && Behavior.RunMode == DNNRunMode.Train)
    //        {
    //            cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(new MatrixData(QueryEmbedOutput)), BuilderParameters.WORD_DROPOUT, Behavior, false));
    //        }

    //        /************************************************************ Word Embedding on Passage. *********/
    //        SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, ContextData, Behavior) { IsBackProp = false, IsUpdate = false });
    //        if (BuilderParameters.WORD_DROPOUT > 0 && Behavior.RunMode == DNNRunMode.Train)
    //        {
    //            cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(new MatrixData(ContextEmbedOutput)), BuilderParameters.WORD_DROPOUT, Behavior, false));
    //        }
    //        #endregion.

    //        #region Character Embedding Q and D .
    //        if (CharEmbedCNN.Count > 0)
    //        {
    //            /******************************************************** Query Character Embedding. **************/
    //            SeqDenseBatchData letterQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
    //                CharEmbedCNN[0], QuerycharData, Behavior)
    //            { name = "char_cnn_query" });
    //            for (int i = 1; i < CharEmbedCNN.Count; i++)
    //            {
    //                letterQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(
    //                    CharEmbedCNN[i], letterQueryEmbed, Behavior));
    //            }
    //            HiddenBatchData letterQueryMaxpool = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(letterQueryEmbed, Behavior) { name = "char_maxpool_query" });
    //            SeqDenseBatchData charQueryEmbed = new SeqDenseBatchData(
    //                new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, letterQueryMaxpool.Dim),
    //                QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
    //                letterQueryMaxpool.Output.Data, letterQueryMaxpool.Deriv.Data, Behavior.Device);
    //            if (BuilderParameters.CHARDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
    //            {
    //                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(new MatrixData(charQueryEmbed)), BuilderParameters.CHARDropout, Behavior, false));
    //            }
    //            QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
    //                new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QueryEmbedOutput, charQueryEmbed }, Behavior));


    //            /******************************************************** Doc Character Embedding. **************/
    //            SeqDenseBatchData letterContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
    //                CharEmbedCNN[0], ContextcharData, Behavior)
    //            { name = "char_cnn_doc" });
    //            for (int i = 1; i < CharEmbedCNN.Count; i++)
    //            {
    //                letterContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(
    //                    CharEmbedCNN[i], letterContextEmbed, Behavior));
    //            }
    //            HiddenBatchData letterContextMaxpool = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(letterContextEmbed, Behavior));
    //            SeqDenseBatchData charContextEmbed = new SeqDenseBatchData(
    //                //ContextEmbedOutput.Stat, 
    //                new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, letterContextMaxpool.Dim),
    //                ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
    //                letterContextMaxpool.Output.Data, letterContextMaxpool.Deriv.Data, Behavior.Device);
    //            if (BuilderParameters.CHARDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
    //            {
    //                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(new MatrixData(charContextEmbed)), BuilderParameters.CHARDropout, Behavior, false));
    //            }
    //            ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
    //                new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextEmbedOutput, charContextEmbed }, Behavior) { name = "_char" });
    //        }
    //        #endregion.

    //        /************************************************************ Highway net on Query and Passage Data *********/
    //        #region Highway Q and D .
    //        int highWayLayer = BuilderParameters.HighWay_Layer;
    //        if (highWayLayer > 0)
    //        {
    //            HiddenBatchData qtmpembed = new HiddenBatchData(QueryEmbedOutput.MAX_SENTSIZE, QueryEmbedOutput.Dim,
    //                                                            QueryEmbedOutput.SentOutput, QueryEmbedOutput.SentDeriv, Behavior.Device);

    //            HiddenBatchData dtmpembed = new HiddenBatchData(ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim,
    //                                                            ContextEmbedOutput.SentOutput, ContextEmbedOutput.SentDeriv, Behavior.Device);

    //            qtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayNetRunner(HighwayConnect, HighwayGate, qtmpembed, Behavior) { name = "queryEbd" });
    //            dtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayNetRunner(HighwayConnect, HighwayGate, dtmpembed, Behavior) { name = "docEbd" });

    //            if (BuilderParameters.EMBEDDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
    //            {
    //                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(qtmpembed, BuilderParameters.EMBEDDropout, Behavior, false));
    //                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(dtmpembed, BuilderParameters.EMBEDDropout, Behavior, false));
    //            }

    //            QueryEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, qtmpembed.Dim),
    //                                                                QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
    //                                                                qtmpembed.Output.Data, qtmpembed.Deriv.Data, Behavior.Device);
    //            ContextEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, dtmpembed.Dim),
    //                                                                ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
    //                                                                dtmpembed.Output.Data, dtmpembed.Deriv.Data, Behavior.Device);
    //        }
    //        #endregion.

    //        #region Multilayer LSTM for Q and D.
    //        List<SeqDenseBatchData> multiLayer_Qs = new List<SeqDenseBatchData>();
    //        List<SeqDenseBatchData> multiLayer_Ds = new List<SeqDenseBatchData>();

    //        multiLayer_Qs.Add(QueryEmbedOutput);
    //        multiLayer_Ds.Add(ContextEmbedOutput);

    //        for (int l = 0; l < EmbedLSTM.Count; l++)
    //        {
    //            ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(EmbedLSTM[l].Item1, EmbedLSTM[l].Item2, ContextEmbedOutput, Behavior) { name = "DocSeqO Layer_" + l.ToString() });
    //            QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(EmbedLSTM[l].Item1, EmbedLSTM[l].Item2, QueryEmbedOutput, Behavior) { name = "QuerySeqO Layer_" + l.ToString() });

    //            multiLayer_Qs.Add(QueryEmbedOutput);
    //            multiLayer_Ds.Add(ContextEmbedOutput);
    //        }
    //        #endregion.

    //        // multilayer lstm net.

    //        SeqDenseBatchData Query = multiLayer_Qs.Last();
    //        SeqDenseBatchData Doc = multiLayer_Ds.Last();
    //        BiMatchBatchData q2dMatchData = (BiMatchBatchData)cg.AddRunner(new CrossSeqBiMatchRunner(Query, Doc, DataPanel.MaxCrossQDLen, Behavior));
    //        BiMatchBatchData d2qMatchData = (BiMatchBatchData)cg.AddRunner(new CrossSeqBiMatchRunner(Doc, Query, DataPanel.MaxCrossQDLen, Behavior));


    //        SeqAlignmentRunnerV2 alignRunner = new SeqAlignmentRunnerV2(new MatrixData(Doc), new MatrixData(Query), d2qMatchData, AlignLayer, Behavior, AlignOp, AlignFunc);
    //        cg.AddRunner(alignRunner);
    //        MemoryRetrievalRunner docRunner = new MemoryRetrievalRunner(alignRunner.Output, Query, d2qMatchData, 1, Behavior);
    //        cg.AddRunner(docRunner);
    //        SeqDenseBatchData dAlignFea = new SeqDenseBatchData(new SequenceDataStat(Doc.MAX_BATCHSIZE, Doc.MAX_SENTSIZE, Doc.Dim), Doc.SampleIdx, Doc.SentMargin,
    //                                                            docRunner.Output.Output.Data, docRunner.Output.Deriv.Data, Behavior.Device);

    //        EnsembleConcateSeqRunner newIRunner = new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { Doc, dAlignFea }, Behavior) { name = "d_merge_q" };
    //        cg.AddRunner(newIRunner);

    //        BiLSTMRunner newQRunner = new BiLSTMRunner(QueryLSTM.Item1, QueryLSTM.Item2, newIRunner.Output, Behavior);
    //        cg.AddRunner(newQRunner);

    //        ReasonNetV3Runner reasonet = new ReasonNetV3Runner(cg, Query, Doc, q2dMatchData, BuilderParameters.RECURRENT_STEP,
    //            new Tuple<LayerStructure, LayerStructure, LayerStructure>(AnsPoolStruct, AnsStartStruct, AnsEndStruct),
    //            new Tuple<List<LayerStructure>, List<LayerStructure>>(TermStartStruct, TermEndStruct),
    //            new Tuple<LayerStructure, int, A_Func>(AlignLayer, AlignOp, AlignFunc),
    //            QueryLSTM, Behavior);
    //        cg.AddRunner(reasonet);

    //        switch (Behavior.RunMode)
    //        {
    //            case DNNRunMode.Train:
    //                cg.AddObjective(new BayesianContrastiveRewardV2Runner(StartPos.Data, reasonet.AnsStartData, reasonet.StartAnswerProb, 1, Behavior));
    //                cg.AddObjective(new BayesianContrastiveRewardV2Runner(EndPos.Data, reasonet.AnsEndData, reasonet.EndAnswerProb, 1, Behavior));
    //                break;
    //            case DNNRunMode.Predict:
    //                cg.AddRunner(new ExactMatchPredictionRunner(reasonet.FinalStartAns, reasonet.FinalEndAns, DataPanel.ValidResults, Behavior, BuilderParameters.ScoreOutputPath, false));
    //                break;
    //        }

    //        return cg;
    //    }


    //    public override void Rock()
    //    {
    //        Logger.OpenLog(BuilderParameters.LogFile, !DeepNet.BuilderParameters.IsLogFile);

    //        DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
    //        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
    //        if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

    //        Logger.WriteLog("Loading Training/Test Data.");
    //        DataPanel.Init();
    //        Logger.WriteLog("Load Training/Test Data Finished.");

    //        CompositeNNStructure modelStructure = new CompositeNNStructure();

    //        // char embedding layer.
    //        List<LayerStructure> charembedLayer = new List<LayerStructure>();

    //        // highway struct.
    //        List<LayerStructure> highwayConnect = new List<LayerStructure>();
    //        List<LayerStructure> highwayGate = new List<LayerStructure>();

    //        // lstm struct.
    //        List<Tuple<LSTMStructure, LSTMStructure>> lstmStruct = new List<Tuple<LSTMStructure, LSTMStructure>>();

    //        // answer struct.
    //        LayerStructure AnsPoolStruct = null;
    //        LayerStructure AnsStartStruct = null;
    //        LayerStructure AnsEndStruct = null;

    //        // terminate struct.
    //        List<LayerStructure> StartTermalStructs = new List<LayerStructure>();
    //        List<LayerStructure> EndTermalStructs = new List<LayerStructure>();

    //        LayerStructure AlignLayer = null;
    //        int AlignOp = 1;
    //        A_Func AlignF = A_Func.Linear;

    //        Tuple<LSTMStructure, LSTMStructure> QueryLstm = null;

    //        bool IsCreateModel = true;
    //        BinaryReader modelReader = null;
    //        if (!BuilderParameters.SeedModel.Equals(string.Empty))
    //        {
    //            modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read));
    //            int modelNum = CompositeNNStructure.DeserializeModelCount(modelReader);
    //            IsCreateModel = false;
    //        }

    //        {
    //            int wordembedDim = BuilderParameters.EMBED_LAYER_DIM.Last();
    //            int charembedDim = BuilderParameters.CHAR_EMBED_DIM.Last();
    //            if (charembedDim > 0)
    //            {
    //                int iDim = DataPanel.CharSize;
    //                for (int i = 0; i < BuilderParameters.CHAR_EMBED_DIM.Length; i++)
    //                {
    //                    LayerStructure charEmbed = IsCreateModel ?
    //                        modelStructure.AddLayer(new LayerStructure(iDim, BuilderParameters.CHAR_EMBED_DIM[i], A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.CHAR_EMBED_WIN[i], 0, false, device)) :
    //                        modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

    //                    iDim = BuilderParameters.CHAR_EMBED_DIM[i];
    //                    charembedLayer.Add(charEmbed);
    //                }
    //            }
    //            Logger.WriteLog("Word Embedding {0}, Char Embedding {1}", wordembedDim, charembedDim);
    //            int embedDim = wordembedDim + charembedDim;
    //            Logger.WriteLog("Total Embedding {0}", embedDim);

    //            for (int i = 0; i < BuilderParameters.HighWay_Layer; i++)
    //            {
    //                highwayConnect.Add(IsCreateModel ? modelStructure.AddLayer(new LayerStructure(embedDim, embedDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true, device)) :
    //                                                   modelStructure.DeserializeModel<LayerStructure>(modelReader, device));

    //                highwayGate.Add(IsCreateModel ? modelStructure.AddLayer(new LayerStructure(embedDim, embedDim, A_Func.Sigmoid, N_Type.Fully_Connected, 1, 0, true, device)) :
    //                                                modelStructure.DeserializeModel<LayerStructure>(modelReader, device));
    //            }
    //            int inputDim = embedDim;

    //            for (int i = 0; i < BuilderParameters.LSTM_DIM.Length; i++)
    //            {
    //                lstmStruct.Add(new Tuple<LSTMStructure, LSTMStructure>(
    //                   IsCreateModel ? modelStructure.AddLayer(new LSTMStructure(inputDim, new int[] { BuilderParameters.LSTM_DIM[i] }, device, BuilderParameters.RndInit)) :
    //                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device),
    //                   IsCreateModel ? modelStructure.AddLayer(new LSTMStructure(inputDim, new int[] { BuilderParameters.LSTM_DIM[i] }, device, BuilderParameters.RndInit)) :
    //                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device)
    //                   ));

    //                inputDim = BuilderParameters.LSTM_DIM[i] * 2;
    //            }

    //            AnsPoolStruct = IsCreateModel ? modelStructure.AddLayer(new LayerStructure(inputDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device)) :
    //                                            modelStructure.DeserializeModel<LayerStructure>(modelReader, device);
    //            AnsStartStruct = IsCreateModel ? modelStructure.AddLayer(new LayerStructure(inputDim, inputDim, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device)) :
    //                                             modelStructure.DeserializeModel<LayerStructure>(modelReader, device);
    //            AnsEndStruct = IsCreateModel ? modelStructure.AddLayer(new LayerStructure(inputDim, inputDim, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device)) :
    //                                           modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

    //            {
    //                int input = inputDim;
    //                for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
    //                {
    //                    StartTermalStructs.Add(
    //                        IsCreateModel ? modelStructure.AddLayer(new LayerStructure(input, BuilderParameters.T_NET[i], i == BuilderParameters.T_NET.Length - 1 ? A_Func.Linear : BuilderParameters.T_AF, N_Type.Fully_Connected, 1, 0, false, device)) :
    //                                        modelStructure.DeserializeModel<LayerStructure>(modelReader, device));

    //                    EndTermalStructs.Add(
    //                        IsCreateModel ? modelStructure.AddLayer(new LayerStructure(input, BuilderParameters.T_NET[i], i == BuilderParameters.T_NET.Length - 1 ? A_Func.Linear : BuilderParameters.T_AF, N_Type.Fully_Connected, 1, 0, false, device)) :
    //                                        modelStructure.DeserializeModel<LayerStructure>(modelReader, device));

    //                    input = BuilderParameters.T_NET[i];
    //                }
    //            }

    //            AlignLayer = IsCreateModel ? modelStructure.AddLayer(new LayerStructure(inputDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device)) :
    //                                         modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

    //            Console.WriteLine("QLstm input {0}, output {1}", inputDim * 2, inputDim / 2);
    //            QueryLstm = new Tuple<LSTMStructure, LSTMStructure>(
    //                    IsCreateModel ? modelStructure.AddLayer(new LSTMStructure(inputDim * 2, new int[] { inputDim / 2 }, device, BuilderParameters.RndInit)) :
    //                                    modelStructure.DeserializeModel<LSTMStructure>(modelReader, device),

    //                    IsCreateModel ? modelStructure.AddLayer(new LSTMStructure(inputDim * 2, new int[] { inputDim / 2 }, device, BuilderParameters.RndInit)) :
    //                                    modelStructure.DeserializeModel<LSTMStructure>(modelReader, device));
    //        }

    //        modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

    //        if (modelReader != null) modelReader.Close();

    //        switch (BuilderParameters.RunMode)
    //        {
    //            case DNNRunMode.Train:
    //                ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainContextChar, DataPanel.TrainQueryChar, DataPanel.TrainStartPos, DataPanel.TrainEndPos,
    //                    DataPanel.InitWordEmbedLayers(DataPanel.trainWordDict, device), charembedLayer, highwayGate, highwayConnect, lstmStruct,
    //                    AnsPoolStruct, AnsStartStruct, AnsEndStruct, StartTermalStructs, EndTermalStructs, AlignLayer, AlignOp, AlignF, QueryLstm, modelStructure,
    //                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, string.Empty);
    //                trainCG.SetDelegateModel(modelStructure);

    //                ComputationGraph validCG = null;
    //                if (BuilderParameters.IsValidFile)
    //                    validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidContextChar, DataPanel.ValidQueryChar, DataPanel.ValidStartPos, DataPanel.ValidEndPos,
    //                        DataPanel.InitWordEmbedLayers(DataPanel.devWordDict, device), charembedLayer, highwayGate, highwayConnect, lstmStruct,
    //                        AnsPoolStruct, AnsStartStruct, AnsEndStruct, StartTermalStructs, EndTermalStructs, AlignLayer, AlignOp, AlignF, QueryLstm, modelStructure,
    //                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, string.Empty);
    //                double bestValidScore = double.MinValue; // validCG.Execute(); //double.MinValue;
    //                //double bestTestScore = double.MinValue;
    //                int bestIter = -1;
    //                for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
    //                {
    //                    double loss = trainCG.Execute();
    //                    Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
    //                    if (DeepNet.BuilderParameters.ModelSavePerIteration)
    //                    {
    //                        using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
    //                        {
    //                            modelStructure.Serialize(writer);
    //                        }
    //                    }

    //                    double validScore = 0;
    //                    if (validCG != null)
    //                    {
    //                        validScore = validCG.Execute();
    //                        Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, iter);
    //                    }

    //                    if (validScore > bestValidScore)
    //                    {
    //                        bestValidScore = validScore; bestIter = iter;
    //                    }
    //                    Logger.WriteLog("Best Valid Score {0},  At iter {1}", bestValidScore, bestIter);
    //                }
    //                break;
    //            case DNNRunMode.Predict:
    //                //ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery,
    //                //    DataPanel.ValidContextL3G, DataPanel.ValidQueryL3G, DataPanel.ValidContextChar,
    //                //    DataPanel.ValidQueryChar, DataPanel.ValidStartPos, DataPanel.ValidEndPos,
    //                //        DataPanel.InitWordEmbed1Layers(DataPanel.devWordDict, device), DataPanel.InitWordEmbed2Layers(DataPanel.devWordDict, device),
    //                //        l3gembedLayer, charembedLayer, highwayGate, highwayConnect,
    //                //        contextD1LSTM, contextD2LSTM, CNNembed, queryD1LSTM, queryD2LSTM, CNNembed,
    //                //        CoAttLayer, SelfM1, SelfM2, SelfAttLayer, M1D1LSTM, M1D2LSTM, M2D1LSTM, M2D2LSTM, M3D1LSTM, M3D2LSTM,
    //                //        MemAnsStruct, StateAnsStruct,
    //                //        AnsStartStruct, AnsEndStruct, InitStateStruct, AttStruct, StateStruct,
    //                //        StartTermalStructs, EndTermalStructs, modelStructure,
    //                //        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, string.Empty);
    //                //predCG.Execute();
    //                break;
    //        }
    //        DataPanel.Deinit();
    //        Logger.CloseLog();
    //    }

    //    /// <summary>
    //    /// Data Panel.
    //    /// </summary>
    //    public class DataPanel
    //    {
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContextChar = null;
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQueryChar = null;
    //        public static DataCashier<DenseBatchData, DenseDataStat> TrainStartPos = null;
    //        public static DataCashier<DenseBatchData, DenseDataStat> TrainEndPos = null;

    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContext = null;
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQuery = null;
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContextChar = null;
    //        public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQueryChar = null;
    //        public static DataCashier<DenseBatchData, DenseDataStat> ValidStartPos = null;
    //        public static DataCashier<DenseBatchData, DenseDataStat> ValidEndPos = null;
    //        public static string[] ValidIds = null;
    //        public static List<HashSet<string>> ValidResults = new List<HashSet<string>>();
    //        public static List<List<int>> ValidWordIdxs = new List<List<int>>();

    //        public static ItemFreqIndexDictionary trainWordDict = null;
    //        public static ItemFreqIndexDictionary devWordDict = null;
    //        public static ItemFreqIndexDictionary charFreqDict = null;
    //        //public static int WordSize { get { return wordFreqDict.ItemDictSize; } }
    //        public static int CharSize { get { return charFreqDict.ItemDictSize; } }

    //        public static int MaxQueryLen = 0;
    //        public static int MaxDocLen = 0;
    //        public static int MaxCrossQDLen = 0;
    //        public static int MaxCrossDDLen = 0;
    //        public static LayerStructure InitWordEmbedLayers(ItemFreqIndexDictionary wordDict, DeviceType device)
    //        {
    //            LayerStructure wordEmbedding = null;
    //            wordEmbedding = new LayerStructure(wordDict.ItemDictSize, BuilderParameters.EMBED_LAYER_DIM[0],
    //                    BuilderParameters.EMBED_ACTIVATION[0], N_Type.Convolution_layer, 1, 0, false, device, false);

    //            #region Glove Work Embedding Initialization.
    //            if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
    //            {
    //                int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];

    //                Logger.WriteLog("Loading Glove Word Embedding ...");
    //                Dictionary<string, float[]> initEmbedding = new Dictionary<string, float[]>();
    //                using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding, Encoding.UTF8))
    //                {
    //                    while (!mreader.EndOfStream)
    //                    {
    //                        string[] items = mreader.ReadLine().Split(' ');
    //                        string word = items[0];
    //                        float[] vec = new float[items.Length - 1];
    //                        for (int i = 1; i < items.Length; i++)
    //                        {
    //                            vec[i - 1] = float.Parse(items[i]);
    //                        }
    //                        if (vec.Length != wordDim) { throw new Exception(string.Format("word {0} embedding {1} does not match with the dim {2}", word, vec.Length, wordDim)); }
    //                        if (initEmbedding.ContainsKey(word))
    //                        {
    //                            Logger.WriteLog(string.Format("line number {0} word has been added {1} continue; ", initEmbedding.Count, word));
    //                            continue;
    //                        }
    //                        initEmbedding.Add(word, vec);
    //                    }
    //                }

    //                Logger.WriteLog("Init Glove Word Embedding ...");

    //                if (BuilderParameters.UNK_INIT == 0) Logger.WriteLog("UNK is initalized to zero!");
    //                int halfWinStart = 0; // * DataPanel.WordDim;

    //                int hit = 0;
    //                int[] HitWords = new int[wordDict.ItemDictSize];
    //                foreach (KeyValuePair<string, int> item in wordDict.ItemIndex.NameDict)
    //                {
    //                    int wordIdx = item.Value;
    //                    string word = item.Key;
    //                    if (!BuilderParameters.IS_WORD_CASE) word = word.ToLower();
    //                    if (initEmbedding.ContainsKey(word))
    //                    {
    //                        float[] vec = initEmbedding[word];
    //                        HitWords[wordIdx] = 1;
    //                        for (int win = -halfWinStart; win < 1 - halfWinStart; win++)
    //                        {
    //                            for (int i = 0; i < wordDim; i++)
    //                            {
    //                                wordEmbedding.weight.MemPtr[((win + halfWinStart) * wordDict.ItemDictSize + wordIdx) * wordDim + i] = vec[i];
    //                            }
    //                        }
    //                        hit++;
    //                    }
    //                    else if (BuilderParameters.UNK_INIT == 0)
    //                    {
    //                        for (int win = -halfWinStart; win < 1 - halfWinStart; win++)
    //                        {
    //                            for (int i = 0; i < wordDim; i++)
    //                            {
    //                                wordEmbedding.weight.MemPtr[((win + halfWinStart) * wordDict.ItemDictSize + wordIdx) * wordDim + i] = 0;
    //                            }
    //                        }
    //                    }
    //                }
    //                wordEmbedding.weight.SyncFromCPU();
    //                Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, wordDict.ItemDictSize);
    //            }
    //            #endregion.

    //            return wordEmbedding;
    //        }

    //        static IEnumerable<Tuple<string, string, string>> ExtractQAS(string paraFile, string queryFile, string ansFile)
    //        {
    //            using (StreamReader paraReader = new StreamReader(paraFile))
    //            using (StreamReader queryReader = new StreamReader(queryFile))
    //            using (StreamReader ansReader = new StreamReader(ansFile))
    //            {
    //                int lineIdx = 0;
    //                while (!ansReader.EndOfStream)
    //                {
    //                    string p = paraReader.ReadLine();
    //                    string q = queryReader.ReadLine();
    //                    string a = ansReader.ReadLine();
    //                    //Console.WriteLine("{0},{1}", p, q);
    //                    yield return new Tuple<string, string, string>(p, q, a);
    //                    lineIdx += 1;
    //                }
    //                Console.WriteLine("Total Number of answers {0}", lineIdx);
    //            }
    //        }

    //        static Tuple<int, int> GetSpan(List<string[]> sentenceIdxs, int startSent, int startIdx, int endSent, int endIdx)
    //        {
    //            if (startSent == -1 || startIdx == -1 || endSent == -1 || endIdx == -1)
    //            {
    //                return new Tuple<int, int>(-1, -1);
    //            }
    //            if (sentenceIdxs.Count <= startSent || sentenceIdxs.Count <= endSent)
    //            {
    //                Console.WriteLine("Sentence number is not enough! {0}, start {1}, end {2}", sentenceIdxs.Count, startSent, endSent);
    //                Console.ReadLine();
    //            }

    //            if (startSent != endSent)
    //            {
    //                Console.WriteLine("Sentence start is not equals to Sentence end! {0} and {1}", startSent, endSent);
    //            }

    //            int answerSentStart = sentenceIdxs.Take(startSent).Sum(i => i.Length) + startIdx;
    //            int answerSentEnd = sentenceIdxs.Take(endSent).Sum(i => i.Length) + endIdx;

    //            return new Tuple<int, int>(answerSentStart, answerSentEnd);
    //        }

    //        static void ExtractCorpusBinary(string paraFile, string queryFile, string ansFile,
    //            string paraFileBin, string queryFileBin, string ansFileBin, bool isShuffle, int miniBatchSize,
    //            Dictionary<int, List<int>> word2char, int wordSize,
    //            int spanSize, List<HashSet<string>> resultSave)
    //        {
    //            SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = wordSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
    //            SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = wordSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
    //            SeqSparseBatchData contextChar = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = CharSize }, DeviceType.CPU);
    //            SeqSparseBatchData queryChar = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = CharSize }, DeviceType.CPU);

    //            DenseBatchData ansStart = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);
    //            DenseBatchData ansEnd = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);

    //            BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(paraFileBin);
    //            BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryFileBin);
    //            BinaryWriter contextCharWriter = FileUtil.CreateBinaryWrite(paraFileBin + ".char");
    //            BinaryWriter queryCharWriter = FileUtil.CreateBinaryWrite(queryFileBin + ".char");
    //            BinaryWriter ansStartWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".1");
    //            BinaryWriter ansEndWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".2");

    //            IEnumerable<Tuple<string, string, string>> QAS = ExtractQAS(paraFile, queryFile, ansFile);

    //            IEnumerable<Tuple<string, string, string>> Input = QAS;
    //            if (isShuffle) Input = CommonExtractor.RandomShuffle<Tuple<string, string, string>>(QAS, 1000000, DeepNet.BuilderParameters.RandomSeed);

    //            int crossQDLen = 0;
    //            int crossDDLen = 0;
    //            int lineIdx = 0;
    //            int overSpanfilterNum = 0;
    //            int notFoundfilterNum = 0;
    //            int overLengthfilterNum = 0;
    //            foreach (Tuple<string, string, string> smp in Input)
    //            {
    //                string p = smp.Item1;
    //                string q = smp.Item2;
    //                string a = smp.Item3;

    //                string[] poses = a.Split(new char[] { ',', ')', '(', '#' }, StringSplitOptions.RemoveEmptyEntries);
    //                List<string[]> sentenceIdxs = p.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries).
    //                    Select(i => i.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)).ToList();
    //                string[] queryIdxs = q.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

    //                int answerSentStart = -1;
    //                int answerSentEnd = -1;
    //                if (resultSave != null) { resultSave.Add(new HashSet<string>()); }
    //                for (int m = 0; m < poses.Length / 4; m++)
    //                {
    //                    int startSent = int.Parse(poses[4 * m + 0]);
    //                    int startIdx = int.Parse(poses[4 * m + 1]);
    //                    int endSent = int.Parse(poses[4 * m + 2]);
    //                    int endIdx = int.Parse(poses[4 * m + 3]);
    //                    Tuple<int, int> r = GetSpan(sentenceIdxs, startSent, startIdx, endSent, endIdx);
    //                    answerSentStart = r.Item1;
    //                    answerSentEnd = r.Item2;

    //                    if (resultSave != null) { resultSave[lineIdx].Add(string.Format("{0}#{1}", answerSentStart, answerSentEnd)); }
    //                }

    //                if (spanSize > 0 && answerSentEnd - answerSentStart + 1 > spanSize) { overSpanfilterNum++; continue; }
    //                if (resultSave == null && (answerSentEnd == -1 || answerSentStart == -1)) { notFoundfilterNum++; continue; }
    //                if (resultSave == null && BuilderParameters.MAX_D_LENGTH > 0 && (answerSentStart >= BuilderParameters.MAX_D_LENGTH || answerSentEnd >= BuilderParameters.MAX_D_LENGTH)) { overLengthfilterNum++; continue; }

    //                List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();

    //                int cpushCount = 0;
    //                int preBatchSize = contextChar.BatchSize;
    //                foreach (string[] term in sentenceIdxs)
    //                {
    //                    foreach (string id in term)
    //                    {
    //                        int termIdx = int.Parse(id);
    //                        var tmp = new Dictionary<int, float>();
    //                        tmp[termIdx] = 1;
    //                        contextFea.Add(tmp);

    //                        List<Dictionary<int, float>> contextCharFea = new List<Dictionary<int, float>>();
    //                        foreach (int cid in word2char[termIdx])
    //                        {
    //                            var chartmp = new Dictionary<int, float>();
    //                            if (cid == -1) chartmp[0] = 0;
    //                            else chartmp[cid] = 1;
    //                            contextCharFea.Add(chartmp);
    //                        }
    //                        if (contextCharFea.Count == 0)
    //                        {
    //                            var tmpd = new Dictionary<int, float>();
    //                            tmpd.Add(0, 0);
    //                            contextCharFea.Add(tmpd);
    //                        }
    //                        cpushCount++;
    //                        contextChar.PushSample(contextCharFea);
    //                    }

    //                    if (resultSave == null && BuilderParameters.MAX_D_LENGTH > 0 && contextFea.Count >= BuilderParameters.MAX_D_LENGTH)
    //                    {
    //                        break;
    //                    }
    //                }

    //                List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
    //                int qpushCount = 0;
    //                foreach (string term in queryIdxs)
    //                {
    //                    int termIdx = int.Parse(term);
    //                    var tmp = new Dictionary<int, float>();
    //                    tmp[termIdx] = 1;
    //                    queryFea.Add(tmp);

    //                    List<Dictionary<int, float>> queryCharFea = new List<Dictionary<int, float>>();
    //                    foreach (int cid in word2char[termIdx])
    //                    {
    //                        var chartmp = new Dictionary<int, float>();
    //                        if (cid == -1) chartmp[0] = 0;
    //                        else chartmp[cid] = 1;
    //                        queryCharFea.Add(chartmp);
    //                    }
    //                    if (queryCharFea.Count == 0)
    //                    {
    //                        var tmpd = new Dictionary<int, float>();
    //                        tmpd.Add(0, 0);
    //                        queryCharFea.Add(tmpd);
    //                    }
    //                    qpushCount++;
    //                    queryChar.PushSample(queryCharFea);
    //                }


    //                float[] tmpAnsStart = new float[contextFea.Count];
    //                float[] tmpAnsEnd = new float[contextFea.Count];
    //                for (int i = 0; i < contextFea.Count; i++)
    //                {
    //                    tmpAnsStart[i] = 0;
    //                    tmpAnsEnd[i] = 0;
    //                    if (i == answerSentStart) { tmpAnsStart[i] = 1; }
    //                    if (i == answerSentEnd) { tmpAnsEnd[i] = 1; }
    //                }

    //                if (contextFea.Count > MaxDocLen) { MaxDocLen = contextFea.Count; }
    //                if (queryFea.Count > MaxQueryLen) { MaxQueryLen = queryFea.Count; }
    //                crossQDLen += contextFea.Count * queryFea.Count;
    //                crossDDLen += contextFea.Count * contextFea.Count;
    //                if (crossQDLen > MaxCrossQDLen) { MaxCrossQDLen = crossQDLen; }
    //                if (crossDDLen > MaxCrossDDLen) { MaxCrossDDLen = crossDDLen; }
    //                context.PushSample(contextFea);
    //                query.PushSample(queryFea);
    //                ansStart.PushSample(tmpAnsStart, contextFea.Count);
    //                ansEnd.PushSample(tmpAnsEnd, contextFea.Count);

    //                if (query.SentSize != queryChar.BatchSize) { throw new Exception(string.Format("query char size should equal {0} and {1}", query.SentSize, queryChar.BatchSize)); }
    //                if (context.SentSize != contextChar.BatchSize) { throw new Exception(string.Format("context char size should equal {0} and {1}", context.SentSize, contextChar.BatchSize)); }

    //                if (answerSentEnd < answerSentStart)
    //                {
    //                    Console.WriteLine("answer sent end smaller than answer sent start {0} {1} {2} ### {3}", lineIdx, answerSentEnd, answerSentStart, string.Join("##", poses));
    //                    Console.ReadLine();
    //                }
    //                if (context.BatchSize >= miniBatchSize)
    //                {
    //                    context.PopBatchToStat(contextWriter);
    //                    query.PopBatchToStat(queryWriter);
    //                    contextChar.PopBatchToStat(contextCharWriter);
    //                    queryChar.PopBatchToStat(queryCharWriter);
    //                    ansStart.PopBatchToStat(ansStartWriter);
    //                    ansEnd.PopBatchToStat(ansEndWriter);
    //                    crossQDLen = 0;
    //                    crossDDLen = 0;
    //                }

    //                if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }

    //                if (resultSave != null)
    //                {
    //                    ValidWordIdxs.Add(contextFea.Select(i => i.Select(k => k.Key)).SelectMany(v => v).ToList());
    //                }
    //            }
    //            context.PopBatchCompleteStat(contextWriter);
    //            query.PopBatchCompleteStat(queryWriter);
    //            contextChar.PopBatchCompleteStat(contextCharWriter);
    //            queryChar.PopBatchCompleteStat(queryCharWriter);
    //            ansStart.PopBatchCompleteStat(ansStartWriter);
    //            ansEnd.PopBatchCompleteStat(ansEndWriter);

    //            if (resultSave != null)
    //            {
    //                using (StreamWriter ansWriter = new StreamWriter("tmp.pos.index"))
    //                {
    //                    foreach (HashSet<string> results in resultSave)
    //                    {
    //                        ansWriter.WriteLine(string.Join("\t", results.ToArray()));
    //                    }
    //                }
    //            }
    //            Console.WriteLine("Context Stat {0}", context.Stat.ToString());
    //            Console.WriteLine("Query Stat {0}", query.Stat.ToString());
    //            Console.WriteLine("Context char Stat {0}", contextChar.Stat.ToString());
    //            Console.WriteLine("Query char Stat {0}", queryChar.Stat.ToString());
    //            Console.WriteLine("Answer Start Stat {0}", ansStart.Stat.ToString());
    //            Console.WriteLine("Answer End Stat {0}", ansEnd.Stat.ToString());
    //            Console.WriteLine("Max Q Len {0}, Max D Len {1}, Max QD Cross Len {2}, Max DD Cross Len {3}", MaxQueryLen, MaxDocLen, MaxCrossQDLen, MaxCrossDDLen);
    //            Console.WriteLine("OverSpan Filter Number {0}, Not Found Filter Number {1}, Over Length Filter Number {2}", overSpanfilterNum, notFoundfilterNum, overLengthfilterNum);
    //        }

    //        public static ItemFreqIndexDictionary Extract_CHAR_Dict(ItemFreqIndexDictionary wordDict)
    //        {
    //            SimpleTextTokenizer textTokenize = new SimpleTextTokenizer() { IgnoreCase = false, TrimSpace = false };
    //            ItemFreqIndexDictionary chardict = new ItemFreqIndexDictionary("char dictionary");
    //            foreach (KeyValuePair<string, int> wordItem in wordDict.ItemIndex.NameDict)
    //            {
    //                int wordIdx = wordItem.Value;
    //                string wordStr = wordItem.Key;
    //                //word2char.Add(wordIdx, new List<int>());
    //                IEnumerable<string> strList = textTokenize.LetterTokenize(wordStr);
    //                foreach (string c in strList)
    //                {
    //                    if (BuilderParameters.IS_CHAR_CASE) chardict.PushItem(c); // ItemIndex.Index(c, true);
    //                    else chardict.PushItem(c.ToLower());
    //                    //word2char[wordIdx].Add(cIdx);
    //                }
    //                //if (word2char[wordIdx].Count == 0) { Console.WriteLine("No Char is found for {0}", wordStr); }
    //            }
    //            Console.WriteLine("extract char vocab size {0}", chardict.ItemDictSize);
    //            return chardict;
    //        }


    //        public static Dictionary<int, List<int>> Word2Char(ItemFreqIndexDictionary wordDict, ItemFreqIndexDictionary charDict)
    //        {
    //            SimpleTextTokenizer textTokenize = new SimpleTextTokenizer() { IgnoreCase = false, TrimSpace = false };
    //            Dictionary<int, List<int>> map = new Dictionary<int, List<int>>();
    //            foreach (KeyValuePair<string, int> wordItem in wordDict.ItemIndex.NameDict)
    //            {
    //                int wordIdx = wordItem.Value;
    //                string wordStr = wordItem.Key;
    //                map.Add(wordIdx, new List<int>());
    //                IEnumerable<string> strList = textTokenize.LetterTokenize(wordStr);
    //                foreach (string c in strList)
    //                {
    //                    int cIdx = -1;
    //                    if (BuilderParameters.IS_CHAR_CASE) cIdx = charDict.IndexItem(c); // .ItemIndex.Index(ngram, true);
    //                    else cIdx = charDict.IndexItem(c.ToLower());
    //                    map[wordIdx].Add(cIdx);
    //                }
    //                if (map[wordIdx].Count == 0) { Console.WriteLine("No char is found for {0}", wordStr); }
    //            }
    //            return map;
    //        }

    //        public static void Init()
    //        {
    //            #region Preprocess Data.

    //            #region extract/load word/char/l3g vocab.

    //            // encoder = Encoding.Default;
    //            // Step 1 : Load Word Vocab .
    //            if (!BuilderParameters.Vocab.Equals(string.Empty))
    //            {
    //                if (!BuilderParameters.IS_UTF8_VOCAB)
    //                {
    //                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab, Encoding.UTF8))
    //                    {
    //                        trainWordDict = new ItemFreqIndexDictionary(mreader, false);
    //                    }
    //                }
    //                else
    //                {
    //                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
    //                    {
    //                        trainWordDict = new ItemFreqIndexDictionary(mreader, false);
    //                    }
    //                }
    //            }

    //            if (trainWordDict != null && !BuilderParameters.IS_FIX_VOCAB)
    //            {
    //                charFreqDict = Extract_CHAR_Dict(trainWordDict);
    //                if (BuilderParameters.CHAR_FREQ > 0)
    //                {
    //                    charFreqDict.Filter(BuilderParameters.CHAR_FREQ, 100000);
    //                }
    //                if (!BuilderParameters.CHAR_Vocab.Equals(string.Empty))
    //                {
    //                    Console.WriteLine("Save char vocab {0}, {1}", BuilderParameters.CHAR_Vocab, charFreqDict.ItemDictSize);
    //                    using (StreamWriter writer = new StreamWriter(BuilderParameters.CHAR_Vocab, false, Encoding.UTF8))
    //                    {
    //                        charFreqDict.Save(writer, false);
    //                        writer.Close();
    //                    }
    //                }
    //            }
    //            else if (File.Exists(BuilderParameters.CHAR_Vocab))
    //            {
    //                using (StreamReader reader = new StreamReader(BuilderParameters.CHAR_Vocab, Encoding.UTF8))
    //                {
    //                    charFreqDict = new ItemFreqIndexDictionary(reader, false);
    //                }
    //            }
    //            else
    //            {
    //                throw new Exception(string.Format("char vocab can not found {0}", BuilderParameters.CHAR_Vocab));
    //            }
    //            #endregion.

    //            if (BuilderParameters.IsTrainFile)
    //            {
    //                Dictionary<int, List<int>> word2char = Word2Char(trainWordDict, charFreqDict);

    //                ExtractCorpusBinary(
    //                        BuilderParameters.TrainContext, BuilderParameters.TrainQuery, BuilderParameters.TrainPos,
    //                        BuilderParameters.TrainContextBin, BuilderParameters.TrainQueryBin, BuilderParameters.TrainPosBin,
    //                        true, BuilderParameters.MiniBatchSize, word2char, trainWordDict.ItemDictSize, BuilderParameters.TRAIN_SPAN_LENGTH, null);
    //            }

    //            if (BuilderParameters.IsValidFile)
    //            {
    //                if (!BuilderParameters.DEV_Vocab.Equals(string.Empty))
    //                {
    //                    using (StreamReader mreader = new StreamReader(BuilderParameters.DEV_Vocab, Encoding.UTF8))
    //                    {
    //                        devWordDict = new ItemFreqIndexDictionary(mreader, false);
    //                    }
    //                }
    //                Dictionary<int, List<int>> devWord2char = Word2Char(devWordDict, charFreqDict);

    //                ExtractCorpusBinary(
    //                    BuilderParameters.ValidContext, BuilderParameters.ValidQuery, BuilderParameters.ValidPos,
    //                    BuilderParameters.ValidContextBin, BuilderParameters.ValidQueryBin, BuilderParameters.ValidPosBin,
    //                    false, BuilderParameters.DevMiniBatchSize, devWord2char, devWordDict.ItemDictSize, 0, ValidResults);
    //            }
    //            else
    //            {
    //                using (StreamReader ansReader = new StreamReader("tmp.pos.index"))
    //                {
    //                    while (!ansReader.EndOfStream)
    //                    {
    //                        string[] items = ansReader.ReadLine().Split('\t');
    //                        ValidResults.Add(new HashSet<string>(items));
    //                    }
    //                }
    //            }
    //            ValidIds = File.ReadAllLines(BuilderParameters.ValidIds);
    //            #endregion.

    //            #region Load Data.
    //            if (BuilderParameters.IsTrainFile)
    //            {
    //                TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin);
    //                TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin);
    //                TrainStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".1");
    //                TrainEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".2");
    //                TrainContextChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin + ".char");
    //                TrainQueryChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin + ".char");

    //                TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
    //                TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
    //                TrainContextChar.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
    //                TrainQueryChar.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
    //                TrainStartPos.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
    //                TrainEndPos.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
    //            }

    //            if (BuilderParameters.IsValidFile)
    //            {
    //                ValidContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin);
    //                ValidQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin);
    //                ValidStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".1");
    //                ValidEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".2");
    //                ValidContextChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin + ".char");
    //                ValidQueryChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin + ".char");

    //                ValidContext.InitThreadSafePipelineCashier(64, false);
    //                ValidQuery.InitThreadSafePipelineCashier(64, false);
    //                ValidContextChar.InitThreadSafePipelineCashier(64, false);
    //                ValidQueryChar.InitThreadSafePipelineCashier(64, false);
    //                ValidStartPos.InitThreadSafePipelineCashier(64, false);
    //                ValidEndPos.InitThreadSafePipelineCashier(64, false);
    //            }
    //            #endregion.
    //        }

    //        public static void Deinit()
    //        {
    //            string paraFileBin = BuilderParameters.ValidContextBin;
    //            string queryFileBin = BuilderParameters.ValidQueryBin;
    //            string ansFileBin = BuilderParameters.ValidPosBin;

    //            File.Delete(paraFileBin);
    //            File.Delete(paraFileBin + ".char");
    //            File.Delete(queryFileBin);
    //            File.Delete(queryFileBin + ".char");
    //            File.Delete(ansFileBin + ".1");
    //            File.Delete(ansFileBin + ".2");
    //        }

    //    }
    //}
}
