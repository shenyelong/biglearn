﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppL3GCNNQABuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                // training data folder.
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("TRAIN-FOLDER", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("ENTITY-VOCAB", new ParameterArgument(string.Empty, "Entity Vocab Data."));
                Argument.Add("L3G-VOCAB", new ParameterArgument(string.Empty, "L3G Vocab Data."));

                Argument.Add("TEST-FOLDER", new ParameterArgument(string.Empty, "Testing Folder"));
                Argument.Add("MINI-BATCH", new ParameterArgument("1", "Mini Batch Size"));

                Argument.Add("CON-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("CON-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));

                Argument.Add("QRY-LSTM-LAYER-DIM", new ParameterArgument("128", "DNN Layer Dim"));

                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string EntityVocab { get { return Argument["ENTITY-VOCAB"].Value == string.Empty ? TrainFolder + ".vocab" : Argument["ENTITY-VOCAB"].Value; } }
            public static string L3GVocab { get { return Argument["L3G-VOCAB"].Value == string.Empty ? TrainFolder + ".vocab" : Argument["L3G-VOCAB"].Value; } }

            public static string TrainFolder { get { return Argument["TRAIN-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!TrainFolder.Equals(string.Empty)); } }            
            public static string TrainContextBinary { get { return TrainFolder + ".l3g.cox.bin"; } }
            public static string TrainQueryBinary { get { return TrainFolder + ".l3g.qry.bin"; } }
            public static string TrainAnswerBinary { get { return TrainFolder + ".l3g.anw.bin"; } }

            public static string TestFolder { get { return Argument["TEST-FOLDER"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }
            public static string TestContextBinary { get { return TestFolder + ".l3g.cox.bin"; } }
            public static string TestQueryBinary { get { return TestFolder + ".l3g.qry.bin"; } }
            public static string TestAnswerBinary { get { return TestFolder + ".l3g.anw.bin"; } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int[] CON_LAYER_DIM { get { return Argument["CON-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] CON_LAYER_WIN { get { return Argument["CON-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] QUERY_LAYER_DIM { get { return Argument["QRY-LSTM-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.APP_L3G_CNN_LSTM_QA; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        class GenerateSelectionMatchRunner : StructRunner
        {
            public new BiMatchBatchData Output { get { return (BiMatchBatchData)base.Output; } set { base.Output = value; } }

            GeneralBatchInputData CandidateIndexData { get; set; }

            SeqDenseBatchData FullContextMem { get; set; }

            public GenerateSelectionMatchRunner(GeneralBatchInputData candidateData, SeqDenseBatchData fullContextMem, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                CandidateIndexData = candidateData;
                FullContextMem = fullContextMem;

                Output = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = candidateData.Stat.MAX_BATCHSIZE,
                    MAX_TGT_BATCHSIZE = FullContextMem.Stat.MAX_SEQUENCESIZE, // candidateData.Stat.MAX_ELEMENTSIZE,
                    MAX_MATCH_BATCHSIZE = candidateData.Stat.MAX_ELEMENTSIZE
                }, Behavior.Device);
            }

            public override void Forward()
            {
                CandidateIndexData.BatchIdx.SyncToCPU(CandidateIndexData.BatchSize);
                CandidateIndexData.FeatureIdx.SyncToCPU(CandidateIndexData.ElementSize);
                CandidateIndexData.FeatureValue.SyncToCPU(CandidateIndexData.ElementSize);
                FullContextMem.SampleIdx.SyncToCPU(CandidateIndexData.BatchSize);

                Output.SrcSize = CandidateIndexData.BatchSize;
                Output.TgtSize = FullContextMem.SampleIdx.MemPtr[CandidateIndexData.BatchSize - 1];
                Output.MatchSize = CandidateIndexData.ElementSize;

                int matchIdx = 0;
                for (int i = 0; i < CandidateIndexData.BatchSize; i++)
                {
                    int feaBegin = i == 0 ? 0 : CandidateIndexData.BatchIdx.MemPtr[i - 1];
                    int feaEnd = CandidateIndexData.BatchIdx.MemPtr[i];

                    int fullMemBegin = i == 0 ? 0 : FullContextMem.SampleIdx.MemPtr[i - 1];
                    for (int f = feaBegin; f < feaEnd; f++)
                    {
                        Output.SrcIdx.MemPtr[matchIdx] = i;
                        Output.TgtIdx.MemPtr[matchIdx] = fullMemBegin + CandidateIndexData.FeatureIdx.MemPtr[f];
                        Output.MatchInfo.MemPtr[matchIdx] = CandidateIndexData.FeatureValue.MemPtr[f];
                        matchIdx++;
                    }
                }

                Util.InverseMatchIdx(Output.SrcIdx.MemPtr, Output.MatchSize, Output.Src2MatchIdx.MemPtr, Output.Src2MatchElement.MemPtr, Output.SrcSize);
                Util.InverseMatchIdx(Output.TgtIdx.MemPtr, Output.MatchSize, Output.Tgt2MatchIdx.MemPtr, Output.Tgt2MatchElement.MemPtr, Output.TgtSize);

                Output.SrcIdx.SyncFromCPU(Output.MatchSize);
                Output.TgtIdx.SyncFromCPU(Output.MatchSize);
                Output.MatchInfo.SyncFromCPU(Output.MatchSize);

                Output.Src2MatchIdx.SyncFromCPU(Output.SrcSize);
                Output.Src2MatchElement.SyncFromCPU(Output.MatchSize);
                Output.Tgt2MatchIdx.SyncFromCPU(Output.TgtSize);
                Output.Tgt2MatchElement.SyncFromCPU(Output.MatchSize);
            }
        }

        class GenerateSelectionPredictionRunner : ObjectiveRunner
        {
            GeneralBatchInputData CandidateInputData { get; set; }

            SeqSparseBatchData ContextInputData { get; set; }

            CudaPieceFloat CandScoreData { get; set; }

            float Gamma { get; set; }

            int SampleNum = 0;
            int HitNum = 0;
            string OutputPath = "";
            StreamWriter OutputWriter = null;
            public GenerateSelectionPredictionRunner(
                GeneralBatchInputData candInputData, SeqSparseBatchData contextInputData, CudaPieceFloat candScoreData, float gamma, RunnerBehavior behavior,
                string outputFile = "") : base(Structure.Empty, behavior)
            {
                CandidateInputData = candInputData;
                ContextInputData = contextInputData;
                Gamma = gamma;
                CandScoreData = candScoreData;

                OutputPath = outputFile;
            }

            public override void Init()
            {
                SampleNum = 0;
                HitNum = 0;

                if (!OutputPath.Equals(""))
                {
                    OutputWriter = new StreamWriter(OutputPath);
                }
            }
            public override void Forward()
            {
                CandidateInputData.BatchIdx.SyncToCPU(CandidateInputData.BatchSize);
                CandidateInputData.FeatureIdx.SyncToCPU(CandidateInputData.ElementSize);
                CandidateInputData.FeatureValue.SyncToCPU(CandidateInputData.ElementSize);
                CandScoreData.SyncToCPU(CandidateInputData.ElementSize);

                ContextInputData.SampleIdx.SyncToCPU(ContextInputData.BatchSize);
                ContextInputData.SequenceIdx.SyncToCPU(ContextInputData.SentSize);
                ContextInputData.FeaIdx.SyncToCPU(ContextInputData.ElementSize);

                for (int i = 0; i < CandidateInputData.BatchSize; i++)
                {
                    Dictionary<int, float> candScoreDict = new Dictionary<int, float>();
                    int trueCandIdx = -1;

                    int sentBgn = i == 0 ? 0 : ContextInputData.SampleIdx.MemPtr[i - 1];
                    int sentEnd = ContextInputData.SampleIdx.MemPtr[i];

                    int candBgn = i == 0 ? 0 : CandidateInputData.BatchIdx.MemPtr[i - 1];
                    int candEnd = CandidateInputData.BatchIdx.MemPtr[i];

                    for (int c = candBgn; c < candEnd; c++)
                    {
                        int candPos = CandidateInputData.FeatureIdx.MemPtr[c];
                        float candLabel = CandidateInputData.FeatureValue.MemPtr[c];
                        float candScore = CandScoreData.MemPtr[c];

                        int wordIdx = ContextInputData.SequenceIdx.MemPtr[sentBgn + candPos] - 1;

                        int candIdx = ContextInputData.FeaIdx.MemPtr[wordIdx] + 1;
                        
                        if (candLabel > 0 && trueCandIdx >= 0 && trueCandIdx != candIdx) { Console.WriteLine("Previous Cand Idx {0}, {1}", trueCandIdx, candIdx); throw new Exception("Candidate Label Error 1 !!"); }

                        if (candLabel > 0) { trueCandIdx = candIdx; }

                        if (!candScoreDict.ContainsKey(candIdx)) { candScoreDict[candIdx] = 0; }
                        candScoreDict[candIdx] += (float)Math.Exp(Gamma * candScore);
                    }

                    if (trueCandIdx == -1) { throw new Exception("Candidate Label Error 2 !!"); }
                    var predCandIdx = candScoreDict.FirstOrDefault(x => x.Value == candScoreDict.Values.Max()).Key;

                    float expSum = candScoreDict.Values.Sum();

                    foreach (int k in new List<int>(candScoreDict.Keys)) candScoreDict[k] = candScoreDict[k] / expSum;

                    OutputWriter.WriteLine("{0}\t{1}\t{2}", trueCandIdx, TextUtil.Dict2Str(candScoreDict), predCandIdx);
                    SampleNum++;
                    if (predCandIdx == trueCandIdx) HitNum++;
                }
            }

            public override void Complete()
            {
                Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum);
                ObjectiveScore = HitNum * 1.0 / SampleNum;
                OutputWriter.Close();
            }
        }

        // This is the main code.
        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> answer,
                                                             // context cnn 
                                                             List<LayerStructure> ContextCNN,

                                                             // bi-directional lstm for query.
                                                             LSTMStructure queryD1LSTM, LSTMStructure queryD2LSTM,

                                                             // match between query and cnn.
                                                             LayerStructure MatchLayer,

                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ CNN of context data *********/
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));

            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(ContextCNN[0], ContextData, Behavior));
            for (int i = 1; i < ContextCNN.Count; i++)
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(ContextCNN[i], ContextEmbedOutput, Behavior));

            /************************************************************ Bi direction LSTM of query data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));

            // standard order -> lstm embedding. 
            SeqDenseRecursiveData QueryD1LSTMData = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(queryD1LSTM.LSTMCells[0], QueryData, false, Behavior));
            // reverse order -> lstm embedding.
            SeqDenseRecursiveData QueryD2LSTMData = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMSparseRunner<SeqSparseBatchData>(queryD2LSTM.LSTMCells[0], QueryData, true, Behavior));

            //SeqDenseBatchData QueryD1LSTMEmbed = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(QueryD1LSTMData, Behavior));
            //SeqDenseBatchData QueryD2LSTMEmbed = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(QueryD2LSTMData, Behavior));

            // last state.
            //HiddenBatchData QueryD1Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD1LSTMData, false, 0, QueryD1LSTMData.MapForward, Behavior));
            //HiddenBatchData QueryD2Output = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(QueryD2LSTMData, false, 0, QueryD2LSTMData.MapForward, Behavior));

            // max pooling.
            HiddenBatchData QueryD1Output = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryD1LSTMData, QueryD1LSTMData.MapForward, Behavior));
            HiddenBatchData QueryD2Output = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(QueryD2LSTMData, QueryD2LSTMData.MapForward, Behavior));

            HiddenBatchData QueryEnsembleOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { QueryD1Output, QueryD2Output }, Behavior));
            HiddenBatchData QueryOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(MatchLayer, QueryEnsembleOutput, Behavior));

            /************************************************************ Candidate and Answer data *********/
            GeneralBatchInputData CandidateAnswerData = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(answer, Behavior));

            HiddenBatchData ContextOutput = new HiddenBatchData(ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim, ContextEmbedOutput.SentOutput, ContextEmbedOutput.SentDeriv, Behavior.Device);
            //(HiddenBatchData)cg.AddRunner(new Transform_SeqMatrix2MatrixRunner(ContextEmbedOutput, Behavior));

            // select candidate entities.
            BiMatchBatchData candidateMatchData = (BiMatchBatchData)cg.AddRunner(new GenerateSelectionMatchRunner(CandidateAnswerData, ContextEmbedOutput, Behavior));
            // similarity between source text and target text.
            HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(QueryOutput, ContextOutput, candidateMatchData, SimilarityType.CosineSimilarity, Behavior));

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                        cg.AddRunner(new ModelDiskDumpRunner(model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\l3g_lstm"));
                    cg.AddObjective(new BayesianRatingRunner(simiOutput.Output.Data, simiOutput.Deriv.Data, candidateMatchData.MatchInfo, candidateMatchData, BuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new GenerateSelectionPredictionRunner(CandidateAnswerData, ContextData, simiOutput.Output.Data, BuilderParameters.Gamma, Behavior, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();
            List<LayerStructure> contextLayers = new List<LayerStructure>();
            LSTMStructure queryD1LSTM = null; LSTMStructure queryD2LSTM = null;

            LayerStructure matchLayer = null;

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int inputDim = DataPanel.WordDim;
                for (int i = 0; i < BuilderParameters.CON_LAYER_DIM.Length; i++)
                {
                    contextLayers.Add(new LayerStructure(inputDim, BuilderParameters.CON_LAYER_DIM[i], A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.CON_LAYER_WIN[i], 0, true));
                    inputDim = BuilderParameters.CON_LAYER_DIM[i];
                }
                queryD1LSTM = new LSTMStructure(DataPanel.WordDim, BuilderParameters.QUERY_LAYER_DIM, device);
                queryD2LSTM = new LSTMStructure(DataPanel.WordDim, BuilderParameters.QUERY_LAYER_DIM, device);

                matchLayer = new LayerStructure( 2 * BuilderParameters.QUERY_LAYER_DIM.Last(), BuilderParameters.CON_LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true);

                for (int i = 0; i < contextLayers.Count; i++) modelStructure.AddLayer(contextLayers[i]);
                modelStructure.AddLayer(queryD1LSTM);
                modelStructure.AddLayer(queryD2LSTM);
                modelStructure.AddLayer(matchLayer);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);
                    for (int i = 0; i < BuilderParameters.CON_LAYER_DIM.Length; i++) contextLayers.Add((LayerStructure)modelStructure.CompositeLinks[i]);
                    queryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[BuilderParameters.CON_LAYER_DIM.Length];
                    queryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[BuilderParameters.CON_LAYER_DIM.Length + 1];
                    matchLayer = (LayerStructure)modelStructure.CompositeLinks.Last();
                }
            }
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainAnswer,
                        contextLayers, queryD1LSTM, queryD2LSTM, matchLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph validCG = null;

                    if (BuilderParameters.IsTestFile)
                        validCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                            contextLayers, queryD1LSTM, queryD2LSTM, matchLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    double bestValidScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            string model_prefix = Path.Combine(BuilderParameters.ModelOutputPath, string.Format("cnnAtt.iter.{0}", iter));
                            modelStructure.Serialize(new BinaryWriter(new FileStream(model_prefix, FileMode.Create, FileAccess.Write)));
                        }

                        if (validCG != null)
                        {
                            double validScore = validCG.Execute();
                            if (validScore > bestValidScore) { bestValidScore = validScore; bestIter = iter; }
                            Logger.WriteLog("Best Valid Score {0}, At iter {1}", bestValidScore, bestIter);
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "cnnAtt.best.model"), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                        contextLayers, queryD1LSTM, queryD2LSTM, matchLayer, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TrainAnswer = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestQuery = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TestAnswer = null;

            static L3GTextHash l3gHash = null;
            static ItemFreqIndexDictionary entityFreqDict = null;

            public static int WordDim { get { return entityFreqDict.ItemDictSize + l3gHash.VocabSize; } }

            /// <summary>
            /// Unknown Index = 0;
            /// </summary>
            static void ExtractCorpusBinary(string questFolder, string contextBin, string queryBin, string answerBin, int miniBatchSize)
            {
                SeqSparseBatchData contextData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData queryData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordDim, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                GeneralBatchInputData answerData = new GeneralBatchInputData(new GeneralBatchInputDataStat() { FeatureType = FeatureDataType.SparseFeature, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(contextBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryBin);
                BinaryWriter answerWriter = FileUtil.CreateBinaryWrite(answerBin);

                int fileIdx = 0;
                DirectoryInfo directory = new DirectoryInfo(questFolder);
                foreach (FileInfo sampleFile in directory.EnumerateFiles())
                {
                    List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                    List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                    Dictionary<int, float> answerFea = new Dictionary<int, float>();

                    using (StreamReader mreader = new StreamReader(sampleFile.FullName))
                    {
                        string url = mreader.ReadLine(); mreader.ReadLine();
                        string context = mreader.ReadLine(); mreader.ReadLine();
                        string query = mreader.ReadLine(); mreader.ReadLine();
                        string answer = mreader.ReadLine(); mreader.ReadLine();

                        int answerIndex = entityFreqDict.IndexItem(answer);

                        foreach (string word in context.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            var tmp = new Dictionary<int, float>();
                            if (word.StartsWith("@entity"))
                            {
                                int idx = entityFreqDict.IndexItem(word);
                                tmp[idx] = 1; 

                                if (answerIndex == idx) { answerFea.Add(contextFea.Count, 1); }
                                else { answerFea.Add(contextFea.Count, 0); }
                            }
                            else
                            {
                                Dictionary<int,float> tmp2 = l3gHash.FeatureExtract(word);
                                foreach(KeyValuePair<int,float> term in tmp2) tmp.Add(term.Key + entityFreqDict.ItemDictSize, term.Value);
                            }
                            if (tmp.Count > 0) { contextFea.Add(tmp); }
                        }

                        foreach (string word in query.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            var tmp = new Dictionary<int, float>();
                            if (word.StartsWith("@entity") || word.StartsWith("@placeholder"))
                            {
                                int idx = entityFreqDict.IndexItem(word);
                                tmp[idx] = 1;
                            }
                            else
                            {
                                Dictionary<int, float> tmp2 = l3gHash.FeatureExtract(word);
                                foreach (KeyValuePair<int, float> term in tmp2) tmp.Add(term.Key + entityFreqDict.ItemDictSize, term.Value);
                            }
                            if (tmp.Count > 0) { queryFea.Add(tmp); }
                        }
                    }
                    contextData.PushSample(contextFea);
                    queryData.PushSample(queryFea);
                    answerData.PushSample(answerFea);

                    if (contextData.BatchSize >= miniBatchSize)
                    {
                        contextData.PopBatchToStat(contextWriter);
                        queryData.PopBatchToStat(queryWriter);
                        answerData.PopBatchToStat(answerWriter);
                    }

                    if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract Index from Corpus {0}", fileIdx); }
                }

                contextData.PopBatchCompleteStat(contextWriter);
                queryData.PopBatchCompleteStat(queryWriter);
                answerData.PopBatchCompleteStat(answerWriter);

                Console.WriteLine("Context Stat {0}", contextData.Stat.ToString());
                Console.WriteLine("Query Stat {0}", queryData.Stat.ToString());
                Console.WriteLine("Answer Stat {0}", answerData.Stat.ToString());
            }

            public static void Init()
            {
                #region Preprocess Data.
                /// Step 1 : Load Vocab.
                using (StreamReader mreader = new StreamReader(BuilderParameters.EntityVocab))
                {
                    entityFreqDict = new ItemFreqIndexDictionary(mreader, true);
                }
                l3gHash = new L3GTextHash(BuilderParameters.L3GVocab);

                /// Step 3 : Raw 2 Binary Data.
                if (BuilderParameters.IsTrainFile &&
                    (!File.Exists(BuilderParameters.TrainContextBinary) || !File.Exists(BuilderParameters.TrainQueryBinary) || !File.Exists(BuilderParameters.TrainAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TrainFolder, BuilderParameters.TrainContextBinary, BuilderParameters.TrainQueryBinary, BuilderParameters.TrainAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsTestFile &&
                    (!File.Exists(BuilderParameters.TestContextBinary) || !File.Exists(BuilderParameters.TestQueryBinary) || !File.Exists(BuilderParameters.TestAnswerBinary)))
                {
                    ExtractCorpusBinary(BuilderParameters.TestFolder, BuilderParameters.TestContextBinary, BuilderParameters.TestQueryBinary, BuilderParameters.TestAnswerBinary, BuilderParameters.MiniBatchSize);
                }
                #endregion.

                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBinary);
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBinary);
                    TrainAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainAnswerBinary);

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainAnswer.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsTestFile)
                {
                    TestContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestContextBinary);
                    TestQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestQueryBinary);
                    TestAnswer = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestAnswerBinary);

                    TestContext.InitThreadSafePipelineCashier(64, false);
                    TestQuery.InitThreadSafePipelineCashier(64, false);
                    TestAnswer.InitThreadSafePipelineCashier(64, false);
                }
            }
        }
    }
}