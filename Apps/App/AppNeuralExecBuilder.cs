﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// The builder shows an example of calculating the math problem given actions. (the action space is small in this case).
    /// </summary>
    public class AppNeuralExecBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("LSTM-DIM", new ParameterArgument("8", "lstm dimension."));
                Argument.Add("CNN-DIM", new ParameterArgument("8", "cnn dimension."));
                Argument.Add("CNN-WIN", new ParameterArgument("1", "cnn window size."));
            }
            public static int[] LSTM { get { return Argument["LSTM-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int CNN { get { return int.Parse(Argument["CNN-DIM"].Value); } }
            public static int CNN_WIN { get { return int.Parse(Argument["CNN-WIN"].Value); } }

        }
        public override BuilderType Type { get { return BuilderType.APP_NEURAL_EXEC; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        
        class ComputeActor : CompositeNetRunner
        {
            public List<Tuple<string, int>> outputStr;
            public List<Tuple<string, int>> inputStr;

            protected SeqSparseBatchData Fea { get; set; }
            //SeqDenseBatchData Data { get; set; }

            protected Random random = new Random();
            protected int MaxLength { get; set; }
            protected int MaxFeaDim { get; set; }
            protected CudaPieceFloat Score { get; set; }
            protected CudaPieceFloat Deriv { get; set; }
            protected List<int> PossibileActs = new List<int>();
            protected List<float> PossibileProb = new List<float>();
            protected int SelectAct { get; set; }
            public float Reward { get; set; }

            public ComputeActor(List<Tuple<string, int>> input, int maxLength, int maxFeaDim, RunnerBehavior behavior) : base(behavior)
            {
                MaxLength = maxLength;
                MaxFeaDim = maxFeaDim;

                inputStr = input;

                Fea = new SeqSparseBatchData(new SequenceDataStat(1, maxLength, maxFeaDim, maxLength), behavior.Device);
                outputStr = new List<Tuple<string, int>>();
            }

            public override void Forward()
            {
                // Fill Data.
                Fea.Clear();
                List<Dictionary<int, float>> feaMap = new List<Dictionary<int, float>>();
                foreach (Tuple<string, int> it in inputStr)
                {
                    Dictionary<int, float> dict = new Dictionary<int, float>();
                    dict[it.Item2] = 1;
                    feaMap.Add(dict);
                }
                Fea.PushSample(feaMap);

                base.Forward();

                //** dynamic construct network structure. **//
                Score.SyncToCPU(inputStr.Count);

                PossibileActs.Clear();
                //int idx = 0;
                //foreach (Tuple<string, int> item in inputStr)
                for (int idx = 1; idx < inputStr.Count - 1; idx++)
                {
                    if (inputStr[idx].Item2 > 0 && inputStr[idx - 1].Item2 == 0 && inputStr[idx + 1].Item2 == 0)
                    {
                        PossibileActs.Add(idx);
                    }
                    //idx += 1;
                }

                float[] score = new float[PossibileActs.Count];
                for (int i = 0; i < PossibileActs.Count; i++)
                {
                    score[i] = Score.MemPtr[PossibileActs[i]];
                }

                // sample action according to policy.
                float[] prob = Util.Softmax(score, 1);

                PossibileProb.Clear();
                PossibileProb.AddRange(prob);

                if (Behavior.RunMode == DNNRunMode.Train)
                {
                    SelectAct = Util.Sample(prob, random);
                }
                else
                {
                    SelectAct = Util.MaximumValue(prob);
                }
                int action = PossibileActs[SelectAct];


                int a = int.Parse(inputStr[action - 1].Item1);
                int b = int.Parse(inputStr[action + 1].Item1);
                int c = 0;
                if (inputStr[action].Item1 == "+")
                {
                    c = a + b;
                }
                else if (inputStr[action].Item1 == "-")
                {
                    c = a - b;
                }
                else if (inputStr[action].Item1 == "*")
                {
                    c = a * b;
                }
                else if (inputStr[action].Item1 == "/")
                {
                    if (b == 0) c = 0;
                    else c = a / b;
                }

                outputStr.Clear();
                for (int i = 0; i < action - 1; i++)
                {
                    outputStr.Add(inputStr[i]);
                }
                int cIdx = outputStr.Count;
                outputStr.Add(new Tuple<string, int>(c.ToString(), 0));
                for (int i = action + 2; i < inputStr.Count; i++)
                {
                    outputStr.Add(inputStr[i]);
                }
                if (outputStr.Count > 1 && cIdx > 0 && cIdx < outputStr.Count - 1)
                {
                    if (outputStr[cIdx - 1].Item1 == "(" && outputStr[cIdx + 1].Item1 == ")")
                    {
                        outputStr.RemoveAt(cIdx + 1);
                        outputStr.RemoveAt(cIdx - 1);
                    }
                }
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.Zero(Deriv, Deriv.EffectiveSize);
                Deriv.SyncToCPU();
                for (int i = 0; i < PossibileActs.Count; i++)
                {
                    int idx = PossibileActs[i];
                    float p = PossibileProb[i];
                    if (i == SelectAct)
                    {
                        Deriv.MemPtr[idx] = (1 - p) * Reward;
                    }
                    else
                    {
                        Deriv.MemPtr[idx] = -p * Reward;
                    }
                }
                Deriv.SyncFromCPU();

                base.Backward(cleanDeriv);
            }
        }
        class LSTMComputeActor : ComputeActor
        {
            public LSTMComputeActor(List<Tuple<string, int>> input, int maxLength, int maxFeaDim, LayerStructure EmbedCNN, LSTMStructure scanfw, LSTMStructure scanbw, LayerStructure att, RunnerBehavior behavior) : 
                base(input, maxLength, maxFeaDim, behavior)
            {
                SeqSparseConvRunner<SeqSparseBatchData> embedRunner = new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, Fea, Behavior);
                LinkRunners.Add(embedRunner);

                BiLSTMRunner lstmRunner = new BiLSTMRunner(scanfw, scanbw, embedRunner.Output, behavior);
                LinkRunners.Add(lstmRunner);
                
                MatrixMultiplicationRunner scoreRunner = new MatrixMultiplicationRunner(new MatrixData(lstmRunner.Output), new MatrixData(att), behavior);
                LinkRunners.Add(scoreRunner);

                //Data = scoreRunner.Output;
                Score = scoreRunner.Output.Output;
                Deriv = scoreRunner.Output.Deriv;
            }

        }

        class CNNComputeActor : ComputeActor
        {
            
            public CNNComputeActor(List<Tuple<string, int>> input, int maxLength, int maxFeaDim, LayerStructure EmbedCNN, LayerStructure cnn,  LayerStructure att, RunnerBehavior behavior) : 
                base(input, maxLength, maxFeaDim, behavior)
            {
                SeqSparseConvRunner<SeqSparseBatchData> embedRunner = new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, Fea, Behavior);
                LinkRunners.Add(embedRunner);

                SeqDenseConvRunner<SeqDenseBatchData> cnnRunner = new SeqDenseConvRunner<SeqDenseBatchData>(cnn, embedRunner.Output, behavior);
                LinkRunners.Add(cnnRunner);

                MatrixMultiplicationRunner scoreRunner = new MatrixMultiplicationRunner(new MatrixData(cnnRunner.Output), new MatrixData(att), behavior);
                LinkRunners.Add(scoreRunner);

                //Data = scoreRunner.Output;
                Score = scoreRunner.Output.Output;
                Deriv = scoreRunner.Output.Deriv;
            }

            
        }

        class ExecRunner : CompositeNetRunner
        {
            List<ComputeActor> Actors = new List<ComputeActor>();

            int ExecLength = 0;
            public List<Tuple<string, int>> inputStr { get; set; }
            public int OutputValue { get; set; }
            public float Reward { get; set; }
            public ExecRunner(List<Tuple<string, int>> input, int maxActor, int maxLength, int maxFeaDim, 
                LayerStructure EmbedCNN, LSTMStructure scanfw, LSTMStructure scanbw, LayerStructure att, RunnerBehavior behavior) : base(behavior)
            {
                List<Tuple<string, int>> I = input;
                for (int i=0;i<maxActor;i++)
                {
                    Actors.Add(new LSTMComputeActor(I, maxLength, maxFeaDim, EmbedCNN, scanfw, scanbw, att, behavior));
                    I = Actors[i].outputStr;
                }
                inputStr = input;
            }

            public ExecRunner(List<Tuple<string, int>> input, int maxActor, int maxLength, int maxFeaDim,
                LayerStructure EmbedCNN, LayerStructure cnn, LayerStructure att, RunnerBehavior behavior) : base(behavior)
            {
                List<Tuple<string, int>> I = input;
                for (int i = 0; i < maxActor; i++)
                {
                    Actors.Add(new CNNComputeActor(I, maxLength, maxFeaDim, EmbedCNN, cnn, att, behavior));
                    I = Actors[i].outputStr;
                }
                inputStr = input;
            }


            public override void Forward()
            {
                ExecLength = 0;
                OutputValue = int.MaxValue;
                for (int i = 0; i < Actors.Count; i++)
                {
                    Actors[i].Forward();

                    ExecLength += 1;
                    if (Actors[i].outputStr.Count == 1)
                    {
                        OutputValue = int.Parse(Actors[i].outputStr[0].Item1);
                        break;
                    }
                    
                }
            }

            public override void Backward(bool cleanDeriv)
            {
                for (int i = ExecLength - 1; i >= 0; i--)
                {
                    Actors[i].Reward = Reward;
                    Actors[i].Backward(cleanDeriv);
                }
            }

            public override void CleanDeriv()
            {
                for (int i = 0; i < ExecLength; i++)
                {
                    Actors[i].CleanDeriv();
                }
            }

            public override void Update()
            {
                for (int i = 0; i < ExecLength; i++)
                {
                    Actors[i].Update();
                }
            }

        }

        Random random = new Random(0);
        static int Number_Range = 20;
        Tuple<List<Tuple<string, int>>, int, int> GenerateFormulation(float genComplex = 0.3f)
        {
            double ap = random.NextDouble();
            List<Tuple<string, int>> a = new List<Tuple<string, int>>();
            int aNum = 0;
            int aSort = 0; //0 : +, - ; 1 : * / ; 2 : single.
            if (ap > genComplex)
            {
                aNum = random.Next(1, Number_Range);
                aSort = 10;
                a.Add(new Tuple<string, int>(aNum.ToString(), 0));
            }
            else
            {
                Tuple<List<Tuple<string, int>>, int, int> ta = GenerateFormulation();
                a = ta.Item1;
                aNum = ta.Item2;
                aSort = ta.Item3;
            }

            double bp = random.NextDouble();
            List<Tuple<string, int>> b = new List<Tuple<string, int>>();
            int bNum = 0;
            int bSort = 0; //0 : +, - ; 1 : * / ; 2 : single.
            if (bp > genComplex)
            {
                bNum = random.Next(1, Number_Range);
                bSort = 10;
                b.Add(new Tuple<string, int>(bNum.ToString(), 0));
            }
            else
            {
                Tuple<List<Tuple<string, int>>, int, int> tb = GenerateFormulation();
                b = tb.Item1;
                bNum = tb.Item2;
                bSort = tb.Item3;
            }

            int cNum = 0;
            int cSort = 0; //0 : +, - ; 1 : * / ; 2 : single.

            int op = random.Next(3);
            string opstr = "";
            if(op == 0)
            {
                cNum = aNum + bNum;
                cSort = 0;
                opstr = "+";
            }
            if (op == 1)
            {
                cNum = aNum - bNum;
                cSort = 1;
                opstr = "-";
            }
            if (op == 2)
            {
                cNum = aNum * bNum;
                cSort = 2;
                opstr = "*";
            }
            if (op == 3)
            {
                if (bNum == 0) cNum = 0;
                else cNum = aNum / bNum;
                cSort = 3;
                opstr = "/";
            }

            List<Tuple<string, int>> c = new List<Tuple<string, int>>();
            if (cSort == 2 && (aSort == 0 || aSort == 1 ))
            {
                c.Add(new Tuple<string, int>("(", 5));
                c.AddRange(a);
                c.Add(new Tuple<string, int>(")", 6));
            }
            else
            {
                c.AddRange(a);
            }

            c.Add(new Tuple<string, int>(opstr, op + 1));

            if( (cSort == 2 && (bSort == 0 || bSort == 1)) || ((cSort == 1) && (bSort <=1)) )
            {
                c.Add(new Tuple<string, int>("(", 5));
                c.AddRange(b);
                c.Add(new Tuple<string, int>(")", 6));
            }
            else
            {
                c.AddRange(b);
            }

            return new Tuple<List<Tuple<string, int>>, int, int>(c, cNum, cSort);
        }


        
        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile, !DeepNet.BuilderParameters.IsLogFile);

            //DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            //IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            DeviceBehavior behavior = new DeviceBehavior(DeepNet.BuilderParameters.GPUID);
            StreamWriter train_file = new StreamWriter("train_data.tsv");
            List<Tuple<List<Tuple<string, int>>, int, int>> TrainDataset = new List<Tuple<List<Tuple<string, int>>, int, int>>();
            int maxLen = 0;
            for (int b=0;b<50000;b++)
            {
                Tuple<List<Tuple<string, int>>, int, int> data = GenerateFormulation();
                if (data.Item1.Count <= 3) continue;
                TrainDataset.Add(data);
                train_file.WriteLine(string.Join(" ", data.Item1.Select(i => i.Item1)) + "\t" + data.Item2.ToString());
                if (data.Item1.Count > maxLen) maxLen = data.Item1.Count;
            }
            train_file.Close();

            StreamWriter valid_file = new StreamWriter("valid_data.tsv");
            List<Tuple<List<Tuple<string, int>>, int, int>> ValidDataset = new List<Tuple<List<Tuple<string, int>>, int, int>>();
            for (int b = 0; b < 1000; b++)
            {
                Tuple<List<Tuple<string, int>>, int, int> data = GenerateFormulation();
                if (data.Item1.Count <= 3) continue;
                ValidDataset.Add(data);

                valid_file.WriteLine(string.Join(" ", data.Item1.Select(i => i.Item1)) + "\t" + data.Item2.ToString());
                if (data.Item1.Count > maxLen) maxLen = data.Item1.Count;
                //Console.WriteLine(string.Join(" ",data.Item1.Select(i => i.Item1)));
                //Console.WriteLine(data.Item2);
                //Console.WriteLine();
            }
            valid_file.Close();

            StreamWriter hard_file = new StreamWriter("hard_data.tsv");
            List<Tuple<List<Tuple<string, int>>, int, int>> HardDataset = new List<Tuple<List<Tuple<string, int>>, int, int>>();
            for (int b = 0; b < 1000; b++)
            {
                Tuple<List<Tuple<string, int>>, int, int> data = GenerateFormulation(0.4f);
                if (data.Item1.Count <= 3) continue;
                HardDataset.Add(data);

                hard_file.WriteLine(string.Join(" ", data.Item1.Select(i => i.Item1)) + "\t" + data.Item2.ToString());
                if (data.Item1.Count > maxLen) maxLen = data.Item1.Count;
                //Console.WriteLine(string.Join(" ",data.Item1.Select(i => i.Item1)));
                //Console.WriteLine(data.Item2);
                //Console.WriteLine();
            }
            hard_file.Close();

            Console.WriteLine("Generate train test finished {0}, {1}, {2}.", TrainDataset.Count, ValidDataset.Count, HardDataset.Count);

            int feaDim = 7;
            int embedDim = 8;
            int lstmDim = BuilderParameters.LSTM[0];
            int maxOperator = maxLen;
            int cnnDim = BuilderParameters.CNN;
            int cnnWin = BuilderParameters.CNN_WIN;

            CompositeNNStructure model = new CompositeNNStructure();

            LayerStructure embed = new LayerStructure(feaDim, embedDim, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false);
            model.AddLayer(embed);

            LSTMStructure scanbw = null;
            LSTMStructure scanfw = null;

            int outputDim = 0;

            LayerStructure cnn = null;
            if (lstmDim > 0)
            {
                scanfw = new LSTMStructure(embedDim, new int[] { lstmDim }, behavior.Device);
                scanbw = new LSTMStructure(embedDim, new int[] { lstmDim }, behavior.Device);
                model.AddLayer(scanfw);
                model.AddLayer(scanbw);

                outputDim = 2 * lstmDim;
            }
            else
            {
                cnn = new LayerStructure(embedDim, cnnDim, A_Func.Tanh, N_Type.Convolution_layer, cnnWin, 0, false, behavior.Device);
                model.AddLayer(cnn);

                outputDim = cnnDim;
            }

            LayerStructure att = new LayerStructure(outputDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, behavior.Device);
            model.AddLayer(att);

            model.InitOptimizer(OptimizerParameters.StructureOptimizer, behavior.TrainMode);

            ExecRunner[] groupRunner = new ExecRunner[10];
            for(int i=0;i<groupRunner.Length;i++)
            {
                List<Tuple<string, int>> I = new List<Tuple<string, int>>();
                if(lstmDim > 0) groupRunner[i] = new ExecRunner(I, maxOperator, maxOperator, feaDim, embed, scanfw, scanbw, att, behavior.TrainMode);
                else groupRunner[i] = new ExecRunner(I, maxOperator, maxOperator, feaDim, embed, cnn, att, behavior.TrainMode);
            }

            List<Tuple<string, int>> PredI = new List<Tuple<string, int>>();
            ExecRunner predRunner = lstmDim > 0 ?
                new ExecRunner(PredI, maxOperator, maxOperator, feaDim, embed, scanfw, scanbw, att, behavior.PredictMode) :
                new ExecRunner(PredI, maxOperator, maxOperator, feaDim, embed, cnn, att, behavior.TrainMode) ;


            for (int iter = 0; iter < 200; iter++)
            {
                double avgR = 0;
                int allZero = 0;
                for (int b = 0; b < TrainDataset.Count; b++)
                {
                    int avgReward = 0;
                    for (int t = 0; t < groupRunner.Length; t++)
                    {
                        groupRunner[t].inputStr.Clear();
                        groupRunner[t].inputStr.AddRange(TrainDataset[b].Item1);
                        groupRunner[t].Forward();

                        if (groupRunner[t].OutputValue == TrainDataset[b].Item2)
                        {
                            avgReward += 1;
                        }
                    }

                    if (avgReward > 0)
                    {
                        for (int t = 0; t < groupRunner.Length; t++)
                        {
                            if (groupRunner[t].OutputValue == TrainDataset[b].Item2)
                            {
                                groupRunner[t].Reward = 1.0f / avgReward - 1.0f / groupRunner.Length;
                            }
                            else
                            {
                                groupRunner[t].Reward = -1.0f / groupRunner.Length;
                            }
                        }

                        foreach (ModelOptimizer g in model.ModelOptimizers)
                        {
                            g.Optimizer.BeforeGradient();
                        }
                        for (int t = 0; t < groupRunner.Length; t++)
                        {
                            groupRunner[t].CleanDeriv();
                            groupRunner[t].Backward(false);
                            groupRunner[t].Update();
                        }
                        foreach (ModelOptimizer g in model.ModelOptimizers)
                        {
                            g.Optimizer.AfterGradient();
                        }
                    }
                    else
                    {
                        allZero += 1;
                    }

                    avgR += avgReward * 1.0 / groupRunner.Length;

                    if((b+1) % 100 == 0) { Console.WriteLine("avg reward {0}, all zero {1}, {2}", avgR / b, allZero * 1.0f/ b, b); }
                }


                int CorrectNum = 0;
                for (int b = 0; b < ValidDataset.Count; b++)
                {
                    predRunner.inputStr.Clear();
                    predRunner.inputStr.AddRange(ValidDataset[b].Item1);
                    predRunner.Forward();

                    if (predRunner.OutputValue == ValidDataset[b].Item2)
                    {
                        CorrectNum += 1;
                    }
                }

                Console.WriteLine("Correct Valid Form {0}, Accuracy {1}", CorrectNum, CorrectNum * 1.0f / ValidDataset.Count);


                int HardCorrectNum = 0;
                for (int b = 0; b < HardDataset.Count; b++)
                {
                    predRunner.inputStr.Clear();
                    predRunner.inputStr.AddRange(HardDataset[b].Item1);
                    predRunner.Forward();

                    if (predRunner.OutputValue == HardDataset[b].Item2)
                    {
                        HardCorrectNum += 1;
                    }
                }
                Console.WriteLine("Correct Hard Form {0}, Accuracy {1}", HardCorrectNum, HardCorrectNum * 1.0f / HardDataset.Count);

            }
            //Console.ReadLine();

            Logger.CloseLog();
        }

    }
}
