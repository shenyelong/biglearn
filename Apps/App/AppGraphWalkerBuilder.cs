﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class AppGraphWalkerBuilder : Builder
    {
        /// <summary>
        /// 
        /// </summary>
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
            public static int EntityNum { get { return int.Parse(Argument["ENTITYNUM"].Value); } }

            public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }
            public static int RelationNum { get { return int.Parse(Argument["RELATIONNUM"].Value); } }

            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static string TestData { get { return Argument["TEST"].Value; } }
            public static bool IsTrainFile { get { return !(TrainData.Equals(string.Empty)); } }
            public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
            public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }
            public static string ValidPathFolder { get { return Argument["VALID-PATH"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
            public static int TrainBeamSize { get { return int.Parse(Argument["TRAIN-BEAM-SIZE"].Value); } }
            public static int TrainSetting { get { return int.Parse(Argument["TRAIN-SETTING"].Value); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            
            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static float T_MAX_CLIP { get { return float.Parse(Argument["T-MAX-CLIP"].Value); } }
            public static float T_MIN_CLIP { get { return float.Parse(Argument["T-MIN-CLIP"].Value); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static float GUID_RATE { get { return float.Parse(Argument["GUID-RATE"].Value); } }
            public static float RAND_RATE { get { return float.Parse(Argument["RAND-RATE"].Value); } }

            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }

            public static float NORM_REWARD { get { return float.Parse(Argument["NORM-REWARD"].Value); } }

            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("ENTITY2ID", new ParameterArgument(string.Empty, "Entity 2 ID."));
                Argument.Add("ENTITYNUM", new ParameterArgument("0", "Entity Number."));

                Argument.Add("RELATION2ID", new ParameterArgument(string.Empty, "Relation 2 ID."));
                Argument.Add("RELATIONNUM", new ParameterArgument("0", "Relation Number."));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
                Argument.Add("VALID-PATH", new ParameterArgument(string.Empty, "Valid Path Data."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("DNN-DIMS", new ParameterArgument("100,100","DNN Map Dimensions."));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
                Argument.Add("TRAIN-BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("TRAIN-SETTING", new ParameterArgument("0", "0:supervised  1:reinforcement"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

                Argument.Add("GUID-RATE", new ParameterArgument("1", "guided path rate."));
                Argument.Add("RAND-RATE", new ParameterArgument("10", "rand path rate."));

                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "Reward discount"));

                Argument.Add("T-MAX-CLIP", new ParameterArgument("0", "Maximum T Clip"));
                Argument.Add("T-MIN-CLIP", new ParameterArgument("0", "Minimum T Clip"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score File."));
                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));

                Argument.Add("NORM-REWARD", new ParameterArgument("0", "normalize reward."));
                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_GRAPH_WALKER; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        class GraphQueryData : BatchData
        {
            public List<int> RawSource = new List<int>();
            public List<Tuple<int, int>> RawQuery = new List<Tuple<int, int>>();
            public List<int> RawTarget = new List<int>();
            public List<HashSet<int>> BlackTargets = new List<HashSet<int>>();

            /// <summary>
            /// guided raw path.
            /// </summary>
            public List<List<Tuple<int, int, int>>> RawPath = new List<List<Tuple<int, int, int>>>();
            public List<int> PathIdx = new List<int>();

            public int BatchSize { get { return RawQuery.Count; } }
            public int MaxBatchSize { get { return MaxGroupNum * GroupSize; } }
            public int GroupNum { get { return BatchSize / GroupSize; } }
            public int MaxGroupNum { get; set; }
            public int GroupSize { get; set; }
            public List<StatusData> StatusPath { get; set; }

            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public GraphQueryData(int maxGroupNum, int groupSize, DeviceType device)
            {
                MaxGroupNum = maxGroupNum;
                GroupSize = groupSize;
                StatusPath = new List<StatusData>();
            }
        }

        class StatusData : BatchData
        {
            /// <summary>
            /// Raw Query.
            /// </summary>
            public GraphQueryData GraphQuery { get; set; }
            /// <summary>
            /// Node ID.
            /// </summary>
            public List<int> NodeID { get; set; }
            /// <summary>
            /// Termination Probability.
            /// </summary>
            public HiddenBatchData Term = null;
            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            List<float> LogProb { get; set; }
            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            public List<Tuple<int, int>> MatchCandidate = null;
            public SeqVectorData MatchCandidateProb = null;
            
            /// <summary>
            /// QueryIdx.
            /// </summary>
            List<int> QueryIndex { get; set; }

            /// <summary>
            /// previous batchIdx and actionIdx.
            /// </summary>
            public List<Tuple<int, int>> SelectedAction { get; set; }

            /// <summary>
            /// constrastive reward.
            /// </summary>
            public List<float> CR { get; set; }
            
            public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.BatchSize; } }

            /// <summary>
            /// foreach query, obtain the batch indexes.
            /// </summary>
            /// <param name="queryId"></param>
            /// <returns></returns>
            public List<int> GetBatchIdxs(int queryId)
            {
                if(QueryIndex == null)
                {
                    return Enumerable.Range(queryId * GraphQuery.GroupSize, GraphQuery.GroupSize).ToList();
                }
                else
                {
                    List<int> r = new List<int>();
                    for(int i=0;i<QueryIndex.Count; i++)
                    {
                        if (QueryIndex[i] == queryId) r.Add(i);
                    }
                    return r;
                }
            }

            /// <summary>
            /// logprobability of the node.
            /// </summary>
            /// <param name="batchIdx"></param>
            /// <returns></returns>
            public float GetLogProb(int batchIdx)
            {
                if (LogProb == null) { return 0; }
                else { return LogProb[batchIdx]; }
            }

            public bool IsTermable(int b)
            {
                if (Term == null) { return false; }
                //if (GraphQuery.BlackTargets[b].Contains(NodeIndex[b])) { return false; }
                return true;
            }


            /// <summary>
            /// Log Termination Probability.
            /// </summary>
            /// <param name="b"></param>
            /// <param name="isT"></param>
            /// <returns></returns>
            public float LogPTerm(int b, bool isT)
            {
                if (isT) { return (float)Util.LogLogistial(Term.Output.Data.MemPtr[b]); }
                else { return (float)Util.LogNLogistial(Term.Output.Data.MemPtr[b]); }
            }


            /// <summary>
            /// QueryIndex.
            /// </summary>
            /// <param name="batchId"></param>
            /// <returns></returns>
            public int GetQueryIndex(int batchId)
            {
                if (QueryIndex != null) { return QueryIndex[batchId]; }
                else { return batchId; }
            }

            public bool IsBlack(int b)
            {
                int queryIdx = GetQueryIndex(b);
                if (GraphQuery.BlackTargets[queryIdx].Contains(NodeID[b])) { return true; }
                else return false;
            }

            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public HiddenBatchData StateEmbed { get; set; }

            public int Step;


            public StatusData(GraphQueryData interData, List<int> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) :
                this(interData, nodeIndex, null, null, null, stateEmbed, device)
            { }

            public StatusData(GraphQueryData interData, 
                              List<int> nodeIndex, List<int> queryIdx, List<float> logProb, List<Tuple<int, int>> selectedAction, 
                              HiddenBatchData stateEmbed, DeviceType device)
            {
                GraphQuery = interData;
                GraphQuery.StatusPath.Add(this);

                NodeID = nodeIndex;
                StateEmbed = stateEmbed;

                QueryIndex = queryIdx;
                LogProb = logProb;
                SelectedAction = selectedAction;

                CR = new List<float>();

                Step = GraphQuery.StatusPath.Count - 1;
            }
        }


        /// <summary>
        /// Sample Graph. 
        /// </summary>
        class SampleRunner : StructRunner
        {
            List<Tuple<int, int, int>> Graph { get; set; }
            List<List<Tuple<int, List<Tuple<int, int, int>>>>> CandidatePath { get; set; }
            public new GraphQueryData Output { get; set; }
            int MaxGroupNum { get; set; }
            int GroupSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            Random random = new Random();

            public SampleRunner(List<Tuple<int, int, int>> graph,
                                List<List<Tuple<int, List<Tuple<int, int, int>>>>> candidatePath,
                                int groupNum, int groupSize,
                                RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Graph = graph;
                CandidatePath = candidatePath;
                MaxGroupNum = groupNum;
                GroupSize = groupSize;

                Shuffle = new DataRandomShuffling(Graph.Count * 2, random);
                Output = new GraphQueryData(MaxGroupNum, groupSize, behavior.Device);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                Output.RawQuery.Clear();
                Output.RawSource.Clear();
                Output.RawTarget.Clear();
                Output.BlackTargets.Clear();
                Output.RawPath.Clear();
                Output.PathIdx.Clear();

                int groupIdx = 0;

                //if (DebugBatch == 0) { IsTerminate = true; return; }
                //DebugBatch--;

                while (groupIdx < MaxGroupNum)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }
                    int srcId = idx < Graph.Count ? Graph[idx].Item1 : Graph[idx - Graph.Count].Item2;
                    int tgtId = idx < Graph.Count ? Graph[idx].Item2 : Graph[idx - Graph.Count].Item1;
                    int linkId = idx < Graph.Count ? Graph[idx].Item3 : (Graph[idx - Graph.Count].Item3 + DataPanel.RelationNum);

                    bool isReverse = false;
                    int pathIdx = -1;
                    int batchIdx = idx;
                    if (idx >= Graph.Count)
                    {
                        isReverse = true;
                        batchIdx = idx - Graph.Count;
                    }

                    // skip no guided path samples.
                    if (CandidatePath != null && CandidatePath[batchIdx].Count == 0)
                    {
                        continue;
                    }

                    int guidRange = (int)BuilderParameters.GUID_RATE;
                    int randRange = (int)(BuilderParameters.GUID_RATE + BuilderParameters.RAND_RATE);

                    for (int b = 0; b < GroupSize; b++)
                    {
                        if (Behavior.RunMode == DNNRunMode.Train)
                        {
                            if (b < guidRange) // && CandidatePath != null && CandidatePath[batchIdx].Count > 0) 
                            {
                                int pathNum = CandidatePath[batchIdx].Count;
                                //sample a path.
                                if (pathNum > 0) { pathIdx = random.Next(pathNum); }
                                if (pathIdx >= 0)
                                {
                                    if (!isReverse)
                                    {
                                        Output.RawPath.Add(CandidatePath[batchIdx][pathIdx].Item2);
                                    }
                                    else
                                    {
                                        Output.RawPath.Add(DataPanel.ReversePath(CandidatePath[batchIdx][pathIdx].Item2));
                                    }

                                    if (!(Output.RawPath.Last().First().Item1 == srcId &&
                                       Output.RawPath.Last().Last().Item2 == tgtId))
                                    {
                                        throw new Exception("Path Error!!");
                                    }
                                    Output.PathIdx.Add(CandidatePath[batchIdx][pathIdx].Item1);
                                }
                            }
                            else if (b < randRange)
                            {
                                //random draw path.
                                Output.RawPath.Add(new List<Tuple<int, int, int>>());
                                Output.PathIdx.Add(-2);
                            }
                            else
                            {
                                //sample path.
                                Output.RawPath.Add(new List<Tuple<int, int, int>>());
                                Output.PathIdx.Add(-1);
                            }
                        }
                        else
                        {
                            Output.RawPath.Add(new List<Tuple<int, int, int>>());
                            Output.PathIdx.Add(-1);
                        }
                        Output.RawQuery.Add(new Tuple<int, int>(srcId, linkId));
                        Output.RawSource.Add(srcId);
                        Output.RawTarget.Add(tgtId);

                        HashSet<int> blackTarget = DataPanel.knowledgeGraph.TrainNeighborHash[srcId].ContainsKey(linkId) ?
                                                    DataPanel.knowledgeGraph.TrainNeighborHash[srcId][linkId] : new HashSet<int>();
                        Output.BlackTargets.Add(blackTarget);
                    }

                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }
        
        class StatusEmbedRunner : CompositeNetRunner
        {
            public new List< Tuple<int, int>> Input { get; set; }

            public new HiddenBatchData Output { get; set; }

            EmbedStructure InNodeEmbed { get; set; }
            EmbedStructure LinkEmbed { get; set; }

            public StatusEmbedRunner(List<Tuple<int,int>> input, int maxBatchSize, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                InNodeEmbed = inNodeEmbed;
                LinkEmbed = linkEmbed;
                // concate of node embedding and relation embedding.
                Output = new HiddenBatchData(maxBatchSize, inNodeEmbed.Dim + linkEmbed.Dim, DNNRunMode.Train, behavior.Device);
                //new StatusData(Input, InNodeEmbed.Dim + LinkEmbed.Dim, behavior.Device);
            }

            public override void Forward()
            {
                InNodeEmbed.Embedding.SyncToCPU();
                LinkEmbed.Embedding.SyncToCPU();

                //BatchLinks.Clear();
                int batchSize = 0;
                while (batchSize < Input.Count)
                {
                    int srcId = Input[batchSize].Item1;
                    int linkId = Input[batchSize].Item2;

                    int bidx = batchSize;

                    //Output.NodeIndex[bidx] = srcId;

                    if (BuilderParameters.N_EmbedDim > 0)
                    {
                        FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim,
                                                InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);
                    }
                    FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim + InNodeEmbed.Dim,
                                            LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);
                    batchSize += 1;
                }
                Output.BatchSize = batchSize;
                Output.Output.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                Output.Deriv.Data.SyncToCPU(Output.Dim * Output.BatchSize);

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

                for (int b = 0; b < Output.BatchSize; b++)
                {
                    int srcIdx = Input[b].Item1;
                    int linkIdx = Input[b].Item2;

                    //InNodeEmbed.EmbeddingOptimizer.PushSparseGradient(srcIdx * InNodeEmbed.Dim, InNodeEmbed.Dim);
                    //LinkEmbed.EmbeddingOptimizer.PushSparseGradient(linkIdx * LinkEmbed.Dim, LinkEmbed.Dim);

                    if (BuilderParameters.N_EmbedDim > 0)
                    {
                        FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
                                        Output.Deriv.Data.MemPtr, b * Output.Dim, InNodeEmbed.Dim,
                                        1, InNodeEmbed.EmbeddingOptimizer.GradientStep);
                    }

                    FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim,
                                    Output.Deriv.Data.MemPtr, b * Output.Dim + InNodeEmbed.Dim, LinkEmbed.Dim,
                                    1, LinkEmbed.EmbeddingOptimizer.GradientStep);
                }

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();

            }
        }

        class CandidateActionRunner : CompositeNetRunner
        {
            public new StatusData Input;

            public new List<Tuple<int, int>> Output = new List<Tuple<int, int>>();
            public BiMatchBatchData Match;

            /// <summary>
            /// Input : State.
            /// node and rel embed.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="nodeEmbed"></param>
            /// <param name="relEmbed"></param>
            /// <param name="behavior"></param>
            public CandidateActionRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                Match = new BiMatchBatchData(new BiMatchBatchDataStat() {
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount }, behavior.Device);
            }

            Random random = new Random();
            public override void Forward()
            {
                Output.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
                int cursor = 0;
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    int seedNode = Input.NodeID[b];

                    int qid = Input.GetQueryIndex(b);
                    int rawSrc = Input.GraphQuery.RawQuery[qid].Item1;
                    int rawR = Input.GraphQuery.RawQuery[qid].Item2;
                    int rawTgt = Input.GraphQuery.RawTarget[qid];

                    int candidateNum = 0;
                    for (int nei = 0; nei < DataPanel.knowledgeGraph.TrainNeighborLink[seedNode].Count; nei++)
                    {
                        int lid = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item1 > 0 ?
                            DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 :
                            DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum;

                        int relid = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item1 > 0 ?
                            DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum :
                            DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 ;

                        int tgtNode = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item3;
                        if (seedNode == rawSrc && lid == rawR && tgtNode == rawTgt) { continue; }
                        if (seedNode == rawTgt && relid == rawR && tgtNode == rawSrc) { continue; } 

                        Output.Add(new Tuple<int, int>(tgtNode, lid));
                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
                        cursor += 1;
                        candidateNum += 1;
                     }
                     
                     if(candidateNum == 0)
                     {
                        Output.Add(new Tuple<int, int>(seedNode, random.Next(DataPanel.RelationNum * 2)));
                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
                        cursor += 1;
                     }
                }
                Match.SetMatch(match);
            }
        }

        static Random SampleRandom = new Random(10);
        class ActionSamplingRunner : CompositeNetRunner
        {
            new StatusData Input { get; set; }

            /// <summary>
            /// new Node Index.
            /// </summary>
            public List<int> NodeIndex { get; set; }

            public List<float> LogProb { get; set; }

            public List<Tuple<int, int>> NodeAction { get; set; }

            public List<Tuple<int, int>> Action { get; set; }

            int lineIdx = 0;
            public override void Init()
            {
                lineIdx = 0;
            }
            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public ActionSamplingRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                NodeIndex = new List<int>();
                LogProb = new List<float>();
                NodeAction = new List<Tuple<int, int>>();
                Action = new List<Tuple<int, int>>();
            }

            public override void Forward()
            {
                NodeIndex.Clear();
                LogProb.Clear();
                NodeAction.Clear();
                Action.Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                if (Input.Term != null) Input.Term.Output.SyncToCPU();

                int currentStep = Input.Step;
                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    int selectIdx = -1;

                    // select an path.
                    if (Input.GraphQuery.RawPath[i].Count > currentStep)
                    {
                        int n = Input.GraphQuery.RawPath[i][currentStep].Item1;
                        int t = Input.GraphQuery.RawPath[i][currentStep].Item2;
                        int r = Input.GraphQuery.RawPath[i][currentStep].Item3;

                        if(n != Input.NodeID[i]) { throw new Exception(string.Format("Path in NodeIndex doesnot match {0}, {1}, line {2}, step {3}, path idx {4}",
                            n, Input.NodeID[i], lineIdx + i / BuilderParameters.BeamSize, currentStep, Input.GraphQuery.PathIdx[i])); }

                        // find the path.
                        for (int m = s; m < e; m++)
                        {
                            if (Input.MatchCandidate[m].Item1 == t && Input.MatchCandidate[m].Item2 == r)
                            {
                                selectIdx = m;
                                break;
                            }
                        }

                        if (selectIdx == -1)
                        {
                            throw new Exception(string.Format("Path not found in source : {0}, relation : {1}, target {2}, line {3}, step {4}, path idx {5}", 
                                                n, r, t, lineIdx + i / BuilderParameters.BeamSize, currentStep, Input.GraphQuery.PathIdx[i]));
                        }
                    }
                    // draw sample actions.
                    else if (Input.GraphQuery.PathIdx[i] == -1)
                    {
                        int idx = Util.Sample(Input.MatchCandidateProb.Output.MemPtr, s, e - s, SampleRandom);
                        selectIdx = s + idx;
                    }
                    // draw random actions.
                    else
                    {
                        selectIdx = SampleRandom.Next(s, e);
                    }

                    NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);
                    NodeAction.Add(new Tuple<int, int>(i, selectIdx));

                    Action.Add(Input.MatchCandidate[selectIdx]);

                    float nonLogTerm = 0;
                    if (Input.IsTermable(i))
                    {
                        nonLogTerm = Input.LogPTerm(i, false);
                    }
                    float prob = Input.GetLogProb(i) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                    LogProb.Add(prob);
                }
                lineIdx += Input.MatchCandidateProb.Segment / BuilderParameters.BeamSize;
            }
        }

        class BeamSearchActionRunner : CompositeNetRunner
        {
            new StatusData Input { get; set; }
            int BeamSize = 1;

            public BiMatchBatchData MatchPath { get; set; }

            /// <summary>
            /// Transist Node Index.
            /// </summary>
            public List<int> NodeIndex { get; set; }

            /// <summary>
            /// Group index.
            /// </summary>
            public List<int> GroupIndex { get; set; }

            /// <summary>
            /// Transist Node Probability.
            /// </summary>
            public List<float> NodeProb { get; set; }


            /// <summary>
            /// batchIdx and actionIdx.
            /// </summary>
            public List<Tuple<int, int>> NodeAction { get; set; }

            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchActionRunner(StatusData input, int beamSize, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                BeamSize = beamSize;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);

                NodeIndex = new List<int>();
                GroupIndex = new List<int>();
                NodeProb = new List<float>();
                NodeAction = new List<Tuple<int, int>>();
            }

            public override void Forward()
            {
                if(Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();
                if(Input.Term != null) Input.Term.Output.SyncToCPU();

                NodeIndex.Clear();
                NodeProb.Clear();
                NodeAction.Clear();
                GroupIndex.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
                // for each beam.
                for (int i = 0; i < Input.GraphQuery.BatchSize; i++) //.MatchCandidateProb.Segment; i++)
                {
                    List<int> idxs = Input.GetBatchIdxs(i);

                    MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                        float nonLogTerm = 0;
                        if (Input.IsTermable(b))
                        {
                            nonLogTerm = Input.LogPTerm(b, false); 
                        }

                        for (int t = s; t < e; t++)
                        {
                            float prob = Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                            topKheap.push_pair(new Tuple<int, int>(b, t), prob);
                        }
                    }
                    
                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();
                        match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));

                        NodeIndex.Add(Input.MatchCandidate[p.Key.Item2].Item1);
                        NodeProb.Add(p.Value);
                        GroupIndex.Add(i);
                        NodeAction.Add(new Tuple<int, int>(p.Key.Item1, p.Key.Item2));
                    }
                }
                MatchPath.SetMatch(match);
            }
        }

        /// <summary>
        /// it is a little difficult.
        /// </summary>
        class RewardRunner : ObjectiveRunner
        {
            Random random = new Random();
            new GraphQueryData Input { get; set; }
            public RewardRunner(GraphQueryData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
            }

            public override void Forward()
            {
                // calculate termination probability.
                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    if (Input.StatusPath[t].Term != null)
                    {
                        ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.BatchSize, 1);
                        ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
                        Input.StatusPath[t].Term.Output.Data.SyncToCPU();
                        Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
                    }
                    if (Input.StatusPath[t].MatchCandidateProb != null)
                    {
                        Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
                    }
                }
                
                // calculate the probability of each stopable sample.
                List<int> SelectedStep = new List<int>();
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    int selectedStep = Input.StatusPath.Count - 1;
                    if (Input.RawPath[b].Count > 0)
                    {
                        selectedStep = Input.RawPath[b].Count;
                    }
                    else
                    {
                        for (int t = 1; t < Input.StatusPath.Count; t++)
                        {
                            StatusData st = Input.StatusPath[t];
                            float tp = st.Term.Output.Data.MemPtr[b];
                            if (random.NextDouble() <= tp)
                            {
                                selectedStep = t;
                                break;
                            }
                        }
                    }
                    SelectedStep.Add(selectedStep);
                }

                float avgProb = 0;
                int avgGroupNum = 0;
                for (int g = 0; g < Input.GroupNum; g++)
                {
                    int targetNum = 0;
                    int blackNum = 0;
                    int falseNum = 0;
                    for (int s = 0; s < Input.GroupSize; s++)
                    {
                        int bidx = g * Input.GroupSize + s;
                        int t = SelectedStep[bidx];

                        int targetId = Input.StatusPath[t].NodeID[bidx];
                        if (targetId == Input.RawTarget[bidx])
                        {
                            targetNum += 1;
                        }
                        else if(Input.BlackTargets[bidx].Contains(targetId))
                        {
                            blackNum += 1;
                        }
                        else
                        {
                            falseNum += 1;
                        }
                    }

                    if(falseNum > 0)
                    {
                        avgProb += targetNum * 1.0f / (targetNum + falseNum);
                        avgGroupNum += 1;
                    }


                    #region constrastive reward calculation.
                    for (int s = 0; s < Input.GroupSize; s++)
                    {
                        int bidx = g * Input.GroupSize + s;
                        int t = SelectedStep[bidx];
                        int targetId = Input.StatusPath[t].NodeID[bidx];

                        float cr = 0;
                        if (BuilderParameters.NORM_REWARD == 0)
                        {
                            if (targetId == Input.RawTarget[bidx])
                            {
                                cr = BuilderParameters.POS_R;// 1.0f; // 1.0f / targetNum - targetNum * 1.0f / (targetNum + falseNum + float.Epsilon);
                            }
                            else if (Input.BlackTargets[bidx].Contains(targetId))
                            {
                                //cr = 0;// -1.0f; // -targetNum * 1.0f / (targetNum + falseNum + float.Epsilon);
                                cr = BuilderParameters.BLACK_R;
                            }
                            else
                            {
                                cr = BuilderParameters.NEG_R;
                            }
                        }
                        else
                        {
                            if (targetId == Input.RawTarget[bidx])
                            {
                                cr = 1.0f / targetNum - 1.0f / (targetNum + falseNum + float.Epsilon);
                            }
                            else if (Input.BlackTargets[bidx].Contains(targetId))
                            {
                                //cr = 0;// -1.0f; // -targetNum * 1.0f / (targetNum + falseNum + float.Epsilon);
                                cr = 0; // BuilderParameters.BLACK_R;
                            }
                            else
                            {
                                cr = - 1.0f / (targetNum + falseNum + float.Epsilon);
                            }
                        }
                        StatusData st = Input.StatusPath[t];
                        st.Term.Deriv.Data.MemPtr[bidx] = cr * (1 - st.Term.Output.Data.MemPtr[bidx]);
                        for (int pt = t - 1; pt >= 0; pt--)
                        {
                            StatusData pst = Input.StatusPath[pt];
                            if (pst.Term != null)
                            {
                                pst.Term.Deriv.Data.MemPtr[bidx] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr * (-pst.Term.Output.Data.MemPtr[bidx]);
                            }
                        }

                        for (int pt = t - 1; pt >= 0; pt--)
                        {
                            StatusData pst = Input.StatusPath[pt];
                            int sidx = st.SelectedAction[bidx].Item2;
                            pst.MatchCandidateProb.Deriv.MemPtr[sidx] = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt) * cr;
                            st = pst;
                        }
                    }
                    #endregion.
                }

                ObjectiveScore = avgProb / (avgGroupNum + float.Epsilon);

                //average ground truth results.

                for (int i = Input.StatusPath.Count - 1; i >= 0; i--)
                {
                    StatusData st = Input.StatusPath[i];
                    if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
                    if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
                }
            }
        }

        class SupervisedRunner : ObjectiveRunner
        {
            Random random = new Random();
            new GraphQueryData Input { get; set; }
            public SupervisedRunner(GraphQueryData input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
            }

            public override void Forward()
            {
                // calculate termination probability.
                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    if (Input.StatusPath[t].Term != null)
                    {
                        ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.BatchSize, 1);
                        ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        Input.StatusPath[t].Term.Output.Data.SyncToCPU();
                        Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
                    }

                    if (Input.StatusPath[t].MatchCandidateProb != null)
                    {
                        Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
                    }
                }

                double _tmpprob = 0;
                // calculate the probability of each stopable sample.
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    int pathT = Input.RawPath[b].Count;

                    StatusData st = Input.StatusPath[pathT];
                    float tP = (float)Math.Log(st.Term.Output.Data.MemPtr[b]);
                    st.Term.Deriv.Data.MemPtr[b] = (1 - st.Term.Output.Data.MemPtr[b]);

                    for (int t = pathT - 1; t >= 0; t--)
                    {
                        StatusData hSt = Input.StatusPath[t];
                        int sidx = st.SelectedAction[b].Item2;

                        tP = tP + (float)Math.Log(hSt.MatchCandidateProb.Output.MemPtr[sidx] + Util.LargeEpsilon);
                        hSt.MatchCandidateProb.Deriv.MemPtr[sidx] = 1;

                        if (hSt.Term != null)
                        {
                            tP = tP + (float)Math.Log(1 - hSt.Term.Output.Data.MemPtr[b]);
                            hSt.Term.Deriv.Data.MemPtr[b] = (-hSt.Term.Output.Data.MemPtr[b]);
                        }
                        st = hSt;
                    }
                    _tmpprob += Math.Exp(tP);
                }

                ObjectiveScore = _tmpprob / Input.BatchSize;

                for (int i = Input.StatusPath.Count - 1; i >= 0; i--)
                {
                    StatusData st = Input.StatusPath[i];
                    if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
                    if (st.MatchCandidateProb != null) st.MatchCandidateProb.Deriv.SyncFromCPU();
                }

            }
        }

        enum DistanceType { L1Dist, L2Dist }
        public enum ObjectiveFunc { FullSoftmax = 0, SampleSoftmax = 1  }
        public enum PredType { RL_MAXITER = 0, RL_AVGSIM = 1 }

        /// <summary>
        /// REINFORCE-WALK prediction Runner.
        /// </summary>
        class TopKPredictionRunner : StructRunner
        {
            int lsum = 0, lsum_filter = 0;
            int rsum = 0, rsum_filter = 0;
            int lp_n = 0, lp_n_filter = 0;
            int rp_n = 0, rp_n_filter = 0;
            int HitK = 10;

            int Iteration = 0;
            int SmpIdx = 0;
            GraphQueryData Query { get; set; }

            public TopKPredictionRunner(GraphQueryData query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Iteration = 0;
            }

            public override void Init()
            {
                SmpIdx = 0;
                lsum = 0;
                lsum_filter = 0;
                rsum = 0;
                rsum_filter = 0;
                lp_n = 0;
                lp_n_filter = 0;
                rp_n = 0;
                rp_n_filter = 0;
            }

            public void Report()
            {
                Logger.WriteLog("Sample Idx {0}", SmpIdx);

                Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));

                Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));

                Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
                Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
                Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
                Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
            }

            public override void Complete()
            {
                Logger.WriteLog("Final Report!");
                {
                    Report();
                }
                Iteration += 1;
            }
            public override void Forward()
            {
                for (int t = Query.StatusPath.Count - 1; t >= 0; t--)
                {
                    if (Query.StatusPath[t].Term != null)
                    {
                        Query.StatusPath[t].Term.Output.Data.SyncToCPU();
                    }
                }

                for (int g = 0; g < Query.BatchSize; g++)
                {
                    int relation = Query.RawQuery[g].Item2;
                    int target = Query.RawTarget[g];
                    HashSet<int> blackTargets = Query.BlackTargets[g];

                    Dictionary<int, float> score = new Dictionary<int, float>();

                    for (int p = 0; p < Query.StatusPath.Count; p++)
                    {
                        foreach(int b in Query.StatusPath[p].GetBatchIdxs(g))
                        {
                            int nidx = Query.StatusPath[p].NodeID[b];
                            float logP = Query.StatusPath[p].GetLogProb(b);
                            if(Query.StatusPath[p].IsTermable(b))
                            {
                                float s = logP + Query.StatusPath[p].LogPTerm(b, true);

                                if (!score.ContainsKey(nidx)) { score[nidx] = float.MinValue; }
                                score[nidx] = Util.LogAdd(score[nidx], s); 
                            }
                        }
                    }
                    var sortD = score.OrderByDescending(pair => pair.Value);
                    int filter = 1;
                    int nonFilter = 1;
                    bool isFound = false;
                    foreach(KeyValuePair<int, float> d in sortD)
                    {
                        int ptail = d.Key;

                        if (ptail == target)
                        {
                            isFound = true;
                            break;
                        }
                        nonFilter += 1;
                        if (!blackTargets.Contains(ptail)) { filter++; }
                    }
                    
                    if(!isFound)
                    {
                        int tail = (DataPanel.EntityNum - nonFilter);
                        int topBlack = nonFilter - filter;
                        int tailBlack = blackTargets.Count - topBlack;
                        nonFilter = nonFilter + tail / 2;
                        filter = filter + (tail - tailBlack) / 2;
                    }
                    //else
                    //{
                    //    Console.WriteLine("Yes, I find it!");
                    //}
                    if (relation < DataPanel.RelationNum)
                    {
                        Interlocked.Add(ref lsum, nonFilter);
                        Interlocked.Add(ref lsum_filter, filter);
                        if (nonFilter <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
                        if (filter <= HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
                    }
                    else
                    {
                        Interlocked.Add(ref rsum, nonFilter);
                        Interlocked.Add(ref rsum_filter, filter);
                        if (nonFilter <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
                        if (filter <= HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
                    }
                }
                SmpIdx += Query.BatchSize;
            }
        }

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure InNodeEmbed { get; set; }
            public EmbedStructure InRelEmbed { get; set; }

            public EmbedStructure CNodeEmbed { get; set; }
            public EmbedStructure CRelEmbed { get; set; }

            public DNNStructure SrcDNN { get; set; }
            public DNNStructure TgtDNN { get; set; }

            public LayerStructure AttEmbed { get; set; }
            public DNNStructure TermDNN { get; set; }

            public GRUCell GruCell { get; set; }

            public NeuralWalkerModel(int nodeDim, int relDim, DeviceType device)
            {
                InNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                InRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum * 2, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
                CNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device));// DeviceType.CPU_FAST_VECTOR));
                CRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum * 2, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

                SrcDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                TgtDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                AttEmbed = AddLayer(new LayerStructure(BuilderParameters.DNN_DIMS.Last(), 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device));


                TermDNN = AddLayer(new DNNStructure(nodeDim + relDim, BuilderParameters.T_NET, BuilderParameters.T_AF,
                                         BuilderParameters.T_NET.Select(i => true).ToArray(), device));

                GruCell = AddLayer(new GRUCell(nodeDim + relDim, nodeDim + relDim, device));

            }
            public NeuralWalkerModel(BinaryReader reader, DeviceType device) 
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                InNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                InRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

                CNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                CRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
                TgtDNN = (DNNStructure)DeserializeNextModel(reader, device);

                AttEmbed = (LayerStructure)DeserializeNextModel(reader, device);
                TermDNN = (DNNStructure)DeserializeNextModel(reader, device);
                GruCell = (GRUCell)DeserializeNextModel(reader, device);

            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public static ComputationGraph BuildComputationGraph(List<Tuple<int, int, int>> graph,
                                                             List<List<Tuple<int, List<Tuple<int, int, int>>>>> path,
                                                             int batchSize, int beamSize,
                                                             NeuralWalkerModel model, RunnerBehavior Behavior)
        {

            ComputationGraph cg = new ComputationGraph();

            // sample a list of tuplet.
            // group of beamsize.
            SampleRunner SmpRunner = new SampleRunner(graph, path, batchSize, beamSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
            cg.AddRunner(statusEmbedRunner);

            StatusData status = new StatusData(interface_data, interface_data.RawSource, statusEmbedRunner.Output, Behavior.Device);

            // travel four steps in the knowledge graph.
            for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
            {
                CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
                cg.AddRunner(candidateActionRunner);

                StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, model.CNodeEmbed, model.CRelEmbed, Behavior);
                cg.AddRunner(candEmbedRunner);

                DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                cg.AddRunner(srcHiddenRunner);

                DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
                cg.AddRunner(candHiddenRunner);

                VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
                                                                            candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
                cg.AddRunner(attentionRunner);

                SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
                                                                            status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
                                                                            candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, 
                                                                            Behavior.Device), 1, Behavior, 
                                                                            true);
                cg.AddRunner(normAttRunner);

                status.MatchCandidate = candidateActionRunner.Output;
                status.MatchCandidateProb = normAttRunner.Output;
                //it will cause un-necessary unstable.

                //status.CandidateProb = normAttRunner.Output;
                ActionSamplingRunner actionRunner = new ActionSamplingRunner(status, Behavior);
                cg.AddRunner(actionRunner);

                StatusEmbedRunner actionEmbedRunner = new StatusEmbedRunner(actionRunner.Action, status.MaxBatchSize, model.CNodeEmbed, model.CRelEmbed, Behavior);
                cg.AddRunner(actionEmbedRunner);

                GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, status.StateEmbed, actionEmbedRunner.Output, Behavior);
                cg.AddRunner(stateRunner);

                StatusData nextStatus = new StatusData(status.GraphQuery, actionRunner.NodeIndex, null, actionRunner.LogProb, actionRunner.NodeAction,  stateRunner.Output, Behavior.Device);

                DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
                cg.AddRunner(termRunner);
                nextStatus.Term = termRunner.Output;
                status = nextStatus;
            }

            if (BuilderParameters.TrainSetting == 0)
            {
                SupervisedRunner supervisedRunner = new SupervisedRunner(interface_data, Behavior);
                //RewardRunner rewardRunner = new RewardRunner(interface_data, Behavior);
                cg.AddObjective(supervisedRunner);
            }
            else if(BuilderParameters.TrainSetting == 1)
            {
                RewardRunner rewardRunner = new RewardRunner(interface_data, Behavior);
                cg.AddObjective(rewardRunner);
            }

            cg.SetDelegateModel(model);
            return cg;
        }

        public static ComputationGraph BuildPredComputationGraph(List<Tuple<int, int, int>> graph,
                                                            int batchSize, int beamSize,
                                                            NeuralWalkerModel model, RunnerBehavior Behavior)
        {

            ComputationGraph cg = new ComputationGraph();

            // sample a list of tuplet.
            SampleRunner SmpRunner = new SampleRunner(graph, null, batchSize, 1, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            // 16 maxbatchsize. 
            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
            cg.AddRunner(statusEmbedRunner);

            // 16 minibatches.
            StatusData status = new StatusData(interface_data, interface_data.RawSource, statusEmbedRunner.Output, Behavior.Device);

            // travel four steps in the knowledge graph.
            for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
            {
                //given status, obtain the match. miniBatch * maxNeighbor number.
                CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
                cg.AddRunner(candidateActionRunner);

                // miniMatch * maxNeighborNumber.
                StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, model.CNodeEmbed, model.CRelEmbed, Behavior);
                cg.AddRunner(candEmbedRunner);

                DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                cg.AddRunner(srcHiddenRunner);

                DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
                cg.AddRunner(candHiddenRunner);

                // alignment miniBatch * miniBatch * neighbor
                VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
                                                                            candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified) { name = "align" + i.ToString() };
                cg.AddRunner(attentionRunner);

                SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
                                                                            status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
                                                                            candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
                                                                            Behavior.Device), 1, Behavior,
                                                                            true);
                cg.AddRunner(normAttRunner);

                status.MatchCandidate = candidateActionRunner.Output;
                status.MatchCandidateProb = normAttRunner.Output;
                //it will cause un-necessary unstable.

                //status.CandidateProb = normAttRunner.Output;
                BeamSearchActionRunner actionRunner = new BeamSearchActionRunner(status, BuilderParameters.BeamSize, Behavior);
                cg.AddRunner(actionRunner);

                // return:
                // MatchPath { get; set; }
                // List<int> NodeIndex { get; set; }
                // List<float> NodeProb { get; set; }
                // List<Tuple<int, int>> NodeAction { get; set; }

                MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, actionRunner.MatchPath, 1, Behavior);
                cg.AddRunner(srcExpRunner);

                MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, actionRunner.MatchPath, 2, Behavior);
                cg.AddRunner(tgtExpRunner);

                //StatusEmbedRunner actionEmbedRunner = new StatusEmbedRunner(actionRunner.SelectAction, status.MaxBatchSize, model.CNodeEmbed, model.CRelEmbed, Behavior);
                //cg.AddRunner(actionEmbedRunner);
                GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
                cg.AddRunner(stateRunner);

                StatusData nextStatus = new StatusData(status.GraphQuery, 
                    actionRunner.NodeIndex, actionRunner.GroupIndex, actionRunner.NodeProb, actionRunner.NodeAction, 
                    stateRunner.Output, Behavior.Device );

                DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
                cg.AddRunner(termRunner);
                nextStatus.Term = termRunner.Output;
                status = nextStatus;
            }

            TopKPredictionRunner predRunner = new TopKPredictionRunner(interface_data, Behavior);
            cg.AddRunner(predRunner);

            cg.SetDelegateModel(model);
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");


            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);

            model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.knowledgeGraph.Valid, 
                        BuilderParameters.GUID_RATE == 0 ? null : DataPanel.ValidPath, BuilderParameters.MiniBatchSize,
                        BuilderParameters.TrainBeamSize, model, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    
                    ComputationGraph predCG = BuildPredComputationGraph(DataPanel.knowledgeGraph.Test, BuilderParameters.TestMiniBatchSize,BuilderParameters.BeamSize, model,
                                       new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    //predCG.Execute();

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        //ActionSampler = new Random(10);
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        if ( (iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);

                            predCG.Execute();
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            {
                                model.Serialize(writer);
                            }
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    //ComputationGraph predCG = BuildComputationGraph(DataPanel.knowledgeGraph.Test, null, BuilderParameters.MiniBatchSize,
                    //    BuilderParameters.BeamSize, model, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    break;
                
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// load data.
        /// </summary>
        public class DataPanel
        {
            public static RelationGraphData knowledgeGraph;

            public static int EntityNum { get { return knowledgeGraph.EntityNum; } }
            public static int RelationNum { get { return knowledgeGraph.RelationNum; } }

            public static List<Tuple<int, int, int>> Train { get { return knowledgeGraph.Train; } }


            public static List<Tuple<int, int, int>> Test
            {
                get { return knowledgeGraph.Test; }
            }

            public static List<Tuple<int, int, int>> Valid
            {
                get { return knowledgeGraph.Valid; }
            }

            /// <summary>
            /// start end relation
            /// </summary>
            public static List<List<Tuple<int, List<Tuple<int, int, int>>>>> ValidPath = new List<List<Tuple<int, List<Tuple<int, int, int>>>>>();

            public static int ReverseRelation(int relation)
            {
                if (relation >= DataPanel.RelationNum) { return relation - DataPanel.RelationNum; }
                else { return relation + DataPanel.RelationNum; }
            }

            public static List<Tuple<int, int, int>> ReversePath(List<Tuple<int, int, int>> path)
            {
                List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();

                for (int i = path.Count - 1; i >= 0; i--)
                {
                    result.Add(new Tuple<int, int, int>(path[i].Item2, path[i].Item1, ReverseRelation(path[i].Item3)));

                    int n1 = path[i].Item2;
                    int n2 = path[i].Item1;
                    int r = ReverseRelation(path[i].Item3);
                    if (n1 == 10565 && r == 10565 && n2 == 4187)
                    {
                        Console.WriteLine("Find the issue in reverse path ");
                        throw new Exception("Error!!");
                    }
                }
                return result;
            }

            static void LoadSamplePath(string pathfolder)
            {
                for (int i = 0; i < Valid.Count; i++)
                {
                    ValidPath.Add(new List<Tuple<int, List<Tuple<int, int, int>>>>());
                }

                string[] fileEntries = Directory.GetFiles(pathfolder);
                foreach (string mpathfile in fileEntries)
                {
                    int pathIdx = int.Parse(mpathfile.Substring(mpathfile.LastIndexOf('.') + 1));

                    using (StreamReader mreader = new StreamReader(mpathfile))
                    {
                        int line = 0;

                        while (!mreader.EndOfStream)
                        {
                            string[] stops = mreader.ReadLine().Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries);
                            if (stops.Length >= 1)
                            {
                                List<Tuple<int, int, int>> path = new List<Tuple<int, int, int>>();
                                foreach (string stop in stops)
                                {
                                    string[] tuple = stop.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                    int n1 = int.Parse(tuple[0]);
                                    int r = int.Parse(tuple[1]);
                                    int n2 = int.Parse(tuple[2]);
                                    path.Add(new Tuple<int, int, int>(n1, n2, r));

                                    if(n1 == 10565 &&  r == 10565 && n2 == 4187)
                                    {
                                        Console.WriteLine("Find the issue {0}, {1}", mpathfile, line);
                                        throw new Exception("Error!!");
                                    }
                                }
                                ValidPath[line].Add(new Tuple<int, List<Tuple<int, int, int>>>(pathIdx, path));
                            }
                            line = line + 1;
                        }
                    }
                    pathIdx += 1;
                }
            }

            public static double[] HeadProb { get { return knowledgeGraph.HeadProb; } }

            public static double[] TailProb { get { return knowledgeGraph.TailProb; } }

            public static int MaxNeighborCount { get { return knowledgeGraph.TrainNeighborLink.Select(i => i.Value.Count).Max(); } }

            public static bool IsInGraph(int eid1, int eid2, int rid)
            {
                if (rid < RelationNum)
                {
                    if (knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
                    if (knowledgeGraph.TrainGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
                }
                else if (rid >= RelationNum)
                {
                    if(knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid2, eid1, rid - RelationNum))) return true;
                    if (knowledgeGraph.TrainGraphHash.Contains(string.Format("{0}#{1}#{2}", eid2, eid1, rid - RelationNum))) return true;
                }
                return false;
            }

            //public static bool IsInTrainGraph(int eid1, int eid2, int rid)
            //{
            //    if (rid < RelationNum && knowledgeGraph.TrainGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
            //    else if (rid >= RelationNum && knowledgeGraph.TrainGraphHash.Contains(string.Format("{0}#{1}#{2}", eid2, eid1, rid - RelationNum))) return true;
            //    else return false;
            //}

            public static void Init()
            {
                knowledgeGraph = new RelationGraphData();

                if (!BuilderParameters.Entity2Id.Equals(string.Empty)) knowledgeGraph.EntityId = RelationGraphData.LoadMapId(BuilderParameters.Entity2Id);
                else knowledgeGraph.EntityNum = BuilderParameters.EntityNum;
                if (!BuilderParameters.Relation2Id.Equals(string.Empty)) knowledgeGraph.RelationId = RelationGraphData.LoadMapId(BuilderParameters.Relation2Id);
                else knowledgeGraph.RelationNum = BuilderParameters.RelationNum;

                knowledgeGraph.Train = knowledgeGraph.LoadGraphV2(BuilderParameters.TrainData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, true);
                if(BuilderParameters.IsValidFile) knowledgeGraph.Valid = knowledgeGraph.LoadGraphV2(BuilderParameters.ValidData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
                if(BuilderParameters.IsTestFile) knowledgeGraph.Test = knowledgeGraph.LoadGraphV2(BuilderParameters.TestData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);

                LoadSamplePath(BuilderParameters.ValidPathFolder);
                int emptyPath = ValidPath.Select(i => i.Count == 0 ? 1 : 0).Sum();
                
                Console.WriteLine("Entity Number {0}, Relation Number {1}", knowledgeGraph.EntityNum, knowledgeGraph.RelationNum);
                Console.WriteLine("Valid Empty Path {0}, Valid Total Path {1}", emptyPath, ValidPath.Count);
            }
        }
    }
}
