﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
namespace BigLearn.DeepNet
{
    public class AppReasoNetGraphReader : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("VERTEX-TYPE", new ParameterArgument("10", "Number of Vertex Type."));
                Argument.Add("VERTEX-NUM", new ParameterArgument("16", "Number of Vertexes"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Training Data"));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Validation Data"));


                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument("0", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("WORD-DROPOUT", new ParameterArgument("0", "Dropout for Word Embedding 1."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding 1 File"));
                Argument.Add("UNK-INIT", new ParameterArgument("0", "0 : zero initialize; 1 : random initialize"));

                Argument.Add("CHAR-EMBED-DIM", new ParameterArgument("0", "Char Embedding Dim."));
                Argument.Add("CHAR-EMBED-WIN", new ParameterArgument("5", "Char Embedding Win."));

                Argument.Add("HIGHWAY-LAYER", new ParameterArgument("2", "Highway Layers."));
                Argument.Add("HIGHWAY-SHARE", new ParameterArgument("0", "0: Nonshare highway; 1 : share highway;"));

                Argument.Add("LSTM-DIM", new ParameterArgument("256,256", "LSTM Dimension."));


                Argument.Add("MEMORY-ENSEMBLE", new ParameterArgument("2", "2 : two-way ensemble; 3 : three-way ensemble;"));

                Argument.Add("SOFT1-DROPOUT", new ParameterArgument("0", "Soft 1 Dropout;"));
                Argument.Add("SOFT2-DROPOUT", new ParameterArgument("0", "Soft 2 Dropout;"));
                Argument.Add("LSTM-DROPOUT", new ParameterArgument("0", "LSTM Embed Dropout;"));
                Argument.Add("EMBED-DROPOUT", new ParameterArgument("0", "Embeding Dropout;"));
                Argument.Add("COATT-DROPOUT", new ParameterArgument("0", "CoATT Dropout;"));
                Argument.Add("CNN-DROPOUT", new ParameterArgument("0", "CNN Dropout;"));
                Argument.Add("L3G-DROPOUT", new ParameterArgument("0", "L3G DNN Dropout;"));
                Argument.Add("CHAR-DROPOUT", new ParameterArgument("0", "Char CNN Dropout;"));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

                Argument.Add("ANS-HID", new ParameterArgument("256", "Answer Hidden Dimension"));
                Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("SELF-ATT-HID", new ParameterArgument("128", "Attention Hidden Dimension"));
                Argument.Add("SELF-ATT-TYPE", new ParameterArgument(((int)CrossSimType.Addition).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                Argument.Add("SELF-ATT-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Attention Af : " + ParameterUtil.EnumValues(typeof(A_Func))));

                Argument.Add("RECURRENT-DIM", new ParameterArgument("128", "Recurrent state dimension,"));
                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.RL)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));
                Argument.Add("QUERY-POOL", new ParameterArgument((((int)PoolingType.LAST)).ToString(), "Query Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));


                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
                Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("RL-ALL", new ParameterArgument("1", "RL All"));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));

                Argument.Add("SPAN-OBJECTIVE", new ParameterArgument("0", "Span Objective; 0 : start-end detection; 1 : span start-end detection."));
                Argument.Add("SPAN-LENGTH", new ParameterArgument("0", "Span length"));
                Argument.Add("TRAIN-SPAN-LENGTH", new ParameterArgument("10000", "Span length"));
                Argument.Add("MAX-D-LENGTH", new ParameterArgument("0", "Maximum Document length"));

                Argument.Add("ENDPOINT-CONFIG", new ParameterArgument("0", "0: old M2; 1: new M2;"));
                Argument.Add("STARTPOINT-CONFIG", new ParameterArgument("0", "0: old M1; 1: new M1; 2: all new M1;"));

                //TERMINATE-DISCOUNT
                Argument.Add("TERMINATE-DISCOUNT", new ParameterArgument("0.1", "Reward discount on terminate gate;"));
            }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static int VERTEX_TYPE { get { return int.Parse(Argument["VERTEX-TYPE"].Value); } }
            public static int VERTEX_NUM { get { return int.Parse(Argument["VERTEX-NUM"].Value); } }

            /// <summary>
            /// Feature 0 : X
            /// Feature 1 : ->
            /// Feature [2, VERTEX_TYPE + 1] : vertex_type
            /// Feature [VERTEX_TYPE + 2, VERTEX_TYPE + 2 + VERTEX_NUM] : vertex_num
            /// </summary>
            public static int FEATURE_DIM { get { return VERTEX_TYPE + VERTEX_NUM + 1 + 1; } }
            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            #region train, test, validation dataset.
            public static string Train { get { return Argument["TRAIN"].Value; } }
            public static bool IsTrainFile { get { return (!Train.Equals(string.Empty)); } }
            public static string TrainContextBin { get { return string.Format("{0}.para.{1}.bin", Train, MiniBatchSize); } }
            public static string TrainQueryBin { get { return string.Format("{0}.ques.{1}.bin", Train, MiniBatchSize); } }
            public static string TrainAnsBin { get { return string.Format("{0}.ans.{1}.bin", Train, MiniBatchSize); } }


            public static string Valid { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return (!Valid.Equals(string.Empty)); } }
            public static string ValidContextBin { get { return string.Format("tmp.para.{0}.bin", MiniBatchSize); } }
            public static string ValidQueryBin { get { return string.Format("tmp.ques.{0}.bin", MiniBatchSize); } }
            public static string ValidAnsBin { get { return string.Format("tmp.ans.{0}.bin", MiniBatchSize); } }
            #endregion.

            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            
            public static int[] LSTM_DIM { get { return Argument["LSTM-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int ANS_HID_DIM { get { return int.Parse(Argument["ANS-HID"].Value); } }
            public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
            
            public static int SELF_ATT_HID_DIM { get { return int.Parse(Argument["SELF-ATT-HID"].Value); } }
            public static int SelfAttType { get { return int.Parse(Argument["SELF-ATT-TYPE"].Value); } }
            public static A_Func SelfAttAF { get { return (A_Func)int.Parse(Argument["SELF-ATT-AF"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

            public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }

            public static float TERMINATE_DISCOUNT { get { return float.Parse(Argument["TERMINATE-DISCOUNT"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_GRAPH_READER; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        public enum PredType { RL_MAXITER, RL_AVGSIM, RL_AVGPROB, RL_ENSEMBLE3 }

        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class MemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }
            public HiddenBatchData SoftmaxAddress { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            CudaPieceInt Tgt2Src;
            CudaPieceFloat Tgt2SrcSoftmax;

            //CudaPieceFloat tmpMem;
            //CudaPieceFloat tmpMem2;
            public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                MatchData = matchData;
                Gamma = gamma;
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
                Tgt2Src = new CudaPieceInt(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                Tgt2SrcSoftmax = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);


                //tmpMem = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
                //tmpMem2 = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
            }

            public override void Forward()
            {
                Output.BatchSize = MatchData.SrcSize;

                // softmax 
                // softmax of attention
                ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

                ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
                    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                    Memory.SentSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {

                //tmpMem.CopyFrom(Memory.SentDeriv);
                //tmpMem.Zero();


                // outputDeriv -> memoryContentDeriv.

                //tmpMem2.CopyFrom(Memory.SentDeriv);
                //if (BuilderParameters.DEBUG_MODE == 0)
                {
                    SoftmaxAddress.Output.Data.SyncToCPU(MatchData.MatchSize);
                    for (int i = 0; i < MatchData.MatchSize; i++)
                    {
                        int matchIdx = MatchData.Tgt2MatchElement.MemPtr[i];
                        Tgt2Src.MemPtr[i] = MatchData.SrcIdx.MemPtr[matchIdx];
                        Tgt2SrcSoftmax.MemPtr[i] = SoftmaxAddress.Output.Data.MemPtr[matchIdx];
                    }
                    Tgt2Src.SyncFromCPU(MatchData.MatchSize);
                    Tgt2SrcSoftmax.SyncFromCPU(MatchData.MatchSize);
                    ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, Tgt2Src, 0, Tgt2SrcSoftmax, // SoftmaxAddress.Output.Data,
                                                 MatchData.Tgt2MatchIdx, MatchData.TgtSize, Memory.SentDeriv /*Memory.SentDeriv*/, 0, CudaPieceInt.Empty, 0,
                                                 MatchData.MatchSize, Memory.Dim, 1, 1);
                    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem, Memory.SentSize, Memory.Dim, 1);
                }
                //else if (BuilderParameters.DEBUG_MODE == 1)
                //{
                //    //tmpMem2.Zero();
                //    ComputeLib.AccurateScale_Matrix(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
                //                                Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
                //                                Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data);
                //    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem2, Memory.SentSize, Memory.Dim, 1);
                //}

                //tmpMem.SyncToCPU(Memory.SentSize * Memory.Dim);
                //tmpMem2.SyncToCPU(Memory.SentSize * Memory.Dim);
                //for (int i = 0; i < Memory.SentSize * Memory.Dim; i++)
                //{
                //    if (Math.Abs(tmpMem2.MemPtr[i] - tmpMem.MemPtr[i]) >= 0.000000001)
                //    {
                //        Console.WriteLine("Adopt Debug Model {3},  Error! {0}, {1}, {2}", tmpMem.MemPtr[i], tmpMem2.MemPtr[i], i, BuilderParameters.DEBUG_MODE);
                //    }
                //}
                //ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, )

                // outputDeriv -> AddressDeriv.
                ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }

        class AnswerRunner : CompositeNetRunner
        {
            public HiddenBatchData AnsOutput { get; set; }
            public HiddenBatchData TerminateOutput { get; set; }
            public AnswerRunner(SeqDenseBatchData status, Tuple<LSTMStructure, LSTMStructure> lstmScan, LayerStructure ansLayer, LayerStructure terminateLayer, RunnerBehavior behavior) : base(behavior)
            {
                BiLSTMRunner scanRunner = new BiLSTMRunner(lstmScan.Item1, lstmScan.Item2, status, behavior, true);
                LinkRunners.Add(scanRunner);

                FullyConnectHiddenRunner<HiddenBatchData> outputRunner = new FullyConnectHiddenRunner<HiddenBatchData>(ansLayer, scanRunner.LastStatus, behavior);
                LinkRunners.Add(outputRunner);

                FullyConnectHiddenRunner<HiddenBatchData> terminateRunner = new FullyConnectHiddenRunner<HiddenBatchData>(terminateLayer, scanRunner.LastStatus, behavior);
                LinkRunners.Add(outputRunner);

                AnsOutput = outputRunner.Output;
                TerminateOutput = terminateRunner.Output;
            }
        }

        class AttentionRunner : CompositeNetRunner
        {
            public new SeqDenseBatchData Output { get; set; }
            public AttentionRunner(SeqDenseBatchData src, SeqDenseBatchData tgt, BiMatchBatchData match, 
                LayerStructure srcM, LayerStructure tgtM, LayerStructure attM, int attOp, A_Func attFunc, RunnerBehavior behavior) : base(behavior)
            {
                FullyConnectHiddenRunner<HiddenBatchData> srcRunner = new FullyConnectHiddenRunner<HiddenBatchData>(srcM, new HiddenBatchData(new MatrixData(src)), behavior);
                LinkRunners.Add(srcRunner);

                FullyConnectHiddenRunner<HiddenBatchData> tgtRunner = new FullyConnectHiddenRunner<HiddenBatchData>(tgtM, new HiddenBatchData(new MatrixData(tgt)), behavior);
                LinkRunners.Add(tgtRunner);

                VecAlignmentRunner alignRunner = new VecAlignmentRunner(new MatrixData(srcRunner.Output), new MatrixData(tgtRunner.Output), match, attM, Behavior, attOp, attFunc);
                LinkRunners.Add(alignRunner);

                MemoryRetrievalRunner memRunner = new MemoryRetrievalRunner(alignRunner.Output, tgt, match, 1, Behavior);
                LinkRunners.Add(memRunner);

                SeqDenseBatchData memData = new SeqDenseBatchData(new SequenceDataStat(src.MAX_BATCHSIZE, src.MAX_SENTSIZE, tgt.Dim), src.SampleIdx, src.SentMargin,
                                                                    memRunner.Output.Output.Data, memRunner.Output.Deriv.Data, Behavior.Device);

                Output = memData;
            }
        }

        class UpdateRunner : CompositeNetRunner
        {
            public new SeqDenseBatchData Output { get; set; }
            public UpdateRunner(SeqDenseBatchData state, SeqDenseBatchData x, Tuple<LSTMStructure, LSTMStructure> updateStruct, RunnerBehavior behavior) : base(behavior)
            {
                EnsembleConcateSeqRunner concateRunner = new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { state, x }, behavior);
                LinkRunners.Add(concateRunner);

                BiLSTMRunner lstmUpdateRunner = new BiLSTMRunner(updateStruct.Item1, updateStruct.Item2, concateRunner.Output, behavior);
                LinkRunners.Add(lstmUpdateRunner);

                Output = lstmUpdateRunner.Output;
            }
        }

        
        class EpisodicReasoNetRunner : CompositeNetRunner
        {
            int MaxT { get; set; }

            SeqDenseBatchData InitState { get; set; }
            public HiddenBatchData[] Ans { get; set; }
            public HiddenBatchData[] Terminate { get; set; }
            public HiddenBatchData[] TermProb { get; set; }
            public EpisodicReasoNetRunner(SeqDenseBatchData initState, SeqDenseBatchData memory, int maxCrossLen, int maxCrossStateLen,

                Tuple<LayerStructure,LayerStructure,LayerStructure,int,A_Func> attMemStruct,
                Tuple<LSTMStructure, LSTMStructure> updateMemStruct,

                Tuple<LayerStructure, LayerStructure, LayerStructure, int, A_Func> attSelfStruct,
                Tuple<LSTMStructure, LSTMStructure> updateSelfStruct,

                Tuple<LSTMStructure, LSTMStructure> ansScanStruct, LayerStructure ansLayer, LayerStructure terminateLayer,
                int maxT, RunnerBehavior behavior) : base(behavior)
            {
                MaxT = maxT;
                InitState = initState;

                // Step 0.1: construct state to memory match data.
                CrossSeqBiMatchRunner matchRunner = new CrossSeqBiMatchRunner(initState, memory, maxCrossLen, Behavior);
                LinkRunners.Add(matchRunner);
                BiMatchBatchData q2dMatchData = matchRunner.Output;

                // Step 0.2: construct state to state match data.
                CrossSeqBiMatchRunner selfMatchRunner = new CrossSeqBiMatchRunner(initState, initState, maxCrossStateLen, Behavior);
                LinkRunners.Add(selfMatchRunner);
                BiMatchBatchData q2qMatchData = selfMatchRunner.Output;

                SeqDenseBatchData currentState = initState;

                Ans = new HiddenBatchData[MaxT];
                Terminate = new HiddenBatchData[MaxT];

                for (int i = 0; i < MaxT; i++)
                {
                    // Step 1: attention from state to memory.
                    AttentionRunner attMemRunner = new AttentionRunner(currentState, memory, q2dMatchData, attMemStruct.Item1, attMemStruct.Item2, attMemStruct.Item3, attMemStruct.Item4, attMemStruct.Item5, behavior);
                    LinkRunners.Add(attMemRunner);
                    UpdateRunner updateMemRunner = new UpdateRunner(currentState, attMemRunner.Output, updateMemStruct, behavior);
                    LinkRunners.Add(updateMemRunner);
                    currentState = updateMemRunner.Output;

                    // Step 2: attention from state to state.
                    AttentionRunner attSelfRunner = new AttentionRunner(currentState, currentState, q2qMatchData, attSelfStruct.Item1, attSelfStruct.Item2, attSelfStruct.Item3, attSelfStruct.Item4, attSelfStruct.Item5, behavior);
                    LinkRunners.Add(attSelfRunner);
                    UpdateRunner updateSelfRunner = new UpdateRunner(currentState, attSelfRunner.Output, updateSelfStruct, behavior);
                    LinkRunners.Add(updateSelfRunner);
                    currentState = updateSelfRunner.Output;

                    AnswerRunner ansRunner = new AnswerRunner(currentState, ansScanStruct, ansLayer, terminateLayer, behavior);
                    LinkRunners.Add(ansRunner);

                    Ans[i] = ansRunner.AnsOutput;
                    Terminate[i] = ansRunner.TerminateOutput;
                }

                TermProb = new HiddenBatchData[MaxT];
                for (int i = 0; i < MaxT; i++)
                {
                    TermProb[i] = new HiddenBatchData(InitState.Stat.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                }
            }

            public override void Forward()
            {
                base.Forward();
                
                // cascading tree termination probability.
                for (int i = 0; i < MaxT; i++)
                {
                    ComputeLib.Logistic(Terminate[i].Output.Data, 0, Terminate[i].Output.Data, 0, Terminate[i].Output.BatchSize, 1);
                    ComputeLib.ClipVector(Terminate[i].Output.Data, Terminate[i].BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
                    Terminate[i].Output.Data.SyncToCPU(Terminate[i].BatchSize);
                }

                for (int b = 0; b < InitState.BatchSize; b++)
                {
                    float acc_log_t = 0;
                    for (int i = 0; i < MaxT; i++)
                    {
                        float t_i = Terminate[i].Output.Data.MemPtr[b];
                        if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                        float p = acc_log_t + (float)Math.Log(t_i);
                        if (i == MaxT - 1) p = acc_log_t;

                        TermProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                        acc_log_t += (float)Math.Log(1 - t_i);
                    }
                }
                for (int i = 0; i < MaxT; i++)
                {
                    TermProb[i].BatchSize = InitState.BatchSize;
                    TermProb[i].Output.Data.SyncFromCPU(TermProb[i].BatchSize);
                }
            }

            public override void CleanDeriv()
            {
                base.CleanDeriv();
                for (int i = 0; i < MaxT; i++)
                {
                    ComputeLib.Zero(TermProb[i].Deriv.Data, TermProb[i].BatchSize);
                }
            }

            public override void Backward(bool cleanDeriv)
            {
                for (int b = 0; b < InitState.BatchSize; b++)
                {
                    for (int i = 0; i < MaxT; i++)
                    {
                        float reward = TermProb[i].Deriv.Data.MemPtr[b] * BuilderParameters.TERMINATE_DISCOUNT;
                        Terminate[i].Deriv.Data.MemPtr[b] += reward * (1 - Terminate[i].Output.Data.MemPtr[b]);
                        for (int hp = 0; hp < i; hp++)
                        {
                            Terminate[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * reward * Terminate[hp].Output.Data.MemPtr[b];
                        }

                        if (Terminate[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
                        {
                            Logger.WriteLog("Start TerminalRunner Deriv is too large {0}, step {1}", Terminate[i].Deriv.Data.MemPtr[b], i);
                        }
                    }
                    Terminate[MaxT - 1].Deriv.Data.MemPtr[b] = 0;
                }

                for (int i = 0; i < MaxT; i++)
                {
                    Terminate[i].Deriv.Data.SyncFromCPU(Terminate[i].BatchSize);
                }

                base.Backward(cleanDeriv);
            }


        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<DenseBatchData, DenseDataStat> ans,

                                                             // text embedding cnn.
                                                             LayerStructure EmbedCNN,

                                                             // multilayer lstm embedding.
                                                             List<Tuple<LSTMStructure, LSTMStructure>> EmbedLSTM,

                                                             Tuple<LayerStructure, LayerStructure, LayerStructure, int, A_Func> AttMemStruct,
                                                             Tuple<LSTMStructure, LSTMStructure> UpdateMemStruct,

                                                             Tuple<LayerStructure, LayerStructure, LayerStructure, int, A_Func> AttSelfStruct,
                                                             Tuple<LSTMStructure, LSTMStructure> UpdateSelfStruct,

                                                             // answer and terminate module.
                                                             Tuple<LSTMStructure, LSTMStructure> AnsScan, LayerStructure AnsLayer, LayerStructure TermLayer,

                                                             // model.
                                                             int maxT, CompositeNNStructure model, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();


            /************************************************************ Query Passage data *********/
            #region Query Doc Answer Data.
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
            DenseBatchData AnsData = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(ans, Behavior));
            #endregion.

            /************************************************************ Word Embedding on Query and Passage. *********/
            #region Word Embedding on Q and D.
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, QueryData, Behavior));
            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, ContextData, Behavior));
            #endregion.

            #region Multilayer LSTM for Q and D.
            List<SeqDenseBatchData> multiLayer_Qs = new List<SeqDenseBatchData>();
            List<SeqDenseBatchData> multiLayer_Ds = new List<SeqDenseBatchData>();

            multiLayer_Qs.Add(QueryEmbedOutput);
            multiLayer_Ds.Add(ContextEmbedOutput);

            for (int l = 0; l < EmbedLSTM.Count; l++)
            {
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(EmbedLSTM[l].Item1, EmbedLSTM[l].Item2, ContextEmbedOutput, Behavior) { name = "DocSeqO Layer_" + l.ToString() });
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(EmbedLSTM[l].Item1, EmbedLSTM[l].Item2, QueryEmbedOutput, Behavior) { name = "QuerySeqO Layer_" + l.ToString() });

                multiLayer_Qs.Add(QueryEmbedOutput);
                multiLayer_Ds.Add(ContextEmbedOutput);
            }
            #endregion.

            SeqDenseBatchData Query = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(multiLayer_Qs, Behavior) { name = "ensemble_Qs" });
            SeqDenseBatchData Doc = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(multiLayer_Ds, Behavior) { name = "ensemble_Ds" });
            // SeqDenseBatchData Query = multiLayer_Qs.Last();
            // SeqDenseBatchData Doc = multiLayer_Ds.Last();
            // define (query and doc) as state and working memory.

            EpisodicReasoNetRunner reasonet = new EpisodicReasoNetRunner(Query, Doc, DataPanel.MaxCrossQDLen, DataPanel.MaxCrossQQLen,
                AttMemStruct, UpdateMemStruct, AttSelfStruct, UpdateSelfStruct, AnsScan, AnsLayer, TermLayer, BuilderParameters.RECURRENT_STEP, Behavior);
            cg.AddRunner(reasonet);

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new MultiClassContrastiveRewardRunner(AnsData.Data, reasonet.Ans, reasonet.TermProb, 1, Behavior));
                    break;
                case DNNRunMode.Predict:
                    for (int d = 0; d < BuilderParameters.RECURRENT_STEP; d++)
                    {
                        cg.AddRunner(new MatrixSoftmaxRunner(new MatrixData(reasonet.Ans[d]), true, Behavior));
                    }
                    HiddenBatchData finalOutput = (HiddenBatchData)cg.AddRunner(new WAdditionMatrixRunner(reasonet.Ans.ToList(), reasonet.TermProb.ToList(), Behavior));
                    cg.AddRunner(new AccuracyDiskDumpRunner(AnsData.Data, finalOutput.Output, 1, BuilderParameters.ScoreOutputPath, true));
                    break;
            }

            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile, !DeepNet.BuilderParameters.IsLogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            bool IsCreateModel = true;
            BinaryReader modelReader = null;
            if (!BuilderParameters.SeedModel.Equals(string.Empty))
            {
                modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read));
                int modelNum = CompositeNNStructure.DeserializeModelCount(modelReader);
                IsCreateModel = false;
            }

            LayerStructure EmbedCNN = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(BuilderParameters.FEATURE_DIM, BuilderParameters.EMBED_LAYER_DIM[0], BuilderParameters.EMBED_ACTIVATION[0], N_Type.Convolution_layer, 1, 0, false, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);


            List<Tuple<LSTMStructure, LSTMStructure>> EmbedLSTM = new List<Tuple<LSTMStructure, LSTMStructure>>();
            int inputDim = BuilderParameters.EMBED_LAYER_DIM[0];
            for (int i = 0; i < BuilderParameters.LSTM_DIM.Length; i++)
            {
                EmbedLSTM.Add(new Tuple<LSTMStructure, LSTMStructure>(
                   IsCreateModel ? modelStructure.AddLayer(new LSTMStructure(inputDim, new int[] { BuilderParameters.LSTM_DIM[i] }, device, BuilderParameters.RndInit)) :
                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device),
                   IsCreateModel ? modelStructure.AddLayer(new LSTMStructure(inputDim, new int[] { BuilderParameters.LSTM_DIM[i] }, device, BuilderParameters.RndInit)) :
                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device)
                   ));
                inputDim = BuilderParameters.LSTM_DIM[i] * 2;
            }

            int stateDim = BuilderParameters.EMBED_LAYER_DIM[0] + BuilderParameters.LSTM_DIM.Sum() * 2;
            int memoryDim = BuilderParameters.EMBED_LAYER_DIM[0] + BuilderParameters.LSTM_DIM.Sum() * 2;


            LayerStructure SrcAttM = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(stateDim, BuilderParameters.ATT_HID_DIM, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

            LayerStructure TgtAttM = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(memoryDim, BuilderParameters.ATT_HID_DIM, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

            LayerStructure AttM = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(BuilderParameters.ATT_HID_DIM, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);
            int AttOp = 1;
            A_Func AttFunc = A_Func.Linear;

            LSTMStructure UMemLSTM1 = IsCreateModel ? modelStructure.AddLayer(
                                   new LSTMStructure(stateDim + memoryDim, new int[] { stateDim / 2 }, device, BuilderParameters.RndInit)) :
                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device);
            LSTMStructure UMemLSTM2 = IsCreateModel ? modelStructure.AddLayer(
                                   new LSTMStructure(stateDim + memoryDim, new int[] { stateDim / 2 }, device, BuilderParameters.RndInit)) :
                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device);


            LayerStructure SelfAttM1 = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(stateDim, BuilderParameters.SELF_ATT_HID_DIM, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

            LayerStructure SelfAttM2 = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(stateDim, BuilderParameters.SELF_ATT_HID_DIM, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

            LayerStructure SelfAttM = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(BuilderParameters.SELF_ATT_HID_DIM, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);
            int SelfAttOp = 1;
            A_Func SelfAttFunc = A_Func.Linear;

            LSTMStructure USelfLSTM1 = IsCreateModel ? modelStructure.AddLayer(
                                   new LSTMStructure(stateDim + stateDim, new int[] { stateDim / 2 }, device, BuilderParameters.RndInit)) :
                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device);
            LSTMStructure USelfLSTM2 = IsCreateModel ? modelStructure.AddLayer(
                                   new LSTMStructure(stateDim + stateDim, new int[] { stateDim / 2 }, device, BuilderParameters.RndInit)) :
                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device);


            LSTMStructure AnsLSTM1 = IsCreateModel ? modelStructure.AddLayer(
                                   new LSTMStructure(stateDim, new int[] { BuilderParameters.ANS_HID_DIM / 2 }, device, BuilderParameters.RndInit)) :
                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device);
            LSTMStructure AnsLSTM2 = IsCreateModel ? modelStructure.AddLayer(
                                   new LSTMStructure(stateDim, new int[] { BuilderParameters.ANS_HID_DIM / 2 }, device, BuilderParameters.RndInit)) :
                                   modelStructure.DeserializeModel<LSTMStructure>(modelReader, device);

            LayerStructure AnsLayer = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(BuilderParameters.ANS_HID_DIM, BuilderParameters.VERTEX_NUM, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

            LayerStructure TermLayer = IsCreateModel ? modelStructure.AddLayer(
                new LayerStructure(BuilderParameters.ANS_HID_DIM, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, device)) :
                modelStructure.DeserializeModel<LayerStructure>(modelReader, device);

            modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            if (modelReader != null) modelReader.Close();

            ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainAns,
                EmbedCNN, EmbedLSTM,
                new Tuple<LayerStructure, LayerStructure, LayerStructure, int, A_Func>(SrcAttM, TgtAttM, AttM, AttOp, AttFunc),
                new Tuple<LSTMStructure, LSTMStructure>(UMemLSTM1, UMemLSTM2),
                new Tuple<LayerStructure, LayerStructure, LayerStructure, int, A_Func>(SelfAttM1, SelfAttM2, SelfAttM, SelfAttOp, SelfAttFunc),
                new Tuple<LSTMStructure, LSTMStructure>(USelfLSTM1, USelfLSTM2),
                new Tuple<LSTMStructure, LSTMStructure>(AnsLSTM1, AnsLSTM2),
                AnsLayer, TermLayer, BuilderParameters.RECURRENT_STEP, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
            trainCG.SetDelegateModel(modelStructure);

            ComputationGraph validCG = null;
            if (BuilderParameters.IsValidFile)
                validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidAns,
                EmbedCNN, EmbedLSTM,
                new Tuple<LayerStructure, LayerStructure, LayerStructure, int, A_Func>(SrcAttM, TgtAttM, AttM, AttOp, AttFunc),
                new Tuple<LSTMStructure, LSTMStructure>(UMemLSTM1, UMemLSTM2),
                new Tuple<LayerStructure, LayerStructure, LayerStructure, int, A_Func>(SelfAttM1, SelfAttM2, SelfAttM, SelfAttOp, SelfAttFunc),
                new Tuple<LSTMStructure, LSTMStructure>(USelfLSTM1, USelfLSTM2),
                new Tuple<LSTMStructure, LSTMStructure>(AnsLSTM1, AnsLSTM2),
                AnsLayer, TermLayer, BuilderParameters.RECURRENT_STEP, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

            double bestValidScore = double.MinValue;
            int bestIter = -1;
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                double loss = trainCG.Execute();
                Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                if (DeepNet.BuilderParameters.ModelSavePerIteration)
                {
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                    {
                        modelStructure.Serialize(writer);
                    }
                }

                double validScore = 0;
                if (validCG != null)
                {
                    validScore = validCG.Execute();
                    Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, iter);
                }
                if (validScore > bestValidScore)
                {
                    bestValidScore = validScore; bestIter = iter;
                }
                Logger.WriteLog("Best Valid Score {0},  At iter {1}", bestValidScore, bestIter);
            }

            DataPanel.Deinit();
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainAns = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQuery = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidAns = null;

            
            public static int MaxQueryLen = 0;
            public static int MaxDocLen = 0;
            public static int MaxCrossQDLen = 0;
            public static int MaxCrossDDLen = 0;
            public static int MaxCrossQQLen = 0;
            

            static IEnumerable<Tuple<string, string, string>> ExtractQAS(string inputFile)
            {
                using (StreamReader inputReader = new StreamReader(inputFile))
                {
                    int lineIdx = 0;
                    while (!inputReader.EndOfStream)
                    {
                        string doc = inputReader.ReadLine();
                        string query = inputReader.ReadLine();
                        string ans = inputReader.ReadLine();
                        inputReader.ReadLine(); // read empty line.

                        string[] items = ans.Trim().Split(' ');

                        if (items.Length >= 2) { continue; }
                        yield return new Tuple<string, string, string>(doc, query, ans);
                        lineIdx += 1;
                    }
                    Console.WriteLine("Total Number of unique answers {0}", lineIdx);
                }
            }


            static void ExtractCorpusBinary(string inputFile, string paraFileBin, string queryFileBin, string ansFileBin, int miniBatchSize)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = BuilderParameters.FEATURE_DIM, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = BuilderParameters.FEATURE_DIM, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                DenseBatchData ans = new DenseBatchData(new DenseDataStat() { Dim = 1, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(paraFileBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryFileBin);
                BinaryWriter ansWriter = FileUtil.CreateBinaryWrite(ansFileBin);

                IEnumerable<Tuple<string, string, string>> QAS = ExtractQAS(inputFile);
                IEnumerable<Tuple<string, string, string>> Input = QAS;
                
                int crossQDLen = 0;
                int crossDDLen = 0;
                int crossQQLen = 0;
                int lineIdx = 0;
                int overSpanfilterNum = 0;
                int notFoundfilterNum = 0;
                int overLengthfilterNum = 0;
                foreach (Tuple<string, string, string> smp in Input)
                {
                    string d = smp.Item1;
                    string q = smp.Item2;
                    string a = smp.Item3;

                    int ans_id = int.Parse(a.Substring(0, a.Length - 1));

                    List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                    foreach (string term in d.Trim().Split(' '))
                    {
                        Dictionary<int, float> fea = new Dictionary<int, float>();
                        if (term.Length == 0) continue;

                        if (term.Equals("->")) { fea.Add(1, 1); }
                        else
                        {
                            int id = int.Parse(term.Substring(0, term.Length - 1));
                            char type = term.Substring(term.Length - 1)[0];
                            int typeId = (int)type - (int)'A';
                            fea.Add(2 + typeId, 1);
                            fea.Add(2 + BuilderParameters.VERTEX_TYPE + id, 1);
                        }
                        contextFea.Add(fea);
                    }

                    Dictionary<string, int> reIndex = new Dictionary<string, int>();
                    string[] qTerms = q.Trim().Split(' ');
                    List<string> newQTerms = new List<string>();

                    foreach (string t in qTerms)
                    {
                        if (t.Length == 0) continue;

                        if (t.Equals("->") || t.Equals("X")) { newQTerms.Add(t); continue; }
                        else
                        {
                            int id = int.Parse(t.Substring(0, t.Length - 1));
                            char type = t.Substring(t.Length - 1)[0];
                            int newIdx = id;
                            if (!reIndex.ContainsKey(t))
                            {
                                reIndex[t] = reIndex.Count;
                            }
                            newIdx = reIndex[t];
                            newQTerms.Add(newIdx.ToString() + type.ToString());
                        }
                    }

                    List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                    foreach (string term in newQTerms)
                    {
                        if (term.Length == 0) continue;

                        Dictionary<int, float> fea = new Dictionary<int, float>();
                        if (term.Equals("->")) { fea.Add(1, 1); }
                        else if (term.Equals("X")) { fea.Add(0, 1); }
                        else
                        {
                            int id = int.Parse(term.Substring(0, term.Length - 1));
                            char type = term.Substring(term.Length - 1)[0];
                            int typeId = (int)type - (int)'A';
                            fea.Add(2 + typeId, 1);
                            fea.Add(2 + BuilderParameters.VERTEX_TYPE + id, 1);
                        }
                        queryFea.Add(fea);
                    }

                    if (contextFea.Count > MaxDocLen) { MaxDocLen = contextFea.Count; }
                    if (queryFea.Count > MaxQueryLen) { MaxQueryLen = queryFea.Count; }

                    crossQDLen += contextFea.Count * queryFea.Count;
                    crossDDLen += contextFea.Count * contextFea.Count;
                    crossQQLen += queryFea.Count * queryFea.Count;

                    if (crossQDLen > MaxCrossQDLen) { MaxCrossQDLen = crossQDLen; }
                    if (crossDDLen > MaxCrossDDLen) { MaxCrossDDLen = crossDDLen; }
                    if (crossQQLen > MaxCrossQQLen) { MaxCrossQQLen = crossQQLen; }

                    context.PushSample(contextFea);
                    query.PushSample(queryFea);
                    ans.PushSample(new float[] { ans_id }, 1);


                    if (context.BatchSize >= miniBatchSize)
                    {
                        context.PopBatchToStat(contextWriter);
                        query.PopBatchToStat(queryWriter);
                        ans.PopBatchToStat(ansWriter);
                        crossQDLen = 0;
                        crossDDLen = 0;
                        crossQQLen = 0;
                    }

                    if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                }
                context.PopBatchCompleteStat(contextWriter);
                query.PopBatchCompleteStat(queryWriter);
                ans.PopBatchCompleteStat(ansWriter);

                Console.WriteLine("Context Stat {0}", context.Stat.ToString());
                Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                Console.WriteLine("Answer Stat {0}", ans.Stat.ToString());
                Console.WriteLine("Max Q Len {0}, Max D Len {1}, Max QD Cross Len {2}, Max DD Cross Len {3}, Max QQ Cross Len", MaxQueryLen, MaxDocLen, MaxCrossQDLen, MaxCrossDDLen, MaxCrossQQLen);
                Console.WriteLine("OverSpan Filter Number {0}, Not Found Filter Number {1}, Over Length Filter Number {2}", overSpanfilterNum, notFoundfilterNum, overLengthfilterNum);
            }

            public static void Init()
            {
                #region Preprocess Data.
                if (BuilderParameters.IsTrainFile)
                {
                    ExtractCorpusBinary(BuilderParameters.Train,
                            BuilderParameters.TrainContextBin, BuilderParameters.TrainQueryBin, BuilderParameters.TrainAnsBin, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsValidFile)
                {
                    ExtractCorpusBinary(
                        BuilderParameters.Valid,
                        BuilderParameters.ValidContextBin, BuilderParameters.ValidQueryBin, BuilderParameters.ValidAnsBin, BuilderParameters.MiniBatchSize);
                }
                #endregion.

                #region Load Data.
                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin);
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin);
                    TrainAns = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainAnsBin);

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainAns.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin);
                    ValidQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin);
                    ValidAns = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidAnsBin);

                    ValidContext.InitThreadSafePipelineCashier(64, false);
                    ValidQuery.InitThreadSafePipelineCashier(64, false);
                    ValidAns.InitThreadSafePipelineCashier(64, false);
                }
                #endregion.
            }

            public static void Deinit()
            {
                string paraFileBin = BuilderParameters.ValidContextBin;
                string queryFileBin = BuilderParameters.ValidQueryBin;
                string ansFileBin = BuilderParameters.ValidAnsBin;

                File.Delete(paraFileBin);
                File.Delete(queryFileBin);
                File.Delete(ansFileBin);
            }

        }
    }
}
