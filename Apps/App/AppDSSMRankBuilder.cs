﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppDSSMRankBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static string TrainStream { get { return Argument["TRAIN"].Value; } }
            public static string TestStream { get { return Argument["TEST"].Value; } }
            public static string WordVocab { get { return Argument["WORD-VOCAB"].Value; } }
            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int[] SRC_LAYER_DIM { get { return Argument["SRC-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] SRC_WINDOW { get { return Argument["SRC-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] TGT_LAYER_DIM { get { return Argument["TGT-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] TGT_WINDOW { get { return Argument["TGT-WINDOW"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value.Equals(string.Empty) ? LogFile + ".tmp.score" : Argument["SCORE-PATH"].Value; } }

            public static string TestEntityPath { get { return Argument["TEST-ENTITY-FILE"].Value; } }

            public static bool IsTrainFile { get { return !TrainStream.Equals(string.Empty); } }
            public static string TrainSrcBin { get { return TrainStream + ".src.bin"; } }
            public static string TrainTgtBin { get { return TrainStream + ".tgt.bin"; } }
            public static string TrainMatchBin { get { return TrainStream + ".match.bin"; } }

            public static bool IsTestFile { get { return !TestStream.Equals(string.Empty); } }
            public static string TestSrcBin { get { return TestStream + ".src.bin"; } }
            public static string TestTgtBin { get { return TestStream + ".tgt.bin"; } }
            public static string TestMatchBin { get { return TestStream + ".match.bin"; } }
            
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            
            static BuilderParameters()
            {
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
                Argument.Add("WORD-VOCAB", new ParameterArgument(string.Empty, "Word Vocab."));
                Argument.Add("MINI-BATCH", new ParameterArgument(string.Empty, "Mini Batch Size."));

                Argument.Add("SRC-LAYER-DIM", new ParameterArgument("300,300", "Source Layer Dim"));
                Argument.Add("SRC-WINDOW", new ParameterArgument("3,1", "Source DNN Window Size"));
                Argument.Add("TGT-LAYER-DIM", new ParameterArgument("300,300", "Target Layer Dim"));
                Argument.Add("TGT-WINDOW", new ParameterArgument("3,1", "Target DNN Window Size"));

                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
                Argument.Add("GPUID", new ParameterArgument("1", "GPU Device ID"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                
                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score Path"));
                Argument.Add("TEST-ENTITY-FILE", new ParameterArgument(string.Empty, "Test Entity Path"));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_DSSM_RANK; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public static ComputationGraph BuildComputationGraph(
                    IDataCashier<SeqSparseBatchData, SequenceDataStat> srcData,
                    IDataCashier<SeqSparseBatchData, SequenceDataStat> tgtData,
                    IDataCashier<BiMatchBatchData, BiMatchBatchDataStat> matchData,
                    RunnerBehavior rb, List<LayerStructure> srcNet, List<LayerStructure> tgtNet, LayerStructure matchNet, CompositeNNStructure deepNet)
        {
            ComputationGraph cg = new ComputationGraph();

            SeqSparseBatchData srcInput = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(srcData, rb));
            SeqSparseBatchData tgtInput = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(tgtData, rb));
            BiMatchBatchData matchInput = (BiMatchBatchData)cg.AddDataRunner(new DataRunner<BiMatchBatchData, BiMatchBatchDataStat>(matchData, rb));

            SeqDenseBatchData srcOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(srcNet[0], srcInput, rb));
            for (int i = 1; i < srcNet.Count; i++)
                srcOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(srcNet[i], srcOutput, rb));

            SeqDenseBatchData tgtOutput = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(tgtNet[0], tgtInput, rb));
            for (int i = 1; i < tgtNet.Count; i++)
                tgtOutput = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(tgtNet[i], tgtOutput, rb));

            // max pooling.
            HiddenBatchData srcMaxOut = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(srcOutput, rb));
            HiddenBatchData tgtMaxOut = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(tgtOutput, rb));

            HiddenBatchData srcMatch = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(matchNet, srcMaxOut, rb));

            /// similarity between source text and target text.
            HiddenBatchData simiOutput = (HiddenBatchData)cg.AddRunner(new SimilarityRunner(srcMatch, tgtMaxOut, matchInput, SimilarityType.CosineSimilarity, rb));

            switch (rb.RunMode)
            {
                case DNNRunMode.Train:
                    if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                        cg.AddRunner(new ModelDiskDumpRunner(deepNet, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\cdssm_rank"));
                    cg.AddObjective(new BayesianRatingRunner(simiOutput.Output.Data, simiOutput.Deriv.Data, matchInput.MatchInfo, matchInput, BuilderParameters.Gamma, rb));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new VectorDumpRunner(simiOutput.Output, BuilderParameters.ScoreOutputPath, true));
                    break;
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure deepModel = null;

            List<LayerStructure> srcLayers = new List<LayerStructure>();
            List<LayerStructure> tgtLayers = new List<LayerStructure>();
            LayerStructure matchLayer = null;

            if (BuilderParameters.SEED_MODEL != string.Empty)
            {
                deepModel = new CompositeNNStructure(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            }
            else
            {
                deepModel = new CompositeNNStructure();

                int inputDim = DataPanel.Hash.VocabSize;
                for (int i = 0; i < BuilderParameters.SRC_LAYER_DIM.Length; i++)
                {
                    deepModel.AddLayer(new LayerStructure(inputDim, BuilderParameters.SRC_LAYER_DIM[i], A_Func.Tanh, 
                        N_Type.Convolution_layer, BuilderParameters.SRC_WINDOW[i], 0, true, device));
                    inputDim = BuilderParameters.SRC_LAYER_DIM[i];
                }

                inputDim = DataPanel.Hash.VocabSize;
                for (int i = 0; i < BuilderParameters.TGT_LAYER_DIM.Length; i++)
                {
                    deepModel.AddLayer(new LayerStructure(inputDim, BuilderParameters.TGT_LAYER_DIM[i], A_Func.Tanh, 
                        N_Type.Convolution_layer, BuilderParameters.TGT_WINDOW[i], 0, true, device));
                    inputDim = BuilderParameters.TGT_LAYER_DIM[i];
                }
                deepModel.AddLayer(new LayerStructure(BuilderParameters.SRC_LAYER_DIM.Last(), BuilderParameters.TGT_LAYER_DIM.Last(), A_Func.Tanh,
                    N_Type.Fully_Connected, 1, 0, true, device));
            }

            for (int i = 0; i < BuilderParameters.SRC_LAYER_DIM.Length; i++) srcLayers.Add((LayerStructure)deepModel.CompositeLinks[i]);
            for (int i = 0; i < BuilderParameters.TGT_LAYER_DIM.Length; i++) tgtLayers.Add((LayerStructure)deepModel.CompositeLinks[i + BuilderParameters.SRC_LAYER_DIM.Length]);
            matchLayer = (LayerStructure)deepModel.CompositeLinks.Last();

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    Logger.WriteLog("Loading DSSM Structure.");
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin, DataPanel.TrainMatchBin, 
                        new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Train }, srcLayers, tgtLayers, matchLayer, deepModel);

                    ComputationGraph validCG = null;
                    if(BuilderParameters.IsTestFile)
                    {
                        validCG = BuildComputationGraph(DataPanel.TestSrcBin, DataPanel.TestTgtBin, DataPanel.TestMatchBin,
                            new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Predict }, srcLayers, tgtLayers, matchLayer, deepModel);
                    }

                    trainCG.InitOptimizer(deepModel, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Predict });

                    double bestValidScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        Logger.WriteLog("Train Learn Rate {0}", OptimizerParameters.LearnRate);
                        double loss = trainCG.Execute(); // trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Total MiniBatch {1}, Avg Loss {2}", iter, DataPanel.TrainSrcBin.Stat.TotalBatchNumber, loss);

                        if (validCG != null)
                        {
                            double validScore = validCG.Execute();

                            //*****Compute Accuracy****//
                            List<float> scores = File.ReadAllLines(BuilderParameters.ScoreOutputPath).Select(i => float.Parse(i)).ToList();
                            List<float[]> labels = File.ReadLines(BuilderParameters.TestEntityPath).
                                                    Select(i => i.Split('\t')[2].Split(new string[] { "###" }, StringSplitOptions.RemoveEmptyEntries).
                                                            Select(l => float.Parse(l)).ToArray()).ToList();
                            List<string[]> entities = File.ReadLines(BuilderParameters.TestEntityPath).
                                                    Select(i => i.Split('\t')[3].Split(new string[] { "###" }, StringSplitOptions.RemoveEmptyEntries)).ToList();

                            int scoreIdx = 0;
                            int HitNum = 0;
                            for (int i = 0; i < entities.Count; i++)
                            {
                                Dictionary<string, float> candScoreDict = new Dictionary<string, float>();
                                string trueCand = string.Empty;

                                for (int c = 0; c < entities[i].Length; c++)
                                {
                                    float candScore = scores[scoreIdx++];
                                    float candLabel = labels[i][c];
                                    if (candLabel > 0) { trueCand = entities[i][c]; }
                                    if (!candScoreDict.ContainsKey(entities[i][c])) { candScoreDict[entities[i][c]] = 0; }
                                    candScoreDict[entities[i][c]] += (float)Math.Exp(BuilderParameters.Gamma * candScore);
                                }

                                if (trueCand == string.Empty) { throw new Exception("Candidate Label Error 2 !!"); }
                                var predCandIdx = candScoreDict.FirstOrDefault(x => x.Value == candScoreDict.Values.Max()).Key;
                                float expSum = candScoreDict.Values.Sum();
                                foreach (string k in new List<string>(candScoreDict.Keys)) candScoreDict[k] = candScoreDict[k] / expSum;

                                //OutputWriter.WriteLine("{0}\t{1}\t{2}", trueCand, TextUtil.Dict2Str(candScoreDict), predCandIdx);
                                if (predCandIdx.Equals(trueCand)) HitNum++;
                            }
                            validScore = HitNum * 1.0 / entities.Count;
                            Logger.WriteLog("Prediction Score {0}", validScore);
                            if (validScore > bestValidScore) { bestValidScore = validScore; bestIter = iter; }
                            Logger.WriteLog("Best Valid Score {0}, At iter {1}", bestValidScore, bestIter);
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "best.model"), FileMode.Create, FileAccess.Write)))
                            {
                                deepModel.Serialize(writer);
                            }
                        }
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(string.Format(@"{0}\\{1}.model", BuilderParameters.ModelOutputPath, iter.ToString()), FileMode.Create, FileAccess.Write)))
                            {
                                deepModel.Serialize(writer);
                            }
                        }
                    }

                    break;
                case DNNRunMode.Predict:
                    ComputationGraph cg = BuildComputationGraph(DataPanel.TestSrcBin, DataPanel.TestTgtBin, DataPanel.TestMatchBin,
                        new RunnerBehavior() { Computelib = computeLib, Device = device, RunMode = DNNRunMode.Predict }, srcLayers, tgtLayers, matchLayer, deepModel);
                    double score = cg.Execute();
                    Logger.WriteLog("Valid Score {0}", score);

                    break;
            }
            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainTgtBin = null;
            public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> TrainMatchBin = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestSrcBin = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TestTgtBin = null;
            public static DataCashier<BiMatchBatchData, BiMatchBatchDataStat> TestMatchBin = null;

            static void ExtractVocab(string dataFile, BasicTextHash hash)
            {
                using (StreamReader mreader = new StreamReader(dataFile))
                {
                    while (!mreader.EndOfStream)
                    {
                        string[] items = mreader.ReadLine().Split('\t');
                        string src = items[0].Trim().ToLower();
                        string[] tgts = items[1].Trim().ToLower().Split(new string[] { "###" }, StringSplitOptions.RemoveEmptyEntries);
                        hash.VocabExtract(src);
                        foreach (string tgt in tgts) hash.VocabExtract(tgt);
                    }
                }
            }

            static void ExtractBinary(string dataFile, int miniBatch, string binSrc, string binTgt, string binMatch, BasicTextHash hash)
            {
                BinaryWriter srcDataWriter = new BinaryWriter(new FileStream(binSrc, FileMode.Create, FileAccess.Write));
                BinaryWriter tgtDataWriter = new BinaryWriter(new FileStream(binTgt, FileMode.Create, FileAccess.Write));
                BinaryWriter matchDataWriter = new BinaryWriter(new FileStream(binMatch, FileMode.Create, FileAccess.Write));

                SeqSparseBatchData srcData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = hash.VocabSize }, DeviceType.CPU);
                SeqSparseBatchData tgtData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = hash.VocabSize }, DeviceType.CPU);
                BiMatchBatchData matchData = new BiMatchBatchData(new BiMatchBatchDataStat(), DeviceType.CPU);

                List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
                using (StreamReader mreader = new StreamReader(dataFile))
                {
                    while (!mreader.EndOfStream)
                    {
                        string[] items = mreader.ReadLine().Split('\t');
                        string src = items[0].Trim().ToLower();
                        string[] tgts = items[1].Trim().ToLower().Split(new string[] { "###" }, StringSplitOptions.RemoveEmptyEntries);
                        string[] labels = items[2].Trim().ToLower().Split(new string[] { "###" }, StringSplitOptions.RemoveEmptyEntries);

                        List<Dictionary<int, float>> src_fea = hash.SeqFeatureExtract(src);
                        if (src_fea.Count == 0)
                        {
                            src_fea.Add(new Dictionary<int, float>());
                            src_fea[0].Add(0, 0);
                        }
                        srcData.PushSample(src_fea);

                        for (int t = 0; t < Math.Min(tgts.Length, labels.Length); t++)
                        {
                            List<Dictionary<int, float>> tgt_fea = hash.SeqFeatureExtract(tgts[t]);
                            if (tgt_fea.Count == 0)
                            {
                                tgt_fea.Add(new Dictionary<int, float>());
                                tgt_fea[0].Add(0, 0);
                            }
                            tgtData.PushSample(tgt_fea);

                            float label = float.Parse(labels[t]);
                            matchList.Add(new Tuple<int, int, float>(srcData.BatchSize - 1, tgtData.BatchSize - 1, label));
                            
                        }

                        if (srcData.BatchSize >= miniBatch)
                        {
                            srcData.PopBatchToStat(srcDataWriter);
                            tgtData.PopBatchToStat(tgtDataWriter);

                            matchData.SetMatch(matchList);
                            matchData.PopBatchToStat(matchDataWriter);
                            matchList.Clear();
                        }
                    }
                }
                srcData.PopBatchCompleteStat(srcDataWriter);
                tgtData.PopBatchCompleteStat(tgtDataWriter);

                if(matchList.Count > 0) matchData.SetMatch(matchList);
                matchData.PopBatchCompleteStat(matchDataWriter);

                Console.WriteLine("Source Data Stat {0}", srcData.Stat.ToString());
                Console.WriteLine("Target Data Stat {0}", tgtData.Stat.ToString());
                Console.WriteLine("Match Data Stat {0}", matchData.Stat.ToString());
            }

            public static BasicTextHash Hash;
            public static void Init()
            {
                if (!File.Exists(BuilderParameters.WordVocab))
                {
                    Hash = new WordTextHash(new SimpleTextTokenizer(new string[] { " " }, new string[] { "." }));
                    ExtractVocab(BuilderParameters.TrainStream, Hash);
                    ExtractVocab(BuilderParameters.TestStream, Hash);
                    using (StreamWriter vocabwriter = new StreamWriter(BuilderParameters.WordVocab))
                    {
                        Hash.Serialize(vocabwriter);
                    }
                }
                else
                {
                    Hash = new WordTextHash(BuilderParameters.WordVocab, new SimpleTextTokenizer(new string[] { " " }, new string[] { "." }));
                }

                #region Extract Binary Feature from raw text data.
                if (BuilderParameters.IsTrainFile)
                {
                    if (!File.Exists(BuilderParameters.TrainSrcBin) || !File.Exists(BuilderParameters.TrainTgtBin) || !File.Exists(BuilderParameters.TrainMatchBin))
                    {
                        ExtractBinary(BuilderParameters.TrainStream, BuilderParameters.MiniBatchSize, BuilderParameters.TrainSrcBin, BuilderParameters.TrainTgtBin, BuilderParameters.TrainMatchBin, Hash);
                    }
                    TrainSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainSrcBin);
                    TrainTgtBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainTgtBin);
                    TrainMatchBin = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>(BuilderParameters.TrainMatchBin);

                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainMatchBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }
                if(BuilderParameters.IsTestFile)
                {
                    if (!File.Exists(BuilderParameters.TestSrcBin) || !File.Exists(BuilderParameters.TestTgtBin) || !File.Exists(BuilderParameters.TestMatchBin))
                    {
                        ExtractBinary(BuilderParameters.TestStream, BuilderParameters.MiniBatchSize, BuilderParameters.TestSrcBin, BuilderParameters.TestTgtBin, BuilderParameters.TestMatchBin, Hash);
                    }
                    TestSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestSrcBin);
                    TestTgtBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TestTgtBin);
                    TestMatchBin = new DataCashier<BiMatchBatchData, BiMatchBatchDataStat>(BuilderParameters.TestMatchBin);

                    TestSrcBin.InitThreadSafePipelineCashier(100, false);
                    TestTgtBin.InitThreadSafePipelineCashier(100, false);
                    TestMatchBin.InitThreadSafePipelineCashier(100, false);
                }
                #endregion
            }
        }
    }
}
