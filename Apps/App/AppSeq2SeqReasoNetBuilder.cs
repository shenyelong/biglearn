﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// Sequence 2 Sequence LSTM Model.
    /// </summary>
    public class AppSeq2SeqReasoNetBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "SRC Train Data."));
                Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "TGT Train Data."));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));

                Argument.Add("DEV-SRC", new ParameterArgument(string.Empty, "SRC Dev Dataset."));
                Argument.Add("DEV-TGT", new ParameterArgument(string.Empty, "TGT Dev Dataset."));

                Argument.Add("SRC-VOCAB-DIM", new ParameterArgument(string.Empty, "Source Vocab Dimension."));
                Argument.Add("TGT-VOCAB-DIM", new ParameterArgument(string.Empty, "Target Vocab Dimension."));

                Argument.Add("TRAIN-MINI-BATCH", new ParameterArgument("64", "Mini Batch for Train."));
                Argument.Add("DEV-MINI-BATCH", new ParameterArgument("32", "Mini Batch for Dev."));

                Argument.Add("MAX-TRAIN-SRC-LENGTH", new ParameterArgument("50", "MAX src length."));
                Argument.Add("MAX-TRAIN-TGT-LENGTH", new ParameterArgument("52", "MAX tgt length."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                
                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
                Argument.Add("LAYER-ATTENTION", new ParameterArgument("1", "Layer Attention"));

                Argument.Add("SRC-EMBED", new ParameterArgument("620", "Src String Dim"));
                Argument.Add("TGT-EMBED", new ParameterArgument("500", "Tgt String Dim"));
                Argument.Add("ATT-HIDDEN", new ParameterArgument("1000", "Attention Layer Hidden Dim"));

                Argument.Add("CONTEXT-LEN", new ParameterArgument("0", "Context Vector length"));
                Argument.Add("DECODE-DIM", new ParameterArgument("200", "Decoding Dimension"));
                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));

                Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("10", "Reasoning Steps."));
                Argument.Add("ENSEMBLE-RL", new ParameterArgument("0", "0:max terminate prob; 1:average terminate prob."));
                Argument.Add("RECURRENT-T", new ParameterArgument("10,10,1", "Recurrent Terminate Net."));

                Argument.Add("GLOBAL-ATT-GAMMA", new ParameterArgument("10", "global attention gamma"));
                Argument.Add("GLOBAL-ATT-HIDDEN", new ParameterArgument("128", "global attention hidden size."));
                Argument.Add("GLOBAL-MEMORY-DIM", new ParameterArgument("256", "global memory dimension"));
                Argument.Add("GLOBAL-MEMORY-SIZE", new ParameterArgument("512", "global memory size"));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            //(DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainSrcData { get { return Argument["TRAIN-SRC"].Value; } }
            public static string TrainTgtData { get { return Argument["TRAIN-TGT"].Value; } }
            public static bool IsTrainFile { get { return (!TrainSrcData.Equals(string.Empty)) && (!TrainTgtData.Equals(string.Empty)); } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

            public static string DevSrcData { get { return Argument["DEV-SRC"].Value; } }
            public static string DevTgtData { get { return Argument["DEV-TGT"].Value; } }
            public static bool IsDevFile { get { return (!DevSrcData.Equals(string.Empty)) && (!DevTgtData.Equals(string.Empty)); } }
            
            public static int Src_Vocab_Dim { get { return int.Parse(Argument["SRC-VOCAB-DIM"].Value); } }
            public static int Tgt_Vocab_Dim { get { return int.Parse(Argument["TGT-VOCAB-DIM"].Value); } }

            public static int Train_Mini_Batch { get { return int.Parse(Argument["TRAIN-MINI-BATCH"].Value); } }
            public static int Dev_Mini_Batch { get { return int.Parse(Argument["DEV-MINI-BATCH"].Value); } }

            public static int MAX_TRAIN_SRC_LENGTH { get { return int.Parse(Argument["MAX-TRAIN-SRC-LENGTH"].Value); } }
            public static int MAX_TRAIN_TGT_LENGTH { get { return int.Parse(Argument["MAX-TRAIN-TGT-LENGTH"].Value); } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }


            
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static bool[] LAYER_ATTENTION { get { return Argument["LAYER-ATTENTION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }

            public static int Src_Embed { get { return int.Parse(Argument["SRC-EMBED"].Value); } }
            public static int Tgt_Embed { get { return int.Parse(Argument["TGT-EMBED"].Value); } }

            public static int Decode_Embed { get { return int.Parse(Argument["DECODE-DIM"].Value); } }
            public static int Context_Len { get { return int.Parse(Argument["CONTEXT-LEN"].Value); } }

            public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            //public static Vocab2Freq mVocabDict = null;
            //public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Tgt_Vocab); return mVocabDict; } }
            //public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            //public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
            public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }


            public static int Recurrent_Step { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static PredType Ensemble_RL { get { return (PredType)int.Parse(Argument["ENSEMBLE-RL"].Value); } }
            public static int[] Recurrent_Terminate { get { return Argument["RECURRENT-T"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static float Global_Att_Gamma { get { return float.Parse(Argument["GLOBAL-ATT-GAMMA"].Value); } }
            public static int Global_Att_Hidden { get { return int.Parse(Argument["GLOBAL-ATT-HIDDEN"].Value); } }
            public static int Global_Mem_Dim { get { return int.Parse(Argument["GLOBAL-MEMORY-DIM"].Value); } }
            public static int Global_Mem_Size { get { return int.Parse(Argument["GLOBAL-MEMORY-SIZE"].Value); } }
            

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_SEQ2SEQ; } }
        public enum PredType { RL_MAXITER, RL_AVGPROB }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> AddLSTMEncoder(ComputationGraph cg, LSTMStructure lstmModel,
            SeqDenseRecursiveData input, RunnerBehavior behavior)
        {
            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> memory = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
            SeqDenseRecursiveData SrclstmOutput = input;
            /***************** LSTM Encoder ******************************/
            for (int i = 0; i < lstmModel.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], SrclstmOutput, behavior);
                SrclstmOutput = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);
                memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(encoderRunner.Output, encoderRunner.C));
            }
            return memory;
        }

        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            HiddenBatchData InitStatus { get; set; }

            MatrixStructure GMemory { get; set; }
            BiMatchBatchData GMemMatch { get; set; }
            MLPAttentionStructure GAttStruct { get; set; }

            GRUCell GruCell = null;
            List<LayerStructure> TermStruct = null;

            public HiddenBatchData[] StateData = null;
            public HiddenBatchData[] TermData = null;

            public HiddenBatchData[] AnswerProb = null;
            public HiddenBatchData FinalState = null;

            int[] termDistribution = null;
            int sampleSize = 0;
            int batchNum = 0;
            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }
            public ReasonNetRunner(ComputationGraph cg,
                HiddenBatchData initStatus, int maxIterateNum,
                MatrixStructure gMem, BiMatchBatchData gMemMatch, MLPAttentionStructure gAttStruct, float gGamma,
                //suppose we don't have the query available. 
                //SeqDenseBatchData qMem, BiMatchBatchData qMatchData, MLPAttentionStructure qAttStruct, float qGamma,

                GRUCell gruCell, List<LayerStructure> termStruct, RunnerBehavior behavior) : base(cg, Structure.Empty, null, behavior)
            {
                InitStatus = initStatus;

                GMemory = gMem;
                GMemMatch = gMemMatch;
                GAttStruct = gAttStruct;

                TermStruct = termStruct;
                GruCell = gruCell;

                MaxIterationNum = maxIterateNum;

                //AttGamma = attgamma;
                //AnsGamma = ansgamma;
                //DebugFile = debugFile;

                //IntArgument batchSizeArgument = new IntArgument("BatchSize");
                //cg.AddRunner(new BatchSizeAssignmentRunner(Memory, batchSizeArgument, behavior));

                //IntArgument sentSizeArgument = new IntArgument("SentSize");
                //cg.AddRunner(new SentSizeAssignmentRunner(Memory, sentSizeArgument, behavior));

                // Memory Matrix;
                MatrixData MemoryMatrix = new MatrixData(gMem);

                // InitState is between -1 to 1;
                //VectorData initWeight = new VectorData(InitState.Neural_In, InitState.weight, InitState.WeightGrad, InitState.DeviceType);
                //cg.AddRunner(new ActivationRunner(new MatrixData(initWeight), A_Func.Tanh, Behavior));
                //// Generate BatchInitVec;
                //MatrixData initState = (MatrixData)cg.AddRunner(new VectorExpansionRunner(initWeight, MemoryMatrix.MaxSegment, batchSizeArgument, Behavior));
                MatrixData initState = new MatrixData(InitStatus);

                // Memory Address Project.
                MatrixData MemoryAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.MemoryDim, gAttStruct.Wm, gAttStruct.WmGrad, gAttStruct.DeviceType);

                // Memory Address.
                MatrixData MemoryAddress = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(MemoryMatrix, MemoryAddressProject, Behavior));

                // State Address Project.
                MatrixData StateAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.InputDim, gAttStruct.Wi, gAttStruct.WiGrad, gAttStruct.DeviceType);

                VectorData AddressScoreProject = new VectorData(gAttStruct.HiddenDim, gAttStruct.Wa, gAttStruct.WaGrad, gAttStruct.DeviceType);
                // For Forward/Backward Propagation.
                {
                    StateData = new HiddenBatchData[MaxIterationNum];
                    TermData = new HiddenBatchData[MaxIterationNum];
                }
                //

                for (int i = 0; i < MaxIterationNum; i++)
                {
                    MatrixData stateAddress = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(initState, StateAddressProject, Behavior));

                    // Attention Input.
                    MatrixData attScore = new MatrixData((HiddenBatchData)cg.AddRunner(new VecAlignmentRunner(stateAddress, MemoryAddress, gMemMatch, AddressScoreProject, Behavior, 1, A_Func.Tanh)));
                    MatrixData attMatrixScore = new MatrixData(gMem.Size, InitStatus.MAX_BATCHSIZE, attScore.Output, attScore.Deriv, Behavior.Device);
                    // Attention Softmax.
                    cg.AddRunner(new MatrixSoftmaxRunner(attMatrixScore, true, Behavior));

                    MatrixData inputMatrix = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(attMatrixScore, MemoryMatrix, Behavior));

                    // RNN State.
                    MatrixData newState = new MatrixData((HiddenBatchData)cg.AddRunner(new GRUQueryRunner(GruCell, new HiddenBatchData(initState), new HiddenBatchData(inputMatrix), Behavior)));

                    StateData[i] = new HiddenBatchData(newState);

                    HiddenBatchData terminateOutput = StateData[i];
                    for (int l = 0; l < TermStruct.Count; l++)
                    {
                        terminateOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct[l], terminateOutput, Behavior));
                    }
                    TermData[i] = terminateOutput;

                    // State Switch.
                    initState = newState;
                }

                {
                    AnswerProb = new HiddenBatchData[MaxIterationNum];
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                    }

                    FinalState = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, gruCell.HiddenStateDim, Behavior.RunMode, Behavior.Device);
                }
            }

            public override void Init()
            {
                sampleSize = 0;
                batchNum = 0;
                termDistribution = new int[MaxIterationNum];
            }

            public override void Complete()
            {
                List<string> termD = new List<string>();
                Logger.WriteLog("Max Termination Step Distribution:");
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    termD.Add(string.Format("{0}:{1}:{2}", i, termDistribution[i], termDistribution[i] * 1.0f / sampleSize));
                }
                Logger.WriteLog(string.Join("\n", termD));
            }

            public override void Forward()
            {
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    ComputeLib.Logistic(TermData[i].Output.Data, 0, TermData[i].Output.Data, 0, TermData[i].Output.BatchSize, 1);
                    ComputeLib.ClipVector(TermData[i].Output.Data, TermData[i].BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                    TermData[i].Output.Data.SyncToCPU(TermData[i].BatchSize);
                    StateData[i].Output.Data.SyncToCPU(StateData[i].BatchSize * StateData[i].Dim);
                }

                FinalState.BatchSize = StateData.Last().BatchSize;
                Array.Clear(FinalState.Output.Data.MemPtr, 0, FinalState.BatchSize * FinalState.Dim);

                for (int b = 0; b < FinalState.BatchSize; b++)
                {
                    int max_iter = 0;
                    double max_p = 0;
                    float acc_log_t = 0;
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        float t_i = TermData[i].Output.Data.MemPtr[b];
                        if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                        float p = acc_log_t + (float)Math.Log(t_i);
                        if (i == MaxIterationNum - 1) p = acc_log_t;

                        float prob = (float)Math.Exp(p);
                        AnswerProb[i].Output.Data.MemPtr[b] = prob;
                        acc_log_t += (float)Math.Log(1 - t_i);

                        if (prob > max_p)
                        {
                            max_p = prob;
                            max_iter = i;
                        }
                    }

                    if (BuilderParameters.Ensemble_RL == PredType.RL_MAXITER)
                    {
                        for (int d = 0; d < FinalState.Dim; d++)
                        {
                            FinalState.Output.Data.MemPtr[b * FinalState.Dim + d] = StateData[max_iter].Output.Data.MemPtr[d];
                        }
                    }
                    termDistribution[max_iter] += 1;
                }

                for (int i = 0; i < MaxIterationNum; i++)
                {
                    AnswerProb[i].BatchSize = FinalState.BatchSize;
                    AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
                }
                FinalState.Output.Data.SyncFromCPU(FinalState.BatchSize * FinalState.Dim);

                sampleSize += FinalState.BatchSize;
                batchNum += 1;
                if(batchNum % 100 == 0)
                {
                    List<string> termD = new List<string>();
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        termD.Add(string.Format("{0}:{1}:{2}", i, termDistribution[i], termDistribution[i] * 1.0f / sampleSize));
                    }
                    Console.WriteLine(string.Join("\n", termD));
                }
            }
            public override void CleanDeriv()
            {
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    ComputeLib.Zero(AnswerProb[i].Deriv.Data, AnswerProb[i].BatchSize);
                }
            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    Array.Clear(TermData[i].Deriv.Data.MemPtr, 0, TermData[i].BatchSize);
                }


                for (int b = 0; b < GMemMatch.SrcSize; b++)
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                        TermData[i].Deriv.Data.MemPtr[b] += reward * (1 - TermData[i].Output.Data.MemPtr[b]);

                        for (int hp = 0; hp < i; hp++)
                        {
                            TermData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(0.95f, i - hp) * reward * TermData[hp].Output.Data.MemPtr[b];
                        }

                        if (TermData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
                        {
                            Logger.WriteLog("Start TerminalRunner Deriv is too large {0}, step {1}", TermData[i].Deriv.Data.MemPtr[b], i);
                        }
                    }
                }

                for (int b = 0; b < GMemMatch.SrcSize; b++)
                {
                    TermData[MaxIterationNum - 1].Deriv.Data.MemPtr[b] = 0;
                }

                for (int i = 0; i < MaxIterationNum; i++)
                {
                    TermData[i].Deriv.Data.SyncFromCPU(TermData[i].BatchSize);
                }
            }

            public override void Update()
            { }
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             EmbedStructure embedSrc, EmbedStructure embedTgt,
                                                             LayerStructure transO, LayerStructure transC,
                                                             LSTMStructure d1Encoder, LSTMStructure d2Encoder, LSTMCell decoder,

                                                             MatrixStructure globalMemory, MLPAttentionStructure globalAttStruct,
                                                             GRUCell gruStruct, List<LayerStructure> termStruct,
                                                             MLPAttentionStructure stateAttention, 
                                                             MLPAttentionStructure srcAttention, 
                                                             EmbedStructure decodeEmbedTgt,
                                                             RunnerBehavior Behavior, CompositeNNStructure Model, EvaluationType evalType)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseBatchData SrcData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(srcData, Behavior));
            //SeqHelpData SrcHelp = (SeqHelpData)cg.AddRunner(new SeqDataHelpRunner(SrcData.SequenceData, Behavior));

            SeqDenseRecursiveData SrcD1Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData, false, Behavior));
            SeqDenseRecursiveData SrcD2Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData, true, Behavior));

            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD1Memory = AddLSTMEncoder(cg, d1Encoder, SrcD1Embed, Behavior);
            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD2Memory = AddLSTMEncoder(cg, d2Encoder, SrcD2Embed, Behavior);

            /**************** Bidirection Source LSTM Encoder Ensemble *********/
            List<SeqDenseBatchData> SrcEnsembleMemory = new List<SeqDenseBatchData>();
            List<Tuple<HiddenBatchData, HiddenBatchData>> SrcEnsembleStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
            for (int i = 0; i < SrcD1Memory.Count; i++)
            {
                HiddenBatchData SrcD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item1, true, 0, SrcD1Memory[i].Item1.MapForward, Behavior));
                HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item1, false, 0, SrcD2Memory[i].Item1.MapForward, Behavior));

                HiddenBatchData SrcD1C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item2, true, 0, SrcD1Memory[i].Item2.MapForward, Behavior));
                HiddenBatchData SrcD2C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item2, false, 0, SrcD2Memory[i].Item2.MapForward, Behavior));

                HiddenBatchData SrcO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1O, SrcD2O }, Behavior));
                HiddenBatchData SrcC = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1C, SrcD2C }, Behavior));

                HiddenBatchData newSrcO = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transO, SrcO, Behavior));
                HiddenBatchData newSrcC = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transC, SrcC, Behavior));
                SrcEnsembleStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(newSrcO, newSrcC));
                //SrcEnsembleStatus.Add(new Tuple<BigLearn.HiddenBatchData, BigLearn.HiddenBatchData>(SrcO, SrcC));

                SeqDenseBatchData SrcSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item1, Behavior));
                SeqDenseBatchData SrcSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item1, Behavior));

                //SeqDenseBatchData SrcSeqD1C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item2, Behavior));
                //SeqDenseBatchData SrcSeqD2C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item2, Behavior));

                SeqDenseBatchData SrcSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1O, SrcSeqD2O }, Behavior));
                //SeqDenseBatchData SrcSeqC = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1C, SrcSeqD2C }, Behavior));

                SrcEnsembleMemory.Add(SrcSeqO);
            }

            HiddenBatchData SrcStatus_O = SrcEnsembleStatus.Last().Item1;
            HiddenBatchData SrcStatus_C = SrcEnsembleStatus.Last().Item2;
            SeqDenseBatchData SrcMemory = SrcEnsembleMemory.Last();

            /// Reasoning process.
            IntArgument batchSize = new IntArgument("size");
            cg.AddRunner(new HiddenDataBatchSizeRunner(SrcStatus_O, batchSize, Behavior));
            BiMatchBatchData GlobalMemMatch = (BiMatchBatchData)cg.AddRunner(new RepeatBiMatchRunner(SrcStatus_O.MAX_BATCHSIZE, globalMemory.Size, batchSize, Behavior));

            ReasonNetRunner ReasonRunner = new ReasonNetRunner(cg, SrcStatus_O, BuilderParameters.Recurrent_Step,
                                                               globalMemory, GlobalMemMatch, globalAttStruct, BuilderParameters.Global_Att_Gamma,
                                                               // QuerySeqMem, QueryMemMatch, QueryAttStruct,
                                                               gruStruct, termStruct, Behavior);
            cg.AddRunner(ReasonRunner);


            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));

            /**************** Target LSTM Embedding *********/
            SeqDenseRecursiveData TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedTgt, TgtData.SequenceData, false, Behavior));


            if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
                cg.AddRunner(new ModelDiskDumpRunner(Model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\Seq2Seq"));

            if (Behavior.RunMode == DNNRunMode.Train || evalType == EvaluationType.PERPLEXITY)
            {
                MatrixData SrcAttProject = new MatrixData(srcAttention.HiddenDim, srcAttention.MemoryDim, srcAttention.Wm, srcAttention.WmGrad, Behavior.Device);
                MatrixData SrcAttData = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(new MatrixData(SrcMemory), SrcAttProject, Behavior));
                SeqDenseBatchData SrcAttSeqData = new SeqDenseBatchData(new SequenceDataStat() { MAX_BATCHSIZE = SrcMemory.MAX_BATCHSIZE, MAX_SEQUENCESIZE = SrcMemory.MAX_SENTSIZE, FEATURE_DIM = SrcAttData.Column },
                     SrcMemory.SampleIdx, SrcMemory.SentMargin, SrcAttData.Output, SrcAttData.Deriv, Behavior.Device);

                MatrixData StateAttProject = new MatrixData(stateAttention.HiddenDim, stateAttention.MemoryDim, stateAttention.Wm, stateAttention.WmGrad, Behavior.Device);
                HiddenBatchData[] StateAttData = new HiddenBatchData[BuilderParameters.Recurrent_Step];
                for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
                {
                    StateAttData[i] = new HiddenBatchData((MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(new MatrixData(ReasonRunner.StateData[i]), StateAttProject, Behavior)));
                }

                MLPAttentionV5Runner srcAttentionRunner = new MLPAttentionV5Runner(srcAttention, SrcMemory, SrcAttSeqData,
                    BuilderParameters.TgtSeqMaxLen, TgtlstmOutput.Stat.MAX_BATCHSIZE, TgtlstmOutput.Stat.MAX_SEQUENCESIZE, Behavior);
                srcAttentionRunner.IsDelegate = false;
                cg.AddRunner(srcAttentionRunner);

                
                List<SeqDenseRecursiveData> TgtDecodeOutputs = new List<SeqDenseRecursiveData>();

                for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
                {
                    SeqDenseBatchData reasonMem = (SeqDenseBatchData)cg.AddRunner(new EnsembleSeqMatrixRunner(ReasonRunner.StateData.Take(i + 1).ToList(), Behavior));
                    SeqDenseBatchData reasonAttMem = (SeqDenseBatchData)cg.AddRunner(new EnsembleSeqMatrixRunner(StateAttData.Take(i + 1).ToList(), Behavior));

                    MLPAttentionV5Runner reasonAttRunner = new MLPAttentionV5Runner(stateAttention, reasonMem, reasonAttMem, 
                        BuilderParameters.TgtSeqMaxLen, TgtlstmOutput.Stat.MAX_BATCHSIZE, TgtlstmOutput.Stat.MAX_SEQUENCESIZE, Behavior);
                    reasonAttRunner.IsDelegate = false;
                    cg.AddRunner(reasonAttRunner);

                    EnsembleAttentionRunner ensembleAttRunner = new EnsembleAttentionRunner(new List<BasicMLPAttentionRunner>() { srcAttentionRunner, reasonAttRunner },
                            BuilderParameters.TgtSeqMaxLen, TgtlstmOutput.Stat.MAX_BATCHSIZE, TgtlstmOutput.Stat.MAX_SEQUENCESIZE, reasonMem.Dim + SrcMemory.Dim, Behavior);

                    SeqDenseRecursiveData TgtlstmDecode = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(decoder,
                            TgtlstmOutput, SrcStatus_O, SrcStatus_C, ensembleAttRunner, Behavior));

                    TgtDecodeOutputs.Add(TgtlstmDecode);
                }

                //if (Behavior.RunMode == DNNRunMode.Train)
                //    cg.AddObjective(new EmbedFullySoftmaxRunner(decodeEmbedTgt, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
                //else
                //    cg.AddRunner(new EmbedFullySoftmaxRunner(decodeEmbedTgt, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath, Behavior));

                cg.AddObjective(new EmbedSoftmaxContrastiveRewardRunner(decodeEmbedTgt, TgtDecodeOutputs.ToArray(),
                    TgtlstmOutput.MapForward, TgtlstmOutput.MapBackward, ReasonRunner.AnswerProb, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior, true));
            }
            else
            {
                //List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
                //for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                //    decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                //        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i], TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);

                //// the probability of exactly generate the true tgt.
                //LSTMEmbedDecodingRunner decodeRunner = new LSTMEmbedDecodingRunner(decoder, embedTgt, decodeEmbedTgt, SrcEnsembleStatus,
                //    decodeAttentionRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), Behavior, true);
                //EnsembleBeamSearchRunner beamRunner = new EnsembleBeamSearchRunner(0, 0,
                //    BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, 
                //    decodeEmbedTgt.VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);
                //beamRunner.AddBeamSearch(decodeRunner);
                //cg.AddRunner(beamRunner);

                //switch (evalType)
                //{
                //    case EvaluationType.ACCURACY:
                //        cg.AddRunner(new BeamAccuracyRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                //        break;
                //    case EvaluationType.BSBLEU:
                //        cg.AddRunner(new BLEUDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                //        break;
                //    case EvaluationType.ROUGE:
                //        cg.AddRunner(new ROUGEDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                //        break;
                //    case EvaluationType.TOPKBEAM:
                //        cg.AddRunner(new TopKBeamRunner(beamRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                //        break;
                //}
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            int aDirectLSTM = 1;
            int bDirectLSTM = 1;
            int ensemble = aDirectLSTM + bDirectLSTM;


            CompositeNNStructure modelStructure = new CompositeNNStructure();

            LSTMStructure d1LSTM = null;
            LSTMStructure d2LSTM = null;
            LSTMCell decodeLSTM = null;

            LayerStructure transO = null;
            LayerStructure transC = null;

            EmbedStructure embedSrc = null;
            EmbedStructure embedTgt = null;
            EmbedStructure embedDecodeTgt = null;
            MLPAttentionStructure srcAttStruct = null;
            MLPAttentionStructure stateAttStruct = null;

            MatrixStructure globalMemory = null;
            MLPAttentionStructure globalAttStruct = null;
            GRUCell gruStruct = null;
            List<LayerStructure> termStruct = new List<LayerStructure>();
            
            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int srcVocabSize = BuilderParameters.Src_Vocab_Dim;
                int tgtVocabSize = BuilderParameters.Tgt_Vocab_Dim;
                int decodeDim = BuilderParameters.LAYER_DIM.Last();
                int srcAttMemDim = ensemble * decodeDim;
                int globalAttMemDim = decodeDim;

                d1LSTM = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM, device);
                d2LSTM = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM, device);

                decodeLSTM = new LSTMCell(BuilderParameters.Tgt_Embed, decodeDim, srcAttMemDim + globalAttMemDim, device, RndRecurrentInit.RndNorm);
                
                transO = new LayerStructure(decodeDim * ensemble,
                        decodeDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);

                transC = new LayerStructure(decodeDim * ensemble,
                        decodeDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);

                embedSrc = new EmbedStructure(srcVocabSize, BuilderParameters.Src_Embed, device);
                embedTgt = new EmbedStructure(tgtVocabSize, BuilderParameters.Tgt_Embed, device);
                embedDecodeTgt = new EmbedStructure(tgtVocabSize, decodeDim, device);

                //ensemble * 
                //for (int i = 0; i < BuilderParameters.LAYER_DIM.Length; i++)
                //    srcAttStruct.Add(new MLPAttentionStructure(BuilderParameters.LAYER_DIM[i], ensemble * BuilderParameters.LAYER_DIM[i], 
                //                BuilderParameters.Attention_Hidden, device));

                srcAttStruct = new MLPAttentionStructure(decodeDim, ensemble * decodeDim, BuilderParameters.Attention_Hidden, device);
                stateAttStruct = new MLPAttentionStructure(decodeDim, decodeDim, BuilderParameters.Attention_Hidden, device);

                modelStructure.AddLayer(d1LSTM);
                modelStructure.AddLayer(d2LSTM);
                modelStructure.AddLayer(decodeLSTM);

                modelStructure.AddLayer(transO);
                modelStructure.AddLayer(transC);

                modelStructure.AddLayer(embedSrc);
                modelStructure.AddLayer(embedTgt);
                modelStructure.AddLayer(embedDecodeTgt);
                modelStructure.AddLayer(srcAttStruct);
                modelStructure.AddLayer(stateAttStruct);

                globalMemory = new MatrixStructure(BuilderParameters.Global_Mem_Dim, BuilderParameters.Global_Mem_Size, device);
                globalAttStruct = new MLPAttentionStructure(decodeDim, BuilderParameters.Global_Mem_Dim, BuilderParameters.Global_Att_Hidden, device);
                gruStruct = new GRUCell(BuilderParameters.Global_Mem_Dim, decodeDim, device, RndRecurrentInit.RndNorm);
                int tInput = decodeDim;
                for (int i = 0; i < BuilderParameters.Recurrent_Terminate.Length; i++)
                {
                    termStruct.Add(new LayerStructure(tInput, BuilderParameters.Recurrent_Terminate[i], 
                        i == BuilderParameters.Recurrent_Terminate.Length - 1 ? A_Func.Linear : A_Func.Tanh, 
                        N_Type.Fully_Connected, 1, 0, false, device));
                    tInput = BuilderParameters.Recurrent_Terminate[i];
                }

                modelStructure.AddLayer(globalMemory);
                modelStructure.AddLayer(globalAttStruct);
                modelStructure.AddLayer(gruStruct);
                for (int i = 0; i < termStruct.Count; i++)
                    modelStructure.AddLayer(termStruct[i]);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, device);
                    int link = 0;
                    d1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    d2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    decodeLSTM = (LSTMCell)modelStructure.CompositeLinks[link++];

                    transO = (LayerStructure)modelStructure.CompositeLinks[link++];
                    transC = (LayerStructure)modelStructure.CompositeLinks[link++];

                    embedSrc = (EmbedStructure)modelStructure.CompositeLinks[link++];
                    embedTgt = (EmbedStructure)modelStructure.CompositeLinks[link++];
                    embedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[link++];
                    srcAttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    stateAttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];

                    globalMemory = (MatrixStructure)modelStructure.CompositeLinks[link++];
                    globalAttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                    gruStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                    for (int i = 0; i < BuilderParameters.Recurrent_Terminate.Length; i++)
                        termStruct.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                }
            }


            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin, 
                        embedSrc, embedTgt, transO, transC, d1LSTM, d2LSTM, decodeLSTM,
                        globalMemory, globalAttStruct, gruStruct, termStruct, 
                        stateAttStruct, srcAttStruct, embedDecodeTgt,
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, 
                        modelStructure, EvaluationType.PERPLEXITY);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                            embedSrc, embedTgt, transO, transC, d1LSTM, d2LSTM, decodeLSTM,
                            globalMemory, globalAttStruct, gruStruct, termStruct,
                            stateAttStruct, srcAttStruct, embedDecodeTgt,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, 
                            modelStructure, BuilderParameters.Evaluation);


                    ComputationGraph devCG = null;
                    if (BuilderParameters.IsDevFile)
                        devCG = BuildComputationGraph(DataPanel.DevSrcBin, DataPanel.DevTgtBin, 
                            embedSrc, embedTgt, transO, transC, d1LSTM, d2LSTM, decodeLSTM,
                            globalMemory, globalAttStruct, gruStruct, termStruct,
                            stateAttStruct, srcAttStruct, embedDecodeTgt,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, 
                            modelStructure, EvaluationType.PERPLEXITY);

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    double bestDevScore = double.MaxValue;
                    double bestDevIter = -1;
                    double bestDevValidScore = 0;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        string modelName = string.Format(@"{0}\\Seq2Seq.{1}.model", BuilderParameters.ModelOutputPath, iter);

                        using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                        {
                            modelStructure.Serialize(writer);
                        }

                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }

                        if (BuilderParameters.IsDevFile)
                        {
                            double devScore = devCG.Execute();
                            if (devScore < bestDevScore)
                            {
                                bestDevScore = devScore;
                                bestDevIter = iter;
                                bestDevValidScore = validScore;
                            }
                            Logger.WriteLog("Best Dev Score {0} at iteration {1}  Valid Score {2}", bestDevScore, bestDevIter, bestDevValidScore);
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                            embedSrc, embedTgt, transO, transC, d1LSTM, d2LSTM, decodeLSTM,
                            globalMemory, globalAttStruct, gruStruct, termStruct,
                            stateAttStruct, srcAttStruct, embedDecodeTgt,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, 
                            modelStructure, BuilderParameters.Evaluation);
                    double predScore = predCG.Execute();
                    Logger.WriteLog("Prediction Score {0}", predScore);
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;
            
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;
            
            /// <summary>
            ///  Report PPL instead of BLEU Score.
            /// </summary>
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> DevSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevTgtBin = null;
            
            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    if (!File.Exists(BuilderParameters.TrainSrcData + ".bin") || !File.Exists(BuilderParameters.TrainTgtData + ".bin"))
                    {
                        List<Tuple<List<int>, List<int>>> data = CommonExtractor.LoadPairSequence(
                                new StreamReader(BuilderParameters.TrainSrcData),
                                new StreamReader(BuilderParameters.TrainTgtData));

                        IEnumerable<Tuple<List<int>, List<int>>> shuffleData = CommonExtractor.RandomShuffle(data, 100000, ParameterSetting.RANDOM_SEED);

                        if(BuilderParameters.MAX_TRAIN_SRC_LENGTH > 0)
                        {
                            shuffleData = shuffleData.Where(i => i.Item1.Count <= BuilderParameters.MAX_TRAIN_SRC_LENGTH);
                        }
                        if (BuilderParameters.MAX_TRAIN_TGT_LENGTH > 0)
                        {
                            shuffleData = shuffleData.Where(i => i.Item2.Count <= BuilderParameters.MAX_TRAIN_TGT_LENGTH);
                        }

                        IEnumerable<List<Dictionary<int, float>>> srcSeqData = shuffleData.Select(i => Util.Enumable2SeqDict<int>(i.Item1));
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.TrainSrcData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(srcSeqData, BuilderParameters.Src_Vocab_Dim, BuilderParameters.Train_Mini_Batch, writer);
                        }

                        IEnumerable<List<int>> tgtSeqData = shuffleData.Select(i => i.Item2);
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.TrainTgtData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(tgtSeqData, BuilderParameters.Tgt_Vocab_Dim, BuilderParameters.Train_Mini_Batch, writer);
                        }
                    }
                }

                if (BuilderParameters.IsValidFile)
                {
                    if (!File.Exists(BuilderParameters.ValidSrcData + ".bin") || !File.Exists(BuilderParameters.ValidTgtData + ".bin"))
                    {
                        List<Tuple<List<int>, List<int>>> data = CommonExtractor.LoadPairSequence(
                                new StreamReader(BuilderParameters.ValidSrcData),
                                new StreamReader(BuilderParameters.ValidTgtData));

                        IEnumerable<List<Dictionary<int, float>>> srcSeqData = data.Select(i => Util.Enumable2SeqDict<int>(i.Item1));
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ValidSrcData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(srcSeqData, BuilderParameters.Src_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                        }

                        IEnumerable<List<int>> tgtSeqData = data.Select(i => i.Item2);
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ValidTgtData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(tgtSeqData, BuilderParameters.Tgt_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                        }
                    }
                }

                if (BuilderParameters.IsDevFile)
                {
                    if (!File.Exists(BuilderParameters.DevSrcData + ".bin") || !File.Exists(BuilderParameters.DevTgtData + ".bin"))
                    {
                        List<Tuple<List<int>, List<int>>> data = CommonExtractor.LoadPairSequence(
                                new StreamReader(BuilderParameters.DevSrcData),
                                new StreamReader(BuilderParameters.DevTgtData));

                        IEnumerable<List<Dictionary<int, float>>> srcSeqData = data.Select(i => Util.Enumable2SeqDict<int>(i.Item1));
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.DevSrcData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(srcSeqData, BuilderParameters.Src_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                        }

                        IEnumerable<List<int>> tgtSeqData = data.Select(i => i.Item2);
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.DevTgtData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(tgtSeqData, BuilderParameters.Tgt_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                        }
                    }
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidSrcData + ".bin");
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData + ".bin");
                
                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.IsDevFile)
                {
                    DevSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.DevSrcData + ".bin");
                    DevTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevTgtData + ".bin");
                
                    DevSrcBin.InitThreadSafePipelineCashier(100, false);
                    DevTgtBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainSrcData + ".bin");
                    TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainTgtData + ".bin");
                    
                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

            }
        }
    }
}