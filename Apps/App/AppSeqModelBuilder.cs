﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// Sequence Model.
    /// </summary>
    public class AppSeqModelBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));

                Argument.Add("VOCAB-DIM", new ParameterArgument(string.Empty, "Source Vocab Dimension."));

                Argument.Add("VOCAB-EMD", new ParameterArgument(string.Empty, "vocab embeddings."));
                Argument.Add("VOCAB-UPDATE", new ParameterArgument("1", "UPDATE vocab embeddings."));

                Argument.Add("MINI-BATCH", new ParameterArgument("64", "Mini Batch Size"));
                Argument.Add("ENCODE-EMBED", new ParameterArgument("64", "Encode Embed Dim"));

                ///Language Word Error Rate.
                //Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));

                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("IS-SHARE-EMD", new ParameterArgument("1", "is share embedding vec."));

            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainData { get { return Argument["TRAIN"].Value; } }

            public static string ValidData { get { return Argument["VALID"].Value; } }

            public static int Vocab_Dim { get { return int.Parse(Argument["VOCAB-DIM"].Value); } }
            public static string Vocab_Emd { get { return (Argument["VOCAB-EMD"].Value); } }
            public static bool Vocab_Update { get { return int.Parse(Argument["VOCAB-UPDATE"].Value) > 0; } }

            public static int Mini_Batch { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int Encode_Embed { get { return int.Parse(Argument["ENCODE-EMBED"].Value); } }
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            
            //public static Vocab2Freq mVocabDict = null;
            //public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Tgt_Vocab); return mVocabDict; } }
            //public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            //public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
         
            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
            public static bool Is_Share_Emd { get { return int.Parse(Argument["IS-SHARE-EMD"].Value) > 0; } }
        }

        public override BuilderType Type { get { return BuilderType.APP_SEQ_MODEL; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        /// <summary>
        /// Sample Data. 
        /// </summary>
        class DataSampleRunner : StructRunner
        {
            List<List<int>> Data { get; set; }
            int MaxBatchSize { get; set; }
            int VocabSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);

            /// <summary>
            /// Sequence Sparse Data.
            /// </summary>
            public new SeqSparseBatchData Output { get; set; }
            public CudaPieceFloat NextWord { get; set; }
            public DataSampleRunner(List<List<int>> data, int maxBatchSize, int vocabSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                MaxBatchSize = maxBatchSize;
                VocabSize = vocabSize;
                Shuffle = new DataRandomShuffling(Data.Count, random);

                Output = new SeqSparseBatchData(new SequenceDataStat()
                {
                    FEATURE_DIM = VocabSize,
                    MAX_ELEMENTSIZE = maxBatchSize * DataPanel.Max_Seq_Len,
                    MAX_SEQUENCESIZE = maxBatchSize * DataPanel.Max_Seq_Len,
                    MAX_BATCHSIZE = maxBatchSize
                }, DeviceType.GPU);

                NextWord = new CudaPieceFloat(maxBatchSize * DataPanel.Max_Seq_Len, behavior.Device);

            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                List<List<Dictionary<int, float>>> dicts = new List<List<Dictionary<int, float>>>();

                int nextIdx = 0;
                int groupIdx = 0;
                Output.Clear();
                while (groupIdx < MaxBatchSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }

                    List<Dictionary<int, float>> wdict = new List<Dictionary<int, float>>();
                    //List<int> label = new List<int>();
                    for (int w = 0; w < Data[idx].Count - 1; w++) // widx in Data[idx])
                    {
                        int widx = Data[idx][w];
                        int nidx = Data[idx][w + 1];
                        Dictionary<int, float> d = new Dictionary<int, float>();
                        d.Add(widx, 1);
                        wdict.Add(d);

                        NextWord.MemPtr[nextIdx] = nidx;
                        nextIdx += 1;
                    }
                    dicts.Add(wdict);
                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
                Output.PushSamples(dicts);

                NextWord.EffectiveSize = nextIdx;
                NextWord.SyncFromCPU();
            }
        }


        public static ComputationGraph BuildComputationGraph(List<List<int>> seqData, int batchSize, int vocabSize,
                                                             LMLSTMModel Model,
                                                             RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            DataSampleRunner dataRunner = new DataSampleRunner(seqData, batchSize, vocabSize, Behavior);
            cg.AddDataRunner(dataRunner);
            SeqSparseBatchData seq = dataRunner.Output;
            CudaPieceFloat next = dataRunner.NextWord;

            //SeqDenseBatchData l3gQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
            //       L3GEmbedCNN, Queryl3gData, Behavior));

            SeqDenseRecursiveData seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(Model.EmbedIn, seq, false, Behavior) { IsUpdate = BuilderParameters.Vocab_Update });

            List<SeqDenseBatchData> seqContext = new List<SeqDenseBatchData>();
            for (int i = 0; i < Model.SeqEncoder.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(Model.SeqEncoder.LSTMCells[i], seqEmbed, Behavior);
                seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);

                Cook_TransposeSeq2SeqRunner transRunner = new Cook_TransposeSeq2SeqRunner(encoderRunner.Output, Behavior);
                cg.AddRunner(transRunner);

                seqContext.Add(transRunner.Output);
            }

            FullyConnectHiddenRunner<HiddenBatchData> decodeRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Decoder, new HiddenBatchData(new MatrixData(seqContext.Last())), Behavior);
            cg.AddRunner(decodeRunner);

            switch(Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    //SeqDenseBatchData seqContextEmbed = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(seqContext.Last().Item1, Behavior));
                    //seqContext.Last().Item1.MapBackward
                    //seqContext.Last().Item1.MapBackward
                    cg.AddObjective(new EmbedFullySoftmaxRunner(Model.EmbedOut, decodeRunner.Output, next, 1, Behavior, true, BuilderParameters.Vocab_Update)); //
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new EmbedFullySoftmaxRunner(Model.EmbedOut, decodeRunner.Output, next, 1, Behavior, false, false)); //
                    break;
            }
            cg.SetDelegateModel(Model);
            return cg;
        }

        public class GraphQueryData : BatchData
        {
            public List<StatusData> StatusPath = new List<StatusData>();
            public List<Tuple<int, int, float>> Results = new List<Tuple<int, int, float>>();

            public int MaxBatchSize { get { return StatusPath[0].MaxBatchSize; } }
            public int BatchSize { get { return StatusPath[0].BatchSize; } }

            /// <summary>
            ///  start with initial stateEmbed.
            /// </summary>
            /// <param name="stateEmbed"></param>
            /// <param name="device"></param>
            public GraphQueryData(List<Tuple<HiddenBatchData, HiddenBatchData>> stateEmbed, DeviceType device)
            {
                StatusData init_status = new StatusData(this, null, stateEmbed, device);
                StatusPath.Add(init_status);
            }

            public void AddNewState(List<int> nodeIdx, List<Tuple<HiddenBatchData, HiddenBatchData>> stateEmbed, List<float> logProb, List<int> preStateIdx, DeviceType device)
            {
                StatusData next_status = new StatusData(this, nodeIdx, stateEmbed, device);
                StatusPath.Add(next_status);
                next_status.LogProb = logProb;
                next_status.PreStatusIndex = preStateIdx;
            }

            public List<int> GetBatchIdxs(int b, int step)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < StatusPath[step].BatchSize; i++)
                {
                    if (StatusPath[step].GetOriginalStatsIndex(i) == b)
                        r.Add(i);
                }
                return r;
            }
        }

        public class StatusData : BatchData
        {
            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public List<Tuple<HiddenBatchData, HiddenBatchData>> StateEmbed { get; set; }
            
            public int MaxBatchSize { get { return StateEmbed.Last().Item1.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.Last().Item1.BatchSize; } }

            /// <summary>
            /// Node ID.
            /// </summary>
            public List<int> NodeID { get; set; }

            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            public List<float> LogProb { get; set; }

            
            
            /// <summary>
            /// Raw Query.
            /// </summary>
            public GraphQueryData GraphQuery { get; set; }

            public HiddenBatchData CandidateProb = null;


            /// <summary>
            /// logprobability of the node.
            /// </summary>
            /// <param name="batchIdx"></param>
            /// <returns></returns>
            public float GetLogProb(int batchIdx)
            {
                if (Step == 0) { return 0; }
                else { return LogProb[batchIdx]; }
            }


            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            //public List<Tuple<int, int>> MatchCandidate = null;
            //public SeqVectorData MatchCandidateProb = null;

            public List<int> PreStatusIndex { get; set; }
            public int GetPreStatusIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else return PreStatusIndex[batchIdx];
            }
            public int GetOriginalStatsIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else
                {
                    int b = GetPreStatusIndex(batchIdx);
                    return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
                }
            }

            public List<int> GetHistoryNodeIdxs(int batchIdx)
            {
                List<int> results = new List<int>();
                if (Step == 0) { results.Add(0); }
                else
                {
                    int b = GetPreStatusIndex(batchIdx);
                    List<int> h = GraphQuery.StatusPath[Step - 1].GetHistoryNodeIdxs(b);
                    results.AddRange(h);
                    results.Add(NodeID[batchIdx]);
                }
                return results;
            }
            
            
            public int Step;

            public StatusData(GraphQueryData interData, List<int> nodeIndex, List<Tuple<HiddenBatchData, HiddenBatchData>> stateEmbed, DeviceType device)
            {
                GraphQuery = interData;
                NodeID = nodeIndex;
                StateEmbed = stateEmbed;
                Step = GraphQuery.StatusPath.Count;
            }
        }


        class BeamSearchRunner : StructRunner
        {
            int BeamSize = 1;
            new StatusData Input { get; set; }

            public List<int> NodeID = null;
            public List<float> NodeProb = null;
            public BiMatchBatchData Match = null;
            public List<int> PreStatusIdx = null;

            CudaPieceFloat tmpWordIndex { get; set; }
            CudaPieceFloat tmpWordProb { get; set; }

            bool IsLastStep { get; set; }
            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchRunner(StatusData data, int beamSize, bool isLastStep, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                BeamSize = beamSize;
                Input = data;
                IsLastStep = isLastStep;

                tmpWordIndex = new CudaPieceFloat(data.MaxBatchSize * beamSize, Behavior.Device);
                tmpWordProb = new CudaPieceFloat(data.MaxBatchSize * beamSize, Behavior.Device);

                NodeID = new List<int>();
                NodeProb = new List<float>();
                PreStatusIdx = new List<int>();
                Match = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);
            }

            public override void Forward()
            {

                NodeID.Clear();
                NodeProb.Clear();
                PreStatusIdx.Clear();
                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();


                if (IsLastStep)
                {
                    Input.CandidateProb.Output.Data.SyncToCPU();
                    for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                    {
                        List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);
                        foreach (int b in idxs)
                        {
                            float pP = Input.GetLogProb(b);
                            float wp = (float)Math.Log(Input.CandidateProb.Output.Data.MemPtr[b * Input.CandidateProb.Dim]);

                            Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, b, wp + pP));
                        }
                    }
                }
                else
                {
                    Cudalib.KLargestValueBatch(Input.CandidateProb.Output.Data.CudaPtr, Input.BatchSize, Input.CandidateProb.Dim, BeamSize, 1, tmpWordProb.CudaPtr, tmpWordIndex.CudaPtr);

                    tmpWordProb.SyncToCPU();
                    tmpWordIndex.SyncToCPU();
                    // for each beam.
                    for (int i = 0; i < Input.GraphQuery.BatchSize; i++) //.MatchCandidateProb.Segment; i++)
                    {
                        List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);

                        MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

                        foreach (int b in idxs)
                        {
                            //int e = Input.CandidateProb.MemPtr[b];
                            //int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                            float pP = Input.GetLogProb(b);
                            for (int t = 0; t < BeamSize; t++)
                            {
                                int widx = (int)tmpWordIndex.MemPtr[b * BeamSize + t];
                                float wp = (float)Math.Log(tmpWordProb.MemPtr[b * BeamSize + t]);

                                topKheap.push_pair(new Tuple<int, int>(b, widx), wp + pP);
                            }
                        }

                        while (!topKheap.IsEmpty)
                        {
                            KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();

                            int widx = p.Key.Item2;
                            if (widx == 0)
                            {
                                // put in results list.
                                Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, p.Key.Item1, p.Value));
                            }
                            else
                            {
                                match.Add(new Tuple<int, int, float>(p.Key.Item1, NodeID.Count, p.Value));

                                NodeID.Add(widx);
                                NodeProb.Add(p.Value);
                                PreStatusIdx.Add(p.Key.Item1);
                            }
                        }
                    }
                }
                Match.SetMatch(match);
            }
        }

        /// <summary>
        /// prediction computation graph with beam search.
        /// </summary>
        /// <param name="seqData"></param>
        /// <param name="batchSize"></param>
        /// <param name="vocabSize"></param>
        /// <param name="Model"></param>
        /// <param name="Behavior"></param>
        /// <returns></returns>
        public static ComputationGraph BuildPredComputationGraph(List<List<int>> seqData, int batchSize, int vocabSize,
                                                             LMLSTMModel Model,
                                                             RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            DataSampleRunner dataRunner = new DataSampleRunner(seqData, batchSize, vocabSize, Behavior);
            cg.AddDataRunner(dataRunner);
            SeqSparseBatchData seq = dataRunner.Output;
            CudaPieceFloat next = dataRunner.NextWord;

            SeqDenseRecursiveData seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(Model.EmbedIn, seq, false, Behavior));

            //List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> seqContext = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
            List<Tuple<HiddenBatchData, HiddenBatchData>> stateContext = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

            for (int i = 0; i < Model.SeqEncoder.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(Model.SeqEncoder.LSTMCells[i], seqEmbed, Behavior);
                seqEmbed = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);

                HiddenBatchData seqS0 = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(encoderRunner.Output, true, 0, encoderRunner.Output.MapForward, Behavior));
                HiddenBatchData seqC0 = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(encoderRunner.C, true, 0, encoderRunner.C.MapForward, Behavior));

                stateContext.Add(new Tuple<HiddenBatchData, HiddenBatchData>(seqS0, seqC0));
            }

            
            // generation.

            MatrixData decodeMatrix = new MatrixData(Model.EmbedOut);
            MatrixData encodeMatrix = new MatrixData(Model.EmbedIn);


            GraphQueryData newQuery = new GraphQueryData(stateContext, Behavior.Device);
            
            for (int i = 0; i < BuilderParameters.BeamSearchDepth; i++)
            {

                FullyConnectHiddenRunner<HiddenBatchData> decodeRunner = new FullyConnectHiddenRunner<HiddenBatchData>(Model.Decoder, newQuery.StatusPath[i].StateEmbed.Last().Item1, Behavior);
                cg.AddRunner(decodeRunner);

                MatrixProductRunner runner = new MatrixProductRunner(new MatrixData(decodeRunner.Output), decodeMatrix, Behavior) { name = "Depth" + i.ToString() };
                cg.AddRunner(runner);

                MatrixSoftmaxRunner softmaxProcessor = new MatrixSoftmaxRunner(runner.Output, true, Behavior);
                cg.AddRunner(softmaxProcessor);

                newQuery.StatusPath[i].CandidateProb = new HiddenBatchData(runner.Output);

                // take action.
                BeamSearchRunner beamRunner = new BeamSearchRunner(newQuery.StatusPath[i], BuilderParameters.BeamSearchCandidate, i== BuilderParameters.BeamSearchDepth - 1, Behavior);
                cg.AddRunner(beamRunner);

                ConvertSparseMatrixRunner wRunner = new ConvertSparseMatrixRunner(beamRunner.NodeID, BuilderParameters.BeamSearchCandidate * batchSize, BuilderParameters.Vocab_Dim, Behavior);
                cg.AddRunner(wRunner);
                SparseMultiplicationRunner wemdRunner = new SparseMultiplicationRunner(wRunner.Output, encodeMatrix, Behavior);
                cg.AddRunner(wemdRunner);

                HiddenBatchData xInput = new HiddenBatchData(wemdRunner.Output);

                List<Tuple<HiddenBatchData, HiddenBatchData>> newStateContext = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

                for (int l = 0; l < Model.SeqEncoder.LSTMCells.Count; l++)
                {
                    MatrixExpansionRunner sExpRunner = new MatrixExpansionRunner(newQuery.StatusPath[i].StateEmbed[l].Item1, beamRunner.Match, 1, Behavior);
                    cg.AddRunner(sExpRunner);
                    MatrixExpansionRunner cExpRunner = new MatrixExpansionRunner(newQuery.StatusPath[i].StateEmbed[l].Item2, beamRunner.Match, 1, Behavior);
                    cg.AddRunner(cExpRunner);

                    Tuple<HiddenBatchData, HiddenBatchData> state = new Tuple<HiddenBatchData, HiddenBatchData>(sExpRunner.Output, cExpRunner.Output);

                    LSTMStateRunner stateRunner = new LSTMStateRunner(Model.SeqEncoder.LSTMCells[l], state.Item1, state.Item2, xInput, Behavior);
                    cg.AddRunner(stateRunner);

                    newStateContext.Add(new Tuple<HiddenBatchData, HiddenBatchData>(stateRunner.O, stateRunner.C));

                    xInput = stateRunner.O;
                }

                newQuery.AddNewState(beamRunner.NodeID, newStateContext, beamRunner.NodeProb, beamRunner.PreStatusIdx, Behavior.Device);
            }
            cg.AddRunner(new DumpResultRunner(newQuery, BuilderParameters.ScoreOutputPath, Behavior));
            cg.SetDelegateModel(Model);
            return cg;
        }



        public class LMLSTMModel : CompositeNNStructure
        {
            public EmbedStructure EmbedIn { get; set; } // = new EmbedStructure(BuilderParameters.Vocab_Dim, BuilderParameters.Encode_Embed, device);
            public EmbedStructure EmbedOut { get; set; }
            public LSTMStructure SeqEncoder { get; set; } // = new LSTMStructure(BuilderParameters.Encode_Embed, BuilderParameters.LAYER_DIM, device);
            public LayerStructure Decoder { get; set; }

            public LMLSTMModel(int vocab, int enDim, int[] lstmDims, DeviceType device)
            {
                EmbedIn = AddLayer(new EmbedStructure(vocab, enDim, device));
                SeqEncoder = AddLayer(new LSTMStructure(enDim, lstmDims, device));
                Decoder = AddLayer(new LayerStructure(lstmDims.Last(), enDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true));
                
                if(BuilderParameters.Is_Share_Emd)
                {
                    EmbedOut = EmbedIn;
                }
                else
                {
                    EmbedOut = AddLayer(new EmbedStructure(vocab, enDim, device));
                }
                if(!BuilderParameters.Vocab_Emd.Equals(string.Empty))
                {
                    int wordDim = enDim;

                    Logger.WriteLog("Loading Glove Word Embedding ...");
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab_Emd, Encoding.UTF8))
                    {
                        while (!mreader.EndOfStream)
                        {
                            string[] items = mreader.ReadLine().Split('\t');
                            int wordIdx = int.Parse(items[0]);
                            string[] vecStr = items[1].Split(' ');
                            float[] vec = vecStr.Select(i => float.Parse(i)).ToArray();

                            if (vec.Length != wordDim) { throw new Exception(string.Format("word {0} embedding {1} does not match with the dim {2}", wordIdx, vec.Length, wordDim)); }
                            Array.Copy(vec, 0, EmbedIn.Embedding.MemPtr, wordIdx * wordDim, wordDim);
                            if(!BuilderParameters.Is_Share_Emd)
                            {
                                Array.Copy(vec, 0, EmbedOut.Embedding.MemPtr, wordIdx * wordDim, wordDim);
                            }
                        }
                    }
                    EmbedIn.Embedding.SyncFromCPU();
                    EmbedOut.Embedding.SyncFromCPU();

                    Logger.WriteLog("Init Glove Word Embedding Done.");
                }
            }

            public LMLSTMModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);
                EmbedIn = (EmbedStructure)DeserializeNextModel(reader, device);
                SeqEncoder = (LSTMStructure)DeserializeNextModel(reader, device);
                Decoder = (LayerStructure)DeserializeNextModel(reader, device);
                if(modelNum > 3)
                {
                    EmbedOut = (EmbedStructure)DeserializeNextModel(reader, device);
                }
                else
                {
                    EmbedOut = EmbedIn;
                }
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                if (!BuilderParameters.Vocab_Update)
                {
                    EmbedIn.EmbeddingOptimizer = new SGDOptimizer(EmbedIn.Embedding, 0, behavior);
                    if(!BuilderParameters.Is_Share_Emd)
                    {
                        EmbedOut.EmbeddingOptimizer = new SGDOptimizer(EmbedOut.Embedding, 0, behavior);
                    }
                }
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        public class DumpResultRunner : ObjectiveRunner
        {
            GraphQueryData Data { get; set; }
            StreamWriter Writer { get; set; }
            string Path { get; set; }
            public DumpResultRunner(GraphQueryData data, string path, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = data;
                Path = path;
            }

            public override void Init()
            {
                Writer = new StreamWriter(Path);
            }

            public override void Complete()
            {
                Writer.Close();
            }

            public override void Forward()
            {
                Dictionary<int, Tuple<string, float>> Results = new Dictionary<int, Tuple<string, float>>();
                for (int i = 0; i < Data.Results.Count; i++)
                {
                    int t = Data.Results[i].Item1;
                    int b = Data.Results[i].Item2;
                    float p = Data.Results[i].Item3;

                    int origialB = Data.StatusPath[t].GetOriginalStatsIndex(b);
                    List<int> r = Data.StatusPath[t].GetHistoryNodeIdxs(b);

                    if (!Results.ContainsKey(origialB))
                    {
                        Results.Add(origialB, new Tuple<string, float>(string.Join(" ", r), p));
                    }
                    else if (p > Results[origialB].Item2)
                    {
                        Results[origialB] = new Tuple<string, float>(string.Join(" ", r), p);
                    }
                }


                for (int i = 0; i < Data.BatchSize; i++)
                {
                    Writer.WriteLine(Results[i].Item1 + "\t" + Results[i].Item2.ToString());
                }

                Data.Results.Clear();
            }
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");

            //DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            //IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            DeviceBehavior behavior = new DeviceBehavior(BuilderParameters.GPUID);
            
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            LMLSTMModel model =
                BuilderParameters.SeedModel.Equals(string.Empty) ?
                new LMLSTMModel(BuilderParameters.Vocab_Dim, BuilderParameters.Encode_Embed, BuilderParameters.LAYER_DIM, behavior.Device) :
                new LMLSTMModel(new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read)), behavior.Device);
            
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    model.InitOptimization(behavior.TrainMode);

                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainCorpus, BuilderParameters.Mini_Batch, BuilderParameters.Vocab_Dim, model, behavior.TrainMode);

                    ComputationGraph validCG = BuildComputationGraph(DataPanel.ValidCorpus, BuilderParameters.Mini_Batch, BuilderParameters.Vocab_Dim, model, behavior.PredictMode);

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MaxValue;
                    double bestValidIter = -1;

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        validScore =  validCG.Execute();
                        if (validScore < bestValidScore)
                        {
                            bestValidScore = validScore;
                            bestValidIter = iter;
                        }
                        Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);

                        if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            //Logger.WriteLog("Evaluation at Iteration {0}", iter);
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            {
                                model.Serialize(writer);
                            }
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildPredComputationGraph(DataPanel.ValidCorpus, BuilderParameters.Mini_Batch, BuilderParameters.Vocab_Dim, model, behavior.PredictMode); 
                    double predScore = predCG.Execute();
                    break;
            }
            Logger.CloseLog();

        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static List<List<int>> TrainCorpus = null;
            public static List<List<int>> ValidCorpus = null;

            public static int Max_Seq_Len { get; set; }

            static List<List<int>> LoadCorpus(string fileName)
            {
                List<List<int>> Data = new List<List<int>>();
                using (StreamReader reader = new StreamReader(fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        List<int> sInts = items.Select(i => int.Parse(i)).ToList();
                        Data.Add(sInts);
                    }
                }
                return Data;
            }

            public static void Init()
            {
                TrainCorpus = LoadCorpus(BuilderParameters.TrainData);
                ValidCorpus = LoadCorpus(BuilderParameters.ValidData);

                int Max_train_Seq_Len = TrainCorpus.Select(i => i.Count).Max();
                Logger.WriteLog("Max Seq Len for training Corpus {0}", Max_train_Seq_Len);

                int Max_valid_Seq_Len = ValidCorpus.Select(i => i.Count).Max();
                Logger.WriteLog("Max Seq Len for Valid Corpus {0}", Max_valid_Seq_Len);

                Max_Seq_Len = Math.Max(Max_train_Seq_Len, Max_valid_Seq_Len);
            }
        }
    }
}