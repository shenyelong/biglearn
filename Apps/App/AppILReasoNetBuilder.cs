﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using BigLearn;namespace BigLearn.DeepNet
{
    /// <summary>
    /// Sequence 2 Sequence LSTM Model.
    /// </summary>
    public class AppILReasoNetBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

                Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "SRC Train Data."));
                Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "TGT Train Data."));

                Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
                Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));

                Argument.Add("DEV-SRC", new ParameterArgument(string.Empty, "SRC Dev Dataset."));
                Argument.Add("DEV-TGT", new ParameterArgument(string.Empty, "TGT Dev Dataset."));

                Argument.Add("SRC-VOCAB-DIM", new ParameterArgument(string.Empty, "Source Vocab Dimension."));
                Argument.Add("TGT-VOCAB-DIM", new ParameterArgument(string.Empty, "Target Vocab Dimension."));

                Argument.Add("TRAIN-MINI-BATCH", new ParameterArgument("64", "Mini Batch for Train."));
                Argument.Add("DEV-MINI-BATCH", new ParameterArgument("32", "Mini Batch for Dev."));

                Argument.Add("MAX-TRAIN-SRC-LENGTH", new ParameterArgument("50", "MAX src length."));
                Argument.Add("MAX-TRAIN-TGT-LENGTH", new ParameterArgument("52", "MAX tgt length."));

                ///Language Word Error Rate.
                Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

                
                Argument.Add("SRC-EMBED", new ParameterArgument("620", "Src String Dim"));
                Argument.Add("SRC-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));

                Argument.Add("TGT-EMBED", new ParameterArgument("620", "Tgt String Dim"));
                Argument.Add("TGT-LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));


                Argument.Add("IS-SHARE-EMBED", new ParameterArgument("0", "0 : nonshared input and output; 1 : shared input and output;"));
                Argument.Add("TGT-DECODE-EMBED", new ParameterArgument("620", "Tgt String  Decode Dim"));

                Argument.Add("ATT-HIDDEN", new ParameterArgument("1000", "Attention Layer Hidden Dim"));


                Argument.Add("CONTEXT-LEN", new ParameterArgument("0", "Context Vector length"));
                Argument.Add("DECODE-DIM", new ParameterArgument("200", "Decoding Dimension"));
                ///Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
                Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));

                Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("10", "Reasoning Steps."));
                Argument.Add("ENSEMBLE-RL", new ParameterArgument("0", "0:max terminate prob; 1:average terminate prob."));
                Argument.Add("RECURRENT-T", new ParameterArgument("10,10,1", "Recurrent Terminate Net."));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));

                Argument.Add("GLOBAL-ATT-GAMMA", new ParameterArgument("10", "global attention gamma"));
                Argument.Add("GLOBAL-ATT-TYPE", new ParameterArgument("1", "0:inner product; 1: weighted inner product;"));
                Argument.Add("GLOBAL-ATT-HIDDEN", new ParameterArgument("128", "global attention hidden size."));
                Argument.Add("GLOBAL-MEMORY-DIM", new ParameterArgument("256", "global memory dimension"));
                Argument.Add("GLOBAL-MEMORY-SIZE", new ParameterArgument("512", "global memory size"));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
            }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            //(DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
            public static string TrainSrcData { get { return Argument["TRAIN-SRC"].Value; } }
            public static string TrainTgtData { get { return Argument["TRAIN-TGT"].Value; } }
            public static bool IsTrainFile { get { return (!TrainSrcData.Equals(string.Empty)) && (!TrainTgtData.Equals(string.Empty)); } }

            public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
            public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
            public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

            public static string DevSrcData { get { return Argument["DEV-SRC"].Value; } }
            public static string DevTgtData { get { return Argument["DEV-TGT"].Value; } }
            public static bool IsDevFile { get { return (!DevSrcData.Equals(string.Empty)) && (!DevTgtData.Equals(string.Empty)); } }

            public static int Src_Vocab_Dim { get { return int.Parse(Argument["SRC-VOCAB-DIM"].Value); } }
            public static int Tgt_Vocab_Dim { get { return int.Parse(Argument["TGT-VOCAB-DIM"].Value); } }

            public static int Train_Mini_Batch { get { return int.Parse(Argument["TRAIN-MINI-BATCH"].Value); } }
            public static int Dev_Mini_Batch { get { return int.Parse(Argument["DEV-MINI-BATCH"].Value); } }

            public static int MAX_TRAIN_SRC_LENGTH { get { return int.Parse(Argument["MAX-TRAIN-SRC-LENGTH"].Value); } }
            public static int MAX_TRAIN_TGT_LENGTH { get { return int.Parse(Argument["MAX-TRAIN-TGT-LENGTH"].Value); } }

            public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }


            public static int Src_Embed { get { return int.Parse(Argument["SRC-EMBED"].Value); } }
            public static int[] SRC_LAYER_DIM { get { return Argument["SRC-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int Tgt_Embed { get { return int.Parse(Argument["TGT-EMBED"].Value); } }
            public static int TGT_LAYER_DIM { get { return int.Parse(Argument["SRC-LAYER-DIM"].Value); } }

            public static int Tgt_Decode_Embed { get { return int.Parse(Argument["TGT-DECODE-EMBED"].Value); } }
            public static bool IS_SHARED_DECODE_EMBED { get { return int.Parse(Argument["IS-SHARE-EMBED"].Value) > 0; } }

            public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            //public static Vocab2Freq mVocabDict = null;
            //public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Tgt_Vocab); return mVocabDict; } }
            //public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
            //public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

            public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
            public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
            public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }


            public static int Recurrent_Step { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static PredType Ensemble_RL { get { return (PredType)int.Parse(Argument["ENSEMBLE-RL"].Value); } }
            public static int[] Recurrent_T { get { return Argument["RECURRENT-T"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static float Global_Att_Gamma { get { return float.Parse(Argument["GLOBAL-ATT-GAMMA"].Value); } }
            public static int Global_Att_Hidden { get { return int.Parse(Argument["GLOBAL-ATT-HIDDEN"].Value); } }
            public static int Global_Att_Type { get { return int.Parse(Argument["GLOBAL-ATT-TYPE"].Value); } }
            public static int Global_Mem_Dim { get { return int.Parse(Argument["GLOBAL-MEMORY-DIM"].Value); } }
            public static int Global_Mem_Size { get { return int.Parse(Argument["GLOBAL-MEMORY-SIZE"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
        }

        public override BuilderType Type { get { return BuilderType.APP_IL_REASONET; } }
        public enum PredType { RL_MAXITER, RL_AVGPROB }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> AddLSTMEncoder(ComputationGraph cg, LSTMStructure lstmModel,
            SeqDenseRecursiveData input, RunnerBehavior behavior)
        {
            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> memory = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
            SeqDenseRecursiveData SrclstmOutput = input;
            /***************** LSTM Encoder ******************************/
            for (int i = 0; i < lstmModel.LSTMCells.Count; i++)
            {
                FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], SrclstmOutput, behavior);
                SrclstmOutput = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);
                memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(encoderRunner.Output, encoderRunner.C));
            }
            return memory;
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> srcData,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
                                                             EmbedStructure embedSrc, EmbedStructure embedTgt,
                                                             LayerStructure transO, LayerStructure transC,
                                                             LSTMStructure d1Encoder, LSTMStructure d2Encoder, LSTMCell decoder,

                                                             MatrixStructure globalMemory, MLPAttentionStructure globalAttStruct,
                                                             GRUCell gruStruct, List<LayerStructure> termStruct,
                                                             MLPAttentionStructure srcAttention,
                                                             EmbedStructure decodeEmbedTgt,
                                                             LayerStructure ansLayer,
                                                             RunnerBehavior Behavior, CompositeNNStructure Model, EvaluationType evalType)
        {
            ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

            /**************** Get Source and Target Data from DataCashier *********/
            SeqSparseBatchData SrcData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(srcData, Behavior));

            SeqDenseRecursiveData SrcD1Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData, false, Behavior));
            SeqDenseRecursiveData SrcD2Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData, true, Behavior));

            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD1Memory = AddLSTMEncoder(cg, d1Encoder, SrcD1Embed, Behavior);
            List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD2Memory = AddLSTMEncoder(cg, d2Encoder, SrcD2Embed, Behavior);

            /**************** Bidirection Source LSTM Encoder Ensemble *********/
            List<SeqDenseBatchData> SrcEnsembleMemory = new List<SeqDenseBatchData>();
            List<Tuple<HiddenBatchData, HiddenBatchData>> SrcEnsembleStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();
            for (int i = 0; i < SrcD1Memory.Count; i++)
            {
                HiddenBatchData SrcD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item1, true, 0, SrcD1Memory[i].Item1.MapForward, Behavior));
                HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item1, false, 0, SrcD2Memory[i].Item1.MapForward, Behavior));

                HiddenBatchData SrcD1C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item2, true, 0, SrcD1Memory[i].Item2.MapForward, Behavior));
                HiddenBatchData SrcD2C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item2, false, 0, SrcD2Memory[i].Item2.MapForward, Behavior));

                HiddenBatchData SrcO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1O, SrcD2O }, Behavior));
                HiddenBatchData SrcC = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1C, SrcD2C }, Behavior));

                HiddenBatchData newSrcO = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transO, SrcO, Behavior));
                HiddenBatchData newSrcC = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transC, SrcC, Behavior));
                SrcEnsembleStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(newSrcO, newSrcC));
                //SrcEnsembleStatus.Add(new Tuple<BigLearn.HiddenBatchData, BigLearn.HiddenBatchData>(SrcO, SrcC));

                SeqDenseBatchData SrcSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item1, Behavior));
                SeqDenseBatchData SrcSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item1, Behavior));

                //SeqDenseBatchData SrcSeqD1C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item2, Behavior));
                //SeqDenseBatchData SrcSeqD2C = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item2, Behavior));

                SeqDenseBatchData SrcSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1O, SrcSeqD2O }, Behavior));
                //SeqDenseBatchData SrcSeqC = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1C, SrcSeqD2C }, Behavior));

                SrcEnsembleMemory.Add(SrcSeqO);
            }

            HiddenBatchData SrcStatus_O = SrcEnsembleStatus.Last().Item1;
            HiddenBatchData SrcStatus_C = SrcEnsembleStatus.Last().Item2;
            SeqDenseBatchData SrcMemory = SrcEnsembleMemory.Last();

            //map memory into hidden Addression.
            MatrixData SrcAttProject = new MatrixData(srcAttention.HiddenDim, srcAttention.MemoryDim, srcAttention.Wm, srcAttention.WmGrad, Behavior.Device);
            MatrixData SrcAttData = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(new MatrixData(SrcMemory), SrcAttProject, Behavior));
            SeqDenseBatchData SrcMemoryHook = new SeqDenseBatchData(
                new SequenceDataStat() { MAX_BATCHSIZE = SrcMemory.MAX_BATCHSIZE, MAX_SEQUENCESIZE = SrcMemory.MAX_SENTSIZE, FEATURE_DIM = SrcAttData.Column },
                SrcMemory.SampleIdx, SrcMemory.SentMargin, SrcAttData.Output, SrcAttData.Deriv, Behavior.Device);

            SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));
            /**************** Target LSTM Embedding *********/
            SeqDenseRecursiveData TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedTgt, TgtData.SequenceData, false, Behavior));

            
            MLPAttentionV5Runner srcAttentionRunner = new MLPAttentionV5Runner(srcAttention, SrcMemory, SrcMemoryHook,
                BuilderParameters.TgtSeqMaxLen, TgtlstmOutput.Stat.MAX_BATCHSIZE, TgtlstmOutput.Stat.MAX_SEQUENCESIZE, Behavior);
            srcAttentionRunner.IsDelegate = false;
            cg.AddRunner(srcAttentionRunner);

            SeqDenseRecursiveData TgtlstmDecode = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(decoder,
                            TgtlstmOutput, SrcStatus_O, SrcStatus_C, srcAttentionRunner, Behavior));
            SeqDenseBatchData TgtSeqStates = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(TgtlstmDecode, Behavior));

            /// Reasoning process.
            //IntArgument sentSize = new IntArgument("size");
            //cg.AddRunner(new SentSizeAssignmentRunner(TgtSeqStates, sentSize, Behavior));
            //BiMatchBatchData GlobalMemMatch = (BiMatchBatchData)cg.AddRunner(new RepeatBiMatchRunner(TgtSeqStates.MAX_SENTSIZE, globalMemory.Size, sentSize, Behavior));
            BasicReasoNetRunner ReasonRunner = new BasicReasoNetRunner(new HiddenBatchData(new MatrixData(TgtSeqStates)), BuilderParameters.Recurrent_Step,
                                                               globalMemory, globalAttStruct, BuilderParameters.Global_Att_Gamma, BuilderParameters.RL_DISCOUNT,
                                                               gruStruct, termStruct, Behavior, (Att_Type)BuilderParameters.Global_Att_Type);
            cg.AddRunner(ReasonRunner);


            //if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
            //    cg.AddRunner(new ModelDiskDumpRunner(Model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\Seq2Seq"));

            if (Behavior.RunMode == DNNRunMode.Train || evalType == EvaluationType.PERPLEXITY)
            {
                List<HiddenBatchData> AnsOutputs = new List<HiddenBatchData>();
                MatrixData decodeEmbed = new MatrixData(decodeEmbedTgt.Dim, decodeEmbedTgt.VocabSize, decodeEmbedTgt.Embedding, decodeEmbedTgt.EmbeddingGrad, Behavior.Device);

                for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
                {
                    HiddenBatchData o = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(ansLayer, ReasonRunner.StatusData[i], Behavior));
                    HiddenBatchData v = new HiddenBatchData((MatrixData)cg.AddRunner(new MatrixProductRunner(new MatrixData(o), decodeEmbed, Behavior)));
                    AnsOutputs.Add(v);
                }

                //if (Behavior.RunMode == DNNRunMode.Train)
                //    cg.AddObjective(new EmbedFullySoftmaxRunner(decodeEmbedTgt, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
                //else
                //    cg.AddRunner(new EmbedFullySoftmaxRunner(decodeEmbedTgt, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath, Behavior));
                cg.AddObjective(new MultiClassContrastiveRewardRunner(TgtData.SequenceLabel, AnsOutputs.ToArray(), ReasonRunner.AnsProb, BuilderParameters.Gamma,  Behavior));
            }
            else
            {
                //List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
                //for (int i = 0; i < SrcEnsembleMemory.Count; i++)
                //    decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
                //        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i], TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);

                //// the probability of exactly generate the true tgt.
                //LSTMEmbedDecodingRunner decodeRunner = new LSTMEmbedDecodingRunner(decoder, embedTgt, decodeEmbedTgt, SrcEnsembleStatus,
                //    decodeAttentionRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), Behavior, true);
                //EnsembleBeamSearchRunner beamRunner = new EnsembleBeamSearchRunner(0, 0,
                //    BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth, 
                //    decodeEmbedTgt.VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);
                //beamRunner.AddBeamSearch(decodeRunner);
                //cg.AddRunner(beamRunner);

                //switch (evalType)
                //{
                //    case EvaluationType.ACCURACY:
                //        cg.AddRunner(new BeamAccuracyRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
                //        break;
                //    case EvaluationType.BSBLEU:
                //        cg.AddRunner(new BLEUDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
                //        break;
                //    case EvaluationType.ROUGE:
                //        cg.AddRunner(new ROUGEDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
                //        break;
                //    case EvaluationType.TOPKBEAM:
                //        cg.AddRunner(new TopKBeamRunner(beamRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
                //        break;
                //}
            }
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);
            Logger.WriteLog("Loading Training/Validation Data.");

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            int aDirectLSTM = 1;
            int bDirectLSTM = 1;
            int ensemble = aDirectLSTM + bDirectLSTM;


            CompositeNNStructure modelStructure = new CompositeNNStructure();

            LSTMStructure d1LSTM = null;
            LSTMStructure d2LSTM = null;
            LSTMCell decodeLSTM = null;

            LayerStructure transO = null;
            LayerStructure transC = null;

            EmbedStructure embedSrc = null;
            EmbedStructure embedTgt = null;
            EmbedStructure embedDecodeTgt = null;
            MLPAttentionStructure srcAttStruct = null;

            LayerStructure AnsStruct = null;

            MatrixStructure globalMemory = null;
            MLPAttentionStructure globalAttStruct = null;
            GRUCell gruStruct = null;
            List<LayerStructure> termStruct = new List<LayerStructure>();

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                int srcVocabSize = BuilderParameters.Src_Vocab_Dim;
                int tgtVocabSize = BuilderParameters.Tgt_Vocab_Dim;

                embedSrc = new EmbedStructure(srcVocabSize, BuilderParameters.Src_Embed, device);
                modelStructure.AddLayer(embedSrc);

                d1LSTM = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.SRC_LAYER_DIM, device);
                d2LSTM = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.SRC_LAYER_DIM, device);
                modelStructure.AddLayer(d1LSTM);
                modelStructure.AddLayer(d2LSTM);

                int srcDim = BuilderParameters.SRC_LAYER_DIM.Last() * ensemble;
                int tgtDim = BuilderParameters.TGT_LAYER_DIM;

                transO = new LayerStructure(srcDim, tgtDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);
                transC = new LayerStructure(srcDim, tgtDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);
                modelStructure.AddLayer(transO);
                modelStructure.AddLayer(transC);

                embedTgt = new EmbedStructure(tgtVocabSize, BuilderParameters.Tgt_Embed, device);
                modelStructure.AddLayer(embedTgt);

                srcAttStruct = new MLPAttentionStructure(tgtDim, srcDim, BuilderParameters.Attention_Hidden, device);
                modelStructure.AddLayer(srcAttStruct);

                decodeLSTM = new LSTMCell(BuilderParameters.Tgt_Embed, tgtDim, srcDim, device, RndRecurrentInit.RndNorm);
                modelStructure.AddLayer(decodeLSTM);

                globalMemory = new MatrixStructure(BuilderParameters.Global_Mem_Dim, BuilderParameters.Global_Mem_Size, device);
                globalAttStruct = new MLPAttentionStructure(tgtDim, BuilderParameters.Global_Mem_Dim, BuilderParameters.Global_Att_Hidden, device);
                gruStruct = new GRUCell(BuilderParameters.Global_Mem_Dim, tgtDim, device, RndRecurrentInit.RndNorm);
                modelStructure.AddLayer(globalMemory);
                modelStructure.AddLayer(globalAttStruct);
                modelStructure.AddLayer(gruStruct);

                int tInput = tgtDim;
                for (int i = 0; i < BuilderParameters.Recurrent_T.Length; i++)
                {
                    termStruct.Add(new LayerStructure(tInput, BuilderParameters.Recurrent_T[i],
                        i == BuilderParameters.Recurrent_T.Length - 1 ? A_Func.Linear : A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false, device));
                    tInput = BuilderParameters.Recurrent_T[i];
                    modelStructure.AddLayer(termStruct[i]);
                }

                int decodeEmbedDim = BuilderParameters.Tgt_Decode_Embed;
                if (BuilderParameters.IS_SHARED_DECODE_EMBED)
                {
                    embedDecodeTgt = embedTgt;
                    decodeEmbedDim = BuilderParameters.Tgt_Embed;
                }
                else
                {
                    embedDecodeTgt = new EmbedStructure(tgtVocabSize, BuilderParameters.Tgt_Decode_Embed, device);
                    modelStructure.AddLayer(embedDecodeTgt);
                }

                AnsStruct = new LayerStructure(tgtDim, decodeEmbedDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false, device);
                modelStructure.AddLayer(AnsStruct);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    int layerNum = CompositeNNStructure.DeserializeModelCount(modelReader);
                    embedSrc = modelStructure.DeserializeModel<EmbedStructure>(modelReader, DeviceType.GPU);

                    d1LSTM = modelStructure.DeserializeModel<LSTMStructure>(modelReader, DeviceType.GPU);

                    d2LSTM = modelStructure.DeserializeModel<LSTMStructure>(modelReader, DeviceType.GPU);

                    transO = modelStructure.DeserializeModel<LayerStructure>(modelReader, DeviceType.GPU);

                    transC = modelStructure.DeserializeModel<LayerStructure>(modelReader, DeviceType.GPU);

                    embedTgt = modelStructure.DeserializeModel<EmbedStructure>(modelReader, DeviceType.GPU);

                    srcAttStruct = modelStructure.DeserializeModel<MLPAttentionStructure>(modelReader, DeviceType.GPU);

                    decodeLSTM = modelStructure.DeserializeModel<LSTMCell>(modelReader, DeviceType.GPU);

                    globalMemory = modelStructure.DeserializeModel<MatrixStructure>(modelReader, DeviceType.GPU);

                    globalAttStruct = modelStructure.DeserializeModel<MLPAttentionStructure>(modelReader, DeviceType.GPU);

                    gruStruct = modelStructure.DeserializeModel<GRUCell>(modelReader, DeviceType.GPU);

                    termStruct = new List<LayerStructure>();
                    for (int i = 0; i < BuilderParameters.Recurrent_T.Length; i++)
                    {
                        termStruct.Add(modelStructure.DeserializeModel<LayerStructure>(modelReader, DeviceType.GPU));
                    }

                    embedDecodeTgt = modelStructure.DeserializeModel<EmbedStructure>(modelReader, DeviceType.GPU);

                    AnsStruct = modelStructure.DeserializeModel<LayerStructure>(modelReader, DeviceType.GPU);
                }
            }


            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin,
                        embedSrc, embedTgt, transO, transC, d1LSTM, d2LSTM, decodeLSTM,
                        globalMemory, globalAttStruct, gruStruct, termStruct,
                         srcAttStruct, embedDecodeTgt, AnsStruct,
                        new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib },
                        modelStructure, EvaluationType.PERPLEXITY);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                            embedSrc, embedTgt, transO, transC, d1LSTM, d2LSTM, decodeLSTM,
                            globalMemory, globalAttStruct, gruStruct, termStruct,
                            srcAttStruct, embedDecodeTgt, AnsStruct,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
                            modelStructure, BuilderParameters.Evaluation);


                    ComputationGraph devCG = null;
                    if (BuilderParameters.IsDevFile)
                        devCG = BuildComputationGraph(DataPanel.DevSrcBin, DataPanel.DevTgtBin,
                            embedSrc, embedTgt, transO, transC, d1LSTM, d2LSTM, decodeLSTM,
                            globalMemory, globalAttStruct, gruStruct, termStruct,
                            srcAttStruct, embedDecodeTgt, AnsStruct,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
                            modelStructure, EvaluationType.PERPLEXITY);

                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    double validScore = 0;
                    double bestValidScore = double.MinValue;
                    double bestValidIter = -1;

                    double bestDevScore = double.MaxValue;
                    double bestDevIter = -1;
                    double bestDevValidScore = 0;

                    string initName = string.Format(@"{0}\\Seq2Seq.{1}.model", BuilderParameters.ModelOutputPath, "init");
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(initName, FileMode.Create, FileAccess.Write)))
                    {
                        modelStructure.Serialize(writer);
                    }

                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        string modelName = string.Format(@"{0}\\Seq2Seq.{1}.model", BuilderParameters.ModelOutputPath, iter);

                        using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
                        {
                            modelStructure.Serialize(writer);
                        }

                        if (BuilderParameters.IsValidFile)
                        {
                            validScore = validCG.Execute();
                            if (validScore >= bestValidScore)
                            {
                                bestValidScore = validScore;
                                bestValidIter = iter;
                            }
                            Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
                        }

                        if (BuilderParameters.IsDevFile)
                        {
                            double devScore = devCG.Execute();
                            if (devScore < bestDevScore)
                            {
                                bestDevScore = devScore;
                                bestDevIter = iter;
                                bestDevValidScore = validScore;
                            }
                            Logger.WriteLog("Best Dev Score {0} at iteration {1}  Valid Score {2}", bestDevScore, bestDevIter, bestDevValidScore);
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
                            embedSrc, embedTgt, transO, transC, d1LSTM, d2LSTM, decodeLSTM,
                            globalMemory, globalAttStruct, gruStruct, termStruct,
                            srcAttStruct, embedDecodeTgt, AnsStruct,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
                            modelStructure, BuilderParameters.Evaluation);
                    double predScore = predCG.Execute();
                    Logger.WriteLog("Prediction Score {0}", predScore);
                    break;
            }
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;

            /// <summary>
            ///  Report PPL instead of BLEU Score.
            /// </summary>
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> DevSrcBin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevTgtBin = null;

            public static void Init()
            {
                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    if (!File.Exists(BuilderParameters.TrainSrcData + ".bin") || !File.Exists(BuilderParameters.TrainTgtData + ".bin"))
                    {
                        List<Tuple<List<int>, List<int>>> data = CommonExtractor.LoadPairSequence(
                                new StreamReader(BuilderParameters.TrainSrcData),
                                new StreamReader(BuilderParameters.TrainTgtData));

                        IEnumerable<Tuple<List<int>, List<int>>> shuffleData = CommonExtractor.RandomShuffle(data, 100000, ParameterSetting.RANDOM_SEED);

                        if (BuilderParameters.MAX_TRAIN_SRC_LENGTH > 0)
                        {
                            shuffleData = shuffleData.Where(i => i.Item1.Count <= BuilderParameters.MAX_TRAIN_SRC_LENGTH);
                        }
                        if (BuilderParameters.MAX_TRAIN_TGT_LENGTH > 0)
                        {
                            shuffleData = shuffleData.Where(i => i.Item2.Count <= BuilderParameters.MAX_TRAIN_TGT_LENGTH);
                        }

                        IEnumerable<List<Dictionary<int, float>>> srcSeqData = shuffleData.Select(i => Util.Enumable2SeqDict<int>(i.Item1));
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.TrainSrcData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(srcSeqData, BuilderParameters.Src_Vocab_Dim, BuilderParameters.Train_Mini_Batch, writer);
                        }

                        IEnumerable<List<int>> tgtSeqData = shuffleData.Select(i => i.Item2);
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.TrainTgtData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(tgtSeqData, BuilderParameters.Tgt_Vocab_Dim, BuilderParameters.Train_Mini_Batch, writer);
                        }
                    }
                }

                if (BuilderParameters.IsValidFile)
                {
                    if (!File.Exists(BuilderParameters.ValidSrcData + ".bin") || !File.Exists(BuilderParameters.ValidTgtData + ".bin"))
                    {
                        List<Tuple<List<int>, List<int>>> data = CommonExtractor.LoadPairSequence(
                                new StreamReader(BuilderParameters.ValidSrcData),
                                new StreamReader(BuilderParameters.ValidTgtData));

                        IEnumerable<List<Dictionary<int, float>>> srcSeqData = data.Select(i => Util.Enumable2SeqDict<int>(i.Item1));
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ValidSrcData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(srcSeqData, BuilderParameters.Src_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                        }

                        IEnumerable<List<int>> tgtSeqData = data.Select(i => i.Item2);
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ValidTgtData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(tgtSeqData, BuilderParameters.Tgt_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                        }
                    }
                }

                if (BuilderParameters.IsDevFile)
                {
                    if (!File.Exists(BuilderParameters.DevSrcData + ".bin") || !File.Exists(BuilderParameters.DevTgtData + ".bin"))
                    {
                        List<Tuple<List<int>, List<int>>> data = CommonExtractor.LoadPairSequence(
                                new StreamReader(BuilderParameters.DevSrcData),
                                new StreamReader(BuilderParameters.DevTgtData));

                        IEnumerable<List<Dictionary<int, float>>> srcSeqData = data.Select(i => Util.Enumable2SeqDict<int>(i.Item1));
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.DevSrcData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(srcSeqData, BuilderParameters.Src_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                        }

                        IEnumerable<List<int>> tgtSeqData = data.Select(i => i.Item2);
                        using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.DevTgtData + ".bin", FileMode.Create, FileAccess.Write)))
                        {
                            CommonExtractor.ExtractSeqSparseDataBinary(tgtSeqData, BuilderParameters.Tgt_Vocab_Dim, BuilderParameters.Dev_Mini_Batch, writer);
                        }
                    }
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidSrcData + ".bin");
                    ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData + ".bin");

                    ValidSrcBin.InitThreadSafePipelineCashier(100, false);
                    ValidTgtBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.IsDevFile)
                {
                    DevSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.DevSrcData + ".bin");
                    DevTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevTgtData + ".bin");

                    DevSrcBin.InitThreadSafePipelineCashier(100, false);
                    DevTgtBin.InitThreadSafePipelineCashier(100, false);
                }

                if (BuilderParameters.RunMode == DNNRunMode.Train)
                {
                    TrainSrcBin = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainSrcData + ".bin");
                    TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainTgtData + ".bin");

                    TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                    TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                }

            }
        }
    }
}