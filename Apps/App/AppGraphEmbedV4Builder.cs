﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class AppGraphEmbedV4Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
            public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }

            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static string TestData { get { return Argument["TEST"].Value; } }

            public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
            public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int IN_EmbedDim { get { return int.Parse(Argument["IN-EMBED-DIM"].Value); } }

            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int OUT_EmbedDim { get { return int.Parse(Argument["OUT-EMBED-DIM"].Value); } }
            public static int MEM_EmbedDim { get { return int.Parse(Argument["MEM-EMBED-DIM"].Value); } }
            public static int MemorySize { get { return int.Parse(Argument["MEMORY-SIZE"].Value); } }
            public static int ATT_DIM { get { return int.Parse(Argument["ATT-DIM"].Value); } }
            public static float ATT_GAMMA { get { return float.Parse(Argument["ATT-GAMMA"].Value); } }
            public static Att_Type AttType { get { return (Att_Type)int.Parse(Argument["ATT-TYPE"].Value); } }
           
            public static int NTRIAL { get { return int.Parse(Argument["NTRIAL"].Value); } }

            public static int PRED_ITER { get { return int.Parse(Argument["PRED-ITER"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static float T_MAX_CLIP { get { return float.Parse(Argument["T-MAX-CLIP"].Value); } }
            public static float T_MIN_CLIP { get { return float.Parse(Argument["T-MIN-CLIP"].Value); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func T_AF { get { return (A_Func)int.Parse(Argument["T-AF"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("ENTITY2ID", new ParameterArgument(string.Empty, "Entity 2 ID."));
                Argument.Add("RELATION2ID", new ParameterArgument(string.Empty, "Relation 2 ID."));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));


                Argument.Add("IN-EMBED-DIM", new ParameterArgument("100", "In Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "In Relation Embedding Dim"));
                Argument.Add("MEMORY-SIZE", new ParameterArgument("64", "Memory Size."));
                Argument.Add("MEM-EMBED-DIM", new ParameterArgument("200", "Memory Embedding Dim"));

                Argument.Add("OUT-EMBED-DIM", new ParameterArgument("100", "Out Node Embedding Dim"));
                
                Argument.Add("ATT-DIM", new ParameterArgument("200", "Attention Hidden Dimension"));
                Argument.Add("ATT-GAMMA", new ParameterArgument("10", "Attention Gamma Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)Att_Type.W_INNER_PRODUCT).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(Att_Type))));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "Reward discount"));

                Argument.Add("T-MAX-CLIP", new ParameterArgument("0", "Maximum T Clip"));
                Argument.Add("T-MIN-CLIP", new ParameterArgument("0", "Minimum T Clip"));

                Argument.Add("NTRIAL", new ParameterArgument("1", "Negative samples"));
                Argument.Add("PRED-ITER", new ParameterArgument("20", "Predict per itertion"));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("6", "Recurrent Step."));

                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));


                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score File."));

                //Argument.Add("EPS-SCHEDULE", new ParameterArgument("0:1,5000000:1,50000000:1,100000000:1,200000000:0.1,400000000:0.05", "Epslion Schedule."));
                //Argument.Add("HOP-SCHEDULE", new ParameterArgument("0:0,5000000:0,50000000:0,100000000:0,200000000:1,400000000:2", "Hop Schedule."));
                //Argument.Add("EBD-LR-SCHEDULE", new ParameterArgument("0:0.01,5000000:0.005,50000000:0.001,100000000:0.0005,200000000:0.0001", "EMD Lr Schedule."));
                //Argument.Add("ATT-LR-SCHEDULE", new ParameterArgument("0:0,5000000:0,50000000:0.01,100000000:0.005,200000000:0.001,400000000:0.0001", "Hop Schedule."));

                //Argument.Add("MAX-NEIGHBOR", new ParameterArgument("10", "Maximum Neighbor Set."));

                //Argument.Add("NTRAIL", new ParameterArgument("50", "Number of Negative Samples"));
                //Argument.Add("L1", new ParameterArgument("1", "L1 Loss"));
                //Argument.Add("MARGIN", new ParameterArgument("1", "Margin Parameter"));

                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_GRAPH_EMBED_V4; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        /// <summary>
        /// Sampleing Graph.
        /// </summary>
        class SampleEmbedV2Runner : StructRunner
        {
            List<Tuple<int, int, int>> Graph { get; set; }

            public HiddenBatchData Status { get; set; }
            public CudaPieceFloat Label { get; set; }

            public int NTrialNum = BuilderParameters.NTRIAL;
            public CudaPieceInt TargetIndex;

            EmbedStructure InNodeEmbed { get; set; }

            EmbedStructure LinkEmbed { get; set; }

            int MiniBatchSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }

            /// <summary>
            /// Tuple (source, target, link).
            /// </summary>
            public List<Tuple<int, int, int>> BatchLinks = new List<Tuple<int, int, int>>();

            public SampleEmbedV2Runner(List<Tuple<int, int, int>> graph, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, int batchSize, RunnerBehavior behavior) :
                base(Structure.Empty, behavior)
            {
                Graph = graph;
                InNodeEmbed = inNodeEmbed;
                LinkEmbed = linkEmbed;

                MiniBatchSize = batchSize;

                int statusDim = InNodeEmbed.Dim + LinkEmbed.Dim;

                Label = new CudaPieceFloat(MiniBatchSize, true, Behavior.Device == DeviceType.GPU);
                TargetIndex = new CudaPieceInt(MiniBatchSize * (NTrialNum + 1), true, Behavior.Device == DeviceType.GPU);

                Status = new HiddenBatchData(MiniBatchSize, statusDim, Behavior.RunMode, Behavior.Device);

                Shuffle = new DataRandomShuffling(Graph.Count * 2);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                // Perf is not threadSafe.
                //var dataCounter = PerfCounter.Manager.Instance["Data"].Begin(); 
                
                InNodeEmbed.Embedding.SyncToCPU();
                LinkEmbed.Embedding.SyncToCPU();

                BatchLinks.Clear();
                int batchSize = 0;
                while (batchSize < MiniBatchSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }

                    int srcId = idx < Graph.Count ? Graph[idx].Item1 : Graph[idx - Graph.Count].Item2;
                    int tgtId = idx < Graph.Count ? Graph[idx].Item2 : Graph[idx - Graph.Count].Item1;

                    int linkId = idx < Graph.Count ? Graph[idx].Item3 : (Graph[idx - Graph.Count].Item3 + DataPanel.RelationNum);

                    BatchLinks.Add(new Tuple<int, int, int>(srcId, tgtId, linkId));

                    Label.MemPtr[batchSize] = tgtId;

                    TargetIndex.MemPtr[batchSize * (NTrialNum + 1)] = tgtId;
                    HashSet<int> negHash = new HashSet<int>();
                    while (negHash.Count < NTrialNum)
                    {
                        int negId = -1;
                        do
                        {
                            negId = Util.URandom.Next(DataPanel.EntityNum);
                        } while (DataPanel.IsInTrainGraph(srcId, negId, linkId) || negHash.Contains(negId));
                        negHash.Add(negId);
                        TargetIndex.MemPtr[batchSize * (NTrialNum + 1) + negHash.Count] = negId;
                    }

                    FastVector.Add_Vector(Status.Output.Data.MemPtr, batchSize * Status.Dim, 
                                            InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);

                    FastVector.Add_Vector(Status.Output.Data.MemPtr, batchSize * Status.Dim + InNodeEmbed.Dim, 
                                            LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);
                    batchSize += 1;
                }

                Status.BatchSize = batchSize;

                if (batchSize == 0) { IsTerminate = true; return; }

                Label.SyncFromCPU(batchSize);
                TargetIndex.SyncFromCPU(batchSize * (NTrialNum + 1));
                Status.Output.Data.SyncFromCPU(Status.BatchSize * Status.Dim);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Status.Deriv.Data, Status.Dim * Status.BatchSize);
            }

            public override void Update()
            {
                Status.Deriv.Data.SyncToCPU(Status.Dim * Status.BatchSize);

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

                for (int b = 0; b < Status.BatchSize; b++)
                {
                    int srcIdx = BatchLinks[b].Item1;
                    int tgtIdx = BatchLinks[b].Item2;
                    int linkIdx = BatchLinks[b].Item3;

                    InNodeEmbed.EmbeddingOptimizer.PushSparseGradient(srcIdx * InNodeEmbed.Dim, InNodeEmbed.Dim);
                    LinkEmbed.EmbeddingOptimizer.PushSparseGradient(linkIdx * LinkEmbed.Dim, LinkEmbed.Dim);

                    FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
                                    Status.Deriv.Data.MemPtr, b * Status.Dim, InNodeEmbed.Dim, 1, InNodeEmbed.EmbeddingOptimizer.GradientStep);
                    FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim, 
                                    Status.Deriv.Data.MemPtr, b * Status.Dim + InNodeEmbed.Dim, LinkEmbed.Dim, 1, LinkEmbed.EmbeddingOptimizer.GradientStep);
                }

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();

            }
        }

        enum DistanceType { L1Dist, L2Dist }
        unsafe class EmbedDistanceRunner : StructRunner
        {
            /// <summary>
            /// one model can be used in two places.
            /// </summary>
            public new EmbedStructure Model { get { return (EmbedStructure)base.Model; } set { base.Model = value; } }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }

            CudaPieceInt Index { get; set; }
            int Dim { get; set; }

            RepeatBiMatchRunner FullMatchRunner = null;
            BiMatchBatchData MatchData;

            HiddenBatchData TmpOutput;

            DistanceType Distance = DistanceType.L1Dist;
            public EmbedDistanceRunner(ComputationGraph cg, EmbedStructure model, HiddenBatchData input, CudaPieceInt index, int dim, DistanceType d, RunnerBehavior behavior) : base(model, behavior)
            {
                Input = input;
                Index = index;

                IntArgument arg = new IntArgument("arg");
                cg.AddRunner(new HiddenDataBatchSizeRunner(Input, arg, behavior));

                Distance = d;
                if (Index == null)
                {
                    Dim = Model.VocabSize;
                    FullMatchRunner = new RepeatBiMatchRunner(Input.MAX_BATCHSIZE, Model.VocabSize, arg, Behavior);
                    //rRunner.Forward();
                    MatchData = FullMatchRunner.Output;
                }
                else
                {
                    Dim = dim;
                    MatchData = new BiMatchBatchData(new BiMatchBatchDataStat() { MAX_SRC_BATCHSIZE = Input.MAX_BATCHSIZE, MAX_TGT_BATCHSIZE = Model.VocabSize, MAX_MATCH_BATCHSIZE = Dim * Input.MAX_BATCHSIZE }, Behavior.Device);
                }

                TmpOutput = new HiddenBatchData(Input.MAX_BATCHSIZE * Dim, Model.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Dim, Behavior.RunMode, Behavior.Device);

            }

            public override void Forward()
            {
                if (Index != null)
                {
                    List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
                    Index.SyncToCPU(Input.BatchSize * Dim);
                    for (int b = 0; b < Input.BatchSize; b++)
                    {
                        for (int d = 0; d < Dim; d++)
                        {
                            matchList.Add(new Tuple<int, int, float>(b, Index.MemPtr[b * Dim + d], 1));
                        }
                    }
                    MatchData.SetMatch(matchList);
                }

                Input.Output.Data.SyncToCPU(Input.BatchSize * Input.Dim);
                Model.Embedding.SyncToCPU(Model.Dim * Model.VocabSize);

                MathOperatorManager.CreateInstance(DeviceType.CPU_FAST_VECTOR).Matrix_AdditionMask(Input.Output.Data, 0, MatchData.SrcIdx, 0,
                                               Model.Embedding, 0, MatchData.TgtIdx, 0,
                                               TmpOutput.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Model.Dim, Input.BatchSize * Dim, 1, -1, 0);
                TmpOutput.Output.Data.SyncFromCPU(Input.BatchSize * Dim * Model.Dim);


                Output.BatchSize = Input.BatchSize;

                if (Distance == DistanceType.L1Dist)
                    ComputeLib.MatrixL1Norm(TmpOutput.Output.Data, Model.Dim, Input.BatchSize * Dim, Output.Output.Data);
                else if (Distance == DistanceType.L2Dist)
                    ComputeLib.MatrixL2Norm(TmpOutput.Output.Data, Model.Dim, Input.BatchSize * Dim, Output.Output.Data);

                Output.Output.Data.SyncToCPU();
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                if (Distance == DistanceType.L1Dist)
                    ComputeLib.DerivMatrixL1Norm(TmpOutput.Output.Data, Model.Dim, Input.BatchSize * Dim, Output.Output.Data, Output.Deriv.Data, TmpOutput.Deriv.Data);
                else if (Distance == DistanceType.L2Dist)
                    ComputeLib.DerivMatrixL2Norm(TmpOutput.Output.Data, Model.Dim, Input.BatchSize * Dim, Output.Output.Data, Output.Deriv.Data, TmpOutput.Deriv.Data);

                ComputeLib.ColumnWiseSumMask(TmpOutput.Deriv.Data, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty, MatchData.Src2MatchIdx, Input.BatchSize, Input.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                            Input.BatchSize * Dim, Model.Dim, 1, 1);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer) { Model.EmbeddingOptimizer.BeforeGradient(); }

                if(FullMatchRunner!= null) { FullMatchRunner.Forward(); }

                //int preE = 0;
                for (int i = 0; i < MatchData.TgtSize; i++)
                {
                    Model.EmbeddingOptimizer.PushSparseGradient(MatchData.Tgt2Idx.MemPtr[i] * Model.Dim, Model.Dim); // i * Model.Dim, Model.Dim);
                }

                TmpOutput.Deriv.Data.SyncToCPU(Input.BatchSize * Dim * Model.Dim);
                BasicMathlib.ColumnWiseSumMask_Safe(TmpOutput.Deriv.Data.CpuPtr, MatchData.Tgt2MatchElement.CpuPtr, null, MatchData.Tgt2MatchIdx.CpuPtr, MatchData.TgtSize,
                                            Model.EmbeddingOptimizer.Gradient.CpuPtr, MatchData.Tgt2Idx.CpuPtr, Input.BatchSize * Dim, Model.Dim, 1, -Model.EmbeddingOptimizer.GradientStep);
                //MathOperatorManager.CreateInstance(DeviceType.CPU_FAST_VECTOR).ColumnWiseSumMask(TmpOutput.Deriv.Data, 0, MatchData.Tgt2MatchElement, 0, CudaPieceFloat.Empty, MatchData.Tgt2MatchIdx, MatchData.TgtSize,
                //                            Model.EmbeddingOptimizer.Gradient, 0, MatchData.Tgt2Idx, 0, Input.BatchSize * Dim, Model.Dim, 1, -Model.EmbeddingOptimizer.GradientStep);

                if (!IsDelegateOptimizer) { Model.EmbeddingOptimizer.AfterGradient(); }
            }
        }

        public enum ObjectiveFunc { FullSoftmax = 0, SampleSoftmax = 1  }
        public enum PredType { RL_MAXITER = 0, RL_AVGSIM = 1 }
        /// <summary>
        /// Knowledge Graph Compeletion.
        /// </summary>
        class KGCPredictionRunner : ObjectiveRunner
        {
            int lsum = 0, lsum_filter = 0;
            int rsum = 0, rsum_filter = 0;
            int lp_n = 0, lp_n_filter = 0;
            int rp_n = 0, rp_n_filter = 0;
            int HitK = 10;

            int Iteration = 0;
            int SmpIdx = 0;

            float AvgReward = 0;

            CudaPieceFloat Label { get; set; }

            HiddenBatchData Score { get; set; }
            List<Tuple<int, int, int>> BatchLinks { get; set; }

            int EvalPerIteration = 1;

            StreamWriter ScoreWriter = null;

            bool IsReverse = false;
            string ScorePath { get; set; }
            public KGCPredictionRunner(CudaPieceFloat label, List<Tuple<int, int, int>> batchLinks, 
                HiddenBatchData score, bool isReverse, string scorePath, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Label = label;
                BatchLinks = batchLinks;
                IsReverse = isReverse;
                Score = score;

                ScorePath = scorePath;
                Iteration = 0;
            }

            public override void Init()
            {
                SmpIdx = 0;

                lsum = 0;
                lsum_filter = 0;
                rsum = 0;
                rsum_filter = 0;
                lp_n = 0;
                lp_n_filter = 0;
                rp_n = 0;
                rp_n_filter = 0;

                AvgReward = 0;
                if(!ScorePath.Equals(string.Empty))
                {
                    if(!Directory.Exists(ScorePath))
                    {
                        Directory.CreateDirectory(ScorePath);
                    }
                    ScoreWriter = new StreamWriter(ScorePath + ".score." + Iteration.ToString());
                }
            }

            public override void Complete()
            {
                if ((Iteration + 1) % EvalPerIteration == 0)
                {
                    Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));

                    Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));

                    Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
                    ObjectiveScore = (lsum_filter + rsum_filter) * 1.0 / SmpIdx;
                }
                else
                {
                    Logger.WriteLog("Average Reward {0}", AvgReward / SmpIdx);
                }

                if (ScoreWriter != null) ScoreWriter.Close();

                Iteration += 1;
            }
            public override void Forward()
            {
                if ((Iteration + 1) % EvalPerIteration == 0)
                {
                    Score.Output.Data.SyncToCPU(Score.BatchSize * Score.Dim);

                    int[] lindicies = new int[Score.BatchSize * Score.Dim];
                    for (int i = 0; i < Score.BatchSize; i++)
                    {
                        for (int c = 0; c < Score.Dim; c++)
                        {
                            lindicies[i * Score.Dim + c] = c;
                        }
                    }

                    //Parallel. for (int b = 0; b < Score.BatchSize; b++)
                    //Parallel.For(0, Score.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, b => //  (int i = 0; i < Label.BatchSize; i++)
                    for(int b =0; b<Score.BatchSize;b++)
                    {
                        Array.Sort(Score.Output.Data.MemPtr, lindicies, b * Score.Dim, Score.Dim);

                        int chead = BatchLinks[b].Item1;
                        int relation = BatchLinks[b].Item3;
                        int ctail = BatchLinks[b].Item2;

                        int filter = 1;
                        for (int c = Score.Dim - 1; c >= 0; c--)
                        {
                            int ptail = IsReverse ? lindicies[b * Score.Dim + Score.Dim - 1 - c] : lindicies[b * Score.Dim + c];
                            
                            if (!DataPanel.IsInGraph(chead, ptail, relation)) filter++;
                            if (ctail == ptail)
                            {
                                if (relation < DataPanel.RelationNum)
                                {
                                    Interlocked.Add(ref lsum, Score.Dim - c);
                                    Interlocked.Add(ref lsum_filter, filter);
                                    if (Score.Dim - c <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
                                    if (filter < HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
                                }
                                else
                                {
                                    Interlocked.Add(ref rsum, Score.Dim - c);
                                    Interlocked.Add(ref rsum_filter, filter);
                                    if (Score.Dim - c <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
                                    if (filter < HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
                                }
                                ScoreWriter.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", chead, relation, ctail, filter, Score.Dim - c);
                                break;
                            }
                        }
                    }//);
                }
                else
                {
                    //ComputeLib.SoftMax(Score.Output.Data, Score.Output.Data, Score.Dim, Score.BatchSize, 1);
                    Score.Output.Data.SyncToCPU(Score.BatchSize * Score.Dim);
                    for (int b = 0; b < Score.BatchSize; b++)
                    {
                        int ctail = BatchLinks[b].Item2;
                        AvgReward += Score.Output.Data.MemPtr[b * Score.Dim + ctail];
                    }
                }
                SmpIdx += Score.BatchSize;
            }
            
        }

        class TopKPredictionRunner : StructRunner
        {
            int lsum = 0, lsum_filter = 0;
            int rsum = 0, rsum_filter = 0;
            int lp_n = 0, lp_n_filter = 0;
            int rp_n = 0, rp_n_filter = 0;
            int HitK = 10;

            int Iteration = 0;
            int SmpIdx = 0;

            CudaPieceFloat Label { get; set; }

            HiddenBatchData Score { get; set; }
            List<Tuple<int, int, int>> BatchLinks { get; set; }

            public List<Tuple<int, int, List<int>>> BatchResults = new List<Tuple<int, int, List<int>>>(); 

            bool IsReverse = false;
            public TopKPredictionRunner(CudaPieceFloat label, List<Tuple<int, int, int>> batchLinks,
                HiddenBatchData score, bool isReverse, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Label = label;
                BatchLinks = batchLinks;
                IsReverse = isReverse;
                Score = score;
                Iteration = 0;
            }

            public override void Init()
            {
                SmpIdx = 0;

                lsum = 0;
                lsum_filter = 0;
                rsum = 0;
                rsum_filter = 0;
                lp_n = 0;
                lp_n_filter = 0;
                rp_n = 0;
                rp_n_filter = 0;
            }

            public override void Complete()
            {
                {
                    Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));

                    Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));

                    Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
                }

                Iteration += 1;
            }
            public override void Forward()
            {
                Score.Output.Data.SyncToCPU(Score.BatchSize * Score.Dim);

                int[] lindicies = new int[Score.BatchSize * Score.Dim];
                for (int i = 0; i < Score.BatchSize; i++)
                {
                    for (int c = 0; c < Score.Dim; c++)
                    {
                        lindicies[i * Score.Dim + c] = c;
                    }
                }

                BatchResults.Clear();

                //Parallel. for (int b = 0; b < Score.BatchSize; b++)
                //Parallel.For(0, Score.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, b => //  (int i = 0; i < Label.BatchSize; i++)
                for (int b = 0; b < Score.BatchSize; b++)
                {
                    Array.Sort(Score.Output.Data.MemPtr, lindicies, b * Score.Dim, Score.Dim);

                    int chead = BatchLinks[b].Item1;
                    int relation = BatchLinks[b].Item3;
                    int ctail = BatchLinks[b].Item2;


                    int filter = 1;
                    int nonFilter = 1;
                    int Filter = 1;
                    List<int> topResult = new List<int>();

                    for (int c = Score.Dim - 1; c >= 0; c--)
                    {
                        int ptail = IsReverse ? lindicies[b * Score.Dim + Score.Dim - 1 - c] : lindicies[b * Score.Dim + c];
                        if (!DataPanel.IsInGraph(chead, ptail, relation))
                        {
                            filter++;
                            if (filter <= HitK) { topResult.Add(ptail); }
                        }


                        if (ctail == ptail)
                        {
                            if (relation < DataPanel.RelationNum)
                            {
                                Interlocked.Add(ref lsum, Score.Dim - c);
                                Interlocked.Add(ref lsum_filter, filter);
                                if (Score.Dim - c <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
                                if (filter < HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
                            }
                            else
                            {
                                Interlocked.Add(ref rsum, Score.Dim - c);
                                Interlocked.Add(ref rsum_filter, filter);
                                if (Score.Dim - c <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
                                if (filter < HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
                            }
                            nonFilter = Score.Dim - c;
                            Filter = filter;
                        }
                        if (topResult.Count >= HitK) break;
                    }
                    BatchResults.Add(new Tuple<int, int, List<int>>(Filter, nonFilter, topResult));
                }//);

                SmpIdx += Score.BatchSize;
            }

        }


        class StatusDumpRunner : StructRunner
        {
            List<Tuple<int, int, int>> BatchLinks { get; set; }
            List<Tuple<int, int, List<int>>>[] BatchResults { get; set; }

            HiddenBatchData[] Prob { get; set; }
            HiddenBatchData[] Status { get; set; }
            string FileName { get; set; }
            StreamWriter Writer { get; set; }
            public StatusDumpRunner(List<Tuple<int,int,int>> batchLinks, List<Tuple<int, int, List<int>>>[] batchResults,
                HiddenBatchData[] prob, HiddenBatchData[] status, string fileName, RunnerBehavior behavior)
                :base(Structure.Empty, behavior)
            {
                BatchLinks = batchLinks;
                BatchResults = batchResults;

                Prob = prob;
                Status = status;
                FileName = fileName;
            }

            public override void Forward()
            {
                for (int s = 0; s < Prob.Length; s++)
                {
                    Prob[s].Output.Data.SyncToCPU();
                    Status[s].Output.Data.SyncToCPU();
                }

                for (int i = 0; i < BatchLinks.Count; i++)
                {
                    Writer.WriteLine(string.Format("{0}\t{1}\t{2}", BatchLinks[i].Item1, BatchLinks[i].Item2, BatchLinks[i].Item3));
                    for (int s = 0; s < Prob.Length; s++)
                    {
                        Writer.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}", s, Prob[s].Output.Data.MemPtr[i], BatchResults[s][i].Item1, BatchResults[s][i].Item2));

                        string topK = string.Join(",", BatchResults[s][i].Item3);
                        Writer.WriteLine(topK);

                        string vecs = string.Join(",", Status[s].Output.Data.MemPtr.Skip(i * Status[s].Dim).Take(Status[s].Dim));
                        Writer.WriteLine(vecs);
                    }
                    Writer.WriteLine();    
                }
            }

            public override void Init()
            {
                Writer = new StreamWriter(FileName);
            }

            public override void Complete()
            {
                Writer.Close();
            }

        }

        class SingleStatusDumpRunner : StructRunner
        {
            List<Tuple<int, int, int>> BatchLinks { get; set; }
            HiddenBatchData Status { get; set; }
            string FileName { get; set; }
            StreamWriter Writer { get; set; }
            public SingleStatusDumpRunner(List<Tuple<int, int, int>> batchLinks, HiddenBatchData status, string fileName, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                BatchLinks = batchLinks;
                Status = status;
                FileName = fileName;
            }

            public override void Forward()
            {
               Status.Output.Data.SyncToCPU();

                for (int i = 0; i < BatchLinks.Count; i++)
                {
                    Writer.WriteLine(string.Format("{0}\t{1}\t{2}", BatchLinks[i].Item1, BatchLinks[i].Item2, BatchLinks[i].Item3));
                    string vecs = string.Join(",", Status.Output.Data.MemPtr.Skip(i * Status.Dim).Take(Status.Dim));
                    Writer.WriteLine(vecs);
                    Writer.WriteLine();
                }
            }

            public override void Init()
            {
                Writer = new StreamWriter(FileName);
            }

            public override void Complete()
            {
                Writer.Close();
            }

        }

        public static ComputationGraph BuildComputationGraphV6(List<Tuple<int, int, int>> graph,
                                                               CompositeNNStructure modelStructure, bool createNew,
                                                               string preScore,
                                                               RunnerBehavior Behavior)
        {
            // word embedding layer.
            EmbedStructure InNodeEmbed = null;
            EmbedStructure LinkEmbed = null;
            EmbedStructure OutNodeEmbed = null;

            LayerStructure AnsStruct = null;
            MatrixStructure Memory = null;
            GRUCell StateStruct = null;
            MLPAttentionStructure AttStruct = null;
            List<LayerStructure> TerminalStruct = new List<LayerStructure>();

            if (createNew)
            {
                if (BuilderParameters.SEED_MODEL.Equals(string.Empty))
                {
                    InNodeEmbed = new EmbedStructure(DataPanel.EntityNum, BuilderParameters.IN_EmbedDim, DeviceType.CPU_FAST_VECTOR);
                    LinkEmbed = new EmbedStructure(DataPanel.RelationNum * 2, BuilderParameters.R_EmbedDim, DeviceType.CPU_FAST_VECTOR);

                    int StatusDim = BuilderParameters.IN_EmbedDim + BuilderParameters.R_EmbedDim;
                    int MemDim = BuilderParameters.MEM_EmbedDim; // StatusDim;
                    int memHid = BuilderParameters.ATT_DIM;
                    int OutDim = BuilderParameters.OUT_EmbedDim;
                    OutNodeEmbed = new EmbedStructure(DataPanel.EntityNum, OutDim, DeviceType.CPU_FAST_VECTOR);

                    AnsStruct = new LayerStructure(StatusDim, OutDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false, Behavior.Device);
                    Memory = new MatrixStructure(MemDim, BuilderParameters.MemorySize, Behavior.Device);
                    StateStruct = new GRUCell(MemDim, StatusDim, Behavior.Device);
                    AttStruct = new MLPAttentionStructure(StatusDim, MemDim, memHid, Behavior.Device);

                    int input = StatusDim;
                    for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
                    {
                        TerminalStruct.Add(new LayerStructure(input, BuilderParameters.T_NET[i], i == BuilderParameters.T_NET.Length - 1 ? A_Func.Linear : BuilderParameters.T_AF, N_Type.Fully_Connected, 1, 0, false, Behavior.Device));
                        input = BuilderParameters.T_NET[i];
                    }

                    //TerminalStruct = new LayerStructure(StatusDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, Behavior.Device);

                    modelStructure.AddLayer(InNodeEmbed);
                    modelStructure.AddLayer(LinkEmbed);
                    modelStructure.AddLayer(OutNodeEmbed);

                    modelStructure.AddLayer(AnsStruct);
                    modelStructure.AddLayer(Memory);
                    modelStructure.AddLayer(StateStruct);
                    modelStructure.AddLayer(AttStruct);
                    for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
                    {
                        modelStructure.AddLayer(TerminalStruct[i]);
                    }
                }
                else
                {
                    using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                    {
                        int modelNum = CompositeNNStructure.DeserializeModelCount(modelReader);

                        InNodeEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);
                        LinkEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);
                        OutNodeEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);

                        AnsStruct = (LayerStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        Memory = (MatrixStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        StateStruct = (GRUCell)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        AttStruct = (MLPAttentionStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);

                        for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
                        {
                            TerminalStruct.Add((LayerStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device));
                        }
                    }
                }
            }
            else
            {
                int link = 0;
                InNodeEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];
                LinkEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];
                OutNodeEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];

                AnsStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                Memory = (MatrixStructure)modelStructure.CompositeLinks[link++];
                StateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                AttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
                {
                    TerminalStruct.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                }
            }

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding,  OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //InNodeEmbed.BiasOptimizer = new SGDOptimizer(InNodeEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                LinkEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(LinkEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //LinkEmbed.BiasOptimizer = new SGDOptimizer(LinkEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                OutNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(OutNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //OutNodeEmbed.BiasOptimizer = new SGDOptimizer(OutNodeEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer, Behavior);
            }

            ComputationGraph cg = new ComputationGraph();
            SampleEmbedV2Runner SmpRunner = new SampleEmbedV2Runner(graph, InNodeEmbed, LinkEmbed,
                        Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.MiniBatchSize : BuilderParameters.TestMiniBatchSize, Behavior);
            cg.AddDataRunner(SmpRunner);

            //IntArgument arg = new IntArgument("arg");
            //cg.AddRunner(new HiddenDataBatchSizeRunner(SmpRunner.Status, arg, Behavior));

            //BiMatchBatchData MemMatchData = (BiMatchBatchData)cg.AddRunner(new RepeatBiMatchRunner(SmpRunner.Status.MAX_BATCHSIZE, Memory.Size, arg, Behavior));

            //ReasonNetRunner reasonRunner = new ReasonNetRunner(SmpRunner.Status, BuilderParameters.RECURRENT_STEP,
            //        Memory, MemMatchData, AttStruct,  AnsStruct, StateStruct, TerminalStruct, 10, Behavior);
            //cg.AddRunner(reasonRunner);

            BasicReasoNetRunner reasonet = new BasicReasoNetRunner(SmpRunner.Status, BuilderParameters.RECURRENT_STEP,
                    Memory, AttStruct, BuilderParameters.ATT_GAMMA, BuilderParameters.REWARD_DISCOUNT, StateStruct, TerminalStruct, Behavior, BuilderParameters.AttType); //  MemMatchData,)
            Logger.WriteLog("Attention Type {0}", BuilderParameters.AttType);

            if (BuilderParameters.T_MAX_CLIP > 0) { reasonet.MaxClipTerm = BuilderParameters.T_MAX_CLIP; }
            if (BuilderParameters.T_MIN_CLIP > 0) { reasonet.MinClipTerm = BuilderParameters.T_MIN_CLIP; }

            cg.AddRunner(reasonet);

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    HiddenBatchData[] distOutputs = new HiddenBatchData[BuilderParameters.RECURRENT_STEP];
                    for (int i = 0; i < BuilderParameters.RECURRENT_STEP; i++)
                    {
                        HiddenBatchData o = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, reasonet.StatusData[i], Behavior));
                        EmbedDistanceRunner distRunner = new EmbedDistanceRunner(cg, OutNodeEmbed, o, SmpRunner.TargetIndex, SmpRunner.NTrialNum + 1, DistanceType.L1Dist, Behavior);
                        cg.AddRunner(distRunner);
                        distOutputs[i] = distRunner.Output;
                    }
                    cg.AddObjective(new MultiClassContrastiveRewardRunner(CudaPieceFloat.Empty, distOutputs, reasonet.AnsProb, BuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    HiddenBatchData[] pred_distOutputs = new HiddenBatchData[BuilderParameters.RECURRENT_STEP];
                    //HiddenBatchData finalOutput = new HiddenBatchData(pred_distOutputs[0].MAX_BATCHSIZE, pred_distOutputs[0].Dim, Behavior.RunMode, Behavior.Device);
                    for (int i = 0; i < BuilderParameters.RECURRENT_STEP; i++)
                    {
                        HiddenBatchData o = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, reasonet.StatusData[i], Behavior));
                        EmbedDistanceRunner distRunner = new EmbedDistanceRunner(cg, OutNodeEmbed, o, null, 0, DistanceType.L1Dist, Behavior);
                        cg.AddRunner(distRunner);
                        pred_distOutputs[i] = distRunner.Output;

                        //MatrixMultiplicationRunner runner = new MatrixProductRunner()
                        //cg.AddRunner(new AdditionMatrixRunner())
                    }
                    HiddenBatchData finalOutput = (HiddenBatchData)cg.AddRunner(new WAdditionMatrixRunner(pred_distOutputs.ToList(), reasonet.AnsProb.ToList(), Behavior));
                    cg.AddRunner(new KGCPredictionRunner(SmpRunner.Label, SmpRunner.BatchLinks, finalOutput, //pred_distOutputs, reasonet.AnsProb,
                        BuilderParameters.Gamma < 0 ? true : false, preScore, Behavior));
                    //EmbedDistanceRunner ansRunner = new EmbedDistanceRunner(cg, OutNodeEmbed, reasonet.FinalAns, null, 0, DistanceType.L1Dist, Behavior);
                    //cg.AddRunner(ansRunner);
                    //cg.AddRunner(new KGCPredictionRunner(SmpRunner.Label, SmpRunner.BatchLinks, ansRunner.Output, true, preScore, Behavior));
                    break;
                case DNNRunMode.GradientCheck:

                    List<Tuple<int, int, List<int>>>[] Results = new List<Tuple<int, int, List<int>>>[BuilderParameters.RECURRENT_STEP];
                    for (int i = 0; i < BuilderParameters.RECURRENT_STEP; i++)
                    {
                        HiddenBatchData o = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, reasonet.StatusData[i], Behavior));

                        EmbedDistanceRunner resultRunner = new EmbedDistanceRunner(cg, OutNodeEmbed, o, null, 0, DistanceType.L1Dist, Behavior);
                        cg.AddRunner(resultRunner);

                        TopKPredictionRunner topKRunner = new TopKPredictionRunner(SmpRunner.Label, SmpRunner.BatchLinks, resultRunner.Output, //pred_distOutputs, reasonet.AnsProb,
                            BuilderParameters.Gamma < 0 ? true : false, Behavior);
                        cg.AddRunner(topKRunner);

                        Results[i] = topKRunner.BatchResults;
                    }

                    StatusDumpRunner runner = new StatusDumpRunner(SmpRunner.BatchLinks, Results, reasonet.AnsProb, reasonet.StatusData, preScore + ".dump", Behavior);
                    cg.AddRunner(runner);
                    break;
                //case DNNRunMode.GradientCheck:
                //    HiddenBatchData[] CheckdistOutputs = new HiddenBatchData[BuilderParameters.RECURRENT_STEP];
                //    for (int i = 0; i < BuilderParameters.RECURRENT_STEP; i++)
                //    {
                //        EmbedDistanceRunner distRunner = new EmbedDistanceRunner(cg, OutNodeEmbed, reasonRunner.AnsData[i], null, 0, DistanceType.L1Dist, Behavior);
                //        cg.AddRunner(distRunner);
                //        CheckdistOutputs[i] = distRunner.Output;
                //    }
                //    cg.AddRunner(new EnsembleKGCPredictionRunner(SmpRunner.Label, SmpRunner.BatchLinks, CheckdistOutputs, reasonRunner.AnswerProb, reasonRunner.MemRetrievalRunner.Select(t => t.SoftmaxAddress).ToArray(), true, preScore, Behavior));
                //    break;
            }
            cg.SetDelegateModel(modelStructure);

            return cg;
        }

        public static ComputationGraph BuildEmbeddingDump(List<Tuple<int, int, int>> graph,
                                                          CompositeNNStructure modelStructure, bool createNew,
                                                          string preScore,
                                                          RunnerBehavior Behavior)
        {
            // word embedding layer.
            EmbedStructure InNodeEmbed = null;
            EmbedStructure LinkEmbed = null;
            EmbedStructure OutNodeEmbed = null;

            LayerStructure AnsStruct = null;
            MatrixStructure Memory = null;
            GRUCell StateStruct = null;
            MLPAttentionStructure AttStruct = null;
            List<LayerStructure> TerminalStruct = new List<LayerStructure>();

            if (createNew)
            {
                if (BuilderParameters.SEED_MODEL.Equals(string.Empty))
                {
                    InNodeEmbed = new EmbedStructure(DataPanel.EntityNum, BuilderParameters.IN_EmbedDim, DeviceType.CPU_FAST_VECTOR);
                    LinkEmbed = new EmbedStructure(DataPanel.RelationNum * 2, BuilderParameters.R_EmbedDim, DeviceType.CPU_FAST_VECTOR);

                    int StatusDim = BuilderParameters.IN_EmbedDim + BuilderParameters.R_EmbedDim;
                    int MemDim = BuilderParameters.MEM_EmbedDim; // StatusDim;
                    int memHid = BuilderParameters.ATT_DIM;
                    int OutDim = BuilderParameters.OUT_EmbedDim;
                    OutNodeEmbed = new EmbedStructure(DataPanel.EntityNum, OutDim, DeviceType.CPU_FAST_VECTOR);

                    AnsStruct = new LayerStructure(StatusDim, OutDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false, Behavior.Device);
                    Memory = new MatrixStructure(MemDim, BuilderParameters.MemorySize, Behavior.Device);
                    StateStruct = new GRUCell(MemDim, StatusDim, Behavior.Device);
                    AttStruct = new MLPAttentionStructure(StatusDim, MemDim, memHid, Behavior.Device);

                    int input = StatusDim;
                    for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
                    {
                        TerminalStruct.Add(new LayerStructure(input, BuilderParameters.T_NET[i], i == BuilderParameters.T_NET.Length - 1 ? A_Func.Linear : BuilderParameters.T_AF, N_Type.Fully_Connected, 1, 0, false, Behavior.Device));
                        input = BuilderParameters.T_NET[i];
                    }

                    //TerminalStruct = new LayerStructure(StatusDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, Behavior.Device);

                    modelStructure.AddLayer(InNodeEmbed);
                    modelStructure.AddLayer(LinkEmbed);
                    modelStructure.AddLayer(OutNodeEmbed);

                    modelStructure.AddLayer(AnsStruct);
                    modelStructure.AddLayer(Memory);
                    modelStructure.AddLayer(StateStruct);
                    modelStructure.AddLayer(AttStruct);
                    for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
                    {
                        modelStructure.AddLayer(TerminalStruct[i]);
                    }
                }
                else
                {
                    using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                    {
                        int modelNum = CompositeNNStructure.DeserializeModelCount(modelReader);

                        InNodeEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);
                        LinkEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);
                        OutNodeEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);

                        AnsStruct = (LayerStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        Memory = (MatrixStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        StateStruct = (GRUCell)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        AttStruct = (MLPAttentionStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);

                        for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
                        {
                            TerminalStruct.Add((LayerStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device));
                        }
                    }
                }
            }
            else
            {
                int link = 0;
                InNodeEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];
                LinkEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];
                OutNodeEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];

                AnsStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                Memory = (MatrixStructure)modelStructure.CompositeLinks[link++];
                StateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                AttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                for (int i = 0; i < BuilderParameters.T_NET.Length; i++)
                {
                    TerminalStruct.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                }
            }

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //InNodeEmbed.BiasOptimizer = new SGDOptimizer(InNodeEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                LinkEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(LinkEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //LinkEmbed.BiasOptimizer = new SGDOptimizer(LinkEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                OutNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(OutNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //OutNodeEmbed.BiasOptimizer = new SGDOptimizer(OutNodeEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer, Behavior);
            }

            ComputationGraph cg = new ComputationGraph();
            SampleEmbedV2Runner SmpRunner = new SampleEmbedV2Runner(graph, InNodeEmbed, LinkEmbed,
                        Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.MiniBatchSize : BuilderParameters.TestMiniBatchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            SingleStatusDumpRunner runner = new SingleStatusDumpRunner(SmpRunner.BatchLinks, SmpRunner.Status, preScore + ".dump", Behavior);
            cg.AddRunner(runner);
            cg.SetDelegateModel(modelStructure);
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure DeepModel = new CompositeNNStructure();

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Train, DeepModel, true, string.Empty,
                                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    
                    ComputationGraph testCG = null;
                    if (BuilderParameters.IsTestFile)
                        testCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Test, DeepModel, false, BuilderParameters.SCORE_PATH + ".test",
                                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Valid, DeepModel, false, BuilderParameters.SCORE_PATH + ".valid",
                                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    double bestValidScore = double.MinValue;
                    double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                                               
                        double testScore = 0;
                        double validScore = 0;

                        if ((iter + 1) % BuilderParameters.PRED_ITER == 0)
                        {
                            if (testCG != null)
                            {
                                testScore = testCG.Execute();
                                Logger.WriteLog("Test Score {0}, At iter {1}", testScore, bestIter);
                            }
                            if (validCG != null)
                            {
                                validScore = validCG.Execute();
                                Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, bestIter);
                            }

                            if (validScore > bestValidScore)
                            {
                                bestValidScore = validScore; bestTestScore = testScore; bestIter = iter;
                                //using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "RecurrentAttention.best.model"), FileMode.Create, FileAccess.Write)))
                                //{
                                //    modelStructure.Serialize(writer);
                                //}
                            }
                            Logger.WriteLog("Best Valid/Test Score {0}, {1}, At iter {2}", bestValidScore, bestTestScore, bestIter);

                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, 
                                string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                DeepModel.Serialize(writer);
                            }

                        }
                        else if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath,
                               string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                DeepModel.Serialize(writer);
                            }
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    ComputationGraph predCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Test, DeepModel, true, BuilderParameters.SCORE_PATH + ".test",
                                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    predCG.Execute();
                    break;
                case DNNRunMode.GradientCheck:
                    ComputationGraph checkCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Test, DeepModel, true, BuilderParameters.SCORE_PATH + ".check",
                                    new RunnerBehavior() { RunMode = DNNRunMode.GradientCheck, Device = device, Computelib = computeLib });
                    checkCG.Execute();
                    break;
                case DNNRunMode.ExecuteOp1:
                    ComputationGraph op1 = BuildEmbeddingDump(DataPanel.knowledgeGraph.Test, DeepModel, true, BuilderParameters.SCORE_PATH + ".check",
                                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    op1.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static RelationGraphData knowledgeGraph;

            public static int EntityNum { get { return knowledgeGraph.EntityId.Count; } }
            public static int RelationNum { get { return knowledgeGraph.RelationId.Count; } }

            public static List<Tuple<int, int, int>> Train
            {
                get { return knowledgeGraph.Train; }
            }

            public static List<Tuple<int, int, int>> Test
            {
                get { return knowledgeGraph.Test; }
            }

            public static List<Tuple<int, int, int>> Valid
            {
                get { return knowledgeGraph.Valid; }
            }

            public static double[] HeadProb { get { return knowledgeGraph.HeadProb; } }

            public static double[] TailProb { get { return knowledgeGraph.TailProb; } }

            public static bool IsInGraph(int eid1, int eid2, int rid)
            {
                if (rid < RelationNum && knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
                else if(rid >= RelationNum && knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid2, eid1, rid - RelationNum))) return true;
                else return false;
            }

            public static bool IsInTrainGraph(int eid1, int eid2, int rid)
            {
                if (rid < RelationNum && knowledgeGraph.TrainGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
                else if (rid >= RelationNum && knowledgeGraph.TrainGraphHash.Contains(string.Format("{0}#{1}#{2}", eid2, eid1, rid - RelationNum))) return true;
                else return false;
            }

            public static void Init()
            {
                knowledgeGraph = new RelationGraphData();

                knowledgeGraph.EntityId = RelationGraphData.LoadMapId(BuilderParameters.Entity2Id);
                knowledgeGraph.RelationId = RelationGraphData.LoadMapId(BuilderParameters.Relation2Id);

                knowledgeGraph.Train = knowledgeGraph.LoadGraph(BuilderParameters.TrainData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, true);
                if (!BuilderParameters.ValidData.Equals(string.Empty))
                {
                    knowledgeGraph.Valid = knowledgeGraph.LoadGraph(BuilderParameters.ValidData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
                }

                if (!BuilderParameters.TestData.Equals(string.Empty))
                {
                    knowledgeGraph.Test = knowledgeGraph.LoadGraph(BuilderParameters.TestData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
                }
            }
        }
    }
}
