﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppDSSMPredictBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static string Model { get { return Argument["MODEL"].Value; } }
            public static DNNModelVersion MODEL_VERSION { get { return (DNNModelVersion)int.Parse(Argument["MODEL-VERSION"].Value); } }
            public static string L3GVocab { get { return Argument["L3G-VOCAB"].Value; } }
            public static string Input { get { return Argument["INPUT"].Value; } }
            public static int DEVICEID { get { return int.Parse(Argument["DEVICE"].Value); } }
            public static string EMBED_FILE { get { return Argument["EMBED-FILE"].Value; } }
            static BuilderParameters()
            {
                Argument.Add("MODEL", new ParameterArgument(string.Empty, "Model."));
                Argument.Add("MODEL-VERSION", new ParameterArgument(((int)DNNModelVersion.C_OR_DSSM_GPU_V4_2015_APR).ToString(), ParameterUtil.EnumValues(typeof(DNNModelVersion))));
                Argument.Add("DEVICE", new ParameterArgument("0", "Device ID."));
                Argument.Add("L3G-VOCAB", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("INPUT", new ParameterArgument(string.Empty, "intput Path"));
                Argument.Add("EMBED-FILE", new ParameterArgument(string.Empty, "Indicates whether embedding is outputted"));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_DSSM_PREDICT; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> data,
                                                             RunnerBehavior Behavior, DNNStructure model)
        {
            ComputationGraph cg = new ComputationGraph();
            SeqSparseBatchData src = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(data, Behavior)); 
            /// embedding for source text.
            HiddenBatchData srcOutput = (HiddenBatchData)cg.AddRunner(new DNNRunner<SeqSparseBatchData>(model, src, Behavior));
            // Dump Embedding Vector.
            if (!BuilderParameters.EMBED_FILE.Equals(string.Empty)) cg.AddRunner(new VectorDumpRunner(srcOutput.Output, BuilderParameters.EMBED_FILE, true));
            return cg;
        }

        public override void Rock()
        {
            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.DEVICEID);
            IMathOperationManager Computelib = MathOperatorManager.CreateInstance(device);

            Logger.OpenLog(BuilderParameters.LogFile);
            
            Logger.WriteLog("Loading Training/Validation Data.");

            L3GTextHash hash = new L3GTextHash(BuilderParameters.L3GVocab);

            MemoryStream mem = CommonExtractor.ExtractSeqSparseDataBinary(
                                    CommonExtractor.ExtractSeqTextFea(
                                            CommonExtractor.StreamRead(new StreamReader(BuilderParameters.Input), 0), hash, false), hash.VocabSize, 1024);
            BinaryReader reader = new BinaryReader(mem);

            DataCashier<SeqSparseBatchData, SequenceDataStat> data = new DataCashier<SeqSparseBatchData, SequenceDataStat>(reader);
            data.InitThreadSafePipelineCashier(100, false);
            Logger.WriteLog("Load Data Finished.");

            DNNStructure SrcDnn = null;
            if (! BuilderParameters.Model.Equals(string.Empty))
            {
                SrcDnn = new DNNStructure(FileUtil.CreateReadFS(BuilderParameters.Model), BuilderParameters.MODEL_VERSION, device);
                Logger.WriteLog("finish loading model at :{0}", DateTime.Now);
                Logger.WriteLog(SrcDnn.DNN_Descr());
            }

            ComputationGraph predCG = BuildComputationGraph(data,new RunnerBehavior()
                { Computelib = Computelib, Device = device, RunMode = DNNRunMode.Predict }, SrcDnn);
            double score = predCG.Execute();
            Logger.CloseLog();
        }
    }
}
