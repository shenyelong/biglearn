﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppReasoNetReaderSQuADV11Builder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Training Folder"));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Testing Folder"));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Validation Folder"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("IS-EMBED-UPDATE", new ParameterArgument("0", "0 : Fix embedding; 1 : Update Embedding."));
                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300,300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-WIN", new ParameterArgument("5,5", "DNN Layer WindowSize"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument("1,1", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("EMBED-LAYER-DROPOUT", new ParameterArgument("0,0", "DNN Layer Dropout."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding File"));
                Argument.Add("UNK-INIT", new ParameterArgument("0", "0 : zero initialize; 1 : random initialize"));

                Argument.Add("L3G-EMBED-DIM", new ParameterArgument("0", "L3G Embedding Dim."));
                Argument.Add("L3G-EMBED-WIN", new ParameterArgument("3", "L3G Embedding Win."));

                Argument.Add("CHAR-EMBED-DIM", new ParameterArgument("0", "Char Embedding Dim."));
                Argument.Add("CHAR-EMBED-WIN", new ParameterArgument("5", "Char Embedding Win."));

                Argument.Add("HIGHWAY-LAYER", new ParameterArgument("2", "Highway Layers."));
                Argument.Add("HIGHWAY-SHARE", new ParameterArgument("0", "0: Nonshare highway; 1 : share highway;"));

                Argument.Add("GRU-DIM", new ParameterArgument("300", "GRU Dimension."));
                Argument.Add("CNN-DIM", new ParameterArgument("0", "CNN Dimension."));
                Argument.Add("CNN-WIN", new ParameterArgument("5", "CNN Window Size."));
                Argument.Add("IS-SHARE-RNN", new ParameterArgument("0", "0 : non-share; 1 : share"));
                Argument.Add("MEMORY-ENSEMBLE", new ParameterArgument("2", "2 : two-way ensemble; 3 : three-way ensemble;"));

                //public static float Soft1Dropout { get { return int.Parse(Argument["SOFT1-DROPOUT"].Value); } }
                //public static float Soft2Dropout { get { return int.Parse(Argument["SOFT2-DROPOUT"].Value); } }
                Argument.Add("SOFT1-DROPOUT", new ParameterArgument("0", "Soft 1 Dropout;"));
                Argument.Add("SOFT2-DROPOUT", new ParameterArgument("0", "Soft 2 Dropout;"));
                Argument.Add("LSTM-DROPOUT", new ParameterArgument("0", "LSTM Embed Dropout;"));
                Argument.Add("EMBED-DROPOUT", new ParameterArgument("0", "Embeding Dropout;"));
                Argument.Add("COATT-DROPOUT", new ParameterArgument("0", "CoATT Dropout;"));

                Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                Argument.Add("ATT-MEM", new ParameterArgument("0", "0 : LSTM; 1 : CoAtt LSTM; 2 : LSTM + CoAtt LSTM; 3 : CoAttOAtt LSTM; 4 : LSTM + CoAttOAtt"));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.RL)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));
                Argument.Add("QUERY-POOL", new ParameterArgument((((int)PoolingType.LAST)).ToString(), "Query Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));

                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
                Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("RL-ALL", new ParameterArgument("1", "RL All"));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));

                Argument.Add("SPAN-OBJECTIVE", new ParameterArgument("0", "Span Objective; 0 : start-end detection; 1 : span start-end detection."));
                Argument.Add("SPAN-LENGTH", new ParameterArgument("0", "Span length"));
                Argument.Add("TRAIN-SPAN-LENGTH", new ParameterArgument("10000", "Span length"));
                Argument.Add("DEBUG-MODE", new ParameterArgument("0", "0: exact compute Model; 1: atomicAdd Model; "));

                Argument.Add("ENDPOINT-CONFIG", new ParameterArgument("0", "0: old M2; 1: new M2;"));
                Argument.Add("STARTPOINT-CONFIG", new ParameterArgument("0", "0: old M1; 1: new M1; 2: all new M1;"));

                //TERMINATE-DISCOUNT
                Argument.Add("TERMINATE-DISCOUNT", new ParameterArgument("0.1", "Reward discount on terminate gate;"));
            }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static string Vocab { get { return Argument["VOCAB"].Value; } }

            #region train, test, validation dataset.
            public static string Train { get { return Argument["TRAIN"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTrainFile { get { return (!Train.Equals(string.Empty)); } }
            public static string TrainContext { get { return string.Format("{0}.para", Train); } }
            public static string TrainQuery { get { return string.Format("{0}.ques", Train); } }
            public static string TrainPos { get { return string.Format("{0}.pos", Train); } }

            public static string TrainContextBin { get { return string.Format("{0}.{1}.bin", TrainContext, MiniBatchSize); } }
            public static string TrainQueryBin { get { return string.Format("{0}.{1}.bin", TrainQuery, MiniBatchSize); } }
            public static string TrainPosBin { get { return string.Format("{0}.{1}.bin", TrainPos, MiniBatchSize); } }

            public static string TestFolder { get { return Argument["TEST"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsTestFile { get { return (!TestFolder.Equals(string.Empty)); } }

            //public static string TestContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", TestFolder, VocabSize, MiniBatchSize); } }
            //public static string TestQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", TestFolder, VocabSize, MiniBatchSize); } }
            //public static string TestAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", TestFolder, VocabSize, MiniBatchSize); } }

            public static string Valid { get { return Argument["VALID"].Value.TrimEnd(new char[] { '\\', '/' }); } }
            public static bool IsValidFile { get { return (!Valid.Equals(string.Empty)); } }
            public static string ValidContext { get { return string.Format("{0}.para", Valid); } }
            public static string ValidQuery { get { return string.Format("{0}.ques", Valid); } }
            public static string ValidPos { get { return string.Format("{0}.pos", Valid); } }
            public static string ValidIds { get { return string.Format("{0}.ids", Valid); } }
            public static string ValidSpan { get { return string.Format("{0}.spans", Valid); } }


            public static string ValidContextBin { get { return string.Format("{0}.{1}.bin", ValidContext, MiniBatchSize); } }
            public static string ValidQueryBin { get { return string.Format("{0}.{1}.bin", ValidQuery, MiniBatchSize); } }
            public static string ValidPosBin { get { return string.Format("{0}.{1}.bin", ValidPos, MiniBatchSize); } }

            //public static string ValidIndexData { get { return string.Format("{0}.{1}.idx.tsv", ValidFolder, VocabSize); } }
            //public static string ValidContextBinary { get { return string.Format("{0}.{1}.{2}.cox.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            //public static string ValidQueryBinary { get { return string.Format("{0}.{1}.{2}.qry.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            //public static string ValidAnswerBinary { get { return string.Format("{0}.{1}.{2}.anw.bin", ValidFolder, VocabSize, MiniBatchSize); } }
            #endregion.

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static bool IS_EMBED_UPDATE { get { return int.Parse(Argument["IS-EMBED-UPDATE"].Value) > 0; } }
            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            public static int[] EMBED_LAYER_WIN { get { return Argument["EMBED-LAYER-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static float[] EMBED_DROPOUT
            {
                get
                {
                    if (Argument["EMBED-LAYER-DROPOUT"].Value.Equals(string.Empty)) return EMBED_LAYER_DIM.Select(i => 0.0f).ToArray();
                    else return Argument["EMBED-LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray();
                }
            }
            public static string InitWordEmbedding { get { return Argument["WORD-EMBEDDING"].Value; } }
            public static int UNK_INIT { get { return int.Parse(Argument["UNK-INIT"].Value); } }

            public static int L3G_EMBED_DIM { get { return int.Parse(Argument["L3G-EMBED-DIM"].Value); } }
            public static int L3G_EMBED_WIN { get { return int.Parse(Argument["L3G-EMBED-WIN"].Value); } }

            public static int[] CHAR_EMBED_DIM { get { return Argument["CHAR-EMBED-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static int[] CHAR_EMBED_WIN { get { return Argument["CHAR-EMBED-WIN"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int HighWay_Layer { get { return int.Parse(Argument["HIGHWAY-LAYER"].Value); } }
            public static bool HighWay_Share { get { return int.Parse(Argument["HIGHWAY-SHARE"].Value) > 0; } }

            public static int GRU_DIM { get { return int.Parse(Argument["GRU-DIM"].Value); } }
            public static int CNN_DIM { get { return int.Parse(Argument["CNN-DIM"].Value); } }
            public static int CNN_WIN { get { return int.Parse(Argument["CNN-WIN"].Value); } }
            public static bool IS_SHARE_RNN { get { return int.Parse(Argument["IS-SHARE-RNN"].Value) > 0; } }
            public static int MEMORY_ENSEMBLE { get { return int.Parse(Argument["MEMORY-ENSEMBLE"].Value); } }

            public static float Soft1Dropout { get { return float.Parse(Argument["SOFT1-DROPOUT"].Value); } }
            public static float Soft2Dropout { get { return float.Parse(Argument["SOFT2-DROPOUT"].Value); } }
            public static float LSTMDropout { get { return float.Parse(Argument["LSTM-DROPOUT"].Value); } }
            public static float CoAttDropout { get { return float.Parse(Argument["COATT-DROPOUT"].Value); } }
            public static float EMBEDDropout { get { return float.Parse(Argument["EMBED-DROPOUT"].Value); } }

            //public static bool IS_ENTITY_LSTM { get { return int.Parse(Argument["IS-ENTITY-LSTM"].Value) > 0; } }
            public static Recurrent_Unit Recurrent_Gate_Type { get { return (Recurrent_Unit)int.Parse(Argument["RECURRENT-GATE-TYPE"].Value); } }

            public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }
            public static CrossSimType AttType { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }
            public static int ATT_MEM { get { return int.Parse(Argument["ATT-MEM"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static PoolingType RECURRENT_POOL { get { return (PoolingType)int.Parse(Argument["RECURRENT-POOL"].Value); } }
            public static PoolingType QUERY_POOL { get { return (PoolingType)int.Parse(Argument["QUERY-POOL"].Value); } }
            public static bool RL_ALL { get { return int.Parse(Argument["RL-ALL"].Value) > 0; } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
            public static bool IS_NORM_REWARD { get { return int.Parse(Argument["IS-NORM-REWARD"].Value) > 0; } }
            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

            public static string DebugFile { get { return Argument["DEBUG-FILE"].Value; } }
            public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }

            public static int SPAN_LENGTH { get { return int.Parse(Argument["SPAN-LENGTH"].Value); } }
            public static int TRAIN_SPAN_LENGTH { get { return int.Parse(Argument["TRAIN-SPAN-LENGTH"].Value); } }
            public static int SPAN_OBJECTIVE { get { return int.Parse(Argument["SPAN-OBJECTIVE"].Value); } }
            public static int DEBUG_MODE { get { return int.Parse(Argument["DEBUG-MODE"].Value); } }

            public static int ENDPOINT_CONFIG { get { return int.Parse(Argument["ENDPOINT-CONFIG"].Value); } }
            public static int STARTPOINT_CONFIG { get { return int.Parse(Argument["STARTPOINT-CONFIG"].Value); } }

            public static float TERMINATE_DISCOUNT { get { return float.Parse(Argument["TERMINATE-DISCOUNT"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_SQUAD_READER_V11; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        enum PredType { RL_MAXITER, RL_AVGSIM, RL_AVGPROB, RL_ENSEMBLE3 }

        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class MemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }
            public HiddenBatchData SoftmaxAddress { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }
            CudaPieceInt Tgt2Src;
            CudaPieceFloat Tgt2SrcSoftmax;

            //CudaPieceFloat tmpMem;
            //CudaPieceFloat tmpMem2;
            public MemoryRetrievalRunner(HiddenBatchData address, SeqDenseBatchData memory, BiMatchBatchData matchData, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                MatchData = matchData;
                Gamma = gamma;
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(MatchData.Stat.MAX_SRC_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
                Tgt2Src = new CudaPieceInt(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);
                Tgt2SrcSoftmax = new CudaPieceFloat(MatchData.Stat.MAX_MATCH_BATCHSIZE, true, Behavior.Device == DeviceType.GPU);


                //tmpMem = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
                //tmpMem2 = new CudaPieceFloat(Memory.SentDeriv.Size, true, true);
            }

            public override void Forward()
            {
                Output.BatchSize = MatchData.SrcSize;

                // softmax 
                // softmax of attention
                ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);

                ComputeLib.ColumnWiseSumMask(Memory.SentOutput, 0, MatchData.TgtIdx, 0,
                    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                    Memory.SentSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {

                //tmpMem.CopyFrom(Memory.SentDeriv);
                //tmpMem.Zero();


                // outputDeriv -> memoryContentDeriv.

                //tmpMem2.CopyFrom(Memory.SentDeriv);
                //if (BuilderParameters.DEBUG_MODE == 0)
                {
                    SoftmaxAddress.Output.Data.SyncToCPU(MatchData.MatchSize);
                    for (int i = 0; i < MatchData.MatchSize; i++)
                    {
                        int matchIdx = MatchData.Tgt2MatchElement.MemPtr[i];
                        Tgt2Src.MemPtr[i] = MatchData.SrcIdx.MemPtr[matchIdx];
                        Tgt2SrcSoftmax.MemPtr[i] = SoftmaxAddress.Output.Data.MemPtr[matchIdx];
                    }
                    Tgt2Src.SyncFromCPU(MatchData.MatchSize);
                    Tgt2SrcSoftmax.SyncFromCPU(MatchData.MatchSize);
                    ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, Tgt2Src, 0, Tgt2SrcSoftmax, // SoftmaxAddress.Output.Data,
                                                 MatchData.Tgt2MatchIdx, MatchData.TgtSize, Memory.SentDeriv /*Memory.SentDeriv*/, 0, CudaPieceInt.Empty, 0,
                                                 MatchData.MatchSize, Memory.Dim, 1, 1);
                    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem, Memory.SentSize, Memory.Dim, 1);
                }
                //else if (BuilderParameters.DEBUG_MODE == 1)
                //{
                //    //tmpMem2.Zero();
                //    ComputeLib.AccurateScale_Matrix(Output.Deriv.Data, 0, MatchData.SrcIdx, 0,
                //                                Memory.SentDeriv, 0, MatchData.TgtIdx, 0,
                //                                Memory.Dim, MatchData.MatchSize, SoftmaxAddress.Output.Data);
                //    //ComputeLib.Matrix_Add(Memory.SentDeriv, tmpMem2, Memory.SentSize, Memory.Dim, 1);
                //}

                //tmpMem.SyncToCPU(Memory.SentSize * Memory.Dim);
                //tmpMem2.SyncToCPU(Memory.SentSize * Memory.Dim);
                //for (int i = 0; i < Memory.SentSize * Memory.Dim; i++)
                //{
                //    if (Math.Abs(tmpMem2.MemPtr[i] - tmpMem.MemPtr[i]) >= 0.000000001)
                //    {
                //        Console.WriteLine("Adopt Debug Model {3},  Error! {0}, {1}, {2}", tmpMem.MemPtr[i], tmpMem2.MemPtr[i], i, BuilderParameters.DEBUG_MODE);
                //    }
                //}
                //ComputeLib.ColumnWiseSumMask(Output.Deriv.Data, 0, )

                // outputDeriv -> AddressDeriv.
                ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, MatchData.MatchSize, 1, 1);
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }
        

        class BatchSizeAssignmentRunner : StructRunner
        {
            SeqDenseBatchData Data;
            IntArgument Arg;
            public BatchSizeAssignmentRunner(SeqDenseBatchData seqData, IntArgument arg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = seqData;
                Arg = arg;
            }
            public override void Forward()
            {
                Arg.Value = Data.BatchSize;
            }
        }

        class SentSizeAssignmentRunner : StructRunner
        {
            SeqDenseBatchData Data;
            IntArgument Arg;
            public SentSizeAssignmentRunner(SeqDenseBatchData seqData, IntArgument arg, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Data = seqData;
                Arg = arg;
            }
            public override void Forward()
            {
                Arg.Value = Data.SentSize;
            }
        }

        /// <summary>
        /// Design a new style reasoning net for span detection.
        /// </summary>
        class ReasonNetV2Runner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            LayerStructure AnsStartStruct { get; set; }
            LayerStructure AnsEndStruct { get; set; }

            SeqDenseBatchData Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }

            LayerStructure InitState = null;
            MLPAttentionStructure AttStruct = null;
            GRUCell GruCell = null;
            LayerStructure StartTermStruct = null;
            LayerStructure EndTermStruct = null;
            string DebugFile { get; set; }

            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }
            float AttGamma { get; set; }
            float AnsGamma { get; set; }

            public HiddenBatchData[] AnsStartData = null;
            public HiddenBatchData[] AnsEndData = null;

            HiddenBatchData[] StartTerminalData = null;
            HiddenBatchData[] EndTerminalData = null;

            /// <summary>
            /// tree probability.
            /// </summary>
            public HiddenBatchData[] StartAnswerProb = null;

            public HiddenBatchData[] EndAnswerProb = null;

            /// <summary>
            /// predict final start Ans.
            /// </summary>
            public HiddenBatchData FinalStartAns = null;

            /// <summary>
            /// predict final end Ans.
            /// </summary>
            public HiddenBatchData FinalEndAns = null;


            public ReasonNetV2Runner(ComputationGraph cg, LayerStructure ansStartStruct, LayerStructure ansEndStruct, int maxIterateNum,
                SeqDenseBatchData mem, BiMatchBatchData matchData,
                LayerStructure initS, MLPAttentionStructure attStruct, CrossSimType attType, GRUCell gruCell, 
                LayerStructure startTermStruct, LayerStructure endTermStruct,
                float attgamma, float ansgamma, RunnerBehavior behavior, string debugFile = "") :
                base(cg, Structure.Empty, null, behavior)
            {
                AnsStartStruct = ansStartStruct;
                AnsEndStruct = ansEndStruct;
                MaxIterationNum = maxIterateNum;

                Memory = mem;
                MatchData = matchData;

                InitState = initS; // draw attention on memory.
                AttStruct = attStruct;
                GruCell = gruCell;

                StartTermStruct = startTermStruct;
                EndTermStruct = endTermStruct;

                AttGamma = attgamma;
                AnsGamma = ansgamma;
                DebugFile = debugFile;

                IntArgument batchSizeArgument = new IntArgument("BatchSize");
                cg.AddRunner(new BatchSizeAssignmentRunner(Memory, batchSizeArgument, behavior));

                IntArgument sentSizeArgument = new IntArgument("SentSize");
                cg.AddRunner(new SentSizeAssignmentRunner(Memory, sentSizeArgument, behavior));

                // Memory Matrix;
                SeqMatrixData MemoryMatrix = new SeqMatrixData(Memory);

                // InitState is between -1 to 1;
                VectorData initWeight = new VectorData(InitState.Neural_In, InitState.weight, InitState.WeightGrad, InitState.DeviceType);
                cg.AddRunner(new ActivationRunner(new MatrixData(initWeight), A_Func.Tanh, Behavior));
                // Generate BatchInitVec;
                MatrixData initState = (MatrixData)cg.AddRunner(new VectorExpansionRunner(initWeight, MemoryMatrix.MaxSegment, batchSizeArgument, Behavior));
                
                // Memory Attention Project.
                MatrixData MemoryAttProject = new MatrixData(AttStruct.HiddenDim, AttStruct.MemoryDim, AttStruct.Wm, AttStruct.WmGrad, AttStruct.DeviceType);

                // Memory Project to Hidden.
                MatrixData MemoryHidden = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(MemoryMatrix, MemoryAttProject, Behavior));

                // Terminate Start Vector.
                VectorData termalStartVec = new VectorData(StartTermStruct.Neural_In, StartTermStruct.weight, StartTermStruct.WeightGrad, StartTermStruct.DeviceType);

                // Terminate End Vector.
                VectorData termalEndVec = new VectorData(EndTermStruct.Neural_In, EndTermStruct.weight, EndTermStruct.WeightGrad, EndTermStruct.DeviceType);

                // For Forward/Backward Propagation.
                {
                    AnsStartData = new HiddenBatchData[MaxIterationNum];
                    AnsEndData = new HiddenBatchData[MaxIterationNum];
                    StartTerminalData = new HiddenBatchData[MaxIterationNum];
                    EndTerminalData = new HiddenBatchData[MaxIterationNum];
                }


                for (int i = 0; i < MaxIterationNum; i++)
                {

                    // Attention Input.
                    HiddenBatchData attVec = (HiddenBatchData)cg.AddRunner(new SoftLinearSimRunner(
                                                new HiddenBatchData(initState),
                                                new SeqDenseBatchData(
                                                    new SequenceDataStat(MemoryMatrix.MaxSegment, MemoryMatrix.MaxRow, MemoryHidden.Column),
                                                    MemoryMatrix.SegmentIdx, MemoryMatrix.SegmentMargin,
                                                    MemoryHidden.Output, MemoryHidden.Deriv, MemoryHidden.DeviceType),
                                                MatchData, attType, AttStruct, Behavior));

                    // Attention Softmax.
                    SeqVectorData softAttVec = (SeqVectorData)cg.AddRunner(
                                                new SeqVecSoftmaxRunner(
                                                new SeqVectorData(MemoryMatrix.MaxLength, MemoryMatrix.MaxSegment, attVec.Output.Data, attVec.Deriv.Data,
                                                MemoryMatrix.SegmentIdx, MemoryMatrix.SegmentMargin, Behavior.Device), AttGamma, Behavior));

                    // Attention Retrieve.
                    MatrixData inputMatrix = (MatrixData)cg.AddRunner(new SeqVecMultiplicationRunner(softAttVec, MemoryMatrix, Behavior));

                    // RNN State.
                    MatrixData newState = new MatrixData((HiddenBatchData)cg.AddRunner(new GRUQueryRunner(GruCell, new HiddenBatchData(initState), new HiddenBatchData(inputMatrix), Behavior)));

                    MatrixData outputState = newState;
                    if(BuilderParameters.Soft1Dropout > 0 && Behavior.RunMode == DNNRunMode.Train)
                    {
                        outputState = (MatrixData)cg.AddRunner(new DropOutRunner(newState, BuilderParameters.Soft1Dropout, Behavior));
                    }

                    // Answer Model.
                    SeqDenseBatchData newMemory = (SeqDenseBatchData)cg.AddRunner(new TwoAdvV2WayEnsembleRunner(
                        new SeqDenseBatchData(MemoryMatrix), new HiddenBatchData(outputState), Behavior)
                    { name = "ensemble_memory" + i.ToString() });

                    // Start Answer.
                    HiddenBatchData startOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStartStruct,
                                                                        new HiddenBatchData(newMemory.MAX_SENTSIZE, newMemory.Dim, newMemory.SentOutput, newMemory.SentDeriv, newMemory.DeviceType),
                                                                        Behavior)
                    { name = "answer_s" + i.ToString() });

                    // End Answer.
                    HiddenBatchData endOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsEndStruct,
                                                                        new HiddenBatchData(newMemory.MAX_SENTSIZE, newMemory.Dim, newMemory.SentOutput, newMemory.SentDeriv, newMemory.DeviceType),
                                                                        Behavior)
                    { name = "answer_e" + i.ToString() });

                    AnsStartData[i] = startOutput;
                    AnsEndData[i] = endOutput;

                    // Terminate Runner.
                    HiddenBatchData startTerminateOutput = new HiddenBatchData((MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(newState, termalStartVec, Behavior) { name = "StartTerminateRunner" }));

                    HiddenBatchData endTerminateOutput = new HiddenBatchData((MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(newState, termalEndVec, Behavior) { name = "EndTerminateRunner" }));

                    StartTerminalData[i] = startTerminateOutput;

                    EndTerminalData[i] = endTerminateOutput;
                    // State Switch.
                    initState = newState;
                }
                {
                    StartAnswerProb = new HiddenBatchData[MaxIterationNum];
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        StartAnswerProb[i] = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                    }

                    EndAnswerProb = new HiddenBatchData[MaxIterationNum];
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        EndAnswerProb[i] = new HiddenBatchData(Memory.Stat.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                    }


                    FinalStartAns = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                    FinalEndAns = new HiddenBatchData(MatchData.Stat.MAX_MATCH_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
                }
            }

            Dictionary<int, int> StartInferenStepStat = new Dictionary<int, int>();
            Dictionary<int, int> EndInferenStepStat = new Dictionary<int, int>();

            /// <summary>
            /// Output Debug File.
            /// </summary>
            public override void Init()
            {
                if (DebugFile != "" && !DebugFile.Equals(string.Empty)) { }

                StartInferenStepStat.Clear();
                EndInferenStepStat.Clear();
            }

            /// <summary>
            /// Save Debug File.
            /// </summary>
            public override void Complete()
            {
                //if (DebugWriter != null)
                //{
                //    DebugWriter.Close();
                //}
                foreach(KeyValuePair<int, int> stat in StartInferenStepStat)
                {
                    Logger.WriteLog("Start Max Inference Step Stat {0}, number {1}", stat.Key, stat.Value);
                }

                foreach (KeyValuePair<int, int> stat in EndInferenStepStat)
                {
                    Logger.WriteLog("End Max Inference Step Stat {0}, number {1}", stat.Key, stat.Value);
                }
            }

            public override void Forward()
            {
                iteration++;
                //foreach (StructRunner runner in SubRunners) { runner.Forward(); }
                if (BuilderParameters.RL_ALL)
                {

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(StartTerminalData[i].Output.Data, 0, StartTerminalData[i].Output.Data, 0, StartTerminalData[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(StartTerminalData[i].Output.Data, StartTerminalData[i].BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
                        StartTerminalData[i].Output.Data.SyncToCPU(StartTerminalData[i].BatchSize);

                        ComputeLib.Logistic(EndTerminalData[i].Output.Data, 0, EndTerminalData[i].Output.Data, 0, EndTerminalData[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(EndTerminalData[i].Output.Data, EndTerminalData[i].BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
                        EndTerminalData[i].Output.Data.SyncToCPU(EndTerminalData[i].BatchSize);


                        StartTerminalData[i].Output.Data.SyncToCPU(StartTerminalData[i].BatchSize);
                        EndTerminalData[i].Output.Data.SyncToCPU(EndTerminalData[i].BatchSize);

                        //float tl1 = TerminalData[i].Output.Data.MemPtr.Take(TerminalData[i].BatchSize).Select(v => Math.Abs(v)).Sum();
                        //float tl2 = TerminalData[i].Output.Data.MemPtr.Take(TerminalData[i].BatchSize).Select(v => v * v).Sum();

                        //float sl1 = AnsStartData[i].Output.Data.MemPtr.Take(AnsStartData[i].BatchSize).Select(v => Math.Abs(v)).Sum();
                        //float sl2 = AnsStartData[i].Output.Data.MemPtr.Take(AnsStartData[i].BatchSize).Select(v => v * v).Sum();

                        //float el1 = AnsEndData[i].Output.Data.MemPtr.Take(AnsEndData[i].BatchSize).Select(v => Math.Abs(v)).Sum();
                        //float el2 = AnsEndData[i].Output.Data.MemPtr.Take(AnsEndData[i].BatchSize).Select(v => v * v).Sum();

                        //Console.WriteLine("Terminate BatchSize {0} AnsStart BatchSize {1} AnsEnd BatchSize {2}", 
                        //    TerminalData[i].BatchSize, AnsStartData[i].BatchSize, AnsEndData[i].BatchSize);

                        //Console.WriteLine("Terminate {0} L1 {1} L2 {2}", i, tl1, tl2);
                        //Console.WriteLine("Start Answer {0} L1 {1} L2 {2}", i, sl1, sl2);
                        //Console.WriteLine("End Answer {0} L1 {1} L2 {2}", i, el1, el2);
                        //Console.WriteLine();
                    }

                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        float acc_log_t_start = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = StartTerminalData[i].Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t_start + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t_start;

                            StartAnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t_start += (float)Math.Log(1 - t_i);
                        }

                        float acc_log_t_end = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = EndTerminalData[i].Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t_end + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t_end;

                            EndAnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t_end += (float)Math.Log(1 - t_i);
                        }
                    }

                }
                else
                {
                    for(int i=0;i<MaxIterationNum;i++)
                    {
                        StartTerminalData[i].Output.Data.SyncToCPU(StartTerminalData[i].BatchSize);
                        EndTerminalData[i].Output.Data.SyncToCPU(EndTerminalData[i].BatchSize);
                    }

                    float[] score = new float[MaxIterationNum];
                    float[] prob = new float[MaxIterationNum];
                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            score[i] = StartTerminalData[i].Output.Data.MemPtr[b];
                        }
                        Util.Softmax(score, prob, 1);
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            StartAnswerProb[i].Output.Data.MemPtr[b] = prob[i];
                        }

                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            score[i] = EndTerminalData[i].Output.Data.MemPtr[b];
                        }
                        Util.Softmax(score, prob, 1);
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            EndAnswerProb[i].Output.Data.MemPtr[b] = prob[i];
                        }

                    }
                }


                for (int i = 0; i < MaxIterationNum; i++)
                {
                    StartAnswerProb[i].BatchSize = MatchData.SrcSize;
                    StartAnswerProb[i].Output.Data.SyncFromCPU(StartAnswerProb[i].BatchSize);

                    EndAnswerProb[i].BatchSize = MatchData.SrcSize;
                    EndAnswerProb[i].Output.Data.SyncFromCPU(EndAnswerProb[i].BatchSize);
                }
                
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    AnsStartData[i].Output.Data.SyncToCPU(AnsStartData[i].BatchSize);
                    AnsEndData[i].Output.Data.SyncToCPU(AnsEndData[i].BatchSize);
                }
                
                

                FinalStartAns.BatchSize = AnsStartData.Last().BatchSize;
                Array.Clear(FinalStartAns.Output.Data.MemPtr, 0, FinalStartAns.BatchSize);

                FinalEndAns.BatchSize = AnsEndData.Last().BatchSize;
                Array.Clear(FinalEndAns.Output.Data.MemPtr, 0, FinalEndAns.BatchSize);

                for (int b = 0; b < MatchData.SrcSize; b++)
                {
                    int max_start_iter = 0;
                    double max_start_p = 0;

                    int max_end_iter = 0;
                    double max_end_p = 0;

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        float startP = StartAnswerProb[i].Output.Data.MemPtr[b];
                        if (startP > max_start_p)
                        {
                            max_start_p = startP;
                            max_start_iter = i;
                        }

                        float endP = EndAnswerProb[i].Output.Data.MemPtr[b];
                        if (endP > max_end_p)
                        {
                            max_end_p = endP;
                            max_end_iter = i;
                        }

                        if (BuilderParameters.PRED_RL == PredType.RL_AVGSIM)
                        {
                            int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalStartAns.Output.Data.MemPtr[m] += startP * AnsStartData[i].Output.Data.MemPtr[m];
                                FinalEndAns.Output.Data.MemPtr[m] += endP * AnsEndData[i].Output.Data.MemPtr[m];
                            }
                        }
                        if (BuilderParameters.PRED_RL == PredType.RL_AVGPROB)
                        {
                            int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                            int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];
                            double expSum_start = 0;
                            double expSum_end = 0;
                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                expSum_start += Math.Exp(AnsGamma * AnsStartData[i].Output.Data.MemPtr[m]);
                                expSum_end += Math.Exp(AnsGamma * AnsEndData[i].Output.Data.MemPtr[m]);
                            }

                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                FinalStartAns.Output.Data.MemPtr[m] += (float)(startP * Math.Exp(AnsGamma * AnsStartData[i].Output.Data.MemPtr[m]) / expSum_start);
                                FinalEndAns.Output.Data.MemPtr[m] += (float)(endP * Math.Exp(AnsGamma * AnsEndData[i].Output.Data.MemPtr[m]) / expSum_end);
                            }
                        }
                    }
                    if (!StartInferenStepStat.ContainsKey(max_start_iter)) StartInferenStepStat[max_start_iter] = 0;
                    StartInferenStepStat[max_start_iter] += 1;

                    if (!EndInferenStepStat.ContainsKey(max_end_iter)) EndInferenStepStat[max_end_iter] = 0;
                    EndInferenStepStat[max_end_iter] += 1;

                    if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
                    {
                        int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                        int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];

                        double expSum_start = 0;
                        double expSum_end = 0;
                        for (int m = matchBgn; m < matchEnd; m++)
                        {
                            expSum_start += Math.Exp(AnsGamma * AnsStartData[max_start_iter].Output.Data.MemPtr[m]);
                            expSum_end += Math.Exp(AnsGamma * AnsEndData[max_end_iter].Output.Data.MemPtr[m]);
                        }
                        for (int m = matchBgn; m < matchEnd; m++)
                        {
                            FinalStartAns.Output.Data.MemPtr[m] = (float)(Math.Exp(AnsGamma * AnsStartData[max_start_iter].Output.Data.MemPtr[m]) / expSum_start);  //AnsStartData[max_iter].Output.Data.MemPtr[m];
                            FinalEndAns.Output.Data.MemPtr[m] = (float)(Math.Exp(AnsGamma * AnsEndData[max_end_iter].Output.Data.MemPtr[m]) / expSum_end); // AnsEndData[max_iter].Output.Data.MemPtr[m];
                        }
                    }
                    if (BuilderParameters.PRED_RL == PredType.RL_ENSEMBLE3)
                    {
                        int matchBgn = b == 0 ? 0 : MatchData.Src2MatchIdx.MemPtr[b - 1];
                        int matchEnd = MatchData.Src2MatchIdx.MemPtr[b];

                        List<float> startProb = new List<float>();
                        List<float> endProb = new List<float>();
                        List<float[]> startAns = new List<float[]>();
                        List<float[]> endAns = new List<float[]>();

                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float startP = StartAnswerProb[i].Output.Data.MemPtr[b];
                            float endP = EndAnswerProb[i].Output.Data.MemPtr[b];
                            startProb.Add(startP);
                            endProb.Add(endP);

                            double expSum_start = 0;
                            double expSum_end = 0;
                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                expSum_start += Math.Exp(AnsGamma * AnsStartData[i].Output.Data.MemPtr[m]);
                                expSum_end += Math.Exp(AnsGamma * AnsEndData[i].Output.Data.MemPtr[m]);
                            }

                            startAns.Add(new float[matchEnd - matchBgn]);
                            endAns.Add(new float[matchEnd - matchBgn]);
                            for (int m = matchBgn; m < matchEnd; m++)
                            {
                                startAns[i][m - matchBgn] = (float)(Math.Exp(AnsGamma * AnsStartData[i].Output.Data.MemPtr[m]) / expSum_start);  //AnsStartData[max_iter].Output.Data.MemPtr[m];
                                endAns[i][m - matchBgn] = (float)(Math.Exp(AnsGamma * AnsEndData[i].Output.Data.MemPtr[m]) / expSum_end); // AnsEndData[max_iter].Output.Data.MemPtr[m];
                            }
                        }

                        float maxSpanValue = 0;
                        int maxSpanStart = 0;
                        int maxSpanEnd = 0;
                        Dictionary<string, float> resultList = new Dictionary<string, float>();
                        for (int si = 0; si < MaxIterationNum; si++)
                        {
                            for (int sj = 0; sj < MaxIterationNum; sj++)
                            {
                                Tuple<int,int,float> result = Util.ScanMaxValueSpan(AnsStartData[si].Output.Data.MemPtr, 
                                    AnsEndData[sj].Output.Data.MemPtr, matchBgn, matchEnd, BuilderParameters.SPAN_LENGTH);

                                string span = string.Format("{0}\t{1}", result.Item1, result.Item2);

                                float spanStartProb = startAns[si][result.Item1 - matchBgn];
                                float spanEndProb = endAns[sj][result.Item2 - matchBgn];
                                float termStartProb = startProb[si];
                                float termEndProb = endProb[sj];

                                if (!resultList.ContainsKey(span)) resultList[span] = 0;
                                resultList[span] += spanStartProb * spanEndProb * termStartProb * termEndProb;

                                if(resultList[span] > maxSpanValue)
                                {
                                    maxSpanValue = resultList[span];
                                    maxSpanStart = result.Item1;
                                    maxSpanEnd = result.Item2;
                                }
                            }
                        }
                        //
                        FinalStartAns.Output.Data.MemPtr[maxSpanStart] = 1;
                        FinalEndAns.Output.Data.MemPtr[maxSpanEnd] = maxSpanValue;
                    }
                }
                FinalStartAns.Output.Data.SyncFromCPU(FinalStartAns.BatchSize);
                FinalEndAns.Output.Data.SyncFromCPU(FinalEndAns.BatchSize);
            }
            public override void CleanDeriv()
            {
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    ComputeLib.Zero(StartAnswerProb[i].Deriv.Data, StartAnswerProb[i].BatchSize);
                    ComputeLib.Zero(EndAnswerProb[i].Deriv.Data, EndAnswerProb[i].BatchSize);
                }
            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    Array.Clear(StartTerminalData[i].Deriv.Data.MemPtr, 0, StartTerminalData[i].BatchSize);
                    Array.Clear(EndTerminalData[i].Deriv.Data.MemPtr, 0, EndTerminalData[i].BatchSize);
                }

                if (BuilderParameters.RL_ALL)
                {
                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float start_reward = StartAnswerProb[i].Deriv.Data.MemPtr[b] * BuilderParameters.TERMINATE_DISCOUNT;
                            float end_reward = EndAnswerProb[i].Deriv.Data.MemPtr[b] * BuilderParameters.TERMINATE_DISCOUNT;
                            if (BuilderParameters.IS_NORM_REWARD) start_reward = start_reward / (i + 1);
                            if (BuilderParameters.IS_NORM_REWARD) end_reward = end_reward / (i + 1);

                            StartTerminalData[i].Deriv.Data.MemPtr[b] += start_reward * (1 - StartTerminalData[i].Output.Data.MemPtr[b]);
                            EndTerminalData[i].Deriv.Data.MemPtr[b] += end_reward * (1 - EndTerminalData[i].Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                StartTerminalData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * start_reward * StartTerminalData[hp].Output.Data.MemPtr[b];
                                EndTerminalData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(BuilderParameters.RL_DISCOUNT, i - hp) * end_reward * EndTerminalData[hp].Output.Data.MemPtr[b];
                            }

                            if (StartTerminalData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("Start TerminalRunner Deriv is too large {0}, step {1}", StartTerminalData[i].Deriv.Data.MemPtr[b], i);
                            }

                            if (EndTerminalData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("End TerminalRunner Deriv is too large {0}, step {1}", EndTerminalData[i].Deriv.Data.MemPtr[b], i);
                            }

                        }
                    }
                }
                else
                {
                    for (int b = 0; b < MatchData.SrcSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float start_reward = StartAnswerProb[i].Deriv.Data.MemPtr[b] * BuilderParameters.TERMINATE_DISCOUNT;
                            float end_reward = EndAnswerProb[i].Deriv.Data.MemPtr[b] * BuilderParameters.TERMINATE_DISCOUNT;
                            if (BuilderParameters.IS_NORM_REWARD) start_reward = start_reward / (i + 1);
                            if (BuilderParameters.IS_NORM_REWARD) end_reward = end_reward / (i + 1);

                            for (int hp = 0; hp < MaxIterationNum; hp++)
                            {
                                if(hp == i)
                                    StartTerminalData[i].Deriv.Data.MemPtr[b] += start_reward * (1 - AnsStartData[i].Output.Data.MemPtr[b]);
                                else
                                    StartTerminalData[hp].Deriv.Data.MemPtr[b] += -start_reward * AnsStartData[hp].Output.Data.MemPtr[b];

                                if (hp == i)
                                    EndTerminalData[i].Deriv.Data.MemPtr[b] += end_reward * (1 - EndAnswerProb[i].Output.Data.MemPtr[b]);
                                else
                                    EndTerminalData[hp].Deriv.Data.MemPtr[b] += -end_reward * EndAnswerProb[hp].Output.Data.MemPtr[b];
                            }
                        }
                    }
                }
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    StartTerminalData[i].Deriv.Data.SyncFromCPU(StartTerminalData[i].BatchSize);
                    EndTerminalData[i].Deriv.Data.SyncFromCPU(EndTerminalData[i].BatchSize);
                }
            }

            public override void Update()
            { }
        }


        class ExactMatchPredictionRunner : ObjectiveRunner
        {
            HiddenBatchData StartPosData;
            HiddenBatchData EndPosData;
            List<HashSet<string>> PosTrueData;
            BiMatchBatchData AnsMatch;

            int SampleNum = 0;
            int HitNum = 0;
            int Iteration = 0;
            bool NeedSoftmax = true;
            float Gamma = 1;
            //List<Dictionary<string, float>> Result = null;
            //List<List<Dictionary<string, float>>> HisResults = new List<List<Dictionary<string, float>>>();

            public ExactMatchPredictionRunner(
                HiddenBatchData startPosData, HiddenBatchData endPosData, BiMatchBatchData ansMatch,
                List<HashSet<string>> posTrueData,
                RunnerBehavior behavior, string outputFile = "", bool needSoftmax = true, float gamma = 1) : base(Structure.Empty, behavior)
            {
                StartPosData = startPosData;
                EndPosData = endPosData;
                PosTrueData = posTrueData;
                AnsMatch = ansMatch;

                OutputPath = outputFile;

                NeedSoftmax = needSoftmax;
                Gamma = gamma;
            }
            string OutputPath = "";
            string resultFile = string.Empty;
            StreamWriter OutputWriter = null;

            string dumpFile = string.Empty;
            StreamWriter DumpWriter = null;
            public override void Init()
            {
                SampleNum = 0;
                HitNum = 0;
                if (!OutputPath.Equals(""))
                {
                    resultFile = OutputPath + "." + Iteration.ToString() + ".result";
                    dumpFile = OutputPath + "." + Iteration.ToString() + ".dump";
                    OutputWriter = new StreamWriter(resultFile);
                    DumpWriter = new StreamWriter(dumpFile);
                }
            }

            public override void Complete()
            {
                Logger.WriteLog("Sample {0}, Hit {1}, Accuracy {2}", SampleNum, HitNum, HitNum * 1.0 / SampleNum);
                if (OutputWriter != null)
                {
                    OutputWriter.Close();
                    DumpWriter.Close();

                    //restore.py --span_path dev.nltk.spans --raw_path dev.doc --ids_path dev.ids --fin score.4.result --fout dump.json
                    using (Process callProcess = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
                            Arguments = string.Format("\"{0}\" --span_path \"{1}\" --raw_path \"{2}\" --ids_path \"{3}\" --fin \"{4}\" --fout {5}",
                            @"\PublicDataSource\SQuADV3\restore.py",
                            BuilderParameters.ValidSpan,
                            @"\PublicDataSource\SQuADV3\dev.doc",
                            @"\PublicDataSource\SQuADV3\dev.ids",
                            resultFile,
                            resultFile + ".json"
                            ),
                            CreateNoWindow = true,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                        }
                    })
                    {
                        callProcess.Start();
                        callProcess.WaitForExit();
                    }

                    string resultsOutput = "";
                    using (Process callProcess = new Process()
                    {
                        StartInfo = new ProcessStartInfo()
                        {
                            FileName = @"C:\cygwin64\bin\python2.7.exe", // executiveFile,
                            Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"",
                            @"\PublicDataSource\SQuADV3\evaluate-v1.1.py",
                            @"\PublicDataSource\SQuADV3\dev-v1.1.json",
                            resultFile + ".json"
                            ),
                            CreateNoWindow = true,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                        }
                    })
                    {
                        callProcess.Start();
                        resultsOutput = callProcess.StandardOutput.ReadToEnd();
                        callProcess.WaitForExit();
                    }

                    Logger.WriteLog("Let see results here {0}", resultsOutput);
                }
                ObjectiveScore = HitNum * 1.0 / SampleNum;
                Iteration += 1;
            }

            public override void Forward()
            {
                if (NeedSoftmax) ComputeLib.SparseSoftmax(AnsMatch.Src2MatchIdx, StartPosData.Output.Data, StartPosData.Output.Data, Gamma, AnsMatch.SrcSize);
                if (NeedSoftmax) ComputeLib.SparseSoftmax(AnsMatch.Src2MatchIdx, EndPosData.Output.Data, EndPosData.Output.Data, Gamma, AnsMatch.SrcSize);

                StartPosData.Output.Data.SyncToCPU(StartPosData.BatchSize);
                EndPosData.Output.Data.SyncToCPU(EndPosData.BatchSize);
                AnsMatch.Src2MatchIdx.SyncToCPU(AnsMatch.BatchSize);

                for (int i = 0; i < AnsMatch.SrcSize; i++)
                {
                    Dictionary<string, float> candScoreDict = new Dictionary<string, float>();
                    string trueCandIdx = string.Empty;

                    int candBgn = i == 0 ? 0 : AnsMatch.Src2MatchIdx.MemPtr[i - 1];
                    int candEnd = AnsMatch.Src2MatchIdx.MemPtr[i];

                    List<string> prob = new List<string>();
                    for (int c = candBgn; c < candEnd; c++)
                    {
                        float candScoreStart = StartPosData.Output.Data.MemPtr[c]; // Util.Logistic();
                        float candScoreEnd = EndPosData.Output.Data.MemPtr[c]; // Util.Logistic(EndPosData.Output.Data.MemPtr[c2]);
                        int index = c - candBgn;
                        prob.Add(string.Format("{0}#{1}#{2}", index, candScoreStart, candScoreEnd));
                    }

                    if (DumpWriter != null) { DumpWriter.WriteLine(string.Join(" ", prob)); }

                    float maxP = 0;
                    float sP = 0;
                    float eP = 0;
                    int maxStart = -1;
                    int maxEnd = -1;
                    for (int c = candBgn; c < candEnd; c++)
                    {
                        float candScoreStart = StartPosData.Output.Data.MemPtr[c]; // Util.Logistic();
                        for (int c2 = c; c2 < candEnd; c2++)
                        {
                            float candScoreEnd = EndPosData.Output.Data.MemPtr[c2]; // Util.Logistic(EndPosData.Output.Data.MemPtr[c2]);

                            if (BuilderParameters.SPAN_LENGTH > 0 && c2 - c + 1 > BuilderParameters.SPAN_LENGTH) continue;
                            if (candScoreStart * candScoreEnd > maxP)
                            {
                                maxP = candScoreStart * candScoreEnd;
                                sP = candScoreStart;
                                eP = candScoreEnd;
                                maxStart = c - candBgn;
                                maxEnd = c2 - candBgn;
                            }
                        }
                    }
                    if (OutputWriter != null) { OutputWriter.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", maxStart, maxEnd, sP, eP, maxP); }
                    if (PosTrueData[SampleNum + i].Contains(string.Format("{0}#{1}", maxStart, maxEnd))) { HitNum += 1; }
                }
                SampleNum += AnsMatch.SrcSize;
            }

        }

        class TwoWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }
            SeqDenseBatchData BData { get; set; }
            CudaPieceInt EnsembleMask;

            public TwoWayEnsembleRunner(SeqDenseBatchData aData, SeqDenseBatchData bData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;
                BData = bData;

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 3, Behavior.Device);

                EnsembleMask = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask.MemPtr[s] = 2 + (s * 3);
                EnsembleMask.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;

                ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
                                             AData.SentOutput, 0, AData.Dim,
                                             Output.SentOutput, 0, Output.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.Matrix_AdditionEx(BData.SentOutput, 0, BData.Dim,
                                             BData.SentOutput, 0, BData.Dim,
                                             Output.SentOutput, AData.Dim, Output.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, BData.SentOutput, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, EnsembleMask, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, BData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              AData.SentSize, AData.Dim, 1, 1);

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, BData.SentOutput, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  BData.SentSize, BData.Dim, 1, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, AData.Dim, Output.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 1);

            }
        }

        class ThreeWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }
            SeqDenseBatchData BData { get; set; }

            HiddenBatchData CData { get; set; }

            CudaPieceInt EnsembleMask1;
            CudaPieceInt EnsembleMask2;

            CudaPieceFloat tmpOutput;
            public ThreeWayEnsembleRunner(SeqDenseBatchData aData, SeqDenseBatchData bData, HiddenBatchData cData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;
                BData = bData;
                CData = cData;

                if(aData.Dim != bData.Dim || aData.Dim != cData.Dim || bData.Dim != cData.Dim)
                {
                    throw new Exception(string.Format("The dimension of a {0} b {1} c {2} should be same", aData.Dim, bData.Dim, cData.Dim));
                }

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 4, Behavior.Device);
                tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);
                EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 2 + (s * 4);
                EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

                EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 3 + (s * 4);
                EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                iteration++;
                //if(name.Equals("memoryEnsemble") && iteration == 31)
                //{
                //    AData.SyncToCPU();
                //    BData.SyncToCPU();
                //    CData.SyncToCPU();
                //}
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;

                ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
                                             AData.SentOutput, 0, AData.Dim,
                                             Output.SentOutput, 0, Output.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.Matrix_AdditionEx(BData.SentOutput, 0, BData.Dim,
                                             BData.SentOutput, 0, BData.Dim,
                                             Output.SentOutput, AData.Dim, Output.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, BData.SentOutput, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0, EnsembleMask1, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, Output.SentMargin, 0, EnsembleMask2, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);

                //if (name.Equals("memoryEnsemble") && iteration == 31)
                //{
                //    Output.SyncToCPU();
                //}
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
                                              Output.SentSize, AData.Dim, 1, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  Output.SentSize, AData.Dim, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
                //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
                //                                  Output.SentSize, AData.Dim, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, BData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask1, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                              AData.SentSize, AData.Dim, 1, 1);

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, BData.SentOutput, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask1, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  BData.SentSize, BData.Dim, 1, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, AData.Dim, Output.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.SentDeriv, 0, BData.Dim,
                                             BData.Dim, BData.SentSize, 1, 0, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 1);

            }
        }

        class TwoAdvWayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }

            HiddenBatchData CData { get; set; }

            CudaPieceInt EnsembleMask1;
            CudaPieceInt EnsembleMask2;

            CudaPieceFloat tmpOutput;

            /// <summary>
            /// output = [CData, AData * CData] 
            /// </summary>
            /// <param name="aData"></param>
            /// <param name="cData"></param>
            /// <param name="behavior"></param>
            public TwoAdvWayEnsembleRunner(SeqDenseBatchData aData, HiddenBatchData cData, RunnerBehavior behavior)
            : base(Structure.Empty, behavior)
            {
                AData = aData;

                CData = cData;

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 2, Behavior.Device);

                tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);

                EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 0 + (s * 2);
                EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

                EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 1 + (s * 2);
                EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;


                ComputeLib.Matrix_AdditionMask(CData.Output.Data, 0, AData.SentMargin, 0,
                                               CData.Output.Data, 0, AData.SentMargin, 0,
                                               Output.SentOutput, 0, EnsembleMask1, 0,
                                               CData.Dim, AData.SentSize, 1, 0, 0);

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, Output.SentMargin, 0, EnsembleMask2, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);

            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
                                              Output.SentSize, AData.Dim, 1, 1);


                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  Output.SentSize, AData.Dim, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
                //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
                //                                  Output.SentSize, AData.Dim, 1);

                ComputeLib.ColumnWiseSumMask(Output.SentDeriv, 0, EnsembleMask1, 0, CudaPieceFloat.Empty,
                                             Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
            }
        }

        class TwoAdvV2WayEnsembleRunner : StructRunner
        {
            public new SeqDenseBatchData Output { get { return (SeqDenseBatchData)base.Output; } set { base.Output = value; } }
            //CudaPieceInt OutputMask;
            SeqDenseBatchData AData { get; set; }

            HiddenBatchData CData { get; set; }

            //CudaPieceInt EnsembleMask1;
            CudaPieceInt EnsembleMask2;

            CudaPieceFloat tmpOutput;

            /// <summary>
            /// output = [AData, AData * CData]
            /// </summary>
            /// <param name="aData"></param>
            /// <param name="cData"></param>
            /// <param name="behavior"></param>
            public TwoAdvV2WayEnsembleRunner(SeqDenseBatchData aData, HiddenBatchData cData, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                AData = aData;

                CData = cData;

                if (AData.Dim != CData.Dim) { throw new Exception(string.Format("dimension adata {0} is not the same as cdata {1}", AData.Dim, CData.Dim)); }

                Output = new SeqDenseBatchData(AData.SampleIdx, AData.SentMargin, AData.MAX_BATCHSIZE, AData.MAX_SENTSIZE, AData.Dim * 2, Behavior.Device);

                tmpOutput = new CudaPieceFloat(AData.MAX_SENTSIZE * AData.Dim, true, Behavior.Device == DeviceType.GPU);

                //EnsembleMask1 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                //for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask1.MemPtr[s] = 0 + (s * 2);
                //EnsembleMask1.SyncFromCPU(AData.MAX_SENTSIZE);

                EnsembleMask2 = new CudaPieceInt(AData.MAX_SENTSIZE, true, Behavior.Device == DeviceType.GPU);
                for (int s = 0; s < AData.MAX_SENTSIZE; s++) EnsembleMask2.MemPtr[s] = 1 + (s * 2);
                EnsembleMask2.SyncFromCPU(AData.MAX_SENTSIZE);
            }

            public override void Forward()
            {
                Output.BatchSize = AData.BatchSize;
                Output.SentSize = AData.SentSize;

                //if(name.StartsWith("ensemble_memory"))
                //{
                //    AData.SentOutput.SyncToCPU(AData.SentSize * AData.Dim);
                //    float al1 = AData.SentOutput.MemPtr.Take(AData.SentSize * AData.Dim).Select(i => Math.Abs(i)).Sum();
                //    float al2 = AData.SentOutput.MemPtr.Take(AData.SentSize * AData.Dim).Select(i => i * i).Sum();

                //    CData.Output.Data.SyncToCPU(CData.BatchSize * CData.Dim);
                //    float cl1 = CData.Output.Data.MemPtr.Take(CData.BatchSize * CData.Dim).Select(i => Math.Abs(i)).Sum();
                //    float cl2 = CData.Output.Data.MemPtr.Take(CData.BatchSize * CData.Dim).Select(i => i * i).Sum();

                //    Console.WriteLine("name {0}, al1 {1}, al2 {2}, cl1 {3}, cl2 {4}", name, al1, al2, cl1, cl2);

                //    ComputeLib.Zero(Output.SentOutput, Output.Dim * Output.SentSize);
                //    Output.SentOutput.SyncToCPU(Output.SentSize * Output.Dim);
                //    Cudalib.TestCuda();
                //    AData.SentMargin.SyncToCPU();   
                //}

                ComputeLib.Matrix_AdditionEx(AData.SentOutput, 0, AData.Dim,
                                             AData.SentOutput, 0, AData.Dim,
                                             Output.SentOutput, 0, Output.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 0);

                //ComputeLib.Matrix_AdditionMask(AData.SentOutput, 0, CudaPieceInt.Empty, 0,
                //                               AData.SentOutput, 0, CudaPieceInt.Empty, 0,
                //                               Output.SentOutput, 0, EnsembleMask1, 0,
                //                               AData.Dim, AData.SentSize, 1, 0, 0);
                //if (name.StartsWith("ensemble_memory"))
                //{
                //    Output.SentOutput.SyncToCPU(Output.SentSize * Output.Dim);
                //    float ol1 = Output.SentOutput.MemPtr.Take(Output.SentSize * Output.Dim).Select(i => Math.Abs(i)).Sum();
                //    float ol2 = Output.SentOutput.MemPtr.Take(Output.SentSize * Output.Dim).Select(i => i * i).Sum();
                //    Cudalib.TestCuda();
                //    Console.WriteLine("name {0}, ol1 {1}, ol2 {2}", name, ol1, ol2);

                //    EnsembleMask2.SyncToCPU();
                //}

                ComputeLib.ElementwiseProductMask(AData.SentOutput, 0, CData.Output.Data, 0, Output.SentOutput, 0,
                                                  CudaPieceInt.Empty, 0, AData.SentMargin, 0, EnsembleMask2, 0,
                                                  AData.SentSize, AData.Dim, 0, 1);


                //if (name.StartsWith("ensemble_memory"))
                //{
                //    Output.SentOutput.SyncToCPU(Output.SentSize * Output.Dim);
                //    float ol1 = Output.SentOutput.MemPtr.Take(Output.SentSize * Output.Dim).Select(i => Math.Abs(i)).Sum();
                //    float ol2 = Output.SentOutput.MemPtr.Take(Output.SentSize * Output.Dim).Select(i => i * i).Sum();

                //    Console.WriteLine("name {0}, ol1 {1}, ol2 {2}", name, ol1, ol2);
                //}
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.SentDeriv, Output.SentSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                //EnsembleMask2.SyncToCPU();

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, AData.SentOutput, 0, tmpOutput, 0, //HiddenStatus.Output.Data, 0,
                                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, CudaPieceInt.Empty, 0,
                                                  Output.SentSize, AData.Dim, 0, 1);

                ComputeLib.ColumnWiseSumMask(tmpOutput, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty,
                                            Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
                //ComputeLib.AccurateElementwiseProduct(Output.SentDeriv, 0, AData.SentOutput, 0, CData.Deriv.Data, 0, //HiddenStatus.Output.Data, 0,
                //                                  EnsembleMask2, 0, CudaPieceInt.Empty, 0, Output.SentMargin, 0,
                //                                  Output.SentSize, AData.Dim, 1);

                ComputeLib.ElementwiseProductMask(Output.SentDeriv, 0, CData.Output.Data, 0, AData.SentDeriv, 0, //HiddenStatus.Output.Data, 0,
                                              EnsembleMask2, 0, Output.SentMargin, 0, CudaPieceInt.Empty, 0,
                                              Output.SentSize, AData.Dim, 1, 1);

                ComputeLib.Matrix_AdditionEx(Output.SentDeriv, 0, Output.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.SentDeriv, 0, AData.Dim,
                                             AData.Dim, AData.SentSize, 1, 0, 1);

                //ComputeLib.Matrix_AdditionMask(Output.SentDeriv, 0, EnsembleMask1, 0,
                //                               AData.SentDeriv, 0, CudaPieceInt.Empty, 0,
                //                               AData.SentDeriv, 0, CudaPieceInt.Empty, 0,
                //                               AData.Dim, AData.SentSize, 1, 0, 1);
                //EnsembleMask1.SyncToCPU();
                //ComputeLib.ColumnWiseSumMask(Output.SentDeriv, 0, EnsembleMask1, 0, CudaPieceFloat.Empty,
                //                             Output.SampleIdx, Output.BatchSize, CData.Deriv.Data, 0, CudaPieceInt.Empty, 0, Output.SentSize, AData.Dim, 1, 1);
            }
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseBatchData, SequenceDataStat> context,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> query,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> contextl3g,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> queryl3g,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> contextchar,
                                                             IDataCashier<SeqSparseBatchData, SequenceDataStat> querychar,
                                                             DataCashier<DenseBatchData, DenseDataStat> startingPos,
                                                             DataCashier<DenseBatchData, DenseDataStat> endPos,
                                                             //IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> answer,

                                                             // text embedding cnn.
                                                             LayerStructure EmbedCNN,
                                                             LayerStructure L3GEmbedCNN,
                                                             List<LayerStructure> CharEmbedCNN,

                                                             List<LayerStructure> HighwayGate,
                                                             List<LayerStructure> HighwayConnect,

                                                             // context lstm 
                                                             LSTMStructure ContextD1LSTM, LSTMStructure ContextD2LSTM,

                                                             LayerStructure ContextCNN,

                                                             // query lstm
                                                             LSTMStructure QueryD1LSTM, LSTMStructure QueryD2LSTM,

                                                             LayerStructure QueryCNN,

                                                             LayerStructure CoAttLayer,

                                                             LayerStructure M1CNN,
                                                             //LSTMStructure M1D1LSTM, LSTMStructure M1D2LSTM,
                                                             
                                                             LayerStructure M2CNN,
                                                             //LSTMStructure M2D1LSTM, LSTMStructure M2D2LSTM,
                                                             
                                                             LayerStructure M3CNN,
                                                             //LSTMStructure M3D1LSTM, LSTMStructure M3D2LSTM,

                                                             LayerStructure AnsStartStruct, LayerStructure AnsEndStruct,

                                                             LayerStructure initState, MLPAttentionStructure attStruct, GRUCell gruCell, 
                                                             
                                                             LayerStructure startTerminateStruct, LayerStructure endTerminateStruct,
                                                             // model.
                                                             CompositeNNStructure model, RunnerBehavior Behavior, string resultFile)
        {
            ComputationGraph cg = new ComputationGraph();

            /************************************************************ Query Passage Candidate data *********/
            SeqSparseBatchData QueryData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(query, Behavior));
            SeqSparseBatchData ContextData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(context, Behavior));
            SeqSparseBatchData Queryl3gData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(queryl3g, Behavior));
            SeqSparseBatchData Contextl3gData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(contextl3g, Behavior));
            SeqSparseBatchData QuerycharData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(querychar, Behavior));
            SeqSparseBatchData ContextcharData = (SeqSparseBatchData)cg.AddDataRunner(new DataRunner<SeqSparseBatchData, SequenceDataStat>(contextchar, Behavior));

            DenseBatchData StartPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(startingPos, Behavior));
            DenseBatchData EndPos = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(endPos, Behavior));

            /************************************************************ Word Embedding on Query data *********/
            SeqDenseBatchData QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, QueryData, Behavior) { IsBackProp = BuilderParameters.IS_EMBED_UPDATE, IsUpdate = BuilderParameters.IS_EMBED_UPDATE });

            /************************************************************ Word Embedding on Passage data *********/
            SeqDenseBatchData ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                new SeqSparseConvRunner<SeqSparseBatchData>(EmbedCNN, ContextData, Behavior)
                { IsBackProp = BuilderParameters.IS_EMBED_UPDATE, IsUpdate = BuilderParameters.IS_EMBED_UPDATE });


            if (L3GEmbedCNN != null)
            {
                SeqDenseBatchData l3gQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    L3GEmbedCNN, Queryl3gData, Behavior));

                SeqDenseBatchData l3gContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    L3GEmbedCNN, Contextl3gData, Behavior));

                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QueryEmbedOutput, l3gQueryEmbed }, Behavior));

                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextEmbedOutput, l3gContextEmbed }, Behavior) {name = "_l3g" });
            }

            if (CharEmbedCNN.Count > 0)
            {
                SeqDenseBatchData letterQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    CharEmbedCNN[0], QuerycharData, Behavior)
                { name = "char_cnn_query" });
                for (int i = 1; i < CharEmbedCNN.Count; i++)
                {
                    letterQueryEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(
                        CharEmbedCNN[i], letterQueryEmbed, Behavior));
                }
                HiddenBatchData letterQueryMaxpool = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(letterQueryEmbed, Behavior) { name = "char_maxpool_query" });
                SeqDenseBatchData charQueryEmbed = new SeqDenseBatchData(
                    new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, letterQueryMaxpool.Dim),
                    QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
                    letterQueryMaxpool.Output.Data, letterQueryMaxpool.Deriv.Data, Behavior.Device);
                QueryEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QueryEmbedOutput, charQueryEmbed }, Behavior));

                SeqDenseBatchData letterContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqSparseConvRunner<SeqSparseBatchData>(
                    CharEmbedCNN[0], ContextcharData, Behavior)
                { name = "char_cnn_doc" });
                for (int i = 1; i < CharEmbedCNN.Count; i++)
                {
                    letterContextEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(
                        CharEmbedCNN[i], letterContextEmbed, Behavior));
                }
                HiddenBatchData letterContextMaxpool = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(letterContextEmbed, Behavior));
                SeqDenseBatchData charContextEmbed = new SeqDenseBatchData(
                    //ContextEmbedOutput.Stat, 
                    new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, letterContextMaxpool.Dim),
                    ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
                    letterContextMaxpool.Output.Data, letterContextMaxpool.Deriv.Data, Behavior.Device);
                ContextEmbedOutput = (SeqDenseBatchData)cg.AddRunner(
                    new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { ContextEmbedOutput, charContextEmbed }, Behavior) {name = "_char" });
            }
            /************************************************************ Highway net on Query and Passage Data *********/

            int highWayLayer = BuilderParameters.HighWay_Layer;

            if (highWayLayer > 0)
            {
                HiddenBatchData qtmpembed = new HiddenBatchData(QueryEmbedOutput.MAX_SENTSIZE, QueryEmbedOutput.Dim,
                                                                QueryEmbedOutput.SentOutput, QueryEmbedOutput.SentDeriv, Behavior.Device);

                HiddenBatchData dtmpembed = new HiddenBatchData(ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim,
                                                                ContextEmbedOutput.SentOutput, ContextEmbedOutput.SentDeriv, Behavior.Device);

                qtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayNetRunner(HighwayConnect, HighwayGate, qtmpembed, Behavior) { name = "queryEbd" });
                dtmpembed = (HiddenBatchData)cg.AddRunner(new HighwayNetRunner(HighwayConnect, HighwayGate, dtmpembed, Behavior) { name = "docEbd" });

                if (BuilderParameters.EMBEDDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
                {
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(qtmpembed, BuilderParameters.EMBEDDropout, Behavior, false));
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(dtmpembed, BuilderParameters.EMBEDDropout, Behavior, false));
                }

                QueryEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(QueryEmbedOutput.MAX_BATCHSIZE, QueryEmbedOutput.MAX_SENTSIZE, QueryEmbedOutput.Dim),
                                                                    QueryEmbedOutput.SampleIdx, QueryEmbedOutput.SentMargin,
                                                                    qtmpembed.Output.Data, qtmpembed.Deriv.Data, Behavior.Device);

                ContextEmbedOutput = new SeqDenseBatchData(new SequenceDataStat(ContextEmbedOutput.MAX_BATCHSIZE, ContextEmbedOutput.MAX_SENTSIZE, ContextEmbedOutput.Dim),
                                                                    ContextEmbedOutput.SampleIdx, ContextEmbedOutput.SentMargin,
                                                                    dtmpembed.Output.Data, dtmpembed.Deriv.Data, Behavior.Device);
            }


            /************************************************************ LSTM on Query data *********/
            SeqDenseBatchData QuerySeqO = null;
            {
                QuerySeqO = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(QueryD1LSTM, QueryD2LSTM, QueryEmbedOutput, Behavior));
            }
            if (QueryCNN != null)
            {
                SeqDenseBatchData QueryCNNEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(QueryCNN, QueryEmbedOutput, Behavior));
                QuerySeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { QuerySeqO, QueryCNNEmbed }, Behavior));
            }
            BiMatchBatchData QueryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(QuerySeqO, Behavior) { IsUpdate = true });

            /************************************************************ LSTM on Passage data *********/
            SeqDenseBatchData MemorySeqO = null;
            {
                MemorySeqO = (SeqDenseBatchData)cg.AddRunner(new BiLSTMRunner(ContextD1LSTM, ContextD2LSTM, ContextEmbedOutput, Behavior) { name = "MemorySeqO"});
            }
            if (ContextCNN != null)
            {
                SeqDenseBatchData ContextCNNEmbed = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(ContextCNN, ContextEmbedOutput, Behavior));
                MemorySeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { MemorySeqO, ContextCNNEmbed }, Behavior));
            }
            BiMatchBatchData MemoryMatch = (BiMatchBatchData)cg.AddRunner(new SeqBiMatchRunner(MemorySeqO, Behavior) { IsUpdate = true });


            if (BuilderParameters.CoAttDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(QuerySeqO.MAX_SENTSIZE, QuerySeqO.Dim, QuerySeqO.SentOutput, QuerySeqO.SentDeriv, Behavior.Device),
                    BuilderParameters.CoAttDropout, Behavior, false));

                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(MemorySeqO.MAX_SENTSIZE, MemorySeqO.Dim, MemorySeqO.SentOutput, MemorySeqO.SentDeriv, Behavior.Device),
                    BuilderParameters.CoAttDropout, Behavior, false));
            }

            SoftSeqLinearSimRunner coattRunner = new SoftSeqLinearSimRunner(MemorySeqO, QuerySeqO, DataPanel.MaxCrossQDLen, CoAttLayer, Behavior);
            BiMatchBatchData d2qMatchData = coattRunner.MatchData;
            HiddenBatchData CoAttL = (HiddenBatchData)cg.AddRunner(coattRunner);

            HiddenBatchData d2qData = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(CoAttL, QuerySeqO, d2qMatchData, 1, Behavior));
            SeqDenseBatchData MemorySeqO2 = new SeqDenseBatchData(new SequenceDataStat(MemorySeqO.MAX_BATCHSIZE, MemorySeqO.MAX_SENTSIZE, d2qData.Dim),
                                                                MemorySeqO.SampleIdx, MemorySeqO.SentMargin,
                                                                d2qData.Output.Data, d2qData.Deriv.Data, Behavior.Device);

            SeqDenseBatchData DocG = null;
            if (BuilderParameters.MEMORY_ENSEMBLE == 2)
            {
                DocG = (SeqDenseBatchData)cg.AddRunner(new TwoWayEnsembleRunner(MemorySeqO, MemorySeqO2, Behavior));
            }
            else if (BuilderParameters.MEMORY_ENSEMBLE == 3)
            {
                HiddenBatchData maxCoeff = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(
                                new SeqDenseBatchData(new SequenceDataStat()
                                {
                                    MAX_BATCHSIZE = d2qMatchData.Stat.MAX_SRC_BATCHSIZE,
                                    MAX_SEQUENCESIZE = d2qMatchData.Stat.MAX_MATCH_BATCHSIZE,
                                    FEATURE_DIM = CoAttL.Dim
                                }, d2qMatchData.Src2MatchIdx, d2qMatchData.SrcIdx,
                                CoAttL.Output.Data, CoAttL.Deriv.Data, Behavior.Device), Behavior));
                HiddenBatchData d2qMaxData = (HiddenBatchData)cg.AddRunner(new MemoryRetrievalRunner(maxCoeff, MemorySeqO, MemoryMatch, 1, Behavior));
                DocG = (SeqDenseBatchData)cg.AddRunner(new ThreeWayEnsembleRunner(MemorySeqO, MemorySeqO2, d2qMaxData, Behavior) { name = "memoryEnsemble" });
            }
            if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(DocG.MAX_SENTSIZE, DocG.Dim, DocG.SentOutput, DocG.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
            }
            if(DocG.Dim != M1CNN.Neural_In) {
                throw new Exception(string.Format("the dimension of docg, and m1 cnn doesn't match {0}, {1}", DocG.Dim, M1CNN.Neural_In)); }

            SeqDenseBatchData DocM1 = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(M1CNN, DocG, Behavior) { name = "M1CNN"});
            if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
            {
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(DocM1.MAX_SENTSIZE, DocM1.Dim, DocM1.SentOutput, DocM1.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
            }

            //[p0, g1] = 4d + 2h
            SeqDenseBatchData DocGM1 = null;
            SeqDenseBatchData DocGM2 = null;

            if (BuilderParameters.STARTPOINT_CONFIG == 0 || BuilderParameters.STARTPOINT_CONFIG == 1)
            {
                DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM1 }, Behavior));
            }
            else if (BuilderParameters.STARTPOINT_CONFIG == 2 || BuilderParameters.STARTPOINT_CONFIG == 3)
            {
                DocGM1 = DocM1;
            }
            else if (BuilderParameters.STARTPOINT_CONFIG == 4 || BuilderParameters.STARTPOINT_CONFIG == 5 || BuilderParameters.STARTPOINT_CONFIG == 6
                || BuilderParameters.STARTPOINT_CONFIG == 7 || BuilderParameters.STARTPOINT_CONFIG == 8 || BuilderParameters.STARTPOINT_CONFIG == 9)
            {
                if (DocM1.Dim != M2CNN.Neural_In)
                {
                    throw new Exception(string.Format("the dimension of docm1, and m2d1 lstm doesn't match {0}, {1}", DocM1.Dim, M2CNN.Neural_In));
                }

                SeqDenseBatchData DocM2 = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(M2CNN, DocM1, Behavior) { name = "M2CNN"});
                if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
                {
                    cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(DocM2.MAX_SENTSIZE, DocM2.Dim, DocM2.SentOutput, DocM2.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
                }

                if (BuilderParameters.STARTPOINT_CONFIG == 4)
                {
                    DocGM1 = DocM2;
                }
                else if (BuilderParameters.STARTPOINT_CONFIG == 5)
                {
                    DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocG, DocM2 }, Behavior) { IsUpdate = true });
                }
                else if (BuilderParameters.STARTPOINT_CONFIG == 6)
                {
                    DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocM1, DocM2 }, Behavior) { IsUpdate = true });
                }

                if(BuilderParameters.STARTPOINT_CONFIG == 7 || BuilderParameters.STARTPOINT_CONFIG == 8 || BuilderParameters.STARTPOINT_CONFIG == 9)
                {
                    SeqDenseBatchData DocM3 = (SeqDenseBatchData)cg.AddRunner(new SeqDenseConvRunner<SeqDenseBatchData>(M3CNN, DocM2, Behavior) { name = "M3CNN" });
                    //new BiLSTMRunner(M3D1LSTM, M3D2LSTM, DocM2, Behavior) { name = "M3LSTM" });
                    if (BuilderParameters.LSTMDropout > 0 && Behavior.RunMode == DNNRunMode.Train)
                    {
                        cg.AddRunner(new DropOutProcessor<HiddenBatchData>(new HiddenBatchData(DocM3.MAX_SENTSIZE, DocM3.Dim, DocM3.SentOutput, DocM3.SentDeriv, Behavior.Device), BuilderParameters.LSTMDropout, Behavior, false));
                    }

                    if (BuilderParameters.STARTPOINT_CONFIG == 7)
                    {
                        DocGM1 = DocM3;
                    }
                    else if (BuilderParameters.STARTPOINT_CONFIG == 8)
                    {
                        DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocM1, DocM2 }, Behavior));
                    }
                    else if (BuilderParameters.STARTPOINT_CONFIG == 9)
                    {
                        DocGM1 = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { DocM1, DocM2, DocM3 }, Behavior));
                    }
                }
            }
            DocGM2 = DocGM1;

            HiddenBatchData startInput = new HiddenBatchData(DocGM1.MAX_SENTSIZE, DocGM1.Dim, DocGM1.SentOutput, DocGM1.SentDeriv, Behavior.Device);
            if (BuilderParameters.Soft2Dropout > 0 && Behavior.RunMode == DNNRunMode.Train)
                cg.AddRunner(new DropOutProcessor<HiddenBatchData>(startInput, BuilderParameters.Soft2Dropout, Behavior, false) { IsUpdate = true });

            //if (BuilderParameters.DEBUG_MODE < 10)
            //{
            //    if (BuilderParameters.DEBUG_MODE == 0)
            //    {
            //        EndP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsEndStruct, endInput, Behavior) { IsUpdate = true });
            //    }
            //    else if (BuilderParameters.DEBUG_MODE >= 1)
            //    {
            //        ReasonNetRunner reasonet = new ReasonNetRunner(cg, AnsStartStruct, AnsEndStruct, 1, DocGM1, DocGM2, MemoryMatch, null, null, 1, 1, Behavior);
            //        cg.AddRunner(reasonet);
            //        StartP = reasonet.FinalStartAns;
            //        EndP = reasonet.FinalEndAns;
            //    }
            //    switch (Behavior.RunMode)
            //    {
            //        case DNNRunMode.Train:
            //            if (BuilderParameters.SPAN_OBJECTIVE == 0)
            //            {
            //                cg.AddObjective(new BayesianPairRunner(StartPos.Data, EndPos.Data, StartP, EndP, MemoryMatch, 1, Behavior));
            //            }
            //            else if (BuilderParameters.SPAN_OBJECTIVE == 1)
            //            {
            //                Logger.WriteLog("Train Span Length {0}", BuilderParameters.TRAIN_SPAN_LENGTH);
            //                cg.AddObjective(new BayesianSpanBoundaryRunner(StartPos.Data, EndPos.Data, StartP, EndP, MemoryMatch, 1, BuilderParameters.TRAIN_SPAN_LENGTH, Behavior));
            //            }
            //            //cg.AddObjective(new BayesianPairRunner(StartPos.Data, EndPos.Data, StartP, EndP, MemoryMatch, 1, Behavior));
            //            break;
            //        case DNNRunMode.Predict:
            //            cg.AddRunner(new ExactMatchPredictionRunner(StartP, EndP, MemoryMatch, DataPanel.ValidResults, Behavior, BuilderParameters.ScoreOutputPath));
            //            break;
            //    }
            //}
            if (BuilderParameters.DEBUG_MODE == 0)
            {
                ReasonNetV2Runner reasonet = new ReasonNetV2Runner(cg, AnsStartStruct, AnsEndStruct, BuilderParameters.RECURRENT_STEP, DocGM2, MemoryMatch,
                    initState, attStruct, BuilderParameters.AttType, gruCell, startTerminateStruct, endTerminateStruct,
                    BuilderParameters.Gamma, 1, Behavior);
                cg.AddRunner(reasonet);
                switch (Behavior.RunMode)
                {
                    case DNNRunMode.Train:
                        cg.AddObjective(new BayesianContrastiveRewardRunner(StartPos.Data, reasonet.AnsStartData, reasonet.StartAnswerProb, MemoryMatch, 1, Behavior));
                        cg.AddObjective(new BayesianContrastiveRewardRunner(EndPos.Data, reasonet.AnsEndData, reasonet.EndAnswerProb, MemoryMatch, 1, Behavior));
                        //cg.AddObjective(new BayesianContrastivePairRewardRunner(StartPos.Data, EndPos.Data, reasonet.AnsStartData, reasonet.AnsEndData, reasonet.AnswerProb, MemoryMatch, 1, Behavior));
                        break;
                    case DNNRunMode.Predict:
                        cg.AddRunner(new ExactMatchPredictionRunner(reasonet.FinalStartAns, reasonet.FinalEndAns, MemoryMatch, DataPanel.ValidResults, Behavior, BuilderParameters.ScoreOutputPath, false));
                        break;
                }
            }
            else
            {
                HiddenBatchData StartP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStartStruct, startInput, Behavior) { IsUpdate = true });
                HiddenBatchData EndP = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(AnsEndStruct, startInput, Behavior) { IsUpdate = true });

                switch (Behavior.RunMode)
                {
                    case DNNRunMode.Train:
                        //if (BuilderParameters.SPAN_OBJECTIVE == 0)
                        //{
                        cg.AddObjective(new BayesianPairRunner(StartPos.Data, EndPos.Data, StartP, EndP, MemoryMatch, 1, Behavior));
                        //}
                        //else if (BuilderParameters.SPAN_OBJECTIVE == 1)
                        //{
                        //    Logger.WriteLog("Train Span Length {0}", BuilderParameters.TRAIN_SPAN_LENGTH);
                        //    cg.AddObjective(new BayesianSpanBoundaryRunner(StartPos.Data, EndPos.Data, StartP, EndP, MemoryMatch, 1, BuilderParameters.TRAIN_SPAN_LENGTH, Behavior));
                        //}
                        //cg.AddObjective(new BayesianPairRunner(StartPos.Data, EndPos.Data, StartP, EndP, MemoryMatch, 1, Behavior));
                        break;
                    case DNNRunMode.Predict:
                        cg.AddRunner(new ExactMatchPredictionRunner(StartP, EndP, MemoryMatch, DataPanel.ValidResults, Behavior, BuilderParameters.ScoreOutputPath));
                        break;
                }
            }
            return cg;
        }

        private LayerStructure InitWordEmbedLayers(DeviceType device)
        {
            LayerStructure wordEmbedding = null;
            wordEmbedding = new LayerStructure(DataPanel.WordSize, BuilderParameters.EMBED_LAYER_DIM[0],
                    BuilderParameters.EMBED_ACTIVATION[0], N_Type.Convolution_layer, BuilderParameters.EMBED_LAYER_WIN[0],
                    BuilderParameters.EMBED_DROPOUT[0], false, device, BuilderParameters.IS_EMBED_UPDATE);

            #region Glove Work Embedding Initialization.

            if (!BuilderParameters.InitWordEmbedding.Equals(string.Empty))
            {
                int wordDim = BuilderParameters.EMBED_LAYER_DIM[0];

                int halfWinStart = BuilderParameters.EMBED_LAYER_WIN[0] / 2; // * DataPanel.WordDim;

                Logger.WriteLog("Init Glove Word Embedding ...");
                int hit = 0;
                using (StreamReader mreader = new StreamReader(BuilderParameters.InitWordEmbedding))
                {
                    int[] HitWords = new int[DataPanel.wordFreqDict.ItemDictSize];
                    while (!mreader.EndOfStream)
                    {
                        string[] items = mreader.ReadLine().Split(' ');
                        string word = items[0];
                        float[] vec = new float[items.Length - 1];
                        for (int i = 1; i < items.Length; i++)
                        {
                            vec[i - 1] = float.Parse(items[i]);
                        }

                        int wordIdx = DataPanel.wordFreqDict.IndexItem(word);

                        if (wordIdx >= 0)
                        {
                            HitWords[wordIdx] = 1;
                            int feaIdx = wordIdx;

                            for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                            {
                                for (int i = 0; i < wordDim; i++)
                                {
                                    wordEmbedding.weight.MemPtr[((win + halfWinStart) * DataPanel.WordSize + feaIdx) * wordDim + i] = vec[i];
                                }
                            }
                            hit++;
                        }
                        else
                        {
                            //....
                        }
                    }
                    if (BuilderParameters.UNK_INIT == 0)
                    {
                        Logger.WriteLog("UNK is initalized to zero!");
                        for (int u = 0; u < DataPanel.wordFreqDict.ItemDictSize; u++)
                        {
                            if (HitWords[u] == 0)
                            {
                                int unkIdx = u;
                                for (int win = -halfWinStart; win < BuilderParameters.EMBED_LAYER_WIN[0] - halfWinStart; win++)
                                {
                                    for (int i = 0; i < wordDim; i++)
                                    {
                                        wordEmbedding.weight.MemPtr[((win + halfWinStart) * DataPanel.WordSize + unkIdx) * wordDim + i] = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                wordEmbedding.weight.SyncFromCPU();
                Logger.WriteLog("Init Glove Word Embedding Done, {0} words initalized in total {1}.", hit, DataPanel.wordFreqDict.ItemDictSize);
            }
            #endregion.

            return wordEmbedding;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            // word embedding layer.
            LayerStructure wordembedLayer = null;

            // l3g embedding layer.
            LayerStructure l3gembedLayer = null;

            // char embedding layer.
            List<LayerStructure> charembedLayer = new List<LayerStructure>();


            List<LayerStructure> highwayConnect = new List<LayerStructure>();
            List<LayerStructure> highwayGate = new List<LayerStructure>();

            LayerStructure CNNembed = null;

            //context lstm layer.
            LSTMStructure contextD1LSTM = null;
            LSTMStructure contextD2LSTM = null;

            //query lstm layer.
            LSTMStructure queryD1LSTM = null;
            LSTMStructure queryD2LSTM = null;


            LayerStructure CoAttLayer = null;

            LayerStructure M1CNN = null;
            LayerStructure M2CNN = null;
            LayerStructure M3CNN = null;

            LayerStructure AnsStartStruct = null;
            LayerStructure AnsEndStruct = null;

            LayerStructure InitStateStruct = null;
            MLPAttentionStructure AttStruct = null;
            GRUCell StateStruct = null;
            LayerStructure StartTermalStruct = null;
            LayerStructure EndTermalStruct = null;

            #region Model Hyperparameter Region

            #endregion

            if (BuilderParameters.SeedModel.Equals(string.Empty))
            {
                wordembedLayer = InitWordEmbedLayers(device);
                if (BuilderParameters.IS_EMBED_UPDATE) { modelStructure.AddLayer(wordembedLayer); }
                else { wordembedLayer.StructureOptimizer.Clear(); }

                int embedDim = wordembedLayer.Neural_Out;
                int l3gembedDim = BuilderParameters.L3G_EMBED_DIM;
                if (l3gembedDim > 0)
                {
                    l3gembedLayer = new LayerStructure(DataPanel.L3gSize, BuilderParameters.L3G_EMBED_DIM, A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.L3G_EMBED_WIN, 0, false, device);
                    modelStructure.AddLayer(l3gembedLayer);
                }

                int charembedDim = BuilderParameters.CHAR_EMBED_DIM.Last();
                if (charembedDim > 0)
                {
                    int inputDim = DataPanel.CharSize;
                    for (int i = 0; i < BuilderParameters.CHAR_EMBED_DIM.Length; i++)
                    {
                        LayerStructure charEmbed = new LayerStructure(inputDim, BuilderParameters.CHAR_EMBED_DIM[i], A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.CHAR_EMBED_WIN[i], 0, false, device);
                        inputDim = BuilderParameters.CHAR_EMBED_DIM[i];
                        charembedLayer.Add(charEmbed);
                        modelStructure.AddLayer(charEmbed);
                    }
                }
                Logger.WriteLog("Word Embedding {0}, L3g Embedding {1}, Char Embedding {2}", embedDim, l3gembedDim, charembedDim);
                embedDim = embedDim + l3gembedDim + charembedDim;
                Logger.WriteLog("Total Embedding {0}", embedDim);

                for (int i = 0; i < BuilderParameters.HighWay_Layer; i++)
                {
                    if (i == 0 || !BuilderParameters.HighWay_Share)
                    {
                        highwayConnect.Add(new LayerStructure(embedDim, embedDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, true, device));
                        highwayGate.Add(new LayerStructure(embedDim, embedDim, A_Func.Sigmoid, N_Type.Fully_Connected, 1, 0, true, device));

                        modelStructure.AddLayer(highwayConnect[i]);
                        modelStructure.AddLayer(highwayGate[i]);
                    }
                    else
                    {
                        highwayConnect.Add(highwayConnect[0]);
                        highwayGate.Add(highwayGate[0]);
                    }
                }
                {
                    contextD1LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    contextD2LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);

                    modelStructure.AddLayer(contextD1LSTM);
                    modelStructure.AddLayer(contextD2LSTM);
                }

                if (!BuilderParameters.IS_SHARE_RNN)
                {
                    queryD1LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);
                    queryD2LSTM = new LSTMStructure(embedDim, new int[] { BuilderParameters.GRU_DIM }, device, BuilderParameters.RndInit);

                    modelStructure.AddLayer(queryD1LSTM);
                    modelStructure.AddLayer(queryD2LSTM);
                }
                else
                {
                    queryD1LSTM = contextD1LSTM;
                    queryD2LSTM = contextD2LSTM;
                }

                if (BuilderParameters.CNN_DIM > 0)
                {
                    CNNembed = (new LayerStructure(embedDim, BuilderParameters.CNN_DIM, A_Func.Tanh, N_Type.Convolution_layer, BuilderParameters.CNN_WIN, 0, true, device, true));
                    modelStructure.AddLayer(CNNembed);
                }

                int D = BuilderParameters.GRU_DIM * 2 + BuilderParameters.CNN_DIM;
                int H = BuilderParameters.GRU_DIM;

                CoAttLayer = new LayerStructure(3 * D, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);

                int G_Dim = 0;
                if (BuilderParameters.MEMORY_ENSEMBLE == 2) { G_Dim = 3 * D; }
                else if (BuilderParameters.MEMORY_ENSEMBLE == 3) { G_Dim = 4 * D; }

                M1CNN = (new LayerStructure(G_Dim, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                //M1D1LSTM = new LSTMStructure(G_Dim, new int[] { H }, device, BuilderParameters.RndInit);
                //M1D2LSTM = new LSTMStructure(G_Dim, new int[] { H }, device, BuilderParameters.RndInit);
                int startMemoryDim = 0;
                switch (BuilderParameters.STARTPOINT_CONFIG)
                {
                    //G + M1;
                    case 0:
                    case 1:
                        startMemoryDim = G_Dim + 2 * H;
                        //AnsStartStruct = new LayerStructure(G_Dim + 2 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                        break;
                    // M1;
                    case 2:
                    case 3:
                        startMemoryDim = 2 * H;
                        //AnsStartStruct = new LayerStructure(2 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                        break;
                    // M2;
                    case 4:
                        startMemoryDim = 2 * H;
                        //AnsStartStruct = new LayerStructure(2 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                        break;
                    //G + M2
                    case 5:
                        startMemoryDim = G_Dim + 2 * H;
                        //AnsStartStruct = new LayerStructure(G_Dim + 2 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                        break;
                    //M1 + M2
                    case 6:
                        startMemoryDim = 4 * H;
                        //AnsStartStruct = new LayerStructure(4 * H, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                        break;
                    // M3
                    case 7:
                        startMemoryDim = 2 * H;
                        break;
                    // M2 + M3;
                    case 8:
                        startMemoryDim = 4 * H;
                        break;
                    // M1 + M2 + M3;
                    case 9:
                        startMemoryDim = 6 * H;
                        break;
                }

                int endMemoryDim = 0;
                switch (BuilderParameters.ENDPOINT_CONFIG)
                {
                    // G + M2; M2 = LSTM(M1)
                    case 0:
                    case 1:
                        endMemoryDim = G_Dim + 2 * H;
                        break;
                    // G + M2; M2 = LSTM(G, M1, M1 * Ali, Ali)
                    //M2D1LSTM = new LSTMStructure(G_Dim + 6 * H, new int[] { H }, device, BuilderParameters.RndInit);
                    //M2D2LSTM = new LSTMStructure(G_Dim + 6 * H, new int[] { H }, device, BuilderParameters.RndInit);
                    //endMemoryDim = G_Dim + 2 * H;
                    //break;
                    // M2; M2 = LSTM(M1 * Ali, Ali)
                    //case 2:
                    //    M2D1LSTM = new LSTMStructure(4 * H, new int[] { H }, device, BuilderParameters.RndInit);
                    //    M2D2LSTM = new LSTMStructure(4 * H, new int[] { H }, device, BuilderParameters.RndInit);
                    //    endMemoryDim = 2 * H;
                    //    break;
                    // M1;
                    case 2:
                    case 3:
                        endMemoryDim = 2 * H;
                        break;
                    // M2; M2 = LSTM(M1)
                    case 4:
                        M2CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        //M2D1LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                        //M2D2LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                        endMemoryDim = 2 * H;
                        break;
                    //G + M2;
                    case 5:
                        M2CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        endMemoryDim = G_Dim + 2 * H;
                        break;
                    //M1 + M2
                    case 6:
                        M2CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        endMemoryDim = 4 * H;
                        break;
                    // M3;
                    case 7:
                        M2CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        M3CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        endMemoryDim = 2 * H;
                        break;
                    // M2 + M3;
                    case 8:
                        M2CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        M3CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        endMemoryDim = 4 * H;
                        break;
                    case 9:
                        M2CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        M3CNN = (new LayerStructure(2 * H, 2 * H, A_Func.Tanh, N_Type.Convolution_layer, 5, 0, false, device));
                        endMemoryDim = 6 * H;
                        break;
                        //// [ali, ali * M2]; M2 = LSTM(M1)
                        //case 7:
                        //    M2D1LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                        //    M2D2LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                        //    endMemoryDim = 4 * H;
                        //    break;
                        //// [M2, ali * M2]; M2 = LSTM(M1)
                        //case 8:
                        //    M2D1LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                        //    M2D2LSTM = new LSTMStructure(2 * H, new int[] { H }, device, BuilderParameters.RndInit);
                        //    endMemoryDim = 4 * H;
                        //    break;
                }
                modelStructure.AddLayer(CoAttLayer);
                modelStructure.AddLayer(M1CNN);

                if(M2CNN != null)
                {
                    modelStructure.AddLayer(M2CNN);
                }
                
                if (M3CNN != null)
                {
                    modelStructure.AddLayer(M3CNN);
                }

                int stateDim = endMemoryDim;
                if (startMemoryDim != endMemoryDim) { throw new Exception(string.Format("Start {0} and End {1} Memory Dim should be the same!", startMemoryDim, endMemoryDim)); }

                int ansDim = startMemoryDim;
                if (BuilderParameters.DEBUG_MODE == 0)
                {
                    InitStateStruct = new LayerStructure(stateDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                    AttStruct = new MLPAttentionStructure(stateDim, endMemoryDim, BuilderParameters.ATT_HID_DIM, device);
                    StateStruct = new GRUCell(endMemoryDim, stateDim, device, BuilderParameters.RndInit);
                    StartTermalStruct = new LayerStructure(stateDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                    EndTermalStruct = new LayerStructure(stateDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);

                    modelStructure.AddLayer(InitStateStruct);
                    modelStructure.AddLayer(AttStruct);
                    modelStructure.AddLayer(StateStruct);
                    modelStructure.AddLayer(StartTermalStruct);
                    modelStructure.AddLayer(EndTermalStruct);

                    ansDim = startMemoryDim + stateDim;
                }

                AnsStartStruct = new LayerStructure(ansDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);
                AnsEndStruct = new LayerStructure(ansDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device);

                modelStructure.AddLayer(AnsStartStruct);
                modelStructure.AddLayer(AnsEndStruct);
            }
            else
            {
                using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                {
                    modelStructure = new CompositeNNStructure(modelReader, device);
                    int link = 0;

                    if (BuilderParameters.IS_EMBED_UPDATE) { wordembedLayer = (LayerStructure)modelStructure.CompositeLinks[link++]; }
                    else { wordembedLayer = InitWordEmbedLayers(device); }

                    if (BuilderParameters.L3G_EMBED_DIM > 0) { l3gembedLayer = (LayerStructure)modelStructure.CompositeLinks[link++]; }


                    if (BuilderParameters.CHAR_EMBED_DIM.Last() > 0)
                    {
                        for (int i = 0; i < BuilderParameters.CHAR_EMBED_DIM.Length; i++)
                        {
                            charembedLayer.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                        }
                    }

                    for (int i = 0; i < BuilderParameters.HighWay_Layer; i++)
                    {
                        if (i == 0 || !BuilderParameters.HighWay_Share)
                        {
                            highwayConnect.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                            highwayGate.Add((LayerStructure)modelStructure.CompositeLinks[link++]);
                        }
                        else
                        {
                            highwayConnect.Add(highwayConnect[0]);
                            highwayGate.Add(highwayGate[0]);
                        }
                    }

                    {
                        contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        contextD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    }
                    if (!BuilderParameters.IS_SHARE_RNN)
                    {
                        queryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        queryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                    }
                    else
                    {
                        queryD1LSTM = contextD1LSTM;
                        queryD2LSTM = contextD2LSTM;
                    }
                    if (BuilderParameters.CNN_DIM > 0) { CNNembed = (LayerStructure)modelStructure.CompositeLinks[link++]; }

                    CoAttLayer = (LayerStructure)modelStructure.CompositeLinks[link++];
                    M1CNN= (LayerStructure)modelStructure.CompositeLinks[link++];


                    if (BuilderParameters.ENDPOINT_CONFIG == 4 || BuilderParameters.ENDPOINT_CONFIG == 5 || BuilderParameters.ENDPOINT_CONFIG == 6)
                    {
                        M2CNN = (LayerStructure)modelStructure.CompositeLinks[link++];
                    }
                    if (BuilderParameters.ENDPOINT_CONFIG == 7 || BuilderParameters.ENDPOINT_CONFIG == 8 || BuilderParameters.ENDPOINT_CONFIG == 9)
                    {
                        M3CNN = (LayerStructure)modelStructure.CompositeLinks[link++];
                    }

                    if (BuilderParameters.DEBUG_MODE == 0)
                    {
                        InitStateStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                        AttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                        StateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                        StartTermalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                        EndTermalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                    }
                    AnsStartStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                    AnsEndStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                }
            }

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainContextL3G, DataPanel.TrainQueryL3G, DataPanel.TrainContextChar, DataPanel.TrainQueryChar, DataPanel.TrainStartPos, DataPanel.TrainEndPos,
                        wordembedLayer, l3gembedLayer, charembedLayer, highwayGate, highwayConnect, contextD1LSTM, contextD2LSTM, CNNembed, queryD1LSTM, queryD2LSTM, CNNembed,
                        CoAttLayer, M1CNN, M2CNN, M3CNN, AnsStartStruct, AnsEndStruct, InitStateStruct, AttStruct, StateStruct, 
                        StartTermalStruct, EndTermalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib }, string.Empty);
                    trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

                    //ComputationGraph testCG = null;
                    //if (BuilderParameters.IsTestFile)
                    //    testCG = BuildComputationGraph(DataPanel.TestContext, DataPanel.TestQuery, DataPanel.TestAnswer,
                    //        embedLayers, contextD1LSTM, contextD2LSTM, queryD1LSTM, queryD2LSTM,
                    //        stateStruct, attStruct, att2Struct, ansStruct, terminalStruct, modelStructure, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, BuilderParameters.ScoreOutputPath + ".Test");

                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidContextL3G, DataPanel.ValidQueryL3G, DataPanel.ValidContextChar, DataPanel.ValidQueryChar, DataPanel.ValidStartPos, DataPanel.ValidEndPos,
                            wordembedLayer, l3gembedLayer, charembedLayer, highwayGate, highwayConnect, contextD1LSTM, contextD2LSTM, CNNembed, queryD1LSTM, queryD2LSTM, CNNembed,
                            CoAttLayer, M1CNN, M2CNN, M3CNN, AnsStartStruct, AnsEndStruct, InitStateStruct, AttStruct, StateStruct, 
                            StartTermalStruct, EndTermalStruct, modelStructure,
                            new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib }, string.Empty);

                    double bestValidScore = double.MinValue; // validCG.Execute(); //double.MinValue;
                    //double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                modelStructure.Serialize(writer);
                            }
                        }

                        double validScore = 0;
                        if (validCG != null)
                        {
                            validScore = validCG.Execute();
                            Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, iter);
                        }

                        if (validScore > bestValidScore)
                        {
                            bestValidScore = validScore; bestIter = iter;
                        }
                        Logger.WriteLog("Best Valid Score {0},  At iter {1}", bestValidScore, bestIter);
                    }
                    break;
                case DNNRunMode.Predict:
                    break;
            }
            DataPanel.Deinit();
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQuery = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContextL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQueryL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainContextChar = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> TrainQueryChar = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainStartPos = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainEndPos = null;

            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContext = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQuery = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContextL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQueryL3G = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidContextChar = null;
            public static DataCashier<SeqSparseBatchData, SequenceDataStat> ValidQueryChar = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidStartPos = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidEndPos = null;
            public static string[] ValidIds = null;
            public static List<HashSet<string>> ValidResults = new List<HashSet<string>>();

            public static ItemFreqIndexDictionary wordFreqDict = null;
            public static ItemFreqIndexDictionary l3gFreqDict = null;
            public static ItemFreqIndexDictionary charFreqDict = null;
            static Dictionary<int, List<int>> word2l3g = new Dictionary<int, List<int>>();
            static Dictionary<int, List<int>> word2char = new Dictionary<int, List<int>>();
            public static int L3gSize { get { return l3gFreqDict.ItemDictSize; } }
            public static int WordSize { get { return wordFreqDict.ItemDictSize; } }
            public static int CharSize { get { return charFreqDict.ItemDictSize; } }

            public static int MaxQueryLen = 0;
            public static int MaxDocLen = 0;
            public static int MaxCrossQDLen = 0;
            static IEnumerable<Tuple<string, string, string>> ExtractQAS(string paraFile, string queryFile, string ansFile)
            {
                using (StreamReader paraReader = new StreamReader(paraFile))
                using (StreamReader queryReader = new StreamReader(queryFile))
                using (StreamReader ansReader = new StreamReader(ansFile))
                {
                    int lineIdx = 0;
                    while (!ansReader.EndOfStream)
                    {
                        string p = paraReader.ReadLine();
                        string q = queryReader.ReadLine();
                        string a = ansReader.ReadLine();
                        //Console.WriteLine("{0},{1}", p, q);
                        yield return new Tuple<string, string, string>(p, q, a);
                        lineIdx += 1;
                    }
                    Console.WriteLine("Total Number of answers {0}", lineIdx);
                }
            }

            static Tuple<int, int> GetSpan(List<string[]> sentenceIdxs, int startSent, int startIdx, int endSent, int endIdx)
            {
                if (startSent == -1 || startIdx == -1 || endSent == -1 || endIdx == -1)
                {
                    return new Tuple<int, int>(-1, -1);
                }
                if (sentenceIdxs.Count <= startSent || sentenceIdxs.Count <= endSent)
                {
                    Console.WriteLine("Sentence number is not enough! {0}, start {1}, end {2}", sentenceIdxs.Count, startSent, endSent);
                    Console.ReadLine();
                }

                if (startSent != endSent)
                {
                    Console.WriteLine("Sentence start is not equals to Sentence end! {0} and {1}", startSent, endSent);
                }

                int answerSentStart = sentenceIdxs.Take(startSent).Sum(i => i.Length) + startIdx;
                int answerSentEnd = sentenceIdxs.Take(endSent).Sum(i => i.Length) + endIdx;

                return new Tuple<int, int>(answerSentStart, answerSentEnd);
            }

            static void ExtractCorpusBinary(string paraFile, string queryFile, string ansFile, string paraFileBin, string queryFileBin, string ansFileBin, bool isShuffle, int miniBatchSize,
                int spanSize, List<HashSet<string>> resultSave)
            {
                SeqSparseBatchData context = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData query = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = WordSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData contextL3g = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = L3gSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData queryL3g = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = L3gSize, MAX_BATCHSIZE = miniBatchSize }, DeviceType.CPU);
                SeqSparseBatchData contextChar = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = CharSize }, DeviceType.CPU);
                SeqSparseBatchData queryChar = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = CharSize }, DeviceType.CPU);

                DenseBatchData ansStart = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);
                DenseBatchData ansEnd = new DenseBatchData(new DenseDataStat() { Dim = 1 }, DeviceType.CPU);

                BinaryWriter contextWriter = FileUtil.CreateBinaryWrite(paraFileBin);
                BinaryWriter queryWriter = FileUtil.CreateBinaryWrite(queryFileBin);
                BinaryWriter contextL3GWriter = FileUtil.CreateBinaryWrite(paraFileBin + ".l3g");
                BinaryWriter queryL3GWriter = FileUtil.CreateBinaryWrite(queryFileBin + ".l3g");
                BinaryWriter contextCharWriter = FileUtil.CreateBinaryWrite(paraFileBin + ".char");
                BinaryWriter queryCharWriter = FileUtil.CreateBinaryWrite(queryFileBin + ".char");
                BinaryWriter ansStartWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".1");
                BinaryWriter ansEndWriter = FileUtil.CreateBinaryWrite(ansFileBin + ".2");

                IEnumerable<Tuple<string, string, string>> QAS = ExtractQAS(paraFile, queryFile, ansFile);

                IEnumerable<Tuple<string, string, string>> Input = QAS;
                if (isShuffle) Input = CommonExtractor.RandomShuffle<Tuple<string, string, string>>(QAS, 1000000, DeepNet.BuilderParameters.RandomSeed);

                int crossLen = 0;
                int lineIdx = 0;
                int overSpanfilterNum = 0;
                int notFoundfilterNum = 0;
                foreach (Tuple<string, string, string> smp in Input)
                {
                    string p = smp.Item1;
                    string q = smp.Item2;
                    string a = smp.Item3;

                    string[] poses = a.Split(new char[] { ',', ')', '(', '#' }, StringSplitOptions.RemoveEmptyEntries);
                    List<string[]> sentenceIdxs = p.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries).
                        Select(i => i.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)).ToList();
                    string[] queryIdxs = q.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    int answerSentStart = -1;
                    int answerSentEnd = -1;
                    if (resultSave != null) { resultSave.Add(new HashSet<string>()); }
                    for (int m = 0; m < poses.Length / 4; m++)
                    {
                        int startSent = int.Parse(poses[4 * m + 0]);
                        int startIdx = int.Parse(poses[4 * m + 1]);
                        int endSent = int.Parse(poses[4 * m + 2]);
                        int endIdx = int.Parse(poses[4 * m + 3]);
                        Tuple<int, int> r = GetSpan(sentenceIdxs, startSent, startIdx, endSent, endIdx);
                        answerSentStart = r.Item1;
                        answerSentEnd = r.Item2;

                        if (resultSave != null) { resultSave[lineIdx].Add(string.Format("{0}#{1}", answerSentStart, answerSentEnd)); }
                    }

                    if (spanSize > 0 && answerSentEnd - answerSentStart + 1 > spanSize) { overSpanfilterNum++; continue; }
                    if (resultSave == null && (answerSentEnd == -1 || answerSentStart == -1)) { notFoundfilterNum++; continue; }

                    List<Dictionary<int, float>> contextFea = new List<Dictionary<int, float>>();
                    List<Dictionary<int, float>> contextL3GFea = new List<Dictionary<int, float>>();

                    int cpushCount = 0;
                    int preBatchSize = contextChar.BatchSize;
                    foreach (string[] term in sentenceIdxs)
                    {
                        foreach (string id in term)
                        {
                            int termIdx = int.Parse(id);
                            var tmp = new Dictionary<int, float>();
                            tmp[termIdx] = 1;
                            contextFea.Add(tmp);
                            var l3gtmp = new Dictionary<int, float>();
                            foreach (int l3gid in word2l3g[termIdx])
                            {
                                if (l3gtmp.ContainsKey(l3gid))
                                {
                                    l3gtmp[l3gid] += 1;
                                }
                                else
                                {
                                    l3gtmp.Add(l3gid, 1);
                                }
                            }
                            contextL3GFea.Add(l3gtmp);

                            List<Dictionary<int, float>> contextCharFea = new List<Dictionary<int, float>>();
                            foreach (int cid in word2char[termIdx])
                            {
                                var chartmp = new Dictionary<int, float>();
                                chartmp[cid] = 1;
                                contextCharFea.Add(chartmp);
                            }
                            cpushCount++;
                            contextChar.PushSample(contextCharFea);
                        }
                    }

                    List<Dictionary<int, float>> queryFea = new List<Dictionary<int, float>>();
                    List<Dictionary<int, float>> queryL3GFea = new List<Dictionary<int, float>>();
                    int qpushCount = 0;
                    foreach (string term in queryIdxs)
                    {
                        int termIdx = int.Parse(term);
                        var tmp = new Dictionary<int, float>();
                        tmp[termIdx] = 1;
                        queryFea.Add(tmp);
                        var l3gtmp = new Dictionary<int, float>();
                        foreach (int l3gid in word2l3g[termIdx])
                        {
                            if (l3gtmp.ContainsKey(l3gid))
                            {
                                l3gtmp[l3gid] += 1;
                            }
                            else
                            {
                                l3gtmp.Add(l3gid, 1);
                            }
                        }
                        queryL3GFea.Add(l3gtmp);

                        List<Dictionary<int, float>> queryCharFea = new List<Dictionary<int, float>>();
                        foreach (int cid in word2char[termIdx])
                        {
                            var chartmp = new Dictionary<int, float>();
                            chartmp[cid] = 1;
                            queryCharFea.Add(chartmp);
                        }
                        qpushCount++;
                        queryChar.PushSample(queryCharFea);
                    }

                    
                    float[] tmpAnsStart = new float[contextFea.Count];
                    float[] tmpAnsEnd = new float[contextFea.Count];
                    for (int i = 0; i < contextFea.Count; i++)
                    {
                        tmpAnsStart[i] = 0;
                        tmpAnsEnd[i] = 0;
                        if (i == answerSentStart) { tmpAnsStart[i] = 1; }
                        if (i == answerSentEnd) { tmpAnsEnd[i] = 1; }
                    }

                    if (contextFea.Count > MaxDocLen) { MaxDocLen = contextFea.Count; }
                    if (queryFea.Count > MaxQueryLen) { MaxQueryLen = queryFea.Count; }
                    crossLen += contextFea.Count * queryFea.Count;
                    if (crossLen > MaxCrossQDLen) { MaxCrossQDLen = crossLen; }

                    context.PushSample(contextFea);
                    query.PushSample(queryFea);
                    contextL3g.PushSample(contextL3GFea);
                    queryL3g.PushSample(queryL3GFea);
                    ansStart.PushSample(tmpAnsStart, contextFea.Count);
                    ansEnd.PushSample(tmpAnsEnd, contextFea.Count);


                    if (query.SentSize != queryChar.BatchSize) { throw new Exception(string.Format("query char size should equal {0} and {1}", query.SentSize, queryChar.BatchSize)); }
                    if (context.SentSize != contextChar.BatchSize) { throw new Exception(string.Format("context char size should equal {0} and {1}", context.SentSize, contextChar.BatchSize)); }

                    if (query.SentSize != queryL3g.SentSize) { throw new Exception(string.Format("query l3g size should equal {0} and {1}", query.SentSize, queryL3g.SentSize)); }
                    if (context.SentSize != contextL3g.SentSize) { throw new Exception(string.Format("context l3g size should equal {0} and {1}", query.SentSize, contextL3g.SentSize)); }


                    if (answerSentEnd < answerSentStart)
                    {
                        Console.WriteLine("answer sent end smaller than answer sent start {0} {1} {2} ### {3}", lineIdx, answerSentEnd, answerSentStart, string.Join("##", poses));
                        Console.ReadLine();
                    }
                    if (context.BatchSize >= miniBatchSize)
                    {
                        context.PopBatchToStat(contextWriter);
                        query.PopBatchToStat(queryWriter);
                        contextL3g.PopBatchToStat(contextL3GWriter);
                        queryL3g.PopBatchToStat(queryL3GWriter);
                        contextChar.PopBatchToStat(contextCharWriter);
                        queryChar.PopBatchToStat(queryCharWriter);
                        ansStart.PopBatchToStat(ansStartWriter);
                        ansEnd.PopBatchToStat(ansEndWriter);
                        crossLen = 0;
                    }

                    if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }

                }
                context.PopBatchCompleteStat(contextWriter);
                query.PopBatchCompleteStat(queryWriter);
                contextL3g.PopBatchCompleteStat(contextL3GWriter);
                queryL3g.PopBatchCompleteStat(queryL3GWriter);
                contextChar.PopBatchCompleteStat(contextCharWriter);
                queryChar.PopBatchCompleteStat(queryCharWriter);
                ansStart.PopBatchCompleteStat(ansStartWriter);
                ansEnd.PopBatchCompleteStat(ansEndWriter);

                if (resultSave != null)
                {
                    using (StreamWriter ansWriter = new StreamWriter(ansFile + ".index"))
                    {
                        foreach (HashSet<string> results in resultSave)
                        {
                            ansWriter.WriteLine(string.Join("\t", results.ToArray()));
                        }
                    }
                }
                Console.WriteLine("Context Stat {0}", context.Stat.ToString());
                Console.WriteLine("Query Stat {0}", query.Stat.ToString());
                Console.WriteLine("Context l3g Stat {0}", contextL3g.Stat.ToString());
                Console.WriteLine("Query l3g Stat {0}", queryL3g.Stat.ToString());
                Console.WriteLine("Context char Stat {0}", contextChar.Stat.ToString());
                Console.WriteLine("Query char Stat {0}", queryChar.Stat.ToString());
                Console.WriteLine("Answer Start Stat {0}", ansStart.Stat.ToString());
                Console.WriteLine("Answer End Stat {0}", ansEnd.Stat.ToString());
                Console.WriteLine("Max Q Len {0}, Max D Len {1}, Max QD Cross Len {2}", MaxQueryLen, MaxDocLen, MaxCrossQDLen);
                Console.WriteLine("OverSpan Filter Number {0}, Not Found Filter Number {1}", overSpanfilterNum, notFoundfilterNum);
            }

            public static void Deinit()
            {
                string paraFileBin = BuilderParameters.ValidContextBin;
                string queryFileBin = BuilderParameters.ValidQueryBin;
                string ansFileBin = BuilderParameters.ValidPosBin;

                File.Delete(paraFileBin);
                File.Delete(paraFileBin + ".l3g");
                File.Delete(paraFileBin + ".char");

                File.Delete(queryFileBin);
                File.Delete(queryFileBin + ".l3g");
                File.Delete(queryFileBin + ".char");

                File.Delete(ansFileBin + ".1");
                File.Delete(ansFileBin + ".2");
            }

            public static void Init()
            {
                #region Preprocess Data.
                // Step 1 : Load Word Vocab .
                {
                    using (StreamReader mreader = new StreamReader(BuilderParameters.Vocab))
                    {
                        wordFreqDict = new ItemFreqIndexDictionary(mreader, false);
                    }
                }

                SimpleTextTokenizer textTokenize = new SimpleTextTokenizer();

                // Step 2 : Extract L3G vocab from word vocab.
                l3gFreqDict = new ItemFreqIndexDictionary("l3g dictionary");
                foreach (KeyValuePair<string, int> wordItem in wordFreqDict.ItemIndex.NameDict)
                {
                    int wordIdx = wordItem.Value;
                    string wordStr = wordItem.Key;
                    word2l3g.Add(wordIdx, new List<int>());
                    IEnumerable<string> strList = textTokenize.LnGTokenize(wordStr, 3);
                    foreach (string ngram in strList)
                    {
                        int ngramIdx = l3gFreqDict.ItemIndex.Index(ngram, true);
                        word2l3g[wordIdx].Add(ngramIdx);
                    }
                    if (word2l3g[wordIdx].Count == 0) { Console.WriteLine("No L3g is found for {0}", wordStr); }
                }
                Console.WriteLine("l3g vocab size {0}", l3gFreqDict.ItemDictSize);

                // Step 3 : Extract char vocab from word vocab.
                charFreqDict = new ItemFreqIndexDictionary("char dictionary");
                foreach (KeyValuePair<string, int> wordItem in wordFreqDict.ItemIndex.NameDict)
                {
                    int wordIdx = wordItem.Value;
                    string wordStr = wordItem.Key;
                    word2char.Add(wordIdx, new List<int>());
                    IEnumerable<string> strList = textTokenize.LetterTokenize(wordStr);
                    foreach (string c in strList)
                    {
                        int cIdx = charFreqDict.ItemIndex.Index(c, true);
                        word2char[wordIdx].Add(cIdx);
                    }
                    if (word2char[wordIdx].Count == 0) { Console.WriteLine("No Char is found for {0}", wordStr); }
                }
                Console.WriteLine("char vocab size {0}", charFreqDict.ItemDictSize);
                //Console.ReadLine();

                // Step 4 : Index 2 Binary Data.
                if (BuilderParameters.IsTrainFile)
                //&& 
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.TrainQueryBin)) &&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.TrainContextBin)))
                {
                    ExtractCorpusBinary(
                        BuilderParameters.TrainContext, BuilderParameters.TrainQuery, BuilderParameters.TrainPos,
                        BuilderParameters.TrainContextBin, BuilderParameters.TrainQueryBin, BuilderParameters.TrainPosBin,
                        true, BuilderParameters.MiniBatchSize, BuilderParameters.TRAIN_SPAN_LENGTH, null);
                }

                if (BuilderParameters.IsValidFile)
                //&&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.ValidQueryBin)) &&
                //!FileUtil.IsFileinUse(new FileInfo(BuilderParameters.ValidContextBin)))
                {
                    ExtractCorpusBinary(
                        BuilderParameters.ValidContext, BuilderParameters.ValidQuery, BuilderParameters.ValidPos,
                        BuilderParameters.ValidContextBin, BuilderParameters.ValidQueryBin, BuilderParameters.ValidPosBin,
                        false, BuilderParameters.MiniBatchSize, 0, ValidResults);
                }
                else
                {
                    using (StreamReader ansReader = new StreamReader(BuilderParameters.ValidPos + ".index"))
                    {
                        while (!ansReader.EndOfStream)
                        {
                            string[] items = ansReader.ReadLine().Split('\t');
                            ValidResults.Add(new HashSet<string>(items));
                        }
                    }
                }
                ValidIds = File.ReadAllLines(BuilderParameters.ValidIds);
                #endregion.

                #region Load Data.
                if (BuilderParameters.IsTrainFile)
                {
                    TrainContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin);
                    TrainQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin);
                    TrainStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".1");
                    TrainEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainPosBin + ".2");
                    TrainContextL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin + ".l3g");
                    TrainQueryL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin + ".l3g");
                    TrainContextChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainContextBin + ".char");
                    TrainQueryChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.TrainQueryBin + ".char");

                    TrainContext.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQuery.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainContextL3G.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQueryL3G.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainContextChar.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainQueryChar.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainStartPos.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                    TrainEndPos.InitThreadSafePipelineCashier(64, ParameterSetting.RANDOM_SEED);
                }

                if (BuilderParameters.IsValidFile)
                {
                    ValidContext = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin);
                    ValidQuery = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin);
                    ValidStartPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".1");
                    ValidEndPos = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidPosBin + ".2");
                    ValidContextL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin + ".l3g");
                    ValidQueryL3G = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin + ".l3g");
                    ValidContextChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidContextBin + ".char");
                    ValidQueryChar = new DataCashier<SeqSparseBatchData, SequenceDataStat>(BuilderParameters.ValidQueryBin + ".char");

                    ValidContext.InitThreadSafePipelineCashier(64, false);
                    ValidQuery.InitThreadSafePipelineCashier(64, false);
                    ValidContextL3G.InitThreadSafePipelineCashier(64, false);
                    ValidQueryL3G.InitThreadSafePipelineCashier(64, false);
                    ValidContextChar.InitThreadSafePipelineCashier(64, false);
                    ValidQueryChar.InitThreadSafePipelineCashier(64, false);
                    ValidStartPos.InitThreadSafePipelineCashier(64, false);
                    ValidEndPos.InitThreadSafePipelineCashier(64, false);
                }
                #endregion.
            }
        }
    }
}
