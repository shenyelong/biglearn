﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;namespace BigLearn.DeepNet
{
    public class AppGraphEmbedV2Builder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
            public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }

            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string ValidData { get { return Argument["VALID"].Value; } }
            public static string TestData { get { return Argument["TEST"].Value; } }

            public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
            public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int IN_EmbedDim { get { return int.Parse(Argument["IN-EMBED-DIM"].Value); } }

            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int OUT_EmbedDim { get { return int.Parse(Argument["OUT-EMBED-DIM"].Value); } }
            public static int MEM_EmbedDim { get { return int.Parse(Argument["MEM-EMBED-DIM"].Value); } }
            public static int MemorySize { get { return int.Parse(Argument["MEMORY-SIZE"].Value); } }
            public static int ATT_DIM { get { return int.Parse(Argument["ATT-DIM"].Value); } }
            public static CrossSimType AttType { get { return (CrossSimType)int.Parse(Argument["ATT-TYPE"].Value); } }

            public static int NTRIAL { get { return int.Parse(Argument["NTRIAL"].Value); } }

            public static int PRED_ITER { get { return int.Parse(Argument["PRED-ITER"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }

            public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
            public static bool IS_NORM_REWARD { get { return int.Parse(Argument["IS-NORM-REWARD"].Value) > 0; } }
            public static ObjectiveFunc OBJ_FUNC { get { return (ObjectiveFunc)int.Parse(Argument["OBJECT-FUNC"].Value); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                Argument.Add("ENTITY2ID", new ParameterArgument(string.Empty, "Entity 2 ID."));
                Argument.Add("RELATION2ID", new ParameterArgument(string.Empty, "Relation 2 ID."));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));


                Argument.Add("IN-EMBED-DIM", new ParameterArgument("100", "In Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "In Relation Embedding Dim"));
                Argument.Add("MEMORY-SIZE", new ParameterArgument("64", "Memory Size."));
                Argument.Add("MEM-EMBED-DIM", new ParameterArgument("200", "Memory Embedding Dim"));

                Argument.Add("OUT-EMBED-DIM", new ParameterArgument("100", "Out Node Embedding Dim"));
                
                Argument.Add("ATT-DIM", new ParameterArgument("200", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("NTRIAL", new ParameterArgument("1", "Negative samples"));
                Argument.Add("PRED-ITER", new ParameterArgument("20", "Predict per itertion"));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("6", "Recurrent Step."));

                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));
                Argument.Add("OBJECT-FUNC", new ParameterArgument((((int)ObjectiveFunc.FullSoftmax)).ToString(), "Objective Type : " + ParameterUtil.EnumValues(typeof(ObjectiveFunc))));


                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));

                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score File."));

                //Argument.Add("EPS-SCHEDULE", new ParameterArgument("0:1,5000000:1,50000000:1,100000000:1,200000000:0.1,400000000:0.05", "Epslion Schedule."));
                //Argument.Add("HOP-SCHEDULE", new ParameterArgument("0:0,5000000:0,50000000:0,100000000:0,200000000:1,400000000:2", "Hop Schedule."));
                //Argument.Add("EBD-LR-SCHEDULE", new ParameterArgument("0:0.01,5000000:0.005,50000000:0.001,100000000:0.0005,200000000:0.0001", "EMD Lr Schedule."));
                //Argument.Add("ATT-LR-SCHEDULE", new ParameterArgument("0:0,5000000:0,50000000:0.01,100000000:0.005,200000000:0.001,400000000:0.0001", "Hop Schedule."));

                //Argument.Add("MAX-NEIGHBOR", new ParameterArgument("10", "Maximum Neighbor Set."));

                //Argument.Add("NTRAIL", new ParameterArgument("50", "Number of Negative Samples"));
                //Argument.Add("L1", new ParameterArgument("1", "L1 Loss"));
                //Argument.Add("MARGIN", new ParameterArgument("1", "Margin Parameter"));

                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_GRAPH_EMBED_V2; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public static List<Tuple<int, float>> ScheduleEpsilon(string schedule)
        {
            return schedule.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
        }
        public float Schedule(string schedule, int step)
        {
            List<Tuple<int, float>> scheduleList = ScheduleEpsilon(schedule);
            for (int i = 0; i < scheduleList.Count; i++)
            {
                if (step < scheduleList[i].Item1)
                {
                    float lambda = (step - scheduleList[i - 1].Item1) * 1.0f / (scheduleList[i].Item1 - scheduleList[i - 1].Item1);
                    return lambda * scheduleList[i].Item2 + (1 - lambda) * scheduleList[i - 1].Item2;
                }
            }
            return scheduleList.Last().Item2;
        }

        /// <summary>
        /// Sampleing Graph.
        /// </summary>
        class SampleEmbedRunner : StructRunner
        {
            List<Tuple<int, int, int>> Graph { get; set; }

            public HiddenBatchData Status { get; set; }
            public CudaPieceFloat Label { get; set; }

            public int NTrialNum = BuilderParameters.NTRIAL;
            public CudaPieceInt TargetIndex;

            EmbedStructure InNodeEmbed { get; set; }

            EmbedStructure LinkEmbed { get; set; }

            int MiniBatchSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            //FastVectorOperation FastVO { get; set; }

            /// <summary>
            /// Tuple (source, target, link).
            /// </summary>
            public List<Tuple<int, int, int>> BatchLinks = new List<Tuple<int, int, int>>();

            public SampleEmbedRunner(List<Tuple<int, int, int>> graph, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, int batchSize, RunnerBehavior behavior) :
                base(Structure.Empty, behavior)
            {
                Graph = graph;
                InNodeEmbed = inNodeEmbed;
                LinkEmbed = linkEmbed;

                MiniBatchSize = batchSize;
                Label = new CudaPieceFloat(MiniBatchSize, true, Behavior.Device == DeviceType.GPU);
                TargetIndex = new CudaPieceInt(MiniBatchSize * (NTrialNum + 1), true, Behavior.Device == DeviceType.GPU);

                Status = new HiddenBatchData(MiniBatchSize, InNodeEmbed.Dim, Behavior.RunMode, Behavior.Device);

                Shuffle = new DataRandomShuffling(Graph.Count * 2);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                InNodeEmbed.Embedding.SyncToCPU();
                LinkEmbed.Embedding.SyncToCPU();

                BatchLinks.Clear();
                int batchSize = 0;
                while (batchSize < MiniBatchSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }

                    int srcId = idx < Graph.Count ? Graph[idx].Item1 : Graph[idx - Graph.Count].Item2;
                    int tgtId = idx < Graph.Count ? Graph[idx].Item2 : Graph[idx - Graph.Count].Item1;

                    int linkId = idx < Graph.Count ? Graph[idx].Item3 : (Graph[idx - Graph.Count].Item3 + DataPanel.RelationNum);

                    BatchLinks.Add(new Tuple<int, int, int>(srcId, tgtId, linkId));

                    Label.MemPtr[batchSize] = tgtId;

                    TargetIndex.MemPtr[batchSize * (NTrialNum + 1)] = tgtId;
                    HashSet<int> negHash = new HashSet<int>();
                    while (negHash.Count < NTrialNum)
                    {
                        int negId = -1;
                        do
                        {
                            negId = Util.URandom.Next(DataPanel.EntityNum);
                        } while (DataPanel.IsInGraph(srcId, negId, linkId) || negHash.Contains(negId));
                        negHash.Add(negId);
                        TargetIndex.MemPtr[batchSize * (NTrialNum + 1) + negHash.Count] = negId;
                    }

                    FastVector.Add_Vector(Status.Output.Data.MemPtr, batchSize * Status.Dim, InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);
                    if (linkId < DataPanel.RelationNum)
                    {
                        FastVector.Add_Vector(Status.Output.Data.MemPtr, batchSize * Status.Dim, LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 1, 1);
                    }
                    else
                    {
                        FastVector.Add_Vector(Status.Output.Data.MemPtr, batchSize * Status.Dim, LinkEmbed.Embedding.MemPtr, (linkId - DataPanel.RelationNum) * LinkEmbed.Dim, LinkEmbed.Dim, 1, -1);
                    }
                    batchSize += 1;
                }

                Status.BatchSize = batchSize;
                if (batchSize == 0) { IsTerminate = true; return; }

                Label.SyncFromCPU(batchSize);
                TargetIndex.SyncFromCPU(batchSize * (NTrialNum + 1));
                Status.Output.Data.SyncFromCPU(Status.BatchSize * Status.Dim);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Status.Deriv.Data, Status.Dim * Status.BatchSize);
            }

            public override void Update()
            {
                Status.Deriv.Data.SyncToCPU(Status.BatchSize * Status.Dim);

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

                for (int b = 0; b < Status.BatchSize; b++)
                {
                    int srcIdx = BatchLinks[b].Item1;
                    int tgtIdx = BatchLinks[b].Item2;
                    int linkIdx = BatchLinks[b].Item3;

                    InNodeEmbed.EmbeddingOptimizer.PushSparseGradient(srcIdx * InNodeEmbed.Dim, InNodeEmbed.Dim);
                    FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim, Status.Deriv.Data.MemPtr, b * Status.Dim, InNodeEmbed.Dim, 1, InNodeEmbed.EmbeddingOptimizer.GradientStep);

                    if (linkIdx < DataPanel.RelationNum)
                    {
                        LinkEmbed.EmbeddingOptimizer.PushSparseGradient(linkIdx * LinkEmbed.Dim, LinkEmbed.Dim);
                        FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim, Status.Deriv.Data.MemPtr, b * Status.Dim, LinkEmbed.Dim, 1, LinkEmbed.EmbeddingOptimizer.GradientStep);
                    }
                    else
                    {
                        LinkEmbed.EmbeddingOptimizer.PushSparseGradient((linkIdx - DataPanel.RelationNum) * LinkEmbed.Dim, LinkEmbed.Dim);
                        FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, (linkIdx - DataPanel.RelationNum) * LinkEmbed.Dim, Status.Deriv.Data.MemPtr, b * Status.Dim, LinkEmbed.Dim, 1, -LinkEmbed.EmbeddingOptimizer.GradientStep);
                    }
                }

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();

            }
        }


        /// <summary>
        /// Sampleing Graph.
        /// </summary>
        class SampleEmbedV2Runner : StructRunner
        {
            List<Tuple<int, int, int>> Graph { get; set; }

            public HiddenBatchData Status { get; set; }
            public CudaPieceFloat Label { get; set; }

            public int NTrialNum = BuilderParameters.NTRIAL;
            public CudaPieceInt TargetIndex;

            EmbedStructure InNodeEmbed { get; set; }

            EmbedStructure LinkEmbed { get; set; }

            int MiniBatchSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }

            /// <summary>
            /// Tuple (source, target, link).
            /// </summary>
            public List<Tuple<int, int, int>> BatchLinks = new List<Tuple<int, int, int>>();

            public SampleEmbedV2Runner(List<Tuple<int, int, int>> graph, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, int batchSize, RunnerBehavior behavior) :
                base(Structure.Empty, behavior)
            {
                Graph = graph;
                InNodeEmbed = inNodeEmbed;
                LinkEmbed = linkEmbed;

                MiniBatchSize = batchSize;

                int statusDim = InNodeEmbed.Dim + LinkEmbed.Dim;

                Label = new CudaPieceFloat(MiniBatchSize, true, Behavior.Device == DeviceType.GPU);
                TargetIndex = new CudaPieceInt(MiniBatchSize * (NTrialNum + 1), true, Behavior.Device == DeviceType.GPU);

                Status = new HiddenBatchData(MiniBatchSize, statusDim, Behavior.RunMode, Behavior.Device);

                Shuffle = new DataRandomShuffling(Graph.Count * 2);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                // Perf is not threadSafe.
                //var dataCounter = PerfCounter.Manager.Instance["Data"].Begin(); 
                
                InNodeEmbed.Embedding.SyncToCPU();
                LinkEmbed.Embedding.SyncToCPU();

                BatchLinks.Clear();
                int batchSize = 0;
                while (batchSize < MiniBatchSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }

                    int srcId = idx < Graph.Count ? Graph[idx].Item1 : Graph[idx - Graph.Count].Item2;
                    int tgtId = idx < Graph.Count ? Graph[idx].Item2 : Graph[idx - Graph.Count].Item1;

                    int linkId = idx < Graph.Count ? Graph[idx].Item3 : (Graph[idx - Graph.Count].Item3 + DataPanel.RelationNum);

                    BatchLinks.Add(new Tuple<int, int, int>(srcId, tgtId, linkId));

                    Label.MemPtr[batchSize] = tgtId;

                    TargetIndex.MemPtr[batchSize * (NTrialNum + 1)] = tgtId;
                    HashSet<int> negHash = new HashSet<int>();
                    while (negHash.Count < NTrialNum)
                    {
                        int negId = -1;
                        do
                        {
                            negId = Util.URandom.Next(DataPanel.EntityNum);
                        } while (DataPanel.IsInGraph(srcId, negId, linkId) || negHash.Contains(negId));
                        negHash.Add(negId);
                        TargetIndex.MemPtr[batchSize * (NTrialNum + 1) + negHash.Count] = negId;
                    }

                    FastVector.Add_Vector(Status.Output.Data.MemPtr, batchSize * Status.Dim, 
                                            InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);

                    FastVector.Add_Vector(Status.Output.Data.MemPtr, batchSize * Status.Dim + InNodeEmbed.Dim, 
                                            LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);
                    batchSize += 1;
                }

                Status.BatchSize = batchSize;

                if (batchSize == 0) { IsTerminate = true; return; }

                Label.SyncFromCPU(batchSize);
                TargetIndex.SyncFromCPU(batchSize * (NTrialNum + 1));
                Status.Output.Data.SyncFromCPU(Status.BatchSize * Status.Dim);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Status.Deriv.Data, Status.Dim * Status.BatchSize);
            }

            public override void Update()
            {

                Status.Deriv.Data.SyncToCPU(Status.Dim * Status.BatchSize);

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

                for (int b = 0; b < Status.BatchSize; b++)
                {
                    int srcIdx = BatchLinks[b].Item1;
                    int tgtIdx = BatchLinks[b].Item2;
                    int linkIdx = BatchLinks[b].Item3;

                    InNodeEmbed.EmbeddingOptimizer.PushSparseGradient(srcIdx * InNodeEmbed.Dim, InNodeEmbed.Dim);
                    LinkEmbed.EmbeddingOptimizer.PushSparseGradient(linkIdx * LinkEmbed.Dim, LinkEmbed.Dim);

                    FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
                                    Status.Deriv.Data.MemPtr, b * Status.Dim, InNodeEmbed.Dim, 1, InNodeEmbed.EmbeddingOptimizer.GradientStep);
                    FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim, 
                                    Status.Deriv.Data.MemPtr, b * Status.Dim + InNodeEmbed.Dim, LinkEmbed.Dim, 1, LinkEmbed.EmbeddingOptimizer.GradientStep);
                }

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();

            }
        }

        enum DistanceType { L1Dist, L2Dist }
        class EmbedDistanceRunner : StructRunner
        {
            /// <summary>
            /// one model can be used in two places.
            /// </summary>
            public new EmbedStructure Model { get { return (EmbedStructure)base.Model; } set { base.Model = value; } }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public new HiddenBatchData Input { get { return (HiddenBatchData)base.Input; } set { base.Input = value; } }

            CudaPieceInt Index { get; set; }
            int Dim { get; set; }

            RepeatBiMatchRunner FullMatchRunner = null;
            BiMatchBatchData MatchData;

            HiddenBatchData TmpOutput;

            DistanceType Distance = DistanceType.L1Dist;
            public EmbedDistanceRunner(ComputationGraph CG, EmbedStructure model, HiddenBatchData input, CudaPieceInt index, int dim, DistanceType d, RunnerBehavior behavior) : base(model, behavior)
            {
                Input = input;
                Index = index;

                IntArgument batchSizeArg = new IntArgument("batchSizeArg");
                CG.AddRunner(new HiddenDataBatchSizeRunner(Input, batchSizeArg, behavior));

                Distance = d;
                if (Index == null)
                {
                    Dim = Model.VocabSize;
                    FullMatchRunner = new RepeatBiMatchRunner(Input.MAX_BATCHSIZE, Model.VocabSize, batchSizeArg, Behavior);
                    //rRunner.Forward();
                    MatchData = FullMatchRunner.Output;
                }
                else
                {
                    Dim = dim;
                    MatchData = new BiMatchBatchData(new BiMatchBatchDataStat() { MAX_SRC_BATCHSIZE = Input.MAX_BATCHSIZE, MAX_TGT_BATCHSIZE = Model.VocabSize, MAX_MATCH_BATCHSIZE = Dim * Input.MAX_BATCHSIZE }, Behavior.Device);
                }

                TmpOutput = new HiddenBatchData(Input.MAX_BATCHSIZE * Dim, Model.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(Input.MAX_BATCHSIZE, Dim, Behavior.RunMode, Behavior.Device);

            }

            public override void Forward()
            {
                if (Index != null)
                {
                    List<Tuple<int, int, float>> matchList = new List<Tuple<int, int, float>>();
                    Index.SyncToCPU(Input.BatchSize * Dim);
                    for (int b = 0; b < Input.BatchSize; b++)
                    {
                        for (int d = 0; d < Dim; d++)
                        {
                            matchList.Add(new Tuple<int, int, float>(b, Index.MemPtr[b * Dim + d], 1));
                        }
                    }
                    MatchData.SetMatch(matchList);
                }

                Input.Output.Data.SyncToCPU(Input.BatchSize * Input.Dim);
                Model.Embedding.SyncToCPU(Model.Dim * Model.VocabSize);

                MathOperatorManager.CreateInstance(DeviceType.CPU_FAST_VECTOR).Matrix_AdditionMask(Input.Output.Data, 0, MatchData.SrcIdx, 0,
                                               Model.Embedding, 0, MatchData.TgtIdx, 0,
                                               TmpOutput.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Model.Dim, Input.BatchSize * Dim, 1, -1, 0);
                TmpOutput.Output.Data.SyncFromCPU(Input.BatchSize * Dim * Model.Dim);


                Output.BatchSize = Input.BatchSize;

                if (Distance == DistanceType.L1Dist)
                    ComputeLib.MatrixL1Norm(TmpOutput.Output.Data, Model.Dim, Input.BatchSize * Dim, Output.Output.Data);
                else if (Distance == DistanceType.L2Dist)
                    ComputeLib.MatrixL2Norm(TmpOutput.Output.Data, Model.Dim, Input.BatchSize * Dim, Output.Output.Data);

                Output.Output.Data.SyncToCPU();
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                if (Distance == DistanceType.L1Dist)
                    ComputeLib.DerivMatrixL1Norm(TmpOutput.Output.Data, Model.Dim, Input.BatchSize * Dim, Output.Output.Data, Output.Deriv.Data, TmpOutput.Deriv.Data);
                else if (Distance == DistanceType.L2Dist)
                    ComputeLib.DerivMatrixL2Norm(TmpOutput.Output.Data, Model.Dim, Input.BatchSize * Dim, Output.Output.Data, Output.Deriv.Data, TmpOutput.Deriv.Data);

                ComputeLib.ColumnWiseSumMask(TmpOutput.Deriv.Data, 0, CudaPieceInt.Empty, 0, CudaPieceFloat.Empty, MatchData.Src2MatchIdx, Input.BatchSize, Input.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                            Input.BatchSize * Dim, Model.Dim, 1, 1);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer) { Model.EmbeddingOptimizer.BeforeGradient(); }

                if(FullMatchRunner!= null) { FullMatchRunner.Forward(); }

                //int preE = 0;
                for (int i = 0; i < MatchData.TgtSize; i++)
                {
                    Model.EmbeddingOptimizer.PushSparseGradient(MatchData.Tgt2Idx.MemPtr[i] * Model.Dim, Model.Dim); // i * Model.Dim, Model.Dim);
                }

                TmpOutput.Deriv.Data.SyncToCPU(Input.BatchSize * Dim * Model.Dim);
                MathOperatorManager.CreateInstance(DeviceType.CPU_FAST_VECTOR).ColumnWiseSumMask(TmpOutput.Deriv.Data, 0, MatchData.Tgt2MatchElement, 0, CudaPieceFloat.Empty, MatchData.Tgt2MatchIdx, MatchData.TgtSize,
                                            Model.EmbeddingOptimizer.Gradient, 0, MatchData.Tgt2Idx, 0, Input.BatchSize * Dim, Model.Dim, 1, -Model.EmbeddingOptimizer.GradientStep);

                if (!IsDelegateOptimizer) { Model.EmbeddingOptimizer.AfterGradient(); }
            }
        }

        public enum ObjectiveFunc { FullSoftmax = 0, SampleSoftmax = 1  }
        public enum PredType { RL_MAXITER = 0, RL_AVGSIM = 1 }
        public enum CrossSimType { Addition = 0, Product = 1, Cosine = 2, SubCosine = 3 };
        class SoftLinearSimRunner : StructRunner
        {
            HiddenBatchData Status { get; set; }

            HiddenBatchData Mem { get; set; }
            BiMatchBatchData MemMatch { get; set; }

            HiddenBatchData HiddenStatus = null;
            HiddenBatchData HiddenMatch = null;
            CudaPieceFloat HiddenMatchTmp = null;

            MLPAttentionStructure AttStruct;

            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            CrossSimType AttType = CrossSimType.Addition;

            SimilarityRunner SimRunner = null;

            public SoftLinearSimRunner(HiddenBatchData status, HiddenBatchData mem, BiMatchBatchData memMatch, CrossSimType attType,
                MLPAttentionStructure attStruct, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Status = status;
                Mem = mem;
                MemMatch = memMatch;

                AttType = attType;
                AttStruct = attStruct;

                HiddenStatus = new HiddenBatchData(status.MAX_BATCHSIZE, AttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);

                switch (AttType)
                {
                    case CrossSimType.Product:
                    case CrossSimType.Addition:
                        HiddenMatchTmp = new CudaPieceFloat(MemMatch.Stat.MAX_MATCH_BATCHSIZE * AttStruct.HiddenDim, true, Behavior.Device == DeviceType.GPU);
                        HiddenMatch = new HiddenBatchData(MemMatch.Stat.MAX_MATCH_BATCHSIZE, AttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);
                        Output = new HiddenBatchData(MemMatch.Stat.MAX_SRC_BATCHSIZE, MemMatch.Dim, Behavior.RunMode, Behavior.Device);
                        break;
                    case CrossSimType.SubCosine:
                        SimRunner = new SimilarityRunner(HiddenStatus, Mem, MemMatch, SimilarityType.CosineSimilarity, Behavior);
                        Output = SimRunner.Output;
                        break;
                }
            }

            public override void Forward()
            {
                HiddenStatus.BatchSize = Status.BatchSize;
                ComputeLib.Sgemm(Status.Output.Data, 0, AttStruct.Wi, 0, HiddenStatus.Output.Data, 0, Status.BatchSize, AttStruct.InputDim, AttStruct.HiddenDim, 0, 1, false, false);

                switch (AttType)
                {
                    case CrossSimType.Addition:
                    case CrossSimType.Product:
                        if (AttType == CrossSimType.Addition)
                        {
                            // hiddenMatch = hiddenStatus + Mem
                            ComputeLib.Matrix_AdditionMask(HiddenStatus.Output.Data, 0, MemMatch.SrcIdx, 0,
                                                           Mem.Output.Data, 0, MemMatch.TgtIdx, 0,
                                                           HiddenMatch.Output.Data, 0, CudaPieceInt.Empty, 0,
                                                           AttStruct.HiddenDim, MemMatch.MatchSize, 1, 1, 0);
                        }
                        else if (AttType == CrossSimType.Product)
                        {
                            ComputeLib.ElementwiseProductMask(HiddenStatus.Output.Data, 0,
                                                              Mem.Output.Data, 0,
                                                              HiddenMatch.Output.Data, 0,
                                                              MemMatch.SrcIdx, 0, MemMatch.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                              MemMatch.MatchSize, AttStruct.HiddenDim, 0, 1);
                        }
                        ComputeLib.Tanh(HiddenMatch.Output.Data, 0, HiddenMatch.Output.Data, 0, AttStruct.HiddenDim * MemMatch.MatchSize);

                        Output.BatchSize = MemMatch.SrcSize;

                        // attention = address * Wa.
                        ComputeLib.Sgemv(HiddenMatch.Output.Data, AttStruct.Wa, MemMatch.MatchSize, HiddenMatch.Dim, Output.Output.Data, false, 0, 1);

                        ComputeLib.Tanh(Output.Output.Data, 0, Output.Output.Data, 0, Output.BatchSize * Output.Dim);
                        break;
                    case CrossSimType.SubCosine:
                        SimRunner.Forward();
                        break;
                }
            }

            public override void CleanDeriv()
            {
                if (AttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(HiddenStatus.Deriv.Data, HiddenStatus.BatchSize * HiddenStatus.Dim);
                }
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                switch (AttType)
                {
                    case CrossSimType.Addition:
                    case CrossSimType.Product:
                        ComputeLib.DerivTanh(Output.Output.Data, 0, Output.Deriv.Data, 0, Output.Deriv.Data, 0, Output.BatchSize * Output.Dim, 0, 1);

                        // HiddenOutput = attentionDeriv * Wa'.
                        ComputeLib.Sgemm(Output.Deriv.Data, 0, AttStruct.Wa, 0, HiddenMatch.Deriv.Data, 0, MemMatch.MatchSize, 1, HiddenMatch.Dim, 0, 1, false, false);

                        // HiddenOutput = HiddenOutput * tanh(address)'
                        ComputeLib.DerivTanh(HiddenMatch.Output.Data, 0, HiddenMatch.Deriv.Data, 0, HiddenMatch.Deriv.Data, 0, MemMatch.MatchSize * HiddenMatch.Dim, 0, 1);

                        if (AttType == CrossSimType.Addition)
                        {
                            ComputeLib.ColumnWiseSum(HiddenMatch.Deriv.Data, Mem.Deriv.Data, Status.BatchSize, Mem.BatchSize * Mem.Dim, 1);

                            //HiddenStatus.Deriv.Data.SyncToCPU();
                            ComputeLib.ColumnWiseSumMask(HiddenMatch.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                                  CudaPieceFloat.Empty, MemMatch.Src2MatchIdx, MemMatch.SrcSize,
                                                                  HiddenStatus.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                                  MemMatch.MatchSize, HiddenStatus.Dim, 0, 1);
                        }
                        else if (AttType == CrossSimType.Product)
                        {
                            ComputeLib.ElementwiseProductMask(HiddenMatch.Deriv.Data, 0,
                                                              HiddenStatus.Output.Data, 0,
                                                              HiddenMatchTmp, 0,
                                                              CudaPieceInt.Empty, 0, MemMatch.SrcIdx, 0, CudaPieceInt.Empty, 0,
                                                              MemMatch.MatchSize, Mem.Dim, 0, 1);

                            ComputeLib.ColumnWiseSum(HiddenMatchTmp, Mem.Deriv.Data, Status.BatchSize, Mem.BatchSize * Mem.Dim, 1);

                            ComputeLib.ElementwiseProductMask(HiddenMatch.Deriv.Data, 0,
                                                              Mem.Output.Data, 0,
                                                              HiddenMatchTmp, 0,
                                                              CudaPieceInt.Empty, 0, MemMatch.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                              MemMatch.MatchSize, Mem.Dim, 0, 1);

                            ComputeLib.ColumnWiseSumMask(HiddenMatchTmp, 0, CudaPieceInt.Empty, 0,
                                                                  CudaPieceFloat.Empty, MemMatch.Src2MatchIdx, MemMatch.SrcSize,
                                                                  HiddenStatus.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                                  MemMatch.MatchSize, HiddenStatus.Dim, 0, 1);
                        }
                        break;
                    case CrossSimType.SubCosine:
                        SimRunner.Backward(cleanDeriv);
                        break;
                }

                // inputDeriv += hiddenDeriv * wi'
                ComputeLib.Sgemm(HiddenStatus.Deriv.Data, 0,
                                          AttStruct.Wi, 0,
                                          Status.Deriv.Data, 0,
                                          Status.BatchSize, AttStruct.HiddenDim, AttStruct.InputDim, 1, 1, false, true);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    AttStruct.WiMatrixOptimizer.BeforeGradient();
                    AttStruct.WaMatrixOptimizer.BeforeGradient();
                }

                if (AttType == CrossSimType.Addition || AttType == CrossSimType.Product)
                {
                    //  Wa  +=  attentionDeriv * address.
                    ComputeLib.Sgemm(Output.Deriv.Data, 0, HiddenMatch.Output.Data, 0, AttStruct.WaMatrixOptimizer.Gradient, 0,
                                              1, MemMatch.MatchSize, HiddenMatch.Dim, 1, AttStruct.WaMatrixOptimizer.GradientStep, false, false);
                }

                ComputeLib.Sgemm(Status.Output.Data, 0,
                                          HiddenStatus.Deriv.Data, 0,
                                          AttStruct.WiMatrixOptimizer.Gradient, 0,
                                          Status.BatchSize, AttStruct.InputDim, AttStruct.HiddenDim,
                                          1, AttStruct.WiMatrixOptimizer.GradientStep, true, false);
                if (!IsDelegateOptimizer)
                {
                    AttStruct.WiMatrixOptimizer.AfterGradient();
                    AttStruct.WaMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class MemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }

            CudaPieceFloat ScaleAddress { get; set; }

            public HiddenBatchData SoftmaxAddress { get; set; }

            MatrixStructure Memory { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public MemoryRetrievalRunner(HiddenBatchData address, MatrixStructure memory, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                Gamma = gamma;

                ScaleAddress = new CudaPieceFloat(Address.MAX_BATCHSIZE * Address.Dim, true, Behavior.Device == DeviceType.GPU);
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(Address.MAX_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
            }

            public override void Forward()
            {
                Output.BatchSize = Address.BatchSize;
                SoftmaxAddress.BatchSize = Address.BatchSize;

                // softmax 
                // softmax of attention
                if (Gamma == 1) ScaleAddress = Address.Output.Data;
                else ComputeLib.Add_Vector(ScaleAddress, Address.Output.Data, Address.Dim * Address.BatchSize, 0, Gamma);
                //ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);
                ComputeLib.SoftMax(ScaleAddress, SoftmaxAddress.Output.Data, Address.Dim, Address.BatchSize, 1);

                ComputeLib.Sgemm(SoftmaxAddress.Output.Data, 0, Memory.Memory, 0, Output.Output.Data, 0, SoftmaxAddress.BatchSize, SoftmaxAddress.Dim, Memory.Dim, 0, 1, false, false);

                //ComputeLib.ColumnWiseSumMask(Memory.Memory, 0, MatchData.TgtIdx, 0,
                //    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                //    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                //    MatchData.MatchSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                // outputDeriv -> AddressDeriv.
                ComputeLib.Sgemm(Output.Deriv.Data, 0, Memory.Memory, 0, SoftmaxAddress.Deriv.Data, 0, Output.BatchSize, Output.Dim, SoftmaxAddress.Dim, 0, 1, false, true);

                //ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                //    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                //    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSoftmax(SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Dim, SoftmaxAddress.BatchSize, 0);
                //ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                //    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, Address.Dim * Address.BatchSize, 1, Gamma);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer) Memory.MemoryOptimizer.BeforeGradient();

                ComputeLib.Sgemm(SoftmaxAddress.Output.Data, 0, Output.Deriv.Data, 0, Memory.MemoryOptimizer.Gradient, 0, SoftmaxAddress.BatchSize, SoftmaxAddress.Dim,
                    Output.Dim, 1, Memory.MemoryOptimizer.GradientStep, true, false);

                if (!IsDelegateOptimizer) Memory.MemoryOptimizer.AfterGradient();
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Fix Iteration, SoftAttention, Supervised Training.
        /// </summary>
        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            HiddenBatchData InitStatus { get; set; }

            MatrixStructure Memory { get; set; }
            BiMatchBatchData MatchData { get; set; }

            //CudaPieceFloat[] TerminalBias;
            //PoolingType ReasonPool = BuilderParameters.RECURRENT_POOL;
            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }
            public ReasonNetRunner(
                HiddenBatchData initStatus, int maxIterateNum,
                MatrixStructure mem, BiMatchBatchData memMatch, MLPAttentionStructure attStruct, LayerStructure ansStruct,
                GRUCell gruCell, LayerStructure termStruct, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                //TerminalBias = new CudaPieceFloat[maxIterateNum];
                //float bias = -4;
                //float biasDis = 0.9f;
                //for (int i = 0; i < maxIterateNum; i++)
                //{
                //    TerminalBias[i] = new CudaPieceFloat(1, true, true);
                //    TerminalBias[i].MemPtr[0] = bias * (float)Math.Pow(biasDis, i);
                //    TerminalBias[i].SyncFromCPU();
                //}
                InitStatus = initStatus;

                Memory = mem;
                MatchData = memMatch;
                MaxIterationNum = maxIterateNum;

                GruCell = gruCell;
                AttStruct = attStruct;
                AnsStruct = ansStruct;
                TermStruct = termStruct;

                HiddenBatchData lastStatus = InitStatus;
                HiddenBatchData lastAtt = null;

                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                AttHidden = new HiddenBatchData(Memory.Size, attStruct.HiddenDim, Behavior.RunMode, Behavior.Device);

                AnsRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, lastStatus, Behavior));
                TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct, lastStatus, Behavior));

                for (int i = 0; i < maxIterateNum - 1; i++)
                {
                    AttRunner.Add(new SoftLinearSimRunner(lastStatus, AttHidden, MatchData, BuilderParameters.AttType, AttStruct, Behavior));
                    lastAtt = AttRunner[i].Output;

                    MemoryRetrievalRunner xRunner = new MemoryRetrievalRunner(lastAtt, Memory, gamma, Behavior);
                    MemRetrievalRunner.Add(xRunner);

                    GRUQueryRunner yRunner = new GRUQueryRunner(gruCell, lastStatus, xRunner.Output, Behavior);
                    StatusRunner.Add(yRunner);
                    lastStatus = yRunner.Output;

                    //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                    //{
                    //SoftLinearSimRunner attRunner = new SoftLinearSimRunner(lastStatus, AttHidden, MatchData, BuilderParameters.AttType, AttStruct, Behavior);
                    //AttRunner.Add(attRunner);
                    //lastAtt = attRunner.Output;
                    //}

                    AnsRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, lastStatus, Behavior));
                    TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct, lastStatus, Behavior));
                }
                //if (ReasonPool == PoolingType.RL)
                {
                    AnswerProb = new HiddenBatchData[MaxIterationNum];
                    AnsData = new HiddenBatchData[MaxIterationNum];

                    //AnsData[0] = InitStatus;
                    //for (int i = 0; i < MaxIterationNum - 1; i++) AnsData[i + 1] = StatusRunner[i].Output;

                    for (int i = 0; i < MaxIterationNum; i++) AnsData[i] = AnsRunner[i].Output;
                    for (int i = 0; i < MaxIterationNum; i++) AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);

                    FinalAns = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, AnsStruct.Neural_Out, DNNRunMode.Train, Behavior.Device);
                    
                }
            }

            GRUCell GruCell = null;
            MLPAttentionStructure AttStruct = null;
            LayerStructure AnsStruct = null;
            LayerStructure TermStruct = null;

            HiddenBatchData AttHidden = null;
            List<SoftLinearSimRunner> AttRunner = new List<SoftLinearSimRunner>();
            List<FullyConnectHiddenRunner<HiddenBatchData>> TerminalRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();
            List<FullyConnectHiddenRunner<HiddenBatchData>> AnsRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

            public List<MemoryRetrievalRunner> MemRetrievalRunner = new List<MemoryRetrievalRunner>();
            List<GRUQueryRunner> StatusRunner = new List<GRUQueryRunner>();

            public HiddenBatchData[] AnsData = null;
            public HiddenBatchData[] AnswerProb = null;
            public HiddenBatchData FinalAns = null;

            Dictionary<int, int> StepStat = new Dictionary<int, int>();

            public override void Forward()
            {
                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                //{
                AttHidden.BatchSize = Memory.Size;
                ComputeLib.Sgemm(Memory.Memory, 0, AttStruct.Wm, 0, AttHidden.Output.Data, 0,
                                 Memory.Size, Memory.Dim, AttStruct.HiddenDim, 0, 1, false, false);
                if (AttStruct.IsBias) ComputeLib.Matrix_Add_Linear(AttHidden.Output.Data, AttStruct.B, Memory.Size, AttStruct.HiddenDim);
                //}
                
                AnsRunner[0].Forward();
                TerminalRunner[0].Forward();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    AttRunner[i].Forward();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Forward();
                    // update Status.
                    StatusRunner[i].Forward();

                    // take attention
                    //AttRunner[i + 1].Forward();
                    
                    // take answer.
                    AnsRunner[i + 1].Forward();
                    // take terminal gate or not.
                    TerminalRunner[i + 1].Forward();
                }

                #region RL Pooling.
                //if (ReasonPool == PoolingType.RL)
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        //ComputeLib.Matrix_Add_Linear(TerminalRunner[i].Output.Output.Data, TerminalBias[i], TerminalRunner[i].Output.BatchSize, 1);
                        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize * AnsData[i].Dim);
                    }

                    FinalAns.BatchSize = AnsData.Last().BatchSize;
                    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize * FinalAns.Dim);

                    for (int b = 0; b < FinalAns.BatchSize; b++)
                    {
                        int max_iter = 0;
                        double max_p = 0;

                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t += (float)Math.Log(1 - t_i);

                            if (Math.Exp(p) > max_p)
                            {
                                max_p = Math.Exp(p);
                                max_iter = i;
                            }

                            if (BuilderParameters.PRED_RL == PredType.RL_AVGSIM)
                            {
                                FastVector.Add_Vector(FinalAns.Output.Data.MemPtr, b * FinalAns.Dim, AnsData[i].Output.Data.MemPtr, b * FinalAns.Dim, FinalAns.Dim, 1, (float)Math.Exp(p));
                            }
                        }
                        if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
                        {
                            FastVector.Add_Vector(FinalAns.Output.Data.MemPtr, b * FinalAns.Dim, AnsData[max_iter].Output.Data.MemPtr, b * FinalAns.Dim, FinalAns.Dim, 0, 1);
                        }

                        StepStat[max_iter] += 1;
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize * FinalAns.Dim);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i].BatchSize = InitStatus.BatchSize;
                        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
                    }
                }
                #endregion.
            }
            public override void CleanDeriv()
            {
                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                //{
                ComputeLib.Zero(AttHidden.Deriv.Data, AttHidden.BatchSize * AttHidden.Dim);
                //}

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    TerminalRunner[i + 1].CleanDeriv();
                    // take Answer.
                    AnsRunner[i + 1].CleanDeriv();
                    // take Attention

                    // update Status.
                    StatusRunner[i].CleanDeriv();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].CleanDeriv();

                    AttRunner[i].CleanDeriv();
                }
                TerminalRunner[0].CleanDeriv();
                AnsRunner[0].CleanDeriv();
                //AttRunner[0].CleanDeriv();

            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                #region RL Pool.
                //if (ReasonPool == PoolingType.RL)
                {
                    //int BatchSize = AnswerSeqData.Last().BatchSize;

                    /// according to loss, assign reward to each terminal node.
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(TerminalRunner[i].Output.Deriv.Data.MemPtr, 0, InitStatus.BatchSize);
                    }

                    for (int b = 0; b < InitStatus.BatchSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                            if (BuilderParameters.IS_NORM_REWARD) reward = reward * 1.0f / (i + 1);

                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] += reward * (1 - TerminalRunner[i].Output.Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                TerminalRunner[hp].Output.Deriv.Data.MemPtr[b] += -reward * TerminalRunner[hp].Output.Output.Data.MemPtr[b];
                            }

                            if (TerminalRunner[i].Output.Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalRunner[i].Output.Deriv.Data.MemPtr[b], i);
                            }
                        }
                    }

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }
                #endregion.

                for (int i = MaxIterationNum - 2; i >= 0; i--)
                {
                    TerminalRunner[i + 1].Backward(cleanDeriv);

                    AnsRunner[i + 1].Backward(cleanDeriv);

                    // update Status.
                    StatusRunner[i].Backward(cleanDeriv);

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Backward(cleanDeriv);

                    AttRunner[i].Backward(cleanDeriv);
                }
                TerminalRunner[0].Backward(cleanDeriv);
                AnsRunner[0].Backward(cleanDeriv);
                //AttRunner[0].Backward(cleanDeriv);

                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                //{
                //    ComputeLib.Sgemm(AttHidden.SentDeriv, 0,
                //                 AttStruct.Wm, 0,
                //                 Memory.SentDeriv, 0,
                //                 Memory.SentSize, AttStruct.HiddenDim, AttStruct.MemoryDim, 1, 1, false, true);
                //}
            }

            public override void Update()
            {
                
                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.BeforeGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.BeforeGradient();
                        Memory.MemoryOptimizer.BeforeGradient();
                    }

                    if (AttStruct.IsBias)
                    {
                        ComputeLib.ColumnWiseSum(AttHidden.Deriv.Data, AttStruct.BMatrixOptimizer.Gradient, Memory.Size, AttStruct.HiddenDim, AttStruct.BMatrixOptimizer.GradientStep);
                    }
                    ComputeLib.Sgemm(Memory.Memory, 0,
                                     AttHidden.Deriv.Data, 0,
                                     AttStruct.WmMatrixOptimizer.Gradient, 0,
                                     Memory.Size, AttStruct.MemoryDim, AttStruct.HiddenDim,
                                     1, AttStruct.WmMatrixOptimizer.GradientStep, true, false);

                    ComputeLib.Sgemm(AttHidden.Deriv.Data, 0,
                                 AttStruct.Wm, 0,
                                 Memory.MemoryOptimizer.Gradient, 0,
                                 Memory.Size, AttStruct.HiddenDim, AttStruct.MemoryDim, 1, Memory.MemoryOptimizer.GradientStep, false, true);
                }

                //AttRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                //AttRunner[0].Update();

                AnsRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AnsRunner[0].Update();

                TerminalRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                TerminalRunner[0].Update();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    AttRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    MemRetrievalRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    StatusRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    
                    AnsRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    TerminalRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;

                    // take action. new Attention
                    AttRunner[i].Update();

                    // obtain the retrieved mem.
                    MemRetrievalRunner[i].Update();

                    // update Status.
                    StatusRunner[i].Update();

                    AnsRunner[i + 1].Update();

                    TerminalRunner[i + 1].Update();
                }
                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                {
                    if (!IsDelegateOptimizer)
                    {
                        AttStruct.WmMatrixOptimizer.AfterGradient();
                        if (AttStruct.IsBias) AttStruct.BMatrixOptimizer.AfterGradient();
                        Memory.MemoryOptimizer.AfterGradient();
                    }
                }

            }

            public override void Init()
            {
                StepStat.Clear();
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    StepStat.Add(i, 0);
                }
            }

            public override void Complete()
            {
                int totalNum = 0;
                for (int i = 0; i < MaxIterationNum; i++) totalNum += StepStat[i];
                for (int i = 0; i < MaxIterationNum; i++)
                {
                    Logger.WriteLog("Step {0} Stat {1}, Percentage {2}", i, StepStat[i], StepStat[i] * 1.0f / totalNum);
                }
            }
        }

        /// <summary>
        /// Knowledge Graph Compeletion.
        /// </summary>
        class KGCPredictionRunner : ObjectiveRunner
        {
            int lsum = 0, lsum_filter = 0;
            int rsum = 0, rsum_filter = 0;
            int lp_n = 0, lp_n_filter = 0;
            int rp_n = 0, rp_n_filter = 0;
            int HitK = 10;

            int Iteration = 0;
            int SmpIdx = 0;

            float AvgReward = 0;

            CudaPieceFloat Label { get; set; }

            HiddenBatchData Score { get; set; }
            List<Tuple<int, int, int>> BatchLinks { get; set; }

            int EvalPerIteration = 1;

            StreamWriter ScoreWriter = null;

            bool IsReverse = false;
            string ScorePath { get; set; }
            public KGCPredictionRunner(CudaPieceFloat label, List<Tuple<int, int, int>> batchLinks, 
                HiddenBatchData score, bool isReverse, string scorePath, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Label = label;
                BatchLinks = batchLinks;
                IsReverse = isReverse;
                Score = score;

                ScorePath = scorePath;
                Iteration = 0;
            }

            public override void Init()
            {
                SmpIdx = 0;

                lsum = 0;
                lsum_filter = 0;
                rsum = 0;
                rsum_filter = 0;
                lp_n = 0;
                lp_n_filter = 0;
                rp_n = 0;
                rp_n_filter = 0;

                AvgReward = 0;
                if(!ScorePath.Equals(string.Empty))
                {
                    if(!Directory.Exists(ScorePath))
                    {
                        Directory.CreateDirectory(ScorePath);
                    }
                    ScoreWriter = new StreamWriter(ScorePath + ".score." + Iteration.ToString());
                }
            }

            public override void Complete()
            {
                if ((Iteration + 1) % EvalPerIteration == 0)
                {
                    Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));

                    Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));

                    Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
                    Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
                    ObjectiveScore = (lsum_filter + rsum_filter) * 1.0 / SmpIdx;
                }
                else
                {
                    Logger.WriteLog("Average Reward {0}", AvgReward / SmpIdx);
                }

                if (ScoreWriter != null) ScoreWriter.Close();

                Iteration += 1;
            }
            public override void Forward()
            {
                if ((Iteration + 1) % EvalPerIteration == 0)
                {
                    Score.Output.Data.SyncToCPU(Score.BatchSize * Score.Dim);

                    int[] lindicies = new int[Score.BatchSize * Score.Dim];
                    for (int i = 0; i < Score.BatchSize; i++)
                    {
                        for (int c = 0; c < Score.Dim; c++)
                        {
                            lindicies[i * Score.Dim + c] = c;
                        }
                    }

                    //Parallel. for (int b = 0; b < Score.BatchSize; b++)
                    //Parallel.For(0, Score.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, b => //  (int i = 0; i < Label.BatchSize; i++)
                    for(int b =0; b<Score.BatchSize;b++)
                    {
                        Array.Sort(Score.Output.Data.MemPtr, lindicies, b * Score.Dim, Score.Dim);

                        int chead = BatchLinks[b].Item1;
                        int relation = BatchLinks[b].Item3;
                        int ctail = BatchLinks[b].Item2;

                        int filter = 1;
                        for (int c = Score.Dim - 1; c >= 0; c--)
                        {
                            int ptail = IsReverse ? lindicies[b * Score.Dim + Score.Dim - 1 - c] : lindicies[b * Score.Dim + c];
                            
                            if (!DataPanel.IsInGraph(chead, ptail, relation)) filter++;
                            if (ctail == ptail)
                            {
                                if (relation < DataPanel.RelationNum)
                                {
                                    Interlocked.Add(ref lsum, Score.Dim - c);
                                    Interlocked.Add(ref lsum_filter, filter);
                                    if (Score.Dim - c <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
                                    if (filter < HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
                                }
                                else
                                {
                                    Interlocked.Add(ref rsum, Score.Dim - c);
                                    Interlocked.Add(ref rsum_filter, filter);
                                    if (Score.Dim - c <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
                                    if (filter < HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
                                }
                                ScoreWriter.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", chead, relation, ctail, filter, Score.Dim - c);
                                break;
                            }
                        }
                    }//);
                }
                else
                {
                    //ComputeLib.SoftMax(Score.Output.Data, Score.Output.Data, Score.Dim, Score.BatchSize, 1);
                    Score.Output.Data.SyncToCPU(Score.BatchSize * Score.Dim);
                    for (int b = 0; b < Score.BatchSize; b++)
                    {
                        int ctail = BatchLinks[b].Item2;
                        AvgReward += Score.Output.Data.MemPtr[b * Score.Dim + ctail];
                    }
                }
                SmpIdx += Score.BatchSize;
            }
            
        }


        class EnsembleKGCPredictionRunner : ObjectiveRunner
        {
            int lsum = 0, lsum_filter = 0;
            int rsum = 0, rsum_filter = 0;
            int lp_n = 0, lp_n_filter = 0;
            int rp_n = 0, rp_n_filter = 0;
            int HitK = 10;

            int Iteration = 0;
            int SmpIdx = 0;

            
            CudaPieceFloat Label { get; set; }

            HiddenBatchData[] Score { get; set; }
            HiddenBatchData[] TreeProb { get; set; }
            HiddenBatchData[] Attention { get; set; }


            List<Tuple<int, int, int>> BatchLinks { get; set; }


            StreamWriter ScoreWriter = null;

            bool IsReverse = false;
            string ScorePath { get; set; }
            public EnsembleKGCPredictionRunner(CudaPieceFloat label, List<Tuple<int, int, int>> batchLinks,
                HiddenBatchData[] scoreList, HiddenBatchData[] treeProb, HiddenBatchData[] attention, bool isReverse, string scorePath, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Label = label;
                BatchLinks = batchLinks;
                IsReverse = isReverse;
                Score = scoreList;
                TreeProb = treeProb;
                Attention = attention;

                ScorePath = scorePath;
                Iteration = 0;
            }

            public override void Init()
            {
                SmpIdx = 0;

                lsum = 0;
                lsum_filter = 0;
                rsum = 0;
                rsum_filter = 0;
                lp_n = 0;
                lp_n_filter = 0;
                rp_n = 0;
                rp_n_filter = 0;

                if (!ScorePath.Equals(string.Empty))
                {
                    if (!Directory.Exists(ScorePath))
                    {
                        Directory.CreateDirectory(ScorePath);
                    }
                    ScoreWriter = new StreamWriter(ScorePath + ".score." + Iteration.ToString());
                }
            }

            public override void Complete()
            {
                Logger.WriteLog(string.Format("Left Mean Rank {0}", lsum * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Left Filter Mean Rank {0}", lsum_filter * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Left Mean Hit@{0} : {1}", HitK, lp_n * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Left Filter Mean Hit@{0} : {1}", HitK, lp_n_filter * 2.0 / SmpIdx));

                Logger.WriteLog(string.Format("Right Mean Rank {0}", rsum * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Right Filter Mean Rank {0}", rsum_filter * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Right Mean Hit@{0} : {1}", HitK, rp_n * 2.0 / SmpIdx));
                Logger.WriteLog(string.Format("Right Filter Mean Hit@{0} : {1}", HitK, rp_n_filter * 2.0 / SmpIdx));

                Logger.WriteLog(string.Format("Overall Mean Rank {0}", (lsum + rsum) * 1.0 / SmpIdx));
                Logger.WriteLog(string.Format("Overall Filter Mean Rank {0}", (lsum_filter + rsum_filter) * 1.0 / SmpIdx));
                Logger.WriteLog(string.Format("Overall Mean Hit@{0} : {1}", HitK, (lp_n + rp_n) * 1.0 / SmpIdx));
                Logger.WriteLog(string.Format("Overall Filter Mean Hit@{0} : {1}", HitK, (lp_n_filter + rp_n_filter) * 1.0 / SmpIdx));
                ObjectiveScore = (lsum_filter + rsum_filter) * 1.0 / SmpIdx;


                if (ScoreWriter != null) ScoreWriter.Close();

                Iteration += 1;
            }

            public override void Forward()
            {
                List<Tuple<float, float, float[], List<int>>[]> rankScore = new List<Tuple<float, float, float[], List<int>>[]>();

                for (int d = 0; d < Score.Length; d++)
                {

                    Score[d].Output.Data.SyncToCPU(Score[d].BatchSize * Score[d].Dim);

                    int[] lindicies = new int[Score[d].BatchSize * Score[d].Dim];
                    for (int i = 0; i < Score[d].BatchSize; i++)
                    {
                        for (int c = 0; c < Score[d].Dim; c++)
                        {
                            lindicies[i * Score[d].Dim + c] = c;
                        }
                    }

                    TreeProb[d].Output.Data.SyncToCPU(Score[d].BatchSize);
                    if (d > 0) Attention[d - 1].Output.Data.SyncToCPU(Attention[d - 1].Dim * Attention[d - 1].BatchSize);
                    rankScore.Add(new Tuple<float, float, float[], List<int>>[Score[d].BatchSize]);

                    //Parallel. for (int b = 0; b < Score.BatchSize; b++)
                    //Parallel.For(0, Score.BatchSize, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 }, b => //  (int i = 0; i < Label.BatchSize; i++)
                    for (int b = 0; b < Score[d].BatchSize; b++)
                    {

                        Array.Sort(Score[d].Output.Data.MemPtr, lindicies, b * Score[d].Dim, Score[d].Dim);

                        int chead = BatchLinks[b].Item1;
                        int relation = BatchLinks[b].Item3;
                        int ctail = BatchLinks[b].Item2;

                        int filter = 1;
                        float treeProb = TreeProb[d].Output.Data.MemPtr[b];
                        float[] attention = new float[Attention[0].Dim];
                        if (d > 0)
                            Array.Copy(Attention[d - 1].Output.Data.MemPtr, b * Attention[d - 1].Dim, attention, 0, Attention[d - 1].Dim);

                        int topK = 10;
                        int groundFilter = 0;
                        List<int> topNeg = new List<int>();
                        for (int c = Score[d].Dim - 1; c >= 0; c--)
                        {
                            int ptail = IsReverse ? lindicies[b * Score[d].Dim + Score[d].Dim - 1 - c] : lindicies[b * Score[d].Dim + c];

                            if (!DataPanel.IsInGraph(chead, ptail, relation))
                            {
                                filter++;
                                if (topNeg.Count < topK)
                                {
                                    topNeg.Add(ptail);
                                }
                            }
                            if (ctail == ptail)
                            {
                                //if (relation < DataPanel.RelationNum)
                                //{
                                //    Interlocked.Add(ref lsum, Score.Dim - c);
                                //    Interlocked.Add(ref lsum_filter, filter);
                                //    if (Score.Dim - c <= HitK) Interlocked.Increment(ref lp_n); // lp_n += 1;
                                //    if (filter < HitK) Interlocked.Increment(ref lp_n_filter); // += 1;
                                //}
                                //else
                                //{
                                //    Interlocked.Add(ref rsum, Score.Dim - c);
                                //    Interlocked.Add(ref rsum_filter, filter);
                                //    if (Score.Dim - c <= HitK) Interlocked.Increment(ref rp_n); // lp_n += 1;
                                //    if (filter < HitK) Interlocked.Increment(ref rp_n_filter); // += 1;
                                //}
                                //ScoreWriter.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", chead, relation, ctail, filter, Score.Dim - c);
                                //rankScore[d][b] = filter;
                                groundFilter = filter;
                            }

                            if(groundFilter > 0 && topNeg.Count >= topK) { break; }
                        }
                        rankScore[d][b] = new Tuple<float, float, float[], List<int>>(groundFilter, treeProb, attention, topNeg);
                    }//);
                }

                HashSet<int> workSamples = new HashSet<int>();

                for (int b = 0; b < Score[0].BatchSize; b++)
                {
                    int chead = BatchLinks[b].Item1;
                    int relation = BatchLinks[b].Item3;
                    int ctail = BatchLinks[b].Item2;

                    ScoreWriter.WriteLine();
                    for (int d = 0; d < Score.Length; d++)
                    {
                        ScoreWriter.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", chead, relation, ctail, rankScore[d][b].Item2, rankScore[d][b].Item1, string.Join(",", rankScore[d][b].Item3),
                           string.Join(",", rankScore[d][b].Item4));

                        //if (rankScore[d][b].Item2 > rankScore[0][b].Item2 && rankScore[d][b].Item1 < rankScore[0][b].Item1)
                        //{
                        //    Console.WriteLine("Sample {0} works", SmpIdx + b);
                        //    if (!workSamples.Contains(b)) workSamples.Add(b);
                        //}
                    }
                }

                //foreach (int b in workSamples)
                //{
                //    int chead = BatchLinks[b].Item1;
                //    int relation = BatchLinks[b].Item3;
                //    int ctail = BatchLinks[b].Item2;
                //    Logger.WriteLog("\nSample Idx {0}", SmpIdx + b);
                //    for (int d = 0; d < Score.Length; d++)
                //    {
                //        Logger.WriteLog("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", chead, relation, ctail, rankScore[d][b].Item2, rankScore[d][b].Item1, string.Join(",", rankScore[d][b].Item3),
                //           string.Join(",", rankScore[d][b].Item4));
                //    }
                //}

                SmpIdx += Score[0].BatchSize;
            }

        }

        public static ComputationGraph BuildComputationGraphV6(List<Tuple<int, int, int>> graph,
                                                               CompositeNNStructure modelStructure, bool createNew,
                                                               string preScore,
                                                               RunnerBehavior Behavior)
        {
            // word embedding layer.
            EmbedStructure InNodeEmbed = null;
            EmbedStructure LinkEmbed = null;
            EmbedStructure OutNodeEmbed = null;

            LayerStructure AnsStruct = null;
            MatrixStructure Memory = null;
            GRUCell StateStruct = null;
            MLPAttentionStructure AttStruct = null;
            LayerStructure TerminalStruct = null;

            if (createNew)
            {
                if (BuilderParameters.SEED_MODEL.Equals(string.Empty))
                {
                    InNodeEmbed = new EmbedStructure(DataPanel.EntityNum, BuilderParameters.IN_EmbedDim, DeviceType.CPU_FAST_VECTOR);
                    LinkEmbed = new EmbedStructure(DataPanel.RelationNum * 2, BuilderParameters.R_EmbedDim, DeviceType.CPU_FAST_VECTOR);

                    int StatusDim = BuilderParameters.IN_EmbedDim + BuilderParameters.R_EmbedDim;
                    int MemDim = BuilderParameters.MEM_EmbedDim; // StatusDim;
                    int memHid = BuilderParameters.ATT_DIM;
                    int OutDim = BuilderParameters.OUT_EmbedDim;
                    OutNodeEmbed = new EmbedStructure(DataPanel.EntityNum, OutDim, DeviceType.CPU_FAST_VECTOR);

                    AnsStruct = new LayerStructure(StatusDim, OutDim, A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false, Behavior.Device);
                    Memory = new MatrixStructure(MemDim, BuilderParameters.MemorySize, Behavior.Device);
                    StateStruct = new GRUCell(MemDim, StatusDim, Behavior.Device);
                    AttStruct = new MLPAttentionStructure(StatusDim, MemDim, memHid, Behavior.Device);
                    TerminalStruct = new LayerStructure(StatusDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, Behavior.Device);

                    modelStructure.AddLayer(InNodeEmbed);
                    modelStructure.AddLayer(LinkEmbed);
                    modelStructure.AddLayer(OutNodeEmbed);

                    modelStructure.AddLayer(AnsStruct);
                    modelStructure.AddLayer(Memory);
                    modelStructure.AddLayer(StateStruct);
                    modelStructure.AddLayer(AttStruct);
                    modelStructure.AddLayer(TerminalStruct);
                }
                else
                {
                    using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                    {
                        int modelNum = CompositeNNStructure.DeserializeModelCount(modelReader);

                        InNodeEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);
                        LinkEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);
                        OutNodeEmbed = (EmbedStructure)modelStructure.DeserializeNextModel(modelReader, DeviceType.CPU_FAST_VECTOR);

                        AnsStruct = (LayerStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        Memory = (MatrixStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        StateStruct = (GRUCell)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        AttStruct = (MLPAttentionStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                        TerminalStruct = (LayerStructure)modelStructure.DeserializeNextModel(modelReader, Behavior.Device);
                    }
                }
            }
            else
            {
                int link = 0;
                InNodeEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];
                LinkEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];
                OutNodeEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];

                AnsStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                Memory = (MatrixStructure)modelStructure.CompositeLinks[link++];
                StateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                AttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                TerminalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
            }

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                InNodeEmbed.BiasOptimizer = new SGDOptimizer(InNodeEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                LinkEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(LinkEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                LinkEmbed.BiasOptimizer = new SGDOptimizer(LinkEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                OutNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(OutNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                OutNodeEmbed.BiasOptimizer = new SGDOptimizer(OutNodeEmbed.Bias, 0, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer);
            }

            ComputationGraph cg = new ComputationGraph();
            SampleEmbedV2Runner SmpRunner = new SampleEmbedV2Runner(graph, InNodeEmbed, LinkEmbed,
                        Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.MiniBatchSize : BuilderParameters.TestMiniBatchSize, Behavior);
            cg.AddDataRunner(SmpRunner);

            IntArgument statSize = new IntArgument("StatusSize");
            cg.AddRunner(new HiddenDataBatchSizeRunner(SmpRunner.Status, statSize, Behavior));

            BiMatchBatchData MemMatchData = (BiMatchBatchData)cg.AddRunner(new RepeatBiMatchRunner(SmpRunner.Status.MAX_BATCHSIZE, Memory.Size, statSize, Behavior));
            ReasonNetRunner reasonRunner = new ReasonNetRunner(SmpRunner.Status, BuilderParameters.RECURRENT_STEP,
                    Memory, MemMatchData, AttStruct,  AnsStruct, StateStruct, TerminalStruct, 10, Behavior);
            cg.AddRunner(reasonRunner);

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    HiddenBatchData[] distOutputs = new HiddenBatchData[BuilderParameters.RECURRENT_STEP];
                    for (int i = 0; i < BuilderParameters.RECURRENT_STEP; i++)
                    {
                        EmbedDistanceRunner distRunner = new EmbedDistanceRunner(cg, OutNodeEmbed, reasonRunner.AnsData[i], SmpRunner.TargetIndex, SmpRunner.NTrialNum + 1, DistanceType.L1Dist, Behavior);
                        cg.AddRunner(distRunner);
                        distOutputs[i] = distRunner.Output;
                    }
                    cg.AddObjective(new MultiClassContrastiveRewardRunner(null, distOutputs, reasonRunner.AnswerProb, BuilderParameters.Gamma, Behavior));
                    break;
                case DNNRunMode.Predict:
                    EmbedDistanceRunner ansRunner = new EmbedDistanceRunner(cg, OutNodeEmbed, reasonRunner.FinalAns, null, 0, DistanceType.L1Dist, Behavior);
                    cg.AddRunner(ansRunner);
                    cg.AddRunner(new KGCPredictionRunner(SmpRunner.Label, SmpRunner.BatchLinks, ansRunner.Output, true, preScore, Behavior));
                    break;
                case DNNRunMode.GradientCheck:
                    HiddenBatchData[] CheckdistOutputs = new HiddenBatchData[BuilderParameters.RECURRENT_STEP];
                    for (int i = 0; i < BuilderParameters.RECURRENT_STEP; i++)
                    {
                        EmbedDistanceRunner distRunner = new EmbedDistanceRunner(cg, OutNodeEmbed, reasonRunner.AnsData[i], null, 0, DistanceType.L1Dist, Behavior);
                        cg.AddRunner(distRunner);
                        CheckdistOutputs[i] = distRunner.Output;
                    }
                    cg.AddRunner(new EnsembleKGCPredictionRunner(SmpRunner.Label, SmpRunner.BatchLinks, CheckdistOutputs, reasonRunner.AnswerProb, reasonRunner.MemRetrievalRunner.Select(t => t.SoftmaxAddress).ToArray(), true, preScore, Behavior));
                    break;
            }
            cg.SetDelegateModel(modelStructure);

            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            CompositeNNStructure DeepModel = new CompositeNNStructure();

            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Train, DeepModel, true, string.Empty,
                                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    
                    ComputationGraph testCG = null;
                    if (BuilderParameters.IsTestFile)
                        testCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Test, DeepModel, false, BuilderParameters.SCORE_PATH + ".test",
                                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
                    ComputationGraph validCG = null;
                    if (BuilderParameters.IsValidFile)
                        validCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Valid, DeepModel, false, BuilderParameters.SCORE_PATH + ".valid",
                                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                    double bestValidScore = double.MinValue;
                    double bestTestScore = double.MinValue;
                    int bestIter = -1;
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                        
                       
                       
                        double testScore = 0;
                        double validScore = 0;

                        if ((iter + 1) % BuilderParameters.PRED_ITER == 0)
                        {
                            if (testCG != null)
                            {
                                testScore = testCG.Execute();
                                Logger.WriteLog("Test Score {0}, At iter {1}", testScore, bestIter);
                            }
                            if (validCG != null)
                            {
                                validScore = validCG.Execute();
                                Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, bestIter);
                            }

                            if (validScore > bestValidScore)
                            {
                                bestValidScore = validScore; bestTestScore = testScore; bestIter = iter;
                                //using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, "RecurrentAttention.best.model"), FileMode.Create, FileAccess.Write)))
                                //{
                                //    modelStructure.Serialize(writer);
                                //}
                            }
                            Logger.WriteLog("Best Valid/Test Score {0}, {1}, At iter {2}", bestValidScore, bestTestScore, bestIter);

                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, 
                                string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                DeepModel.Serialize(writer);
                            }

                        }
                        else if (DeepNet.BuilderParameters.ModelSavePerIteration)
                        {
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath,
                               string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                            {
                                DeepModel.Serialize(writer);
                            }
                        }
                    }


                    break;
                case DNNRunMode.GradientCheck:
                    ComputationGraph predCG = BuildComputationGraphV6(DataPanel.knowledgeGraph.Test, DeepModel, true, BuilderParameters.SCORE_PATH + ".test",
                                    new RunnerBehavior() { RunMode = DNNRunMode.GradientCheck, Device = device, Computelib = computeLib });
                    predCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static RelationGraphData knowledgeGraph;

            public static int EntityNum { get { return knowledgeGraph.EntityId.Count; } }
            public static int RelationNum { get { return knowledgeGraph.RelationId.Count; } }

            public static List<Tuple<int, int, int>> Train
            {
                get { return knowledgeGraph.Train; }
            }

            public static List<Tuple<int, int, int>> Test
            {
                get { return knowledgeGraph.Test; }
            }

            public static List<Tuple<int, int, int>> Valid
            {
                get { return knowledgeGraph.Valid; }
            }

            public static double[] HeadProb { get { return knowledgeGraph.HeadProb; } }

            public static double[] TailProb { get { return knowledgeGraph.TailProb; } }

            public static bool IsInGraph(int eid1, int eid2, int rid)
            {
                if (rid < RelationNum && knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid1, eid2, rid))) return true;
                else if(rid >= RelationNum && knowledgeGraph.ValidGraphHash.Contains(string.Format("{0}#{1}#{2}", eid2, eid1, rid - RelationNum))) return true;
                else return false;
            }


            public static void Init()
            {
                knowledgeGraph = new RelationGraphData();

                knowledgeGraph.EntityId = RelationGraphData.LoadMapId(BuilderParameters.Entity2Id);
                knowledgeGraph.RelationId = RelationGraphData.LoadMapId(BuilderParameters.Relation2Id);

                knowledgeGraph.Train = knowledgeGraph.LoadGraph(BuilderParameters.TrainData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, true);
                knowledgeGraph.Valid = knowledgeGraph.LoadGraph(BuilderParameters.ValidData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
                knowledgeGraph.Test = knowledgeGraph.LoadGraph(BuilderParameters.TestData, knowledgeGraph.EntityId, knowledgeGraph.RelationId, false);
            }
        }
    }
}
