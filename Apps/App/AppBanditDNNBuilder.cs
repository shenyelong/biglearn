﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    public class AppBanditDNNBuilder : Builder
    {
        class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            
            public static string TrainData { get { return Argument["TRAIN-DATA"].Value; } }
            public static string TrainLabel { get { return Argument["TRAIN-LABEL"].Value; } }

            public static string ValidData { get { return Argument["VALID-DATA"].Value; } }
            public static string ValidLabel { get { return Argument["VALID-LABEL"].Value; } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            //public static int[] SX { get { return Argument["SX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            //public static int[] Stride { get { return Argument["STRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            //public static int[] O { get { return Argument["O"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            //public static int[] Pad { get { return Argument["PAD"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            //public static A_Func[] Afs { get { return Argument["AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            //public static int[] PoolSX { get { return Argument["POOLSX"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            //public static int[] PoolStride { get { return Argument["POOLSTRIDE"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            //public static List<ImageFilterParameter> Filters(int depth)
            //{
            //    List<ImageFilterParameter> mFilters = new List<ImageFilterParameter>();
            //    int mdepth = depth;
            //    for (int i = 0; i < SX.Length; i++)
            //    {
            //        mFilters.Add(new ImageFilterParameter(SX[i], Stride[i], mdepth, O[i], Pad[i], Afs[i], PoolSX[i], PoolStride[i]));
            //        mdepth = O[i];
            //    }
            //    return mFilters;
            //}
            public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            //public static A_Func[] ACTIVATION { get { return Argument["ACTIVATION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }
            //public static bool[] LAYER_BIAS { get { return Argument["LAYER-BIAS"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }
            //public static float[] LAYER_DROPOUT { get { return Argument["LAYER-DROPOUT"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => float.Parse(i)).ToArray(); } }

            public static float BetaPrior { get { return float.Parse(Argument["B-PRIOR"].Value); } }
            public static float BetaAlpha { get { return float.Parse(Argument["B-ALPHA"].Value); } }

            public static int Iteration { get { return int.Parse(Argument["ITERATION"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }

            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                
                Argument.Add("TRAIN-DATA", new ParameterArgument(string.Empty, "Train Image Data"));
                Argument.Add("TRAIN-LABEL", new ParameterArgument(string.Empty, "Train Image Label"));

                Argument.Add("VALID-DATA", new ParameterArgument(string.Empty, "Valid Image Data"));
                Argument.Add("VALID-LABEL", new ParameterArgument(string.Empty, "Valid Image Label"));

                Argument.Add("B-PRIOR", new ParameterArgument("1", "Prior Beta Distribution."));
                Argument.Add("B-ALPHA", new ParameterArgument("0.99999", "Alpha distribution."));


                Argument.Add("MINI-BATCH", new ParameterArgument(string.Empty, "Mini Batch Size."));

                Argument.Add("LAYER-DIM", new ParameterArgument(string.Empty, "Layer Dimension"));

                Argument.Add("LEARN-RATE", new ParameterArgument("0.01", "Model Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("ITERATION", new ParameterArgument("300", "Train Iteration."));

                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "DNN Seed Model."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_BANDIT_DNN; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public class BanditSelectionRunner : ObjectiveRunner
        {
            public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }
            public DenseBatchData Label { get; set; }

            public RewardData Reward { get; set; }

            /// <summary>
            /// Label could be null, the first element will be viewed as positive, others are negative.
            /// </summary>
            /// <param name="label"></param>
            /// <param name="output"></param>
            /// <param name="behavior"></param>
            public BanditSelectionRunner(DenseBatchData label, MatrixData output, RewardData reward, RunnerBehavior behavior)
                : base(Structure.Empty, behavior)
            {
                Output = output;
                Label = label;
                Reward = reward;
            }

            public override void Forward()
            {
                Reward.Fail = 0;
                Reward.Success = 0;
                if (Output.Row == 0)
                {
                    ObjectiveScore = 0; return;
                }

                Output.Output.SyncToCPU(Output.Row * Output.Column);
                Label.Data.SyncToCPU(Output.Row);

                for (int i = 0; i < Output.Row; i++)
                {
                    int target = (int)(Label.Data.MemPtr[i]);

                    float[] predArray = Output.Output.MemPtr.Skip(i * Output.Column).Take(Output.Column).ToArray();
                    int maxIndex = Util.MaximumValue(predArray);
                    if (maxIndex == target) Reward.Success += 1;
                    else Reward.Fail += 1;
                }

                ObjectiveScore = (Reward.Success) * 1.0f / (Reward.Fail + Reward.Success);
            }
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<ImageDataSource, ImageDataStat> imgData,
            IDataCashier<DenseBatchData, DenseDataStat> labelData,
            RunnerBehavior Behavior, BanditLayerStruct layer)
        {
            ComputationGraph cg = new ComputationGraph();

            /**************** Get Data from DataCashier *********/
            ImageDataSource image = (ImageDataSource)cg.AddDataRunner(new DataRunner<ImageDataSource, ImageDataStat>(imgData, Behavior));
            DenseBatchData label = (DenseBatchData)cg.AddDataRunner(new DataRunner<DenseBatchData, DenseDataStat>(labelData, Behavior));

            //ActivationRunner runner = new ActivationRunner();
            RewardData reward = new RewardData();


            MatrixData weight = (MatrixData)cg.AddRunner(new BanditLayerThompsonSamplingRunner(layer, reward, Behavior));

            MatrixData ImageData = new MatrixData(image);

            MatrixData imageV1 = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(ImageData, weight, Behavior));

            //foreach (RLImageLinkStructure linkModel in ConvStruct)
            //{
            //    cg.AddRunner(new RLSampleRunner(linkModel.RLFilter, reward, Behavior));
            //}
            //foreach (RLLayerStructure linkModel in LayerStruct)
            //{
            //    cg.AddRunner(new RLSampleRunner(linkModel.RLWeight, reward, Behavior));
            //}

            //ImageDataSource layerInput = image;
            //foreach (RLImageLinkStructure linkModel in ConvStruct)
            //{
            //    RLImageConvRunner<ImageDataSource> linkRunner = new RLImageConvRunner<ImageDataSource>(linkModel, layerInput, Behavior);
            //    cg.AddRunner(linkRunner);
            //    layerInput = linkRunner.Output;
            //}

            //RLImageFullyConnectRunner<ImageDataSource> fullRunner = new RLImageFullyConnectRunner<ImageDataSource>(LayerStruct[0], layerInput, Behavior);
            //cg.AddRunner(fullRunner);
            //HiddenBatchData nnLayerInput = fullRunner.Output;
            //for (int i = 1; i < LayerStruct.Count; i++)
            //{
            //    RLFullyConnectHiddenRunner<HiddenBatchData> nnRunner = new RLFullyConnectHiddenRunner<HiddenBatchData>(LayerStruct[i], nnLayerInput, Behavior);
            //    cg.AddRunner(nnRunner);
            //    nnLayerInput = nnRunner.Output;
            //}


            ///*************** DNN for Source Input. ********************/
            //HiddenBatchData pred = nnLayerInput;
            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:
                    cg.AddObjective(new BanditSelectionRunner(label, imageV1, reward, Behavior));
                    break;
                case DNNRunMode.Predict:
                    cg.AddRunner(new AccuracyDiskDumpRunner(label.Data, new DenseBatchData(new DenseDataStat() { MAX_BATCHSIZE = imageV1.MaxRow, Dim = imageV1.Column }, imageV1.Output, Behavior.Device), 
                        BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath));
                    break;
            }
            return cg;
        }

        public sealed class BanditLayerStruct : Structure
        {
            public int Neural_In;
            public int Neural_Out;

            public A_Func Af;

            public int ArmNum;
            public int BanditNum { get { return Neural_In * Neural_Out; } }
            public CudaPieceFloat Arm;

            public CudaPieceFloat S;
            public CudaPieceFloat F;

            public override DataSourceID Type { get { return DataSourceID.BanditLayerStructure; } }

            public BanditLayerStruct(int layer_in, int layer_out, List<float> arm, A_Func af, DeviceType device)
            {
                Neural_In = layer_in;
                Neural_Out = layer_out;
                Af = af;

                ArmNum = arm.Count;

                DeviceType = device;

                Arm = new CudaPieceFloat(ArmNum, true, device == DeviceType.GPU);
                for (int i = 0; i < arm.Count; i++) Arm.MemPtr[i] = arm[i];
                Arm.SyncFromCPU();

                S = new CudaPieceFloat(BanditNum * ArmNum, true, device == DeviceType.GPU);
                F = new CudaPieceFloat(BanditNum * ArmNum, true, device == DeviceType.GPU);
                S.Zero();
                F.Zero();
            }
        }

        public class BanditLayerThompsonSamplingRunner : StructRunner
        {
            BanditLayerStruct Layer;
            RewardData Reward;
            public new MatrixData Output { get { return (MatrixData)base.Output; } set { base.Output = value; } }
            CudaPieceInt Index;
            float Smooth = BuilderParameters.BetaPrior;
            float Alpha = BuilderParameters.BetaAlpha;
            public Random random = new Random(ParameterSetting.Random.Next());
            public BanditLayerThompsonSamplingRunner(BanditLayerStruct layer, RewardData reward, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Reward = reward;
                Layer = layer;
                Output = new MatrixData(Layer.Neural_Out, Layer.Neural_In, new CudaPieceFloat(Layer.Neural_In * Layer.Neural_Out, behavior.Device), CudaPieceFloat.Empty, behavior.Device);
                Index = new CudaPieceInt(Layer.Neural_In * Layer.Neural_Out, Behavior.Device);
            }

            public override void Forward()
            {
                ///This can be called in GPU if possibile.
                Layer.S.SyncToCPU();
                Layer.F.SyncToCPU();
                for (int i = 0; i < Layer.BanditNum; i++)
                {
                    double maxP = -1;
                    int maxA = -1;
                    for (int a = 0; a < Layer.ArmNum; a++)
                    {
                        double p = MathUtil.Beta_Sample(Layer.S.MemPtr[i * Layer.ArmNum + a] + Smooth, Layer.F.MemPtr[i * Layer.ArmNum + a] + Smooth, random);
                        if (p > maxP) { maxP = p; maxA = a; }
                    }
                    Output.Output.MemPtr[i] = Layer.Arm.MemPtr[maxA];
                    Index.MemPtr[i] = maxA;
                }
                Output.Output.SyncFromCPU();
                Index.SyncFromCPU();
                //Cudalib.UCTStateSampling(Data.Count.CudaPtr, Data.Success.CudaPtr, Data.Status.CudaPtr, Data.StatusNum, Data.Data.CudaPtr, Data.Data.Size, Data.C);
            }

            public override void Update()
            {
                Cudalib.BanditStateUpdating(Layer.F.CudaPtr, Layer.S.CudaPtr, Index.CudaPtr, Layer.ArmNum, Layer.BanditNum, Reward.Fail, Reward.Success, Alpha);
                //Cudalib.StateUpdating(Layer.F.CudaPtr, Layer.S.CudaPtr, Data.Status.CudaPtr, Data.StatusNum, Data.Data.CudaPtr, Data.Data.Size, 1, Reward.Success * 1.0f / Reward.Count);
            }
            //public override void Update()
            //{
            //    //policy network for update.
            //    Cudalib.StateUpdating(Data.Count.CudaPtr, Data.Success.CudaPtr, Data.Status.CudaPtr, Data.StatusNum, Data.Data.CudaPtr, Data.Data.Size, 1, Reward.Success * 1.0f / Reward.Count);
            //}
        }


        public override void Rock()
        {

            //test beta distribution.
            Logger.OpenLog(BuilderParameters.LogFile);

            //DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            //IMathOperationManager ComputeLib = MathOperatorManager.CreateInstance(device);

            DeviceBehavior behavior = new DeviceBehavior(BuilderParameters.GPUID);
            Logger.WriteLog("Loading Training/Validation Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            //List<RLImageLinkStructure> ConvLinkStruct = new List<RLImageLinkStructure>();
            //List<RLLayerStructure> FullLinkStruct = new List<RLLayerStructure>();

            //Console.WriteLine(Cudalib.CudaDeviceProperties(0));
            //CompositeNNStructure cnnModel = null;
            //    new ImageInputParameter() { Width = DataPanel.Train.Stat.SrcStat.Width, Height = DataPanel.Train.Stat.SrcStat.Height, Depth = DataPanel.Train.Stat.SrcStat.Depth },
            //    ImageCNNBuilderParameters.Filters(DataPanel.Train.Stat.SrcStat.Depth),
            //    ImageCNNBuilderParameters.LAYER_DIM, ImageCNNBuilderParameters.ACTIVATION, ImageCNNBuilderParameters.LAYER_DROPOUT, ImageCNNBuilderParameters.LAYER_BIAS);
            Logger.WriteLog("Init CNN Structure.");

            int outWidth = DataPanel.TrainImage.Stat.Width;
            int outHeight = DataPanel.TrainImage.Stat.Height;
            BanditLayerStruct banditLayer = new BanditLayerStruct(outWidth * outHeight, BuilderParameters.LAYER_DIM.Last(), new List<float>() { -1, 0, 1 }, A_Func.Tanh, behavior.Device);
            //List<ImageFilterParameter> filters = BuilderParameters.Filters(DataPanel.TrainImage.Stat.Depth);
            //foreach (ImageFilterParameter filter in filters)
            //{
            //    ConvLinkStruct.Add(new RLImageLinkStructure(filter, device));
            //    outWidth = filter.OutputWidth(outWidth);
            //    outHeight = filter.OutputHeight(outHeight);
            //}
            //int featureNum = filters.Last().O * outWidth * outHeight;
            //for (int i = 0; i < BuilderParameters.LAYER_DIM.Length; i++)
            //{
            //    RLLayerStructure link = new RLLayerStructure(featureNum, BuilderParameters.LAYER_DIM[i], BuilderParameters.ACTIVATION[i], device);
            //    FullLinkStruct.Add(link);
            //    featureNum = BuilderParameters.LAYER_DIM[i];
            //}

            ComputationGraph train_cg = BuildComputationGraph(DataPanel.TrainImage, DataPanel.TrainLabel, behavior.TrainMode, banditLayer);

            ComputationGraph valid_cg = BuildComputationGraph(DataPanel.ValidImage, DataPanel.ValidLabel, behavior.PredictMode, banditLayer);


            //ImageLabelDataSource trainSet = new ImageLabelDataSource(DataPanel.Train, DataPanel.TrainLabel);
            //ImageLabelDataSource validSet = new ImageLabelDataSource(DataPanel.Valid, DataPanel.ValidLabel);

            //LossDerivParameter lossDerivParameter = new LossDerivParameter()
            //{
            //    InputDataType = DataSourceID.ImageLabelData,
            //    OutputDataType = DataSourceID.HiddenBatchData,
            //    lossType = ImageCNNBuilderParameters.LossFunction,
            //    Gamma = ImageCNNBuilderParameters.Gamma,
            //    LabelMinValue = ImageCNNBuilderParameters.LabelMinValue,
            //    LabelMaxValue = ImageCNNBuilderParameters.LabelMaxValue
            //};

            //trainSet , 
            //ImageCNNRunner<ImageLabelDataSource> trainer = new ImageCNNRunner<ImageLabelDataSource>(cnnModel, new RunnerBehavior() { RunMode = DNNRunMode.Train });
            //trainer.LossParameter = lossDerivParameter;

            //if (trainer.GradientCheckRandomVector(DataPanel.Train.GetInstances(false)))
            //    Logger.WriteLog("Gradient Check Successfully!");
            //else
            //    Logger.WriteLog("Gradient Check Failed!");

            //cnnModel.InitOptimizer(new StructureLearner()
            //{
            //    LearnRate = ImageCNNBuilderParameters.LearnRate,
            //    Optimizer = ImageCNNBuilderParameters.Optimizer,
            //    AdaBoost = ImageCNNBuilderParameters.AdaBoost,
            //    BatchNum = DataPanel.Train.Stat.SrcStat.TotalBatchNumber,
            //    EpochNum = ImageCNNBuilderParameters.Iteration
            //});

            //validSet, 
            //ImageCNNRunner<ImageLabelDataSource> evaler = new ImageCNNRunner<ImageLabelDataSource>(cnnModel, new RunnerBehavior() { RunMode = DNNRunMode.Predict });
            //evaler.Predict(DataPanel.Valid.GetInstances(false), evalParameter);
            double score = valid_cg.Execute();
            Logger.WriteLog("Prediction Accuracy {0}, at Iteration {1}", score, -1);

            double best_score = 0;
            double best_iter = -1;
            for (int iter = 0; iter < BuilderParameters.Iteration; iter++)
            {
                double loss = train_cg.Execute();
                Logger.WriteLog("Train Loss {0}", loss);
                score = valid_cg.Execute();
                Logger.WriteLog("Prediction Accuracy {0}, at Iteration {1}", score, iter);

                if (score > best_score)
                {
                    best_score = score;
                    best_iter = iter;
                }
                Logger.WriteLog("Best Accuracy {0}, at Iteration {1}", best_score, best_iter);
            }
            Logger.CloseLog();
        }

        public class DataPanel
        {
            public static DataCashier<ImageDataSource, ImageDataStat> TrainImage = null;
            public static DataCashier<DenseBatchData, DenseDataStat> TrainLabel = null;

            public static DataCashier<ImageDataSource, ImageDataStat> ValidImage = null;
            public static DataCashier<DenseBatchData, DenseDataStat> ValidLabel = null;

            public static void Init()
            {
                ExtractBinary(BuilderParameters.TrainData, BuilderParameters.TrainLabel,
                    BuilderParameters.TrainData + ".bin", BuilderParameters.TrainLabel + ".bin", BuilderParameters.MiniBatchSize);

                ExtractBinary(BuilderParameters.ValidData, BuilderParameters.ValidLabel,
                    BuilderParameters.ValidData + ".bin", BuilderParameters.ValidLabel + ".bin", 64);



                TrainImage = new DataCashier<ImageDataSource, ImageDataStat>(BuilderParameters.TrainData + ".bin");
                TrainLabel = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.TrainLabel + ".bin");

                TrainImage.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                TrainLabel.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);


                ValidImage = new DataCashier<ImageDataSource, ImageDataStat>(BuilderParameters.ValidData + ".bin");
                ValidLabel = new DataCashier<DenseBatchData, DenseDataStat>(BuilderParameters.ValidLabel + ".bin");

                ValidImage.InitThreadSafePipelineCashier(100, false);
                ValidLabel.InitThreadSafePipelineCashier(100, false);
            }


            public static int ReadInt32(BinaryReader reader)
            {
                byte[] a = reader.ReadBytes(4);
                Array.Reverse(a);
                return BitConverter.ToInt32(a, 0);
            }

            public static byte[] ExtractLabels(string labelFile)
            {
                using (BinaryReader labelReader = new BinaryReader(new FileStream(labelFile, FileMode.Open, FileAccess.Read)))
                {
                    int magicNum = ReadInt32(labelReader);
                    int labelNumber = ReadInt32(labelReader);

                    byte[] labelArray = new byte[labelNumber];
                    for (int i = 0; i < labelNumber; i++)
                    {
                        labelArray[i] = labelReader.ReadByte();
                    }
                    return labelArray;
                }
            }

            public static Tuple<int, int, int, byte[]> ExtractImages(string imageFile)
            {
                using (BinaryReader imageReader = new BinaryReader(new FileStream(imageFile, FileMode.Open, FileAccess.Read)))
                {
                    int magicNumber = ReadInt32(imageReader);
                    int imageNum = ReadInt32(imageReader);
                    int width = ReadInt32(imageReader);
                    int height = ReadInt32(imageReader);

                    byte[] imageData = new byte[imageNum * width * height];
                    for (int i = 0; i < imageNum * width * height; i++)
                    {
                        imageData[i] = imageReader.ReadByte();
                    }
                    return new Tuple<int, int, int, byte[]>(imageNum, width, height, imageData);
                }
            }

            public static void ExtractBinary(string imageFile, string labelFile, string imagebin, string labelbin, int batchSize)
            {
                using (BinaryWriter imageWriter = new BinaryWriter(new FileStream(imagebin, FileMode.Create, FileAccess.Write)))
                {
                    using (BinaryWriter labelWriter = new BinaryWriter(new FileStream(labelbin, FileMode.Create, FileAccess.Write)))
                    {
                        byte[] labels = ExtractLabels(labelFile);
                        Tuple<int, int, int, byte[]> images = ExtractImages(imageFile);

                        ImageDataStat imageStat = new ImageDataStat() { Width = images.Item2, Height = images.Item3, Depth = 1, IsSource = true, MAX_BATCHSIZE = batchSize };
                        ImageDataSource imageData = new ImageDataSource(imageStat, DeviceType.CPU); // imageStat);
                                                                                                    //    imageWriter, images.Item2, images.Item3, 1);

                        DenseDataStat labelStat = new DenseDataStat() { Dim = 1 };
                        DenseBatchData labelData = new DenseBatchData(labelStat, DeviceType.CPU);  // BatchInputMode.MakeContainer);
                                                                                                   //labelData.Init(labelStat, DeviceType.CPU);

                        int batchNum = (labels.Length - 1 + batchSize) / batchSize;
                        float[] data = new float[batchSize * images.Item2 * images.Item3];
                        float[] label = new float[batchSize];
                        for (int i = 0; i < batchNum; i++)
                        {
                            
                            int mbatchSize = labels.Length - i * batchSize;
                            if (mbatchSize > batchSize) mbatchSize = batchSize;

                            IEnumerable<byte> imagedata = images.Item4.Skip(i * batchSize * images.Item2 * images.Item3).
                                Take(mbatchSize * images.Item2 * images.Item3);


                            for (int x = 0; x < mbatchSize * images.Item2 * images.Item3; x++)
                            {
                                data[x] = images.Item4[i * batchSize * images.Item2 * images.Item3 + x] / 255.0f;
                            }

                            for (int x = 0; x < mbatchSize; x++)
                            {
                                label[x] = labels[i * batchSize + x];
                            }

                            imageData.PushSample(data, mbatchSize);
                            labelData.PushSample(label, mbatchSize);

                            imageData.PopBatchToStat(imageWriter);
                            labelData.PopBatchToStat(labelWriter);

                            if (i % 100 == 0) { Console.WriteLine("Process MiniBatches {0}", i); }
                        }

                        imageData.PopBatchCompleteStat(imageWriter);
                        labelData.PopBatchCompleteStat(labelWriter);
                    }
                }
            }
        }
    }
}
