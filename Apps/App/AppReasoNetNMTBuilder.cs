﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    //public class AppReasoNetNMTBuilder : Builder
    //{
    //    class BuilderParameters : BaseModelArgument<BuilderParameters>
    //    {
    //        static BuilderParameters()
    //        {
    //            Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
    //            Argument.Add("RUN-MODE", new ParameterArgument("0", "Train : 0; Predict : 1"));

    //            Argument.Add("TRAIN-SRC", new ParameterArgument(string.Empty, "SRC Train Data."));
    //            Argument.Add("TRAIN-TGT", new ParameterArgument(string.Empty, "TGT Train Data."));

    //            Argument.Add("VALID-SRC", new ParameterArgument(string.Empty, "SRC Valid Data."));
    //            Argument.Add("VALID-TGT", new ParameterArgument(string.Empty, "TGT Valid Data."));

    //            Argument.Add("DEV-SRC", new ParameterArgument(string.Empty, "SRC Dev Dataset."));
    //            Argument.Add("DEV-TGT", new ParameterArgument(string.Empty, "TGT Dev Dataset."));

    //            Argument.Add("VOCAB", new ParameterArgument(string.Empty, "Vocab Data."));

    //            ///Language Word Error Rate.
    //            Argument.Add("EVALUATION", new ParameterArgument(((int)EvaluationType.AUC).ToString(), ParameterUtil.EnumValues(typeof(EvaluationType))));

    //            Argument.Add("LAYER-DIM", new ParameterArgument("1", "DNN Layer Dim"));
    //            Argument.Add("LAYER-ATTENTION", new ParameterArgument("1", "Layer Attention"));

    //            Argument.Add("SRC-EMBED", new ParameterArgument("620", "Src String Dim"));
    //            Argument.Add("TGT-EMBED", new ParameterArgument("500", "Tgt String Dim"));
    //            Argument.Add("ATT-HIDDEN", new ParameterArgument("1000", "Attention Layer Hidden Dim"));

    //            Argument.Add("RECURRENT-STEP", new ParameterArgument("10", "Reasoning Steps."));
    //            Argument.Add("ENSEMBLE-RL", new ParameterArgument("0", "0:max terminate prob; 1:average terminate prob."));

    //            Argument.Add("CONTEXT-LEN", new ParameterArgument("0", "Context Vector length"));
    //            Argument.Add("DECODE-DIM", new ParameterArgument("200", "Decoding Dimension"));

    //            ///Softmax Randomup.
    //            Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));
    //            Argument.Add("BEAM-CANDIDATE", new ParameterArgument("10", " Beam Search Candidate Number."));
    //            Argument.Add("BEAM-DEPTH", new ParameterArgument("10", " Beam Search Max Depth."));
    //            Argument.Add("TGT-MAX-LEN", new ParameterArgument("30", " Tgt Seq Max Length."));

    //            Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
    //            Argument.Add("METRIC-PATH", new ParameterArgument("output.metric", "Output Metric Path"));
    //            Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
    //            Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));
    //        }

    //        public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
    //        public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
    //        //(DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } }
    //        public static string TrainSrcData { get { return Argument["TRAIN-SRC"].Value; } }
    //        public static string TrainTgtData { get { return Argument["TRAIN-TGT"].Value; } }
    //        public static bool IsTrainFile { get { return (!TrainSrcData.Equals(string.Empty)) && (!TrainTgtData.Equals(string.Empty)); } }

    //        public static string ValidSrcData { get { return Argument["VALID-SRC"].Value; } }
    //        public static string ValidTgtData { get { return Argument["VALID-TGT"].Value; } }
    //        public static bool IsValidFile { get { return (!ValidSrcData.Equals(string.Empty)) && (!ValidTgtData.Equals(string.Empty)); } }

    //        public static string DevSrcData { get { return Argument["DEV-SRC"].Value; } }
    //        public static string DevTgtData { get { return Argument["DEV-TGT"].Value; } }
    //        public static bool IsDevFile { get { return (!DevSrcData.Equals(string.Empty)) && (!DevTgtData.Equals(string.Empty)); } }

    //        public static EvaluationType Evaluation { get { return (EvaluationType)(int.Parse(Argument["EVALUATION"].Value)); } }
    //        public static int[] LAYER_DIM { get { return Argument["LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
    //        public static bool[] LAYER_ATTENTION { get { return Argument["LAYER-ATTENTION"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i) > 0).ToArray(); } }

    //        public static int Src_Embed { get { return int.Parse(Argument["SRC-EMBED"].Value); } }
    //        public static int Tgt_Embed { get { return int.Parse(Argument["TGT-EMBED"].Value); } }

    //        public static int Decode_Embed { get { return int.Parse(Argument["DECODE-DIM"].Value); } }
    //        public static int Context_Len { get { return int.Parse(Argument["CONTEXT-LEN"].Value); } }

    //        public static int Attention_Hidden { get { return int.Parse(Argument["ATT-HIDDEN"].Value); } }

    //        public static int Recurrent_Step { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
    //        public static PredType Ensemble_RL { get { return (PredType)int.Parse(Argument["ENSEMBLE-RL"].Value); } }
    //        public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

    //        public static string Vocab { get { return Argument["VOCAB"].Value; } }
    //        public static Vocab2Freq mVocabDict = null;
    //        public static Vocab2Freq VocabDict { get { if (mVocabDict == null) mVocabDict = new Vocab2Freq(Vocab); return mVocabDict; } }
    //        public static int BeginWordIndex { get { return VocabDict.VocabTermIndex["<#begin#>"]; } }
    //        public static int TerminalWordIndex { get { return VocabDict.VocabTermIndex["<#end#>"]; } }

    //        public static int BeamSearchCandidate { get { return int.Parse(Argument["BEAM-CANDIDATE"].Value); } }
    //        public static int BeamSearchDepth { get { return int.Parse(Argument["BEAM-DEPTH"].Value); } }
    //        public static int TgtSeqMaxLen { get { return int.Parse(Argument["TGT-MAX-LEN"].Value); } }


    //        public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
    //        public static string MetricOutputPath { get { return Argument["METRIC-PATH"].Value; } }
    //        public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
    //        public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }
    //    }

    //    public override BuilderType Type { get { return BuilderType.APP_REASONET_NMT; } }

    //    public enum PredType { RL_MAXITER, RL_AVGPROB }

    //    public override void InitStartup(string fileName)
    //    {
    //        BuilderParameters.Parse(fileName);
    //        //MathOperatorManager.SetDevice(BuilderParameters.GPUID);
    //    }

    //    /// <summary>
    //    /// Fix Iteration, SoftAttention, Supervised Training.
    //    /// </summary>
    //    class ReasonNetRunner : StructRunner
    //    {
    //        /// <summary>
    //        /// Input Query;
    //        /// </summary>
    //        HiddenBatchData InitStatus { get; set; }

    //        MatrixStructure GMemory { get; set; }
    //        BiMatchBatchData GMemMatch { get; set; }
    //        MLPAttentionStructure GAttStruct { get; set; }

    //        //List<GlobalSoftLinearSimRunner> GAttRunner = new List<GlobalSoftLinearSimRunner>();
    //        //List<GlobalMemoryRetrievalRunner> GMemRetrievalRunner = new List<GlobalMemoryRetrievalRunner>();

    //        //SeqDenseBatchData QMemory { get; set; }
    //        //BiMatchBatchData QMemMatch { get; set; }
    //        //MLPAttentionStructure QAttStruct { get; set; }
    //        //SeqDenseBatchData QAttHidden { get; set; }

    //        //List<SoftLinearSimRunner> QAttRunner = new List<SoftLinearSimRunner>();
    //        //List<MemoryRetrievalRunner> QMemRetrievalRunner = new List<MemoryRetrievalRunner>();

    //        //List<EnsembleMatrixRunner> RetrievalEnsembleRunner = new List<EnsembleMatrixRunner>();

    //        GRUCell GruCell = null;
    //        List<LayerStructure> TermStruct = null;

    //        //List<GRUQueryRunner> StatusRunner = new List<GRUQueryRunner>();
    //        //List<FullyConnectHiddenRunner<HiddenBatchData>> AnsRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();
    //        //List<FullyConnectHiddenRunner<HiddenBatchData>> TerminalRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

    //        public HiddenBatchData[] StateData = null;
    //        public HiddenBatchData[] TermData = null;

    //        public HiddenBatchData[] AnswerProb = null;
    //        public HiddenBatchData FinalState = null;

    //        //PoolingType ReasonPool = BuilderParameters.RECURRENT_POOL;
    //        /// <summary>
    //        /// maximum iteration number.
    //        /// </summary>
    //        int MaxIterationNum { get; set; }
    //        public ReasonNetRunner(ComputationGraph cg,
    //            HiddenBatchData initStatus, int maxIterateNum,
    //            MatrixStructure gMem, BiMatchBatchData gMemMatch, MLPAttentionStructure gAttStruct, float gGamma,
    //            //suppose we don't have the query available. 
    //            //SeqDenseBatchData qMem, BiMatchBatchData qMatchData, MLPAttentionStructure qAttStruct, float qGamma,

    //            GRUCell gruCell, List<LayerStructure> termStruct, RunnerBehavior behavior) : base(cg, Structure.Empty, null, behavior)
    //        {
    //            InitStatus = initStatus;

    //            GMemory = gMem;
    //            GMemMatch = gMemMatch;
    //            GAttStruct = gAttStruct;

    //            TermStruct = termStruct;
    //            GruCell = gruCell;

    //            MaxIterationNum = maxIterateNum;

    //            //AttGamma = attgamma;
    //            //AnsGamma = ansgamma;
    //            //DebugFile = debugFile;

    //            //IntArgument batchSizeArgument = new IntArgument("BatchSize");
    //            //cg.AddRunner(new BatchSizeAssignmentRunner(Memory, batchSizeArgument, behavior));

    //            //IntArgument sentSizeArgument = new IntArgument("SentSize");
    //            //cg.AddRunner(new SentSizeAssignmentRunner(Memory, sentSizeArgument, behavior));

    //            // Memory Matrix;
    //            MatrixData MemoryMatrix = new MatrixData(gMem);

    //            // InitState is between -1 to 1;
    //            //VectorData initWeight = new VectorData(InitState.Neural_In, InitState.weight, InitState.WeightGrad, InitState.DeviceType);
    //            //cg.AddRunner(new ActivationRunner(new MatrixData(initWeight), A_Func.Tanh, Behavior));
    //            //// Generate BatchInitVec;
    //            //MatrixData initState = (MatrixData)cg.AddRunner(new VectorExpansionRunner(initWeight, MemoryMatrix.MaxSegment, batchSizeArgument, Behavior));
    //            MatrixData initState = new MatrixData(InitStatus);

    //            // Memory Address Project.
    //            MatrixData MemoryAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.MemoryDim, gAttStruct.Wm, gAttStruct.WmGrad, gAttStruct.DeviceType);

    //            // Memory Address.
    //            MatrixData MemoryAddress = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(MemoryMatrix, MemoryAddressProject, Behavior));

    //            // State Address Project.
    //            MatrixData StateAddressProject = new MatrixData(gAttStruct.HiddenDim, gAttStruct.InputDim, gAttStruct.Wi, gAttStruct.WiGrad, gAttStruct.DeviceType);

    //            VectorData AddressScoreProject = new VectorData(gAttStruct.HiddenDim, gAttStruct.Wa, gAttStruct.WaGrad, gAttStruct.DeviceType);
    //            // For Forward/Backward Propagation.
    //            {
    //                StateData = new HiddenBatchData[MaxIterationNum];
    //                TermData = new HiddenBatchData[MaxIterationNum];
    //            }
    //            //

    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                MatrixData stateAddress = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(initState, StateAddressProject, Behavior));

    //                // Attention Input.
    //                MatrixData attScore = new MatrixData((HiddenBatchData)cg.AddRunner(new SeqAlignmentRunnerV2(stateAddress, MemoryAddress, gMemMatch, AddressScoreProject, Behavior, 1, A_Func.Tanh)));

    //                // Attention Softmax.
    //                cg.AddRunner(new MatrixSoftmaxProcessor(attScore, gGamma, Behavior));

    //                MatrixData inputMatrix = (MatrixData)cg.AddRunner(new MatrixMultiplicationRunner(attScore, MemoryMatrix, Behavior));

    //                // RNN State.
    //                MatrixData newState = new MatrixData((HiddenBatchData)cg.AddRunner(new GRUQueryRunner(GruCell, new HiddenBatchData(initState), new HiddenBatchData(inputMatrix), Behavior)));

    //                StateData[i] = new HiddenBatchData(newState);

    //                HiddenBatchData terminateOutput = StateData[i];
    //                for (int l = 0; l < TermStruct.Count; l++)
    //                {
    //                    terminateOutput = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct[l], terminateOutput, Behavior));
    //                }
    //                TermData[i] = terminateOutput;

    //                // State Switch.
    //                initState = newState;
    //            }

    //            {
    //                AnswerProb = new HiddenBatchData[MaxIterationNum];
    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, Behavior.RunMode, Behavior.Device);
    //                }

    //                FinalState = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, gruCell.HiddenStateDim, Behavior.RunMode, Behavior.Device);
    //            }

    //            //GAttHidden = new HiddenBatchData(GMemory.Size, GAttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);

    //            //if (BuilderParameters.IS_QUERY_MEM)
    //            //{
    //            //    QMemory = qMem;
    //            //    QMemMatch = qMatchData;
    //            //    QAttStruct = qAttStruct;
    //            //    QAttHidden = new SeqDenseBatchData(new SequenceDataStat()
    //            //    {
    //            //        MAX_BATCHSIZE = QMemory.Stat.MAX_BATCHSIZE,
    //            //        MAX_SEQUENCESIZE = QMemory.Stat.MAX_SEQUENCESIZE,
    //            //        FEATURE_DIM = QAttStruct.HiddenDim
    //            //    }, QMemory.SampleIdx, QMemory.SentMargin, Behavior.Device);
    //            //}

    //            //MaxIterationNum = maxIterateNum;

    //            //GruCell = gruCell;
    //            //AnsStruct = ansStruct;
    //            //TermStruct = termStruct;
    //            //HiddenBatchData lastStatus = InitStatus;

    //            //AnsRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, lastStatus, Behavior));
    //            //TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct, lastStatus, Behavior));

    //            //HiddenBatchData GAtt = null;
    //            //HiddenBatchData QAtt = null;
    //            //for (int i = 0; i < maxIterateNum - 1; i++)
    //            //{
    //            //    GAttRunner.Add(new GlobalSoftLinearSimRunner(lastStatus, GAttHidden, GMemMatch, BuilderParameters.M_ATT_TYPE, GAttStruct, Behavior));
    //            //    GAtt = GAttRunner[i].Output;
    //            //    GlobalMemoryRetrievalRunner gMemRunner = new GlobalMemoryRetrievalRunner(GAtt, GMemory, gamma, Behavior);
    //            //    GMemRetrievalRunner.Add(gMemRunner);

    //            //    HiddenBatchData X = gMemRunner.Output;

    //            //    if (BuilderParameters.IS_QUERY_MEM)
    //            //    {
    //            //        QAttRunner.Add(new SoftLinearSimRunner(lastStatus, QAttHidden, QMemMatch, BuilderParameters.Q_ATT_TYPE, QAttStruct, Behavior));
    //            //        QAtt = QAttRunner[i].Output;
    //            //        MemoryRetrievalRunner qMemRunner = new MemoryRetrievalRunner(QAtt, QMemory, QMemMatch, gamma, Behavior);
    //            //        QMemRetrievalRunner.Add(qMemRunner);

    //            //        EnsembleMatrixRunner eRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { gMemRunner.Output, qMemRunner.Output }, Behavior);
    //            //        RetrievalEnsembleRunner.Add(eRunner);

    //            //        X = eRunner.Output;
    //            //    }

    //            //    GRUQueryRunner yRunner = new GRUQueryRunner(gruCell, lastStatus, X, Behavior);
    //            //    StatusRunner.Add(yRunner);
    //            //    lastStatus = yRunner.Output;

    //            //    AnsRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, lastStatus, Behavior));
    //            //    TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, lastStatus, Behavior));
    //            //}
    //            //{
    //            //    AnswerProb = new HiddenBatchData[MaxIterationNum];
    //            //    AnsData = new HiddenBatchData[MaxIterationNum];
    //            //    for (int i = 0; i < MaxIterationNum; i++) AnsData[i] = AnsRunner[i].Output;
    //            //    for (int i = 0; i < MaxIterationNum; i++) AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);
    //            //    FinalAns = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, AnsStruct.Neural_Out, DNNRunMode.Train, Behavior.Device);
    //            //}
    //        }

    //        public override void Forward()
    //        {
    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                ComputeLib.Logistic(TermData[i].Output.Data, 0, TermData[i].Output.Data, 0, TermData[i].Output.BatchSize, 1);
    //                ComputeLib.ClipVector(TermData[i].Output.Data, TermData[i].BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

    //                TermData[i].Output.Data.SyncToCPU(TermData[i].BatchSize);
    //                StateData[i].Output.Data.SyncToCPU(StateData[i].BatchSize * StateData[i].Dim);
    //            }

    //            FinalState.BatchSize = StateData.Last().BatchSize;
    //            Array.Clear(FinalState.Output.Data.MemPtr, 0, FinalState.BatchSize * FinalState.Dim);


    //            for (int b = 0; b < FinalState.BatchSize; b++)
    //            {
    //                int max_iter = 0;
    //                double max_p = 0;
    //                float acc_log_t = 0;
    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    float t_i = TermData[i].Output.Data.MemPtr[b];
    //                    if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

    //                    float p = acc_log_t + (float)Math.Log(t_i);
    //                    if (i == MaxIterationNum - 1) p = acc_log_t;

    //                    float prob = (float)Math.Exp(p);
    //                    AnswerProb[i].Output.Data.MemPtr[b] = prob;
    //                    acc_log_t += (float)Math.Log(1 - t_i);

    //                    if (prob > max_p)
    //                    {
    //                        max_p = prob;
    //                        max_iter = i;
    //                    }
    //                }

    //                if (BuilderParameters.Ensemble_RL == PredType.RL_MAXITER)
    //                {
    //                    for (int d = 0; d < FinalState.Dim; d++)
    //                    {
    //                        FinalState.Output.Data.MemPtr[b * FinalState.Dim + d] = StateData[max_iter].Output.Data.MemPtr[d];
    //                    }
    //                }
    //            }

    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                AnswerProb[i].BatchSize = FinalState.BatchSize;
    //                AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
    //            }
    //            FinalState.Output.Data.SyncFromCPU(FinalState.BatchSize * FinalState.Dim);

    //            #region comments.
    //            // Global Memory Hidden.
    //            //{
    //            //    GAttHidden.BatchSize = GMemory.Size;
    //            //    ComputeLib.Sgemm(GMemory.Memory, 0, GAttStruct.Wm, 0, GAttHidden.Output.Data, 0,
    //            //                     GMemory.Size, GMemory.Dim, GAttStruct.HiddenDim, 0, 1, false, false);
    //            //    if (GAttStruct.IsBias) ComputeLib.Matrix_Add_Linear(GAttHidden.Output.Data, GAttStruct.B, GMemory.Size, GAttStruct.HiddenDim);
    //            //}

    //            //// Question Memory Hidden.
    //            //if (BuilderParameters.IS_QUERY_MEM)
    //            //{
    //            //    QAttHidden.BatchSize = QMemory.BatchSize;
    //            //    QAttHidden.SentSize = QMemory.SentSize;
    //            //    ComputeLib.Sgemm(QMemory.SentOutput, 0, QAttStruct.Wm, 0, QAttHidden.SentOutput, 0,
    //            //                     QMemory.SentSize, QMemory.Dim, QAttStruct.HiddenDim, 0, 1, false, false);
    //            //    if (QAttStruct.IsBias) ComputeLib.Matrix_Add_Linear(QAttHidden.SentOutput, QAttStruct.B, QMemory.SentSize, QAttStruct.HiddenDim);
    //            //}

    //            //AnsRunner[0].Forward();
    //            //TerminalRunner[0].Forward();

    //            //for (int i = 0; i < MaxIterationNum - 1; i++)
    //            //{
    //            //    GAttRunner[i].Forward();
    //            //    // obtain the retrieved mem.
    //            //    GMemRetrievalRunner[i].Forward();

    //            //    if (BuilderParameters.IS_QUERY_MEM)
    //            //    {
    //            //        QAttRunner[i].Forward();
    //            //        QMemRetrievalRunner[i].Forward();
    //            //        RetrievalEnsembleRunner[i].Forward();
    //            //    }

    //            //    // update Status.
    //            //    StatusRunner[i].Forward();

    //            //    // take answer.
    //            //    AnsRunner[i + 1].Forward();
    //            //    // take terminal gate or not.
    //            //    TerminalRunner[i + 1].Forward();
    //            //}

    //            //#region RL Pooling.
    //            //{
    //            //    for (int i = 0; i < MaxIterationNum; i++)
    //            //    {
    //            //        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
    //            //        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

    //            //        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
    //            //        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize * AnsData[i].Dim);
    //            //    }

    //            //    FinalAns.BatchSize = AnsData.Last().BatchSize;
    //            //    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize * FinalAns.Dim);

    //            //    for (int b = 0; b < FinalAns.BatchSize; b++)
    //            //    {
    //            //        int max_iter = 0;
    //            //        double max_p = 0;

    //            //        float acc_log_t = 0;
    //            //        for (int i = 0; i < MaxIterationNum; i++)
    //            //        {
    //            //            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
    //            //            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

    //            //            float p = acc_log_t + (float)Math.Log(t_i);
    //            //            if (i == MaxIterationNum - 1) p = acc_log_t;

    //            //            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
    //            //            acc_log_t += (float)Math.Log(1 - t_i);

    //            //            if (Math.Exp(p) > max_p)
    //            //            {
    //            //                max_p = Math.Exp(p);
    //            //                max_iter = i;
    //            //            }

    //            //            if (BuilderParameters.PRED_RL == PredType.RL_AVGSIM)
    //            //            {
    //            //                FastVector.Add_Vector(FinalAns.Output.Data.MemPtr, b * FinalAns.Dim, AnsData[i].Output.Data.MemPtr, b * FinalAns.Dim, FinalAns.Dim, 1, (float)Math.Exp(p));
    //            //            }
    //            //        }
    //            //        if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
    //            //        {
    //            //            FastVector.Add_Vector(FinalAns.Output.Data.MemPtr, b * FinalAns.Dim, AnsData[max_iter].Output.Data.MemPtr, b * FinalAns.Dim, FinalAns.Dim, 0, 1);
    //            //        }
    //            //    }
    //            //    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize * FinalAns.Dim);

    //            //    for (int i = 0; i < MaxIterationNum; i++)
    //            //    {
    //            //        AnswerProb[i].BatchSize = InitStatus.BatchSize;
    //            //        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
    //            //    }
    //            //}
    //            //#endregion.
    //            #endregion.

    //        }
    //        public override void CleanDeriv()
    //        {
    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                ComputeLib.Zero(AnswerProb[i].Deriv.Data, AnswerProb[i].BatchSize);
    //            }
    //        }

    //        /// <summary>
    //        /// Stage 1 : Iterative Attention Model (Supervised Training).
    //        /// </summary>
    //        /// <param name="cleanDeriv"></param>
    //        public override void Backward(bool cleanDeriv)
    //        {
    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                Array.Clear(TermData[i].Deriv.Data.MemPtr, 0, TermData[i].BatchSize);
    //            }


    //            for (int b = 0; b < GMemMatch.SrcSize; b++)
    //            {
    //                for (int i = 0; i < MaxIterationNum; i++)
    //                {
    //                    float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

    //                    TermData[i].Deriv.Data.MemPtr[b] += reward * (1 - TermData[i].Output.Data.MemPtr[b]);

    //                    for (int hp = 0; hp < i; hp++)
    //                    {
    //                        TermData[hp].Deriv.Data.MemPtr[b] += -(float)Math.Pow(0.95f, i - hp) * reward * TermData[hp].Output.Data.MemPtr[b];
    //                    }

    //                    if (TermData[i].Deriv.Data.MemPtr[b] > Math.Abs(1))
    //                    {
    //                        Logger.WriteLog("Start TerminalRunner Deriv is too large {0}, step {1}", TermData[i].Deriv.Data.MemPtr[b], i);
    //                    }
    //                }
    //            }

    //            for (int b = 0; b < GMemMatch.SrcSize; b++)
    //            {
    //                TermData[MaxIterationNum - 1].Deriv.Data.MemPtr[b] = 0;
    //            }

    //            for (int i = 0; i < MaxIterationNum; i++)
    //            {
    //                TermData[i].Deriv.Data.SyncFromCPU(TermData[i].BatchSize);
    //            }
    //        }

    //        public override void Update()
    //        { }
    //    }

    //    public static List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> AddLSTMEncoder(ComputationGraph cg, LSTMStructure lstmModel,
    //        SeqDenseRecursiveData input, RunnerBehavior behavior)
    //    {
    //        List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> memory = new List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>>();
    //        SeqDenseRecursiveData SrclstmOutput = input;
    //        /***************** LSTM Encoder ******************************/
    //        for (int i = 0; i < lstmModel.LSTMCells.Count; i++)
    //        {
    //            FastLSTMDenseRunner<SeqDenseRecursiveData> encoderRunner = new FastLSTMDenseRunner<SeqDenseRecursiveData>(lstmModel.LSTMCells[i], SrclstmOutput, behavior);
    //            SrclstmOutput = (SeqDenseRecursiveData)cg.AddRunner(encoderRunner);
    //            memory.Add(new Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>(encoderRunner.Output, encoderRunner.C));
    //        }
    //        return memory;
    //    }

    //    public static ComputationGraph BuildComputationGraph(IDataCashier<SeqSparseDataSource, SequenceDataStat> srcData,
    //                                                         IDataCashier<SeqSparseDataSource, SequenceDataStat> tgtData,
    //                                                         EmbedStructure embedSrc, EmbedStructure embedTgt,
    //                                                         LayerStructure transO, LayerStructure transC,
    //                                                         LSTMStructure encoder, LSTMStructure reverseEncoder,

    //                                                         MatrixStructure globalMemory, MLPAttentionStructure globalAttStruct,
    //                                                         GRUCell gruStruct, LayerStructure ansStruct, List<LayerStructure> termStruct,

    //                                                         LSTMStructure decoder,
    //                                                         MLPAttentionStructure attentions, MLPAttentionStructure reasonAtt, EmbedStructure decodeEmbedTgt,
    //                                                         RunnerBehavior Behavior, CompositeNNStructure Model, EvaluationType evalType)
    //    {
    //        ComputationGraph cg = new ComputationGraph() { StatusReportSteps = Behavior.RunMode == DNNRunMode.Train ? 50 : 500 };

    //        /**************** Get Source and Target Data from DataCashier *********/
    //        SeqSparseDataSource SrcData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(srcData, Behavior));
    //        SeqSparseDataSource TgtData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(tgtData, Behavior));


    //        SeqDenseRecursiveData SrcD1Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData.SequenceData, false, Behavior));
    //        SeqDenseRecursiveData SrcD2Embed = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedSrc, SrcData.SequenceData, true, Behavior));

    //        List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD1Memory = AddLSTMEncoder(cg, encoder, SrcD1Embed, Behavior);
    //        List<Tuple<SeqDenseRecursiveData, SeqDenseRecursiveData>> SrcD2Memory = AddLSTMEncoder(cg, reverseEncoder, SrcD2Embed, Behavior);

    //        /**************** Bidirection Source LSTM Encoder Ensemble *********/
    //        List<SeqDenseBatchData> SrcEnsembleMemory = new List<SeqDenseBatchData>();
    //        List<Tuple<HiddenBatchData, HiddenBatchData>> SrcEnsembleStatus = new List<Tuple<HiddenBatchData, HiddenBatchData>>();

    //        for (int i = 0; i < SrcD1Memory.Count; i++)
    //        {
    //            HiddenBatchData SrcD1O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item1, true, 0, SrcD1Memory[i].Item1.MapForward, Behavior));
    //            HiddenBatchData SrcD2O = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item1, false, 0, SrcD2Memory[i].Item1.MapForward, Behavior));

    //            HiddenBatchData SrcD1C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD1Memory[i].Item2, true, 0, SrcD1Memory[i].Item2.MapForward, Behavior));
    //            HiddenBatchData SrcD2C = (HiddenBatchData)cg.AddRunner(new SeqOrderMatrixRunner(SrcD2Memory[i].Item2, false, 0, SrcD2Memory[i].Item2.MapForward, Behavior));

    //            HiddenBatchData SrcO = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1O, SrcD2O }, Behavior));
    //            HiddenBatchData SrcC = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { SrcD1C, SrcD2C }, Behavior));

    //            HiddenBatchData newSrcO = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transO, SrcO, Behavior));
    //            HiddenBatchData newSrcC = (HiddenBatchData)cg.AddRunner(new FullyConnectHiddenRunner<HiddenBatchData>(transC, SrcC, Behavior));
    //            SrcEnsembleStatus.Add(new Tuple<HiddenBatchData, HiddenBatchData>(newSrcO, newSrcC));
    //            //SrcEnsembleStatus.Add(new Tuple<BigLearn.HiddenBatchData, BigLearn.HiddenBatchData>(SrcO, SrcC));

    //            SeqDenseBatchData SrcSeqD1O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD1Memory[i].Item1, Behavior));
    //            SeqDenseBatchData SrcSeqD2O = (SeqDenseBatchData)cg.AddRunner(new Cook_TransposeSeq2SeqRunner(SrcD2Memory[i].Item1, Behavior));

    //            SeqDenseBatchData SrcSeqO = (SeqDenseBatchData)cg.AddRunner(new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { SrcSeqD1O, SrcSeqD2O }, Behavior));

    //            SrcEnsembleMemory.Add(SrcSeqO);
    //        }

    //        HiddenBatchData finalQueryOutput = SrcEnsembleStatus.Last().Item1;

    //        /// Reasoning process.
    //        IntArgument batchSize = new IntArgument("size");
    //        cg.AddRunner(new HiddenDataBatchSizeRunner(finalQueryOutput, batchSize, Behavior));

    //        BiMatchBatchData GlobalMemMatch = (BiMatchBatchData)cg.AddRunner(new RepeatBiMatchRunner(finalQueryOutput.MAX_BATCHSIZE, globalMemory.Size, batchSize, Behavior));

    //        ReasonNetRunner ReasonRunner = new ReasonNetRunner(cg, finalQueryOutput, BuilderParameters.Recurrent_Step,
    //                                                           globalMemory, GlobalMemMatch, globalAttStruct, BuilderParameters.Gamma,
    //                                                           // QuerySeqMem, QueryMemMatch, QueryAttStruct,
    //                                                           gruStruct, termStruct, Behavior);
    //        cg.AddRunner(ReasonRunner);


    //        if (Behavior.RunMode == DNNRunMode.Train || evalType == EvaluationType.PERPLEXITY)
    //        {
    //            //List<MLPAttentionV2Runner> tgtAttentionRunners = new List<MLPAttentionV2Runner>();
    //            //for (int i = 0; i < SrcEnsembleMemory.Count; i++)
    //            //    tgtAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
    //            //        new MLPAttentionV2Runner(attentions[i], SrcEnsembleMemory[i], null, BuilderParameters.TgtSeqMaxLen,
    //            //        TgtData.SequenceData.Stat.MAX_BATCHSIZE, TgtData.SequenceData.Stat.MAX_SEQUENCESIZE, Behavior) : null);

    //            //SeqDenseRecursiveData TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedTgt, TgtData.SequenceData, false, Behavior));

    //            //for (int i = 0; i < decoder.LSTMCells.Count; i++)
    //            //    TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(decoder.LSTMCells[i],
    //            //        TgtlstmOutput, SrcEnsembleStatus[i].Item1, SrcEnsembleStatus[i].Item2, tgtAttentionRunners[i], Behavior));

    //            //if (DeepNet.BuilderParameters.ModelUpdatePerSave > 0)
    //            //    cg.AddRunner(new ModelDiskDumpRunner(Model, DeepNet.BuilderParameters.ModelUpdatePerSave, BuilderParameters.ModelOutputPath + "\\Seq2Seq"));

    //            //if (Behavior.RunMode == DNNRunMode.Train)
    //            //    cg.AddObjective(new EmbedFullySoftmaxRunner(decodeEmbedTgt, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior));
    //            //else
    //            //    cg.AddRunner(new EmbedFullySoftmaxRunner(decodeEmbedTgt, TgtlstmOutput, TgtlstmOutput.MapBackward, TgtData.SequenceLabel, BuilderParameters.Gamma, BuilderParameters.ScoreOutputPath, Behavior));

    //            /**************** Target LSTM Decoder *********/
    //            SeqDenseRecursiveData TgtlstmInput = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(embedTgt, TgtData.SequenceData, false, Behavior));

    //            MLPAttentionV2Runner alignmentRunner = new MLPAttentionV2Runner(attentions, SrcEnsembleMemory.Last(), BuilderParameters.TgtSeqMaxLen, TgtlstmInput.MAX_BATCHSIZE, TgtlstmInput.MAX_SENTSIZE, Behavior);
    //            alignmentRunner.IsDelegate = false;
    //            cg.AddRunner(alignmentRunner);

    //            List<SeqDenseRecursiveData> TgtDecodeOutputs = new List<SeqDenseRecursiveData>();
    //            for (int i = 0; i < BuilderParameters.Recurrent_Step; i++)
    //            {
    //                SeqDenseBatchData reasonMem = (SeqDenseBatchData)cg.AddRunner(new EnsembleSeqMatrixRunner(ReasonRunner.StateData.Take(i + 1).ToList(), Behavior));
    //                MLPAttentionV2Runner reasonAttRunner = new MLPAttentionV2Runner(reasonAtt, reasonMem, BuilderParameters.TgtSeqMaxLen, TgtlstmInput.MAX_BATCHSIZE, TgtlstmInput.MAX_SENTSIZE, Behavior);
    //                reasonAttRunner.IsDelegate = false;
    //                cg.AddRunner(reasonAttRunner);

    //                EnsembleAttentionRunner ensembleAttRunner = new EnsembleAttentionRunner(new List<BasicMLPAttentionRunner>() { alignmentRunner, reasonAttRunner }, 
    //                    BuilderParameters.TgtSeqMaxLen, TgtlstmInput.MAX_BATCHSIZE, TgtlstmInput.MAX_SENTSIZE, reasonMem.Dim + SrcEnsembleMemory.Last().Dim, Behavior);

    //                SeqDenseRecursiveData TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(decoder.LSTMCells[0],
    //                        TgtlstmInput, finalQueryOutput, finalQueryOutput, ensembleAttRunner, Behavior));
    //                TgtDecodeOutputs.Add(TgtlstmOutput);
    //            }
    //            cg.AddObjective(new EmbedSoftmaxContrastiveRewardRunner(decodeEmbedTgt, TgtDecodeOutputs.ToArray(),
    //                TgtlstmInput.MapForward, TgtlstmInput.MapBackward, ReasonRunner.AnswerProb, TgtData.SequenceLabel, BuilderParameters.Gamma, Behavior, true));
    //        }
    //        else
    //        {
    //            List<MLPAttentionV2Runner> decodeAttentionRunners = new List<MLPAttentionV2Runner>();
    //            for (int i = 0; i < SrcEnsembleMemory.Count; i++)
    //                decodeAttentionRunners.Add(BuilderParameters.LAYER_ATTENTION[i] ?
    //                    new MLPAttentionV2Runner(attentions, SrcEnsembleMemory[i], TgtData.Stat.MAX_BATCHSIZE, BuilderParameters.BeamSearchCandidate * BuilderParameters.BeamSearchCandidate, Behavior) : null);

    //            // the probability of exactly generate the true tgt.
    //            LSTMEmbedDecodingRunner decodeRunner = new LSTMEmbedDecodingRunner(decoder, embedTgt, decodeEmbedTgt, SrcEnsembleStatus,
    //                decodeAttentionRunners.Select(i => (BasicMLPAttentionRunner)i).ToList(), Behavior, true);
    //            EnsembleBeamSearchRunner beamRunner = new EnsembleBeamSearchRunner(BuilderParameters.BeginWordIndex, BuilderParameters.TerminalWordIndex,
    //                BuilderParameters.BeamSearchCandidate, BuilderParameters.BeamSearchDepth,
    //                decodeEmbedTgt.VocabSize, TgtData.Stat.MAX_BATCHSIZE, Behavior);
    //            beamRunner.AddBeamSearch(decodeRunner);
    //            cg.AddRunner(beamRunner);

    //            switch (evalType)
    //            {
    //                case EvaluationType.ACCURACY:
    //                    cg.AddRunner(new BeamAccuracyRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel));
    //                    break;
    //                case EvaluationType.BSBLEU:
    //                    cg.AddRunner(new BLEUDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath, BuilderParameters.MetricOutputPath));
    //                    break;
    //                case EvaluationType.ROUGE:
    //                    cg.AddRunner(new ROUGEDiskDumpRunner(beamRunner.BatchResult, TgtData.SampleIdx, TgtData.SequenceLabel, BuilderParameters.ScoreOutputPath));
    //                    break;
    //                case EvaluationType.TOPKBEAM:
    //                    cg.AddRunner(new TopKBeamRunner(beamRunner.BatchResult, BuilderParameters.BeamSearchCandidate, BuilderParameters.ScoreOutputPath));
    //                    break;
    //            }
    //        }
    //        return cg;
    //    }

    //    public override void Rock()
    //    {
    //        Logger.OpenLog(BuilderParameters.LogFile);
    //        Logger.WriteLog("Loading Training/Validation Data.");

    //        DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
    //        IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

    //        DataPanel.Init();
    //        Logger.WriteLog("Load Data Finished.");

    //        int aDirectLSTM = 1;
    //        int bDirectLSTM = 1;
    //        int ensemble = aDirectLSTM + bDirectLSTM;


    //        CompositeNNStructure modelStructure = new CompositeNNStructure();

    //        LSTMStructure D1Encoder = null;
    //        LSTMStructure D2Encoder = null;
    //        LSTMStructure Decoder = null;

    //        LayerStructure transO = null;
    //        LayerStructure transC = null;

    //        EmbedStructure embedEncodeSrc = null;
    //        EmbedStructure embedEncodeTgt = null;
    //        EmbedStructure embedDecodeTgt = null;

    //        List<MLPAttentionStructure> attentionStructures = new List<MLPAttentionStructure>();

    //        if (BuilderParameters.SeedModel.Equals(string.Empty))
    //        {
    //            D1Encoder = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM, DeviceType.GPU);
    //            D2Encoder = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM, DeviceType.GPU);
    //            Decoder = new LSTMStructure(BuilderParameters.Tgt_Embed, BuilderParameters.LAYER_DIM, BuilderParameters.LAYER_DIM.Select(i => i * ensemble).ToArray(), DeviceType.GPU);

    //            //encodeLSTMStructure = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM.Select(i => i / ensemble).ToArray(), DeviceType.GPU);
    //            //reverseEncodeLSTMStructure = new LSTMStructure(BuilderParameters.Src_Embed, BuilderParameters.LAYER_DIM.Select(i => i / ensemble).ToArray(), DeviceType.GPU);
    //            //decodeLSTMStructure = new LSTMStructure(BuilderParameters.Tgt_Embed, BuilderParameters.LAYER_DIM, BuilderParameters.LAYER_DIM, DeviceType.GPU);

    //            transO = new LayerStructure(BuilderParameters.LAYER_DIM.Last() * ensemble, BuilderParameters.LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);
    //            transC = new LayerStructure(BuilderParameters.LAYER_DIM.Last() * ensemble, BuilderParameters.LAYER_DIM.Last(), A_Func.Tanh, N_Type.Fully_Connected, 1, 0, false);

    //            embedEncodeSrc = new EmbedStructure(DataPanel.TrainSrcBin.Stat.FEATURE_DIM, BuilderParameters.Src_Embed, DeviceType.GPU);
    //            embedEncodeTgt = new EmbedStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.Tgt_Embed, DeviceType.GPU);
    //            embedDecodeTgt = new EmbedStructure(DataPanel.TrainTgtBin.Stat.FEATURE_DIM, BuilderParameters.LAYER_DIM.Last(), DeviceType.GPU);

    //            //ensemble * 
    //            for (int i = 0; i < BuilderParameters.LAYER_DIM.Length; i++)
    //                attentionStructures.Add(new MLPAttentionStructure(BuilderParameters.LAYER_DIM[i], ensemble * BuilderParameters.LAYER_DIM[i], BuilderParameters.Attention_Hidden, DeviceType.GPU));

    //            modelStructure.AddLayer(D1Encoder);
    //            modelStructure.AddLayer(D2Encoder);
    //            modelStructure.AddLayer(Decoder);

    //            modelStructure.AddLayer(transO);
    //            modelStructure.AddLayer(transC);

    //            modelStructure.AddLayer(embedEncodeSrc);
    //            modelStructure.AddLayer(embedEncodeTgt);
    //            modelStructure.AddLayer(embedDecodeTgt);

    //            for (int i = 0; i < attentionStructures.Count; i++)
    //                modelStructure.AddLayer(attentionStructures[i]);
    //        }
    //        else
    //        {
    //            using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SeedModel, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
    //            {
    //                modelStructure = new CompositeNNStructure(modelReader, DeviceType.GPU);

    //                D1Encoder = (LSTMStructure)modelStructure.CompositeLinks[0];
    //                D2Encoder = (LSTMStructure)modelStructure.CompositeLinks[1];
    //                Decoder = (LSTMStructure)modelStructure.CompositeLinks[2];

    //                transO = (LayerStructure)modelStructure.CompositeLinks[3];
    //                transC = (LayerStructure)modelStructure.CompositeLinks[4];

    //                embedEncodeSrc = (EmbedStructure)modelStructure.CompositeLinks[5];
    //                embedEncodeTgt = (EmbedStructure)modelStructure.CompositeLinks[6];
    //                embedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[7];
    //                for (int i = 8; i < modelStructure.CompositeLinks.Count; i++)
    //                    attentionStructures.Add((MLPAttentionStructure)modelStructure.CompositeLinks[i]);
    //            }
    //        }


    //        switch (BuilderParameters.RunMode)
    //        {
    //            case DNNRunMode.Train:

    //                //modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer);

    //                ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrcBin, DataPanel.TrainTgtBin,
    //                    embedEncodeSrc, embedEncodeTgt, transO, transC, D1Encoder, D2Encoder, Decoder, attentionStructures, embedDecodeTgt,
    //                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib },
    //                    modelStructure, EvaluationType.PERPLEXITY);
    //                trainCG.InitOptimizer(modelStructure, OptimizerParameters.StructureOptimizer, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });


    //                ComputationGraph validCG = null;
    //                if (BuilderParameters.IsValidFile)
    //                    validCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
    //                        embedEncodeSrc, embedEncodeTgt, transO, transC, D1Encoder, D2Encoder, Decoder, attentionStructures, embedDecodeTgt,
    //                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
    //                        modelStructure, BuilderParameters.Evaluation);


    //                ComputationGraph devCG = null;
    //                if (BuilderParameters.IsDevFile)
    //                    devCG = BuildComputationGraph(DataPanel.DevSrcBin, DataPanel.DevTgtBin,
    //                        embedEncodeSrc, embedEncodeTgt, transO, transC, D1Encoder, D2Encoder, Decoder, attentionStructures, embedDecodeTgt,
    //                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
    //                        modelStructure, EvaluationType.PERPLEXITY);

    //                if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

    //                double validScore = 0;
    //                double bestValidScore = double.MinValue;
    //                double bestValidIter = -1;

    //                double bestDevScore = double.MaxValue;
    //                double bestDevIter = -1;
    //                double bestDevValidScore = 0;

    //                for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
    //                {
    //                    double loss = trainCG.Execute();
    //                    Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
    //                    string modelName = string.Format(@"{0}\\ReasoNet.Seq2Seq.{1}.model", BuilderParameters.ModelOutputPath, iter);

    //                    using (BinaryWriter writer = new BinaryWriter(new FileStream(modelName, FileMode.Create, FileAccess.Write)))
    //                    {
    //                        modelStructure.Serialize(writer);
    //                    }

    //                    if (BuilderParameters.IsValidFile)
    //                    {
    //                        validScore = validCG.Execute();
    //                        if (validScore >= bestValidScore)
    //                        {
    //                            bestValidScore = validScore;
    //                            bestValidIter = iter;
    //                        }
    //                        Logger.WriteLog("Best Valid Score {0} at iteration {1}", bestValidScore, bestValidIter);
    //                    }

    //                    if (BuilderParameters.IsDevFile)
    //                    {
    //                        double devScore = devCG.Execute();
    //                        if (devScore < bestDevScore)
    //                        {
    //                            bestDevScore = devScore;
    //                            bestDevIter = iter;
    //                            bestDevValidScore = validScore;
    //                        }
    //                        Logger.WriteLog("Best Dev Score {0} at iteration {1}  Valid Score {2}", bestDevScore, bestDevIter, bestDevValidScore);
    //                    }
    //                }
    //                break;
    //            case DNNRunMode.Predict:
    //                ComputationGraph predCG = BuildComputationGraph(DataPanel.ValidSrcBin, DataPanel.ValidTgtBin,
    //                        embedEncodeSrc, embedEncodeTgt, transO, transC, D1Encoder, D2Encoder,
    //                        Decoder, attentionStructures, embedDecodeTgt,
    //                        new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib },
    //                        modelStructure, BuilderParameters.Evaluation);
    //                double predScore = predCG.Execute();
    //                Logger.WriteLog("Prediction Score {0}", predScore);
    //                break;
    //        }
    //        Logger.CloseLog();

    //    }

    //    /// <summary>
    //    /// Data Panel.
    //    /// </summary>
    //    public class DataPanel
    //    {

    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainSrcBin = null;
    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;

    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidSrcBin = null;
    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;

    //        /// <summary>
    //        ///  Report PPL instead of BLEU Score.
    //        /// </summary>
    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevSrcBin = null;
    //        public static DataCashier<SeqSparseDataSource, SequenceDataStat> DevTgtBin = null;

    //        public static void Init()
    //        {
    //            if (BuilderParameters.IsValidFile)
    //            {
    //                ValidSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidSrcData);
    //                ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.ValidTgtData);

    //                ValidSrcBin.InitThreadSafePipelineCashier(100, false);
    //                ValidTgtBin.InitThreadSafePipelineCashier(100, false);
    //            }

    //            if (BuilderParameters.IsDevFile)
    //            {
    //                DevSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevSrcData);
    //                DevTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.DevTgtData);

    //                DevSrcBin.InitThreadSafePipelineCashier(100, false);
    //                DevTgtBin.InitThreadSafePipelineCashier(100, false);
    //            }

    //            if (BuilderParameters.RunMode == DNNRunMode.Train)
    //            {
    //                TrainSrcBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainSrcData);
    //                TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainTgtData);

    //                TrainSrcBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //                TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
    //            }

    //        }
    //    }
    //}
}
