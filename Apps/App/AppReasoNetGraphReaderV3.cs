﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;
namespace BigLearn.DeepNet
{
    public class AppReasoNetGraphReaderV3 : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("VERTEX-TYPE", new ParameterArgument("10", "Number of Vertex Type."));
                Argument.Add("VERTEX-NUM", new ParameterArgument("16", "Number of Vertexes"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size"));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Training Data"));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Validation Data"));


                Argument.Add("EMBED-LAYER-DIM", new ParameterArgument("300", "DNN Layer Dim"));
                Argument.Add("EMBED-LAYER-AF", new ParameterArgument("0", "DNN Layer AF" + ParameterUtil.EnumValues(typeof(A_Func))));
                Argument.Add("WORD-DROPOUT", new ParameterArgument("0", "Dropout for Word Embedding 1."));
                Argument.Add("WORD-EMBEDDING", new ParameterArgument(string.Empty, "Word Embedding 1 File"));
                Argument.Add("UNK-INIT", new ParameterArgument("0", "0 : zero initialize; 1 : random initialize"));

                Argument.Add("CHAR-EMBED-DIM", new ParameterArgument("0", "Char Embedding Dim."));
                Argument.Add("CHAR-EMBED-WIN", new ParameterArgument("5", "Char Embedding Win."));

                Argument.Add("HIGHWAY-LAYER", new ParameterArgument("2", "Highway Layers."));
                Argument.Add("HIGHWAY-SHARE", new ParameterArgument("0", "0: Nonshare highway; 1 : share highway;"));

                Argument.Add("LSTM-DIM", new ParameterArgument("256,256", "LSTM Dimension."));


                Argument.Add("MEMORY-ENSEMBLE", new ParameterArgument("2", "2 : two-way ensemble; 3 : three-way ensemble;"));

                Argument.Add("SOFT1-DROPOUT", new ParameterArgument("0", "Soft 1 Dropout;"));
                Argument.Add("SOFT2-DROPOUT", new ParameterArgument("0", "Soft 2 Dropout;"));
                Argument.Add("LSTM-DROPOUT", new ParameterArgument("0", "LSTM Embed Dropout;"));
                Argument.Add("EMBED-DROPOUT", new ParameterArgument("0", "Embeding Dropout;"));
                Argument.Add("COATT-DROPOUT", new ParameterArgument("0", "CoATT Dropout;"));
                Argument.Add("CNN-DROPOUT", new ParameterArgument("0", "CNN Dropout;"));
                Argument.Add("L3G-DROPOUT", new ParameterArgument("0", "L3G DNN Dropout;"));
                Argument.Add("CHAR-DROPOUT", new ParameterArgument("0", "Char CNN Dropout;"));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

                Argument.Add("ANS-HID", new ParameterArgument("256", "Answer Hidden Dimension"));
                Argument.Add("ATT-HID", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("ATT-TYPE", new ParameterArgument(((int)CrossSimType.Product).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("SELF-ATT-HID", new ParameterArgument("128", "Attention Hidden Dimension"));
                Argument.Add("SELF-ATT-TYPE", new ParameterArgument(((int)CrossSimType.Addition).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));
                Argument.Add("SELF-ATT-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Attention Af : " + ParameterUtil.EnumValues(typeof(A_Func))));

                Argument.Add("RECURRENT-DIM", new ParameterArgument("128", "Recurrent state dimension,"));
                Argument.Add("RECURRENT-STEP", new ParameterArgument("3", "Recurrent Steps"));
                Argument.Add("RECURRENT-POOL", new ParameterArgument((((int)PoolingType.RL)).ToString(), "Recurrent Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));
                Argument.Add("QUERY-POOL", new ParameterArgument((((int)PoolingType.LAST)).ToString(), "Query Pool Type : " + ParameterUtil.EnumValues(typeof(PoolingType))));


                // Softmax Randomup.
                Argument.Add("GAMMA", new ParameterArgument("1", " Smooth Parameter."));

                Argument.Add("SCORE-PATH", new ParameterArgument("output.score", "Output Score Path"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "SEED Model"));

                Argument.Add("DEBUG-FILE", new ParameterArgument(string.Empty, "DEBUG File."));
                Argument.Add("R-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("RL-ALL", new ParameterArgument("1", "RL All"));
                Argument.Add("RL-DISCOUNT", new ParameterArgument("0.95", "RL All"));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));

                Argument.Add("SPAN-OBJECTIVE", new ParameterArgument("0", "Span Objective; 0 : start-end detection; 1 : span start-end detection."));
                Argument.Add("SPAN-LENGTH", new ParameterArgument("0", "Span length"));
                Argument.Add("TRAIN-SPAN-LENGTH", new ParameterArgument("10000", "Span length"));
                Argument.Add("MAX-D-LENGTH", new ParameterArgument("0", "Maximum Document length"));

                Argument.Add("ENDPOINT-CONFIG", new ParameterArgument("0", "0: old M2; 1: new M2;"));
                Argument.Add("STARTPOINT-CONFIG", new ParameterArgument("0", "0: old M1; 1: new M1; 2: all new M1;"));

                //TERMINATE-DISCOUNT
                Argument.Add("TERMINATE-DISCOUNT", new ParameterArgument("0.1", "Reward discount on terminate gate;"));
            }

            /// <summary>
            /// DNN Run Mode.
            /// </summary>
            public static DNNRunMode RunMode { get { return IsTrainFile ? DNNRunMode.Train : DNNRunMode.Predict; } }
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static int VERTEX_TYPE { get { return int.Parse(Argument["VERTEX-TYPE"].Value); } }
            public static int VERTEX_NUM { get { return int.Parse(Argument["VERTEX-NUM"].Value); } }

            /// <summary>
            /// Feature 0 : X
            /// Feature 1 : ->
            /// Feature [2, VERTEX_TYPE + 1] : vertex_type
            /// Feature [VERTEX_TYPE + 2, VERTEX_TYPE + 2 + VERTEX_NUM] : vertex_num
            /// </summary>
            public static int FEATURE_DIM { get { return VERTEX_TYPE + VERTEX_NUM + 1 + 1; } }
            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            #region train, test, validation dataset.
            public static string Train { get { return Argument["TRAIN"].Value; } }
            public static bool IsTrainFile { get { return (!Train.Equals(string.Empty)); } }
            public static string TrainContextBin { get { return string.Format("{0}.para.{1}.bin", Train, MiniBatchSize); } }
            public static string TrainQueryBin { get { return string.Format("{0}.ques.{1}.bin", Train, MiniBatchSize); } }
            public static string TrainAnsBin { get { return string.Format("{0}.ans.{1}.bin", Train, MiniBatchSize); } }


            public static string Valid { get { return Argument["VALID"].Value; } }
            public static bool IsValidFile { get { return (!Valid.Equals(string.Empty)); } }
            public static string ValidContextBin { get { return string.Format("tmp.para.{0}.bin", MiniBatchSize); } }
            public static string ValidQueryBin { get { return string.Format("tmp.ques.{0}.bin", MiniBatchSize); } }
            public static string ValidAnsBin { get { return string.Format("tmp.ans.{0}.bin", MiniBatchSize); } }
            #endregion.

            public static int[] EMBED_LAYER_DIM { get { return Argument["EMBED-LAYER-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] EMBED_ACTIVATION { get { return Argument["EMBED-LAYER-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] LSTM_DIM { get { return Argument["LSTM-DIM"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int ANS_HID_DIM { get { return int.Parse(Argument["ANS-HID"].Value); } }
            public static int ATT_HID_DIM { get { return int.Parse(Argument["ATT-HID"].Value); } }

            public static int SELF_ATT_HID_DIM { get { return int.Parse(Argument["SELF-ATT-HID"].Value); } }
            public static int SelfAttType { get { return int.Parse(Argument["SELF-ATT-TYPE"].Value); } }
            public static A_Func SelfAttAF { get { return (A_Func)int.Parse(Argument["SELF-ATT-AF"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static float RL_DISCOUNT { get { return float.Parse(Argument["RL-DISCOUNT"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }

            public static string ScoreOutputPath { get { return Argument["SCORE-PATH"].Value; } }
            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SeedModel { get { return Argument["SEED-MODEL"].Value; } }

            public static RndRecurrentInit RndInit { get { return (RndRecurrentInit)int.Parse(Argument["R-INIT"].Value); } }

            public static float TERMINATE_DISCOUNT { get { return float.Parse(Argument["TERMINATE-DISCOUNT"].Value); } }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_GRAPH_READER_V3; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }
        public enum PredType { RL_MAXITER, RL_AVGSIM, RL_AVGPROB, RL_ENSEMBLE3 }


        class EpisodicMemRunner : CompositeNetRunner
        {
            public List<Dictionary<string, Tuple<float, float>>> Mem = new List<Dictionary<string, Tuple<float, float>>>();

            public EpisodicMemRunner(int maxBatchSize, RunnerBehavior behavior) : base(behavior)
            {
                for(int i=0;i<maxBatchSize;i++)
                {
                    Mem.Add(new Dictionary<string, Tuple<float, float>>());
                }
            }

            public override void Forward()
            {
                for(int i=0;i<Mem.Count;i++)
                {
                    Mem[i].Clear();
                }
            }

            public Tuple<float, float> LookUp(int b, string key)
            {
                if (!Mem[b].ContainsKey(key))
                {
                    Mem[b].Add(key, new Tuple<float, float>(0, 0));
                }
                return Mem[b][key];
            }
            public static Random random = new Random();
            public static float c = 2f;
            public static int UCBBandit(List<Tuple<float, float>> arms, float c, Random random)
            {
                float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);
                List<float> v = new List<float>();
                foreach (Tuple<float, float> arm in arms)
                {
                    v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
                }
                int idx = Util.MaximumValue(v.ToArray());
                return idx;
            }

            public void Write(int b, string key, float reward)
            {
                Mem[b][key] = new Tuple<float, float>(Mem[b][key].Item1 + reward, Mem[b][key].Item2 + 1);
            }

        }

        class EpisodicMemUpdateRunner : CompositeNetRunner
        {
            List<List<string>> Actions { get; set; }
            List<float> R { get; set; }
            EpisodicMemRunner EpisodicMem { get; set; }
            public EpisodicMemUpdateRunner(List<List<string>> actions, List<float> r, EpisodicMemRunner mem, RunnerBehavior behavior) : base(behavior)
            {
                EpisodicMem = mem;
                Actions = actions;
                R = r;
            }

            public override void Forward()
            {
                for (int b = 0; b < Actions.Count; b++)
                {
                    foreach (string a in Actions[b])
                    {
                        float v = 1 - R[b] * 1.0f / Actions[b].Count;
                        v = v <= 0 ? 0 : v;
                        v = v >= 1 ? 1 : v;
                        EpisodicMem.Write(b, a, v == 1 ? 1 : 0 ); // R[b] > 0 ? 0 : 1);//
                    } 
                }
            }
        }

        class EpisodicAttRunner : CompositeNetRunner
        {
            //public new SeqDenseBatchData Output { get; set; }
            List<List< string>> Tgt { get; set; }
            List<List< string>> Src { get; set; }

            public List<List<string>> Attention = new List<List<string>>();

            public List<List<string>> Actions = new List<List<string>>();
            /// <summary>
            /// 
            /// </summary>
            EpisodicMemRunner EpisodicMem { get; set; }

            public EpisodicAttRunner(List<List<string>> src, List<List<string>> tgt, EpisodicMemRunner mem, RunnerBehavior behavior) : base(behavior)
            {
                Src = src;
                Tgt = tgt;
                EpisodicMem = mem;
            }

            bool IsMatchNode(string src, string tgt)
            {
                if (src.Equals("X") && DataPanel.IsNode(tgt))
                {
                    return true;
                }
                if (DataPanel.IsNode(src) && DataPanel.IsNode(tgt) && DataPanel.NodeType(src) == DataPanel.NodeType(tgt))
                {
                    return true;
                }
                return false;
            }

            Tuple<HashSet<string>, HashSet<string>> GetCompatiableActions(int srcIdx, List<string> src, List<string> action)
            {
                HashSet<string> Must = new HashSet<string>();
                HashSet<string> MustNot = new HashSet<string>();

                int type = -1;
                int id = -1;

                if (!src[srcIdx].Equals("X"))
                {
                    type = DataPanel.NodeType(src[srcIdx]);
                    id = DataPanel.NodeId(src[srcIdx]);
                }

                for (int i = 0; i<srcIdx; i++)
                {
                    if (src[i].Equals(src[srcIdx]))
                    {
                        if (!Must.Contains(action[i])) Must.Add(action[i]);
                    }
                    else if (DataPanel.IsNode(src[i]))
                    {
                        //int ntype = -1;
                        //int nid = -1;

                        //if (!src[i].Equals("X"))
                        //{
                        //    ntype = DataPanel.NodeType(src[i]);
                        //    nid = DataPanel.NodeId(src[i]);
                        //}

                        //if( (ntype == -1) || (ntype == type && id != nid) || (type == -1))
                        //{
                        if (!MustNot.Contains(action[i])) MustNot.Add(action[i]);
                        //}
                    }
                }
                return new Tuple<HashSet<string>, HashSet<string>>(Must, MustNot);
            } 

            public override void Forward()
            {
                Actions.Clear();
                Attention.Clear();
                for (int b = 0; b < Src.Count; b++)
                {
                    Actions.Add(new List<string>());
                    List<string> att = new List<string>();

                    for (int srcIdx = 0; srcIdx < Src[b].Count; srcIdx++)
                    {
                        string srcNode = Src[b][srcIdx];

                        if (DataPanel.IsNode(srcNode))
                        {
                            List<Tuple<float, float>> arms = new List<Tuple<float, float>>();
                            List<Tuple<string, string>> actions = new List<Tuple<string, string>>();

                            Tuple<HashSet<string>, HashSet<string>> m = GetCompatiableActions(srcIdx, Src[b], att);

                            for (int tgtIdx = 0; tgtIdx < Tgt[b].Count; tgtIdx++)
                            {
                                if (IsMatchNode(srcNode, Tgt[b][tgtIdx]))
                                {
                                    if (m.Item1.Count > 0 && !m.Item1.Contains(Tgt[b][tgtIdx])) continue;
                                    if (m.Item2.Count > 0 && m.Item2.Contains(Tgt[b][tgtIdx])) continue;

                                    string key = string.Format("{0}#{1}", srcNode, Tgt[b][tgtIdx]);
                                    arms.Add(EpisodicMem.LookUp(b, key));
                                    actions.Add(new Tuple<string, string>(key, Tgt[b][tgtIdx]));
                                }
                            }

                            if (arms.Count > 0)
                            {
                                int bestArm = EpisodicMemRunner.UCBBandit(arms, EpisodicMemRunner.c, EpisodicMemRunner.random);
                                Actions[b].Add(actions[bestArm].Item1);
                                att.Add(actions[bestArm].Item2);
                            }
                            else
                            {
                                att.Add("UNK");
                            }
                        }
                        else
                        {
                            att.Add(srcNode);
                        }
                        
                    }
                    Attention.Add(att);
                }
            }
        }

        class EpisodicAttV2Runner : CompositeNetRunner
        {
            //public new SeqDenseBatchData Output { get; set; }
            List<List<Tuple<string, string, string>>> Tgt { get; set; }
            List<List<Tuple<string, string, string>>> Src { get; set; }

            public List<List<string>> Attention = new List<List<string>>();

            public List<List<string>> Actions = new List<List<string>>();
            /// <summary>
            /// 
            /// </summary>
            EpisodicMemRunner EpisodicMem { get; set; }

            public EpisodicAttV2Runner(List<List<Tuple<string,string,string>>> src, List<List<Tuple<string, string, string>>> tgt, EpisodicMemRunner mem, RunnerBehavior behavior) : base(behavior)
            {
                Src = src;
                Tgt = tgt;
                EpisodicMem = mem;
            }

            bool IsMatchNode(string src, string tgt)
            {
                if (src.Equals("X") && DataPanel.IsNode(tgt))
                {
                    return true;
                }
                if (DataPanel.IsNode(src) && DataPanel.IsNode(tgt) && DataPanel.NodeType(src) == DataPanel.NodeType(tgt))
                {
                    return true;
                }
                return false;
            }

            bool IsMatch(Tuple<string, string, string> srclink, Tuple<string, string, string> tgtlink)
            {
                if (!DataPanel.IsNode(srclink.Item2) || !DataPanel.IsNode(tgtlink.Item2) || !IsMatchNode(srclink.Item2, tgtlink.Item2))
                {
                    return false;
                }

                if (!srclink.Item1.Equals(""))
                {
                    if (tgtlink.Item1.Equals(""))
                    {
                        return false;
                    }

                    HashSet<string> s1 = new HashSet<string>(srclink.Item1.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));
                    HashSet<string> t1 = new HashSet<string>(tgtlink.Item1.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));

                    
                    // target dictionary.
                    Dictionary<int, int> targetDict = new Dictionary<int, int>();
                    foreach (string t in t1)
                    {
                        int nodeType = DataPanel.NodeType(t);
                        if (!targetDict.ContainsKey(nodeType))
                        {
                            targetDict.Add(nodeType, 1);
                        }
                        else
                        {
                            targetDict[nodeType] += 1;
                        }
                    }


                    // source dictionary.
                    bool isX = false;
                    Dictionary<int, int> sourceDict = new Dictionary<int, int>();
                    foreach (string s in s1)
                    {
                        if (s.Equals("X")) { isX = true; continue; }
                        int nodeType = DataPanel.NodeType(s);
                        if (!sourceDict.ContainsKey(nodeType))
                        {
                            sourceDict.Add(nodeType, 1);
                        }
                        else
                        {
                            sourceDict[nodeType] += 1;
                        }
                    }



                    foreach (KeyValuePair<int, int> sitem in sourceDict)
                    {
                        if (targetDict.ContainsKey(sitem.Key) && targetDict[sitem.Key] >= sourceDict[sitem.Key])
                        {
                            continue;
                        }
                        return false;
                    }
                    if (isX && targetDict.Select(i => i.Value).Sum() <= sourceDict.Select(i => i.Value).Sum())
                    {
                        return false;
                    }

                }


                if (!srclink.Item3.Equals(""))
                {
                    if (tgtlink.Item3.Equals(""))
                    {
                        return false;
                    }

                    HashSet<string> s3 = new HashSet<string>(srclink.Item3.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));
                    HashSet<string> t3 = new HashSet<string>(tgtlink.Item3.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));
                    
                    // source dictionary.
                    bool isX = false;
                    Dictionary<int, int> sourceDict = new Dictionary<int, int>();
                    foreach (string s in s3)
                    {
                        if (s.Equals("X")) { isX = true; continue; }
                        int nodeType = DataPanel.NodeType(s);
                        if (!sourceDict.ContainsKey(nodeType))
                        {
                            sourceDict.Add(nodeType, 1);
                        }
                        else
                        {
                            sourceDict[nodeType] += 1;
                        }
                    }

                    // target dictionary.
                    Dictionary<int, int> targetDict = new Dictionary<int, int>();
                    foreach (string t in t3)
                    {
                        int nodeType = DataPanel.NodeType(t);
                        if (!targetDict.ContainsKey(nodeType))
                        {
                            targetDict.Add(nodeType, 1);
                        }
                        else
                        {
                            targetDict[nodeType] += 1;
                        }
                    }


                    foreach (KeyValuePair<int, int> sitem in sourceDict)
                    {
                        if (targetDict.ContainsKey(sitem.Key) && targetDict[sitem.Key] >= sourceDict[sitem.Key])
                        {
                            continue;
                        }
                        return false;
                    }

                    if (isX && targetDict.Select(i => i.Value).Sum() <= sourceDict.Select(i => i.Value).Sum())
                    {
                        return false;
                    }
                }

                return true;
            }




            Tuple<HashSet<string>, HashSet<string>> GetCompatiableActions(int srcIdx, List<string> src, List<string> action)
            {
                HashSet<string> Must = new HashSet<string>();
                HashSet<string> MustNot = new HashSet<string>();

                int type = -1;
                int id = -1;

                if (!src[srcIdx].Equals("X"))
                {
                    type = DataPanel.NodeType(src[srcIdx]);
                    id = DataPanel.NodeId(src[srcIdx]);
                }

                for (int i = 0; i < srcIdx; i++)
                {
                    if (src[i].Equals(src[srcIdx]))
                    {
                        if (!Must.Contains(action[i])) Must.Add(action[i]);
                    }
                    else if (DataPanel.IsNode(src[i]))
                    {
                        //int ntype = -1;
                        //int nid = -1;

                        //if (!src[i].Equals("X"))
                        //{
                        //    ntype = DataPanel.NodeType(src[i]);
                        //    nid = DataPanel.NodeId(src[i]);
                        //}

                        //if( (ntype == -1) || (ntype == type && id != nid) || (type == -1))
                        //{
                        if (!MustNot.Contains(action[i])) MustNot.Add(action[i]);
                        //}
                    }
                }
                return new Tuple<HashSet<string>, HashSet<string>>(Must, MustNot);
            }

            public override void Forward()
            {
                Actions.Clear();
                Attention.Clear();
                for (int b = 0; b < Src.Count; b++)
                {
                    Actions.Add(new List<string>());
                    List<string> att = new List<string>();

                    for (int srcIdx = 0; srcIdx < Src[b].Count; srcIdx++)
                    {
                        string srcNode = Src[b][srcIdx].Item2;
                        Tuple<string, string, string> srcPattern = Src[b][srcIdx];
                        if (DataPanel.IsNode(srcNode))
                        {
                            List<Tuple<float, float>> arms = new List<Tuple<float, float>>();
                            List<Tuple<string, string>> actions = new List<Tuple<string, string>>();

                            Tuple<HashSet<string>, HashSet<string>> m = GetCompatiableActions(srcIdx, Src[b].Select(i => i.Item2).ToList(), att);

                            for (int tgtIdx = 0; tgtIdx < Tgt[b].Count; tgtIdx++)
                            {
                                if (IsMatch(srcPattern, Tgt[b][tgtIdx]))
                                {
                                    if (m.Item1.Count > 0 && !m.Item1.Contains(Tgt[b][tgtIdx].Item2)) continue;
                                    if (m.Item2.Count > 0 && m.Item2.Contains(Tgt[b][tgtIdx].Item2)) continue;

                                    string key = string.Format("{0}#{1}", srcNode, Tgt[b][tgtIdx]);
                                    arms.Add(EpisodicMem.LookUp(b, key));
                                    actions.Add(new Tuple<string, string>(key, Tgt[b][tgtIdx].Item2));
                                }
                            }

                            if (arms.Count > 0)
                            {
                                int bestArm = EpisodicMemRunner.UCBBandit(arms, EpisodicMemRunner.c, EpisodicMemRunner.random);
                                Actions[b].Add(actions[bestArm].Item1);
                                att.Add(actions[bestArm].Item2);
                            }
                            else
                            {
                                att.Add("UNK");
                            }
                        }
                        else
                        {
                            att.Add(srcNode);
                        }

                    }
                    Attention.Add(att);
                }
            }
        }


        class AttentionRefLinkRunner : CompositeNetRunner
        {
            //public new SeqDenseBatchData Output { get; set; }
            List<List<Tuple<string, string, string>>> Tgt { get; set; }
            List<List<Tuple<string, string, string, string>>> Src { get; set; }

            public List<List<Tuple<string,string>>> Attention = new List<List<Tuple<string, string>>>();

            public AttentionRefLinkRunner(List<List<Tuple<string, string, string, string>>> src, List<List<Tuple<string, string, string>>> tgt, RunnerBehavior behavior) : base(behavior)
            {
                Src = src;
                Tgt = tgt;

            }

            bool IsMatch(Tuple<string, string, string, string> srclink, Tuple<string, string, string> tgtlink)
            {
                if (!DataPanel.IsNode(srclink.Item1) || !DataPanel.IsNode(tgtlink.Item2))
                {
                    return false;
                }

                if (!srclink.Item2.Equals(""))
                {
                    if (tgtlink.Item1.Equals(""))
                    {
                        return false;
                    }
                    if (!srclink.Item2.Contains(tgtlink.Item1) || !srclink.Item3.Contains(tgtlink.Item2))
                    {
                        return false;
                    }
                }


                if (!srclink.Item4.Equals(""))
                {
                    if (tgtlink.Item3.Equals(""))
                    {
                        return false;
                    }
                    if (!srclink.Item4.Contains(tgtlink.Item3) || !srclink.Item3.Contains(tgtlink.Item2))
                    {
                        return false;
                    }
                }

                return true;
            }

            public override void Forward()
            {
                Attention.Clear();
                for (int b = 0; b < Src.Count; b++)
                {
                    List<Tuple<string, string>> att = new List<Tuple<string, string>>();
                    for (int srcIdx = 0; srcIdx < Src[b].Count; srcIdx++)
                    {
                        HashSet<string> att_fea = new HashSet<string>();

                        Tuple<string, string, string, string> link = Src[b][srcIdx];

                        for (int tgtIdx = 0; tgtIdx < Tgt[b].Count; tgtIdx++)
                        {
                            if (IsMatch(link, Tgt[b][tgtIdx]))
                            {
                                if (!att_fea.Contains(Tgt[b][tgtIdx].Item2))
                                {
                                    att_fea.Add(Tgt[b][tgtIdx].Item2);
                                }
                            }
                        }
                        att.Add(new Tuple<string, string>(link.Item1, string.Join("#", att_fea)));
                    }
                    Attention.Add(att);
                }
            }
        }


        class UpdateRunner : CompositeNetRunner
        {
            public new SeqDenseBatchData Output { get; set; }
            public UpdateRunner(SeqDenseBatchData state, SeqDenseBatchData x, Tuple<LSTMStructure, LSTMStructure> updateStruct, RunnerBehavior behavior) : base(behavior)
            {
                EnsembleConcateSeqRunner concateRunner = new EnsembleConcateSeqRunner(new List<SeqDenseBatchData>() { state, x }, behavior);
                LinkRunners.Add(concateRunner);

                BiLSTMRunner lstmUpdateRunner = new BiLSTMRunner(updateStruct.Item1, updateStruct.Item2, concateRunner.Output, behavior);
                LinkRunners.Add(lstmUpdateRunner);

                Output = lstmUpdateRunner.Output;
            }
        }


        class EpisodicReasoNetRunner : CompositeNetRunner
        {
            int MaxT { get; set; }

            public List<string> Result { get; set; }

            public List<List<List<string>>> MData { get; set; }
            public List<List<float>> MReward { get; set; }


            public EpisodicReasoNetRunner(List<List<string>> qself, List<List<Tuple<string,string,string>>> qhighOrder,  List<List<Tuple<string, string, string>>> dlink, List<List<Tuple<string, string, string>>> dhighOrder,
                int maxCrossLen, int maxCrossStateLen, int maxT, RunnerBehavior behavior) : base(behavior)
            {
                MReward = new List<List<float>>();
                MData = new List<List<List<string>>>();

                MaxT = maxT;

                //BiLinkRunner linkDRunner = new BiLinkRunner(dself, behavior);
                //LinkRunners.Add(linkDRunner);
                //List<List<Tuple<string, string, string>>> dRef = linkDRunner.InOutLinkData;

                EpisodicMemRunner memRunner = new EpisodicMemRunner(BuilderParameters.MiniBatchSize, behavior);
                LinkRunners.Add(memRunner);
                for (int i = 0; i < MaxT; i++)
                {
                    EpisodicAttV2Runner attRunner = new EpisodicAttV2Runner(qhighOrder, dhighOrder, memRunner, behavior);
                    LinkRunners.Add(attRunner);

                    List<List<string>> att = attRunner.Attention;
                    BiLinkRunner linkAttRunner = new BiLinkRunner(att, behavior);
                    LinkRunners.Add(linkAttRunner);
                    List<List<Tuple<string, string, string>>> attRef = linkAttRunner.InOutLinkData;

                    DiscremRunner disRunner = new DiscremRunner(dlink, attRef, behavior);
                    LinkRunners.Add(disRunner);
                    List<float> R = disRunner.MisMatch;

                    EpisodicMemUpdateRunner updateRunner = new EpisodicMemUpdateRunner(attRunner.Actions, R, memRunner, behavior);
                    LinkRunners.Add(updateRunner);

                    MReward.Add(R);
                    MData.Add(att);
                }
                XAnswerRunner ansRunner = new XAnswerRunner(qself, MData, MReward, MaxT, behavior);
                LinkRunners.Add(ansRunner);
                Result = ansRunner.answer;
            }

            //public override void Forward()
            //{

            //}
        }

        class DiscremRunner : CompositeNetRunner
        {
            List<List<Tuple<string, string, string>>> D { get; set; }
            List<List<Tuple<string, string, string>>> QTry { get; set; }

            public List<float> MisMatch = new List<float>();
            public DiscremRunner(List<List<Tuple<string, string, string>>> d, List<List<Tuple<string, string, string>>> qtry, RunnerBehavior behavior) : base(behavior)
            {
                D = d;
                QTry = qtry;
            }

            public override void Forward()
            {
                MisMatch.Clear();
                for (int b = 0; b < QTry.Count; b++)
                {
                    float cc = 0;
                    for (int srcIdx = 0; srcIdx < QTry[b].Count; srcIdx++)
                    {
                        Tuple<string, string, string> me = QTry[b][srcIdx];
                        bool isThere = false;
                        if (DataPanel.IsNode(me.Item2))
                        {
                            for (int tgtIdx = 0; tgtIdx < D[b].Count; tgtIdx++)
                            {
                                Tuple<string, string, string> you = D[b][tgtIdx];

                                if(me.Item2.Equals(you.Item2) && me.Item1.Equals(you.Item1) && me.Item3.Equals(you.Item3))
                                {
                                    isThere = true;
                                    break;
                                }
                            }
                            if (!isThere)
                            {
                                cc += 1;
                            }
                        }
                    }
                    MisMatch.Add(cc);
                }
            }
        }

        class XAnswerRunner : CompositeNetRunner
        {
            List<List<List<string>>> Data { get; set; }
            List<List<float>> Reward { get; set; }
            List<List<string>> Q { get; set; }
            int MaxT { get; set; }
            public List<string> answer = new List<string>();
            public XAnswerRunner(List<List<string>> q,  List<List<List<string>>> data, List<List<float>> reward, int maxT, RunnerBehavior behavior) : base(behavior)
            {
                Data = data;
                Reward = reward;
                Q = q;
                MaxT = maxT;
            }

            public override void Forward()
            {
                answer.Clear();
                for (int b = 0; b < Reward[0].Count; b++)
                {
                    int idx = 0;
                    float minR = float.MaxValue;
                    for(int t=0;t<MaxT;t++)
                    {
                        if(Reward[t][b] < minR)
                        {
                            idx = t;
                            minR = Reward[t][b];
                        }
                    }
                    //int idx = Util.MinimumValue(Reward[b].ToArray());

                    if (minR > 0)
                    {
                        Console.WriteLine("something error!! {0}", minR);
                    }

                    Dictionary<string, int> Count = new Dictionary<string, int>();

                    string result = "";
                    int maxV = 0;

                    for (int srcIdx = 0; srcIdx < Data[idx][b].Count; srcIdx++)
                    {
                        if (Q[b][srcIdx].Equals("X"))
                        {

                            if (Count.ContainsKey(Data[idx][b][srcIdx])) Count[Data[idx][b][srcIdx]] += 1;
                            else Count[Data[idx][b][srcIdx]] = 1;

                            if (Count[Data[idx][b][srcIdx]] > maxV) { maxV = Count[Data[idx][b][srcIdx]]; result = Data[idx][b][srcIdx]; }



                        }
                    }
                    answer.Add(result);
                }

            }
        }

        class SampleRunner<T> : StructRunner
        {
            List<T> Data { get; set; }

            int Cursor = 0;
            int MiniBatchSize { get; set; }

            public new List<T> Output = new List<T>();

            public SampleRunner(List<T> data, int batchSize, RunnerBehavior behavior) :
                base(Structure.Empty, behavior)
            {
                Data = data;
                MiniBatchSize = batchSize;
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Cursor = 0;
            }

            public override void Forward()
            {
                Output.Clear();

                int batchSize = 0;
                while (batchSize < MiniBatchSize && batchSize + Cursor < Data.Count)
                {
                    Output.Add(Data[batchSize + Cursor]);
                    batchSize += 1;
                }
                Cursor += batchSize;
                if (batchSize == 0) { IsTerminate = true; return; }
            }

        }

        class BiLinkRunner : CompositeNetRunner
        {
            public List<List<Tuple<string, string, string>>> InOutLinkData { get; set; }

            List<List<string>> Data { get; set; }

            public BiLinkRunner(List<List<string>> data, RunnerBehavior behavior) : base(behavior)
            {
                Data = data;
                InOutLinkData = new List<List<Tuple<string, string, string>>>();
            }

            public override void Forward()
            {
                InOutLinkData.Clear();
                for (int b = 0; b < Data.Count; b++)
                {
                    int idx = 0;
                    List<Tuple<string, string, string>> in_out_Links = new List<Tuple<string, string, string>>();
                    foreach (string s in Data[b])
                    {
                        string o = "";
                        string i = "";

                        if (DataPanel.IsNode(s) && Data[b].Count > idx + 2 && Data[b][idx + 1].Equals("->"))
                        {
                            o = Data[b][idx + 2];
                        }
                        if (DataPanel.IsNode(s) && idx - 2 >= 0 && Data[b][idx - 1].Equals("->"))
                        {
                            i = Data[b][idx - 2];
                        }
                        in_out_Links.Add(new Tuple<string, string, string>(i, s, o));
                        idx += 1;
                    }
                    InOutLinkData.Add(in_out_Links);
                }

            }
        }

        class BiLinkRefRunner : CompositeNetRunner
        {
            public List<List<Tuple<string, string, string, string>>> InOutLinkData { get; set; }

            List<List<string>> Data { get; set; }
            List<List<string>> RefLink { get; set; }

            public BiLinkRefRunner(List<List<string>> data, List<List<string>> refLink, RunnerBehavior behavior) : base(behavior)
            {
                Data = data;
                RefLink = refLink;
                InOutLinkData = new List<List<Tuple<string, string, string, string>>>();
            }

            public override void Forward()
            {
                InOutLinkData.Clear();
                for (int b = 0; b < Data.Count; b++)
                {
                    int idx = 0;
                    List<Tuple<string, string, string, string>> in_out_Links = new List<Tuple<string, string, string, string>>();
                    foreach (string s in Data[b])
                    {
                        string o = "";
                        string i = "";

                        if (DataPanel.IsNode(s) && Data[b].Count > idx + 2 && Data[b][idx + 1].Equals("->"))
                        {
                            o = RefLink[b][idx + 2];
                        }
                        if (DataPanel.IsNode(s) && idx - 2 >= 0 && Data[b][idx - 1].Equals("->"))
                        {
                            i = RefLink[b][idx - 2];
                        }
                        in_out_Links.Add(new Tuple<string, string, string, string>(s, i, RefLink[b][idx], o));
                        idx += 1;
                    }
                    InOutLinkData.Add(in_out_Links);
                }

            }
        }

        class SelfLinkRefRunner : CompositeNetRunner
        {
            
            public List<List<string>> Attention = new List<List<string>>();

            List<List<Tuple<string, string>>> RefLink { get; set; }

            public SelfLinkRefRunner(List<List<Tuple<string, string>>> refLink, RunnerBehavior behavior) : base(behavior)
            {
                RefLink = refLink;
            }

            bool IsMatch(Tuple<string, string> src, Tuple<string, string> tgt)
            {
                if(DataPanel.IsNode(src.Item1) && src.Item1.Equals(tgt.Item1))
                {
                    return true;
                }
                return false;
            }

            bool IsNiMatch(Tuple<string, string> src, Tuple<string, string> tgt)
            {
                if (DataPanel.IsNode(src.Item1) && DataPanel.IsNode(tgt.Item1) && !src.Item1.Equals(tgt.Item1))
                {
                    return true;
                }
                return false;
            }


            public override void Forward()
            {
                Attention.Clear();
                for (int b = 0; b < RefLink.Count; b++)
                {
                    List<string> att = new List<string>();
                    for (int srcIdx = 0; srcIdx < RefLink[b].Count; srcIdx++)
                    {
                        
                        Tuple<string, string > link = RefLink[b][srcIdx];
                        HashSet<string> att_fea = new HashSet<string>(link.Item2.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));

                        for (int tgtIdx = 0; tgtIdx < RefLink[b].Count; tgtIdx++)
                        {
                            if (IsMatch(link, RefLink[b][tgtIdx]))
                            {
                                HashSet<string> new_att = new HashSet<string>();
                                foreach(string s in RefLink[b][tgtIdx].Item2.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    if(att_fea.Contains(s))
                                    {
                                        new_att.Add(s);
                                    }
                                }
                                att_fea = new_att;
                            }

                            if(IsNiMatch(link, RefLink[b][tgtIdx]))
                            {
                                string[] t = RefLink[b][tgtIdx].Item2.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);

                                if (t.Length == 1)
                                {
                                    if (att_fea.Contains(t[0]))
                                    {
                                        att_fea.Remove(t[0]);
                                    }
                                }
                            }
                        }
                        att.Add(string.Join("#", att_fea));
                    }
                    Attention.Add(att);
                }

            }
        }

        class SelfRefRunner : CompositeNetRunner
        {

            public List<List<Tuple<string, string, string>>> HighRefLink { get; set; }


            List<List<Tuple<string, string, string>>> RefLink { get; set; }

            public SelfRefRunner(List<List<Tuple<string, string, string>>> refLink, RunnerBehavior behavior) : base(behavior)
            {
                RefLink = refLink;
                HighRefLink = new List<List<Tuple<string, string, string>>>();
            }

            bool IsMatch(Tuple<string, string, string> src, Tuple<string, string, string> tgt)
            {
                if (DataPanel.IsNode(src.Item2) && src.Item2.Equals(tgt.Item2))
                {
                    return true;
                }
                return false;
            }
            
            public override void Forward()
            {
                HighRefLink.Clear();
                for (int b = 0; b < RefLink.Count; b++)
                {
                    List<Tuple<string, string, string>> att = new List<Tuple<string, string, string>>();
                    for (int srcIdx = 0; srcIdx < RefLink[b].Count; srcIdx++)
                    {
                        Tuple<string, string, string> link = RefLink[b][srcIdx];
                        HashSet<string> att_fea_1 = new HashSet<string>(link.Item1.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));
                        HashSet<string> att_fea_2 = new HashSet<string>(link.Item3.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));

                        if (DataPanel.IsNode(link.Item2))
                        {
                            for (int tgtIdx = 0; tgtIdx < RefLink[b].Count; tgtIdx++)
                            {
                                if (IsMatch(link, RefLink[b][tgtIdx]))
                                {
                                    foreach (string s in RefLink[b][tgtIdx].Item1.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries))
                                    {
                                        if (!att_fea_1.Contains(s))
                                        {
                                            att_fea_1.Add(s);
                                        }
                                    }

                                    foreach (string s in RefLink[b][tgtIdx].Item3.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries))
                                    {
                                        if (!att_fea_2.Contains(s))
                                        {
                                            att_fea_2.Add(s);
                                        }
                                    }
                                }

                            }
                        }
                        att.Add(new Tuple<string, string, string>(string.Join("#", att_fea_1), link.Item2, string.Join("#", att_fea_2)));
                    }
                    HighRefLink.Add(att);
                }

            }
        }


        class EvaluateRunner : ObjectiveRunner
        {
            List<int> Label { get; set; }
            List<List<Tuple<string, string, string, string>>> Answer { get; set; }
            List<List<string>> Q { get; set; }
            List<List<string>> D { get; set; }

            string ExtractAnswer(List<Tuple<string, string, string, string>> data)
            {
                Dictionary<string, int> Count = new Dictionary<string, int>();
                string result = "";
                int maxV = 0;
                for (int srcIdx = 0; srcIdx < data.Count; srcIdx++)
                {
                    if (data[srcIdx].Item1.Equals("X"))
                    {
                        HashSet<string> answer = new HashSet<string>(data[srcIdx].Item3.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));
                        foreach (string s in answer)
                        {
                            if (Count.ContainsKey(s)) Count[s] += 1;
                            else Count[s] = 1;

                            if (Count[s] > maxV) { maxV = Count[s]; result = s; }
                        }


                    }
                }
                return result;
            }
            int Hit = 0;
            int Num = 0;
            public EvaluateRunner(List<List<string>> q, List<List<string>> d, List<int> label, List<List<Tuple<string, string, string, string>>> ans, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Label = label;
                Answer = ans;
                Q = q;
                D = d;

            }

            public override void Init()
            {
                Hit = 0;
                Num = 0;
            }

            public override void Complete()
            {
                Logger.WriteLog("Hit {0}, Num {1}", Hit, Num);
            }

            public override void Forward()
            {

                for (int b = 0; b < Label.Count; b++)
                {
                    string s = ExtractAnswer(Answer[b]);
                    if(DataPanel.NodeId(s) == Label[b])
                    {
                        Hit += 1;
                    }
                    else
                    {
                        Console.WriteLine(string.Join(" ", D[b]));

                        Console.WriteLine(string.Join(" ", Q[b]));
                        Console.WriteLine("Answer {0}", Label[b]);
                        Console.WriteLine("Pred {0}", s);

                        Console.WriteLine();
                    }
                    Num += 1;
                }

                ObjectiveScore = Hit * 1.0f / Num;
            }
        }


        class EvaluateV2Runner : ObjectiveRunner
        {
            List<int> Label { get; set; }
            List<string> R { get; set; }
            List<List<string>> Q { get; set; }
            List<List<string>> D { get; set; }
            string ExtractAnswer(List<Tuple<string, string, string, string>> data)
            {
                Dictionary<string, int> Count = new Dictionary<string, int>();
                string result = "";
                int maxV = 0;
                for (int srcIdx = 0; srcIdx < data.Count; srcIdx++)
                {
                    if (data[srcIdx].Item1.Equals("X"))
                    {
                        HashSet<string> answer = new HashSet<string>(data[srcIdx].Item3.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries));
                        foreach (string s in answer)
                        {
                            if (Count.ContainsKey(s)) Count[s] += 1;
                            else Count[s] = 1;

                            if (Count[s] > maxV) { maxV = Count[s]; result = s; }
                        }


                    }
                }
                return result;
            }
            int Hit = 0;
            int Num = 0;
            public EvaluateV2Runner(List<List<string>> q, List<List<string>> d, List<string> r, List<int> label,  RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Label = label;
                R = r;
                Q = q;
                D = d;
            }

            public override void Init()
            {
                Hit = 0;
                Num = 0;
            }

            public override void Complete()
            {
                Logger.WriteLog("Hit {0}, Num {1}", Hit, Num);
            }

            public override void Forward()
            {

                for (int b = 0; b < Label.Count; b++)
                {
                    string s = R[b];
                    if (!s.Equals("UNK") && DataPanel.NodeId(s) == Label[b])
                    {
                        Hit += 1;
                    }
                    else
                    {
                        Console.WriteLine(string.Join(" ", D[b]));
                        Console.WriteLine(string.Join(" ", Q[b]));

                        Console.WriteLine("Answer {0}", Label[b]);
                        Console.WriteLine("Pred {0}", s);
                        Console.WriteLine();
                    }
                    Num += 1;
                }

                ObjectiveScore = Hit * 1.0f / Num;

                Console.WriteLine(ObjectiveScore);
            }
        }



        public static ComputationGraph BuildComputationGraph(List<List<string>> context,
                                                             List<List<string>> query,
                                                             List<int> ans, 
                                                             // model.
                                                             int maxT, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();


            #region Query Doc Answer Data.
            SampleRunner<List<string>> contextRunner = new SampleRunner<List<string>>(context, BuilderParameters.MiniBatchSize, Behavior);
            cg.AddDataRunner(contextRunner);
            List<List<string>> cData = contextRunner.Output;

            SampleRunner<List<string>> queryRunner = new SampleRunner<List<string>>(query, BuilderParameters.MiniBatchSize, Behavior);
            cg.AddDataRunner(queryRunner);
            List<List<string>> qData = queryRunner.Output;

            SampleRunner<int> ansRunner = new SampleRunner<int>(ans, BuilderParameters.MiniBatchSize, Behavior);
            cg.AddDataRunner(ansRunner);
            List<int> aData = ansRunner.Output;
            #endregion.

            // first order relation.
            BiLinkRunner contextLinkRunner = new BiLinkRunner(cData, Behavior);
            cg.AddRunner(contextLinkRunner);
            List<List<Tuple<string, string, string>>> cLinkData = contextLinkRunner.InOutLinkData;


            SelfRefRunner cSelfRefRunner = new SelfRefRunner(cLinkData, Behavior);
            cg.AddRunner(cSelfRefRunner);
            List<List<Tuple<string, string, string>>> highOrderCLinkData = cSelfRefRunner.HighRefLink;

            // first order relation.
            BiLinkRunner queryLinkRunner = new BiLinkRunner(qData, Behavior);
            cg.AddRunner(queryLinkRunner);
            List<List<Tuple<string, string, string>>> qLinkData = queryLinkRunner.InOutLinkData;


            SelfRefRunner qSelfRefRunner = new SelfRefRunner(qLinkData, Behavior);
            cg.AddRunner(qSelfRefRunner);
            List<List<Tuple<string, string, string>>> highOrderQLinkData = qSelfRefRunner.HighRefLink;

            EpisodicReasoNetRunner reasonet = new EpisodicReasoNetRunner(qData, highOrderQLinkData, cLinkData, highOrderCLinkData, DataPanel.MaxCrossQDLen, DataPanel.MaxCrossQQLen, BuilderParameters.RECURRENT_STEP, Behavior);
            cg.AddRunner(reasonet);
            List<string> Results = reasonet.Result;

            EvaluateV2Runner evalRunner = new EvaluateV2Runner(qData, cData, Results, aData, Behavior); // qData, cData, aData, reasonet.MData, Behavior);
            if (Behavior.RunMode == DNNRunMode.Train) cg.AddObjective(evalRunner);
            else cg.AddRunner(evalRunner);

            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile, !DeepNet.BuilderParameters.IsLogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");


            ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainContext, DataPanel.TrainQuery, DataPanel.TrainAns,
                 BuilderParameters.RECURRENT_STEP, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            ComputationGraph validCG = null;
            if (BuilderParameters.IsValidFile)
                validCG = BuildComputationGraph(DataPanel.ValidContext, DataPanel.ValidQuery, DataPanel.ValidAns,
                 BuilderParameters.RECURRENT_STEP, new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

            double bestValidScore = double.MinValue;
            int bestIter = -1;
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                double loss = trainCG.Execute();
                Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
               
                double validScore = 0;
                if (validCG != null)
                {
                    validScore = validCG.Execute();
                    Logger.WriteLog("Valid Score {0}, At iter {1}", validScore, iter);
                }
                if (validScore > bestValidScore)
                {
                    bestValidScore = validScore; bestIter = iter;
                }
                Logger.WriteLog("Best Valid Score {0},  At iter {1}", bestValidScore, bestIter);
            }

            DataPanel.Deinit();
            Logger.CloseLog();
        }

        /// <summary>
        /// Data Panel.
        /// </summary>
        public class DataPanel
        {
            public static List<List<string>> TrainContext = new List<List<string>>();
            public static List<List<string>> TrainQuery = new List<List<string>>();
            public static List<int> TrainAns = new List<int>();

            public static List<List<string>> ValidContext = new List<List<string>>();
            public static List<List<string>> ValidQuery = new List<List<string>>();
            public static List<int> ValidAns = new List<int>();

            public static int MaxQueryLen = 0;
            public static int MaxDocLen = 0;
            public static int MaxCrossQDLen = 0;
            public static int MaxCrossDDLen = 0;
            public static int MaxCrossQQLen = 0;

            public static bool IsNode(string term)
            {
                if (term.Equals("->"))
                {
                    return false;
                }
                return true;
            }

            public static int NodeType(string t)
            {
                int id = int.Parse(t.Substring(0, t.Length - 1));
                char type = t.Substring(t.Length - 1)[0];
                return type - 'A';
            }

            public static int NodeId(string t)
            {
                int id = int.Parse(t.Substring(0, t.Length - 1));
                return id;
            }

            static IEnumerable<Tuple<string, string, string>> ExtractQAS(string inputFile)
            {
                using (StreamReader inputReader = new StreamReader(inputFile))
                {
                    int lineIdx = 0;
                    while (!inputReader.EndOfStream)
                    {
                        string doc = inputReader.ReadLine();
                        string query = inputReader.ReadLine();
                        string ans = inputReader.ReadLine();
                        inputReader.ReadLine(); // read empty line.

                        string[] items = ans.Trim().Split(' ');

                        if (items.Length >= 2) { continue; }
                        yield return new Tuple<string, string, string>(doc, query, ans);
                        lineIdx += 1;
                    }
                    Console.WriteLine("Total Number of unique answers {0}", lineIdx);
                }
            }


            static void ExtractCorpusBinary(string inputFile, List<List<string>> context, List<List<string>> query, List<int> ans, int miniBatchSize)
            {
                IEnumerable<Tuple<string, string, string>> QAS = ExtractQAS(inputFile);
                IEnumerable<Tuple<string, string, string>> Input = QAS;

                int crossQDLen = 0;
                int crossDDLen = 0;
                int crossQQLen = 0;
                int lineIdx = 0;
                int overSpanfilterNum = 0;
                int notFoundfilterNum = 0;
                int overLengthfilterNum = 0;
                foreach (Tuple<string, string, string> smp in Input)
                {
                    string d = smp.Item1;
                    string q = smp.Item2;
                    string a = smp.Item3;

                    int ans_id = int.Parse(a.Substring(0, a.Length - 1));

                    List<string> contextFea = new List<string>();
                    foreach (string term in d.Trim().Split(' '))
                    {
                        if (term.Length == 0) continue;
                        contextFea.Add(term);
                    }

                    Dictionary<string, int> reIndex = new Dictionary<string, int>();
                    string[] qTerms = q.Trim().Split(' ');
                    List<string> queryFea = new List<string>();

                    foreach (string t in qTerms)
                    {
                        if (t.Length == 0) continue;

                        if (t.Equals("->") || t.Equals("X")) { queryFea.Add(t); continue; }
                        else
                        {
                            int id = int.Parse(t.Substring(0, t.Length - 1));
                            char type = t.Substring(t.Length - 1)[0];
                            int newIdx = id;
                            if (!reIndex.ContainsKey(t))
                            {
                                reIndex[t] = reIndex.Count;
                            }
                            newIdx = reIndex[t];
                            queryFea.Add(newIdx.ToString() + type.ToString());
                        }
                    }

                    if (contextFea.Count > MaxDocLen) { MaxDocLen = contextFea.Count; }
                    if (queryFea.Count > MaxQueryLen) { MaxQueryLen = queryFea.Count; }

                    crossQDLen += contextFea.Count * queryFea.Count;
                    crossDDLen += contextFea.Count * contextFea.Count;
                    crossQQLen += queryFea.Count * queryFea.Count;

                    if (crossQDLen > MaxCrossQDLen) { MaxCrossQDLen = crossQDLen; }
                    if (crossDDLen > MaxCrossDDLen) { MaxCrossDDLen = crossDDLen; }
                    if (crossQQLen > MaxCrossQQLen) { MaxCrossQQLen = crossQQLen; }

                    context.Add(contextFea);
                    query.Add(queryFea);
                    ans.Add(ans_id);


                    if (context.Count % miniBatchSize == 0)
                    {
                        crossQDLen = 0;
                        crossDDLen = 0;
                        crossQQLen = 0;
                    }

                    if (++lineIdx % 1000 == 0) { Console.WriteLine("Extract Binary from Corpus {0}", lineIdx); }
                }

                Console.WriteLine("Max Q Len {0}, Max D Len {1}, Max QD Cross Len {2}, Max DD Cross Len {3}, Max QQ Cross Len", MaxQueryLen, MaxDocLen, MaxCrossQDLen, MaxCrossDDLen, MaxCrossQQLen);
                Console.WriteLine("OverSpan Filter Number {0}, Not Found Filter Number {1}, Over Length Filter Number {2}", overSpanfilterNum, notFoundfilterNum, overLengthfilterNum);
            }

            public static void Init()
            {
                #region Preprocess Data.
                if (BuilderParameters.IsTrainFile)
                {
                    ExtractCorpusBinary(BuilderParameters.Train, TrainContext, TrainQuery, TrainAns, BuilderParameters.MiniBatchSize);
                }
                if (BuilderParameters.IsValidFile)
                {
                    ExtractCorpusBinary(BuilderParameters.Valid, ValidContext, ValidQuery, ValidAns, BuilderParameters.MiniBatchSize);
                }
                #endregion.
            }

            public static void Deinit()
            {
                string paraFileBin = BuilderParameters.ValidContextBin;
                string queryFileBin = BuilderParameters.ValidQueryBin;
                string ansFileBin = BuilderParameters.ValidAnsBin;

                File.Delete(paraFileBin);
                File.Delete(queryFileBin);
                File.Delete(ansFileBin);
            }

        }
    }
}
