﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigLearn;

namespace BigLearn.DeepNet
{
    /// <summary>
    /// 1. Reinforcement Learning.
    /// 2. Generate Path without source and target.
    /// 3. Memory Attention on Answer.
    /// </summary>
    public class AppReasoNetPathV3GenerationBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }

            public static string TrainData { get { return Argument["TRAIN"].Value; } }
            public static string TrainQuery { get { return string.Format("{0}.query", TrainData); } }
            public static string TrainAnswer { get { return string.Format("{0}.answer", TrainData); } }

            public static string TestData { get { return Argument["TEST"].Value; } }
            public static string TestQuery { get { return string.Format("{0}.query", TestData); } }
            public static string TestAnswer { get { return string.Format("{0}.answer", TestData); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }

            public static int MaxDecodeLength { get { return int.Parse(Argument["MAX-DECODE-LENGTH"].Value); } }


            public static int S_EmbedDim { get { return int.Parse(Argument["SYMBOL-EMBED-DIM"].Value); } }
            public static A_Func S_EmbedAf { get { return (A_Func)int.Parse(Argument["SYMBOL-EMBED-AF"].Value); } }

            public static int R_EmbedDim { get { return int.Parse(Argument["RECURRENT-DIM"].Value); } }
            public static RndRecurrentInit R_Init { get { return (RndRecurrentInit)int.Parse(Argument["RECURRENT-INIT"].Value); } }

            public static int MemorySize { get { return int.Parse(Argument["MEMORY-SIZE"].Value); } }

            public static int M_ATT_DIM { get { return int.Parse(Argument["MEMORY-ATT-DIM"].Value); } }
            public static CrossSimType M_ATT_TYPE { get { return (CrossSimType)int.Parse(Argument["MEMORY-ATT-TYPE"].Value); } }

            public static int Q_ATT_DIM { get { return int.Parse(Argument["QUERY-ATT-DIM"].Value); } }
            public static CrossSimType Q_ATT_TYPE { get { return (CrossSimType)int.Parse(Argument["QUERY-ATT-TYPE"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static float SoftmaxGamma { get { return float.Parse(Argument["SOFTMAX-GAMMA"].Value); } }

            public static int RECURRENT_STEP { get { return int.Parse(Argument["RECURRENT-STEP"].Value); } }
            public static PredType PRED_RL { get { return (PredType)int.Parse(Argument["PRED-RL"].Value); } }
            public static bool IS_NORM_REWARD { get { return int.Parse(Argument["IS-NORM-REWARD"].Value) > 0; } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }

            public static bool IS_QUERY_MEM { get { return int.Parse(Argument["IS-QUERY-MEM"].Value) > 0 ? false : false; } }

            public static int BEAM { get { return int.Parse(Argument["BEAM"].Value); } }
            public static string GRAPH { get { return Argument["GRAPH"].Value; } }
            public static bool IS_SHARE_EMD { get { return int.Parse(Argument["IS-SHARE-EMD"].Value) > 0; } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));

                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));

                Argument.Add("MAX-DECODE-LENGTH", new ParameterArgument("10", "Max Decode Length."));

                Argument.Add("SYMBOL-EMBED-DIM", new ParameterArgument("64", "Symbol Embedding Dim"));
                Argument.Add("SYMBOL-EMBED-AF", new ParameterArgument(((int)A_Func.Linear).ToString(), "Symbol Embedding AF" + ParameterUtil.EnumValues(typeof(A_Func))));

                Argument.Add("RECURRENT-DIM", new ParameterArgument("64", "Recurrent Embedding Dim"));
                Argument.Add("RECURRENT-INIT", new ParameterArgument(((int)RndRecurrentInit.RndNorm).ToString(), "Recurrent Weight Init."));

                Argument.Add("MEMORY-SIZE", new ParameterArgument("64", "Memory Size."));

                Argument.Add("MEMORY-ATT-DIM", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("MEMORY-ATT-TYPE", new ParameterArgument(((int)CrossSimType.SubCosine).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("QUERY-ATT-DIM", new ParameterArgument("300", "Attention Hidden Dimension"));
                Argument.Add("QUERY-ATT-TYPE", new ParameterArgument(((int)CrossSimType.SubCosine).ToString(), "Attention Type : " + ParameterUtil.EnumValues(typeof(CrossSimType))));

                Argument.Add("GAMMA", new ParameterArgument("10", "Attention Smooth Parameter."));
                Argument.Add("SOFTMAX-GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));

                Argument.Add("RECURRENT-STEP", new ParameterArgument("6", "Recurrent Step."));
                Argument.Add("PRED-RL", new ParameterArgument((((int)PredType.RL_MAXITER)).ToString(), "RL Pred Type : " + ParameterUtil.EnumValues(typeof(PredType))));
                Argument.Add("IS-NORM-REWARD", new ParameterArgument("0", "0 : No Norm Reward; 1 : Norm Reward."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));
                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score File."));

                Argument.Add("IS-QUERY-MEM", new ParameterArgument("0", "0 : No Query Memory; 1 : Query Memory."));
                Argument.Add("BEAM", new ParameterArgument("5", "Beam Size."));
                Argument.Add("GRAPH", new ParameterArgument(string.Empty, "Graph Structure!"));
                Argument.Add("IS-SHARE-EMD", new ParameterArgument("0", "Share Symbol Embedding!"));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_REASONET_PATHV3GENERATION; } }

        public override void InitStartup(string fileName)
        {
            BuilderParameters.Parse(fileName);
        }

        public enum PredType { RL_MAXITER = 0, RL_AVGSIM = 1 }

        class GlobalSoftLinearSimRunner : StructRunner
        {
            HiddenBatchData Status { get; set; }

            HiddenBatchData Mem { get; set; }
            BiMatchBatchData MemMatch { get; set; }

            HiddenBatchData HiddenStatus = null;
            HiddenBatchData HiddenMatch = null;
            CudaPieceFloat HiddenMatchTmp = null;

            MLPAttentionStructure AttStruct;

            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            CrossSimType AttType = CrossSimType.Addition;

            SimilarityRunner SimRunner = null;

            public GlobalSoftLinearSimRunner(HiddenBatchData status, HiddenBatchData mem, BiMatchBatchData memMatch, CrossSimType attType,
                MLPAttentionStructure attStruct, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Status = status;
                Mem = mem;
                MemMatch = memMatch;

                AttType = attType;
                AttStruct = attStruct;

                HiddenStatus = new HiddenBatchData(status.MAX_BATCHSIZE, AttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);

                switch (AttType)
                {
                    case CrossSimType.Product:
                    case CrossSimType.Addition:
                        HiddenMatchTmp = new CudaPieceFloat(MemMatch.Stat.MAX_MATCH_BATCHSIZE * AttStruct.HiddenDim, true, Behavior.Device == DeviceType.GPU);
                        HiddenMatch = new HiddenBatchData(MemMatch.Stat.MAX_MATCH_BATCHSIZE, AttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);
                        Output = new HiddenBatchData(MemMatch.Stat.MAX_SRC_BATCHSIZE, MemMatch.Dim, Behavior.RunMode, Behavior.Device);
                        break;
                    case CrossSimType.SubCosine:
                        SimRunner = new SimilarityRunner(HiddenStatus, Mem, MemMatch, SimilarityType.CosineSimilarity, Behavior);
                        Output = SimRunner.Output;
                        break;
                }
            }

            public override void Forward()
            {
                HiddenStatus.BatchSize = Status.BatchSize;
                ComputeLib.Sgemm(Status.Output.Data, 0, AttStruct.Wi, 0, HiddenStatus.Output.Data, 0, Status.BatchSize, AttStruct.InputDim, AttStruct.HiddenDim, 0, 1, false, false);

                switch (AttType)
                {
                    case CrossSimType.Addition:
                    case CrossSimType.Product:
                        if (AttType == CrossSimType.Addition)
                        {
                            // hiddenMatch = hiddenStatus + Mem
                            ComputeLib.Matrix_AdditionMask(HiddenStatus.Output.Data, 0, MemMatch.SrcIdx, 0,
                                                           Mem.Output.Data, 0, MemMatch.TgtIdx, 0,
                                                           HiddenMatch.Output.Data, 0, CudaPieceInt.Empty, 0,
                                                           AttStruct.HiddenDim, MemMatch.MatchSize, 1, 1, 0);
                        }
                        else if (AttType == CrossSimType.Product)
                        {
                            ComputeLib.ElementwiseProductMask(HiddenStatus.Output.Data, 0,
                                                              Mem.Output.Data, 0,
                                                              HiddenMatch.Output.Data, 0,
                                                              MemMatch.SrcIdx, 0, MemMatch.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                              MemMatch.MatchSize, AttStruct.HiddenDim, 0, 1);
                        }
                        ComputeLib.Tanh(HiddenMatch.Output.Data, 0, HiddenMatch.Output.Data, 0, AttStruct.HiddenDim * MemMatch.MatchSize);

                        Output.BatchSize = MemMatch.SrcSize;

                        // attention = address * Wa.
                        ComputeLib.Sgemv(HiddenMatch.Output.Data, AttStruct.Wa, MemMatch.MatchSize, HiddenMatch.Dim, Output.Output.Data, false, 0, 1);

                        ComputeLib.Tanh(Output.Output.Data, 0, Output.Output.Data, 0, Output.BatchSize * Output.Dim);
                        break;
                    case CrossSimType.SubCosine:
                        SimRunner.Forward();
                        break;
                }
            }

            public override void CleanDeriv()
            {
                if (AttType == CrossSimType.SubCosine)
                {
                    ComputeLib.Zero(HiddenStatus.Deriv.Data, HiddenStatus.BatchSize * HiddenStatus.Dim);
                }
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                switch (AttType)
                {
                    case CrossSimType.Addition:
                    case CrossSimType.Product:
                        ComputeLib.DerivTanh(Output.Output.Data, 0, Output.Deriv.Data, 0, Output.Deriv.Data, 0, Output.BatchSize * Output.Dim, 0, 1);

                        // HiddenOutput = attentionDeriv * Wa'.
                        ComputeLib.Sgemm(Output.Deriv.Data, 0, AttStruct.Wa, 0, HiddenMatch.Deriv.Data, 0, MemMatch.MatchSize, 1, HiddenMatch.Dim, 0, 1, false, false);

                        // HiddenOutput = HiddenOutput * tanh(address)'
                        ComputeLib.DerivTanh(HiddenMatch.Output.Data, 0, HiddenMatch.Deriv.Data, 0, HiddenMatch.Deriv.Data, 0, MemMatch.MatchSize * HiddenMatch.Dim, 0, 1);

                        if (AttType == CrossSimType.Addition)
                        {
                            ComputeLib.ColumnWiseSum(HiddenMatch.Deriv.Data, Mem.Deriv.Data, Status.BatchSize, Mem.BatchSize * Mem.Dim, 1);

                            //HiddenStatus.Deriv.Data.SyncToCPU();
                            ComputeLib.ColumnWiseSumMask(HiddenMatch.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                                  CudaPieceFloat.Empty, MemMatch.Src2MatchIdx, MemMatch.SrcSize,
                                                                  HiddenStatus.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                                  MemMatch.MatchSize, HiddenStatus.Dim, 0, 1);
                        }
                        else if (AttType == CrossSimType.Product)
                        {
                            ComputeLib.ElementwiseProductMask(HiddenMatch.Deriv.Data, 0,
                                                              HiddenStatus.Output.Data, 0,
                                                              HiddenMatchTmp, 0,
                                                              CudaPieceInt.Empty, 0, MemMatch.SrcIdx, 0, CudaPieceInt.Empty, 0,
                                                              MemMatch.MatchSize, Mem.Dim, 0, 1);

                            ComputeLib.ColumnWiseSum(HiddenMatchTmp, Mem.Deriv.Data, Status.BatchSize, Mem.BatchSize * Mem.Dim, 1);

                            ComputeLib.ElementwiseProductMask(HiddenMatch.Deriv.Data, 0,
                                                              Mem.Output.Data, 0,
                                                              HiddenMatchTmp, 0,
                                                              CudaPieceInt.Empty, 0, MemMatch.TgtIdx, 0, CudaPieceInt.Empty, 0,
                                                              MemMatch.MatchSize, Mem.Dim, 0, 1);

                            ComputeLib.ColumnWiseSumMask(HiddenMatchTmp, 0, CudaPieceInt.Empty, 0,
                                                                  CudaPieceFloat.Empty, MemMatch.Src2MatchIdx, MemMatch.SrcSize,
                                                                  HiddenStatus.Deriv.Data, 0, CudaPieceInt.Empty, 0,
                                                                  MemMatch.MatchSize, HiddenStatus.Dim, 0, 1);
                        }
                        break;
                    case CrossSimType.SubCosine:
                        SimRunner.Backward(cleanDeriv);
                        break;
                }

                // inputDeriv += hiddenDeriv * wi'
                ComputeLib.Sgemm(HiddenStatus.Deriv.Data, 0,
                                          AttStruct.Wi, 0,
                                          Status.Deriv.Data, 0,
                                          Status.BatchSize, AttStruct.HiddenDim, AttStruct.InputDim, 1, 1, false, true);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    AttStruct.WiMatrixOptimizer.BeforeGradient();
                    AttStruct.WaMatrixOptimizer.BeforeGradient();
                }

                if (AttType == CrossSimType.Addition || AttType == CrossSimType.Product)
                {
                    //  Wa  +=  attentionDeriv * address.
                    ComputeLib.Sgemm(Output.Deriv.Data, 0, HiddenMatch.Output.Data, 0, AttStruct.WaMatrixOptimizer.Gradient, 0,
                                              1, MemMatch.MatchSize, HiddenMatch.Dim, 1, AttStruct.WaMatrixOptimizer.GradientStep, false, false);
                }

                ComputeLib.Sgemm(Status.Output.Data, 0,
                                          HiddenStatus.Deriv.Data, 0,
                                          AttStruct.WiMatrixOptimizer.Gradient, 0,
                                          Status.BatchSize, AttStruct.InputDim, AttStruct.HiddenDim,
                                          1, AttStruct.WiMatrixOptimizer.GradientStep, true, false);
                if (!IsDelegateOptimizer)
                {
                    AttStruct.WiMatrixOptimizer.AfterGradient();
                    AttStruct.WaMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Memory Retrieval Runner.
        /// </summary>
        class GlobalMemoryRetrievalRunner : StructRunner
        {
            HiddenBatchData Address { get; set; }

            CudaPieceFloat ScaleAddress { get; set; }

            HiddenBatchData SoftmaxAddress { get; set; }

            MatrixStructure Memory { get; set; }
            float Gamma { get; set; }
            /// <summary>
            /// softmax on address given query.
            /// </summary>
            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            public GlobalMemoryRetrievalRunner(HiddenBatchData address, MatrixStructure memory, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Address = address;
                Memory = memory;
                Gamma = gamma;

                ScaleAddress = new CudaPieceFloat(Address.MAX_BATCHSIZE * Address.Dim, true, Behavior.Device == DeviceType.GPU);
                SoftmaxAddress = new HiddenBatchData(Address.MAX_BATCHSIZE, Address.Dim, Behavior.RunMode, Behavior.Device);
                Output = new HiddenBatchData(Address.MAX_BATCHSIZE, Memory.Dim, Behavior.RunMode, Behavior.Device);
            }

            public override void Forward()
            {
                Output.BatchSize = Address.BatchSize;
                SoftmaxAddress.BatchSize = Address.BatchSize;

                // softmax 
                // softmax of attention
                if (Gamma == 1) ScaleAddress = Address.Output.Data;
                else ComputeLib.Add_Vector(ScaleAddress, Address.Output.Data, Address.Dim * Address.BatchSize, 0, Gamma);
                //ComputeLib.SparseSoftmax(MatchData.Src2MatchIdx, Address.Output.Data, SoftmaxAddress.Output.Data, Gamma, MatchData.SrcSize);
                ComputeLib.SoftMax(ScaleAddress, SoftmaxAddress.Output.Data, Address.Dim, Address.BatchSize, 1);

                ComputeLib.Sgemm(SoftmaxAddress.Output.Data, 0, Memory.Memory, 0, Output.Output.Data, 0, SoftmaxAddress.BatchSize, SoftmaxAddress.Dim, Memory.Dim, 0, 1, false, false);

                //ComputeLib.ColumnWiseSumMask(Memory.Memory, 0, MatchData.TgtIdx, 0,
                //    SoftmaxAddress.Output.Data, MatchData.Src2MatchIdx, MatchData.SrcSize,
                //    Output.Output.Data, 0, CudaPieceInt.Empty, 0,
                //    MatchData.MatchSize, Memory.Dim, 0, 1);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.BatchSize * Output.Dim);
            }

            public override void Backward(bool cleanDeriv)
            {
                // outputDeriv -> AddressDeriv.
                ComputeLib.Sgemm(Output.Deriv.Data, 0, Memory.Memory, 0, SoftmaxAddress.Deriv.Data, 0, Output.BatchSize, Output.Dim, SoftmaxAddress.Dim, 0, 1, false, true);

                //ComputeLib.Inner_Product_Matching(Output.Deriv.Data, 0, Memory.SentOutput, 0, SoftmaxAddress.Deriv.Data, 0,
                //    MatchData.SrcIdx, MatchData.TgtIdx, Output.BatchSize, Memory.SentSize, MatchData.MatchSize,
                //    Memory.Dim, Util.GPUEpsilon);

                ComputeLib.DerivSoftmax(SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Dim, SoftmaxAddress.BatchSize, 0);
                //ComputeLib.DerivSparseMultiClassSoftmax(MatchData.Src2MatchIdx,
                //    SoftmaxAddress.Output.Data, SoftmaxAddress.Deriv.Data, SoftmaxAddress.Deriv.Data, Gamma, MatchData.SrcSize);

                ComputeLib.Add_Vector(Address.Deriv.Data, SoftmaxAddress.Deriv.Data, Address.Dim * Address.BatchSize, 1, Gamma);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer) Memory.MemoryOptimizer.BeforeGradient();

                ComputeLib.Sgemm(SoftmaxAddress.Output.Data, 0, Output.Deriv.Data, 0, Memory.MemoryOptimizer.Gradient, 0, SoftmaxAddress.BatchSize, SoftmaxAddress.Dim,
                    Output.Dim, 1, Memory.MemoryOptimizer.GradientStep, true, false);

                if (!IsDelegateOptimizer) Memory.MemoryOptimizer.AfterGradient();
            }
        }

        /// <summary>
        /// GRU Query Runner.
        /// </summary>
        public class GRUQueryRunner : StructRunner<GRUCell, HiddenBatchData>
        {
            public HiddenBatchData Query { get; set; }

            public new HiddenBatchData Output { get { return (HiddenBatchData)base.Output; } set { base.Output = value; } }

            HiddenBatchData GateR; //reset gate
            HiddenBatchData GateZ; //update gate
            HiddenBatchData HHat; //new memory
            HiddenBatchData RestH; //new memory

            CudaPieceFloat Ones;
            CudaPieceFloat Tmp;
            public GRUQueryRunner(GRUCell model, HiddenBatchData query, HiddenBatchData input, RunnerBehavior behavior) : base(model, input, behavior)
            {
                Query = query;

                Output = new HiddenBatchData(query.MAX_BATCHSIZE, query.Dim, Behavior.RunMode, Behavior.Device);

                GateR = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                GateZ = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                HHat = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);
                RestH = new HiddenBatchData(Query.MAX_BATCHSIZE, Query.Dim, Behavior.RunMode, Behavior.Device);

                Ones = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
                Ones.Init(1);

                Tmp = new CudaPieceFloat(Query.MAX_BATCHSIZE * Query.Dim, true, Behavior.Device == DeviceType.GPU);
            }

            public override void Forward()
            {
                //SubQuery.BatchSize = 1;
                /*Wr X -> GateR*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wr, 0, GateR.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateR*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Ur, 0, GateR.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateR.Output.Data, Model.Br, Query.BatchSize, Model.HiddenStateDim);

                /*Wz X -> GateZ*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wz, 0, GateZ.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                /*h_t-1 -> GateZ*/
                ComputeLib.Sgemm(Query.Output.Data, 0, Model.Uz, 0, GateZ.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Sigmoid(GateZ.Output.Data, Model.Bz, Query.BatchSize, Model.HiddenStateDim);

                /*Wh X -> HHat*/
                ComputeLib.Sgemm(Input.Output.Data, 0, Model.Wh, 0, HHat.Output.Data, 0, Input.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 0, 1, false, false);
                ComputeLib.ElementwiseProduct(GateR.Output.Data, Query.Output.Data, RestH.Output.Data, Query.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.Sgemm(RestH.Output.Data, 0, Model.Uh, 0, HHat.Output.Data, 0, Query.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, false);
                ComputeLib.Matrix_Add_Tanh(HHat.Output.Data, Model.Bh, Query.BatchSize, Model.HiddenStateDim);

                Output.BatchSize = Query.BatchSize;

                //H(t) = (1 - GateZ(t)) h_t-1 + GateZ(t) * HHat
                // tmp = (1 - GateZ(t))
                ComputeLib.Matrix_AdditionMask(Ones, 0, CudaPieceInt.Empty, 0,
                                               GateZ.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);

                // NewQuery = GateZ(t) * HHat
                ComputeLib.ElementwiseProduct(HHat.Output.Data, GateZ.Output.Data, Output.Output.Data, Output.BatchSize, Output.Dim, 0);

                // NewQuery += tmp * h_t-1
                ComputeLib.ElementwiseProduct(Query.Output.Data, Tmp, Output.Output.Data, Output.BatchSize, Output.Dim, 1);
            }

            public override void Backward(bool cleanDeriv)
            {
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, Query.Deriv.Data, Query.BatchSize, Model.HiddenStateDim, 1);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, GateZ.Output.Data, HHat.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                // tmp = (HHat.Output.Data.MemPtr[ii] - Query.Output.Data.MemPtr[ii])
                ComputeLib.Matrix_AdditionMask(HHat.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Query.Output.Data, 0, CudaPieceInt.Empty, 0,
                                               Tmp, 0, CudaPieceInt.Empty, 0,
                                               Model.HiddenStateDim, Output.BatchSize, 1, -1, 0);
                ComputeLib.ElementwiseProduct(Output.Deriv.Data, Tmp, GateZ.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);

                ComputeLib.Deriv_Tanh(HHat.Deriv.Data, HHat.Output.Data, Output.BatchSize, Output.Dim);
                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Uh, 0, RestH.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 0, 1, false, true);

                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, Query.Output.Data, GateR.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 0);
                ComputeLib.ElementwiseProduct(RestH.Deriv.Data, GateR.Output.Data, Query.Deriv.Data, Output.BatchSize, Model.HiddenStateDim, 1);

                ComputeLib.DerivLogistic(GateR.Output.Data, 0, GateR.Deriv.Data, 0, GateR.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);
                ComputeLib.DerivLogistic(GateZ.Output.Data, 0, GateZ.Deriv.Data, 0, GateZ.Deriv.Data, 0, Output.BatchSize * Model.HiddenStateDim, 0, 1);

                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Ur, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Uz, 0, Query.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, 1, false, true);

                ComputeLib.Sgemm(HHat.Deriv.Data, 0, Model.Wh, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateR.Deriv.Data, 0, Model.Wr, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
                ComputeLib.Sgemm(GateZ.Deriv.Data, 0, Model.Wz, 0, Input.Deriv.Data, 0, Output.BatchSize, Model.HiddenStateDim, Model.InputFeatureDim, 1, 1, false, true);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.BeforeGradient();
                    Model.WrMatrixOptimizer.BeforeGradient();
                    Model.WzMatrixOptimizer.BeforeGradient();

                    Model.UhMatrixOptimizer.BeforeGradient();
                    Model.UrMatrixOptimizer.BeforeGradient();
                    Model.UzMatrixOptimizer.BeforeGradient();

                    Model.BhMatrixOptimizer.BeforeGradient();
                    Model.BrMatrixOptimizer.BeforeGradient();
                    Model.BzMatrixOptimizer.BeforeGradient();

                }
                ComputeLib.Sgemm(Input.Output.Data, 0, HHat.Deriv.Data, 0, Model.WhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateR.Deriv.Data, 0, Model.WrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Input.Output.Data, 0, GateZ.Deriv.Data, 0, Model.WzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.InputFeatureDim, Model.HiddenStateDim, 1, Model.WzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.Sgemm(RestH.Output.Data, 0, HHat.Deriv.Data, 0, Model.UhMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UhMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateR.Deriv.Data, 0, Model.UrMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UrMatrixOptimizer.GradientStep, true, false);
                ComputeLib.Sgemm(Query.Output.Data, 0, GateZ.Deriv.Data, 0, Model.UzMatrixOptimizer.Gradient, 0, Output.BatchSize, Model.HiddenStateDim, Model.HiddenStateDim, 1, Model.UzMatrixOptimizer.GradientStep, true, false);

                ComputeLib.ColumnWiseSum(HHat.Deriv.Data, Model.BhMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BhMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateR.Deriv.Data, Model.BrMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BrMatrixOptimizer.GradientStep);
                ComputeLib.ColumnWiseSum(GateZ.Deriv.Data, Model.BzMatrixOptimizer.Gradient, Output.BatchSize, Model.HiddenStateDim, Model.BzMatrixOptimizer.GradientStep);

                if (!IsDelegateOptimizer)
                {
                    Model.WhMatrixOptimizer.AfterGradient();
                    Model.WrMatrixOptimizer.AfterGradient();
                    Model.WzMatrixOptimizer.AfterGradient();

                    Model.UhMatrixOptimizer.AfterGradient();
                    Model.UrMatrixOptimizer.AfterGradient();
                    Model.UzMatrixOptimizer.AfterGradient();

                    Model.BhMatrixOptimizer.AfterGradient();
                    Model.BrMatrixOptimizer.AfterGradient();
                    Model.BzMatrixOptimizer.AfterGradient();
                }
            }
        }

        /// <summary>
        /// Fix Iteration, SoftAttention, Supervised Training.
        /// </summary>
        class ReasonNetRunner : StructRunner
        {
            /// <summary>
            /// Input Query;
            /// </summary>
            HiddenBatchData InitStatus { get; set; }

            MatrixStructure GMemory { get; set; }
            BiMatchBatchData GMemMatch { get; set; }
            MLPAttentionStructure GAttStruct { get; set; }
            HiddenBatchData GAttHidden { get; set; }
            List<GlobalSoftLinearSimRunner> GAttRunner = new List<GlobalSoftLinearSimRunner>();
            List<GlobalMemoryRetrievalRunner> GMemRetrievalRunner = new List<GlobalMemoryRetrievalRunner>();

            SeqDenseBatchData QMemory { get; set; }
            BiMatchBatchData QMemMatch { get; set; }
            MLPAttentionStructure QAttStruct { get; set; }
            SeqDenseBatchData QAttHidden { get; set; }
            List<SoftLinearSimRunner> QAttRunner = new List<SoftLinearSimRunner>();
            List<MemoryRetrievalRunner> QMemRetrievalRunner = new List<MemoryRetrievalRunner>();

            List<EnsembleMatrixRunner> RetrievalEnsembleRunner = new List<EnsembleMatrixRunner>();

            GRUCell GruCell = null;
            LayerStructure AnsStruct = null;
            LayerStructure TermStruct = null;

            List<GRUQueryRunner> StatusRunner = new List<GRUQueryRunner>();
            List<FullyConnectHiddenRunner<HiddenBatchData>> AnsRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();
            List<FullyConnectHiddenRunner<HiddenBatchData>> TerminalRunner = new List<FullyConnectHiddenRunner<HiddenBatchData>>();

            public HiddenBatchData[] StatusData = null;
            public HiddenBatchData[] AnsData = null;
            public HiddenBatchData[] AnswerProb = null;
            public HiddenBatchData FinalAns = null;

            //PoolingType ReasonPool = BuilderParameters.RECURRENT_POOL;
            /// <summary>
            /// maximum iteration number.
            /// </summary>
            int MaxIterationNum { get; set; }
            public ReasonNetRunner(
                HiddenBatchData initStatus, int maxIterateNum,
                MatrixStructure gMem, BiMatchBatchData gMemMatch, MLPAttentionStructure gAttStruct,
                SeqDenseBatchData qMem, BiMatchBatchData qMatchData, MLPAttentionStructure qAttStruct,
                GRUCell gruCell, LayerStructure ansStruct, LayerStructure termStruct, float gamma, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                InitStatus = initStatus;

                GMemory = gMem;
                GMemMatch = gMemMatch;
                GAttStruct = gAttStruct;
                GAttHidden = new HiddenBatchData(GMemory.Size, GAttStruct.HiddenDim, Behavior.RunMode, Behavior.Device);

                if (BuilderParameters.IS_QUERY_MEM)
                {
                    QMemory = qMem;
                    QMemMatch = qMatchData;
                    QAttStruct = qAttStruct;
                    QAttHidden = new SeqDenseBatchData(new SequenceDataStat()
                    {
                        MAX_BATCHSIZE = QMemory.Stat.MAX_BATCHSIZE,
                        MAX_SEQUENCESIZE = QMemory.Stat.MAX_SEQUENCESIZE,
                        FEATURE_DIM = QAttStruct.HiddenDim
                    }, QMemory.SampleIdx, QMemory.SentMargin, Behavior.Device);
                }

                MaxIterationNum = maxIterateNum;

                GruCell = gruCell;
                AnsStruct = ansStruct;
                TermStruct = termStruct;
                HiddenBatchData lastStatus = InitStatus;

                AnsRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, lastStatus, Behavior));
                TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(TermStruct, lastStatus, Behavior));

                HiddenBatchData GAtt = null;
                HiddenBatchData QAtt = null;
                for (int i = 0; i < maxIterateNum - 1; i++)
                {
                    GAttRunner.Add(new GlobalSoftLinearSimRunner(lastStatus, GAttHidden, GMemMatch, BuilderParameters.M_ATT_TYPE, GAttStruct, Behavior));
                    GAtt = GAttRunner[i].Output;
                    GlobalMemoryRetrievalRunner gMemRunner = new GlobalMemoryRetrievalRunner(GAtt, GMemory, gamma, Behavior);
                    GMemRetrievalRunner.Add(gMemRunner);

                    HiddenBatchData X = gMemRunner.Output;

                    if (BuilderParameters.IS_QUERY_MEM)
                    {
                        QAttRunner.Add(new SoftLinearSimRunner(lastStatus, QAttHidden, QMemMatch, BuilderParameters.Q_ATT_TYPE, QAttStruct, Behavior));
                        QAtt = QAttRunner[i].Output;
                        MemoryRetrievalRunner qMemRunner = new MemoryRetrievalRunner(QAtt, QMemory, QMemMatch, gamma, Behavior);
                        QMemRetrievalRunner.Add(qMemRunner);

                        EnsembleMatrixRunner eRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { gMemRunner.Output, qMemRunner.Output }, Behavior);
                        RetrievalEnsembleRunner.Add(eRunner);

                        X = eRunner.Output;
                    }

                    GRUQueryRunner yRunner = new GRUQueryRunner(gruCell, lastStatus, X, Behavior);
                    StatusRunner.Add(yRunner);
                    lastStatus = yRunner.Output;

                    AnsRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(AnsStruct, lastStatus, Behavior));
                    TerminalRunner.Add(new FullyConnectHiddenRunner<HiddenBatchData>(termStruct, lastStatus, Behavior));
                }
                {
                    AnswerProb = new HiddenBatchData[MaxIterationNum];
                    AnsData = new HiddenBatchData[MaxIterationNum];
                    StatusData = new HiddenBatchData[MaxIterationNum];

                    StatusData[0] = InitStatus;
                    for (int i = 1; i < MaxIterationNum; i++) StatusData[i] = StatusRunner[i - 1].Output;
                    for (int i = 0; i < MaxIterationNum; i++) AnsData[i] = AnsRunner[i].Output;
                    for (int i = 0; i < MaxIterationNum; i++) AnswerProb[i] = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, 1, DNNRunMode.Train, Behavior.Device);

                    FinalAns = new HiddenBatchData(InitStatus.MAX_BATCHSIZE, AnsStruct.Neural_Out, DNNRunMode.Train, Behavior.Device);
                }
            }

            public override void Forward()
            {
                // Global Memory Hidden.
                {
                    GAttHidden.BatchSize = GMemory.Size;
                    ComputeLib.Sgemm(GMemory.Memory, 0, GAttStruct.Wm, 0, GAttHidden.Output.Data, 0,
                                     GMemory.Size, GMemory.Dim, GAttStruct.HiddenDim, 0, 1, false, false);
                    if (GAttStruct.IsBias) ComputeLib.Matrix_Add_Linear(GAttHidden.Output.Data, GAttStruct.B, GMemory.Size, GAttStruct.HiddenDim);
                }

                // Question Memory Hidden.
                if (BuilderParameters.IS_QUERY_MEM)
                {
                    QAttHidden.BatchSize = QMemory.BatchSize;
                    QAttHidden.SentSize = QMemory.SentSize;
                    ComputeLib.Sgemm(QMemory.SentOutput, 0, QAttStruct.Wm, 0, QAttHidden.SentOutput, 0,
                                     QMemory.SentSize, QMemory.Dim, QAttStruct.HiddenDim, 0, 1, false, false);
                    if (QAttStruct.IsBias) ComputeLib.Matrix_Add_Linear(QAttHidden.SentOutput, QAttStruct.B, QMemory.SentSize, QAttStruct.HiddenDim);
                }

                AnsRunner[0].Forward();
                TerminalRunner[0].Forward();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    GAttRunner[i].Forward();
                    // obtain the retrieved mem.
                    GMemRetrievalRunner[i].Forward();

                    if (BuilderParameters.IS_QUERY_MEM)
                    {
                        QAttRunner[i].Forward();
                        QMemRetrievalRunner[i].Forward();
                        RetrievalEnsembleRunner[i].Forward();
                    }

                    // update Status.
                    StatusRunner[i].Forward();

                    // take answer.
                    AnsRunner[i + 1].Forward();
                    // take terminal gate or not.
                    TerminalRunner[i + 1].Forward();
                }

                #region RL Pooling.
                {
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        ComputeLib.Logistic(TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.Output.Data, 0, TerminalRunner[i].Output.BatchSize, 1);
                        ComputeLib.ClipVector(TerminalRunner[i].Output.Output.Data, TerminalRunner[i].Output.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);

                        TerminalRunner[i].Output.Output.Data.SyncToCPU(TerminalRunner[i].Output.BatchSize);
                        AnsData[i].Output.Data.SyncToCPU(AnsData[i].BatchSize * AnsData[i].Dim);
                    }

                    FinalAns.BatchSize = AnsData.Last().BatchSize;
                    Array.Clear(FinalAns.Output.Data.MemPtr, 0, FinalAns.BatchSize * FinalAns.Dim);

                    for (int b = 0; b < FinalAns.BatchSize; b++)
                    {
                        int max_iter = 0;
                        double max_p = 0;

                        float acc_log_t = 0;
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float t_i = TerminalRunner[i].Output.Output.Data.MemPtr[b];
                            if (t_i < Util.GPUEpsilon || t_i > 1 - Util.GPUEpsilon) { Console.WriteLine("Bug 1: terminal prob over the threshold {0}!!!", t_i); }

                            float p = acc_log_t + (float)Math.Log(t_i);
                            if (i == MaxIterationNum - 1) p = acc_log_t;

                            AnswerProb[i].Output.Data.MemPtr[b] = (float)Math.Exp(p);
                            acc_log_t += (float)Math.Log(1 - t_i);

                            if (Math.Exp(p) > max_p)
                            {
                                max_p = Math.Exp(p);
                                max_iter = i;
                            }

                            if (BuilderParameters.PRED_RL == PredType.RL_AVGSIM)
                            {
                                FastVector.Add_Vector(FinalAns.Output.Data.MemPtr, b * FinalAns.Dim, AnsData[i].Output.Data.MemPtr, b * FinalAns.Dim, FinalAns.Dim, 1, (float)Math.Exp(p));
                            }
                        }
                        if (BuilderParameters.PRED_RL == PredType.RL_MAXITER)
                        {
                            FastVector.Add_Vector(FinalAns.Output.Data.MemPtr, b * FinalAns.Dim, AnsData[max_iter].Output.Data.MemPtr, b * FinalAns.Dim, FinalAns.Dim, 0, 1);
                        }
                    }
                    FinalAns.Output.Data.SyncFromCPU(FinalAns.BatchSize * FinalAns.Dim);

                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        AnswerProb[i].BatchSize = InitStatus.BatchSize;
                        AnswerProb[i].Output.Data.SyncFromCPU(AnswerProb[i].BatchSize);
                    }
                }
                #endregion.
            }
            public override void CleanDeriv()
            {
                {
                    ComputeLib.Zero(GAttHidden.Deriv.Data, GAttHidden.BatchSize * GAttHidden.Dim);
                }

                if (BuilderParameters.IS_QUERY_MEM)
                {
                    ComputeLib.Zero(QAttHidden.SentDeriv, QAttHidden.SentSize * QAttHidden.Dim);
                }

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {
                    TerminalRunner[i + 1].CleanDeriv();
                    // take Answer.
                    AnsRunner[i + 1].CleanDeriv();

                    // update Status.
                    StatusRunner[i].CleanDeriv();

                    if (BuilderParameters.IS_QUERY_MEM)
                    {
                        // obtain the retrieved mem.
                        RetrievalEnsembleRunner[i].CleanDeriv();
                        // retrieval mem zero.
                        QMemRetrievalRunner[i].CleanDeriv();
                        QAttRunner[i].CleanDeriv();
                    }

                    GMemRetrievalRunner[i].CleanDeriv();
                    // retrieval Attention zero.
                    GAttRunner[i].CleanDeriv();
                }

                TerminalRunner[0].CleanDeriv();
                AnsRunner[0].CleanDeriv();
            }

            /// <summary>
            /// Stage 1 : Iterative Attention Model (Supervised Training).
            /// </summary>
            /// <param name="cleanDeriv"></param>
            public override void Backward(bool cleanDeriv)
            {
                #region RL Pool.
                {
                    /// according to loss, assign reward to each terminal node.
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        Array.Clear(TerminalRunner[i].Output.Deriv.Data.MemPtr, 0, InitStatus.BatchSize);
                    }
                    for (int b = 0; b < InitStatus.BatchSize; b++)
                    {
                        for (int i = 0; i < MaxIterationNum; i++)
                        {
                            float reward = AnswerProb[i].Deriv.Data.MemPtr[b];

                            if (BuilderParameters.IS_NORM_REWARD) reward = reward / (i + 1);
                            TerminalRunner[i].Output.Deriv.Data.MemPtr[b] += reward * (1 - TerminalRunner[i].Output.Output.Data.MemPtr[b]);
                            for (int hp = 0; hp < i; hp++)
                            {
                                TerminalRunner[hp].Output.Deriv.Data.MemPtr[b] += -reward * TerminalRunner[hp].Output.Output.Data.MemPtr[b];
                            }

                            if (TerminalRunner[i].Output.Deriv.Data.MemPtr[b] > Math.Abs(1))
                            {
                                Logger.WriteLog("TerminalRunner Deriv is too large {0}, step {1}", TerminalRunner[i].Output.Deriv.Data.MemPtr[b], i);
                            }
                        }
                    }
                    for (int i = 0; i < MaxIterationNum; i++)
                    {
                        TerminalRunner[i].Output.Deriv.Data.SyncFromCPU(TerminalRunner[i].Output.BatchSize);
                    }
                }
                #endregion.

                for (int i = MaxIterationNum - 2; i >= 0; i--)
                {
                    TerminalRunner[i + 1].Backward(cleanDeriv);

                    AnsRunner[i + 1].Backward(cleanDeriv);

                    // update Status.
                    StatusRunner[i].Backward(cleanDeriv);

                    if (BuilderParameters.IS_QUERY_MEM)
                    {
                        // obtain the retrieved mem.
                        RetrievalEnsembleRunner[i].Backward(cleanDeriv);
                        // retrieval mem zero.
                        QMemRetrievalRunner[i].Backward(cleanDeriv);
                        QAttRunner[i].Backward(cleanDeriv);
                    }

                    GMemRetrievalRunner[i].Backward(cleanDeriv);
                    // retrieval Attention zero.
                    GAttRunner[i].Backward(cleanDeriv);
                }
                TerminalRunner[0].Backward(cleanDeriv);
                AnsRunner[0].Backward(cleanDeriv);

                // backpropagation to QMem.
                if (BuilderParameters.IS_QUERY_MEM)
                {
                    ComputeLib.Sgemm(QAttHidden.SentDeriv, 0,
                                 QAttStruct.Wm, 0,
                                 QMemory.SentDeriv, 0,
                                 QMemory.SentSize, QAttStruct.HiddenDim, QAttStruct.MemoryDim, 1, 1, false, true);
                }
            }

            public override void Update()
            {
                if (!IsDelegateOptimizer)
                {
                    if (BuilderParameters.IS_QUERY_MEM)
                    {
                        QAttStruct.WmMatrixOptimizer.BeforeGradient();
                        if (QAttStruct.IsBias) QAttStruct.BMatrixOptimizer.BeforeGradient();
                    }

                    GAttStruct.WmMatrixOptimizer.BeforeGradient();
                    if (GAttStruct.IsBias) GAttStruct.BMatrixOptimizer.BeforeGradient();
                    GMemory.MemoryOptimizer.BeforeGradient();
                }

                {
                    if (BuilderParameters.IS_QUERY_MEM)
                    {
                        if (QAttStruct.IsBias)
                            ComputeLib.ColumnWiseSum(QAttHidden.SentDeriv, QAttStruct.BMatrixOptimizer.Gradient, QMemory.SentSize, QAttStruct.HiddenDim, QAttStruct.BMatrixOptimizer.GradientStep);
                        ComputeLib.Sgemm(QMemory.SentOutput, 0,
                                         QAttHidden.SentDeriv, 0,
                                         QAttStruct.WmMatrixOptimizer.Gradient, 0,
                                         QMemory.SentSize, QAttStruct.MemoryDim, QAttStruct.HiddenDim,
                                         1, QAttStruct.WmMatrixOptimizer.GradientStep, true, false);
                    }

                    if (GAttStruct.IsBias) ComputeLib.ColumnWiseSum(GAttHidden.Deriv.Data, GAttStruct.BMatrixOptimizer.Gradient, GMemory.Size, GAttStruct.HiddenDim, GAttStruct.BMatrixOptimizer.GradientStep);
                    ComputeLib.Sgemm(GMemory.Memory, 0,
                                     GAttHidden.Deriv.Data, 0,
                                     GAttStruct.WmMatrixOptimizer.Gradient, 0,
                                     GMemory.Size, GAttStruct.MemoryDim, GAttStruct.HiddenDim,
                                     1, GAttStruct.WmMatrixOptimizer.GradientStep, true, false);

                    ComputeLib.Sgemm(GAttHidden.Deriv.Data, 0,
                                 GAttStruct.Wm, 0,
                                 GMemory.MemoryOptimizer.Gradient, 0,
                                 GMemory.Size, GAttStruct.HiddenDim, GAttStruct.MemoryDim, 1, GMemory.MemoryOptimizer.GradientStep, false, true);
                }



                AnsRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                AnsRunner[0].Update();

                TerminalRunner[0].IsDelegateOptimizer = IsDelegateOptimizer;
                TerminalRunner[0].Update();

                for (int i = 0; i < MaxIterationNum - 1; i++)
                {

                    if (BuilderParameters.IS_QUERY_MEM)
                    {
                        RetrievalEnsembleRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                        RetrievalEnsembleRunner[i].Update();

                        QAttRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                        QAttRunner[i].Update();

                        QMemRetrievalRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                        QMemRetrievalRunner[i].Update();
                    }

                    GAttRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    GAttRunner[i].Update();

                    // obtain the retrieved mem.
                    GMemRetrievalRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    GMemRetrievalRunner[i].Update();



                    // update Status.
                    StatusRunner[i].IsDelegateOptimizer = IsDelegateOptimizer;
                    StatusRunner[i].Update();

                    // take answer.
                    AnsRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    AnsRunner[i + 1].Update();

                    // take terminal gate or not.
                    TerminalRunner[i + 1].IsDelegateOptimizer = IsDelegateOptimizer;
                    TerminalRunner[i + 1].Update();
                }
                //if (BuilderParameters.AttType == CrossSimType.Addition || BuilderParameters.AttType == CrossSimType.Product || BuilderParameters.AttType == CrossSimType.SubCosine)
                if (!IsDelegateOptimizer)
                {
                    if (BuilderParameters.IS_QUERY_MEM)
                    {
                        QAttStruct.WmMatrixOptimizer.AfterGradient();
                        if (QAttStruct.IsBias) QAttStruct.BMatrixOptimizer.AfterGradient();
                    }

                    GAttStruct.WmMatrixOptimizer.AfterGradient();
                    if (GAttStruct.IsBias) GAttStruct.BMatrixOptimizer.AfterGradient();

                    GMemory.MemoryOptimizer.AfterGradient();
                }
            }
        }

        public class EnsemblePathTopKBeamRunner : ObjectiveRunner
        {
            List<List<BeamAction>>[] BatchResult;
            HiddenBatchData[] TreeProb;

            CudaPieceFloat TgtLabel;
            CudaPieceInt TgtSmpIdx;
            int TotalBatchNum = 0;
            int TopK = 0;

            int UnCompleteSeq = 0;

            string OutputFileName;
            int Iteration = 0;
            StreamWriter dumpWriter1;
            StreamWriter dumpWriter2;

            Dictionary<int, Dictionary<int, float>> TargetGraph;

            List<float> PathPredLen1 = new List<float>();
            List<float> PathPredLen2 = new List<float>();

            List<float> PathGroundLen = new List<float>();
            List<Tuple<int, int>> PathScrTgt;

            int EMPTY_PATH_CODE = 1000000;

            int UNREACHABLE_PATH_CODE = 1000002;
            float CalculatePathLen(int[] steps, int src, int tgt)
            {
                if (steps.Length == 0)
                {
                    return EMPTY_PATH_CODE;
                }
                else
                {
                    float pathLen = 0;
                    int s = src;
                    for (int i = 0; i < steps.Length + 1; i++)
                    {
                        int t = i == steps.Length ? tgt : steps[i];
                        if (TargetGraph.ContainsKey(s) && TargetGraph[s].ContainsKey(t))
                        {
                            pathLen += TargetGraph[s][t];
                        }
                        else
                        {
                            return UNREACHABLE_PATH_CODE;
                        }
                        s = t;
                    }
                    return pathLen;
                }
            }

            public EnsemblePathTopKBeamRunner(List<List<BeamAction>>[] beamResults, CudaPieceInt tgtSmpIdx, CudaPieceFloat tgtLabel, 
                Dictionary<int, Dictionary<int, float>> graph, List<Tuple<int,int>> testList, HiddenBatchData[] treeprob, int topK, string dumpFile)
                : base(Structure.Empty, new RunnerBehavior())
            {
                BatchResult = beamResults;
                TgtSmpIdx = tgtSmpIdx;
                TgtLabel = tgtLabel;
                TargetGraph = graph;

                PathScrTgt = testList;

                TreeProb = treeprob;
                TopK = topK;
                OutputFileName = dumpFile;
            }

            public override void Init()
            {
                dumpWriter1 = new StreamWriter(OutputFileName + ".p1." + Iteration.ToString());
                dumpWriter2 = new StreamWriter(OutputFileName + ".p2." + Iteration.ToString());

                TotalBatchNum = 0;

                UnCompleteSeq = 0;

                PathPredLen1.Clear();
                PathPredLen2.Clear();
                PathGroundLen.Clear();
            }

            double StatPath(int start, int end, List<float> PathPredLen)
            {
                int emptySeq = 0;
                int unSuccessSeq = 0;
                int correct_path = 0;
                int reachable_path = 0;
                float pred_reachable_path_len = 0;
                float true_reachable_path_len = 0;
                for (int i = start; i < end; i++)
                {
                    if (PathPredLen[i] == EMPTY_PATH_CODE) emptySeq += 1;
                    else if (PathPredLen[i] == UNREACHABLE_PATH_CODE) unSuccessSeq += 1;
                    else
                    {
                        reachable_path += 1;
                        if ((PathPredLen[i] - PathGroundLen[i]) <= 0.00001) correct_path += 1;

                        pred_reachable_path_len += PathPredLen[i];
                        true_reachable_path_len += PathGroundLen[i];
                    }
                }
                Logger.WriteLog("Empty Seq {0}, {1}", emptySeq, emptySeq * 1.0 / (end-start));

                Logger.WriteLog("UnSuccess Seq {0}, {1}", unSuccessSeq, unSuccessSeq * 1.0 /(end-start));

                Logger.WriteLog("Reachable Seq {0}, {1}", reachable_path, reachable_path * 1.0 / (end - start));
                Logger.WriteLog("Perfect Seq {0}, {1}", correct_path, correct_path * 1.0 / (end - start));

                Logger.WriteLog("Reachable Seq Len {0}, ground {1}, oversize {2}", pred_reachable_path_len, true_reachable_path_len,
                    pred_reachable_path_len * 1.0 / true_reachable_path_len);

                return correct_path * 1.0 / (end - start);
            }

            public override void Complete()
            {

                Logger.WriteLog("Total UnComplete Seq {0} in Total {1}", UnCompleteSeq, TotalBatchNum);

                int validLen = TotalBatchNum / 2;

                Logger.WriteLog("\nDevSet Evaluation 1**************");
                double dev_S1 = StatPath(validLen, TotalBatchNum, PathPredLen1);
                Logger.WriteLog("\nValidSet Evaluation 1**************");
                double valid_S1 = StatPath(0, validLen, PathPredLen1);

                Logger.WriteLog("\nDevSet Evaluation 2**************");
                double dev_S2 = StatPath(validLen, TotalBatchNum, PathPredLen2);
                Logger.WriteLog("\nValidSet Evaluation 2**************");
                double valid_S2 = StatPath(0, validLen, PathPredLen2);

                ObjectiveScore = dev_S2;

                Iteration += 1;
                dumpWriter1.Close();
                dumpWriter2.Close();
            }

            // 0 : maxTreeProb
            // 1 : maxTreeProb * GenerationProb;
            public override void Forward()
            {
                int BatchSize = BatchResult[0].Count;
                TgtSmpIdx.SyncToCPU();
                TgtLabel.SyncToCPU();

                for (int d = 0; d < BatchResult.Length; d++)
                {
                    TreeProb[d].Output.Data.SyncToCPU(BatchSize);
                }

                for (int b = 0; b < BatchSize; b++)
                {
                    int srcIdx = PathScrTgt[TotalBatchNum + b].Item1;
                    int tgtIdx = PathScrTgt[TotalBatchNum + b].Item2;

                    int start = b == 0 ? 0 : TgtSmpIdx.MemPtr[b - 1];
                    int len = TgtSmpIdx.MemPtr[b] - start - 1;
                    string groundT = string.Join(" ", TgtLabel.MemPtr.Skip(start).Take(len));
                    int[] groundSteps = groundT.Split(' ').Select(i => int.Parse(i)).ToArray();

                    for (int d = 0; d < BatchResult.Length; d++)
                    {
                        BatchResult[d][b].Sort((a, c) => (c.Prob).CompareTo(a.Prob));
                    }

                    float gLen = CalculatePathLen(groundSteps, srcIdx, tgtIdx);
                    if (gLen == EMPTY_PATH_CODE || gLen == UNREACHABLE_PATH_CODE)
                    {
                        Console.WriteLine("Wrong Here, pls debug 1");
                        Console.ReadLine();
                    }
                    PathGroundLen.Add(gLen);

                    string trueSrcIdx = DataPanel.InputSymbol[srcIdx];
                    string trueTgtIdx = DataPanel.InputSymbol[tgtIdx];

                    dumpWriter1.WriteLine();
                    dumpWriter1.WriteLine(string.Format("{0} {1}\t{0} {2} {1}\t{3}", trueSrcIdx, trueTgtIdx, string.Join(" ", groundSteps.Select(i => DataPanel.OutputSymbol[i])), gLen));

                    dumpWriter2.WriteLine();
                    dumpWriter2.WriteLine(string.Format("{0} {1}\t{0} {2} {1}\t{3}", trueSrcIdx, trueTgtIdx, string.Join(" ", groundSteps.Select(i => DataPanel.OutputSymbol[i])), gLen));

                    List<Tuple<float, float, float, string, float>> results = new List<Tuple<float, float, float, string, float>>();
                    float log_sum = -100000;

                    for (int d = 0; d < BatchResult.Length; d++)
                    {
                        string predT = string.Join(" ", BatchResult[d][b][0].Words.Skip(1));
                        List<int> tmpSteps = predT.Split(' ').Select(i => int.Parse(i)).ToList();
                        double gProb = BatchResult[d][b][0].Prob;
                        if (tmpSteps.Last() == 0)
                        {
                            tmpSteps.RemoveAt(tmpSteps.Count - 1);
                            if (tmpSteps.Count == 0) predT = "";
                            else predT = string.Join(" ", tmpSteps);
                        }
                        else
                        {
                            gProb = -100000;
                        }

                        float treeProb = TreeProb[d].Output.Data.MemPtr[b];
                        float predLen = CalculatePathLen(tmpSteps.ToArray(), srcIdx, tgtIdx);

                        float log_prob = (float)(gProb + Math.Log(treeProb));
                        results.Add(new Tuple<float, float, float, string, float>(treeProb, (float)gProb, log_prob, string.Join(" ", tmpSteps.Select(i => DataPanel.OutputSymbol[i])), predLen));
                        log_sum = Util.LogAdd(log_sum, log_prob);
                    }

                    int maxD1 = -1;
                    float maxP1 = -float.MaxValue;

                    int maxD2 = -1;
                    float maxP2 = -float.MaxValue;

                    for (int d = 0; d < BatchResult.Length; d++)
                    {
                        if (results[d].Item1 > maxP1)
                        {
                            maxP1 = results[d].Item1;
                            maxD1 = d;
                        }
                        if (results[d].Item3 > maxP2)
                        {
                            maxP2 = results[d].Item3;
                            maxD2 = d;
                        }
                    }

                    dumpWriter1.WriteLine(string.Format("{0} {1}\t{0} {2} {1}\t{3}\t{4}", trueSrcIdx, trueTgtIdx, results[maxD1].Item4, results[maxD1].Item5, maxD1));
                    dumpWriter2.WriteLine(string.Format("{0} {1}\t{0} {2} {1}\t{3}\t{4}", trueSrcIdx, trueTgtIdx, results[maxD2].Item4, results[maxD2].Item5, maxD2));

                    for (int d = 0; d < BatchResult.Length; d++)
                    {
                        dumpWriter1.WriteLine(string.Format("{0} {1}\t{0} {2} {1}\t{3}\t{4}", trueSrcIdx, trueTgtIdx, results[d].Item4, results[d].Item5, results[d].Item1));
                        dumpWriter2.WriteLine(string.Format("{0} {1}\t{0} {2} {1}\t{3}\t{4}", trueSrcIdx, trueTgtIdx, results[d].Item4, results[d].Item5, Math.Exp(results[d].Item3 - log_sum)));
                    }

                    PathPredLen1.Add(results[maxD1].Item5);
                    PathPredLen2.Add(results[maxD2].Item5);
                }
                TotalBatchNum += BatchResult[0].Count;
            }
        }

        public static ComputationGraph BuildComputationGraph(IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> query1,
                                                             IDataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> query2,
                                                             IDataCashier<SeqSparseDataSource, SequenceDataStat> answer,
                                                             CompositeNNStructure modelStructure, bool createNew, RunnerBehavior Behavior)
        {
            EmbedStructure QueryEmbed = null;
            //LSTMStructure QueryD1LSTM = null;
            //LSTMStructure QueryD2LSTM = null;

            MatrixStructure GlobalMemory = null;
            MLPAttentionStructure GlobalAttStruct = null;

            //MLPAttentionStructure QueryAttStruct = null;

            GRUCell StateStruct = null;
            LayerStructure TerminalStruct = null;
            LayerStructure AnsStruct = null;

            /// decode generation model.
            EmbedStructure TgtEmbed = null;
            MLPAttentionStructure AnsAttention = null;
            LSTMCell AnsDecoder = null;
            EmbedStructure EmbedDecodeTgt = null;

            #region Model Architecture Parameters.
            if (createNew)
            {
                if (BuilderParameters.SEED_MODEL.Equals(string.Empty))
                {
                    //QueryEmbed = new ConvStructure(new LayerStructure(DataPanel.InputDim, BuilderParameters.S_EmbedDim,
                    //    BuilderParameters.S_EmbedAf, N_Type.Convolution_layer, 1, 0, false, Behavior.Device));

                    QueryEmbed = new EmbedStructure(DataPanel.InputDim, BuilderParameters.S_EmbedDim, Behavior.Device);

                    //QueryD1LSTM = new LSTMStructure(BuilderParameters.S_EmbedDim, new int[] { BuilderParameters.R_EmbedDim }, Behavior.Device, BuilderParameters.R_Init);
                    //QueryD2LSTM = new LSTMStructure(BuilderParameters.S_EmbedDim, new int[] { BuilderParameters.R_EmbedDim }, Behavior.Device, BuilderParameters.R_Init);
                    int StateDim = 2 * BuilderParameters.S_EmbedDim;

                    GlobalMemory = new MatrixStructure(StateDim, BuilderParameters.MemorySize, Behavior.Device);
                    GlobalAttStruct = new MLPAttentionStructure(StateDim, StateDim, BuilderParameters.M_ATT_DIM, Behavior.Device);

                    //QueryAttStruct = new MLPAttentionStructure(StateDim, StateDim, BuilderParameters.Q_ATT_DIM, Behavior.Device);

                    StateStruct = new GRUCell(BuilderParameters.IS_QUERY_MEM ? StateDim + StateDim : StateDim, StateDim, Behavior.Device, BuilderParameters.R_Init);
                    AnsStruct = new LayerStructure(StateDim, StateDim, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, Behavior.Device);
                    TerminalStruct = new LayerStructure(StateDim, 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, true, Behavior.Device);

                    TgtEmbed = BuilderParameters.IS_SHARE_EMD ? QueryEmbed : new EmbedStructure(DataPanel.OutputDim, BuilderParameters.S_EmbedDim, Behavior.Device);
                    AnsDecoder = new LSTMCell(BuilderParameters.S_EmbedDim, StateDim, StateDim, Behavior.Device);

                    AnsAttention = new MLPAttentionStructure(StateDim, StateDim, StateDim, Behavior.Device);
                    EmbedDecodeTgt = new EmbedStructure(DataPanel.OutputDim, StateDim, Behavior.Device);

                    modelStructure.AddLayer(QueryEmbed);
                    //modelStructure.AddLayer(QueryD1LSTM);
                    //modelStructure.AddLayer(QueryD2LSTM);

                    modelStructure.AddLayer(GlobalMemory);
                    modelStructure.AddLayer(GlobalAttStruct);
                    //modelStructure.AddLayer(QueryAttStruct);

                    modelStructure.AddLayer(StateStruct);
                    modelStructure.AddLayer(AnsStruct);
                    modelStructure.AddLayer(TerminalStruct);

                    modelStructure.AddLayer(TgtEmbed);
                    modelStructure.AddLayer(AnsDecoder);
                    modelStructure.AddLayer(AnsAttention);
                    modelStructure.AddLayer(EmbedDecodeTgt);
                }
                else
                {
                    using (BinaryReader modelReader = new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read))) // DeviceType.GPU);
                    {
                        modelStructure = new CompositeNNStructure(modelReader, Behavior.Device);

                        int link = 0;
                        //QueryEmbed = (ConvStructure)modelStructure.CompositeLinks[link++];
                        QueryEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];

                        //QueryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                        //QueryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];

                        GlobalMemory = (MatrixStructure)modelStructure.CompositeLinks[link++];
                        GlobalAttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                        //QueryAttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];

                        StateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                        AnsStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                        TerminalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];

                        TgtEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];
                        AnsDecoder = (LSTMCell)modelStructure.CompositeLinks[link++];
                        AnsAttention = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                        EmbedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[link++];
                    }
                }
                modelStructure.InitOptimizer(OptimizerParameters.StructureOptimizer);
            }
            else
            {
                int link = 0;
                //QueryEmbed = (ConvStructure)modelStructure.CompositeLinks[link++];
                QueryEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];

                //QueryD1LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];
                //QueryD2LSTM = (LSTMStructure)modelStructure.CompositeLinks[link++];

                GlobalMemory = (MatrixStructure)modelStructure.CompositeLinks[link++];
                GlobalAttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                //QueryAttStruct = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];

                StateStruct = (GRUCell)modelStructure.CompositeLinks[link++];
                AnsStruct = (LayerStructure)modelStructure.CompositeLinks[link++];
                TerminalStruct = (LayerStructure)modelStructure.CompositeLinks[link++];

                TgtEmbed = (EmbedStructure)modelStructure.CompositeLinks[link++];
                AnsDecoder = (LSTMCell)modelStructure.CompositeLinks[link++];
                AnsAttention = (MLPAttentionStructure)modelStructure.CompositeLinks[link++];
                EmbedDecodeTgt = (EmbedStructure)modelStructure.CompositeLinks[link++];

            }
            #endregion.

            ComputationGraph cg = new ComputationGraph();

            GeneralBatchInputData Query1 = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(query1, Behavior));
            GeneralBatchInputData Query2 = (GeneralBatchInputData)cg.AddDataRunner(new DataRunner<GeneralBatchInputData, GeneralBatchInputDataStat>(query2, Behavior));

            SeqSparseDataSource AnswerData = (SeqSparseDataSource)cg.AddDataRunner(new DataRunner<SeqSparseDataSource, SequenceDataStat>(answer, Behavior));

            /************************************************************ Query Embedding Data *********/
            
            HiddenBatchData QueryD1O = (HiddenBatchData)cg.AddRunner(new AdvancedEmbedRunner(QueryEmbed, Query1, Behavior));
            HiddenBatchData QueryD2O = (HiddenBatchData)cg.AddRunner(new AdvancedEmbedRunner(QueryEmbed, Query2, Behavior));

            HiddenBatchData QueryOutput = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { QueryD1O, QueryD2O }, Behavior));
            IntArgument arg = new IntArgument("batchSize");
            cg.AddRunner(new HiddenDataBatchSizeRunner(QueryOutput, arg, Behavior));
            BiMatchBatchData GlobalMemMatch = (BiMatchBatchData)cg.AddRunner(new RepeatBiMatchRunner(QueryOutput.MAX_BATCHSIZE, GlobalMemory.Size, arg, Behavior));

            ReasonNetRunner ReasonRunner = new ReasonNetRunner(QueryOutput, BuilderParameters.RECURRENT_STEP,
                                                               GlobalMemory, GlobalMemMatch, GlobalAttStruct,
                                                               null, null, null,
                                                               StateStruct, AnsStruct, TerminalStruct, BuilderParameters.Gamma, Behavior);
            cg.AddRunner(ReasonRunner);

            switch (Behavior.RunMode)
            {
                case DNNRunMode.Train:

                    SeqDenseRecursiveData TgtlstmInput = (SeqDenseRecursiveData)cg.AddRunner(new AdvancedSeqEmbedRunner(TgtEmbed, AnswerData.SequenceData, false, Behavior));

                    List<SeqDenseRecursiveData> TgtDecodeOutputs = new List<SeqDenseRecursiveData>();
                    for (int i = 0; i < BuilderParameters.RECURRENT_STEP; i++)
                    {
                        //MLPAttentionV3Runner attRunner = new MLPAttentionV3Runner(AnsAttention, ReasonRunner.AnsData.Take(i + 1).ToArray(),
                        //     DataPanel.MAX_SEQ_LEN + 5, TgtlstmInput.MAX_BATCHSIZE, TgtlstmInput.MAX_SENTSIZE, Behavior);

                        MLPAttentionV4Runner attRunner = new MLPAttentionV4Runner(AnsAttention, ReasonRunner.StatusData.Take(i + 1).ToArray(), ReasonRunner.AnsData.Take(i + 1).ToArray(),
                             DataPanel.MAX_SEQ_LEN + 5, TgtlstmInput.MAX_BATCHSIZE, TgtlstmInput.MAX_SENTSIZE, Behavior);
                        SeqDenseRecursiveData TgtlstmOutput = (SeqDenseRecursiveData)cg.AddRunner(new FastLSTMDenseRunner<SeqDenseRecursiveData>(AnsDecoder,
                                TgtlstmInput, QueryOutput, QueryOutput, attRunner, Behavior));
                        TgtDecodeOutputs.Add(TgtlstmOutput);
                    }
                    cg.AddObjective(new EmbedSoftmaxContrastiveRewardRunner(EmbedDecodeTgt, TgtDecodeOutputs.ToArray(),
                        TgtlstmInput.MapForward, TgtlstmInput.MapBackward, ReasonRunner.AnswerProb, AnswerData.SequenceLabel, BuilderParameters.SoftmaxGamma, Behavior, true));
                    break;
                case DNNRunMode.Predict:
                    List<EnsembleBeamSearchRunner> beamRunners = new List<EnsembleBeamSearchRunner>();
                    for (int i = 0; i < BuilderParameters.RECURRENT_STEP; i++)
                    {
                        //MLPAttentionV3Runner attRunner = new MLPAttentionV3Runner(AnsAttention, ReasonRunner.AnsData.Take(i + 1).ToArray(),
                        //    QueryOutput.Stat.MAX_BATCHSIZE, BuilderParameters.BEAM * BuilderParameters.BEAM, Behavior);
                        MLPAttentionV4Runner attRunner = new MLPAttentionV4Runner(AnsAttention, ReasonRunner.StatusData.Take(i + 1).ToArray(),
                            ReasonRunner.AnsData.Take(i + 1).ToArray(), QueryOutput.MAX_BATCHSIZE, BuilderParameters.BEAM * BuilderParameters.BEAM, Behavior);

                        LSTMEmbedDecodingRunner decodeRunner = new LSTMEmbedDecodingRunner(
                            new LSTMStructure(new List<LSTMCell>() { AnsDecoder }), TgtEmbed, EmbedDecodeTgt,
                            new List<Tuple<HiddenBatchData, HiddenBatchData>>() { new Tuple<HiddenBatchData, HiddenBatchData>(QueryOutput, QueryOutput) },
                            new List<BasicMLPAttentionRunner>() { attRunner }, Behavior, true);
                        EnsembleBeamSearchRunner beamRunner = new EnsembleBeamSearchRunner(0, 0, BuilderParameters.BEAM, DataPanel.MAX_SEQ_LEN + 5, EmbedDecodeTgt.VocabSize, QueryOutput.MAX_BATCHSIZE, Behavior);
                        beamRunner.AddBeamSearch(decodeRunner);
                        cg.AddRunner(beamRunner);

                        beamRunners.Add(beamRunner);
                    }

                    cg.AddRunner(new EnsemblePathTopKBeamRunner(beamRunners.Select(i => i.BatchResult).ToArray(),
                            AnswerData.SampleIdx, AnswerData.SequenceLabel, DataPanel.Graph, DataPanel.TestSrcTgt, ReasonRunner.AnswerProb, 1, BuilderParameters.SCORE_PATH));
                    break;
            }

            cg.SetDelegateModel(modelStructure);
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);
            if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

            Logger.WriteLog("Loading Training/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Training/Test Data Finished.");

            CompositeNNStructure modelStructure = new CompositeNNStructure();

            ComputationGraph trainCG = BuildComputationGraph(DataPanel.TrainSrc1Bin, DataPanel.TrainSrc2Bin, DataPanel.TrainTgtBin, modelStructure, true,
                    new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            ComputationGraph testCG = BuildComputationGraph(DataPanel.ValidSrc1Bin, DataPanel.ValidSrc2Bin, DataPanel.ValidTgtBin, modelStructure, false,
                    new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

            //double bestValidScore = double.MinValue;
            double bestTestScore = double.MinValue;
            int bestIter = -1;
            for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
            {
                double loss = trainCG.Execute();
                Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);
                if (DeepNet.BuilderParameters.ModelSavePerIteration)
                {
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(BuilderParameters.ModelOutputPath, string.Format("RecurrentAttention.iter.{0}", iter)), FileMode.Create, FileAccess.Write)))
                    {
                        modelStructure.Serialize(writer);
                    }
                }

                double testScore = 0;
                if (testCG != null)
                {
                    testScore = testCG.Execute();
                    Logger.WriteLog("Test Score {0}, At iter {1}", testScore, iter);
                }

                if (bestTestScore < testScore) { bestTestScore = testScore; bestIter = iter; }
                Logger.WriteLog("Best Valid/Test Score {0}, At iter {1}", bestTestScore, bestIter);
            }

        }

        public class DataPanel
        {
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TrainSrc1Bin = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> TrainSrc2Bin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> TrainTgtBin = null;

            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> ValidSrc1Bin = null;
            public static DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat> ValidSrc2Bin = null;
            public static DataCashier<SeqSparseDataSource, SequenceDataStat> ValidTgtBin = null;

            static List<string> inputSymbol = new List<string>();
            static List<string> outputSymbol = new List<string>();
            public static List<string> InputSymbol { get { return inputSymbol; } set { inputSymbol = value; } }
            public static List<string> OutputSymbol
            {
                get { return BuilderParameters.IS_SHARE_EMD ? inputSymbol : outputSymbol; }
                set
                {
                    if (BuilderParameters.IS_SHARE_EMD) inputSymbol = value;
                    else outputSymbol = value;
                }
            }

            static Dictionary<string, int> inputSymbolDict = new Dictionary<string, int>();
            static Dictionary<string, int> outputSymbolDict = new Dictionary<string, int>();

            public static Dictionary<string, int> InputSymbolDict { get { return inputSymbolDict; } set { inputSymbolDict = value; } }
            public static Dictionary<string, int> OutputSymbolDict
            {
                get { return BuilderParameters.IS_SHARE_EMD ? inputSymbolDict : outputSymbolDict; }
                set
                {
                    if (BuilderParameters.IS_SHARE_EMD) inputSymbolDict = value;
                    else outputSymbolDict = value;
                }
            }


            public static int InputDim { get { return InputSymbolDict.Count; } }
            public static int OutputDim { get { return OutputSymbolDict.Count; } }


            public static Dictionary<int, Dictionary<int, float>> Graph = new Dictionary<int, Dictionary<int, float>>();

            public static List<Tuple<int, int>> TestSrcTgt = new List<Tuple<int, int>>();

            public static int MAX_SEQ_LEN = 0;
            public static int MIN_SEQ_LEN = 0;

            #region Symbol Extraction.
            static void ScanSymbol(string infile)
            {
                List<int> PathLen = new List<int>();
                using (StreamReader mreader = new StreamReader(infile))
                {
                    int fileIdx = 0;
                    while (!mreader.EndOfStream)
                    {
                        string[] qa = mreader.ReadLine().Split('\t');
                        string q = qa[0];
                        string a = qa[1];

                        string[] qItems = q.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string qterm in qItems)
                        {
                            if (qterm.Trim().Equals("")) continue;
                            if (!InputSymbolDict.ContainsKey(qterm))
                            {
                                InputSymbolDict.Add(qterm, InputSymbolDict.Count);
                                InputSymbol.Add(qterm);
                            }
                        }

                        string[] aItems = a.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string aterm in aItems)
                        {
                            if (aterm.Trim().Equals("")) continue;
                            if (!OutputSymbolDict.ContainsKey(aterm))
                            {
                                OutputSymbolDict.Add(aterm, OutputSymbolDict.Count);
                                OutputSymbol.Add(aterm);
                            }
                        }

                        PathLen.Add(aItems.Length);
                        //if (aItems.Length > MAX_SEQ_LEN) { MAX_SEQ_LEN = aItems.Length; }
                        if (++fileIdx % 1000 == 0) { Console.WriteLine("Extract vocab from Train Corpus {0}", fileIdx); }
                    }
                    Console.WriteLine("Query Symbol {0}", InputSymbolDict.Count.ToString());
                    Console.WriteLine("Answer Symbol {0}", OutputSymbolDict.Count.ToString());

                    MAX_SEQ_LEN = PathLen.Max();
                    MIN_SEQ_LEN = PathLen.Min();
                }
            }
            #endregion.

            #region Extract Index.
            static List<Tuple<List<int>, List<int>>> LoadData(string dataStream)
            {
                List<Tuple<List<int>, List<int>>> Data = new List<Tuple<List<int>, List<int>>>();
                using (StreamReader dataReader = new StreamReader(dataStream))
                {
                    while (!dataReader.EndOfStream)
                    {
                        string[] items = dataReader.ReadLine().Split('\t');
                        string[] srcItems = items[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] tgtItems = items[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        List<int> srcInts = srcItems.Select(i => InputSymbolDict[i]).ToList();
                        List<int> tgtInts = tgtItems.Select(i => OutputSymbolDict[i]).ToList();
                        tgtInts[0] = 0; //.Insert(0, 0);
                        tgtInts[tgtInts.Count - 1] = 0; //.Add(0);
                        Data.Add(new Tuple<List<int>, List<int>>(srcInts, tgtInts));
                    }
                }
                return Data;
            }
            #endregion.

            static void ExtractBinary(string input, string srcbin, string tgtbin)
            {
                List<Tuple<List<int>, List<int>>> data = LoadData(input);

                BinaryWriter src1BatchDataWriter = new BinaryWriter(new FileStream(srcbin + ".1", FileMode.Create, FileAccess.Write));
                //SeqSparseBatchData srcBatchData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = InputDim }, DeviceType.CPU);
                GeneralBatchInputData src1Data = new GeneralBatchInputData(new GeneralBatchInputDataStat()
                {
                    MAX_FEATUREDIM = InputDim,
                    MAX_BATCHSIZE = BuilderParameters.MiniBatchSize,
                    FeatureType = FeatureDataType.SparseFeature
                }, DeviceType.CPU);

                BinaryWriter src2BatchDataWriter = new BinaryWriter(new FileStream(srcbin + ".2", FileMode.Create, FileAccess.Write));
                //SeqSparseBatchData srcBatchData = new SeqSparseBatchData(new SequenceDataStat() { FEATURE_DIM = InputDim }, DeviceType.CPU);
                GeneralBatchInputData src2Data = new GeneralBatchInputData(new GeneralBatchInputDataStat()
                {
                    MAX_FEATUREDIM = InputDim,
                    MAX_BATCHSIZE = BuilderParameters.MiniBatchSize,
                    FeatureType = FeatureDataType.SparseFeature
                }, DeviceType.CPU);

                BinaryWriter tgtBatchDataWriter = new BinaryWriter(new FileStream(tgtbin, FileMode.Create, FileAccess.Write));
                SeqSparseDataSource tgtBatchData = new SeqSparseDataSource(new SequenceDataStat() { FEATURE_DIM = OutputDim }, DeviceType.CPU);

                int lineIdx = 0;
                int maxSrcLength = 0;
                int maxTgtLength = 0;
                foreach (Tuple<List<int>, List<int>> smp in data)
                {
                    Dictionary<int, float> src1fea = new Dictionary<int, float>();
                    Dictionary<int, float> src2fea = new Dictionary<int, float>();
                    src1fea.Add(smp.Item1[0], 1);
                    src2fea.Add(smp.Item1[1], 1);

                    List<Tuple<Dictionary<int, float>, float>> tgtFea = new List<Tuple<Dictionary<int, float>, float>>();
                    for (int tIdx = 0; tIdx < smp.Item2.Count - 1; tIdx++)
                    {
                        int tgtId = smp.Item2[tIdx];
                        Dictionary<int, float> d = new Dictionary<int, float>();
                        d.Add(tgtId, 1);
                        tgtFea.Add(new Tuple<Dictionary<int, float>, float>(d, smp.Item2[tIdx + 1]));
                    }

                    if (++lineIdx % 100000 == 0) Console.WriteLine("Loading Data {0}", lineIdx);

                    if (tgtFea.Count >= maxTgtLength) maxTgtLength = tgtFea.Count;

                    src1Data.PushSample(src1fea, 0);
                    src2Data.PushSample(src2fea, 0);
                    tgtBatchData.PushSample(tgtFea);

                    if (src1Data.BatchSize >= BuilderParameters.MiniBatchSize ||
                        src2Data.BatchSize >= BuilderParameters.MiniBatchSize ||
                        tgtBatchData.BatchSize >= BuilderParameters.MiniBatchSize)
                    {
                        src1Data.PopBatchToStat(src1BatchDataWriter);
                        src2Data.PopBatchToStat(src2BatchDataWriter);
                        tgtBatchData.PopBatchToStat(tgtBatchDataWriter);
                    }
                }
                Console.WriteLine("Completed! Saving Statistic...");
                src1Data.PopBatchCompleteStat(src1BatchDataWriter);
                src2Data.PopBatchCompleteStat(src2BatchDataWriter);
                tgtBatchData.PopBatchCompleteStat(tgtBatchDataWriter);
                Console.WriteLine("Done!");

                Console.WriteLine("Source Data SampleNumber : {0}, Feature Dim : {1}, MAX Src Length : {2}",
                    src1Data.Stat.TotalSampleNumber, src1Data.Stat.MAX_FEATUREDIM, maxSrcLength);

                Console.WriteLine("Source Data SampleNumber : {0}, Feature Dim : {1}, MAX Src Length : {2}",
                    src2Data.Stat.TotalSampleNumber, src2Data.Stat.MAX_FEATUREDIM, maxSrcLength);

                Console.WriteLine("Target Data SampleNumber : {0}, Feature Dim : {1}, MAX Tgt Length : {2}",
                    tgtBatchData.Stat.TotalSampleNumber, tgtBatchData.Stat.FEATURE_DIM, maxTgtLength);
            }

            public static void Init()
            {
                OutputSymbolDict.Add("<SEQ>", 0);
                OutputSymbol.Add("<SEQ>");
                ScanSymbol(BuilderParameters.TrainData);
                ScanSymbol(BuilderParameters.TestData);

                if (!BuilderParameters.GRAPH.Equals(string.Empty))
                {
                    Console.WriteLine("Loading Underlying Graph for Evaluation");
                    string[] lines = File.ReadAllLines(BuilderParameters.GRAPH);
                    foreach (string item in lines)
                    {
                        string[] kv = item.Split('\t');
                        int srcId = OutputSymbolDict[kv[0]];
                        int tgtId = OutputSymbolDict[kv[1]];

                        float wei = 1;
                        if(item.Length >= 3) wei = float.Parse(kv[2]);

                        if (!Graph.ContainsKey(srcId)) { Graph.Add(srcId, new Dictionary<int, float>()); }
                        Graph[srcId][tgtId] = wei;
                    }
                }

                string[] testlines = File.ReadAllLines(BuilderParameters.TestData);
                foreach (string item in testlines)
                {
                    string[] kv = item.Split('\t')[0].Split(' ');
                    int srcId = OutputSymbolDict[kv[0]];
                    int tgtId = OutputSymbolDict[kv[1]];
                    TestSrcTgt.Add(new Tuple<int, int>(srcId, tgtId));
                }

                Console.WriteLine("Min Decode Length {0}", MIN_SEQ_LEN);
                Console.WriteLine("Max Decode Length {0}", MAX_SEQ_LEN);

                ExtractBinary(BuilderParameters.TrainData, BuilderParameters.TrainQuery, BuilderParameters.TrainAnswer);
                ExtractBinary(BuilderParameters.TestData, BuilderParameters.TestQuery, BuilderParameters.TestAnswer);

                ValidSrc1Bin = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestQuery + ".1");
                ValidSrc2Bin = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TestQuery + ".2");
                ValidTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TestAnswer);

                ValidSrc1Bin.InitThreadSafePipelineCashier(100, false);
                ValidSrc2Bin.InitThreadSafePipelineCashier(100, false);
                ValidTgtBin.InitThreadSafePipelineCashier(100, false);

                TrainSrc1Bin = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainQuery + ".1");
                TrainSrc2Bin = new DataCashier<GeneralBatchInputData, GeneralBatchInputDataStat>(BuilderParameters.TrainQuery + ".2");
                TrainTgtBin = new DataCashier<SeqSparseDataSource, SequenceDataStat>(BuilderParameters.TrainAnswer);

                TrainSrc1Bin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                TrainSrc2Bin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
                TrainTgtBin.InitThreadSafePipelineCashier(100, ParameterSetting.RANDOM_SEED);
            }
        }
    }
}
