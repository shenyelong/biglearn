﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
namespace BigLearn.DeepNet
{
    public class AppReinforceWalkLinkPredictionV2_PREDBuilder : Builder
    {
        /// <summary>
        /// 
        /// </summary>
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string Entity2Id { get { return Argument["ENTITY2ID"].Value; } }
            public static int EntityNum { get { return int.Parse(Argument["ENTITYNUM"].Value); } }

            public static string Relation2Id { get { return Argument["RELATION2ID"].Value; } }
            public static int RelationNum { get { return int.Parse(Argument["RELATIONNUM"].Value); } }

            public static string Folder { get { return Argument["FOLDER"].Value; } }
            public static string TrainData { get { return Folder == string.Empty ? Argument["TRAIN"].Value : Folder + @"\\train.txt"; } }
            public static string ValidData { get { return Folder == string.Empty ? Argument["VALID"].Value : Folder + @"\\valid.txt"; } }
            public static string TestData { get { return Folder == string.Empty ? Argument["TEST"].Value : Folder + @"\\test.txt"; } }

            public static bool IsTrainFile { get { return !(TrainData.Equals(string.Empty)); } }
            public static bool IsValidFile { get { return !(ValidData.Equals(string.Empty)); } }
            public static bool IsTestFile { get { return !(TestData.Equals(string.Empty)); } }
            public static string ValidPathFolder { get { return Argument["VALID-PATH"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }
            public static int TrainBeamSize { get { return int.Parse(Argument["TRAIN-BEAM-SIZE"].Value); } }
            public static int TrainSetting { get { return int.Parse(Argument["TRAIN-SETTING"].Value); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (Argument["MODEL-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }

            public static float Gamma { get { return float.Parse(Argument["GAMMA"].Value); } }
            public static float T_MAX_CLIP { get { return float.Parse(Argument["T-MAX-CLIP"].Value); } }
            public static float T_MIN_CLIP { get { return float.Parse(Argument["T-MIN-CLIP"].Value); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static float GUID_RATE { get { return float.Parse(Argument["GUID-RATE"].Value); } }
            public static float RAND_RATE { get { return float.Parse(Argument["RAND-RATE"].Value); } }

            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float TNEG_R { get { return float.Parse(Argument["TNEG-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }

            public static float NORM_REWARD { get { return float.Parse(Argument["NORM-REWARD"].Value); } }

            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int TRAIN_DEBUG { get { return int.Parse(Argument["TRAIN-DEBUG"].Value); } }

            public static int PRE_MCTS_NUM { get { return int.Parse(Argument["PRE-MCTS-NUM"].Value); } }

            public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static int SEED_VERSION { get { return int.Parse(Argument["SEED-VERSION"].Value); } }

            public static float MSE_LAMBDA { get { return float.Parse(Argument["MSE-LAMBDA"].Value); } }


            public static float RAND_TERM { get { return float.Parse(Argument["RAND-TERM"].Value); } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static List<Tuple<int, float>> ScheduleRewardDiscount
            {
                get
                {
                    return Argument["R-FB"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static float Epsilon(int step)
            {
                for (int i = 0; i < ScheduleRewardDiscount.Count; i++)
                {
                    if (step < ScheduleRewardDiscount[i].Item1)
                    {
                        float lambda = (step - ScheduleRewardDiscount[i - 1].Item1) * 1.0f / (ScheduleRewardDiscount[i].Item1 - ScheduleRewardDiscount[i - 1].Item1);
                        return lambda * ScheduleRewardDiscount[i].Item2 + (1 - lambda) * ScheduleRewardDiscount[i - 1].Item2;
                    }
                }
                return ScheduleRewardDiscount.Last().Item2;
            }

            public static int EStrategy(int mcts_idx)
            {
                string[] idxs = Argument["MCTS-EXP"].Value.Split(',').ToArray();
                for (int i = 0; i < idxs.Length; i++)
                {
                    if (mcts_idx < int.Parse(idxs[i]))
                    {
                        return i;
                    }
                }
                return idxs.Length - 1;
            }

            public static int MEM_CLEAN { get { return int.Parse(Argument["MEM-CLEAN"].Value); } }

            public static int SCORE_TYPE { get { return int.Parse(Argument["SCORE-TYPE"].Value); } }

            public static float UPDATE_R_DISCOUNT { get { return float.Parse(Argument["UPDATE-R-DISCOUNT"].Value); } }

            public static float BASE_LAMBDA { get { return float.Parse(Argument["BASE-LAMBDA"].Value); } }
            public static float PROB_LAMBDA { get { return float.Parse(Argument["PROB-LAMBDA"].Value); } }
            public static float SCORE_LAMBDA { get { return float.Parse(Argument["SCORE-LAMBDA"].Value); } }

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }

            public static int CRITIC_TYPE { get { return int.Parse(Argument["CRITIC-TYPE"].Value); } }
            public static string RESULT_FILE { get { return Argument["RESULT-FILE"].Value; } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("ENTITYNUM", new ParameterArgument("0", "Entity Number."));
                Argument.Add("RELATIONNUM", new ParameterArgument("0", "Relation Number."));

                Argument.Add("FOLDER", new ParameterArgument(string.Empty, "train/valid/test Path"));
                Argument.Add("TRAIN", new ParameterArgument(string.Empty, "Train Data."));
                Argument.Add("VALID", new ParameterArgument(string.Empty, "Valid Data."));
                Argument.Add("TEST", new ParameterArgument(string.Empty, "Test Data."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));
                Argument.Add("TRAIN-BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("TRAIN-SETTING", new ParameterArgument("0", "0:supervised  1:reinforcement"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));

                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("GUID-RATE", new ParameterArgument("1", "guided path rate."));
                Argument.Add("RAND-RATE", new ParameterArgument("10", "rand path rate."));

                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));
                Argument.Add("TNEG-R", new ParameterArgument("0", "designed neg reward"));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.9", "Reward discount"));

                Argument.Add("T-MAX-CLIP", new ParameterArgument("0", "Maximum T Clip"));
                Argument.Add("T-MIN-CLIP", new ParameterArgument("0", "Minimum T Clip"));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument(string.Empty, "Model Path"));
                Argument.Add("SEED-VERSION", new ParameterArgument("1", "0:old version of model; 1:new version of model"));

                Argument.Add("SCORE-PATH", new ParameterArgument(string.Empty, "Output Score File."));
                Argument.Add("GAMMA", new ParameterArgument("10", "Softmax Smooth Parameter."));

                Argument.Add("NORM-REWARD", new ParameterArgument("0", "normalize reward."));
                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));

                Argument.Add("TRAIN-DEBUG", new ParameterArgument("0", "0:use dev set as train; 1: use train set"));

                Argument.Add("PRE-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));


                Argument.Add("MCTS-NUM", new ParameterArgument("64", "Monto Carlo Tree Search Number"));

                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("RAND-TERM", new ParameterArgument("0.3", "Random termination gate."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("MEM-CLEAN", new ParameterArgument("1", "1: clean memory everytime."));
                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("CRITIC-TYPE", new ParameterArgument("0", "0:no critic; 1:critic baseline."));


                Argument.Add("RESULT-FILE", new ParameterArgument("tmp.result", "result file"));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_REINFORCE_WALK_LINK_PREDICTION_V2_PRED; } }

        public override void InitStartup(string fileName) {
            BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] addArgs)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(addArgs);
        }


        /// <summary>
        /// episodic memory structure.
        /// </summary>
        public class EpisodicMemory
        {
            Dictionary<string, Tuple<List<Tuple<float, float>>, int>> Mem = new Dictionary<string, Tuple<List<Tuple<float, float>>, int>>();
            public bool IsGlobal = true;
            int Time { get; set; }
            public EpisodicMemory(bool isGlobal)
            {
                IsGlobal = isGlobal;
                Time = 0;
            }
            public void Clear()
            {
                Mem.Clear();
                Time = 0;
            }
            /// <summary>
            /// memory index.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="actionCount"></param>
            /// <param name="topK"></param>
            /// <returns></returns>
            public List<Tuple<float, float>> Search(string key)
            {
                if (Mem.ContainsKey(key))
                {
                    return Mem[key].Item1;
                }
                else
                {
                    return null;
                }
            }

            public void Update(string key, int actid, int totalAct, float reward, float v = 1.0f)
            {
                float new_u = 0;
                float new_c = 0;

                // update memory.
                if (Mem.ContainsKey(key))
                {
                    float u = Mem[key].Item1[actid].Item1;
                    float c = Mem[key].Item1[actid].Item2;
                    new_u = c / (c + v) * u + v / (c + v) * reward;
                    new_c = c + v;
                }
                else
                {
                    Mem.Add(key, new Tuple<List<Tuple<float, float>>, int>(new List<Tuple<float, float>>(), Time));
                    for (int a = 0; a < totalAct; a++)
                    {
                        Mem[key].Item1.Add(new Tuple<float, float>(0, 0));
                    }
                    new_u = reward;
                    new_c = v;
                }
                Mem[key].Item1[actid] = new Tuple<float, float>(new_u, new_c);
            }

            public void UpdateTiming()
            {
                Time += 1;
            }
        }

        public class BanditAlg
        {
            /// <summary>
            /// Exp 0.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UniformRandomStrategy0(int dim, Random random)
            {
                return random.Next(dim);
            }

            /// <summary>
            /// Exp 1.
            /// </summary>
            /// <param name="actdim"></param>
            /// <param name="rand_term"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int CascadeRandomStrategy1(int actdim, float rand_term, Random random)
            {
                if (random.NextDouble() > rand_term)
                {
                    return actdim;
                }
                return random.Next(actdim);
            }

            /// <summary>
            /// Exp 2.
            /// </summary>
            /// <param name="prior"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int ThompasSampling(float[] prior, Random random)
            {
                int idx = Util.Sample(prior.ToArray(), random);
                return idx;
            }


            /// <summary>
            /// Exp 3. Standard UCB Bandit.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int UCB1Bandit(List<Tuple<float, float>> arms, int dim, float c, Random random)
            {
                if (arms == null)
                {
                    return UniformRandomStrategy0(dim, random);
                }

                float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 0.1f);
                List<float> v = new List<float>();
                foreach (Tuple<float, float> arm in arms)
                {
                    v.Add(arm.Item1 / (arm.Item2 + 0.1f) + c * (float)Math.Sqrt(log_total / (arm.Item2 + 0.1f)) + (float)random.NextDouble() * 0.0001f);
                }
                int idx = Util.MaximumValue(v.ToArray());
                return idx;
            }

            public static int PUCB(List<Tuple<float, float>> arms, List<float> prior, float mb, float c, float m, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float sum = prior.Sum() + mb * prior.Count;

                    float t = (float)arms.Select(i => i.Item2).Sum();

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float si = arms[i].Item1;
                        float xi = (arms[i].Item2 == 0 ? 1 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        float ci = si == 0 ? 0 : c * (float)Math.Sqrt(Math.Log(t) / si);
                        float mi = (t == 0 ? 1 : (float)Math.Sqrt(Math.Log(t) / t)) * m * sum / (prior[i] + mb);
                        float s = xi + ci - mi;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            /// <summary>
            /// Exp 4. PUCT.
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="c"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int AlphaGoZeroBandit(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(log_total) / (arms[i].Item2 + 1.0f) +
                                                        (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            public static int AlphaGoZeroBanditV2(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float total = (float)arms.Select(i => i.Item2).Sum();

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(total) / (arms[i].Item2 + 1.0f) +
                                                       (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }

            public static int AlphaGoZeroBanditV3(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.1f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum() + 1.0f);

                    for (int i = 0; i < arms.Count; i++)
                    {
                        float s = c * (float)Math.Pow(prior[i], BuilderParameters.PUCT_D) * (float)Math.Sqrt(log_total / (arms[i].Item2 + 1.0f)) +
                                                        (arms[i].Item2 == 0 ? 0 : arms[i].Item1 * 1.0f / (arms[i].Item2));
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                return maxI;
            }


            /// <summary>
            /// mb = 0.00001;
            /// c = exploration.
            /// m = 
            /// </summary>
            /// <param name="arms"></param>
            /// <param name="prior"></param>
            /// <param name="mb"></param>
            /// <param name="c"></param>
            /// <param name="m"></param>
            /// <param name="random"></param>
            /// <returns></returns>
            public static int PUCB2(List<Tuple<float, float>> arms, List<float> prior, float c, Random random)
            {
                int maxI = 0;
                float maxV = float.MinValue;

                if (arms == null)
                {
                    // select the max prior value.
                    for (int i = 0; i < prior.Count; i++)
                    {
                        float s = prior[i] + (float)random.NextDouble() * 0.001f;
                        if (s > maxV)
                        {
                            maxV = s;
                            maxI = i;
                        }
                    }
                }
                else
                {
                    float log_total = (float)Math.Log(arms.Select(i => i.Item2).Sum());
                    List<float> v = new List<float>();
                    foreach (Tuple<float, float> arm in arms)
                    {
                        v.Add(arm.Item1 / (arm.Item2 + 1.0f) + c * (float)Math.Sqrt(log_total / (arm.Item2)) + (float)random.NextDouble() * 0.0001f);
                    }
                    int idx = Util.MaximumValue(v.ToArray());
                }
                return maxI;
            }


        }

        public class GraphQueryData : BatchData
        {
            public List<int> RawSource = new List<int>();
            public List<Tuple<int, int>> RawQuery = new List<Tuple<int, int>>();

            public List<Tuple<List<int>, List<float>>> RawTarget = new List<Tuple<List<int>, List<float>>>();

            // the edge already exists in KB.
            public List<HashSet<int>> BlackTargets = new List<HashSet<int>>();

            public List<int> RawIndex = new List<int>();

            public int BatchSize { get { return RawQuery.Count; } }
            public int MaxBatchSize { get; set; }

            public List<StatusData> StatusPath = new List<StatusData>();
            public List<Tuple<int, int>> Results = new List<Tuple<int, int>>();

            /// <summary>
            /// max group number, and group size;
            /// </summary>
            /// <param name="maxGroupNum"></param>
            /// <param name="groupSize"></param>
            /// <param name="device"></param>
            public GraphQueryData(int maxBatchSize, DeviceType device)
            {
                MaxBatchSize = maxBatchSize;
            }

            public GraphQueryData(GraphQueryData data)
            {
                RawSource = data.RawSource;
                RawQuery = data.RawQuery;
                RawTarget = data.RawTarget;
                BlackTargets = data.BlackTargets;
                RawIndex = data.RawIndex;

                MaxBatchSize = data.MaxBatchSize;
            }

            public List<int> GetBatchIdxs(int b, int step)
            {
                List<int> r = new List<int>();
                for (int i = 0; i < StatusPath[step].BatchSize; i++)
                {
                    if (StatusPath[step].GetOriginalStatsIndex(i) == b)
                        r.Add(i);
                }
                return r;
            }
        }

        public class StatusData : BatchData
        {
            /// <summary>
            /// Raw Query.
            /// </summary>
            public GraphQueryData GraphQuery { get; set; }

            /// <summary>
            /// Node ID.
            /// </summary>
            public List<int> NodeID { get; set; }

            /// <summary>
            /// Termination Probability.
            /// </summary>
            public HiddenBatchData Term = null;

            /// <summary>
            /// Score for each instance.
            /// </summary>
            public HiddenBatchData Score = null;

            /// <summary>
            /// LogProbability to this node.
            /// </summary>
            List<float> LogProb { get; set; }

            /// <summary>
            /// MatchCandidate and MatchCandidateProb.
            /// </summary>
            public List<Tuple<int, int>> MatchCandidate = null;
            public SeqVectorData MatchCandidateProb = null;

            public List<int> PreSelActIndex = null;
            public List<int> PreStatusIndex = null;
            public int GetPreStatusIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else return PreStatusIndex[batchIdx];
            }
            public int GetOriginalStatsIndex(int batchIdx)
            {
                if (Step == 0) { return batchIdx; }
                else
                {
                    int b = GetPreStatusIndex(batchIdx);
                    return GraphQuery.StatusPath[Step - 1].GetOriginalStatsIndex(b);
                }
            }

            public List<string> StatusKey = null;
            public string GetStatusKey(int b)
            {
                if (Step == 0) return string.Format("N:{0}-R:{1}", GraphQuery.RawQuery[b].Item1, GraphQuery.RawQuery[b].Item2);
                else return StatusKey[b];
            }

            /// <summary>
            /// constrastive reward.
            /// </summary>
            public List<float> CR { get; set; }

            public int MaxBatchSize { get { return StateEmbed.MAX_BATCHSIZE; } }
            public int BatchSize { get { return StateEmbed.BatchSize; } }

            /// <summary>
            /// logprobability of the node.
            /// </summary>
            /// <param name="batchIdx"></param>
            /// <returns></returns>
            public float GetLogProb(int batchIdx)
            {
                if (Step == 0) { return 0; }
                else { return LogProb[batchIdx]; }
            }

            /// <summary>
            /// Log Termination Probability.
            /// </summary>
            /// <param name="b"></param>
            /// <param name="isT"></param>
            /// <returns></returns>
            public float LogPTerm(int b, bool isT)
            {
                if (isT) { return (float)Util.LogLogistial(Term.Output.Data.MemPtr[b]); }
                else { return (float)Util.LogNLogistial(Term.Output.Data.MemPtr[b]); }
            }

            public int GetActionDim(int b)
            {
                return GetActionEndIndex(b) - GetActionStartIndex(b);
            }

            public int GetActionStartIndex(int b)
            {
                return b == 0 ? 0 : MatchCandidateProb.SegmentIdx.MemPtr[b - 1];
            }
            public int GetActionEndIndex(int b)
            {
                return MatchCandidateProb.SegmentIdx.MemPtr[b];
            }

            /// <summary>
            /// Embedding of the State.
            /// </summary>
            public HiddenBatchData StateEmbed { get; set; }

            public int Step;

            public StatusData(GraphQueryData interData, List<int> nodeIndex, HiddenBatchData stateEmbed, DeviceType device) :
                this(interData, nodeIndex, null, null, null, null, stateEmbed, device)
            { }

            public StatusData(GraphQueryData interData,
                              List<int> nodeIndex, List<float> logProb, List<int> selectedAction, List<int> preStatusIndex, List<string> statusKey,
                              HiddenBatchData stateEmbed, DeviceType device)
            {
                GraphQuery = interData;
                GraphQuery.StatusPath.Add(this);

                NodeID = nodeIndex;
                StateEmbed = stateEmbed;

                LogProb = logProb;
                PreSelActIndex = selectedAction;
                PreStatusIndex = preStatusIndex;
                StatusKey = statusKey;
                //SelectedAction = selectedAction;

                CR = new List<float>();

                Step = GraphQuery.StatusPath.Count - 1;
            }
        }

        /// <summary>
        /// Sample Graph. 
        /// </summary>
        class SampleRunner : StructRunner
        {
            List<Tuple<int, int, List<int>, List<float>>> Graph { get; set; }
            public new GraphQueryData Output { get; set; }
            int MaxBatchSize { get; set; }
            DataRandomShuffling Shuffle { get; set; }
            Random random = new Random(DeepNet.BuilderParameters.RandomSeed + 1);

            public SampleRunner(List<Tuple<int, int, List<int>, List<float>>> graph, int maxBatchSize, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Graph = graph;
                MaxBatchSize = maxBatchSize;
                Shuffle = new DataRandomShuffling(Graph.Count, random);
                Output = new GraphQueryData(maxBatchSize, behavior.Device);
            }

            public override void Init()
            {
                IsTerminate = false;
                IsContinue = true;
                Shuffle.Init();
            }

            public override void Forward()
            {
                Output.RawQuery.Clear();
                Output.RawSource.Clear();
                Output.RawTarget.Clear();
                Output.BlackTargets.Clear();
                Output.RawIndex.Clear();

                int groupIdx = 0;

                while (groupIdx < MaxBatchSize)
                {
                    int idx = Behavior.RunMode == DNNRunMode.Train ? Shuffle.RandomNext() : Shuffle.OrderNext();
                    if (idx <= -1) { break; }

                    int srcId = Graph[idx].Item1;
                    int linkId = Graph[idx].Item2;

                    Output.RawQuery.Add(new Tuple<int, int>(srcId, linkId));
                    Output.RawSource.Add(srcId);
                    Output.RawIndex.Add(idx);
                    Output.RawTarget.Add(new Tuple<List<int>, List<float>>(Graph[idx].Item3, Graph[idx].Item4));

                    HashSet<int> blackTarget = null;

                    // black is empty.
                    if (Behavior.RunMode == DNNRunMode.Train)
                    {
                        blackTarget = DataPanel.knowledgeGraph.TrainNeighborHash[srcId].ContainsKey(linkId) ?
                                                DataPanel.knowledgeGraph.TrainNeighborHash[srcId][linkId] : new HashSet<int>();
                    }
                    else if (Behavior.RunMode == DNNRunMode.Predict)
                    {
                        blackTarget = DataPanel.knowledgeGraph.AllNeighborHash[srcId].ContainsKey(linkId) ?
                                                DataPanel.knowledgeGraph.AllNeighborHash[srcId][linkId] : new HashSet<int>();
                    }
                    Output.BlackTargets.Add(blackTarget);
                    groupIdx += 1;
                }
                if (groupIdx == 0) { IsTerminate = true; return; }
            }
        }

        class StatusEmbedRunner : CompositeNetRunner
        {
            public new List<Tuple<int, int>> Input { get; set; }

            public new HiddenBatchData Output { get; set; }

            EmbedStructure InNodeEmbed { get; set; }
            EmbedStructure LinkEmbed { get; set; }

            public StatusEmbedRunner(List<Tuple<int, int>> input, int maxBatchSize, EmbedStructure inNodeEmbed, EmbedStructure linkEmbed, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;
                InNodeEmbed = inNodeEmbed;
                LinkEmbed = linkEmbed;
                // concate of node embedding and relation embedding.
                Output = new HiddenBatchData(maxBatchSize, inNodeEmbed.Dim + linkEmbed.Dim, DNNRunMode.Train, behavior.Device);
            }

            public override unsafe void Forward()
            {
                InNodeEmbed.Embedding.SyncToCPU();
                LinkEmbed.Embedding.SyncToCPU();

                //BatchLinks.Clear();
                int batchSize = 0;
                while (batchSize < Input.Count)
                {
                    int srcId = Input[batchSize].Item1;
                    int linkId = Input[batchSize].Item2;

                    int bidx = batchSize;

                    if (BuilderParameters.N_EmbedDim > 0)
                    {
                        fixed(float * ap = &Output.Output.Data.MemPtr[bidx * Output.Dim])
                        fixed(float * bp = &InNodeEmbed.Embedding.MemPtr[srcId * InNodeEmbed.Dim])
                        {
                            SSElib.SSE_AddScaleEx(0, 1,  bp, bp, ap, InNodeEmbed.Dim);
                        }
                        //float a, float b, float * pa, float * pb, float * pd, int c);
                        
                        //FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim,
                        //                        InNodeEmbed.Embedding.MemPtr, srcId * InNodeEmbed.Dim, InNodeEmbed.Dim, 0, 1);
                    }

                    fixed(float * ap = &Output.Output.Data.MemPtr[bidx * Output.Dim+ InNodeEmbed.Dim])
                    fixed(float * bp = &InNodeEmbed.Embedding.MemPtr[linkId * LinkEmbed.Dim])
                    {
                        SSElib.SSE_AddScaleEx(0, 1,  bp, bp, ap, LinkEmbed.Dim);
                    }
                    //FastVector.Add_Vector(Output.Output.Data.MemPtr, bidx * Output.Dim + InNodeEmbed.Dim,
                    //                        LinkEmbed.Embedding.MemPtr, linkId * LinkEmbed.Dim, LinkEmbed.Dim, 0, 1);
                    batchSize += 1;
                }
                Output.BatchSize = batchSize;
                Output.Output.Data.SyncFromCPU(Output.BatchSize * Output.Dim);
            }

            public override void CleanDeriv()
            {
                ComputeLib.Zero(Output.Deriv.Data, Output.Dim * Output.BatchSize);
            }

            public override void Update()
            {
                Output.Deriv.Data.SyncToCPU(Output.Dim * Output.BatchSize);

                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncToCPU();

                for (int b = 0; b < Output.BatchSize; b++)
                {
                    int srcIdx = Input[b].Item1;
                    int linkIdx = Input[b].Item2;

                    if (BuilderParameters.N_EmbedDim > 0)
                    {
                        FastVector.Add_Vector(InNodeEmbed.EmbeddingOptimizer.Gradient.MemPtr, srcIdx * InNodeEmbed.Dim,
                                        Output.Deriv.Data.MemPtr, b * Output.Dim, InNodeEmbed.Dim, 1, 1);
                    }
                    FastVector.Add_Vector(LinkEmbed.EmbeddingOptimizer.Gradient.MemPtr, linkIdx * LinkEmbed.Dim,
                                    Output.Deriv.Data.MemPtr, b * Output.Dim + InNodeEmbed.Dim, LinkEmbed.Dim, 1, 1);
                }
                InNodeEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();
                LinkEmbed.EmbeddingOptimizer.Gradient.SyncFromCPU();

            }
        }

        class CandidateActionRunner : CompositeNetRunner
        {
            public new StatusData Input;

            public new List<Tuple<int, int>> Output = new List<Tuple<int, int>>();
            public BiMatchBatchData Match;

            /// <summary>
            /// Input : State.
            /// node and rel embed.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="nodeEmbed"></param>
            /// <param name="relEmbed"></param>
            /// <param name="behavior"></param>
            public CandidateActionRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                Match = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount
                }, behavior.Device);
            }

            //Random random = new Random();
            public override void Forward()
            {
                Output.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();
                int cursor = 0;
                for (int b = 0; b < Input.BatchSize; b++)
                {
                    int seedNode = Input.NodeID[b];

                    int qid = Input.GetOriginalStatsIndex(b); // GetQueryIndex(b);
                    //int rawSrc = Input.GraphQuery.RawQuery[qid].Item1;
                    //int rawR = Input.GraphQuery.RawQuery[qid].Item2;
                    //int rawTgt = Input.GraphQuery.RawTarget[qid];

                    int candidateNum = 0;
                    for (int nei = 0; nei < DataPanel.knowledgeGraph.TrainNeighborLink[seedNode].Count; nei++)
                    {
                        int lid = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2;

                        //int relid = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item1 > 0 ?
                        //    DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 + DataPanel.RelationNum :
                        //    DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item2 ;

                        int tgtNode = DataPanel.knowledgeGraph.TrainNeighborLink[seedNode][nei].Item3;
                        //if (seedNode == rawSrc && lid == rawR && tgtNode == rawTgt) { continue; }
                        //if (seedNode == rawTgt && relid == rawR && tgtNode == rawSrc) { continue; } 

                        if (lid == 450)
                        {
                            Console.WriteLine("Got U!!");
                        }

                        Output.Add(new Tuple<int, int>(tgtNode, lid));
                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
                        cursor += 1;
                        candidateNum += 1;
                    }

                    if (candidateNum == 0)
                    {
                        Output.Add(new Tuple<int, int>(seedNode, SampleRandom.Next(DataPanel.RelationNum)));
                        match.Add(new Tuple<int, int, float>(b, cursor, 1));
                        cursor += 1;
                    }
                }
                Match.SetMatch(match);
            }
        }

        static Random SampleRandom = new Random(DeepNet.BuilderParameters.RandomSeed);

        public class BasicPolicyRunner : CompositeNetRunner
        {
            public new StatusData Input { get; set; }

            /// <summary>
            /// MatchData.
            /// </summary>
            public BiMatchBatchData MatchPath = null;

            /// <summary>
            /// new Node Index.
            /// </summary>
            public List<int> NodeIndex { get; set; }

            /// <summary>
            /// next step log probability.
            /// </summary>
            public List<float> LogProb { get; set; }

            /// <summary>
            /// pre sel action and pre status. 
            /// </summary>
            public List<int> PreSelActIndex { get; set; }
            public List<int> PreStatusIndex { get; set; }

            public List<string> StatusKey { get; set; }

            public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                NodeIndex = new List<int>();
                LogProb = new List<float>();
                PreSelActIndex = new List<int>();
                PreStatusIndex = new List<int>();
                StatusKey = new List<string>();
            }
        }

        /// <summary>
        /// take action.
        /// </summary>
        class ActionSamplingRunner : BasicPolicyRunner
        {
            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public ActionSamplingRunner(StatusData input, RunnerBehavior behavior) : base(input, behavior)
            {
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
                NodeIndex.Clear();
                LogProb.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                StatusKey.Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                if (Input.Term != null) Input.Term.Output.SyncToCPU();

                int currentStep = Input.Step;
                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    string Qkey = Input.GetStatusKey(i);

                    float[] policy_pi = null;

                    int actionDim = e - s;
                    if (Input.Term != null)
                    {
                        policy_pi = new float[actionDim + 1];
                        Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, policy_pi, 0, actionDim);

                        float t = Util.Logistic(Input.Term.Output.Data.MemPtr[i]);
                        if (t >= BuilderParameters.T_MAX_CLIP) { t = BuilderParameters.T_MAX_CLIP; }
                        if (t <= BuilderParameters.T_MIN_CLIP) { t = BuilderParameters.T_MIN_CLIP; }
                        policy_pi[actionDim] = t;

                        for (int a = 0; a < actionDim; a++) { policy_pi[a] = policy_pi[a] * (1 - t); }
                    }
                    else
                    {
                        policy_pi = new float[actionDim];
                        Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, policy_pi, 0, actionDim);
                    }

                    int idx = 0;
                    float r = (float)SampleRandom.NextDouble();
                    // draw sample actions.
                    //if (r > BuilderParameters.RAND_RATE) {
                    idx = Util.Sample(policy_pi, SampleRandom);
                    //}
                    // draw random actions.
                    //else { idx = SampleRandom.Next(0, policy_pi.Length); }

                    int selectIdx = s + idx;

                    if (Input.Term != null && idx == actionDim)
                    {
                        Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, i));
                    }
                    else
                    {
                        PreSelActIndex.Add(selectIdx);
                        PreStatusIndex.Add(i);

                        string statusQ = string.Format("{0}-N:{1}-R:{2}", Qkey, Input.MatchCandidate[selectIdx].Item1, Input.MatchCandidate[selectIdx].Item2);
                        NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);

                        StatusKey.Add(statusQ);

                        float nonLogTerm = 0;
                        if (Input.Term != null) { nonLogTerm = Input.LogPTerm(i, false); }

                        float prob = Input.GetLogProb(i) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[idx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                        LogProb.Add(prob);

                        match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
                    }
                }
                MatchPath.SetMatch(match);
            }
        }

        class BeamSearchActionRunner : BasicPolicyRunner
        {
            int BeamSize = 1;

            bool IsLast = false;
            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchActionRunner(StatusData input, int beamSize, RunnerBehavior behavior, bool isLast = false) : base(input, behavior)
            {
                BeamSize = beamSize;
                IsLast = isLast;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);
            }

            public override void Forward()
            {
                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();
                if (Input.Term != null) Input.Term.Output.SyncToCPU();

                NodeIndex.Clear();
                LogProb.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                StatusKey.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                // for each beam.
                for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                {
                    List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);

                    MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                        float nonLogTerm = Input.LogPTerm(b, false);
                        //if (Input.Step > 0)
                        {
                            //nonLogTerm =
                            Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, b));
                        }

                        for (int t = s; t < e; t++)
                        {
                            float prob = Input.GetLogProb(b) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                            topKheap.push_pair(new Tuple<int, int>(b, t), prob);
                        }
                    }

                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();
                        match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));

                        NodeIndex.Add(Input.MatchCandidate[p.Key.Item2].Item1);
                        LogProb.Add(p.Value);

                        PreStatusIndex.Add(p.Key.Item1);
                        PreSelActIndex.Add(p.Key.Item2);

                        string statusQ = string.Format("{0}-N:{1}-R:{2}", Input.GetStatusKey(p.Key.Item1), Input.MatchCandidate[p.Key.Item2].Item1, Input.MatchCandidate[p.Key.Item2].Item2);

                        StatusKey.Add(statusQ);
                    }
                }
                MatchPath.SetMatch(match);
            }
        }

        class MCTSActionSamplingRunner : BasicPolicyRunner
        {
            int lineIdx = 0;
            List<EpisodicMemory> Memory { get; set; }
            //Random random = new Random(21);
            public int MCTSIdx { get; set; }
            public override void Init()
            {
                lineIdx = 0;
            }
            bool IsLast = false;
            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSActionSamplingRunner(StatusData input, int mctsIdx, List<EpisodicMemory> memory, RunnerBehavior behavior, bool isLast = false) : base(input, behavior)
            {
                Memory = memory;
                MCTSIdx = mctsIdx;
                IsLast = isLast;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * DataPanel.MaxNeighborCount,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
                NodeIndex.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                LogProb.Clear();
                StatusKey.Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                if (Input.Term != null) Input.Term.Output.SyncToCPU();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                int currentStep = Input.Step;
                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    int selectIdx = s;

                    string Qkey = Input.GetStatusKey(i);

                    //key = key + "-" + 
                    List<Tuple<float, float>> m = Memory[0].Search(Qkey);
                    float[] prior_r = null;

                    int actionDim = e - s;
                    //if (Input.Term != null)
                    {
                        prior_r = new float[actionDim + 1];
                        Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                        float t = Util.Logistic(Input.Term.Output.Data.MemPtr[i]);
                        if (t >= BuilderParameters.T_MAX_CLIP) { t = BuilderParameters.T_MAX_CLIP; }
                        if (t <= BuilderParameters.T_MIN_CLIP) { t = BuilderParameters.T_MIN_CLIP; }
                        prior_r[actionDim] = t;
                        for (int a = 0; a < actionDim; a++) { prior_r[a] = prior_r[a] * (1 - t); }
                    }
                    //else
                    //{
                    //    prior_r = new float[actionDim];
                    //    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);
                    //}

                    //if (m != null && actionDim != m.Count) { throw new Exception(string.Format("the dimension doesn't match {0}, {1}", actionDim, m.Count)); }
                    //int idx = BanditAlg.UCBBandit_Sample(m, prior_r, 2, random);
                    //int idx = BanditAlg.AlphaGoZeroBandit(m, prior_r.ToList(), 2, random);

                    int idx = 0;

                    if (IsLast)
                    {
                        idx = actionDim;
                    }
                    else
                    {
                        int strategy = BuilderParameters.EStrategy(MCTSIdx);
                        //Console.WriteLine("MCTS Index {0}, strategy {1}", MCTSIdx, strategy);
                        switch (strategy)
                        {
                            case 0: idx = BanditAlg.UniformRandomStrategy0(prior_r.Length, SampleRandom); break;
                            case 1:
                                //if (Input.Term == null)
                                //{
                                //    idx = BanditAlg.UniformRandomStrategy0(prior_r.Length, SampleRandom);
                                //}
                                //else
                                {
                                    idx = BanditAlg.CascadeRandomStrategy1(actionDim, BuilderParameters.RAND_TERM, SampleRandom);
                                }
                                break;
                            case 2: idx = BanditAlg.ThompasSampling(prior_r, SampleRandom); break;
                            case 3: idx = BanditAlg.UCB1Bandit(m, prior_r.Length, BuilderParameters.UCB1_C, SampleRandom); break;
                            case 4: idx = BanditAlg.AlphaGoZeroBandit(m, prior_r.ToList(), BuilderParameters.PUCT_C, SampleRandom); break;
                            case 5: idx = BanditAlg.AlphaGoZeroBanditV2(m, prior_r.ToList(), BuilderParameters.PUCT_C, SampleRandom); break;
                            case 6: idx = BanditAlg.PUCB(m, prior_r.ToList(), BuilderParameters.PUCB_B, BuilderParameters.PUCB_C, BuilderParameters.PUCB_M, SampleRandom); break;
                        }
                    }
                    //int idx = BanditAlg.ThompasSampling(m, prior_r.ToList(), 2, random);
                    // sample to termination.
                    if (idx == actionDim)
                    {
                        Input.GraphQuery.Results.Add(new Tuple<int, int>(Input.Step, i));
                    }
                    else
                    {
                        selectIdx = s + idx;

                        PreSelActIndex.Add(selectIdx);
                        PreStatusIndex.Add(i);

                        string statusQ = string.Format("{0}-N:{1}-R:{2}", Qkey, Input.MatchCandidate[selectIdx].Item1, Input.MatchCandidate[selectIdx].Item2);
                        StatusKey.Add(statusQ);

                        NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);

                        //float f = prior_r[idx];
                        //if (f <= Util.SmallEpsilon) { f = Util.SmallEpsilon; }

                        //float prob = Input.GetLogProb(i) + (float)Math.Log(f); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                        //LogProb.Add(prob);

                        float nonLogTerm = 0;
                        if (Input.Term != null) { nonLogTerm = Input.LogPTerm(i, false); }

                        float prob = Input.GetLogProb(i) + nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);
                        LogProb.Add(prob);

                        match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
                    }
                }
                MatchPath.SetMatch(match);
                lineIdx += Input.MatchCandidateProb.Segment;
            }
        }

        // update reward feedback 
        class RewardFeedbackRunner : StructRunner
        {
            new GraphQueryData Input { get; set; }
            List<EpisodicMemory> Memory { get; set; }

            float PosMean = 0;
            float NegMean = 0;
            int PosNum = 0;
            int NegNum = 0;

            int Epoch = 0;
            public override void Init()
            {
                Epoch = 0;
                PosMean = 0;
                PosNum = 0;

                NegMean = 0;
                NegNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosMean / (PosNum + 0.000001f));
                Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegMean / (NegNum + 0.000001f));

            }

            public RewardFeedbackRunner(GraphQueryData input, List<EpisodicMemory> memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                // calculate termination probability.
                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    //if (Input.StatusPath[t].Term != null)
                    {
                        ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.BatchSize, 1);
                        ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.BatchSize, 1 - Util.GPUEpsilon, Util.GPUEpsilon);
                        Input.StatusPath[t].Term.Output.Data.SyncToCPU();
                        Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
                    }
                    //if (Input.StatusPath[t].MatchCandidateProb != null)
                    {
                        Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
                    }
                    //if (Input.StatusPath[t].Score != null)
                    {
                        Input.StatusPath[t].Score.Output.Data.SyncToCPU();
                        Array.Clear(Input.StatusPath[t].Score.Deriv.Data.MemPtr, 0, Input.StatusPath[t].Score.Output.Data.EffectiveSize);
                    }
                }

                // add last step states.
                //if (Input.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
                //{
                //for (int b = 0; b < Input.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
                //{
                //    Input.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
                //}
                //}

                for (int i = 0; i < Input.Results.Count; i++)
                {
                    int t = Input.Results[i].Item1;
                    int b = Input.Results[i].Item2;
                    int origialB = Input.StatusPath[t].GetOriginalStatsIndex(b);
                    int predId = Input.StatusPath[t].NodeID[b];

                    List<int> targetIdx = Input.RawTarget[origialB].Item1;
                    List<float> targetLabel = Input.RawTarget[origialB].Item2;

                    float feedback_v = 0;

                    float estimate_v = Util.Logistic(Input.StatusPath[t].Score.Output.Data.MemPtr[b]);// Math.Max(0, );
                                                                    
                    { feedback_v = estimate_v; }

                    feedback_v = feedback_v * BuilderParameters.UPDATE_R_DISCOUNT;
                    float v = 1 * BuilderParameters.UPDATE_R_DISCOUNT;

                    StatusData st = Input.StatusPath[t];
                    string key = st.GetStatusKey(b);
                    //if (t != BuilderParameters.MAX_HOP)
                    //{
                    //float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t);
                    Memory[0].Update(key, st.GetActionDim(b), st.GetActionDim(b) + 1, feedback_v, v);
                    //}

                    for (int pt = t - 1; pt >= 0; pt--)
                    {
                        StatusData pst = Input.StatusPath[pt];
                        int pb = st.GetPreStatusIndex(b);
                        string pkey = pst.GetStatusKey(pb);

                        int sidx = st.PreSelActIndex[b] - pst.GetActionStartIndex(pb);
                        float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);
                        Memory[0].Update(pkey, sidx, pst.GetActionDim(pb) + 1, feedback_v * discount, v * discount);

                        st = pst;
                        b = pb;
                    }
                }
                Memory[0].UpdateTiming();
            }
        }

        public class MCTSResult
        {
            public List<int> OriginalBatchIndex = new List<int>();
            public List<int> PredIndex = new List<int>();
            public List<Tuple<List<int>, List<float>>> RawTarget = new List<Tuple<List<int>, List<float>>>();
            public List<float> Score = new List<float>();
            public List<int> RawIndex = new List<int>();
            public List<int> TermStep = new List<int>();
            public List<string> PredPath = new List<string>();
            public void Clear()
            {
                OriginalBatchIndex.Clear();
                PredIndex.Clear();
                RawTarget.Clear();
                Score.Clear();
                RawIndex.Clear();
                TermStep.Clear();
            }

            public MCTSResult(GraphQueryData data)
            {
                for (int i = 0; i < data.Results.Count; i++)
                {
                    int t = data.Results[i].Item1;
                    int b = data.Results[i].Item2;
                    int origialB = data.StatusPath[t].GetOriginalStatsIndex(b);
                    int predId = data.StatusPath[t].NodeID[b];

                    float v = BuilderParameters.PROB_LAMBDA * (data.StatusPath[t].GetLogProb(b) + (float)Math.Log(data.StatusPath[t].Term.Output.Data.MemPtr[b]));

                    float ms = Util.Logistic(data.StatusPath[t].Score.Output.Data.MemPtr[b]);

                    float s = (float)Math.Log(ms + BuilderParameters.BASE_LAMBDA);
                    v = v + s * BuilderParameters.SCORE_LAMBDA;

                    string mkey = string.Format("Term {0}", t);
                    int rawIdx = data.RawIndex[origialB];

                    OriginalBatchIndex.Add(origialB);
                    PredIndex.Add(predId);
                    RawTarget.Add(data.RawTarget[origialB]);
                    RawIndex.Add(rawIdx);

                    Score.Add(v);
                    TermStep.Add(t);

                    PredPath.Add(data.StatusPath[t].GetStatusKey(b));
                }
                data.Results.Clear();
            }
        }

        class MAPPredictionV4Runner : StructRunner
        {
            float MAP = 0;
            int Iteration = 0;
            int SmpIdx = 0;
            List<MCTSResult> Results { get; set; }
            GraphQueryData Query { get; set; }
            EpisodicMemory Memory { get; set; }
            int nonHit = 0;
            int effectNonHit = 0;
            StreamWriter resultWriter = null;
            public MAPPredictionV4Runner(List<MCTSResult> results, GraphQueryData query, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Results = results;
                Memory = memory;

                Iteration = 0;
            }

            Dictionary<int, float> Success = new Dictionary<int, float>();
            Dictionary<int, float> StepSuccess = new Dictionary<int, float>();

            public override void Init()
            {
                Success.Clear();
                StepSuccess.Clear();

                SmpIdx = 0;
                MAP = 0;
                nonHit = 0;
                effectNonHit = 0;

                resultWriter = new StreamWriter(BuilderParameters.RESULT_FILE);
            }
            public void Report()
            {
                Logger.WriteLog("Sample Idx {0}", SmpIdx);
                Logger.WriteLog(string.Format("MAP {0}", MAP / SmpIdx));
                Logger.WriteLog("nonhit {0}", nonHit);
                Logger.WriteLog("effectNonhit {0}", effectNonHit);

            }

            public override void Complete()
            {
                Logger.WriteLog("Final Report!");
                {
                    Report();
                }
                resultWriter.Close();
                foreach (KeyValuePair<int, float> i in StepSuccess)
                {
                    Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
                }
                int sc = Success.Where(i => i.Value > 0).Count();
                int t = Success.Count;
                Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
                Iteration += 1;
            }
            public override void Forward()
            {
                Dictionary<int, float>[] scoreDict = new Dictionary<int, float>[Query.BatchSize];
                for (int i = 0; i < Query.BatchSize; i++)
                {
                    scoreDict[i] = new Dictionary<int, float>();
                }


                Dictionary<int, List<Tuple<string, float, float>>> resultPath = new Dictionary<int, List<Tuple<string, float, float>>>();
                Dictionary<int, Tuple<string, float, float>> selectPath = new Dictionary<int, Tuple<string, float, float>>();


                // add results.
                for (int m = 0; m < Results.Count; m++)
                {
                    for (int i = 0; i < Results[m].OriginalBatchIndex.Count; i++)
                    {
                        //int t = Results[m].Results[i].Item1;
                        //int b = Results[m].Results[i].Item2;
                        int origialB = Results[m].OriginalBatchIndex[i]; // StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = Results[m].PredIndex[i];

                        //float v = Results[m].StatusPath[t].GetLogProb(b);
                        //if (t != BuilderParameters.MAX_HOP)
                        //{ v = v + (float)Math.Log(Query[m].StatusPath[t].Term.Output.Data.MemPtr[b]); }// LogPTerm(b, true); }
                        //float s = (float)Math.Log(Query[m].StatusPath[t].Score.Output.Data.MemPtr[b] + BuilderParameters.BASE_LAMBDA);
                        //v = v + s * BuilderParameters.MSE_LAMBDA;

                        float v = Results[m].Score[i];

                        if (!scoreDict[origialB].ContainsKey(predId))
                        {
                            scoreDict[origialB][predId] = v;
                        }
                        else
                        {
                            scoreDict[origialB][predId] = Util.LogAdd(scoreDict[origialB][predId], v);
                        }

                        HashSet<int> posIdx = new HashSet<int>();

                        for (int kk = 0; kk < Results[m].RawTarget[i].Item1.Count; kk++)
                        {
                            if (Results[m].RawTarget[i].Item2[kk] > 0)
                                posIdx.Add(Results[m].RawTarget[i].Item1[kk]);
                        }

                        float true_v = 0;
                        if (posIdx.Contains(predId)) true_v = 1;

                        int rawIdx = Results[m].RawIndex[i];
                        if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
                        Success[rawIdx] += true_v;

                        int t = Results[m].TermStep[i];

                        if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = 0;
                        StepSuccess[t] += true_v;


                        if (!resultPath.ContainsKey(origialB))
                        {
                            resultPath.Add(origialB, new List<Tuple<string, float, float>>());
                            selectPath.Add(origialB, null);
                        }


                        string path = Results[m].PredPath[i];

                        Tuple<string, float, float> currentPath = new Tuple<string, float, float>(path, v, true_v);
                        resultPath[origialB].Add(currentPath);

                        if (selectPath[origialB] == null || selectPath[origialB].Item2 < v)
                        {
                            selectPath[origialB] = currentPath;
                        }
                    }
                }


                {
                    for (int g = 0; g < Query.BatchSize; g++)
                    {
                        int gidx = g;

                        resultWriter.WriteLine(string.Format("{0}->{1}->{2}", selectPath[gidx].Item1, selectPath[gidx].Item2, selectPath[gidx].Item3));
                        resultWriter.WriteLine(string.Join("\t", resultPath[gidx].Select(gs => string.Format("{0}->{1}->{2}", gs.Item1, gs.Item2, gs.Item3))));
                        resultWriter.WriteLine();
                    }
                }



                for (int g = 0; g < Query.BatchSize; g++)
                {
                    int relation = Query.RawQuery[g].Item2;
                    //Dictionary<int, float> target = Query.RawTarget[g];
                    HashSet<int> blackTargets = Query.BlackTargets[g];

                    Dictionary<int, float> score = scoreDict[g];
                    List<Tuple<float, float>> combine = new List<Tuple<float, float>>();

                    int posNum = Query.RawTarget[g].Item2.Where(m => m > 0).Count();
                    bool isGood = posNum == Query.RawTarget[g].Item2.Count || posNum == 0;

                    for(int i=0;i<Query.RawTarget[g].Item1.Count; i++) // each (int mk in target.Keys)
                    {
                        int mk = Query.RawTarget[g].Item1[i];
                        float label = Query.RawTarget[g].Item2[i];

                        float s = float.MinValue;
                        if (score.ContainsKey(mk)) s = score[mk];
                        combine.Add(new Tuple<float, float>(label, s));

                        if (label > 0 && !score.ContainsKey(mk))
                        {
                            nonHit += 1;
                        }
                        if (label > 0 && !score.ContainsKey(mk) && !isGood)
                        {
                            effectNonHit += 1;
                        }
                    }
                    var sortD = combine.OrderByDescending(pair => pair.Item2);

                    float ap = 0;

                    int idx = 0;
                    int hit = 0;
                    foreach (Tuple<float, float> mk in sortD)
                    {
                        idx += 1;
                        if (mk.Item1 > 0)
                        {
                            hit += 1;
                            ap = ap + hit * 1.0f / idx;
                        }
                    }
                    ap = isGood ? 1 : ap / (hit + 0.000000001f);
                    MAP += ap;
                }

                SmpIdx += Query.BatchSize;

                Memory.Clear();
            }
        }

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure InNodeEmbed { get; set; }
            public EmbedStructure InRelEmbed { get; set; }

            public EmbedStructure CNodeEmbed { get; set; }
            public EmbedStructure CRelEmbed { get; set; }

            public DNNStructure SrcDNN { get; set; }
            public DNNStructure TgtDNN { get; set; }

            public LayerStructure AttEmbed { get; set; }
            public DNNStructure TermDNN { get; set; }
            public DNNStructure ScoreDNN { get; set; }

            public GRUCell GruCell { get; set; }

            public NeuralWalkerModel(int nodeDim, int relDim, DeviceType device)
            {
                InNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                InRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));
                CNodeEmbed = AddLayer(new EmbedStructure(DataPanel.EntityNum, nodeDim, device));// DeviceType.CPU_FAST_VECTOR));
                CRelEmbed = AddLayer(new EmbedStructure(DataPanel.RelationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

                int StateDim = nodeDim + relDim;

                SrcDNN = AddLayer(new DNNStructure(StateDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                TgtDNN = AddLayer(new DNNStructure(StateDim, BuilderParameters.DNN_DIMS,
                                           BuilderParameters.DNN_DIMS.Select(i => A_Func.Tanh).ToArray(),
                                           BuilderParameters.DNN_DIMS.Select(i => false).ToArray(),
                                           device));

                AttEmbed = AddLayer(new LayerStructure(BuilderParameters.DNN_DIMS.Last(), 1, A_Func.Linear, N_Type.Fully_Connected, 1, 0, false, device));

                TermDNN = AddLayer(new DNNStructure(StateDim + BuilderParameters.DNN_DIMS.Last(), BuilderParameters.T_NET, BuilderParameters.T_AF,
                                         BuilderParameters.T_NET.Select(i => true).ToArray(), device));

                ScoreDNN = AddLayer(new DNNStructure(StateDim + BuilderParameters.DNN_DIMS.Last(), BuilderParameters.S_NET, BuilderParameters.S_AF,
                                         BuilderParameters.S_NET.Select(i => true).ToArray(), device));

                GruCell = AddLayer(new GRUCell(StateDim + BuilderParameters.DNN_DIMS.Last(), StateDim, device));
            }
            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                InNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                InRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

                CNodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                CRelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
                TgtDNN = (DNNStructure)DeserializeNextModel(reader, device);

                AttEmbed = (LayerStructure)DeserializeNextModel(reader, device);
                TermDNN = (DNNStructure)DeserializeNextModel(reader, device);

                //if (BuilderParameters.SEED_VERSION == 1)
                //{
                ScoreDNN = (DNNStructure)DeserializeNextModel(reader, device);
                //}
                //else
                //{
                //    ScoreDNN = AddLayer(new DNNStructure(BuilderParameters.N_EmbedDim + BuilderParameters.R_EmbedDim, BuilderParameters.S_NET, BuilderParameters.S_AF,
                //                         BuilderParameters.S_NET.Select(i => true).ToArray(), device));
                //}
                GruCell = (GRUCell)DeserializeNextModel(reader, device);

            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                //InNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //InRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(InRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                //CNodeEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CNodeEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });
                //CRelEmbed.EmbeddingOptimizer = new SparseSGDOptimizer(CRelEmbed.Embedding, OptimizerParameters.LearnRate, VecNormType.L2BallNorm, new RunnerBehavior() { Device = DeviceType.CPU_FAST_VECTOR });

                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }

        class MemoryScheduleRunner : StructRunner
        {
            EpisodicMemory GMemory { get; set; }
            EpisodicMemory LMemory { get; set; }
            public new List<EpisodicMemory> Output { get; set; }
            public MemoryScheduleRunner(EpisodicMemory globalMemory, EpisodicMemory localMemory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                GMemory = globalMemory;
                LMemory = localMemory;
                Output = new List<EpisodicMemory>();
            }
            int Epoch = 0;
            public override void Complete()
            {
                Epoch += 1;
            }
            public override void Forward()
            {
                Output.Clear();
                if (Behavior.RunMode == DNNRunMode.Train && SampleRandom.NextDouble() < BuilderParameters.Epsilon(Epoch)) { Output.Add(GMemory); }
                else { LMemory.Clear(); Output.Add(LMemory); }
                //Epoch += 1;
            }
        }

        public static ComputationGraph BuildComputationGraph(List<Tuple<int, int, List<int>, List<float>>> graph, int batchSize,
                                                             NeuralWalkerModel model, EpisodicMemory gmemory, EpisodicMemory tmpMemory, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            // sample a list of tuplet from the graph.
            SampleRunner SmpRunner = new SampleRunner(graph, batchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            MemoryScheduleRunner memRunner = new MemoryScheduleRunner(gmemory, tmpMemory, Behavior);
            cg.AddRunner(memRunner);
            List<EpisodicMemory> aMem = memRunner.Output;

            // get the embedding from the query data.
            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
            cg.AddRunner(statusEmbedRunner);

            List<GraphQueryData> Queries = new List<GraphQueryData>();

            // MCTS path number.
            for (int j = 0; j < (Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.MCTS_NUM : 1); j++)
            {
                GraphQueryData newQuery = new GraphQueryData(interface_data);
                StatusData status = new StatusData(newQuery, newQuery.RawSource, statusEmbedRunner.Output, Behavior.Device);

                #region multi-hop expan
                // travel four steps in the knowledge graph.
                for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
                {
                    // given status, obtain the match. miniBatch * maxNeighbor number.
                    CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
                    cg.AddRunner(candidateActionRunner);

                    // miniMatch * maxNeighborNumber.
                    StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, model.CNodeEmbed, model.CRelEmbed, Behavior);
                    cg.AddRunner(candEmbedRunner);

                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                    cg.AddRunner(srcHiddenRunner);

                    DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
                    cg.AddRunner(candHiddenRunner);

                    SeqDenseBatchData neiSeqData = new SeqDenseBatchData(new SequenceDataStat() { FEATURE_DIM = candHiddenRunner.Output.Dim, MAX_BATCHSIZE = status.MaxBatchSize, MAX_SEQUENCESIZE = candHiddenRunner.Output.MAX_BATCHSIZE },
                                                                             candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, candHiddenRunner.Output.Output.Data, candHiddenRunner.Output.Deriv.Data, Behavior.Device);

                    // neighbor dataset.
                    HiddenBatchData neiData = (HiddenBatchData)cg.AddRunner(new MaxPoolingRunner<SeqDenseBatchData>(neiSeqData, Behavior));

                    // alignment miniBatch * miniBatch * neighbor.
                    VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
                                                                                candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
                    cg.AddRunner(attentionRunner);

                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
                                                                                status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
                                                                                candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
                                                                                Behavior.Device), 1, Behavior, true);
                    cg.AddRunner(normAttRunner);

                    status.MatchCandidate = candidateActionRunner.Output;
                    status.MatchCandidateProb = normAttRunner.Output;

                    MatrixData newStateData = (MatrixData)cg.AddRunner(new MatrixConcateRunner(new List<MatrixData>() { new MatrixData(status.StateEmbed), new MatrixData(neiData) }, Behavior));

                    DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, new HiddenBatchData(newStateData), Behavior);
                    cg.AddRunner(termRunner);
                    status.Term = termRunner.Output;

                    DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, new HiddenBatchData(newStateData), Behavior) { name = "V_SCORE" };
                    cg.AddRunner(scoreRunner);
                    status.Score = scoreRunner.Output;

                    BasicPolicyRunner policyRunner = null;

                    policyRunner = new BeamSearchActionRunner(status, BuilderParameters.BeamSize, Behavior, i == BuilderParameters.MAX_HOP - 1);

                    cg.AddRunner(policyRunner);

                    if (i < BuilderParameters.MAX_HOP - 1)
                    {
                        #region take action.
                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(srcExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
                        cg.AddRunner(tgtExpRunner);

                        MatrixExpansionRunner neiExpRunner = new MatrixExpansionRunner(neiData, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(neiExpRunner);

                        HiddenBatchData concateData = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { neiExpRunner.Output, tgtExpRunner.Output }, Behavior));

                        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, concateData, Behavior);
                        cg.AddRunner(stateRunner);
                        #endregion.

                        StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                            policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, policyRunner.StatusKey, stateRunner.Output, Behavior.Device);
                        status = nextStatus;
                    }
                }
                #endregion.

                RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(newQuery, aMem, Behavior);
                cg.AddRunner(rewardFeedRunner);
                Queries.Add(newQuery);
            }

            {
                MAPPredictionV2Runner predV2Runner = new MAPPredictionV2Runner(Queries[0], Behavior);
                cg.AddRunner(predV2Runner);
            }
            cg.SetDelegateModel(model);
            return cg;
        }


        class MAPPredictionV2Runner : StructRunner
        {
            float MAP = 0;
            int Iteration = 0;
            int SmpIdx = 0;
            int nonHit = 0;
            int effectNonHit = 0;
            GraphQueryData Query { get; set; }

            Dictionary<int, float> Success = new Dictionary<int, float>();
            Dictionary<int, float> StepSuccess = new Dictionary<int, float>();

            public MAPPredictionV2Runner(GraphQueryData query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Iteration = 0;
            }

            public override void Init()
            {
                Success.Clear();
                StepSuccess.Clear();

                SmpIdx = 0;
                MAP = 0;

                nonHit = 0;
                effectNonHit = 0;
            }

            public void Report()
            {
                Logger.WriteLog("Sample Idx {0}", SmpIdx);
                Logger.WriteLog(string.Format("MAP {0}", MAP / SmpIdx));
                Logger.WriteLog("nonhit {0}", nonHit);
                Logger.WriteLog("effectNonhit {0}", effectNonHit);
            }

            public override void Complete()
            {
                Logger.WriteLog("Final Report!");
                {
                    Report();
                }

                foreach (KeyValuePair<int, float> i in StepSuccess)
                {
                    Logger.WriteLog("Step Success {0}, {1}", i.Key, i.Value);
                }
                int sc = Success.Where(i => i.Value > 0).Count();
                int t = Success.Count;
                Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);

                Iteration += 1;
            }
            public override void Forward()
            {
                Dictionary<int, float>[] scoreDict = new Dictionary<int, float>[Query.BatchSize];
                for (int i = 0; i < Query.BatchSize; i++)
                {
                    scoreDict[i] = new Dictionary<int, float>();
                }

                // add last step nodes.
                //if (Query.StatusPath.Count == BuilderParameters.MAX_HOP + 1)
                //{
                //    for (int b = 0; b < Query.StatusPath[BuilderParameters.MAX_HOP].BatchSize; b++)
                //    {
                //        Query.Results.Add(new Tuple<int, int>(BuilderParameters.MAX_HOP, b));
                //    }
                //}

                for (int t = Query.StatusPath.Count - 1; t >= 0; t--)
                {
                    if (Query.StatusPath[t].Term != null) { Query.StatusPath[t].Term.Output.Data.SyncToCPU(); }
                    if (Query.StatusPath[t].Score != null) { Query.StatusPath[t].Score.Output.Data.SyncToCPU(); }
                }

                // add results.
                for (int i = 0; i < Query.Results.Count; i++)
                {
                    int t = Query.Results[i].Item1;
                    int b = Query.Results[i].Item2;
                    int origialB = Query.StatusPath[t].GetOriginalStatsIndex(b);
                    int predId = Query.StatusPath[t].NodeID[b];

                    
                    float v =  (Query.StatusPath[t].GetLogProb(b) + (float)Query.StatusPath[t].LogPTerm(b, true));

                    //Dictionary<int, float> targetId = Query.RawTarget[origialB];

                    //float score = Util.Logistic(Query.StatusPath[t].Score.Output.Data.MemPtr[b]);
                    ////if (score >= 1) score = 1;
                    ////else if (score <= 0) score = 0;

                    //float s = (float)Math.Log(score + BuilderParameters.BASE_LAMBDA);
                    //v = v + s * BuilderParameters.SCORE_LAMBDA;

                    if (!scoreDict[origialB].ContainsKey(predId))
                    {
                        scoreDict[origialB][predId] = v;
                    }
                    else
                    {
                        scoreDict[origialB][predId] = Util.LogAdd(scoreDict[origialB][predId], v);
                    }

                    int rawIdx = Query.RawIndex[origialB];
                }


                for (int g = 0; g < Query.BatchSize; g++)
                {
                    int relation = Query.RawQuery[g].Item2;
                    //Dictionary<int, float> target = Query.RawTarget[g];
                    List<int> tgtIdx = Query.RawTarget[g].Item1;
                    List<float> tgtLabel = Query.RawTarget[g].Item2;

                    HashSet<int> blackTargets = Query.BlackTargets[g];

                    Dictionary<int, float> score = scoreDict[g];

                    int posNum = tgtLabel.Where(m => m > 0).Count();
                    bool isGood = posNum == tgtLabel.Count || posNum == 0;

                    List<Tuple<float, float>> combine = new List<Tuple<float, float>>();

                    for (int i = 0; i < tgtIdx.Count; i++) // each (int mk in target.Keys)
                    {
                        int mk = tgtIdx[i];
                        float l = tgtLabel[i];

                        float s = float.MinValue;
                        if (score.ContainsKey(mk)) s = score[mk];
                        combine.Add(new Tuple<float, float>(l, s));

                        if (l > 0 && !score.ContainsKey(mk))
                        {
                            nonHit += 1;
                        }
                        if (l > 0 && !score.ContainsKey(mk) && !isGood)
                        {
                            effectNonHit += 1;
                        }
                    }
                    var sortD = combine.OrderByDescending(pair => pair.Item2);

                    float ap = 0;

                    int idx = 0;
                    int hit = 0;
                    foreach (Tuple<float, float> mk in sortD)
                    {
                        idx += 1;
                        if (mk.Item1 > 0)
                        {
                            hit += 1;
                            ap = ap + hit * 1.0f / idx;
                        }
                    }
                    ap = isGood ? 1 : ap / (hit + 0.00001f);
                    MAP += ap;
                }

                SmpIdx += Query.BatchSize;
                Query.Results.Clear();
                //Report();
            }
        }



        public class MCTSPredRunner : CompositeNetRunner
        {
            int MCTS { get; set; }
            GraphQueryData RawInput { get; set; }
            HiddenBatchData Embed { get; set; }
            EpisodicMemory Memory { get; set; }
            new NeuralWalkerModel Model { get; set; }
            List<MCTSActionSamplingRunner> PolicyRunner = new List<MCTSActionSamplingRunner>();
            public List<MCTSResult> Results = new List<MCTSResult>();
            public MCTSPredRunner(GraphQueryData interface_data, HiddenBatchData embed, EpisodicMemory memory, NeuralWalkerModel model, RunnerBehavior behavior) : base(behavior)
            {
                MCTS = BuilderParameters.MCTS_NUM;

                List<EpisodicMemory> Mems = new List<EpisodicMemory>() { memory };
                RawInput = interface_data;
                Embed = embed;
                Memory = memory;

                Model = model;

                //GraphQueryData newQuery = new GraphQueryData(interface_data);
                StatusData status = new StatusData(RawInput, RawInput.RawSource, embed, Behavior.Device);

                #region multi-hop expan
                // travel four steps in the knowledge graph.
                for (int i = 0; i < BuilderParameters.MAX_HOP; i++)
                {
                    // given status, obtain the match. miniBatch * maxNeighbor number.
                    CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, Behavior);
                    LinkRunners.Add(candidateActionRunner);

                    // miniMatch * maxNeighborNumber.
                    StatusEmbedRunner candEmbedRunner = new StatusEmbedRunner(candidateActionRunner.Output, candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE,
                        model.CNodeEmbed, model.CRelEmbed, Behavior);
                    LinkRunners.Add(candEmbedRunner);

                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, status.StateEmbed, Behavior);
                    LinkRunners.Add(srcHiddenRunner);

                    DNNRunner<HiddenBatchData> candHiddenRunner = new DNNRunner<HiddenBatchData>(model.TgtDNN, candEmbedRunner.Output, Behavior);
                    LinkRunners.Add(candHiddenRunner);


                    SeqDenseBatchData neiSeqData = new SeqDenseBatchData(new SequenceDataStat() { FEATURE_DIM = candHiddenRunner.Output.Dim, MAX_BATCHSIZE = status.MaxBatchSize, MAX_SEQUENCESIZE = candHiddenRunner.Output.MAX_BATCHSIZE },
                                                                             candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, candHiddenRunner.Output.Output.Data, candHiddenRunner.Output.Deriv.Data, Behavior.Device);

                    // neighbor dataset.
                    MaxPoolingRunner<SeqDenseBatchData> neiPoolRunner = new MaxPoolingRunner<SeqDenseBatchData>(neiSeqData, Behavior);
                    LinkRunners.Add(neiPoolRunner);
                    HiddenBatchData neiData = neiPoolRunner.Output;


                    // alignment miniBatch * miniBatch * neighbor.
                    VecAlignmentRunner attentionRunner = new VecAlignmentRunner(new MatrixData(srcHiddenRunner.Output), new MatrixData(candHiddenRunner.Output),
                                                                                candidateActionRunner.Match, model.AttEmbed, Behavior, 0, A_Func.Rectified);
                    LinkRunners.Add(attentionRunner);

                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(new SeqVectorData(attentionRunner.Output.MAX_BATCHSIZE,
                                                                                status.MaxBatchSize, attentionRunner.Output.Output.Data, attentionRunner.Output.Deriv.Data,
                                                                                candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx,
                                                                                Behavior.Device), 1, Behavior, true);
                    LinkRunners.Add(normAttRunner);

                    status.MatchCandidate = candidateActionRunner.Output;
                    status.MatchCandidateProb = normAttRunner.Output;
                    // it will cause un-necessary unstable.


                    MatrixConcateRunner state_neiConcateRunner = new MatrixConcateRunner(new List<MatrixData>() { new MatrixData(status.StateEmbed), new MatrixData(neiData) }, Behavior);
                    LinkRunners.Add(state_neiConcateRunner);
                    MatrixData newStateData = state_neiConcateRunner.Output;

                    DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, new HiddenBatchData(newStateData), Behavior);
                    LinkRunners.Add(termRunner);
                    status.Term = termRunner.Output;

                    DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, new HiddenBatchData(newStateData), Behavior);
                    LinkRunners.Add(scoreRunner);
                    status.Score = scoreRunner.Output;

                    //BasicPolicyRunner PolicyRunner = null;

                    // select the most promising state.
                    MCTSActionSamplingRunner tmpPolicyRunner = new MCTSActionSamplingRunner(status, -1, Mems, Behavior, i == BuilderParameters.MAX_HOP - 1);
                    LinkRunners.Add(tmpPolicyRunner);

                    PolicyRunner.Add(tmpPolicyRunner);

                    if (i < BuilderParameters.MAX_HOP - 1)
                    {
                        #region version 2 of action selection.
                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, tmpPolicyRunner.MatchPath, 1, Behavior);
                        LinkRunners.Add(srcExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, tmpPolicyRunner.MatchPath, 2, Behavior);
                        LinkRunners.Add(tgtExpRunner);

                        MatrixExpansionRunner neiExpRunner = new MatrixExpansionRunner(neiData, tmpPolicyRunner.MatchPath, 1, Behavior);
                        LinkRunners.Add(neiExpRunner);

                        EnsembleMatrixRunner enseRunner = new EnsembleMatrixRunner(new List<HiddenBatchData>() { neiExpRunner.Output, tgtExpRunner.Output }, Behavior);
                        LinkRunners.Add(enseRunner);
                        HiddenBatchData concateData = enseRunner.Output;


                        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, concateData, Behavior);
                        LinkRunners.Add(stateRunner);
                        #endregion.

                        StatusData nextStatus = new StatusData(status.GraphQuery, tmpPolicyRunner.NodeIndex, tmpPolicyRunner.LogProb,
                            tmpPolicyRunner.PreSelActIndex, tmpPolicyRunner.PreStatusIndex, tmpPolicyRunner.StatusKey, stateRunner.Output, Behavior.Device);

                        //DNNRunner<HiddenBatchData> termRunner = new DNNRunner<HiddenBatchData>(model.TermDNN, nextStatus.StateEmbed, Behavior);
                        //LinkRunners.Add(termRunner);
                        //nextStatus.Term = termRunner.Output;

                        //DNNRunner<HiddenBatchData> scoreRunner = new DNNRunner<HiddenBatchData>(model.ScoreDNN, nextStatus.StateEmbed, Behavior);
                        //LinkRunners.Add(scoreRunner);
                        //nextStatus.Score = scoreRunner.Output;

                        status = nextStatus;
                    }
                }
                RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(RawInput, Mems, Behavior);
                LinkRunners.Add(rewardFeedRunner);
                #endregion.   
            }

            // only works for prediction. in the prediction, things are changed.
            public override void Forward()
            {
                Results.Clear();
                for (int i = 0; i < MCTS; i++)
                {
                    foreach (MCTSActionSamplingRunner runner in PolicyRunner)
                        runner.MCTSIdx = i;
                    base.Forward();
                    Results.Add(new MCTSResult(RawInput));
                }
            }
        }

        public static ComputationGraph BuildPredComputationGraph(List<Tuple<int, int, List<int>, List<float>>> graph, int batchSize,
                                                             NeuralWalkerModel model, EpisodicMemory memory, RunnerBehavior Behavior)
        {
            ComputationGraph cg = new ComputationGraph();

            // sample a list of tuplet from the graph.
            SampleRunner SmpRunner = new SampleRunner(graph, batchSize, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            // get the embedding from the query data.
            StatusEmbedRunner statusEmbedRunner = new StatusEmbedRunner(interface_data.RawQuery, interface_data.MaxBatchSize, model.InNodeEmbed, model.InRelEmbed, Behavior);
            cg.AddRunner(statusEmbedRunner);

            MCTSPredRunner predRunner = new MCTSPredRunner(interface_data, statusEmbedRunner.Output, memory, model, Behavior);
            cg.AddRunner(predRunner);

            MAPPredictionV4Runner predV4Runner = new MAPPredictionV4Runner(predRunner.Results, interface_data, memory, Behavior);
            cg.AddRunner(predV4Runner);
            cg.SetDelegateModel(model);
            return cg;
        }


        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);

            EpisodicMemory tmpMemory = new EpisodicMemory(false);

            ComputationGraph predCG = null;
            if (BuilderParameters.SCORE_TYPE == 2)
            {
                predCG = BuildPredComputationGraph(DataPanel.knowledgeGraph.SignedTestV2, BuilderParameters.TestMiniBatchSize, model, tmpMemory,
                                new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            }
            else if(BuilderParameters.SCORE_TYPE == 1)
            {
                predCG = BuildComputationGraph(DataPanel.knowledgeGraph.SignedTestV2, BuilderParameters.TestMiniBatchSize, model, tmpMemory, tmpMemory,
                                new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });
            }
            predCG.Execute();
            Logger.CloseLog();
        }

        /// <summary>
        /// load data.
        /// </summary>
        public class DataPanel
        {
            public static RelationGraphData knowledgeGraph;

            public static int EntityNum { get { return knowledgeGraph.EntityNum; } }
            public static int RelationNum { get { return knowledgeGraph.RelationNum; } }
            public static int MaxNeighborCount { get { return knowledgeGraph.TrainNeighborLink.Select(i => i.Value.Count).Max(); } }

            public static void Init()
            {
                knowledgeGraph = new RelationGraphData();
                knowledgeGraph.EntityNum = BuilderParameters.EntityNum;
                knowledgeGraph.RelationNum = BuilderParameters.RelationNum;

                knowledgeGraph.Train = knowledgeGraph.LoadGraphV4(BuilderParameters.TrainData);
                knowledgeGraph.SignedValidV2 = knowledgeGraph.LoadSignedGraphV2(BuilderParameters.ValidData);
                knowledgeGraph.SignedTestV2 = knowledgeGraph.LoadSignedGraphV2(BuilderParameters.TestData);

                Console.WriteLine("Entity Number {0}, Relation Number {1}", knowledgeGraph.EntityNum, knowledgeGraph.RelationNum);
            }
        }
    }
}
