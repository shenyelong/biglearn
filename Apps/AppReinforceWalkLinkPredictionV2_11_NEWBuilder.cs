﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BigLearn;
using Newtonsoft.Json;
using System.Diagnostics;
namespace BigLearn.DeepNet
{
    public class AppReinforceWalkLinkPredictionV2_11_NEWBuilder : Builder
    {
        public class BuilderParameters : BaseModelArgument<BuilderParameters>
        {
            public static int GPUID { get { return int.Parse(Argument["GPUID"].Value); } }
            public static DNNRunMode RunMode { get { return (DNNRunMode)int.Parse(Argument["RUN-MODE"].Value); } } // == 0 ? DNNRunMode.Train : DNNRunMode.Predict; } }

            #region Input Data Argument.
            public static string InputDir { get { return Argument["INPUT-DIR"].Value; } }
            #endregion.

            public static int BeamSize { get { return int.Parse(Argument["BEAM-SIZE"].Value); } }

            public static int MiniBatchSize { get { return int.Parse(Argument["MINI-BATCH"].Value); } }
            public static int TestMiniBatchSize { get { return int.Parse(Argument["TEST-MINI-BATCH"].Value); } }

            public static int N_EmbedDim { get { return int.Parse(Argument["N-EMBED-DIM"].Value); } }
            public static int R_EmbedDim { get { return int.Parse(Argument["R-EMBED-DIM"].Value); } }
            public static int RNN_Dim { get { return int.Parse(Argument["RNN-DIM"].Value); } }
            public static int[] DNN_DIMS { get { return Argument["DNN-DIMS"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }

            public static int[] S_NET { get { return Argument["S-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] S_AF { get { return Argument["S-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }

            public static int[] T_NET { get { return Argument["T-NET"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray(); } }
            public static A_Func[] T_AF { get { return Argument["T-AF"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(i => (A_Func)int.Parse(i)).ToArray(); } }


            public static float BLACK_R { get { return float.Parse(Argument["BLACK-R"].Value); } }
            public static float POS_R { get { return float.Parse(Argument["POS-R"].Value); } }
            public static float NEG_R { get { return float.Parse(Argument["NEG-R"].Value); } }
            
            public static int MAX_HOP { get { return int.Parse(Argument["MAX-HOP"].Value); } }

            public static int MIN_HOP { get { return int.Parse(Argument["MIN-HOP"].Value); } }

            public static int SCORE_TYPE { get { return int.Parse(Argument["SCORE-TYPE"].Value); } }
            public static int ROLL_NUM { get { return int.Parse(Argument["ROLL-NUM"].Value); } }

            public static float REWARD_DISCOUNT { get { return float.Parse(Argument["REWARD-DISCOUNT"].Value); } }
            public static float REWARD_MCTS_DISCOUNT { get { return float.Parse(Argument["REWARD-MCTS-DISCOUNT"].Value); } }

            public static string ModelOutputPath { get { return (LogFile + Argument["MODEL-PATH"].Value); } }
            public static string SCORE_PATH { get { return (Argument["SCORE-PATH"].Value); } }
            public static string SEED_MODEL { get { return Argument["SEED-MODEL"].Value; } }

            public static int TEST_MCTS_NUM { get { return int.Parse(Argument["TEST-MCTS-NUM"].Value); } }

            public static int MCTS_NUM { get { return int.Parse(Argument["MCTS-NUM"].Value); } }

            public static float MSE_LAMBDA { get { return float.Parse(Argument["MSE-LAMBDA"].Value); } }

            public static float UCB1_C { get { return float.Parse(Argument["UCB1-C"].Value); } }
            public static float PUCT_C { get { return float.Parse(Argument["PUCT-C"].Value); } }
            public static float PUCT_D { get { return float.Parse(Argument["PUCT-D"].Value); } }

            public static List<Tuple<int, float>> Reward_Feedback
            {
                get
                {
                    return Argument["R-FB"].Value.Split(',').Select(i => Tuple.Create<int, float>(int.Parse(i.Split(':')[0]), float.Parse(i.Split(':')[1]))).ToList();
                }
            }

            public static float Epsilon(int step)
            {
                for (int i = 0; i < Reward_Feedback.Count; i++)
                {
                    if (step < Reward_Feedback[i].Item1)
                    {
                        float lambda = (step - Reward_Feedback[i - 1].Item1) * 1.0f / (Reward_Feedback[i].Item1 - Reward_Feedback[i - 1].Item1);
                        return lambda * Reward_Feedback[i].Item2 + (1 - lambda) * Reward_Feedback[i - 1].Item2;
                    }
                }
                return Reward_Feedback.Last().Item2;
            }

            public static int EStrategy(int mcts_idx)
            {
                string[] idxs = Argument["MCTS-EXP"].Value.Split(',').ToArray();
                for (int i = 0; i < idxs.Length; i++)
                {
                    if (mcts_idx < int.Parse(idxs[i]))
                    {
                        return i;
                    }
                }
                return idxs.Length - 1;
            }

            public static float UPDATE_R_DISCOUNT { get { return float.Parse(Argument["UPDATE-R-DISCOUNT"].Value); } }

            public static float BASE_LAMBDA { get { return float.Parse(Argument["BASE-LAMBDA"].Value); } }
            public static float PROB_LAMBDA { get { return float.Parse(Argument["PROB-LAMBDA"].Value); } }
            public static float SCORE_LAMBDA { get { return float.Parse(Argument["SCORE-LAMBDA"].Value); } }

            public static float PUCB_C { get { return float.Parse(Argument["PUCB-C"].Value); } }
            public static float PUCB_B { get { return float.Parse(Argument["PUCB-B"].Value); } }
            public static float PUCB_M { get { return float.Parse(Argument["PUCB-M"].Value); } }

            public static int MAX_ACTION_NUM { get { return int.Parse(Argument["MAX-ACTION-NUM"].Value); } }

            public static int AGG_SCORE { get { return int.Parse(Argument["AGG-SCORE"].Value); } }

            public static int MAP_EVAL { get { return int.Parse(Argument["MAP-EVAL"].Value); } }

            public static float BETA { get { return float.Parse(Argument["BETA"].Value); } }
            public static float ALPHA_1 { get { return float.Parse(Argument["ALPHA-1"].Value); } }
            public static float ALPHA_2 { get { return float.Parse(Argument["ALPHA-2"].Value); } }

            public static float TRAIN_C { get { return float.Parse(Argument["TRAIN-C"].Value); } }
            public static float TRAIN_L { get { return float.Parse(Argument["TRAIN-L"].Value); } }
            public static float TEST_C { get { return float.Parse(Argument["TEST-C"].Value); } }
            public static float TEST_L { get { return float.Parse(Argument["TEST-L"].Value); } }

            public static int TRAIN_STRATEGY { get { return int.Parse(Argument["TRAIN-STR"].Value); } }

            public static int TRAIN_SAMPLES { get { return int.Parse(Argument["TRAIN-SAMPLES"].Value); } }
            public static int TEST_SAMPLES { get { return int.Parse(Argument["TEST-SAMPLES"].Value); } }

            public static int STEP_FILTER { get { return int.Parse(Argument["STEP-FILTER"].Value); } }

            public static int DEV_AS_TRAIN { get { return int.Parse(Argument["DEV-TRAIN"].Value); } }

            static BuilderParameters()
            {
                Argument.Add("GPUID", new ParameterArgument("2", "GPU Device ID"));
                Argument.Add("RUN-MODE", new ParameterArgument(@"0", "Run Mode"));

                #region Input Data Argument.
                Argument.Add("INPUT-DIR", new ParameterArgument(string.Empty, "dir input."));
                #endregion.

                Argument.Add("N-EMBED-DIM", new ParameterArgument("100", "Node Embedding Dim"));
                Argument.Add("R-EMBED-DIM", new ParameterArgument("100", "Relation Embedding Dim"));
                Argument.Add("RNN-DIM", new ParameterArgument("100", "RNN hidden state dim."));

                Argument.Add("DNN-DIMS", new ParameterArgument("100,100", "DNN Map Dimensions."));
                //Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                //Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Termination Network AF."));
                Argument.Add("S-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("S-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));

                Argument.Add("T-NET", new ParameterArgument("1", "Termination Network."));
                Argument.Add("T-AF", new ParameterArgument(((int)A_Func.Tanh).ToString(), "Scoring Network AF."));


                Argument.Add("BLACK-R", new ParameterArgument("0", "black reward"));
                Argument.Add("POS-R", new ParameterArgument("1", "pos reward"));
                Argument.Add("NEG-R", new ParameterArgument("0", "neg reward"));

                Argument.Add("MAX-HOP", new ParameterArgument("5", "maximum number of hops"));
                Argument.Add("MIN-HOP", new ParameterArgument("2", "minimum number of hops"));

                Argument.Add("MINI-BATCH", new ParameterArgument("32", "Mini Batch Size."));
                Argument.Add("TEST-MINI-BATCH", new ParameterArgument("1024", "Mini Batch Size."));

                Argument.Add("SCORE-TYPE", new ParameterArgument("0", "0:probability; 1:pesudo reward;"));
                Argument.Add("ROLL-NUM", new ParameterArgument("1", "roll number"));

                Argument.Add("BEAM-SIZE", new ParameterArgument("10", "beam size."));

                Argument.Add("REWARD-DISCOUNT", new ParameterArgument("0.99", "Reward discount"));
                Argument.Add("REWARD-MCTS-DISCOUNT", new ParameterArgument("0.8", "mcts reward discount."));

                Argument.Add("SEED-MODEL", new ParameterArgument(string.Empty, "Seed Model for an optional seed model"));

                Argument.Add("MODEL-PATH", new ParameterArgument("model/", "Model Path"));
                Argument.Add("SCORE-PATH", new ParameterArgument("./", "Output Score File."));

                Argument.Add("TEST-MCTS-NUM", new ParameterArgument("64", "Pre Monto Carlo Tree Search Number"));
                Argument.Add("MCTS-NUM", new ParameterArgument("64", "Monto Carlo Tree Search Number"));

                Argument.Add("R-FB", new ParameterArgument("0:1.0,100:0.5f,500:0.1f", "Reward feedback epislon"));
                Argument.Add("MSE-LAMBDA", new ParameterArgument("0.01", "MSE lambda in objective function."));
                Argument.Add("BASE-LAMBDA", new ParameterArgument("1.0", "Base lambda in scoring function."));
                Argument.Add("SCORE-LAMBDA", new ParameterArgument("1.0", "Score lambda in scoring function."));
                Argument.Add("PROB-LAMBDA", new ParameterArgument("1.0", "Prob lambda in scoring function."));

                Argument.Add("MCTS-EXP", new ParameterArgument("8,16,24,48,64", "Exploration strategy."));
                Argument.Add("UCB1-C", new ParameterArgument("1.4", "UCB1 bound."));
                Argument.Add("PUCT-C", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCT-D", new ParameterArgument("1", "P UCB bound."));

                Argument.Add("PUCB-M", new ParameterArgument("2", "P UCB bound."));
                Argument.Add("PUCB-C", new ParameterArgument("1.2", "P UCB bound."));
                Argument.Add("PUCB-B", new ParameterArgument("0.0001", "P UCB bound."));

                Argument.Add("UPDATE-R-DISCOUNT", new ParameterArgument("1", "mcts reward discount."));
                Argument.Add("MAX-ACTION-NUM", new ParameterArgument("0", "Default max action number."));

                Argument.Add("EVALUATION-TOOL", new ParameterArgument(string.Empty, "Evaluation path tool."));

                Argument.Add("AGG-SCORE", new ParameterArgument("0", "0:enumate; 1:max pool; 2:sum pool;"));
                Argument.Add("MAP-EVAL", new ParameterArgument("1", "0:no map eval; 1:mapeval;"));
                Argument.Add("BETA", new ParameterArgument("1", "moving average baseline estimation."));
                Argument.Add("ALPHA-1", new ParameterArgument("0","max entropy for termination gate."));
                Argument.Add("ALPHA-2", new ParameterArgument("0", "max entropy for action gate."));

                Argument.Add("TRAIN-STR", new ParameterArgument("0","0:standard train strategy; 1:mcts train."));
                Argument.Add("TRAIN-L", new ParameterArgument("1", "train random lambda"));
                Argument.Add("TRAIN-C", new ParameterArgument("1", "train self lambda."));
                Argument.Add("TEST-L", new ParameterArgument("1", "test random lambda."));
                Argument.Add("TEST-C", new ParameterArgument("1", "test self lambda."));

                Argument.Add("TRAIN-SAMPLES", new ParameterArgument("0", "training samples per epoch."));
                Argument.Add("TEST-SAMPLES", new ParameterArgument("0", "testing samples per epoch."));

                Argument.Add("STEP-FILTER", new ParameterArgument("0", "step filter"));
                Argument.Add("DEV-TRAIN", new ParameterArgument("0", "use dev set as training set."));
            }
        }

        public override BuilderType Type { get { return BuilderType.APP_REINFORCE_WALK_LINK_PREDICTION_V2_11_NEW; } }

        public override void InitStartup(string fileName) { BuilderParameters.Parse(fileName); }

        public override void InitStartup(string fileName, string[] additonalArg)
        {
            BuilderParameters.Parse(fileName);
            BuilderParameters.Parse(additonalArg);
        }

        public class BasicPolicyRunner : CompositeNetRunner
        {
            public new StatusData Input { get; set; }

            /// <summary>
            /// MatchData.
            /// </summary>
            public BiMatchBatchData MatchPath = null;

            /// <summary>
            /// new Node Index.
            /// </summary>
            public List<int> NodeIndex { get; set; }

            /// <summary>
            /// next step log probability.
            /// </summary>
            public List<float> LogProb { get; set; }

            /// <summary>
            /// pre sel action and pre status. 
            /// </summary>
            public List<int> PreSelActIndex { get; set; }
            public List<int> PreStatusIndex { get; set; }

            public List<string> StatusKey { get; set; }

            public BasicPolicyRunner(StatusData input, RunnerBehavior behavior) : base(behavior)
            {
                Input = input;

                NodeIndex = new List<int>();
                LogProb = new List<float>();
                PreSelActIndex = new List<int>();
                PreStatusIndex = new List<int>();
                StatusKey = new List<string>();
            }
        }

        class BeamSearchActionRunner : BasicPolicyRunner
        {
            int BeamSize = 1;

            bool IsLast = false;
            /// <summary>
            /// group aware beam search.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public BeamSearchActionRunner(StatusData input, int maxNeighbor, int beamSize, RunnerBehavior behavior, bool isLast = false) : base(input, behavior)
            {
                BeamSize = beamSize;
                IsLast = isLast;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.GraphQuery.MaxBatchSize * BeamSize
                }, behavior.Device);
            }

            public override void Forward()
            {
                if (Input.MatchCandidateProb.Segment != Input.BatchSize)
                {
                    throw new Exception(string.Format("the number of match prob and batch size doesn't match {0} and {1}",
                                    Input.MatchCandidateProb.Segment, Input.BatchSize));
                }

                Input.MatchCandidateProb.Output.SyncToCPU();
                //if (Input.Term != null) Input.Term.Output.SyncToCPU();

                NodeIndex.Clear();
                LogProb.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                StatusKey.Clear();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                // for each beam.
                for (int i = 0; i < Input.GraphQuery.BatchSize; i++)
                {
                    List<int> idxs = Input.GraphQuery.GetBatchIdxs(i, Input.Step);

                    MinMaxHeap<Tuple<int, int>> topKheap = new MinMaxHeap<Tuple<int, int>>(BeamSize, 1);

                    foreach (int b in idxs)
                    {
                        int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[b];
                        int s = b == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[b - 1];

                        float prob = Input.GetLogProb(b) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[e - 1]);

                        //float nonLogTerm = Input.LogPTerm(b, false);
                        //float logTerm = Input.LogPTerm(b, true);
                        //if (IsLast)
                        //{
                            //nonLogTerm =
                        //    Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, b, prob));
                        //}
                        //else
                        {
                            for (int t = s; t < e - 1; t++)
                            {
                                float tmp_prob = Input.GetLogProb(b) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t]);
                                topKheap.push_pair(new Tuple<int, int>(b, t), tmp_prob);
                            }
                            topKheap.push_pair(new Tuple<int, int>(b, e - 1), prob);
                        }
                    }

                    while (!topKheap.IsEmpty)
                    {
                        KeyValuePair<Tuple<int, int>, float> p = topKheap.PopTop();

                        //if (p.Key.Item2 == -1) { Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, p.Key.Item1, p.Value)); continue; }

                        match.Add(new Tuple<int, int, float>(p.Key.Item1, p.Key.Item2, p.Value));
                        NodeIndex.Add(Input.MatchCandidate[p.Key.Item2].Item1);
                        LogProb.Add(p.Value);

                        PreStatusIndex.Add(p.Key.Item1);
                        PreSelActIndex.Add(p.Key.Item2);

                        string statusQ = string.Format("{0}-N:{1}-R:{2}", Input.GetStatusKey(p.Key.Item1), Input.MatchCandidate[p.Key.Item2].Item1, Input.MatchCandidate[p.Key.Item2].Item2);

                        StatusKey.Add(statusQ);
                    }
                }
                MatchPath.SetMatch(match);
            }
        }

        class MCTSActionSamplingRunner : BasicPolicyRunner
        {
            int lineIdx = 0;
            EpisodicMemory Memory { get; set; }
            //Random random = new Random(21);
            public int MCTSIdx { get; set; }
            public override void Init()
            {
                lineIdx = 0;
            }
            bool IsLast = false;
            bool IsStart = true;
            /// <summary>
            /// match candidate, and match probability.
            /// </summary>
            /// <param name="input"></param>
            /// <param name="behavior"></param>
            public MCTSActionSamplingRunner(StatusData input, int maxNeighbor, int mctsIdx, EpisodicMemory memory, RunnerBehavior behavior, bool isLast = false, bool isStart = true) : base(input, behavior)
            {
                Memory = memory;
                MCTSIdx = mctsIdx;
                IsLast = isLast;
                IsStart = isStart;
                MatchPath = new BiMatchBatchData(new BiMatchBatchDataStat()
                {
                    MAX_SRC_BATCHSIZE = Input.MaxBatchSize,
                    MAX_TGT_BATCHSIZE = Input.MaxBatchSize * maxNeighbor,
                    MAX_MATCH_BATCHSIZE = Input.MaxBatchSize,
                }, behavior.Device);
            }

            public override void Forward()
            {
                NodeIndex.Clear();
                PreSelActIndex.Clear();
                PreStatusIndex.Clear();
                LogProb.Clear();
                StatusKey.Clear();

                Input.MatchCandidateProb.Output.SyncToCPU();
                Input.MatchCandidateQ.Output.SyncToCPU();

                //Logger.WriteLog("Size {0}", Input.MatchCandidateProb.Output.Size);
                //for(int i=0;i  < Input.MatchCandidateProb.Output.Size;i++)
                //{
                //    Logger.WriteLog(Input.MatchCandidateProb.Output.MemPtr[i].ToString());
                //}
                //Console.ReadLine();

                //if (Input.Term != null) Input.Term.Output.SyncToCPU();

                List<Tuple<int, int, float>> match = new List<Tuple<int, int, float>>();

                int currentStep = Input.Step;
                for (int i = 0; i < Input.MatchCandidateProb.Segment; i++)
                {
                    int e = Input.MatchCandidateProb.SegmentIdx.MemPtr[i];
                    int s = i == 0 ? 0 : Input.MatchCandidateProb.SegmentIdx.MemPtr[i - 1];

                    int selectIdx = s;

                    string Qkey = Input.GetStatusKey(i);

                    int actionDim = e - s;

                    //key = key + "-" + 
                    List<Tuple<float, float>> m = Memory.Search(Qkey);
                    float[] prior_r = null;
                                        
                    // probability of the policy.
                    //if(currentStep >= BuilderParameters.MIN_HOP)
                    //{
                    //    prior_r = new float[actionDim + 1];
                    //    Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);

                    //    float t = Util.Logistic(Input.Term.Output.Data.MemPtr[i]);
                    //    if (t >= 1 - Util.LargeEpsilon) { t = 1 - Util.LargeEpsilon; }
                    //    if (t <= Util.LargeEpsilon) { t = Util.LargeEpsilon; }
                    //    prior_r[actionDim] = t;
                    //    for (int a = 0; a < actionDim; a++) { prior_r[a] = prior_r[a] * (1 - t); }
                    //}
                    //else
                    {
                        prior_r = new float[actionDim];
                        Array.Copy(Input.MatchCandidateProb.Output.MemPtr, s, prior_r, 0, actionDim);
                    }
                    //float[] prior_q = null;
                    //{
                    //    prior_q = new float[actionDim + 1];
                    //    Array.Copy(Input.MatchCandidateQ.Output.MemPtr, s, prior_q, 0, actionDim);
                    //    prior_q[actionDim] = Input.Term.Output.Data.MemPtr[i];
                    //}
                    
                    int idx = 0;

                    //if (IsLast)
                    //{
                    //    idx = actionDim - 1;
                    //}
                    //else
                    {
                        int strategy = BuilderParameters.EStrategy(MCTSIdx);
                        //Console.WriteLine("MCTS Index {0}, strategy {1}", MCTSIdx, strategy);
                        switch (strategy)
                        {
                            case 0: idx = BanditAlg.UniformRandomStrategy0(prior_r.Length, SampleRandom); break;
                            case 1: idx = BanditAlg.ThompasSampling(prior_r, SampleRandom); break;
                            case 2: idx = BanditAlg.UCB1Bandit(m, prior_r.Length, BuilderParameters.UCB1_C, SampleRandom); break;
                            case 3: idx = BanditAlg.AlphaGoZeroBandit(m, prior_r.ToList(), BuilderParameters.PUCT_C, SampleRandom); break;
                            case 4: idx = BanditAlg.AlphaGoZeroBanditV2(m, prior_r.ToList(), BuilderParameters.PUCT_C, SampleRandom); break;
                            case 5: idx = BanditAlg.PUCB(m, prior_r.ToList(), BuilderParameters.PUCB_B, BuilderParameters.PUCB_C, BuilderParameters.PUCB_M, SampleRandom); break;
                            //case 6: idx = BanditAlg.AlphaGoZeroBandit_Stochastic(m, prior_q.ToList(), Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.TRAIN_C : BuilderParameters.TEST_C,
                            //                                                                          Behavior.RunMode == DNNRunMode.Train ? BuilderParameters.TRAIN_L : BuilderParameters.TEST_L, SampleRandom); break;
                        }
                    }

                    selectIdx = s + idx;
                    float prob = Input.GetLogProb(i) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]); // nonLogTerm + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[selectIdx]);

                    //if (IsLast) // idx == actionDim - 1)
                    //{
                    //    Input.GraphQuery.Results.Add(new Tuple<int, int, float>(Input.Step, i, prob));
                    //}
                    //else
                    {
                        PreSelActIndex.Add(selectIdx);
                        PreStatusIndex.Add(i);

                        string statusQ = string.Format("{0}-N:{1}-R:{2}", Qkey, Input.MatchCandidate[selectIdx].Item1, Input.MatchCandidate[selectIdx].Item2);
                        StatusKey.Add(statusQ);

                        NodeIndex.Add(Input.MatchCandidate[selectIdx].Item1);

                        //float nonLogTerm = 0;
                        //if (Input.Term != null) { nonLogTerm = Input.LogPTerm(i, false); }
                        //nonLogTerm + 
                        LogProb.Add(prob);

                        match.Add(new Tuple<int, int, float>(i, selectIdx, 1));
                    }
                }
                MatchPath.SetMatch(match);
                lineIdx += Input.MatchCandidateProb.Segment;
            }
        }

      

        /// <summary>
        /// it is a little difficult.
        /// </summary>
        class RewardRunner : ObjectiveRunner
        {
            //Random random = new Random();
            new List<GraphQueryData> Input { get; set; }
            Dictionary<int, float> Success = new Dictionary<int, float>();
            Dictionary<int, Tuple<float, float>> StepSuccess = new Dictionary<int, Tuple<float, float>>();
            int Epoch = 0;
            float baseline = 0;
            int uniqueNum = 0;
            int uniqueLeafNum = 0;
            int totalBatchNum = 0;
            public override void Init()
            {
                Success.Clear();
                StepSuccess.Clear();
                uniqueNum = 0;
                uniqueLeafNum = 0;
            }

            public void Update(float avgReward)
            {
                baseline = baseline * BuilderParameters.BETA + avgReward * (1 - BuilderParameters.BETA);
            }

            public override void Complete()
            {
                Epoch += 1;
                foreach (KeyValuePair<int, Tuple<float, float>> i in StepSuccess)
                {
                    Logger.WriteLog("Step {0}, Visit {1} , Success  {2}", i.Key, i.Value.Item1, i.Value.Item2);
                }
                int sc = Success.Where(i => i.Value > 0).Count();
                int t = Success.Count;
                Logger.WriteLog("Success validation rate {0}, success {1}, total {2}", sc * 1.0f / t, sc, t);
                Logger.WriteLog("Baseline {0}", baseline);
                Logger.WriteLog("unique leaf number {0}", uniqueLeafNum * 1.0f / uniqueNum);


                float alpha_entropy = BuilderParameters.ALPHA_2 * (float)Math.Pow(0.995f, totalBatchNum / 200);
                Logger.WriteLog("Entropy Term {0}", alpha_entropy);
            }

            public RewardRunner(List<GraphQueryData> input, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
            }

            public override void Forward()
            {
                float trueReward = 0;
                int sampleNum = 0;
                //float pos_mseloss = 0;
                //float neg_mseloss = 0;
                int pos_smp = 0;
                int neg_smp = 0;
                ObjectiveDict.Clear();

                Dictionary<int, HashSet<int>> visitNodes = new Dictionary<int, HashSet<int>>();
                Dictionary<int, float> TmpSuccess = new Dictionary<int, float>();
                // mcts sampling.
                float avg_t_prob = 0;
                int avg_t_count = 0;

                float avg_act_prob = 0;
                int avg_act_count = 0;

                //float 
                for (int m = 0; m < Input.Count; m++)
                {
                    for (int i = 0; i < Input[m].StatusPath.Last().BatchSize; i++)
                    {
                        int t = Input[m].StatusPath.Last().Step;
                        int b = i;
                        //int t = Input[m].Results[i].Item1;
                        //int b = Input[m].Results[i].Item2;
                        int origialB = Input[m].StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = Input[m].StatusPath[t].NodeID[b];
                        float neg_likihood = Input[m].StatusPath[t].GetLogProb(b);
                        // avoid the zero step.
                        //if (t == 0) continue;

                        //Input.GetLogProb(b) + (float)Math.Log(Input.MatchCandidateProb.Output.MemPtr[t])

                        int targetId = Input[m].RawTarget[origialB];
                        int rawIdx = Input[m].RawIndex[origialB];

                        string mkey = string.Format("Term Visit {0}", t);
                        if (!ObjectiveDict.ContainsKey(mkey)) { ObjectiveDict[mkey] = 0; }
                        ObjectiveDict[mkey] += 1;

                        if (!visitNodes.ContainsKey(rawIdx))
                        {
                            visitNodes.Add(rawIdx, new HashSet<int>());
                        }

                        if (!visitNodes[rawIdx].Contains(predId))
                        {
                            visitNodes[rawIdx].Add(predId);
                        }

                        float update_v = 0;
                        //float estimate_v = Util.Logistic(Input[m].StatusPath[t].Score.Output.Data.MemPtr[b]);
                        float true_v = 0;
                        if (targetId == predId)
                        {
                            true_v = 1;
                            update_v = BuilderParameters.POS_R;
                            //pos_mseloss += Math.Abs(true_v - estimate_v);
                            pos_smp += 1;
                        }
                        else
                        {
                            true_v = 0;
                            update_v = BuilderParameters.NEG_R;
                            //neg_mseloss += Math.Abs(true_v - estimate_v);
                            neg_smp += 1;
                        }


                        trueReward += true_v;
                        sampleNum += 1;

                        if (!Success.ContainsKey(rawIdx)) Success[rawIdx] = 0;
                        Success[rawIdx] += true_v;

                        if (!StepSuccess.ContainsKey(t)) StepSuccess[t] = new Tuple<float, float>(0, 0);
                        StepSuccess[t] = new Tuple<float, float>(StepSuccess[t].Item1 + 1, StepSuccess[t].Item2 + update_v);
                        
                        if (!TmpSuccess.ContainsKey(rawIdx)) TmpSuccess[rawIdx] = 0;
                        TmpSuccess[rawIdx] += true_v;

                        if (Input[m].BlackTargets[origialB].Contains(predId) && targetId != predId && BuilderParameters.BLACK_R != -10000)
                            continue;

                        string mSucKey = string.Format("Term Reward {0}", t);
                        if (!ObjectiveDict.ContainsKey(mSucKey)) { ObjectiveDict[mSucKey] = 0; }
                        ObjectiveDict[mSucKey] += true_v;

                        {
                            // Policy gradient for path finding.
                            StatusData st = Input[m].StatusPath[t];

                            float adv = update_v - baseline;

                            //float p = st.Term.Output.Data.MemPtr[b];
                            //if (t != BuilderParameters.MAX_HOP)
                            if (t == BuilderParameters.MAX_HOP && st.MatchCandidateProb == null)
                            {
                                //float actSelect = SampleRandom.Next();
                                //if (actSelect < st.Term.Output.Data.MemPtr[b])
                                //{
                                //    st.Term.Deriv.Data.MemPtr[b] = (float)(adv * (1 - p) -
                                //        BuilderParameters.ALPHA_1 * (p * (1 - p) * (Math.Log(p + Util.LargeEpsilon) - Math.Log(1 - p + Util.LargeEpsilon))));
                                //    avg_t_prob += p;
                                //    avg_t_count += 1;
                                //}
                                //else
                                //{
                                //    st.Term.Deriv.Data.MemPtr[b] = (float)(adv * (- p) -
                                //        BuilderParameters.ALPHA_1 * (p * (1 - p) * (Math.Log(p + Util.LargeEpsilon) - Math.Log(1 - p + Util.LargeEpsilon))));
                                //    avg_t_prob += 1 - p;
                                //    avg_t_count += 1;
                                //}
                            }
                            else
                            {
                                int termIdx = st.GetActionEndIndex(b) - 1;
                                st.MatchCandidateProb.Deriv.MemPtr[termIdx] = adv;
                                float termP = st.MatchCandidateProb.Output.MemPtr[termIdx];
                                //st.Term.Deriv.Data.MemPtr[b] = (float)( adv * (1 - p) -
                                //        BuilderParameters.ALPHA_1 * (p * (1 - p) * (Math.Log(p + Util.LargeEpsilon) - Math.Log(1 - p + Util.LargeEpsilon))));
                                avg_t_prob += termP;
                                avg_t_count += 1;
                                throw new NotImplementedException(string.Format("The termination should all at the last step, {0}", t));
                            }
                            // MSE error for reward estimation.
                            // st.Score.Deriv.Data.MemPtr[b] = BuilderParameters.MSE_LAMBDA * (update_v - estimate_v);

                            for (int pt = t - 1; pt >= 0; pt--)
                            {
                                StatusData pst = Input[m].StatusPath[pt];
                                int pb = st.GetPreStatusIndex(b);
                                int sidx = st.PreSelActIndex[b];

                                float discount = (float)Math.Pow(BuilderParameters.REWARD_DISCOUNT, t - pt);
                                {
                                    //if (pst.Term != null)
                                    //{
                                    //    float tmp_p = pst.Term.Output.Data.MemPtr[pb];
                                    //    pst.Term.Deriv.Data.MemPtr[pb] = (float)( discount * adv * (- tmp_p) -
                                    //        BuilderParameters.ALPHA_1 * (tmp_p * (1 - tmp_p) * (Math.Log(tmp_p + Util.LargeEpsilon) - Math.Log(1 - tmp_p + Util.LargeEpsilon))));
                                    //    avg_t_prob += 1 - tmp_p;
                                    //    avg_t_count += 1;
                                    //}
                                    pst.MatchCandidateProb.Deriv.MemPtr[sidx] = discount * adv;
                                    avg_act_prob += pst.MatchCandidateProb.Output.MemPtr[sidx];
                                    avg_act_count += 1;
                                }
                                
                                st = pst;
                                b = pb;
                            }
                        }
                    }
                }
                uniqueNum += visitNodes.Count;
                uniqueLeafNum += visitNodes.Select(t => t.Value.Count).Sum();
                Update(trueReward * 1.0f / sampleNum);

                ObjectiveDict["TERM-AVG-PROB"] = avg_t_prob / (avg_t_count + 0.0001f);
                ObjectiveDict["ACT-AVG-PROB"] = avg_act_prob / avg_act_count;

                //ObjectiveDict["AVG-TRUE-PATH"] = trueReward / (sampleNum + float.Epsilon);
                //ObjectiveDict["TERM-POS-MSE-LOSS"] = pos_mseloss / (pos_smp + 1);
                //ObjectiveDict["TERM-NEG-MSE-LOSS"] = neg_mseloss / (neg_smp + 1);

                ObjectiveDict["TERM-POS-NUM"] = pos_smp;
                ObjectiveDict["TERM-NEG-NUM"] = neg_smp;

                ObjectiveDict["Success-Rate"] = TmpSuccess.Where(i => i.Value > 0).Count() * 1.0f / TmpSuccess.Count;

                ObjectiveScore = trueReward / (sampleNum + float.Epsilon);

                //if(BuilderParameters.MEM_CLEAN > 0 && Epoch % BuilderParameters.MEM_CLEAN == 0) Memory.Clear();
                //average ground truth results.
                totalBatchNum += 1;
                float alpha_entropy = BuilderParameters.ALPHA_2 * (float)Math.Pow(0.995f, totalBatchNum / 200);

                for (int p = 0; p < Input.Count; p++)
                {
                    for (int i = Input[p].StatusPath.Count - 1; i >= 0; i--)
                    {
                        StatusData st = Input[p].StatusPath[i];
                        //if (st.Term != null) st.Term.Deriv.Data.SyncFromCPU();
                        if (st.Step != BuilderParameters.MAX_HOP && st.MatchCandidateProb != null)
                        {
                            st.MatchCandidateProb.Deriv.SyncFromCPU();
                            Behavior.Computelib.DerivLogProbEntropy(st.MatchCandidateProb.SegmentIdx, st.MatchCandidateProb.Output,
                                st.MatchCandidateProb.Deriv, 1, alpha_entropy, Util.LargeEpsilon, st.MatchCandidateProb.Segment);
                        }
                        //if (st.Score != null) st.Score.Deriv.SyncFromCPU();
                    }
                    //Input[p].Results.Clear();
                }
            }
        }

        class RewardFeedbackRunner : StructRunner
        {
            new GraphQueryData Input { get; set; }
            EpisodicMemory Memory { get; set; }

            float PosMean = 0;
            float NegMean = 0;
            int PosNum = 0;
            int NegNum = 0;

            int Epoch = 0;
            public override void Init()
            {
                Epoch = 0;
                PosMean = 0;
                PosNum = 0;

                NegMean = 0;
                NegNum = 0;
            }

            public override void Complete()
            {
                Epoch += 1;
                Logger.WriteLog("Pos Num {0}, Pos Mean {1}", PosNum, PosMean / (PosNum + 0.000001f));
                Logger.WriteLog("Neg Num {0}, Neg Mean {1}", NegNum, NegMean / (NegNum + 0.000001f));

            }

            public RewardFeedbackRunner(GraphQueryData input, EpisodicMemory memory, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Input = input;
                Memory = memory;
            }

            public override void Forward()
            {
                // calculate termination probability.
                for (int t = Input.StatusPath.Count - 1; t >= 0; t--)
                {
                    //if (Input.StatusPath[t].Term != null)

                    //if (Input.StatusPath[t].Term != null)
                    //{
                    //    ComputeLib.Logistic(Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].Term.Output.Data, 0, Input.StatusPath[t].BatchSize, 1);
                    //    ComputeLib.ClipVector(Input.StatusPath[t].Term.Output.Data, Input.StatusPath[t].BatchSize, 1 - Util.LargeEpsilon, Util.LargeEpsilon);
                    //    Input.StatusPath[t].Term.Output.Data.SyncToCPU();
                    //    Array.Clear(Input.StatusPath[t].Term.Deriv.Data.MemPtr, 0, Input.StatusPath[t].BatchSize);
                    //}
                    if (Input.StatusPath[t].MatchCandidateProb != null)
                    {
                        Array.Clear(Input.StatusPath[t].MatchCandidateProb.Deriv.MemPtr, 0, Input.StatusPath[t].MatchCandidateProb.Length);
                    }

                    //if (Input.StatusPath[t].Score != null)
                    //{
                    //    Input.StatusPath[t].Score.Output.Data.SyncToCPU();
                    //    Array.Clear(Input.StatusPath[t].Score.Deriv.Data.MemPtr, 0, Input.StatusPath[t].Score.Output.Data.EffectiveSize);
                    //}
                }

            }
        }

        
        class HitKRunner : StructRunner
        {
            //float MAP = 0;

            //int SmpIdx = 0;
            List<GraphQueryData> Query { get; set; }
            int Iteration = 0;
            int TotalResultNode = 0;
            int Hit1 = 0;
            int Hit3 = 0;
            int Hit5 = 0;
            int Hit10 = 0;
            int Hit20 = 0;
            float AP = 0;
            int SmpNum = 0;

            Dictionary<int, int> Top1StepDistr = new Dictionary<int, int>();

            public HitKRunner(List<GraphQueryData> query, RunnerBehavior behavior) : base(Structure.Empty, behavior)
            {
                Query = query;
                Iteration = 0;
                
            }

            public override void Init()
            {
                Hit1 = 0;
                Hit3 = 0;
                Hit5 = 0;
                Hit10 = 0;
                Hit20 = 0;
                AP = 0;
                SmpNum = 0;
                TotalResultNode = 0;
                Top1StepDistr.Clear();
            }


            public override void Complete()
            {
                Logger.WriteLog("MRR {0}", AP * 1.0f / SmpNum);
                Logger.WriteLog("Hit1 {0}", Hit1 * 1.0f / SmpNum);
                Logger.WriteLog("Hit3 {0}", Hit3 * 1.0f / SmpNum);
                Logger.WriteLog("Hit5 {0}", Hit5 * 1.0f / SmpNum);
                Logger.WriteLog("Hit10 {0}", Hit10 * 1.0f / SmpNum);
                Logger.WriteLog("Hit20 {0}", Hit20 * 1.0f / SmpNum);
                Logger.WriteLog("Avg Visit Node {0}", TotalResultNode * 1.0f / SmpNum);
                foreach (KeyValuePair<int, int> i in Top1StepDistr)
                {
                    Logger.WriteLog("Top 1, Step {0}, Num {1}", i.Key, i.Value);
                }
                Iteration += 1;
            }
            public override void Forward()
            {
                List<Tuple<int, int, float>>[] scoreDict = DataUtil.AggregrateScore(Query, BuilderParameters.PROB_LAMBDA, BuilderParameters.SCORE_LAMBDA, BuilderParameters.AGG_SCORE);

                for (int originB = 0; originB < Query[0].BatchSize; originB++)
                {
                    int rawSource = Query[0].RawSource[originB];
                    int rawTarget = Query[0].RawTarget[originB];
                    scoreDict[originB].Sort((x, y) => y.Item3.CompareTo(x.Item3));

                    int pos = 1;
                    bool isFound = false;
                    HashSet<int> visitIdx = new HashSet<int>();
                    foreach (Tuple<int, int, float> item in scoreDict[originB])
                    {
                        if (visitIdx.Contains(item.Item1)) continue;
                        visitIdx.Add(item.Item1);

                        if(pos == 1)
                        {
                            if(!Top1StepDistr.ContainsKey(item.Item2))
                            {
                                Top1StepDistr.Add(item.Item2, 0);
                            }
                            Top1StepDistr[item.Item2] += 1;
                        }


                        if(item.Item1 == rawTarget)
                        {
                            isFound = true;
                            break;
                        }
                        pos += 1;
                    }

                    if (isFound)
                    {
                        AP += 1.0f / pos;
                        if(pos <= 1) { Hit1 += 1; }
                        if (pos <= 3) { Hit3 += 1; }
                        if (pos <= 5) { Hit5 += 1; }
                        if (pos <= 10) { Hit10 += 1; }
                        if (pos <= 20) { Hit20 += 1; }
                    }
                }

                SmpNum += Query[0].BatchSize;

                HashSet<int>[] scoreHash = DataUtil.AggregrateUnique(Query);
                TotalResultNode += scoreHash.Select(m => m.Count).Sum();

                for (int p = 0; p < Query.Count; p++)
                {
                    //Query[p].Results.Clear();
                }
            }
        }

        public class NeuralWalkerModel : CompositeNNStructure
        {
            public EmbedStructure NodeEmbed { get; set; }
            public EmbedStructure RelEmbed { get; set; }

            public DNNStructure SrcDNN { get; set; }
            public GRUCell GruCell { get; set; }

            public NeuralWalkerModel(int entityNum, int relationNum, int nodeDim, int relDim, int rnnDim, DeviceType device)
            {
                Logger.WriteLog("Node Num {0} Embed {1}", entityNum, nodeDim);
                Logger.WriteLog("Relation Num {0} Embed {1}", relationNum, relDim);
                
                NodeEmbed = AddLayer(new EmbedStructure(entityNum, nodeDim, device)); // DeviceType.CPU_FAST_VECTOR));
                RelEmbed = AddLayer(new EmbedStructure(relationNum, relDim, device)); // DeviceType.CPU_FAST_VECTOR));

                int embedDim = nodeDim + relDim;

                //GruCell = AddLayer(new GRUCell(StateDim + BuilderParameters.DNN_DIMS.Last(), StateDim, device));
                GruCell = AddLayer(new GRUCell(embedDim, rnnDim, device));

                int inputDim = embedDim + relDim;

                List<int> mapLayers = new List<int>(BuilderParameters.DNN_DIMS);
                mapLayers.Add(embedDim);

                SrcDNN = AddLayer(new DNNStructure(inputDim, mapLayers.ToArray(),
                                           mapLayers.Select(i => A_Func.Tanh).ToArray(),
                                           mapLayers.Select(i => true).ToArray(),
                                           device));
            }
            public NeuralWalkerModel(BinaryReader reader, DeviceType device)
            {
                int modelNum = CompositeNNStructure.DeserializeModelCount(reader);

                NodeEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);
                RelEmbed = (EmbedStructure)DeserializeNextModel(reader, device); // DeviceType.CPU_FAST_VECTOR);

                GruCell = (GRUCell)DeserializeNextModel(reader, device);

                SrcDNN = (DNNStructure)DeserializeNextModel(reader, device);
            }

            public void InitOptimization(RunnerBehavior behavior)
            {
                InitOptimizer(OptimizerParameters.StructureOptimizer, behavior);
            }
        }
       
        public static ComputationGraph BuildComputationGraph(DataEnviroument data, GraphEnviroument graph, 
            int batchSize, int roll, int debugSamples, int beamSize, int policyId,
            int maxHop, NeuralWalkerModel model, RunnerBehavior Behavior) 
        {
            ComputationGraph cg = new ComputationGraph();

            // sample a list of tuplet from the graph.
            SampleRunner SmpRunner = new SampleRunner(data, batchSize, roll, debugSamples, Behavior);
            cg.AddDataRunner(SmpRunner);
            GraphQueryData interface_data = SmpRunner.Output;

            //HiddenBatchData srcEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawSource, graphQuery.MaxBatchSize, model.NodeEmbed, Behavior));
            HiddenBatchData relEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawRel, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));

            IntArgument batchSizeArg = new IntArgument("sample-batch-size"); 
            cg.AddRunner(new HiddenDataBatchSizeRunner(relEmbed, batchSizeArg, Behavior));

            VectorExpanRunner startRunner = new VectorExpanRunner(DataPanel.START_IDX, batchSizeArg, graphQuery.MaxBatchSize, Behavior);
            cg.AddRunner(startRunner);
            HiddenBatchData startEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(startRunner.Ints, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));

            //HiddenBatchData startEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(graphQuery.RawStart, graphQuery.MaxBatchSize, model.RelEmbed, Behavior));
            HiddenBatchData initZeroH0 = (HiddenBatchData)cg.AddRunner(new ZeroMatrixRunner(startEmbed.MAX_BATCHSIZE, model.GruCell.HiddenStateDim, batchSizeArg, Behavior));

            GRUStateRunner initStateRunner = new GRUStateRunner(model.GruCell, initZeroH0, startEmbed, Behavior);
            cg.AddRunner(initStateRunner);
            
            HiddenBatchData H1 = initStateRunner.Output;
            HiddenBatchData Rq = relEmbed;
            
            //HiddenBatchData RawQueryEmbed = statusEmbedRunner.Output;
            //List<GraphQueryData> Queries = new List<GraphQueryData>();
            // MCTS path number.
            //for (int j = 0; j < mctsNum; j++)
            {
                //GraphQueryData newQuery = new GraphQueryData(interface_data);
                StatusData status = new StatusData(interface_data, newQuery.RawSource, H1, Behavior.Device);

                #region multi-hop expansion
                for (int i = 0; i < maxHop; i++)
                {
                    // given status, obtain the match. miniBatch * maxNeighbor number.
                    CandidateActionRunner candidateActionRunner = new CandidateActionRunner(status, graph, Behavior);
                    cg.AddRunner(candidateActionRunner);
                    status.MatchCandidate = candidateActionRunner.Output;

                    HiddenBatchData candRelEmbed = (HiddenBatchData)cg.AddRunner(new LookupEmbedRunner(candidateActionRunner.Output.Item2, 
                                                                        candidateActionRunner.Match.Stat.MAX_MATCH_BATCHSIZE, 
                                                                        model.RelEmbed, Behavior));
                
                    HiddenBatchData I_data = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>()
                                            { status.StateEmbed, Rq }, Behavior));

                    DNNRunner<HiddenBatchData> srcHiddenRunner = new DNNRunner<HiddenBatchData>(model.SrcDNN, I_data, Behavior);
                    cg.AddRunner(srcHiddenRunner);
                    
                    MatrixInnerProductRunner innerRunner = new MatrixInnerProductRunner(new MatrixData(srcHiddenRunner.Output),
                                                                                        new MatrixData(candRelEmbed), candidateActionRunner.Match, Behavior);
                    cg.AddRunner(innerRunner);

                    status.MatchCandidateQ = new SeqVectorData(innerRunner.Output.MaxLength,
                                                           status.MaxBatchSize, innerRunner.Output.Output, innerRunner.Output.Deriv,
                                                           candidateActionRunner.Match.Src2MatchIdx, candidateActionRunner.Match.SrcIdx, Behavior.Device);

                    SeqVecSoftmaxRunner normAttRunner = new SeqVecSoftmaxRunner(status.MatchCandidateQ, 1, Behavior, true);
                    cg.AddRunner(normAttRunner);

                    status.MatchCandidateProb = normAttRunner.Output;

                    BasicPolicyRunner policyRunner = null;
                    if (policyId == 0)
                    {
                        policyRunner = new MCTSActionSamplingRunner(status, graph.MaxNeighborNum, j, mem, Behavior, i == maxHop - 1, i >= BuilderParameters.MIN_HOP);
                    }
                    else if(policyId == 1)
                    {
                        policyRunner = new BeamSearchActionRunner(status, graph.MaxNeighborNum, beamSize, Behavior, i == maxHop - 1);
                    }
                    cg.AddRunner(policyRunner);

                    //if (i < maxHop - 1)
                    //{
                        #region take action.
                        MatrixExpansionRunner srcExpRunner = new MatrixExpansionRunner(status.StateEmbed, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(srcExpRunner);

                        MatrixExpansionRunner tgtExpRunner = new MatrixExpansionRunner(candEmbedRunner.Output, policyRunner.MatchPath, 2, Behavior);
                        cg.AddRunner(tgtExpRunner);

                        MatrixExpansionRunner rQExpRunner = new MatrixExpansionRunner(Rq, policyRunner.MatchPath, 1, Behavior);
                        cg.AddRunner(rQExpRunner);
                        Rq = rQExpRunner.Output;

                        //1111. MatrixExpansionRunner neiExpRunner = new MatrixExpansionRunner(neiData, policyRunner.MatchPath, 1, Behavior);
                        //1111. cg.AddRunner(neiExpRunner);

                        //1111. HiddenBatchData concateData = (HiddenBatchData)cg.AddRunner(new EnsembleMatrixRunner(new List<HiddenBatchData>() { neiExpRunner.Output, tgtExpRunner.Output }, Behavior));

                        //1111. GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, concateData, Behavior);
                        GRUStateRunner stateRunner = new GRUStateRunner(model.GruCell, srcExpRunner.Output, tgtExpRunner.Output, Behavior);
                        cg.AddRunner(stateRunner);
                        #endregion.

                        StatusData nextStatus = new StatusData(status.GraphQuery, policyRunner.NodeIndex, policyRunner.LogProb,
                            policyRunner.PreSelActIndex, policyRunner.PreStatusIndex, policyRunner.StatusKey, stateRunner.Output, Behavior.Device);
                        status = nextStatus;
                    //}
                }
                #endregion.

                {
                    RewardFeedbackRunner rewardFeedRunner = new RewardFeedbackRunner(newQuery, mem, Behavior);
                    cg.AddRunner(rewardFeedRunner);
                }
                Queries.Add(newQuery);
            }

            if (Behavior.RunMode == DNNRunMode.Train)
            {
                if (BuilderParameters.TRAIN_STRATEGY == 0)
                {
                    RewardRunner rewardRunner = new RewardRunner(Queries, Behavior); //aMem,
                    cg.AddObjective(rewardRunner);
                }
            }
            else
            {
                if (eval_type > 0)
                {
                    //MAPPredictionRunner predRunner = new MAPPredictionRunner(Queries, Behavior);
                    //cg.AddRunner(predRunner);
                }
                else
                {
                    HitKRunner hitRunner = new HitKRunner(Queries, Behavior);
                    cg.AddRunner(hitRunner);
                }
            }
            cg.SetDelegateModel(model);
            return cg;
        }

        public override void Rock()
        {
            Logger.OpenLog(BuilderParameters.LogFile);

            DeviceType device = MathOperatorManager.SetDevice(BuilderParameters.GPUID);
            IMathOperationManager computeLib = MathOperatorManager.CreateInstance(device);

            Logger.WriteLog("Loading Training/Validation/Test Data.");
            DataPanel.Init();
            Logger.WriteLog("Load Data Finished.");

            NeuralWalkerModel model =
                BuilderParameters.SEED_MODEL.Equals(string.Empty) ?
                new NeuralWalkerModel(DataPanel.EntityDict.Count, DataPanel.RelationDict.Count, 
                                      BuilderParameters.N_EmbedDim, BuilderParameters.R_EmbedDim, BuilderParameters.RNN_Dim, device) :
                new NeuralWalkerModel(new BinaryReader(new FileStream(BuilderParameters.SEED_MODEL, FileMode.Open, FileAccess.Read)), device);
            model.InitOptimization(new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });

            EpisodicMemory Memory = new EpisodicMemory(true);
            ComputationGraph testCG = null;
            ComputationGraph validCG = null;
            // prediction. 0:beam search; 1:mcts search;
            //if (BuilderParameters.SCORE_TYPE == 0)
            {
                testCG = BuildComputationGraph(DataPanel.TestData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                                 1, 1, BuilderParameters.BeamSize, BuilderParameters.MAX_HOP, Memory, model,
                                                 BuilderParameters.MAP_EVAL,
                                                 new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

                validCG = BuildComputationGraph(DataPanel.DevData, DataPanel.GraphEnv, BuilderParameters.TestMiniBatchSize,
                                             1, 1, BuilderParameters.BeamSize, BuilderParameters.MAX_HOP, Memory, model,
                                             0,
                                             new RunnerBehavior() { RunMode = DNNRunMode.Predict, Device = device, Computelib = computeLib });

            }
            
            switch (BuilderParameters.RunMode)
            {
                case DNNRunMode.Train:
                    if (!Directory.Exists(BuilderParameters.ModelOutputPath)) Directory.CreateDirectory(BuilderParameters.ModelOutputPath);

                    ComputationGraph trainCG = BuildComputationGraph(BuilderParameters.DEV_AS_TRAIN > 0 ? DataPanel.DevData : DataPanel.TrainData, DataPanel.GraphEnv, BuilderParameters.MiniBatchSize,
                                                                     BuilderParameters.ROLL_NUM, BuilderParameters.MCTS_NUM, 1, BuilderParameters.MAX_HOP, Memory, model,
                                                                     0, new RunnerBehavior() { RunMode = DNNRunMode.Train, Device = device, Computelib = computeLib });
                    //testCG.Execute();
                    for (int iter = 0; iter < OptimizerParameters.Iteration; iter++)
                    {
                        //ActionSampler = new Random(10);
                        double loss = trainCG.Execute();
                        Logger.WriteLog("Iteration {0}, Avg Loss {1}", iter, loss);

                        if ((iter + 1) % BigLearn.DeepNet.BuilderParameters.ModelSaveIteration == 0)
                        {
                            Logger.WriteLog("Evaluation at Iteration {0}", iter);
                            //validCG.Execute();
                            testCG.Execute();
                            validCG.Execute();
                            using (BinaryWriter writer = new BinaryWriter(new FileStream(BuilderParameters.ModelOutputPath + "model." + iter.ToString(), FileMode.Create, FileAccess.Write)))
                            {
                                model.Serialize(writer);
                            }
                        }
                    }
                    break;
                case DNNRunMode.Predict:
                    testCG.Execute();
                    break;
            }
            Logger.CloseLog();
        }
        
        class DataUtil 
        {
            /// <summary>
            /// type 0: list all scores; 1: max aggregrate; 2 : sum aggregrate.
            /// </summary>
            /// <param name="query"></param>
            /// <param name="prob_wei"></param>
            /// <param name="score_wei"></param>
            /// <param name="type"></param>
            /// <returns></returns>
            public static List<Tuple<int, int, float>>[] AggregrateScore(List<GraphQueryData> query, float prob_wei, float score_wei, int aggType = 0)
            {
                List<Tuple<int, int, float>>[] result = new List<Tuple<int, int, float>>[query[0].BatchSize];
                //List<Tuple<float, float, float>>[] prob = new List<Tuple<float, float, float>>[query[0].BatchSize];
                for (int b = 0; b < query[0].BatchSize; b++)
                {
                    result[b] = new List<Tuple<int, int, float>>();
                    //prob[b] = new List<Tuple<float, float, float>>();
                }

                for (int m = 0; m < query.Count; m++)
                {
                    for (int i = 0; i < query[m].StatusPath.Last().BatchSize; i++) //.Results.Count; i++)
                    {
                        int t = query[m].StatusPath.Last().Step;
                        int b = i;
                        //int t = query[m].Results[i].Item1;
                        //int b = query[m].Results[i].Item2;
                        int origialB = query[m].StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = query[m].StatusPath[t].NodeID[b];

                        float v = prob_wei * query[m].StatusPath[t].GetLogProb(b); // query[m].Results[i].Item3; // (query[m].StatusPath[t].GetLogProb(b));
                       

                        // filter the edge which already exists.
                        if(query[m].BlackTargets[origialB].Contains(predId) && query[m].RawTarget[origialB] != predId)
                        {
                            continue;
                        }

                        result[origialB].Add(new Tuple<int, int, float>(predId, t, v));
                    }
                }
                
                return result;
            }

            public static HashSet<int>[] AggregrateUnique(List<GraphQueryData> query)
            {
                HashSet<int>[] result = new HashSet<int>[query[0].BatchSize];
                for (int b = 0; b < query[0].BatchSize; b++)
                {
                    result[b] = new HashSet<int>();
                }


                for (int m = 0; m < query.Count; m++)
                {
                    for (int i = 0; i < query[m].StatusPath.Last().BatchSize; i++) // .Results.Count; i++)
                    {
                        int t = query[m].StatusPath.Last().Step; // Results[i].Item1;
                        int b = i; // query[m].Results[i].Item2;
                        int origialB = query[m].StatusPath[t].GetOriginalStatsIndex(b);
                        int predId = query[m].StatusPath[t].NodeID[b];

                        // filter the edge which already exists.
                        if (query[m].BlackTargets[origialB].Contains(predId) && query[m].RawTarget[origialB] != predId)
                        {
                            continue;
                        }
                        if (!result[origialB].Contains(predId))
                        {
                            result[origialB].Add(predId);
                        }
                    }
                }

                return result;
            }

        }

        public class DataPanel 
        {
            public static GraphEnviroument GraphEnv { get; set; }

            public static DataEnviroument TrainData { get; set; }
            public static DataEnviroument TestData { get; set; }
            public static DataEnviroument DevData { get; set; }

            public static Dictionary<string, int> EntityDict { get; set; }
            public static Dictionary<string, int> RelationDict { get; set; }

            public static Dictionary<int, string> EntityLookup { get; set; }
            public static Dictionary<int, string> RelationLookup { get; set; }

            public static int ReverseRelation(int relation)
            {
                string relStr = RelationLookup[relation];
                if(relStr.StartsWith("_"))
                {
                    string newRel = relStr.Substring(1);
                    if(RelationDict.ContainsKey(newRel))
                    {
                        return RelationDict[newRel];
                    }
                }

                string newRel2 = "_" + relStr;
                if (RelationDict.ContainsKey(newRel2))
                {
                    return RelationDict[newRel2];
                }


                if(relStr.EndsWith("_inv"))
                {
                    string newRel3 = relStr.Substring(0, relStr.Length - 4);
                    if(RelationDict.ContainsKey(newRel3))
                    {
                        return RelationDict[newRel3];
                    }
                }

                string newRel4 = relStr + "_inv";
                if (RelationDict.ContainsKey(newRel4))
                {
                    return RelationDict[newRel4];
                }

                return -1;
                //throw new NotImplementedException("Cannot find the reverse Relation " + relStr);
            }

            public static string START_SYMBOL = "#START#";
            public static string END_SYMBOL = "#END#";
            public static int START_IDX { get { return RelationDict[START_SYMBOL]; } }
            public static int END_IDX { get { return RelationDict[END_SYMBOL]; } }

            public static int MAX_ACTION_NUM { get { return GraphEnv.MaxNeighborNum; } }
            
            public static void Init()
            {
                EntityDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + @"/vocab/entity_vocab.json"));
                RelationDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(File.ReadAllText(BuilderParameters.InputDir + @"/vocab/relation_vocab.json"));
                
                RelationDict.Add(END_SYMBOL, RelationDict.Count);
                RelationDict.Add(START_SYMBOL, RelationDict.Count);

                EntityLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in EntityDict) EntityLookup.Add(item.Value, item.Key);
 
                RelationLookup = new Dictionary<int, string>();
                foreach (KeyValuePair<string, int> item in RelationDict) RelationLookup.Add(item.Value, item.Key);

                GraphEnv = new GraphEnviroument(EntityDict, RelationDict, BuilderParameters.MAX_ACTION_NUM, BuilderParameters.InputDir + @"/graph.txt");

                TrainData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"/train.txt",
                                            new string[] { BuilderParameters.InputDir + @"/train.txt" });

                TestData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"/test.txt",
                                            new string[] { BuilderParameters.InputDir + @"/test.txt",
                                                           BuilderParameters.InputDir + @"/train.txt",
                                                           BuilderParameters.InputDir + @"/dev.txt",
                                                           BuilderParameters.InputDir + @"/graph.txt" });

                DevData = new DataEnviroument(EntityDict, RelationDict, BuilderParameters.InputDir + @"/dev.txt",
                                            new string[] { BuilderParameters.InputDir + @"/test.txt",
                                                           BuilderParameters.InputDir + @"/train.txt",
                                                           BuilderParameters.InputDir + @"/dev.txt",
                                                           BuilderParameters.InputDir + @"/graph.txt" });
            }

        }

    }
}
