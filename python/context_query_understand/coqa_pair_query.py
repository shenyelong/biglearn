"""
preprocess CoQA for extractive question answering
2019/1/22: distinguish yes-no and wh- question
"""

import argparse
import json
import re
import time
import string
from collections import Counter
from collections import OrderedDict
from pycorenlp import StanfordCoreNLP

import tokenization


nlp = StanfordCoreNLP('http://localhost:9000')
UNK = 'unknown'


def _str(s):
    """ Convert PTB tokens to normal tokens """
    if (s.lower() == '-lrb-'):
        s = '('
    elif (s.lower() == '-rrb-'):
        s = ')'
    elif (s.lower() == '-lsb-'):
        s = '['
    elif (s.lower() == '-rsb-'):
        s = ']'
    elif (s.lower() == '-lcb-'):
        s = '{'
    elif (s.lower() == '-rcb-'):
        s = '}'
    return s


def process(text):
    paragraph = nlp.annotate(text, properties={
                             'annotators': 'tokenize, ssplit',
                             'outputFormat': 'json',
                             'ssplit.newlineIsSentenceBreak': 'two'})

    output = {'word': [], 'offsets': []}

    try:
        for sent in paragraph['sentences']:
            for token in sent['tokens']:
                output['word'].append(_str(token['word']))
                output['offsets'].append((token['characterOffsetBegin'], token['characterOffsetEnd']))
    except:
        print("error in line 45: {}".format(paragraph))
    return output


def normalize_answer(s):
    """Lower text and remove punctuation, storys and extra whitespace."""

    def remove_articles(text):
        regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
        return re.sub(regex, ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))


def find_span_with_gt(context, offsets, ground_truth, w_start = -1, w_end = -1):
    """
    find answer from paragraph so that best f1 score is achieved
    """
    best_f1 = 0.0
    best_span = (len(context)-1, len(context)-1)
    best_pred = ''
    gt = normalize_answer(ground_truth).split()

    if(w_start == -1):
        w_start = 0
    if(w_end == -1):
        w_end = len(offsets)

    ls = [i for i in range(w_start, w_end) # len(offsets))
          if context[offsets[i][0]:offsets[i][1]].lower() in gt]

    for i in range(len(ls)):
        for j in range(i, len(ls)):
            pred = normalize_answer(context[offsets[ls[i]][0]: offsets[ls[j]][1]]).split()
            common = Counter(pred) & Counter(gt)
            num_same = sum(common.values())
            if num_same > 0:
                precision = 1.0 * num_same / len(pred)
                recall = 1.0 * num_same / len(gt)
                f1 = (2 * precision * recall) / (precision + recall)
                if f1 > best_f1:
                    best_f1 = f1
                    best_span = (offsets[ls[i]][0], offsets[ls[j]][1])
                    best_pred = context[offsets[ls[i]][0]: offsets[ls[j]][1]]
    return best_span, best_f1, best_pred

def f1Score(gt, pred):
    norm_gt = normalize_answer(gt).split()
    norm_pred = normalize_answer(pred).split()

    common = Counter(norm_pred) & Counter(norm_gt)
    num_same = sum(common.values())
    if num_same > 0:
        precision = 1.0 * num_same / len(norm_pred)
        recall = 1.0 * num_same / len(norm_gt)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1
    return 0

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    parser.add_argument("--vocab_file", default=None, type=str, help="tokenizer vocab file.")

    #parser.add_argument('--turn', default=None, type=str, help='dialogue turn.')

    args = parser.parse_args()

    tokenizer = tokenization.FullTokenizer(args.vocab_file) 

    with open(args.data_file, 'r') as f:
        dataset = json.load(f)

    data = OrderedDict()
    data['version'] = dataset['version']
    _datum_list = []
    yes_cnt = 0
    no_cnt = 0
    unk_cnt = 0
    true_cnt = 0
    false_cnt = 0
    fuz_num = 0

    _f1 = 0
    _num = 0
    _sf1 = 0
    _pnum = 0
    #_debug_f = open('detect.span', 'w')
    _wf1 = 0
    _wnum = 0
    
    turn_1_query = []
    turn_2_query = []
    bad_quality_num = 0
    for i, datum in enumerate(dataset['data']):
        if (i % 10 == 0):
            print("processig {} examples".format(i))
            
        _datum = OrderedDict()
        source = datum["source"]
        paragraph_id = datum["id"]
        filename = datum["filename"]
        context_str = datum["story"]
        _datum["source"] = source
        _datum["id"] = paragraph_id
        _datum["filename"] = filename
        _datum["story"] = context_str

        # question & answer
        questions = []
        answers = []

        idx = 0
        for question, answer in zip(datum['questions'], datum['answers']):
            assert question['turn_id'] == answer['turn_id']
            turn_id = question['turn_id']


            question_text = question['input_text']
            #question_tokens = tokenizer.tokenize(question_text)
            #question_idx = tokenizer.convert_tokens_to_ids(question_tokens)

            questions.append(question_text)

            ans_span_text = answer['span_text']
            ans_input_text = answer['input_text']
            ans_start = answer['span_start']
            ans_end = answer['span_end']

            answers.append(ans_input_text)

            if(turn_id == 1):
                turn_1_query.append(question_text)
            if(turn_id == 2):
                turn_2_query.append([questions[0],answers[0],questions[1]])
            idx += 1

    data['data'] = _datum_list
    print("# of questions : {}, {}".format(len(turn_1_query), len(turn_2_query)))
    #_debug_f.close()
    data = OrderedDict()

    with open(args.output_file, 'w') as output_file:
        json.dump(data, output_file, sort_keys=True, indent=4)




