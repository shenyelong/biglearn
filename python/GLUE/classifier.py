from __future__ import division
from torchvision import models
from torchvision import transforms
from PIL import Image

import argparse
import torch
import torchvision
import torch.nn as nn
import numpy as np
import math

import sys
import os
import clr as net_clr
from System import Array, IntPtr, Int32, Int64

from LanguageDataset import LanguageDataset
from torch.utils.data import DataLoader

from progress.bar import Bar as Bar

import tokenization

parser = argparse.ArgumentParser(description='Finetune Neural Language Classification.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--train', required=True, type=str, help='training dataset.')
#parser.add_argument('--vocab', type=str, default='vocab file')
parser.add_argument('--max_seq_length', type=int, default=128, help="maximum sequence length")

parser.add_argument('--workers', type=int, help='number of data loading workers', default=0)
parser.add_argument('--batch_size', type=int, default=64, help='input batch size')

parser.add_argument("--seed_bert", type=str, default="seed_bert", help="seed bert model")

parser.add_argument('--niter', default=5, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,10000:0.00003,50000:0.000001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')

parser.add_argument("--output_model", type=str, default="/data1/yelongshen/tmp_model/", help="seed bert model")

parser.add_argument('--init_type', type=int, default=1, help='transform init model 0:no init; 1:google base bert; 2:in house pretrained bert.')

parser.add_argument('--cate', type=int, default=2, help='classification category.')

parser.add_argument('--vocab', type=int, default=30522, help='vocab size') # 21128

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, IntArgument, CudaPieceFloat, CudaPieceInt, Session, NdArrayData, ComputationGraph, A_Func

from BigLearn import BaseBertModel, LayerStructure, Structure 
#from BigLearn.DeepNet.Language import Bert, DistBert
#from BigLearn.DeepNet.Image import ImageNet_VGG16Net, DistImageNet_VGG16Net

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create storm enviroument")
env_mode = DeviceBehavior(0).TrainMode

dataset = LanguageDataset(args.train, args.max_seq_length, args.batch_size)

data_loader = DataLoader(dataset, batch_size=args.batch_size, num_workers=args.workers, shuffle=True, drop_last=True)


class Discriminator:
    ## construct model 
    def __init__(self, seed_bert, cate, batch_size, max_seq_length, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length

        ## init pretrained bert. (google)
        if(args.init_type == 0):
            self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(behavior.Device))
        elif(args.init_type == 1):
            # int frozenlayer, int mvocabSize, int mcate, int mlayer, int mdim, string meta_file, string bin_file, DeviceType device) 
            self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(-1, args.vocab, 2, 12, 768, seed_bert + 'exp.label', seed_bert + 'exp.bin', behavior.Device))
        elif(args.init_type == 2):
            self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(behavior.Device))
            print('bert base parameter number', self.Models['bert_base'].ParamNumber)
            self.Models['bert_base'].Load(seed_bert)

        ## init pretrained bert. (in house)
        #self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(behavior.Device))
        
        # self.Session.Model.AddLayer(EmbedStructure(vocab_size, in_dim, self.Behavior.Device))
        # self.Models['lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        self.Models['classifier'] =  self.Session.Model.AddLayer(LayerStructure(self.Models['bert_base'].embed, cate, A_Func.Linear, True, behavior.Device))
        #self.Session.Model.AddLayer(
        
        self.Session.Model.AllocateGradient(self.Behavior.Device)

        self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, 0.0001, 1.0))
        #StructureLearner.AdamLearner(args.lr, 0.5, 0.999)) # (args.lr, 0.5, 0.999))

        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.segments = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        self.labels = CudaPieceInt(batch_size, self.Behavior.Device)

        bert_feature = self.Session.BaseBertFeaturizer(self.Models['bert_base'], self.tokens, self.segments, self.masks, batch_size, max_seq_length)

        self.slice_feature = bert_feature.Item2

        self.pred = self.Session.FNN(self.slice_feature, self.Models['classifier'])
        self.loss = self.Session.SoftmaxLoss(self.pred, self.labels)

    def Train(self, tokens, segments, masks, labels):
        ## load 
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.segments.CopyTorch(IntPtr.op_Explicit(Int64(segments.data_ptr())), self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(masks.data_ptr())), self.batch_size * self.max_seq_length)
        
        self.labels.CopyTorch(IntPtr.op_Explicit(Int64(labels.data_ptr())),  self.batch_size)

        self.Session.StepV3Run()

        self.Session.StepV3Update()
        
        return self.loss.Value

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

classifier = Discriminator(args.seed_bert, args.cate, args.batch_size, args.max_seq_length, env_mode)

#classifier.Session.InitOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))
#classifier.Build(args.batch_size, args.max_seq_length)

for epoch in range(args.niter):

    classifier.Session.Init()
    
    losses = AverageMeter()
    
    bar = Bar('Training', max=len(data_loader))

    for batch_idx, data in enumerate(data_loader):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        tokens = data['input_ids']
        segments = data['input_segments']
        masks = data['input_masks']
        labels = data['label']

        loss = classifier.Train(tokens, segments, masks, labels)
        
        #classifier.pred.Output.SyncToCPU()
        #print(classifier.pred.Output[0], classifier.pred.Output[1], classifier.pred.Output[2])
        losses.update(loss)
        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Loss {losses.val:.4f} ({losses.avg:.4f})'.format(
                    batch=batch_idx + 1,
                    size=len(data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    losses=losses                  
                    )
        bar.next()
    bar.finish()
    classifier.Session.Complete()

    classifier.Session.Model.Save(args.output_model + '.'+str(epoch)+'.storm.ckpt')
