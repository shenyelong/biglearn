from __future__ import division
from torchvision import models
from torchvision import transforms
from PIL import Image

import argparse
import torch
import torchvision
import torch.nn as nn
import numpy as np
import math

import sys
import os
import clr as net_clr
from System import Array, IntPtr, Int32, Int64

from LanguageDataset import LanguageDataset
from torch.utils.data import DataLoader

from progress.bar import Bar as Bar

import tokenization

parser = argparse.ArgumentParser(description='Finetune Neural Language Classification.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--test', required=True, type=str, help='training dataset.')
#parser.add_argument('--vocab', type=str, default='vocab file')
parser.add_argument('--max_seq_length', type=int, default=128, help="maximum sequence length")

parser.add_argument('--workers', type=int, help='number of data loading workers', default=0)
parser.add_argument('--batch_size', type=int, default=64, help='input batch size')

parser.add_argument("--init_ckpt", type=str, default="/data1/yelongshen/tmp_model/", help="saved model")

parser.add_argument('--cate', type=int, default=2, help='classification category.')

parser.add_argument('--vocab', type=int, default=30522, help='vocab size') # 21128

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, IntArgument, CudaPieceFloat, CudaPieceInt, Session, NdArrayData, ComputationGraph, A_Func

from BigLearn import BaseBertModel, LayerStructure, Structure 
#from BigLearn.DeepNet.Language import Bert, DistBert
#from BigLearn.DeepNet.Image import ImageNet_VGG16Net, DistImageNet_VGG16Net

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create storm enviroument")
env_mode = DeviceBehavior(0).PredictMode

dataset = LanguageDataset(args.test, args.max_seq_length, args.batch_size)
data_loader = DataLoader(dataset, batch_size=args.batch_size, num_workers=args.workers, shuffle=True, drop_last=True)

class Discriminator:
    ## construct model 
    def __init__(self, seed_bert, cate, batch_size, max_seq_length, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length
        self.cate = cate

        # int mcate, int mlayer, int frozenlayer, int mvocabSize, DeviceType device
        self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(2, 12, -1, args.vocab, behavior.Device)) 
            #BaseBertModel(behavior.Device))

        ## init pretrained bert. (google)
        #if(args.init_type == 0):
        #    self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(behavior.Device))
        #elif(args.init_type == 1):
        #    self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(seed_bert + 'exp.label', seed_bert + 'exp.bin', behavior.Device))
        #elif(args.init_type == 2):
        #    self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(behavior.Device))
        #    print('bert base parameter number', self.Models['bert_base'].ParamNumber)
        #    self.Models['bert_base'].Load(seed_bert)

        ## init pretrained bert. (in house)
        #self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(behavior.Device))
        
        # self.Session.Model.AddLayer(EmbedStructure(vocab_size, in_dim, self.Behavior.Device))
        # self.Models['lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        self.Models['classifier'] =  self.Session.Model.AddLayer(LayerStructure(self.Models['bert_base'].embed, cate, A_Func.Linear, True, behavior.Device))
        #self.Session.Model.AddLayer(
        
        #self.Session.Model.AllocateGradient(self.Behavior.Device)
        #self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(0.0, 1.0, '0:0.0', 0.0, 1.0))
        #StructureLearner.AdamLearner(args.lr, 0.5, 0.999)) # (args.lr, 0.5, 0.999))

        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.segments = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        #self.labels = CudaPieceInt(batch_size, self.Behavior.Device)

        bert_feature = self.Session.BaseBertFeaturizer(self.Models['bert_base'], self.tokens, self.segments, self.masks, batch_size, max_seq_length)

        self.slice_feature = bert_feature.Item2

        self.pred = self.Session.FNN(self.slice_feature, self.Models['classifier'])
        #self.loss = self.Session.SoftmaxLoss(self.pred, self.labels)

    def Predict(self, tokens, segments, masks, output):
        ## load 
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.segments.CopyTorch(IntPtr.op_Explicit(Int64(segments.data_ptr())), self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(masks.data_ptr())), self.batch_size * self.max_seq_length)
        
        #self.labels.CopyTorch(IntPtr.op_Explicit(Int64(labels.data_ptr())),  self.batch_size)
        self.Session.ForwardV3()
        
        self.pred.Output.PasteTorch(IntPtr.op_Explicit(Int64(output.data_ptr())),  self.batch_size * self.cate)

        #self.Session.StepV3Run()
        #self.Session.StepV3Update()
        
        #return self.loss.Value

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def accuracy(output, target):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        batch_size = target.size(0)

        _, pred = output.topk(1, 1, True, True)
        pred = pred.t().type(torch.IntTensor)
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        return correct.view(-1).float().sum(0, keepdim=True).mul_(100.0 / batch_size).item()

predictor = Discriminator(args.init_ckpt, args.cate, args.batch_size, args.max_seq_length, env_mode)

predictor.Session.Model.Load(args.init_ckpt)

#classifier.Session.InitOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))
#classifier.Build(args.batch_size, args.max_seq_length)

for epoch in range(1):

    predictor.Session.Init()
    
    acc = AverageMeter()
    
    output = torch.zeros([args.batch_size, args.cate], dtype=torch.float)

    bar = Bar('Predicting', max=len(data_loader))

    for batch_idx, data in enumerate(data_loader):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        tokens = data['input_ids']
        segments = data['input_segments']
        masks = data['input_masks']
        labels = data['label']

        predictor.Predict(tokens, segments, masks, output)
        
        acc1 = accuracy(output, labels)

        acc.update(acc1)

        #classifier.pred.Output.SyncToCPU()
        #print(classifier.pred.Output[0], classifier.pred.Output[1], classifier.pred.Output[2])
        #losses.update(loss)
        
        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | acc {acc.val:.4f} ({acc.avg:.4f})'.format(
                    batch=batch_idx + 1,
                    size=len(data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    acc=acc                  
                    )
        bar.next()
    bar.finish()
    predictor.Session.Complete()

    #classifier.Session.Model.Save(args.output_model + '.'+str(epoch)+'.storm.ckpt')
