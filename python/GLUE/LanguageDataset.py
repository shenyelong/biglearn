from torch.utils.data import Dataset
import tqdm
import torch
import random
import collections
from collections import OrderedDict

class LanguageDataset(Dataset):

    def load_corpus(self, corpus_path):
        all_documents = []

        with open(corpus_path, "r") as reader:
            while True:
                line = reader.readline()
                if not line:
                    break
                line = line.strip().strip('\n')
                items = line.split('\t')
                label_id = int(items[0])
                input_ids = [ int(m) for m in items[1].split(' ') ]
                input_masks = [ int(m) for m in items[2].split(' ') ]
                input_segments = [ int(m) for m in items[3].split(' ') ]

                _datum = collections.OrderedDict()
                _datum['label'] = label_id
                _datum['input_ids'] = input_ids
                _datum['input_masks'] = input_masks
                _datum['input_segments'] = input_segments
                all_documents.append(_datum)
        return all_documents

    def __init__(self, corpus_path, max_seq_length, fill_batch, rseed = 110):
        self.corpus_path = corpus_path
        self.max_seq_length = max_seq_length
        self.fill_batch = fill_batch

        self.all_documents = self.load_corpus(corpus_path)
        self.rng = random.Random(rseed)

        print('all document number', len(self.all_documents))
        self.corpus_lines = len(self.all_documents)

    def __len__(self):
        return int((self.corpus_lines + self.fill_batch - 1)/ self.fill_batch) * self.fill_batch

    def __getitem__(self, item):
        if(item >= self.corpus_lines):
            item = self.rng.randint(0, self.corpus_lines - 1)

        _datum = self.all_documents[item]
        
        return {key: torch.tensor(value, dtype=torch.int32) for key, value in _datum.items()}

        
