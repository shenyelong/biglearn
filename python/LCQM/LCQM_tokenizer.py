# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""BERT finetuning runner."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import csv
import os
#import modeling
#import optimization
import tokenization
import argparse

#import tensorflow as tf

parser = argparse.ArgumentParser(description='data preprocess pipeline.')

parser.add_argument('--vocab_file', default='vocab', type=str, help='vocab file')

parser.add_argument('--do_lower_case', dest='do_lower_case', action='store_true',
                    help='lower case')

parser.add_argument('--data_dir', default='', type=str, metavar='PATH_IN',
                    help='path to input data (default: none)')

parser.add_argument('--output_dir', default='', type=str, metavar='PATH_OUT',
                    help='path to output data (default: none)')

parser.add_argument('--max_seq_length', default=256, type=int, metavar='L',
                    help='max length of input sequence')

args = parser.parse_args()

class InputExample(object):
  """A single training/test example for simple sequence classification."""

  def __init__(self, text_a, text_b=None, label=None):
    """Constructs a InputExample.

    Args:
      guid: Unique id for the example.
      text_a: string. The untokenized text of the first sequence. For single
        sequence tasks, only this sequence must be specified.
      text_b: (Optional) string. The untokenized text of the second sequence.
        Only must be specified for sequence pair tasks.
      label: (Optional) string. The label of the example. This should be
        specified for train and dev examples, but not for test examples.
    """
    #self.guid = guid
    self.text_a = text_a
    self.text_b = text_b
    self.label = label


class InputFeatures(object):
  """A single set of features of data."""

  def __init__(self, input_ids, input_mask, segment_ids, label_id):
    self.input_ids = input_ids
    self.input_mask = input_mask
    self.segment_ids = segment_ids
    self.label_id = label_id


class DataProcessor(object):
  """Base class for data converters for sequence classification data sets."""

  def get_train_examples(self, data_dir):
    """Gets a collection of `InputExample`s for the train set."""
    raise NotImplementedError()

  def get_dev_examples(self, data_dir):
    """Gets a collection of `InputExample`s for the dev set."""
    raise NotImplementedError()

  def get_test_examples(self, data_dir):
    """Gets a collection of `InputExample`s for prediction."""
    raise NotImplementedError()

  def get_labels(self):
    """Gets the list of labels for this data set."""
    raise NotImplementedError()

  @classmethod
  def _read_tsv(cls, input_file, quotechar=None):
    """Reads a tab separated value file."""
    with open(input_file, "r") as f:
      reader = csv.reader(f, delimiter="\t", quotechar=quotechar)
      lines = []
      for line in reader:
        lines.append(line)
      return lines

class LcqmProcessor(DataProcessor):
  """Processor for the MultiNLI data set (GLUE version)."""

  def get_train_examples(self, data_dir):
    """See base class."""
    return self._create_examples(self._read_tsv(os.path.join(data_dir, "train.tsv")), "train")

  def get_dev_examples(self, data_dir):
    """See base class."""
    return self._create_examples(self._read_tsv(os.path.join(data_dir, "dev_matched.tsv")), "dev_matched")

  def get_test_examples(self, data_dir):
    """See base class."""
    return self._create_examples(self._read_tsv(os.path.join(data_dir, "test_matched.tsv")), "test")

  def get_labels(self):
    """See base class."""
    return ["0", "1"]

  def _create_examples(self, lines, set_type):
    """Creates examples for the training and dev sets."""
    examples = []
    for (i, line) in enumerate(lines):
      #if i == 0:
      #  continue
      #guid = "%s-%s" % (set_type, tokenization.convert_to_unicode(line[0]))
      text_a = tokenization.convert_to_unicode(line[0])
      text_b = tokenization.convert_to_unicode(line[1])
      #if set_type == "test":
      #  label = "contradiction"
      #else:
      label = tokenization.convert_to_unicode(line[2])
      examples.append(InputExample(text_a=text_a, text_b=text_b, label=label))
    return examples


def convert_single_example(ex_index, example, label_list, max_seq_length, tokenizer):
  """Converts a single `InputExample` into a single `InputFeatures`."""
  label_map = {}
  for (i, label) in enumerate(label_list):
    label_map[label] = i

  tokens_a = tokenizer.tokenize(example.text_a)
  tokens_b = None
  if example.text_b:
    tokens_b = tokenizer.tokenize(example.text_b)

  if tokens_b:
    # Modifies `tokens_a` and `tokens_b` in place so that the total
    # length is less than the specified length.
    # Account for [CLS], [SEP], [SEP] with "- 3"
    _truncate_seq_pair(tokens_a, tokens_b, max_seq_length - 3)
  else:
    # Account for [CLS] and [SEP] with "- 2"
    if len(tokens_a) > max_seq_length - 2:
      tokens_a = tokens_a[0:(max_seq_length - 2)]

  # The convention in BERT is:
  # (a) For sequence pairs:
  #  tokens:   [CLS] is this jack ##son ##ville ? [SEP] no it is not . [SEP]
  #  type_ids: 0     0  0    0    0     0       0 0     1  1  1  1   1 1
  # (b) For single sequences:
  #  tokens:   [CLS] the dog is hairy . [SEP]
  #  type_ids: 0     0   0   0  0     0 0
  #
  # Where "type_ids" are used to indicate whether this is the first
  # sequence or the second sequence. The embedding vectors for `type=0` and
  # `type=1` were learned during pre-training and are added to the wordpiece
  # embedding vector (and position vector). This is not *strictly* necessary
  # since the [SEP] token unambiguously separates the sequences, but it makes
  # it easier for the model to learn the concept of sequences.
  #
  # For classification tasks, the first vector (corresponding to [CLS]) is
  # used as as the "sentence vector". Note that this only makes sense because
  # the entire model is fine-tuned.
  tokens = []
  segment_ids = []

  tokens.append("[CLS]")
  segment_ids.append(0)
  
  for token in tokens_a:
    tokens.append(token)
    segment_ids.append(0)
  tokens.append("[SEP]")
  segment_ids.append(0)

  if tokens_b:
    for token in tokens_b:
      tokens.append(token)
      segment_ids.append(1)
    tokens.append("[SEP]")
    segment_ids.append(1)

  input_ids = tokenizer.convert_tokens_to_ids(tokens)

  # The mask has 1 for real tokens and 0 for padding tokens. Only real
  # tokens are attended to.
  input_mask = [1] * len(input_ids)

  # Zero-pad up to the sequence length.
  while len(input_ids) < max_seq_length:
    input_ids.append(0)
    input_mask.append(0)
    segment_ids.append(0)

  assert len(input_ids) == max_seq_length
  assert len(input_mask) == max_seq_length
  assert len(segment_ids) == max_seq_length
  
  label_id = label_map[example.label]
  #if ex_index < 5:
  #  tf.logging.info("*** Example ***")
  #  tf.logging.info("guid: %s" % (example.guid))
  #  tf.logging.info("tokens: %s" % " ".join([tokenization.printable_text(x) for x in tokens]))
  #  tf.logging.info("input_ids: %s" % " ".join([str(x) for x in input_ids]))
  #  tf.logging.info("input_mask: %s" % " ".join([str(x) for x in input_mask]))
  #  tf.logging.info("segment_ids: %s" % " ".join([str(x) for x in segment_ids]))
  #  tf.logging.info("label: %s (id = %d)" % (example.label, label_id))

  feature = InputFeatures(
      input_ids=input_ids,
      input_mask=input_mask,
      segment_ids=segment_ids,
      label_id=label_id)
  return feature


def file_based_convert_examples_to_features(examples, label_list, max_seq_length, tokenizer, output_file):
  """Convert a set of `InputExample`s to a TFRecord file."""
  # writer = tf.python_io.TFRecordWriter(output_file)
  writer = open(output_file, 'w')
  for (ex_index, example) in enumerate(examples):
    if ex_index % 10000 == 0:
      print("Writing example " , str(ex_index), " of ", str(ex_index * 1.0 / len(examples)))
      #tf.logging.info("Writing example %d of %d" % (ex_index, len(examples)))

    feature = convert_single_example(ex_index, example, label_list, max_seq_length, tokenizer)

    def create_int_feature(values):
      return " ".join([str(x) for x in values])
    
    writer.write(create_int_feature([feature.label_id]) + '\t')
    writer.write(create_int_feature(feature.input_ids) + '\t')
    writer.write(create_int_feature(feature.input_mask) + '\t')
    writer.write(create_int_feature(feature.segment_ids) + '\n')

  ## we don't have tf record.    
  writer.close()



def _truncate_seq_pair(tokens_a, tokens_b, max_length):
  """Truncates a sequence pair in place to the maximum length."""

  # This is a simple heuristic which will always truncate the longer sequence
  # one token at a time. This makes more sense than truncating an equal percent
  # of tokens from each, since if one sequence is very short then each token
  # that's truncated likely contains more information than a longer sequence.
  while True:
    total_length = len(tokens_a) + len(tokens_b)
    if total_length <= max_length:
      break
    if len(tokens_a) > len(tokens_b):
      tokens_a.pop()
    else:
      tokens_b.pop()

def main():
  # take mrpc as an example.
  global args

  processor = LcqmProcessor() # processors[task_name]()

  label_list = processor.get_labels()

  tokenizer = tokenization.FullTokenizer(vocab_file=args.vocab_file, do_lower_case=args.do_lower_case)

  if True: #FLAGS.do_train:
    train_examples = processor.get_train_examples(args.data_dir)
    train_file = os.path.join(args.output_dir, "train.idx")
    file_based_convert_examples_to_features(train_examples, label_list, args.max_seq_length, tokenizer, train_file)
    
  if True: #FLAGS.do_eval:
    eval_examples = processor.get_dev_examples(args.data_dir)
    eval_file = os.path.join(args.output_dir, "dev.idx")
    file_based_convert_examples_to_features(eval_examples, label_list, args.max_seq_length, tokenizer, eval_file)

  if True: #FLAGS.do_predict:
    predict_examples = processor.get_test_examples(args.data_dir)
    predict_file = os.path.join(args.output_dir, "test.idx")
    file_based_convert_examples_to_features(predict_examples, label_list, args.max_seq_length, tokenizer, predict_file)

if __name__ == "__main__":
  main()
