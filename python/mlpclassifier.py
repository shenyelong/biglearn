from sklearn.neural_network import MLPClassifier
from sklearn.datasets import load_svmlight_file
import sys
import io
import numpy as np
from sklearn import metrics

data_path = sys.argv[1]


train = load_svmlight_file(data_path+'/train.index')
dev = load_svmlight_file(data_path+'/dev.index')
test = load_svmlight_file(data_path+'/test.index')



clf = MLPClassifier(solver='adam', alpha=1e-4, hidden_layer_sizes=(8, 1), activation='relu', random_state=1)

clf.fit(train[0], train[1])

dev_score = clf.predict_proba(dev[0])
test_score = clf.predict_proba(test[0])

dev_pred = dev_score[:,1]
test_pred = test_score[:,1]

def Auc(pred, true):
	fpr, tpr, thresholds = metrics.roc_curve(true, pred)
	return metrics.auc(fpr, tpr)

test_auc = Auc(test_pred, test[1])
print('test-auc:'+str(test_auc))

dev_auc = Auc(dev_pred, dev[1])
print('dev-auc:'+str(dev_auc))

## dota 2
## solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(100, 1), random_state=1		  					test-auc:0.603394694386 & dev-auc:0.588088562378	
## solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(100, 1), activation='tanh', random_state=1. 		test-auc:0.599829269854	& dev-auc:0.587614337193
## solver='adam', alpha=1e-5, hidden_layer_sizes=(100, 1), activation='tanh', random_state=1		test-auc:0.581267534222 & dev-auc:0.566024700779
## solver='adam', alpha=1e-5, hidden_layer_sizes=(100, 100, 1), activation='relu', random_state=1	test-auc:0.556972908319 & dev-auc:0.545509206342
## solver='adam', alpha=1e-5, hidden_layer_sizes=(300, 1), activation='relu', random_state=1		test-auc:0.555148863761 & dev-auc:0.541819694931
## solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(10, 1), activation='relu', random_state=1		test-auc:0.500090876045 & dev-auc:0.50009049093
## solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(100, 1), activation='relu', random_state=1		test-auc:0.603394694386 & dev-auc:0.588088562378

## mushroom
## solver='adam', alpha=1e-5, hidden_layer_sizes=(300, 1), activation='relu', random_state=1		test-auc:0.555148863761 & dev-auc:0.541819694931

## poker.

## income
## test-auc:0.8474047679237287
## dev-auc:0.8532695855136845
## (solver='adam', alpha=1e-4, hidden_layer_sizes=(8, 1), activation='relu', random_state=1) test-auc:0.8768435490133719. dev-auc:0.879714165132743

## income_real
## (solver='adam', alpha=1e-4, hidden_layer_sizes=(8, 1), activation='relu', random_state=1) test-auc:0.8925318050116455 dev-auc:0.8849544744693136
