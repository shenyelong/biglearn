import sys
import io
import os
os.environ["CUDA_VISIBLE_DEVICES"]="-1" 

import tensorflow as tf
## load tensorflow bert model, and save the model checkpoints into binary. 

tf.reset_default_graph()

sess = tf.Session()

#uncased_L-24_H-1024_A-16/
meta = tf.train.import_meta_graph("./uncased_L-24_H-1024_A-16/bert_model.ckpt.meta")
saver = tf.train.Saver()
saver.restore(sess, "./uncased_L-24_H-1024_A-16/bert_model.ckpt")

f_meta = open('./uncased_L-24_H-1024_A-16/exp.label','w')
f_bin = open('./uncased_L-24_H-1024_A-16/exp.bin','bw')

for i in range(0, len(saver._var_list)):
    name = saver._var_list[i].name
    dat = saver._var_list[i].eval(session=sess).flatten()
    f_meta.write(name+'\t'+str(len(dat))+'\n')
    f_bin.write(dat)

f_meta.close()
f_bin.close()

