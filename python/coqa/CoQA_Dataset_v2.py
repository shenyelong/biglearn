from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import json
import math

import pickle
import numpy as np

import os
import tempfile
import argparse

import tqdm
from tqdm import tqdm, trange

import random
import collections

import torch
from torch.utils.data import Dataset
import tokenize

class CoQAExample(object):
    """
    a single training/test example for CoQA dataset.
    """
    def __init__(self,
                 paragraph_id,
                 turn_id,
                 doc,
                 doc_tokens,
                 doc_w2tokens,
                 doc_woffset,
                 doc_wnum,
                 query_tokens,
                 query_mask,
                 start_position=None,
                 end_position=None,
                 yes_no_flag=None,
                 yes_no_ans=None):
        self.paragraph_id = paragraph_id
        self.turn_id = turn_id

        self.doc = doc
        self.doc_tokens = doc_tokens
        self.doc_w2tokens = doc_w2tokens
        self.doc_woffset = doc_woffset
        self.doc_wnum = doc_wnum

        self.query_tokens = query_tokens
        self.query_mask = query_mask

        self.start_position = start_position
        self.end_position = end_position
        self.yes_no_flag = yes_no_flag
        self.yes_no_ans = yes_no_ans

class CoQA_Dataset_v2(Dataset):
    ## corpus path, appending queries into history.
    ## select available turn_ids.

    def __init__(self, corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, fill_batch, is_enumate_doc_span, turn_ids, skip_bad = 0, qorder = 0, slide_win = 128, rseed = 110):

        self.corpus_path = corpus_path
        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length
        self.max_query_history = max_query_history
        
        self.cls_token = cls_token
        self.sep_token = sep_token
        self.pad_token = pad_token
        self.vocabsize = vocabsize

        self.turn_ids = turn_ids
        self.slide_win = slide_win

        ## enumate examples as doc span. 
        self.is_enumate_doc_span = is_enumate_doc_span
        self.fill_batch = fill_batch

        self.para_dict = {}
        self.para_list = []

        self.skip_bad = skip_bad
        self.q_order = qorder

        self.examples = self.read_coqa_examples(corpus_path)
        self.doc_spans = self.get_example_spans(self.examples)

        self.rng = random.Random(rseed)

        self.example_num = len(self.examples)
        self.doc_span_num = len(self.doc_spans)

        print('all example number :', self.example_num, 'all doc span number :', self.doc_span_num)

    def __len__(self):
        if(self.is_enumate_doc_span):
            return int((self.doc_span_num + self.fill_batch - 1)/ self.fill_batch) * self.fill_batch
        else:
            return int((self.example_num + self.fill_batch - 1)/ self.fill_batch) * self.fill_batch

    def batch_num(self):
        if(self.is_enumate_doc_span):
            return int((self.doc_span_num + self.fill_batch - 1)/ self.fill_batch) 
        else:
            return int((self.example_num + self.fill_batch - 1)/ self.fill_batch)

    def __getitem__(self, item):
        if(self.is_enumate_doc_span):
            (p_id, turn_id, doc_word_num, doc_word_offset, doc_start, doc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans) = self.doc_span_sample(item)
        else:
            (p_id, turn_id, doc_word_num, doc_word_offset, doc_start, doc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans) = self.example_sample(item)

        tok_len = len(tokens)
        padding = [self.pad_token for _ in range(self.max_seq_length - tok_len)]
        
        tokens.extend(padding)
        segment_ids.extend(padding)
        doc_mask.extend(padding)
        query_mask.extend(padding)

        one_padding = [1 for _ in range(tok_len)]
        zero_padding = [0 for _ in range(self.max_seq_length - tok_len)]

        mask_ids = []
        mask_ids.extend(one_padding)
        mask_ids.extend(zero_padding)

        output = {"para_id" : p_id,
                  "turn_id" : turn_id,

                  "doc_word_num" : doc_word_num,
                  "doc_word_offset" : doc_word_offset,
                  
                  "doc_start" : doc_start,
                  "doc_end" : doc_end,

                  "token_input" : tokens,
                  "seg_input" : segment_ids,
                  "mask_input" : mask_ids,

                  "doc_mask" : doc_mask,
                  "query_mask" : query_mask,

                  "start_position" : start_position,
                  "end_position" : end_position,
                  
                  "yes_no_ans": yes_no_ans }

        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
    
    def paragraph_ip2id(self, ipx):
        if(not ipx in self.para_dict):
            self.para_dict[ipx] = len(self.para_list)
            self.para_list.append(ipx)
        return self.para_dict[ipx]
        
    def paragraph_id2ip(self, idx):
        return self.para_list[idx]

    def getspan(self, pid, startword, endword):
        idx = self.paragraph_ip2id(pid)
        story = self.story[idx]
        story_wordoffset = self.story_wordoffset[idx]
        soffset = story_wordoffset[startword][0]
        eoffset = story_wordoffset[endword][1]
        return story[soffset : eoffset]

    def read_coqa_examples(self, input_file):
        """
        read a CoQA json file into a list of QA examples
        """
        with open(input_file, "r", encoding='utf-8') as reader:
            input_data = json.load(reader)['data']
        
        self.story = {}
        self.story_wordoffset = {}
        
        span_stat = {}

        examples = []
        for entry in input_data:
            # process story text
            paragraph_id = entry["id"]
            idx = self.paragraph_ip2id(paragraph_id)

            story = entry["story"]
            story_tokens = entry["story_subtoken_idx"]
            story_word2subtokens = entry["story_word2subtoken"]
            story_wordoffset = entry["story_wordoffset"]
            story_wordnum = len(story_wordoffset)

            assert len(story_word2subtokens) == len(story_wordoffset) + 1

            self.story[idx] = story
            self.story_wordoffset[idx] = story_wordoffset

            question_history = []
            ans_history = []

            for (question, ans) in zip(entry['questions'], entry['answers']):

                cur_question_tokens = question['input_text_subtoken_idx']
                question_history.append(cur_question_tokens)

                turn_ans = ans['input_text_tokens']
                ans_history.append(turn_ans)

                question_id = int(question["turn_id"])
                ans_id = int(ans["turn_id"])
                
                assert question_id == ans_id

                if not ans_id in self.turn_ids:
                    continue

                yes_no_flag = int(ans["yes_no_flag"])
                yes_no_ans = int(ans["yes_no_ans"])

                start_position = int(ans['word_span_start'])
                end_position = int(ans['word_span_end'])
                
                if(self.skip_bad == 1):
                    quality = int(ans['quality'])
                    if(quality == 0):
                        continue

                if(end_position < start_position):
                    print(end_position, start_position)

                span_len = end_position - start_position

                if(span_len in span_stat):
                    span_stat[span_len] += 1
                else:
                    span_stat[span_len] = 1

                his_question_tokens = []
                his_question_mask = []

                for h in range(min(question_id, self.max_query_history), 1, -1):
                    
                    mask_padding = [0 for _ in range(len(question_history[question_id - h]) + 1 + len(ans_history[question_id - h]) + 1)]

                    #if(self.q_order == 0):
                    his_question_tokens.extend(question_history[question_id - h])
                    his_question_tokens.append(self.sep_token)

                    his_question_tokens.extend(ans_history[question_id - h])
                    his_question_tokens.append(self.sep_token)
                    
                    his_question_mask.extend(mask_padding)
                    #else:
                    #    his_question_tokens = question_history[question_id - h] + [ self.sep_token ] + his_question_tokens    
                    #    his_question_mask = mask_padding + his_question_mask 

                his_question_tokens.extend(question_history[question_id - 1])
                his_question_tokens.append(self.sep_token)
                
                his_question_mask.extend([1 for _ in range(len(question_history[question_id - 1]) + 1)])

                example = CoQAExample(
                    paragraph_id=idx,
                    turn_id=question_id,
                    doc=story,
                    doc_tokens=story_tokens,
                    doc_w2tokens=story_word2subtokens,
                    doc_woffset=story_wordoffset,
                    doc_wnum=story_wordnum,
                    query_tokens=his_question_tokens,
                    query_mask=his_question_mask,
                    start_position=start_position,
                    end_position=end_position,
                    yes_no_flag=yes_no_flag,
                    yes_no_ans=yes_no_ans)
                examples.append(example)
        print(span_stat)
        return examples

    def get_example_spans(self, examples):
        doc_spans = []
        max_num_tokens = self.max_seq_length - 2
        example_idx = 0
        span_count = [0] * 20
        for example in examples:
            query_len = min(self.max_query_length, len(example.query_tokens))
            max_doc_len = max_num_tokens - query_len
            
            all_doc_tokens = (example.doc_tokens)
            #doc_spans = []
            seg = 0
            start_offset = 0
            while start_offset < len(all_doc_tokens):
                length = len(all_doc_tokens) - start_offset
                if length > max_doc_len:
                    length = max_doc_len
                doc_spans.append((example_idx, start_offset, length))
                if start_offset + length == len(all_doc_tokens):
                    break
                start_offset += min(length, self.slide_win)
                seg += 1
            if(seg >= 19):
                seg = 19    
            span_count[seg] += 1    
            #doc_segments.append(doc_spans)
            example_idx += 1

        for i in range(0, 20):
            print("doc span distribution ", i, ":", span_count[i])
        return doc_spans

    def get_word_mask(self, word2token, tokenlen):
        word_mask = []
        widx = 0
        for i in range(0, tokenlen):
            if(word2token[widx] == i):
                word_mask.append(1)
                widx += 1
            else:
                word_mask.append(0)
        return word_mask

    def doc_span_sample(self, idx):
        if(idx >= self.doc_span_num):
            idx = self.rng.randint(0, self.doc_span_num - 1)
        doc_span = self.doc_spans[idx]

        example_id = doc_span[0]
        start_offset = doc_span[1]
        length = doc_span[2]

        example = self.examples[example_id]

        max_num_tokens = self.max_seq_length - 2

        tokens_a = example.doc_tokens
        doc_word_mask = self.get_word_mask(example.doc_w2tokens, len(tokens_a))

        tokens_b = example.query_tokens
        tokens_b_mask = example.query_mask

        if len(tokens_b) > self.max_query_length:
            if(self.q_order == 0):
                tokens_b = tokens_b[-1 * self.max_query_length : ]
                tokens_b_mask = tokens_b_mask[-1 * self.max_query_length : ]
            elif(self.q_order == 1):
                tokens_b = tokens_b[ : self.max_query_length]
                tokens_b_mask = tokens_b_mask[ : self.max_query_length]

        max_doc_len = max_num_tokens - len(tokens_b)
        
        yes_no_flag = example.yes_no_flag
        yes_no_ans = example.yes_no_ans

        tokens_a = tokens_a[start_offset : start_offset + length]
        tokens_a_mask = doc_word_mask[start_offset : start_offset + length]

        skip_num = sum(doc_word_mask[ : start_offset])

        start_position = example.start_position - skip_num
        end_position = example.end_position - skip_num

        #if(start_position >= mask_num or end_position >= mask_num):
        #    print("start position , end position, mask num ", start_position, end_position, mask_num)

        tokens = []
        segment_ids = []
        doc_mask = []
        query_mask = []

        tokens.append(self.cls_token)
        tokens.extend(tokens_a)
        tokens.append(self.sep_token)
        tokens.extend(tokens_b)

        segment_ids.append(0)
        segment_ids.extend([0 for _ in range(len(tokens_a))])
        segment_ids.append(0)
        segment_ids.extend([1 for _ in range(len(tokens_b))])
        
        doc_mask.append(0)
        doc_mask.extend(tokens_a_mask)
        doc_mask.append(0)
        doc_mask.extend([0 for _ in range(len(tokens_b))])
        
        query_mask.append(0)
        query_mask.extend([0 for _ in range(len(tokens_a))])
        query_mask.append(0)
        query_mask.extend(tokens_b_mask)

        assert len(tokens) <= self.max_seq_length
        assert len(segment_ids) <= self.max_seq_length
        assert len(doc_mask) <= self.max_seq_length
        assert len(query_mask) <= self.max_seq_length

        if(yes_no_flag == 1):
            return (example.paragraph_id, example.turn_id, example.doc_wnum, skip_num, start_offset, start_offset + length, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans + 1)
        else:
            return (example.paragraph_id, example.turn_id, example.doc_wnum, skip_num, start_offset, start_offset + length, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, 0) # yes_no_flag, yes_no_ans)

    def example_sample(self, idx):
        # sample another example.
        if(idx >= self.example_num):
            idx = self.rng.randint(0, self.example_num - 1)
        example = self.examples[idx]

        # Account for [CLS], [SEP]
        max_num_tokens = self.max_seq_length - 2

        tokens_a = example.doc_tokens
        doc_word_mask = self.get_word_mask(example.doc_w2tokens, len(tokens_a))

        tokens_b = example.query_tokens
        tokens_b_mask = example.query_mask

        if len(tokens_b) > self.max_query_length:
            if(self.q_order == 0):
                tokens_b = tokens_b[-1 * self.max_query_length : ]
                tokens_b_mask = tokens_b_mask[-1 * self.max_query_length : ]
            elif(self.q_order == 1):
                tokens_b = tokens_b[ : self.max_query_length]
                tokens_b_mask = tokens_b_mask[ : self.max_query_length]
            #tokens_b = tokens_b[-1 * self.max_query_length : ]
            #tokens_b_mask = tokens_b_mask[-1 * self.max_query_length : ]
        max_doc_len = max_num_tokens - len(tokens_b)
        
        yes_no_flag = example.yes_no_flag
        yes_no_ans = example.yes_no_ans

        start_position = example.start_position
        end_position = example.end_position

        if(len(tokens_a) <= max_doc_len):
            left_num = 0
            trunc_start = 0
            trunc_end = len(tokens_a)
            tokens_a_mask = doc_word_mask
        else:
            loop = 0
            while(loop < 100):
                start_pos = self.rng.randint(0, len(tokens_a) - max_doc_len)
                trunc_start = start_pos
                trunc_end = start_pos + max_doc_len
                
                left_num = sum(doc_word_mask[ : trunc_start])
                mask_num = sum(doc_word_mask[trunc_start : trunc_end])

                if(start_position == -1 or end_position == -1):
                    break
                elif(start_position >= left_num and end_position < left_num + mask_num):
                    break
                loop += 1
                
            tokens_a = tokens_a[trunc_start : trunc_end]
            tokens_a_mask = doc_word_mask[trunc_start : trunc_end]

        start_position = start_position - left_num
        end_position = end_position - left_num

        #if(start_position >= mask_num or end_position >= mask_num):
        #    print("start position , end position, mask num ", start_position, end_position, mask_num)

        tokens = []
        segment_ids = []
        doc_mask = []
        query_mask = []

        tokens.append(self.cls_token)
        tokens.extend(tokens_a)
        tokens.append(self.sep_token)
        tokens.extend(tokens_b)

        segment_ids.append(0)
        segment_ids.extend([0 for _ in range(len(tokens_a))])
        segment_ids.append(0)
        segment_ids.extend([1 for _ in range(len(tokens_b))])
        
        doc_mask.append(0)
        doc_mask.extend(tokens_a_mask)
        doc_mask.append(0)
        doc_mask.extend([0 for _ in range(len(tokens_b))])
        
        query_mask.append(0)
        query_mask.extend([0 for _ in range(len(tokens_a))])
        query_mask.append(0)
        query_mask.extend(tokens_b_mask)

        assert len(tokens) <= self.max_seq_length
        assert len(segment_ids) <= self.max_seq_length
        assert len(doc_mask) <= self.max_seq_length
        assert len(query_mask) <= self.max_seq_length


        if(yes_no_flag == 1):
            return (example.paragraph_id, example.turn_id, example.doc_wnum, left_num, trunc_start, trunc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans + 1)
        else:
            return (example.paragraph_id, example.turn_id, example.doc_wnum, left_num, trunc_start, trunc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, 0) # yes_no_flag, yes_no_ans)

        
