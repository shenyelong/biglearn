import argparse

from CoQA_Dataset_v2 import CoQA_Dataset_v2
from torch.utils.data import DataLoader
import sys
import torch

import numpy
import math
import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import json

import collections
from collections import Counter
from collections import OrderedDict

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--pred_dataset', required=True, type=str, help='predict dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-s", "--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("-q", "--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")
parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--seed_model', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')
parser.add_argument("--pred_path", type=str, default="pred.txt", help="prediction file name.")
parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')
parser.add_argument('--max_span', default='30', type=int, help='max prediction span.')
parser.add_argument('--q_order', default=0, type=int, help='question order')
parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')

parser.add_argument('--turn_ids', default='1', type=str, help='predict turns')
parser.add_argument('--max_history', default=20, type=int, help='max query history.')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior
from BigLearn.DeepNet.Language import LargeBertQA, DistLargeBertQA

turn_ids = [ int(x) for x in args.turn_ids.split(',') ] 

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Loading prediction Dataset . . .", args.pred_dataset)
# corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, rseed = 110):
pred_data = CoQA_Dataset_v2(corpus_path=args.pred_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history=args.max_history,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), turn_ids=turn_ids, qorder=args.q_order)

pred_loader = DataLoader(pred_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=False, drop_last=True)

#print("Loading dev Dataset . . .", args.dev_dataset)
#dev_data = CoQA_Dataset_v1(corpus_path=args.dev_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length,
#                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize)
#dev_loader = DataLoader(dev_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=False, drop_last=False)

print("Create bert model and setup enviroument")
#device_id = 0 # int(args.gpu_id)
#env_mode = DeviceBehavior(device_id).TrainMode

layer = 24
category = 6
#if args.is_seed == 0:
#    model = LargeBertQA.BertQAModel(layer, env_mode.Device)
#else:
# int frozenlayer, int special_category, string meta_file, string bin_file,   DeviceType device)

# span, no, yes, unknown, false, true,
model = LargeBertQA.BertQAModel(category, DeviceType.CPU) # args.seed_model, DeviceType.CPU) # env_mode.Device)
model.Load(args.seed_model)

#model.InitOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr), env_mode)

# BertQAModel model, RunnerBehavior behavior)
#net = LargeBertQA(model, env_mode)
net = DistLargeBertQA(model, StructureLearner.SGDLearner(0.0, 0.0, 0.0), deviceNum)

net.BuildCG(args.batch_size, args.max_seq_length, 0.0, args.pool, 0, 0, 1.0, 1.0)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

# BuildCG(int batchSize, int max_seq_len) 
#net.BuildCG(args.batch_size, args.max_seq_length)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def SpanLogit(startlogit, endlogit):
    return -(math.log(1 + math.exp(-startlogit)) + math.log(1 + math.exp(-endlogit)))

def GetBestSpan(startLogit, endLogit, wordNum, maxSpanLen):
    best_span_logit = -10000000
    best_span_start = -1
    best_span_end = -1
    for i in range(0, wordNum):
        for j in range(i, min(i + maxSpanLen, wordNum)):
            spanlogit = SpanLogit(startLogit[i], endLogit[j])
            if(spanlogit > best_span_logit):
                best_span_logit = spanlogit
                best_span_start = i
                best_span_end = j
    return (best_span_start, best_span_end, best_span_logit)

for epoch in range(0, 1):    
    ## set training mode.
    net.SetPredictMode()
    net.Init()
    
    bar = Bar('prediction', max=len(pred_loader))
    
    cateProb = torch.zeros([args.batch_size, 6], dtype=torch.float)
    startLogit = torch.zeros([args.batch_size * args.max_seq_length], dtype=torch.float)
    endLogit = torch.zeros([args.batch_size * args.max_seq_length], dtype=torch.float)
    
    p_cateProb = IntPtr.op_Explicit(Int64(cateProb.data_ptr()))
    p_startLogit = IntPtr.op_Explicit(Int64(startLogit.data_ptr()))
    p_endLogit = IntPtr.op_Explicit(Int64(endLogit.data_ptr()))

    pred_dict = {}
    pred_span_start = {}
    pred_span_end = {}

    for batch_idx, data in enumerate(pred_loader):
        data = {key: value for key, value in data.items()}
        input_tokens = data['token_input']
        input_segments = data['seg_input']
        input_mask = data['mask_input']

        doc_mask = data['doc_mask']
        query_mask = data['query_mask']

        start_position = data['start_position']
        end_position = data['end_position']

        yes_no_ans = data['yes_no_ans']

        p_tokens = IntPtr.op_Explicit(Int64(input_tokens.data_ptr()))
        p_segs = IntPtr.op_Explicit(Int64(input_segments.data_ptr()))
        p_msks = IntPtr.op_Explicit(Int64(input_mask.data_ptr()))
        
        p_doc_mask = IntPtr.op_Explicit(Int64(doc_mask.data_ptr()))
        p_qry_mask = IntPtr.op_Explicit(Int64(query_mask.data_ptr()))
        
        p_start_pos = IntPtr.op_Explicit(Int64(start_position.data_ptr()))
        p_end_pos = IntPtr.op_Explicit(Int64(end_position.data_ptr()))

        p_yes_no = IntPtr.op_Explicit(Int64(yes_no_ans.data_ptr()))

        #Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize)
        doc_spans = net.Predict(p_tokens, p_segs, p_msks, p_doc_mask, p_cateProb, p_startLogit, p_endLogit, args.batch_size)

        p_id = data['para_id']
        turn_id = data['turn_id']

        doc_num = data['doc_word_num']
        doc_offset = data['doc_word_offset']
        doc_start = data['doc_start']
        doc_end = data['doc_end']

        for i in range(0, args.batch_size):
            paragraph_id = pred_data.paragraph_id2ip(p_id[i].item())
            t_id = turn_id[i].item()

            ## max pooling over pid and tid.
            if(not (paragraph_id, t_id) in pred_dict):
                pred_dict[(paragraph_id, t_id)] = [-1000] * 6
                pred_span_start[(paragraph_id, t_id)] = [-1000] * doc_num[i].item()
                pred_span_end[(paragraph_id, t_id)] = [-1000] * doc_num[i].item()

                pred_dict[(paragraph_id, t_id)][3] = 1000; 

            for p in range(0, 6):
                if(p == 3):
                    pred_dict[(paragraph_id, t_id)][p] = min(pred_dict[(paragraph_id, t_id)][p], cateProb[i][p].item()) 
                else:
                    pred_dict[(paragraph_id, t_id)][p] = max(pred_dict[(paragraph_id, t_id)][p], cateProb[i][p].item()) 
                    
            for p in range(doc_spans[i], doc_spans[i+1]):
                p_idx = doc_offset[i].item() + (p - doc_spans[i])
                pred_span_start[(paragraph_id, t_id)][p_idx] = max(pred_span_start[(paragraph_id, t_id)][p_idx], startLogit[p].item())
                pred_span_end[(paragraph_id, t_id)][p_idx] = max(pred_span_end[(paragraph_id, t_id)][p_idx], endLogit[p].item())
            

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} '.format(
                    batch=batch_idx + 1,
                    size=len(pred_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td
                    )
        
        bar.next()
    bar.finish()
    net.Complete()

    all_predictions = []
    # aggragate prediction.
    for (p_id, turn_id) in pred_dict:

        cur_prediction = collections.OrderedDict()
        cur_prediction["id"] = p_id
        cur_prediction["turn_id"] = turn_id
        
        #print(pred_span_start[(p_id, turn_id)])
        #print(pred_span_end[(p_id, turn_id)])
        #print(pred_dict[(p_id, turn_id)])

        cate_id = numpy.argmax(pred_dict[(p_id, turn_id)])
        
        if(pred_dict[(p_id, turn_id)][3] > 0):
            cate_id = 3
        else:
            cate_id = numpy.argmax(pred_dict[(p_id, turn_id)][0:3] + pred_dict[(p_id, turn_id)][4:6])
            if(cate_id >= 3):
                cate_id += 1

        if(cate_id == 1):
            cur_prediction["answer"] = 'no'
        elif(cate_id == 2):
            cur_prediction["answer"] = 'yes'
        elif(cate_id == 3):
            cur_prediction["answer"] = 'unknown'
        elif(cate_id == 4):
            cur_prediction["answer"] = 'false'
        elif(cate_id == 5):
            cur_prediction["answer"] = 'true'
        ## get the span of the prediction.
        elif(cate_id == 0):
            (span_start, span_end, span_logit) = GetBestSpan(pred_span_start[(p_id, turn_id)], pred_span_end[(p_id, turn_id)], len(pred_span_start[(p_id, turn_id)]), args.max_span)
            text_span = pred_data.getspan(p_id, span_start, span_end)
            cur_prediction["answer"] = text_span
        all_predictions.append(cur_prediction)

    with open(args.pred_path, "w") as writer:
        writer.write(json.dumps(all_predictions, indent=4) + "\n")


