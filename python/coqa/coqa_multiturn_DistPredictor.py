import argparse

from CoQA_MultiTurn_Dataset import CoQA_MultiTurn_Dataset
from torch.utils.data import DataLoader
import sys
import torch

import numpy
import math
import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import json

import collections
from collections import Counter
from collections import OrderedDict

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--pred_dataset', required=True, type=str, help='predict dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]") 
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-s", "--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("-q", "--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")
parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--seed_model', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')
parser.add_argument("--pred_path", type=str, default="pred.txt", help="prediction file name.")
parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')
parser.add_argument('--max_span', default='30', type=int, help='max prediction span.')
parser.add_argument('--q_order', default=0, type=int, help='question order')
parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')
parser.add_argument('--turn_ids', default='1', type=str, help='predict turns')

parser.add_argument('--slide', default=128, type=int, help='chunk slides')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager, Session, ComputationGraph
from BigLearn import BaseBertModel, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure, TransformerStructure
from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData
from BigLearn import GradientOptimizer, ParameterSetting, NCCL
from BigLearn import CrossEntropyRunner

device_num = len(args.gpu_id.split(','))
print("set gpu number ", device_num)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Loading prediction Dataset . . .", args.pred_dataset)

# corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, rseed = 110):
#pred_data = CoQA_Dataset_v1(corpus_path=args.pred_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history=20,
#                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), qorder=args.q_order, slide_win=args.slide)
#pred_loader = DataLoader(pred_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=False, drop_last=True)

turn_ids = [ int(x) for x in args.turn_ids.split(',') ] 

pred_data = CoQA_MultiTurn_Dataset(corpus_path=args.pred_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history = 100,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=True, turn_ids=turn_ids, skip_bad = False, qorder = 0, slide_win = args.slide)

pred_loader = DataLoader(pred_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

print("Create bert model and setup enviroument")
#device_id = 0 # int(args.gpu_id)
#env_mode = DeviceBehavior(device_id).TrainMode

def SetupDocMask(mask_doc_tensor, mask_doc_piece, batch_size, max_seq_length):
    acc_nums = [0] * (batch_size + 1)
    acc_nums[0] = 0
    batch_inc = 0
    for b in range(batch_size):
        word_inc = 0
        for s in range(max_seq_length):
            if(mask_doc_tensor[b][s].item() > 0.5):
                mask_doc_piece[batch_inc + word_inc] = b * max_seq_length + s
                word_inc = word_inc + 1
        batch_inc = batch_inc + word_inc
        acc_nums[b + 1] = batch_inc

    mask_doc_piece.EffectiveSize = batch_inc
    mask_doc_piece.SyncFromCPU()
    return acc_nums 

def SetupLabel(cate_label, start_label, end_label, cate_piece, start_piece, end_piece, acc_nums, batch_size):
    start_piece.EffectiveSize = acc_nums[batch_size]  
    end_piece.EffectiveSize = acc_nums[batch_size]  

    cate_piece.EffectiveSize = 6 * batch_size

    start_piece.Zero()
    end_piece.Zero()
    cate_piece.Zero()

    start_only = 0
    end_only = 0
    unk_only = 0

    # ["yes", "no", "unknown", "true", "false"]

    for b in range(batch_size):
        start_idx = start_label[b]
        end_idx = end_label[b]

        batch_inc = acc_nums[b]
        word_num = acc_nums[b + 1] - acc_nums[b]

        is_start = False
        if start_idx >= 0 and start_idx < word_num:
            start_piece[batch_inc + start_idx] = 1.0
            is_start = True

        is_end = False
        if end_idx >=0 and end_idx < word_num:
            end_piece[batch_inc + end_idx] = 1.0
            is_end = True

        if is_start and is_end:
            conv = 1 
        elif not is_start and not is_end:
            conv = 0
            unk_only += 1
        elif not is_start and is_end:
            conv = end_idx / (end_idx - start_idx);
            end_only += 1;
        elif is_start and not is_end:
            conv = (word_num - start_idx) / (end_idx - start_idx);
            start_only += 1

        cate_piece[b * 6 + cate_label[b]] = conv
        cate_piece[b * 6 + 3] = 1 - conv

    start_piece.SyncFromCPU()
    end_piece.SyncFromCPU()
    cate_piece.SyncFromCPU()

    start_only_ratio = start_only * 1.0 / batch_size
    end_only_ratio = end_only * 1.0 / batch_size
    unk_ratio = unk_only * 1.0 / batch_size

    return start_only_ratio, end_only_ratio, unk_ratio

class MrcModule:
    # dual module for Generator and Discriminator 
    def __init__(self, vocab_size, layer, seed_mrc, seed_model, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        # int frozenlayer, int mvocabSize, int mcate, int mlayer, int mdim, string meta_file, string bin_file, DeviceType device) 
        self.Models['mrc_model'] = self.Session.Model.AddLayer(BaseBertModel(-1, vocab_size, 2, 24, 1024, seed_mrc + 'exp.label', seed_mrc + 'exp.bin', behavior.Device))
        self.Models['cate_classifier'] =  self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 6, A_Func.Linear, True, behavior.Device))
        self.Models['span_start_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 1, A_Func.Linear, False, behavior.Device))
        self.Models['span_end_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 1, A_Func.Linear, False, behavior.Device))

        #for i in range(0, layer):
        #    self.Models['layer_' + str(i)] = self.Session.Model.AddLayer(TransformerStructure(1024, behavior.Device))
        self.Session.Model.Load(seed_model)

        #self.grad = self.Session.Model.AllocateGradient(self.Behavior.Device)
        # StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema)
        #self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(0.0, 0.0, '0:0', 0.0)) 
        #StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema))
        #StructureLearner.AdamLearner(args.lr, 0.9, 0.999, 1.0, 0.0004)) # (args.lr, 0.5, 0.999))

class MrcNet:
    ## construct model 
    def __init__(self, module, batch_size, max_seq_length, max_query_length, dropout, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Module = module
        self.Models = module.Models

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length

        max_doc_length = max_seq_length - max_query_length

        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        #self.segments = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.mask_doc = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        
        emd = 1024
        att_head = 16

        #bert_feature = self.Session.BaseBertFeaturizer(self.Models['mrc_model'], self.tokens, self.segments, self.masks, batch_size, max_seq_length, att_head)

        #dim = IntArgument('dim', self.Models['bert_base'].embed)
        #batch = IntArgument('batch', batch_size)
        #seq = IntArgument('seq', max_seq_length)
        #batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)

        #self.embed_feature = self.Session.Dropout(bert_feature.Item1, dropout)
        #self.slice_feature = self.Session.Dropout(bert_feature.Item2, dropout)

        ### start our show in mrc net.
        ### self.query_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)
        w_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.tokens)
        w_embed = self.Session.Reshape(w_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", batch_size))

        pos_ids = [i for i in range(max_seq_length)]
        pos_embed = self.Session.LookupEmbed(self.Models['mrc_model'].PosEmbed, pos_ids)
        pos_embed = self.Session.Reshape(pos_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))

        seg_ids = [0 for i in range(max_doc_length)] + [1 for i in range(max_query_length)]
        seg_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenTypeEmbed, seg_ids)
        seg_embed = self.Session.Reshape(seg_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))

        embed = self.Session.Add(w_embed, pos_embed)
        embed = self.Session.Add(embed, seg_embed)

        _norm_emd = self.Session.Norm(embed, 0) 
        norm_emd = self.Session.DotAndAdd(_norm_emd, self.Models['mrc_model'].NdNormScale, self.Models['mrc_model'].NdNormBias)
        norm_emd = self.Session.Reshape(norm_emd, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", batch_size))

        self.scaleVec = CudaPieceFloat(2, self.Behavior.Device)
        self.scaleVec[0] = 0
        self.scaleVec[1] = 1.0 / math.sqrt(emd / att_head)
        self.scaleVec.SyncFromCPU()

        self.biasVec = CudaPieceFloat(2, self.Behavior.Device)
        self.biasVec[0] = -1e9
        self.biasVec[1] = 0
        self.biasVec.SyncFromCPU()

        _mask_vocab = IntArgument('mask_vocab', 2)

        #new IntArgument[] { emd_dim, firstSlice, batch_size },

        _dim = IntArgument('dim', self.Models['mrc_model'].embed)
        _batch = IntArgument('batch', batch_size)
        _seq = IntArgument('seq', max_seq_length)
        _batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)
        _default = IntArgument('default', 1)

        #DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions) 
        self.mask_scale = self.Session.LookupEmbed(NdArrayData(self.Behavior.Device, self.scaleVec, None, _default, _mask_vocab), self.masks)
        #CudaPieceFloat(self.max_seq_length * self.batch_size, self.Behavior.Device)
        
        self.mask_bias = self.Session.LookupEmbed(NdArrayData(self.Behavior.Device, self.biasVec, None, _default, _mask_vocab), self.masks)
        #CudaPieceFloat(self.max_seq_length * self.batch_size, self.Behavior.Device)

        # TransformerStructure block, NdArrayData input, int head_num, CudaPieceFloat maskScale, CudaPieceFloat maskBias, float dropout)
        for i in range(24):
            norm_emd = self.Session.Transformer(self.Models['mrc_model'].Blocks[i], norm_emd, 16, self.mask_scale.Output, self.mask_bias.Output, 0.0)

        # NdArrayData data, int[] offset, params IntArgument[] shape)
        slice_embed = self.Session.Slice(norm_emd, [0, 0, 0], _dim, _default, _batch)
        #bert_feature = self.Session.BaseBertFeaturizer(self.Models['mrc_model'], self.tokens, self.segments, self.masks, batch_size, max_seq_length)
        slice_embed = self.Session.Reshape(slice_embed, _dim, _batch)
        
        self.embed_feature = self.Session.Dropout(norm_emd, 0)
        self.slice_feature = self.Session.Dropout(slice_embed, 0)

        self.cate_flag = self.Session.FNN(self.slice_feature, self.Models['cate_classifier'])
        #self.cate_loss = self.Session.CrossEntropyLoss(self.cate_flag, self.cate_label, args.cate_w, False)

        self.embed_2d_feature = self.Session.Reshape(self.embed_feature, _dim, _batch_seq)
        self.doc_embed = self.Session.LookupEmbed(self.embed_2d_feature, self.mask_doc)

        self.start_flag = self.Session.FNN(self.doc_embed, self.Models['span_start_classifier'])
        self.end_flag = self.Session.FNN(self.doc_embed, self.Models['span_end_classifier'])

        #self.start_loss = self.Session.CrossEntropyLoss(self.start_flag, self.start_label, args.span_w, False)
        #self.end_loss = self.Session.CrossEntropyLoss(self.end_flag, self.end_label, args.span_w, False)

    def SetupPredict(self, over_tokens, over_mask, over_segment, doc_word_masks): # cate_label, start_label, end_label):
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(over_tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        #self.segments.CopyTorch(IntPtr.op_Explicit(Int64(over_segment.data_ptr())), self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(over_mask.data_ptr())), self.batch_size * self.max_seq_length)

        self.acc_nums = SetupDocMask(doc_word_masks, self.mask_doc, self.batch_size, self.max_seq_length)
        
        #self.start_only_ratio, self.end_only_ratio, self.unk_ratio = SetupLabel(cate_label, start_label, end_label, self.cate_label, self.start_label, self.end_label, acc_nums, self.batch_size) 

class DistMrcNet:
        
    def __init__(self, seed_bert, seed_model, vocab_size, batch_size, max_query_length, max_seq_length, dropout, device_num):
        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).TrainMode
            ParameterSetting.ResetRandom()
            mrcModule = MrcModule(vocab_size, 1, seed_bert, seed_model, env_mode)
            return MrcNet(mrcModule, self.sub_batch_size, max_seq_length, max_query_length, dropout, env_mode)
            # vocab_size, layer, seed_mrc, behavior):
            #return MrcModule(vocab_size, 3, seed_bert, cate, self.sub_batch_size, max_seq_length, dropout, env_mode)
        self.device_num = device_num

        self.batch_size = batch_size
        self.sub_batch_size = (int)(batch_size / device_num)

        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length

        #self.pool.map(__construct_model, range(device_num))

        self.models = [] #* device_num
        for idx in range(device_num):
            self.models.append(__construct_model(idx))

        self.parallel_run = NCCL(device_num)

        self.sessions =  [m.Session for m in self.models]
        #self.grads = [m.Module.grad for m in self.models]
        #self.gradSessions = [m.Module.Session for m in self.models]

    def Predict(self, over_tokens, over_mask, over_segment, doc_word_masks):
        def __setup(device_id):
            self.models[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            #l_start = device_id * self.sub_batch_size
            #l_end = (device_id + 1) * self.sub_batch_size

            self.models[device_id].SetupPredict(over_tokens[b_start : b_end], over_mask[b_start : b_end], over_segment[b_start : b_end], doc_word_masks[b_start : b_end])
        
        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        self.parallel_run.Predict(self.sessions, self.device_num)

        acc_nums = [ m.acc_nums for m in self.models ]
        
        for m in self.models:
            m.cate_flag.Output.SyncToCPU()
            m.start_flag.Output.SyncToCPU()
            m.end_flag.Output.SyncToCPU()

        cate_logits = [ m.cate_flag.Output for m in self.models ]
        start_logits = [ m.start_flag.Output for m in self.models ]
        end_logits = [ m.end_flag.Output for m in self.models ]

        return cate_logits, start_logits, end_logits, acc_nums 


# BuildCG(int batchSize, int max_seq_len) 
#net.BuildCG(args.batch_size, args.max_seq_length)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def SpanLogit(startlogit, endlogit):
    return -(math.log(1 + math.exp(-startlogit)) + math.log(1 + math.exp(-endlogit)))

def GetBestSpan(startLogit, endLogit, wordNum, maxSpanLen):
    best_span_logit = -10000000
    best_span_start = -1
    best_span_end = -1
    for i in range(0, wordNum):
        for j in range(i, min(i + maxSpanLen, wordNum)):
            spanlogit = SpanLogit(startLogit[i], endLogit[j])
            if(spanlogit > best_span_logit):
                best_span_logit = spanlogit
                best_span_start = i
                best_span_end = j
    return (best_span_start, best_span_end, best_span_logit)

# self, seed_bert, seed_model, vocab_size, batch_size, max_query_length, max_seq_length, dropout, device_num):
qaPredictor = DistMrcNet(args.seed_bert, args.seed_model, args.vocabsize, args.batch_size, args.max_query_length, args.max_seq_length, 0.0, device_num)

for epoch in range(0, 1):    
    ## set training mode.
    #net.SetPredictMode()
    #net.Init()
    
    bar = Bar('prediction', max=len(pred_loader))
    
    #cateProb = torch.zeros([args.batch_size, 6], dtype=torch.float)
    #startLogit = torch.zeros([args.batch_size * args.max_seq_length], dtype=torch.float)
    #endLogit = torch.zeros([args.batch_size * args.max_seq_length], dtype=torch.float)
    
    #p_cateProb = IntPtr.op_Explicit(Int64(cateProb.data_ptr()))
    #p_startLogit = IntPtr.op_Explicit(Int64(startLogit.data_ptr()))
    #p_endLogit = IntPtr.op_Explicit(Int64(endLogit.data_ptr()))

    pred_dict = {}
    pred_span_start = {}
    pred_span_end = {}

    for batch_idx, data in enumerate(pred_loader):
        data = {key: value for key, value in data.items()}
        
        #input_tokens = data['token_input']
        #input_segments = data['seg_input']
        #input_mask = data['mask_input']

        #doc_mask = data['doc_mask']
        #query_mask = data['query_mask']

        #start_position = data['start_position']
        #end_position = data['end_position']

        #yes_no_ans = data['yes_no_ans']

        doc_tokens = data['doc_tokens']
        query_tokens = data['query_tokens']

        over_tokens = data['over_tokens']
        over_masks = data['over_mask']
        over_segs = data['over_segments']
        
        doc_word_mask = data['doc_word_mask']

        #start_label = data['start_position']
        #end_label = data['end_position']

        #cate_label = data['yes_no_ans']

        #p_tokens = IntPtr.op_Explicit(Int64(input_tokens.data_ptr()))
        #p_segs = IntPtr.op_Explicit(Int64(input_segments.data_ptr()))
        #p_msks = IntPtr.op_Explicit(Int64(input_mask.data_ptr()))
        
        #p_doc_mask = IntPtr.op_Explicit(Int64(doc_mask.data_ptr()))
        #p_qry_mask = IntPtr.op_Explicit(Int64(query_mask.data_ptr()))
        
        #p_start_pos = IntPtr.op_Explicit(Int64(start_position.data_ptr()))
        #p_end_pos = IntPtr.op_Explicit(Int64(end_position.data_ptr()))

        #p_yes_no = IntPtr.op_Explicit(Int64(yes_no_ans.data_ptr()))

        #Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize)
        
        #doc_spans = net.Predict(p_tokens, p_segs, p_msks, p_doc_mask, p_cateProb, p_startLogit, p_endLogit, args.batch_size)

        # over_tokens, over_mask, over_segment, doc_word_masks):
        cate_logits, start_logits, end_logits, acc_nums = qaPredictor.Predict(over_tokens, over_masks, over_segs, doc_word_mask)

        p_id = data['para_id']
        turn_id = data['turn_id']

        doc_num = data['doc_word_num']
        doc_offset = data['doc_word_offset']
        doc_start = data['doc_start']
        doc_end = data['doc_end']

        for i in range(0, args.batch_size):
            paragraph_id = pred_data.paragraph_id2ip(p_id[i].item())
            t_id = turn_id[i].item()

            ## max pooling over pid and tid.
            if(not (paragraph_id, t_id) in pred_dict):
                pred_dict[(paragraph_id, t_id)] = [-1000] * 6
                pred_span_start[(paragraph_id, t_id)] = [-1000] * doc_num[i].item()
                pred_span_end[(paragraph_id, t_id)] = [-1000] * doc_num[i].item()

                pred_dict[(paragraph_id, t_id)][3] = 1000; 

            #for p in range(doc_spans[i], doc_spans[i+1]):
            device_idx = int(i / qaPredictor.sub_batch_size)
            g_idx = i % qaPredictor.sub_batch_size

            for p in range(0, 6):
                if(p == 3):
                    pred_dict[(paragraph_id, t_id)][p] = min(pred_dict[(paragraph_id, t_id)][p], cate_logits[device_idx][g_idx * 6 + p]) 
                else:
                    pred_dict[(paragraph_id, t_id)][p] = max(pred_dict[(paragraph_id, t_id)][p], cate_logits[device_idx][g_idx * 6 + p]) 
                    
            for p in range(acc_nums[device_idx][g_idx], acc_nums[device_idx][g_idx + 1]):
                p_idx = doc_offset[i].item() + (p - acc_nums[device_idx][g_idx]) # (p - doc_spans[i])
                pred_span_start[(paragraph_id, t_id)][p_idx] = max(pred_span_start[(paragraph_id, t_id)][p_idx], start_logits[device_idx][p])
                pred_span_end[(paragraph_id, t_id)][p_idx] = max(pred_span_end[(paragraph_id, t_id)][p_idx], end_logits[device_idx][p])
            
        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} '.format(
                    batch=batch_idx + 1,
                    size=len(pred_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td
                    )
        
        bar.next()
    bar.finish()
    #net.Complete()

    all_predictions = []
    # aggragate prediction.
    for (p_id, turn_id) in pred_dict:

        cur_prediction = collections.OrderedDict()
        cur_prediction["id"] = p_id
        cur_prediction["turn_id"] = turn_id
        
        #print(pred_span_start[(p_id, turn_id)])
        #print(pred_span_end[(p_id, turn_id)])
        #print(pred_dict[(p_id, turn_id)])

        cate_id = numpy.argmax(pred_dict[(p_id, turn_id)])
        
        if(pred_dict[(p_id, turn_id)][3] > 0):
            cate_id = 3
        else:
            cate_id = numpy.argmax(pred_dict[(p_id, turn_id)][0:3] + pred_dict[(p_id, turn_id)][4:6])
            if(cate_id >= 3):
                cate_id += 1

        if(cate_id == 1):
            cur_prediction["answer"] = 'no'
        elif(cate_id == 2):
            cur_prediction["answer"] = 'yes'
        elif(cate_id == 3):
            cur_prediction["answer"] = 'unknown'
        elif(cate_id == 4):
            cur_prediction["answer"] = 'false'
        elif(cate_id == 5):
            cur_prediction["answer"] = 'true'
        ## get the span of the prediction.
        elif(cate_id == 0):
            (span_start, span_end, span_logit) = GetBestSpan(pred_span_start[(p_id, turn_id)], pred_span_end[(p_id, turn_id)], len(pred_span_start[(p_id, turn_id)]), args.max_span)
            text_span = pred_data.getspan(p_id, span_start, span_end)
            cur_prediction["answer"] = text_span
        all_predictions.append(cur_prediction)

    with open(args.pred_path, "w") as writer:
        writer.write(json.dumps(all_predictions, indent=4) + "\n")


