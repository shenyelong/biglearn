"""
preprocess CoQA for extractive question answering
2019/1/22: distinguish yes-no and wh- question
"""

import argparse
import json
import re
import time
import string
from collections import Counter
from collections import OrderedDict
from pycorenlp import StanfordCoreNLP

import tokenization


nlp = StanfordCoreNLP('http://localhost:9000')
UNK = 'unknown'


def _str(s):
    """ Convert PTB tokens to normal tokens """
    if (s.lower() == '-lrb-'):
        s = '('
    elif (s.lower() == '-rrb-'):
        s = ')'
    elif (s.lower() == '-lsb-'):
        s = '['
    elif (s.lower() == '-rsb-'):
        s = ']'
    elif (s.lower() == '-lcb-'):
        s = '{'
    elif (s.lower() == '-rcb-'):
        s = '}'
    return s


def process(text):
    paragraph = nlp.annotate(text, properties={
                             'annotators': 'tokenize, ssplit',
                             'outputFormat': 'json',
                             'ssplit.newlineIsSentenceBreak': 'two'})

    output = {'word': [], 'offsets': []}

    try:
        for sent in paragraph['sentences']:
            for token in sent['tokens']:
                output['word'].append(_str(token['word']))
                output['offsets'].append((token['characterOffsetBegin'], token['characterOffsetEnd']))
    except:
        print("error in line 45: {}".format(paragraph))
    return output


def normalize_answer(s):
    """Lower text and remove punctuation, storys and extra whitespace."""

    def remove_articles(text):
        regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
        return re.sub(regex, ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))


def find_span_with_gt(context, offsets, ground_truth, w_start = -1, w_end = -1):
    """
    find answer from paragraph so that best f1 score is achieved
    """
    best_f1 = 0.0
    best_span = (len(context)-1, len(context)-1)
    best_pred = ''
    gt = normalize_answer(ground_truth).split()

    if(w_start == -1):
        w_start = 0
    if(w_end == -1):
        w_end = len(offsets)

    ls = [i for i in range(w_start, w_end) # len(offsets))
          if context[offsets[i][0]:offsets[i][1]].lower() in gt]

    for i in range(len(ls)):
        for j in range(i, len(ls)):
            pred = normalize_answer(context[offsets[ls[i]][0]: offsets[ls[j]][1]]).split()
            common = Counter(pred) & Counter(gt)
            num_same = sum(common.values())
            if num_same > 0:
                precision = 1.0 * num_same / len(pred)
                recall = 1.0 * num_same / len(gt)
                f1 = (2 * precision * recall) / (precision + recall)
                if f1 > best_f1:
                    best_f1 = f1
                    best_span = (offsets[ls[i]][0], offsets[ls[j]][1])
                    best_pred = context[offsets[ls[i]][0]: offsets[ls[j]][1]]
    return best_span, best_f1, best_pred

def f1Score(gt, pred):
    norm_gt = normalize_answer(gt).split()
    norm_pred = normalize_answer(pred).split()

    common = Counter(norm_pred) & Counter(norm_gt)
    num_same = sum(common.values())
    if num_same > 0:
        precision = 1.0 * num_same / len(norm_pred)
        recall = 1.0 * num_same / len(norm_gt)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1
    return 0

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    parser.add_argument("--vocab_file", default=None, type=str, help="tokenizer vocab file.")

    args = parser.parse_args()

    tokenizer = tokenization.FullTokenizer(args.vocab_file) 

    with open(args.data_file, 'r') as f:
        dataset = json.load(f)

    data = OrderedDict()
    data['version'] = dataset['version']
    _datum_list = []
    yes_cnt = 0
    no_cnt = 0
    unk_cnt = 0
    true_cnt = 0
    false_cnt = 0
    fuz_num = 0

    _f1 = 0
    _num = 0
    _sf1 = 0
    _pnum = 0
    _debug_f = open('detect.span', 'w')
    _wf1 = 0
    _wnum = 0
    
    bad_quality_num = 0
    for i, datum in enumerate(dataset['data']):
        if (i % 10 == 0):
            print("processig {} examples".format(i))
            
        _datum = OrderedDict()
        source = datum["source"]
        paragraph_id = datum["id"]
        filename = datum["filename"]
        context_str = datum["story"]
        _datum["source"] = source
        _datum["id"] = paragraph_id
        _datum["filename"] = filename
        _datum["story"] = context_str

        annotated_context_str = process(context_str)
        offsets = annotated_context_str['offsets']
        words = annotated_context_str['word']
        subtokens, word2subtoken = tokenizer.tokenize_words(words)
        subtokens_idx = tokenizer.convert_tokens_to_ids(subtokens)

        _datum["story_subtoken_idx"] = subtokens_idx
        _datum["story_word2subtoken"] = word2subtoken
        _datum["story_wordoffset"] = offsets

        char_to_word_offset = []
        widx = 0
        for c_story in range(0, len(context_str)):
            if (widx + 1 >= len(words)):
                char_to_word_offset.append(widx)
                continue

            if (c_story < offsets[widx][0]):
                char_to_word_offset.append(widx)
                continue

            if (c_story >= offsets[widx][0] and c_story < offsets[widx][1]):
                char_to_word_offset.append(widx)
                continue

            if (c_story >= offsets[widx][1]):
                if(len(words) > widx + 1):
                    char_to_word_offset.append(widx + 1)
                else:
                    char_to_word_offset.append(widx)
                widx = widx + 1
                continue
        assert len(char_to_word_offset) == len(context_str)

        #offsets.append((len(_datum['story']) - len(UNK), len(_datum['story'])))
        # ?? add additional_answers ?? not used. 
        if ('additional_answers' in datum):
            _datum['additional_answers'] = datum['additional_answers']

        # question & answer
        questions = []
        answers = []

        idx = 0
        for question, answer in zip(datum['questions'], datum['answers']):
            assert question['turn_id'] == answer['turn_id']
            turn_id = question['turn_id']
            
            if turn_id > 1:
                break

            question_text = question['input_text']
            question_tokens = tokenizer.tokenize(question_text)
            question_idx = tokenizer.convert_tokens_to_ids(question_tokens)

            cur_question = OrderedDict()
            cur_question['input_text'] = question_text
            cur_question['turn_id'] = turn_id
            cur_question['input_text_subtoken_idx'] = question_idx
            questions.append(cur_question)

            ans_span_text = answer['span_text']
            ans_input_text = answer['input_text']

            ans_start = answer['span_start']
            ans_end = answer['span_end']
            
            cur_answer = OrderedDict() # attr: span_start, span_end, text, turn_id
            start = ans_start
            end = ans_end

            chosen_text = _datum['story'][ans_start:ans_end].lower()
            ## this is a chosen story.

            # remove space from start
            while len(chosen_text) > 0 and chosen_text[0] in string.whitespace:
                chosen_text = chosen_text[1:]
                start += 1
            # remove space from end
            while len(chosen_text) > 0 and chosen_text[-1] in string.whitespace:
                chosen_text = chosen_text[:-1]
                end -= 1

            input_text = ans_input_text.strip().lower()
            # yes_no_flag, yes_no_ans
            # deal with yes/no question
            if (input_text == 'yes' or input_text == 'yes.' or input_text == '.yes' or input_text == 'yese' or input_text.startswith('yes, ')):
            #if (normalize_answer(input_text) == 'yes'):
                yes_no_flag = 1
                yes_no_ans = 1
                yes_cnt += 1
                input_text = 'yes'
            #elif (normalize_answer(input_text) == 'no'):
            elif (input_text == 'no' or input_text == 'no.' or input_text == '. no' or input_text == 'he is not' or input_text == 'not really' or input_text.startswith('no, ')):
                yes_no_flag = 1
                yes_no_ans = 0
                no_cnt += 1
                input_text = 'no'
            #elif (input_text == 'unknown' or input_text == 'unknown.'):
            elif (normalize_answer(input_text) == 'unknown'):
                yes_no_flag = 1
                yes_no_ans = 2
                unk_cnt += 1
                input_text = 'unknown'
            #elif(input_text == 'false'):
            elif (normalize_answer(input_text) == 'false'):
                yes_no_flag = 1
                yes_no_ans = 3
                false_cnt += 1
                input_text = 'false'
            #elif(input_text == 'true'):
            elif (normalize_answer(input_text) == 'true'):
                yes_no_flag = 1
                yes_no_ans = 4
                true_cnt += 1
                input_text = 'true'
            else:
                yes_no_flag = 0
                yes_no_ans = -1
                
            #f1 = 1
            #sf1 = 1
            quality = 1
            if (input_text in ["yes", "no", "unknown", "true", "false"]):
                ans_span = (start, end) ## selected ans_span by char.
            elif input_text in chosen_text:
                i = chosen_text.find(input_text)
                ans_span = (start + i, start + i + len(input_text)) 
            else:
                c_w_start = char_to_word_offset[ans_start]
                c_w_end = char_to_word_offset[ans_end - 1]

                ans_span, f1, pred = find_span_with_gt(_datum['story'], offsets, input_text, c_w_start, c_w_end + 1)

                sf1 = f1Score(input_text, chosen_text)  
                
                fuz_num += 1

                if(sf1 > f1 or f1 <= 0.6):
                    ans_span = (start, end)
                    _debug_f.write(question_text + '\t' + input_text + '\t' + pred + '\t' + str(f1) + '\t' + chosen_text + '\t' + str(sf1) + '\n')
                    _pnum += 1 
                    _f1 += f1 
                    _sf1 += sf1

                    if(sf1 <= 0.6):
                        quality = 0 
            _num += 1

            ## char span.
            cur_answer['char_span_start'] = ans_span[0]
            cur_answer['char_span_end'] = ans_span[1]
            cur_answer['char_span_text'] = _datum['story'][ans_span[0] : ans_span[1]]

            ## check the corresponding sub-token based on character.
            if(ans_span[0] == -1 or ans_span[1] == -1):
                w_start = -1
                w_end = -1
            else:
                w_start = char_to_word_offset[ans_span[0]]
                w_end = char_to_word_offset[ans_span[1]-1]

            cur_answer['word_span_start'] = w_start
            cur_answer['word_span_end'] = w_end

            #cur_answer['word_span_text'] = _datum['story'][offsets[w_start][0] : offsets[w_end][1]] #words[w_start:w_end+1]

            if(yes_no_flag == 0):
                wf1 = f1Score(input_text, _datum['story'][offsets[w_start][0] : offsets[w_end][1]]) # cur_answer['word_span_text'])
                _wf1 += wf1
                _wnum += 1

            #cur_answer['sub_token_start'] = word2subtoken[w_start]
            #cur_answer['sub_token_end'] = word2subtoken[w_end]
            #cur_answer['sub_token_text'] = subtokens[word2subtoken[w_start] : word2subtoken[w_end + 1]]

            cur_answer['yes_no_flag'] = yes_no_flag
            cur_answer['yes_no_ans'] = yes_no_ans
            cur_answer['input_text'] = answer['input_text']
            cur_answer['span_text'] = answer['span_text']
            cur_answer['turn_id'] = answer['turn_id']
            cur_answer['quality'] = quality

            if(quality == 0):
                bad_quality_num += 1
            if(w_end < w_start):
                #print(cur_answer['word_span_text'])
                print(cur_answer['char_span_text'])
                print(cur_answer['input_text'])
                print(cur_answer['span_text'])
                print('wrong line\n')
            
            answers.append(cur_answer)
            idx += 1

        _datum["questions"] = questions[:]
        _datum["answers"] = answers[:]
        _datum_list.append(_datum)
    data['data'] = _datum_list
    print("# of questions : {}, # of answer true : {}, # of answer false : {}, # of answer yes: {}, # of answer no: {}, # of unk : {}, # of fuz span : {}, avg f1 : {}, avg sf1 : {}, # of f1 : {}, # of wf1 : {}, # of bad quality : {}".format(_num, true_cnt, false_cnt, yes_cnt, no_cnt, unk_cnt, fuz_num, _f1 * 1.0 / _pnum, _sf1 * 1.0 / _pnum, _pnum, _wf1 * 1.0 / _wnum, bad_quality_num))
    _debug_f.close()

    with open(args.output_file, 'w') as output_file:
        json.dump(data, output_file, sort_keys=True, indent=4)




