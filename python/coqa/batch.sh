#for i in {6..20}
#do
  #python CoQADistPredictor_v2.py --lib /data/home/yelongshen/sharelib/storm70 --pred_dataset /data1/yelongshen/CoQA/dev.span.v2.refine.json --batch_size 6 --seed_model /data1/yelongshen/coqa_bert_model/bert.coqa.ca13.v100.2.model --pool 0 --pred_path dev.t$i.ca13.v100.i2.json --gpu_id 5,6,7 --max_span 15 --turn_ids $i

#echo $i
#done

for i in {1..3}
do
    #python CoQADistPredictor_v3.py --lib /data/home/yelongshen/sharelib/storm72 --pred_dataset /data1/yelongshen/CoQA/dev.span.v2.refine.json --batch_size 6 --seed_model /data1/yelongshen/coqa_bert_model/bert.coqa.ca13.v200.$i.model --pool 0 --pred_path dev.ca13.v200.i$i.json --gpu_id 5,6,7 --max_span 15 --slide 64 --turn_ids 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26
    python evaluate-coqa.py --data-file /data1/yelongshen/CoQA/coqa-dev-v1.0.json --pred-file dev.ca13.v200.i$i.json
done

#python evaluate-coqa.py --data-file /data1/yelongshen/CoQA/coqa-dev-v1.0.json --pred-file dev.ca13.v200.i0.json



