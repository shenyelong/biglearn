import argparse

from CoQA_Style_Dataset import CoQA_Style_Dataset
from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os

parser = argparse.ArgumentParser(description='Python call coqa transfer learning.')

parser.add_argument('--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('--train_dataset', required=True, type=str, help='train dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("--batch_size", type=int, default=48, help="number of batch_size")   
parser.add_argument("--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--epochs', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,2000:0.00001,50000:0.000001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')
parser.add_argument('--decay', default=0.01, type=float, metavar='DECAY', help='decay of model parameters.')
parser.add_argument('--opt', default='adambert', type=str, metavar='OPT', help='optimization algorithm.')
parser.add_argument('--ema', default='1.0', type=float, metavar='EMA', help='exp moving average.')

parser.add_argument('--is_seed', default=0, type=int, metavar='S', help='use seed model or random initialized.')

parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--output_model', default='bert_CoQA', type=str, help='output model.')

parser.add_argument('--dropout', default='0', type=float, help='droput rate.')

parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')

parser.add_argument('--skip_bad', default='1', type=int, help='skip bad quality data training. 0:noskip; 1:skip;')

parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')

#parser.add_argument('--unknown', default='0', type=int, help='unknown category.')
#parser.add_argument('--distavg', default='0', type=int, help='0:default weight between type_loss and span_loss; 1:standard normalize')

parser.add_argument('--avg_cate', default='0', type=int, help='average category loss.')
parser.add_argument('--avg_span', default='0', type=int, help='average span loss.')

parser.add_argument('--cate_w', default='0.1', type=float, help='weight of category loss.')
parser.add_argument('--span_w', default='0.1', type=float, help='weight of span loss.')

parser.add_argument('--q_order', default=0, type=int, help='question order')

parser.add_argument('--turn_ids', default='1', type=str, help='train turns')


args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior
from BigLearn.DeepNet.Language import LargeBertQA, DistLargeBertQA


deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

turn_ids = [ int(x) for x in args.turn_ids.split(',') ] 

print("Loading train Dataset . . .", args.train_dataset)

# corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, fill_batch, is_enumate_doc_span, turn_ids,
train_data = CoQA_Style_Dataset(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history=20,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, 
                         is_enumate_doc_span=(args.enumate_span == 1), turn_ids=turn_ids, skip_bad=args.skip_bad, qorder=args.q_order)

train_loader = DataLoader(train_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)


print("Create bert model and setup enviroument")

#layer = 24

#model = LargeBertQA.BertQAModel(-1, 6, args.seed_bert + 'exp.label', args.seed_bert + 'exp.bin', DeviceType.CPU) # env_mode.Device)

#model.InitOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr), env_mode)

# BertQAModel model, RunnerBehavior behavior)
#net = LargeBertQA(model, env_mode)

#if(args.opt == 'adambert'):
#    opt = StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema)
#elif(args.opt == 'sgd'):
#    opt = StructureLearner.SGDLearner(args.lr, args.grad_clip, args.schedule_lr, 0.9, args.decay)
#elif(args.opt == 'adamdelta'):
#    opt = StructureLearner.AdamDeltaLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema)
#net = DistLargeBertQA(model, opt, deviceNum)


# int isAvgCateLoss, int isAvgSpanLoss, float cateWeight, float spanWeight) 
#net.BuildCG(args.batch_size, args.max_seq_length, args.dropout, args.pool, args.avg_cate, args.avg_span, args.cate_w, args.span_w)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

# BuildCG(int batchSize, int max_seq_len) 
#net.BuildCG(args.batch_size, args.max_seq_length)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

class StyleQANet:
    ## construct model 
    def __init__(self, seed_qa_model, cate, layer, dim, batch_size, max_seq_length, max_query_length, max_doc_length, vocab_size, dropout, behavior):
        self.Behavior = behavior

        
        self.Models = {}

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length

        ## QA session 
        self.SessionQA = Session(behavior)

        self.Models['transformers'] = self.SessionQA.Model.AddLayer(BaseBertModel(2, 24, -1, dim, 512, vocab_size, behavior.Device))  #vocab_size, 2, layer, dim, seed_bert + 'exp.label', seed_bert + 'exp.bin', behavior.Device))
        self.Models['cate_classifier'] =  self.SessionQA.Model.AddLayer(LayerStructure(self.Models['transformers'].embed, cate, A_Func.Linear, True, behavior.Device))
        self.Models['span_start_classifier'] = self.SessionQA.Model.AddLayer(LayerStructure(self.Models['transformers'].embed, 1, A_Func.Linear, True, behavior.Device))
        self.Models['span_end_classifier'] = self.SessionQA.Model.AddLayer(LayerStructure(self.Models['transformers'].embed, 1, A_Func.Linear, True, behavior.Device))

        self.SessionQA.Model.Load(seed_qa_model)



        _dim = IntArgument('dim', self.Models['transformers'].embed)
        _batch = IntArgument('batch', batch_size)
        _max_seq_len = IntArgument('max_seq_len', max_seq_length)
        _max_query_len = IntArgument('max_query_len', max_query_length)
        _max_doc_len = IntArgument('max_doc_len', max_doc_length)

        #batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)


        ## embed session.
        self.doc_tokens = CudaPieceInt(batch_size * max_doc_length, self.Behavior.Device)
        
        self.q_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)
        self.q_len = CudaPieceInt(batch_size, self.Behavior.Device)

        self.t1_q_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)
        self.t1_q_len = CudaPieceInt(batch_size, self.Behavior.Device)

        self.SessionEmbed = Session(behavior)
        doc_embed = self.SessionEmbed.LookupEmbed(self.Models['transformers'].TokenEmbed, self.doc_tokens)
        q_embed = self.SessionEmbed.LookupEmbed(self.Models['transformers'].TokenEmbed, self.q_tokens)
        t1_q_embed = self.SessionEmbed.LookupEmbed(self.Models['transformers'].TokenEmbed, self.t1_q_tokens)


        ## lstm t1_q token embed.
        self.SessionEncoder = Session(behavior)

        self.Models['en_lstms'] = self.SessionEncoder.Model.AddLayer(LSTMStructure(dim, dim, self.Behavior.Device))

        t1_q_embed = self.SessionEncoder.Reshape(t1_q_embed, _dim, _max_query_len, _batch)
        t1_q_seq = self.SessionEncoder.Tensor2SeqData(t1_q_embed, self.t1_q_len)
        self.state_t1_q = self.SessionEncoder.LSTM(self.Models['en_lstms'], t1_q_seq)

        self.state_t1_style

        q_embed = self.SessionEncoder.Reshape(q_embed, _dim, _max_query_len, _batch)
        q_seq = self.SessionEncoder.Tensor2SeqData(q_embed, self.q_len)
        self.state_q = self.SessionEncoder.LSTM(self.Models['en_lstms'], q_seq)

        self.SessionEncoder.Model.AllocateGradient(self.Behavior.Device)
        self.SessionEncoder.AllocateOptimizer(StructureLearner.AdamLearner(args.lr, 0.5, 0.999)) # (args.lr, 0.5, 0.999))


    # category label and turn.
    def Train(self, q, q_len, d, d_len, t1_q, t1_q_len, cate_label, start_label, end_label): 


        self.qa_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.qa_segments = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.qa_masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        self.qa_mask_doc = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        
        self.qa_start_label = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)
        self.qa_end_label = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)

        self.qa_cate_label = CudaPieceFloat(batch_size * cate, self.Behavior.Device)





        embed_features = self.SessionQA.BaseBertFeaturizer(self.Models['transformers'], self.qa_tokens, self.qa_segments, self.qa_masks, batch_size, max_seq_length)

        _embed = embed_features.Item1
        _slice = embed_features.Item2

        

        cate_flag = self.Session.FNN(_slice, self.Models['cate_classifier'])
        self.cate_loss = self.Session.CrossEntropyLoss(cate_flag, self.qa_cate_label, 0.1, False)

        self.embed_2d_feature = self.Session.Reshape(self.embed_feature, dim, batch_seq)
        self.doc_embed = self.Session.LookupEmbed(self.embed_2d_feature, self.mask_doc)

        self.start_flag = self.Session.FNN(self.doc_embed, self.Models['span_start_classifier'])
        self.end_flag = self.Session.FNN(self.doc_embed, self.Models['span_end_classifier'])

        self.start_loss = self.Session.CrossEntropyLoss(self.start_flag, self.start_label, 0.1, False)
        self.end_loss = self.Session.CrossEntropyLoss(self.end_flag, self.end_label, 0.1, False)



        lstm_dims = [ 512 for _ in range(1) ]
        self.Models['en_lstms'] = self.Session.Model.AddLayer(LSTMStructure(dim, lstm_dims, behavior.Device))


        self.grad = self.Session.Model.AllocateGradient(behavior.Device)
        self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay))


        self.q_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)
        self.d_tokens = CudaPieceInt(batch_size * (max_seq_length - 2 - max_query_length), self.Behavior.Device)
        self.t1_q_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)



        

        

        dim = IntArgument('dim', self.Models['bert_base'].embed)
        batch = IntArgument('batch', batch_size)
        seq = IntArgument('seq', max_seq_length)
        batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)

        self.embed_feature = self.Session.Dropout(bert_feature.Item1, dropout)
        self.slice_feature = self.Session.Dropout(bert_feature.Item2, dropout)

        self.unk_flag = self.Session.FNN(self.slice_feature, self.Models['unk_classifier'])
        self.unk_loss = self.Session.CrossEntropyLoss(self.unk_flag, self.cate_label, 0.1, False)

        self.embed_2d_feature = self.Session.Reshape(self.embed_feature, dim, batch_seq)
        self.doc_embed = self.Session.LookupEmbed(self.embed_2d_feature, self.mask_doc)

        self.start_flag = self.Session.FNN(self.doc_embed, self.Models['span_start_classifier'])
        self.end_flag = self.Session.FNN(self.doc_embed, self.Models['span_end_classifier'])

        self.start_loss = self.Session.CrossEntropyLoss(self.start_flag, self.start_label, 0.1, False)
        self.end_loss = self.Session.CrossEntropyLoss(self.end_flag, self.end_label, 0.1, False)

class StyleQANet:
    def __init__(self, models, cate, batch_size, nhead, max_seq_length, dropout, behavior):
        self.Behavior = behavior
        self.max_seq_length = max_seq_length

        self.Models = models
        self.Session = Session(behavior)
        
        _dim = IntArgument('dim', self.Models['bert_model'].embed)
        _batch = IntArgument('batch', batch_size)
        _seq = IntArgument('seq', max_seq_length)
        _batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)

        # DeviceType device, bool requireGrad, params IntArgument[] dimensions) 
        self.tokens = NdArrayData(behavior.Device, True, _dim, _seq, _batch) # CudaPieceFloat(batch_size * max_seq_length * self.Models['bert_model'].embed, behavior.Device)
        self.segments = CudaPieceInt(batch_size * max_seq_length, behavior.Device)
        self.masks = CudaPieceInt(batch_size * max_seq_length, behavior.Device)

        # BaseBertFeaturizer(BaseBertModel model, NdArrayData token_embeds, CudaPieceInt seg_ids, CudaPieceInt mask_ids, int batch_size, int max_seq_len, int nhead)
        bert_feature = self.Session.BaseBertFeaturizer(self.Models['bert_model'], self.tokens, self.segments, self.masks, batch_size, max_seq_length, nhead)

        embed_feature = self.Session.Dropout(bert_feature.Item1, dropout)
        slice_feature = self.Session.Dropout(bert_feature.Item2, dropout)

        self.cate_label = CudaPieceFloat(batch_size * cate, self.Behavior.Device)

        self.cate_flag = self.Session.FNN(slice_feature, self.Models['cate_classifier'])
        self.cate_loss = self.Session.CrossEntropyLoss(self.cate_flag, self.cate_label, 0.1, False)

        ## category label.
                
        self.mask_doc = CudaPieceInt(batch_size * max_seq_length, behavior.Device)
        self.start_label = CudaPieceFloat(batch_size * max_seq_length, behavior.Device)
        self.end_label = CudaPieceFloat(batch_size * max_seq_length, behavior.Device)

        embed_2d_feature = self.Session.Reshape(embed_feature, dim, batch_seq)
        doc_embed = self.Session.LookupEmbed(embed_2d_feature, self.mask_doc)

        self.start_flag = self.Session.FNN(doc_embed, self.Models['span_start_classifier'])
        self.end_flag = self.Session.FNN(doc_embed, self.Models['span_end_classifier'])

        self.start_loss = self.Session.CrossEntropyLoss(self.start_flag, self.start_label, 0.1, False)
        self.end_loss = self.Session.CrossEntropyLoss(self.end_flag, self.end_label, 0.1, False)


    def SetupDocMask(self, mask_doc):

        acc_nums = [0] * (self.batch_size + 1)
        acc_nums[0] = 0

        batch_inc = 0
        for b in range(self.batch_size):
            word_inc = 0
            for s in range(self.max_seq_length):
                #print(b,s)
                #print(mask_doc)
                #print(mask_doc[0])

                if(mask_doc[b][s].item() > 0.01):
                    self.mask_doc[batch_inc + word_inc] = b * self.max_seq_length + s
                    word_inc = word_inc + 1
            batch_inc = batch_inc + word_inc
            acc_nums[b + 1] = batch_inc

        self.mask_doc.EffectiveSize = batch_inc
        self.mask_doc.SyncFromCPU()

        return acc_nums

    def SetupLabel(self, cate_label, start_label, end_label, acc_nums):
        
        self.start_label.EffectiveSize = acc_nums[self.batch_size];  
        self.end_label.EffectiveSize = acc_nums[self.batch_size];  
        
        self.start_label.Zero()
        self.end_label.Zero()

        start_only = 0
        end_only = 0
        unk_only = 0
        for b in range(self.batch_size):
            start_idx = start_label[b]
            end_idx = end_label[b]

            batch_inc = acc_nums[b]
            word_num = acc_nums[b + 1] - acc_nums[b]

            is_start = False
            if(start_idx >= 0 and start_idx < word_num):
                self.start_label[ batch_inc + start_idx ] = 1.0
                is_start = True

            is_end = False
            if(end_idx >=0 and end_idx < word_num):
                self.end_label[batch_inc + end_idx] = 1.0
                is_end = True

            if(is_start and is_end):
                conv = 1

            if(not is_start and not is_end):
                conv = 0
                unk_only += 1

            if(not is_start and is_end):
                conv = end_idx / (end_idx - start_idx);
                end_only += 1;

            if(is_start and not is_end):
                conv = (word_num - start_idx) / (end_idx - start_idx);
                start_only += 1

            self.cate_label[b * 2] = conv
            self.cate_label[b * 2 + 1] = 1 - conv

        self.start_label.SyncFromCPU()
        self.end_label.SyncFromCPU()
        self.cate_label.SyncFromCPU()

        self.start_only_ratio = start_only / self.batch_size
        self.end_only_ratio = end_only / self.batch_size
        self.unk_ratio = unk_only / self.batch_size

    def SetupTrain(self, tokens, segments, masks, mask_doc, cate_label, start_label, end_label):
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.segments.CopyTorch(IntPtr.op_Explicit(Int64(segments.data_ptr())), self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(masks.data_ptr())), self.batch_size * self.max_seq_length)
        
        acc_nums = self.SetupDocMask(mask_doc)
        self.SetupLabel(cate_label, start_label, end_label, acc_nums)


class DistStyleQANet:
    def __init__(self, batch_size, max_seq_length, max_pred_length, dropout, device_num):

        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).TrainMode
            ParameterSetting.ResetRandom()
            return ConvTransformerLM(self.sub_batch_size, max_seq_length, max_pred_length, dropout, env_mode)

        self.device_num = device_num

        self.batch_size = batch_size
        self.sub_batch_size = (int)(batch_size / device_num)
        self.max_seq_length = max_seq_length

        self.models = [] #* device_num
        for idx in range(device_num):
            self.models.append(__construct_model(idx))

        self.grad_reducer = NCCL(device_num)

        self.sessions =  [m.Session for m in self.models]
        self.grads = [m.grad for m in self.models]

    def printshape(self, name, data):
        shape_idx = 0
        for shape in data.Shape:
            print(name, shape_idx, shape.Default, shape.Value)
            shape_idx += 1

    def Train(self, tokens, segments, masks, out_tokens):
        def __setup(device_id):
            self.models[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            self.models[device_id].SetupTrain(tokens[b_start : b_end], segments[b_start : b_end], masks[b_start : b_end], out_tokens[b_start : b_end])

        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        #self.models[0].Session.StepV3Run()

        #self.printshape('masked_embed', self.models[0].masked_embed)
        #self.printshape('_mw', self.models[0]._mw)
        #self.printshape('mw', self.models[0].mw)
        #self.printshape('_normw', self.models[0]._normw)
        #self.printshape('normw', self.models[0].normw)
        #self.printshape('_word_scores', self.models[0]._word_scores)
        #self.printshape('word_scores', self.models[0].word_scores)
        
        self.grad_reducer.Execute(self.sessions, self.grads, self.device_num)

        loss_es = [ m.loss.Value for m in self.models ]
        loss_avg = sum(loss_es) / self.device_num # [ x / self.device_num for x in [sum(loss) for loss in zip(*loss_es)]]
        return loss_avg #, ratio_avg


class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


for epoch in range(0, args.epochs):
    print('\nEpoch: [%d | %d] LR: %f' % (epoch + 1, args.epochs, args.lr))
    
    ## set training mode.
    net.SetTrainMode()
    net.Init()
    
    cate_loss = AverageMeter()
    start_loss = AverageMeter()
    end_loss = AverageMeter()

    unk_loss = AverageMeter()
    lefto_loss = AverageMeter()
    righto_loss = AverageMeter()

    bar = Bar('training', max=len(train_loader))
    
    for batch_idx, data in enumerate(train_loader):
        # 0. batch_data will be sent into the device(GPU or cpu)
        #print(batch_idx)
        #print(data)
        data = {key: value for key, value in data.items()}

        ## q1 tokens
        ## q1 a1 q2 tokens
        ## document tokens
        ## a2 [start end]
        q_tokens = data['q_tokens']
        q_len = data['q_len']

        d_tokens = data['d_tokens']
        d_len = data['d_len']

        t1_q_tokens = data['t1_q_tokens']
        t1_q_len = data['t1_q_len']

        #input_segments = data['seg_input']
        #input_mask = data['mask_input']

        #doc_mask = data['doc_mask']
        #query_mask = data['query_mask']

        start_position = data['start_position']
        end_position = data['end_position']

        yes_no_ans = data['yes_no_ans']



        z_t1_q_embed = p_encoder.encode(p_tok_ids, p_tok_len, 1, args.batch_size)


        p_tokens = IntPtr.op_Explicit(Int64(input_tokens.data_ptr()))
        p_segs = IntPtr.op_Explicit(Int64(input_segments.data_ptr()))
        p_msks = IntPtr.op_Explicit(Int64(input_mask.data_ptr()))
        
        p_doc_mask = IntPtr.op_Explicit(Int64(doc_mask.data_ptr()))
        p_qry_mask = IntPtr.op_Explicit(Int64(query_mask.data_ptr()))
        
        p_start_pos = IntPtr.op_Explicit(Int64(start_position.data_ptr()))
        p_end_pos = IntPtr.op_Explicit(Int64(end_position.data_ptr()))

        p_yes_no = IntPtr.op_Explicit(Int64(yes_no_ans.data_ptr()))

        #Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize)

        c_loss, s_loss, e_loss, unk, lefto, righto = net.Run(p_tokens, p_segs, p_msks, p_doc_mask, p_yes_no, p_start_pos, p_end_pos, args.batch_size) 
        cate_loss.update(c_loss)
        start_loss.update(s_loss)
        end_loss.update(e_loss)

        unk_loss.update(unk)
        lefto_loss.update(lefto)
        righto_loss.update(righto)

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | cate_loss: {cate_loss:.4f} | start_loss: {start_loss:.4f} | end_loss : {end_loss:.4f} | avg_cate_loss: {avg_cate_loss:.4f} | avg_start_loss: {avg_start_loss:.4f} | avg_end_loss : {avg_end_loss:.4f} | unk_avg: {avg_unk:.4f} | lefto_avg: {avg_lefto:.4f} | righto_avg : {avg_righto:.4f} '.format(
                    batch=batch_idx + 1,
                    size=len(train_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    cate_loss=c_loss,
                    start_loss=s_loss,
                    end_loss=e_loss,
                    avg_cate_loss=cate_loss.avg,
                    avg_start_loss=start_loss.avg,
                    avg_end_loss=end_loss.avg,
                    avg_unk=unk_loss.avg,
                    avg_lefto=lefto_loss.avg,
                    avg_righto=righto_loss.avg                    
                    )
        bar.next()
        #break
    #break
    bar.finish()
    net.Complete()
    net.SaveModel(args.output_model + '.' + str(epoch) + '.model')
