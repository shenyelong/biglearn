import argparse

from CoQA_MultiTurn_Dataset import CoQA_MultiTurn_Dataset
from torch.utils.data import DataLoader
import sys
import torch

import numpy
import math
import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import json

import random

import collections
from collections import Counter
from collections import OrderedDict

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--pred_dataset', required=True, type=str, help='predict dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]") 
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-s", "--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("-q", "--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")
parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--seed_model', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')
parser.add_argument("--pred_path", type=str, default="pred.txt", help="prediction file name.")
parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')
parser.add_argument('--max_span', default='30', type=int, help='max prediction span.')
parser.add_argument('--q_order', default=0, type=int, help='question order')
parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')
parser.add_argument('--turn_ids', default='1', type=str, help='predict turns')

parser.add_argument('--slide', default=128, type=int, help='chunk slides')
parser.add_argument('--t_type', default=1, type=int, help='type ids')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager, Session, ComputationGraph
from BigLearn import BaseBertModel, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure, TransformerStructure
from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData
from BigLearn import GradientOptimizer, ParameterSetting, NCCL
from BigLearn import CrossEntropyRunner

device_num = len(args.gpu_id.split(','))
print("set gpu number ", device_num)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Loading prediction Dataset . . .", args.pred_dataset)

# corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, rseed = 110):
#pred_data = CoQA_Dataset_v1(corpus_path=args.pred_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history=20,
#                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), qorder=args.q_order, slide_win=args.slide)
#pred_loader = DataLoader(pred_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=False, drop_last=True)

turn_ids = [ int(x) for x in args.turn_ids.split(',') ] 

#pred_data = CoQA_MultiTurn_Dataset(corpus_path=args.pred_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history = 100,
#                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=True, turn_ids=turn_ids, skip_bad = False, qorder = 0, slide_win = args.slide)

#pred_loader = DataLoader(pred_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)


t1_pred_data = CoQA_MultiTurn_Dataset(corpus_path=args.pred_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history = 100,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=True, turn_ids=[1], skip_bad = 0, qorder = 0, slide_win = args.slide)
t1_pred_loader = DataLoader(t1_pred_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

t2_pred_data = CoQA_MultiTurn_Dataset(corpus_path=args.pred_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history = 100,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=True, turn_ids=turn_ids, skip_bad = 0, qorder = 0, slide_win = args.slide)
t2_pred_loader = DataLoader(t2_pred_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)


print("Create bert model and setup enviroument")
#device_id = 0 # int(args.gpu_id)
#env_mode = DeviceBehavior(device_id).TrainMode

def SetupDocMask(mask_doc_tensor, mask_doc_piece, batch_size, max_seq_length):
    acc_nums = [0] * (batch_size + 1)
    acc_nums[0] = 0
    batch_inc = 0
    for b in range(batch_size):
        word_inc = 0
        for s in range(max_seq_length):
            if(mask_doc_tensor[b][s].item() > 0.5):
                mask_doc_piece[batch_inc + word_inc] = b * max_seq_length + s
                word_inc = word_inc + 1
        batch_inc = batch_inc + word_inc
        acc_nums[b + 1] = batch_inc

    mask_doc_piece.EffectiveSize = batch_inc
    mask_doc_piece.SyncFromCPU()
    return acc_nums 

class MrcModule:
    # dual module for Generator and Discriminator 
    def __init__(self, vocab_size, layer, seed_mrc, seed_model, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        # int frozenlayer, int mvocabSize, int mcate, int mlayer, int mdim, string meta_file, string bin_file, DeviceType device) 
        self.Models['mrc_model'] = self.Session.Model.AddLayer(BaseBertModel(-1, vocab_size, 2, 24, 1024, seed_mrc + 'exp.label', seed_mrc + 'exp.bin', behavior.Device))
        self.Models['cate_classifier'] =  self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 6, A_Func.Linear, True, behavior.Device))
        self.Models['span_start_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 1, A_Func.Linear, False, behavior.Device))
        self.Models['span_end_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 1, A_Func.Linear, False, behavior.Device))

        self.layer = layer

        for i in range(0, layer):
            self.Models['layer_' + str(i)] = self.Session.Model.AddLayer(TransformerStructure(1024, behavior.Device))

        self.Session.Model.Load(seed_model)
        #self.grad = self.Session.Model.AllocateGradient(self.Behavior.Device)
        #self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema))

class MrcNet:
    ## construct model 
    def __init__(self, module, batch_size, max_seq_length, max_query_length, dropout, t_type, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Module = module
        self.Models = module.Models

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length

        max_doc_length = max_seq_length - max_query_length
        self.max_doc_length = max_doc_length

        self.t_type = t_type

        emd = 1024
        att_head = 16

        self.mask_doc = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)        
        
        self.start_label = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)
        self.end_label = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)
        self.cate_label = CudaPieceFloat(batch_size * 6, self.Behavior.Device)
        
        self.masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        _dim = IntArgument('dim', self.Models['mrc_model'].embed)
        _batch = IntArgument('batch', batch_size)
        _seq = IntArgument('seq', max_seq_length)
        _batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)
        _batch_q_seq = IntArgument('batch_seq', batch_size * max_query_length)
        _default = IntArgument('default', 1)
        _q_seq = IntArgument('q_seq', max_query_length)
        _d_seq = IntArgument('d_seq', max_doc_length)

        if t_type == 0:
            self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
            w_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.tokens)
            w_embed = self.Session.Reshape(w_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", batch_size))

        elif t_type == 1:
            self.q_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)
            q_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.q_tokens)
            q_embed = self.Session.Reshape(q_embed, _dim, _q_seq, _batch)

            q_pos_ids = [i for i in range(max_query_length)]
            q_pos_embed = self.Session.LookupEmbed(self.Models['mrc_model'].PosEmbed, q_pos_ids)
            q_pos_embed = self.Session.Reshape(q_pos_embed, _dim, _q_seq, _default)

            q_embed = self.Session.Add(q_embed, q_pos_embed)

            _q_norm_emd = self.Session.Norm(q_embed, 0) 
            q_norm_emd = self.Session.DotAndAdd(_q_norm_emd, self.Models['mrc_model'].NdNormScale, self.Models['mrc_model'].NdNormBias)
            q_norm_emd = self.Session.Reshape(q_norm_emd, _dim, _q_seq, _batch)

            q_mask_bias = CudaPieceFloat(batch_size * max_query_length, self.Behavior.Device)
            q_mask_bias.Init(0.0)

            q_mask_scale = CudaPieceFloat(batch_size * max_query_length, self.Behavior.Device)
            q_mask_scale.Init(1.0 / math.sqrt(emd / att_head))

            for i in range(self.Module.layer):
                q_norm_emd = self.Session.Transformer(self.Models['layer_' + str(i)], q_norm_emd, 16, q_mask_scale, q_mask_bias, 0.1)

            self.d_tokens = CudaPieceInt(batch_size * max_doc_length, self.Behavior.Device)
            d_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.d_tokens)
            d_embed = self.Session.Reshape(d_embed, _dim, _d_seq, _batch)

            #. q_norm_emd
            w_embed = self.Session.Cat([d_embed, q_norm_emd], 1)
        
        ## enable gumbel softmax in the output layer.
        elif t_type == 2:
            self.q_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)
            q_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.q_tokens)
            q_embed = self.Session.Reshape(q_embed, _dim, _q_seq, _batch)

            q_pos_ids = [i for i in range(max_query_length)]
            q_pos_embed = self.Session.LookupEmbed(self.Models['mrc_model'].PosEmbed, q_pos_ids)
            q_pos_embed = self.Session.Reshape(q_pos_embed, _dim, _q_seq, _default)

            q_embed = self.Session.Add(q_embed, q_pos_embed)

            _q_norm_emd = self.Session.Norm(q_embed, 0) 
            q_norm_emd = self.Session.DotAndAdd(_q_norm_emd, self.Models['mrc_model'].NdNormScale, self.Models['mrc_model'].NdNormBias)
            q_norm_emd = self.Session.Reshape(q_norm_emd, _dim, _q_seq, _batch)

            q_mask_bias = CudaPieceFloat(batch_size * max_query_length, self.Behavior.Device)
            q_mask_bias.Init(0.0)

            q_mask_scale = CudaPieceFloat(batch_size * max_query_length, self.Behavior.Device)
            q_mask_scale.Init(1.0 / math.sqrt(emd / att_head))

            for i in range(self.Module.layer):
                q_norm_emd = self.Session.Transformer(self.Models['layer_' + str(i)], q_norm_emd, 16, q_mask_scale, q_mask_bias, 0.1)
            
            q_norm_emd = self.Session.Reshape(q_norm_emd, _dim, _batch_q_seq)

            q_logit = self.Session.MatMul(q_norm_emd, 0, self.Models['mrc_model'].NdTokenEmbed, 1)

            q_wei = FloatArgument('gumbel_w', 0.3)
            global_update = IntArgument("global_update", 0)
            sched = RateScheduler(global_update, '0:1.0')
            q_gumbel = self.Session.GumbelSoftmax(q_logit, sched, q_wei, True)
            
            q_logit = q_gumbel.Item1
            q_ids = q_gumbel.Item2

            q_embed = self.Session.MatMul(q_logit, 0, self.Models['mrc_model'].NdTokenEmbed, 0)
            q_embed = self.Session.Reshape(q_embed, _dim, _q_seq, _batch)

            self.d_tokens = CudaPieceInt(batch_size * max_doc_length, self.Behavior.Device)
            d_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.d_tokens)
            d_embed = self.Session.Reshape(d_embed, _dim, _d_seq, _batch)

            #. q_norm_emd
            w_embed = self.Session.Cat([d_embed, q_embed], 1)

        pos_ids = [i for i in range(max_seq_length)]
        pos_embed = self.Session.LookupEmbed(self.Models['mrc_model'].PosEmbed, pos_ids)
        pos_embed = self.Session.Reshape(pos_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))

        seg_ids = [0 for i in range(max_doc_length)] + [1 for i in range(max_query_length)]
        seg_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenTypeEmbed, seg_ids)
        seg_embed = self.Session.Reshape(seg_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))

        embed = self.Session.Add(w_embed, pos_embed)
        embed = self.Session.Add(embed, seg_embed)

        _norm_emd = self.Session.Norm(embed, 0) 
        norm_emd = self.Session.DotAndAdd(_norm_emd, self.Models['mrc_model'].NdNormScale, self.Models['mrc_model'].NdNormBias)
        norm_emd = self.Session.Reshape(norm_emd, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", batch_size))

        self.scaleVec = CudaPieceFloat(2, self.Behavior.Device)
        self.scaleVec[0] = 0
        self.scaleVec[1] = 1.0 / math.sqrt(emd / att_head)
        self.scaleVec.SyncFromCPU()

        self.biasVec = CudaPieceFloat(2, self.Behavior.Device)
        self.biasVec[0] = -1e9
        self.biasVec[1] = 0
        self.biasVec.SyncFromCPU()

        _mask_vocab = IntArgument('mask_vocab', 2)

        #new IntArgument[] { emd_dim, firstSlice, batch_size },
        self.mask_scale = self.Session.LookupEmbed(NdArrayData(self.Behavior.Device, self.scaleVec, None, _default, _mask_vocab), self.masks)        
        self.mask_bias = self.Session.LookupEmbed(NdArrayData(self.Behavior.Device, self.biasVec, None, _default, _mask_vocab), self.masks)

        norm_emd, slice_embed = self.transformer_blocks(norm_emd, _dim, _default, _batch)

        self.build_ans_net(norm_emd, slice_embed, dropout, _dim, _batch_seq)

    def transformer_blocks(self, norm_emd, _dim, _default, _batch):
        for i in range(24):
            norm_emd = self.Session.Transformer(self.Models['mrc_model'].Blocks[i], norm_emd, 16, self.mask_scale.Output, self.mask_bias.Output, 0.1)
        slice_embed = self.Session.Slice(norm_emd, [0, 0, 0], _dim, _default, _batch)
        slice_embed = self.Session.Reshape(slice_embed, _dim, _batch)
        return norm_emd, slice_embed

    def build_ans_net(self, norm_emd, slice_embed, dropout, _dim, _batch_seq):
        self.embed_feature = self.Session.Dropout(norm_emd, dropout)
        self.slice_feature = self.Session.Dropout(slice_embed, dropout)

        self.cate_flag = self.Session.FNN(self.slice_feature, self.Models['cate_classifier'])
        #self.cate_loss = self.Session.CrossEntropyLoss(self.cate_flag, self.cate_label, args.cate_w, False)

        self.embed_2d_feature = self.Session.Reshape(self.embed_feature, _dim, _batch_seq)
        self.doc_embed = self.Session.LookupEmbed(self.embed_2d_feature, self.mask_doc)

        self.start_flag = self.Session.FNN(self.doc_embed, self.Models['span_start_classifier'])
        self.end_flag = self.Session.FNN(self.doc_embed, self.Models['span_end_classifier'])

        #self.start_loss = self.Session.CrossEntropyLoss(self.start_flag, self.start_label, args.span_w, False)
        #self.end_loss = self.Session.CrossEntropyLoss(self.end_flag, self.end_label, args.span_w, False)

    def SetupPredict_0(self, over_tokens, over_mask, over_segment, doc_word_masks):
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(over_tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(over_mask.data_ptr())), self.batch_size * self.max_seq_length)

        self.acc_nums = SetupDocMask(doc_word_masks, self.mask_doc, self.batch_size, self.max_seq_length)

    def SetupPredict_1(self, d_tokens, q_tokens, over_mask, over_segment, doc_word_masks):
        self.q_tokens.CopyTorch(IntPtr.op_Explicit(Int64(q_tokens.data_ptr())),  self.batch_size * self.max_query_length)
        self.d_tokens.CopyTorch(IntPtr.op_Explicit(Int64(d_tokens.data_ptr())),  self.batch_size * self.max_doc_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(over_mask.data_ptr())), self.batch_size * self.max_seq_length)
        
        self.acc_nums = SetupDocMask(doc_word_masks, self.mask_doc, self.batch_size, self.max_seq_length)

class DistMrcNet:
        
    def __init__(self, seed_bert, seed_model, vocab_size, batch_size, max_query_length, max_seq_length, dropout, device_num):
        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).PredictMode
            ParameterSetting.ResetRandom()
            mrcModule = MrcModule(vocab_size, 4, seed_bert, seed_model, env_mode)
            
            mrc_net_0 = MrcNet(mrcModule, self.sub_batch_size, max_seq_length, max_query_length, dropout, 0, env_mode)
            mrc_net_1 = MrcNet(mrcModule, self.sub_batch_size, max_seq_length, max_query_length, dropout, args.t_type, env_mode)
            return mrcModule,mrc_net_0, mrc_net_1

        self.device_num = device_num

        self.batch_size = batch_size
        self.sub_batch_size = (int)(batch_size / device_num)

        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length


        self.modules = []
        self.models_0 = [] #* device_num
        self.models_1 = []
        for idx in range(device_num):
            module, m_0, m_1 = __construct_model(idx)
            self.modules.append(module)
            self.models_0.append(m_0)
            self.models_1.append(m_1)

        self.parallel_run = NCCL(device_num)

        self.sessions_0 = [m.Session for m in self.models_0]
        self.sessions_1 = [m.Session for m in self.models_1]

    def Predict_0(self, over_tokens, over_mask, over_segment, doc_word_masks):
        def __setup(device_id):
            self.models_0[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            self.models_0[device_id].SetupPredict_0(over_tokens[b_start : b_end], over_mask[b_start : b_end], over_segment[b_start : b_end], doc_word_masks[b_start : b_end])
        
        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        self.parallel_run.Predict(self.sessions_0, self.device_num)

        acc_nums = [ m.acc_nums for m in self.models_0 ]
        
        for m in self.models_0:
            m.cate_flag.Output.SyncToCPU()
            m.start_flag.Output.SyncToCPU()
            m.end_flag.Output.SyncToCPU()

        cate_logits = [ m.cate_flag.Output for m in self.models_0 ]
        start_logits = [ m.start_flag.Output for m in self.models_0 ]
        end_logits = [ m.end_flag.Output for m in self.models_0 ]

        return cate_logits, start_logits, end_logits, acc_nums 

    def Predict_1(self, d_tokens, q_tokens, over_mask, over_segment, doc_word_masks):
        def __setup(device_id):
            self.models_1[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            self.models_1[device_id].SetupPredict_1(d_tokens[b_start : b_end], q_tokens[b_start : b_end], over_mask[b_start : b_end], over_segment[b_start : b_end], doc_word_masks[b_start : b_end])
        
        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        self.parallel_run.Predict(self.sessions_1, self.device_num)

        acc_nums = [ m.acc_nums for m in self.models_1 ]
        
        for m in self.models_1:
            m.cate_flag.Output.SyncToCPU()
            m.start_flag.Output.SyncToCPU()
            m.end_flag.Output.SyncToCPU()

        cate_logits = [ m.cate_flag.Output for m in self.models_1 ]
        start_logits = [ m.start_flag.Output for m in self.models_1 ]
        end_logits = [ m.end_flag.Output for m in self.models_1 ]

        return cate_logits, start_logits, end_logits, acc_nums 

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def SpanLogit(startlogit, endlogit):
    return -(math.log(1 + math.exp(-startlogit)) + math.log(1 + math.exp(-endlogit)))

def GetBestSpan(startLogit, endLogit, wordNum, maxSpanLen):
    best_span_logit = -10000000
    best_span_start = -1
    best_span_end = -1
    for i in range(0, wordNum):
        for j in range(i, min(i + maxSpanLen, wordNum)):
            spanlogit = SpanLogit(startLogit[i], endLogit[j])
            if(spanlogit > best_span_logit):
                best_span_logit = spanlogit
                best_span_start = i
                best_span_end = j
    return (best_span_start, best_span_end, best_span_logit)

#(self, seed_bert, seed_model, vocab_size, batch_size, max_query_length, max_seq_length, dropout, device_num):
qaPredictor = DistMrcNet(args.seed_bert, args.seed_model, args.vocabsize, args.batch_size, args.max_query_length, args.max_seq_length, 0.0, device_num)

for epoch in range(0, 1):    
    ## set training mode.
    #net.SetPredictMode()
    #net.Init()

    batch_nums = [0] * 2
    batch_nums[0] = t1_pred_data.batch_num()
    batch_nums[1] = t2_pred_data.batch_num()
    total_batch = batch_nums[0] + batch_nums[1]
    print(batch_nums[0])
    print(batch_nums[1])

    t1_iter = iter(t1_pred_loader)
    t2_iter = iter(t2_pred_loader)
    t1_per = batch_nums[0] * 1.0 / total_batch
    t2_per = batch_nums[1] * 1.0 / total_batch

    bar = Bar('prediction', max=total_batch)
    
    pred_dict = {}
    pred_span_start = {}
    pred_span_end = {}

    #for batch_idx, data in enumerate(pred_loader):
    #    data = {key: value for key, value in data.items()}
    
    for batch_idx in range(0, total_batch):
    #for batch_idx, data in enumerate(train_loader):
        rnd = random.random()
        d = 0
        if rnd < t1_per:
            d = 0
        else:
            d = 1
        if batch_nums[d] == 0:
            d = 1 - d
        if d == 0:
            data = next(t1_iter)  
        elif d == 1:
            data = next(t2_iter)
        batch_nums[d] -= 1

        data = {key: value for key, value in data.items()}

        doc_tokens = data['doc_tokens']
        query_tokens = data['query_tokens']

        over_tokens = data['over_tokens']
        over_masks = data['over_mask']
        over_segs = data['over_segments']
        
        doc_word_mask = data['doc_word_mask']

        if d == 0:
            cate_logits, start_logits, end_logits, acc_nums = qaPredictor.Predict_0(over_tokens, over_masks, over_segs, doc_word_mask)
        elif d == 1:
            cate_logits, start_logits, end_logits, acc_nums = qaPredictor.Predict_1(doc_tokens, query_tokens, over_masks, over_segs, doc_word_mask)

        #cate_logits, start_logits, end_logits, acc_nums = qaPredictor.Predict(over_tokens, over_masks, over_segs, doc_word_mask)

        p_id = data['para_id']
        turn_id = data['turn_id']

        doc_num = data['doc_word_num']
        doc_offset = data['doc_word_offset']
        doc_start = data['doc_start']
        doc_end = data['doc_end']

        for i in range(0, args.batch_size):
            paragraph_id = t1_pred_data.paragraph_id2ip(p_id[i].item())
            t_id = turn_id[i].item()

            ## max pooling over pid and tid.
            if(not (paragraph_id, t_id) in pred_dict):
                pred_dict[(paragraph_id, t_id)] = [-1000] * 6
                pred_span_start[(paragraph_id, t_id)] = [-1000] * doc_num[i].item()
                pred_span_end[(paragraph_id, t_id)] = [-1000] * doc_num[i].item()

                pred_dict[(paragraph_id, t_id)][3] = 1000; 

            #for p in range(doc_spans[i], doc_spans[i+1]):
            device_idx = int(i / qaPredictor.sub_batch_size)
            g_idx = i % qaPredictor.sub_batch_size

            for p in range(0, 6):
                if(p == 3):
                    pred_dict[(paragraph_id, t_id)][p] = min(pred_dict[(paragraph_id, t_id)][p], cate_logits[device_idx][g_idx * 6 + p]) 
                else:
                    pred_dict[(paragraph_id, t_id)][p] = max(pred_dict[(paragraph_id, t_id)][p], cate_logits[device_idx][g_idx * 6 + p]) 
                    
            for p in range(acc_nums[device_idx][g_idx], acc_nums[device_idx][g_idx + 1]):
                p_idx = doc_offset[i].item() + (p - acc_nums[device_idx][g_idx]) # (p - doc_spans[i])
                pred_span_start[(paragraph_id, t_id)][p_idx] = max(pred_span_start[(paragraph_id, t_id)][p_idx], start_logits[device_idx][p])
                pred_span_end[(paragraph_id, t_id)][p_idx] = max(pred_span_end[(paragraph_id, t_id)][p_idx], end_logits[device_idx][p])
            
        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} '.format(
                    batch=batch_idx + 1,
                    size=total_batch,
                    total=bar.elapsed_td,
                    eta=bar.eta_td
                    )
        
        bar.next()
    bar.finish()
    #net.Complete()

    all_predictions = []
    # aggragate prediction.
    for (p_id, turn_id) in pred_dict:

        cur_prediction = collections.OrderedDict()
        cur_prediction["id"] = p_id
        cur_prediction["turn_id"] = turn_id

        cate_id = numpy.argmax(pred_dict[(p_id, turn_id)])
        
        if(pred_dict[(p_id, turn_id)][3] > 0):
            cate_id = 3
        else:
            cate_id = numpy.argmax(pred_dict[(p_id, turn_id)][0:3] + pred_dict[(p_id, turn_id)][4:6])
            if(cate_id >= 3):
                cate_id += 1

        if(cate_id == 1):
            cur_prediction["answer"] = 'no'
        elif(cate_id == 2):
            cur_prediction["answer"] = 'yes'
        elif(cate_id == 3):
            cur_prediction["answer"] = 'unknown'
        elif(cate_id == 4):
            cur_prediction["answer"] = 'false'
        elif(cate_id == 5):
            cur_prediction["answer"] = 'true'
        ## get the span of the prediction.
        elif(cate_id == 0):
            (span_start, span_end, span_logit) = GetBestSpan(pred_span_start[(p_id, turn_id)], pred_span_end[(p_id, turn_id)], len(pred_span_start[(p_id, turn_id)]), args.max_span)
            text_span = t1_pred_data.getspan(p_id, span_start, span_end)
            cur_prediction["answer"] = text_span
        all_predictions.append(cur_prediction)

    with open(args.pred_path, "w") as writer:
        writer.write(json.dumps(all_predictions, indent=4) + "\n")


