
## build an framework for understanding the discrete variables in the deep neural networks.

import argparse

from CoQA_MultiTurn_Dataset import CoQA_MultiTurn_Dataset
from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import torch
import random
import math
import tokenization 
parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')


parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--train_dataset', required=True, type=str, help='train dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")

parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-s", "--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("-q", "--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")   
parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--epochs', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,2000:0.00001,50000:0.000001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')
parser.add_argument('--decay', default=0.01, type=float, metavar='DECAY', help='decay of model parameters.')
parser.add_argument('--opt', default='adambert', type=str, metavar='OPT', help='optimization algorithm.')
parser.add_argument('--ema', default='1.0', type=float, metavar='EMA', help='exp moving average.')

parser.add_argument('--is_seed', default=0, type=int, metavar='S', help='use seed model or random initialized.')

parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--output_model', default='bert_CoQA', type=str, help='output model.')

parser.add_argument('--dropout', default='0', type=float, help='droput rate.')

parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')

parser.add_argument('--skip_bad', default='1', type=int, help='skip bad quality data training. 0:noskip; 1:skip;')

parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')

parser.add_argument('--avg_cate', default='0', type=int, help='average category loss.')
parser.add_argument('--avg_span', default='0', type=int, help='average span loss.')

parser.add_argument('--cate_w', default='0.1', type=float, help='weight of category loss.')
parser.add_argument('--span_w', default='0.1', type=float, help='weight of span loss.')

parser.add_argument('--q_order', default=0, type=int, help='question order')

parser.add_argument('--turn_ids', default='1', type=str, help='train turns')

parser.add_argument('--slide', default=128, type=int, help='question order')

parser.add_argument('--t_type', default=1, type=int, help='type ids')

parser.add_argument('--gumbel_w', default=0.3, type=float, help='gumbel weight.')

parser.add_argument('--vocab', default='/data1/yelongshen/bert_model/uncased_L-12_H-768_A-12/vocab.txt', type=str, help='vocab')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

## recurrent chunk mechism for qa model.

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager, Session, ComputationGraph
from BigLearn import BaseBertModel, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure, TransformerStructure
from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData
from BigLearn import GradientOptimizer, ParameterSetting, NCCL
from BigLearn import CrossEntropyRunner

device_num = len(args.gpu_id.split(','))
print("set gpu number ", device_num)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

turn_ids = [ int(x) for x in args.turn_ids.split(',') ] 

print("Loading train Dataset . . .", args.train_dataset)

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


# corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, rseed = 110):
# (self, corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, fill_batch, is_enumate_doc_span, turn_ids, skip_bad = 0, qorder = 0, slide_win = 128, rseed = 110)

t1_train_data = CoQA_MultiTurn_Dataset(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history = 100,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), turn_ids=[1], skip_bad = args.skip_bad, qorder = 0, slide_win = args.slide)
t1_train_loader = DataLoader(t1_train_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

t2_train_data = CoQA_MultiTurn_Dataset(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history = 100,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), turn_ids=turn_ids, skip_bad = args.skip_bad, qorder = 0, slide_win = args.slide)
t2_train_loader = DataLoader(t2_train_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

#train_data = CoQA_MultiTurn_Dataset(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history = 100,
#                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), turn_ids=turn_ids, skip_bad = args.skip_bad, qorder = 0, slide_win = args.slide)

#train_loader = DataLoader(train_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

#sys.exit()

def SetupDocMask(mask_doc_tensor, mask_doc_piece, batch_size, max_seq_length):
    acc_nums = [0] * (batch_size + 1)
    acc_nums[0] = 0
    batch_inc = 0
    for b in range(batch_size):
        word_inc = 0
        for s in range(max_seq_length):
            if(mask_doc_tensor[b][s].item() > 0.5):
                mask_doc_piece[batch_inc + word_inc] = b * max_seq_length + s
                word_inc = word_inc + 1
        batch_inc = batch_inc + word_inc
        acc_nums[b + 1] = batch_inc

    mask_doc_piece.EffectiveSize = batch_inc
    mask_doc_piece.SyncFromCPU()
    return acc_nums 

def SetupLabel(cate_label, start_label, end_label, cate_piece, start_piece, end_piece, acc_nums, batch_size):
    start_piece.EffectiveSize = acc_nums[batch_size]  
    end_piece.EffectiveSize = acc_nums[batch_size]  

    cate_piece.EffectiveSize = 6 * batch_size

    start_piece.Zero()
    end_piece.Zero()
    cate_piece.Zero()

    start_only = 0
    end_only = 0
    unk_only = 0

    # ["yes", "no", "unknown", "true", "false"]
    for b in range(batch_size):
        start_idx = start_label[b]
        end_idx = end_label[b]

        batch_inc = acc_nums[b]
        word_num = acc_nums[b + 1] - acc_nums[b]

        is_start = False
        if start_idx >= 0 and start_idx < word_num:
            start_piece[batch_inc + start_idx] = 1.0
            is_start = True

        is_end = False
        if end_idx >=0 and end_idx < word_num:
            end_piece[batch_inc + end_idx] = 1.0
            is_end = True

        if is_start and is_end:
            conv = 1 
        elif not is_start and not is_end:
            conv = 0
            unk_only += 1
        elif not is_start and is_end:
            conv = end_idx / (end_idx - start_idx);
            end_only += 1;
        elif is_start and not is_end:
            conv = (word_num - start_idx) / (end_idx - start_idx);
            start_only += 1

        cate_piece[b * 6 + cate_label[b]] = conv
        cate_piece[b * 6 + 3] = 1 - conv

    start_piece.SyncFromCPU()
    end_piece.SyncFromCPU()
    cate_piece.SyncFromCPU()

    start_only_ratio = start_only * 1.0 / batch_size
    end_only_ratio = end_only * 1.0 / batch_size
    unk_ratio = unk_only * 1.0 / batch_size

    return start_only_ratio, end_only_ratio, unk_ratio

class MrcModule:
    # dual module for Generator and Discriminator 
    def __init__(self, vocab_size, layer, seed_mrc, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        # int frozenlayer, int mvocabSize, int mcate, int mlayer, int mdim, string meta_file, string bin_file, DeviceType device) 
        self.Models['mrc_model'] = self.Session.Model.AddLayer(BaseBertModel(-1, vocab_size, 2, 24, 1024, seed_mrc + 'exp.label', seed_mrc + 'exp.bin', behavior.Device))
        self.Models['cate_classifier'] =  self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 6, A_Func.Linear, True, behavior.Device))
        self.Models['span_start_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 1, A_Func.Linear, False, behavior.Device))
        self.Models['span_end_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 1, A_Func.Linear, False, behavior.Device))

        self.layer = layer

        for i in range(0, layer):
            self.Models['layer_' + str(i)] = self.Session.Model.AddLayer(TransformerStructure(1024, behavior.Device))

        self.grad = self.Session.Model.AllocateGradient(self.Behavior.Device)
        self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema))

class MrcNet:
    ## construct model 
    def __init__(self, module, batch_size, max_seq_length, max_query_length, dropout, t_type, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Module = module
        self.Models = module.Models

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length

        max_doc_length = max_seq_length - max_query_length
        self.max_doc_length = max_doc_length

        self.t_type = t_type

        emd = 1024
        att_head = 16

        self.mask_doc = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)        
        
        self.start_label = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)
        self.end_label = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)
        self.cate_label = CudaPieceFloat(batch_size * 6, self.Behavior.Device)
        
        self.masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        _dim = IntArgument('dim', self.Models['mrc_model'].embed)
        _batch = IntArgument('batch', batch_size)
        _seq = IntArgument('seq', max_seq_length)
        _batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)
        _batch_q_seq = IntArgument('batch_seq', batch_size * max_query_length)
        _default = IntArgument('default', 1)
        _q_seq = IntArgument('q_seq', max_query_length)
        _d_seq = IntArgument('d_seq', max_doc_length)

        if t_type == 0:
            self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
            w_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.tokens)
            w_embed = self.Session.Reshape(w_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", batch_size))

        elif t_type == 1:
            self.q_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)
            q_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.q_tokens)
            q_embed = self.Session.Reshape(q_embed, _dim, _q_seq, _batch)

            q_pos_ids = [i for i in range(max_query_length)]
            q_pos_embed = self.Session.LookupEmbed(self.Models['mrc_model'].PosEmbed, q_pos_ids)
            q_pos_embed = self.Session.Reshape(q_pos_embed, _dim, _q_seq, _default)

            q_embed = self.Session.Add(q_embed, q_pos_embed)

            _q_norm_emd = self.Session.Norm(q_embed, 0) 
            q_norm_emd = self.Session.DotAndAdd(_q_norm_emd, self.Models['mrc_model'].NdNormScale, self.Models['mrc_model'].NdNormBias)
            q_norm_emd = self.Session.Reshape(q_norm_emd, _dim, _q_seq, _batch)

            q_mask_bias = CudaPieceFloat(batch_size * max_query_length, self.Behavior.Device)
            q_mask_bias.Init(0.0)

            q_mask_scale = CudaPieceFloat(batch_size * max_query_length, self.Behavior.Device)
            q_mask_scale.Init(1.0 / math.sqrt(emd / att_head))

            for i in range(self.Module.layer):
                q_norm_emd = self.Session.Transformer(self.Models['layer_' + str(i)], q_norm_emd, 16, q_mask_scale, q_mask_bias, 0.1)

            self.d_tokens = CudaPieceInt(batch_size * max_doc_length, self.Behavior.Device)
            d_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.d_tokens)
            d_embed = self.Session.Reshape(d_embed, _dim, _d_seq, _batch)

            #. q_norm_emd
            w_embed = self.Session.Cat([d_embed, q_norm_emd], 1)
                
            self.q_emd = self.Session.Reshape(q_norm_emd, _dim, _batch_q_seq)
            # q_logit.
            self.decode_session()
            
        ## enable gumbel softmax in the output layer.
        elif t_type == 2:
            self.q_tokens = CudaPieceInt(batch_size * max_query_length, self.Behavior.Device)
            q_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.q_tokens)
            q_embed = self.Session.Reshape(q_embed, _dim, _q_seq, _batch)

            q_pos_ids = [i for i in range(max_query_length)]
            q_pos_embed = self.Session.LookupEmbed(self.Models['mrc_model'].PosEmbed, q_pos_ids)
            q_pos_embed = self.Session.Reshape(q_pos_embed, _dim, _q_seq, _default)

            q_embed = self.Session.Add(q_embed, q_pos_embed)

            _q_norm_emd = self.Session.Norm(q_embed, 0) 
            q_norm_emd = self.Session.DotAndAdd(_q_norm_emd, self.Models['mrc_model'].NdNormScale, self.Models['mrc_model'].NdNormBias)
            q_norm_emd = self.Session.Reshape(q_norm_emd, _dim, _q_seq, _batch)

            q_mask_bias = CudaPieceFloat(batch_size * max_query_length, self.Behavior.Device)
            q_mask_bias.Init(0.0)

            q_mask_scale = CudaPieceFloat(batch_size * max_query_length, self.Behavior.Device)
            q_mask_scale.Init(1.0 / math.sqrt(emd / att_head))

            for i in range(self.Module.layer):
                q_norm_emd = self.Session.Transformer(self.Models['layer_' + str(i)], q_norm_emd, 16, q_mask_scale, q_mask_bias, 0.1)
            
            q_norm_emd = self.Session.Reshape(q_norm_emd, _dim, _batch_q_seq)

            q_logit = self.Session.MatMul(q_norm_emd, 0, self.Models['mrc_model'].NdTokenEmbed, 1)

            q_wei = FloatArgument('gumbel_w', args.gumbel_w)
            global_update = IntArgument("global_update", 0)
            sched = RateScheduler(global_update, '0:1.0')
            q_gumbel = self.Session.GumbelSoftmax(q_logit, sched, q_wei, True)
            
            q_logit = q_gumbel.Item1
            q_ids = q_gumbel.Item2

            q_embed = self.Session.MatMul(q_logit, 0, self.Models['mrc_model'].NdTokenEmbed, 0)
            q_embed = self.Session.Reshape(q_embed, _dim, _q_seq, _batch)

            self.d_tokens = CudaPieceInt(batch_size * max_doc_length, self.Behavior.Device)
            d_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.d_tokens)
            d_embed = self.Session.Reshape(d_embed, _dim, _d_seq, _batch)

            #. q_norm_emd
            w_embed = self.Session.Cat([d_embed, q_embed], 1)

            self.q_embed = self.Session.Reshape(q_embed, _dim, _batch_q_seq)
            self.decode_session()

        ## check the generated intermedia sentence.


        pos_ids = [i for i in range(max_seq_length)]
        pos_embed = self.Session.LookupEmbed(self.Models['mrc_model'].PosEmbed, pos_ids)
        pos_embed = self.Session.Reshape(pos_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))

        seg_ids = [0 for i in range(max_doc_length)] + [1 for i in range(max_query_length)]
        seg_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenTypeEmbed, seg_ids)
        seg_embed = self.Session.Reshape(seg_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))

        embed = self.Session.Add(w_embed, pos_embed)
        embed = self.Session.Add(embed, seg_embed)

        _norm_emd = self.Session.Norm(embed, 0) 
        norm_emd = self.Session.DotAndAdd(_norm_emd, self.Models['mrc_model'].NdNormScale, self.Models['mrc_model'].NdNormBias)
        norm_emd = self.Session.Reshape(norm_emd, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", batch_size))

        self.scaleVec = CudaPieceFloat(2, self.Behavior.Device)
        self.scaleVec[0] = 0
        self.scaleVec[1] = 1.0 / math.sqrt(emd / att_head)
        self.scaleVec.SyncFromCPU()

        self.biasVec = CudaPieceFloat(2, self.Behavior.Device)
        self.biasVec[0] = -1e9
        self.biasVec[1] = 0
        self.biasVec.SyncFromCPU()

        _mask_vocab = IntArgument('mask_vocab', 2)

        #new IntArgument[] { emd_dim, firstSlice, batch_size },
        self.mask_scale = self.Session.LookupEmbed(NdArrayData(self.Behavior.Device, self.scaleVec, None, _default, _mask_vocab), self.masks)        
        self.mask_bias = self.Session.LookupEmbed(NdArrayData(self.Behavior.Device, self.biasVec, None, _default, _mask_vocab), self.masks)

        norm_emd, slice_embed = self.transformer_blocks(norm_emd, _dim, _default, _batch)

        self.build_ans_net(norm_emd, slice_embed, dropout, _dim, _batch_seq)


    def decode_session(self) : # _dim, _batch_q_seq):
        self.decode_session = Session(self.Behavior)
        #q_emd = self.decode_session.Reshape(self.q_emd, _dim, _batch_q_seq)
        q_logit = self.decode_session.MatMul(self.q_emd, 0, self.Models['mrc_model'].NdTokenEmbed, 1)
        self.q_idx = self.decode_session.Argmax(q_logit, 0) # _batch_q_seq size.


    def transformer_blocks(self, norm_emd, _dim, _default, _batch):
        for i in range(24):
            norm_emd = self.Session.Transformer(self.Models['mrc_model'].Blocks[i], norm_emd, 16, self.mask_scale.Output, self.mask_bias.Output, 0.1)
        slice_embed = self.Session.Slice(norm_emd, [0, 0, 0], _dim, _default, _batch)
        slice_embed = self.Session.Reshape(slice_embed, _dim, _batch)
        return norm_emd, slice_embed

    def build_ans_net(self, norm_emd, slice_embed, dropout, _dim, _batch_seq):
        self.embed_feature = self.Session.Dropout(norm_emd, dropout)
        self.slice_feature = self.Session.Dropout(slice_embed, dropout)

        self.cate_flag = self.Session.FNN(self.slice_feature, self.Models['cate_classifier'])
        self.cate_loss = self.Session.CrossEntropyLoss(self.cate_flag, self.cate_label, args.cate_w, False)

        self.embed_2d_feature = self.Session.Reshape(self.embed_feature, _dim, _batch_seq)
        self.doc_embed = self.Session.LookupEmbed(self.embed_2d_feature, self.mask_doc)

        self.start_flag = self.Session.FNN(self.doc_embed, self.Models['span_start_classifier'])
        self.end_flag = self.Session.FNN(self.doc_embed, self.Models['span_end_classifier'])

        self.start_loss = self.Session.CrossEntropyLoss(self.start_flag, self.start_label, args.span_w, False)
        self.end_loss = self.Session.CrossEntropyLoss(self.end_flag, self.end_label, args.span_w, False)

    def SetupTrain_0(self, over_tokens, over_mask, over_segment, doc_word_masks, cate_label, start_label, end_label):
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(over_tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(over_mask.data_ptr())), self.batch_size * self.max_seq_length)
        
        acc_nums = SetupDocMask(doc_word_masks, self.mask_doc, self.batch_size, self.max_seq_length)
        self.start_only_ratio, self.end_only_ratio, self.unk_ratio = SetupLabel(cate_label, start_label, end_label, self.cate_label, self.start_label, self.end_label, acc_nums, self.batch_size) 

    def SetupTrain_1(self, d_tokens, q_tokens, over_mask, over_segment, doc_word_masks, cate_label, start_label, end_label):
        self.q_tokens.CopyTorch(IntPtr.op_Explicit(Int64(q_tokens.data_ptr())),  self.batch_size * self.max_query_length)
        self.d_tokens.CopyTorch(IntPtr.op_Explicit(Int64(d_tokens.data_ptr())),  self.batch_size * self.max_doc_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(over_mask.data_ptr())), self.batch_size * self.max_seq_length)
        
        acc_nums = SetupDocMask(doc_word_masks, self.mask_doc, self.batch_size, self.max_seq_length)
        self.start_only_ratio, self.end_only_ratio, self.unk_ratio = SetupLabel(cate_label, start_label, end_label, self.cate_label, self.start_label, self.end_label, acc_nums, self.batch_size) 

class DistMrcNet:
        
    def __init__(self, seed_bert, cate, vocab_size, batch_size, max_query_length, max_seq_length, dropout, device_num):
        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).TrainMode
            ParameterSetting.ResetRandom()
            mrcModule = MrcModule(vocab_size, 4, seed_bert, env_mode)
            mrc_net_0 = MrcNet(mrcModule, self.sub_batch_size, max_seq_length, max_query_length, dropout, 0, env_mode)
            mrc_net_1 = MrcNet(mrcModule, self.sub_batch_size, max_seq_length, max_query_length, dropout, args.t_type, env_mode)
            return mrcModule,mrc_net_0, mrc_net_1

        self.device_num = device_num

        self.batch_size = batch_size
        self.sub_batch_size = (int)(batch_size / device_num)

        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length

        #self.pool.map(__construct_model, range(device_num))

        self.modules = []
        self.models_0 = [] #* device_num
        self.models_1 = []
        for idx in range(device_num):
            module, m_0, m_1 = __construct_model(idx)
            self.modules.append(module)
            self.models_0.append(m_0)
            self.models_1.append(m_1)

        self.grad_reducer = NCCL(device_num)

        self.sessions_0 = [m.Session for m in self.models_0]
        self.sessions_1 = [m.Session for m in self.models_1]
        self.sessions_decode = [m.decode_session for m in self.models_1]

        self.grads = [m.Module.grad for m in self.models_0]
        self.gradSessions = [m.Module.Session for m in self.models_0]

    def Train_0(self, over_tokens, over_mask, over_segment, doc_word_masks, cate_label, start_label, end_label):
        def __setup(device_id):
            self.models_0[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            #l_start = device_id * self.sub_batch_size
            #l_end = (device_id + 1) * self.sub_batch_size

            self.models_0[device_id].SetupTrain_0(over_tokens[b_start : b_end], over_mask[b_start : b_end], over_segment[b_start : b_end], doc_word_masks[b_start : b_end], cate_label[b_start : b_end], start_label[b_start : b_end], end_label[b_start : b_end])
        
        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        self.grad_reducer.AllRun(self.sessions_0, self.device_num)

        self.grad_reducer.AllReduceUpdate(self.gradSessions, self.grads, self.device_num)
        # Execute(self.sessions, self.grads, self.device_num)

        loss_es = [ (m.cate_loss.Value, m.start_loss.Value, m.end_loss.Value) for m in self.models_0 ]
        ratio_es = [ (m.start_only_ratio, m.end_only_ratio, m.unk_ratio) for m in self.models_0 ]

        loss_avg = [ x / self.device_num for x in [sum(loss) for loss in zip(*loss_es)]]
        ratio_avg = [x /self.device_num for x in [sum(ratio) for ratio in zip(*ratio_es)]]
        return loss_avg, ratio_avg

    def Train_1(self, d_tokens, q_tokens, over_mask, over_segment, doc_word_masks, cate_label, start_label, end_label):
        def __setup(device_id):
            self.models_1[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            #l_start = device_id * self.sub_batch_size
            #l_end = (device_id + 1) * self.sub_batch_size

            self.models_1[device_id].SetupTrain_1(d_tokens[b_start : b_end], q_tokens[b_start : b_end], over_mask[b_start : b_end], over_segment[b_start : b_end], doc_word_masks[b_start : b_end], cate_label[b_start : b_end], start_label[b_start : b_end], end_label[b_start : b_end])
        
        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        self.grad_reducer.AllRun(self.sessions_1, self.device_num)

        self.grad_reducer.AllReduceUpdate(self.gradSessions, self.grads, self.device_num)
        # Execute(self.sessions, self.grads, self.device_num)

        loss_es = [ (m.cate_loss.Value, m.start_loss.Value, m.end_loss.Value) for m in self.models_1 ]
        ratio_es = [ (m.start_only_ratio, m.end_only_ratio, m.unk_ratio) for m in self.models_1 ]

        loss_avg = [ x / self.device_num for x in [sum(loss) for loss in zip(*loss_es)]]
        ratio_avg = [x /self.device_num for x in [sum(ratio) for ratio in zip(*ratio_es)]]
        return loss_avg, ratio_avg

    def Decode(self):
        self.grad_reducer.AllRun(self.sessions_decode, self.device_num)

        q_tok_ids = torch.zeros([self.batch_size, self.max_query_length], dtype = torch.int32)

        m_p = 0
        for m in self.models_1:
            m.Behavior.Setup()
            m.q_idx.SyncFromCPU()

            for l in range(self.sub_batch_size * self.max_query_length):
                m_idx = int(l / self.max_query_length)
                q_idx = int(l % self.max_query_length)

                q_tok_ids[int(m_p * self.sub_batch_size) + m_idx][q_idx] = m.q_idx[l]
                # tok_ids[b][i] = self.decode_tokens[i][b]
            m_p += 1
        
        #q_tok_ids.PasteTorch(IntPtr.op_Explicit(Int64(tok_len.data_ptr())), self.batch_size)
        return q_tok_ids

print("Create bert model and setup enviroument")

# seed_bert, cate, vocab_size, batch_size, max_query_length, max_seq_length, dropout, device_num):
mrcTrainer = DistMrcNet(args.seed_bert, 6, args.vocabsize, args.batch_size, args.max_query_length, args.max_seq_length, args.dropout, device_num)
mrcTrainer.modules[0].Session.Model.Save(args.output_model + '.init.model')

tokenizer = tokenization.FullTokenizer(args.vocab) 


for epoch in range(0, args.epochs):
    print('\nEpoch: [%d | %d] LR: %f' % (epoch + 1, args.epochs, args.lr))
    
    ## set training mode.
    #net.SetTrainMode()
    #net.Init()
    
    cate_loss = AverageMeter()
    start_loss = AverageMeter()
    end_loss = AverageMeter()

    unk_loss = AverageMeter()
    lefto_loss = AverageMeter()
    righto_loss = AverageMeter()
    
    batch_nums = [0] * 2

    batch_nums[0] = t1_train_data.batch_num()
    batch_nums[1] = t2_train_data.batch_num()

    total_batch = batch_nums[0] + batch_nums[1]

    print(batch_nums[0])
    print(batch_nums[1])

    t1_iter = iter(t1_train_loader)
    t2_iter = iter(t2_train_loader)

    t1_per = batch_nums[0] * 1.0 / total_batch
    t2_per = batch_nums[1] * 1.0 / total_batch

    bar = Bar('training', max= total_batch)

    log_out_file = open(args.output_model + '.'+str(epoch)+'.log','w')

    for batch_idx in range(0, total_batch):
    #for batch_idx, data in enumerate(train_loader):
        rnd = random.random()
        d = 0
        if rnd < t1_per:
            d = 0
        else:
            d = 1
        if batch_nums[d] == 0:
            d = 1 - d
        if d == 0:
            data = next(t1_iter)  
        elif d == 1:
            data = next(t2_iter)
        batch_nums[d] -= 1

        data = {key: value for key, value in data.items()}

        doc_tokens = data['doc_tokens']
        query_tokens = data['query_tokens']

        over_tokens = data['over_tokens']
        over_masks = data['over_mask']
        over_segs = data['over_segments']
        
        doc_word_mask = data['doc_word_mask']

        start_label = data['start_position']
        end_label = data['end_position']

        cate_label = data['yes_no_ans']

        if d == 0:
            cse_loss, seu_ratio = mrcTrainer.Train_0(over_tokens, over_masks, over_segs, doc_word_mask, cate_label, start_label, end_label)
        elif d == 1:
            cse_loss, seu_ratio = mrcTrainer.Train_1(doc_tokens, query_tokens, over_masks, over_segs, doc_word_mask, cate_label, start_label, end_label)            
            q_token_idx = mrcTrainer.Decode()

            #
            for b in range(args.batch_size):
                q_org_tokens = tokenizer.convert_ids_to_tokens(query_tokens[b].numpy())
                q_new_tokens = tokenizer.convert_ids_to_tokens(q_token_idx[b].numpy())

                log_out_file.write(' '.join(q_org_tokens) + '\t' + ' '.join(q_new_tokens) + '\n')
            ## let's logout the intermedia hidden states.
        ## check the 


        cate_loss.update(cse_loss[0])
        start_loss.update(cse_loss[1])
        end_loss.update(cse_loss[2])

        lefto_loss.update(seu_ratio[0])
        righto_loss.update(seu_ratio[1])
        unk_loss.update(seu_ratio[2])

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | cate_loss: {cate_loss.val:.4f} ({cate_loss.avg:.4f}) | start_loss: {start_loss.val:.4f} ({start_loss.avg:.4f}) | end_loss : {end_loss.val:.4f} ({end_loss.avg:.4f}) | unk_avg: {avg_unk:.4f} | lefto_avg: {avg_lefto:.4f} | righto_avg : {avg_righto:.4f} '.format(
                    batch=batch_idx + 1,
                    size=total_batch,
                    total=bar.elapsed_td,
                    eta=bar.eta_td,

                    cate_loss=cate_loss,
                    start_loss=start_loss,
                    end_loss=end_loss,
                    
                    avg_unk=unk_loss.avg,
                    avg_lefto=lefto_loss.avg,
                    avg_righto=righto_loss.avg                    
                    )
        bar.next()
        #break
    #break
    bar.finish()
    log_out_file.close()

    mrcTrainer.modules[0].Session.Model.Save(args.output_model + '.'+str(epoch)+'.model')

           
        #print(data['para_id'])

        
        