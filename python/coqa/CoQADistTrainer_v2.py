import argparse

from CoQA_Dataset_v2 import CoQA_Dataset_v2
from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--train_dataset', required=True, type=str, help='train dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-s", "--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("-q", "--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")   
parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--epochs', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,2000:0.00001,50000:0.000001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')
parser.add_argument('--decay', default=0.01, type=float, metavar='DECAY', help='decay of model parameters.')
parser.add_argument('--opt', default='adambert', type=str, metavar='OPT', help='optimization algorithm.')
parser.add_argument('--ema', default='1.0', type=float, metavar='EMA', help='exp moving average.')

parser.add_argument('--is_seed', default=0, type=int, metavar='S', help='use seed model or random initialized.')

parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--output_model', default='bert_CoQA', type=str, help='output model.')

parser.add_argument('--dropout', default='0', type=float, help='droput rate.')

parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')

parser.add_argument('--skip_bad', default='1', type=int, help='skip bad quality data training. 0:noskip; 1:skip;')

parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')

#parser.add_argument('--unknown', default='0', type=int, help='unknown category.')
#parser.add_argument('--distavg', default='0', type=int, help='0:default weight between type_loss and span_loss; 1:standard normalize')

parser.add_argument('--avg_cate', default='0', type=int, help='average category loss.')
parser.add_argument('--avg_span', default='0', type=int, help='average span loss.')

parser.add_argument('--cate_w', default='0.1', type=float, help='weight of category loss.')
parser.add_argument('--span_w', default='0.1', type=float, help='weight of span loss.')

parser.add_argument('--q_order', default=0, type=int, help='question order')

parser.add_argument('--turn_ids', default='1', type=str, help='train turns')

parser.add_argument('--slide', default=128, type=int, help='question order')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior
from BigLearn.DeepNet.Language import LargeBertQA, DistLargeBertQA

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

turn_ids = [ int(x) for x in args.turn_ids.split(',') ] 

print("Loading train Dataset . . .", args.train_dataset)
# corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, rseed = 110):
train_data = CoQA_Dataset_v2(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history=20,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), turn_ids=turn_ids, skip_bad=args.skip_bad, qorder=args.q_order, slide_win=args.slide)

train_loader = DataLoader(train_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

#print("Loading dev Dataset . . .", args.dev_dataset)
#dev_data = CoQA_Dataset_v1(corpus_path=args.dev_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length,
#                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize)
#dev_loader = DataLoader(dev_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=False, drop_last=False)

print("Create bert model and setup enviroument")
#device_id = 0 # int(args.gpu_id)
#env_mode = DeviceBehavior(device_id).TrainMode

layer = 24
#if args.is_seed == 0:
#    model = LargeBertQA.BertQAModel(layer, env_mode.Device)
#else:
# int frozenlayer, int special_category, string meta_file, string bin_file,   DeviceType device)

# span, no, yes, unknown, false, true,
model = LargeBertQA.BertQAModel(-1, 6, args.seed_bert + 'exp.label', args.seed_bert + 'exp.bin', DeviceType.CPU) # env_mode.Device)

#model.InitOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr), env_mode)

# BertQAModel model, RunnerBehavior behavior)
#net = LargeBertQA(model, env_mode)

if(args.opt == 'adambert'):
    opt = StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema)
elif(args.opt == 'sgd'):
    opt = StructureLearner.SGDLearner(args.lr, args.grad_clip, args.schedule_lr, 0.9, args.decay)
elif(args.opt == 'adamdelta'):
    opt = StructureLearner.AdamDeltaLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema)
net = DistLargeBertQA(model, opt, deviceNum)


# int isAvgCateLoss, int isAvgSpanLoss, float cateWeight, float spanWeight) 
net.BuildCG(args.batch_size, args.max_seq_length, args.dropout, args.pool, args.avg_cate, args.avg_span, args.cate_w, args.span_w)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

# BuildCG(int batchSize, int max_seq_len) 
#net.BuildCG(args.batch_size, args.max_seq_length)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


for epoch in range(0, args.epochs):
    print('\nEpoch: [%d | %d] LR: %f' % (epoch + 1, args.epochs, args.lr))
    
    ## set training mode.
    net.SetTrainMode()
    net.Init()
    
    cate_loss = AverageMeter()
    start_loss = AverageMeter()
    end_loss = AverageMeter()

    unk_loss = AverageMeter()
    lefto_loss = AverageMeter()
    righto_loss = AverageMeter()

    bar = Bar('training', max=len(train_loader))
    
    for batch_idx, data in enumerate(train_loader):
        # 0. batch_data will be sent into the device(GPU or cpu)
        #print(batch_idx)
        #print(data)
        data = {key: value for key, value in data.items()}
        input_tokens = data['token_input']
        input_segments = data['seg_input']
        input_mask = data['mask_input']

        doc_mask = data['doc_mask']
        query_mask = data['query_mask']

        start_position = data['start_position']
        end_position = data['end_position']

        yes_no_ans = data['yes_no_ans']

        p_tokens = IntPtr.op_Explicit(Int64(input_tokens.data_ptr()))
        p_segs = IntPtr.op_Explicit(Int64(input_segments.data_ptr()))
        p_msks = IntPtr.op_Explicit(Int64(input_mask.data_ptr()))
        
        p_doc_mask = IntPtr.op_Explicit(Int64(doc_mask.data_ptr()))
        p_qry_mask = IntPtr.op_Explicit(Int64(query_mask.data_ptr()))
        
        p_start_pos = IntPtr.op_Explicit(Int64(start_position.data_ptr()))
        p_end_pos = IntPtr.op_Explicit(Int64(end_position.data_ptr()))

        p_yes_no = IntPtr.op_Explicit(Int64(yes_no_ans.data_ptr()))

        #Run(IntPtr tokenIdx, IntPtr segIdx, IntPtr maskIdx, IntPtr maskDoc, IntPtr specLabel, IntPtr startLabel, IntPtr endLabel, int batchSize)

        c_loss, s_loss, e_loss, unk, lefto, righto = net.Run(p_tokens, p_segs, p_msks, p_doc_mask, p_yes_no, p_start_pos, p_end_pos, args.batch_size) 
        cate_loss.update(c_loss)
        start_loss.update(s_loss)
        end_loss.update(e_loss)

        unk_loss.update(unk)
        lefto_loss.update(lefto)
        righto_loss.update(righto)

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | cate_loss: {cate_loss:.4f} | start_loss: {start_loss:.4f} | end_loss : {end_loss:.4f} | avg_cate_loss: {avg_cate_loss:.4f} | avg_start_loss: {avg_start_loss:.4f} | avg_end_loss : {avg_end_loss:.4f} | unk_avg: {avg_unk:.4f} | lefto_avg: {avg_lefto:.4f} | righto_avg : {avg_righto:.4f} '.format(
                    batch=batch_idx + 1,
                    size=len(train_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    cate_loss=c_loss,
                    start_loss=s_loss,
                    end_loss=e_loss,
                    avg_cate_loss=cate_loss.avg,
                    avg_start_loss=start_loss.avg,
                    avg_end_loss=end_loss.avg,
                    avg_unk=unk_loss.avg,
                    avg_lefto=lefto_loss.avg,
                    avg_righto=righto_loss.avg                    
                    )
        bar.next()
        #break
    #break
    bar.finish()
    net.Complete()
    net.SaveModel(args.output_model + '.' + str(epoch) + '.model')
