import argparse

from CoQA_Dataset_v1 import CoQA_Dataset_v1
from torch.utils.data import DataLoader
import sys
import torch

import numpy
import math
import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import json

import collections
from collections import Counter
from collections import OrderedDict

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--pred_dataset', required=True, type=str, help='predict dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-s", "--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("-q", "--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")
parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--seed_model', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')
parser.add_argument("--pred_path", type=str, default="pred.txt", help="prediction file name.")
parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')
parser.add_argument('--max_span', default='30', type=int, help='max prediction span.')
parser.add_argument('--q_order', default=0, type=int, help='question order')
parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')

# moving slide.
parser.add_argument('--slide', default=128, type=int, help='chunk slides')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior
from BigLearn.DeepNet.Language import LargeBertQA, DistLargeBertQA

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Loading prediction Dataset . . .", args.pred_dataset)

pred_data = CoQA_Dataset_v1(corpus_path=args.pred_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history=20,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), qorder=args.q_order, slide_win=args.slide)

chunk_num = {}

for i in range(0, len(pred_data.doc_spans)):
    example_id = pred_data.doc_spans[i][0]
    example = pred_data.examples[example_id]
    example_pid = example.paragraph_id
    paragraph_id = pred_data.paragraph_id2ip(example_pid)
    turn_id = example.turn_id

    mkey = paragraph_id + '###' + str(turn_id) 
    if mkey in chunk_num:
        chunk_num[mkey] += 1
    else:
        chunk_num[mkey] = 1



with open(args.pred_dataset + '.slide.'+str(args.slide)+'.stat', "w") as writer:
    for p_id in chunk_num:
        writer.write(p_id + '\t' + str(chunk_num[p_id]) + '\n')


