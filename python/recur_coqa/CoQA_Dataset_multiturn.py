from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import json
import math

import pickle
import numpy as np

import os
import tempfile
import argparse

import tqdm
from tqdm import tqdm, trange

import random
import collections

import torch
from torch.utils.data import Dataset

# recurrent coqa.
class CoQAExample(object):
    """
    a single training/test example for CoQA dataset.
    """
    def __init__(self,
                 paragraph_id,
                 turn_id,
                 doc,
                 doc_tokens,
                 doc_w2tokens,
                 doc_woffset,
                 doc_wnum,
                 query_tokens,
                 query_mask,
                 start_position=None,
                 end_position=None,
                 yes_no_flag=None,
                 yes_no_ans=None):

        self.paragraph_id = paragraph_id
        self.turn_id = turn_id

        self.doc = doc
        self.doc_tokens = doc_tokens
        self.doc_w2tokens = doc_w2tokens
        self.doc_woffset = doc_woffset
        self.doc_wnum = doc_wnum

        self.query_tokens = query_tokens
        self.query_mask = query_mask

        self.start_position = start_position
        self.end_position = end_position
        self.yes_no_flag = yes_no_flag
        self.yes_no_ans = yes_no_ans

class CoQA_Dataset_multiturn(Dataset):
    ## corpus path, appending queries into history.
    def __init__(self, corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, fill_batch, is_enumate_doc_span, rseed = 110):
        self.corpus_path = corpus_path
        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length
        self.max_query_history = max_query_history
        
        self.cls_token = cls_token
        self.sep_token = sep_token
        self.pad_token = pad_token
        self.vocabsize = vocabsize

        ## enumate examples as doc span. 
        self.is_enumate_doc_span = is_enumate_doc_span
        self.fill_batch = fill_batch

        self.para_dict = {}
        self.para_list = []

        self.examples = self.read_coqa_examples(corpus_path)
        
        self.doc_spans = self.get_example_spans(self.examples)

        self.rng = random.Random(rseed)

        self.example_num = len(self.examples)
        self.doc_span_num = len(self.doc_spans)

        
        self.example_order = [i for i in range(0, self.example_num)]
        
        print('all example number :', self.example_num, 'all doc span number :', self.doc_span_num)

    def reset_shuffle_examples(self):
        self.rng.shuffle(self.example_order)
        example_cur = 0
                
        record_states = [(-1, -1, 0)] * self.fill_batch
        
        self.record = []

        if(self.is_enumate_doc_span):
            assert 1 == 0

        for i in range(0, self.minibatch()):
            for j in range(0, self.fill_batch):
                example_id = (record_states[j][0])
                turn_id = record_states[j][1]

                if(example_id == -1 or turn_id >= len(self.examples[example_id]) -1): 
                    example_id = example_order[example_cur]
                    example_cur += 1
                    example_cur = example_cur % len(example_order)
                    turn_id = 0
                else:
                    turn_id += 1

                record_states[j] = (example_id, turn_id, 0)
                self.record.append(record_states[j])

    def minibatch(self):
        if(self.is_enumate_doc_span):
            return int((self.doc_span_num + self.fill_batch - 1)/ self.fill_batch) 
        else:
            return int((self.example_num + self.fill_batch - 1)/ self.fill_batch) 

    def __len__(self):
        return self.minibatch() * self.fill_batch

    def __getitem__(self, item):
        #if(self.is_enumate_doc_span):
        #    (p_id, turn_id, doc_word_num, doc_word_offset, doc_start, doc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans) = self.doc_span_sample(item)
        #else:
        (p_id, turn_id, doc_word_num, doc_word_offset, doc_start, doc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans) = self.example_sample(item)

        tok_len = len(tokens)
        padding = [self.pad_token for _ in range(self.max_seq_length - tok_len)]
        
        tokens.extend(padding)
        segment_ids.extend(padding)
        doc_mask.extend(padding)
        query_mask.extend(padding)

        one_padding = [1 for _ in range(tok_len)]
        zero_padding = [0 for _ in range(self.max_seq_length - tok_len)]

        mask_ids = []
        mask_ids.extend(one_padding)
        mask_ids.extend(zero_padding)

        output = {"para_id" : p_id,
                  "turn_id" : turn_id,

                  "doc_word_num" : doc_word_num,
                  "doc_word_offset" : doc_word_offset,
                  
                  "doc_start" : doc_start,
                  "doc_end" : doc_end,

                  "token_input" : tokens,
                  "seg_input" : segment_ids,
                  "mask_input" : mask_ids,

                  "doc_mask" : doc_mask,
                  "query_mask" : query_mask,

                  "start_position" : start_position,
                  "end_position" : end_position,
                  
                  "yes_no_ans": yes_no_ans }

        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
    
    def paragraph_ip2id(self, ipx):
        if(not ipx in self.para_dict):
            self.para_dict[ipx] = len(self.para_list)
            self.para_list.append(ipx)
        return self.para_dict[ipx]
        
    def paragraph_id2ip(self, idx):
        return self.para_list[idx]

    def getspan(self, pid, startword, endword):
        idx = self.paragraph_ip2id(pid)
        story = self.story[idx]
        story_wordoffset = self.story_wordoffset[idx]
        soffset = story_wordoffset[startword][0]
        eoffset = story_wordoffset[endword][1]
        return story[soffset : eoffset]

    def read_coqa_examples(self, input_file):
        """
        read a CoQA json file into a list of QA examples
        """
        with open(input_file, "r", encoding='utf-8') as reader:
            input_data = json.load(reader)['data']
        
        self.story = {}
        self.story_wordoffset = {}
        
        span_stat = {}

        examples = []
        for entry in input_data:
            # process story text
            paragraph_id = entry["id"]
            idx = self.paragraph_ip2id(paragraph_id)

            story = entry["story"]
            story_tokens = entry["story_subtoken_idx"]
            story_word2subtokens = entry["story_word2subtoken"]
            story_wordoffset = entry["story_wordoffset"]
            story_wordnum = len(story_wordoffset)

            assert len(story_word2subtokens) == len(story_wordoffset) + 1

            self.story[idx] = story
            self.story_wordoffset[idx] = story_wordoffset

            qa_turns = []
            
            question_history = []
            for (question, ans) in zip(entry['questions'], entry['answers']):

                cur_question_tokens = question['input_text_subtoken_idx']
                question_history.append(cur_question_tokens)

                question_id = int(question["turn_id"])
                ans_id = int(ans["turn_id"])
                
                assert question_id == ans_id

                yes_no_flag = int(ans["yes_no_flag"])
                yes_no_ans = int(ans["yes_no_ans"])

                start_position = int(ans['word_span_start'])
                end_position = int(ans['word_span_end'])

                # statistic on : span length 
                span_len = end_position - start_position
                if(span_len in span_stat):
                    span_stat[span_len] += 1
                else:
                    span_stat[span_len] = 1

                his_question_tokens = []
                his_question_mask = []

                for h in range(min(question_id, self.max_query_history), 0, -1):
                    his_question_tokens.extend(question_history[question_id - h])
                    his_question_tokens.append(self.sep_token)
                    if(h == 1):
                        mask_padding = [1 for _ in range(len(question_history[question_id - h]) + 1)]
                    else:
                        mask_padding = [0 for _ in range(len(question_history[question_id - h]) + 1)]
                    his_question_mask.extend(mask_padding)

                example = CoQAExample(
                    paragraph_id=idx,
                    turn_id=question_id,
                    doc=story,
                    doc_tokens=story_tokens,
                    doc_w2tokens=story_word2subtokens,
                    doc_woffset=story_wordoffset,
                    doc_wnum=story_wordnum,
                    
                    query_tokens=his_question_tokens,
                    query_mask=his_question_mask,

                    start_position=start_position,
                    end_position=end_position,
                    yes_no_flag=yes_no_flag,
                    yes_no_ans=yes_no_ans)
                
                qa_turns.append(example)
            
            examples.append(qa_turns)
        print('span statistic:')
        print(span_stat)
        return examples

    def get_example_spans(self, examples):
        doc_spans = []
        max_num_tokens = self.max_seq_length - 2
        example_idx = 0
        #span_count = [0] * 10;
        for qa_turns in examples:
            for turn_id in range(0, len(qa_turns)):
                example = qa_turns[turn_id]

                query_len = min(self.max_query_length, len(example.query_tokens))

                max_doc_len = max_num_tokens - query_len
                
                all_doc_tokens = (example.doc_tokens)
                
                seg = 0
                start_offset = 0
                while start_offset < len(all_doc_tokens):
                    length = len(all_doc_tokens) - start_offset
                    if length > max_doc_len:
                        length = max_doc_len
                    doc_spans.append((example_idx, turn_id, start_offset, length))
                    if start_offset + length == len(all_doc_tokens):
                        break
                    start_offset += min(length, 128)
                    seg += 1
            #if(seg >= 9):
            #    seg = 9    
            #span_count[seg] += 1    
            #doc_segments.append(doc_spans)
            example_idx += 1

        #for i in range(0, 10):
        #    print("doc span distribution ", i, ":", span_count[i])
        return doc_spans

    def get_word_mask(self, word2token, tokenlen):
        word_mask = []
        widx = 0
        for i in range(0, tokenlen):
            if(word2token[widx] == i):
                word_mask.append(1)
                widx += 1
            else:
                word_mask.append(0)
        return word_mask
        
    def example_sample(self, idx):
        # sample another example.
        #if(idx >= self.example_num):
        #    idx = self.rng.randint(0, self.example_num - 1)
        (example_idx, turn_idx, _) = self.record[idx]

        example = self.examples[example_idx][turn_idx]

        # Account for [CLS], [SEP]
        max_num_tokens = self.max_seq_length - 2

        tokens_a = example.doc_tokens
        doc_word_mask = self.get_word_mask(example.doc_w2tokens, len(tokens_a))

        tokens_b = example.query_tokens
        tokens_b_mask = example.query_mask

        if len(tokens_b) > self.max_query_length:
            tokens_b = tokens_b[-1 * self.max_query_length : ]
            tokens_b_mask = tokens_b_mask[-1 * self.max_query_length : ]
        max_doc_len = max_num_tokens - len(tokens_b)
        
        yes_no_flag = example.yes_no_flag
        yes_no_ans = example.yes_no_ans

        start_position = example.start_position
        end_position = example.end_position

        if(len(tokens_a) <= max_doc_len):
            left_num = 0
            trunc_start = 0
            trunc_end = len(tokens_a)
            tokens_a_mask = doc_word_mask
        else:
            loop = 0
            while(loop < 100):
                start_pos = self.rng.randint(0, len(tokens_a) - max_doc_len)
                trunc_start = start_pos
                trunc_end = start_pos + max_doc_len
                
                left_num = sum(doc_word_mask[ : trunc_start])
                mask_num = sum(doc_word_mask[trunc_start : trunc_end])

                if(start_position == -1 or end_position == -1):
                    break
                elif(start_position >= left_num and end_position < left_num + mask_num):
                    break
                loop += 1
                
            tokens_a = tokens_a[trunc_start : trunc_end]
            tokens_a_mask = doc_word_mask[trunc_start : trunc_end]

        start_position = start_position - left_num
        end_position = end_position - left_num

        tokens = []
        segment_ids = []
        doc_mask = []
        query_mask = []

        tokens.append(self.cls_token)
        tokens.extend(tokens_a)
        tokens.append(self.sep_token)
        tokens.extend(tokens_b)

        segment_ids.append(0)
        segment_ids.extend([0 for _ in range(len(tokens_a))])
        segment_ids.append(0)
        segment_ids.extend([1 for _ in range(len(tokens_b))])
        
        doc_mask.append(0)
        doc_mask.extend(tokens_a_mask)
        doc_mask.append(0)
        doc_mask.extend([0 for _ in range(len(tokens_b))])
        
        query_mask.append(0)
        query_mask.extend([0 for _ in range(len(tokens_a))])
        query_mask.append(0)
        query_mask.extend(tokens_b_mask)

        assert len(tokens) <= self.max_seq_length
        assert len(segment_ids) <= self.max_seq_length
        assert len(doc_mask) <= self.max_seq_length
        assert len(query_mask) <= self.max_seq_length

        if(yes_no_flag == 1):
            return (example.paragraph_id, example.turn_id, example.doc_wnum, left_num, trunc_start, trunc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans + 1)
        else:
            return (example.paragraph_id, example.turn_id, example.doc_wnum, left_num, trunc_start, trunc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, 0) # yes_no_flag, yes_no_ans)

        
