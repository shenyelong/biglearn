import gym
from PIL import Image

env = gym.make('CartPole-v0')
env.reset()
for _ in range(10):
    #env.render()
    img = env.render(mode = 'rgb_array')
    img = Image.fromarray(img)
    img.save('./gif/{}.jpg'.format(t)) 
    
    env.step(env.action_space.sample()) # take a random action
env.close()
