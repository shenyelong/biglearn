from __future__ import division
from torchvision import models
from torchvision import transforms
from PIL import Image

import argparse
import torch
import torchvision
import torch.nn as nn
import numpy as np
import math

import sys
import os
import clr as net_clr
from System import Array, IntPtr, Int32, Int64

import tokenization

parser = argparse.ArgumentParser(description='Bert Neural Style Transformer.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')

parser.add_argument('--content', type=str, default='content path')
parser.add_argument('--style', type=str, default='style path')
parser.add_argument('--vocab', type=str, default='vocab file')
parser.add_argument('--max_size', type=int, default=400)
parser.add_argument('--total_step', type=int, default=2000)
parser.add_argument('--log_step', type=int, default=10)
parser.add_argument('--sample_step', type=int, default=100)
parser.add_argument('--style_weight', type=float, default=100.0)
parser.add_argument('--lr', type=float, default=0.00001)
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")
parser.add_argument("--seed_model", type=str, default="seed_model", help="seed model")
args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, IntArgument, CudaPieceFloat, CudaPieceInt, Session, NdArrayData, ComputationGraph

from BigLearn.DeepNet.Language import Bert, DistBert
#from BigLearn.DeepNet.Image import ImageNet_VGG16Net, DistImageNet_VGG16Net

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create bert model and setup enviroument")
env_mode = DeviceBehavior(0).TrainMode

# define bert style net. it appears doesn't work.
class BertStyleNet:
    def __init__(self, bert_model, behavior):
        self.Model = bert_model
        self.Behavior = behavior
        self.Session = Session(behavior)
        self.Features = {}

    def Build(self, word_embed):
        type_ids = [0 for _ in range(512)]
        type_embed = self.Session.LookupEmbed(self.Model.TokenTypeEmbed, type_ids)
        
        pos_ids = [i for i in range(512)]
        pos_embed = self.Session.LookupEmbed(self.Model.PosEmbed, pos_ids)

        wt_embed = self.Session.Add(word_embed, type_embed)
        wtp_embed = self.Session.Add(wt_embed, pos_embed)

        _norm_wtp = self.Session.Norm(wtp_embed, 0) 
        norm_wtp = self.Session.DotAndAdd(_norm_wtp, self.Model.NdNormScale, self.Model.NdNormBias)

        norm_wtp = self.Session.Reshape(norm_wtp, IntArgument("embed", 768), IntArgument("seq", 512), IntArgument("batch_size", 1))
        
        mask_scale = CudaPieceFloat([1.0 / math.sqrt(768 / 12) for _ in range(512)], self.Behavior.Device)
        mask_bias = CudaPieceFloat([0.0 for _ in range(512)], self.Behavior.Device)

        for i in range(12):
            norm_wtp = self.Session.Transformer(self.Model.Blocks[i], norm_wtp, 12, mask_scale, mask_bias, 0.0)
            self.Features["transformer-"+str(i)] = self.Session.Reshape(norm_wtp, IntArgument("embed", 768), IntArgument("seq", 512))
        
    def BuildFeatureExtractor(self, word_ids):
        word_embed = self.Session.LookupEmbed(self.Model.TokenEmbed, word_ids)
        self.Build(word_embed)    

    def GetFeatureMaps(self, fealist):
        features = []
        for name in fealist:
            features.append(self.Features[name])
        return features

def main():
    tokenizer = tokenization.FullTokenizer(args.vocab) 

    content_text = tokenization.convert_to_unicode(open(args.content).readline())
    style_text = tokenization.convert_to_unicode(open(args.style).readline())

    content_tokens = tokenizer.tokenize(content_text)
    style_tokens = tokenizer.tokenize(style_text)

    content_ids = [101] + tokenizer.convert_tokens_to_ids(content_tokens)
    style_ids = [101] + tokenizer.convert_tokens_to_ids(style_tokens)

    if(len(content_ids) < 512):
        padding = [0 for _ in range(512 - len(content_ids))]
        content_ids.extend(padding)

    if(len(style_ids) < 512):
        padding = [0 for _ in range(512 - len(style_ids))]
        style_ids.extend(padding)

    model = Bert.BertBaseModel(2, args.seed_model + 'exp.label', args.seed_model + 'exp.bin', env_mode.Device)

    select_features = ['transformer-0'] #, 'transformer-3','transformer-5','transformer-7','transformer-9','transformer-11'] # 'conv-2-1','conv-3-1', 'conv-4-1', 'conv-1-1', 
    feature_num = len(select_features)

    content_net = BertStyleNet(model, env_mode)
    content_net.BuildFeatureExtractor(content_ids)
    con_features = content_net.GetFeatureMaps(select_features)
    
    style_net = BertStyleNet(model, env_mode)
    style_net.BuildFeatureExtractor(style_ids)
    c_st_features = style_net.GetFeatureMaps(select_features)

    style_features = []
    for i in range(feature_num):
        s_fea = style_net.Session.MatMul(c_st_features[i], 1, c_st_features[i], 0)
        style_features.append(s_fea)
    
    target_encode_session = Session(env_mode)
    target_embed = target_encode_session.LookupEmbed(model.TokenEmbed, style_ids)
    

    target_decode_session = Session(env_mode)
    token_l2_norm = target_decode_session.L2Norm(model.NdTokenEmbed, 0)
    token_l2 = target_decode_session.Dot(token_l2_norm, token_l2_norm)
    token_l2 = target_decode_session.Reshape(token_l2, token_l2.Shape[1], token_l2.Shape[0])
    token_score = target_decode_session.MatMul(target_embed, 0, model.NdTokenEmbed, 1)
    token_score = target_decode_session.Add(token_score, 2.0, token_l2, -1.0)
    token_idx = target_decode_session.Argmax(token_score, 0)

    transfer_net = BertStyleNet(model, env_mode)
    transfer_net.Build(target_embed)
    target_features = transfer_net.GetFeatureMaps(select_features)

    con_losses = []
    style_losses = []
    #style_mses = []
    #target_style_features = []
    for i in range(feature_num):
        con_mse = transfer_net.Session.Add(con_features[i], 1.0, target_features[i], -1.0)
        con_mse_loss = transfer_net.Session.MSE(con_mse, 1.0, 0.0)
        con_losses.append(con_mse_loss)

        s_fea = transfer_net.Session.MatMul(target_features[i], 1, target_features[i], 0)
        #target_style_features.append(s_fea)

        style_mse = transfer_net.Session.Add(style_features[i], 1.0, s_fea, -1.0)
        style_mse_loss = transfer_net.Session.MSE(style_mse, args.style_weight * 1.0 / (512 * 768), 0.0)
        style_losses.append(style_mse_loss)
        #style_mses.append(style_mse)

    transfer_net.Session.AddOptimizer(StructureLearner.AdamLearner(args.lr, 0.5, 0.999), target_embed)

    #vgg = VGGNet().to(device).eval()
    transfer_net.Session.SetTrainMode()
    transfer_net.Session.Init()

    ## content feature
    content_net.Session.Forward()

    ## style feature
    style_net.Session.Forward()

    ## init target encoding
    target_encode_session.Forward()

    for step in range(0, args.total_step):
        transfer_net.Session.StepV3Run()
        #style_mses[0].Deriv.SyncToCPU()
        #print(style_mses[0].Deriv[0], style_mses[0].Deriv[1], style_mses[0].Deriv[2], style_mses[0].Deriv[3])

        #target_style_features[0].Deriv.SyncToCPU()
        #print(target_style_features[0].Deriv[0], target_style_features[0].Deriv[1], target_style_features[0].Deriv[2], target_style_features[0].Deriv[3])

        #target_features[0].Deriv.SyncToCPU()
        #print(target_features[0].Deriv[0], target_features[0].Deriv[1], target_features[0].Deriv[2], target_features[0].Deriv[3])
        transfer_net.Session.StepV3Update()

        content_loss = sum([obj.Value for obj in con_losses]) # 0
        style_loss = sum([obj.Value for obj in style_losses])

        if (step+1) % args.log_step == 0:
            print ('Step [{}/{}], Content Loss: {:.4f}, Style Loss: {:.4f}'.format(step+1, args.total_step, content_loss, style_loss))

        if (step+1) % args.sample_step == 0:
            target_decode_session.Forward()

            #print(token_l2.Shape[0].Default, token_l2.Shape[1].Default)
            #print(token_score.Shape[0].Default, token_score.Shape[1].Default)

            #token_score.Output.SyncToCPU()
            #print(token_score.Output[0], token_score.Output[1], token_score.Output[2], token_score.Output[3], token_score.Output[4])

            #token_idx.SyncToCPU()
            #print(token_idx[0], token_idx[1], token_idx[2])

            token_idx_list = token_idx.ToCPU()
            #print(token_idx_list[0], token_idx_list[1], token_idx_list[2])

            target_tokens = tokenizer.convert_ids_to_tokens(token_idx_list)
            out_file = open('output-'+str(step+1)+'.txt','w')
            out_file.write(' '.join(target_tokens))
            out_file.close()
            
            # Save the generated image # [123.68, 116.78, 103.94]
            # denorm = transforms.Normalize((-0.485, -0.458, -0.408), (1, 1, 1))
            # img = target.clone()
            # img = denorm(img).clamp_(0, 1)
            # torchvision.utils.save_image(img, 'output-{}.png'.format(step+1))

    transfer_net.Session.Complete()

if __name__ == "__main__":
    main()