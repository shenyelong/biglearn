from __future__ import division
from torchvision import models
from torchvision import transforms
from PIL import Image

import argparse
import torch
import torchvision
import torch.nn as nn
import numpy as np
import math

import sys
import os
import clr as net_clr
from System import Array, IntPtr, Int32, Int64

import tokenization

parser = argparse.ArgumentParser(description='Bert Neural Style Transformer.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')

parser.add_argument('--vocab', type=str, default='vocab file')

parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")
parser.add_argument("--seed_model", type=str, default="seed_model", help="seed model")

args = parser.parse_args()

### sentence embedding.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, IntArgument, CudaPieceFloat, CudaPieceInt, Session, NdArrayData, ComputationGraph

from BigLearn.DeepNet.Language import Bert, DistBert
#from BigLearn.DeepNet.Image import ImageNet_VGG16Net, DistImageNet_VGG16Net

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create bert model and setup enviroument")
env_mode = DeviceBehavior(0).TrainMode

# define bert style net. it appears doesn't work.
class BertEmbedNet:
    def __init__(self, bert_model, max_seq_length, behavior):
        self.Model = bert_model
        self.Behavior = behavior
        self.Session = Session(behavior)
        self.Features = {}

        self.max_seq_length = max_seq_length

        _dim = IntArgument('dim', 768)
        _max_seq_length = IntArgument('max_seq_size', max_seq_length)
        _batch = IntArgument('batch', 1)
        _pad = IntArgument('pad', 1)

        type_ids = [0 for _ in range(max_seq_length)]
        pos_ids = [ i for i in range(max_seq_length)]
        self.word_ids = CudaPieceInt(max_seq_length, self.Behavior.Device)

        word_embed = self.Session.LookupEmbed(self.Model.TokenEmbed, self.word_ids)
        type_embed = self.Session.LookupEmbed(self.Model.TokenTypeEmbed, type_ids)
        pos_embed = self.Session.LookupEmbed(self.Model.PosEmbed, pos_ids)


        wt_embed = self.Session.Add(word_embed, type_embed)
        wtp_embed = self.Session.Add(wt_embed, pos_embed)
        _norm_wtp = self.Session.Norm(wtp_embed, 0) 
        norm_wtp = self.Session.DotAndAdd(_norm_wtp, self.Model.NdNormScale, self.Model.NdNormBias)

        norm_wtp = self.Session.Reshape(norm_wtp, IntArgument("embed", 768), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))
        
        self.mask_scale = CudaPieceFloat(max_seq_length, self.Behavior.Device)
        #  mask_scale = CudaPieceFloat([1.0 / math.sqrt(768 / 12) for _ in range(512)], 
        self.mask_bias = CudaPieceFloat(max_seq_length, self.Behavior.Device)
        #mask_bias = CudaPieceFloat([0.0 for _ in range(512)], self.Behavior.Device)

        #for i in range(0):
        #    norm_wtp = self.Session.Transformer(self.Model.Blocks[i], norm_wtp, 12, self.mask_scale, self.mask_bias, 0.0)
            #self.Features["transformer-"+str(i)] = self.Session.Reshape(norm_wtp, IntArgument("embed", 768), IntArgument("seq", 512))
        
        slice_embed = self.Session.Slice(norm_wtp, [0, 0, 0], _dim, _pad, _batch)  
        self.slice_embed = self.Session.Reshape(slice_embed, _dim, _batch)

    def ExtractSentFeature(self, doc_tokens):
        cls_token = 101

        tokens = [cls_token] + doc_tokens

        token_len = len(tokens)

        if(token_len < self.max_seq_length):
            padding = [0 for _ in range(self.max_seq_length - token_len)]
            tokens.extend(padding)

        token_tensor = torch.tensor(tokens, dtype=torch.int32)
        self.word_ids.CopyTorch(IntPtr.op_Explicit(Int64(token_tensor.data_ptr())),  self.max_seq_length)

        for i in range(self.max_seq_length):
            if i < token_len:
                self.mask_scale[i] = 1.0 / math.sqrt(768 / 12)
                self.mask_bias[i] = 0.0
            else:
                self.mask_scale[i] = 0.0
                self.mask_bias[i] = -1.0e9
        self.mask_scale.SyncFromCPU()
        self.mask_bias.SyncFromCPU()

        self.Session.ForwardV3()

        embed_tensor = torch.zeros([768], dtype=torch.float)

        self.slice_embed.Output.PasteTorch(IntPtr.op_Explicit(Int64(embed_tensor.data_ptr())), 768)

        return embed_tensor

def main():

    doc = '我今天想回家吃饭。'
    tokenizer = tokenization.FullTokenizer(args.vocab) 
    vocab_size = len(tokenizer.vocab)

    doc_text = tokenization.convert_to_unicode(doc)
    doc_tokens = tokenizer.tokenize(doc_text)

    doc_ids = tokenizer.convert_tokens_to_ids(doc_tokens)

    model = Bert.BertBaseModel(vocab_size, 2, args.seed_model + 'exp.label', args.seed_model + 'exp.bin', env_mode.Device)

    net = BertEmbedNet(model, 512, env_mode)

    net.Session.SetPredictMode()
    net.Session.Init()

    embed = net.ExtractSentFeature(doc_ids)
    print(embed)
    net.Session.Complete()

if __name__ == "__main__":
    main()