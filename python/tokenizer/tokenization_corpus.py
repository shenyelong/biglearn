# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tempfile
import tokenization
import argparse
import random
parser = argparse.ArgumentParser(description='Text Corpus Tokenization.')

# Datasets
parser.add_argument('-v', '--vocab', default='vocab.txt', type=str)
parser.add_argument('-i', '--input', default='input.txt', type=str)
parser.add_argument('-o', '--output', default='output.txt', type=str)

args = parser.parse_args()

def create_feature_str(values):
  return " ".join([str(x) for x in values])

class TokenizationCorpus(object):
  
  def tokenize(self, vocab_file, input_file, output_file):
    tokenizer = tokenization.FullTokenizer(vocab_file) 
    all_documents = [[]]
    with open(input_file, "r") as reader:
      line_idx = 0
      while True:
        line = tokenization.convert_to_unicode(reader.readline())
        if not line:
          break
        line = line.strip()

        # Empty lines are used as document delimiters
        if not line:
          all_documents.append([])
        tokens = tokenizer.tokenize(line)
        if tokens:
          all_documents[-1].append(tokens)
        line_idx += 1
        if(line_idx % 100000 == 0):
          print("extract line ", line_idx)
          print("document num ", len(all_documents)) 
          #if(len(all_documents) >= 5):
          #break
    all_documents = [x for x in all_documents if x]
    
    rng = random.Random(103)

    rng.shuffle(all_documents)
    writer = open(output_file,"w")
    for doc in all_documents:
      for tokens in doc:
        token_ids = tokenizer.convert_tokens_to_ids(tokens)
        writer.write(create_feature_str(token_ids)+'\n')
      ## document split.
      writer.write('\n')
    writer.close()

if __name__ == "__main__":
  tokenizer = TokenizationCorpus()
  tokenizer.tokenize(args.vocab, args.input, args.output)
  #tokenizer.test_full_tokenizer()
  #tokenizer.test_wordpiece_tokenizer()
