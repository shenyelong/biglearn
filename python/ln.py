import torch
import torch.nn as nn
import torch.nn.functional as F
import math
from torch.autograd import Variable
def look(t, num):
    tflat = t.view(-1)
    for i in range(0, num):
        print(tflat[i])

class LayerNorm(nn.Module):
    def __init__(self, eps=1e-6):
        super().__init__()
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True) * math.sqrt(767) / math.sqrt(768)
        return (x - mean) / (std + self.eps)

ln = LayerNorm()
i = torch.randn(1, 16, 256, 768)

iflat = i.view(-1)
for idx in range(0, 16 * 256 * 768):
    iflat[idx] = 0.1 * (idx + 1)

iv = Variable(i, requires_grad=True)
o = ln(iv)


look(i, 100)
look(o, 100)

g = torch.randn(1, 16, 256, 768)
gflat = g.view(-1)
for idx in range(0, 16 * 256 * 768):
    gflat[idx] = 2.3 * idx * math.sqrt(idx) + math.tanh(idx / 10.0 * 0.2) * 0.1 * (idx * 0.08 + math.sqrt(idx) * 7.2 + 1)  # 0.1 * (idx + 1)
o.backward(g)
look(iv.grad,100)
