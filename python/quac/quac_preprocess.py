"""
preprocess CoQA for extractive question answering
2019/1/22: distinguish yes-no and wh- question
"""

import argparse
import json
import re
import time
import string
from collections import Counter
from collections import OrderedDict
from pycorenlp import StanfordCoreNLP

import tokenization


nlp = StanfordCoreNLP('http://localhost:9000')
UNK = 'unknown'

def _str(s):
    """ Convert PTB tokens to normal tokens """
    if (s.lower() == '-lrb-'):
        s = '('
    elif (s.lower() == '-rrb-'):
        s = ')'
    elif (s.lower() == '-lsb-'):
        s = '['
    elif (s.lower() == '-rsb-'):
        s = ']'
    elif (s.lower() == '-lcb-'):
        s = '{'
    elif (s.lower() == '-rcb-'):
        s = '}'
    return s


def process(text):
    paragraph = nlp.annotate(text, properties={
                             'annotators': 'tokenize, ssplit',
                             'outputFormat': 'json',
                             'ssplit.newlineIsSentenceBreak': 'two'})

    output = {'word': [], 'offsets': []}

    try:
        for sent in paragraph['sentences']:
            for token in sent['tokens']:
                if len(token['word']) == 0 :
                    print('word tokenized length is zero')
                    continue

                output['word'].append(_str(token['word']))
                output['offsets'].append((token['characterOffsetBegin'], token['characterOffsetEnd']))
    except:
        print("error in line 45: {}".format(paragraph))
    return output


def normalize_answer(s):
    """Lower text and remove punctuation, storys and extra whitespace."""

    def remove_articles(text):
        regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
        return re.sub(regex, ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))


def find_span_with_gt(context, offsets, ground_truth, w_start = -1, w_end = -1):
    """
    find answer from paragraph so that best f1 score is achieved
    """
    best_f1 = 0.0
    best_span = (len(context)-1, len(context)-1)
    best_pred = ''
    gt = normalize_answer(ground_truth).split()

    if(w_start == -1):
        w_start = 0
    if(w_end == -1):
        w_end = len(offsets)

    ls = [i for i in range(w_start, w_end) # len(offsets))
          if context[offsets[i][0]:offsets[i][1]].lower() in gt]

    for i in range(len(ls)):
        for j in range(i, len(ls)):
            pred = normalize_answer(context[offsets[ls[i]][0]: offsets[ls[j]][1]]).split()
            common = Counter(pred) & Counter(gt)
            num_same = sum(common.values())
            if num_same > 0:
                precision = 1.0 * num_same / len(pred)
                recall = 1.0 * num_same / len(gt)
                f1 = (2 * precision * recall) / (precision + recall)
                if f1 > best_f1:
                    best_f1 = f1
                    best_span = (offsets[ls[i]][0], offsets[ls[j]][1])
                    best_pred = context[offsets[ls[i]][0]: offsets[ls[j]][1]]
    return best_span, best_f1, best_pred

def f1Score(gt, pred):
    norm_gt = normalize_answer(gt).split()
    norm_pred = normalize_answer(pred).split()

    common = Counter(norm_pred) & Counter(norm_gt)
    num_same = sum(common.values())
    if num_same > 0:
        precision = 1.0 * num_same / len(norm_pred)
        recall = 1.0 * num_same / len(norm_gt)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1
    return 0

def get_char2word(context_str, words, offsets):
    # char to word offset.
    char_to_word_offset = []
    
    widx = 0
    for c_story in range(0, len(context_str)):
        if (widx + 1 >= len(words)):
            char_to_word_offset.append(widx)
            continue

        if (c_story < offsets[widx][0]):
            char_to_word_offset.append(widx)
            continue

        if (c_story >= offsets[widx][0] and c_story < offsets[widx][1]):
            char_to_word_offset.append(widx)
            continue

        if (c_story >= offsets[widx][1]):
            if(len(words) > widx + 1):
                char_to_word_offset.append(widx + 1)
            else:
                char_to_word_offset.append(widx)
            widx = widx + 1
            continue
    assert len(char_to_word_offset) == len(context_str)

    return char_to_word_offset


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    parser.add_argument("--vocab_file", default=None, type=str, help="tokenizer vocab file.")

    args = parser.parse_args()

    tokenizer = tokenization.FullTokenizer(args.vocab_file) 

    #with open(args.data_file, 'r') as f:
    #    dataset = json.load(f)

    #data = OrderedDict()
    #data['version'] = dataset['version']
    #_datum_list = []

    __datum_list = json.load(open(args.data_file, 'r'))['data']

    data = OrderedDict()
    _datum_list = []

    yes_cnt = 0
    no_cnt = 0
    unk_cnt = 0
    true_cnt = 0
    false_cnt = 0
    fuz_num = 0

    _f1 = 0
    _num = 0
    _sf1 = 0
    _pnum = 0
    _debug_f = open('detect.span', 'w')
    _wf1 = 0
    _wnum = 0
    
    bad_quality_num = 0
    for i, datum in enumerate(__datum_list):
        if (i % 10 == 0):
            print("processig {} examples".format(i))
        
        #print(datum.keys())
        #sys.exit(0)

        if not 'paragraphs' in datum.keys():
            print(datum.keys())
            print('key paragraphs not in datum...', datum)
            continue

        for paragraph in datum['paragraphs']:
            context_str = paragraph['context']
            annotated_context_str = process(context_str)
            offsets = annotated_context_str['offsets']
            words = annotated_context_str['word']
            
            # subtokens, word2subtokens.
            subtokens, word2subtoken = tokenizer.tokenize_words(words)
            
            # subtoken idxes.
            subtokens_idx = tokenizer.convert_tokens_to_ids(subtokens)

            # char to word offset.
            char_to_word_offset = get_char2word(context_str, words, offsets)

            _datum = OrderedDict()
            _datum["story"] = context_str
            _datum["story_subtoken_idx"] = subtokens_idx
            _datum["story_word2subtoken"] = word2subtoken
            _datum["story_wordoffset"] = offsets

            _questions = []
            _answers = []

            turn_idx_cursor = 0
            # qas. 
            for qas in paragraph['qas']:
                _id = qas['id']
                
                yes_no_tag = qas['yesno']
                question_text = qas['question']
                ans = qas['orig_answer']

                ans_text = ans['text']
                ans_pos = ans['answer_start']

                question_tokens = tokenizer.tokenize(question_text)
                question_idx = tokenizer.convert_tokens_to_ids(question_tokens)

                turn_id = int(_id.split('_q#')[1]) + 1
                turn_idx_cursor = turn_idx_cursor + 1

                if turn_id != turn_idx_cursor:
                    print(turn_id, turn_idx_cursor)
                    turn_idx_cursor = turn_id
                    
                #assert turn_id == turn_idx_cursor

                cur_question = OrderedDict()
                cur_question['input_text'] = question_text
                cur_question['turn_id'] = turn_id
                cur_question['input_text_subtoken_idx'] = question_idx
                cur_question['q_id'] = _id

                followups = qas['followup']
                cur_question['followup'] = followups

                _questions.append(cur_question)
                
                cur_answer = OrderedDict() # attr: span_start, span_end, text, turn_id
                
                cate_flag = 0
                if yes_no_tag == 'y':
                    cate_flag = 1
                    yes_cnt += 1
                elif yes_no_tag == 'n':
                    cate_flag = 2
                    no_cnt += 1
                elif ans_text == 'CANNOTANSWER':
                    cate_flag = 3
                    unk_cnt += 1

                ans_pos_end = ans_pos + len(ans_text)

                chosen_text = context_str[ans_pos : ans_pos_end]
                # remove space from start
                
                ans_span = (ans_pos, ans_pos_end)
                quality = 1

                if cate_flag == 3:
                    ans_span = (-1, -1)
                elif not chosen_text == ans_text:
                    c_w_start = char_to_word_offset[ans_pos]
                    c_w_end = char_to_word_offset[ans_pos_end - 1]

                    fuz_num += 1

                    ans_span, f1, pred = find_span_with_gt(context_str, offsets, ans_text, c_w_start, c_w_end + 1)

                    _f1 += f1

                    sf1 = f1Score(ans_text, chosen_text)  
                    _sf1 += sf1

                    if(sf1 > f1 or f1 <= 0.6):
                        ans_span = (ans_pos, ans_pos_end)
                        _debug_f.write(question_text + '\t' + ans_text + '\t' + pred + '\t' + str(f1) + '\t' + chosen_text + '\t' + str(sf1) + '\n')
                        _pnum += 1 
                        _f1 += f1 
                        _sf1 += sf1
                        if(sf1 <= 0.6):
                            quality = 0 

                _num += 1

                ## char span.
                cur_answer['char_span_start'] = ans_span[0]
                cur_answer['char_span_end'] = ans_span[1]
                
                ## check the corresponding sub-token based on character.
                if(ans_span[0] == -1 or ans_span[1] == -1):
                    w_start = -1
                    w_end = -1
                else:
                    w_start = char_to_word_offset[ans_span[0]]
                    w_end = char_to_word_offset[ans_span[1]-1]

                    wf1 = f1Score(ans_text, context_str[offsets[w_start][0] : offsets[w_end][1]]) # cur_answer['word_span_text'])
                    _wf1 += wf1
                    _wnum += 1

                cur_answer['word_span_start'] = w_start
                cur_answer['word_span_end'] = w_end

                if ans_span[0] == -1 or ans_span[1] == -1:
                    cur_answer['char_span_text'] = 'unk'
                    cur_answer['word_span_text'] = 'unk' #words[w_start:w_end+1]
                else:
                    cur_answer['char_span_text'] = context_str[ans_span[0] : ans_span[1]]
                    cur_answer['word_span_text'] = context_str[offsets[w_start][0] : offsets[w_end][1]] #words[w_start:w_end+1]

                cur_answer['cate_flag'] = cate_flag
                cur_answer['turn_id'] = turn_id

                cur_answer['orig_answer'] = ans_text
                cur_answer['quality'] = quality
                _answers.append(cur_answer)

                if(quality == 0):
                    bad_quality_num += 1
            
            _datum["questions"] = _questions[:]
            _datum["answers"] = _answers[:]

        _datum_list.append(_datum)
    data['data'] = _datum_list
    print("# of questions : {}, # of answer yes: {}, # of answer no: {}, # of unk : {}, # of fuz span : {}, avg f1 : {}, avg sf1 : {}, avg wf1 : {} : {}, # of bad quality : {}".format(_num, yes_cnt, no_cnt, unk_cnt, fuz_num, _f1 * 1.0 / (fuz_num + 1.0), _sf1 * 1.0 / (fuz_num + 1.0), _wf1 * 1.0 / (_wnum + 1.0), _wnum, bad_quality_num))
    _debug_f.close()

    with open(args.output_file, 'w') as output_file:
        json.dump(data, output_file, sort_keys=True, indent=4)


