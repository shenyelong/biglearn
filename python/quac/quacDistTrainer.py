
## build an framework for understanding the discrete variables in the deep neural networks.

import argparse

from QuAC_Dataset_v1 import QuAC_Dataset_v1
from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import torch
import random
import math

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')


parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--train_dataset', required=True, type=str, help='train dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-s", "--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("-q", "--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")   
parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--epochs', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,2000:0.00001,50000:0.000001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')
parser.add_argument('--decay', default=0.01, type=float, metavar='DECAY', help='decay of model parameters.')
parser.add_argument('--opt', default='adambert', type=str, metavar='OPT', help='optimization algorithm.')
parser.add_argument('--ema', default='1.0', type=float, metavar='EMA', help='exp moving average.')

parser.add_argument('--is_seed', default=0, type=int, metavar='S', help='use seed model or random initialized.')

parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--output_model', default='bert_CoQA', type=str, help='output model.')

parser.add_argument('--dropout', default='0', type=float, help='droput rate.')

parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')

parser.add_argument('--skip_bad', default='1', type=int, help='skip bad quality data training. 0:noskip; 1:skip;')

parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')

parser.add_argument('--avg_cate', default='0', type=int, help='average category loss.')
parser.add_argument('--avg_span', default='0', type=int, help='average span loss.')

parser.add_argument('--cate_w', default='0.1', type=float, help='weight of category loss.')
parser.add_argument('--span_w', default='0.1', type=float, help='weight of span loss.')

parser.add_argument('--q_order', default=0, type=int, help='question order')

#parser.add_argument('--turn_ids', default='1', type=str, help='train turns')

parser.add_argument('--slide', default=128, type=int, help='question order')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

## recurrent chunk mechism for qa model.

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager, Session, ComputationGraph
from BigLearn import BaseBertModel, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure, TransformerStructure
from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData
from BigLearn import GradientOptimizer, ParameterSetting, NCCL
from BigLearn import CrossEntropyRunner

device_num = len(args.gpu_id.split(','))
print("set gpu number ", device_num)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

#turn_ids = [ int(x) for x in args.turn_ids.split(',') ] 

print("Loading train Dataset . . .", args.train_dataset)

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

train_data = QuAC_Dataset_v1(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, max_query_history = 100,
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1), skip_bad = args.skip_bad, qorder = 0, slide_win = args.slide)

train_loader = DataLoader(train_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

def SetupDocMask(mask_doc_tensor, mask_doc_piece, batch_size, max_seq_length):
    acc_nums = [0] * (batch_size + 1)
    acc_nums[0] = 0
    batch_inc = 0
    for b in range(batch_size):
        word_inc = 0
        for s in range(max_seq_length):
            if(mask_doc_tensor[b][s].item() > 0.5):
                mask_doc_piece[batch_inc + word_inc] = b * max_seq_length + s
                word_inc = word_inc + 1
        batch_inc = batch_inc + word_inc
        acc_nums[b + 1] = batch_inc

    mask_doc_piece.EffectiveSize = batch_inc
    mask_doc_piece.SyncFromCPU()
    return acc_nums 

def SetupLabel(cate_label, start_label, end_label, cate_piece, start_piece, end_piece, acc_nums, batch_size):
    start_piece.EffectiveSize = acc_nums[batch_size]  
    end_piece.EffectiveSize = acc_nums[batch_size]  

    cate_piece.EffectiveSize = 4 * batch_size

    start_piece.Zero()
    end_piece.Zero()
    cate_piece.Zero()

    start_only = 0
    end_only = 0
    unk_only = 0

    # ["yes", "no", "unknown", "true", "false"]

    for b in range(batch_size):
        start_idx = start_label[b]
        end_idx = end_label[b]

        batch_inc = acc_nums[b]
        word_num = acc_nums[b + 1] - acc_nums[b]

        is_start = False
        if start_idx >= 0 and start_idx < word_num:
            start_piece[batch_inc + start_idx] = 1.0
            is_start = True

        is_end = False
        if end_idx >=0 and end_idx < word_num:
            end_piece[batch_inc + end_idx] = 1.0
            is_end = True

        if is_start and is_end:
            conv = 1 
        elif not is_start and not is_end:
            conv = 0
            unk_only += 1
        elif not is_start and is_end:
            conv = end_idx / (end_idx - start_idx);
            end_only += 1;
        elif is_start and not is_end:
            conv = (word_num - start_idx) / (end_idx - start_idx);
            start_only += 1

        cate_piece[b * 4 + cate_label[b]] = conv
        cate_piece[b * 4 + 3] = 1 - conv

    start_piece.SyncFromCPU()
    end_piece.SyncFromCPU()
    cate_piece.SyncFromCPU()

    start_only_ratio = start_only * 1.0 / batch_size
    end_only_ratio = end_only * 1.0 / batch_size
    unk_ratio = unk_only * 1.0 / batch_size

    return start_only_ratio, end_only_ratio, unk_ratio

class MrcModule:
    # dual module for Generator and Discriminator 
    def __init__(self, vocab_size, layer, seed_mrc, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        # int frozenlayer, int mvocabSize, int mcate, int mlayer, int mdim, string meta_file, string bin_file, DeviceType device) 
        self.Models['mrc_model'] = self.Session.Model.AddLayer(BaseBertModel(-1, vocab_size, 2, 24, 1024, seed_mrc + 'exp.label', seed_mrc + 'exp.bin', behavior.Device))
        self.Models['cate_classifier'] =  self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 4, A_Func.Linear, True, behavior.Device))
        self.Models['span_start_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 1, A_Func.Linear, False, behavior.Device))
        self.Models['span_end_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['mrc_model'].embed, 1, A_Func.Linear, False, behavior.Device))

        #for i in range(0, layer):
        #    self.Models['layer_' + str(i)] = self.Session.Model.AddLayer(TransformerStructure(1024, behavior.Device))

        self.grad = self.Session.Model.AllocateGradient(self.Behavior.Device)

        # StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema)
        self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay, args.ema))
        #StructureLearner.AdamLearner(args.lr, 0.9, 0.999, 1.0, 0.0004)) # (args.lr, 0.5, 0.999))

class MrcNet:
    ## construct model 
    def __init__(self, module, batch_size, max_seq_length, max_query_length, dropout, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Module = module
        self.Models = module.Models

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length

        max_doc_length = max_seq_length - max_query_length

        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.segments = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        self.mask_doc = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        
        self.start_label = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)
        self.end_label = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)
        self.cate_label = CudaPieceFloat(batch_size * 6, self.Behavior.Device)

        emd = 1024
        att_head = 16

        w_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenEmbed, self.tokens)
        w_embed = self.Session.Reshape(w_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", batch_size))

        pos_ids = [i for i in range(max_seq_length)]
        pos_embed = self.Session.LookupEmbed(self.Models['mrc_model'].PosEmbed, pos_ids)
        pos_embed = self.Session.Reshape(pos_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))

        #seg_ids = [0 for i in range(max_doc_length)] + [1 for i in range(max_query_length)]
        seg_embed = self.Session.LookupEmbed(self.Models['mrc_model'].TokenTypeEmbed, self.segments)
        seg_embed = self.Session.Reshape(seg_embed, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", 1))

        embed = self.Session.Add(w_embed, pos_embed)
        embed = self.Session.Add(embed, seg_embed)

        _norm_emd = self.Session.Norm(embed, 0) 
        norm_emd = self.Session.DotAndAdd(_norm_emd, self.Models['mrc_model'].NdNormScale, self.Models['mrc_model'].NdNormBias)
        norm_emd = self.Session.Reshape(norm_emd, IntArgument("embed", emd), IntArgument("seq", max_seq_length), IntArgument("batch_size", batch_size))

        self.scaleVec = CudaPieceFloat(2, self.Behavior.Device)
        self.scaleVec[0] = 0
        self.scaleVec[1] = 1.0 / math.sqrt(emd / att_head)
        self.scaleVec.SyncFromCPU()

        self.biasVec = CudaPieceFloat(2, self.Behavior.Device)
        self.biasVec[0] = -1e9
        self.biasVec[1] = 0
        self.biasVec.SyncFromCPU()

        _mask_vocab = IntArgument('mask_vocab', 2)

        #new IntArgument[] { emd_dim, firstSlice, batch_size },

        _dim = IntArgument('dim', self.Models['mrc_model'].embed)
        _batch = IntArgument('batch', batch_size)
        _seq = IntArgument('seq', max_seq_length)
        _batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)
        _default = IntArgument('default', 1)

        #DeviceType device, CudaPieceFloat data, CudaPieceFloat deriv, params IntArgument[] dimensions) 
        self.mask_scale = self.Session.LookupEmbed(NdArrayData(self.Behavior.Device, self.scaleVec, None, _default, _mask_vocab), self.masks)
        #CudaPieceFloat(self.max_seq_length * self.batch_size, self.Behavior.Device)
        
        self.mask_bias = self.Session.LookupEmbed(NdArrayData(self.Behavior.Device, self.biasVec, None, _default, _mask_vocab), self.masks)
        #CudaPieceFloat(self.max_seq_length * self.batch_size, self.Behavior.Device)

        # TransformerStructure block, NdArrayData input, int head_num, CudaPieceFloat maskScale, CudaPieceFloat maskBias, float dropout)
        for i in range(24):
            norm_emd = self.Session.Transformer(self.Models['mrc_model'].Blocks[i], norm_emd, 16, self.mask_scale.Output, self.mask_bias.Output, 0.1)

        # NdArrayData data, int[] offset, params IntArgument[] shape)
        slice_embed = self.Session.Slice(norm_emd, [0, 0, 0], _dim, _default, _batch)
        #bert_feature = self.Session.BaseBertFeaturizer(self.Models['mrc_model'], self.tokens, self.segments, self.masks, batch_size, max_seq_length)
        slice_embed = self.Session.Reshape(slice_embed, _dim, _batch)
        
        self.embed_feature = self.Session.Dropout(norm_emd, dropout)
        self.slice_feature = self.Session.Dropout(slice_embed, dropout)

        self.cate_flag = self.Session.FNN(self.slice_feature, self.Models['cate_classifier'])
        self.cate_loss = self.Session.CrossEntropyLoss(self.cate_flag, self.cate_label, args.cate_w, False)

        self.embed_2d_feature = self.Session.Reshape(self.embed_feature, _dim, _batch_seq)
        self.doc_embed = self.Session.LookupEmbed(self.embed_2d_feature, self.mask_doc)

        self.start_flag = self.Session.FNN(self.doc_embed, self.Models['span_start_classifier'])
        self.end_flag = self.Session.FNN(self.doc_embed, self.Models['span_end_classifier'])

        self.start_loss = self.Session.CrossEntropyLoss(self.start_flag, self.start_label, args.span_w, False)
        self.end_loss = self.Session.CrossEntropyLoss(self.end_flag, self.end_label, args.span_w, False)

    def SetupTrain(self, over_tokens, over_mask, over_segment, doc_word_masks, cate_label, start_label, end_label):
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(over_tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.segments.CopyTorch(IntPtr.op_Explicit(Int64(over_segment.data_ptr())), self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(over_mask.data_ptr())), self.batch_size * self.max_seq_length)

        acc_nums = SetupDocMask(doc_word_masks, self.mask_doc, self.batch_size, self.max_seq_length)
        self.start_only_ratio, self.end_only_ratio, self.unk_ratio = SetupLabel(cate_label, start_label, end_label, self.cate_label, self.start_label, self.end_label, acc_nums, self.batch_size) 

class DistMrcNet:
        
    def __init__(self, seed_bert, cate, vocab_size, batch_size, max_query_length, max_seq_length, dropout, device_num):
        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).TrainMode
            ParameterSetting.ResetRandom()
            mrcModule = MrcModule(vocab_size, 1, seed_bert, env_mode)
            return MrcNet(mrcModule, self.sub_batch_size, max_seq_length, max_query_length, dropout, env_mode)
            # vocab_size, layer, seed_mrc, behavior):
            #return MrcModule(vocab_size, 3, seed_bert, cate, self.sub_batch_size, max_seq_length, dropout, env_mode)
        self.device_num = device_num

        self.batch_size = batch_size
        self.sub_batch_size = (int)(batch_size / device_num)

        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length

        #self.pool.map(__construct_model, range(device_num))

        self.models = [] #* device_num
        for idx in range(device_num):
            self.models.append(__construct_model(idx))

        self.grad_reducer = NCCL(device_num)

        self.sessions =  [m.Session for m in self.models]
        self.grads = [m.Module.grad for m in self.models]
        self.gradSessions = [m.Module.Session for m in self.models]

    def Train(self, over_tokens, over_mask, over_segment, doc_word_masks, cate_label, start_label, end_label):
        def __setup(device_id):
            self.models[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            #l_start = device_id * self.sub_batch_size
            #l_end = (device_id + 1) * self.sub_batch_size

            self.models[device_id].SetupTrain(over_tokens[b_start : b_end], over_mask[b_start : b_end], over_segment[b_start : b_end], doc_word_masks[b_start : b_end], cate_label[b_start : b_end], start_label[b_start : b_end], end_label[b_start : b_end])
        
        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        self.grad_reducer.AllRun(self.sessions, self.device_num)

        self.grad_reducer.AllReduceUpdate(self.gradSessions, self.grads, self.device_num)
        # Execute(self.sessions, self.grads, self.device_num)

        loss_es = [ (m.cate_loss.Value, m.start_loss.Value, m.end_loss.Value) for m in self.models ]
        ratio_es = [ (m.start_only_ratio, m.end_only_ratio, m.unk_ratio) for m in self.models ]

        loss_avg = [ x / self.device_num for x in [sum(loss) for loss in zip(*loss_es)]]
        ratio_avg = [x /self.device_num for x in [sum(ratio) for ratio in zip(*ratio_es)]]
        return loss_avg, ratio_avg

print("Create bert model and setup enviroument")

# seed_bert, cate, vocab_size, batch_size, max_query_length, max_seq_length, dropout, device_num):
mrcTrainer = DistMrcNet(args.seed_bert, 4, args.vocabsize, args.batch_size, args.max_query_length, args.max_seq_length, args.dropout, device_num)

for epoch in range(0, args.epochs):
    print('\nEpoch: [%d | %d] LR: %f' % (epoch + 1, args.epochs, args.lr))
    
    ## set training mode.
    #net.SetTrainMode()
    #net.Init()
    
    cate_loss = AverageMeter()
    start_loss = AverageMeter()
    end_loss = AverageMeter()

    unk_loss = AverageMeter()
    lefto_loss = AverageMeter()
    righto_loss = AverageMeter()
    
    #total_batch = train_data.batch_num()
    bar = Bar('training', max=len(train_loader))
    #bar = Bar('training', max = total_batch)

    #for batch_idx in range(0, total_batch):
    for batch_idx, data in enumerate(train_loader):
        data = {key: value for key, value in data.items()}

        # (doc_tokens, query_tokens, over_mask, over_segment, doc_word_masks, cate_label, start_label, end_label):
        #doc_tokens = data['doc_tokens']
        #query_tokens = data['query_tokens']

        over_tokens = data['token_input']
        over_masks = data['mask_input']
        over_segs = data['seg_input']
        
        doc_word_mask = data['doc_mask']

        start_label = data['start_position']
        end_label = data['end_position']

        cate_label = data['cate_flag']

        cse_loss, seu_ratio = mrcTrainer.Train(over_tokens, over_masks, over_segs, doc_word_mask, cate_label, start_label, end_label)
        
        #c_loss, s_loss, e_loss, unk, lefto, righto = net.Run(p_tokens, p_segs, p_msks, p_doc_mask, p_unk_flag, p_start_pos, p_end_pos, 1, args.batch_size) 
        
        cate_loss.update(cse_loss[0])
        start_loss.update(cse_loss[1])
        end_loss.update(cse_loss[2])

        lefto_loss.update(seu_ratio[0])
        righto_loss.update(seu_ratio[1])
        unk_loss.update(seu_ratio[2])

        #{batch_time.val:.3f} ({batch_time.avg:.3f})
        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | cate_loss: {cate_loss.val:.4f} ({cate_loss.avg:.4f}) | start_loss: {start_loss.val:.4f} ({start_loss.avg:.4f}) | end_loss : {end_loss.val:.4f} ({end_loss.avg:.4f}) | unk_avg: {avg_unk:.4f} | lefto_avg: {avg_lefto:.4f} | righto_avg : {avg_righto:.4f} '.format(
                    batch=batch_idx + 1,
                    size=len(train_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,

                    cate_loss=cate_loss,
                    start_loss=start_loss,
                    end_loss=end_loss,
                    
                    avg_unk=unk_loss.avg,
                    avg_lefto=lefto_loss.avg,
                    avg_righto=righto_loss.avg                    
                    )
        bar.next()
        #break
    #break
    bar.finish()
    mrcTrainer.models[0].Module.Session.Model.Save(args.output_model + '.'+str(epoch)+'.model')

           
        #print(data['para_id'])

        
        