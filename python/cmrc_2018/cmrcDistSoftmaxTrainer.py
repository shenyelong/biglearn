import argparse

from cmrc_Dataset import cmrc_Dataset
from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import torch

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--train_dataset', required=True, type=str, help='train dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-s", "--max_seq_length", type=int, default=512, help="maximum sequence length")
parser.add_argument("-q", "--max_query_length", type=int, default=64, help="maximum query length")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")
parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--epochs', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,2000:0.00001,50000:0.000001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')
parser.add_argument('--decay', default=0.01, type=float, metavar='DECAY', help='decay of model parameters.')
parser.add_argument('--opt', default='adambert', type=str, metavar='OPT', help='optimization algorithm.')
parser.add_argument('--ema', default='1.0', type=float, metavar='EMA', help='exp moving average.')

parser.add_argument('--is_seed', default=0, type=int, metavar='S', help='use seed model or random initialized.')

parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--output_model', default='bert_CoQA', type=str, help='output model.')

parser.add_argument('--dropout', default='0', type=float, help='droput rate.')

parser.add_argument('--pool', default='0', type=int, help='0:no pretrained pooling layer; 1:pretrained pooling layer')

parser.add_argument('--enumate_span', default='1', type=int, help='0: sample per doc; 1: sample per doc span;')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager, Session, ComputationGraph

from BigLearn import BaseBertModel, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure

from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData

from BigLearn import GradientOptimizer, ParameterSetting, NCCL

from BigLearn import CrossEntropyRunner


device_num = len(args.gpu_id.split(','))
print("set gpu number ", device_num)

os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)


print("Loading train Dataset . . .", args.train_dataset)
# corpus_path, max_seq_length, max_query_length, max_query_history, cls_token, sep_token, pad_token, vocabsize, rseed = 110):
train_data = cmrc_Dataset(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_query_length=args.max_query_length, 
                         cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token, vocabsize=args.vocabsize, fill_batch=args.batch_size, is_enumate_doc_span=(args.enumate_span == 1))

train_loader = DataLoader(train_data, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

#sys.exit()

print("Create bert model and setup enviroument")

class BertQA:
    ## construct model 
    def __init__(self, seed_bert, cate, batch_size, max_seq_length, dropout, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length

        self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(21128, 2, seed_bert + 'exp.label', seed_bert + 'exp.bin', behavior.Device))

        #self.Models['unk_classifier'] =  self.Session.Model.AddLayer(LayerStructure(self.Models['bert_base'].embed, 2, A_Func.Linear, True, behavior.Device))
        self.Models['span_start_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['bert_base'].embed, 1, A_Func.Linear, False, behavior.Device))
        self.Models['span_end_classifier'] = self.Session.Model.AddLayer(LayerStructure(self.Models['bert_base'].embed, 1, A_Func.Linear, False, behavior.Device))

        #self.Session.Model.AddLayer(
        
        self.grad = self.Session.Model.AllocateGradient(self.Behavior.Device)

        self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.decay))

        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.segments = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        self.extend_mask_doc = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        
        self.start_label = CudaPieceInt(batch_size, self.Behavior.Device)
        self.end_label = CudaPieceInt(batch_size, self.Behavior.Device)

        #self.cate_label = CudaPieceFloat(batch_size * 2, self.Behavior.Device)

        bert_feature = self.Session.BaseBertFeaturizer(self.Models['bert_base'], self.tokens, self.segments, self.masks, batch_size, max_seq_length)

        dim = IntArgument('dim', self.Models['bert_base'].embed)
        batch = IntArgument('batch', batch_size)
        seq = IntArgument('seq', max_seq_length)
        batch_seq = IntArgument('batch_seq', batch_size * max_seq_length)

        self.embed_feature = self.Session.Dropout(bert_feature.Item1, dropout)

        self.embed_2d_feature = self.Session.Reshape(self.embed_feature, dim, batch_seq)

        start_flag = self.Session.FNN(self.embed_2d_feature, self.Models['span_start_classifier'])
        end_flag = self.Session.FNN(self.embed_2d_feature, self.Models['span_end_classifier'])

        start_flag = self.Session.Reshape(start_flag, seq, batch)
        end_flag = self.Session.Reshape(end_flag, seq, batch)

        scale_bias_mask = self.Session.Mask(self.extend_mask_doc)  #Tuple<NdArrayData, NdArrayData> Mask(CudaPieceInt mask)

        scale_mask = self.Session.Reshape(scale_bias_mask.Item1, seq, batch)
        bias_mask = self.Session.Reshape(scale_bias_mask.Item2, seq, batch)

        self.mask_start_flag = self.Session.DotAndAdd(start_flag, scale_mask, bias_mask)
        self.mask_end_flag = self.Session.DotAndAdd(end_flag, scale_mask, bias_mask)

        self.start_loss = self.Session.SoftmaxLoss(self.mask_start_flag, self.start_label)
        self.end_loss = self.Session.SoftmaxLoss(self.mask_end_flag, self.end_label)

    def SetupTrain(self, tokens, segments, masks, mask_doc, start_label, end_label):
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.segments.CopyTorch(IntPtr.op_Explicit(Int64(segments.data_ptr())), self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(masks.data_ptr())), self.batch_size * self.max_seq_length)
        
        self.extend_mask_doc.CopyTorch(IntPtr.op_Explicit(Int64(mask_doc.data_ptr())), self.batch_size * self.max_seq_length)

        self.start_label.CopyTorch(IntPtr.op_Explicit(Int64(start_label.data_ptr())), self.batch_size)
        self.end_label.CopyTorch(IntPtr.op_Explicit(Int64(end_label.data_ptr())), self.batch_size)


class DistBertQA:
    def __init__(self, seed_bert, cate, batch_size, max_seq_length, dropout, device_num):

        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).TrainMode
            ParameterSetting.ResetRandom()
            return BertQA(seed_bert, cate, self.sub_batch_size, max_seq_length, dropout, env_mode)

        self.device_num = device_num

        self.batch_size = batch_size
        self.sub_batch_size = (int)(batch_size / device_num)

        self.max_seq_length = max_seq_length
        #self.pool.map(__construct_model, range(device_num))

        self.models = [] #* device_num
        for idx in range(device_num):
            self.models.append(__construct_model(idx))

        self.grad_reducer = NCCL(device_num)

        self.sessions =  [m.Session for m in self.models]
        self.grads = [m.grad for m in self.models]

    def Train(self, tokens, segments, masks, mask_doc, start_label, end_label):
        def __setup(device_id):
            self.models[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            self.models[device_id].SetupTrain(tokens[b_start : b_end], segments[b_start : b_end], masks[b_start : b_end], mask_doc[b_start : b_end], start_label[b_start : b_end], end_label[b_start : b_end])
        
        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        self.grad_reducer.Execute(self.sessions, self.grads, self.device_num)

        loss_es = [ ( m.start_loss.Value, m.end_loss.Value) for m in self.models ]

        loss_avg = [ x / self.device_num for x in [sum(loss) for loss in zip(*loss_es)]]

        return loss_avg

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

qaTrainer = DistBertQA(args.seed_bert, 2, args.batch_size, args.max_seq_length, args.dropout, device_num)

for epoch in range(0, args.epochs):
    print('\nEpoch: [%d | %d] LR: %f' % (epoch + 1, args.epochs, args.lr))
    
    start_loss = AverageMeter()
    end_loss = AverageMeter()

    bar = Bar('training', max=len(train_loader))
    
    for batch_idx, data in enumerate(train_loader):
        # 0. batch_data will be sent into the device(GPU or cpu)

        data = {key: value for key, value in data.items()}
        tokens = data['token_input']
        segs = data['seg_input']
        masks = data['mask_input']

        doc_mask = data['doc_mask']
        extend_doc_mask = data['extend_doc_mask']

        start_label = data['token_start']
        end_label = data['token_end']

        cse_loss = qaTrainer.Train(tokens, segs, masks, extend_doc_mask, start_label, end_label)
        
        start_loss.update(cse_loss[0])
        end_loss.update(cse_loss[1])

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | start_loss: {start_loss.val:.4f} ({start_loss.avg:.4f}) | end_loss : {end_loss.val:.4f} ({end_loss.avg:.4f}) '.format(
                    batch=batch_idx + 1,
                    size=len(train_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,

                    start_loss=start_loss,
                    end_loss=end_loss                  
                    )
        bar.next()

    bar.finish()
    qaTrainer.models[0].Session.Model.Save(args.output_model + '.'+str(epoch)+'.model')
