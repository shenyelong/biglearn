"""
preprocess CoQA for extractive question answering
2019/1/22: distinguish yes-no and wh- question
"""

import argparse
import json
import re
import time
import string
from collections import Counter
from collections import OrderedDict
from pycorenlp import StanfordCoreNLP

import tokenization

import unicodedata

#nlp = StanfordCoreNLP('http://localhost:9000')
#UNK = 'unknown'

def is_whitespace(c):
    if c == " " or c == "\t" or c == "\r" or c == "\n" or ord(c) == 0x202F:
        return True
    return False

def is_control(char):
    """Checks whether `chars` is a control character."""
    # These are technically control characters but we count them as whitespace characters.
    if char == "\t" or char == "\n" or char == "\r":
        return False
    cat = unicodedata.category(char)
    if cat.startswith("C"):
        return True
    return False

def process(text):
    #paragraph = nlp.annotate(text, properties={
    #                         'annotators': 'tokenize, ssplit',
    #                         'outputFormat': 'json',
    #                         'ssplit.newlineIsSentenceBreak': 'two'})
    output = {'word': [], 'offsets': []}
    try:
        c_idx = 0
        for c in text:
            c_idx += 1
            if(is_whitespace(c) or is_control(c)):
                continue
            output['word'].append(c)
            output['offsets'].append((c_idx - 1, c_idx ))
    except:
        print("error in line 45: {}".format(text))
    return output


def _str(s):
    """ Convert PTB tokens to normal tokens """
    if (s.lower() == '-lrb-'):
        s = '('
    elif (s.lower() == '-rrb-'):
        s = ')'
    elif (s.lower() == '-lsb-'):
        s = '['
    elif (s.lower() == '-rsb-'):
        s = ']'
    elif (s.lower() == '-lcb-'):
        s = '{'
    elif (s.lower() == '-rcb-'):
        s = '}'
    return s


def normalize_answer(s):
    """Lower text and remove punctuation, storys and extra whitespace."""

    def remove_articles(text):
        regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
        return re.sub(regex, ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))


def find_span_with_gt(context, offsets, ground_truth, w_start = -1, w_end = -1):
    """
    find answer from paragraph so that best f1 score is achieved
    """
    best_f1 = 0.0
    best_span = (len(context)-1, len(context)-1)
    best_pred = ''
    gt = normalize_answer(ground_truth).split()

    if(w_start == -1):
        w_start = 0
    if(w_end == -1):
        w_end = len(offsets)

    ls = [i for i in range(w_start, w_end) # len(offsets))
          if context[offsets[i][0]:offsets[i][1]].lower() in gt]

    for i in range(len(ls)):
        for j in range(i, len(ls)):
            pred = normalize_answer(context[offsets[ls[i]][0]: offsets[ls[j]][1]]).split()
            common = Counter(pred) & Counter(gt)
            num_same = sum(common.values())
            if num_same > 0:
                precision = 1.0 * num_same / len(pred)
                recall = 1.0 * num_same / len(gt)
                f1 = (2 * precision * recall) / (precision + recall)
                if f1 > best_f1:
                    best_f1 = f1
                    best_span = (offsets[ls[i]][0], offsets[ls[j]][1])
                    best_pred = context[offsets[ls[i]][0]: offsets[ls[j]][1]]
    return best_span, best_f1, best_pred

def f1Score(gt, pred):
    norm_gt = normalize_answer(gt).split()
    norm_pred = normalize_answer(pred).split()

    common = Counter(norm_pred) & Counter(norm_gt)
    num_same = sum(common.values())
    if num_same > 0:
        precision = 1.0 * num_same / len(norm_pred)
        recall = 1.0 * num_same / len(norm_gt)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1
    return 0

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    parser.add_argument("--vocab_file", default=None, type=str, help="tokenizer vocab file.")

    args = parser.parse_args()

    tokenizer = tokenization.FullTokenizer(args.vocab_file) 

    with open(args.data_file, 'r') as f:
        dataset = json.load(f)
        dataset = dataset['data']

    data = OrderedDict()
    data['data'] = []

    unique_ids = {}
    lines = 0
    for article in dataset:
        for paragraph in article['paragraphs']:
            context = tokenization.convert_to_unicode(paragraph["context"])

            #_datum = OrderedDict()
            #_datum["story"] = paragraph["context"]

            annotated_context_str = process(context)
            context_offsets = annotated_context_str['offsets']
            context_words = annotated_context_str['word']
            #context_words = [c for c in context]
            subtokens, word2subtoken = tokenizer.tokenize_words(context_words)
            subtokens_idx = tokenizer.convert_tokens_to_ids(subtokens)
            
            if(len(subtokens_idx) < len(context_words)):
                print('warning', len(subtokens_idx), len(context_words))

            _datum = OrderedDict()

            _datum["story"] = context

            _datum["story_subtoken_idx"] = subtokens_idx

            # word 2 token
            _datum["story_word2subtoken"] = word2subtoken

            # word 2 char
            _datum["story_wordoffset"] = context_offsets

            char_to_word_offset = []
            widx = 0
            for c_story in range(len(context)):
                if (widx + 1 >= len(context_words)):
                    char_to_word_offset.append(widx)
                    continue

                if (c_story < context_offsets[widx][0]):
                    char_to_word_offset.append(widx)
                    continue

                if (c_story >= context_offsets[widx][0] and c_story < context_offsets[widx][1]):
                    char_to_word_offset.append(widx)
                    continue

                if (c_story >= context_offsets[widx][1]):
                    if(len(context_words) > widx + 1):
                        char_to_word_offset.append(widx + 1)
                    else:
                        char_to_word_offset.append(widx)
                    widx = widx + 1
                    continue

            _datum["qa"] = []

            #context_tokens, context_token_spans = self.tokenizer.word_tokenizer(context)
            for question_answer in paragraph['qas']:

                _qa = OrderedDict()

                question = question_answer["question"]

                question_tokens = tokenizer.tokenize(question)
                question_idx = tokenizer.convert_tokens_to_ids(question_tokens)

                #question_tokens, _ = self.tokenizer.word_tokenizer(question)
                answers, span_starts, span_ends = [], [], []
                
                qid = question_answer['id']
                lines = lines + 1
                unique_ids[qid] = 1

                if "answers" in question_answer:
                    answers = [answer['text'].strip() for answer in question_answer['answers']]
                    span_starts = [answer['answer_start'] for answer in question_answer['answers']]
                    span_ends = [start + len(answer) for start, answer in zip(span_starts, answers)]
                    
                    m_ans = answers[0]


                    if(len(span_starts) >= 1 and len(span_ends) >= 1):
                        if(span_ends[0] - 1 >= len(char_to_word_offset)):
                            #span_starts[0] = span_starts[0] - 1
                            span_ends[0] = len(char_to_word_offset) - 1

                        span_word_start = char_to_word_offset[span_starts[0]]
                        span_word_end = char_to_word_offset[span_ends[0]-1]

                    for i in range(-2, 3):
                        for j in range(-2, 3):
                            if(span_word_start + i >= 0 and span_word_start + i < len(context_offsets) and span_word_end + j >= 0 and span_word_end + j < len(context_offsets)):

                                t_ans = context[context_offsets[span_word_start + i][0] : context_offsets[span_word_end + j][1]]

                                if(m_ans == t_ans):
                                    span_word_start = span_word_start + i
                                    span_word_end = span_word_end + j
                                    break 

                    #if(m_ans != t_ans):
                    #    span_word_start = span_word_start - 1
                    #    span_word_end = span_word_end - 1

                if 'is_impossible' in question_answer and question_answer['is_impossible']:
                    span_starts = [-1]
                    span_ends = [-1]

                    span_word_start = -1
                    span_word_end = -1

                #answer_char_spans = zip(span_starts, span_ends) if len( span_starts) > 0 and len(span_ends) > 0 else None
                answers = answers if len(answers) > 0 else [""]

                is_impossible = False if 'is_impossible' not in question_answer else question_answer['is_impossible'] 
                
                _qa["id"] = qid
                _qa["question"] = question_idx
                _qa["start"] = span_word_start
                _qa["end"] = span_word_end
                _qa["unk"] = is_impossible
                
                _qa["answer"] = answers
                _qa["word_answer"] =  context[context_offsets[span_word_start][0] : context_offsets[span_word_end][1]] if (span_word_start >= 0 and span_word_end >= 0) else "unk"

                _datum["qa"].append(_qa)

                if(_qa["answer"][0] != _qa["word_answer"]):
                    print(qid)
                    print(_qa["answer"][0])
                    print(_qa["word_answer"])

            data['data'].append(_datum)
            #break

    print('total paring lines', lines)
    print('total story', len(data['data']))
    print('unique qids', len(unique_ids))
    #data['data'] = _datum_list

                #span_word_start = char_to_word_offset[span_starts[0]]
                #span_word_end = char_to_word_offset[span_ends[0]]

                #yield self._make_instance(context, context_tokens, context_token_spans,
                #                          question, question_tokens, answer_char_spans, answers, is_impossible, qid)
    #_debug_f.close()

    with open(args.output_file, 'w') as output_file:
        json.dump(data, output_file, ensure_ascii=False, sort_keys=True, indent=4)




