from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import json
import math

import pickle
import numpy as np

import os
import tempfile
import argparse

import tqdm
from tqdm import tqdm, trange

import random
import collections

import torch
from torch.utils.data import Dataset

class cmrcExample(object):
    """
    a single training/test example for Squad dataset.
    """
    def __init__(self,
                 qid,
                 doc,
                 doc_tokens,
                 doc_w2tokens,
                 doc_woffset,
                 doc_wnum,
                 query_tokens,
                 start_position=None,
                 end_position=None,
                 unk_flag=None):
        self.qid = qid

        self.doc = doc
        self.doc_tokens = doc_tokens
        self.doc_w2tokens = doc_w2tokens
        self.doc_woffset = doc_woffset
        self.doc_wnum = doc_wnum

        self.query_tokens = query_tokens

        self.start_position = start_position
        self.end_position = end_position
        self.unk_flag = unk_flag

class cmrc_Dataset(Dataset):
    def __init__(self, corpus_path, max_seq_length, max_query_length, cls_token, sep_token, pad_token, vocabsize, fill_batch, is_enumate_doc_span, rseed = 110):
        self.corpus_path = corpus_path
        self.max_seq_length = max_seq_length
        self.max_query_length = max_query_length
        
        self.cls_token = cls_token
        self.sep_token = sep_token
        self.pad_token = pad_token
        self.vocabsize = vocabsize

        self.slide_chunk = 128

        ## enumate examples as doc span. 
        self.is_enumate_doc_span = is_enumate_doc_span
        self.fill_batch = fill_batch

        self.para_dict = {}
        self.para_list = []

        self.examples = self.read_cmrc_examples(corpus_path)
        self.doc_spans = self.get_example_spans(self.examples)

        self.rng = random.Random(rseed)

        self.example_num = len(self.examples)
        self.doc_span_num = len(self.doc_spans)

        print('all example number :', self.example_num, 'all doc span number :', self.doc_span_num)
        print('all qids :', len(self.qid2qaidx))

    def __len__(self):
        if(self.is_enumate_doc_span):
            return int((self.doc_span_num + self.fill_batch - 1)/ self.fill_batch) * self.fill_batch
        else:
            return int((self.example_num + self.fill_batch - 1)/ self.fill_batch) * self.fill_batch

    def __getitem__(self, item):
        if(self.is_enumate_doc_span):
            (p_id, doc_word_num, doc_word_offset, doc_start, doc_end, doc_token_count, tokens, segment_ids, doc_mask, extend_doc_mask, token_start, token_end, start_position, end_position, unk_flag) = self.doc_span_sample(item)
        else:
            (p_id, doc_word_num, doc_word_offset, doc_start, doc_end, doc_token_count, tokens, segment_ids, doc_mask, extend_doc_mask, token_start, token_end, start_position, end_position, unk_flag) = self.example_sample(item)

        tok_len = len(tokens)
        padding = [self.pad_token for _ in range(self.max_seq_length - tok_len)]
        
        tokens.extend(padding)
        segment_ids.extend(padding)
        doc_mask.extend(padding)
        extend_doc_mask.extend(padding)

        one_padding = [1 for _ in range(tok_len)]
        zero_padding = [0 for _ in range(self.max_seq_length - tok_len)]

        mask_ids = []
        mask_ids.extend(one_padding)
        mask_ids.extend(zero_padding)

        output = {"para_id" : self.qid2qaidx[p_id],

                  "doc_word_num" : doc_word_num,
                  "doc_word_offset" : doc_word_offset,
                  
                  "doc_start" : doc_start,
                  "doc_end" : doc_end,
                  "doc_count" : doc_token_count,

                  "token_input" : tokens,
                  "seg_input" : segment_ids,
                  "mask_input" : mask_ids,

                  "doc_mask" : doc_mask,
                  "extend_doc_mask" : extend_doc_mask,

                  "start_position" : start_position,
                  "end_position" : end_position,
                  
                  "token_start" : token_start,
                  "token_end" : token_end,

                  "unk_flag": unk_flag }

        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
    
    #def paragraph_ip2id(self, ipx):
    #    if(not ipx in self.para_dict):
    #        self.para_dict[ipx] = len(self.para_list)
    #        self.para_list.append(ipx)
    #    return self.para_dict[ipx]
        
    #def paragraph_id2ip(self, idx):
    #    return self.para_list[idx]

    def getspan(self, qid, startword, endword):
        idx = self.qid2storyidx[qid]
        story = self.story[idx]
        story_wordoffset = self.story_wordoffset[idx]
        soffset = story_wordoffset[startword][0]
        eoffset = story_wordoffset[endword][1]
        return story[soffset : eoffset]



    def get_token_span(self, qid, start_token, end_token):
        idx = self.qid2storyidx[qid]
        story = self.story[idx]
        
        story_wordoffset = self.story_wordoffset[idx]

        qa_id = self.qid2qaidx[qid]
        example = self.examples[qa_id]

        token2word = self.get_token2word_mask(example.doc_w2tokens, len(example.doc_tokens))

        start_word_idx = token2word[start_token]
        end_word_idx = token2word[end_token]

        soffset = story_wordoffset[start_word_idx][0]
        eoffset = story_wordoffset[end_word_idx][1]
        return story[soffset : eoffset]        

    def read_cmrc_examples(self, input_file):
        """
        read a Squad json file into a list of QA examples
        """
        with open(input_file, "r", encoding='utf-8') as reader:
            input_data = json.load(reader)['data']
        
        self.story = []
        self.story_wordoffset = []

        self.qid2storyidx = {}
        self.qid2qaidx = {}
        self.qids = []

        span_stat = {}

        examples = []
        
        #storyidx = 0
        #qaidx = 0

        print(len(input_data))
        for entry in input_data:
            # process story text
            #paragraph_id = entry["id"]
            #idx = self.paragraph_ip2id(paragraph_id)

            story = entry["story"]
            story_tokens = entry["story_subtoken_idx"]
            story_word2subtokens = entry["story_word2subtoken"]
            story_wordoffset = entry["story_wordoffset"]
            story_wordnum = len(story_wordoffset)

            assert len(story_word2subtokens) == len(story_wordoffset) + 1
 
            assert story_word2subtokens[story_wordnum] == len(story_tokens)


            #storyidx += 1
            self.story.append(story)
            self.story_wordoffset.append(story_wordoffset)

            for qa in entry['qa']:
                question_tokens = qa['question']
                qid = qa['id']

                start = qa["start"]
                end = qa["end"]
                unk = qa["unk"]

                self.qids.append(qid)
                self.qid2storyidx[qid] = len(self.story) - 1 #storyidx - 1
                self.qid2qaidx[qid] = len(self.qids) - 1 

                #if(qid == 'DEV_168_QUERY_0'):
                #    print('story word number :', story_wordnum)
                #    print('story token number :', len(story_tokens))

                span_len = end - start
                if(span_len in span_stat):
                    span_stat[span_len] += 1
                else:
                    span_stat[span_len] = 1

                example = cmrcExample(
                    qid=qid,
                    doc=story,
                    doc_tokens=story_tokens,
                    doc_w2tokens=story_word2subtokens,
                    doc_woffset=story_wordoffset,
                    doc_wnum=story_wordnum,
                    query_tokens=question_tokens,
                    start_position=start,
                    end_position=end,
                    unk_flag=unk)
                examples.append(example)
        print(span_stat)
        print(len(self.qids))
        print(len(self.qid2storyidx))
        print(len(self.qid2qaidx))
        
        return examples

    def get_example_spans(self, examples):
        doc_spans = []
        max_num_tokens = self.max_seq_length - 3
        example_idx = 0
        span_count = [0] * 10;

        for example in examples:
            query_len = min(self.max_query_length, len(example.query_tokens))
            max_doc_len = max_num_tokens - query_len
            
            all_doc_tokens = (example.doc_tokens)
            #doc_spans = []
            seg = 0
            start_offset = 0
            while start_offset < len(all_doc_tokens):
                length = len(all_doc_tokens) - start_offset
                if length > max_doc_len:
                    length = max_doc_len
                doc_spans.append((example_idx, start_offset, length))
                if start_offset + length == len(all_doc_tokens):
                    break
                start_offset += min(length, self.slide_chunk)
                seg += 1
            if(seg >= 9):
                seg = 9    
            span_count[seg] += 1    
            #doc_segments.append(doc_spans)
            example_idx += 1

        for i in range(0, 10):
            print("doc span distribution ", i, ":", span_count[i])
        return doc_spans

    def get_word_mask(self, word2token, tokenlen):
        word_mask = []
        widx = 0
        for i in range(0, tokenlen):
            if(word2token[widx] == i):
                word_mask.append(1)
                widx += 1
            else:
                word_mask.append(0)
        return word_mask
    
    def get_token2word_mask(self, word2token, tokenlen):
        token2word = []
        widx = 0
        for i in range(0, tokenlen):
            if(word2token[widx] == i):
                token2word.append(widx)
                widx += 1
            else:
                token2word.append(widx)
        return token2word


    def doc_span_sample(self, idx):
        if(idx >= self.doc_span_num):
            idx = self.rng.randint(0, self.doc_span_num - 1)
        doc_span = self.doc_spans[idx]

        example_id = doc_span[0]
        start_offset = doc_span[1]
        length = doc_span[2]

        trunc_start = start_offset
        trunc_end = start_offset + length

        example = self.examples[example_id]

        max_num_tokens = self.max_seq_length - 3

        tokens_a = example.doc_tokens
        doc_word_mask = self.get_word_mask(example.doc_w2tokens, len(tokens_a))

        tokens_b = example.query_tokens
        if len(tokens_b) > self.max_query_length:
            tokens_b = tokens_b[ : self.max_query_length]

        max_doc_len = max_num_tokens - len(tokens_b)
        
        token_start = example.doc_w2tokens[example.start_position]
        token_end = example.doc_w2tokens[example.end_position]

        unk_flag = example.unk_flag

        tokens_a = tokens_a[start_offset : start_offset + length]
        tokens_a_mask = doc_word_mask[start_offset : start_offset + length]

        skip_num = sum(doc_word_mask[ : start_offset])
        word_num = sum(tokens_a_mask)

        #if(example.qid == 'DEV_168_QUERY_0'):
        #    print('skip word number', skip_num)
        #    print('chunk word number', word_num)

        start_position = example.start_position - skip_num
        end_position = example.end_position - skip_num

        if(token_start < trunc_start or token_start >= trunc_end):
            token_start = 0
        else:
            token_start = token_start - trunc_start + 1

        if(token_end < trunc_start or token_end >= trunc_end):
            token_end = 0
        else:
            token_end = token_end - trunc_start + 1

        tokens = []
        segment_ids = []
        doc_mask = []
        extend_doc_mask = []

        tokens.append(self.cls_token)
        tokens.extend(tokens_a)
        tokens.append(self.sep_token)
        tokens.extend(tokens_b)
        tokens.append(self.sep_token)

        segment_ids.append(0)
        segment_ids.extend([0 for _ in range(len(tokens_a))])
        segment_ids.append(0)
        segment_ids.extend([1 for _ in range(len(tokens_b))])
        segment_ids.append(1)

        doc_mask.append(0)
        doc_mask.extend(tokens_a_mask)
        doc_mask.append(0)
        doc_mask.extend([0 for _ in range(len(tokens_b))])
        doc_mask.append(0)

        extend_doc_mask.append(1)
        extend_doc_mask.extend(tokens_a_mask)
        extend_doc_mask.append(0)
        extend_doc_mask.extend([0 for _ in range(len(tokens_b))])
        extend_doc_mask.append(0)

        assert len(tokens) <= self.max_seq_length
        assert len(segment_ids) <= self.max_seq_length
        assert len(doc_mask) <= self.max_seq_length

        return (example.qid, example.doc_wnum, skip_num, start_offset, start_offset + length, len(example.doc_tokens), tokens, segment_ids, doc_mask, extend_doc_mask, token_start, token_end, start_position, end_position, unk_flag * 1)

    def example_sample(self, idx):
        # sample another example.
        if(idx >= self.example_num):
            idx = self.rng.randint(0, self.example_num - 1)
        example = self.examples[idx]

        # Account for [CLS], [SEP], [SEP]
        max_num_tokens = self.max_seq_length - 3

        tokens_a = example.doc_tokens
        doc_word_mask = self.get_word_mask(example.doc_w2tokens, len(tokens_a))

        tokens_b = example.query_tokens
        tokens_b_mask = example.query_mask

        if len(tokens_b) > self.max_query_length:
            tokens_b = tokens_b[ : self.max_query_length]
            #tokens_b = tokens_b[-1 * self.max_query_length : ]
            #tokens_b_mask = tokens_b_mask[-1 * self.max_query_length : ]
        max_doc_len = max_num_tokens - len(tokens_b)
        
        unk_flag = example.unk_flag

        start_position = example.start_position
        end_position = example.end_position

        token_start = example.doc_w2tokens[start_position]
        token_end = example.doc_w2tokens[end_position]

        if(len(tokens_a) <= max_doc_len):
            left_num = 0
            trunc_start = 0
            trunc_end = len(tokens_a)
            tokens_a_mask = doc_word_mask
        else:
            loop = 0
            while(loop < 100):
                start_pos = self.rng.randint(0, len(tokens_a) - max_doc_len)
                trunc_start = start_pos
                trunc_end = start_pos + max_doc_len
                
                left_num = sum(doc_word_mask[ : trunc_start])
                mask_num = sum(doc_word_mask[trunc_start : trunc_end])

                if(start_position == -1 or end_position == -1):
                    break
                elif(start_position >= left_num and end_position < left_num + mask_num):
                    break
                loop += 1
                
            tokens_a = tokens_a[trunc_start : trunc_end]
            tokens_a_mask = doc_word_mask[trunc_start : trunc_end]

            #token_start = token_start - trunc_start
            #token_end = token_end - trunc_start

        if(token_start < trunc_start or token_start >= trunc_end):
            token_start = 0
        else:
            token_start = token_start - trunc_start + 1


        if(token_end < trunc_start or token_end >= trunc_end):
            token_end = 0
        else:
            token_end = token_end - trunc_start + 1


        start_position = start_position - left_num
        end_position = end_position - left_num

        tokens = []
        segment_ids = []
        doc_mask = []
        extend_doc_mask = []

        tokens.append(self.cls_token)
        tokens.extend(tokens_a)
        tokens.append(self.sep_token)
        tokens.extend(tokens_b)
        tokens.append(self.sep_token)

        segment_ids.append(0)
        segment_ids.extend([0 for _ in range(len(tokens_a))])
        segment_ids.append(0)
        segment_ids.extend([1 for _ in range(len(tokens_b))])
        segment_ids.extend(1)

        doc_mask.append(0)
        doc_mask.extend(tokens_a_mask)
        doc_mask.append(0)
        doc_mask.extend([0 for _ in range(len(tokens_b))])
        doc_mask.append(0)

        extend_doc_mask.append(1)
        extend_doc_mask.extend(tokens_a_mask)
        extend_doc_mask.append(0)
        extend_doc_mask.extend([0 for _ in range(len(tokens_b))])
        extend_doc_mask.append(0)

        assert len(tokens) <= self.max_seq_length
        assert len(segment_ids) <= self.max_seq_length
        assert len(doc_mask) <= self.max_seq_length

        return (example.qid, example.doc_wnum, left_num, trunc_start, trunc_end, len(example.doc_tokens), tokens, segment_ids, doc_mask, extend_doc_mask, token_start, token_end, start_position, end_position, unk_flag * 1)

        
