import sys
import io
import os
os.environ["CUDA_VISIBLE_DEVICES"]="-1" 

import tensorflow as tf
## load tensorflow bert model, and save the model checkpoints into binary. 

vgg16 = tf.train.NewCheckpointReader(sys.argv[1]) ## 'ckpt'

f_meta = open(sys.argv[1] + '.label','w')
f_bin = open(sys.argv[1] + '.bin','bw')

variables = vgg16.get_variable_to_shape_map()
for var_name in variables:
	if(var_name == 'global_step'):
		continue
	name = var_name
	shape = variables[name]
	dat = vgg16.get_tensor(name).flatten()
	f_meta.write(name + '\t' + str(len(dat)) + '\t' + str(shape) + '\n')
	f_bin.write(dat)

f_meta.close()
f_bin.close()

