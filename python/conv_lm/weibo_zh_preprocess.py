# preprocess wiki_zh corpus.


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tempfile
import tokenization
import argparse
import random
import json
import re

parser = argparse.ArgumentParser(description='Text Corpus Tokenization.')

# Datasets
parser.add_argument('-v', '--vocab', default='vocab.txt', type=str)
parser.add_argument('--input_post', default='input weibo post folder', type=str)
parser.add_argument('--input_response', default='input weibo response folder', type=str)

parser.add_argument('-o', '--output', default='output wiki index file', type=str)

args = parser.parse_args()

def split_paragraph(paragraph):
    for sent in re.findall(u'[^!?。\n]+[!?。\n]?', paragraph, flags=re.U):
        yield sent

def parse_wiki(file, tokenizer):
    docs = []
    with open(os.path.join(file), "r") as reader:
        line_idx = 0
        while True:
            text = reader.readline()
            if(not text):
                break
            try:
                doc = json.loads(text)
            except:
                print("error load json fail : ", text, '\n', file, line_idx)
                break

            line_idx += 1
            content = doc['text']

            document = []
            paragraphs = content.splitlines()
            
            for para in paragraphs:
                sentences = split_paragraph(para) #re.split('。|\n|！|？', content)
                for sent in sentences:
                    sent = tokenization.convert_to_unicode(sent)
                    tokens = tokenizer.tokenize(sent)
                    token_ids = tokenizer.convert_tokens_to_ids(tokens)
                    
                    if(len(token_ids) > 0):
                        document.append(token_ids)

            docs.append(document)
            #break
    return docs

def parse_weibo(file_post, file_response, tokenizer):
    all_docs = []

    with open(os.path.join(file_post), "r") as post_reader:
        with open(os.path.join(file_response), "r") as response_reader:
            line_idx = 0
            while True:
                post = post_reader.readline()
                response = response_reader.readline()

                if(not post or not response):
                    break

                post = tokenization.convert_to_unicode(post)
                post_tokens = tokenizer.tokenize(post)
                post_token_ids = tokenizer.convert_tokens_to_ids(post_tokens)

                response = tokenization.convert_to_unicode(response)
                response_tokens = tokenizer.tokenize(response)
                response_token_ids = tokenizer.convert_tokens_to_ids(response_tokens)

                if(len(post_token_ids) == 0 or len(response_token_ids) == 0):
                    continue

                document = []
                document.append(post_token_ids)
                document.append(response_token_ids)

                all_docs.append(document)

                line_idx = line_idx + 1

    return all_docs

def save(save_path, all_docs):
    with open(save_path, 'w') as writer:
        for doc in all_docs:
            for tokens in doc:
                writer.write(" ".join(str(x) for x in tokens)+'\n')
            writer.write('\n')

if __name__ == "__main__":
    tokenizer = tokenization.FullTokenizer(args.vocab) 
    all_docs = parse_weibo(args.input_post, args.input_response, tokenizer)
    save(args.output, all_docs)

