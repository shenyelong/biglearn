import argparse

from RecurConvDataset import RecurConvDataset
from ConditionalLMDataset import ConditionalLMDataset

from torch.utils.data import DataLoader
import sys
import torch

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import time
import math

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--train_dataset', required=True, type=str, help='train dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("--batch_size", type=int, default=48, help="number of batch_size")
parser.add_argument("--max_seq_length", type=int, default=256, help="maximum sequence length")
parser.add_argument("--max_chunk_length", type=int, default=20, help="maximum chunk length")
parser.add_argument("--max_memory_slot", type=int, default=512, help="maximum memory slot")

parser.add_argument("--embed", type=int, default=768, help="embedding dim")
parser.add_argument("--head", type=int, default=12, help="multi head attention")
parser.add_argument("--stack", type=int, default=3, help="stacking layers")


parser.add_argument("-r", "--short_lm_prob", type=float, default=0.1, help="short sequence probability")

parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")

parser.add_argument("-g", "--gpu_id", type=str, default=0, help="gpu id")

parser.add_argument('--epochs', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,10000:0.0001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')
parser.add_argument('--weight_decay', default=0.01, type=float, help='weight decay parameter')

#parser.add_argument('--is_seed', default=0, type=int, metavar='S', help='use seed model or random initialized.')

#parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--output_model', default='recur_transformer_lm', type=str, help='output model.')

parser.add_argument('--dropout', default='0.1', type=float, help='dropout rate in output layer.')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager, Session, ComputationGraph

from BigLearn import BaseBertModel, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure, TransformerStructure, AttentionTransformer

from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData, CudaPieceHelper, Shape

from BigLearn import GradientOptimizer, ParameterSetting, NCCL

from BigLearn import CrossEntropyRunner

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

# corpus_path, max_seq_length, max_predictions_per_seq, masked_lm_prob, short_seq_prob, cls_token, sep_token, pad_token, msk_token, vocabsize, rseed = 110):
print("Loading conv lm Dataset,", args.train_dataset)

# self, corpus_path, max_seq_length, max_pred_per_chunk, short_seq_prob, cls_token, sep_token, pad_token, msk_token, vocabsize, stop_batches, batch_size, rseed = 110):
#     def __init__(self, corpus_path, max_seq_length, max_chunk_length, short_seq_prob, cls_token, sep_token, pad_token, msk_token, vocabsize, stop_batches, batch_size, rseed = 110):

train_lm = RecurConvDataset(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_chunk_length=args.max_chunk_length,
                         short_seq_prob=args.short_lm_prob, cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token,
                         msk_token=args.msk_token, vocabsize=args.vocabsize, stop_batches=2000, batch_size=args.batch_size)

print("Creating Dataloader.")
train_data_loader = DataLoader(train_lm, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

print("Create bert model and setup enviroument.")


class ConvRecurrentTransformer:
    ## construct model 
    def __init__(self, vocab_size, chunk_size, memory_slot, embed, multi_head, batch_size, max_seq_length, dropout, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length

        self.vocab_size = vocab_size
        self.chunk_size = chunk_size
        self.embed = embed
        self.memory_slot = memory_slot
        self.chunk_num = int(self.max_seq_length / self.chunk_size)

        print('chunk number', self.chunk_num)
        self.stack_layer = args.stack

        self.multi_head = multi_head

        self.Models['token_embed'] = self.Session.Model.AddLayer(EmbedStructure(self.vocab_size, self.embed, self.Behavior.Device))
        self.Models['token_bias'] = self.Session.Model.AddLayer(LayerStructure(1, self.vocab_size, A_Func.Linear, True, self.Behavior.Device))

        self.Models['pos_embed'] = self.Session.Model.AddLayer(EmbedStructure(self.chunk_size, self.embed, self.Behavior.Device))
        #self.Models['pos_embed'] = self.Session.Model.AddLayer(EmbedStructure(self.max_seq_length, self.embed, self.Behavior.Device))

        self.Models['input_norm'] = self.Session.Model.AddLayer(LayerStructure(1, self.embed, A_Func.Linear, True, self.Behavior.Device))

        for block in range(self.stack_layer):
            self.Models['transformers_' + str(block) + '_1'] = self.Session.Model.AddLayer(TransformerStructure(self.embed, self.Behavior.Device))            
            self.Models['memory_' + str(block)] = self.Session.Model.AddLayer(EmbedStructure(self.memory_slot, self.embed, self.Behavior.Device))
            self.Models['recur_history_'+str(block)] = self.Session.Model.AddLayer(AttentionTransformer(self.embed, self.Behavior.Device))
            self.Models['recur_memory_' + str(block)] = self.Session.Model.AddLayer(AttentionTransformer(self.embed, self.Behavior.Device))
            self.Models['transformers_' + str(block) + '_2'] = self.Session.Model.AddLayer(TransformerStructure(self.embed, self.Behavior.Device))

        self.Models['output_layer'] = self.Session.Model.AddLayer(LayerStructure(self.embed, self.embed, A_Func.Linear, True, self.Behavior.Device))
        self.Models['output_norm'] = self.Session.Model.AddLayer(LayerStructure(1, self.embed, A_Func.Linear, True, self.Behavior.Device))
        
        self.grad = self.Session.Model.AllocateGradient(self.Behavior.Device)
        self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.weight_decay))

        # build computation graph.
        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        #self.mask_scale = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)
        #self.mask_bias = CudaPieceFloat(batch_size * max_seq_length, self.Behavior.Device)

        self.pred_masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.pred_idx = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        _dim = IntArgument('dim', self.embed)
        _chunk_size = IntArgument('chunk_size', self.chunk_size)
        _max_seq_length = IntArgument('max_seq_size', self.max_seq_length)
        _batch = IntArgument('batch', self.batch_size)
        _chunk_num = IntArgument('chunk_num', self.chunk_num)
        _pad = IntArgument('pad', 1)
        _batch_seq = IntArgument('batch_seq', self.batch_size * self.max_seq_length)
        _batch_chunk = IntArgument('batch_seq', self.batch_size * self.chunk_size)

        token_embed = self.Session.LookupEmbed(self.Models['token_embed'], self.tokens)

        #token_embed = self.Session.Reshape(token_embed, _dim, _max_seq_length, _batch)
        #pos_embed = self.Session.Reshape(self.Models['pos_embed'].NDEmbed, _dim, _max_seq_length, _pad)
        
        token_embed = self.Session.Reshape(token_embed, _dim, _chunk_size, _batch, _chunk_num)
        pos_embed = self.Session.Reshape(self.Models['pos_embed'].NDEmbed, _dim, _chunk_size, _pad, _pad)
        
        #token_embed = self.Session.Reshape(token_embed, _dim, _chunk_size, _batch)
        #pos_embed = self.Session.Reshape(self.Models['pos_embed'].NDEmbed, _dim, _chunk_size, _pad)

        tensor_embed = self.Session.Add(token_embed, pos_embed)
        
        _norm_embed = self.Session.Norm(tensor_embed, 0)
        self.norm_embed = self.Session.DotAndAdd(_norm_embed, self.Models['input_norm'].NDWeight, self.Models['input_norm'].NDBias)
            
        memory = []
        for block in range(self.stack_layer):
            _memory = self.Models['memory_' + str(block)].NDEmbed
            memory.append(self.Session.Expand(_memory, 2, _batch))

        #mask_scale = self.Session.Reshape(self.mask_scale, _chunk_size, _batch, _chunk_num)
        #mask_bias = self.Session.Reshape(self.mask_bias, _chunk_size, _batch, _chunk_num)

        chunk_embeds = []

        for chunk_idx in range(self.chunk_num): #self.chunk_num):
        #for chunk_idx in range(1): #self.chunk_num):
            #slice_embed = self.norm_embed # self.Session.Slice(self.norm_embed, chunk_idx, 1)
            #norm_chunk_embed = self.Session.Reshape(slice_embed, _dim, _max_seq_length, _batch)
            
            slice_embed = self.Session.Slice(self.norm_embed, [0, 0, 0, chunk_idx], _dim, _chunk_size, _batch, _pad)  
            norm_chunk_embed = self.Session.Reshape(slice_embed, _dim, _chunk_size, _batch)
            
            #chunk_pos_embed = self.Session.Reshape(self.Models['pos_embed'].NDEmbed, _dim, _chunk_size, _pad)
            #chunk_embed = self.Session.Add(chunk_token_embed, chunk_pos_embed)
            #_norm_chunk_embed = self.Session.Norm(chunk_embed, 0)
            #norm_chunk_embed = self.Session.DotAndAdd(_norm_chunk_embed, self.Models['input_norm'].NDWeight, self.Models['input_norm'].NDBias)
            
            #chunk_mask_scale = self.Session.Reshape(self.Session.Slice(mask_scale, chunk_idx, 1), _chunk_size, _batch)
            #chunk_mask_bias = self.Session.Reshape(self.Session.Slice(mask_bias, chunk_idx, 1), _chunk_size, _batch)

            for block in range(self.stack_layer):

                norm_chunk_embed = self.Session.Transformer(self.Models['transformers_' + str(block) + '_1'], norm_chunk_embed, self.multi_head, dropout)
                
                history_chunk_embed = self.Session.AttentionTransformer(self.Models['recur_history_'+str(block)], norm_chunk_embed, self.multi_head, memory[block], dropout)
                
                new_memory = self.Session.AttentionTransformer(self.Models['recur_memory_'+str(block)], memory[block], self.multi_head, norm_chunk_embed, dropout)
                
                norm_chunk_embed = self.Session.Transformer(self.Models['transformers_' + str(block) + '_2'], history_chunk_embed, self.multi_head, dropout)

                #norm_chunk_embed = self.Session.Transformer(self.Models['transformers_' + str(block) + '_2'], norm_chunk_embed, self.multi_head, dropout)
                
                memory[block] = new_memory

            chunk_embeds.append(norm_chunk_embed)

        print('number of chunk embeds 1 :', len(chunk_embeds))
        print('number of chunk embeds 2 :', self.chunk_num)
        
        print('chunk embeds shape dim', chunk_embeds[0].Shape.Length)
        for i in range(chunk_embeds[0].Shape.Length):
            print('chunk embeds shape', chunk_embeds[0].Shape[i].Default)
        
        self.all_embed = self.Session.Cat(chunk_embeds, 1)
        
        self.all_embed = self.Session.Dropout(self.all_embed, dropout)

        self.embed_2d_feature = self.Session.Reshape(self.all_embed, _dim, _batch_seq)
        #self.embed_2d_feature = self.Session.Reshape(self.all_embed, _dim, _batch_chunk)

        self.masked_embed = self.Session.LookupEmbed(self.embed_2d_feature, self.pred_masks)
        
        self._mw = self.Session.FNN(self.masked_embed, self.Models['output_layer'])

        self.mw = self.Session.Act(self._mw, A_Func.Gelu)
        
        self._normw = self.Session.Norm(self.mw, 0)
        
        self.normw = self.Session.DotAndAdd(self._normw, self.Models['output_norm'].NDWeight, self.Models['output_norm'].NDBias)

        self._word_scores = self.Session.MatMul(self.normw, 0, self.Models['token_embed'].NDEmbed, 1)

        self.word_scores = self.Session.Add(self._word_scores, self.Models['token_bias'].NDBias)

        self.loss = self.Session.SoftmaxLoss(self.word_scores, self.pred_idx)

    def printdata(self, name, data, topk):
        print(name)
        data.SyncToCPU()
        for i in range(min(topk, data.EffectiveSize)):
            print(data[i])

    def SetupPredMask(self, masks):
        acc_nums = [0] * (self.batch_size + 1)
        acc_nums[0] = 0

        batch_inc = 0
        for b in range(self.batch_size):
            word_inc = 0
            for s in range(self.max_seq_length):
            #for s in range(self.chunk_size):
                if(masks[b][s].item() >= 1):
                    self.pred_masks[batch_inc + word_inc] = b * self.max_seq_length + s
                    #self.pred_masks[batch_inc + word_inc] = b * self.chunk_size + s
                    word_inc = word_inc + 1
            batch_inc = batch_inc + word_inc
            acc_nums[b + 1] = batch_inc

        self.pred_masks.EffectiveSize = batch_inc
        self.pred_masks.SyncFromCPU()

        return acc_nums

    def SetupPredIndex(self, pred_tokens, acc_nums):
        self.pred_idx.EffectiveSize = acc_nums[self.batch_size];  

        for b in range(self.batch_size):
            batch_inc = acc_nums[b]
            word_num = acc_nums[b + 1] - acc_nums[b]
            for i in range(word_num):
                self.pred_idx[batch_inc + i] = pred_tokens[b][i].item()

        self.pred_idx.SyncFromCPU()

    def SetupTrain(self, tokens, masks, pred_tokens):
        #print(tokens)
        new_tokens = torch.transpose(torch.reshape(tokens, (self.batch_size, self.chunk_num, self.chunk_size)), 0, 1).contiguous()
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(new_tokens.data_ptr())),  self.batch_size * self.chunk_num * self.chunk_size)
        #print(new_tokens)
        #self.printdata('new tokens', self.tokens, 10000)

        #self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        
        #print(masks)
        #print(pred_tokens)

        #self.pred_masks.CopyTorch(IntPtr.op_Explicit(Int64(masks.data_ptr())),  self.batch_size * self.max_seq_length)

        #chunk_masks = torch.transpose(torch.reshape(masks, (self.batch_size, self.chunk_num, self.chunk_size)), 0, 1)
        #att_per_head = self.embed / self.multi_head
        #mask_scale = chunk_masks * 1.0 / math.sqrt(att_per_head)
        #mask_bias = (chunk_masks - 1) * 1e9

        #self.mask_scale.CopyTorch(IntPtr.op_Explicit(Int64(mask_scale.data_ptr())), self.batch_size * self.max_seq_length)
        #self.mask_bias.CopyTorch(IntPtr.op_Explicit(Int64(mask_bias.data_ptr())), self.batch_size * self.max_seq_length)

        acc_nums = self.SetupPredMask(masks)

        self.SetupPredIndex(pred_tokens, acc_nums)

class DistConvRecurrentTransformer:

    def __init__(self, vocab_size, chunk_size, memory_slot, embed, multi_head, batch_size, max_seq_length, dropout, device_num):

        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).TrainMode
            ParameterSetting.ResetRandom()
            return ConvRecurrentTransformer(vocab_size, chunk_size, memory_slot, embed, multi_head, self.sub_batch_size, max_seq_length, dropout, env_mode) 
            #def __init__(self, vocab_size, chunk_size, memory_slot, embed, multi_head, batch_size, max_seq_length, dropout, behavior):

        self.device_num = device_num

        self.batch_size = batch_size
        self.sub_batch_size = (int)(batch_size / device_num)
        print('sub batch size', self.sub_batch_size)

        self.max_seq_length = max_seq_length

        self.models = [] #* device_num
        for idx in range(device_num):
            self.models.append(__construct_model(idx))

        self.grad_reducer = NCCL(device_num)

        self.sessions =  [m.Session for m in self.models]
        self.grads = [m.grad for m in self.models]

    def printshape(self, name, data):
        shape_idx = 0
        for shape in data.Shape:
            print(name, shape_idx, shape.Default, shape.Value)
            shape_idx += 1
    
    def printdata(self, name, data, topk):
        print(name)
        data.SyncToCPU()
        for i in range(min(topk, data.EffectiveSize)):
            print(data[i])
        print('\n')

    def Train(self, tokens, masks, pred_tokens):
        def __setup(device_id):
            self.models[device_id].Behavior.Setup()

            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            self.models[device_id].SetupTrain(tokens[b_start : b_end], masks[b_start : b_end], pred_tokens[b_start : b_end])

        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)

        #self.models[0].Session.StepV3Run()

        #self.printshape('all embed shape', self.models[0].all_embed)
        #self.printdata('all embed data', self.models[0].all_embed.Output, 100)
        #self.models[0].all_embed.Output.Print('all embed', 100, False)


        #
        #self.printshape('all embed', self.models[0].all_embed)

        #self.printshape('masked_embed', self.models[0].masked_embed)
        #self.printshape('_mw', self.models[0]._mw)
        #self.printshape('mw', self.models[0].mw)
        #self.printshape('_normw', self.models[0]._normw)
        #self.printshape('normw', self.models[0].normw)
        #self.printshape('_word_scores', self.models[0]._word_scores)
        #self.printshape('word_scores', self.models[0].word_scores)
        
        self.grad_reducer.Execute(self.sessions, self.grads, self.device_num)

        #self.printshape('all embed shape', self.models[0].all_embed)
        #self.printdata('all embed data', self.models[0].all_embed.Output, 100)
        #self.models[0].all_embed.Output.Print('all embed', 100, False)

        #self.printshape('norm embed shape', self.models[0].norm_embed)
        #sys.exit(0)

        loss_es = [ m.loss.Value for m in self.models ]
        loss_avg = sum(loss_es) / self.device_num # [ x / self.device_num for x in [sum(loss) for loss in zip(*loss_es)]]
        return loss_avg #, ratio_avg

#     def __init__(self, vocab_size, chunk_size, memory_slot, embed, multi_head, batch_size, max_seq_length, dropout, device_num):
convLMTrainer = DistConvRecurrentTransformer(args.vocabsize, args.max_chunk_length, args.max_memory_slot, args.embed, args.head, args.batch_size, args.max_seq_length, args.dropout, deviceNum)

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

for epoch in range(0, args.epochs):
    print('\nEpoch: [%d | %d] LR: %f' % (epoch + 1, args.epochs, args.lr))
    ## set training mode.
    #dist_net.SetTrainMode()
    #dist_net.SetPredictMode()
    #dist_net.Init()
    loss = AverageMeter()
    data_time = AverageMeter()
    batch_time = AverageMeter()

    bar = Bar('training', max=len(train_data_loader))
    
    end = time.time()

    for batch_idx, data in enumerate(train_data_loader):
        data = {key: value for key, value in data.items()}
        
        input_tokens = data['tokens']
        input_masks = data['masks']

        output_tokens = data['pred_tokens']
        data_time.update(time.time() - end)
        wloss = convLMTrainer.Train(input_tokens, input_masks, output_tokens)
        loss.update(wloss)

        batch_time.update(time.time() - end)
        end = time.time()

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Data : {data_time.val:.4f} ({data_time.avg:.4f}) | Batch : {batch_time.val:.4f} ({batch_time.avg:.4f}) | loss: {loss.val:.4f} ({loss.avg:.4f})'.format(
                    batch=batch_idx + 1,
                    size=len(train_data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    data_time=data_time,
                    batch_time=batch_time,
                    loss=loss
                    )
        bar.next()
        
        #sys.exit(0)

        #print(sent_loss)
        #print(word_loss)
        #break
        #sent_label = data['is_next']      
    #break
    bar.finish()
    #dist_net.Complete()
    convLMTrainer.models[0].Session.Model.Save(args.output_model + '.'+str(epoch)+'.model')
    #dist_net.SaveModel(args.output_model + '.' + str(epoch) + '.model')
