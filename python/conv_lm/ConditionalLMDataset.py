from torch.utils.data import Dataset
import tqdm
import torch
import random
import collections

class ConditionalLMDataset(Dataset):
    ## load corpus.
    def load_corpus(self, corpus_path):
        all_documents = [[]]

        sent_line = 0
        with open(corpus_path, "r") as reader:
            while True:
                line = reader.readline()
                if not line:
                    break
                line = line.strip().strip('\n')
                if not line:
                    #doc2idx.append(sent_line)
                    all_documents.append([])
                    continue

                tokens = line.split(' ')
                tok_ids = []
                for token in tokens:
                    tok_ids.append(int(token))
                if(tok_ids):
                    all_documents[-1].append(tok_ids)                
                sent_line += 1
                if(sent_line % 500000 == 0):
                    print('loading sentence', sent_line, 'document num', len(all_documents))
                    #break
        all_documents = [x for x in all_documents if x]

        # skip the first line as target.
        doc2idx = []
        doc2idx.append(0)
        for x in all_documents:
            doc2idx.append(len(x) - 1 + doc2idx[-1])

        sent2doc = []
        doc_idx = 0
        for x in all_documents:
            sent2doc.extend([ doc_idx for _ in range(len(x) - 1 ) ])
            doc_idx += 1

        return (all_documents, sent2doc, doc2idx)

    def shuffle(self):
        self.shuffle_cursor += self.stop_batches * self.batch_size

    def __init__(self, corpus_path, max_seq_length, max_pred_per_chunk, short_seq_prob, cls_token, sep_token, pad_token, msk_token, vocabsize, stop_batches, batch_size, rseed = 110):
        self.corpus_path = corpus_path
        self.max_seq_length = max_seq_length
        self.max_pred_per_chunk = max_pred_per_chunk
        self.short_seq_prob = short_seq_prob

        self.cls_token = cls_token
        self.sep_token = sep_token
        self.pad_token = pad_token
        self.msk_token = msk_token

        self.stop_batches = stop_batches
        self.batch_size = batch_size

        self.vocabsize = vocabsize

        (self.all_documents, self.sent2doc, self.doc2idx) = self.load_corpus(corpus_path)
        self.rng = random.Random(rseed)

        print('all document number', len(self.all_documents))
        print('all sentence number', len(self.sent2doc))
        
        self.document_lines = len(self.all_documents)
        self.corpus_lines = len(self.sent2doc)
        
        self.shuffle_cursor = 0
        self.shuffle_cache = list(range(self.corpus_lines))
        random.shuffle(self.shuffle_cache)

    def __len__(self):
        return self.stop_batches * self.batch_size # self.corpus_lines

    def __getitem__(self, item):
        (input_tokens, output_tokens) = self.sample(item)

        input_token_len = len(input_tokens)
        output_token_len = len(output_tokens)

        #tok_len = len(tokens)
        input_padding = [self.pad_token for _ in range(self.max_seq_length - input_token_len)]
        output_padding = [self.pad_token for _ in range(self.max_pred_per_chunk + 1 - output_token_len)]

        seg_0_padding = [0 for _ in range(input_token_len - output_token_len)]
        seg_1_padding = [1 for _ in range(output_token_len)]

        segment_ids = seg_0_padding + seg_1_padding + input_padding

        #mask_1_padding = [1 for _ in range(input_token_len)]
        #mask_ids = mask_1_padding + input_padding

        input_tokens.extend(input_padding)
        output_tokens.extend(output_padding)

        mask_1_padding = [1 for _ in range(input_token_len - output_token_len)]
        mask_0_padding = [0 for _ in range(output_token_len)]
        mask_ids = mask_1_padding + mask_0_padding + input_padding

        att_mask = [ mask_ids if s < input_token_len - output_token_len or s >= input_token_len else [1 for _ in range(s + 1)] + [0 for _ in range(self.max_seq_length - s - 1)] for s in range(self.max_seq_length)]

        output = {"token_input": input_tokens,
                  "seg_input": segment_ids,
                  "att_mask": att_mask,

                  "token_output" : output_tokens,

                  "input_length": input_token_len,
                  "output_length": output_token_len }

        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
    

    def sample(self, idx):
        #self.sample_count += 1
        all_sent_idx = (idx + self.shuffle_cursor) % self.corpus_lines

        doc_idx = self.sent2doc[all_sent_idx]
        doc_start_idx = self.doc2idx[doc_idx]
        sent_idx = all_sent_idx - doc_start_idx + 1

        document = self.all_documents[doc_idx]

        target_seq = document[sent_idx]
        target_len = len(target_seq)

        if target_len > self.max_pred_per_chunk:
            input_tokens = [ self.msk_token ] + target_seq[ : self.max_pred_per_chunk ] #+  target_seq[- self.max_pred_per_chunk : ]
            output_tokens = target_seq[ : self.max_pred_per_chunk ] + [ self.cls_token ]
        else:
            input_tokens = [ self.msk_token ] + target_seq
            output_tokens = target_seq + [ self.cls_token ]
        
        #remaining_short = self.short_seq_prob
        k = 0
        while len(input_tokens) < self.max_seq_length and sent_idx > k :
            #p_end = random.random() 
            #if(p_end <= remaining_short):
            #    break
            input_tokens = document[sent_idx - k - 1] + input_tokens
            k = k + 1
            #remaining_short = self.short_seq_prob / ( 1 - k * self.short_seq_prob)

        if len(input_tokens) > self.max_seq_length:
            input_tokens = input_tokens[-self.max_seq_length : ]

        return (input_tokens, output_tokens)


        
