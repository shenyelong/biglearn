import argparse

from LMConvDataset import LMConvDataset
from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os
import time

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--train_dataset', required=True, type=str, help='train dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")
parser.add_argument("-s", "--max_seq_length", type=int, default=256, help="maximum sequence length")
parser.add_argument("-p", "--max_pred_per_chunk", type=int, default=20, help="maximum prediction per chunk")

parser.add_argument("-r", "--short_lm_prob", type=float, default=0.1, help="short sequence probability")

parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")

parser.add_argument("-g", "--gpu_id", type=str, default=0, help="gpu id")

parser.add_argument('--epochs', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,10000:0.0001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')
parser.add_argument('--weight_decay', default=0.01, type=float, help='weight decay parameter')

parser.add_argument('--is_seed', default=0, type=int, metavar='S', help='use seed model or random initialized.')

#parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--output_model', default='bert_CoQA', type=str, help='output model.')

parser.add_argument('--dropout', default='0.1', type=float, help='dropout rate in output layer.')

parser.add_argument('--layer', default=12, type=int, help='default transformer layer.')

parser.add_argument('--stop_batch', default=10000, type=int, help='shuffle per stop_batch.')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager, Session, ComputationGraph

from BigLearn import BaseBertModel, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure

from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData

from BigLearn import GradientOptimizer, ParameterSetting, NCCL

from BigLearn import CrossEntropyRunner

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

# corpus_path, max_seq_length, max_predictions_per_seq, masked_lm_prob, short_seq_prob, cls_token, sep_token, pad_token, msk_token, vocabsize, rseed = 110):
print("Loading conv lm Dataset,", args.train_dataset)

# self, corpus_path, max_seq_length, max_pred_per_chunk, short_seq_prob, cls_token, sep_token, pad_token, msk_token, vocabsize, stop_batches, batch_size, rseed = 110):
train_lm = LMConvDataset(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, short_seq_prob=args.short_lm_prob, cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token,
                         msk_token=args.msk_token, vocabsize=args.vocabsize, stop_batches = args.stop_batch, batch_size = args.batch_size)

print("Creating Dataloader.")
train_data_loader = DataLoader(train_lm, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

print("Create bert model and setup enviroument.")

class ConvTransformerLM:
    ## construct model 
    def __init__(self, batch_size, max_seq_length, dropout, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length

        # int mcate, int mlayer, int frozenlayer, int mvocabSize, DeviceType device)
        self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(2, args.layer, -1, args.vocabsize, behavior.Device))
        
        self.grad = self.Session.Model.AllocateGradient(self.Behavior.Device)

        self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.weight_decay))

        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        
        self.pred_idx = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        
        self.mask_fea = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        _dim = IntArgument('dim', 768)
        _max_seq_length = IntArgument('max_seq_size', self.max_seq_length)
        _batch = IntArgument('batch', self.batch_size)
        _pad = IntArgument('pad', 1)
        _batch_seq = IntArgument('batch_seq', self.batch_size * self.max_seq_length)

        
        token_embed = self.Session.LookupEmbed(self.Models['bert_base'].TokenEmbed, self.tokens)
        
        token_embed = self.Session.Reshape(token_embed, _dim, _max_seq_length, _batch)
        pos_embed = self.Session.Reshape(self.Models['bert_base'].PosEmbed.NDEmbed, _dim, _max_seq_length, _pad)
        tensor_embed = self.Session.Add(token_embed, pos_embed)
        
        _norm_embed = self.Session.Norm(tensor_embed, 0)
        norm_embed = self.Session.DotAndAdd(_norm_embed, self.Models['bert_base'].NdNormScale, self.Models['bert_base'].NdNormBias)

        for block in range(args.layer):
            norm_embed = self.Session.Transformer(self.Models['bert_base'].Blocks[block], norm_embed, 12, 0.1)
        
        self.embed_feature = self.Session.Dropout(norm_embed, dropout)
        
        self.embed_2d_feature = self.Session.Reshape(self.embed_feature, _dim, _batch_seq)
        self.masked_embed = self.Session.LookupEmbed(self.embed_2d_feature, self.mask_fea)

        self._mw = self.Session.FNN(self.masked_embed, self.Models['bert_base'].OutputLayer)
        self.mw = self.Session.Act(self._mw, A_Func.Gelu)
        self._normw = self.Session.Norm(self.mw, 0)
        self.normw = self.Session.DotAndAdd(self._normw, self.Models['bert_base'].NdOutputNormScale, self.Models['bert_base'].NdOutputNormBias)

        self._word_scores = self.Session.MatMul(self.normw, 0, self.Models['bert_base'].NdTokenEmbed, 1)

        self.word_scores = self.Session.Add(self._word_scores, self.Models['bert_base'].NdTBias)

        self.loss = self.Session.SoftmaxLoss(self.word_scores, self.pred_idx)

    def SetupMaskFea(self, tok_len):
        acc_nums = [0] * (self.batch_size + 1)
        acc_nums[0] = 0

        batch_inc = 0
        for b in range(self.batch_size):
            word_inc = 0
            for s in range(tok_len[b].item()):
                self.mask_fea[batch_inc + word_inc] = b * self.max_seq_length + s
                word_inc = word_inc + 1
            batch_inc = batch_inc + word_inc
            acc_nums[b + 1] = batch_inc

        self.mask_fea.EffectiveSize = batch_inc
        self.mask_fea.SyncFromCPU()

        return acc_nums

    def SetupLabel(self, out_tokens, acc_nums):
        self.pred_idx.EffectiveSize = acc_nums[self.batch_size];  

        for b in range(self.batch_size):
            batch_inc = acc_nums[b]
            word_num = acc_nums[b + 1] - acc_nums[b]
            for i in range(word_num):
                self.pred_idx[batch_inc + i] = out_tokens[b][i].item()

        self.pred_idx.SyncFromCPU()

    def SetupTrain(self, tokens, tok_len, out_tokens):
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        
        acc_nums = self.SetupMaskFea(tok_len)

        self.SetupLabel(out_tokens, acc_nums)

class DistConvTransformerLM:

    def __init__(self, batch_size, max_seq_length, dropout, device_num):

        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).TrainMode
            ParameterSetting.ResetRandom()
            return ConvTransformerLM(self.sub_batch_size, max_seq_length, dropout, env_mode)

        self.device_num = device_num

        self.batch_size = batch_size
        self.sub_batch_size = (int)(batch_size / device_num)
        self.max_seq_length = max_seq_length

        self.models = [] #* device_num
        for idx in range(device_num):
            self.models.append(__construct_model(idx))

        self.grad_reducer = NCCL(device_num)

        self.sessions =  [m.Session for m in self.models]
        self.grads = [m.grad for m in self.models]

    def printshape(self, name, data):
        shape_idx = 0
        for shape in data.Shape:
            print(name, shape_idx, shape.Default, shape.Value)
            shape_idx += 1

    def Train(self, tokens, tok_len, out_tokens):
        def __setup(device_id):
            self.models[device_id].Behavior.Setup()
            b_start = device_id * self.sub_batch_size 
            b_end = (device_id + 1) * self.sub_batch_size 

            self.models[device_id].SetupTrain(tokens[b_start : b_end], tok_len[b_start : b_end], out_tokens[b_start : b_end])
        #print(mask_doc)
        for idx in range(self.device_num):
            __setup(idx)
        
        self.grad_reducer.Execute(self.sessions, self.grads, self.device_num)

        loss_es = [ m.loss.Value for m in self.models ]
        loss_avg = sum(loss_es) / self.device_num # [ x / self.device_num for x in [sum(loss) for loss in zip(*loss_es)]]
        return loss_avg #, ratio_avg

convLMTrainer = DistConvTransformerLM(args.batch_size, args.max_seq_length, args.dropout, deviceNum)

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

for epoch in range(0, args.epochs):
    print('\nEpoch: [%d | %d] LR: %f' % (epoch + 1, args.epochs, args.lr))
    ## set training mode.
    #dist_net.SetTrainMode()
    #dist_net.SetPredictMode()
    #dist_net.Init()
    loss = AverageMeter()
    data_time = AverageMeter()
    batch_time = AverageMeter()

    bar = Bar('training', max=len(train_data_loader))
    
    end = time.time()
    train_lm.shuffle()
    for batch_idx, data in enumerate(train_data_loader):
        data = {key: value for key, value in data.items()}
        
        input_tokens = data['token_input'] 

        output_tokens = data['token_output']
        
        token_len = data['token_len']

        data_time.update(time.time() - end)

        wloss = convLMTrainer.Train(input_tokens, token_len, output_tokens)
        loss.update(wloss)

        batch_time.update(time.time() - end)
        end = time.time()

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Data : {data_time.val:.4f} ({data_time.avg:.4f}) | Batch : {batch_time.val:.4f} ({batch_time.avg:.4f}) | loss: {loss.val:.4f} ({loss.avg:.4f})'.format(
                    batch=batch_idx + 1,
                    size=len(train_data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    data_time=data_time,
                    batch_time=batch_time,
                    loss=loss
                    )
        bar.next()
            
    #break
    bar.finish()
    #dist_net.Complete()
    convLMTrainer.models[0].Session.Model.Save(args.output_model + '.'+str(epoch)+'.model')
    #dist_net.SaveModel(args.output_model + '.' + str(epoch) + '.model')
