# preprocess wiki_zh corpus.


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tempfile
import tokenization
import argparse
import random
import json
import re

parser = argparse.ArgumentParser(description='Text Corpus Tokenization.')

# Datasets
parser.add_argument('-v', '--vocab', default='vocab.txt', type=str)
parser.add_argument('-i', '--input', default='input chinese xhj data file', type=str)
parser.add_argument('-o', '--output', default='output xhj index file', type=str)

args = parser.parse_args()

def split_paragraph(paragraph):
    for sent in re.findall(u'[^!?。\n]+[!?。\n]?', paragraph, flags=re.U):
        yield sent

def parse_wiki(file, tokenizer):
    docs = []
    with open(os.path.join(file), "r") as reader:
        line_idx = 0
        while True:
            text = reader.readline()
            if(not text):
                break
            try:
                doc = json.loads(text)
            except:
                print("error load json fail : ", text, '\n', file, line_idx)
                break

            line_idx += 1
            content = doc['text']

            document = []
            paragraphs = content.splitlines()
            
            for para in paragraphs:
                sentences = split_paragraph(para) #re.split('。|\n|！|？', content)
                for sent in sentences:
                    sent = tokenization.convert_to_unicode(sent)
                    tokens = tokenizer.tokenize(sent)
                    token_ids = tokenizer.convert_tokens_to_ids(tokens)
                    
                    if(len(token_ids) > 0):
                        document.append(token_ids)

            docs.append(document)
            #break
    return docs

def parse_xhj(data_file, tokenizer):
    all_docs = []
    entry_num = 0
    for sub_folder in os.listdir(folder):
        if(sub_folder.startswith('.')):
            continue

        sub_folder = os.path.join(folder, sub_folder)

        for entry in os.listdir(sub_folder):
            if(entry.startswith('.')):
                continue
            entry = os.path.join(sub_folder, entry)
            print('parsing ', entry , '...')
            sub_docs = parse_wiki(entry, tokenizer)
            all_docs.extend(sub_docs)
            #break
        #break
    return all_docs

def save_wikiset(save_path, all_docs):
    with open(save_path, 'w') as writer:
        for doc in all_docs:
            for tokens in doc:
                writer.write(" ".join(str(x) for x in tokens)+'\n')
            writer.write('\n')

if __name__ == "__main__":
    tokenizer = tokenization.FullTokenizer(args.vocab) 
    all_docs = parse_wikiset(args.input, tokenizer)
    save_wikiset(args.output, all_docs)

