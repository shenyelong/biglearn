import argparse

from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os

import torch

from pathos.multiprocessing import ProcessingPool as Pool
#from torch.multiprocessing import Pool, Process, set_start_method

#from pathos.multiprocessing import Process as Process

#import multiprocessing
#from eventlet import GreenPool
#from multiprocessing import Pool

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/data/home/yelongshen/sharelib/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument("-g", "--gpu_id", type=str, default=0, help="gpu id")

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, CudaPieceFloat, NCCL
from BigLearn.DeepNet.UnitTest import AllReduceTest

#from BigLearn.DeepNet.Language import LargeBert, Bert, DistBert

device_num = len(args.gpu_id.split(','))
print("set gpu number ", device_num)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

#nccl = NCCL(device_num)

#pool = GreenPool(device_num)
pool = Pool(device_num)

data_size = 8

def __init_data(idx):
	return torch.rand([8])

vecs = pool.map(__init_data, range(device_num))
print(vecs)

#pool.close()

#mode = DeviceBehavior(0).TrainMode
#nccl = NCCL(device_num)

test = AllReduceTest(device_num)
test.Run()

def __all_reduce(idx):
	#print(idx)
	#os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)
	env_mode = DeviceBehavior(idx).TrainMode
	#env_mode.Setup()
	#d = CudaPieceFloat(8, env_mode.Device)
	#d.CopyTorch(IntPtr.op_Explicit(Int64(vecs[idx].data_ptr())), 8)
	#nccl.AllReduce(d, idx)
	#d.PasteTorch(IntPtr.op_Explicit(Int64(vecs[idx].data_ptr())), 8)

#pool2 = Pool(device_num)

pool.map(__all_reduce, range(device_num))

#pool.map(__all_reduce, range(device_num))


pool.close()

print(vecs)

#nccl = NCCL(device_num)



#test.RunBcast()
