import argparse

from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument("-g", "--gpu_id", type=int, default=0, help="gpu id")

parser.add_argument('--rank', '-r', default=0, type=int, help='Rank of the current process.')
parser.add_argument('--world_size','-s', default=1, type=int, help='Number of processes participating in the job.')
parser.add_argument('--ip','-i', type=str, default='10.12.192.13', help='ip specifying how to initialize the package.')
parser.add_argument('--port', '-p', default='32145', type=str, help='Rank of the current process.')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, CudaPieceFloat
from BigLearn.DeepNet.UnitTest import GPUCommTest

import torch
import torch.distributed as dist

#from BigLearn.DeepNet.Language import LargeBert, Bert, DistBert
print("set gpu id ", args.gpu_id)
torch.cuda.set_device(args.gpu_id)

os.environ['MASTER_ADDR'] = args.ip # '10.12.192.13'
os.environ['MASTER_PORT'] = args.port
dist.init_process_group(backend='nccl', rank=args.rank, world_size=args.world_size)

data = torch.zeros([ 100 ], dtype=torch.float, device='cuda:'+str(args.gpu_id))
p_data = IntPtr.op_Explicit(Int64(data.data_ptr()))

# public static void RunAssginCuda(int length, IntPtr data, float v, int deviceId)
bl_data = GPUCommTest.RunAssginCuda(100, p_data, args.gpu_id, args.gpu_id)
dist.broadcast(data, 0)
#dist.all_reduce(data, op=dist.reduce_op.SUM) #, group=group)
GPUCommTest.Print(bl_data) 
#.Print('data', 100)





