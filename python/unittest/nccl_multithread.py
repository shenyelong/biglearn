import argparse

from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os

import torch


#from pathos.multiprocessing import ProcessingPool as Pool
#from torch.multiprocessing import Pool, Process, set_start_method

#from pathos.multiprocessing import Process as Process

import threading 

#from eventlet import GreenPool
#from multiprocessing import Pool

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/data/home/yelongshen/sharelib/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument("-g", "--gpu_id", type=str, default=0, help="gpu id")

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager, Session, ComputationGraph
from BigLearn import BaseBertModel, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure
from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData, CudaPieceHelper
from BigLearn import GradientOptimizer, ParameterSetting, NCCL
from BigLearn import CrossEntropyRunner
from BigLearn.DeepNet.UnitTest import AllReduceTest

#from BigLearn.DeepNet.Language import LargeBert, Bert, DistBert

device_num = len(args.gpu_id.split(','))
print("set gpu number ", device_num)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

class baseNet:
    def __init__(self, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        _batch = IntArgument('batch', 5)
        _dim = IntArgument('dim', 4)

        x = NdArrayData(self.Behavior.Device, _dim, _batch)
        x.Output.Init(10.0)

        y = NdArrayData(self.Behavior.Device, _dim, _batch)
        y.Output.Init(5.0)

        self.z = self.Session.Add(x, y)


    def run(self):
        self.Behavior.Setup()
        self.Session.ForwardV3()

class DistNet:
    def __init__(self, device_num):
        def __construct_model(device_id):
            env_mode = DeviceBehavior(device_id).TrainMode
            ParameterSetting.ResetRandom()
            return baseNet(env_mode)

        self.device_num = device_num
  

        self.nets = [] #* device_num
        self.threads = []
        for idx in range(device_num):
            self.nets.append(__construct_model(idx))
            self.threads.append(threading.Thread(target=self.nets[idx].run, args = ()))
        
        self.grad_reducer = NCCL(device_num)

        #self.sessions =  [m.Session for m in self.models]
        self.grads = [m.z.Output for m in self.nets]

    def run(self):
        for i in range(self.device_num):
            self.threads[i].start()

        start

        for i in range(self.device_num):
            CudaPieceHelper.Print(self.nets[i].z.Output, 'device'+str(i), 100, False)
        
        self.grad_reducer.AllReduce(self.grads, self.device_num)

        for i in range(self.device_num):
            CudaPieceHelper.Print(self.nets[i].z.Output, 'device'+str(i), 100, False)
        
        #Execute(self.sessions, self.grads, self.device_num)

        print('multi-threading running done.')
distnet = DistNet(device_num)
distnet.run()

#nccl = NCCL(device_num)

#pool = GreenPool(device_num)
#pool = Pool(device_num)

#data_size = 8

#def __init_data(idx):
#    return torch.rand([8])

#vecs = pool.map(__init_data, range(device_num))
#print(vecs)

#pool.close()

#mode = DeviceBehavior(0).TrainMode
#nccl = NCCL(device_num)

#test = AllReduceTest(device_num)
#test.Run()

#def __all_reduce(idx):
    #print(idx)
    #os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)
#    env_mode = DeviceBehavior(idx).TrainMode
    #env_mode.Setup()
    #d = CudaPieceFloat(8, env_mode.Device)
    #d.CopyTorch(IntPtr.op_Explicit(Int64(vecs[idx].data_ptr())), 8)
    #nccl.AllReduce(d, idx)
    #d.PasteTorch(IntPtr.op_Explicit(Int64(vecs[idx].data_ptr())), 8)

#pool2 = Pool(device_num)

#pool.map(__all_reduce, range(device_num))

#pool.map(__all_reduce, range(device_num))


#pool.close()

#print(vecs)

#nccl = NCCL(device_num)



#test.RunBcast()
