import argparse
import os
import random
import shutil
import time
import warnings
import sys

parser = argparse.ArgumentParser(description='Generate Arithmetic Word Problem.')

parser.add_argument('-o', '--maxop', default=4, type=int, metavar='M',
                    help='max operations (default: 5)')

parser.add_argument('-n', '--num', default=100, type=int, metavar='N',
                    help='range of input (default: 100)')

def main():
    args = parser.parse_args()
    max_op = args.maxop
    num = args.num

    for i in range(0, max_op):
        

if __name__ == '__main__':
    main()