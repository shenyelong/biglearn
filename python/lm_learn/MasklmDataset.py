from torch.utils.data import Dataset
import tqdm
import torch
import random
import collections


class MasklmDataset(Dataset):
    def __init__(self, corpus_path, max_seq_length, max_predictions_per_seq, masked_lm_prob, short_seq_prob, cls_token, sep_token, pad_token, msk_token, vocabsize, rseed = 110):
        self.corpus_path = corpus_path
        self.max_seq_length = max_seq_length
        self.max_predictions_per_seq = max_predictions_per_seq
        self.masked_lm_prob = masked_lm_prob
        self.short_seq_prob = short_seq_prob
        
        self.cls_token = cls_token
        self.sep_token = sep_token
        self.pad_token = pad_token
        self.msk_token = msk_token

        self.vocabsize = vocabsize

        (self.all_documents, self.sent2doc, self.doc2idx) = self.load_corpus(corpus_path)
        self.rng = random.Random(rseed)

        print('all document number', len(self.all_documents))
        print('all sentence number', len(self.sent2doc))
        
        self.corpus_lines = len(self.sent2doc)
        self.label_stat = [0, 0]
        self.sample_count = 0

    def __len__(self):
        return self.corpus_lines

    def __getitem__(self, item):
        (tokens, segment_ids, is_random_next, masked_lm_positions, masked_lm_labels) = self.random_pair(item)
        #print(item)
        tok_len = len(tokens)
        padding = [self.pad_token for _ in range(self.max_seq_length - tok_len)]
        
        tokens.extend(padding)
        segment_ids.extend(padding)

        one_padding = [1 for _ in range(tok_len)]
        zero_padding = [0 for _ in range(self.max_seq_length - tok_len)]
        mask_ids = []
        mask_ids.extend(one_padding)
        mask_ids.extend(zero_padding)

        maksed_num = len(masked_lm_labels)
        mask_padding = [self.pad_token for _ in range(self.max_predictions_per_seq - maksed_num)]
        masked_lm_positions.extend(mask_padding)
        masked_lm_labels.extend(mask_padding)

        output = {"token_input": tokens,
                  "seg_input": segment_ids,
                  "mask_input": mask_ids,

                  "masked_lm_positions": masked_lm_positions,
                  "masked_lm_labels": masked_lm_labels,
                  
                  "maksed_num" : maksed_num,
                  "is_random_next": is_random_next }

        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
    
    ## load corpus.
    def load_corpus(self, corpus_path):
        all_documents = [[]]
        sent2doc = []
        doc2idx = []

        sent_line = 0
        doc2idx.append(0)
        with open(corpus_path, "r") as reader:
            while True:
                line = reader.readline()
                if not line:
                    break
                line = line.strip().strip('\n')

                if not line:
                    doc2idx.append(sent_line)
                    all_documents.append([])
                    continue
                tokens = line.split(' ')
                tok_ids = []
                #print(line)
                for token in tokens:
                    tok_ids.append(int(token))
                if(tok_ids):
                    all_documents[-1].append(tok_ids)
                    sent2doc.append(len(all_documents) - 1)
                    sent_line += 1
                if(sent_line % 500000 == 0):
                    print('loading sentence', sent_line, 'document num', len(all_documents))
                    #break
        all_documents = [x for x in all_documents if x]
        return (all_documents, sent2doc, doc2idx)

    def truncate_seq_pair(self, tokens_a, tokens_b, max_num_tokens, rng):
        """Truncates a pair of sequences to a maximum sequence length."""
        while True:
            total_length = len(tokens_a) + len(tokens_b)
            if total_length <= max_num_tokens:
                break

            trunc_tokens = tokens_a if len(tokens_a) > len(tokens_b) else tokens_b
            assert len(trunc_tokens) >= 1
            # We want to sometimes truncate from the front and sometimes from the
            # back to add more randomness and avoid biases.
            if rng.random() < 0.5:
                del trunc_tokens[0]
            else:
                trunc_tokens.pop()

    #MaskedLmInstance = collections.namedtuple("MaskedLmInstance", ["index", "label"])
    def create_masked_lm_predictions(self, tokens, masked_lm_prob, max_predictions_per_seq, rng, cls_token, sep_token, msk_token, vocabsize):
        """Creates the predictions for the masked LM objective."""

        cand_indexes = []
        for (i, token) in enumerate(tokens):
            if token == cls_token or token == sep_token:
                continue
            cand_indexes.append(i)

        rng.shuffle(cand_indexes)

        output_tokens = list(tokens)

        num_to_predict = min(max_predictions_per_seq, max(1, int(round(len(cand_indexes) * masked_lm_prob))))

        #masked_lms = []
        masked_lm_positions = []
        masked_lm_labels = []

        covered_indexes = set()
        for index in cand_indexes:
            if len(masked_lm_labels) >= num_to_predict:
                break
            if index in covered_indexes:
                continue
            covered_indexes.add(index)

            masked_token = None
            # 80% of the time, replace with [MASK]
            if rng.random() < 0.8:
                masked_token = msk_token
            else:
                # 10% of the time, keep original
                if rng.random() < 0.5:
                    masked_token = tokens[index]
                # 10% of the time, replace with random word
                else:
                    masked_token = rng.randint(0, vocabsize - 1)

            output_tokens[index] = masked_token

            masked_lm_positions.append(index)
            masked_lm_labels.append(tokens[index])
            #masked_lms.append(MaskedLmInstance(index=index, label=tokens[index]))
        #masked_lms = sorted(masked_lms, key=lambda x: x.index)
        #for p in masked_lms:
        return (output_tokens, masked_lm_positions, masked_lm_labels)

    def random_pair(self, idx):
        self.sample_count += 1
        #if(self.sample_count % 1000 == 0):
        #    print(self.label_stat)
        document_index = self.sent2doc[idx]
        doc_start_idx = self.doc2idx[document_index]
        
        a_idx = idx - doc_start_idx
        document = self.all_documents[document_index]


        # Account for [CLS], [SEP], [SEP]
        max_num_tokens = self.max_seq_length - 3

          # We *usually* want to fill up the entire sequence since we are padding
          # to `max_seq_length` anyways, so short sequences are generally wasted
          # computation. However, we *sometimes*
          # (i.e., short_seq_prob == 0.1 == 10% of the time) want to use shorter
          # sequences to minimize the mismatch between pre-training and fine-tuning.
          # The `target_seq_length` is just a rough target however, whereas
          # `max_seq_length` is a hard limit.
        target_seq_length = max_num_tokens
        if self.rng.random() < self.short_seq_prob:
            target_seq_length = self.rng.randint(2, max_num_tokens)

        current_chunk = []
        current_length = 0

        while a_idx < len(document):
            segment = document[a_idx]
            current_chunk.append(segment)
            current_length += len(segment)
            
            if a_idx == len(document) - 1 or current_length >= target_seq_length:
                if current_chunk:
                    # `a_end` is how many segments from `current_chunk` go into the `A`
                    # (first) sentence.
                    a_end = 1
                    if len(current_chunk) >= 2:
                        a_end = self.rng.randint(1, len(current_chunk) - 1)

                    tokens_a = []
                    for j in range(a_end):
                        tokens_a.extend(current_chunk[j])

                    tokens_b = []
                    # Random next
                    is_random_next = 0
                    if len(current_chunk) == 1 or self.rng.random() < 0.5:
                        is_random_next = 1
                        target_b_length = target_seq_length - len(tokens_a)

                          # This should rarely go for more than one iteration for large
                          # corpora. However, just to be careful, we try to make sure that
                          # the random document is not the same as the document
                          # we're processing.
                        for _ in range(10):
                            random_document_index = self.rng.randint(0, len(self.all_documents) - 1)
                            if random_document_index != document_index:
                                break

                        random_document = self.all_documents[random_document_index]
                        random_start = self.rng.randint(0, len(random_document) - 1)
                        for j in range(random_start, len(random_document)):
                            tokens_b.extend(random_document[j])
                            if len(tokens_b) >= target_b_length:
                                break
                      # We didn't actually use these segments so we "put them back" so
                      # they don't go to waste.
                      #num_unused_segments = len(current_chunk) - a_end
                      #i -= num_unused_segments
                    # Actual next
                    else:
                        is_random_next = 0
                        for j in range(a_end, len(current_chunk)):
                            tokens_b.extend(current_chunk[j])

                    self.truncate_seq_pair(tokens_a, tokens_b, max_num_tokens, self.rng)

                    #assert len(tokens_a) >= 1
                    #assert len(tokens_b) >= 1
                    tokens = []
                    segment_ids = []
                    tokens.append(self.cls_token)
                    segment_ids.append(0)
                    for token in tokens_a:
                        tokens.append(token)
                        segment_ids.append(0)
                    tokens.append(self.sep_token)
                    segment_ids.append(0)

                    for token in tokens_b:
                        tokens.append(token)
                        segment_ids.append(1)
                    tokens.append(self.sep_token)
                    segment_ids.append(1)

                    #tokens,  create_masked_lm_predictions(tokens, masked_lm_prob, max_predictions_per_seq, rng, cls_token, sep_token, msk_token)

                    (tokens, masked_lm_positions,
                     masked_lm_labels) = self.create_masked_lm_predictions(tokens, self.masked_lm_prob, self.max_predictions_per_seq, self.rng, self.cls_token, self.sep_token, self.msk_token, self.vocabsize)
                    
                    self.label_stat[is_random_next] += 1

                    return (tokens, segment_ids, is_random_next, masked_lm_positions, masked_lm_labels)
                #else:
            a_idx += 1


        
