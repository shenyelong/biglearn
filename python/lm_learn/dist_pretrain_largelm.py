import argparse

from MasklmDataset import MasklmDataset
from torch.utils.data import DataLoader
import sys

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar
import os

parser = argparse.ArgumentParser(description='Python call pretrain mask language model.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument('-c', '--train_dataset', required=True, type=str, help='train dataset')

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")
parser.add_argument("--sep_token", type=int, default=102, help="id of [sep]")
parser.add_argument("--pad_token", type=int, default=0, help="id of [pad]")
parser.add_argument("--msk_token", type=int, default=103, help="id of [msk]")
parser.add_argument("--vocabsize", type=int, default=30522, help="vocab size.")

parser.add_argument("-b", "--batch_size", type=int, default=48, help="number of batch_size")
parser.add_argument("-s", "--max_seq_length", type=int, default=256, help="maximum sequence length")
parser.add_argument("-p", "--max_predictions_per_seq", type=int, default=20, help="maximum prediction per sequence")

parser.add_argument("-k", "--masked_lm_prob", type=float, default=0.15, help="short sequence probability")
parser.add_argument("-r", "--short_seq_prob", type=float, default=0.1, help="short sequence probability")

parser.add_argument("-w", "--num_workers", type=int, default=0, help="dataloader worker size")

parser.add_argument("-g", "--gpu_id", type=str, default=0, help="gpu id")

parser.add_argument('--epochs', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,10000:0.0001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')
parser.add_argument('--weight_decay', default=0.01, type=float, help='weight decay parameter')
parser.add_argument('--is_seed', default=0, type=int, metavar='S', help='use seed model or random initialized.')

parser.add_argument('--seed_bert', default='/data1/yelongshen/uncased_L-12_H-768_A-12/', type=str, help='seed bert model.')

parser.add_argument('--output_model', default='bert_CoQA', type=str, help='output model.')

parser.add_argument('--is_ln', default=1, type=int, metavar='L', help='is layer normalization used in transformer.')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior
from BigLearn.DeepNet.Language import LargeBert, DistLargeBert, Bert, DistBert

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

# corpus_path, max_seq_length, max_predictions_per_seq, masked_lm_prob, short_seq_prob, cls_token, sep_token, pad_token, msk_token, vocabsize, rseed = 110):
print("Loading lm Dataset", args.train_dataset)
train_lm = MasklmDataset(corpus_path=args.train_dataset, max_seq_length=args.max_seq_length, max_predictions_per_seq=args.max_predictions_per_seq,
                         masked_lm_prob=args.masked_lm_prob, short_seq_prob=args.short_seq_prob, cls_token=args.cls_token, sep_token=args.sep_token, pad_token=args.pad_token,
                         msk_token=args.msk_token, vocabsize=args.vocabsize)

print("Creating Dataloader")
train_data_loader = DataLoader(train_lm, batch_size=args.batch_size, num_workers=args.num_workers, shuffle=True, drop_last=True)

print("Create bert model and setup enviroument")
#device_id = 0 # int(args.gpu_id)
#env_mode = DeviceBehavior(device_id).TrainMode

layer = 24
if args.is_seed == 0:
    #layer = 6
    model = LargeBert.BertLargeModel(layer, DeviceType.CPU)
else:
    #layer = 12
    model = LargeBert.BertLargeModel(-1, 2, args.seed_bert + 'exp.label', args.seed_bert + 'exp.bin', DeviceType.CPU)

#model.InitOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr), env_mode)
#model.InitOptimizer(StructureLearner.AdamLearner(args.lr), env_mode)

#net = Bert(model, env_mode)
#net.BuildMaskLM(args.batch_size, args.max_seq_length, layer, args.max_predictions_per_seq)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

dist_net = DistLargeBert(model, StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, args.weight_decay, 1.0), deviceNum)
dist_net.BuildMaskLM(args.batch_size, args.max_seq_length, layer, args.max_predictions_per_seq, args.is_ln)  # batchSize, int max_seq_len, int layer, int max_mask_len) #BuildCG(args.batch_size, args.max_seq_length);

class AverageMeter(object):
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

for epoch in range(0, args.epochs):
    print('\nEpoch: [%d | %d] LR: %f' % (epoch + 1, args.epochs, args.lr))
    
    ## set training mode.
    dist_net.SetTrainMode()
    #dist_net.SetPredictMode()

    dist_net.Init()
    
    next_sent_loss = AverageMeter()
    mask_word_loss = AverageMeter()
    next_sent_acc = AverageMeter()

    bar = Bar('training', max=len(train_data_loader))
    
    for batch_idx, data in enumerate(train_data_loader):
        # 0. batch_data will be sent into the device(GPU or cpu)
        #print(batch_idx)
        #print(data)
        data = {key: value for key, value in data.items()}
        input_tokens = data['token_input']
        input_segments = data['seg_input']
        input_mask = data['mask_input']

        mask_lm_position = data['masked_lm_positions']
        mask_lm_labels = data['masked_lm_labels']

        mask_lm_num = data['maksed_num']

        is_random_next = data['is_random_next']

        p_tokens = IntPtr.op_Explicit(Int64(input_tokens.data_ptr()))
        p_segs = IntPtr.op_Explicit(Int64(input_segments.data_ptr()))
        p_msks = IntPtr.op_Explicit(Int64(input_mask.data_ptr()))
        
        p_label = IntPtr.op_Explicit(Int64(is_random_next.data_ptr()))

        p_msk_pos = IntPtr.op_Explicit(Int64(mask_lm_position.data_ptr()))
        p_msk_label = IntPtr.op_Explicit(Int64(mask_lm_labels.data_ptr()))
        p_msk_num = IntPtr.op_Explicit(Int64(mask_lm_num.data_ptr()))

        #print(input_mask)
        #print(is_random_next)
        #print(mask_lm_num)
        #print(mask_lm_position)
        #print(mask_lm_labels)
        #print(input_tokens)

        sloss, wloss, sacc = dist_net.RunMaskLM(p_tokens, p_segs, p_msks, p_msk_pos, p_msk_label, p_msk_num, p_label, args.batch_size) 
        next_sent_loss.update(sloss)
        mask_word_loss.update(wloss)
        next_sent_acc.update(sacc)

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | sent_loss: {sent_loss:.4f} | word_loss: {word_loss:.4f} | sent_acc : {sent_acc:.4f} | avg_sent_loss: {avg_sent_loss:.4f} | avg_word_loss: {avg_word_loss:.4f} | avg_sent_acc : {avg_sent_acc:.4f} '.format(
                    batch=batch_idx + 1,
                    size=len(train_data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    sent_loss=sloss,
                    word_loss=wloss,
                    sent_acc=sacc,
                    avg_sent_loss=next_sent_loss.avg,
                    avg_word_loss=mask_word_loss.avg,
                    avg_sent_acc=next_sent_acc.avg
                    )
        bar.next()
        #print(sent_loss)
        #print(word_loss)
        #break
        #sent_label = data['is_next']      
    
    #break
    bar.finish()
    dist_net.Complete()
    dist_net.SaveModel(args.output_model + '.' + str(epoch) + '.model')
