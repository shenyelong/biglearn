from torch.utils.data import Dataset
import tqdm
import torch
import random
import collections

class InputFeatures(object):
    """A single set of features of data."""
    def __init__(self, input_ids, input_mask, segment_ids, masked_lm_positions, masked_lm_ids, masked_lm_weights, next_sentence_labels):
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.masked_lm_positions = masked_lm_positions
        self.masked_lm_ids = masked_lm_ids
        self.masked_lm_weights = masked_lm_weights
        self.next_sentence_labels = next_sentence_labels

class MasklmV2Dataset(Dataset):
    def __init__(self, corpus_path, max_seq_length, max_predictions_per_seq):
        self.corpus_path = corpus_path
        self.max_seq_length = max_seq_length
        self.max_predictions_per_seq = max_predictions_per_seq

        self.features = self.read_features(corpus_path)

        self.corpus_lines = len(self.features)

        print('all training examples', self.corpus_lines)

    def __len__(self):
        return self.corpus_lines

    def __getitem__(self, item):
        #.   input_ids=parse2ints(fields[0]),
        #    input_mask=parse2ints(fields[1]),
        #    segment_ids=parse2ints(fields[2]),
        #    masked_lm_positions=parse2ints(fields[3]),
        #    masked_lm_ids=parse2ints(fields[4]),
        #    masked_lm_weights=parse2floats(fields[5]),
        #    next_sentence_labels=int(fields[6])))
        output = {"input_ids": self.features[item].input_ids,
                  "input_mask": self.features[item].input_mask,
                  "segment_ids": self.features[item].segment_ids,
                  "masked_lm_positions": self.features[item].masked_lm_positions,
                  "masked_lm_ids": self.features[item].masked_lm_ids,
                  "masked_lm_weights": self.features[item].masked_lm_weights,
                  "next_sentence_labels": self.features[item].next_sentence_labels }

        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
    
    def parse2ints(self, field):
        tokens = field.split(' ')
        tok_ids = []
        for token in tokens:
            tok_ids.append(int(token))
        return tok_ids

    def parse2floats(self, field):
        tokens = field.split(' ')
        tok_ids = []
        for token in tokens:
            tok_ids.append(int(float(token)))
        return tok_ids

    def read_features(self, corpus_path):
        """Read a list of `InputExample`s from an input file."""
        mf = []
        with open(corpus_path, "r") as reader:
            while True:
                line = reader.readline()
                if not line:
                    break
                fields = line.strip().split('\t')
                mf.append(InputFeatures(input_ids=self.parse2ints(fields[0]), input_mask=self.parse2ints(fields[1]), segment_ids=self.parse2ints(fields[2]), masked_lm_positions=self.parse2ints(fields[3]), masked_lm_ids=self.parse2ints(fields[4]), masked_lm_weights=self.parse2floats(fields[5]), next_sentence_labels=int(fields[6])))
        return mf



        
