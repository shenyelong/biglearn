import io
import sys
import clr
sys.path.append('/home/yelongshen/biglearn/biglearn/Targets')
clr.AddReference('BigLearn')
clr.AddReference('BigLearn.DeepNet')

import System
import BigLearn
import BigLearn.DeepNet
import BigLearn.DeepNet.Image
from BigLearn.DeepNet.Image import VGG11Net
from System import Array
import numpy

from BigLearn import DeviceType
from BigLearn import DNNRunMode
from BigLearn import DeviceBehavior
from BigLearn import RunnerBehavior

behavior = DeviceBehavior(0)
print('set up behavior')

vgg11 = VGG11Net()

print(vgg11.name)

x = numpy.array([1.0,2.0,3.0,4.0,5.0])
ix = numpy.array([1,2,3,4,5,6,7])
#cx = Array[float](x)
r1 = vgg11.Sum(x, 0, 5)
r2 = vgg11.Sum(x, 1, 4)
r3 = vgg11.SumInt(ix, 0, 5)
print(r1)
print(r2)
print(r3)

#cg = BigLearn.ComputationGraph();
#print(len(cg.Session))

#print('load bigLearn')
