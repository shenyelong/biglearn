from __future__ import division
from torchvision import models
from torchvision import transforms
from PIL import Image

import argparse
import torch
import torchvision
import torch.nn as nn
import numpy as np

import sys
import os
import clr as net_clr
from System import Array, IntPtr, Int32, Int64

parser = argparse.ArgumentParser(description='VGG16 Neural Style Transformer.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')

parser.add_argument('--content', type=str, default='png/content.png')
parser.add_argument('--style', type=str, default='png/style.png')
parser.add_argument('--max_size', type=int, default=400)
parser.add_argument('--total_step', type=int, default=2000)
parser.add_argument('--log_step', type=int, default=10)
parser.add_argument('--sample_step', type=int, default=500)
parser.add_argument('--style_weight', type=float, default=100.0)
parser.add_argument('--lr', type=float, default=0.003)
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")
parser.add_argument("--seed_model", type=str, default="seed_model", help="seed model")

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior
from BigLearn import Tensor4DData, HiddenBatchData
from BigLearn.DeepNet.Image import ImageNet_VGG16Net, DistImageNet_VGG16Net

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create enviroument")
env_mode = DeviceBehavior(0).TrainMode

def main():
    transform = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.458, 0.408], std=[1.0, 1.0, 1.0]),
        ])

    content_image = Image.open(args.content)
    content = transform(content_image)
    p_content = IntPtr.op_Explicit(Int64(content.data_ptr()))

    style_image = Image.open(args.style)
    style = transform(style_image)
    p_style = IntPtr.op_Explicit(Int64(style.data_ptr()))

    # Initialize a target image with the content image
    target = content.clone()
    p_target = IntPtr.op_Explicit(Int64(target.data_ptr()))

    # load the pre-trained vgg model.
    model = ImageNet_VGG16Net.VGG16Model(1000, args.seed_model + '.label', args.seed_model + '.bin', env_mode.Device)

    select_features = ['conv-1-1', 'conv-2-1','conv-3-1', 'conv-4-1', 'conv-5-1'] # 'conv-2-1','conv-3-1', 'conv-4-1', 'conv-1-1',
    feature_num = len(select_features)

    content_net = ImageNet_VGG16Net(model, env_mode)
    content_net.BuildFeatureGraph(224, 224, 3, select_features, feature_num)
    content_features = content_net.ExtractContentFeature(p_content)

    style_net = ImageNet_VGG16Net(model, env_mode)
    style_net.BuildFeatureGraph(224, 224, 3, select_features, feature_num)
    style_features = style_net.ExtractStyleFeature(p_style)

    transfer_net = ImageNet_VGG16Net(model, env_mode)
    transfer_net.BuildTransferGraph(224, 224, 3, select_features, content_features, style_features, args.style_weight, feature_num)
    transfer_net.SetStyleOptimizer(StructureLearner.AdamLearner(args.lr, 0.5, 0.999))

    #optimizer = torch.optim.Adam([target], lr=config.lr, betas=[0.5, 0.999])
    #vgg = VGGNet().to(device).eval()
    transfer_net.SetTrainMode()
    transfer_net.Init()
    for step in range(0, args.total_step):
        mloss = transfer_net.StyleTransfer(p_target)
        content_loss = mloss[0]
        style_loss = mloss[1]

        if (step+1) % args.log_step == 0:
            print ('Step [{}/{}], Content Loss: {:.4f}, Style Loss: {:.4f}'.format(step+1, args.total_step, content_loss, style_loss))

        if (step+1) % args.sample_step == 0:
            # Save the generated image # [123.68, 116.78, 103.94]
            denorm = transforms.Normalize((-0.485, -0.458, -0.408), (1, 1, 1))
            img = target.clone()
            img = denorm(img).clamp_(0, 1)
            torchvision.utils.save_image(img, 'output-{}.png'.format(step+1))

    transfer_net.Complete()

if __name__ == "__main__":
    main()