import argparse
import json
import re
import time
import string
from collections import Counter
from collections import OrderedDict
from pycorenlp import StanfordCoreNLP

import tokenization
import re

import multiprocessing as mp
import codecs

def dump_dureader_examples(input_file, output_file):
    """
    read a Squad json file into a list of QA examples
    """
    with open(input_file, "r", encoding='utf-8') as reader:
        input_data = json.load(reader)['data']

    print(len(input_data))
    
    with codecs.open(output_file, "w", encoding="utf-8") as writer:

    	for entry in input_data:
            # process story text
            story = entry["story"].replace('\t', ' ')

            qa = entry['qa'][0]
           
            qid = qa['id']
            query = qa['raw_question'].replace('\t',' ')
            ans = qa['answer'].replace('\t', ' ')
            writer.write(str(qid)+'\t'+story+'\t'+query+'\t'+ans+'\n')

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--world', type=int, required=True)
    parser.add_argument('--duplicate', type=int, required=True)
    parser.add_argument('--input_file', type=str, required=True)

    args = parser.parse_args()
    
    writers = []
    for i in range(0, args.world):
        writers.append( codecs.open(args.input_file + '.rank.' + str(i), "w", encoding="utf-8") )

    widx = 0
    with codecs.open(args.input_file, "r", encoding="utf-8") as reader:
        for line in reader:
            for d in range(0, args.duplicate):
                writers[widx].write(line)
                widx = widx + 1
                widx = widx % args.world

    for i in range(0, args.world):
        writers[i].close()