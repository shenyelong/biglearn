import argparse
import json
import re
import time
import string
from collections import Counter
from collections import OrderedDict
from pycorenlp import StanfordCoreNLP

import tokenization
import re

import multiprocessing as mp
import codecs

def dump_dureader_examples(input_file, output_file):
    """
    read a Squad json file into a list of QA examples
    """
    with open(input_file, "r", encoding='utf-8') as reader:
        input_data = json.load(reader)['data']

    print(len(input_data))
    
    with codecs.open(output_file, "w", encoding="utf-8") as writer:

    	for entry in input_data:
            # process story text
            story = entry["story"].replace('\t', ' ')

            qa = entry['qa'][0]
           
            qid = qa['id']
            query = qa['raw_question'].replace('\t',' ')
            ans = qa['answer'].replace('\t', ' ')
            writer.write(str(qid)+'\t'+story+'\t'+query+'\t'+ans+'\n')

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    args = parser.parse_args()

    dump_dureader_examples(args.input_file, args.output_file)
