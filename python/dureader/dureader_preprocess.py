"""
preprocess CoQA for extractive question answering
2019/1/22: distinguish yes-no and wh- question
"""

import argparse
import json
import re
import time
import string
from collections import Counter
from collections import OrderedDict
from pycorenlp import StanfordCoreNLP

import tokenization
import re

import multiprocessing as mp


#nlp = StanfordCoreNLP('http://localhost:9000')
#UNK = 'unknown'

def _str(s):
    """ Convert PTB tokens to normal tokens """
    if (s.lower() == '-lrb-'):
        s = '('
    elif (s.lower() == '-rrb-'):
        s = ')'
    elif (s.lower() == '-lsb-'):
        s = '['
    elif (s.lower() == '-rsb-'):
        s = ']'
    elif (s.lower() == '-lcb-'):
        s = '{'
    elif (s.lower() == '-rcb-'):
        s = '}'
    return s

#def process(text):
#    paragraph = nlp.annotate(text, properties={
#                             'annotators': 'tokenize, ssplit',
#                             'outputFormat': 'json',
#                             'ssplit.newlineIsSentenceBreak': 'two'})
#    output = {'word': [], 'offsets': []}
#    try:
#        for sent in paragraph['sentences']:
#            for token in sent['tokens']:
#                output['word'].append(_str(token['word']))
#                output['offsets'].append((token['characterOffsetBegin'], token['characterOffsetEnd']))
#    except:
#        print("error in line 45: {}".format(paragraph))
#    return output

def is_whitespace(c):
    if c == " " or c == "\t" or c == "\r" or c == "\n" or ord(c) == 0x202F:
        return True
    return False

def is_control(char):
    """Checks whether `chars` is a control character."""
    # These are technically control characters but we count them as whitespace characters.
    if char == "\t" or char == "\n" or char == "\r":
        return False
    #cat = unicodedata.category(char)
    #if cat.startswith("C"):
    #    return True
    return False

def is_alphabeta(c):
    if(ord(c) >= ord('a') and ord(c) <= ord('z')):
        return True
                
    if(ord(c) >= ord('A') and ord(c) <= ord('Z')):
        return True

    if(ord(c) >= ord('0') and ord(c) <= ord('9')):
        return True

    if(c == '-' ):
        return True

    if( c == ':'):
        return True

    if( c == '/'):
        return True

    if( c == '@'):
        return True
    
    if ( c== '.'):
        return True

    return False

def process(text):
    output = {'word': [], 'offsets': []}
    try:
        c_idx = 0

        is_character = False
        character_start = 0

        for c in text:
            #print(c)

            c_idx += 1
            
            if(is_character):
                if(is_alphabeta(c)):
                    continue
                else:
                    output['word'].append(text[character_start : c_idx - 1].lower())
                    output['offsets'].append((character_start, c_idx - 1))
                    is_character = False
            else:
                if(is_alphabeta(c)):
                    is_character = True
                    character_start = c_idx - 1
                    continue
            
            if(is_whitespace(c)): # or is_control(c)):
                continue

            output['word'].append(c.lower())
            output['offsets'].append((c_idx - 1, c_idx))

        if(is_character):
            output['word'].append(text[character_start : c_idx].lower())
            output['offsets'].append((character_start, c_idx))
    except:
        print("error in line 65: {}".format(text))

    #print(output['word'])
    return output


def normalize_answer(s):
    """Lower text and remove punctuation, storys and extra whitespace."""
    def remove_articles(text):
        regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
        return re.sub(regex, ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))
    #return white_space_fix(remove_punc(lower(s)))

def find_span_with_gt(context, context_token, offsets, char_to_segments, ground_truth, w_start = -1, w_end = -1):
    """
    find answer from paragraph so that best f1 score is achieved
    """
    best_f1 = 0.0
    best_span = (len(context)-1, len(context)-1)
    best_pred = ''
    gt = ground_truth #[c.lower() for c in (ground_truth)] #.split()

    if(w_start == -1):
        w_start = 0
    if(w_end == -1):
        w_end = len(offsets)

    ls = [i for i in range(w_start, w_end) # len(offsets))
          if context_token[i] in gt]

    gtc = Counter(gt)
    #print('candidate ls length', len(ls))
    for i in range(len(ls)):
        for j in range(i, len(ls)):

            char_pos_start = offsets[ls[i]][0]
            char_pos_end = offsets[ls[j]][1]-1

            if(char_to_segments[char_pos_start] == -1 or char_to_segments[char_pos_end] == -1):
                break

            if(char_to_segments[char_pos_start] != char_to_segments[char_pos_end]):
                break

            pred = context_token[ls[i] : ls[j] + 1] #[c for c in (context[offsets[ls[i]][0]: offsets[ls[j]][1]])] #.split()
            common = Counter(pred) & gtc
            num_same = sum(common.values())
            if num_same > 0:
                precision = 1.0 * num_same / len(pred)
                recall = 1.0 * num_same / len(gt)
                f1 = (2 * precision * recall) / (precision + recall)
                if f1 > best_f1:
                    best_f1 = f1
                    best_span = (ls[i], ls[j]) #(offsets[ls[i]][0], offsets[ls[j]][1])
                    best_pred = context[offsets[ls[i]][0]: offsets[ls[j]][1]]
    return best_span, best_f1, best_pred

def f1Score(gt, pred):
    norm_gt = normalize_answer(gt).split()
    norm_pred = normalize_answer(pred).split()

    common = Counter(norm_pred) & Counter(norm_gt)
    num_same = sum(common.values())
    if num_same > 0:
        precision = 1.0 * num_same / len(norm_pred)
        recall = 1.0 * num_same / len(norm_gt)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1
    return 0

cleanr = re.compile('<.*?>')

def cleanhtml(raw_html):
    global cleanr    
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def preprocess(example, tokenizer, doc_idx, all_idx):

    if(all_idx % 100 == 0):
        print('preprocessing ...',all_idx)

    question = example['question']
    ans_list = example['answers']

    q_type = example['question_type']
    q_id = example['question_id']
    fact_or_option = example['fact_or_opinion']
    
    doc = example['documents'][doc_idx]

    question_tokens = tokenizer.tokenize(question)
    question_idx = tokenizer.convert_tokens_to_ids(question_tokens)

    annotated_ans_str = [process(ans_str) for ans_str in ans_list]
    ans_words = [ x['word'] for x in annotated_ans_str ]


    is_selected = doc['is_selected']
    title = doc['title']
    paragraphs = [cleanhtml(para) for para in doc['paragraphs']]

    doc_text = title + ' # ' + ' # '.join(paragraphs)

    

    char_to_segments = []
    #doc_segments = []
    #doc_segments.append((0, len(title)))
    char_to_segments.extend([0 for i in range(len(title))])
 
    #segend = len(title)
    seg_idx = 1
    for para in paragraphs:
        char_to_segments.extend([-1, -1, -1])
        char_to_segments.extend([seg_idx for i in range(len(para))])
        #doc_segments.append((segend + 3, segend + 3 + len(para)))
        #segend = segend + 3 + len(para)
        seg_idx += 1

    assert len(char_to_segments) == len(doc_text)

    annotated_doc_str = process(doc_text)
    doc_offsets = annotated_doc_str['offsets']
    doc_words = annotated_doc_str['word']
    subtokens, word2subtoken = tokenizer.tokenize_words(doc_words)
    subtokens_idx = tokenizer.convert_tokens_to_ids(subtokens)

    char_to_word_offset = []

    widx = 0
    for c_story in range(0, len(doc_text)):
        if (widx + 1 >= len(doc_offsets)):
            char_to_word_offset.append(widx)
            continue

        if (c_story < doc_offsets[widx][0]):
            char_to_word_offset.append(widx)
            continue

        if (c_story >= doc_offsets[widx][0] and c_story < doc_offsets[widx][1]):
            char_to_word_offset.append(widx)
            continue

        if (c_story >= doc_offsets[widx][1]):
            if(len(doc_offsets) > widx + 1):
                char_to_word_offset.append(widx + 1)
            else:
                char_to_word_offset.append(widx)
            widx = widx + 1
            continue

    doc_word_num = len(doc_words)
    assert doc_offsets[doc_word_num - 1][1] <= len(doc_text)


    #best_f1 = 0.0
    span_word_start = -1
    span_word_end = -1

    best_pred = ''
    best_answer = ''
    best_f1 = 0.0

    if(is_selected):
        #print('ans list', len(ans_list))
        #print('document length', len(doc_offsets))
        for gt in ans_words:
            b_span, b_f1, b_pred = find_span_with_gt(doc_text, doc_words, doc_offsets, char_to_segments, gt)
            if(b_f1 > best_f1):
                best_f1 = b_f1
                best_span = b_span
                best_pred = b_pred
                best_answer = gt

        if(best_f1 == 0.0):
            span_word_start = -1
            span_word_end = -1
            print(doc_text)
            print(doc_words)
            print(ans_words)
            unk = True
        else:
            span_word_start = best_span[0] #char_to_word_offset[best_span[0]]
            span_word_end = best_span[1] #char_to_word_offset[best_span[1]-1]
        
        #print('doc text', doc_text)
        #print('ground truth', best_answer)
        #print('best pred', best_pred)
        #print('best f1', best_f1)
        unk = False
    #    if(best_f1 <= 0.1):
    #        print('f1 score is very low', best_f1)
    #        unk = True
    else:
        unk = True

    _datum = OrderedDict()
    _datum["story"] = doc_text

    _datum["story_subtoken_idx"] = subtokens_idx

    # word 2 token
    _datum["story_word2subtoken"] = word2subtoken

    # word 2 char
    _datum["story_wordoffset"] = doc_offsets

    _datum["qa"] = []

    _qa = OrderedDict()
    _qa["id"] = str(q_id) + '_doc_' + str(doc_idx)
    _qa["question"] = question_idx

    _qa["start"] = span_word_start
    _qa["end"] = span_word_end

    _qa["unk"] = unk
    _qa["f1"] = best_f1
    _qa["answer"] = best_answer
    _qa["word_answer"] = best_pred

    _datum["qa"].append(_qa)

    return _datum
#def inc(x, y):
#    return x + y

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    parser.add_argument("--vocab_file", default=None, type=str, help="tokenizer vocab file.")
    parser.add_argument('--rank', default=0, type=int, help='rank of the world dataset.')
    parser.add_argument('--world', default=1, type=int, help='split the whole dataset into several parts')
    args = parser.parse_args()

    tokenizer = tokenization.FullTokenizer(args.vocab_file) 

    print('loading json files...')
    examples = []
    line_idx = 0
    with open(args.data_file, 'r') as f:
        for line in f:
            examples.append(json.loads(line))
            line_idx += 1
            if(line_idx % 100 == 0):
                print('loading json file idx', line_idx)
                #break

    qa_samples = []

    query_idx = 0
    doc_idx = 0
    selected_doc = 0
    all_idx = 0
    for example in examples:
        query_idx += 1
        doc_list = example['documents']
        doc_idx += len(doc_list)

        i = 0
        for doc in doc_list:
            is_selected = doc['is_selected']
            if(is_selected):
                selected_doc += 1

            qa_samples.append((example, i, all_idx))
            all_idx = all_idx + 1
            i = i + 1

    print('query number : ', query_idx, 'document number : ', doc_idx, 'selected doc number : ', selected_doc)

    parts = int((all_idx + args.world - 1) / args.world)

    s_offset = args.rank * parts
    s_end = (args.rank + 1) * parts
    
    print('document rank range: ', s_offset, s_end)

    qa_samples = qa_samples[s_offset : s_end]

    pool = mp.Pool(mp.cpu_count())

    print('mp cpu count ', mp.cpu_count())

    datums = pool.starmap(preprocess, [ (x[0], tokenizer, x[1], x[2]) for x in qa_samples ])

    data = OrderedDict()
    data['data'] = datums

    with open(args.output_file, 'w') as output_file:
        json.dump(data, output_file, ensure_ascii=False, sort_keys=True, indent=4)




