from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import json
import math

import pickle
import numpy as np

import os
import tempfile
import argparse

import tqdm
from tqdm import tqdm, trange

import random
import collections

import torch
from torch.utils.data import Dataset
import tokenization


class YelpDataset2(Dataset):
    ## corpus path, appending queries into history.
    def __init__(self, corpus_path, vocab_file, max_seq_length, rseed = 110):
        self.tokenizer = tokenization.FullTokenizer(vocab_file) 

        self.token_ids = []
        self.label = []
        self.max_seq_length = max_seq_length

        with open(corpus_path, 'r') as f:
            for line in f:
                line = line.strip()
                items = line.split('\t')
                _text = tokenization.convert_to_unicode(items[0])
                _label = int(items[1])
                
                _tokens = self.tokenizer.tokenize(_text)
                _token_ids = self.tokenizer.convert_tokens_to_ids(_tokens)
                if(len(_token_ids) == 0):
                    continue
                self.token_ids.append(_token_ids)
                self.label.append(_label)
                #if(len(self.token_ids) >= 100000):
                #    break
        print('\nCorpus Size %d ' % len(self.token_ids))

    def vocab_size(self):
        return len(self.tokenizer.vocab)

    def __len__(self):
        return len(self.token_ids)

    def __getitem__(self, item):
        #if(self.is_enumate_doc_span):
        #    (p_id, turn_id, doc_word_num, doc_word_offset, doc_start, doc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans) = self.doc_span_sample(item)
        #else:
        max_target_len = self.max_seq_length - 2

        ids = self.token_ids[item]
        l = self.label[item]


        if(len(ids) > max_target_len):
            ids = ids[ : max_target_len]

        ids = [101] + ids + [102]
        segments = [0] * len(ids)
        masks = [1] * len(ids)

        tok_len = len(ids)
        if(tok_len < self.max_seq_length):
            padding = [0 for _ in range(self.max_seq_length - tok_len)]
            ids.extend(padding)
            segments.extend(padding)
            masks.extend(padding)

        assert len(ids) == self.max_seq_length

        output = {"tokens" : ids,
                  "segments" : segments,
                  "masks" : masks,
                  "tok_len" : tok_len,
                  "labels"  : l }
        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
