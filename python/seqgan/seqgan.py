from __future__ import print_function
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data

from YelpDataset import YelpDataset
from torch.utils.data import DataLoader

import numpy

import io
import sys

## first pretrain the generator and discriminator
parser = argparse.ArgumentParser()

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='storm lib path')

parser.add_argument('--gpu_id', type=str, default='0', help='gpu ids to use.')

parser.add_argument('--dataset', type=str, help='training dataset.')
parser.add_argument('--vocab', type=str, help='vocab file.')
parser.add_argument('--max_seq_length', type=int, default=256, help="maximum sequence length")

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")

parser.add_argument('--workers', type=int, help='number of data loading workers', default=0)
parser.add_argument('--batch_size', type=int, default=64, help='input batch size')

parser.add_argument('--niter', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')

parser.add_argument('--sched_temp', type=str, default='0:0.3,30000:0.7', help='schedule temperature for gumbel softmax')
parser.add_argument('--output', type=str, default='/data2/yelongshen/seqgan', help='data model output folder')

parser.add_argument('--in_dim', type=int, default=300, help='word embedding dim')
parser.add_argument('--hid_dim', type=int, default=300, help='rnn hidden dimension')
parser.add_argument('--latent_code', type=int, default=300, help='latent code dimension')

parser.add_argument('--layer', type=int, default=2, help='rnn layers')

parser.add_argument('--pre_gnet', type=str, default='', help='pretrained generator net.')

#parser.add_argument('--pre_dnet_epoch', type=int, default=0, help='discriminator pre training.')
#parser.add_argument('--pre_dnet_lr', type=float, default=0.0001, help='discriminator pre training learning rate.')

args = parser.parse_args()
print(args)

import clr as net_clr
from System import Array, IntPtr, Int32, Int64, Collections
from progress.bar import Bar as Bar

# /10T/yelongshen/biglearn/Targets
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, A_Func, IntArgument, RateScheduler, ResourceManager

from BigLearn import Session, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure

from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData

from BigLearn import GradientOptimizer

#from BigLearn.DeepNet.Language import LargeBert, Bert, DistBert

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create storm enviroument")
env_mode = DeviceBehavior(0).TrainMode


class SeqGanModule:
    # dual module for Generator and Discriminator 
    def __init__(self, vocab_size, in_dim, hid_dim, layer, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)

        out_dims = [ hid_dim for _ in range(layer) ]

        self.Models = {}

        self.Models['embed'] = self.Session.Model.AddLayer(EmbedStructure(vocab_size, in_dim, self.Behavior.Device))

        self.Models['dis_lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        self.Models['dis_classifier'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, 2, A_Func.Linear, True, self.Behavior.Device))

        self.Models['gen_lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        self.Models['gen_decoder'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, in_dim, A_Func.Linear, True, self.Behavior.Device))

        self.Session.Model.AllocateGradient(self.Behavior.Device)

## create Generator model. 
class Generator:
    def __init__(self, models, hid_dim, max_seq_length, batch_size, rate_sched, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        batch = IntArgument('batch', batch_size)
        dim = IntArgument('dim', hid_dim)

        self.Noise = NdArrayData(behavior.Device, False, dim, batch) 

        init_tokens = [101 for _ in range(batch_size)]
        embed = self.Session.LookupEmbed(self.Models['embed'], init_tokens)

        layer = self.Models['gen_lstms'].Layer
        _pc = [self.Noise for _ in range(layer)]
        _po = [self.Noise for _ in range(layer)]

        self.MaxSeq = max_seq_length

        self.decode_tokens = []
        self.decode_b_logit = []

        for i in range(max_seq_length):
            _out_lstm = self.Session.LSTMState(self.Models['gen_lstms'], _po, _pc, embed)
            _po = _out_lstm.Item1
            _pc = _out_lstm.Item2

            embed = _po[layer - 1]

            decode = self.Session.FNN(embed, self.Models['gen_decoder'])
            logit = self.Session.MatMul(decode, 0, self.Models['embed'].NDEmbed, 1)
            _out_gumbel = self.Session.GumbelSoftmax(logit, rate_sched, True)
            b_logit = _out_gumbel.Item1
            tok_ids = _out_gumbel.Item2

            self.decode_b_logit.append(b_logit)
            self.decode_tokens.append(tok_ids)

            tok_embed = self.Session.MatMul(b_logit, 0, self.Models['embed'].NDEmbed, 0)
            embed = tok_embed

        # self.gen_tok_embed = self.Session.Cat(self.decode_embed, 0)
        ## 
        self.Session.Model.AddLayer(self.Models['embed'])
        self.Session.Model.AddLayer(self.Models['gen_lstms'])
        self.Session.Model.AddLayer(self.Models['gen_decoder'])
        self.Session.AllocateOptimizer(StructureLearner.AdamLearner(args.lr, 0.5, 0.999))

        #self.Session.AddOptimizer(self.Models['embed'].ModelOptimizers)
        #self.Session.AddOptimizer(self.Models['gen_lstms'].ModelOptimizers)
        #self.Session.AddOptimizer(self.Models['gen_decoder'].ModelOptimizers)

    def Forward(self, noise, batch_size, dim):
        self.Noise.Output.CopyTorch(IntPtr.op_Explicit(Int64(noise.data_ptr())),  batch_size * dim)
        #print(noise)
        self.Session.ForwardV3()

        tok_ids = torch.zeros([batch_size, self.MaxSeq], dtype = torch.int32)
        tok_len = torch.zeros([batch_size], dtype = torch.int32)

        for i in range(self.MaxSeq):
            self.decode_tokens[i].SyncToCPU()
            for b in range(batch_size):
                if(self.decode_tokens[i][b] == 0 and tok_len[b] == 0):
                    tok_len[b] == i + 1

                if(tok_len[b] == 0):
                    tok_ids[b][i] = self.decode_tokens[i][b]
                else:
                    tok_ids[b][i] = 0

        for b in range(batch_size):
            if(tok_len[b] == 0):
                tok_len[b] = self.MaxSeq

        return tok_ids, tok_len
        ##
    def Update(self):
        self.Session.BackwardV3()
        self.Session.StepV3Update()

            ##
class Classifier:
    def __init__(self, models, in_dim, max_seq_length, batch_size, behavior):

        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.token_lens = CudaPieceInt(batch_size, self.Behavior.Device)
        self.labels = CudaPieceInt(batch_size, self.Behavior.Device)

        self.batch = IntArgument('batch', batch_size)
        self.seq = IntArgument('seq', max_seq_length)
        self.dim = IntArgument('dim', in_dim)

        token_embed = self.Session.LookupEmbed(self.Models['embed'], self.tokens)
        token_embed = self.Session.Reshape(token_embed, self.dim, self.seq, self.batch)
        token_seq_data = self.Session.Tensor2SeqData(token_embed, self.token_lens)
        self.lstm_embed = self.Session.LSTM(self.Models['dis_lstms'], token_seq_data)
        self.pred = self.Session.FNN(self.lstm_embed, self.Models['dis_classifier'])
        self.loss = self.Session.SoftmaxLoss(self.pred, self.labels)        

        self.Session.Model.AddLayer(self.Models['embed'])
        self.Session.Model.AddLayer(self.Models['dis_lstms'])
        self.Session.Model.AddLayer(self.Models['dis_classifier'])
        self.Session.AllocateOptimizer(StructureLearner.AdamLearner(args.lr, 0.5, 0.999))
        
    def Train(self, real_token_id, real_token_len, real_batch, fake_token_id, fake_token_len, fake_batch):
        tok_ids = torch.cat((real_token_id, fake_token_id), 0)
        tok_lens = torch.cat((real_token_len, fake_token_len), 0)

        real = torch.ones([real_batch], dtype=torch.int32)
        fake = torch.zeros([fake_batch], dtype=torch.int32)
        label = torch.cat((real, fake), 0)

        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tok_ids.data_ptr())), (real_batch + fake_batch) * self.seq.Default)
        self.token_lens.CopyTorch(IntPtr.op_Explicit(Int64(tok_lens.data_ptr())),  (real_batch + fake_batch))
        self.labels.CopyTorch(IntPtr.op_Explicit(Int64(label.data_ptr())), (real_batch + fake_batch))

        self.Session.StepV3Run()
        self.Session.StepV3Update()
        
        return self.loss.Value

# assume it is an classification task first.
class Discriminator:
    ## construct model 
    def __init__(self, models, fake_tok_logits, vocab_size, in_dim, max_seq_length, batch_size, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = models

        self.labels = CudaPieceInt(batch_size, self.Behavior.Device)
        self.labels.Init(1)

        self.token_lens = CudaPieceInt(batch_size, self.Behavior.Device)

        batch = IntArgument('batch', batch_size)
        seq = IntArgument('seq', max_seq_length)
        vocab = IntArgument('vocab', vocab_size)
        embed = IntArgument('embed', in_dim)

        batch_seq = IntArgument('seq', max_seq_length * batch_size)
        logit_concate = self.Session.Cat(fake_tok_logits, 0)
        logit_concate = self.Session.Reshape(logit_concate, vocab, batch_seq)

        fake_tok_embed = self.Session.MatMul(logit_concate, 0, self.Models['embed'].NDEmbed.NoGrad(), 0)
        
        fake_tok_embed = self.Session.Reshape(fake_tok_embed, embed, seq, batch)


        token_seq_data = self.Session.Tensor2SeqData(fake_tok_embed, self.token_lens)
        
        self.lstm_embed = self.Session.LSTM(self.Models['dis_lstms'], token_seq_data, False)
        
        # FNN(NdArrayData data, NdArrayData weight, NdArrayData bias, A_Func af)
        self.pred = self.Session.FNN(self.lstm_embed, self.Models['dis_classifier'].NDWeight.NoGrad(), self.Models['dis_classifier'].NDBias.NoGrad(), self.Models['dis_classifier'].Af)
        
        self.loss = self.Session.SoftmaxLoss(self.pred, self.labels)        

    ## back-prop gradient to fake_tok_embed.
    def Train(self, tok_len, batch_size): 
        self.token_lens.CopyTorch(IntPtr.op_Explicit(Int64(tok_len.data_ptr())), batch_size)
        #self.Models['dis_lstms'].NoGrad = True
        #print(tok_len)
        self.Session.StepV3Run()
        #self.Models['dis_lstms'].NoGrad = False
        return self.loss.Value


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


print('loading dataset ...', args.dataset)
dataset = YelpDataset(args.dataset, args.vocab, args.max_seq_length)
data_loader = DataLoader(dataset, batch_size=args.batch_size, num_workers=args.workers, shuffle=True, drop_last=True)

vocab_size = len(dataset.tokenizer.vocab)
print('vocab size', vocab_size)

global_update = IntArgument("global_update", 0)
sched = RateScheduler(global_update, args.sched_temp)

seqGan = SeqGanModule(vocab_size, 300, 300, 2, env_mode)
#seqGan.Session.InitOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))

# max_seq_length, batch_size, rate_sched, behavior):
#models, hid_dim, max_seq_length, batch_size, rate_sched, behavior):
gnet = Generator(seqGan.Models, 300, args.max_seq_length, args.batch_size, sched, env_mode)

if(args.pre_gnet != ''):
    gnet.Session.Model.Load(args.pre_gnet)

classifier = Classifier(seqGan.Models, 300, args.max_seq_length, args.batch_size * 2, env_mode) 

# models, fake_tok_logits, vocab_size, embed_dim, max_seq_length, batch_size, behavior):
dnet = Discriminator(seqGan.Models, gnet.decode_b_logit, vocab_size, 300, args.max_seq_length, args.batch_size, env_mode)

fixed_noise = torch.randn(args.batch_size, 300)

def generation_gnet(gnet, fixed_noise, batch_size, tokenizer, fname):
    
    gnet.Session.SetPredictMode()

    fake_tok_ids, fake_tok_len = gnet.Forward(fixed_noise, batch_size, 300)
    
    #out_file = open(args.output + '/yelp.seqgan-'+str(epoch)+'.txt','w')
    out_file = open(fname,'w')
    for b in range(batch_size):
        target_tokens = tokenizer.convert_ids_to_tokens(fake_tok_ids[b].numpy())
        out_file.write(' '.join(target_tokens)+'\n')
    out_file.close()

def train_classifier(data_loader, gnet, classifier):
    bar = Bar('Training classifier', max=len(data_loader))

    gnet.Session.SetTrainMode()
    classifier.Session.SetTrainMode()
    
    dis_losses = AverageMeter()

    for batch_idx, data in enumerate(data_loader):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        tok_ids = data['tok_ids']
        tok_len = data['tok_len']
        label = data['label']

        noise = torch.randn(args.batch_size, 300)

        fake_tok_ids, fake_tok_len = gnet.Forward(noise, args.batch_size, 300)

        d_loss = classifier.Train(tok_ids, tok_len, args.batch_size, fake_tok_ids, fake_tok_len, args.batch_size)

        dis_losses.update(d_loss)

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Discriminator Loss {dis_losses.val:.4f} ({dis_losses.avg:.4f})'.format(
                    batch=batch_idx + 1,
                    size=len(data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    dis_losses=dis_losses                  
                    )
        bar.next()
    bar.finish()

env_mode.Resource.InitResource()

if not os.path.exists(args.output):
    os.makedirs(args.output)

generation_gnet(gnet, fixed_noise, args.batch_size, dataset.tokenizer, args.output + '/yelp.lm.init.txt')

#classifier.Session.AdjustLR(args.pre_dnet_lr)
#for epoch in range(args.pre_dnet_epoch):
#    train_classifier(data_loader, gnet, classifier)
#classifier.Session.AdjustLR(args.lr)

for epoch in range(args.niter):
    ## Training.
    gen_losses = AverageMeter()
    dis_losses = AverageMeter()

    gnet.Session.SetTrainMode()
    bar = Bar('Training', max=len(data_loader))

    for batch_idx, data in enumerate(data_loader):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        tok_ids = data['tok_ids']
        tok_len = data['tok_len']
        label = data['label']

        # generate noise vector.
        noise = torch.randn(args.batch_size, 300)

        #print('gnet forward')
        fake_tok_ids, fake_tok_len = gnet.Forward(noise, args.batch_size, 300)

        #save_fake_tokens(fake_tok_ids, dataset.tokenizer, args.batch_size, 'output-tmp.txt')
        #print('train classifier')
        d_loss = classifier.Train(tok_ids, tok_len, args.batch_size, fake_tok_ids, fake_tok_len, args.batch_size)

        #print('dnet train')
        g_loss = dnet.Train(fake_tok_len, args.batch_size)
        
        #print('gnet backward')
        gnet.Update()

        #print('increase counter')
        sched.Incr()

        #classifier.pred.Output.SyncToCPU()
        #print(classifier.pred.Output[0], classifier.pred.Output[1], classifier.pred.Output[2])
        gen_losses.update(g_loss)
        dis_losses.update(d_loss)

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Generator Loss {gen_losses.val:.4f} ({gen_losses.avg:.4f}) | Discriminator Loss {dis_losses.val:.4f} ({dis_losses.avg:.4f})'.format(
                    batch=batch_idx + 1,
                    size=len(data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    gen_losses=gen_losses,
                    dis_losses=dis_losses                  
                    )
        bar.next()

    #if i % 100 == 0:
    generation_gnet(gnet, fixed_noise, args.batch_size, dataset.tokenizer, args.output + '/yelp.seqgan-'+str(epoch)+'.txt')

    

    #vutils.save_image(real_cpu, '%s/real_samples.png' % opt.outf, normalize=True)    
    bar.finish()

    #seqGan.Session.Complete()
    seqGan.Session.Model.Save(args.output + '/yelp.seqgan.'+str(epoch)+'.storm.ckpt')










