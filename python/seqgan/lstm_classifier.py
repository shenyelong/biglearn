from __future__ import print_function
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data

from YelpDataset import YelpDataset
from torch.utils.data import DataLoader

import numpy

import io
import sys

parser = argparse.ArgumentParser()

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='storm lib path')

parser.add_argument('--gpu_id', type=str, default='0', help='gpu ids to use.')

parser.add_argument('--dataset', required=True, type=str, help='training dataset.')
parser.add_argument('--vocab', required=True, type=str, help='vocab file.')
parser.add_argument('--max_seq_length', type=int, default=256, help="maximum sequence length")

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")

parser.add_argument('--workers', type=int, help='number of data loading workers', default=0)
parser.add_argument('--batch_size', type=int, default=64, help='input batch size')

parser.add_argument('--niter', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')



args = parser.parse_args()
print(args)

import clr as net_clr
from System import Array, IntPtr, Int32, Int64
from progress.bar import Bar as Bar

# /10T/yelongshen/biglearn/Targets
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, A_Func, Session, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, CudaPieceInt, IntArgument
#from BigLearn.DeepNet.Language import LargeBert, Bert, DistBert

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create storm enviroument")
env_mode = DeviceBehavior(0).TrainMode

dataset = YelpDataset(args.dataset, args.vocab, args.max_seq_length)
data_loader = DataLoader(dataset, batch_size=args.batch_size, num_workers=args.workers, shuffle=True, drop_last=True)

## create Generator model. 
#class Generator:
#    def __init__(in_dim, hid_dim, behavior):
#        self.Behavior = behavior
#        self.Session = Session(behavior)
#        self.Models = {}

#        _in = in_dim
#        _out = hid_dim
#        for i in range(layer):
#            self.Models['lstm_'+str(i)] = LSTMCell(_in, _out, behavior.Device) 
#            _in = _out


        #   int input, int cell, DeviceType device,
#    def Build(self, tok_embedding, max_seq_length, batch_size):
        # init word input is [cls]
#        init_tokens = [101 for _ in range(batch_size)]
#        _in = self.Session.LookupEmbed(tok_embedding, init_tokens)

#        for i in range(max_seq_length):
#            _out = self.Session.LSTM()
        # greedy decoding.

# assume it is an classification task first.

class Discriminator:
    ## construct model 
    def __init__(self, vocab_size, in_dim, hid_dim, layer, cate, behavior):
        self.Layer = layer
        self.InDim = in_dim
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        out_dims = [ hid_dim for _ in range(layer) ]

        self.Models = {}

        self.Models['embed'] = self.Session.Model.AddLayer(EmbedStructure(vocab_size, in_dim, self.Behavior.Device))
        self.Models['lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        self.Models['classifier'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, cate, A_Func.Linear, True, self.Behavior.Device))
    
    ## construct computation graph.
    def Build(self, batch_size, max_seq_length): 
        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.token_lens = CudaPieceInt(batch_size, self.Behavior.Device)
        self.labels = CudaPieceInt(batch_size, self.Behavior.Device)

        self.batch = IntArgument('batch', batch_size)
        self.seq = IntArgument('seq', max_seq_length)
        self.dim = IntArgument('dim', self.InDim)

        token_embed = self.Session.LookupEmbed(self.Models['embed'], self.tokens)
        token_embed = self.Session.Reshape(token_embed, self.dim, self.seq, self.batch)
        token_seq_data = self.Session.Tensor2SeqData(token_embed, self.token_lens)
        self.lstm_embed = self.Session.LSTM(self.Models['lstms'], token_seq_data)
        self.pred = self.Session.FNN(self.lstm_embed, self.Models['classifier'])
        self.loss = self.Session.SoftmaxLoss(self.pred, self.labels)


    def Train(self, token_id, token_len, label):
        ## load 
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(token_id.data_ptr())),  self.batch.Default * self.seq.Default)
        self.token_lens.CopyTorch(IntPtr.op_Explicit(Int64(token_len.data_ptr())),  self.batch.Default)
        self.labels.CopyTorch(IntPtr.op_Explicit(Int64(label.data_ptr())),  self.batch.Default)

        self.Session.StepV3Run()
        self.Session.StepV3Update()
        
        return self.loss.Value

#class SeqGanNet:
#    def __init__(self, gnet, ):
#        self.GNet = gnet

#    def Run(tokens, noises):
#        self.GNet.Input.Init(noises)
#        self.GNet.Session.Forward()

#        self.DNet.Input.
class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count
print('vocab size', len(dataset.tokenizer.vocab))
classifier = Discriminator(len(dataset.tokenizer.vocab), 300, 300, 2, 2, env_mode)

classifier.Session.InitOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))

classifier.Build(args.batch_size, args.max_seq_length)

for epoch in range(args.niter):

    classifier.Session.Init()
    
    losses = AverageMeter()
    
    bar = Bar('Training', max=len(data_loader))

    for batch_idx, data in enumerate(data_loader):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        tok_ids = data['tok_ids']
        tok_len = data['tok_len']
        label = data['label']

        loss = classifier.Train(tok_ids, tok_len, label)
        
        #classifier.pred.Output.SyncToCPU()
        #print(classifier.pred.Output[0], classifier.pred.Output[1], classifier.pred.Output[2])
        losses.update(loss)
        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Loss {losses.val:.4f} ({losses.avg:.4f})'.format(
                    batch=batch_idx + 1,
                    size=len(data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    losses=losses                  
                    )
        bar.next()
    bar.finish()
    classifier.Session.Complete()

    classifier.Session.Model.Save('model/yelp.lstm.'+str(epoch)+'.storm.ckpt')

    # do checkpointing
    # torch.save(netG.state_dict(), '%s/netG_epoch_%d.pth' % (opt.outf, epoch))
    # torch.save(netD.state_dict(), '%s/netD_epoch_%d.pth' % (opt.outf, epoch))










