from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import json
import math

import pickle
import numpy as np

import os
import tempfile
import argparse

import tqdm
from tqdm import tqdm, trange

import random
import collections

import torch
from torch.utils.data import Dataset
import tokenization

class DoublePoemDatasetV4(Dataset):
    def text_to_ids(self, text):
        ids = []
        for c in text:
            if(c in self.re_vocab):
                ids.append(self.re_vocab[c])
            else:
                ids.append(self.re_vocab['[UNK]'])
        return ids

    def ids_to_text(self, ids):
        text = []
        for i in ids:
            text.append(self.vocab[i])
        return text
        
    def load_corpus(self, corpus_path, max_seq_length):
        token_ids = []
        with open(corpus_path, 'r') as f:
            for line in f:
                line = tokenization.convert_to_unicode(line.strip())
                items = line.split(':')
                header = items[0]
                content = items[1]

                _token_ids = self.text_to_ids(content)

                if(len(_token_ids) == 0):
                    continue

                _token_ids = [self.re_vocab['[CLS]']] + _token_ids
                if(len(_token_ids) >= max_seq_length - 1):
                    _token_ids = _token_ids[ : max_seq_length - 1]
                _token_ids.append(self.re_vocab['[ELS]'])

                token_ids.append(_token_ids)
        print('\nCorpus %s, Corpus Size %d' % (corpus_path, len(token_ids)))
        return token_ids

    def load_vocab(self, vocab_file):
        vocab_list = []
        vocab_list.append('[PAD]')
        vocab_list.append('[UNK]')
        vocab_list.append('[CLS]')
        vocab_list.append('[ELS]')

        #vocab_list.append('[PAD]')
        with open(vocab_file, 'r') as f:
            for line in f:
                line = tokenization.convert_to_unicode(line) 
                k = line.strip().split('\t')[0]
                vocab_list.append(k) 
        
        re_vocab = {}
        re_idx = 0
        for k in vocab_list:
            re_vocab[k] = re_idx
            re_idx += 1

        return vocab_list, re_vocab

    ## corpus path, appending queries into history.
    def __init__(self, poem1_path, poem2_path, vocab_file, max_seq_length, rseed = 110):
        self.vocab, self.re_vocab = self.load_vocab(vocab_file) # tokenization.FullTokenizer(vocab_file) 
        self.max_seq_length = max_seq_length

        self.poem1_token_ids = self.load_corpus(poem1_path, max_seq_length)
        self.poem2_token_ids = self.load_corpus(poem2_path, max_seq_length)

        self.rng = random.Random(rseed)

    def vocab_size(self):
        return len(self.vocab)

    def __len__(self):
        return max(len(self.poem1_token_ids), len(self.poem2_token_ids))

    def __getitem__(self, item):
        #if(self.is_enumate_doc_span):
        #    (p_id, turn_id, doc_word_num, doc_word_offset, doc_start, doc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans) = self.doc_span_sample(item)
        #else:
        #    max_target_len = self.max_seq_length - 1

        p1_item = item
        if(item >= len(self.poem1_token_ids)):
            p1_item = self.rng.randint(0, len(self.poem1_token_ids) - 1)

        p2_item = item
        if(item >= len(self.poem2_token_ids)):
            p2_item = self.rng.randint(0, len(self.poem2_token_ids) - 1)

        p1_ids = self.poem1_token_ids[p1_item]
        p2_ids = self.poem2_token_ids[p2_item]
        
        p1_tok_len = len(p1_ids)
        p2_tok_len = len(p2_ids)
        
        if(p1_tok_len < self.max_seq_length):
            padding = [self.re_vocab['[PAD]'] for _ in range(self.max_seq_length - p1_tok_len)]
            p1_ids.extend(padding)

        if(p2_tok_len < self.max_seq_length):
            padding = [self.re_vocab['[PAD]'] for _ in range(self.max_seq_length - p2_tok_len)]
            p2_ids.extend(padding)

        #assert len(ids) == self.max_seq_length
        
        output = {"p_tok_ids" : p1_ids,
                  "p_tok_len" : p1_tok_len,
                  "n_tok_ids"  : p2_ids,
                  "n_tok_len" :  p2_tok_len}
        
        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
