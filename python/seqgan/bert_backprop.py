from __future__ import division
from torchvision import models
from torchvision import transforms
from PIL import Image

import argparse
import torch
import torchvision
import torch.nn as nn
import numpy as np
import math

import sys
import os
import clr as net_clr
from System import Array, IntPtr, Int32, Int64

from YelpDataset2 import YelpDataset2
from torch.utils.data import DataLoader

from progress.bar import Bar as Bar


import tokenization

parser = argparse.ArgumentParser(description='Bert Neural Style Transformer.')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')
parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument('--dev', required=True, type=str, help='training dataset.')
parser.add_argument('--vocab', type=str, default='vocab file')
parser.add_argument('--max_seq_length', type=int, default=256, help="maximum sequence length")

parser.add_argument('--workers', type=int, help='number of data loading workers', default=0)
parser.add_argument('--batch_size', type=int, default=64, help='input batch size')

parser.add_argument("--seed_bert", type=str, default="seed_bert", help="seed bert model")

parser.add_argument('--niter', default=300, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('--lr', default=0.00001, type=float, metavar='LR', help='initial learning rate')
parser.add_argument('--schedule_lr', default='0:0,10000:0.00001', type=str, metavar='SLR', help='schedule lr')
parser.add_argument('--grad_clip', default=1.0, type=float, metavar='GCLIP', help='gradient clip')

#parser.add_argument("--output_model", type=str, default="/data1/yelongshen/tmp_model/", help="seed bert model")
parser.add_argument('--init_ckpt', type=str, default='model/yelp.lstm.0.storm.ckpt', help='init ckpt model')

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, RunHelper, IntArgument, CudaPieceFloat, CudaPieceInt, Session, NdArrayData, ComputationGraph, A_Func

from BigLearn import BaseBertModel, LayerStructure 
#from BigLearn.DeepNet.Language import Bert, DistBert
#from BigLearn.DeepNet.Image import ImageNet_VGG16Net, DistImageNet_VGG16Net

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create storm enviroument")
env_mode = DeviceBehavior(0).TrainMode

dataset = YelpDataset2(args.dev, args.vocab, args.max_seq_length)
data_loader = DataLoader(dataset, batch_size=args.batch_size, num_workers=args.workers, shuffle=True, drop_last=True)


class Discriminator:
    ## construct model 
    def __init__(self, seed_bert, cate, batch_size, max_seq_length, behavior):
        self.Behavior = behavior
        self.Session = Session(behavior)
        
        self.Models = {}

        self.batch_size = batch_size
        self.max_seq_length = max_seq_length
        self.cate = cate

        self.Models['bert_base'] = self.Session.Model.AddLayer(BaseBertModel(seed_bert + 'exp.label', seed_bert + 'exp.bin', behavior.Device))
        # self.Session.Model.AddLayer(EmbedStructure(vocab_size, in_dim, self.Behavior.Device))
        # self.Models['lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        self.Models['classifier'] =  self.Session.Model.AddLayer(LayerStructure(self.Models['bert_base'].embed, cate, A_Func.Linear, True, behavior.Device))
        #self.Session.Model.AddLayer(
        
        self.Session.Model.AllocateGradient(self.Behavior.Device)
        
        self.Session.AllocateOptimizer(StructureLearner.AdamBertLearner(args.lr, args.grad_clip, args.schedule_lr, 0.0001, 1.0))
        #StructureLearner.AdamLearner(args.lr, 0.5, 0.999)) # (args.lr, 0.5, 0.999))

        self.tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.segments = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.masks = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)

        self.labels = CudaPieceInt(batch_size, self.Behavior.Device)

        bert_feature = self.Session.BaseBertFeaturizer(self.Models['bert_base'], self.tokens, self.segments, self.masks, batch_size, max_seq_length)

        self.slice_feature = bert_feature.Item2

        self.pred = self.Session.FNN(self.slice_feature, self.Models['classifier'])
        self.loss = self.Session.SoftmaxLoss(self.pred, self.labels)

    def Train(self, tokens, segments, masks, labels):
        ## load 
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.segments.CopyTorch(IntPtr.op_Explicit(Int64(segments.data_ptr())), self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(masks.data_ptr())), self.batch_size * self.max_seq_length)
        
        self.labels.CopyTorch(IntPtr.op_Explicit(Int64(labels.data_ptr())),  self.batch_size)

        self.Session.StepV3Run()

        self.Session.StepV3Update()
        
        return self.loss.Value

    def Predict(self, tokens, segments, masks, output):
        self.tokens.CopyTorch(IntPtr.op_Explicit(Int64(tokens.data_ptr())),  self.batch_size * self.max_seq_length)
        self.segments.CopyTorch(IntPtr.op_Explicit(Int64(segments.data_ptr())), self.batch_size * self.max_seq_length)
        self.masks.CopyTorch(IntPtr.op_Explicit(Int64(masks.data_ptr())), self.batch_size * self.max_seq_length)
        
        #self.labels.CopyTorch(IntPtr.op_Explicit(Int64(labels.data_ptr())),  self.batch_size)

        self.Session.ForwardV3()
        
        self.pred.Output.PasteTorch(IntPtr.op_Explicit(Int64(output.data_ptr())),  self.batch_size * self.cate)

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def accuracy(output, target):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        batch_size = target.size(0)

        _, pred = output.topk(1, 1, True, True)
        pred = pred.t().type(torch.IntTensor)
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        return correct.view(-1).float().sum(0, keepdim=True).mul_(100.0 / batch_size).item()

predict = Discriminator(args.seed_bert, 2, args.batch_size, args.max_seq_length, env_mode)

predict.Session.Model.Load(args.init_ckpt)


#classifier.Session.InitOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))
#classifier.Build(args.batch_size, args.max_seq_length)

def main():
    tokenizer = tokenization.FullTokenizer(args.vocab) 
    text = ''
    style = 1
    target_style = 0

    text = tokenization.convert_to_unicode(text)

    tokens = tokenizer.tokenize(text)

    tok_ids = tokenizer.convert_tokens_to_ids(tokens)
    if(len(tok_ids) > args.max_seq_length - 2):
        tok_ids = tok_ids[ : args.max_seq_length - 2]

    tok_ids = [101] + tok_ids + [102]
    segments = [0] * len(tok_ids)
    
    masks = [1] * len(tok_ids)


    select_features = ['transformer-0'] #, 'transformer-3','transformer-5','transformer-7','transformer-9','transformer-11'] # 'conv-2-1','conv-3-1', 'conv-4-1', 'conv-1-1', 
    feature_num = len(select_features)

    content_net = BertStyleNet(model, env_mode)
    content_net.BuildFeatureExtractor(content_ids)
    con_features = content_net.GetFeatureMaps(select_features)
    
    style_net = BertStyleNet(model, env_mode)
    style_net.BuildFeatureExtractor(style_ids)
    c_st_features = style_net.GetFeatureMaps(select_features)

    style_features = []
    for i in range(feature_num):
        s_fea = style_net.Session.MatMul(c_st_features[i], 1, c_st_features[i], 0)
        style_features.append(s_fea)
    
    target_encode_session = Session(env_mode)
    target_embed = target_encode_session.LookupEmbed(model.TokenEmbed, style_ids)
    

    target_decode_session = Session(env_mode)
    token_l2_norm = target_decode_session.L2Norm(model.NdTokenEmbed, 0)
    token_l2 = target_decode_session.Dot(token_l2_norm, token_l2_norm)
    token_l2 = target_decode_session.Reshape(token_l2, token_l2.Shape[1], token_l2.Shape[0])
    token_score = target_decode_session.MatMul(target_embed, 0, model.NdTokenEmbed, 1)
    token_score = target_decode_session.Add(token_score, 2.0, token_l2, -1.0)
    token_idx = target_decode_session.Argmax(token_score, 0)

    transfer_net = BertStyleNet(model, env_mode)
    transfer_net.Build(target_embed)
    target_features = transfer_net.GetFeatureMaps(select_features)

    con_losses = []
    style_losses = []
    #style_mses = []
    #target_style_features = []
    for i in range(feature_num):
        con_mse = transfer_net.Session.Add(con_features[i], 1.0, target_features[i], -1.0)
        con_mse_loss = transfer_net.Session.MSE(con_mse, 1.0, 0.0)
        con_losses.append(con_mse_loss)

        s_fea = transfer_net.Session.MatMul(target_features[i], 1, target_features[i], 0)
        #target_style_features.append(s_fea)

        style_mse = transfer_net.Session.Add(style_features[i], 1.0, s_fea, -1.0)
        style_mse_loss = transfer_net.Session.MSE(style_mse, args.style_weight * 1.0 / (512 * 768), 0.0)
        style_losses.append(style_mse_loss)
        #style_mses.append(style_mse)

    transfer_net.Session.AddOptimizer(StructureLearner.AdamLearner(args.lr, 0.5, 0.999), target_embed)

    #vgg = VGGNet().to(device).eval()
    transfer_net.Session.SetTrainMode()
    transfer_net.Session.Init()

    ## content feature
    content_net.Session.Forward()

    ## style feature
    style_net.Session.Forward()

    ## init target encoding
    target_encode_session.Forward()

    for step in range(0, args.total_step):
        transfer_net.Session.StepV3Run()
        #style_mses[0].Deriv.SyncToCPU()
        #print(style_mses[0].Deriv[0], style_mses[0].Deriv[1], style_mses[0].Deriv[2], style_mses[0].Deriv[3])

        #target_style_features[0].Deriv.SyncToCPU()
        #print(target_style_features[0].Deriv[0], target_style_features[0].Deriv[1], target_style_features[0].Deriv[2], target_style_features[0].Deriv[3])

        #target_features[0].Deriv.SyncToCPU()
        #print(target_features[0].Deriv[0], target_features[0].Deriv[1], target_features[0].Deriv[2], target_features[0].Deriv[3])
        transfer_net.Session.StepV3Update()

        content_loss = sum([obj.Value for obj in con_losses]) # 0
        style_loss = sum([obj.Value for obj in style_losses])

        if (step+1) % args.log_step == 0:
            print ('Step [{}/{}], Content Loss: {:.4f}, Style Loss: {:.4f}'.format(step+1, args.total_step, content_loss, style_loss))

        if (step+1) % args.sample_step == 0:
            target_decode_session.Forward()

            #print(token_l2.Shape[0].Default, token_l2.Shape[1].Default)
            #print(token_score.Shape[0].Default, token_score.Shape[1].Default)

            #token_score.Output.SyncToCPU()
            #print(token_score.Output[0], token_score.Output[1], token_score.Output[2], token_score.Output[3], token_score.Output[4])

            #token_idx.SyncToCPU()
            #print(token_idx[0], token_idx[1], token_idx[2])

            token_idx_list = token_idx.ToCPU()
            #print(token_idx_list[0], token_idx_list[1], token_idx_list[2])

            target_tokens = tokenizer.convert_ids_to_tokens(token_idx_list)
            out_file = open('output-'+str(step+1)+'.txt','w')
            out_file.write(' '.join(target_tokens))
            out_file.close()
            
            # Save the generated image # [123.68, 116.78, 103.94]
            # denorm = transforms.Normalize((-0.485, -0.458, -0.408), (1, 1, 1))
            # img = target.clone()
            # img = denorm(img).clamp_(0, 1)
            # torchvision.utils.save_image(img, 'output-{}.png'.format(step+1))

    transfer_net.Session.Complete()

if __name__ == "__main__":
    main()

    #classifier.Session.Model.Save(args.output_model + 'yelp.base_bert.'+str(epoch)+'.storm.ckpt')
