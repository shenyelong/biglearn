from __future__ import print_function
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data

from YelpDataset import YelpDataset
from torch.utils.data import DataLoader

import numpy

import io
import sys

## first pretrain the generator and discriminator
parser = argparse.ArgumentParser()

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='storm lib path')

parser.add_argument('--gpu_id', type=str, default='0', help='gpu ids to use.')

parser.add_argument('--dataset', type=str, help='training dataset.')
parser.add_argument('--vocab', type=str, help='vocab file.')
parser.add_argument('--max_seq_length', type=int, default=256, help="maximum sequence length")

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")

parser.add_argument('--workers', type=int, help='number of data loading workers', default=0)
parser.add_argument('--batch_size', type=int, default=64, help='input batch size')

parser.add_argument('--niter', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')

parser.add_argument('--sched_temp', type=str, default='0:0.3,30000:0.7', help='schedule temperature for gumbel softmax')
parser.add_argument('--output', type=str, default='/data2/yelongshen/seqgan_lm', help='data model output folder')

parser.add_argument('--in_dim', type=int, help='word embedding dim', default=300)
parser.add_argument('--hid_dim', type=int, default=300, help='rnn hidden dimension')
parser.add_argument('--latent_code', type=int, help='latent code dimension', default=300)

parser.add_argument('--layer', type=int, default=2, help='rnn layers')

parser.add_argument('--kl', type=float, default=0.1, help='kl loss weight')
args = parser.parse_args()
print(args)

import clr as net_clr
from System import Array, IntPtr, Int32, Int64, Collections
from progress.bar import Bar as Bar

# /10T/yelongshen/biglearn/Targets
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager

from BigLearn import Session, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure

from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData

from BigLearn import GradientOptimizer, StandardKLRunner

#from BigLearn.DeepNet.Language import LargeBert, Bert, DistBert

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create storm enviroument")
env_mode = DeviceBehavior(0).TrainMode


class VAEModule:
    # dual module for Generator and Discriminator 
    def __init__(self, vocab_size, in_dim, hid_dim, latent_code, layer, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)

        out_dims = [ hid_dim for _ in range(layer) ]

        self.Models = {}

        self.Models['embed'] = self.Session.Model.AddLayer(EmbedStructure(vocab_size, in_dim, self.Behavior.Device))
        self.Models['en_lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        
        self.Models['en_mean'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, latent_code, A_Func.Tanh, True, self.Behavior.Device))
        self.Models['en_var'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, latent_code, A_Func.Tanh, True, self.Behavior.Device))

        self.Models['de_o'] = self.Session.Model.AddLayer(LayerStructure(latent_code, hid_dim, A_Func.Tanh, True, self.Behavior.Device))
        self.Models['de_c'] = self.Session.Model.AddLayer(LayerStructure(latent_code, hid_dim, A_Func.Tanh, True, self.Behavior.Device))

        self.Models['gen_lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        
        self.Models['gen_decoder'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, in_dim, A_Func.Linear, True, self.Behavior.Device))
        
        self.Session.Model.AllocateGradient(self.Behavior.Device)

## create Generator model. 
class VAENet:
    def __init__(self, models, in_dim, hid_dim, latent_code, max_seq_length, batch_size, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        self.max_seq_length = max_seq_length
        self.hid_dim = hid_dim
        self.in_dim = in_dim
        self.latent_code = latent_code
        
        self.batch = IntArgument('batch', batch_size)
        self.seq = IntArgument('seq', max_seq_length)
        self.hdim = IntArgument('dim', hid_dim)
        self.idim = IntArgument('dim', in_dim)
        self.lscode = IntArgument('latent_code', latent_code)
        ## noise as input.

        self.C_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.I_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.O_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.token_len = CudaPieceInt(batch_size, self.Behavior.Device)

        ## encoder model.
        c_token_embed = self.Session.LookupEmbed(self.Models['embed'], self.C_tokens)
        c_token_embed = self.Session.Reshape(c_token_embed, self.idim, self.seq, self.batch)
        c_token_seq_data = self.Session.Tensor2SeqData(c_token_embed, self.token_len)
        
        self.state = self.Session.LSTM(self.Models['en_lstms'], c_token_seq_data)
        self.mean = self.Session.FNN(self.state, self.Models['en_mean'])
        self.log_std = self.Session.FNN(self.state, self.Models['en_var'])
        #self.scaled_log_std = self.Session.Scale(self.log_std, 1.0)

        # NdArrayData data, float scale)
        self.Noise = NdArrayData(behavior.Device, False, self.lscode, self.batch) 
        
        self.log_std_v2 = self.Session.Scale(self.log_std, 0.5)
        self.std = self.Session.Act(self.log_std_v2, A_Func.Exp)
        self.noise_std = self.Session.Dot(self.Noise, self.std)
        self.noise_smp = self.Session.Add(self.noise_std, self.mean)

        state_o = self.Session.FNN(self.noise_smp, self.Models['de_o'])
        state_c = self.Session.FNN(self.noise_smp, self.Models['de_c'])

        in_token_embed = self.Session.LookupEmbed(self.Models['embed'], self.I_tokens)
        in_token_embed = self.Session.Reshape(in_token_embed, self.idim, self.seq, self.batch)
        in_token_seq_data = self.Session.Tensor2SeqData(in_token_embed, self.token_len)

        layer = self.Models['gen_lstms'].Layer
        _pc = [state_c for _ in range(layer)]
        _po = [state_o for _ in range(layer)]

        self.weight = FloatArgument('kl weight', args.kl) # 0.5)

        lstm_embed = self.Session.LSTMEncode(self.Models['gen_lstms'], in_token_seq_data, _po, _pc)

        lstm_embed = self.Session.SeqData2CompactTensor(lstm_embed)

        decode = self.Session.FNN(lstm_embed, self.Models['gen_decoder'])

        self.logit = self.Session.MatMul(decode, 0, self.Models['embed'].NDEmbed, 1)

        self.loss = self.Session.SoftmaxLoss(self.logit, self.O_tokens)        

        self.klloss = self.Session.StandardKLLoss(self.mean, self.log_std, self.weight)

        self.Session.Model.AddLayer(self.Models['embed'])
        self.Session.Model.AddLayer(self.Models['en_lstms'])
        self.Session.Model.AddLayer(self.Models['en_mean'])
        self.Session.Model.AddLayer(self.Models['en_var'])

        self.Session.Model.AddLayer(self.Models['de_o'])
        self.Session.Model.AddLayer(self.Models['de_c'])
        
        self.Session.Model.AddLayer(self.Models['gen_lstms'])
        self.Session.Model.AddLayer(self.Models['gen_decoder'])
        self.Session.AllocateOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))

    def Train(self, noise, tok_ids, tok_len, batch_size):
        i_tok_ids = torch.zeros([batch_size, self.max_seq_length], dtype = torch.int32)
        o_tok_ids = torch.zeros([batch_size * self.max_seq_length], dtype = torch.int32)

        eff_size = 0
        for b in range(batch_size):

            i_tok_ids[b][0] = 101            
            
            for t in range(tok_len[b] - 1):
                i_tok_ids[b][t + 1] = tok_ids[b][t]
                o_tok_ids[eff_size] = tok_ids[b][t]
                eff_size += 1

            o_tok_ids[eff_size] = tok_ids[b][tok_len[b] - 1]

            eff_size += 1
            
        self.I_tokens.CopyTorch(IntPtr.op_Explicit(Int64(i_tok_ids.data_ptr())), batch_size * self.max_seq_length)
        
        self.O_tokens.EffectiveSize = eff_size;
        self.O_tokens.CopyTorch(IntPtr.op_Explicit(Int64(o_tok_ids.data_ptr())), eff_size)
        self.token_len.CopyTorch(IntPtr.op_Explicit(Int64(tok_len.data_ptr())),  batch_size)

        self.C_tokens.CopyTorch(IntPtr.op_Explicit(Int64(tok_ids.data_ptr())),  batch_size * self.max_seq_length)

        self.Noise.Output.CopyTorch(IntPtr.op_Explicit(Int64(noise.data_ptr())), batch_size * self.latent_code)
        
        #print(noise)
        self.Session.StepV3Run()
        self.Session.StepV3Update()

        #return self.loss.Value

class Generator:
    def __init__(self, models, in_dim, hid_dim, latent_code, max_seq_length, batch_size, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        self.in_dim = in_dim
        self.hid_dim = hid_dim
        self.ls_code = latent_code

        self.max_seq_length = max_seq_length

        batch = IntArgument('batch', batch_size)
        
        seq = IntArgument('seq', max_seq_length)
        
        hdim = IntArgument('hdim', hid_dim)
        idim = IntArgument('idim', in_dim)
        lscode = IntArgument('latent_code', latent_code)

        self.Noise = NdArrayData(behavior.Device, False, lscode, batch) 
        
        #zeros = NdArrayData(behavior.Device, False, dim, batch) 
        #zeros.Output.Zero();

        init_tokens = [101 for _ in range(batch_size)]
        embed = self.Session.LookupEmbed(self.Models['embed'], init_tokens)

        layer = self.Models['gen_lstms'].Layer

        state_o = self.Session.FNN(self.Noise, self.Models['de_o'])
        state_c = self.Session.FNN(self.Noise, self.Models['de_c'])

        _pc = [state_c for _ in range(layer)] #[zeros for _ in range(layer-1)] + [ self.Noise ] #[zeros for _ in range(layer)]
        _po = [state_o for _ in range(layer)] #[zeros for _ in range(layer-1)] + [ self.Noise ]

        self.decode_tokens = []
        self.decode_b_logit = []

        for i in range(max_seq_length):
            _out_lstm = self.Session.LSTMState(self.Models['gen_lstms'], _po, _pc, embed)
            _po = _out_lstm.Item1
            _pc = _out_lstm.Item2

            embed = _po[layer - 1]

            decode = self.Session.FNN(embed, self.Models['gen_decoder'])
            logit = self.Session.MatMul(decode, 0, self.Models['embed'].NDEmbed, 1)
            #logit = self.Session.Add(logit, self.Models['de_bias'].NDBias)

            _out_gumbel = self.Session.GumbelSoftmax(logit, True)
            b_logit = _out_gumbel.Item1
            tok_ids = _out_gumbel.Item2

            self.decode_b_logit.append(b_logit)
            self.decode_tokens.append(tok_ids)

            tok_embed = self.Session.MatMul(b_logit, 0, self.Models['embed'].NDEmbed, 0)
            embed = tok_embed

    def Forward(self, noise, batch_size):
        self.Noise.Output.CopyTorch(IntPtr.op_Explicit(Int64(noise.data_ptr())),  batch_size * self.ls_code)
        #print(noise)
        self.Session.ForwardV3()

        tok_ids = torch.zeros([batch_size, self.max_seq_length], dtype = torch.int32)
        tok_len = torch.zeros([batch_size], dtype = torch.int32)

        for i in range(self.max_seq_length):
            self.decode_tokens[i].SyncToCPU()
            for b in range(batch_size):
                if(self.decode_tokens[i][b] == 0 and tok_len[b] == 0):
                    tok_len[b] == i + 1

                if(tok_len[b] == 0):
                    tok_ids[b][i] = self.decode_tokens[i][b]
                else:
                    tok_ids[b][i] = 0

        for b in range(batch_size):
            if(tok_len[b] == 0):
                tok_len[b] = self.max_seq_length

        return tok_ids, tok_len

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


print('loading dataset ...', args.dataset)
dataset = YelpDataset(args.dataset, args.vocab, args.max_seq_length)
data_loader = DataLoader(dataset, batch_size=args.batch_size, num_workers=args.workers, shuffle=True, drop_last=True)

vocab_size = len(dataset.tokenizer.vocab)
print('vocab size', vocab_size)

#global_update = IntArgument("global_update", 0)
#sched = RateScheduler(global_update, args.sched_temp)
# in_dim, hid_dim, latent_code, max_seq_length, batch_size, behavior):
#     def __init__(self, vocab_size, in_dim, hid_dim, latent_code, layer, behavior):

seqlm = VAEModule(vocab_size, args.in_dim, args.hid_dim, args.latent_code, args.layer, env_mode)
#seqGan.Session.InitOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))

# max_seq_length, batch_size, rate_sched, behavior):
#models, hid_dim, max_seq_length, batch_size, rate_sched, behavior):
#    def __init__(self, models, in_dim, hid_dim, latent_code, max_seq_length, batch_size, behavior):
lmnet = VAENet(seqlm.Models, args.in_dim, args.hid_dim, args.latent_code, args.max_seq_length, args.batch_size, env_mode)
#classifier = Classifier(seqGan.Models, 300, args.max_seq_length, args.batch_size * 2, env_mode) 

# models, fake_tok_logits, vocab_size, embed_dim, max_seq_length, batch_size, behavior):
#dnet = Discriminator(seqGan.Models, gnet.decode_b_logit, vocab_size, 300, args.max_seq_length, args.batch_size, env_mode)

#fixed_noise = torch.randn(args.batch_size, 300)

#def save_fake_tokens(tok_ids, tokenizer, batch_size, fname):
#    out_file = open(fname,'w')
#    for b in range(batch_size):
#        target_tokens = tokenizer.convert_ids_to_tokens(tok_ids[b].numpy())
#        out_file.write(' '.join(target_tokens)+'\n')
#    out_file.close()
#     def __init__(self, models, in_dim, hid_dim, max_seq_length, batch_size, behavior):

#     def __init__(self, models, in_dim, hid_dim, latent_code, max_seq_length, batch_size, behavior):

g_net =  Generator(seqlm.Models, args.in_dim, args.hid_dim, args.latent_code, args.max_seq_length, args.batch_size, env_mode)

fixed_noise = torch.randn(args.batch_size, args.latent_code)

#env_mode.Resource.InitResource()
if not os.path.exists(args.output):
    os.makedirs(args.output)

def generation_gnet(g_net, fixed_noise, batch_size, tokenizer, fname):
    
    g_net.Session.SetPredictMode()

    fake_tok_ids, fake_tok_len = g_net.Forward(fixed_noise, batch_size)
    
    #out_file = open(args.output + '/yelp.seqgan-'+str(epoch)+'.txt','w')
    out_file = open(fname,'w')
    for b in range(batch_size):
        target_tokens = tokenizer.convert_ids_to_tokens(fake_tok_ids[b].numpy())
        out_file.write(' '.join(target_tokens)+'\n')
    out_file.close()

for epoch in range(args.niter):

    seqlm.Session.Init()
    
    losses = AverageMeter()
    kl_losses = AverageMeter()
    wkl_losses = AverageMeter()

    mu = AverageMeter()
    delta = AverageMeter()

    #dis_losses = AverageMeter()

    lmnet.Session.SetTrainMode()
    bar = Bar('Training', max=len(data_loader))

    for batch_idx, data in enumerate(data_loader):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        tok_ids = data['tok_ids']
        tok_len = data['tok_len']
        label = data['label']

        # generate noise vector.
        noise = torch.randn(args.batch_size, args.latent_code)

        #print('gnet forward')
        #fake_tok_ids, fake_tok_len = gnet.Forward(noise, args.batch_size, 300)

        #save_fake_tokens(fake_tok_ids, dataset.tokenizer, args.batch_size, 'output-tmp.txt')
        #print('train classifier')noise, tok_ids, tok_len, batch_size):
        lmnet.Train(noise, tok_ids, tok_len, args.batch_size) #:(tok_ids, tok_len, args.batch_size, fake_tok_ids, fake_tok_len, args.batch_size)
        loss = lmnet.loss.Value
        wkl = lmnet.klloss.Value
        kl = lmnet.klloss.KLValue

        mu.update(lmnet.klloss.Mu)
        delta.update(lmnet.klloss.Delta)
        #print('dnet train')
        #g_loss = dnet.Train(fake_tok_len, args.batch_size)
        
        #print('gnet backward')
        #gnet.Update()

        #print('increase counter')
        #sched.Incr()

        #classifier.pred.Output.SyncToCPU()
        #print(classifier.pred.Output[0], classifier.pred.Output[1], classifier.pred.Output[2])
        #gen_losses.update(g_loss)
        losses.update(loss)
        kl_losses.update(kl)
        wkl_losses.update(wkl)

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | word Loss {losses.val:.4f} ({losses.avg:.4f}) | KL Loss {kl_losses.val:.4f} ({kl_losses.avg:.4f}) | Delta Loss {delta.val:.4f} ({delta.avg:.4f}) | Mu Loss {mu.val:.4f} ({mu.avg:.4f}) | Weighted KL Loss {wkl_losses.val:.4f} ({wkl_losses.avg:.4f})'.format(
                    batch=batch_idx + 1,
                    size=len(data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    losses=losses,
                    kl_losses=kl_losses,
                    delta=delta,
                    mu=mu,
                    wkl_losses=wkl_losses
                    )
        bar.next()

    #if i % 100 == 0:
    
    #lmnet.Session.SetPredictMode()

    #fake_tok_ids, fake_tok_len = gnet.Forward(fixed_noise, args.batch_size, 300)
    
    #out_file = open('output/yelp.seqgan-'+str(epoch+1)+'.txt','w')
    #for b in range(args.batch_size):
    #    target_tokens = dataset.tokenizer.convert_ids_to_tokens(fake_tok_ids[b].numpy())
    #    out_file.write(' '.join(target_tokens)+'\n')
    #out_file.close()

    #vutils.save_image(real_cpu, '%s/real_samples.png' % opt.outf, normalize=True)    
    bar.finish()
    seqlm.Session.Complete()
    
    generation_gnet(g_net, fixed_noise, args.batch_size, dataset.tokenizer, args.output + '/yelp-'+str(epoch)+'.txt')

    seqlm.Session.Model.Save(args.output + '/yelp.lm.'+str(epoch)+'.storm.ckpt')










