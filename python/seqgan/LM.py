from __future__ import print_function
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data

from YelpDataset import YelpDataset
from torch.utils.data import DataLoader

import numpy

import io
import sys

## first pretrain the generator and discriminator
parser = argparse.ArgumentParser()

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='storm lib path')

parser.add_argument('--gpu_id', type=str, default='0', help='gpu ids to use.')

parser.add_argument('--dataset', type=str, help='training dataset.')
parser.add_argument('--vocab', type=str, help='vocab file.')
parser.add_argument('--max_seq_length', type=int, default=256, help="maximum sequence length")

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")

parser.add_argument('--workers', type=int, help='number of data loading workers', default=0)
parser.add_argument('--batch_size', type=int, default=64, help='input batch size')

parser.add_argument('--niter', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')

parser.add_argument('--sched_temp', type=str, default='0:0.3,30000:0.7', help='schedule temperature for gumbel softmax')
parser.add_argument('--output', type=str, default='/data2/yelongshen/seqgan_lm', help='data model output folder')

args = parser.parse_args()
print(args)

import clr as net_clr
from System import Array, IntPtr, Int32, Int64, Collections
from progress.bar import Bar as Bar

# /10T/yelongshen/biglearn/Targets
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, A_Func, IntArgument, RateScheduler, ResourceManager

from BigLearn import Session, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure

from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData

from BigLearn import GradientOptimizer

#from BigLearn.DeepNet.Language import LargeBert, Bert, DistBert

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create storm enviroument")
env_mode = DeviceBehavior(0).TrainMode


class LMModule:
    # dual module for Generator and Discriminator 
    def __init__(self, vocab_size, in_dim, hid_dim, layer, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)

        out_dims = [ hid_dim for _ in range(layer) ]

        self.Models = {}

        self.Models['embed'] = self.Session.Model.AddLayer(EmbedStructure(vocab_size, in_dim, self.Behavior.Device))

        self.Models['gen_lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        self.Models['gen_decoder'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, in_dim, A_Func.Linear, True, self.Behavior.Device))

        self.Session.Model.AllocateGradient(self.Behavior.Device)

## create Generator model. 
class LMNet:
    def __init__(self, models, hid_dim, max_seq_length, batch_size, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        self.max_seq_length = max_seq_length

        self.batch = IntArgument('batch', batch_size)
        self.seq = IntArgument('seq', max_seq_length)
        self.dim = IntArgument('dim', hid_dim)

        #self.Noise = NdArrayData(behavior.Device, False, dim, batch) 

        self.I_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.O_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.token_len = CudaPieceInt(batch_size, self.Behavior.Device)

        in_token_embed = self.Session.LookupEmbed(self.Models['embed'], self.I_tokens)
        in_token_embed = self.Session.Reshape(in_token_embed, self.dim, self.seq, self.batch)
        in_token_seq_data = self.Session.Tensor2SeqData(in_token_embed, self.token_len)

        lstm_embed = self.Session.LSTMEncode(self.Models['gen_lstms'], in_token_seq_data)
        lstm_embed = self.Session.SeqData2CompactTensor(lstm_embed)

        decode = self.Session.FNN(lstm_embed, self.Models['gen_decoder'])

        self.logit = self.Session.MatMul(decode, 0, self.Models['embed'].NDEmbed, 1)

        self.loss = self.Session.SoftmaxLoss(self.logit, self.O_tokens)        

        self.Session.Model.AddLayer(self.Models['embed'])
        self.Session.Model.AddLayer(self.Models['gen_lstms'])
        self.Session.Model.AddLayer(self.Models['gen_decoder'])
        self.Session.AllocateOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))

    def Train(self, tok_ids, tok_len, batch_size):
        i_tok_ids = torch.zeros([batch_size, self.max_seq_length], dtype = torch.int32)
        o_tok_ids = torch.zeros([batch_size * self.max_seq_length], dtype = torch.int32)

        eff_size = 0
        for b in range(batch_size):

            i_tok_ids[b][0] = 101            
            
            for t in range(tok_len[b] - 1):
                i_tok_ids[b][t + 1] = tok_ids[b][t]
                o_tok_ids[eff_size] = tok_ids[b][t]
                eff_size += 1

            o_tok_ids[eff_size] = tok_ids[b][tok_len[b] - 1]

            eff_size += 1
            
        self.I_tokens.CopyTorch(IntPtr.op_Explicit(Int64(i_tok_ids.data_ptr())), batch_size * self.max_seq_length)
        
        self.O_tokens.EffectiveSize = eff_size;
        self.O_tokens.CopyTorch(IntPtr.op_Explicit(Int64(o_tok_ids.data_ptr())), eff_size)
        self.token_len.CopyTorch(IntPtr.op_Explicit(Int64(tok_len.data_ptr())),  batch_size)

        #self.Noise.Output.CopyTorch(IntPtr.op_Explicit(Int64(noise.data_ptr())),  batch_size * dim)
        #print(noise)
        self.Session.StepV3Run()
        self.Session.StepV3Update()

        return self.loss.Value
    
class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


print('loading dataset ...', args.dataset)
dataset = YelpDataset(args.dataset, args.vocab, args.max_seq_length)
data_loader = DataLoader(dataset, batch_size=args.batch_size, num_workers=args.workers, shuffle=True, drop_last=True)

vocab_size = len(dataset.tokenizer.vocab)
print('vocab size', vocab_size)

#global_update = IntArgument("global_update", 0)
#sched = RateScheduler(global_update, args.sched_temp)

seqlm = LMModule(vocab_size, 300, 300, 2, env_mode)
#seqGan.Session.InitOptimizer(StructureLearner.AdamLearner(args.lr, 0.9, 0.999))

# max_seq_length, batch_size, rate_sched, behavior):
#models, hid_dim, max_seq_length, batch_size, rate_sched, behavior):
lmnet = LMNet(seqlm.Models, 300, args.max_seq_length, args.batch_size, env_mode)
#classifier = Classifier(seqGan.Models, 300, args.max_seq_length, args.batch_size * 2, env_mode) 

# models, fake_tok_logits, vocab_size, embed_dim, max_seq_length, batch_size, behavior):
#dnet = Discriminator(seqGan.Models, gnet.decode_b_logit, vocab_size, 300, args.max_seq_length, args.batch_size, env_mode)

#fixed_noise = torch.randn(args.batch_size, 300)

#def save_fake_tokens(tok_ids, tokenizer, batch_size, fname):
#    out_file = open(fname,'w')
#    for b in range(batch_size):
#        target_tokens = tokenizer.convert_ids_to_tokens(tok_ids[b].numpy())
#        out_file.write(' '.join(target_tokens)+'\n')
#    out_file.close()

#env_mode.Resource.InitResource()
if not os.path.exists(args.output):
    os.makedirs(args.output)

for epoch in range(args.niter):

    seqlm.Session.Init()
    
    losses = AverageMeter()
    #dis_losses = AverageMeter()

    lmnet.Session.SetTrainMode()
    bar = Bar('Training', max=len(data_loader))

    for batch_idx, data in enumerate(data_loader):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        tok_ids = data['tok_ids']
        tok_len = data['tok_len']
        label = data['label']

        # generate noise vector.
        #noise = torch.randn(args.batch_size, 300)

        #print('gnet forward')
        #fake_tok_ids, fake_tok_len = gnet.Forward(noise, args.batch_size, 300)

        #save_fake_tokens(fake_tok_ids, dataset.tokenizer, args.batch_size, 'output-tmp.txt')
        #print('train classifier')
        loss = lmnet.Train(tok_ids, tok_len, args.batch_size) #:(tok_ids, tok_len, args.batch_size, fake_tok_ids, fake_tok_len, args.batch_size)

        #print('dnet train')
        #g_loss = dnet.Train(fake_tok_len, args.batch_size)
        
        #print('gnet backward')
        #gnet.Update()

        #print('increase counter')
        #sched.Incr()

        #classifier.pred.Output.SyncToCPU()
        #print(classifier.pred.Output[0], classifier.pred.Output[1], classifier.pred.Output[2])
        #gen_losses.update(g_loss)
        losses.update(loss)

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | word Loss {losses.val:.4f} ({losses.avg:.4f}) '.format(
                    batch=batch_idx + 1,
                    size=len(data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    losses=losses
                    )
        bar.next()

    #if i % 100 == 0:
    
    #lmnet.Session.SetPredictMode()

    #fake_tok_ids, fake_tok_len = gnet.Forward(fixed_noise, args.batch_size, 300)
    
    #out_file = open('output/yelp.seqgan-'+str(epoch+1)+'.txt','w')
    #for b in range(args.batch_size):
    #    target_tokens = dataset.tokenizer.convert_ids_to_tokens(fake_tok_ids[b].numpy())
    #    out_file.write(' '.join(target_tokens)+'\n')
    #out_file.close()

    #vutils.save_image(real_cpu, '%s/real_samples.png' % opt.outf, normalize=True)    
    bar.finish()

    seqlm.Session.Complete()
    seqlm.Session.Model.Save(args.output + '/yelp.lm.'+str(epoch)+'.storm.ckpt')










