from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import io
import sys

import tokenization

import logging
import json
import math

import pickle
import numpy as np

import os
import tempfile
import argparse

import tqdm
from tqdm import tqdm, trange

import random
import collections
import operator


rf = open(sys.argv[1])

vocab = {}
for line in rf:
    _text = tokenization.convert_to_unicode(line)
    items = _text.strip().split(':')
    if(len(items) > 2 or len(items) <2):
        print(line)
    head = items[0]
    content = items[1]
    for c in content:
    	if(c in vocab):
    		vocab[c] += 1
    	else:
    		vocab[c] = 1
rf.close()

cd = sorted(vocab.items(),key=operator.itemgetter(1),reverse=True)

#sorted(vocab.items(), key = lambda kv:(kv[1], kv[0]))

wf = open(sys.argv[1]+'.vocab', 'w') 
for c in cd:
	wf.write(c[0]+'\t'+str(c[1])+'\n')
wf.close()
