from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import json
import math

import pickle
import numpy as np

import os
import tempfile
import argparse

import tqdm
from tqdm import tqdm, trange

import random
import collections

import torch
from torch.utils.data import Dataset
import tokenization


class DoubleYelpDataset(Dataset):
    def load_corpus(self, corpus_path, max_seq_length):
        token_ids = []
        with open(corpus_path, 'r') as f:
            for line in f:
                line = line.strip()
                items = line.split('\t')
                _text = tokenization.convert_to_unicode(items[0])
                _label = int(items[1])
                
                _tokens = self.tokenizer.tokenize(_text)
                _token_ids = self.tokenizer.convert_tokens_to_ids(_tokens)
                if(len(_token_ids) == 0):
                    continue

                if(len(_token_ids) >= max_seq_length):
                    _token_ids = _token_ids[ : max_seq_length]
                else:
                    _token_ids.append(0)

                token_ids.append(_token_ids)
        print('\nCorpus %s, Corpus Size %d' % (corpus_path, len(token_ids)))
        return token_ids

    ## corpus path, appending queries into history.
    def __init__(self, neg_path, pos_path, vocab_file, max_seq_length, rseed = 110):
        self.tokenizer = tokenization.FullTokenizer(vocab_file) 
        self.max_seq_length = max_seq_length

        self.pos_token_ids = self.load_corpus(pos_path, max_seq_length)
        self.neg_token_ids = self.load_corpus(neg_path, max_seq_length)

        self.rng = random.Random(rseed)

    def vocab_size(self):
        return len(self.tokenizer.vocab)

    def __len__(self):
        return max(len(self.pos_token_ids), len(self.neg_token_ids))

    def __getitem__(self, item):
        #if(self.is_enumate_doc_span):
        #    (p_id, turn_id, doc_word_num, doc_word_offset, doc_start, doc_end, tokens, segment_ids, doc_mask, query_mask, start_position, end_position, yes_no_ans) = self.doc_span_sample(item)
        #else:
        max_target_len = self.max_seq_length - 1

        pos_item = item
        if(item >= len(self.pos_token_ids)):
            pos_item = self.rng.randint(0, len(self.pos_token_ids) - 1)

        neg_item = item
        if(item >= len(self.neg_token_ids)):
            neg_item = self.rng.randint(0, len(self.neg_token_ids) - 1)


        p_ids = self.pos_token_ids[pos_item]
        n_ids = self.neg_token_ids[neg_item]
        
        p_tok_len = len(p_ids)
        n_tok_len = len(n_ids)
        
        if(p_tok_len < self.max_seq_length):
            padding = [0 for _ in range(self.max_seq_length - p_tok_len)]
            p_ids.extend(padding)

        if(n_tok_len < self.max_seq_length):
            padding = [0 for _ in range(self.max_seq_length - n_tok_len)]
            n_ids.extend(padding)

        #assert len(ids) == self.max_seq_length

        output = {"p_tok_ids" : p_ids,
                  "p_tok_len" : p_tok_len,
                  "n_tok_ids"  : n_ids,
                  "n_tok_len" :  n_tok_len}
        
        return {key: torch.tensor(value, dtype=torch.int32) for key, value in output.items()}
