from __future__ import print_function
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data

from DoublePoemDataset import DoublePoemDataset
from torch.utils.data import DataLoader

import numpy

import io
import sys

## first pretrain the generator and discriminator
parser = argparse.ArgumentParser()

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='storm lib path')

parser.add_argument('--gpu_id', type=str, default='0', help='gpu ids to use.')

parser.add_argument('--train_d0', type=str, help='style 1 dataset.')
parser.add_argument('--train_d1', type=str, help='style 2 dataset.')

#parser.add_argument('--dev_data1', type=str, help='style 1 dataset.')
#parser.add_argument('--dev_data2', type=str, help='style 1 dataset.')

parser.add_argument('--vocab', type=str, help='vocab file.')
parser.add_argument('--max_seq_length', type=int, default=256, help="maximum sequence length")

parser.add_argument("--cls_token", type=int, default=101, help="id of [cls]")

parser.add_argument('--workers', type=int, help='number of data loading workers', default=0)
parser.add_argument('--batch_size', type=int, default=64, help='input batch size')

parser.add_argument('--niter', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')

parser.add_argument('--sched_temp', type=str, default='0:1.0', help='schedule temperature for gumbel softmax')
parser.add_argument('--output', type=str, default='/data2/yelongshen/seqgan', help='data model output folder')

parser.add_argument('--in_dim', type=int, default=300, help='word embedding dim')
parser.add_argument('--hid_dim', type=int, default=300, help='rnn hidden dimension')
parser.add_argument('--style_dim', type=int, default=300, help='style embedding dim')
parser.add_argument('--layer', type=int, default=2, help='rnn layers')

#parser.add_argument('--latent_code', type=int, default=300, help='latent code dimension')

#parser.add_argument('--pre_gnet', type=str, default='', help='pretrained generator net.')

#parser.add_argument('--pre_dnet_epoch', type=int, default=0, help='discriminator pre training.')
#parser.add_argument('--pre_dnet_lr', type=float, default=0.0001, help='discriminator pre training learning rate.')

args = parser.parse_args()
print(args)

import clr as net_clr
from System import Array, IntPtr, Int32, Int64, Collections
from progress.bar import Bar as Bar

# /10T/yelongshen/biglearn/Targets
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

import BigLearn
from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior, A_Func, IntArgument, FloatArgument, RateScheduler, ResourceManager

from BigLearn import Session, EmbedStructure, LSTMStructure, LayerStructure, CompositeNNStructure, LSTMCell, Structure

from BigLearn import CudaPieceInt, CudaPieceFloat, NdArrayData, SeqDenseBatchData

from BigLearn import GradientOptimizer

#from BigLearn.DeepNet.Language import LargeBert, Bert, DistBert

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)

print("Create storm enviroument")
env_mode = DeviceBehavior(0).TrainMode

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class VaeTransferModule:
    # dual module for Generator and Discriminator 
    def __init__(self, vocab_size, in_dim, hid_dim, latent_code, layer, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)

        out_dims = [ hid_dim for _ in range(layer) ]

        self.Models = {}

        self.Models['embed'] = self.Session.Model.AddLayer(EmbedStructure(vocab_size, in_dim, self.Behavior.Device))
        self.Models['en_lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        
        self.Models['y_embed'] = self.Session.Model.AddLayer(EmbedStructure(2, in_dim, self.Behavior.Device))
        self.Models['en_o'] = self.Session.Model.AddLayer(LayerStructure(in_dim, hid_dim, A_Func.Tanh, True, self.Behavior.Device))
        self.Models['en_c'] = self.Session.Model.AddLayer(LayerStructure(in_dim, hid_dim, A_Func.Tanh, True, self.Behavior.Device))
        
        self.Models['de_o'] = self.Session.Model.AddLayer(LayerStructure(in_dim + hid_dim, hid_dim, A_Func.Tanh, True, self.Behavior.Device))
        self.Models['de_c'] = self.Session.Model.AddLayer(LayerStructure(in_dim + hid_dim, hid_dim, A_Func.Tanh, True, self.Behavior.Device))

        self.Models['gen_lstms'] = self.Session.Model.AddLayer(LSTMStructure(in_dim, out_dims, self.Behavior.Device))
        self.Models['gen_decoder'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, in_dim, A_Func.Linear, True, self.Behavior.Device))


        self.Models['adv_n_lstms'] = self.Session.Model.AddLayer(LSTMStructure(hid_dim, out_dims, self.Behavior.Device))
        self.Models['adv_n_classifier'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, 2, A_Func.Linear, True, self.Behavior.Device))

        self.Models['adv_p_lstms'] = self.Session.Model.AddLayer(LSTMStructure(hid_dim, out_dims, self.Behavior.Device))
        self.Models['adv_p_classifier'] = self.Session.Model.AddLayer(LayerStructure(hid_dim, 2, A_Func.Linear, True, self.Behavior.Device))

        self.Session.Model.AllocateGradient(self.Behavior.Device)
        self.Session.AllocateOptimizer(StructureLearner.AdamLearner(args.lr, 0.5, 0.999)) # (args.lr, 0.5, 0.999))
        
        #self.Session.AllocateOptimizer(StructureLearner.AdamLearner(args.lr, 0.5, 0.999))
## create Generator model. 

# encode sentence and style, to get latent representation of z.
class Encoder:
    def __init__(self, models, style_dim, in_dim, hid_dim, layer, max_seq_length, batch_size, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        self.in_dim = in_dim
        self.hid_dim = hid_dim
        self.layer = layer
        self.style_dim = style_dim
        self.max_seq_length = max_seq_length

        batch = IntArgument('batch', batch_size)
        seq = IntArgument('seq', max_seq_length)

        idim = IntArgument('idim', in_dim)
        hdim = IntArgument('hdim', hid_dim)
        sdim = IntArgument('sdim', style_dim)

        self.C_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.token_len = CudaPieceInt(batch_size, self.Behavior.Device)
        
        #self.Y = CudaPieceInt(batch_size, self.Behavior.Device)
        #y_embed = self.Session.LookupEmbed(self.Models['y_embed'], self.Y)

        #state_o = self.Session.FNN(y_embed, self.Models['en_o'])
        #state_c = self.Session.FNN(y_embed, self.Models['en_c'])

        #_po = [state_o for _ in range(layer)]
        #_pc = [state_c for _ in range(layer)]
        c_token_embed = self.Session.LookupEmbed(self.Models['embed'], self.C_tokens)
        c_token_embed = self.Session.Reshape(c_token_embed, idim, seq, batch)
        c_token_seq_data = self.Session.Tensor2SeqData(c_token_embed, self.token_len)
        
        self.state_z = self.Session.LSTM(self.Models['en_lstms'], c_token_seq_data)

        self.state_style = self.Session.FNN(state_z, self.Models['en_style'])
        self.state_content = self.Session.FNN(state_z, self.Models['en_content'])

    def forward(self, tok_ids, tok_len, batch_size): #Forward(self, tok_ids, tok_len, y):
        self.C_tokens.CopyTorch(IntPtr.op_Explicit(Int64(tok_ids.data_ptr())), batch_size * self.max_seq_length)
        self.token_len.CopyTorch(IntPtr.op_Explicit(Int64(tok_len.data_ptr())), batch_size)
        #self.Y.Init(style_idx) 
        self.Session.ForwardV3()

    def train(self):
        self.Session.BackwardV3()


## teacher generator.
class TeaGenerator:
    def __init__(self, models, z, style_idx, style_dim, in_dim, hid_dim, layer, max_seq_length, batch_size, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        self.max_seq_length = max_seq_length

        batch = IntArgument('batch', batch_size)
        seq = IntArgument('seq', max_seq_length)
        idim = IntArgument('idim', in_dim)
        hdim = IntArgument('hdim', hid_dim)

        ## input tokens. 
        self.I_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.O_tokens = CudaPieceInt(batch_size * max_seq_length, self.Behavior.Device)
        self.token_len = CudaPieceInt(batch_size, self.Behavior.Device)
        
        self.Y = CudaPieceInt(batch_size, self.Behavior.Device)
        self.Y.Init(style_idx)

        y_embed = self.Session.LookupEmbed(self.Models['y_embed'], self.Y)

        condition_embed = self.Session.Cat([y_embed, z], 0)

        #self.Noise = NdArrayData(behavior.Device, False, lscode, batch) 
        state_o = self.Session.FNN(condition_embed, self.Models['de_o'])
        state_c = self.Session.FNN(condition_embed, self.Models['de_c'])

        _pc = [state_c for _ in range(layer)]
        _po = [state_o for _ in range(layer)]

        i_token_embed = self.Session.LookupEmbed(self.Models['embed'], self.I_tokens)
        i_token_embed = self.Session.Reshape(i_token_embed, idim, seq, batch)
        i_token_seq_data = self.Session.Tensor2SeqData(i_token_embed, self.token_len)
        
        # LSTMEncode(LSTMStructure model, SeqDenseBatchData data, NdArrayData[] preO, NdArrayData[] preC)
        self.h = self.Session.LSTMEncode(self.Models['gen_lstms'], i_token_seq_data, _po, _pc)

        lstm_embed = self.Session.SeqData2CompactTensor(self.h)

        decode = self.Session.FNN(lstm_embed, self.Models['gen_decoder'])

        self.logit = self.Session.MatMul(decode, 0, self.Models['embed'].NDEmbed, 1)

        self.loss = self.Session.SoftmaxLoss(self.logit, self.O_tokens)        

    # p_z, 1, p_tok_ids, p_tok_len, args.batch_size)
    def generate(self, tok_ids, tok_len, batch_size):
        eff_size = 0
        for b in range(batch_size):
            self.I_tokens[b * self.max_seq_length ] = 101
            for i in range(tok_len[b] - 1):
                self.I_tokens[b * self.max_seq_length + i + 1] = tok_ids[b][i]
                self.O_tokens[eff_size] = tok_ids[b][i]
                eff_size += 1
            self.O_tokens[eff_size] = tok_ids[b][tok_len[b] - 1]
            eff_size += 1
            #assert tok_ids[b][tok_len[b] - 1] == 0

        self.I_tokens.SyncFromCPU()
        self.O_tokens.EffectiveSize = eff_size;
        self.O_tokens.SyncFromCPU()
        self.token_len.CopyTorch(IntPtr.op_Explicit(Int64(tok_len.data_ptr())),  batch_size)
        
        self.Session.ForwardV3() 
        
        return self.h
        #self.Session.Loss()
    def train(self):
        self.Session.Loss()
        self.Session.BackwardV3()

        return self.loss.Value;

class Generator:
    def __init__(self, models, z, style_id, style_dim, in_dim, hid_dim, layer, max_seq_length, batch_size, behavior):
        self.Behavior = behavior

        self.Models = models

        self.max_seq_length = max_seq_length

        batch = IntArgument('batch', batch_size)
        seq = IntArgument('seq', max_seq_length)
        
        idim = IntArgument('idim', in_dim)
        hdim = IntArgument('hdim', hid_dim)
        sdim = IntArgument('sdim', style_dim)

        self.Y = CudaPieceInt(batch_size, self.Behavior.Device)
        self.Y.Init(style_id) 

        self.Session = Session(behavior)
        
        y_embed = self.Session.LookupEmbed(self.Models['y_embed'], self.Y)

        condition_embed = self.Session.Cat([y_embed, z], 0)

        state_o = self.Session.FNN(condition_embed, self.Models['de_o'])
        state_c = self.Session.FNN(condition_embed, self.Models['de_c'])

        _pc = [state_c for _ in range(layer)]
        _po = [state_o for _ in range(layer)]

        init_tokens = [101 for _ in range(batch_size)]
        embed = self.Session.LookupEmbed(self.Models['embed'], init_tokens)

        self.decode_tokens = []
        self.decode_logit = []
        self.decode_b_logit = []
        self.hs = []

        wei = FloatArgument('gumbel_w', 0.1)
        global_update = IntArgument("global_update", 0)
        sched = RateScheduler(global_update, args.sched_temp)
        
        for i in range(max_seq_length):
            _out_lstm = self.Session.LSTMState(self.Models['gen_lstms'], _po, _pc, embed)
            _po = _out_lstm.Item1
            _pc = _out_lstm.Item2

            self.hs.append(_po[layer - 1])

            #embed = _po[layer - 1]
            decode = self.Session.FNN(_po[layer - 1], self.Models['gen_decoder'])
            logit = self.Session.MatMul(decode, 0, self.Models['embed'].NDEmbed, 1)

            _out_gumbel = self.Session.GumbelSoftmax(logit, sched, wei, True)
            b_logit = _out_gumbel.Item1
            tok_ids = _out_gumbel.Item2

            self.decode_logit.append(logit)
            self.decode_b_logit.append(b_logit)
            self.decode_tokens.append(tok_ids)

            tok_embed = self.Session.MatMul(b_logit, 0, self.Models['embed'].NDEmbed, 0)
            embed = tok_embed


        self.token_lens = CudaPieceInt(batch_size, self.Behavior.Device)
        
        self.Session2 = Session(behavior)
        h = self.Session2.Cat(self.hs, 0)
        h = self.Session2.Reshape(h, hdim, seq, batch)
        self.h = self.Session2.Tensor2SeqData(h, self.token_lens)

    def generate(self, batch_size):
        self.Session.ForwardV3()

        tok_ids = torch.zeros([batch_size, self.max_seq_length], dtype = torch.int32)
        tok_len = torch.zeros([batch_size], dtype = torch.int32)

        for i in range(self.max_seq_length):
            self.decode_tokens[i].SyncToCPU()
            for b in range(batch_size):
                if(self.decode_tokens[i][b] == 0 and tok_len[b] == 0):
                    tok_len[b] == i + 1

                if(tok_len[b] == 0):
                    tok_ids[b][i] = self.decode_tokens[i][b]
                else:
                    tok_ids[b][i] = 0

        for b in range(batch_size):
            if(tok_len[b] == 0):
                tok_len[b] = self.max_seq_length

        self.token_lens.CopyTorch(IntPtr.op_Explicit(Int64(tok_len.data_ptr())), batch_size)
        self.Session2.ForwardV3()

        return tok_ids, tok_len, self.h

    def train(self):
        self.Session2.BackwardV3()
        self.Session.BackwardV3()


class AdvClassifier:
    def __init__(self, models, h, np_point, label_idx, style_dim, in_dim, hid_dim, layer, max_seq_length, batch_size, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        self.H = h
        self.max_seq_length = max_seq_length
        
        self.labels = CudaPieceInt(batch_size, self.Behavior.Device)
        self.labels.Init(label_idx)

        if(np_point == 0):
            adv = self.Session.LSTM(self.Models['adv_n_lstms'], h)
            self.pred = self.Session.FNN(adv, self.Models['adv_n_classifier'].NDWeight, self.Models['adv_n_classifier'].NDBias, self.Models['adv_n_classifier'].Af)
        elif(np_point == 1):
            adv = self.Session.LSTM(self.Models['adv_p_lstms'], h)
            self.pred = self.Session.FNN(adv, self.Models['adv_p_classifier'].NDWeight, self.Models['adv_p_classifier'].NDBias, self.Models['adv_p_classifier'].Af)

        #self.pred = self.Session.FNN(adv, self.Models['adv_classifier'])
        self.loss = self.Session.SoftmaxLoss(self.pred, self.labels)        

    def train(self, batch_size):
        self.Session.StepV3Run()
        self.H.SentDeriv.Zero()
        return self.loss.Value

class AdvDiscriminator:
    def __init__(self, models, h, np_point, label_idx, style_dim, in_dim, hid_dim, layer, max_seq_length, batch_size, behavior):
        self.Behavior = behavior

        self.Session = Session(behavior)
        self.Models = models

        self.H = h
        self.max_seq_length = max_seq_length
        
        self.labels = CudaPieceInt(batch_size, self.Behavior.Device)
        self.labels.Init(label_idx)

        if(np_point == 0):
            adv = self.Session.LSTM(self.Models['adv_n_lstms'], h, False)
            self.pred = self.Session.FNN(adv, self.Models['adv_n_classifier'].NDWeight.NoGrad(), self.Models['adv_n_classifier'].NDBias.NoGrad(), self.Models['adv_n_classifier'].Af)
        elif(np_point == 1):
            adv = self.Session.LSTM(self.Models['adv_p_lstms'], h, False)
            self.pred = self.Session.FNN(adv, self.Models['adv_p_classifier'].NDWeight.NoGrad(), self.Models['adv_p_classifier'].NDBias.NoGrad(), self.Models['adv_p_classifier'].Af)
            
        self.loss = self.Session.SoftmaxLoss(self.pred, self.labels)        

    def train(self, batch_size):
        self.Session.StepV3Run()
        return self.loss.Value

print('loading dataset ...', args.train_d0, args.train_d1)
dataset = DoublePoemDataset(args.train_d0, args.train_d1, args.vocab, args.max_seq_length)
data_loader = DataLoader(dataset, batch_size=args.batch_size, num_workers=args.workers, shuffle=True, drop_last=True)

vocab_size = dataset.vocab_size()
print('vocab size', vocab_size)


vae_transfer_module = VaeTransferModule(vocab_size, args.style_dim, args.in_dim, args.hid_dim, args.layer, env_mode)

# style_dim, in_dim, hid_dim, layer, max_seq_length, batch_size, behavior):
p_encoder = Encoder(vae_transfer_module.Models, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
n_encoder = Encoder(vae_transfer_module.Models, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)

# teacher generator.
p_tea_generator = TeaGenerator(vae_transfer_module.Models, p_encoder.state_z, 1, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
n_tea_generator = TeaGenerator(vae_transfer_module.Models, n_encoder.state_z, 0, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)

p_hat_generator = Generator(vae_transfer_module.Models, n_encoder.state_z, 1, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
n_hat_generator = Generator(vae_transfer_module.Models, p_encoder.state_z, 0, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)

p_real_classifier = AdvClassifier(vae_transfer_module.Models, p_tea_generator.h, 1, 1, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
p_fake_classifier = AdvClassifier(vae_transfer_module.Models, p_hat_generator.h, 1, 0, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
p_n_classifier = AdvClassifier(vae_transfer_module.Models, n_tea_generator.h, 1, 0, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)

n_real_classifier = AdvClassifier(vae_transfer_module.Models, n_tea_generator.h, 0, 1, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
n_fake_classifier = AdvClassifier(vae_transfer_module.Models, n_hat_generator.h, 0, 0, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
n_p_classifier = AdvClassifier(vae_transfer_module.Models, p_tea_generator.h, 0, 0, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)

#p_real_discriminator = AdvDiscriminator(vae_transfer_module.Models, p_tea_generator.h, 0, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
p_fake_discriminator = AdvDiscriminator(vae_transfer_module.Models, p_hat_generator.h, 1, 1, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)
n_fake_discriminator = AdvDiscriminator(vae_transfer_module.Models, n_hat_generator.h, 0, 1, args.style_dim, args.in_dim, args.hid_dim, args.layer, args.max_seq_length, args.batch_size, env_mode)


def generate(data_loader, dataset, vae_transfer, p_encoder, n_encoder, p_hat_generator, n_hat_generator, fname, behavior):
    test_num = 5

    out_file = open(fname,'w')
    for batch_idx, data in enumerate(data_loader):
        p_tok_ids = data['p_tok_ids']
        p_tok_len = data['p_tok_len']
        n_tok_ids = data['n_tok_ids']
        n_tok_len = data['n_tok_len']


        z_p = p_encoder.encode(p_tok_ids, p_tok_len, 1, args.batch_size)
        z_n = n_encoder.encode(n_tok_ids, n_tok_len, 0, args.batch_size)

        p_hat_tok_ids, p_hat_tok_len, h_p_hat = p_hat_generator.generate(args.batch_size)
        n_hat_tok_ids, n_hat_tok_len, h_n_hat = n_hat_generator.generate(args.batch_size)


        for b in range(args.batch_size):
            p_tokens = dataset.ids_to_text(p_tok_ids[b].numpy())
            

            n_tokens = dataset.ids_to_text(n_tok_ids[b].numpy())

            n2p_tokens = dataset.ids_to_text(p_hat_tok_ids[b].numpy())
            p2n_tokens = dataset.ids_to_text(n_hat_tok_ids[b].numpy())

            out_file.write(' '.join(p_tokens) + '\t' + ' '.join(p2n_tokens) + '\n')
            out_file.write(' '.join(n_tokens) + '\t' + ' '.join(n2p_tokens) + '\n\n')

        test_num -= 1
        if(test_num == 0):
            break
    
    out_file.close()


def train_joint(data_loader, vae_transfer, p_encoder, n_encoder, p_tea_generator, n_tea_generator, p_hat_generator, n_hat_generator, p_n_classifier, n_p_classifier, p_real_classifier, p_fake_classifier, n_real_classifier, n_fake_classifier, p_fake_discriminator, n_fake_discriminator, behavior):
    bar = Bar('Training', max=len(data_loader))
    
    behavior.SetTrainMode()

    p_rec_losses = AverageMeter()
    n_rec_losses = AverageMeter()

    p_real_losses = AverageMeter()
    p_fake_losses = AverageMeter()

    n_real_losses = AverageMeter()
    n_fake_losses = AverageMeter()

    p_fake_dis_losses = AverageMeter()
    n_fake_dis_losses = AverageMeter()

    p_n_losses = AverageMeter()
    n_p_losses = AverageMeter()

    n_critis = 5

    for batch_idx, data in enumerate(data_loader):
        ############################
        # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
        ###########################
        p_tok_ids = data['p_tok_ids']
        p_tok_len = data['p_tok_len']
        n_tok_ids = data['n_tok_ids']
        n_tok_len = data['n_tok_len']
        
        #noise = torch.randn(args.batch_size, args.latent_code)
        ## encode p_z & n_z
        z_p = p_encoder.encode(p_tok_ids, p_tok_len, 1, args.batch_size)
        z_n = n_encoder.encode(n_tok_ids, n_tok_len, 0, args.batch_size)

        ## auto decoder for p_tok_ids & n_tok_ids
        h_p = p_tea_generator.generate(p_tok_ids, p_tok_len, args.batch_size)
        h_n = n_tea_generator.generate(n_tok_ids, n_tok_len, args.batch_size)

        ## style transfered-decoder for p_tok_ids & n_tok_ids
        p_hat_tok_ids, p_hat_tok_len, h_p_hat = p_hat_generator.generate(args.batch_size)
        n_hat_tok_ids, n_hat_tok_len, h_n_hat = n_hat_generator.generate(args.batch_size)

        ## train classifier.
        p_real_loss = p_real_classifier.train(args.batch_size) 
        #p_fake_loss = p_fake_classifier.train(args.batch_size) 
        p_fake_loss = 0

        n_real_loss = n_real_classifier.train(args.batch_size)
        #n_fake_loss = n_fake_classifier.train(args.batch_size)
        n_fake_loss = 0

        p_n_loss = p_n_classifier.train(args.batch_size)
        n_p_loss = n_p_classifier.train(args.batch_size)

        p_fake_dis_loss = p_fake_discriminator.train(args.batch_size)
        n_fake_dis_loss = n_fake_discriminator.train(args.batch_size)

        p_hat_generator.train()
        n_hat_generator.train()

        p_tea_loss = p_tea_generator.train()
        n_tea_loss = n_tea_generator.train()

        p_encoder.train()
        n_encoder.train()

        p_rec_losses.update(p_tea_loss)
        n_rec_losses.update(n_tea_loss)

        p_real_losses.update(p_real_loss)
        p_fake_losses.update(p_fake_loss)

        n_real_losses.update(n_real_loss)
        n_fake_losses.update(n_fake_loss)

        p_fake_dis_losses.update(p_fake_dis_loss)
        n_fake_dis_losses.update(n_fake_dis_loss)

        p_n_losses.update(p_n_loss)
        n_p_losses.update(n_p_loss)

        ## model update.
        vae_transfer.Session.StepV3Update()
        #p_h, loss_p_r = p_tea_generator.generate(p_z, 1, p_tok_ids, p_tok_len, args.batch_size) ## calculate p_h and gradient gradient
        #n_h_hat = n_generate.generate(p_z, 0, args.batch_size)
        
        #n_z = n_encoder.encode(n_tok_ids, n_tok_len, 0, args.batch_size)
        #n_h, loss_n_r = n_tea_generator.generate(n_z, 0, n_tok_ids, n_tok_len, args.batch_size)
        #p_h_hat = p_generate.generate(n_z, 1, args.batch_size)

        ## calculate gradient on classifiers.
        #c_p = p_classifier.train(p_h, p_h_hat, args.batch_size) 
        #c_n = n_classifier.train(n_h, n_h_hat, args.batch_size)


        #dis_p = p_discriminator.train(p_h_hat, args.batch_size)
        #dis_n = n_discriminator.train(n_h_hat, args.batch_size)

        #p_generate.backward()
        #n_tea_generator.backward()
        #n_encoder.backward()

        #n_generate.backward()
        #p_tea_generator.backward()
        #p_encoder.backward()

        # generate noise vector.
        #if(n_critis > 0):                
        #    fake_tok_ids, fake_tok_len = gnet.Forward(noise, args.batch_size)
        #    d_loss = classifier.Train(tok_ids, tok_len, args.batch_size, fake_tok_ids, fake_tok_len, args.batch_size)
        #    dis_losses.update(d_loss)
        #    n_critis -= 1
        #if(n_critis == 0):
        #    g_loss = dnet.Train(fake_tok_len, args.batch_size)
        #    gnet.Update()
        #    gen_losses.update(g_loss)
        #    n_critis = 5

        #sched.Incr()

        #classifier.pred.Output.SyncToCPU()
        #print(classifier.pred.Output[0], classifier.pred.Output[1], classifier.pred.Output[2])
        #gen_losses.update(g_loss)
        #dis_losses.update(d_loss)

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | p_n_losses {p_n_losses.val:.4f} ({p_n_losses.avg:.4f}) | n_p_losses {n_p_losses.val:.4f} ({n_p_losses.avg:.4f}) | p_rec {p_rec_losses.val:.4f} ({p_rec_losses.avg:.4f}) | n_rec {n_rec_losses.val:.4f} ({n_rec_losses.avg:.4f}) | p_real {p_real_losses.val:.4f} ({p_real_losses.avg:.4f})  | p_fake {p_fake_losses.val:.4f} ({p_fake_losses.avg:.4f}) | n_real {n_real_losses.val:.4f} ({n_real_losses.avg:.4f}) | n_fake {n_fake_losses.val:.4f} ({n_fake_losses.avg:.4f}) | p_fake_dis {p_fake_dis_losses.val:.4f} ({p_fake_dis_losses.avg:.4f}) | n_fake_dis {n_fake_dis_losses.val:.4f} ({n_fake_dis_losses.avg:.4f})  '.format(
                    batch=batch_idx + 1,
                    size=len(data_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    
                    p_n_losses=p_n_losses,
                    n_p_losses=n_p_losses,

                    p_rec_losses=p_rec_losses,
                    n_rec_losses=n_rec_losses,
                    p_real_losses=p_real_losses,
                    p_fake_losses=p_fake_losses,
                    n_real_losses=n_real_losses,
                    n_fake_losses=n_fake_losses,
                    p_fake_dis_losses=p_fake_dis_losses,
                    n_fake_dis_losses=n_fake_dis_losses,
                    )
        bar.next()

env_mode.Resource.InitResource()

if not os.path.exists(args.output):
    os.makedirs(args.output)

#generation_gnet(gnet, fixed_noise, args.batch_size, dataset.tokenizer, args.output + '/yelp.init.txt')

#classifier.Session.AdjustLR(args.pre_dnet_lr)
#for epoch in range(args.pre_dnet_epoch):
#    train_classifier(data_loader, gnet, classifier)
#classifier.Session.AdjustLR(args.lr)

generate(data_loader, dataset, vae_transfer_module, p_encoder, n_encoder, p_hat_generator, n_hat_generator, args.output + '/yelp.init.txt', env_mode)

for epoch in range(args.niter):
    
    print('epoch ', epoch)
    # def train_joint(data_loader, vae_transfer, p_encoder, n_encoder, p_tea_generator, n_tea_generator, behavior):
#def train_joint(data_loader, vae_transfer, p_encoder, n_encoder, p_tea_generator, n_tea_generator, p_hat_generator, n_hat_generator, p_real_classifier, p_fake_classifier, n_real_classifier, n_fake_classifier, p_fake_discriminator, n_fake_discriminator, behavior):
    train_joint(data_loader, vae_transfer_module, p_encoder, n_encoder, p_tea_generator, n_tea_generator, p_hat_generator, n_hat_generator, p_n_classifier, n_p_classifier, p_real_classifier, p_fake_classifier, n_real_classifier, n_fake_classifier, p_fake_discriminator, n_fake_discriminator, env_mode)
    #train_classifier(data_loader, gnet, classifier, 10)

    generate(data_loader, dataset, vae_transfer_module, p_encoder, n_encoder, p_hat_generator, n_hat_generator, args.output + '/yelp.'+str(epoch)+'.txt', env_mode)

    #train_generator(gnet, dnet, 10)

    #if epoch % 100 == 0:
    #generation_gnet(gnet, fixed_noise, args.batch_size, dataset.tokenizer, args.output + '/yelp.seqgan-'+str(epoch)+'.txt')
    #seqGan.Session.Model.Save(args.output + '/yelp.seqgan.'+str(epoch)+'.storm.ckpt')

    #seqGan.Session.Complete()
        









