import argparse
import os
import random
import shutil
import time
import warnings
import sys

import torch
import torch.nn as nn
import torch.nn.parallel
#import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models

import io
import clr
sys.path.append('/10T/yelongshen/storm') #/home/yelongshen/biglearn/biglearn/Targets')
clr.AddReference('BigLearn')
clr.AddReference('BigLearn.DeepNet')

import System
import BigLearn
import BigLearn.DeepNet
import BigLearn.DeepNet.Image
from BigLearn.DeepNet.Image import VGG11Net
from System import Array
import numpy
from System import IntPtr, Int32, Int64

from BigLearn import DeviceType
from BigLearn import DNNRunMode
from BigLearn import DeviceBehavior
from BigLearn import RunnerBehavior
from BigLearn import Logger

from BigLearn.DeepNet import OptimizerParameters

model_names = sorted(name for name in models.__dict__
    if name.islower() and not name.startswith("__")
    and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='BigLearn VGG11 ImageNet Training')
parser.add_argument('data', metavar='DIR',
                    help='path to dataset')
parser.add_argument('--arch', '-a', metavar='ARCH', default='resnet18',
                    choices=model_names,
                    help='model architecture: ' +
                        ' | '.join(model_names) +
                        ' (default: resnet18)')

parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')

parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--weight-decay', '--wd', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)')

parser.add_argument('--resume', default='', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                    help='use pre-trained model')
parser.add_argument('--world-size', default=1, type=int,
                    help='number of distributed processes')
parser.add_argument('--dist-url', default='tcp://224.66.41.62:23456', type=str,
                    help='url used to set up distributed training')
parser.add_argument('--dist-backend', default='gloo', type=str,
                    help='distributed backend')
parser.add_argument('--seed', default=None, type=int,
                    help='seed for initializing training. ')

parser.add_argument('--gpu', default=None, type=int,
                    help='GPU id to use.')
parser.add_argument('--config', default='', type=str, metavar='PATH',
                    help='path to the optimizer config')
parser.add_argument('-b', '--batch-size', default=32, type=int,
                    metavar='N', help='mini-batch size (default: 256)')

parser.add_argument('--epochs', default=90, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')

parser.add_argument('--print-freq', '-p', default=10, type=int,
                    metavar='N', help='print frequency (default: 20)')

best_acc1 = 0


def main():
    global args, best_acc1
    args = parser.parse_args()

    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        #cudnn.deterministic = True
        warnings.warn('You have chosen to seed training. '
                      'This will turn on the CUDNN deterministic setting, '
                      'which can slow down your training considerably! '
                      'You may see unexpected behavior when restarting '
                      'from checkpoints.')

    if args.gpu is not None:
        warnings.warn('You have chosen a specific GPU. This will completely '
                      'disable data parallelism.')
    print('gpu id',args.gpu)
    #return
    args.distributed = args.world_size > 1
    
    OptimizerParameters.Parse(args.config);
    Logger.OpenLog(OptimizerParameters.LogFile);

    # setup up running enviroument.
    behavior = DeviceBehavior(args.gpu).TrainMode;
    
    # create model and optimizer.
    print('create vgg11 model and its optimizer.')
    model = VGG11Net.VGG11Model(behavior.Device)
    model.InitOptimizer(behavior)
    
    #return
    print('create training computation graph.')
    vgg11 = VGG11Net(model, behavior)
    vgg11.BuildCG(args.batch_size)

    #print('create validation computation graph.')
    #valider = VGG11Net(model, behavior.PredictMode)
    #valider.BuildCG(args.batch_size)

    #test_x = numpy.array([0.1, 0.2, 0.3, 0.4, 0.5])
    #for i in range(0, 5):
    #    test_r = trainer.testSoftmax(test_x, 5)
    #    print(test_r)
    #    for tmp_r in test_r:
    #        print(tmp_r)
    #test_r = trainer.testSum(test_x, 5)
    #print(test_r)
    #return

    #x = torch.tensor([ [[0,1,2,3,4],[1,2,4,5,6],[1,3,4,5,6]], [[0,1,2,3,4],[1,2,4,5,6],[1,3,4,5,6]] ] , dtype=torch.float) #, device='cuda:0')
    #i = Int64(x.data_ptr())
    #p = IntPtr.op_Explicit(i)
    #trainer.testPointer(p, 30)
    #print(x)
    #return
    
    #return
    #if args.distributed:
    #    dist.init_process_group(backend=args.dist_backend, init_method=args.dist_url,
    #                            world_size=args.world_size)

    # create model
    #if args.pretrained:
    #    print("=> using pre-trained model '{}'".format(args.arch))
    #    model = models.__dict__[args.arch](pretrained=True)
    #else:
    #    print("=> creating model '{}'".format(args.arch))
    #    model = models.__dict__[args.arch]()
    

    #if args.gpu is not None:
    #    model = model.cuda(args.gpu)
    #elif args.distributed:
    #    model.cuda()
    #    model = torch.nn.parallel.DistributedDataParallel(model)
    #else:
    #    if args.arch.startswith('alexnet') or args.arch.startswith('vgg'):
    #        model.features = torch.nn.DataParallel(model.features)
    #        model.cuda()
    #    else:
    #        model = torch.nn.DataParallel(model).cuda()

    # define loss function (criterion) and optimizer
    #criterion = nn.CrossEntropyLoss().cuda(args.gpu)

    #optimizer = torch.optim.SGD(model.parameters(), args.lr,
    #                            momentum=args.momentum,
    #                            weight_decay=args.weight_decay)

    # optionally resume from a checkpoint
    #if args.resume:
    #    if os.path.isfile(args.resume):
    #        print("=> loading checkpoint '{}'".format(args.resume))
    #        checkpoint = torch.load(args.resume)
    #        args.start_epoch = checkpoint['epoch']
    #        best_acc1 = checkpoint['best_acc1']
    #        model.load_state_dict(checkpoint['state_dict'])
    #        optimizer.load_state_dict(checkpoint['optimizer'])
    #        print("=> loaded checkpoint '{}' (epoch {})"
    #              .format(args.resume, checkpoint['epoch']))
    #    else:
    #        print("=> no checkpoint found at '{}'".format(args.resume))

    #cudnn.benchmark = True

    # Data loading code
    traindir = os.path.join(args.data, 'train')
    valdir = os.path.join(args.data, 'val')
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    train_dataset = datasets.ImageFolder(
        traindir,
        transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            normalize,
        ]))

    #if args.distributed:
    #    train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    #else:
    train_sampler = None

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=True, #(train_sampler is None),
        num_workers=4, pin_memory=True, sampler=train_sampler, drop_last=True)

    val_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(valdir, transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=True,
        num_workers=4, pin_memory=True, drop_last=True)

    #if args.evaluate:
    #validate(val_loader, valider, 0)
    #    return
    print('start model training.')
    for epoch in range(args.start_epoch, args.epochs):
        #if args.distributed:
        #    train_sampler.set_epoch(epoch)
        #adjust_learning_rate(optimizer, epoch)

        vgg11.SetTrainMode()
        # train for one epoch
        vgg11.Init()
        train(train_loader, vgg11, epoch)
        vgg11.Complete()
        # evaluate on validation set
        
        vgg11.SaveModel(epoch)
        
        vgg11.SetPredictMode()
        vgg11.Init()
        acc1 = validate(val_loader, vgg11, epoch)
        vgg11.Complete()

        # remember best acc@1 and save checkpoint
        is_best = acc1 > best_acc1
        best_acc1 = max(acc1, best_acc1)
        
        #if(is_best):
        #    vgg11.SaveModel(epoch)
        #save_checkpoint({
        #    'epoch': epoch + 1,
        #    'arch': args.arch,
        #    'state_dict': model.state_dict(),
        #    'best_acc1': best_acc1,
        #    'optimizer' : optimizer.state_dict(),
        #}, is_best)

def train(train_loader, trainer, epoch):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    global args

    output = torch.zeros([ args.batch_size, 1000 ], dtype=torch.float)
    p_output = IntPtr.op_Explicit(Int64(output.data_ptr()))

    # switch to train mode
    # model.train()

    end = time.time()
    for i, (input, target) in enumerate(train_loader):
        # measure data loading time

        #if args.gpu is not None:
        #    input = input.cuda(args.gpu, non_blocking=True)
        #target = target.cuda(args.gpu, non_blocking=True)
        #flat_input = input.numpy().flatten()
        #flat_target = target.numpy().flatten()
        #print(len(flat_input))
        #print(len(flat_target))
        #print(target.dtype)
        #print(input.dtype)
        
        p_input = IntPtr.op_Explicit(Int64(input.data_ptr()))
        p_target = IntPtr.op_Explicit(Int64(target.data_ptr()))
        
        data_time.update(time.time() - end)
        
        loss = trainer.Run(p_input, p_target, p_output, 0, args.batch_size)
        # compute output
        #output = model(input)
        #loss = criterion(output, target)
        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        
        losses.update(loss)
        top1.update(acc1[0], input.size(0))
        top5.update(acc5[0], input.size(0))

            # compute gradient and do SGD step
            #optimizer.zero_grad()
            #loss.backward()
            #optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            print('Epoch: [{0}][{1}/{2}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                  'Loss {loss.val:.6f} ({loss.avg:.6f})\t'
                  'Acc@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                  'Acc@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                   epoch, i, len(train_loader), batch_time=batch_time,
                   data_time=data_time, loss=losses, top1=top1, top5=top5))
        #break

def validate(val_loader, valider, epoch):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    # switch to evaluate mode
    #model.eval()
    global args

    output = torch.zeros([ args.batch_size, 1000 ], dtype=torch.float)
    p_output = IntPtr.op_Explicit(Int64(output.data_ptr()))

    end = time.time()
    for i, (input, target) in enumerate(val_loader):
        #if args.gpu is not None:
            #input = input.cuda(args.gpu, non_blocking=True)
            #target = target.cuda(args.gpu, non_blocking=True)
        p_input = IntPtr.op_Explicit(Int64(input.data_ptr()))
        p_target = IntPtr.op_Explicit(Int64(target.data_ptr()))

        #for idx in range(0, 64):

        data_time.update(time.time() - end)

        loss = valider.Run(p_input, p_target, p_output, 0, args.batch_size)

        losses.update(loss)
        # compute output
        # output = model(input)
        # loss = criterion(output, target)

        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        top1.update(acc1[0], input.size(0))
        top5.update(acc5[0], input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            print('Test: [{0}/{1}]\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Loss {loss.val:.6f} ({loss.avg:.6f})\t'
                  'Acc@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                  'Acc@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                   i, len(val_loader), data_time=data_time, batch_time=batch_time, loss = losses, top1=top1, top5=top5))

    print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'.format(top1=top1, top5=top5))
    return top1.avg


def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'model_best.pth.tar')

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * (0.1 ** (epoch // 30))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    #with torch.no_grad():
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res

if __name__ == '__main__':
    main()
