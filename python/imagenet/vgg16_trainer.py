import argparse
import os
import random
import shutil
import time
import warnings
import sys

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.multiprocessing as mp
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models

import io
import clr

import numpy as np
import clr as net_clr
from System import Array, IntPtr, Int32, Int64

from progress.bar import Bar as Bar

#model_names = sorted(name for name in models.__dict__
#    if name.islower() and not name.startswith("__")
#    and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='BigLearn ImageNet Training')

parser.add_argument('--traindir', metavar='DIR', help='path to training dataset')
parser.add_argument('--valdir', metavar='DIR', help='path to validation dataset')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')

parser.add_argument('-j', '--workers', default=0, type=int, metavar='N', help='number of data loading workers (default: 4)')

parser.add_argument('-b', '--batch_size', default=256, type=int,
                    metavar='N',
                    help='mini-batch size (default: 256), this is the total '
                         'batch size of all GPUs on the current node when '
                         'using Data Parallel or Distributed Data Parallel')

parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument("--seed_model", type=str, default="", help="seed model")

parser.add_argument('--epochs', default=90, type=int, metavar='N', help='number of total epochs to run')

parser.add_argument('--start_epoch', default=0, type=int, metavar='N', help='manual epoch number (useful on restarts)')

parser.add_argument('--lr', '--learning_rate', default=0.1, type=float, metavar='LR', help='initial learning rate', dest='lr')

parser.add_argument('--momentum', default=0.9, type=float, metavar='M', help='momentum')

parser.add_argument('--wd', '--weight_decay', default=1e-4, type=float, metavar='W', help='weight decay (default: 1e-4)', dest='weight_decay')

parser.add_argument('--output_model', default='vgg16', type=str, help='output_model path')

parser.add_argument('--decay_epoch', default=30, type=int, help='weight decay epoches')
parser.add_argument('--decay_rate', default=0.1, type=float, help='decay learning rate')
args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior
from BigLearn.DeepNet.Image import ImageNet_VGG16Net, DistImageNet_VGG16Net

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)


def main():
    # [123.68, 116.779, 103.939]
    # [123.68, 116.78, 103.94]
    #mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
    #normalize = transforms.Normalize(mean=[0.485, 0.458, 0.408], std=[1.0/255, 1.0/255, 1.0/255])
    #question 
    
    #transform_train = transforms.Compose([
    #    transforms.RandomCrop(32, padding=4),
    #    transforms.RandomHorizontalFlip(),
    #    transforms.ToTensor(),
    #    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    #])

    train_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(args.traindir, transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.458, 0.408], std=[1, 1, 1]), #0.229, 0.224, 0.225]),
        ])),
        batch_size=args.batch_size, shuffle=True, num_workers=args.workers, pin_memory=False, drop_last=True)

    val_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(args.valdir, transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.458, 0.408], std=[1, 1, 1]), #[0.229, 0.224, 0.225]),
        ])),
        batch_size=args.batch_size, shuffle=False, num_workers=args.workers, pin_memory=False, drop_last=True)

    model = ImageNet_VGG16Net.VGG16Model(1000, DeviceType.CPU) # env_mode.Device)  # args.weight_decay

    if(args.seed_model != ""):
        model.Load(args.seed_model)

    net = DistImageNet_VGG16Net(model, StructureLearner.SGDLearner(args.lr, args.momentum, 0.0), deviceNum)

    net.BuildCG(224, 224, 3, args.batch_size)

    # SGDLearner(float lr, float gradclip, string schedulelr, float momentum, float decay)
    print('start model training.')
    best_acc1 = 0
    for epoch in range(args.start_epoch, args.epochs):

        adjust_learning_rate(net, epoch, args)

        # train for one epoch
        train(train_loader, net, args)

        # evaluate on validation set
        acc1 = validate(val_loader, net, args)

        # remember best acc@1 and save checkpoint
        is_best = acc1 > best_acc1

        best_acc1 = max(acc1, best_acc1)

        net.SaveModel(os.path.join(args.output_model, 'vgg16.' + str(epoch) + '.model'))

def train(train_loader, net, args):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    output = torch.zeros([ args.batch_size, 1000 ], dtype=torch.float)
    p_output = IntPtr.op_Explicit(Int64(output.data_ptr()))

    net.SetTrainMode()
    # train for one epoch
    net.Init()

    end = time.time()
    
    bar = Bar('Training', max=len(train_loader))

    for i, (input, target) in enumerate(train_loader):
        p_input = IntPtr.op_Explicit(Int64(input.data_ptr()))
        p_target = IntPtr.op_Explicit(Int64(target.data_ptr()))
        
        data_time.update(time.time() - end)

        loss = net.Train(p_input, p_target, p_output, args.batch_size)
        
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        
        losses.update(loss)
        top1.update(acc1[0], input.size(0))
        top5.update(acc5[0], input.size(0))

        batch_time.update(time.time() - end)
        end = time.time()

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Batch Time {batch_time.val:.3f} ({batch_time.avg:.3f}) | Data Time {data_time.val:.3f} ({data_time.avg:.3f}) | Loss {losses.val:.4f} ({losses.avg:.4f}) | Acc@1 {top1.val:.3f} ({top1.avg:.3f}) | Acc@5 {top5.val:.3f} ({top5.avg:.3f}) '.format(
                    batch = i + 1,
                    size = len(train_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    batch_time=batch_time,
                    data_time=data_time,
                    losses=losses,
                    top1=top1,
                    top5=top5
                    )
        bar.next()

    net.Complete()

def validate(val_loader, net, args):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    output = torch.zeros([ args.batch_size, 1000 ], dtype=torch.float)
    p_output = IntPtr.op_Explicit(Int64(output.data_ptr()))

    net.SetPredictMode()
    # train for one epoch
    net.Init()

    end = time.time()
    
    bar = Bar('Validation', max=len(val_loader))

    for i, (input, target) in enumerate(val_loader):
        
        p_input = IntPtr.op_Explicit(Int64(input.data_ptr()))
        p_target = IntPtr.op_Explicit(Int64(target.data_ptr()))
        
        #for idx in range(0, 1):
        data_time.update(time.time() - end)

        net.Predict(p_input, p_output, args.batch_size)
        
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        
        top1.update(acc1[0], input.size(0))
        top5.update(acc5[0], input.size(0))

            # compute gradient and do SGD step
            #optimizer.zero_grad()
            #loss.backward()
            #optimizer.step()

            # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Batch Time {batch_time.val:.3f} ({batch_time.avg:.3f}) | Data Time {data_time.val:.3f} ({data_time.avg:.3f}) | Acc@1 {top1.val:.3f} ({top1.avg:.3f}) | Acc@5 {top5.val:.3f} ({top5.avg:.3f}) '.format(
                    batch = i + 1,
                    size = len(val_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    batch_time=batch_time,
                    data_time=data_time,
                    top1=top1,
                    top5=top5
                    )
        bar.next()

    net.Complete()
    return top1.avg


def save_checkpoint(state, filename='checkpoint.pth.tar'):
    torch.save(state, filename)

def adjust_learning_rate(net, epoch, args):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * (args.decay_rate ** (epoch // args.decay_epoch))
    print('learning rate :', lr, 'epoch : ', epoch)
    net.AdjustLR(lr)

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


if __name__ == '__main__':
    main()
