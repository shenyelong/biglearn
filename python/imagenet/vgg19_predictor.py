import argparse
import os
import random
import shutil
import time
import warnings
import sys

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.multiprocessing as mp
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models

import io
import clr

import numpy as np
import clr as net_clr
from System import Array, IntPtr, Int32, Int64

from progress.bar import Bar as Bar

#model_names = sorted(name for name in models.__dict__
#    if name.islower() and not name.startswith("__")
#    and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='BigLearn ImageNet Predicting')

parser.add_argument('--valdir', metavar='DIR', help='path to validation dataset')

parser.add_argument('-l', '--lib', default='/home/yelongshen/biglearn/biglearn/Targets', type=str, help='biglearn lib path')

parser.add_argument('-j', '--workers', default=0, type=int, metavar='N', help='number of data loading workers (default: 4)')

parser.add_argument('-b', '--batch_size', default=256, type=int,
                    metavar='N',
                    help='mini-batch size (default: 256), this is the total '
                         'batch size of all GPUs on the current node when '
                         'using Data Parallel or Distributed Data Parallel')

parser.add_argument("-g", "--gpu_id", type=str, default="0", help="gpu id")

parser.add_argument("--seed_model", type=str, default="seed_model", help="seed model")

args = parser.parse_args()

### please try best to re-use the pytorch io.
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')

from BigLearn import StructureLearner, DeviceType, DNNRunMode, RunnerBehavior, DeviceBehavior
from BigLearn.DeepNet.Image import ImageNet_VGG19Net, DistImageNet_VGG19Net

deviceNum = len(args.gpu_id.split(','))
print("set gpu number ", deviceNum)
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu_id)


def main():
    # [123.68, 116.779, 103.939]
    # [123.68, 116.78, 103.94]
    #mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
    #normalize = transforms.Normalize(mean=[0.485, 0.458, 0.408], std=[1.0/255, 1.0/255, 1.0/255])
    #question 
    val_loader = torch.utils.data.DataLoader(
        datasets.ImageFolder(args.valdir, transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.458, 0.408], std=[1.0, 1.0, 1.0]),
        ])),
        batch_size=args.batch_size, shuffle=False, num_workers=args.workers, pin_memory=True, drop_last=True)

    # int category, string meta_file, string bin_file, DeviceType device
    model = ImageNet_VGG19Net.VGG19Model(1000, args.seed_model + '.label', args.seed_model + '.bin', DeviceType.CPU) # env_mode.Device)

    #int width, int height, int channel, int batchSize)
    # ImageNet_VGG16Net.VGG16Model model, StructureLearner learner, int deviceNum)
    net = DistImageNet_VGG19Net(model, StructureLearner.SGDLearner(0.0, 0.0, 0.0), deviceNum)

    net.BuildCG(224, 224, 3, args.batch_size)

    bar = Bar('prediction', max=len(val_loader))
    
    batch_time = AverageMeter()
    data_time = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    output = torch.zeros([args.batch_size, 1000], dtype=torch.float)
    p_output = IntPtr.op_Explicit(Int64(output.data_ptr()))

    net.SetPredictMode()
    net.Init()
    
    cMatrix = [0] * 1000 * 1000

    end = time.time()
    for i, (input, target) in enumerate(val_loader):

        p_input = IntPtr.op_Explicit(Int64(input.data_ptr()))
        
        #p_target = IntPtr.op_Explicit(Int64(target.data_ptr()))
        data_time.update(time.time() - end)

        net.Predict(p_input, p_output, args.batch_size)

        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        
        acc_matrix(output, target, cMatrix)

        top1.update(acc1[0], input.size(0))
        top5.update(acc5[0], input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        bar.suffix  = '({batch}/{size}) Total: {total:} | ETA: {eta:} | Batch Time {batch_time.val:.3f} ({batch_time.avg:.3f}) | Data Time {data_time.val:.3f} ({data_time.avg:.3f}) | Acc@1 {top1.val:.3f} ({top1.avg:.3f}) | Acc@5 {top5.val:.3f} ({top5.avg:.3f}) '.format(
                    batch = i + 1,
                    size = len(val_loader),
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    batch_time=batch_time,
                    data_time=data_time,
                    top1=top1,
                    top5=top5
                    )
        bar.next()

    print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'.format(top1=top1, top5=top5))

    bar.finish()
    net.Complete()
    
    for i in range(0, 1000):
        f = np.argmax(cMatrix[i * 1000 : (i + 1) * 1000])
        print( i , "->", f ,' : ', cMatrix[i * 1000 + f] / 50.0)


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def acc_matrix(output, target, cMatrix):
    batch_size = target.size(0)
    _, pred = output.topk(1, 1, True, True)
    top1_pred = pred.t().flatten()
    for i in range(0, batch_size):
        t = top1_pred[i].item()
        s = target[i].item()
        cMatrix[s * 1000 + t] += 1

def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


if __name__ == '__main__':
    main()
