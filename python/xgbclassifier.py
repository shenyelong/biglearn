## xgboost examples.
import xgboost as xgb
import sys
import io

data_path = sys.argv[1]

train = xgb.DMatrix(data_path+'/train.index')
dev = xgb.DMatrix(data_path+'/dev.index')
test = xgb.DMatrix(data_path+'/test.index')


## multiclass classification.
#param = {'max_depth': 3, 'eta': 0.3, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } #'binary:logistic'}
#param['nthread'] = 4
#param['eval_metric'] = 'merror' #'auc'

## binary classification.
param = {'max_depth': 1, 'eta': 0.5, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'}
param['nthread'] = 4
param['eval_metric'] = 'auc'


evallist = [(test, 'test'), (train, 'train'), (dev, 'dev')]

num_round = 10000
bst = xgb.train(param, train, num_round, evallist, early_stopping_rounds=300)


## dota2 dataset.
#max_depth:10, eta: 1; silent : 1; num_round = 20; test-auc:0.5939 train-auc:0.700034      dev-auc:0.588804
#max_depth:8, eta: 1; silent : 1; num_round = 50; test-auc:0.602401       train-auc:0.727771      dev-auc:0.593688
#max_depth:5, eta: 1; silent : 1; num_round = 100; test-auc:0.615696       train-auc:0.672109      dev-auc:0.603343
#max_depth:5, eta: 1; silent : 1; num_round = 100; test-auc:0.609308       train-auc:0.677273      dev-auc:0.611233
#max_depth:3, eta: 1; silent : 1; num_round = 100; test-auc:0.620885       train-auc:0.653962      dev-auc:0.616118
#max_depth:2, eta: 1; silent : 1; num_round = 100; test-auc:0.627093       train-auc:0.648352      dev-auc:0.621665
#max_depth:1, eta: 1; silent : 1; num_round = 100; test-auc:0.62494        train-auc:0.636589      dev-auc:0.625488
#
#max_depth:4, eta: 0.5; silent : 1; num_round = 100; test-auc:0.616831       train-auc:0.674835      dev-auc:0.616084
#max_depth:3, eta: 0.5; silent : 1; num_round = 100; test-auc:0.622031       train-auc:0.658993      dev-auc:0.622305
#max_depth:2, eta: 0.5; silent : 1; num_round = 100; test-auc:0.624343       train-auc:0.644647      dev-auc:0.62496
#
#
#
#max_depth:3, eta: 0.5; silent : 1; num_round = 100; min_child_weight : 5; test-auc:0.623832       train-auc:0.65831       dev-auc:0.622612
#max_depth:2, eta: 0.5; silent : 1; num_round = 100; min_child_weight : 5; test-auc:0.62744        train-auc:0.653087      dev-auc:0.62589
#max_depth:1, eta: 0.5; silent : 1; num_round = 100; min_child_weight : 5; test-auc:0.623275       train-auc:0.631979      dev-auc:0.62456
#
#

# poker
# 
# {'max_depth': 4, 'eta': 1, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 }  [125]   test-merror:0.061665    train-merror:0.008092   dev-merror:0.061845
# {'max_depth': 6, 'eta': 1, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 }. [74]    test-merror:0.066638    train-merror:5e-05      dev-merror:0.066776
# {'max_depth': 3, 'eta': 0.5, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [170]   test-merror:0.074481    train-merror:0.062404   dev-merror:0.076433
# {'max_depth': 3, 'eta': 1, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [103]   test-merror:0.070527    train-merror:0.052127   dev-merror:0.069036
# {'max_depth': 4, 'eta': 0.5, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [86]    test-merror:0.074304    train-merror:0.056446   dev-merror:0.077255
# {'max_depth': 4, 'eta': 0.8, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [670]   test-merror:0.044874    train-merror:0  dev-merror:0.043353
# {'max_depth': 6, 'eta': 0.8, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [230]   test-merror:0.05534     train-merror:0  dev-merror:0.056092
# {'max_depth': 6, 'eta': 0.8, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [539]   test-merror:0.054996    train-merror:0  dev-merror:0.054859
# {'max_depth': 3, 'eta': 0.8, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [999]   test-merror:0.04646     train-merror:0  dev-merror:0.046435
# {'max_depth': 5, 'eta': 0.8, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [290]   test-merror:0.041511    train-merror:0  dev-merror:0.036367
# {'max_depth': 5, 'eta': 1, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [251]   test-merror:0.045159    train-merror:0  dev-merror:0.040477
# {'max_depth': 5, 'eta': 0.5, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [531]   test-merror:0.043652    train-merror:0  dev-merror:0.039038
# {'max_depth': 5, 'eta': 0.3, 'silent': 1, 'min_child_weight' : 1, 'objective': 'multi:softmax', 'num_class' : 10 } [999]   test-merror:0.045991    train-merror:0  dev-merror:0.045613



#income
## {'max_depth': 5, 'eta': 0.3, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [101]   test-auc:0.876363       train-auc:0.896404      dev-auc:0.882113
## {'max_depth': 7, 'eta': 0.3, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [40]    test-auc:0.877332       train-auc:0.897513      dev-auc:0.88125 
## {'max_depth': 3, 'eta': 0.3, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [231]   test-auc:0.878256       train-auc:0.892198      dev-auc:0.883076 
## {'max_depth': 1, 'eta': 0.3, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [640]   test-auc:0.878632       train-auc:0.880856      dev-auc:0.878631
## {'max_depth': 2, 'eta': 0.3, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [635]   test-auc:0.878781       train-auc:0.889338      dev-auc:0.882003
## {'max_depth': 3, 'eta': 1, 'silent': 1, 'min_child_weight' : 10, 'objective': 'binary:logistic'}  [79]    test-auc:0.875698       train-auc:0.887101      dev-auc:0.879878
## {'max_depth': 5, 'eta': 0.5, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [32]    test-auc:0.877814       train-auc:0.891305      dev-auc:0.88215


# income real
# {'max_depth': 5, 'eta': 0.5, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [37]    test-auc:0.894887       train-auc:0.913974      dev-auc:0.895663
# {'max_depth': 3, 'eta': 0.5, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [58]    test-auc:0.896435       train-auc:0.907772      dev-auc:0.894491
# {'max_depth': 1, 'eta': 0.5, 'silent': 1, 'min_child_weight' : 1, 'objective': 'binary:logistic'} [286]   test-auc:0.89644        train-auc:0.900411      dev-auc:0.89216
#
#
