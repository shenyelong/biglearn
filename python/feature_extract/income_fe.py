import pandas as pd
import numpy as np
## dump to svmlight format dataset.
from sklearn.datasets import dump_svmlight_file
#import category_encoders as ce
#from sklearn.preprocessing import LabelEncoder
import random
import sys
import io
import os
from sklearn.utils import shuffle

def extract_data_frame(mf):
	fn = pd.read_csv(mf, header=None)
	#fn.drop(1, axis=1, inplace=True)
	#fn.drop(2, axis=1, inplace=True)
	#fn.drop(3, axis=1, inplace=True)
	return fn

path = sys.argv[1]

train_dev = shuffle(extract_data_frame(path + '/adult.data'))
test = extract_data_frame(path + '/adult.test')
all = pd.concat([train_dev, test])


all_column = all.shape[1]


def extract_fea(x, y, fea_dim, s, e, mf):
	col = x.shape[1]
	
	fout = open(mf,'w')
	for i in range(s, e):
		row_idx = i

		l = y.iloc[row_idx]
		
		mline = str(l)

		cur_dim = 0

		for c in range(0, col):
			s = x.iloc[row_idx, c] #4 + c][i]
			
			mline = mline + ' ' + str(cur_dim + s) + ':1'
			cur_dim = cur_dim + fea_dim[c]
		fout.write(mline+'\n')
	fout.close()


y = all[all_column - 1].astype('category')
y = y.cat.rename_categories([0,1])

avail_cols = [1, 3, 5, 6, 7, 8, 9, 13]

x_dim = [ 0 ] * len(avail_cols)
col_idx = 0

x = all[avail_cols]
for i in avail_cols:
	cn = i
	x[cn] = x[cn].astype('category')
	
	x_dim[col_idx] = x[cn].cat.categories.size
	x[cn] = x[cn].cat.rename_categories(list(range(0, x_dim[col_idx])))
	col_idx += 1


pos1 = round(train_dev.shape[0] * 0.8)
pos2 = train_dev.shape[0]

extract_fea(x, y, x_dim, 0, pos1, path+'/train.index')
extract_fea(x, y, x_dim, pos1, pos2, path+'/dev.index')
extract_fea(x, y, x_dim, pos2, all.shape[0], path+'/test.index')


