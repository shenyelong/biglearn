import pandas as pd
import numpy as np
## dump to svmlight format dataset.
from sklearn.datasets import dump_svmlight_file
#import category_encoders as ce
#from sklearn.preprocessing import LabelEncoder

import sys
import io
import os

def extract_data_frame(mf):
	fn = pd.read_csv(mf, header=None)
	fn.drop(1, axis=1, inplace=True)
	fn.drop(2, axis=1, inplace=True)
	fn.drop(3, axis=1, inplace=True)
	return fn

def extract_fea(mdata, mf):
	y = mdata[0]
	x = mdata[mdata.columns[1:]]
	row = x.shape[0]
	col = x.shape[1]
	fout = open(mf,'w')
	for i in range(0, row):
		l = y.iloc[i]
		if(l < 0): 
			l = 0
		mline = str(l)
		for c in range(0, col):
			s = x.iloc[i, c] #4 + c][i]
			if( s > 0 ):
				mline = mline + ' ' + str(2 * c + 0)+':1'
			elif( s < 0 ):
				mline = mline + ' ' + str(2 * c + 1)+':1'
		fout.write(mline+'\n')
	fout.close()

path = sys.argv[1]

train_all = extract_data_frame(path + '/dota2Train.csv')
msk = np.random.rand(train_all.shape[0]) < 0.8
train = train_all[msk]
dev = train_all[~msk]
test = extract_data_frame(path + '/dota2Test.csv')



extract_fea(train, path+'/train.index')
extract_fea(dev, path+'/dev.index')
extract_fea(test, path+'/test.index')


#dump_svmlight_file(X, y, 'tmp.dat', zero_based=True, multilabel=False)




