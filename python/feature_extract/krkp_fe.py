import pandas as pd
import numpy as np
## dump to svmlight format dataset.
from sklearn.datasets import dump_svmlight_file
#import category_encoders as ce
#from sklearn.preprocessing import LabelEncoder
import random
import sys
import io
import os

def extract_data_frame(mf):
	fn = pd.read_csv(mf, header=None)
	#fn.drop(1, axis=1, inplace=True)
	#fn.drop(2, axis=1, inplace=True)
	#fn.drop(3, axis=1, inplace=True)
	return fn


def extract_fea(x, y, fea_dim, idx, s, e, mf):
	col = x.shape[1]
	
	fout = open(mf,'w')
	for i in range(s, e):
		row_idx = idx[i]

		l = y.iloc[row_idx]
		
		mline = str(l)

		cur_dim = 0

		for c in range(0, col):
			s = x.iloc[row_idx, c] #4 + c][i]
			
			mline = mline + ' ' + str(cur_dim + s) + ':1'
			cur_dim = cur_dim + fea_dim[c]
		fout.write(mline+'\n')
	fout.close()

path = sys.argv[1]

all = extract_data_frame(path + '/kr-vs-kp.data')

all_column = all.shape[1]


y = all[all_column - 1].astype('category')
y = y.cat.rename_categories([0,1])

x_dim = [0] * (all_column - 1)
x = all[all.columns[0: (all_column - 1)]]
for i in range(0, all_column - 1):
	cn = x.columns[i]
	x[cn] = x[cn].astype('category')
	x_dim[i] = x[cn].cat.categories.size
	x[cn] = x[cn].cat.rename_categories(list(range(0, x_dim[i])))


row = y.shape[0]
all_idx = list(range(0, row))
random.shuffle(all_idx)
pos1 = round(row * 0.8)
pos2 = round(row * 0.9)

extract_fea(x, y, x_dim, all_idx, 0, pos1, path+'/train.index')
extract_fea(x, y, x_dim, all_idx, pos1, pos2, path+'/dev.index')
extract_fea(x, y, x_dim, all_idx, pos2, row, path+'/test.index')





