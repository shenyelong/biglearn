Storm : Neural Networks for Deep Learning (The strom project is only be used for research purpose). 

Enviroument Setup (Linux):

Step 1 : Install Anaconda3; 

Step 2 : Install Mono

	conda install -c conda-forge mono 

Step 3 : Install pythonnet (if you want to use biglearn package in python)

	conda install -c pythonnet pythonnet


Storm Compile (Linux):

Step 1 : Compile Cuda and SSE library. 

	sh blib.sh

Step 2 : Compile Biglearn main library.

	sh bnet.sh 


Storm Call from Python (Linux):

import clr as net_clr
sys.path.append(args.lib) 
net_clr.AddReference('BigLearn')
net_clr.AddReference('BigLearn.DeepNet')
import BigLearn




